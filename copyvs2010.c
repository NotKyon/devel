#include <errno.h>
#include <stdio.h>
#include <dirent.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#if !defined(_MSC_VER)
typedef int errno_t;
#endif

#if !defined(__STDC_WANT_SECURE_LIB__)
errno_t strcpy_s(char *dst, size_t dstn, const char *src)
{
	if (!dst || !dstn || !src)
		return EINVAL;

	strncpy(dst, src, dstn - 1);
	dst[dstn - 1] = '\0';

	return 0;
}
errno_t strncpy_s(char *dst, size_t dstn, const char *src, size_t srccnt)
{
	return strcpy_s(dst, srccnt < dstn ? srccnt : dstn, src);
}
errno_t strcat_s(char *dst, size_t dstn, const char *src)
{
	char *p;

	if (!dst || !dstn || !src)
		return EINVAL;

	p = strchr(dst, '\0');

	if ((size_t)(p - dst) > dstn)
		return EINVAL;

	if (!(dstn - (p - dst)))
		return 0;

	strncpy(p, src, dstn - (p - dst));
	p[dstn - (p - dst) - 1] = '\0';

	return 0;
}
errno_t strerror_s(char *dst, size_t dstn, int errnum)
{
	return strcpy_s(dst, dstn, strerror(errnum));
}
#endif

int ErrorReport(const char *file)
{
	errno_t e;
	char errbuf[256];

	e = errno;
	if (!e)
		return 0;

	errbuf[0] = '\0';

	if (strerror_s(errbuf, sizeof(errbuf), e) != 0)
		strcpy_s(errbuf, sizeof(errbuf), "[strerror_s() failed]");

	fprintf(stderr, "ERROR: %s: %s (%i)\n", file, errbuf, e);
	fflush(stderr);

	return e;
}

int CopyVCX(const char *basname)
{
	const char *exts[] = { ".vcxproj", ".vcxproj.filters", ".vcxproj.user" };
	size_t i, n, errcnt;
	FILE *fs,*fd;
	char newname[256];
	char srcname[256], dstname[256];
	char buf[4096];

	strcpy_s(newname, sizeof(newname), basname);
	strcat_s(newname, sizeof(newname), "-vs2010");

	errcnt = 0;

	for(i=0; i<sizeof(exts)/sizeof(exts[0]); i++)
	{
		strcpy_s(srcname, sizeof(srcname), basname);
		strcat_s(srcname, sizeof(srcname), exts[i]);

		strcpy_s(dstname, sizeof(dstname), newname);
		strcat_s(dstname, sizeof(dstname), exts[i]);

		fs = fopen(srcname, "rb");
		if (!fs)
		{
			ErrorReport(srcname);
			errcnt++;
			continue;
		}

		fd = fopen(dstname, "wb");
		if (!fd)
		{
			ErrorReport(dstname);
			errcnt++;
			fclose(fs);
			continue;
		}

		while(1)
		{
			n = fread(buf, 1, sizeof(buf), fs);
			if (n)
				fwrite(buf, n, 1, fd);

			if (feof(fs) || ferror(fs))
				break;
		}

		fclose(fd);

		if (ferror(fs))
		{
			ErrorReport(srcname);
			errcnt++;
			remove(dstname);
		}

		fclose(fs);
	}

	if (errcnt > 0)
		return -1; /*errno should be set*/

	return 0;
}

int ParseString(const char *src, char *dstbase, size_t dstbasen, char *dstpath, size_t dstpathn)
{
	char *p;

	if (!src || !dstbase || !dstbasen || !dstpath || !dstpathn)
		return EINVAL;

	p = strchr(src, ':');
	if (!p)
		return EINVAL;

	strncpy_s(dstbase, dstbasen, src, p - src + 1);

	p++;

	strcpy_s(dstpath, dstpathn, p);
	p = strchr(dstpath, '\r');
	if (!p)
		p = strchr(dstpath, '\n');

	if (p)
		*p = '\0';

	return 0;
}
int DoRound(const char *base, const char *path)
{
	char dir[256];
	int r;

	if (!getcwd(dir, sizeof(dir)))
		return ErrorReport("getcwd(dir)");

	if (chdir(path)!=0)
		return ErrorReport(path);

	r = CopyVCX(base);

	if (chdir(dir)!=0)
		return ErrorReport(dir);

	return r;
}

int RunFile(const char *filename)
{
	FILE *f;
	char line[512], base[256], path[256];
	int r;

	f = fopen(filename, "r");
	if (!f)
		return ErrorReport(filename);

	while(fgets(line, sizeof(line), f) != (char *)0)
	{
		if (ParseString(line, base, sizeof(base), path, sizeof(path)) != 0)
		{
			if (feof(f))
				break;

			continue;
		}

		r = DoRound(base, path);

		if (feof(f))
			break;
	}

	fclose(f);

	return r;
}

int main(int argc, char **argv)
{
	static const int mode = 0; /*0=any; 1=all*/
	int any, all, r;
	int i;

	if (!argc)
	{
		RunFile("options.txt");
		return EXIT_FAILURE;
	}

	any = 0;
	all = 1;
	for(i=1; i<argc; i++)
	{
		r    = !RunFile(argv[i]);
		any |= r;
		all &= r;
	}

	if (!any && all)
		any = 1;

	return (mode==0 ? any : all)!=0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
