#include "string.h"

#include "memory.h"
#include "report.h"

#include <stdlib.h>
#include <string.h>

#define STR_CHUNK 64

void InitString(string_t *str) {
	ASSERT(str != (string_t *)0);

	str->capacity = 0;
	str->length = 0;
	str->p = (char *)0;
}
void FiniString(string_t *str) {
	ASSERT(str != (string_t *)0);

	str->capacity = 0;
	str->length = 0;
	str->p = (char *)Memory((void *)str->p, 0);
}

void ResizeString(string_t *str, size_t len) {
	ASSERT(str != (string_t *)0);

	if (len + 1 > str->capacity) {
		str->capacity  = len + 1;
		str->capacity -= str->capacity%STR_CHUNK;
		str->capacity += STR_CHUNK;

		str->p = (char *)Memory((void *)str->p, str->capacity);
	}

	str->length = len;
}

void PushChar(string_t *str, char ch) {
	size_t len;

	ASSERT(str != (string_t *)0);

	len = str->length;
	ResizeString(str, len + 1);

	str->p[len + 0] = ch;
	str->p[len + 1] = '\0';
}
void PushStringN(string_t *str, const char *s, size_t n) {
	size_t l1, l2;

	ASSERT(str != (string_t *)0);
	ASSERT(s != (const char *)0);

	l1 = str->length;
	l2 = n ? n : strlen(s);

	ResizeString(str, l1 + l2);

	memcpy(&str->p[l1], (const void *)s, l2);
	str->p[l1 + l2] = '\0';
}
void PushString(string_t *str, const char *s) {
	ASSERT(str != (string_t *)0);
	ASSERT(s != (const char *)0);

	PushStringN(str, s, 0);
}

size_t StringCapacity(const string_t *str) {
	ASSERT(str != (const string_t *)0);

	return str->capacity;
}
size_t StringLength(const string_t *str) {
	ASSERT(str != (const string_t *)0);

	return str->length;
}
const char *StringBuffer(const string_t *str) {
	ASSERT(str != (const string_t *)0);

	return str->p;
}
char *StringData(string_t *str) {
	ASSERT(str != (string_t *)0);

	return str->p;
}

void SetStringN(string_t *str, const char *s, size_t n) {
	size_t len;

	ASSERT(str != (string_t *)0);
	ASSERT(s != (const char *)0);

	len = n ? n : strlen(s);
	ResizeString(str, len);

	memcpy(&str->p[0], (const void *)s, len);
	str->p[len] = 0;
}
void SetString(string_t *str, const char *s) {
	ASSERT(str != (string_t *)0);
	ASSERT(s != (const char *)0);

	return SetStringN(str, s, 0);
}
