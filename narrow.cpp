#include <stdint.h>
#include <stdlib.h>

int main() {
	uint16_t a = 3, b = 2;

	uint16_t arr[ 2 ] = {
		( uint16_t )( a - b ),
		( uint16_t )( b - a )
	};

	( void )arr; //unused

	return EXIT_SUCCESS;
}
