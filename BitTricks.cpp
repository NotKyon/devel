struct false_type { static const bool value = false; };
struct true_type { static const bool value = true; };
template< typename T, T V >
struct integral_constant { static const T value = V; };
template<> struct integral_constant< bool, false >: public false_type {};
template<> struct integral_constant< bool, true >: public true_type {};

typedef unsigned char u8;
typedef signed char s8;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned int u32;
typedef signed int s32;
typedef unsigned long long int u64;
typedef signed long long int s64;

namespace detail
{

	template< bool HiSigned >
	struct high_type
	{
		typedef u64 type;
	};
	template<>
	struct high_type< true >
	{
		typedef s64 type;
	};

}

template< bool Signed >
class int128
{
public:
	inline int128( u64 low = 0, u64 high = 0 ): mLo( low ), mHi( high )
	{
	}
	inline int128( const int128 &v ): mLo( v.mLo ), mHi( v.mHi )
	{
	}
	inline int128( const int128< Signed ^ true > &v ): mLo( v.mLo ),
	mHi( v.mHi )
	{
	}

	template< bool OtherSigned >
	inline int128 &operator=( const int128< OtherSigned > &x )
	{
		mLo = x.mLo;
		mHi = x.mHi;
	}

	inline int128 operator+( const int128 &x ) const
	{
		const u64 t = mLo + x.mLo;
		const u64 c = ( ( mLo & x.mLo ) | ( ( mLo | x.mLo ) & ~t ) ) >> 63;
		return int128( t, mHi + x.mHi + c );
	}
	inline int128 operator-( const int128 &x ) const
	{
		const u64 t = mLo - x.mLo;
		const u64 b = ( ( ~mLo & x.mLo ) | ( ~( mLo ^ x.mLo ) & t ) ) >> 63;
		return int128( t, mHi - x.mHi - b );
	}

	inline int128 operator&( const int128 &x ) const
	{
		return int128( mLo & x.mLo, mHi & x.mHi );
	}
	inline int128 operator|( const int128 &x ) const
	{
		return int128( mLo | x.mLo, mHi | x.mHi );
	}
	inline int128 operator^( const int128 &x ) const
	{
		return int128( mLo ^ x.mLo, mHi ^ x.mHi );
	}

	inline int128 operator<<( unsigned int n ) const
	{
		return
			int128(
				mLo << n,
				mHi << n | ( mLo >> ( 64 - n ) ) | mLo << ( n - 64 )
			);
	}
	inline int128 operator>>( unsigned int n ) const
	{
		if( Signed )
		{
			return
				int128(
					( n < 64 )
						? mLo >> n | mHi << ( 64 - n )
						: mHi >> ( n - 64 ),
					mHi >> n
				);
		}

		return
			int128(
				mLo >> n | mHi << ( 64 - n ) | ( ( u64 )mHi ) >> ( n - 64 ),
				( ( u64 )mHi ) >> n
			);
	}

	inline bool operator!() const
	{
		return !( mHi | mLo );
	}

	inline bool operator<( const int128 &x ) const
	{
		return mHi < x.mHi || ( mHi == x.mHi && mLo < x.mLo );
	}
	inline bool operator==( const int128 &x ) const
	{
		return mHi == x.mHi && mLo == x.mLo;
	}

	inline bool operator!=( const int128 &x ) const
	{
		return !( *this == x );
	}
	inline bool operator<=( const int128 &x ) const
	{
		return !( x < *this );
	}
	inline bool operator>=( const int128 &x ) const
	{
		return !( *this < x );
	}
	inline bool operator>( const int128 &x ) const
	{
		return x < *this;
	}

private:
	u64 mLo;
	typename detail::high_type< Signed >::type mHi;
};

typedef int128< false > u128;
typedef int128< true > s128;

template< int Bytes > struct sized_type {};
template<>
struct sized_type< 1 >
{
	typedef false_type has_real;
	typedef u8 uint;
	typedef s8 sint;
	static const u8 kBitMask = 0xFF;
};
template<>
struct sized_type< 2 >
{
	typedef false_type has_real;
	typedef u16 uint;
	typedef s16 sint;

	static const u16 kBitMask = 0xFFFF;
};
template<>
struct sized_type< 4 >
{
	typedef true_type has_real;
	typedef u32 uint;
	typedef s32 sint;
	typedef float real;

	static const u32 kBitMask = 0xFFFFFFFF;
};
template<>
struct sized_type< 8 >
{
	typedef true_type has_real;
	typedef u64 uint;
	typedef s64 sint;
	typedef double real;

	static const u64 kBitMask = 0xFFFFFFFFFFFFFFFF;
};
template<>
struct sized_type< 16 >
{
	typedef false_type has_real;
	typedef u128 uint;
	typedef s128 sint;
};

typedef sized_type< sizeof( void * ) >::uint uptr;
typedef sized_type< sizeof( void * ) >::sint sptr;

typedef sized_type< sizeof( int ) >::uint uint;
typedef sized_type< sizeof( int ) >::sint sint;

template< uint Bytes, u8 Value >
struct byte_replicator
{
};

template< u8 Value >
struct byte_replicator< 1, Value >
{
	typedef u8 value_type;
	static const value_type even_value = Value;
	static const value_type odd_value = Value;
	static const value_type value = Value;
};
template< u16 Value >
struct byte_replicator< 2, Value >
{
	typedef u16 value_type;
	static const value_type even_value = ( u16 )Value;
	static const value_type odd_value = ( ( u16 )Value ) << 8;
	static const value_type value = even_value | odd_value;
};
template< u32 Value >
struct byte_replicator< 4, Value >
{
	typedef u32 value_type;
	static const value_type even_value =
		( ( ( u32 )byte_replicator< 2, Value >::even_value ) << 16 ) |
		( ( ( u32 )byte_replicator< 2, Value >::even_value ) << 0  ) ;
	static const value_type odd_value =
		( ( ( u32 )byte_replicator< 2, Value >::odd_value ) << 16 ) |
		( ( ( u32 )byte_replicator< 2, Value >::odd_value ) << 0  ) ;
	static const value_type value = even_value | odd_value;
};
template< u64 Value >
struct byte_replicator< 8, Value >
{
	typedef u64 value_type;
	static const value_type even_value =
		( ( ( u64 )byte_replicator< 4, Value >::even_value ) << 32 ) |
		( ( ( u64 )byte_replicator< 4, Value >::even_value ) << 0  ) ;
	static const value_type odd_value =
		( ( ( u64 )byte_replicator< 4, Value >::odd_value ) << 32 ) |
		( ( ( u64 )byte_replicator< 4, Value >::odd_value ) << 0  ) ;
};

template< typename T > struct is_integral: public false_type {};
template<> struct is_integral< u8 >: public true_type {};
template<> struct is_integral< u16 >: public true_type {};
template<> struct is_integral< u32 >: public true_type {};
template<> struct is_integral< u64 >: public true_type {};
template<> struct is_integral< s8 >: public true_type {};
template<> struct is_integral< s16 >: public true_type {};
template<> struct is_integral< s32 >: public true_type {};
template<> struct is_integral< s64 >: public true_type {};
template<> struct is_integral< signed long >: public true_type {};
template<> struct is_integral< unsigned long >: public true_type {};

template< typename T >
struct is_signed: public integral_constant< bool, T( -1 ) < T( 0 ) >
{
};
template< typename T >
struct is_unsigned: public integral_constant< bool, T( 0 ) < T( -1 ) >
{
};

template< typename T > struct add_const { typedef const T type; };
template< typename T > struct add_const< const T > { typedef const T type; };

template< typename T > struct add_volatile { typedef volatile T type; };
template< typename T >
struct add_volatile< volatile T >
{
	typedef volatile T type;
};

template< typename T >
struct add_cv
{
	typedef typename add_const< typename add_volatile< T >::type >::type type;
};

template< typename Dst, typename Src >
struct copy_cv
{
	typedef Dst type;
};
template< typename Dst, typename Src >
struct copy_cv< Dst, const Src >
{
	typedef typename add_const< Dst >::type type;
};
template< typename Dst, typename Src >
struct copy_cv< Dst, volatile Src >
{
	typedef typename add_volatile< Dst >::type type;
};
template< typename Dst, typename Src >
struct copy_cv< Dst, const volatile Src >
{
	typedef typename add_cv< Dst >::type type;
};

template< typename T >
struct make_signed
{
	static_assert( is_integral< T >::value, "Integral type required" );

	typedef
		typename copy_cv<
			typename sized_type< sizeof( T ) >::sint,
			T
		>::type type;
};
template< typename T >
struct make_unsigned
{
	static_assert( is_integral< T >::value, "Integral type required" );

	typedef
		typename copy_cv<
			typename sized_type< sizeof( T ) >::uint,
			T
		>::type type;
};

// Unsigned bit-shift right
template< typename Int >
inline Int bitShiftRightU( Int x, Int y )
{
	typedef typename make_unsigned< Int >::type Uint;

	return
		static_cast< Int >(
			static_cast< Uint >( x ) >> y
		);
}
// Signed bit-shift right
template< typename Int >
inline Int bitShiftRightS( Int x, Int y )
{
	typedef typename make_signed< Int >::type Sint;

	return
		static_cast< Int >(
			static_cast< Sint >( x ) >> y
		);
}

// Put a zero bit between each of the lower bits of the given value
template< typename Int >
inline Int bitExpand( Int x )
{
	static_assert( is_integral< Int >::value, "Must use integral types" );

	Int r = 0;

	for( uint i = 0; i < ( sizeof( x )*8 )/2; ++i )
	{
		r |= ( x & ( 1 << i ) ) << i;
	}

	return r;
}
// Take each other bit of a value and merge into one value
template< typename Int >
inline Int bitMerge( Int x )
{
	static_assert( is_integral< Int >::value, "Must use integral types" );

	Int r = 0;

	for( uint i = 0; i < ( sizeof( x )*8 )/2; ++i )
	{
		r |= ( x & ( 1 << ( i*2 ) ) ) >> i;
	}

	return r;
}

// Specialization of bitExpand() for u32
inline u32 bitExpand( u32 x )
{
	// http://fgiesen.wordpress.com/2009/12/13/decoding-morton-codes/
	x &= 0xFFFF;

	x = ( x ^ ( x << 8 ) ) & 0x00FF00FF;
	x = ( x ^ ( x << 4 ) ) & 0x0F0F0F0F;
	x = ( x ^ ( x << 2 ) ) & 0x33333333;
	x = ( x ^ ( x << 1 ) ) & 0x55555555;

	return x;
}
// Specialization of bitMerge() for u32
inline u32 bitMerge( u32 x )
{
	// http://fgiesen.wordpress.com/2009/12/13/decoding-morton-codes/
	x &= 0x55555555;

	x = ( x ^ ( x >> 1 ) ) & 0x33333333;
	x = ( x ^ ( x >> 2 ) ) & 0x0F0F0F0F;
	x = ( x ^ ( x >> 4 ) ) & 0x00FF00FF;
	x = ( x ^ ( x >> 8 ) ) & 0x0000FFFF;

	return x;
}

// Turn off the right-most set bit
// e.g., 01011000 -> 01010000
template< typename Int >
inline Int bitRemoveLowestSet( Int x )
{
	return x & ( x - 1 );
}
// Determine whether a number is a power of two
template< typename Int >
inline bool bitIsPowerOfTwo( Int x )
{
	return bitRemoveLowestSet( x ) == 0;
}
// Isolate the right-most set bit
// e.g., 01011000 -> 00001000
template< typename Int >
inline Int bitIsolateLowestSet( Int x )
{
	return x & ( -x );
}
// Isolate the right-most clear bit
// e.g., 10100111 -> 00001000
template< typename Int >
inline Int bitIsolateLowestClear( Int x )
{
	return -x & ( x + 1 );
}
// Create a mask of the trailing clear bits
// e.g., 01011000 -> 00000111
template< typename Int >
inline Int bitIdentifyLowestClears( Int x )
{
	return -x & ( x - 1 );
}
// Create a mask that identifies the least significant set bit and the trailing
// clear bits
// e.g., 01011000 -> 00001111
template< typename Int >
inline Int bitIdentifyLowestSetAndClears( Int x )
{
	return x ^ ( x - 1 );
}
// Propagate the lowest set bit to the lower clear bits
template< typename Int >
inline Int bitPropagateLowestSet( Int x )
{
	return x | ( x - 1 );
}
// Find the absolute value of an integer
template< typename Int >
inline Int bitAbs( Int x )
{
	const Int y = bitShiftRightS( x, sizeof( x )*8 - 1 );
	return ( x ^ y ) - y;
}
// Find the sign of an integer
template< typename Int >
inline Int bitSign( Int x )
{
	static const Int s = sizeof( Int )*8 - 1;
	return bitShiftRightS( x, s ) | bitShiftRightU( -x, s );
}
// Transfer the sign of src into dst
template< typename Int >
inline Int bitCopySign( Int dst, Int src )
{
	const Int t = bitShiftRightS( src, sizeof( Int )*8 - 1 );
	return ( bitAbs( dst ) + t ) ^ t;
}
// Rotate a field of bits left
template< typename Int >
inline Int bitRotateLeft( Int x, Int y )
{
	return ( x << y ) | bitShiftRightU( x, sizeof( x )*8 - y );
}
// Rotate a field of bits right
template< typename Int >
inline Int bitRotateRight( Int x, Int y )
{
	return bitShiftRightU( x, y ) | ( x << ( sizeof( x )*8 - y ) );
}
// Count the number of set bits
template< typename Int >
inline Int bitCount( Int x )
{
	Int r = x;

	for( int i = 0; i < sizeof( x )*8; ++i )
	{
		r -= x >> ( 1 << i );
	}

	return r;
}
// Specialization of bitCount() for 32-bit integers
inline u32 bitCount( u32 x )
{
	x = x - ( ( x >> 1 ) & 0x55555555 );
	x = ( x & 0x33333333 ) + ( ( x >> 2 ) & 0x33333333 );
	x = ( x + ( x >> 4 ) ) & 0x0F0F0F0F;
	x = x + ( x >> 8 );
	x = x + ( x >> 16 );
	return x & 0x0000003F;
}
// Compute the parity of an integer (true for odd, false for even)
template< typename Int >
inline Int bitParity( Int x )
{
	x = x ^ ( x >> 1 );
	x = x ^ ( x >> 2 );
	x = x ^ ( x >> 4 );

	if( sizeof( x ) > 1 )
	{
		x = x ^ ( x >> 8 );
		
		if( sizeof( x ) > 2 )
		{
			x = x ^ ( x >> 16 );

			if( sizeof( x ) > 4 )
			{
				x = x ^ ( x >> 32 );

				int i = 8;
				while( sizeof( x ) > i )
				{
					x = x ^ ( x >> ( i*8 ) );
					i = i*2;
				}
			}
		}
	}

	return x;
}

void LinearToQuadRow( uint linear, uint &qx, uint &qy )
{
	qx = bitMerge( linear >> 0 );
	qy = bitMerge( linear >> 1 );
}
void LinearToQuadColumn( uint linear, uint &qx, uint &qy )
{
	qx = bitMerge( linear >> 1 );
	qy = bitMerge( linear >> 0 );
}
uint QuadRowToLinear( uint qx, uint qy )
{
	return bitExpand( qx ) | ( bitExpand( qy ) << 1 );
}
uint QuadColumnToLinear( uint qx, uint qy )
{
	return bitExpand( qy ) | ( bitExpand( qx ) << 1 );
}

#include <stdio.h>
#include <stdlib.h>

/*
	0 -> 0 :: 0000 -> 0000
	1 -> 1 :: 0001 -> 0001
	2 -> 0 :: 0010 -> 0000
	3 -> 2 :: 0011 -> 0010
	4 -> 0 :: 0100 -> 0000
	5 -> 1 :: 0101 -> 0001
	6 -> 0 :: 0110 -> 0000
	7 -> 3 :: 0111 -> 0011
	8 -> 0 :: 1000 -> 0000
	9 -> 1 :: 1001 -> 0001
	A -> 0 :: 1010 -> 0000
	B -> 2 :: 1011 -> 0010
	C -> 0 :: 1100 -> 0000
	D -> 1 :: 1101 -> 0001
	E -> 0 :: 1110 -> 0000
	F -> 4 :: 1111 -> 0100

	.                    .
	 +------------------+
	4|                x |
	3|        x         |
	2|    x       x     |
	1|  x   x   x   x   |
	0| x x x x x x x x  |
	 +------------------+
	.  0123456789ABCDEF  .

	Horizontal axis is x
	Vertical axis is result of f(x)
	"A" = 10, "B" = 11, "C" = 12, "D" = 13, "E" = 14, and "F" = 15

	The formula for this is:

		f(x; n) = \sum{^n_i=1}{\floor{(x mod(2^i))/(2^i - 1)}}

	Where 'n' is the highest band to be calculated.

	One implementation in C:

		typedef unsigned int uint;
		uint f( uint x ) {
			static const uint n = sizeof( x )*8 - 1;
			uint sum = 0;

			for( uint i = 1; i <= n; ++i ) {
				const uint _2powi = 1<<i;
				sum += ( x%_2powi )/( _2powi - 1 );
			}

			return sum;
		}
*/
uint Pattern( uint x )
{
/*
	return
		( ( x%2 )/1 ) +
		( ( x%4 )/3 ) +
		( ( x%8 )/7 ) +
		( ( x%16 )/15 ) +
		( ( x%32 )/31 ) +
		( ( x%64 )/63 ) +
		( ( x%128 )/127 ) +
		( ( x%256 )/255 ) +
		( ( x%512 )/511 ) +
		( ( x%1024 )/1023 ) +
		( ( x%2048 )/2047 ) +
		( ( x%4096 )/4095 ) +
		( ( x%8192 )/8191 ) +
		( ( x%16384 )/16383 )
		;
*/
	static const uint n = sizeof( x )*8;
	uint sum = 0;

	for( uint i = 1; i < n; ++i )
	{
		const uint _2powi = 1<<i;
		sum += ( x%_2powi )/( _2powi - 1 );
	}

	return sum;
}

/*
	Interesting pattern:
	
		f(x) = ( x%2 )/1 XOR ( ( x%3 )/2 )<<1
		f(x) = x%2 + ( ( x%3 )/2 )*2

	.                    .
	 +------------------+
	4|                  |
	3|      x     x     |
	2|   x     x     x  |
	1|  x x   x x   x x |
	0| x   x x   x x    |
	 +------------------+
	.  0123456789ABCDEF  .


	Another Pattern:

	.                    .
	 +------------------+
	4|   x       x      |
	3|       x       x  |
	2|  x x     x x     |
	1|      x x     x x |
	0| x   x   x   x    |
	 +------------------+
	.  0123456789ABCDEF  .


	Another Pattern (2):

	.                    .
	 +------------------+
	4|  x           x   |
	3| x x x       x x  |
	2|    x x x       x |
	1|       x x x      |
	0|          x x     |
	 +------------------+
	.  0123456789ABCDEF  .

*/

typedef uint( *PatternFunction_t )( uint );
void PlotGraph( const char *pszTitle, PatternFunction_t pfnPattern )
{
	uint points[ 16 ];
	char graph[ 16*5 ];

	for( uint i = 0; i < sizeof( graph ); ++i ) { graph[ i ] = ' '; }

	for( uint i = 0; i < 16; ++i ) {
		points[ i ] = pfnPattern( i );
		if( points[ i ] <= 4 ) {
			graph[ ( 4 - points[ i ] )*16 + i ] = 'x';
		}
	}


	printf( "\n  %s:\n", pszTitle );
	printf( "  .                    .\n" );
	printf( "   +------------------+ \n" );
	printf( "  4| %.16s | \n", &graph[ 16*0 ] );
	printf( "  3| %.16s | \n", &graph[ 16*1 ] );
	printf( "  2| %.16s | \n", &graph[ 16*2 ] );
	printf( "  1| %.16s | \n", &graph[ 16*3 ] );
	printf( "  0| %.16s | \n", &graph[ 16*4 ] );
	printf( "   +------------------+ \n" );
	printf( "  .  0123456789ABCDEF  .\n" );
	printf( "\n" );
}

int main()
{
	for( uint i = 0; i < 16; ++i )
	{
		uint x, y;
		LinearToQuadRow( i, x, y );
		printf( "%u -> (%u,%u) -> %u :: %u\n",
			i, x, y, QuadRowToLinear( x, y ), Pattern( i ) );
	}
	
	PlotGraph( "Interesting Pattern", Pattern );

	return EXIT_SUCCESS;
}
