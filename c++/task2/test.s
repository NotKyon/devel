	.file	"test.c"
	.text
	.p2align 4,,15
.globl _InterlockedOr
	.def	_InterlockedOr;	.scl	2;	.type	32;	.endef
_InterlockedOr:
	subl	$8, %esp
	movl	12(%esp), %edx
	movl	%ebx, (%esp)
	movl	16(%esp), %ebx
	movl	%esi, 4(%esp)
	movl	(%edx), %eax
L2:
	movl	%eax, %ecx
	movl	%eax, %esi
	orl	%ebx, %ecx
	lock cmpxchgl	%ecx, (%edx)
	jne	L2
	movl	%esi, %eax
	movl	(%esp), %ebx
	movl	4(%esp), %esi
	addl	$8, %esp
	ret
