#ifndef MEMORY_H
#define MEMORY_H

#include "local.h"

#include <stddef.h>

void *Alloc(size_t n);
void *AllocZero(size_t n);
void *Free(void *p);

void *Memory(void *p, size_t n);

char *DuplicateN(const char *src, size_t srcn);
char *Duplicate(const char *src);
char *CopyN(char *dst, const char *src, size_t srcn);
char *Copy(char *dst, const char *src);
char *AppendNChar(char *dst, const char *src, size_t srcn, char ch);
char *AppendN(char *dst, const char *src, size_t srcn);
char *Append(char *dst, const char *src);

char *TrimAppendChar(char *dst, const char *src, char ch);
char *TrimAppend(char *dst, const char *src);

char *StrCpy(char *dst, size_t dstn, const char *src);
char *StrCpyN(char *dst, size_t dstn, const char *src, size_t srcn);

char *StrCat(char *dst, size_t dstn, const char *src);
char *StrCatN(char *dst, size_t dstn, const char *src, size_t srcn);

#endif
