#include <AxTek/Threading/CThread.h>
#include <AxTek/Threading/CLock.h>
#include <AxTek/Containers/TList.h>
#include "IsPrime.h"

#include <stdio.h>
#include <stdlib.h> //for rand() and EXIT_SUCCESS

//
//	The following data structure is used to demonstrate how you might use locks.
//	This structure represents a linked list of integers which can be added and
//	removed from the list in a thread-safe manner.
//
struct SData {
	int amount;
	ax::lib::TListLink<SData> link;

	static ax::lib::TListBase<SData> g_list;
	static ax::task::CLock g_lock;

	inline SData(): amount(0), link(this) {
		g_lock.Lock();
		g_list.AddLast(&link);
		g_lock.Unlock();
	}
	inline ~SData() {
		g_lock.Lock();
		link.Remove();
		g_lock.Unlock();
	}

	inline void SetAmount(int x) {
		amount = x;
	}
	inline int GetAmount() const {
		return amount;
	}
};
ax::lib::TListBase<SData> SData::g_list;
ax::task::CLock SData::g_lock;

bool g_start = false; //tells the threads to start working

//
//	Each thread will add a couple of random numbers to the list, then remove a
//	couple of the added numbers.
//
//	Threads will also wait until they have been explicitly told to start.
//
ax::task::result_t THREADPROCAPI thread_f(void *) {
	size_t index;
	SData *p[50];

	// wait for the start signal
	while(!g_start)
		continue;

	// allocate random numbers for the list
	for(index=0; index<sizeof(p)/sizeof(p[0]); index++) {
		p[index] = new SData();
		p[index]->SetAmount(rand() % 1000);
	}

	// remove a couple of the random numbers
	for(index=0; index<sizeof(p)/sizeof(p[0]); index++) {
		// 75% of the added numbers will be removed
		if (rand() % 100 > 100 - 75) {
			delete p[index];
			p[index] = (SData *)NULL;
		}
	}

	// done
	return (ax::task::result_t)0;
}

//
//	Create a couple of threads to test with. Then display the results
//
int main() {
	ax::task::CThread threads[32];
	size_t i;
	SData *p;

	// create the threads
	for(i=0; i<sizeof(threads)/sizeof(threads[0]); i++)
		threads[i].Init(thread_f);

	// tell them all to get started!
	g_start = true;

	// wait for all of the threads to finish
	for(i=0; i<sizeof(threads)/sizeof(threads[0]); i++)
		threads[i].Join();

	// display the results
	i = 0;
	for(p=SData::g_list.First(); p; p=p->link.After())
		printf("%i%c", p->GetAmount(), ++i%8==0 ? '\n' : '\t');

	// remove all entries
	SData::g_list.SetDeleteObjectOnRemove(true);
	SData::g_list.RemoveAll();

	// done
	return EXIT_SUCCESS;
}
