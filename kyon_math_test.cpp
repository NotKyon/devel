#include "kyon_math.hpp"
#include <stdio.h>
#include <stdlib.h>

int main() {

	using namespace kyon_math;

	const float3 p( 0, 1, 2 );
	const float3 q( 2, 7, 3 );

	const float3 v = p + q.zyx();

	printf( "%u: (%g,%g,%g)\n", ( unsigned int )sizeof( float3 ), v.x, v.y, v.z );

	return EXIT_SUCCESS;

}
