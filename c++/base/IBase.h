#ifndef IBASE_H
#define IBASE_H

class IBase {
public:
	IBase(): mRefCnt(1), mName(0) {}
	virtual ~IBase() {}

	virtual void Grab() const { mRefCnt++; }
	virtual bool Drop() const { return (--mRefCnt==0) ? ((delete this), true) : false; }

	virtual const char *GetDebugName() { return mName; }

protected:
	mutable unsigned int mRefCnt;
	const char *mName;

	virtual void SetDebugName(const char *name) { mName = name; }
};

#endif
