#include <windows.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/gl.h>

#ifndef SECURE_LIB_ENABLED
# if defined( __STDC_WANT_SECURE_LIB__ ) && defined( _MSC_VER )
#  define SECURE_LIB_ENABLED 1
# else
#  define SECURE_LIB_ENABLED 0
# endif
#endif

#ifndef DEBUG_ENABLED
# if defined( _DEBUG ) || defined( DEBUG ) || defined( __debug__ )
#  define DEBUG_ENABLED 1
# else
#  define DEBUG_ENABLED 0
# endif
#endif

#if DEBUG_ENABLED
# if defined( __GNUC__ )
#  define DEBUG_BREAK() __builtin_trap()
# elif defined( _WIN32 )
#  define DEBUG_BREAK() DebugBreak()
# else
#  define DEBUG_BREAK() ( void )0
# endif
#else
# define DEBUG_BREAK() ( void )0
#endif

const char *GL_GetErrorString( int err ) {
	switch( err ) {
#define CASE(x) case x: return #x
	CASE( GL_NO_ERROR );
	CASE( GL_INVALID_ENUM );
	CASE( GL_INVALID_VALUE );
	CASE( GL_INVALID_OPERATION );
	CASE( GL_OUT_OF_MEMORY );
	CASE( GL_STACK_UNDERFLOW );
	CASE( GL_STACK_OVERFLOW );
#undef CASE

	default:
		break;
	}

	return "(unknown error)";
}

#if SECURE_LIB_ENABLED
# define SEC_Vsprintf vsprintf_s
# define SEC_Sprintf sprintf_s
#else
# define SEC_Vsprintf vsnprintf
# define SEC_Sprintf snprintf
#endif

void COM_Error( const char *format, ... ) {
	char buf[ 4096 ];
	va_list args;

	int lastWinErr = GetLastError();
	int lastErrno = errno;
	int lastGLErr = glGetError();

	va_start( args, format );
	SEC_Vsprintf( buf, sizeof( buf ), format, args );
	buf[ sizeof( buf ) - 1 ] = '\0';
	va_end( args );

	char errbuf[ 1024 ];

	errbuf[ 0 ] = '\0';
	if( lastGLErr != 0 ) {
		SEC_Sprintf( errbuf, sizeof( errbuf ), ": %s (%i)",
			GL_GetErrorString( lastGLErr ), lastGLErr );
		errbuf[ sizeof( errbuf ) - 1 ] = '\0';
	} else if( lastWinErr != 0 ) {
		char tmp[ 512 ];

		FormatMessageA( FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS, NULL, lastWinErr,
			MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ), tmp, sizeof( tmp ),
			NULL );

		SEC_Sprintf( errbuf, sizeof( errbuf ), ": %s (%i)", tmp, lastWinErr );
		errbuf[ sizeof( errbuf ) - 1 ] = '\0';
	} else if( lastErrno != 0 ) {
		char tmp[ 256 ];

#if SECURE_LIB_ENABLED
		if( strerror_s( tmp, sizeof( tmp ), lastErrno ) == 0 ) {
			SEC_Sprintf( errbuf, sizeof( errbuf ), ": %s (%i)", tmp,
				lastErrno );
		}
#else
		SEC_Sprintf( errbuf, sizeof( errbuf ), ": %s (%i)",
			strerror( lastErrno ), lastErrno );
		errbuf[ sizeof( errbuf ) - 1 ] = '\0';
#endif
	}

	fprintf( stderr, "ERROR: %s%s\n", buf, errbuf );
	fflush( stderr );

	DEBUG_BREAK();
}

int gScreenResX = 0;
int gScreenResY = 0;

LRESULT CALLBACK MSWIN_MsgProc_f( HWND wnd, UINT msg, WPARAM wp, LPARAM lp ) {
	switch( msg ) {
	case WM_DESTROY:
		PostQuitMessage( 0 );
		return 0;

	case WM_SIZE:
		gScreenResX = LOWORD( lp );
		gScreenResY = HIWORD( lp );
		return 0;

	default:
		break;
	}

	return DefWindowProcA( wnd, msg, wp, lp );
}

#define MSWIN_WINDOW_CLASS "OpenGLWindow"

HWND gWindowHandle = NULL;

void MSWIN_Init() {
	WNDCLASSEXA wc;

	// Prepare information about the window class
	wc.cbSize = sizeof( wc );
	wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = &MSWIN_MsgProc_f;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = GetModuleHandleA( NULL );
	wc.hIcon = LoadIcon( NULL, IDI_APPLICATION );
	wc.hCursor = LoadCursor( NULL, IDC_ARROW );
	wc.hbrBackground = ( HBRUSH )NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = MSWIN_WINDOW_CLASS;
	wc.hIconSm = LoadIcon( NULL, IDI_APPLICATION );

	// Register the class used to create the window
	if( !RegisterClassExA( &wc ) ) {
		COM_Error( "MSWIN_Init: RegisterClassExA failed" );
		exit( EXIT_FAILURE );
	}

	// Create the window
	HWND wnd = CreateWindowExA( 0, MSWIN_WINDOW_CLASS, "OpenGL Test",
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, GetModuleHandleA( NULL ), NULL );
	if( !wnd ) {
		COM_Error( "MSWIN_Init: CreateWindowExA failed" );
		exit( EXIT_FAILURE );
	}

	// store the window handle for OpenGL to access
	gWindowHandle = wnd;
}

bool MSWIN_Loop() {
	MSG m;
	bool r = true;

	while( PeekMessageA( &m, 0, 0, 0, PM_REMOVE ) != 0 ) {
		if( m.message == WM_QUIT ) {
			r = false;
		}

		TranslateMessage( &m );
		DispatchMessageA( &m );
	}

	return r;
}


/*
===============================================================================

	WGL STUFF

** Copyright (c) 2007 The Khronos Group Inc.
** 
** Permission is hereby granted, free of charge, to any person obtaining a
** copy of this software and/or associated documentation files (the
** "Materials"), to deal in the Materials without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Materials, and to
** permit persons to whom the Materials are furnished to do so, subject to
** the following conditions:
** 
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Materials.
** 
** THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.

===============================================================================
*/
/* ------------------------- WGL_ARB_create_context ------------------------ */

#ifndef WGL_ARB_create_context
#define WGL_ARB_create_context 1

#define WGL_CONTEXT_DEBUG_BIT_ARB 0x0001
#define WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x0002
#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092
#define WGL_CONTEXT_LAYER_PLANE_ARB 0x2093
#define WGL_CONTEXT_FLAGS_ARB 0x2094
#define ERROR_INVALID_VERSION_ARB 0x2095
#define ERROR_INVALID_PROFILE_ARB 0x2096

typedef HGLRC( WINAPI *PFNWGLCREATECONTEXTATTRIBSARBPROC )( HDC hDC,
	HGLRC hShareContext, const int* attribList );

PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = NULL;

#endif /* WGL_ARB_create_context */

#ifndef GL_ARB_vertex_array_object
#define GL_ARB_vertex_array_object 1
typedef void (WINAPI * PFNGLBINDVERTEXARRAYPROC) (GLuint array);
typedef void (WINAPI * PFNGLDELETEVERTEXARRAYSPROC) (GLsizei n, const GLuint *arrays);
typedef void (WINAPI * PFNGLGENVERTEXARRAYSPROC) (GLsizei n, GLuint *arrays);
typedef GLboolean (WINAPI * PFNGLISVERTEXARRAYPROC) (GLuint array);

PFNGLBINDVERTEXARRAYPROC glBindVertexArray = NULL;
PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays = NULL;
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = NULL;
PFNGLISVERTEXARRAYPROC glIsVertexArray = NULL;
#endif


#ifndef GL_VERSION_1_5
#define GL_VERSION_1_5 1

#define GL_BUFFER_SIZE                    0x8764
#define GL_BUFFER_USAGE                   0x8765
#define GL_QUERY_COUNTER_BITS             0x8864
#define GL_CURRENT_QUERY                  0x8865
#define GL_QUERY_RESULT                   0x8866
#define GL_QUERY_RESULT_AVAILABLE         0x8867
#define GL_ARRAY_BUFFER                   0x8892
#define GL_ELEMENT_ARRAY_BUFFER           0x8893
#define GL_ARRAY_BUFFER_BINDING           0x8894
#define GL_ELEMENT_ARRAY_BUFFER_BINDING   0x8895
#define GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING 0x889F
#define GL_READ_ONLY                      0x88B8
#define GL_WRITE_ONLY                     0x88B9
#define GL_READ_WRITE                     0x88BA
#define GL_BUFFER_ACCESS                  0x88BB
#define GL_BUFFER_MAPPED                  0x88BC
#define GL_BUFFER_MAP_POINTER             0x88BD
#define GL_STREAM_DRAW                    0x88E0
#define GL_STREAM_READ                    0x88E1
#define GL_STREAM_COPY                    0x88E2
#define GL_STATIC_DRAW                    0x88E4
#define GL_STATIC_READ                    0x88E5
#define GL_STATIC_COPY                    0x88E6
#define GL_DYNAMIC_DRAW                   0x88E8
#define GL_DYNAMIC_READ                   0x88E9
#define GL_DYNAMIC_COPY                   0x88EA
#define GL_SAMPLES_PASSED                 0x8914
#define GL_SRC1_ALPHA                     0x8589
#define GL_VERTEX_ARRAY_BUFFER_BINDING    0x8896
#define GL_NORMAL_ARRAY_BUFFER_BINDING    0x8897
#define GL_COLOR_ARRAY_BUFFER_BINDING     0x8898
#define GL_INDEX_ARRAY_BUFFER_BINDING     0x8899
#define GL_TEXTURE_COORD_ARRAY_BUFFER_BINDING 0x889A
#define GL_EDGE_FLAG_ARRAY_BUFFER_BINDING 0x889B
#define GL_SECONDARY_COLOR_ARRAY_BUFFER_BINDING 0x889C
#define GL_FOG_COORDINATE_ARRAY_BUFFER_BINDING 0x889D
#define GL_WEIGHT_ARRAY_BUFFER_BINDING    0x889E
#define GL_FOG_COORD_SRC                  0x8450
#define GL_FOG_COORD                      0x8451
#define GL_CURRENT_FOG_COORD              0x8453
#define GL_FOG_COORD_ARRAY_TYPE           0x8454
#define GL_FOG_COORD_ARRAY_STRIDE         0x8455
#define GL_FOG_COORD_ARRAY_POINTER        0x8456
#define GL_FOG_COORD_ARRAY                0x8457
#define GL_FOG_COORD_ARRAY_BUFFER_BINDING 0x889D
#define GL_SRC0_RGB                       0x8580
#define GL_SRC1_RGB                       0x8581
#define GL_SRC2_RGB                       0x8582
#define GL_SRC0_ALPHA                     0x8588
#define GL_SRC2_ALPHA                     0x858A

typedef ptrdiff_t GLintptr;
typedef ptrdiff_t GLsizeiptr;

typedef void (WINAPI * PFNGLGENQUERIESPROC) (GLsizei n, GLuint *ids);
typedef void (WINAPI * PFNGLDELETEQUERIESPROC) (GLsizei n, const GLuint *ids);
typedef GLboolean (WINAPI * PFNGLISQUERYPROC) (GLuint id);
typedef void (WINAPI * PFNGLBEGINQUERYPROC) (GLenum target, GLuint id);
typedef void (WINAPI * PFNGLENDQUERYPROC) (GLenum target);
typedef void (WINAPI * PFNGLGETQUERYIVPROC) (GLenum target, GLenum pname, GLint *params);
typedef void (WINAPI * PFNGLGETQUERYOBJECTIVPROC) (GLuint id, GLenum pname, GLint *params);
typedef void (WINAPI * PFNGLGETQUERYOBJECTUIVPROC) (GLuint id, GLenum pname, GLuint *params);
typedef void (WINAPI * PFNGLBINDBUFFERPROC) (GLenum target, GLuint buffer);
typedef void (WINAPI * PFNGLDELETEBUFFERSPROC) (GLsizei n, const GLuint *buffers);
typedef void (WINAPI * PFNGLGENBUFFERSPROC) (GLsizei n, GLuint *buffers);
typedef GLboolean (WINAPI * PFNGLISBUFFERPROC) (GLuint buffer);
typedef void (WINAPI * PFNGLBUFFERDATAPROC) (GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);
typedef void (WINAPI * PFNGLBUFFERSUBDATAPROC) (GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid *data);
typedef void (WINAPI * PFNGLGETBUFFERSUBDATAPROC) (GLenum target, GLintptr offset, GLsizeiptr size, GLvoid *data);
typedef GLvoid* (WINAPI * PFNGLMAPBUFFERPROC) (GLenum target, GLenum access);
typedef GLboolean (WINAPI * PFNGLUNMAPBUFFERPROC) (GLenum target);
typedef void (WINAPI * PFNGLGETBUFFERPARAMETERIVPROC) (GLenum target, GLenum pname, GLint *params);
typedef void (WINAPI * PFNGLGETBUFFERPOINTERVPROC) (GLenum target, GLenum pname, GLvoid* *params);

PFNGLGENQUERIESPROC glGenQueries = NULL;
PFNGLDELETEQUERIESPROC glDeleteQueries = NULL;
PFNGLISQUERYPROC glIsQuery = NULL;
PFNGLBEGINQUERYPROC glBeginQuery = NULL;
PFNGLENDQUERYPROC glEndQuery = NULL;
PFNGLGETQUERYIVPROC glGetQueryiv = NULL;
PFNGLGETQUERYOBJECTIVPROC glGetQueryObjectiv = NULL;
PFNGLGETQUERYOBJECTUIVPROC glGetQueryObjectuiv = NULL;
PFNGLBINDBUFFERPROC glBindBuffer = NULL;
PFNGLDELETEBUFFERSPROC glDeleteBuffers = NULL;
PFNGLGENBUFFERSPROC glGenBuffers = NULL;
PFNGLISBUFFERPROC glIsBuffer = NULL;
PFNGLBUFFERDATAPROC glBufferData = NULL;
PFNGLBUFFERSUBDATAPROC glBufferSubdata = NULL;
PFNGLGETBUFFERSUBDATAPROC glGetBufferSubdata = NULL;
PFNGLMAPBUFFERPROC glMapBuffer = NULL;
PFNGLUNMAPBUFFERPROC glUnmapBuffer = NULL;
PFNGLGETBUFFERPARAMETERIVPROC glGetBufferParameteriv = NULL;
PFNGLGETBUFFERPOINTERVPROC glGetBufferPointerv = NULL;
#endif


#ifndef GL_VERSION_2_0
#define GL_VERSION_2_0 1

#define GL_BLEND_EQUATION_RGB             0x8009
#define GL_VERTEX_ATTRIB_ARRAY_ENABLED    0x8622
#define GL_VERTEX_ATTRIB_ARRAY_SIZE       0x8623
#define GL_VERTEX_ATTRIB_ARRAY_STRIDE     0x8624
#define GL_VERTEX_ATTRIB_ARRAY_TYPE       0x8625
#define GL_CURRENT_VERTEX_ATTRIB          0x8626
#define GL_VERTEX_PROGRAM_POINT_SIZE      0x8642
#define GL_VERTEX_ATTRIB_ARRAY_POINTER    0x8645
#define GL_STENCIL_BACK_FUNC              0x8800
#define GL_STENCIL_BACK_FAIL              0x8801
#define GL_STENCIL_BACK_PASS_DEPTH_FAIL   0x8802
#define GL_STENCIL_BACK_PASS_DEPTH_PASS   0x8803
#define GL_MAX_DRAW_BUFFERS               0x8824
#define GL_DRAW_BUFFER0                   0x8825
#define GL_DRAW_BUFFER1                   0x8826
#define GL_DRAW_BUFFER2                   0x8827
#define GL_DRAW_BUFFER3                   0x8828
#define GL_DRAW_BUFFER4                   0x8829
#define GL_DRAW_BUFFER5                   0x882A
#define GL_DRAW_BUFFER6                   0x882B
#define GL_DRAW_BUFFER7                   0x882C
#define GL_DRAW_BUFFER8                   0x882D
#define GL_DRAW_BUFFER9                   0x882E
#define GL_DRAW_BUFFER10                  0x882F
#define GL_DRAW_BUFFER11                  0x8830
#define GL_DRAW_BUFFER12                  0x8831
#define GL_DRAW_BUFFER13                  0x8832
#define GL_DRAW_BUFFER14                  0x8833
#define GL_DRAW_BUFFER15                  0x8834
#define GL_BLEND_EQUATION_ALPHA           0x883D
#define GL_MAX_VERTEX_ATTRIBS             0x8869
#define GL_VERTEX_ATTRIB_ARRAY_NORMALIZED 0x886A
#define GL_MAX_TEXTURE_IMAGE_UNITS        0x8872
#define GL_FRAGMENT_SHADER                0x8B30
#define GL_VERTEX_SHADER                  0x8B31
#define GL_MAX_FRAGMENT_UNIFORM_COMPONENTS 0x8B49
#define GL_MAX_VERTEX_UNIFORM_COMPONENTS  0x8B4A
#define GL_MAX_VARYING_FLOATS             0x8B4B
#define GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS 0x8B4C
#define GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS 0x8B4D
#define GL_SHADER_TYPE                    0x8B4F
#define GL_FLOAT_VEC2                     0x8B50
#define GL_FLOAT_VEC3                     0x8B51
#define GL_FLOAT_VEC4                     0x8B52
#define GL_INT_VEC2                       0x8B53
#define GL_INT_VEC3                       0x8B54
#define GL_INT_VEC4                       0x8B55
#define GL_BOOL                           0x8B56
#define GL_BOOL_VEC2                      0x8B57
#define GL_BOOL_VEC3                      0x8B58
#define GL_BOOL_VEC4                      0x8B59
#define GL_FLOAT_MAT2                     0x8B5A
#define GL_FLOAT_MAT3                     0x8B5B
#define GL_FLOAT_MAT4                     0x8B5C
#define GL_SAMPLER_1D                     0x8B5D
#define GL_SAMPLER_2D                     0x8B5E
#define GL_SAMPLER_3D                     0x8B5F
#define GL_SAMPLER_CUBE                   0x8B60
#define GL_SAMPLER_1D_SHADOW              0x8B61
#define GL_SAMPLER_2D_SHADOW              0x8B62
#define GL_DELETE_STATUS                  0x8B80
#define GL_COMPILE_STATUS                 0x8B81
#define GL_LINK_STATUS                    0x8B82
#define GL_VALIDATE_STATUS                0x8B83
#define GL_INFO_LOG_LENGTH                0x8B84
#define GL_ATTACHED_SHADERS               0x8B85
#define GL_ACTIVE_UNIFORMS                0x8B86
#define GL_ACTIVE_UNIFORM_MAX_LENGTH      0x8B87
#define GL_SHADER_SOURCE_LENGTH           0x8B88
#define GL_ACTIVE_ATTRIBUTES              0x8B89
#define GL_ACTIVE_ATTRIBUTE_MAX_LENGTH    0x8B8A
#define GL_FRAGMENT_SHADER_DERIVATIVE_HINT 0x8B8B
#define GL_SHADING_LANGUAGE_VERSION       0x8B8C
#define GL_CURRENT_PROGRAM                0x8B8D
#define GL_POINT_SPRITE_COORD_ORIGIN      0x8CA0
#define GL_LOWER_LEFT                     0x8CA1
#define GL_UPPER_LEFT                     0x8CA2
#define GL_STENCIL_BACK_REF               0x8CA3
#define GL_STENCIL_BACK_VALUE_MASK        0x8CA4
#define GL_STENCIL_BACK_WRITEMASK         0x8CA5
#define GL_VERTEX_PROGRAM_TWO_SIDE        0x8643
#define GL_POINT_SPRITE                   0x8861
#define GL_COORD_REPLACE                  0x8862
#define GL_MAX_TEXTURE_COORDS             0x8871

typedef char GLchar;

typedef void (WINAPI * PFNGLBLENDEQUATIONSEPARATEPROC) (GLenum modeRGB, GLenum modeAlpha);
typedef void (WINAPI * PFNGLDRAWBUFFERSPROC) (GLsizei n, const GLenum *bufs);
typedef void (WINAPI * PFNGLSTENCILOPSEPARATEPROC) (GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass);
typedef void (WINAPI * PFNGLSTENCILFUNCSEPARATEPROC) (GLenum face, GLenum func, GLint ref, GLuint mask);
typedef void (WINAPI * PFNGLSTENCILMASKSEPARATEPROC) (GLenum face, GLuint mask);
typedef void (WINAPI * PFNGLATTACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (WINAPI * PFNGLBINDATTRIBLOCATIONPROC) (GLuint program, GLuint index, const GLchar *name);
typedef void (WINAPI * PFNGLCOMPILESHADERPROC) (GLuint shader);
typedef GLuint (WINAPI * PFNGLCREATEPROGRAMPROC) (void);
typedef GLuint (WINAPI * PFNGLCREATESHADERPROC) (GLenum type);
typedef void (WINAPI * PFNGLDELETEPROGRAMPROC) (GLuint program);
typedef void (WINAPI * PFNGLDELETESHADERPROC) (GLuint shader);
typedef void (WINAPI * PFNGLDETACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (WINAPI * PFNGLDISABLEVERTEXATTRIBARRAYPROC) (GLuint index);
typedef void (WINAPI * PFNGLENABLEVERTEXATTRIBARRAYPROC) (GLuint index);
typedef void (WINAPI * PFNGLGETACTIVEATTRIBPROC) (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
typedef void (WINAPI * PFNGLGETACTIVEUNIFORMPROC) (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
typedef void (WINAPI * PFNGLGETATTACHEDSHADERSPROC) (GLuint program, GLsizei maxCount, GLsizei *count, GLuint *obj);
typedef GLint (WINAPI * PFNGLGETATTRIBLOCATIONPROC) (GLuint program, const GLchar *name);
typedef void (WINAPI * PFNGLGETPROGRAMIVPROC) (GLuint program, GLenum pname, GLint *params);
typedef void (WINAPI * PFNGLGETPROGRAMINFOLOGPROC) (GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
typedef void (WINAPI * PFNGLGETSHADERIVPROC) (GLuint shader, GLenum pname, GLint *params);
typedef void (WINAPI * PFNGLGETSHADERINFOLOGPROC) (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
typedef void (WINAPI * PFNGLGETSHADERSOURCEPROC) (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *source);
typedef GLint (WINAPI * PFNGLGETUNIFORMLOCATIONPROC) (GLuint program, const GLchar *name);
typedef void (WINAPI * PFNGLGETUNIFORMFVPROC) (GLuint program, GLint location, GLfloat *params);
typedef void (WINAPI * PFNGLGETUNIFORMIVPROC) (GLuint program, GLint location, GLint *params);
typedef void (WINAPI * PFNGLGETVERTEXATTRIBDVPROC) (GLuint index, GLenum pname, GLdouble *params);
typedef void (WINAPI * PFNGLGETVERTEXATTRIBFVPROC) (GLuint index, GLenum pname, GLfloat *params);
typedef void (WINAPI * PFNGLGETVERTEXATTRIBIVPROC) (GLuint index, GLenum pname, GLint *params);
typedef void (WINAPI * PFNGLGETVERTEXATTRIBPOINTERVPROC) (GLuint index, GLenum pname, GLvoid* *pointer);
typedef GLboolean (WINAPI * PFNGLISPROGRAMPROC) (GLuint program);
typedef GLboolean (WINAPI * PFNGLISSHADERPROC) (GLuint shader);
typedef void (WINAPI * PFNGLLINKPROGRAMPROC) (GLuint program);
typedef void (WINAPI * PFNGLSHADERSOURCEPROC) (GLuint shader, GLsizei count, const GLchar* const *string, const GLint *length);
typedef void (WINAPI * PFNGLUSEPROGRAMPROC) (GLuint program);
typedef void (WINAPI * PFNGLUNIFORM1FPROC) (GLint location, GLfloat v0);
typedef void (WINAPI * PFNGLUNIFORM2FPROC) (GLint location, GLfloat v0, GLfloat v1);
typedef void (WINAPI * PFNGLUNIFORM3FPROC) (GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
typedef void (WINAPI * PFNGLUNIFORM4FPROC) (GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
typedef void (WINAPI * PFNGLUNIFORM1IPROC) (GLint location, GLint v0);
typedef void (WINAPI * PFNGLUNIFORM2IPROC) (GLint location, GLint v0, GLint v1);
typedef void (WINAPI * PFNGLUNIFORM3IPROC) (GLint location, GLint v0, GLint v1, GLint v2);
typedef void (WINAPI * PFNGLUNIFORM4IPROC) (GLint location, GLint v0, GLint v1, GLint v2, GLint v3);
typedef void (WINAPI * PFNGLUNIFORM1FVPROC) (GLint location, GLsizei count, const GLfloat *value);
typedef void (WINAPI * PFNGLUNIFORM2FVPROC) (GLint location, GLsizei count, const GLfloat *value);
typedef void (WINAPI * PFNGLUNIFORM3FVPROC) (GLint location, GLsizei count, const GLfloat *value);
typedef void (WINAPI * PFNGLUNIFORM4FVPROC) (GLint location, GLsizei count, const GLfloat *value);
typedef void (WINAPI * PFNGLUNIFORM1IVPROC) (GLint location, GLsizei count, const GLint *value);
typedef void (WINAPI * PFNGLUNIFORM2IVPROC) (GLint location, GLsizei count, const GLint *value);
typedef void (WINAPI * PFNGLUNIFORM3IVPROC) (GLint location, GLsizei count, const GLint *value);
typedef void (WINAPI * PFNGLUNIFORM4IVPROC) (GLint location, GLsizei count, const GLint *value);
typedef void (WINAPI * PFNGLUNIFORMMATRIX2FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (WINAPI * PFNGLUNIFORMMATRIX3FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (WINAPI * PFNGLUNIFORMMATRIX4FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (WINAPI * PFNGLVALIDATEPROGRAMPROC) (GLuint program);
typedef void (WINAPI * PFNGLVERTEXATTRIB1DPROC) (GLuint index, GLdouble x);
typedef void (WINAPI * PFNGLVERTEXATTRIB1DVPROC) (GLuint index, const GLdouble *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB1FPROC) (GLuint index, GLfloat x);
typedef void (WINAPI * PFNGLVERTEXATTRIB1FVPROC) (GLuint index, const GLfloat *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB1SPROC) (GLuint index, GLshort x);
typedef void (WINAPI * PFNGLVERTEXATTRIB1SVPROC) (GLuint index, const GLshort *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB2DPROC) (GLuint index, GLdouble x, GLdouble y);
typedef void (WINAPI * PFNGLVERTEXATTRIB2DVPROC) (GLuint index, const GLdouble *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB2FPROC) (GLuint index, GLfloat x, GLfloat y);
typedef void (WINAPI * PFNGLVERTEXATTRIB2FVPROC) (GLuint index, const GLfloat *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB2SPROC) (GLuint index, GLshort x, GLshort y);
typedef void (WINAPI * PFNGLVERTEXATTRIB2SVPROC) (GLuint index, const GLshort *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB3DPROC) (GLuint index, GLdouble x, GLdouble y, GLdouble z);
typedef void (WINAPI * PFNGLVERTEXATTRIB3DVPROC) (GLuint index, const GLdouble *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB3FPROC) (GLuint index, GLfloat x, GLfloat y, GLfloat z);
typedef void (WINAPI * PFNGLVERTEXATTRIB3FVPROC) (GLuint index, const GLfloat *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB3SPROC) (GLuint index, GLshort x, GLshort y, GLshort z);
typedef void (WINAPI * PFNGLVERTEXATTRIB3SVPROC) (GLuint index, const GLshort *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4NBVPROC) (GLuint index, const GLbyte *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4NIVPROC) (GLuint index, const GLint *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4NSVPROC) (GLuint index, const GLshort *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4NUBPROC) (GLuint index, GLubyte x, GLubyte y, GLubyte z, GLubyte w);
typedef void (WINAPI * PFNGLVERTEXATTRIB4NUBVPROC) (GLuint index, const GLubyte *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4NUIVPROC) (GLuint index, const GLuint *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4NUSVPROC) (GLuint index, const GLushort *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4BVPROC) (GLuint index, const GLbyte *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4DPROC) (GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w);
typedef void (WINAPI * PFNGLVERTEXATTRIB4DVPROC) (GLuint index, const GLdouble *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4FPROC) (GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w);
typedef void (WINAPI * PFNGLVERTEXATTRIB4FVPROC) (GLuint index, const GLfloat *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4IVPROC) (GLuint index, const GLint *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4SPROC) (GLuint index, GLshort x, GLshort y, GLshort z, GLshort w);
typedef void (WINAPI * PFNGLVERTEXATTRIB4SVPROC) (GLuint index, const GLshort *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4UBVPROC) (GLuint index, const GLubyte *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4UIVPROC) (GLuint index, const GLuint *v);
typedef void (WINAPI * PFNGLVERTEXATTRIB4USVPROC) (GLuint index, const GLushort *v);
typedef void (WINAPI * PFNGLVERTEXATTRIBPOINTERPROC) (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer);

PFNGLBLENDEQUATIONSEPARATEPROC glBlendEquationSeparate = NULL;
PFNGLDRAWBUFFERSPROC glDrawBuffers = NULL;
PFNGLSTENCILOPSEPARATEPROC glStencilOpSeparate = NULL;
PFNGLSTENCILFUNCSEPARATEPROC glStencilFuncSeparate = NULL;
PFNGLSTENCILMASKSEPARATEPROC glStencilMaskSeparate = NULL;
PFNGLATTACHSHADERPROC glAttachShader = NULL;
PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = NULL;
PFNGLCOMPILESHADERPROC glCompileShader = NULL;
PFNGLCREATEPROGRAMPROC glCreateProgram = NULL;
PFNGLCREATESHADERPROC glCreateShader = NULL;
PFNGLDELETEPROGRAMPROC glDeleteProgram = NULL;
PFNGLDELETESHADERPROC glDeleteShader = NULL;
PFNGLDETACHSHADERPROC glDetachShader = NULL;
PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = NULL;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = NULL;
PFNGLGETACTIVEATTRIBPROC glGetActiveAttrib = NULL;
PFNGLGETATTACHEDSHADERSPROC glGetAttachedShaders = NULL;
PFNGLGETATTRIBLOCATIONPROC glGetAttribLocation = NULL;
PFNGLGETPROGRAMIVPROC glGetProgramiv = NULL;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = NULL;
PFNGLGETSHADERIVPROC glGetShaderiv = NULL;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = NULL;
PFNGLGETSHADERSOURCEPROC glGetShaderSource = NULL;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = NULL;
PFNGLGETUNIFORMFVPROC glGetUniformfv = NULL;
PFNGLGETUNIFORMIVPROC glGetUniformiv = NULL;
PFNGLGETVERTEXATTRIBDVPROC glGetVertexAttribdv = NULL;
PFNGLGETVERTEXATTRIBFVPROC glGetVertexAttribfv = NULL;
PFNGLGETVERTEXATTRIBIVPROC glGetVertexAttribiv = NULL;
PFNGLGETVERTEXATTRIBPOINTERVPROC glGetVertexAttribPointerv = NULL;
PFNGLISPROGRAMPROC glIsProgram = NULL;
PFNGLISSHADERPROC glIsShader = NULL;
PFNGLLINKPROGRAMPROC glLinkProgram = NULL;
PFNGLSHADERSOURCEPROC glShaderSource = NULL;
PFNGLUSEPROGRAMPROC glUseProgram = NULL;
PFNGLUNIFORM1FPROC glUniform1f = NULL;
PFNGLUNIFORM2FPROC glUniform2f = NULL;
PFNGLUNIFORM3FPROC glUniform3f = NULL;
PFNGLUNIFORM4FPROC glUniform4f = NULL;
PFNGLUNIFORM1IPROC glUniform1i = NULL;
PFNGLUNIFORM2IPROC glUniform2i = NULL;
PFNGLUNIFORM3IPROC glUniform3i = NULL;
PFNGLUNIFORM4IPROC glUniform4i = NULL;
PFNGLUNIFORM1FVPROC glUniform1fv = NULL;
PFNGLUNIFORM2FVPROC glUniform2fv = NULL;
PFNGLUNIFORM3FVPROC glUniform3fv = NULL;
PFNGLUNIFORM4FVPROC glUniform4fv = NULL;
PFNGLUNIFORM1IVPROC glUniform1iv = NULL;
PFNGLUNIFORM2IVPROC glUniform2iv = NULL;
PFNGLUNIFORM3IVPROC glUniform3iv = NULL;
PFNGLUNIFORM4IVPROC glUniform4iv = NULL;
PFNGLUNIFORMMATRIX2FVPROC glUniformMatrix2fv = NULL;
PFNGLUNIFORMMATRIX3FVPROC glUniformMatrix3fv = NULL;
PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv = NULL;
PFNGLVALIDATEPROGRAMPROC glValidateProgram = NULL;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = NULL;

#endif

/*
===============================================================================

	OPENGL FUNCTIONS

===============================================================================
*/
HDC gGLDevCtx = NULL;
HGLRC gGLRenderCtx = NULL;

const GLchar *const gDefVertShader =
	"#version 140\n"
	"\n"
	"in vec3 in_Position;\n"
	"in vec3 in_Color;\n"
	"out vec3 ex_Color;\n"
	"\n"
	"void main() {\n"
	"    gl_Position = vec4( in_Position, 1.0 );\n"
	"    ex_Color = in_Color;\n"
	"}\n";
const GLchar *const gDefFragShader =
	"#version 140\n"
	"\n"
	"precision highp float;\n"
	"\n"
	"in vec3 ex_Color;\n"
	"out vec4 out_Color;\n"
	"\n"
	"void main() {\n"
	"    out_Color = vec4( ex_Color, 1.0 );\n"
	"}\n";

struct vertex_t {
	float x, y, z;
	float r, g, b;
};

void GL_CheckError() {
	int lastGLErr = glGetError();
	if( lastGLErr == GL_NO_ERROR ) {
		return;
	}
	
	COM_Error( "OpenGL Error: %s (%i)", GL_GetErrorString( lastGLErr ),
		lastGLErr );
#if DEBUG_ENABLED
	DEBUG_BREAK();
#endif
}

GLuint gVAO = 0;

void GL_Init() {
	// window handle is invalid
	if( !gWindowHandle ) {
		COM_Error( "GL_Init: Invalid window handle" );
		exit( EXIT_FAILURE );
	}
	
	// get the device context of the window
	HDC devCtx = GetDC( gWindowHandle );
	if( !devCtx ) {
		COM_Error( "GL_Init: GetDC failed" );
		exit( EXIT_FAILURE );
	}

	// prepare the pixel format descriptor
	PIXELFORMATDESCRIPTOR pfd;

	pfd.nSize = sizeof( pfd );
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |
		PFD_GENERIC_ACCELERATED | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cRedBits = 8;
	pfd.cRedShift = 0;
	pfd.cGreenBits = 8;
	pfd.cGreenShift = 0;
	pfd.cBlueBits = 8;
	pfd.cBlueShift = 0;
	pfd.cAlphaBits = 0;
	pfd.cAlphaShift = 0;
	pfd.cAccumBits = 0;
	pfd.cAccumRedBits = 0;
	pfd.cAccumGreenBits = 0;
	pfd.cAccumBlueBits = 0;
	pfd.cAccumAlphaBits = 0;
	pfd.cDepthBits = 24;
	pfd.cStencilBits = 8;
	pfd.cAuxBuffers = 0;
	pfd.iLayerType = 0;
	pfd.bReserved = 0;
	pfd.dwLayerMask = 0;
	pfd.dwVisibleMask = 0;
	pfd.dwDamageMask = 0;

	// get the pixel format
	int pixelFormat = ChoosePixelFormat( devCtx, &pfd );
	if( !pixelFormat ) {
		COM_Error( "GL_Init: ChoosePixelFormat failed" );
		exit( EXIT_FAILURE );
	}

	// select the pixel format for the device context
	if( !SetPixelFormat( devCtx, pixelFormat, &pfd ) ) {
		COM_Error( "GL_Init: SetPixelFormat failed" );
		exit( EXIT_FAILURE );
	}

	// create the rendering context
	HGLRC renderCtx = wglCreateContext( devCtx );
	if( !renderCtx ) {
		COM_Error( "GL_Init: wglCreateContext failed" );
		exit( EXIT_FAILURE );
	}

	// set the context as current
	if( !wglMakeCurrent( devCtx, renderCtx ) ) {
		COM_Error( "GL_Init: wglMakeCurrent failed" );
		exit( EXIT_FAILURE );
	}

	// need to make a more advanced context
	*( ( void ** )&wglCreateContextAttribsARB ) =
		( void * )wglGetProcAddress( "wglCreateContextAttribsARB" );

	if( !wglCreateContextAttribsARB ) {
		COM_Error( "GL_Init: wglCreateContextAttribsARB not there" );
		exit( EXIT_FAILURE );
	}
	
	// delete the previous context
	wglDeleteContext( renderCtx );
	renderCtx = NULL;

	// create a core context
	const int attribs[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 3,
		WGL_CONTEXT_FLAGS_ARB, 0,
		0, 0
	};
	
	renderCtx = wglCreateContextAttribsARB( devCtx, ( HGLRC )NULL, attribs );
	if( !renderCtx ) {
		COM_Error( "GL_Init: wglCreateContextAttribsARB for 3.3 failed" );
		exit( EXIT_FAILURE );
	}
	
	// set this new context as the current
	wglMakeCurrent( devCtx, renderCtx );

	// load up more functions
#define Q(x) *( ( void ** )&x ) = ( void * )wglGetProcAddress( #x )
	Q( glBindVertexArray );
	Q( glDeleteVertexArrays );
	Q( glGenVertexArrays );
	Q( glIsVertexArray );
	Q( glGenQueries );
	Q( glDeleteQueries );
	Q( glIsQuery );
	Q( glBeginQuery );
	Q( glEndQuery );
	Q( glGetQueryiv );
	Q( glGetQueryObjectiv );
	Q( glGetQueryObjectuiv );
	Q( glBindBuffer );
	Q( glDeleteBuffers );
	Q( glGenBuffers );
	Q( glIsBuffer );
	Q( glBufferData );
	Q( glBufferSubdata );
	Q( glGetBufferSubdata );
	Q( glMapBuffer );
	Q( glUnmapBuffer );
	Q( glGetBufferParameteriv );
	Q( glGetBufferPointerv );
	Q( glBlendEquationSeparate );
	Q( glDrawBuffers );
	Q( glStencilOpSeparate );
	Q( glStencilFuncSeparate );
	Q( glStencilMaskSeparate );
	Q( glAttachShader );
	Q( glBindAttribLocation );
	Q( glCompileShader );
	Q( glCreateProgram );
	Q( glCreateShader );
	Q( glDeleteProgram );
	Q( glDeleteShader );
	Q( glDetachShader );
	Q( glDisableVertexAttribArray );
	Q( glEnableVertexAttribArray );
	Q( glGetActiveAttrib );
	Q( glGetAttachedShaders );
	Q( glGetAttribLocation );
	Q( glGetProgramiv );
	Q( glGetProgramInfoLog );
	Q( glGetShaderiv );
	Q( glGetShaderInfoLog );
	Q( glGetShaderSource );
	Q( glGetUniformLocation );
	Q( glGetUniformfv );
	Q( glGetUniformiv );
	Q( glGetVertexAttribdv );
	Q( glGetVertexAttribfv );
	Q( glGetVertexAttribiv );
	Q( glGetVertexAttribPointerv );
	Q( glIsProgram );
	Q( glIsShader );
	Q( glLinkProgram );
	Q( glShaderSource );
	Q( glUseProgram );
	Q( glUniform1f );
	Q( glUniform2f );
	Q( glUniform3f );
	Q( glUniform4f );
	Q( glUniform1i );
	Q( glUniform2i );
	Q( glUniform3i );
	Q( glUniform4i );
	Q( glUniform1fv );
	Q( glUniform2fv );
	Q( glUniform3fv );
	Q( glUniform4fv );
	Q( glUniform1iv );
	Q( glUniform2iv );
	Q( glUniform3iv );
	Q( glUniform4iv );
	Q( glUniformMatrix2fv );
	Q( glUniformMatrix3fv );
	Q( glUniformMatrix4fv );
	Q( glValidateProgram );
	Q( glVertexAttribPointer );
#undef Q

	// store the handles
	gGLDevCtx = devCtx;
	gGLRenderCtx = renderCtx;

	// list some opengl rendering information
	const char *versionStr = ( const char * )glGetString( GL_VERSION );
	const char *renderStr = ( const char * )glGetString( GL_RENDERER );
	const char *vendorStr = ( const char * )glGetString( GL_VENDOR );
	printf( "GL version: %s\nGL renderer: %s\nGL vendor: %s\n\n",
		versionStr, renderStr, vendorStr );

	// vertex data
	const vertex_t verts[] = {
		{ -1, -1, 0, 1, 0, 0 },
		{  0,  1, 0, 0, 1, 0 },
		{  1, -1, 0, 0, 0, 1 }
	};

	// create the VAO
	GLuint vao;

	glGenVertexArrays( 1, &vao );
	GL_CheckError();
	glBindVertexArray( vao );
	GL_CheckError();

	// create and initialize the VBO
	GLuint vbo;

	glGenBuffers( 1, &vbo );
	GL_CheckError();
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	GL_CheckError();
	glBufferData( GL_ARRAY_BUFFER, sizeof( verts ), &verts[ 0 ],
		GL_STATIC_DRAW );
	GL_CheckError();

	// create the vertex shader
	GLuint vertShader = glCreateShader( GL_VERTEX_SHADER );
	GL_CheckError();
	glShaderSource( vertShader, 1, &gDefVertShader, NULL );
	GL_CheckError();
	glCompileShader( vertShader );
	GL_CheckError();

	// create the fragment shader
	GLuint fragShader = glCreateShader( GL_FRAGMENT_SHADER );
	GL_CheckError();
	glShaderSource( fragShader, 1, &gDefFragShader, NULL );
	GL_CheckError();
	glCompileShader( fragShader );
	GL_CheckError();
	
	// create the program
	GLuint program = glCreateProgram();
	GL_CheckError();
	glAttachShader( program, vertShader );
	GL_CheckError();
	glAttachShader( program, fragShader );
	GL_CheckError();
	glLinkProgram( program );
	GL_CheckError();

	// get rid of the shaders
	glDeleteShader( vertShader );
	GL_CheckError();
	glDeleteShader( fragShader );
	GL_CheckError();

	// retrieve the attribute locations
	GLuint inPosLoc = glGetAttribLocation( program, "in_Position" );
	GL_CheckError();
	GLuint inColLoc = glGetAttribLocation( program, "in_Color" );
	GL_CheckError();

	// set the vertex attribute pointers
#define OFF( x ) ( ( void * )( char * )( x ) )
	glVertexAttribPointer( inPosLoc, 3, GL_FLOAT, GL_FALSE, sizeof( vertex_t ),
		OFF( 0 ) );
	GL_CheckError();
	glVertexAttribPointer( inColLoc, 4, GL_FLOAT, GL_FALSE, sizeof( vertex_t ),
		OFF( 12 ) );
	GL_CheckError();
#undef OFF

	// set the rendering program
	glUseProgram( program );
	GL_CheckError();

	// enable the vertex attribute arrays
	glEnableVertexAttribArray( inPosLoc );
	GL_CheckError();
	glEnableVertexAttribArray( inColLoc );
	GL_CheckError();
	
	// global setting
	gVAO = vao;
	
	// show the window
	ShowWindow( gWindowHandle, SW_SHOW );
}

void GL_Flip() {
	SwapBuffers( gGLDevCtx );
}

int main() {
	MSWIN_Init();
	GL_Init();

	while( MSWIN_Loop() ) {
		glViewport( 0, 0, gScreenResX, gScreenResY );

		glClearColor( 0.4f, 0.4f, 0.4f, 1.0f );
		glClearDepth( 1.0f );
		glClearStencil( -1 );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
			GL_STENCIL_BUFFER_BIT );

		glBindVertexArray( gVAO );
		glDrawArrays( GL_TRIANGLES, 0, 3 );

		GL_Flip();
	}

	return EXIT_SUCCESS;
}
