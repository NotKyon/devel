#include <stdio.h>
#include <stdlib.h>
#if _WIN32
# include <windows.h>
# if _MSC_VER
typedef unsigned __int64 u64_t;
# else
#  include <stdint.h>
typedef uint64_t u64_t;
# endif
#else
# include <sys/time.h>
# include <stdint.h>
typedef uint64_t u64_t;
#endif
#include <xmmintrin.h>
#include <emmintrin.h>
#include <math.h>

#ifndef __forceinline
# if __GNUC__ || __clang__
#  define __forceinline __inline __attribute__((always_inline))
# elif !_MSC_VER && !__INTEL_COMPILER
#  define __forceinline
# endif
#endif

typedef struct { float x, y, z, w; } vec4_t;

__forceinline float sse_dot(__m128 a, __m128 b) {
	__m128 r, t;

	/* t = xx, yy, zz, ww */
	t = _mm_mul_ps(a, b);

	/* r = yy, xx, zz, ww */
	r = _mm_shuffle_ps(t, t, _MM_SHUFFLE(1, 0, 3, 2));

	/* r = xx+zz, yy+ww, zz+xx, ww+yy (last two are redundant) */
	r = _mm_add_ps(r, t);

	/* t = yy+ww, xx+zz, xx+zz, xx+zz */
	t = _mm_shuffle_ps(r, r, _MM_SHUFFLE(0, 0, 0, 1));

	/* r = (xx+zz)+(yy+ww) */
	r = _mm_add_ss(r, t);

	/* done */
	return r[0];
}

__forceinline float fpu_dot(const vec4_t *a, const vec4_t *b) {
	return a->x*b->x + a->y*b->y + a->z*b->z + a->w*b->w;
}

__forceinline void sse_cross(vec4_t *d, __m128 a, __m128 b) {
	__m128 r, s, t;

	/* get the first columns (A=y,z,x; B=z,x,y) */
	s = _mm_shuffle_ps(a, a, _MM_SHUFFLE(0, 0, 2, 1));
	t = _mm_shuffle_ps(b, b, _MM_SHUFFLE(0, 1, 0, 2));

	/* multiply (Ay*Bz, Az*Bx, Ax*By) */
	r = _mm_mul_ps(s, t);

	/* get the second columns (A=z,x,y; B=y,z,x) */
	s = _mm_shuffle_ps(a, a, _MM_SHUFFLE(0, 1, 0, 2));
	t = _mm_shuffle_ps(b, b, _MM_SHUFFLE(0, 0, 2, 1));

	/* multiply (Az*By, Ax*Bz, Ay*Bx) */
	s = _mm_mul_ps(s, t);

	/* find the cross product */
	r = _mm_sub_ps(r, s);
	d->x = r[0];
	d->y = r[1];
	d->z = r[2];
}

__forceinline void fpu_cross(vec4_t *d, const vec4_t *a, const vec4_t *b) {
	d->x = a->y*b->z - a->z*b->y;
	d->y = a->z*b->x - a->x*b->z;
	d->z = a->x*b->y - a->y*b->x;
}

__forceinline void sse_cross4(vec4_t *d, __m128 a, __m128 b, __m128 c) {
	__m128 t[11];

	/* (Bz,Bz,By,By)*(Cw,Cw,Cw,Cz) */
	t[0] = _mm_mul_ps(_mm_shuffle_ps(b, b, _MM_SHUFFLE(1, 1, 2, 2)),
	                  _mm_shuffle_ps(c, c, _MM_SHUFFLE(2, 3, 3, 3)));

	/* (Cz,Cz,Cy,Cy)*(Bw,Bw,Bw,Bz) */
	t[1] = _mm_mul_ps(_mm_shuffle_ps(c, c, _MM_SHUFFLE(1, 1, 2, 2)),
	                  _mm_shuffle_ps(b, b, _MM_SHUFFLE(2, 3, 3, 3)));

	/* (By,Bx,Bx,Bx)*(Cw,Cw,Cw,Cz) */
	t[2] = _mm_mul_ps(_mm_shuffle_ps(b, b, _MM_SHUFFLE(0, 0, 0, 1)),
	                  _mm_shuffle_ps(c, c, _MM_SHUFFLE(2, 3, 3, 3)));

	/* (Cy,Cx,Cx,Cx)*(Bw,Bw,Bw,Bz) */
	t[3] = _mm_mul_ps(_mm_shuffle_ps(c, c, _MM_SHUFFLE(0, 0, 0, 1)),
	                  _mm_shuffle_ps(b, b, _MM_SHUFFLE(2, 3, 3, 3)));

	/* (By,Bx,Bx,Bx)*(Cz,Cz,Cy,Cy) */
	t[4] = _mm_mul_ps(_mm_shuffle_ps(b, b, _MM_SHUFFLE(0, 0, 0, 1)),
	                  _mm_shuffle_ps(c, c, _MM_SHUFFLE(1, 1, 2, 2)));

	/* (Cy,Cx,Cx,Cy)*(Bz,Bz,By,By) */
	t[5] = _mm_mul_ps(_mm_shuffle_ps(c, c, _MM_SHUFFLE(1, 0, 0, 1)),
	                  _mm_shuffle_ps(b, b, _MM_SHUFFLE(1, 1, 2, 2)));

	/* ((Bz,Bz,By,By)*(Cw,Cw,Cw,Cz) - (Cz,Cz,Cy,Cy)*(Bw,Bw,Bw,Bz))
	 * *(Ay,Ax,Ax,Ax)
	 */
	t[6] = _mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(0, 0, 0, 1)),
	                  _mm_sub_ps(t[0], t[1]));

	/* ((By,Bx,Bx,Bx)*(Cw,Cw,Cw,Cz) - (Cy,Cx,Cx,Cx)*(Bw,Bw,Bw,Bz))
	 * *(Az,Az,Ay,Ay)
	 */
	t[7] = _mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(1, 1, 2, 2)),
	                  _mm_sub_ps(t[2], t[3]));

	/* ((By,Bx,Bx,Bx)*(Cz,Cz,Cy,Cy) - (Cy,Cx,Cx,Cy)*(Bz,Bz,By,By))
	 * *(Aw,Aw,Aw,Aw)
	 */
	t[8] = _mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 3, 3, 3)),
	                  _mm_sub_ps(t[4], t[5]));

	/* <first> - <second> + <third> */
	t[9] = _mm_add_ps(_mm_sub_ps(t[6], t[7]), t[8]);

	/* prepare the result (signs: +,-,+,-) */
	t[10] = _mm_mul_ps(_mm_set_ps(-1,1,-1,1), t[9]);

	/* done! */
	_mm_store_ps((float *)d, t[10]);
}
__forceinline void fpu_cross4(vec4_t *d, const vec4_t *a, const vec4_t *b,
const vec4_t *c) {
	float Ax=a->x, Ay=a->y, Az=a->z, Aw=a->w;
	float Bx=b->x, By=b->y, Bz=b->z, Bw=b->w;
	float Cx=c->x, Cy=c->y, Cz=c->z, Cw=c->w;

	d->x =  (Ay*(Bz*Cw - Cz*Bw) - Az*(By*Cw - Cy*Bw) + Aw*(By*Cz - Cy*Bz));
	d->y = -(Ax*(Bz*Cw - Cz*Bw) - Az*(Bx*Cw - Cx*Bw) + Aw*(Bx*Cz - Cx*Bz));
	d->z =  (Ax*(By*Cw - Cy*Bw) - Ay*(Bx*Cw - Cx*Bw) + Aw*(Bx*Cy - Cx*By));
	d->w = -(Ax*(By*Cz - Cy*Bz) - Ay*(Bx*Cz - Cx*Bz) + Aw*(Bx*Cy - Cy*By));
}

#define TB_RES 65536 /*this table uses 0.25 megabytes at TB_RES of 65536*/
#define PI 3.1415926535897932384626433832795028841971693993751058209
static float g_sin_tb[TB_RES];
void init_tb() {
	int i;

	for(i=0; i<TB_RES; i++)
		g_sin_tb[i] = (float)sin(((double)i/(double)TB_RES)*PI*2);
}
__forceinline float sse_sin(float deg) {
	return g_sin_tb[((unsigned int)(deg/360.0f*(float)TB_RES))%TB_RES];
}
__forceinline float sse_cos(float deg) {
	return sse_sin(deg + 90.0f);
}
__forceinline void sse_sincos(float *r, float deg) {
	r[0] = sse_sin(deg);
	r[1] = sse_cos(deg);
}

__forceinline float fpu_sin(float deg) {
	return sinf(deg/180.0f*(float)PI);
}
__forceinline float fpu_cos(float deg) {
	return sinf(deg/180.0f*(float)PI);
}
__forceinline void fpu_sincos(float *r, float deg) {
	float x = deg/180.0f*(float)PI;

	r[0] = sinf(x);
	r[1] = cosf(x);
}

__forceinline __m128 sse_matrow(__m128 v, __m128 a, __m128 b, __m128 c,
__m128 d) {
	__m128 t, r, s;

	/* multiply Vx with the A column (VxAx, VxAy, VxAz, VxAw) */
	s = _mm_shuffle_ps(v, v, _MM_SHUFFLE(0, 0, 0, 0));
	r = _mm_mul_ps(s, a);

	/* multiply Vy with the B column (VyBx, VyBy, VyBz, VyBw) */
	s = _mm_shuffle_ps(v, v, _MM_SHUFFLE(1, 1, 1, 1));
	t = _mm_mul_ps(s, b);

	/* sum the results of Vx(Axyzw) and Bx(Axyzw) to R */
	r = _mm_add_ps(r, t);

	/* multiply Vz with the C column (VzCx, VzCy, VzCz, VzCw) */
	s = _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 2, 2, 2));
	t = _mm_mul_ps(s, c);

	/* sum the result of Vz(Cxyzw) to R */
	r = _mm_add_ps(r, t);

	/* multiply Vw with the D column (VwDx, VwDy, VwDz, VwDw) */
	s = _mm_shuffle_ps(v, v, _MM_SHUFFLE(3, 3, 3, 3));
	t = _mm_mul_ps(s, d);

	/* sum the results of Vw(Dxyzw) to R */
	r = _mm_add_ps(r, t);

	/* done */
	return r;
}
__forceinline void sse_matmult(vec4_t o[4], const __m128 a[4],
const __m128 b[4]) {
#if 0
	__m128 t[4];

	/* NOT WORKING */

	t[0] = sse_matrow(b[0], a[0], a[1], a[2], a[3]);
	t[1] = sse_matrow(b[1], a[0], a[1], a[2], a[3]);
	t[2] = sse_matrow(b[2], a[0], a[1], a[2], a[3]);
	t[3] = sse_matrow(b[3], a[0], a[1], a[2], a[3]);

	_mm_store_ps((float *)&o[0], t[0]);
	_mm_store_ps((float *)&o[1], t[1]);
	_mm_store_ps((float *)&o[2], t[2]);
	_mm_store_ps((float *)&o[3], t[3]);
#else
	__m128 x, y, z, w, tx, ty, tz, tw;

	tx = _mm_unpacklo_ps(b[0], b[1]);
	ty = _mm_unpackhi_ps(b[0], b[1]);
	tz = _mm_unpacklo_ps(b[2], b[3]);
	tw = _mm_unpackhi_ps(b[2], b[3]);
	x = _mm_movelh_ps(tx, tz);
	y = _mm_movehl_ps(tz, tx);
	z = _mm_movelh_ps(ty, tw);
	w = _mm_movehl_ps(tw, ty);

	tx = _mm_shuffle_ps(a[0], a[0], _MM_SHUFFLE(0,0,0,0));
	ty = _mm_shuffle_ps(a[1], a[1], _MM_SHUFFLE(0,0,0,0));
	tz = _mm_shuffle_ps(a[2], a[2], _MM_SHUFFLE(0,0,0,0));
	tw = _mm_shuffle_ps(a[3], a[3], _MM_SHUFFLE(0,0,0,0));
	_mm_store_ps((float *)&o[0],
		_mm_add_ps(_mm_add_ps(_mm_mul_ps(tx, x), _mm_mul_ps(ty, y)),
		           _mm_add_ps(_mm_mul_ps(tz, z), _mm_mul_ps(tw, w))));

	tx = _mm_shuffle_ps(a[0], a[0], _MM_SHUFFLE(1,1,1,1));
	ty = _mm_shuffle_ps(a[1], a[1], _MM_SHUFFLE(1,1,1,1));
	tz = _mm_shuffle_ps(a[2], a[2], _MM_SHUFFLE(1,1,1,1));
	tw = _mm_shuffle_ps(a[3], a[3], _MM_SHUFFLE(1,1,1,1));
	_mm_store_ps((float *)&o[1],
		_mm_add_ps(_mm_add_ps(_mm_mul_ps(tx, x), _mm_mul_ps(ty, y)),
		           _mm_add_ps(_mm_mul_ps(tz, z), _mm_mul_ps(tw, w))));

	tx = _mm_shuffle_ps(a[0], a[0], _MM_SHUFFLE(2,2,2,2));
	ty = _mm_shuffle_ps(a[1], a[1], _MM_SHUFFLE(2,2,2,2));
	tz = _mm_shuffle_ps(a[2], a[2], _MM_SHUFFLE(2,2,2,2));
	tw = _mm_shuffle_ps(a[3], a[3], _MM_SHUFFLE(2,2,2,2));
	_mm_store_ps((float *)&o[2],
		_mm_add_ps(_mm_add_ps(_mm_mul_ps(tx, x), _mm_mul_ps(ty, y)),
		           _mm_add_ps(_mm_mul_ps(tz, z), _mm_mul_ps(tw, w))));

	tx = _mm_shuffle_ps(a[0], a[0], _MM_SHUFFLE(3,3,3,3));
	ty = _mm_shuffle_ps(a[1], a[1], _MM_SHUFFLE(3,3,3,3));
	tz = _mm_shuffle_ps(a[2], a[2], _MM_SHUFFLE(3,3,3,3));
	tw = _mm_shuffle_ps(a[3], a[3], _MM_SHUFFLE(3,3,3,3));
	_mm_store_ps((float *)&o[3],
		_mm_add_ps(_mm_add_ps(_mm_mul_ps(tx, x), _mm_mul_ps(ty, y)),
		           _mm_add_ps(_mm_mul_ps(tz, z), _mm_mul_ps(tw, w))));
#endif
}

__forceinline void fpu_matmult(vec4_t o[4], const vec4_t a[4],
const vec4_t b[4]) {
	o[0].x = a[0].x*b[0].x + a[1].x*b[0].y + a[2].x*b[0].z + a[3].x*b[0].w;
	o[0].y = a[0].x*b[1].x + a[1].x*b[1].y + a[2].x*b[1].z + a[3].x*b[1].w;
	o[0].z = a[0].x*b[2].x + a[1].x*b[2].y + a[2].x*b[2].z + a[3].x*b[2].w;
	o[0].w = a[0].x*b[3].x + a[1].x*b[3].y + a[2].x*b[3].z + a[3].x*b[3].w;

	o[1].x = a[0].y*b[0].x + a[1].y*b[0].y + a[2].y*b[0].z + a[3].y*b[0].w;
	o[1].y = a[0].y*b[1].x + a[1].y*b[1].y + a[2].y*b[1].z + a[3].y*b[1].w;
	o[1].z = a[0].y*b[2].x + a[1].y*b[2].y + a[2].y*b[2].z + a[3].y*b[2].w;
	o[1].w = a[0].y*b[3].x + a[1].y*b[3].y + a[2].y*b[3].z + a[3].y*b[3].w;

	o[2].x = a[0].z*b[0].x + a[1].z*b[0].y + a[2].z*b[0].z + a[3].z*b[0].w;
	o[2].y = a[0].z*b[1].x + a[1].z*b[1].y + a[2].z*b[1].z + a[3].z*b[1].w;
	o[2].z = a[0].z*b[2].x + a[1].z*b[2].y + a[2].z*b[2].z + a[3].z*b[2].w;
	o[2].w = a[0].z*b[3].x + a[1].z*b[3].y + a[2].z*b[3].z + a[3].z*b[3].w;

	o[3].x = a[0].w*b[0].x + a[1].w*b[0].y + a[2].w*b[0].z + a[3].w*b[0].w;
	o[3].y = a[0].w*b[1].x + a[1].w*b[1].y + a[2].w*b[1].z + a[3].w*b[1].w;
	o[3].z = a[0].w*b[2].x + a[1].w*b[2].y + a[2].w*b[2].z + a[3].w*b[2].w;
	o[3].w = a[0].w*b[3].x + a[1].w*b[3].y + a[2].w*b[3].z + a[3].w*b[3].w;
}

__forceinline float sse_rsqrt(float x) {
#if 0
	/* This SSE version just isn't worth it. */
	__m128 r;
	int i;

	i = 0x5F3759DF - ((*(int *)&x) >> 1);
	r = _mm_load_ss((float *)&i);

	r = _mm_mul_ss(r, _mm_sub_ss(_mm_set_ss(1.5f),
	                             _mm_mul_ss(_mm_set_ss(0.5f),
	                                        _mm_mul_ss(_mm_load_ss(&x),
	                                                   _mm_mul_ss(r, r)))));
	_mm_store_ss(&x, r);
	return x;
#elif 1
	/* this is faster than the other method */
	return _mm_store_ss(&x, _mm_rsqrt_ss(_mm_load_ss(&x))), x;
#else
	union { float f; int i; } v;

	v.f = x;
	v.i = 0x5F3759DF - (v.i >> 1);

	return v.f*(1.5f - 0.5f*x*v.f*v.f);
#endif
}
__forceinline float fpu_rsqrt(float x) {
	return 1.0f/sqrtf(x);
}

int    A1,B1,C1,D1,E1,F1;
double A2,B2,C2,D2,E2,F2;
float  A3,B3,C3,D3,E3,F3;

__forceinline float sse_sqrt(float x) {
#if 0
	/* this is faster than the SSE method */
	union { float f; int i; } v;

	v.f   = x;
	v.i  -= 0x3F800000;
	v.i >>= 1;
	v.i  += 0x3F800000;

	return v.f;
#elif 0
	return _mm_store_ss(&x, _mm_sqrt_ss(_mm_load_ss(&x))), x;
#else
	return A3*(x + E3)*C3 + B3*x*D3 + C3*(x + F3)*E3;
#endif
}
__forceinline float fpu_sqrt(float x) {
	return sqrtf(x);
}

/*__forceinline int sse_clamp(int x) {*/
__forceinline float sse_clamp(float x) {
#if 0
	/* this version is slower */
	asm
	(
	"andl $0x7FFFFFFF, %[a]\n\t"   /* do one test (|a| <= |1|) instead of two (a>=-1 && a<=1) */
	"sub  %[a], %[c]\n\t"          /* '1.0 - a' will set the sign bit if 'a' is > 1.0 */
	"shrl $31, %[c]\n\t"           /* move that sign bit down */
	"neg  %[c]\n\t"                /* negate (-1=0xFFFFFFFF, -0=0x00000000) */
	"and  %[c], %[d]\n\t"          /* if outside of range, clamp to -1 or 1 */
	"mov  %[d], %[A]\n\t"          /* set to the output value */
	"xorl $0xFFFFFFFF, %[c]\n\t"   /* flip the bits (if -1 then 0; if 0 then -1) */
	"and  %[b], %[c]\n\t"          /* set to the original value if inside the range */
	"or   %[c], %[A]\n\t"          /* add to the output */
	: [A] "=r" (x)
	: [a] "r" (x),
	  [b] "r" (x),
	  [c] "r" (0x3F800000 /*1.0f*/),
	  [d] "r" ((0x80000000&*(int *)&x)|0x3F800000)
	);

	return x;
#elif 0
	/* mixed results; sometimes slower, sometimes faster */
	if ((x&0x7FFFFFFF) > 0x3F800000)
		x = (x&0x80000000) | 0x3F800000;

	return x;
#elif 1
	/* faster than both gcc and clang */
	_mm_store_ss(&x, _mm_min_ss(_mm_max_ss(_mm_set_ss(x),_mm_set_ss(-1.0f)),
		_mm_set_ss(1.0f)));
	return x;
#else
# error "Select a method!"
#endif
}
__forceinline float fpu_clamp(float x) {
	return x<-1 ? -1 : x>1 ? 1 : x;
}

__forceinline int sse_abs(int x) {
	return x ^ (-((x & 0x80000000) >> 31));
}
__forceinline int fpu_abs(int x) {
	return x < 0 ? -x : x;
}

__forceinline float sse_fabs(float x) {
	union { float f; int i; } v;

	v.f = x;
	v.i &= 0x7FFFFFFF;

	return v.f;
}
__forceinline float fpu_fabs(float x) {
	return fabs(x);
}

__forceinline float sse_sel(float f, float x, float y) {
	unsigned int msk, res;

	msk = -(((*(unsigned int *)&f)>>31)&1);
	res = ((msk)&*(unsigned int *)&y)|((~msk)&*(unsigned int *)&x);

	return *(float *)&res;
}
__forceinline float fpu_sel(float f, float x, float y) {
	return f < 0.0f ? y : x;
}

static const unsigned long long mask_int = 0x7FFFFFFFFFFFFFFFULL;
__m128d mask;
__forceinline double sse_diffabsaccum(double a, double b) {
	_mm_store_sd(&a, _mm_and_pd(_mm_sub_pd(_mm_load_pd(&a), _mm_load_pd(&b)), mask));
	return a;
}
__forceinline double fpu_diffabsaccum(double a, double b) {
	return fabs(a - b);
}

__forceinline double sse_muldiv(double a, double b) {
	return (double)(((int)(a))/A1/B1/C1 + ((int)(b))/D1/E1/F1);
}
__forceinline double fpu_muldiv(double a, double b) {
	return a*A2*B2*C2 + b*D2*E2*F2;
}

__forceinline int sse_clear() {
	__asm__ __volatile__("xorl %eax, %eax");
}
__forceinline int fpu_clear() {
	__asm__ __volatile__("movl $0, %eax");
}

__forceinline void sse_norm( float *dstx, float *dsty, float p1x, float p1y,
float p2x, float p2y ) {
	float dx = p2x - p1x;
	float dy = p2y - p1y;
	float dz = 0;
	
	float ex = 0;
	float ey = 0;
	float ez = 1;
	
	float rx = dy*ez - dz*ey;
	float ry = dz*ex - dx*ez;
	float rz = dx*ey - dy*ex;
	
	//float invMag = 1.0f/sqrtf( dx*dx + dy*dy );
	float invMag = 1.0f/sqrtf( rx*rx + ry*ry + rz*rz );
	
	//float vx = -dy*invMag;
	//float vy = dx*invMag;
	float vx = invMag*rx;
	float vy = invMag*ry;
	
	*dstx = vx;
	*dsty = vy;
}
__forceinline void fpu_norm( float *dstx, float *dsty, float p1x, float p1y,
float p2x, float p2y ) {
	float result = atan2( p2x - p1x, p2y - p1y );
#if 1
	float xnorm = cos( result );
	float ynorm = sin( result );
#else
	float xnorm = result*2.6161;
	float ynorm = result*1.5123;
#endif
	
	*dstx = xnorm;
	*dsty = ynorm;
}

__forceinline double timer() {
#if _WIN32
	u64_t t, f;

	QueryPerformanceCounter((LARGE_INTEGER *)&t);
	QueryPerformanceFrequency((LARGE_INTEGER *)&f);

	return (double)t/(double)f;
#else
	struct timeval tv;

	gettimeofday(&tv, (void *)0);

	return (double)tv.tv_sec + ((double)tv.tv_nsec)/1000000000.0;
#endif
}

float rnd(float l, float h) {
	return l + ((float)(rand() % 256000)/256000.0f)*(h - l);
}

#if 1
# define NUM_RUNS 10240
# define NUM_TESTS 2048
#else
# define NUM_RUNS 1
# define NUM_TESTS 8
#endif

#define DOT_PRODUCT 0 /* sse:32.054721 seconds; fpu:31.758225 seconds */
#define CROSS_PRODUCT 1 /* sse:58.257577 seconds; fpu:59.147841 seconds */
#define MATRIX_MULTIPLY 2 /* sse:3.928469683 minutes; fpu:4.18722065 minutes */
#define CROSS_PRODUCT4 3
#define SIN_COS 4
#define RSQRT 5
#define CLAMP 6
#define SQRT 7
#define ABS 8
#define FABS 9
#define FSEL 10
#define DIFFABSACCUM 11
#define MULDIV 12
#define CLEAR 13
#define NORM 14

static const char *g_testnames[] = {
	"dot product",
	"cross product",
	"matrix multiply",
	"cross product 4",
	"sine/cosine",
	"reciprocal square root",
	"clamp",
	"square root",
	"integer absolute",
	"floating-point absolute",
	"floating-point selection",
	"difference absolute accumulation",
	"double multiply versus integer divide",
	"xor versus mov clear",
	"normal calculation"
};

#ifndef TEST
# define TEST NORM
#endif

#if TEST==DOT_PRODUCT || TEST==RSQRT || TEST==CLAMP || TEST==SQRT || \
TEST==FABS || TEST==FSEL
static float g_result[NUM_TESTS][2];
#elif TEST==CROSS_PRODUCT || TEST==CROSS_PRODUCT4
static vec4_t g_vresult[NUM_TESTS][2];
#elif TEST==MATRIX_MULTIPLY
static vec4_t g_mresult[NUM_TESTS][4][2];
#elif TEST==SIN_COS
static float g_result[NUM_TESTS][2][2];
#elif TEST==ABS || TEST==CLEAR
static int g_result[NUM_TESTS][2];
#elif TEST==DIFFABSACCUM || TEST==MULDIV
static double g_result[2];
#elif TEST==NORM
static float g_result[NUM_TESTS][2][2];
#else
# error RESULTS
#endif

#if TEST==DOT_PRODUCT || TEST==CROSS_PRODUCT
static vec4_t g_vecs[NUM_TESTS][2];
static __m128 g_ssevecs[NUM_TESTS][2];
#elif TEST==CROSS_PRODUCT4
static vec4_t g_vecs[NUM_TESTS][3];
static __m128 g_ssevecs[NUM_TESTS][3];
#elif TEST==MATRIX_MULTIPLY
static vec4_t g_mats[NUM_TESTS][4][2];
static __m128 g_ssemats[NUM_TESTS][4][2];
#elif TEST==SIN_COS
static float g_degs[NUM_TESTS];
#elif TEST==RSQRT || TEST==CLAMP || TEST==SQRT || TEST==FABS
static float g_vals[NUM_TESTS];
#elif TEST==FSEL
static float g_vals[NUM_TESTS][3];
#elif TEST==ABS
static int g_vals[NUM_TESTS];
#elif TEST==DIFFABSACCUM || TEST==MULDIV
static double g_vals1[NUM_TESTS];
static double g_vals2[NUM_TESTS];
#elif TEST==NORM
static float g_vals[NUM_TESTS][4];
#else
# error INPUTS
#endif

int main() {
	double s, e, a[2];
	size_t i, j;
	FILE *f;

#if TEST==DOT_PRODUCT || TEST==CROSS_PRODUCT
	for(i=0; i<NUM_TESTS; i++) {
		for(j=0; j<2; j++) {
			g_vecs[i][j].x = rnd(-1000.0f, 1000.0f);
			g_vecs[i][j].y = rnd(-1000.0f, 1000.0f);
			g_vecs[i][j].z = rnd(-1000.0f, 1000.0f);
			g_vecs[i][j].w = rnd(-1000.0f, 1000.0f);

			g_ssevecs[i][j][0] = g_vecs[i][j].x;
			g_ssevecs[i][j][1] = g_vecs[i][j].y;
			g_ssevecs[i][j][2] = g_vecs[i][j].z;
			g_ssevecs[i][j][3] = g_vecs[i][j].w;
		}
	}
#elif TEST==CROSS_PRODUCT4
	for(i=0; i<NUM_TESTS; i++) {
		for(j=0; j<3; j++) {
			g_vecs[i][j].x = rnd(-1000.0f, 1000.0f);
			g_vecs[i][j].y = rnd(-1000.0f, 1000.0f);
			g_vecs[i][j].z = rnd(-1000.0f, 1000.0f);
			g_vecs[i][j].w = rnd(-1000.0f, 1000.0f);

			g_ssevecs[i][j][0] = g_vecs[i][j].x;
			g_ssevecs[i][j][1] = g_vecs[i][j].y;
			g_ssevecs[i][j][2] = g_vecs[i][j].z;
			g_ssevecs[i][j][3] = g_vecs[i][j].w;
		}
	}
#elif TEST==MATRIX_MULTIPLY
	for(i=0; i<NUM_TESTS; i++) {
		for(j=0; j<8; j++) {
			g_mats[i][j&3][(j&4)>>2].x = rnd(-1000.0f, 1000.0f);
			g_mats[i][j&3][(j&4)>>2].y = rnd(-1000.0f, 1000.0f);
			g_mats[i][j&3][(j&4)>>2].z = rnd(-1000.0f, 1000.0f);
			g_mats[i][j&3][(j&4)>>2].w = rnd(-1000.0f, 1000.0f);

			g_ssemats[i][j&3][(j&4)>>2][0] = g_mats[i][j&3][(j&4)>>2].x;
			g_ssemats[i][j&3][(j&4)>>2][1] = g_mats[i][j&3][(j&4)>>2].y;
			g_ssemats[i][j&3][(j&4)>>2][2] = g_mats[i][j&3][(j&4)>>2].z;
			g_ssemats[i][j&3][(j&4)>>2][3] = g_mats[i][j&3][(j&4)>>2].w;
		}
	}
#elif TEST==SIN_COS
	init_tb();

	for(i=0; i<NUM_TESTS; i++) {
		g_degs[i] = rnd(-8192.0f, 8192.0f);
	}
#elif TEST==RSQRT || TEST==SQRT
	for(i=0; i<NUM_TESTS; i++) {
		g_vals[i] = rnd(1.0f, 65536.0f);
	}
#elif TEST==CLAMP
	for(i=0; i<NUM_TESTS; i++) {
		g_vals[i] = rnd(-2.0f, 2.0f);
	}
#elif TEST==ABS
	for(i=0; i<NUM_TESTS; i++) {
		g_vals[i] = (int)rnd(-3000.0f, 3000.0f);
	}
#elif TEST==FABS
	for(i=0; i<NUM_TESTS; i++) {
		g_vals[i] = rnd(-3000.0f, 3000.0f);
	}
#elif TEST==FSEL
	for(i=0; i<NUM_TESTS; i++) {
		g_vals[i][0] = rnd(-3000.0f, 3000.0f);
		g_vals[i][1] = rnd(-3000.0f, 3000.0f);
		g_vals[i][2] = rnd(-3000.0f, 3000.0f);
	}
#elif TEST==DIFFABSACCUM || TEST==MULDIV
	for(i=0; i<NUM_TESTS; i++) {
		g_vals1[i] = rnd(-3000.0f, 3000.0f);
		g_vals2[i] = rnd(-3000.0f, 3000.0f);
	}
#elif TEST==NORM
	for(i=0; i<NUM_TESTS; i++) {
		g_vals[i][0] = rnd(-3000.0f, 3000.0f);
		g_vals[i][1] = rnd(-3000.0f, 3000.0f);
		g_vals[i][2] = rnd(-3000.0f, 3000.0f);
		g_vals[i][3] = rnd(-3000.0f, 3000.0f);
	}
#endif

	a[0] = 0.0;
	a[1] = 0.0;

	A2 = rnd(1.0f, 1000.0f);
	B2 = rnd(1.0f, 1000.0f);
	C2 = rnd(1.0f, 1000.0f);
	D2 = rnd(1.0f, 1000.0f);
	E2 = rnd(1.0f, 1000.0f);
	F2 = rnd(1.0f, 1000.0f);

	A1 = (int)A2;
	B1 = (int)B2;
	C1 = (int)C2;
	D1 = (int)D2;
	E1 = (int)E2;
	F1 = (int)F2;

	printf("Running %s test...\n", g_testnames[TEST]);
	fflush(stdout);

#if TEST==DOT_PRODUCT
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][0] = sse_dot(g_ssevecs[i][0], g_ssevecs[i][1]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][1] = fpu_dot(&g_vecs[i][0], &g_vecs[i][1]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==CROSS_PRODUCT
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			sse_cross(&g_vresult[i][0], g_ssevecs[i][0], g_ssevecs[i][1]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			fpu_cross(&g_vresult[i][1], &g_vecs[i][0], &g_vecs[i][1]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==CROSS_PRODUCT4
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			sse_cross4(&g_vresult[i][0], g_ssevecs[i][0], g_ssevecs[i][1],
				g_ssevecs[i][2]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			fpu_cross4(&g_vresult[i][1], &g_vecs[i][0], &g_vecs[i][1],
				&g_vecs[i][2]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==MATRIX_MULTIPLY
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			sse_matmult(g_mresult[i][0], g_ssemats[i][0], g_ssemats[i][1]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			fpu_matmult(g_mresult[i][1], g_mats[i][0], g_mats[i][1]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==SIN_COS
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			sse_sincos(g_result[i][0], g_degs[i]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			fpu_sincos(g_result[i][1], g_degs[i]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==RSQRT
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][0] = sse_rsqrt(g_vals[i]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][1] = fpu_rsqrt(g_vals[i]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==CLAMP
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++) {
# if 0
			int x = sse_clamp(*(int *)&g_vals[i]);
			g_result[i][0] = *(float *)&x;/*sse_clamp(*(int *)&g_vals[i]);*/
# else
			g_result[i][0] = sse_clamp(g_vals[i]);
# endif
		}
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][1] = fpu_clamp(g_vals[i]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==SQRT
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][0] = sse_sqrt(g_vals[i]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][1] = fpu_sqrt(g_vals[i]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==ABS
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][0] = sse_abs(g_vals[i]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][1] = fpu_abs(g_vals[i]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==FABS
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][0] = sse_fabs(g_vals[i]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][1] = fpu_fabs(g_vals[i]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==FSEL
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][0] = sse_sel(g_vals[i][0], g_vals[i][1], g_vals[i][2]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][1] = fpu_sel(g_vals[i][0], g_vals[i][1], g_vals[i][2]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==DIFFABSACCUM
	mask = _mm_set1_pd(*(const double *)&mask_int);
	for(j=0; j<NUM_RUNS; j++) {
		g_result[0] = g_result[1] = 0.0f;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[0] += sse_diffabsaccum(g_vals1[i], g_vals2[i]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[1] += fpu_diffabsaccum(g_vals1[i], g_vals2[i]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==MULDIV
	for(j=0; j<NUM_RUNS; j++) {
		g_result[0] = g_result[1] = 0.0f;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[0] += sse_muldiv(g_vals1[i], g_vals2[i]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[1] += fpu_muldiv(g_vals1[i], g_vals2[i]);
		e = timer();
		a[1] += e - s;
	}
#elif TEST==CLEAR
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][0] = sse_clear();
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			g_result[i][1] = fpu_clear();
		e = timer();
		a[1] += e - s;
	}
#elif TEST==NORM
	for(j=0; j<NUM_RUNS; j++) {
		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			sse_norm(
				&g_result[i][0][0], &g_result[i][1][0],
				g_vals[i][0], g_vals[i][1],
				g_vals[i][2], g_vals[i][3]);
		e = timer();
		a[0] += e - s;

		s = timer();
		for(i=0; i<NUM_TESTS; i++)
			fpu_norm(
				&g_result[i][0][1], &g_result[i][1][1],
				g_vals[i][0], g_vals[i][1],
				g_vals[i][2], g_vals[i][3]);
		e = timer();
		a[1] += e - s;
	}
#else
# error "No test selected."
#endif

	printf("SSE Total Time  : %f second(s)\n", a[0]);
	printf("FPU Total Time  : %f second(s)\n", a[1]);
	printf("\n");

	printf("SSE Average Time: %f second(s)\n", a[0] / NUM_RUNS);
	printf("FPU Average Time: %f second(s)\n", a[1] / NUM_RUNS);

	printf("Writing report to 'results.txt'...\n");
	fflush(stdout);

	if ((f = fopen("results.txt", "wb")) != (FILE *)0) {
		fprintf(f, "SSE Total Time  : %f second(s)\n", a[0]);
		fprintf(f, "FPU Total Time  : %f second(s)\n", a[1]);
		fprintf(f, "\n");

		fprintf(f, "SSE Average Time: %f second(s)\n", a[0] / NUM_RUNS);
		fprintf(f, "FPU Average Time: %f second(s)\n", a[1] / NUM_RUNS);
		fprintf(f, "\n");

		for(i=0; i<NUM_TESTS; i++) {
#if TEST==DOT_PRODUCT
			fprintf(f, "sse: %f (%f,%f,%f,%f;%f,%f,%f,%f);\n",
				g_result[i][0],
				g_ssevecs[i][0][0],g_ssevecs[i][0][1],g_ssevecs[i][0][2],
				g_ssevecs[i][0][3],
				g_ssevecs[i][1][0],g_ssevecs[i][1][1],g_ssevecs[i][1][2],
				g_ssevecs[i][1][3]);
			fprintf(f, "fpu: %f (%f,%f,%f,%f;%f,%f,%f,%f);\n",
				g_result[i][1],
				g_vecs[i][0].x,g_vecs[i][0].y,g_vecs[i][0].z,g_vecs[i][0].w,
				g_vecs[i][1].x,g_vecs[i][1].y,g_vecs[i][1].z,g_vecs[i][1].w);
#elif TEST==CROSS_PRODUCT
			fprintf(f, "sse: %f,%f,%f (%f,%f,%f;%f,%f,%f);\n",
				g_vresult[i][0].x,g_vresult[i][0].y,g_vresult[i][0].z,
				g_ssevecs[i][0][0],g_ssevecs[i][0][1],g_ssevecs[i][0][2],
				g_ssevecs[i][1][0],g_ssevecs[i][1][1],g_ssevecs[i][1][2]);
			fprintf(f, "fpu: %f,%f,%f (%f,%f,%f;%f,%f,%f);\n",
				g_vresult[i][1].x,g_vresult[i][1].y,g_vresult[i][1].z,
				g_vecs[i][0].x,g_vecs[i][0].y,g_vecs[i][0].z,
				g_vecs[i][1].x,g_vecs[i][1].y,g_vecs[i][1].z);
#elif TEST==CROSS_PRODUCT4
			fprintf(f, "sse: %f,%f,%f,%f (%f,%f,%f,%f;%f,%f,%f,%f;"
				"%f,%f,%f,%f);\n",
				g_vresult[i][0].x,g_vresult[i][0].y,g_vresult[i][0].z,
				g_vresult[i][0].w,
				g_ssevecs[i][0][0],g_ssevecs[i][0][1],g_ssevecs[i][0][2],
				g_ssevecs[i][0][3],g_ssevecs[i][1][0],g_ssevecs[i][1][1],
				g_ssevecs[i][1][2],g_ssevecs[i][1][3],g_ssevecs[i][2][0],
				g_ssevecs[i][2][1],g_ssevecs[i][2][2],g_ssevecs[i][2][3]);
			fprintf(f, "fpu: %f,%f,%f,%f (%f,%f,%f,%f;%f,%f,%f,%f;"
				"%f,%f,%f,%f);\n",
				g_vresult[i][1].x,g_vresult[i][1].y,g_vresult[i][1].z,
				g_vresult[i][1].w,
				g_vecs[i][0].x,g_vecs[i][0].y,g_vecs[i][0].z,g_vecs[i][0].w,
				g_vecs[i][1].x,g_vecs[i][1].y,g_vecs[i][1].z,g_vecs[i][1].w,
				g_vecs[i][2].x,g_vecs[i][2].y,g_vecs[i][2].z,g_vecs[i][2].w);
#elif TEST==MATRIX_MULTIPLY
			fprintf(f, "sse: %f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f;\n"
				"fpu: %f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f;\n",
				g_mresult[i][0][0].x,g_mresult[i][0][0].y,g_mresult[i][0][0].z,
				g_mresult[i][0][0].w,g_mresult[i][0][1].x,g_mresult[i][0][1].y,
				g_mresult[i][0][1].z,g_mresult[i][0][1].w,g_mresult[i][0][2].x,
				g_mresult[i][0][2].y,g_mresult[i][0][2].z,g_mresult[i][0][2].w,
				g_mresult[i][0][3].x,g_mresult[i][0][3].y,g_mresult[i][0][3].z,
				g_mresult[i][0][3].w,g_mresult[i][1][0].x,g_mresult[i][1][0].y,
				g_mresult[i][1][0].z,g_mresult[i][1][0].w,g_mresult[i][1][1].x,
				g_mresult[i][1][1].y,g_mresult[i][1][1].z,g_mresult[i][1][1].w,
				g_mresult[i][1][2].x,g_mresult[i][1][2].y,g_mresult[i][1][2].z,
				g_mresult[i][1][2].w,g_mresult[i][1][3].x,g_mresult[i][1][3].y,
				g_mresult[i][1][3].z,g_mresult[i][1][3].w);
#elif TEST==SIN_COS
			fprintf(f, "sse: %f, %f (%f);\n",
				g_result[i][0][0],g_result[i][0][1],g_degs[i]);
			fprintf(f, "fpu: %f, %f (%f);\n",
				g_result[i][1][0],g_result[i][1][1],g_degs[i]);
#elif TEST==RSQRT || TEST==CLAMP || TEST==SQRT || TEST==FABS
			fprintf(f, "sse: %f (%f);\n", g_result[i][0],g_vals[i]);
			fprintf(f, "fpu: %f (%f);\n", g_result[i][1],g_vals[i]);
#elif TEST==ABS
			fprintf(f, "sse: %i (%i);\n", g_result[i][0],g_vals[i]);
			fprintf(f, "fpu: %i (%i);\n", g_result[i][1],g_vals[i]);
#elif TEST==FSEL
			fprintf(f, "sse: %f (%f,%f,%f);\n",
				g_result[i][0],g_vals[i][0],g_vals[i][1],g_vals[i][2]);
			fprintf(f, "fpu: %f (%f,%f,%f);\n",
				g_result[i][1],g_vals[i][0],g_vals[i][1],g_vals[i][2]);
#elif TEST==DIFFABSACCUM || TEST==MULDIV
			fprintf(f, "sse: %f\n", g_result[0]);
			fprintf(f, "fpu: %f\n", g_result[1]);
#elif TEST==CLEAR
			fprintf(f, "sse: %i;\n", g_result[i][0]);
			fprintf(f, "fpu: %i;\n", g_result[i][1]);
#elif TEST==NORM
			fprintf(f, "sse: %f,%f (%f,%f,%f,%f);\n",
				g_result[i][0][0], g_result[i][1][0],
				g_vals[i][0],g_vals[i][1],g_vals[i][2],g_vals[i][3]);
			fprintf(f, "fpu: %f,%f (%f,%f,%f,%f);\n",
				g_result[i][0][1], g_result[i][1][1],
				g_vals[i][0],g_vals[i][1],g_vals[i][2],g_vals[i][3]);
#else
# error "Don't forget to write results for this test."
#endif
			fprintf(f, "\n");
		}
		fclose(f);
	} else
		perror("failed to write to 'results.txt'");

	return 0;
}
