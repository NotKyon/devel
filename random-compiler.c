#include <assert.h>
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct lineInfo_t {
	unsigned int offset;
	unsigned int line;
	unsigned int column;
};

void Lex_GetLineInfo( lineInfo_t *linfo, const char *base,
const char *curr ) {
	assert( linfo != NULL );
	assert( base != NULL );
	assert( curr != NULL );

	linfo->offset = curr - base;

	linfo->line = 1;
	const char *s = base;
	for( const char *p = base; p < curr; ++p ) {
		if( *p == '\n' ) {
			++linfo->line;
			s = p;
		}
	}
	
	linfo->offset = p - s; //should be offset by +1 as required
}
void Lex_Error( const char *filename, const lineInfo_t *linfo,
const char *message ) {
	assert( filename != NULL );
	assert( linfo != NULL );
	assert( linfo->line > 0 );
	assert( linfo->column > 0 );
	assert( message != NULL );

	fprintf( stderr, "[%s(%i:%i)] ERROR: %s\n", filename, linfo->line,
		linfo->column, message );
}
const char *Lex_SkipWhitespace( const char *p ) {
	assert( p != NULL );

	while( *p <= ' ' ) {
		++p;
	}

	return p;
}
const char *Lex_SkipLine( const char *p ) {
	assert( p != NULL );

	while( *p != '\n' && *p != '\0' ) {
		++p;
	}

	return p;
}

int Lex_IsAlpha( char ch ) {
	if( ch >= 'a' && ch <= 'z' ) {
		return 1;
	}
	if( ch >= 'A' && ch <= 'Z' ) {
		return 1;
	}

	return 0;
}
int Lex_IsDigit( char ch ) {
	if( ch >= '0' && ch <= '9' ) {
		return 1;
	}

	return 0;
}
int Lex_IsIdentifierBase( char ch ) {
	if( ch == '_' ) {
		return 1;
	}

	return Lex_IsAlpha( ch );
}
int Lex_IsIdentifier( char ch ) {
	if( Lex_IsDigit( ch ) ) {
		return 1;
	}

	return Lex_IsIdentifierBase( ch );
}

/*
	FIXME: Although these will work, they don't account for needed delimiters,
	-      so you could have completely invalid statements read in and processed
	-      without error, like 1.03e+27fsomeIdentifier (would return two
	-      separate tokens)
*/

const char *Lex_SkipIdentifier( const char *p ) {
	if( !Lex_IsIdentifierBase( *p ) ) {
		return NULL;
	}

	++p;
	while( Lex_IsIdentifier( *p ) ) {
		++p;
	}

	return p;
}
const char *Lex_SkipNumber( const char *p ) {
	while( !Lex_IsDigit( *p ) ) {
		return NULL;
	}

	++p;
	while( Lex_IsDigit( *p ) ) {
		++p;
	}

	if( *p == '.' ) {
		++p;
		
		while( Lex_IsDigit( *p ) ) {
			++p;
		}
	}

	if( *p == 'e' || *p == 'E' ) {
		++p;
		
		if( *p == '-' || *p == '+' ) {
			++p;
		}
		
		while( Lex_IsDigit( *p ) ) {
			++p;
		}
	}

	if( *p == 'f' || *p == 'F' ) {
		++p;
	}
}
const char *Lex_SkipString( const char *p ) {
	if( *p != '\"' ) {
		return NULL;
	}

	++p;
	while( *p != '\"' ) {
		if( *p == '\0' ) {
			break;
		}

		if( *p == '\\' ) {
			++p;
		}
		
		++p;
	}
	
	return p;
}

char *Lex_OpenFile( const char *filename ) {
	FILE *fp;
	int e;

	fp = fopen( filename, "r" );
	if( !fp ) {
		e = errno;

		fprintf( stderr, "[%s] ERROR: Could not open file: %s (%i)\n", filename,
			strerror( e ), e );
		return NULL;
	}
	
	fseek( fp, 0, SEEK_END );
	unsigned int size = ftell( fp );
	fseek( fp, 0, SEEK_SET );

	char *p = ( char * )malloc( size + 1 );
	if( !p ) {
		e = errno;

		fprintf( stderr, "[%s] ERROR: Failed to allocate memory: %s (%i)\n",
			filename, strerror( e ), e );
		
		fclose( fp );
		return NULL;
	}
	
	if( !fread( ( void * )p, size, 1, fp ) ) {
		e = errno;
		
		fprintf( stderr, "[%s] ERROR: Could not read from file: %s (%i)\n",
			filename, strerror( e ), e );

		fclose( fp );
		return NULL;
	}

	fclose( fp );

	p[ size ] = '\0';
	return p;
}
const char *Lex_Close( const char *base ) {
	free( p );
	return NULL;
}

const char *Parse_SkipWhitespace( const char *base ) {
	const char *p;
	int x;
	
	p = base;
	do {
		x = 0;
		p = Lex_SkipWhitespace( base );

		// handle single-line comments
		if( *p == '/' && *( p + 1 ) == '/' ) {
			p = Lex_SkipLine( p );
			x = 1;
			continue;
		}
		
		// handle multi-line comments
		if( *p == '/' && *( p + 1 ) == '*' ) {
			const char *q;
			
			x = 1;
			q = strstr( p, "*/" );
			if( !q ) {
				x = 0;
				q = strchr( p, '\0' );
			}
			
			p = q;
		}
	} while( x == 1 );
	
	return p;
}
void Parse_LiftDefinitions( const char *filename, const char *base ) {
}

int main( int argc, char **argv ) {
	return EXIT_SUCCESS;
}
