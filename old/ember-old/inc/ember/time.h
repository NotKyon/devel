#ifndef EMBER_TIME_H
#define EMBER_TIME_H

typedef uint64_t timestamp_t;

void time_init();
void time_update();
float time_tween();
void time_yield();

#endif
