//
//	This demonstrates how to use the dictionary system, which allows you to
//	store key/value pairs.
//
#define GB_INIT 1
#include <gamebase.h>

//
//	We'll describe what sort of value we're storing.
//
struct Value {
	int a, b;
};

//
//	Also put the dictionary somewhere we can find it...
//
gb_dictionary_t g_dict;

//
//	Create a new key (or set if it already exists)
//
void NewKey(const char *key, Value *v) {
	gbNewEntry(&g_dict, key, (void *)v);
}

//
//	Set a value to a key
//	Get a value from a key
//
void SetKey(const char *key, Value *v) {
	gbSetEntry(&g_dict, key, (void *)v);
}
Value *GetKey(const char *key) {
	return (Value *)gbGetEntry(&g_dict, key);
}

//
//	Helper function to display the value by key name
//
void PrintKey(const char *key) {
	Value *p;

	p = GetKey(key);

	if (!p) {
		gbPrint(gbF("key '%s' was not set!", key));
		return;
	}

	gbPrint(gbF("%s -> (%i, %i)", key, p->a, p->b));
}

//
//	Here's the main test
//
void GBMain() {
	Value *a, *b, *c;

	// we need to initialize the dictionary...
	// only allow lowercase alphabetical characters (a-z)
	gbInitDict(&g_dict, GB_DICT_LOWER);

	// some testing...
	a = (Value *)gbMemAlloc(sizeof(*a));
	b = (Value *)gbMemAlloc(sizeof(*b));
	c = (Value *)gbMemAlloc(sizeof(*c));

	NewKey("ape", (Value *)NULL);
	NewKey("apple", b);
	NewKey("crate", c);

	SetKey("ape", a);

	// values can be modified after they're set (pointers)
	a->a = 1;
	b->a = 2;
	c->a = 3;
	a->b = 4;
	b->b = 5;
	c->b = 6;

	// display them
	PrintKey("ape");
	PrintKey("apple");
	PrintKey("crate");
	PrintKey("a"); //will display that 'a' wasn't set, correctly

	// finalize
	gbFiniDict(&g_dict);

	// cleanup
	a = (Value *)gbMemFree((void *)a);
	b = (Value *)gbMemFree((void *)b);
	c = (Value *)gbMemFree((void *)c);
}
