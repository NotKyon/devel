#include <vector>
#include <string>
#include <stddef.h> //for size_t
#include <string.h> //for strlen, strstr, strchr, etc

#include <stdio.h> //for printf
#include <stdlib.h> //for EXIT_SUCCESS

std::vector< std::string > explode( const char *pszText, const char *pszDelim )
{
	std::vector< std::string > results;
	const char *s = pszText;

	const size_t cDelim = strlen( pszDelim );

	for(;;) {
		const char *p = strstr( s, pszDelim );
		if( !p ) {
			p = strchr( s, '\0' );
		}

		results.push_back( std::string( s, size_t( p - s ) ) );
		if( *p == '\0' ) {
			break;
		}

		s = p + cDelim;
	}

	return results;
}

void test( const char *pszText, const char *pszDelim )
{
	const char *pszDispDelim = pszDelim;
	if( strcmp( pszDelim, "\n" ) == 0 ) { pszDispDelim = "\\n"; }
	if( strcmp( pszDelim, " " ) == 0 ) { pszDispDelim = "' '"; }

	printf( "Converting \"%s\" (with delim=\"%s\")...\n", pszText, pszDispDelim );

	std::vector< std::string > results = explode( pszText, pszDelim );
	for( std::vector< std::string >::iterator i = results.begin(); i != results.end(); ++i ) {
		printf( "\t<%s>\n", i->c_str() );
	}

	printf( "Done!\n\n" );
}

int main()
{
	test( "hello;my;fair;lady", ";" );
	return EXIT_SUCCESS;
}
