/*
==========================================================================
	Simple Static Water Surface using scaled and translated bumpmaps for
		normal generation.
	The lighting model uses a cubemap reflection and refraction model blended
		by the fresnel term.

==========================================================================
*/

// ==== ** PRIMARY STRUCTURES ** ==== //

struct vertex_t {
	float3 position : POSITION;
	float3 normal : NORMAL;
	float4 tangent : TANGENT;
	float3 bitangent : BINORMAL;
	float2 texcoord0 : TEXCOORD0;
	float4 color : COLOR;
};
struct fragment_t {
	float4 position : POSITION;
	float4 color : COLOR;
	float4 texcoord0 : TEXCOORD0;
	float4 texcoord1 : TEXCOORD1;
	float4 texcoord2 : TEXCOORD2;
};
struct rasterizer_t {
	float4 color : COLOR0;
};

// ==== ** TEXTURES ** ==== //
texture envMap_tex <
	//string ResourceName = "../../env/skytest2a.dds";
	string ResourceName = "../../env/gskydome.dds";
	string ResourceType = "CUBE";
>;
texture sparebumpmap_tex <
	string ResourceName = "../../textures/water/waves1_local.png";
	string ResourceType = "2D";
>;
texture sparebumpmap2_tex <
	string ResourceName = "../../textures/water/waves2_local.png";
	string ResourceType = "2D";
>;

samplerCUBE dynamicEnvMap = sampler_state {
	Texture = <envMap_tex>;
    MinFilter = Linear;
    MipFilter = Linear;
    MagFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};
sampler2D spareBumpMap = sampler_state {
	Texture = <sparebumpmap_tex>;
    MinFilter = Linear;
    MipFilter = Linear;
    MagFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};
sampler2D spareBumpMap2 = sampler_state {
	Texture = <sparebumpmap2_tex>;
    MinFilter = Linear;
    MipFilter = Linear;
    MagFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};

// ==== ** TWEAKABLES ** ==== //

// .x=Transmission coefficient for air/water interface, .y = 1-x
float4 waterFresnel = float4( 0.0204, 0.9795, 0.0, 0.0 );

// .xy = scale, .zw = translate
float4 waveBumpScaleBias1 = float4( 0.050, 0.050, -0.025, -0.002 );
float4 waveBumpScaleBias2 = float4( 0.070, 0.070,  0.030, -0.009 );
float4 waveBumpScaleBias3 = float4( 1.000, 1.000,  0.050, -0.010 );
float4 waveBumpScaleBias4 = float4( 0.095, 0.095,  0.040, -0.030 );

float4 deepWaterColor     = float4( 0.010, 0.010,  0.010,  0.040 );
float4 shallowWaterColor  = float4( 0.025, 0.030,  0.040,  0.040 );
float4 reflectionColor    = float4( 1.000, 1.000,  1.000,  1.000 );
float4 reflectionAmount   = float4( 0.800, 0.800,  0.800,  0.800 );
float4 waterAmount        = float4( 0.700, 0.700,  0.700,  0.700 );

// ==== ** UNTWEAKABLES ** ==== //

float time : Time;

float4x4 mvpMatrix : WorldViewProjection;
float4x4 inverseModelMatrix : WorldInverse;
float4x4 globalViewOriginMatrix : ViewInverse;

//float4 localViewOrigin = mul( globalViewOriginMatrix[ 3 ].xyzw, inverseModelMatrix );


// ==== ** VERTEX PROGRAM ** ==== //

fragment_t waterSurfaceScroll_vp( in vertex_t vertex ) {

	fragment_t result;
	
	result.position = mul( float4( vertex.position.xyz, 1 ), mvpMatrix );
	
	float3 normal = vertex.normal.xyz*2.0 - 1.0;
	float4 tangent = vertex.tangent*2.0 - 1.0;

	normal.xyz = normalize( normal.xyz );
	tangent.xyz = normalize( tangent.xyz - normal.xyz*dot( normal.xyz, tangent.xyz ) );
	
	float3 bitangent = cross( normal.xyz, tangent.xyz )*tangent.w;
	
	// Transform local view vector into tangent space
	//float3 r0 = localViewOrigin.xyz - vertex.position.xyz;
	float3 r0 = mul( globalViewOriginMatrix[ 3 ].xyzw, inverseModelMatrix ).xyz - vertex.position.xyz;
	result.texcoord0.x = dot( tangent.xyz,   r0 );
	result.texcoord0.y = dot( bitangent.xyz, r0 );
	result.texcoord0.z = dot( normal.xyz,    r0 );
	result.texcoord0.w = result.position.w;
	
	// Scale texture coordinates to get mix of low/high frequency details
	result.texcoord1.xy = frac( waveBumpScaleBias1.zw*time ) + vertex.texcoord0.xy*waveBumpScaleBias1.xy;
	result.texcoord1.zw = frac( waveBumpScaleBias2.zw*time ) + vertex.texcoord0.xy*waveBumpScaleBias2.xy;
	result.texcoord2.xy = frac( waveBumpScaleBias3.zw*time ) + vertex.texcoord0.xy*waveBumpScaleBias3.xy;
	result.texcoord2.zw = frac( waveBumpScaleBias4.zw*time ) + vertex.texcoord0.xy*waveBumpScaleBias4.xy;
	
	result.color = vertex.color;
	
	return result;
	
}

// ==== ** FRAGMENT PROGRAM ** ==== //

rasterizer_t waterSurfaceScroll_fp( in fragment_t fragment ) {

	rasterizer_t result;
	
	// normalize tangent space view vector
	float3 viewT = normalize( fragment.texcoord0.xyz );
	
	// Average the bump layers
	float3 localNormal;
	
	localNormal.xy  = tex2D( spareBumpMap,  fragment.texcoord1.xy ).xy;
	localNormal.xy += tex2D( spareBumpMap,  fragment.texcoord1.zw ).xy;
	localNormal.xy += tex2D( spareBumpMap2, fragment.texcoord2.xy ).xy;
	localNormal.xy += tex2D( spareBumpMap2, fragment.texcoord2.zw ).xy;
	localNormal.xyz = normalize( half3( 2.0*localNormal.xy - 4.0, 4.0 ) );
	
	// Apply individual bump scales for refraction and reflection vectors
	float3 vRefrBump = localNormal.xyz*half3( 0.15, 0.15, 1.0 );
	float3 vReflBump = localNormal.xyz*half3( 0.20, 0.20, 1.0 );
	
	float4 R;
	R.xyz = reflect( -viewT.xyz, vReflBump.xyz );
	R.w = 0.0;
	
	float4 RF;
	RF.xyz = refract( -viewT.xyz, vRefrBump.xyz, 1.33 );
	RF.w = 0.0;
	
	half4 vReflection = texCUBElod( dynamicEnvMap, R );
	half4 vRefraction = texCUBElod( dynamicEnvMap, RF );
	
	half angle = max( dot( viewT, vReflBump ), 0.0 );
	half facing = ( 1.0 - angle );
	
	// Use distance to lerp between refraction and deep water color
	half4 deepColor = lerp( deepWaterColor, vRefraction, saturate( 10.0/fragment.texcoord0.w ) );
	
	// Lerp between water color and deep water color
	half4 waterColor = lerp( deepColor, shallowWaterColor, facing )*waterAmount;
	half4 reflectColor = vReflection*reflectionColor*reflectionAmount*facing;
	
	// Calculate specular term
	half specTerm = dot( localNormal.xyz*0.8, half3( 1.0, 1.0, 1.0 ) - viewT.xyz );
	specTerm = ( specTerm > 0 ) ? pow( specTerm, 4.0 ) : 0.0;
	
	result.color = reflectColor + waterColor + ( specTerm*half4( 0.1, 0.1, 0.1, 0.0 ) );
	
	return result;
	
}

// ==== ** TECHNIQUE ** ==== //

technique waterSurfaceScroll {

	pass waterSurfaceScroll {
	
		VertexShader = compile vs_3_0 waterSurfaceScroll_vp();
		PixelShader = compile ps_3_0 waterSurfaceScroll_fp();
	
	}

}

technique waterSurfaceScroll_shadow {

	pass waterSurfaceScroll_shadow {
	
		VertexShader = compile vs_3_0 waterSurfaceScroll_vp();
		StencilEnable = True;
		StencilFunc = Less;
		StencilRef = 1;
		StencilZFail = Keep;
		StencilFail = Keep;
		StencilPass = Keep;
		PixelShader = compile ps_3_0 waterSurfaceScroll_fp();
	
	}

}
