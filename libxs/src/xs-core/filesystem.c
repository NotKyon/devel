#include <xs/filesystem.h>
#include <xs/array.h>
#include <xs/assert.h>
#include <xs/memory.h>
#include <xs/string.h>

#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

static xs_array_t g_fs_dirstack = (xs_array_t)0;

static xs_array_t DirStack() {
	if (!g_fs_dirstack)
		g_fs_dirstack = XsNewArray();

	return g_fs_dirstack;
}

char *XsGetCWD(char *cwd, xs_size_t n) {
	char *p;

	XS_ASSERT(cwd != (char *)0);
	XS_ASSERT(n > 1);

	if (!(p = getcwd(cwd, n)))
		return (char *)0;

#if _WIN32
	while((p=strchr(p, '\\')) != (char *)0)
		*p = '/';
#endif

	return cwd;
}

int XsEnterDir(const char *path) {
	xs_path_t cwd;

	if (!XsGetCWD(cwd, sizeof(cwd)))
		return 0;

	XsArrayPush(DirStack(), cwd);

	if (chdir(path)==-1)
		return 0;

	return 1;
}
int XsExitDir() {
	xs_size_t i;

	XS_ASSERT(g_fs_dirstack != (xs_array_t)0);
	XS_ASSERT(XsArraySize(g_fs_dirstack) > 0);

	i = XsArraySize(g_fs_dirstack) - 1;

	if (chdir(XsArrayElement(g_fs_dirstack, i))==-1)
		return 0;

	XsSetArrayElement(g_fs_dirstack, i, (const char *)0);
	XsResizeArray(g_fs_dirstack, i);

	return 1;
}

int XsIsFile(const char *path) {
	struct stat s;

	if (stat(path, &s)==-1)
		return 0;

	if (!S_ISREG(s.st_mode)) {
		errno = S_ISDIR(s.st_mode) ? EISDIR : EBADF;
		return 0;
	}

	return 1;
}
int XsIsDir(const char *path) {
	struct stat s;

	if (stat(path, &s)==-1)
		return 0;

	if (!S_ISDIR(s.st_mode)) {
		errno = ENOTDIR;
		return 0;
	}

	return 1;
}

int XsMakeDirs(const char *dirs) {
	const char *p;
	xs_path_t buf;
	char *path;

	/* ignore the root directory */
	if (dirs[0]=='/') {
		buf[0] = dirs[0];
		path = &buf[1];
		p = &dirs[1];
	} else if(dirs[1]==':' && dirs[2]=='/') {
		buf[0] = dirs[0];
		buf[1] = dirs[1];
		buf[2] = dirs[2];
		path = &buf[3];
		p = &dirs[3];
	} else {
		path = &buf[0];
		p = &dirs[0];
	}

	/* make each directory, one by one */
	while(1) {
		if (*p=='/' || *p==0) {
			*path = 0;

			errno = 0;
#if _WIN32
			mkdir(buf);
#else
			mkdir(buf, S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH);
#endif
			if (errno && errno!=EEXIST)
				return 0;

			if (!(*path++ = *p++))
				break;

			if (*p==0)
				break;
		} else
			*path++ = *p++;

		if (path==&buf[sizeof(buf) - 1])
			return 0;
	}

	return 1;
}

char *XsRealPath(const char *filename, char *resolvedname, xs_size_t maxn) {
#if _WIN32
	static xs_path_t buf;
	xs_size_t i;
	DWORD r;

	if (!(r = GetFullPathNameA(filename, sizeof(buf), buf, (char **)0))) {
		errno = ENOSYS;
		return (char *)0;
	}

	if (r >= (DWORD)maxn) {
		errno = ERANGE;
		return (char *)0;
	}

	for(i=0; i<sizeof(buf); i++) {
		if (!buf[i])
			break;

		if (buf[i]=='\\')
			buf[i] = '/';
	}

	if (buf[i]==':') {
		if (buf[0]>='A' && buf[0]<='Z')
			buf[0] = buf[0] - 'A' + 'a';
	}

	XsStrCpy(resolvedname, maxn, buf);
	return resolvedname;
#else /*#elif __linux__||__linux||linux*/
	xs_size_t namepart;
	xs_path_t dir;
	char *p;
	int e;

	XsStrCpy(dir, sizeof(dir), filename);
	if ((p = strrchr(dir, '/')) != (char *)0) {
		namepart = (p + 1) - dir;
		if (*(p + 1)==0)
			*p = 0;
		else
			*(p + 1) = 0;
	} else {
		namepart = 0;
		p = "./";
	}

	if (!XsEnterDir(dir))
		return (char *)0;

	if (!XsGetCWD(dir, sizeof(dir)))
		return (char *)0;

	XsExitDir();

	XsStrCat(dir, sizeof(dir), &filename[namepart]);
	e = errno;
	if (XsIsDir(dir))
		XsStrCat(dir, sizeof(dir), "/");
	errno = e; /*errno will be set if XsIsDir() fails, don't let it*/

	XsStrCpy(resolvedname, maxn, dir);
	return resolvedname;
#endif
}

const char *XsGetDir(char *buf, xs_size_t n, const char *filename) {
	const char *p;

	XS_ASSERT(buf != (char *)0);
	XS_ASSERT(n > 1);
	XS_ASSERT(filename != (const char *)0);

	if ((p = strrchr(filename, '/')) != (const char *)0) {
		XsStrCpyN(buf, n, filename, (p - filename) + 1);
		return &p[1];
	}

	*buf = 0;
	return filename;
}

void XsExtSubst(char *dst, xs_size_t dstn, const char *src, const char *ext) {
	const char *p;

	XS_ASSERT(dst != (char *)0);
	XS_ASSERT(dstn > 1);
	XS_ASSERT(ext != (const char *)0);

	p = strrchr(src, '.');
	if (!p)
		p = strchr(src, 0);

	XS_ASSERT(p != (const char *)0);

	XsStrCpyN(dst, dstn, src, p - src);
	XsStrCpy(&dst[p - src], dstn - (xs_size_t)(p - src), ext);
}
