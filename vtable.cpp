#include <cstdio>
#include <cstring>

#include <stddef.h>

extern "C" void *malloc(size_t n);
extern "C" void free(void *p);

class IVariable {
public:
	virtual void Retain() = 0;
	virtual void Release() = 0;
};

class CVariable: public IVariable {
public:
	CVariable(): m_refCnt(1) {}
	//virtual ~CVariable() {}

	virtual void Retain() { m_refCnt++; }
	virtual void Release() { if (--m_refCnt==0) delete this; }

protected:
	int m_refCnt;
};

struct IVariableLocal_Vtbl;
typedef struct IVariableLocal {
	struct IVariableLocal_Vtbl *vtbl;
	int m_refCnt;
} IVariableLocal;
struct IVariableLocal_Vtbl {
	void(*Retain)(IVariable *self);
	void(*Release)(IVariable *self);
};

void IVariableLocal_Retain(IVariableLocal *self) {
	printf("Retain\n");fflush(stdout);
	self->m_refCnt++;
}
void IVariableLocal_Release(IVariableLocal *self) {
	printf("Release\n");
	if (--self->m_refCnt<0) free((void *)self);
}

IVariableLocal_Vtbl IVariableLocal_VtblImpl[] = {
	(void(*)(IVariable *))IVariableLocal_Retain,
	(void(*)(IVariable *))IVariableLocal_Release
};

IVariable *NewVariable() {
	IVariableLocal *var;

	var = (IVariableLocal *)malloc(sizeof(*var));
	var->vtbl = IVariableLocal_VtblImpl;

	var->m_refCnt = 1;

	return (IVariable *)var;
}
void DeleteVariable(IVariable *var) {
	free((void *)var);
}

void IVariableLocalOverride_Retain(IVariableLocal *self) {
	printf("!!! RETAIN !!!\n");fflush(stdout);
	self->m_refCnt++;
}

#if CSTYLE
# define IVariable_Retain(self)\
	(((IVariableLocal *)(self))->vtbl->Retain(self))
# define IVariable_Release(self)\
	(((IVariableLocal *)(self))->vtbl->Release(self))
#else
# define IVariable_Retain(self) (self)->Retain()
# define IVariable_Release(self) (self)->Release()
#endif

typedef void(*fn_t)();

namespace Adder {
int GetSum(int x, int y) { return x + y; }
}

inline void DbgMsg(const char *file, int line, const char *fn, const char *x) {
	fprintf(stdout, "[%s(%i) %s] %s\n", file, line, fn, x);
	fflush(stdout);
}

#if DEBUG||_DEBUG||__debug__ || MK_DEBUG
# if (_MSC_VER || __INTEL_COMPILER) && !defined(__func__)
#  define __func__ __FUNCTION__
# endif
# define DBG(x) DbgMsg(__FILE__, __LINE__, __func__, (x))
#else
# define DBG(x)
#endif

int main() {
	IVariable *var;
	
#define TEST(x) printf("%s = %i\n", #x, (int)(x))
	TEST(sizeof(void *));

	TEST(sizeof(IVariable));
	TEST(sizeof(IVariableLocal));
	TEST(sizeof(CVariable));
#undef TEST

#if CSTYLE
	DBG("Allocating variable (CSTYLE)");
	var = NewVariable();

	DBG("Grabbing vtable");
	fn_t *vtable = *(fn_t **)var;

	DBG("Overwriting 'Retain' function");
	vtable[0] = (fn_t)IVariableLocalOverride_Retain;

	void(*IVariable_Retain)(IVariable *self);
	void(*IVariable_Release)(IVariable *self);

	DBG("Setting functions");
	*(fn_t *)&IVariable_Retain = vtable[0];
	*(fn_t *)&IVariable_Release = vtable[1];
#else
	DBG("Allocating variable (C++)");
	var = new CVariable();

	DBG("Retrieving vtable");
	fn_t *vtable = *(fn_t **)var;
	
	printf("vtable = %p\n", (void *)vtable);

	DBG("Overwriting 'Retain' function");
	vtable[0] = (fn_t)IVariableLocalOverride_Retain;
#endif

	DBG("Running test code...");
	IVariable_Retain(var);
	IVariable_Release(var);
	IVariable_Release(var);

	DBG("Done!");
	return 0;
}
