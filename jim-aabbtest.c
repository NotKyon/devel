typedef float vec2f[2];
typedef struct { vec2f center, halfSize; } AABB;
typedef enum { false, true } bool;

bool Method1(AABB *aabb, vec2f point)
{
	if(!aabb || !point) return false;
	float left = aabb->center[0] - aabb->halfSize[0];
	float right = aabb->center[0] + aabb->halfSize[0];
	float top = aabb->center[1] + aabb->halfSize[1];
	float bottom = aabb->center[1] - aabb->halfSize[1];
	if(point[0] >= left && point[0] <= right &&
	   point[1] >= bottom && point[1] <= top)
	{
		return true;
	}
	return false;
}
bool Method2(AABB *aabb, vec2f point)
{
#define left   (aabb->center[0] - aabb->halfSize[0])
#define right  (aabb->center[0] + aabb->halfSize[0])
#define top    (aabb->center[1] + aabb->halfSize[1])
#define bottom (aabb->center[1] - aabb->halfSize[1])
	if(!aabb || !point) return false;
	
	if(point[0] >= left && point[0] <= right &&
	   point[1] >= bottom && point[1] <= top)
	{
		return true;
	}
	return false;
#undef left
#undef right
#undef top
#undef bottom
}
