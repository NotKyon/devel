#if 0
void blah() {}
typedef void(*pfv_t)();

int N = 1;
int *A[] = { &N };
pfv_t B[] = { &blah, &blah, &blah };

const char *const p = "Hello";
const char *const P[] = { p, p, p };
#endif

#if 0
bool do_not_i( int x ) { return !x; }
bool do_not_p( const void *p ) { return !p; }

bool do_and_i( int x, int y ) { return x && y; }
bool do_and_p( const void *x, const void *y ) { return x && y; }

bool do_or_i( int x, int y ) { return x || y; }
bool do_or_p( const void *x, const void *y ) { return x || y; }
#endif

int deref( const int *x )
{
	return *x;
}
int pass()
{
	int x = 5;
	return deref( &x );
}

int sum( int x, int y )
{
	return x + y;
}
int test()
{
	int x = 2, y = 3;
	return sum( x, y );
}

int deref2( const int *x )
{
	return x[ 2 ];
}
int arrtest()
{
	int x[ 3 ];

	x[ 0 ] = 2;
	x[ 1 ] = 1;
	x[ 2 ] = 0;

	return deref( x );
}
