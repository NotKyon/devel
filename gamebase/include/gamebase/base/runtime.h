#ifndef GB_BASE_RUNTIME_H
#define GB_BASE_RUNTIME_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Runtime Debugging/Error Functions */
#if _MSC_VER||__INTEL_COMPILER
# undef __func__
# define __func__ __FUNCTION__
#endif

#ifndef GB_DEBUG
# if DEBUG||_DEBUG||__debug__ || MK_DEBUG
#  define GB_DEBUG 1
# endif
#endif

#if GB_DEBUG
# ifndef gbDebugStop
#  if MK_ARCH_X86||MK_ARCH_X86_64 || __i386__||__x86_64__ || _M_IX86||_M_IX64
#   if __GNUC__ || __clang__
#    define gbDebugStop() __asm__("int $3")
#   else
#    define gbDebugStop() __asm{int 3}
#   endif
#  elif __GNUC__ || __clang__
#   define gbDebugStop() __builtin_trap()
#  else
#   error "Please provide a gbDebugStop() macro for your platform."
#  endif
# else
#  define gbDebugStop()
# endif
# ifndef gbDebugLog
#  define gbDebugLog(x)\
	gbReport(kGBLogType_Debug, __FILE__,__LINE__,__func__, "%s", x)
# endif
#else
# ifndef gbDebugStop
#  define gbDebugStop()
# endif
# ifndef gbDebugLog
#  define gbDebugLog(x)
# endif
#endif

#if GB_DEBUG
# define gbAssert(x)\
	if(!(x)){gbError(__FILE__,__LINE__,__func__,"Assert Failure: %s",#x);\
		gbDebugStop();}
#else
# define gbAssert(x) /*do nothing!*/
#endif

const char *gbFV(const char *format, va_list args);
const char *gbF(const char *format, ...);

void gbRuntimeError(const char *message);

typedef void(*gb_fnOnEnd_t)();
void gbOnEnd(gb_fnPtr_t fnOnEnd);

void gbSetExitFlag(int flag);
int gbGetExitFlag();
void gbEnd();

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
