#include <cstdio>
#include <cstdlib>

class C {
protected:
	int m;

public:
	C(int x): m(x) {}
	C(const C &c): m(c.m) {}
	~C() {}

	void Print() { printf("%i\n", m); }

	C &operator++(   ) { ++m; return *this; }   //pre-fix
	C  operator++(int) { return C(m + 1);   }   //post-fix
};

int main() {
	C c(0);

	++c.Print();
	c++.Print();

	return EXIT_SUCCESS;
}
