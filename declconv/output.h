/*
 * Nuclear Fusion Plugin Loader File
 * Converted by declconv from "test.decls"
 * declconv was written by Aaron J. Miller <nocannedmeat@gmail.com> on June 7th, 2011.
 */

#ifndef _TEST_DECLS_
#define _TEST_DECLS_

/* function prototype macros */
#if defined(__cplusplus)||defined(cplusplus)
# define __D(v) =v
# ifdef NF_RUN_MODE
#  define __E extern "C"
# else
#  define __E extern "C" extern
# endif
#else
# define __D(v) /* empty */
# ifdef NF_RUN_MODE
#  define __E /* empty */
# else
#  define __E extern
# endif
#endif
#if defined(__GNUC__)
# define __Prototype(r,n) __E r __attribute__((stdcall)) (*__##n)
#else
# define __Prototype(r,n) __E r (__stdcall *__##n)
#endif

/* constants */
#define MY_CONST 42
#define MY_HEX_CONST 2147483647

/* function declarations */
__Prototype(int, get_sum)(int x, int y);
__Prototype(float, get_real_sum)(float x, float y);
__Prototype(const char *, get_sum_str)(int x, int y);
__Prototype(int, add_stuff)(int *a, int x, int y);
__Prototype(int, add_stuff2)(int *a, int x, int y);
__Prototype(int, pointer_func)(void *p);
__Prototype(void *, pointer_ret)(void *p);
#if defined(__cplusplus)||defined(cplusplus)
inline int get_sum(int x, int y) { return __get_sum(x, y); }
inline float get_real_sum(float x, float y) { return __get_real_sum(x, y); }
inline const char *get_sum_str(int x, int y) { return __get_sum_str(x, y); }
inline int add_stuff(int *a, int x, int y) { return __add_stuff(a, x, y); }
inline int add_stuff2(int *a, int x __D(2), int y __D(2)) { return __add_stuff2(a, x, y); }
inline int pointer_func(void *p) { return __pointer_func(p); }
inline void *pointer_ret(void *p) { return __pointer_ret(p); }
#else
# ifdef NF_RUN_MODE
#  define __F(r,n,p,c) r n p { return __##n(c); }
# else
#  define __F(r,n,p,c) extern r n p;
# endif
__F(int,get_sum,(int x, int y),(x, y))
__F(float,get_real_sum,(float x, float y),(x, y))
__F(const char *,get_sum_str,(int x, int y),(x, y))
__F(int,add_stuff,(int *a, int x, int y),(a, x, y))
__F(int,add_stuff2,(int *a, int x, int y),(a, x, y))
__F(int,pointer_func,(void *p),(p))
__F(void *,pointer_ret,(void *p),(p))
#endif

/* loader */
__E void LoadPlugin_Test(void);
#ifdef NF_RUN_MODE
void LoadPlugin_Test(void)
{
	NUKE p;

	if ((p=LoadPlugin("adder.dll")) == (NUKE)0)
		DebugError("Could not load 'adder.dll'");
	if ((*((void **)&__get_sum)=(void *)GetPluginFunction(p, "get_sum@8"))==(void *)0)
		DebugError("Could not load 'get_sum@8' from 'adder.dll'");
	if ((*((void **)&__get_real_sum)=(void *)GetPluginFunction(p, "get_real_sum@8"))==(void *)0)
		DebugError("Could not load 'get_real_sum@8' from 'adder.dll'");
	if ((*((void **)&__get_sum_str)=(void *)GetPluginFunction(p, "get_sum_str@8"))==(void *)0)
		DebugError("Could not load 'get_sum_str@8' from 'adder.dll'");
	if ((*((void **)&__add_stuff)=(void *)GetPluginFunction(p, "add_stuff@12"))==(void *)0)
		DebugError("Could not load 'add_stuff@12' from 'adder.dll'");
	if ((*((void **)&__add_stuff2)=(void *)GetPluginFunction(p, "add_stuff2@12"))==(void *)0)
		DebugError("Could not load 'add_stuff2@12' from 'adder.dll'");
	if ((*((void **)&__pointer_func)=(void *)GetPluginFunction(p, "pointer_func@4"))==(void *)0)
		DebugError("Could not load 'pointer_func@4' from 'adder.dll'");
	if ((*((void **)&__pointer_ret)=(void *)GetPluginFunction(p, "pointer_ret@4"))==(void *)0)
		DebugError("Could not load 'pointer_ret@4' from 'adder.dll'");
}
#endif

#undef __Prototype
#undef __E
#undef __D

#endif
