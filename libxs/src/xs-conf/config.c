#include <xs/config.h>
#include <xs/assert.h>
#include <xs/string.h>
#include <string.h>

struct xs_config_s {
	struct xs_section_s *head, *tail;
};

struct xs_section_s {
	struct xs_config_s *conf;

	char *name;

	struct xs_section_s *prnt, **p_head, **p_tail;
	struct xs_section_s *head, *tail;
	struct xs_section_s *prev, *next;

	struct xs_key_s *k_head, *k_tail;
};

struct xs_key_s {
	struct xs_section_s *sect;

	char *name;
	char *value;

	struct xs_key_s *prev, *next; /* linked list */
};

enum {
	kXsConfToken_EOF,

	kXsConfToken_Equals = '=',
	kXsConfToken_LBrace = '{',
	kXsConfToken_RBrace = '}',

	__kXsConfToken_Start__ = 256,

	kXsConfToken_Ident,
	kXsConfToken_String,

	kXsConfToken_Invalid = -1,
	kXsConfToken_Overflow = -2
};

static const char *TokenName(int token) {
#define CHECK(x,y) if(token==kXsConfToken_##x)return y
	CHECK(EOF, "end-of-file");
	CHECK(Equals, "'='");
	CHECK(LBrace, "'{'");
	CHECK(RBrace, "'}'");
	CHECK(Ident, "identifier");
	CHECK(String, "string");
	CHECK(Invalid, "(invalid)");
	CHECK(Overflow, "(overflow)");

	return "(unknown)";
#undef CHECK
}

static int Lex(xs_buffer_t buf, char *lexan, xs_size_t n) {
	/*static int nummap[256];*/
	static int tokens[256];
	static int didinit = 0;
	unsigned int c;
	xs_size_t i;
	/*int num, mul;*/

	XS_ASSERT(n > 1);

	if (!didinit) {
		memset(tokens, 0, sizeof(tokens));

		tokens['='] = kXsConfToken_Equals;

		tokens['{'] = kXsConfToken_LBrace;
		tokens['}'] = kXsConfToken_RBrace;

#if 0
		for(i=0; i<256; i++)
			nummap[i] = -1;

		for(i=(xs_size_t)'0'; i<=(xs_size_t)'9'; i++)
			nummap[i] = i - (xs_size_t)'0';

		for(i=(xs_size_t)'A'; i<=(xs_size_t)'Z'; i++)
			nummap[i] = i - (xs_size_t)'A' + 10;

		for(i=(xs_size_t)'a'; i<=(xs_size_t)'z'; i++)
			nummap[i] = i - (xs_size_t)'a' + 10;
#endif

		didinit = 1;
	}

	while(1) {
		XsSkipBufferWhitespace(buf);

		if (XsBufferEnd(buf))
			return kXsConfToken_EOF;

		c = (unsigned int)(unsigned char)XsPeekBuffer(buf);

		if (c=='#') {
			XsSkipBufferLine(buf);
			continue;
		}

		break;
	}

	if (tokens[c]) {
		lexan[0] = XsPullBuffer(buf);
		lexan[1] = 0;

		return tokens[c];
	}

	i = 0;

	if ((c>='a'&&c<='z')||(c>='A'&&c<='Z')||c=='_') {
		do {
			lexan[i++] = XsPullBuffer(buf);
			if (i + 1 >= n)
				return kXsConfToken_Overflow;
			c = (unsigned int)(unsigned char)XsPeekBuffer(buf);
		} while((c>='a'&&c<='z')||(c>='A'&&c<='Z')||(c>='0'&&c<='9')
		        ||c=='.'||c=='_');

		lexan[i] = 0;
		return kXsConfToken_Ident;
	} /* identifier */

	if (c=='\"') {
	    XsPullBuffer(buf); /*eat the quote*/

		while(XsPeekBuffer(buf)!='\"' && !XsBufferEnd(buf)) {
			c = (unsigned int)(unsigned char)XsPeekBuffer(buf);

			if (c=='\\') {
				char hex[2];
				int x;

				XsPullBuffer(buf);
				switch(XsPeekBuffer(buf)) {
				case '\'': c = '\''; XsPullBuffer(buf); break;
				case '\"': c = '\"'; XsPullBuffer(buf); break;
				case '\\': c = '\\'; XsPullBuffer(buf); break;
				case 'b': c = '\b'; XsPullBuffer(buf); break;
				case 'f': c = '\f'; XsPullBuffer(buf); break;
				case 'n': c = '\n'; XsPullBuffer(buf); break;
				case 'r': c = '\r'; XsPullBuffer(buf); break;
				case 't': c = '\t'; XsPullBuffer(buf); break;
				case 'v': c = '\v'; XsPullBuffer(buf); break;
				case 'x':
					XsPullBuffer(buf);

					c = XsPeekBuffer(buf);
					if (!((c>='0'&&c<='9')||(c>='a'&&c<='f')
					||(c>='A'&&c<='F'))) {
						XsWarnBuffer(buf, "Invalid hex code in string.");
						continue;
					}
					hex[0] = (char)XsPullBuffer(buf);

					c = XsPeekBuffer(buf);
					if (!((c>='0'&&c<='9')||(c>='a'&&c<='f')
					||(c>='A'&&c<='F'))) {
						XsWarnBuffer(buf, "Invalid hex code in string.");
						continue;
					}
					hex[1] = (char)XsPullBuffer(buf);

					x = (hex[0]>='0'&&hex[0]<='9') ? hex[0]-'0' :
					    (hex[0]>='a'&&hex[0]<='f') ? hex[0]-'a'+10 :
					    hex[0]-'A'+10;

					x <<= 4;

					x |= (hex[0]>='0'&&hex[0]<='9') ? hex[0]-'0' :
					     (hex[0]>='a'&&hex[0]<='f') ? hex[0]-'a'+10 :
					     hex[0]-'A'+10;

					c = (unsigned char)x;
				} /*switch*/
			} else /*if c=='\\'*/
				XsPullBuffer(buf);

			lexan[i++] = c;
			if (i + 1 >= n)
				return kXsConfToken_Overflow;
		} /*while*/

		XsPullBuffer(buf); /*eat the quote*/

		lexan[i] = 0;
		return kXsConfToken_String;
	} /* string */

#if 0
	num = 0;
	mul = 1;

	if (c=='-' || c=='+') {
		if (c=='-')
			mul = -1;

		XsPullBuffer(buf); /* eat the '-' */
		c = (unsigned int)(unsigned char)XsPeekBuffer(buf);
	}

	if (c>='0' && c<='9') {
		c = (unsigned int)(unsigned char)XsPullBuffer(buf); /* eat c */

		if (c=='0' && (XsPeekBuffer(buf)=='x' || XsPeekBuffer(buf)=='X')) {
			c = (unsigned int)(unsigned char)XsEatBuffer(buf);
			mul *= 16;
		} /* hex */

		/*
		 * TODO
		 */
	} /* number */
#endif

	XsWarnBuffer(buf, "Unknown character '%c' (0x%.2X) -- treating as EOF.",
		c, c);

	lexan[0] = 0;
	return kXsConfToken_EOF;
}

static char *SkipToNull(char *p) {
	if (!p)
		return (char *)0;

	return strchr(p, '\0');
}
static char *Append(char *p, xs_size_t *n, const char *s) {
	char *e;

	p = XsStrCpy(p, *n, s);
	e = SkipToNull(p);

	if (e && p)
		*n -= (e - p);

	return e;
}
static int ExpectAny(xs_buffer_t buf, int input, int n, ...) {
	xs_size_t l;
	va_list args;
	char expected[2048], *p;
	int i, token;

	l = sizeof(expected);

	p = Append(expected, &l, "Expected ");

	va_start(args, n);
	for(i=0; i<n; i++) {
		token = va_arg(args, int);

		if (input==token)
			return i;

		p = Append(p, &l, TokenName(token));
		if (i + 2 == n)
			p = Append(p, &l, ", or ");
		else if(i + 1 < n)
			p = Append(p, &l, ", ");
	}

	p = Append(p, &l, "; got ");
	p = Append(p, &l, TokenName(input));

	XsErrorBuffer(buf, "%s", expected);
	return -1;
}
static int Expect(xs_buffer_t buf, int input, int token) {
	return ExpectAny(buf, input, 1, token);
}

/*
 * Example config file:
 *
 *	Entity {
 *		name = "Boss"
 *		health = "200"
 *		maxHealth = "300"
 *		takeDamage = "true"
 *
 *		Weapons {
 *			slot0 = "Switchblade"
 *			slot1 = "BFG10K"
 *		}
 *	}
 */
static int Parse(xs_config_t conf, xs_buffer_t buf) {
	xs_section_t sect;
	char lexan[8192];
	char *ident;
	int token, r;

	sect = conf->head;

	while(1) {
		if ((token = Lex(buf, lexan, sizeof(lexan)))==kXsConfToken_Invalid) {
			XsErrorBuffer(buf, "Invalid token found.");
			return 0;
		}

		if (token==kXsConfToken_EOF)
			break;

		if ((r = ExpectAny(buf, token, 2,
		kXsConfToken_RBrace, kXsConfToken_Ident))==-1)
			return 0;

		if (r==0 /*rbrace*/) {
			if (sect==XsFirstRootSection(conf)) {
				XsErrorBuffer(buf, "No corresponding '}'");
				return 0;
			}

			sect = XsSectionParent(sect);

			XS_ASSERT(sect != (xs_section_t)0);

			continue;
		}

		ident = XsDuplicate(lexan);

		if ((token = Lex(buf, lexan, sizeof(lexan)))==kXsConfToken_Invalid) {
			XsErrorBuffer(buf, "Invalid token found.");
			XsMemory((void *)ident, 0);
			return 0;
		}

		if ((r = ExpectAny(buf, token, 2,
		kXsConfToken_LBrace, kXsConfToken_Equals))==-1)
			return 0;

		if (r==0 /*lbrace*/) {
			/*
			 * TODO: Add a marker of some kind here in case a corresponding '}'
			 *       isn't found.
			 */
			sect = XsNewSection(conf, sect, ident);
			ident = (char *)XsMemory((void *)ident, 0);
			continue;
		}

		/* it's an equal sign */
		XS_ASSERT(token==kXsConfToken_Equals);

		if ((token = Lex(buf, lexan, sizeof(lexan)))==kXsConfToken_Invalid) {
			XsErrorBuffer(buf, "Invalid token found.");
			XsMemory((void *)ident, 0);
			return 0;
		}

		if (Expect(buf, token, kXsConfToken_String)==-1)
			return 0;

		XsNewKey(sect, ident, lexan);
	}

	if (sect!=XsFirstRootSection(conf)) {
		/*
		 * TODO: Check the marker. (See the comment in the lbrace check.)
		 */
		XsErrorBuffer(buf, "Mismatched '}'");
		return 0;
	}

	return 1;
}

xs_config_t XsNewConfig() {
	xs_config_t conf;

	conf = (xs_config_t)XsMemory((void *)0, sizeof(*conf));

	conf->head = (xs_section_t)0;
	conf->tail = (xs_section_t)0;

	if (!XsNewSection(conf, (xs_section_t)0, ""))
		return XsDeleteConfig(conf);

	return conf;
}
xs_config_t XsLoadConfigFromBuffer(xs_buffer_t buf) {
	xs_config_t conf;

	conf = XsNewConfig();

	if (!Parse(conf, buf)) {
		XsDeleteConfig(conf);
		return 0;
	}

	return conf;
}
xs_config_t XsLoadConfig(const char *filename) {
	xs_buffer_t buf;
	xs_config_t conf;

	if (!(buf = XsLoadBuffer(filename)))
		return (xs_config_t)0;

	conf = XsLoadConfigFromBuffer(buf);

	XsDeleteBuffer(buf);

	return conf;
}

xs_config_t XsDeleteConfig(xs_config_t conf) {
	if (!conf)
		return (xs_config_t)0;

	while(conf->head)
		XsDeleteSection(conf->head);

	return (xs_config_t)XsMemory((void *)conf, 0);
}

xs_section_t XsNewSection(xs_config_t conf, xs_section_t prnt,
const char *name) {
	xs_section_t sect;

	sect = (xs_section_t)XsMemory((void *)0, sizeof(*sect));

	sect->conf = conf;

	sect->name = XsDuplicate(name);

	sect->prnt = prnt;
	sect->p_head = prnt ? &prnt->head : &conf->head;
	sect->p_tail = prnt ? &prnt->tail : &conf->tail;
	sect->head = (xs_section_t)0;
	sect->tail = (xs_section_t)0;
	sect->prev = (xs_section_t)0;
	sect->next = (xs_section_t)0;

	if ((sect->prev = *sect->p_tail) != (xs_section_t)0)
		(*sect->p_tail)->next = sect;
	else
		*sect->p_head = sect;
	*sect->p_tail = sect;

	sect->k_head = (xs_key_t)0;
	sect->k_tail = (xs_key_t)0;

	return sect;
}
xs_section_t XsDeleteSection(xs_section_t sect) {
	if (!sect)
		return (xs_section_t)0;

	while(sect->k_head)
		XsDeleteKey(sect->k_head);

	while(sect->head)
		XsDeleteSection(sect->head);

	if (sect->prev)
		sect->prev->next = sect->next;
	if (sect->next)
		sect->next->prev = sect->prev;

	if (*sect->p_head==sect)
		*sect->p_head = sect->next;
	if (*sect->p_tail==sect)
		*sect->p_tail = sect->prev;

	XsMemory((void *)sect->name, 0);

	return (xs_section_t)XsMemory((void *)sect, 0);
}

xs_config_t XsSectionConfig(xs_section_t sect) {
	return sect->conf;
}
const char *XsSectionName(xs_section_t sect) {
	return sect->name;
}
xs_section_t XsSectionParent(xs_section_t sect) {
	return sect->prnt;
}
xs_section_t XsFirstRootSection(xs_config_t conf) {
	return conf->head;
}
xs_section_t XsLastRootSection(xs_config_t conf) {
	return conf->tail;
}
xs_section_t XsFirstSection(xs_section_t sect) {
	return sect->head;
}
xs_section_t XsLastSection(xs_section_t sect) {
	return sect->tail;
}
xs_section_t XsSectionBefore(xs_section_t sect) {
	return sect->prev;
}
xs_section_t XsSectionAfter(xs_section_t sect) {
	return sect->next;
}

xs_key_t XsNewKey(xs_section_t sect, const char *name, const char *value) {
	xs_key_t key;

	key = (xs_key_t)XsMemory((void *)0, sizeof(*key));

	key->sect = sect;

	key->name = XsDuplicate(name);
	key->value = XsDuplicate(value);

	key->next = (xs_key_t)0;
	if ((key->prev = sect->k_tail) != (xs_key_t)0)
		sect->k_tail->next = key;
	else
		sect->k_head = key;
	sect->k_tail = key;

	return key;
}
xs_key_t XsDeleteKey(xs_key_t key) {
	if (!key)
		return (xs_key_t)0;

	if (key->prev)
		key->prev->next = key->next;
	if (key->next)
		key->next->prev = key->prev;

	if (key->sect->k_head==key)
		key->sect->k_head = key->next;
	if (key->sect->k_tail==key)
		key->sect->k_tail = key->prev;

	XsMemory((void *)key->name, 0);
	XsMemory((void *)key->value, 0);

	return (xs_key_t)XsMemory((void *)key, 0);
}
xs_section_t XsKeySection(xs_key_t key) {
	return key->sect;
}
const char *XsKeyName(xs_key_t key) {
	return key->name;
}
const char *XsKeyValue(xs_key_t key) {
	return key->value;
}
xs_key_t XsFirstKey(xs_section_t sect) {
	return sect->k_head;
}
xs_key_t XsLastKey(xs_section_t sect) {
	return sect->k_tail;
}
xs_key_t XsKeyBefore(xs_key_t key) {
	return key->prev;
}
xs_key_t XsKeyAfter(xs_key_t key) {
	return key->next;
}

xs_section_t XsFindSection(xs_config_t conf, const char *name) {
	xs_section_t sect;
	const char *s, *e;
	char path[512];

	s = name;
	if (!(e = strchr(name, '/')))
		e = strchr(name, '\0');

	XsStrCpyN(path, sizeof(path), s, e - s);

	for(sect=conf->head; sect; sect=sect->next) {
		if (!strcmp(sect->name, path))
			break;
	}

	while(sect) {
		if (*e==0)
			break;

		s = ++e;
		if (!(e = strchr(name, '/')))
			e = strchr(name, '\0');

		XsStrCpyN(path, sizeof(path), s, e - s);

		for(sect=sect->head; sect; sect=sect->next) {
			if (!strcmp(sect->name, path))
				break;
		}
	}

	return sect;
}
xs_key_t XsFindKeyInSection(xs_section_t sect, const char *name) {
	xs_key_t key;

	for(key=sect->k_head; key; key=key->next) {
		if (!strcmp(key->name, name))
			return key;
	}

	return (xs_key_t)0;
}
xs_key_t XsFindKey(xs_config_t conf, const char *name) {
	xs_section_t sect;
	const char *p;
	char sectname[4096];
	char keyname[512];

	if (!(p = strrchr(name, '/'))) {
		sectname[0] = 0;
		XsStrCpy(keyname, sizeof(keyname), name);
	} else {
		XsStrCpyN(sectname, sizeof(sectname), name, p - name);
		XsStrCpy(keyname, sizeof(keyname), ++p);
	}

	if (!(sect = XsFindSection(conf, sectname)))
		return (xs_key_t)0;

	return XsFindKeyInSection(sect, keyname);
}
