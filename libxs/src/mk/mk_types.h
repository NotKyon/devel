#ifndef MK_TYPES_H
#define MK_TYPES_H

#if _WIN32
# if _MSC_VER
#  define _CRT_SECURE_NO_WARNINGS 1
# endif
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#else
# include <limits.h>
#endif
#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <dirent.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <xs/xs.h>

#ifndef PATH_MAX
# define PATH_MAX 256
#endif

#ifndef bitfield_t_defined
# define bitfield_t_defined 1
typedef size_t bitfield_t;
#endif

#ifndef BIT
# define BIT(x) (1<<(x))
#endif

#if DEBUG||_DEBUG
# undef assert
# undef assertmsg
# if __i386__||__x86_64__||_M_IX86
#  if __GNUC__||__clang__
#   define breakpoint() __asm("int $3")
#  else
#   define breakpoint() __asm int 3
#  endif
# else
#  error "please define 'breakpoint' for your platform"
# endif
# define assertmsg(x,m) if(!(x)){Error(__FILE__,__LINE__,__func__,m);\
	breakpoint();}
# define assert(x) assertmsg((x),"assert(" #x ") failed!")
#else
# undef assert
# undef assertmsg
# define assert(x) /*nothing*/
# define assertmsg() /*nothing*/
# define breakpoint() /*nothing*/
#endif

void ErrorMessage(const char *message);
void ErrorExit(const char *message);
void Error(const char *file, unsigned int line, const char *func,
	const char *message);

#endif
