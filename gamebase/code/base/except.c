#if _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif
#include <gamebase.h>

#define MAX_EXCEPT_STACK 1024
static gb_fnExcept_t g_exceptHandler[MAX_EXCEPT_STACK];
static size_t g_exceptHandlerP = 0;

const char *gbExceptionName(gb_exceptKind_t kind) {
	switch(kind) {
#define HANDLE(x,y) case kGBExcept_##x: return y
	HANDLE(None						, "None"						);

	HANDLE(RuntimeError				, "Runtime Error"				);
	HANDLE(AssertError				, "Assert Failure"				);

	HANDLE(Fault					, "Fault"						);
	HANDLE(StackOverflow			, "Stack Overflow"				);

	HANDLE(MemoryViolation			, "Memory Violation"			);
	HANDLE(DivideByZero				, "Divide by Zero"				);

	HANDLE(UndefinedInstruction		, "Undefined Instruction"		);
	HANDLE(PrivilegedInstruction	, "Privileged Instruction"		);
	HANDLE(Breakpoint				, "Breakpoint Encountered"		);
#undef HANDLE
	}

	return "(unknown)";
}

gb_exceptResult_t gbDefaultException_f(const gb_exception_t *except) {
	gbSetConsoleTextColor(kGBConCol_Red);
	gbWriteStderr("EXCEPTION CAUGHT! ");
	gbSetConsoleTextColor(kGBConCol_BrightRed);
	gbWriteStderr(gbExceptionName(except->kind));
	gbSetConsoleTextColor(kGBConCol_Red);
	gbWriteStderr(" - ");
	gbSetConsoleTextColor(kGBConCol_Blue);
	gbWriteStderr("(");
	gbSetConsoleTextColor(kGBConCol_BrightCyan);
	gbWriteStderr(gbF("%i", except->kind));
	gbSetConsoleTextColor(kGBConCol_Blue);
	gbWriteStderr(")\n");

	if (except->message) {
		gbSetConsoleTextColor(kGBConCol_White);
		gbWriteStderr("User Message: ");
		gbSetConsoleTextColor(kGBConCol_BrightWhite);
		gbWriteStderr(except->message);
		gbWriteStderr("\n");
	}

	if (except->kind==kGBExcept_MemoryViolation) {
		gbSetConsoleTextColor(kGBConCol_White);
		gbWriteStderr("Read/Write: ");
		gbSetConsoleTextColor(kGBConCol_BrightYellow);
		gbWriteStderr(except->data.accessViolation->writeAccess ? "W" : "R");
		gbSetConsoleTextColor(kGBConCol_White);
		gbWriteStderr("\nAddress: ");
		gbSetConsoleTextColor(kGBConCol_BrightYellow);
		gbWriteStderr(gbF("%p\n", except->data.accessViolation->address));
	} else if(except->data.p) {
		gbSetConsoleTextColor(kGBConCol_White);
		gbWriteStderr("User Data: ");
		gbSetConsoleTextColor(kGBConCol_BrightWhite);
		gbWriteStderr(gbF("%p\n", except->data.p));
	}

#if _WIN32
	{
		const char *message, *title;

		title = gbExceptionName(except->kind);

		message = except->message ? except->message : title;
		MessageBoxA(GetActiveWindow(), message, title, MB_ICONERROR|MB_OK);
	}
#endif

	return kGBExceptRes_Break;
}

void gbPushExceptionHandler() {
	gbAssert(g_exceptHandlerP < MAX_EXCEPT_STACK - 1);

	g_exceptHandler[g_exceptHandlerP + 1] = g_exceptHandler[g_exceptHandlerP];
	g_exceptHandlerP++;
}
void gbPopExceptionHandler() {
	gbAssert(g_exceptHandlerP > 0);

	g_exceptHandlerP--;
}

void gbSetExceptionHandler(gb_fnExcept_t fnExcept) {
	g_exceptHandler[g_exceptHandlerP] = fnExcept;
}
gb_fnExcept_t gbGetExceptionHandler() {
	return g_exceptHandler[g_exceptHandlerP];
}

gb_bool_t gbRaiseException(const gb_exception_t *except) {
	gb_exceptResult_t r;
	size_t i;

	r = kGBExceptRes_RunNext;

	i = g_exceptHandlerP + 1;
	while(i > 0) {
		i--;

		if (!g_exceptHandler[i])
			continue;

		r = g_exceptHandler[i](except);

		if (r==kGBExceptRes_Terminate) {
			gbSetExitFlag(except->kind);
			gbEnd();
		}

		if (r==kGBExceptRes_RunNext)
			continue;

		if (r==kGBExceptRes_Break)
			break;

		gbAssert(0);
	}

	/*
	 * Return whether we're continuing to the next system exception handler, or
	 * not.
	 */
	return r==kGBExceptRes_RunNext ? GB_TRUE : GB_FALSE;
}
