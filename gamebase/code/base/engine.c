#include <gamebase.h>

#include <stdio.h>
#include <string.h>

#define ENGINE_NAME "GameBase"
#define ENGINE_VERSION_MAJOR 0
#define ENGINE_VERSION_MINOR 4

const char *gbEngineName() {
	static char buf[256] = { '\0', };

	if (GB_UNLIKELY(!buf[0])) {
#if __STDC_WANT_SECURE_LIB__
		sprintf_s(buf, sizeof(buf), "%s - %i.%.2i",
			ENGINE_NAME, ENGINE_VERSION_MAJOR, ENGINE_VERSION_MINOR);
#else
		snprintf(buf, sizeof(buf) - 1, "%s - %i.%.2i",
			ENGINE_NAME, ENGINE_VERSION_MAJOR, ENGINE_VERSION_MINOR);
		buf[sizeof(buf) - 1] = '\0';
#endif
	}

	return buf;
}
int gbEngineMajorVersion() {
	return ENGINE_VERSION_MAJOR;
}
int gbEngineMinorVersion() {
	return ENGINE_VERSION_MINOR;
}
gb_bool_t gbEngineDebugBuild() {
#if GB_DEBUG
	return GB_TRUE;
#else
	return GB_FALSE;
#endif
}
const char *gbEngineBuildDate() {
	return __DATE__;
}
const char *gbEngineBuildTime() {
	return __TIME__;
}
