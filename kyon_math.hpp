#pragma once

#include <math.h>

#undef min
#undef max

namespace kyon_math {


/*
===============================================================================

	COMMON MATH

===============================================================================
*/

template< typename tType >
inline tType sign( tType a )
{
	return tType( a < 0 ? -1 : a > 0 ? 1 : 0 );
}
template< typename tType >
inline tType heaviside( tType a )
{
	return tType( a > 0 ? 1 : 0 );
}
template< typename tType >
inline tType copy_sign( tType target_value, tType source_sign )
{
	return sign( source_sign )*abs( target_value );
}

template< typename tType >
inline tType min( tType a, tType b )
{
	return a < b ? a : b;
}
template< typename tType >
inline tType max( tType a, tType b )
{
	return a > b ? a : b;
}
template< typename tType >
inline tType clamp( tType value, tType min_value, tType max_value )
{
	return max( min( value, max_value ), min_value );
}
template< typename tType >
inline tType clamp_snorm( tType value )
{
	return clamp( value, -1, 1 );
}
template< typename tType >
inline tType clamp_unorm( tType value )
{
	return clamp( value, 0, 1 );
}
template< typename tType >
inline tType saturate( tType value )
{
	return clamp( value, 0, 1 );
}

template< typename tType >
inline tType negate_signed( tType value, tType check_sign )
{
	return check_sign < 0 ? -value : value;
}
template< typename tType >
inline tType negate_unsigned( tType value, tType check_sign )
{
	return check_sign < 0 ? value : -value;
}

template< typename tType >
inline tType wrap360( tType angle )
{
	return ( angle >= tType( 360 ) ) ? angle - floor( angle/360 )*360 : angle;
}
template< typename tType >
inline tType wrap180( tType angle )
{
	const tType wrapped360 = wrap360( angle );
	return ( angle >= 180.0f ) ? angle - 360.0f : angle;
}
template< typename tType >
inline tType angle_delta( const tType &a, const tType &b )
{
	return wrap180( a - b );
}

template< typename tType, typename tTimeType >
inline tType lerp( const tType &source, const tType &target, tTimeType t )
{
	return source + ( target - source )*t;
}
template< typename tType, typename tTimeType >
inline tType cerp( const tType &x, const tType &y, const tType &z, const tType &w, const tTimeType t )
{
	const tType a = ( w - z ) - ( x - y );
	const tType b = ( x - y ) - a;
	const tType c = ( z - x );
	return t*( t*( t*a + b ) + c ) + y;
}
template< typename tType, typename tTimeType >
inline tType slerp( const tType &a, const tType &b, const tTimeType t )
{
	if( t <= tTimeType( 0 ) ) {
		return a;
	}
	if( t >= tTimeType( 1 ) ) {
		return b;
	}

	tTimeType factor[ 2 ] = { tTimeType( 1 ) - t, t };
	const tTimeType cosangle = dot( a, b );
	if( tTimeType( 1 ) - cosangle > 1e-8f ) {
		const tTimeType angle = acos( cosangle );
		const tTimeType sinangle = sin( angle );
		factor[ 0 ] = sin( factor[ 0 ]*angle )/sinangle;
		factor[ 1 ] = sin( factor[ 1 ]*angle )/sinangle;
	}

	return a*factor[ 0 ] + b*factor[ 1 ];
}

template< typename tType >
inline tType frac( tType a )
{
	return a - floor( a );
}

template< typename tType >
inline tType tween_value( tType a, tType b, tType x )
{
	return ( x - a )/( b - a );
}
template< typename tType >
inline tType curve_value( tType a, tType b, tType t )
{
	return sin( tType( 3.14159265358979323846 )*tween_value( a, b, t ) );
}

template< typename tType >
inline tType smooth_step( tType a, tType b, tType x )
{
	if( x < a ) {
		return tType( 0 );
	}
	if( x > b ) {
		return tType( 1 );
	}

	const tType y = ( x - a )/( b - a );
	return y*y*( tType( 3 ) - tType( 2 )*y );
}
template< typename tType >
inline tType step( tType a, tType x )
{
	return x < a ? tType( 0 ) : tType( 1 );
}
template< typename tType >
inline tType over( tType x, tType y )
{
	return tType( 1 ) - ( tType( 1 ) - x )*( tType( 1 ) - y );
}

template< typename tType >
inline tType tri_wave( tType a, tType x )
{
	const tType y2 = frac( x/tType( 2*3.141592653589793238462643383279502884197169 ) );
	const tType y1 = y2 < tType( 0 ) ? 1.0 + y2 : y2;
	const tType y = y1 < a ? y1/a : tType( 1 ) - tween_value( a, 1, y1 );
	return tType( -1 ) + tType( 2 )*y;
}
template< typename tType >
inline tType saw_wave( tType a, tType x )
{
	const tType f = frac( x );
	return f < a ? f/a : tType( 1 ) - ( f - a )/( tType( 1 ) - a );
}
template< typename tType >
inline tType square_wave( tType a, tType x )
{
	return sin( x ) > a ? tType( 1 ) : tType( -1 );
}


/*
===============================================================================

	VECTOR PROLOGUE

===============================================================================
*/

namespace detail
{

	template< typename tType, unsigned int tNumDimensions >
	struct vector_comps
	{
		static_assert( tNumDimensions > 0, "Invalid number of dimensions" );
		tType						data				[ tNumDimensions ];
	};
	template< typename tType >
	struct vector_comps< tType, 1 >
	{
		union { tType				x, r, data[1]; };
	};
	template< typename tType >
	struct vector_comps< tType, 2 >
	{
		union { tType				x, r, data[1]; };
		union { tType				y, g; };
	};
	template< typename tType >
	struct vector_comps< tType, 3 >
	{
		union { tType				x, r, data[1]; };
		union { tType				y, g; };
		union { tType				z, b; };
	};
	template< typename tType >
	struct vector_comps< tType, 4 >
	{
		union { tType				x, r, data[1]; };
		union { tType				y, g; };
		union { tType				z, b; };
		union { tType				w, a; };
	};

}

template< typename tType, unsigned int tNumDimensions >
struct vector_x: public detail::vector_comps< tType, tNumDimensions >
{
	static const unsigned int		kNumDimensions		= tNumDimensions;
	typedef tType					component_type;
	
	inline tType &get( unsigned int i )
	{
		return *( reinterpret_cast< tType * >( this ) + i );
	}
	inline const tType &get( unsigned int i ) const
	{
		return *( reinterpret_cast< const tType * >( this ) + i );
	}
	
	inline tType &operator[]( unsigned int i )
	{
		return *( reinterpret_cast< tType * >( this ) + i );
	}
	inline const tType &operator[]( unsigned int i ) const
	{
		return *( reinterpret_cast< const tType * >( this ) + i );
	}

	inline vector_x( tType f = tType( 0 ) )
	{
		for( unsigned int i = 0; i < tNumDimensions; ++i ) {
			get( i ) = f;
		}
	}
	inline vector_x( tType f1, tType f2 )
	{
		static_assert( tNumDimensions > 1, "2D constructor on 1D vector" );

		get( 0 ) = f1;
		get( 1 ) = f2;

		for( unsigned int i = 2; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}
	inline vector_x( tType f1, tType f2, tType f3 )
	{
		static_assert( tNumDimensions > 2, "3D constructor on 1D/2D vector" );

		get( 0 ) = f1;
		get( 1 ) = f2;
		get( 2 ) = f3;

		for( unsigned int i = 3; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}
	inline vector_x( tType f1, tType f2, tType f3, tType f4 )
	{
		static_assert( tNumDimensions > 3, "4D constructor on 1D/2D/3D vector" );

		get( 0 ) = f1;
		get( 1 ) = f2;
		get( 2 ) = f3;
		get( 3 ) = f4;

		for( unsigned int i = 4; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}

	inline vector_x( tType f1, const vector_x< tType, 2 > &v2 )
	{
		static_assert( tNumDimensions > 2, "3D constructor on 1D/2D vector" );

		get( 0 ) = f1;
		get( 1 ) = v2.data[ 0 ];
		get( 2 ) = v2.data[ 1 ];

		for( unsigned int i = 3; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}
	inline vector_x( const vector_x< tType, 2 > &v1, tType f2 )
	{
		static_assert( tNumDimensions > 2, "3D constructor on 1D/2D vector" );

		get( 0 ) = v1.data[ 0 ];
		get( 1 ) = v1.data[ 1 ];
		get( 2 ) = f2;

		for( unsigned int i = 3; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}
	inline vector_x( tType f1, const vector_x< tType, 2 > &v2, tType f3 )
	{
		static_assert( tNumDimensions > 3, "4D constructor on 1D/2D/3D vector" );

		get( 0 ) = f1;
		get( 1 ) = v2.data[ 0 ];
		get( 2 ) = v2.data[ 1 ];
		get( 3 ) = f3;

		for( unsigned int i = 4; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}
	inline vector_x( const vector_x< tType, 2 > &v1, tType f2, tType f3 )
	{
		static_assert( tNumDimensions > 3, "4D constructor on 1D/2D/3D vector" );

		get( 0 ) = v1.data[ 0 ];
		get( 1 ) = v1.data[ 1 ];
		get( 2 ) = f2;
		get( 3 ) = f3;

		for( unsigned int i = 4; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}
	inline vector_x( tType f1, tType f2, const vector_x< tType, 2 > &v3 )
	{
		static_assert( tNumDimensions > 3, "4D constructor on 1D/2D/3D vector" );

		get( 0 ) = f1;
		get( 1 ) = f2;
		get( 2 ) = v3.data[ 0 ];
		get( 3 ) = v3.data[ 1 ];

		for( unsigned int i = 4; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}
	inline vector_x( tType f1, const vector_x< tType, 3 > &v2 )
	{
		static_assert( tNumDimensions > 3, "4D constructor on 1D/2D/3D vector" );

		get( 0 ) = f1;
		get( 1 ) = v2.data[ 0 ];
		get( 2 ) = v2.data[ 1 ];
		get( 3 ) = v2.data[ 2 ];

		for( unsigned int i = 4; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}
	inline vector_x( const vector_x< tType, 3 > &v1, tType f2 )
	{
		static_assert( tNumDimensions > 3, "4D constructor on 1D/2D/3D vector" );

		get( 0 ) = v1.data[ 0 ];
		get( 1 ) = v1.data[ 1 ];
		get( 2 ) = v1.data[ 2 ];
		get( 3 ) = f2;

		for( unsigned int i = 4; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}
	inline vector_x( const vector_x< tType, 2 > &v1, const vector_x< tType, 2 > &v2 )
	{
		static_assert( tNumDimensions > 3, "4D constructor on 1D/2D/3D vector" );

		get( 0 ) = v1.data[ 0 ];
		get( 1 ) = v1.data[ 1 ];
		get( 2 ) = v2.data[ 0 ];
		get( 3 ) = v2.data[ 1 ];

		for( unsigned int i = 4; i < tNumDimensions; ++i ) {
			get( i ) = 0;
		}
	}

	template< unsigned int tOtherNumDimensions >
	inline vector_x( const vector_x< tType, tOtherNumDimensions > &other )
	{
		unsigned int i = 0;
		for( i = 0; i < kNumDimensions && i < tOtherNumDimensions; ++i ) {
			get( i ) = other.get( i );
		}
		while( i < kNumDimensions ) {
			get( i++ ) = 0;
		}
	}

#define IMPL_VEC_BINOP__(Op_)\
	inline vector_x &operator Op_##=( const vector_x &other )\
	{\
		for( unsigned int i = 0; i < tNumDimensions; ++i ) {\
			get( i ) Op_##= other.get( i );\
		}\
		return *this;\
	}\
	inline vector_x &operator Op_##=( const tType other )\
	{\
		for( unsigned int i = 0; i < tNumDimensions; ++i ) {\
			get( i ) Op_##= other;\
		}\
		return *this;\
	}\
	inline vector_x operator Op_( const vector_x &other ) const\
	{\
		return vector_x( *this ) Op_##= other;\
	}\
	inline vector_x operator Op_( const tType other ) const\
	{\
		return vector_x( *this ) Op_##= other;\
	}\
	template< typename tOtherType, unsigned int tOtherNumDimensions >\
	inline vector_x &operator Op_##=( const vector_x< tOtherType, tOtherNumDimensions > &other )\
	{\
		unsigned int i = 0;\
		for( i = 0; i < tNumDimensions && i < tOtherNumDimensions; ++i ) {\
			get( i ) Op_##= tType( other.get( i ) );\
		}\
		return *this;\
	}\
	template< typename tOtherType, unsigned int tOtherNumDimensions >\
	inline vector_x operator Op_( const vector_x< tOtherType, tOtherNumDimensions > &other ) const\
	{\
		return vector_x( *this ) Op_##= other;\
	}
	
	IMPL_VEC_BINOP__(+)
	IMPL_VEC_BINOP__(-)
	IMPL_VEC_BINOP__(*)
	IMPL_VEC_BINOP__(/)
	IMPL_VEC_BINOP__(%)
	IMPL_VEC_BINOP__(&)
	IMPL_VEC_BINOP__(|)
	IMPL_VEC_BINOP__(^)
	IMPL_VEC_BINOP__(<<)
	IMPL_VEC_BINOP__(>>)

#undef IMPL_VEC_BINOP__

#define IMPL_VEC_UNIOP__(Op_)\
	inline vector_x operator Op_() const\
	{\
		vector_x result;\
		for( unsigned int i = 0; i < tNumDimensions; ++i ) {\
			result.get( i ) = Op_ get( i );\
		}\
		return result;\
	}
	
	IMPL_VEC_UNIOP__(~)
	IMPL_VEC_UNIOP__(-)

#undef IMPL_VEC_UNIOP__

#define IMPL_VEC_COND_BINOP__(Op_)\
	inline vector_x< bool, tNumDimensions > operator Op_( const vector_x &other ) const\
	{\
		vector_x< bool, tNumDimensions > result;\
		for( unsigned int i = 0; i < tNumDimensions; ++i ) {\
			result.get( i ) = get( i ) Op_ other.get( i );\
		}\
		return result;\
	}
	
	IMPL_VEC_COND_BINOP__(<)
	IMPL_VEC_COND_BINOP__(>)
	IMPL_VEC_COND_BINOP__(<=)
	IMPL_VEC_COND_BINOP__(>=)
	IMPL_VEC_COND_BINOP__(==)
	IMPL_VEC_COND_BINOP__(!=)

#undef IMPL_VEC_COND_BINOP__

#define IMPL_VEC_COND_UNIOP__(Op_)\
	inline vector_x< bool, tNumDimensions > operator Op_() const\
	{\
		vector_x< bool, tNumDimensions > result;\
		for( unsigned int i = 0; i < tNumDimensions; ++i ) {\
			result.get( i ) = Op_ get( i );\
		}\
		return result;\
	}

	//IMPL_VEC_COND_UNIOP__(!)

#undef IMPL_VEC_COND_UNIOP__

	inline bool any() const
	{
		for( unsigned int i = 0; i < tNumDimensions; ++i ) {
			if( !!get( i ) ) {
				return true;
			}
		}
		
		return false;
	}
	inline bool all() const
	{
		for( unsigned int i = 0; i < tNumDimensions; ++i ) {
			if( !get( i ) ) {
				return false;
			}
		}

		return true;
	}

	inline bool operator!() const
	{
		return !any();
	}
	inline operator bool() const
	{
		return any();
	}

	inline tType length_sq() const
	{
		tType result = tType( 0 );

		for( unsigned int i = 0; i < tNumDimensions; ++i ) {
			result += get( i )*get( i );
		}

		return result;
	}
	inline tType length() const
	{
		return sqrt( length_sq() );
	}

	inline vector_x normalized() const
	{
		vector_x< tType, tNumDimensions > result;
		
		//
		//	ＴＯＤＯ：Use a different method for integer vectors
		//
		
		const tType rcpmag = tType( 1 )/length_sq();
		
		for( unsigned int i = 0; i < tNumDimensions; ++i ) {
			result.get( i ) = get( i )*rcpmag;
		}

		return result;
	}

	inline vector_x snapped() const
	{
		vector_x< tType, tNumDimensions > result;

		for( unsigned int i = 0; i < tNumDimensions; ++i ) {
			result.get( i ) = round( get( i ) );
		}

		return result;
	}
	inline vector_x int_snapped() const
	{
		vector_x< tType, tNumDimensions > result;

		for( unsigned int i = 0; i < tNumDimensions; ++i ) {
			result.get( i ) = ( tType )int( get( i ) );
		}

		return result;
	}
	
	template< unsigned int tX, unsigned int tY >
	inline vector_x< tType, 2 > swizzle() const
	{
		static_assert( tX < tNumDimensions, "Invalid X component for swizzle" );
		static_assert( tY < tNumDimensions, "Invalid Y component for swizzle" );

		vector_x< tType, 2 > r;
		r.get( 0 ) = get( tX );
		r.get( 1 ) = get( tY );

		return r;
	}
	template< unsigned int tX, unsigned int tY, unsigned int tZ >
	inline vector_x< tType, 3 > swizzle() const
	{
		static_assert( tX < tNumDimensions, "Invalid X component for swizzle" );
		static_assert( tY < tNumDimensions, "Invalid Y component for swizzle" );
		static_assert( tZ < tNumDimensions, "Invalid Z component for swizzle" );

		vector_x< tType, 3 > r;
		r.get( 0 ) = get( tX );
		r.get( 1 ) = get( tY );
		r.get( 2 ) = get( tZ );

		return r;
	}
	template< unsigned int tX, unsigned int tY, unsigned int tZ, unsigned int tW >
	inline vector_x< tType, 4 > swizzle() const
	{
		static_assert( tX < tNumDimensions, "Invalid X component for swizzle" );
		static_assert( tY < tNumDimensions, "Invalid Y component for swizzle" );
		static_assert( tZ < tNumDimensions, "Invalid Z component for swizzle" );
		static_assert( tW < tNumDimensions, "Invalid W component for swizzle" );

		vector_x< tType, 4 > r;
		r.get( 0 ) = get( tX );
		r.get( 1 ) = get( tY );
		r.get( 2 ) = get( tZ );
		r.get( 3 ) = get( tW );

		return r;
	}

#define SWIZZLE_COMP_x__ 0
#define SWIZZLE_COMP_y__ 1
#define SWIZZLE_COMP_z__ 2
#define SWIZZLE_COMP_w__ 3

#define SWIZZLE2_IMPL__(X_,Y_)\
	inline vector_x< tType, 2 > X_##Y_() const\
	{\
		return swizzle< SWIZZLE_COMP_##X_##__, SWIZZLE_COMP_##Y_##__ >();\
	}
#define SWIZZLE3_IMPL__(X_,Y_,Z_)\
	inline vector_x< tType, 3 > X_##Y_##Z_() const\
	{\
		return swizzle< SWIZZLE_COMP_##X_##__, SWIZZLE_COMP_##Y_##__, SWIZZLE_COMP_##Z_##__ >();\
	}
#define SWIZZLE4_IMPL__(X_,Y_,Z_,W_)\
	inline vector_x< tType, 4 > X_##Y_##Z_##W_() const\
	{\
		return swizzle< SWIZZLE_COMP_##X_##__, SWIZZLE_COMP_##Y_##__, SWIZZLE_COMP_##Z_##__, SWIZZLE_COMP_##W_##__ >();\
	}

#define SWIZZLE2_W1(X_)				SWIZZLE2_IMPL__(X_,x) SWIZZLE2_IMPL__(X_,y) SWIZZLE2_IMPL__(X_,z) SWIZZLE2_IMPL__(X_,w)
#define SWIZZLE2_W					SWIZZLE2_W1(x) SWIZZLE2_W1(y) SWIZZLE2_W1(z) SWIZZLE2_W1(w)

#define SWIZZLE3_W2(X_,Y_)			SWIZZLE3_IMPL__(X_,Y_,x) SWIZZLE3_IMPL__(X_,Y_,y) SWIZZLE3_IMPL__(X_,Y_,z) SWIZZLE3_IMPL__(X_,Y_,w)
#define SWIZZLE3_W1(X_)				SWIZZLE3_W2(X_,x) SWIZZLE3_W2(X_,y) SWIZZLE3_W2(X_,z) SWIZZLE3_W2(X_,w)
#define SWIZZLE3_W					SWIZZLE3_W1(x) SWIZZLE3_W1(y) SWIZZLE3_W1(z) SWIZZLE3_W1(w)

#define SWIZZLE4_W3(X_,Y_,Z_)		SWIZZLE4_IMPL__(X_,Y_,Z_,x) SWIZZLE4_IMPL__(X_,Y_,Z_,y) SWIZZLE4_IMPL__(X_,Y_,Z_,z) SWIZZLE4_IMPL__(X_,Y_,Z_,w)
#define SWIZZLE4_W2(X_,Y_)			SWIZZLE4_W3(X_,Y_,x) SWIZZLE4_W3(X_,Y_,y) SWIZZLE4_W3(X_,Y_,z) SWIZZLE4_W3(X_,Y_,w)
#define SWIZZLE4_W1(X_)				SWIZZLE4_W2(X_,x) SWIZZLE4_W2(X_,y) SWIZZLE4_W2(X_,z) SWIZZLE4_W2(X_,w)
#define SWIZZLE4_W					SWIZZLE4_W1(x) SWIZZLE4_W1(y) SWIZZLE4_W1(z) SWIZZLE4_W1(w)

	SWIZZLE2_W
	SWIZZLE3_W
	SWIZZLE4_W

#undef SWIZZLE4_W
#undef SWIZZLE4_W1
#undef SWIZZLE4_W2
#undef SWIZZLE4_W3
#undef SWIZZLE3_W
#undef SWIZZLE3_W1
#undef SWIZZLE3_W2
#undef SWIZZLE2_W
#undef SWIZZLE2_W1

#undef SWIZZLE_COMP_x__
#undef SWIZZLE_COMP_y__
#undef SWIZZLE_COMP_z__
#undef SWIZZLE_COMP_w__
#undef SWIZZLE4_IMPL__
#undef SWIZZLE3_IMPL__
#undef SWIZZLE2_IMPL__
};

#define IMPL_VEC_BINOP__(Op_)\
	template< typename tType, unsigned int tNumDimensions >\
	inline vector_x< tType, tNumDimensions > operator Op_( const tType &a, const vector_x< tType, tNumDimensions > &b )\
	{\
		return vector_x< tType, tNumDimensions >( a ) Op_ b;\
	}
	
	IMPL_VEC_BINOP__(+)
	IMPL_VEC_BINOP__(-)
	IMPL_VEC_BINOP__(*)
	IMPL_VEC_BINOP__(/)
	IMPL_VEC_BINOP__(%)
	IMPL_VEC_BINOP__(&)
	IMPL_VEC_BINOP__(|)
	IMPL_VEC_BINOP__(^)
	IMPL_VEC_BINOP__(<<)
	IMPL_VEC_BINOP__(>>)

#undef IMPL_VEC_BINOP__


/*
===============================================================================

	VECTOR EPILOGUE

===============================================================================
*/

typedef vector_x< bool, 2 >			bool2;
typedef vector_x< bool, 3 >			bool3;
typedef vector_x< bool, 4 >			bool4;
typedef vector_x< float, 2 >		float2;
typedef vector_x< float, 3 >		float3;
typedef vector_x< float, 4 >		float4;
typedef vector_x< int, 2 >			int2;
typedef vector_x< int, 3 >			int3;
typedef vector_x< int, 4 >			int4;
typedef vector_x< unsigned int, 2 >	uint2;
typedef vector_x< unsigned int, 3 >	uint3;
typedef vector_x< unsigned int, 4 >	uint4;

#define VECX__ vector_x< tType, tNumDimensions >

template< typename tType, unsigned int tNumDimensions >
inline tType dot( const VECX__ &a, const VECX__ &b )
{
	tType r = 0;
	for( unsigned int i = 0; i < tNumDimensions; ++i ) {
		r += a.get( i )*b.get( i );
	}
	return r;
}

template< typename tType, unsigned int tNumDimensions >
inline tType distance( const VECX__ &a, const VECX__ &b )
{
	return ( a - b ).length();
}
template< typename tType, unsigned int tNumDimensions >
inline tType distance_sq( const VECX__ &a, const VECX__ &b )
{
	return ( a - b ).length_sq();
}

template< typename tType, unsigned int tNumDimensions >
inline VECX__ reflect( const VECX__ &incident, const VECX__ &normal )
{
	return incident - normal*( dot( incident, normal )*2 );
}

template< typename tType, unsigned int tNumDimensions >
inline VECX__ wrap360( const VECX__ &angles )
{
	VECX__ result;
	for( unsigned int i = 0; i < tNumDimensions; ++i ) {
		result.get( i ) = wrap360( angles.get( i ) );
	}
	return result;
}
template< typename tType, unsigned int tNumDimensions >
inline VECX__ wrap180( const VECX__ &angles )
{
	VECX__ result;
	for( unsigned int i = 0; i < tNumDimensions; ++i ) {
		result.get( i ) = wrap180( angles.get( i ) );
	}
	return result;
}

template< typename tType, unsigned int tNumDimensions >
inline VECX__ closest_point_on_line( const VECX__ &line_start, const VECX__ &line_end, const VECX__ &point )
{
	const VECX__ line_diff = line_end - line_start;
	const tType t = dot( point - line_start, line_diff )/line_diff.length_sq();
	return line_start + line_diff*t;
}
template< typename tType, unsigned int tNumDimensions >
inline VECX__ closest_point_on_segment( const VECX__ &line_start, const VECX__ &line_end, const VECX__ &point )
{
	const VECX__ line_diff = line_end - line_start;
	const tType t = dot( point - line_start, line_diff )/line_diff.length_sq();
	return line_start + line_diff*clamp_unorm( t );
}

#undef VECX__

template< typename tType >
inline vector_x< tType, 2 > screen_to_projection( const vector_x< tType, 2 > &src, const vector_x< tType, 2 > &size )
{
	return vector_x< tType, 2 >( -1 + ( src.x*2 )/size.x, 1 - ( src.y*2 )/size.y );
}

enum class aspect_mode
{
	none,
	fit,
	fill
};
inline int2 aspect_resize( const int2 &in_size, const int2 &bounding_size, aspect_mode mode = aspect_mode::fit )
{
	const double res_x = double( bounding_size.x );
	const double res_y = double( bounding_size.y );

	if( in_size.y == 0 ) {
		return in_size;
	}
	const double ratio = double( in_size.x )/double( in_size.y );

	if( abs( ratio ) < 1e-8f || !bounding_size.x || !bounding_size.y ) {
		return bounding_size;
	}

	const double target_ratio = res_x/( bounding_size.y == 0 ? 1.0 : res_y );

	switch( mode )
	{
	case aspect_mode::none:
		break;

	case aspect_mode::fit:
		if( target_ratio > ratio ) {
			return int2( int( res_y*ratio + 0.5 ), bounding_size.y );
		}
		return int2( bounding_size.x, int( res_x/ratio + 0.5 ) );

	case aspect_mode::fill:
		if( target_ratio > ratio ) {
			return int2( bounding_size.x, int( res_x/ratio + 0.5 ) );
		}
		return int2( int( res_y*ratio + 0.5 ), bounding_size.y );
	}

	return in_size;
}

template< typename tType >
inline vector_x< tType, 2 > line_normal( const vector_x< tType, 2 > &line_start, const vector_x< tType, 2 > &line_end )
{
	// NOTE: swap "line_start" and "line_end" to have this work in screen space (y down)
	return vector_x< tType, 2 >( line_start.y - line_end.y, line_end.x - line_start.x ).normalized();
}
template< typename tType >
inline tType line_angle( const vector_x< tType, 2 > &line_start, const vector_x< tType, 2 > &line_end )
{
	const vector_x< tType, 2 > normal = line_normal( line_start, line_end );
	return atan2( normal.y, normal.x );
}

template< typename tType >
inline vector_x< tType, 3 > cross( const vector_x< tType, 3 > &a, const vector_x< tType, 3 > &b )
{
	vector_x< tType, 3 > r;

	r.x = a.y*b.z - a.z*b.y;
	r.y = a.z*b.x - a.x*b.z;
	r.z = a.x*b.y - a.y*b.x;

	return r;
}

template< typename tType >
inline vector_x< tType, 3 > look_at( const vector_x< tType, 3 > &source, const vector_x< tType, 3 > &target )
{
	const vector_x< tType, 3 > diff( target - source );
	const vector_x< tType, 3 > p( diff.x, 0, diff.z );

	const tType y = p.length_sq() > 1e-8f ? negate_signed( acos( clamp_snorm( p.normalized().z ) ), p.x ) : 0;
	
	const tType s = sin( -y );
	const tType c = cos( -y );

	const vector_x< tType, 3 > q( diff.x*c + diff.z*s, diff.y, diff.x*-s + diff.z*c );

	const tType x = q.length_sq() > 1e-8f ? negate_unsigned( acos( clamp_snorm( q.normalized().z ) ), y ) : 0;

	return vector_x< tType, 3 >( x, y, 0 );
}

template< typename tType >
inline vector_x< tType, 4 > cross( const vector_x< tType, 4 > &a, const vector_x< tType, 4 > &b, const vector_x< tType, 4 > &c )
{
	const tType Ax = a.x, Ay = a.y, Az = a.z, Aw = a.w;
	const tType Bx = b.x, By = b.y, Bz = b.z, Bw = b.w;
	const tType Cx = c.x, Cy = c.y, Cz = c.z, Cw = c.w;

	vector_x< tType, 4 > r;

	r.x =  ( Ay*( Bz*Cw - Cz*Bw ) - Az*( By*Cw - Cy*Bw ) + Aw*( By*Cz - Cy*Bz ) );
	r.y = -( Ax*( Bz*Cw - Cz*Bw ) - Az*( Bx*Cw - Cx*Bw ) + Aw*( Bx*Cz - Cx*Bz ) );
	r.z =  ( Ax*( By*Cw - Cy*Bw ) - Ay*( Bx*Cw - Cx*Bw ) + Aw*( Bx*Cy - Cx*By ) );
	r.w = -( Ax*( By*Cz - Cy*Bz ) - Ay*( Bx*Cz - Cx*Bz ) + Aw*( Bx*Cy - Cy*By ) );

	return r;
}


/*
===============================================================================

	MATRIX PROLOGUE

===============================================================================
*/

namespace detail
{

	template< typename tColumnVectorType, typename tVectorType, unsigned int tNumVectors >
	inline tColumnVectorType get_column( const tVectorType( &row )[ tNumVectors ], unsigned int column )
	{
		tColumnVectorType result;

		const unsigned int c = column;
		for( unsigned int r = 0; r < tColumnVectorType::kNumDimensions && r < tVectorType::kNumDimensions; ++r ) {
			result[ r ] = row[ r ][ c ];
		}

		return result;
	}

	template< typename tVectorType, unsigned int tNumVectors >
	struct matrix_components
	{
		union
		{
			typename tVectorType::component_type data[ tVectorType::kNumDimensions*tNumVectors ];
			tVectorType row[ tNumVectors ];
		};
	};
	template< typename tVectorType >
	struct matrix_components< tVectorType, 1 >
	{
		union
		{
			typename tVectorType::component_type data[ tVectorType::kNumDimensions*1 ];
			tVectorType row[ 1 ];
			tVectorType x;
		};
	};
	template< typename tVectorType >
	struct matrix_components< tVectorType, 2 >
	{
		union
		{
			typename tVectorType::component_type data[ tVectorType::kNumDimensions*2 ];
			tVectorType row[ 2 ];
			struct
			{
				tVectorType x;
				tVectorType y;
			};
		};
	};
	template< typename tVectorType >
	struct matrix_components< tVectorType, 3 >
	{
		union
		{
			typename tVectorType::component_type data[ tVectorType::kNumDimensions*3 ];
			tVectorType row[ 3 ];
			struct
			{
				tVectorType x;
				tVectorType y;
				tVectorType z;
			};
		};
	};
	template< typename tVectorType >
	struct matrix_components< tVectorType, 4 >
	{
		union
		{
			typename tVectorType::component_type data[ tVectorType::kNumDimensions*4 ];
			tVectorType row[ 4 ];
			struct
			{
				tVectorType x;
				tVectorType y;
				tVectorType z;
				tVectorType w;
			};
		};
	};

}

template< typename tType, unsigned int tNumRows, unsigned int tNumColumns >
struct matrix_xy: public detail::matrix_components< vector_x< tType, tNumColumns >, tNumRows >
{
	typedef tType type;
	typedef matrix_xy< tType, tNumRows, tNumColumns > this_type;
	static const unsigned int kNumRows = tNumRows;
	static const unsigned int kNumColumns = tNumColumns;
	static const bool kIsSquare = tNumRows == tNumColumns;

	typedef vector_x< tType, tNumColumns > row_vector;
	typedef vector_x< tType, tNumRows > column_vector;

	typedef detail::matrix_components< row_vector, tNumRows > super_type;

	using super_type::data;
	using super_type::row;

	inline matrix_xy()
	{
		to_identity();
	}

	inline matrix_xy &to_identity()
	{
		for( unsigned int c = 0; c < tNumColumns; ++c ) {
			for( unsigned int r = 0; r < tNumRows; ++r ) {
				data[ c*tNumRows + r ] = float( +( c == r ) );
			}
		}
	}

	inline column_vector column( unsigned int index ) const
	{
		return detail::get_column< column_vector >( row, index );
	}

	tType determinant() const;

	matrix_xy &inverse_to( matrix_xy &dst, tType determinant ) const;
	inline matrix_xy &inverse_to( matrix_xy &dst ) const
	{
		return inverse_to( dst, determinant() );
	}
	inline matrix_xy &inverse()
	{
		*this = inverse_to( matrix_xy(), determinant() );
		return *this;
	}

	matrix_xy operator*( const matrix_xy &src ) const;
	inline matrix_xy &operator*=( const matrix_xy &src )
	{
		*this = ( *this )*src;
		return *this;
	}

	inline matrix_xy &operator+=( const matrix_xy &src )
	{
		for( unsigned int r = 0; r < kNumRows; ++r ) {
			row[ r ] += src.row[ r ];
		}

		return *this;
	}
	inline matrix_xy &operator-=( const matrix_xy &src )
	{
		for( unsigned int r = 0; r < kNumRows; ++r ) {
			row[ r ] -= src.row[ r ];
		}

		return *this;
	}

	inline matrix_xy operator+( const matrix_xy &src ) const
	{
		return matrix_xy( *this ) += src;
	}
	inline matrix_xy operator-( const matrix_xy &src ) const
	{
		return matrix_xy( *this ) -= src;
	}

	/*
		TODO: Lots

		- Multiply
		- Transpose
		- Scale
		- Generic determinant
		- Generic inverse
	*/
};

template< typename tType >
inline tType get_determinant( const matrix_xy< tType, 2, 2 > &m )
{
	return m.x.x*m.y.y - m.x.y*m.y.x;
}
template< typename tType >
inline tType get_determinant( const matrix_xy< tType, 3, 3 > &m )
{
	return dot( m.x, cross( m.y, m.z ) );
}
template< typename tType >
inline tType get_determinant( const matrix_xy< tType, 4, 4 > &m )
{
#define xx m.x.x
#define xy m.x.y
#define xz m.x.z
#define xw m.x.w
#define yx m.y.x
#define yy m.y.y
#define yz m.y.z
#define yw m.y.w
#define zx m.z.x
#define zy m.z.y
#define zz m.z.z
#define zw m.z.w
#define wx m.w.x
#define wy m.w.y
#define wz m.w.z
#define ww m.w.w
	return
		xw*yz*zy*wx - xz*yw*zy*wx - xw*yy*zz*wx + xy*yw*zz*wx +
		xz*yy*zw*wx - xy*yz*zw*wx - xw*yz*zx*wy + xz*yw*zx*wy +
		xw*yx*zz*wy - xx*yw*zz*wy - xz*yx*zw*wy + xx*yz*zw*wy +
		xw*yy*zx*wz - xy*yw*zx*wz - xw*yx*zy*wz + xx*yw*zy*wz +
		xy*yx*zw*wz - xx*yy*zw*wz - xz*yy*zx*ww + xy*yz*zx*ww +
		xz*yx*zy*ww - xx*yz*zy*ww - xy*yx*zz*ww + xx*yy*zz*ww;
#undef xx
#undef xy
#undef xz
#undef xw
#undef yx
#undef yy
#undef yz
#undef yw
#undef zx
#undef zy
#undef zz
#undef zw
#undef wx
#undef wy
#undef wz
#undef ww
}

template< typename tType, unsigned int tNumRows, unsigned int tNumColumns >
inline tType matrix_xy< tType, tNumRows, tNumColumns >::determinant() const
{
	return get_determinant( *this );
}

template< typename tType >
inline bool get_inverse( matrix_xy< tType, 2, 2 > &dst, const matrix_xy< tType, 2, 2 > &m, tType determinant )
{
	if( !determinant ) {
		return false;
	}

	const tType invdet = tType( 1 )/determinant;

	dst.x.x = m.y.y*invdet;
	dst.y.y = m.x.x*invdet;
	dst.x.y = -m.y.x*invdet;
	dst.y.x = -m.x.y*invdet;

	return true;
}
template< typename tType >
inline bool get_inverse( matrix_xy< tType, 3, 3 > &dst, const matrix_xy< tType, 3, 3 > &m, tType determinant )
{
	if( !determinant ) {
		return false;
	}

	const tType invdet = tType( 1 )/determinant;

	const vector_x< tType, 3 > u = m.column( 0 );
	const vector_x< tType, 3 > v = m.column( 1 );
	const vector_x< tType, 3 > w = m.column( 2 );

	const vector_x< tType, 3 > vw = cross( v, w )*invdet;
	const vector_x< tType, 3 > wu = cross( w, u )*invdet;
	const vector_x< tType, 3 > uv = cross( u, v )*invdet;

	dst.x.x = vw.x;
	dst.x.y = wu.x;
	dst.x.z = uv.x;
	
	dst.y.x = vw.y;
	dst.y.y = wu.y;
	dst.y.z = uv.y;
	
	dst.z.x = vw.z;
	dst.z.y = wu.z;
	dst.z.z = uv.z;

	return true;
}
template< typename tType >
inline bool get_inverse( matrix_xy< tType, 4, 4 > &dst, const matrix_xy< tType, 4, 4 > &m, tType determinant )
{
	if( !determinant ) {
		return false;
	}

	const tType invdet = tType( 1 )/determinant;

	const tType xx = m.x.x, yx = m.y.x, zx = m.z.x, wx = m.w.x;
	const tType xy = m.x.y, yy = m.y.y, zy = m.z.y, wy = m.w.y;
	const tType xz = m.x.z, yz = m.y.z, zz = m.z.z, wz = m.w.z;
	const tType xw = m.x.w, yw = m.y.w, zw = m.z.w, ww = m.w.w;

    dst.x.x = ( yz*zw*wy - yw*zz*wy + yw*zy*wz - yy*zw*wz - yz*zy*ww + yy*zz*ww )*invdet;
    dst.x.y = ( xw*zz*wy - xz*zw*wy - xw*zy*wz + xy*zw*wz + xz*zy*ww - xy*zz*ww )*invdet;
    dst.x.z = ( xz*yw*wy - xw*yz*wy + xw*yy*wz - xy*yw*wz - xz*yy*ww + xy*yz*ww )*invdet;
    dst.x.w = ( xw*yz*zy - xz*yw*zy - xw*yy*zz + xy*yw*zz + xz*yy*zw - xy*yz*zw )*invdet;
    dst.y.x = ( yw*zz*wx - yz*zw*wx - yw*zx*wz + yx*zw*wz + yz*zx*ww - yx*zz*ww )*invdet;
    dst.y.y = ( xz*zw*wx - xw*zz*wx + xw*zx*wz - xx*zw*wz - xz*zx*ww + xx*zz*ww )*invdet;
    dst.y.z = ( xw*yz*wx - xz*yw*wx - xw*yx*wz + xx*yw*wz + xz*yx*ww - xx*yz*ww )*invdet;
    dst.y.w = ( xz*yw*zx - xw*yz*zx + xw*yx*zz - xx*yw*zz - xz*yx*zw + xx*yz*zw )*invdet;
    dst.z.x = ( yy*zw*wx - yw*zy*wx + yw*zx*wy - yx*zw*wy - yy*zx*ww + yx*zy*ww )*invdet;
    dst.z.y = ( xw*zy*wx - xy*zw*wx - xw*zx*wy + xx*zw*wy + xy*zx*ww - xx*zy*ww )*invdet;
    dst.z.z = ( xy*yw*wx - xw*yy*wx + xw*yx*wy - xx*yw*wy - xy*yx*ww + xx*yy*ww )*invdet;
    dst.z.w = ( xw*yy*zx - xy*yw*zx - xw*yx*zy + xx*yw*zy + xy*yx*zw - xx*yy*zw )*invdet;
    dst.w.x = ( yz*zy*wx - yy*zz*wx - yz*zx*wy + yx*zz*wy + yy*zx*wz - yx*zy*wz )*invdet;
    dst.w.y = ( xy*zz*wx - xz*zy*wx + xz*zx*wy - xx*zz*wy - xy*zx*wz + xx*zy*wz )*invdet;
    dst.w.z = ( xz*yy*wx - xy*yz*wx - xz*yx*wy + xx*yz*wy + xy*yx*wz - xx*yy*wz )*invdet;
    dst.w.w = ( xy*yz*zx - xz*yy*zx + xz*yx*zy - xx*yz*zy - xy*yx*zz + xx*yy*zz )*invdet;

	return true;
}

template< typename tType, unsigned int tNumRows, unsigned int tNumColumns >
inline matrix_xy< tType, tNumRows, tNumColumns > &matrix_xy< tType, tNumRows, tNumColumns >::inverse_to( matrix_xy< tType, tNumRows, tNumColumns > &dst, tType determinant ) const
{
	return get_inverse( dst, *this, determinant );
}

template< typename tType >
inline matrix_xy< tType, 2, 2 > mul( const matrix_xy< tType, 2, 2 > &a, const matrix_xy< tType, 2, 2 > &b )
{
	matrix_xy< tType, 2, 2 > result;

	result.x.x = a.x.x*b.x.x + a.x.y*b.y.x;
	result.x.y = a.x.x*b.x.y + a.x.y*b.y.y;
	result.y.x = a.y.x*b.x.x + a.y.y*b.y.x;
	result.y.y = a.y.x*b.x.y + a.y.y*b.y.y;

	return result;
}
template< typename tType >
inline matrix_xy< tType, 3, 3 > mul( const matrix_xy< tType, 3, 3 > &a, const matrix_xy< tType, 3, 3 > &b )
{
	matrix_xy< tType, 3, 3 > result;

	result.x.x = a.x.x*b.x.x + a.x.y*b.y.x + a.x.z*b.z.x;
	result.x.y = a.x.x*b.x.y + a.x.y*b.y.y + a.x.z*b.z.y;
	result.x.z = a.x.x*b.x.z + a.x.y*b.y.z + a.x.z*b.z.z;

	result.y.x = a.y.x*b.x.x + a.y.y*b.y.x + a.y.z*b.z.x;
	result.y.y = a.y.x*b.x.y + a.y.y*b.y.y + a.y.z*b.z.y;
	result.y.z = a.y.x*b.x.z + a.y.y*b.y.z + a.y.z*b.z.z;

	result.z.x = a.z.x*b.x.x + a.z.y*b.y.x + a.z.z*b.z.x;
	result.z.y = a.z.x*b.x.y + a.z.y*b.y.y + a.z.z*b.z.y;
	result.z.z = a.z.x*b.x.z + a.z.y*b.y.z + a.z.z*b.z.z;

	return result;
}
template< typename tType >
inline matrix_xy< tType, 4, 4 > mul( const matrix_xy< tType, 4, 4 > &a, const matrix_xy< tType, 4, 4 > &b )
{
	matrix_xy< tType, 4, 4 > result;

	result.x.x = a.x.x*b.x.x + a.x.y*b.y.x + a.x.z*b.z.x + a.x.w*b.w.x;
	result.x.y = a.x.x*b.x.y + a.x.y*b.y.y + a.x.z*b.z.y + a.x.w*b.w.y;
	result.x.z = a.x.x*b.x.z + a.x.y*b.y.z + a.x.z*b.z.z + a.x.w*b.w.z;
	result.x.w = a.x.x*b.x.w + a.x.y*b.y.w + a.x.z*b.z.w + a.x.w*b.w.w;

	result.y.x = a.y.x*b.x.x + a.y.y*b.y.x + a.y.z*b.z.x + a.y.w*b.w.x;
	result.y.y = a.y.x*b.x.y + a.y.y*b.y.y + a.y.z*b.z.y + a.y.w*b.w.y;
	result.y.z = a.y.x*b.x.z + a.y.y*b.y.z + a.y.z*b.z.z + a.y.w*b.w.z;
	result.y.w = a.y.x*b.x.w + a.y.y*b.y.w + a.y.z*b.z.w + a.y.w*b.w.w;

	result.z.x = a.z.x*b.x.x + a.z.y*b.y.x + a.z.z*b.z.x + a.z.w*b.w.x;
	result.z.y = a.z.x*b.x.y + a.z.y*b.y.y + a.z.z*b.z.y + a.z.w*b.w.y;
	result.z.z = a.z.x*b.x.z + a.z.y*b.y.z + a.z.z*b.z.z + a.z.w*b.w.z;
	result.z.w = a.z.x*b.x.w + a.z.y*b.y.w + a.z.z*b.z.w + a.z.w*b.w.w;

	result.w.x = a.w.x*b.x.x + a.w.y*b.y.x + a.w.z*b.z.x + a.w.w*b.w.x;
	result.w.y = a.w.x*b.x.y + a.w.y*b.y.y + a.w.z*b.z.y + a.w.w*b.w.y;
	result.w.z = a.w.x*b.x.z + a.w.y*b.y.z + a.w.z*b.z.z + a.w.w*b.w.z;
	result.w.w = a.w.x*b.x.w + a.w.y*b.y.w + a.w.z*b.z.w + a.w.w*b.w.w;

	return result;
}
template< typename tType, unsigned int tDim >
inline matrix_xy< tType, tDim, tDim > mul( const matrix_xy< tType, tDim, tDim > &a, const matrix_xy< tType, tDim, tDim > &b )
{
	matrix_xy< tType, tDim, tDim > result;

	for( unsigned int r = 0; r < kNumRows; ++r ) {
		for( unsigned int c = 0; c < kNumColumns; ++c ) {
			tType sum = tType( 0 );
			for( unsigned int i = 0; i < kNumColumns; ++i ) {
				sum += row[ r ][ c ]*src.row[ c ][ i ];
			}
			result.row[ r ][ c ] = sum;
		}
	}

	return result;
}

template< typename tType, unsigned int tNumRows, unsigned int tNumColumns >
inline matrix_xy< tType, tNumRows, tNumColumns > &matrix_xy< tType, tNumRows, tNumColumns >::operator*( const matrix_xy< tType, tNumRows, tNumColumns > &src ) const
{
	return mul( *this, src );
}


/*
===============================================================================

	MATRIX EPILOGUE

===============================================================================
*/

template< typename tType, unsigned int tNumDimensions >
using matrix_x = matrix_xy< tType, tNumDimensions, tNumDimensions >;

typedef matrix_xy< float, 2, 2 >	float2x2;
typedef matrix_xy< float, 3, 3 >	float3x3;
typedef matrix_xy< float, 4, 4 >	float4x4;
typedef matrix_xy< double, 2, 2 >	double2x2;
typedef matrix_xy< double, 3, 3 >	double3x3;
typedef matrix_xy< double, 4, 4 >	double4x4;


template< typename tType >
inline vector_x< tType, 2 > mul( const matrix_xy< tType, 2, 2 > &a, const vector_x< tType, 2 > &b )
{
	vector_x< tType, 2 > result;

	result.x = a.x.x*b.x + a.x.y*b.y;
	result.y = a.y.x*b.x + a.y.y*b.y;

	return result;
}
template< typename tType >
inline vector_x< tType, 3 > mul( const matrix_xy< tType, 3, 3 > &a, const vector_x< tType, 3 > &b )
{
	vector_x< tType, 3 > result;

	result.x = a.x.x*b.x + a.x.y*b.y + a.x.z*b.z;
	result.y = a.y.x*b.x + a.y.y*b.y + a.y.z*b.z;
	result.z = a.z.x*b.x + a.z.y*b.y + a.z.z*b.z;

	return result;
}
template< typename tType >
inline vector_x< tType, 4 > mul( const matrix_xy< tType, 4, 4 > &a, const vector_x< tType, 4 > &b )
{
	vector_x< tType, 4 > result;

	result.x = a.x.x*b.x + a.x.y*b.y + a.x.z*b.z + a.x.w*b.w;
	result.y = a.y.x*b.x + a.y.y*b.y + a.y.z*b.z + a.y.w*b.w;
	result.z = a.z.x*b.x + a.z.y*b.y + a.z.z*b.z + a.z.w*b.w;
	result.w = a.w.x*b.x + a.w.y*b.y + a.w.z*b.z + a.w.w*b.w;

	return result;
}

template< typename tType, unsigned int tDim >
inline vector_x< tType, tDim > operator*( const matrix_xy< tType, tDim, tDim > &a, const vector_x< tType, tDim > &b )
{
	return mul( a, b );
}


} //kyon_math
