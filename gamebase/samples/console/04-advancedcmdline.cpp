#define GB_INIT 1
#include <gamebase.h>

//
//	Instead of processing each argument manually, you can take advantage of the
//	included command-line argument processing system. Here is a brief example of
//	how you might accomplish that.
//

//------------------------------------------------------------------------------

// We'll define some flags (used later).
enum {
	kFlag0_Help_Bit = (1<<0),
	kFlag0_Version_Bit = (1<<1)
};

// The flags above correspond to this array.
static int g_flags[1] = { 0 };

// We'll declare data for the help system.
static char *g_helpArg = (char *)NULL;
static void ShowHelp(const char *parm);

//------------------------------------------------------------------------------

// The actual argument processor takes in an option table.
static gb_opt_t g_optTable[] = {

	// define a switch that also takes a parameter but does not allow itself to
	// be referenced multiple times...
	// we'll use 'help' for this
	GB_OPT_SWITCH_PARM_LOOSE("help", ShowHelp, &g_flags[0], kFlag0_Help_Bit),
		//the first parameter is the name of the command-line parameter; 'help'
		//in this case.
		//the second parameter is the function to invoke when this argument is
		//encountered.
		//the third parameter is a pointer to the bitfield to update.
		//the fourth parameter is a mask of bits to set to the bitfield.

	// define a very simple switch that only sets/clears flags...
	GB_OPT_SWITCH("version", &g_flags[0], kFlag0_Version_Bit)

};

// Now define the size of the table.
static size_t g_optTableSize = sizeof(g_optTable)/sizeof(g_optTable[0]);

// We're also going to make use of an 'alias' table. This will be useful for
// accepting "short names." e.g., '-h' is short for '--help'.
static const char *g_optAlias[256];

//------------------------------------------------------------------------------

// Here's our ShowHelp() function. It just sets a global parameter and will get
// to work once argument processing has finished.
static void ShowHelp(const char *parm) {
	g_helpArg = gbCopy(g_helpArg, parm);
}

//------------------------------------------------------------------------------

// Entry point!
void GBMain() {
	size_t i, n;

	// clear the alias table
	gbMemClear((void *)g_optAlias, sizeof(g_optAlias));

	// now add aliases
	g_optAlias['h'] = "help";
	g_optAlias['v'] = "version";

	// set the options table and aliases
	gbSetOptTable(g_optTable, g_optTableSize);
	gbSetOptAlias(g_optAlias);

	// now process the command-line arguments
	gbProcessArgs();

	// check the flags
	if (g_flags[0] & kFlag0_Help_Bit)
		gbPrint(gbF("help! %s", g_helpArg ? g_helpArg : "(null)"));
	if (g_flags[0] & kFlag0_Version_Bit)
		gbPrint(gbF("compiled on %s at %s", __DATE__, __TIME__));

	// now we'll display all the files passed to the command-line!
	n = gbCountArgFiles();
	for(i=0; i<n; i++) {
		gbPrint(gbF("%.2u/%u: %s", (unsigned int)i, (unsigned int)n,
			gbGetArgFile(i)));
	}
}

//------------------------------------------------------------------------------

//
//	The command-line argument processing system takes arguments in a specific
//	form.
//
//*	Each argument must begin with '-'.
//*	Short arguments can only begin with a single '-'.
//*	Long arguments can begin with either a '-' or '--'. (The latter form exists
//	to avoid issues with aliases that take parameters.)
//*	Short arguments can take parameters. They can be in the form of '-Xblah' or
//	'-X blah'.
//*	Long arguments can also take parameters. They can be in the form of
//	'arg=blah' or 'arg blah' (with either the '-' or '--' prefix).
//*	Switch arguments can be negated by using the long form name with a prefix of
//	'no-'. e.g., the code above allows turning off the version switch (after it
//	has been turned on) by passing '--no-version' to the command-line.
//*	If a single '-' is passed as an argument (with no other text), all following
//	arguments are considered to be files. They will not be processed.
//
//	If you try '-help=that' on the command-line, it will not work as you might
//	expect it to. The single '-' causes the aliasing system to kick in and views
//	the rest of the argument as a parameter to the help switch. Use
//	'--help=that' instead.
//
