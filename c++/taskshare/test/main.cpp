//
//	TASK TESTING FRAMEWORK
//

//------------------------------------------------------------------------------
// INCLUDES

#include "Task.h"

#include <ctime>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "../taskshare/test/Alloc.cpp"

//------------------------------------------------------------------------------
// TASK FUNCTIONS

extern void Init();
extern void Fini();
extern void Run();
extern void Debug();

//------------------------------------------------------------------------------
// APP HANG

#if _DEBUG && 1
# if _WIN32
HANDLE g_appHangEvent;
task::thread_t g_appHangThread;
bool g_didAppHangInit = false;

static task::threadResult_t THREADPROCAPI CatchAppHang_f(void *) {
	static const int hangSeconds = 2;

	if (WaitForSingleObject(g_appHangEvent, hangSeconds*1000)==WAIT_OBJECT_0)
		goto __done;

#if __GNUC__
	__asm__ __volatile__("int $3");
#else
	__asm { int 3 };
#endif

__done:
	return (task::threadResult_t)NULL;
}
void InitAppHangQuery() {
	if (g_didAppHangInit)
		return;

	g_appHangEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	g_appHangThread = task::NewThread(CatchAppHang_f, (void *)NULL);

	g_didAppHangInit = true;
}
void FiniAppHangQuery() {
	if (!g_didAppHangInit)
		return;

	SetEvent(g_appHangEvent);
	task::JoinThread(g_appHangThread);
	task::KillThread(g_appHangThread);

	g_didAppHangInit = false;
}
# else
task::signal_t g_appHangSig;
task::thread_t g_appHangThread;
bool g_didAppHangInit = false;

static task::threadResult_t THREADPROCAPI CatchAppHang_f(void *) {
	static const int hangSeconds = 2;

#if _WIN32
	Sleep(1000*hangSeconds);
#else
	task::f80 s, e;

	s = task::SampleTimer();
	do {
		task::Relinquish();
		e = task::SampleTimer();
	} while((e - s) < (task::f80)hangSeconds);
#endif

	if (!task::TrySyncSignal(g_appHangSig))
#if __GNUC__
		__asm__ __volatile__("int $3");
#else
		__asm { int 3 };
#endif

	return (task::threadResult_t)NULL;
}

void InitAppHangQuery() {
	if (g_didAppHangInit)
		return;

	task::InitSignal(g_appHangSig, 1);
	g_appHangThread = task::NewThread(CatchAppHang_f, (void *)NULL);

	g_didAppHangInit = true;
}
void FiniAppHangQuery() {
	if (!g_didAppHangInit)
		return;

	task::RaiseSignal(g_appHangSig);
	task::JoinThread(g_appHangThread);
	task::KillThread(g_appHangThread);

	g_didAppHangInit = false;
}
# endif
#else
void InitAppHangQuery() {
}
void FiniAppHangQuery() {
}
#endif

//------------------------------------------------------------------------------
// THREADS

task::thread_t g_threads[64];
size_t g_numThreads = 0;
bool g_didThreadInit = false;

void InitThreads(task::threadFunc_t fn, void *p) {
	size_t i;

	if (g_didThreadInit)
		return;

	for(i=0; i<g_numThreads; i++) {
		g_threads[i] = task::NewThread(fn, p);
		task::SetThreadCPU(g_threads[i],
			i/(((size_t)task::IsHyperthreadingAvailable()) + 1));
	}

	g_didThreadInit = true;
}
void FiniThreads() {
	size_t i;

	if (!g_didThreadInit)
		return;

	for(i=0; i<g_numThreads; i++) {
		task::JoinThread(g_threads[i]);
		task::KillThread(g_threads[i]);
	}

	g_didThreadInit = false;
}

//------------------------------------------------------------------------------

int main(int argc, char **argv) {
	task::perfQuery_t pq;
	unsigned int cpucorecount, cputhreadcount;
	unsigned int i, n;
	bool info;
	bool justcores;
	bool dontrun;
	bool dodebug;

	n = 0;
	info = false;
	justcores = false;
	dontrun = false;
	dodebug = false;

	cpucorecount = task::GetCPUCoreCount();
	task::GetCPUThreadCount();
	cputhreadcount = task::GetCPUThreadCount();

	for(i=1; i<(unsigned int)argc; i++) {
		if (!strcmp(argv[i], "--info"))
			info = true;
		else if(!strcmp(argv[i], "--just-cores"))
			justcores = true;
		else if(!strcmp(argv[i], "--dont-run"))
			dontrun = true;
		else if(!strcmp(argv[i], "--no-info"))
			info = false;
		else if(!strcmp(argv[i], "--no-just-cores"))
			justcores = false;
		else if(!strcmp(argv[i], "--no-dont-run"))
			dontrun = false;
		else if(!strcmp(argv[i], "--debug-run"))
			dodebug = true;
		else if(!strcmp(argv[i], "--no-debug-run"))
			dodebug = false;
		else if(!strcmp(argv[i], "--streamers")) {
			if (i + 1 < (unsigned int)argc)
				n = atoi(argv[++i]);
			else
				fprintf(stderr, "error: --streamers expects argument\n");
		} else
			fprintf(stderr, "error: unknown option '%s'\n", argv[i]);
	}

	if (!n) {
		if (justcores)
			n = cpucorecount;
		else
			n = cputhreadcount;
	}

	if (n > (sizeof(g_threads)/sizeof(g_threads[0])))
		n = (sizeof(g_threads)/sizeof(g_threads[0]));

	g_numThreads = n;

	if (info) {
		printf("implementation: ");

#if defined(TASK_CONF_LOCKFREE)
# if TASK_CONF_LOCKFREE
		printf("lockfree");
# else
		printf("mutex");
# endif
#else
		printf("(undetermined)");
#endif

		printf(" (%i)\n", TASK_JOBQUEUE_VERSION);

#if defined(TASK_CONF_BUFFER_SIZE)
		printf("buffer-size: %u\n", TASK_CONF_BUFFER_SIZE);
#endif

		printf("compiled with: ");
#if __GNUC__
		printf("gcc (%i)", __GNUC__);
#elif __clang__
		printf("clang (%i)", __clang__);
#elif __INTEL_COMPILER
		printf("icc (%i)", __INTEL_COMPILER);
#elif _MSC_VER
		printf("msc (%i)", _MSC_VER);
#else
		printf("(unknown)");
#endif
		printf("\n");

		printf("built: %s %s\n", __DATE__, __TIME__);

		printf("\n");
		printf("cpu-core-count  : %u\n", cpucorecount);
		printf("cpu-thread-count: %u\n", cputhreadcount);
		printf("usr-thread-count: %u\n", n);
		printf("hyper-threading : %u\n",
			(unsigned int)task::IsHyperthreadingAvailable());
	}

	if (dontrun)
		return EXIT_SUCCESS;

	Init();

	task::InitPerfQuery(pq);
	if (dodebug)
		Debug();
	else
		Run();
	task::FiniPerfQuery(pq);

	InitAppHangQuery();
	Fini();
	FiniAppHangQuery();

	FiniThreads();

	return 0;
}
