#ifndef MATRIX_H
#define MATRIX_H

#include "export.h"
#include <math.h>

#define MAX_TEMPORARY_VECTORS 128

typedef struct matrix_s {
	float	m11, m21, m31, m41,
			m12, m22, m32, m42,
			m13, m23, m33, m43,
			m14, m24, m34, m44;
} matrix_t;

typedef struct vec3_s {
	float	x, y, z;
} vec3_t;
typedef struct vec4_s {
	float	x, y, z, w;
} vec4_t;

typedef struct quat_s {
	float	w;
	float	x, y, z;
} quat_t;

inline float Cos(float x) { return cosf(x/180.0f*3.1415926535f); }
inline float Sin(float x) { return sinf(x/180.0f*3.1415926535f); }
inline float Tan(float x) { return tanf(x/180.0f*3.1415926535f); }
inline float ACos(float x) { return acosf(x)/3.1415926535f*180.0f; }
inline float ASin(float x) { return asinf(x)/3.1415926535f*180.0f; }
inline float ATan(float x) { return atanf(x)/3.1415926535f*180.0f; }
inline float ATan2(float y, float x) { return atan2f(y,x)/3.1415926535f*180.0f; }

// Fast Math Functions
EXPORT int IntSign(int x);
EXPORT float FloatSign(float x);
EXPORT int IntAbs(int x);
EXPORT float FloatAbs(float x);
EXPORT int FloatToIntFast(float x);
EXPORT unsigned int FloatToUIntFast(float x);
EXPORT float SqrtFast(float x);
EXPORT float InvSqrtFast(float x);
EXPORT float CopySign(float a, float b);

// Vector Functions
EXPORT const vec3_t *Vec3(float x, float y, float z);
EXPORT const vec4_t *Vec4(float x, float y, float z, float w);
EXPORT const vec3_t *Vec3_Right();
EXPORT const vec3_t *Vec3_Up();
EXPORT const vec3_t *Vec3_Forward();
EXPORT void Vec3_Add(vec3_t *dst, const vec3_t *a, const vec3_t *b);
EXPORT void Vec3_Subtract(vec3_t *dst, const vec3_t *a, const vec3_t *b);
EXPORT void Vec3_Multiply(vec3_t *dst, const vec3_t *a, const vec3_t *b);
EXPORT void Vec3_Scale(vec3_t *dst, const vec3_t *a, float b);
EXPORT void Vec3_Divide(vec3_t *dst, const vec3_t *a, const vec3_t *b);
EXPORT void Vec3_LerpEx(vec3_t *dst, const vec3_t *a, const vec3_t *b, const vec3_t *w);
EXPORT void Vec3_Lerp(vec3_t *dst, const vec3_t *a, const vec3_t *b, float w);
EXPORT void Vec4_Add(vec4_t *dst, const vec4_t *a, const vec4_t *b);
EXPORT void Vec4_Subtract(vec4_t *dst, const vec4_t *a, const vec4_t *b);
EXPORT void Vec4_Multiply(vec4_t *dst, const vec4_t *a, const vec4_t *b);
EXPORT void Vec4_Scale(vec4_t *dst, const vec4_t *a, float b);
EXPORT void Vec4_Divide(vec4_t *dst, const vec4_t *a, const vec4_t *b);
EXPORT void Vec4_LerpEx(vec4_t *dst, const vec4_t *a, const vec4_t *b, const vec4_t *w);
EXPORT void Vec4_Lerp(vec4_t *dst, const vec4_t *a, const vec4_t *b, float w);
EXPORT float Vec3_Dot(const vec3_t *a, const vec3_t *b);
EXPORT void Vec3_Cross(vec3_t *dst, const vec3_t *a, const vec3_t *b);
EXPORT float Vec3_LengthSq(const vec3_t *src);
EXPORT float Vec3_Length(const vec3_t *src);
EXPORT float Vec3_LengthFast(const vec3_t *src);
EXPORT float Vec3_InvLengthFast(const vec3_t *src);
EXPORT void Vec3_Normalize(vec3_t *dst, const vec3_t *src);
EXPORT void Vec3_NormalizeFast(vec3_t *dst, const vec3_t *src);
EXPORT float Vec3_Distance(const vec3_t *a, const vec3_t *b);
EXPORT float Vec3_DistanceFast(const vec3_t *a, const vec3_t *b);
EXPORT float Vec4_Dot(const vec4_t *a, const vec4_t *b);
EXPORT void Vec4_Cross(vec4_t *dst, const vec4_t *a, const vec4_t *b, const vec4_t *c);
EXPORT float Vec4_LengthSq(const vec4_t *src);
EXPORT float Vec4_Length(const vec4_t *src);
EXPORT float Vec4_LengthFast(const vec4_t *src);
EXPORT float Vec4_InvLengthFast(const vec4_t *src);
EXPORT void Vec4_Normalize(vec4_t *dst, const vec4_t *src);
EXPORT void Vec4_NormalizeFast(vec4_t *dst, const vec4_t *src);
EXPORT float Vec4_Distance(const vec4_t *a, const vec4_t *b);
EXPORT float Vec4_DistanceFast(const vec4_t *a, const vec4_t *b);
EXPORT void Vec3_Transform(vec3_t *dst, const vec3_t *a, const matrix_t *b);
EXPORT void Vec3_Transform3x3(vec3_t *dst, const vec3_t *a, const matrix_t *b);
EXPORT void Vec3_Point(vec3_t *dst, const vec3_t *src, const vec3_t *target);
EXPORT void Vec3_TriangleNormal(vec3_t *dst, const vec3_t *verts);
EXPORT void Vec3_TriangleSlide(vec3_t *dst, const vec3_t *src, const vec3_t *norm, float speed);
EXPORT void Vec3_RotateQuaternion(vec3_t *dst, const vec3_t *src, const quat_t *rot);

// Matrix Functions
EXPORT void Matrix_Identity(matrix_t *dst);
EXPORT void Matrix_Transpose(matrix_t *dst, const matrix_t *src);
EXPORT void Matrix_Transpose3x3(matrix_t *dst, const matrix_t *src);
EXPORT void Matrix_RigidInverse(matrix_t *dst, const matrix_t *src);
EXPORT void Matrix_Multiply(matrix_t *dst, const matrix_t *a, const matrix_t *b);
EXPORT void Matrix_Multiply3x3(matrix_t *dst, const matrix_t *a, const matrix_t *b);
EXPORT void Matrix_RigidMultiply(matrix_t *dst, const matrix_t *a, const matrix_t *b);
EXPORT void Matrix_Translation(matrix_t *dst, const vec3_t *v);
EXPORT void Matrix_SetTranslation(matrix_t *dst, const vec3_t *v);
EXPORT void Matrix_Move(matrix_t *dst, const vec3_t *v);
EXPORT void Matrix_MoveX(matrix_t *dst, float x);
EXPORT void Matrix_MoveY(matrix_t *dst, float y);
EXPORT void Matrix_MoveZ(matrix_t *dst, float z);
EXPORT void Matrix_RotationX(matrix_t *dst, float angle);
EXPORT void Matrix_RotationY(matrix_t *dst, float angle);
EXPORT void Matrix_RotationZ(matrix_t *dst, float angle);
EXPORT void Matrix_Rotation(matrix_t *dst, const vec3_t *v);
EXPORT void Matrix_TurnX(matrix_t *dst, float angle);
EXPORT void Matrix_TurnY(matrix_t *dst, float angle);
EXPORT void Matrix_TurnZ(matrix_t *dst, float angle);
EXPORT void Matrix_Turn(matrix_t *dst, const vec3_t *v);
EXPORT void Matrix_Scaling(matrix_t *dst, const vec3_t *v);
EXPORT void Matrix_ScaleEx(matrix_t *dst, const matrix_t *src, const vec3_t *v);
EXPORT void Matrix_Scale(matrix_t *dst, const vec3_t *v);
EXPORT void Matrix_ScaleX(matrix_t *dst, float factor);
EXPORT void Matrix_ScaleY(matrix_t *dst, float factor);
EXPORT void Matrix_ScaleZ(matrix_t *dst, float factor);
EXPORT void Matrix_GetTranslation(vec3_t *dst, const matrix_t *src);
EXPORT void Matrix_GetScaling(vec3_t *dst, const matrix_t *src);
EXPORT void Matrix_GetScalingFast(vec3_t *dst, const matrix_t *src);
EXPORT void Matrix_GetInvScalingF(vec3_t *dst, const matrix_t *src);
EXPORT void Matrix_GetRotationExFast(vec3_t *dst, const matrix_t *src, const vec3_t *s);
EXPORT void Matrix_GetRotationEx(vec3_t *dst, const matrix_t *src, const vec3_t *s);
EXPORT void Matrix_GetQuaternionExFast(quat_t *dst, const matrix_t *src, const vec3_t *s);
EXPORT void Matrix_GetQuaternionEx(quat_t *dst, const matrix_t *src, const vec3_t *s);
EXPORT void Matrix_GetRotation(vec3_t *dst, const matrix_t *src);
EXPORT void Matrix_GetRotationFast(vec3_t *dst, const matrix_t *src);
EXPORT void Matrix_GetQuaternion(quat_t *dst, const matrix_t *src);
EXPORT void Matrix_GetQuaternionFast(quat_t *dst, const matrix_t *src);
EXPORT void Matrix_Perspective(matrix_t *dst, float fov, float aspect, float zn, float zf);

// Quaternion Functions
EXPORT void Quat_Identity(quat_t *dst);
EXPORT void Quat_Axis(quat_t *dst, float angle, float x, float y, float z);
EXPORT void Quat_RotationX(quat_t *dst, float angle);
EXPORT void Quat_RotationY(quat_t *dst, float angle);
EXPORT void Quat_RotationZ(quat_t *dst, float angle);
EXPORT void Quat_Multiply(quat_t *dst, const quat_t *a, const quat_t *b);
EXPORT void Quat_Rotation(quat_t *dst, const vec3_t *src);
EXPORT float Quat_Dot(const quat_t *a, const quat_t *b);
EXPORT float Quat_LengthSq(const quat_t *src);
EXPORT float Quat_Length(const quat_t *src);
EXPORT float Quat_LengthFast(const quat_t *src);
EXPORT float Quat_InvLengthFast(const quat_t *src);
EXPORT void Quat_Normalize(quat_t *dst, const quat_t *src);
EXPORT void Quat_NormalizeFast(quat_t *dst, const quat_t *src);
EXPORT void Quat_Conjugate(quat_t *dst, const quat_t *src);
EXPORT void Quat_Inverse(quat_t *dst, const quat_t *src);
EXPORT void Quat_InverseFast(quat_t *dst, const quat_t *src);
EXPORT void Quat_Slerp(quat_t *dst, const quat_t *a, const quat_t *b, float w);
EXPORT void Quat_Lerp(quat_t *dst, const quat_t *a, const quat_t *b, float w);

#endif
