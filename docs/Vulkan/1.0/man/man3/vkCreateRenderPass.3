'\" t
.\"     Title: vkCreateRenderPass
.\"    Author: Khronos Group
.\" Generator: DocBook XSL Stylesheets v1.78.1 <http://docbook.sf.net/>
.\"      Date: 03/03/2016
.\"    Manual: \ \&
.\"    Source: \ \&
.\"  Language: English
.\"
.TH "VKCREATERENDERPASS" "3" "03/03/2016" "\ \&" "\ \&"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
vkCreateRenderPass \- Create a new render pass object\&.
.SH "C SPECIFICATION"
.sp
.if n \{\
.RS 4
.\}
.nf
VkResult vkCreateRenderPass(
    VkDevice                                    device,
    const VkRenderPassCreateInfo*               pCreateInfo,
    const VkAllocationCallbacks*                pAllocator,
    VkRenderPass*                               pRenderPass);
.fi
.if n \{\
.RE
.\}
.SH "PARAMETERS"
.PP
\fIdevice\fR
.RS 4
The device with which to create the render pass object\&.
.RE
.PP
\fIpCreateInfo\fR
.RS 4
A pointer to a structure containing information to be placed in the object\&.
.RE
.PP
\fIpRenderPass\fR
.RS 4
A pointer to a variable which will receive the handle to the new object\&.
.RE
.SH "DESCRIPTION"
.sp
\fBvkCreateRenderPass\fR creates a new render pass object using the information contained in \fIpCreateInfo\fR and the device specified in \fIdevice\fR\&. Upon success, a handle to the new render pass object is deposited in the variable pointed to by \fIpRenderPass\fR, \fIpCreateInfo\fR should point to an instance of the \fBVkRenderPassCreateInfo\fR structure, the definition of which is:
.sp
.if n \{\
.RS 4
.\}
.nf
typedef struct VkRenderPassCreateInfo {
    VkStructureType                   sType;
    const void*                       pNext;
    VkRenderPassCreateFlags           flags;
    uint32_t                          attachmentCount;
    const VkAttachmentDescription*    pAttachments;
    uint32_t                          subpassCount;
    const VkSubpassDescription*       pSubpasses;
    uint32_t                          dependencyCount;
    const VkSubpassDependency*        pDependencies;
} VkRenderPassCreateInfo;
.fi
.if n \{\
.RE
.\}
.sp
A render pass is a sequence of subpasses, each of which reads from some framebuffer attachments and writes to others as color and depth/stencil\&. The subpasses all render to the same dimensions, and fragments for pixel (x,y,layer) in one subpass only read framebuffer contents written by earlier subpasses at that same (x,y,layer) location\&. It is quite common for a render pass to only contain a single subpass\&.
.sp
Dependencies between subpasses describe ordering restrictions between them\&. Without dependencies, implementations may reorder or overlap execution of two subpasses\&.
.SS "Attachments"
.sp
The attachments used in the render pass are described by a \fBVkAttachmentDescription\fR structure, defined as:
.sp
.if n \{\
.RS 4
.\}
.nf
typedef struct VkAttachmentDescription {
    VkAttachmentDescriptionFlags    flags;
    VkFormat                        format;
    VkSampleCountFlagBits           samples;
    VkAttachmentLoadOp              loadOp;
    VkAttachmentStoreOp             storeOp;
    VkAttachmentLoadOp              stencilLoadOp;
    VkAttachmentStoreOp             stencilStoreOp;
    VkImageLayout                   initialLayout;
    VkImageLayout                   finalLayout;
} VkAttachmentDescription;
.fi
.if n \{\
.RE
.\}
.sp
The \fIformat\fR and \fIsamples\fR members are respectively the format and the number of samples of the image that will be used for the attachment\&.
.sp
The \fIloadOp\fR defines how the contents of the attachment within the render area will be treated at the beginning of the render pass\&. A load op of \fBVK_ATTACHMENT_LOAD_OP_LOAD\fR means the contents within the render area will be preserved; \fBVK_ATTACHMENT_LOAD_OP_CLEAR\fR means the contents within the render area will be cleared to a uniform value; \fBVK_ATTACHMENT_LOAD_OP_DONT_CARE\fR means the application intends to overwrite all samples in the render area without reading the initial contents, so their initial contents are unimportant\&. If the attachment format has both depth and stencil components, \fIloadOp\fR applies only to the depth data, while \fIstencilLoadOp\fR defines how the stencil data is handled\&. \fIstencilLoadOp\fR is ignored for other formats\&.
.sp
The \fIstoreOp\fR defines whether data rendered to the attachment is committed to memory at the end of the render pass\&. \fBVK_ATTACHMENT_STORE_OP_STORE\fR means the data is committed to memory and will be available for reading after the render pass completes\&. \fBVK_ATTACHMENT_STORE_OP_DONT_CARE\fR means the data is not needed after rendering, and may be discarded; the contents of the attachment will be undefined inside the render area\&. If the attachment format has both depth and stencil components, \fIstoreOp\fR applies only to the depth data, while \fIstencilStoreOp\fR defines how the stencil data is handled\&. \fIstencilStoreOp\fR is ignored for other formats\&.
.sp
\fIinitialLayout\fR is the layout the attachment image will be in when the render pass begins\&.
.sp
\fIfinalLayout\fR is the layout the attachment image will be transitioned to when the render pass ends\&.
.SS "Subpasses"
.sp
Subpasses of a render pass are described by a \fBVkSubpassDescription\fR structure, defined as:
.sp
.if n \{\
.RS 4
.\}
.nf
typedef struct VkSubpassDescription {
    VkSubpassDescriptionFlags       flags;
    VkPipelineBindPoint             pipelineBindPoint;
    uint32_t                        inputAttachmentCount;
    const VkAttachmentReference*    pInputAttachments;
    uint32_t                        colorAttachmentCount;
    const VkAttachmentReference*    pColorAttachments;
    const VkAttachmentReference*    pResolveAttachments;
    const VkAttachmentReference*    pDepthStencilAttachment;
    uint32_t                        preserveAttachmentCount;
    const uint32_t*                 pPreserveAttachments;
} VkSubpassDescription;
.fi
.if n \{\
.RE
.\}
.sp
The \fIpipelineBindPoint\fR indicates whether this is a compute or graphics subpass\&. Only graphics subpasses are currently allowed\&.
.sp
The \fIflags\fR member is currently unused and must be zero\&.
.sp
\fIpInputAttachments\fR lists which of the render pass\(cqs attachments will be read in the shader in the subpass, and what layout the attachment images should be transitioned to before the subpass\&. \fIinputAttachmentCount\fR indicates the number of input attachments\&. Input attachments must also be bound to the pipeline with a descriptor set\&.
.sp
\fIpColorAttachments\fR lists which of the render pass\(cqs attachments will be used as color attachments in the subpass, and what layout the attachment images should be transitioned to before the subpass\&. \fIcolorAttachmentCount\fR indicates the number of color attachments\&.
.sp
Each entry in \fIpResolveAttachments\fR corresponds to an entry in \fIpColorAttachments\fR; either \fIpResolveAttachments\fR must be NULL or it must have \fIcolorAttachmentCount\fR entries\&. If \fIpResolveAttachments\fR is not NULL, each of its elements corresponds to a color attachment (the element in \fIpColorAttachments\fR at the same index)\&. At the end of each subpass, the subpass\(cqs color attachments will be resolved to the corresponding resolve attachments, unless the resolve attachment index is \fBVK_ATTACHMENT_UNUSED\fR\&.
.sp
The \fIdepthStencilAttachment\fR indicates which attachment will be used for depth/stencil data and the layout it should be transitioned to before the subpass\&. If no depth/stencil attachment is used in the subpass, the attachment index must be \fBVK_ATTACHMENT_UNUSED\fR\&.
.sp
The \fIpPreserveAttachments\fR are the attachments that aren\(cqt used by a subpass, but whose contents must be preserved throughout the subpass\&. If the contents of an attachment are produced in one subpass and consumed in a later subpass, the attachment must be preserved in any subpasses on dependency chains from the producer to consumer\&. \fIpreserveAttachmentCount\fR indicates the number of preserved attachments\&.
.sp
If a subpass uses an attachment as both an input attachment and either a color attachment or a depth/stencil attachment, all pipelines used in the subpass must disable writes to any components of the attachment format that are used as input\&.
.SS "Dependencies"
.sp
Dependencies describe a pipeline barrier that must occur between two subpasses, usually because the destination subpass reads attachment contents written by the source subpass\&. Dependencies are described by \fBVkSubpassDependency\fR structures, defined as:
.sp
.if n \{\
.RS 4
.\}
.nf
typedef struct VkSubpassDependency {
    uint32_t                srcSubpass;
    uint32_t                dstSubpass;
    VkPipelineStageFlags    srcStageMask;
    VkPipelineStageFlags    dstStageMask;
    VkAccessFlags           srcAccessMask;
    VkAccessFlags           dstAccessMask;
    VkDependencyFlags       dependencyFlags;
} VkSubpassDependency;
.fi
.if n \{\
.RE
.\}
.sp
The \fIsrcSubpass\fR and \fIdstSubpass\fR are producer and consumer subpasses, respectively\&. \fIsrcSubpass\fR must be less than or equal to \fIdstSubpass\fR, so that the order of subpass descriptions is always a valid execution ordering, and so the dependency graph cannot have cycles\&.
.sp
The \fIsrcStageMask\fR, \fIdstStageMask\fR, \fIoutputMask\fR, \fIinputMask\fR, and \fIbyRegion\fR describe the barrier, and have the same meaning as the VkCmdPipelineBarrier parameters and \fBVkMemoryBarrier\fR members with the same names\&.
.sp
If \fIbyRegion\fR is \fBVK_TRUE\fR, it describes a per\-region (x,y,layer) dependency, that is for each region, the \fIsrcStageMask\fR stages must have finished in \fIsrcSubpass\fR before any \fIdstStageMask\fR stage starts in \fIdstSubpass\fR for the same region\&. If \fIbyRegion\fR is \fBVK_FALSE\fR, it describes a global dependency, that is the \fIsrcStageMask\fR stages must have finished for all regions in \fIsrcSubpass\fR before any \fIdstStageMask\fR stage starts in \fIdstSubpass\fR for any region\&.
.SH "VALID USAGE"
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
\fIdevice\fRmust
be a valid
\fBVkDevice\fR
handle
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
\fIpCreateInfo\fRmust
be a pointer to a valid
\fBVkRenderPassCreateInfo\fR
structure
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
If
\fIpAllocator\fR
is not
NULL,
\fIpAllocator\fRmust
be a pointer to a valid
\fBVkAllocationCallbacks\fR
structure
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
\fIpRenderPass\fRmust
be a pointer to a
\fBVkRenderPass\fR
handle
.RE
.SH "RETURN CODES"
.PP
On success, this command returns
.RS 4
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
\fBVK_SUCCESS\fR
.RE
.RE
.PP
On failure, this command returns
.RS 4
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
\fBVK_ERROR_OUT_OF_HOST_MEMORY\fR
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
\fBVK_ERROR_OUT_OF_DEVICE_MEMORY\fR
.RE
.RE
.SH "SEE ALSO"
.sp
vkCmdBeginRenderPass, vkCmdEndRenderPass
.SH "COPYRIGHT"
.sp
Copyright \(co 2014\-2016 Khronos Group\&. This material may be distributed subject to the terms and conditions set forth in the Open Publication License, v 1\&.0, 8 June 1999\&. http://opencontent\&.org/openpub/\&.
.SH "AUTHOR"
.PP
\fBKhronos Group\fR
.RS 4
Author.
.RE
