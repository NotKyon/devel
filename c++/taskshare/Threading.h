#ifndef TASKSHARE_THREADING_H
#define TASKSHARE_THREADING_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

//------------------------------------------------------------------------------
// THREADS

__TS_NS_INIT__

inline thread_t NewThread(threadFunc_t func, void *p) {
	thread_t h;

#if __USE_WIN32_THREADS__
	h = CreateThread(0, 0, func, p, 0, 0);
#else
	memset(&h, 0, sizeof(h));
	pthread_create(&h, (const pthread_attr_t *)0, func, p);
#endif

	return h;
}

inline void SetThreadCPU(thread_t &thread, u32 cpu) {
#if __USE_WIN32_THREADS__
	DWORD_PTR mask, cpuMask, stride;

	stride = GetCPUThreadCount()/GetCPUCoreCount();

	cpuMask  = 1;
	cpuMask |= stride&(1<<1);
	//cpuMask |= stride&(1<<2);	//maybe there will be more threads one day
	//cpuMask |= stride&(1<<3);

	mask = cpuMask<<(cpu*stride);

	SetThreadAffinityMask(thread, mask);
#else
# if __CYGWIN__
	// can't set affinity of thread via pthreads... big performance sink
# else
	// HELPFUL: http://bit.ly/yw7R3W
	cpu_set_t cpuset;

	cpuset = cpu;
	pthread_setaffinity_np(thread, sizeof(cpuset), &cpuset);
# endif
#endif
}

inline void JoinThread(thread_t &thread) {
#if __USE_WIN32_THREADS__
	WaitForSingleObject(thread, INFINITE);
#else
	pthread_join(thread, (void **)0);
#endif
}

inline void KillThread(thread_t &thread) {
#if __USE_WIN32_THREADS__
	//TerminateThread(thread, 0);
	CloseHandle(thread);

	thread = (thread_t)0;
#else
	// no pthread_destroy()? there's a pthread_kill()
	memset((void *)&thread, 0, sizeof(thread));
#endif
}

inline void Relinquish() {
#if __USE_WIN32_THREADS__
	Sleep(0);
#else
	sched_yield(); //nanosleep()?
#endif
}

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
