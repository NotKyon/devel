//#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

#include <stdio.h>
#include <stdlib.h>

LONG gNumCompleted = 0;

extern "C" BOOL WINAPI QueueUserWorkItem(LPTHREAD_START_ROUTINE Function,
	PVOID Context, ULONG Flags);

DWORD WINAPI DisplayNumber(void *p) {
//void WINAPI DisplayNumber(ULONG_PTR p) {
	char buf[32];
	int i, j;

	i = (int)(size_t)p;

	if (i<=1)
		snprintf(buf, sizeof(buf) - 1, "%i isn't prime", i);
	else if(i==2)
		snprintf(buf, sizeof(buf) - 1, "%i is prime", i);
	else {
		for(j=3; j /* *j */<i; j+=2) {
			if (i%j == 0) {
				snprintf(buf, sizeof(buf) - 1, "%i isn't prime", i);
				j = 0;
				break;
			}
		}

		if (j != 0)
			snprintf(buf, sizeof(buf) - 1, "%i is prime", i);
	}

	buf[sizeof(buf) - 1] = 0;
	printf("%s\n", buf);

	//InterlockedIncrement(&gNumCompleted);
	gNumCompleted++;

	return 0;
}

void TestScheduler() {
	int i, x, n;

	n = 1024;
	gNumCompleted = 0;

	x = 100000000;
	for(i=x; i<x + n; i++)
		QueueUserWorkItem(DisplayNumber, (void *)(size_t)i, WT_EXECUTEDEFAULT);

	while(gNumCompleted != n)
		Sleep(0);
}

int main() {
	DWORD s, e;

	s = GetTickCount();
	TestScheduler();
	e = GetTickCount();

	printf("%.3f seconds\n", ((double)(e - s))/1000.0f);

	return EXIT_SUCCESS;
}
