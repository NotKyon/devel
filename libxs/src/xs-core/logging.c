#include <xs/logging.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

void XsLogMessageV(const char *filename, xs_uint_t line, const char *function,
int type, const char *format, va_list args) {
	static const char *typenames[4] = {
		"error", "warning", "note", "message"
	};
	int e;

	e = errno;

	if (filename) {
		if (line && ~type & kXsLog_IgnoreLine_Bit)
			fprintf(stderr, "%s(%u): ", filename, (unsigned int)line);
		else
			fprintf(stderr, "%s: ", filename);
	}

	if (function && ~type & kXsLog_IgnoreFunction_Bit)
		fprintf(stderr, "%s: ", function);

	fprintf(stderr, "%s: ", typenames[type & kXsLog_Text_Mask]);

	vfprintf(stderr, format, args);
	if (e && ~type & kXsLog_IgnoreErrno_Bit)
		fprintf(stderr, ": %s [errno:%i]\n", strerror(e), e);
	else
		fprintf(stderr, "\n");

	fflush(stderr);
}

void XsLogMessage(const char *filename, xs_uint_t line, const char *function,
int type, const char *format, ...) {
	va_list args;

	va_start(args, format);
	XsLogMessageV(filename, line, function, type, format, args);
	va_end(args);
}
