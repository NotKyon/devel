//
//	This is a concept file.
//	Not meant for compilation.
//

#include <cmath>
#include <cassert>
#include <cstddef>

#include <xmmintrin.h> //SSE1
#include <emmintrin.h> //SSE2

#define EE_NAMESPACE_START namespace ee {
#define EE_NAMESPACE_END }

EE_NAMESPACE_START

//==============================================================================
//
//	LINKED LISTS
//
//==============================================================================

template<typename T> class ListItem;
template<typename T> class ListBase;

template<typename T>
class ListItem {
protected:
	friend class ListBase<T>;

	T *m_obj;
	ListItem<T> *m_prev, *m_next;
	ListBase<T> *m_base;

public:
	ListItem(T *obj=(T *)0, ListBase<T> *base=(ListBase<T> *)0): m_obj(obj),
	m_base(base) {
		if (m_base) {
			m_next = (ListItem<T> *)0;
			if ((m_prev = m_base->m_tail) != (ListItem<T> *)0)
				m_base->m_tail->m_next = this;
			else
				m_base->m_head = this;
			m_base->m_tail = this;
		}
	}
	~ListItem() {
		Remove();
	}

	void Remove() {
		if (m_prev)
			m_prev->m_next = m_next;
		if (m_next)
			m_next->m_prev = m_prev;

		if (m_base) {
			if (m_base->m_head == this)
				m_base->m_head = m_next;
			if (m_base->m_tail == this)
				m_base->m_tail = m_prev;
		}

		m_prev = (ListItem<T> *)0;
		m_next = (ListItem<T> *)0;

		m_base = (ListBase<T> *)0;
	}

	void SetObject(T *obj) {
		m_obj = obj;
	}
	T *Object() const {
		return m_obj;
	}

	const T *operator->() const {
		return m_obj;
	}
	T *operator->() {
		return m_obj;
	}

	ListItem<T> *ItemBefore() { return m_prev; }
	const ListItem<T> *ItemBefore() const { return m_prev; }

	ListItem<T> *ItemAfter() { return m_next; }
	const ListItem<T> *ItemAfter() const { return m_next; }

	T *Before() { return m_prev ? m_prev->m_obj : (T *)0; }
	const T *Before() const { return m_prev ? m_prev->m_obj : (const T *)0; }

	T *After() { return m_next ? m_next->m_obj : (T *)0; }
	const T *After() const { return m_next ? m_next->m_obj : (const T *)0; }
};

template<typename T>
class ListBase {
protected:
	friend class ListItem<T>;

	ListItem<T> *m_head, *m_tail;

public:
	ListBase(): m_head((ListItem<T> *)0), m_tail((ListItem<T> *)0) {
	}
	~ListBase() {
		RemoveAll();
	}

	void RemoveAll() {
		while(m_head)
			m_head->Remove();
	}
	void DeleteAll() {
		while(m_head) {
			T *obj = m_head->Object();
			m_head->Remove();
			delete obj;
		}
	}

	ListItem<T> *FirstItem() { return m_head; }
	const ListItem<T> *FirstItem() const { return m_head; }

	ListItem<T> *LastItem() { return m_tail; }
	const ListItem<T> *LastItem() const { return m_tail; }

	T *First() { return m_head ? m_head->m_obj : (T *)0; }
	const T *First() const { return m_head ? m_head->m_obj : (T *)0; }

	T *Last() { return m_tail ? m_tail->m_obj : (T *)0; }
	const T *Last() const { return m_tail ? m_tail->m_obj : (T *)0; }
};

//==============================================================================
//
//	MATH
//
//==============================================================================

#if EE_DOUBLE
typedef double Real;
#else
typedef float Real;
#endif

#if __GNUC__ || __clang__
typedef signed long long int int64;
typedef unsigned long long int uint64;
#elif _MSC_VER || __INTEL_COMPILER
typedef signed __int64 int64;
typedef unsigned __int64 uint64;
#else
# include <cstdint>
typedef int64_t int64;
typedef uint64_t uint64;
#endif

//static const Real PI = 3.141592653589793238462643383279502884197169;
static const Real PI = 3.141592653589793; //that's how much accuracy we get
static const Real TWO_PI = PI*2;
static const Real HALF_PI = PI*0.5;
static const Real EPSILON = 0.000001;

class MathTable {
public:
	enum {
		RESOLUTION=32768 //This should be a power-of-two
	};

	static double g_sin[RESOLUTION];
	static double g_cos[RESOLUTION];
	static MathTable *g_self;

	static inline size_t Degrees(Real f) {
		// NOTE: After optimizations, this is two multiplies, and one bit-and.
		return ((size_t)((f/360) * RESOLUTION)) % RESOLUTION;
	}

	inline MathTable() {
		double rad;
		size_t i;

		assert(!g_self); //singleton

		for(i=0; i<RESOLUTION; i++) {
			rad = (((double)i)/RESOLUTION)*TWO_PI;

			g_sin[i] = sin(rad);
			g_cos[i] = cos(rad);
		}
	}
};
extern MathTable g_mathTable;

inline Real DegToRad(Real x) { return x/180.0*PI; }
inline Real RadToDeg(Real x) { return x/PI*180.0; }

inline Real Sin(Real x) { return MathTable::g_sin[MathTable::Degrees(x)]; }
inline Real Cos(Real x) { return MathTable::g_cos[MathTable::Degrees(x)]; }
inline Real Tan(Real x) { return tan(DegToRad(x)); }

inline Real ArcSin(Real x) { return RadToDeg(asin(x)); }
inline Real ArcCos(Real x) { return RadToDeg(acos(x)); }
inline Real ArcTan(Real x) { return RadToDeg(atan(x)); }
inline Real ArcTanFull(Real y, Real x) { return RadToDeg(atan2(y, x)); }

inline Real Lerp(Real x, Real y, Real t) { return x + (y - x)*t; }

inline Real NormalizeAngle360(Real angle) {
#if EE_DOUBLE
	if (angle < 0.0 || angle > 360.0)
		angle -= floor(angle/360.0)*360.0;
#else
	// This simultaneously checks whether angle is above 360.0f or below 0.0
	// (negatives get the sign-bit set). 0x43B40000 == 360.0f.
	if ((*(unsigned int *)&angle) >= 0x43B40000)
		angle -= floorf(angle/360.0f)*360.0f;
#endif

	return angle;
}

inline Real NormalizeAngle180(Real angle) {
	angle = NormalizeAngle360(angle);
	/*
	 * No branching version (32-bit)
	 * -----------------------------
	 * union { float f; int i; } v;
	 *
	 * v.f  = angle;
	 * v.i -= <bit-vector of 180.0f>;
	 * v.i  = -((v.i&0x80000000)>>31) & <bit-vector of 360.0>
	 *
	 * return angle - v.f;
	 *
	 * Assembly (dst, src)
	 * -------------------
	 * mov r1, [angle] //although, r1 will already be angle from the call above
	 * mov r2, r1
	 * sub r2, 180.0f
	 * and r2, 1<<31
	 * shr r2, 31
	 * neg r2
	 * and r2, 360.0f
	 * sub r1, r2
	 *
	 * Analysis
	 * --------
	 * Each of the instructions above should take one cycle. Ignoring the first
	 * mov, that would be a total of 7 cycles.
	 *
	 * If a branch misprediction costs 50 cycles then on average we should
	 * optimistically expect 9 cycles lost due to branch misprediction. The
	 * branching version would then cost around 11~15 cycles. (Worst case at 60
	 * cycles.)
	 *
	 * The non-branching version is better, but GCC does not produce optimal
	 * code matching the assembly. Since this function is not called very often,
	 * there's no point in going through the trouble of writing the assembly due
	 * to portability issues.
	 *
	 * Still, this is an interesting observation.
	 */
	return angle > 180.0 ? angle - 360.0 : angle;
}

inline Real AngleDelta(Real a, Real b) {
	return NormalizeAngle180(a - b);
}

inline int Sign(int x) { return 1 | (x>>(sizeof(x)*8 - 1)); }
inline int64 Sign(int64 x) { return 1 | (x>>(sizeof(x)*8 - 1)); }

inline int Abs(int x) {
	return (x + (x>>(sizeof(x)*8 - 1)))^(x>>(sizeof(x)*8 - 1));
}
inline int64 Abs(int64 x) {
	return (x + (x>>(sizeof(x)*8 - 1)))^(x>>(sizeof(x)*8 - 1));
}

inline float Abs(float x) {
	union { float f; int i; } v;

	v.f  = x;
	v.i &= 0x7FFFFFFF;

	return v.f;
}
inline double Abs(double x) {
	union { double f; int64 i; } v;

	v.f  = x;
	v.i &= 0x7FFFFFFFFFFFFFFF;

	return v.f;
}

// Convert a float to an integer with proper rounding
inline int FloatToInt(float x) {
	union { float f; int i; } v;

	v.f   = x + (float)(3 << 21);
	v.i  -= 0x4AC00000;
	v.i >>= 1;

	return v.i;
}

// Approximate the square root of 'x'
inline float Sqrt(float x) {
	// The SSE version is slower, but more accurate
	union { float f; int i; } v;

	v.f   = x;
	v.i  -= 0x3F800000;
	v.i >>= 1;
	v.i  += 0x3F800000;

	return v.f;
}

// Approximate the reciprocal square root of 'x'
inline float InvSqrt(float x) {
#if EE_USE_CORE_SIMD
	_mm_store_ss(&x, _mm_rsqrt_ss(_mm_load_ss(&x)));
	return x;
#else
	union { float f; int i; } v;

	v.f = x;
	v.i = 0x5F3759DF - (v.i >> 1);

	return v.f*(1.5f - 0.5f*x*v.f*v.f);
#endif
}

// Copy the sign of 'b' into 'a'
inline float CopySign(float a, float b) {
	int i;

	i = ((*(int *)&a)&0x7FFFFFFF)|((*(int *)&b)&0x80000000);

	return *(float *)&i;
}
inline double CopySign(double a, double b) {
	int64 i;

	i = ((*(int64 *)&a)&0x7FFFFFFFFFFFFFFF)|((*(int64 *)&b)&0x8000000000000000);

	return *(double *)&i;
}

// Round 'x' to the nearest fitting power-of-two */
inline int NextPowerOfTwo(int x) {
	if ((x & (x - 1)) != 0) {
		x--;
		x |= x>>1;
		x |= x>>2;
		x |= x>>4;
		x |= x>>8;
		x |= x>>16;
		x++;
	}

	return x;
}
inline int NextSquarePowerOfTwo(int x, int y) {
	return NextPowerOfTwo(x > y ? x : y);
}

// Set the seed for random number generation
inline void Seed(int x) {
	srand(x);
}

// Generate a random number between 'l' and 'h'
inline int Rand(int l, int h) {
	return l + rand()%(h - l);
}
inline float Rand(float l, float h) {
	return l + ((float)(rand() % 256000)/256000.0f)*(h - l);
}
inline double Rand(double l, double h) {
	return l + ((double)(rand() % 1024000)/1024000.0)*(h - l);
}

#if EE_DOUBLE
# define _mm_set_sr        _mm_set_sd
# define _mm_store_sr      _mm_store_sd
# define _mm_min_sr        _mm_min_sd
# define _mm_max_sr        _mm_max_sr
#else
# define _mm_set_sr        _mm_set_ss
# define _mm_store_sr      _mm_store_ss
# define _mm_min_sr        _mm_min_ss
# define _mm_max_sr        _mm_max_ss
#endif

inline Real Clamp(Real x, Real l, Real h) {
#if EE_USE_CORE_SIMD
	_mm_store_sr(&x, _mm_min_sr(_mm_max_sr(_mm_set_sr(x),_mm_set_sr(l)),
		_mm_set_sr(h)));

	return x;
#else
	return x < l ? l : x > h ? h : x;
#endif
}

inline Real ClampSNorm(Real x) {
	return Clamp(x, -1, 1);
}
inline Real ClampUNorm(Real x) {
	return Clamp(x, 0, 1);
}

inline Real Min(Real x, Real y) {
#if EE_USE_CORE_SIMD
	_mm_store_sr(&x, _mm_min_sr(_mm_set_sr(x), _mm_set_sr(y)));
	return x;
#else
	return x < y ? x : y;
#endif
}
inline Real Max(Real x, Real y) {
#if EE_USE_CORE_SIMD
	_mm_store_sr(&x, _mm_max_sr(_mm_set_sr(x), _mm_set_sr(y)));
	return x;
#else
	return x > y ? x : y;
#endif
}

//==============================================================================
//
//	VECTOR 3
//
//==============================================================================

template<typename T>
struct TVec3 {
	union { T x, r, s; };
	union { T y, g, t; };
	union { T z, b, p; };
	//union { T w, a, q; };

	inline TVec3(T x=0, T y=0, T z=0): x(x), y(y), z(z) {}
	inline TVec3(const TVec3<T> &v): x(v.x), y(v.y), z(v.z) {}
	inline ~TVec3() {}

	inline void Zero() { x=0; y=0; z=0; }

	inline T Length() const {
		return Dot(*this, *this);
	}
	inline T Magnitude() const {
		return sqrt(Length());
	}

	inline void Normalize() {
		float rcpmag;

		rcpmag = 1.0f/Magnitude();

		x *= rcpmag;
		y *= rcpmag;
		z *= rcpmag;
	}

	inline TVec3<T> operator+(const TVec3<T> &v) const {
		return TVec3<T>(x + v.x, y + v.y, z + v.z);
	}
	inline TVec3<T> operator-(const TVec3<T> &v) const {
		return TVec3<T>(x - v.x, y - v.y, z - v.z);
	}
	inline TVec3<T> operator*(const T &f) const {
		return TVec3<T>(x*f, y*f, z*f);
	}
	inline TVec3<T> operator*(const TVec3<T> &v) const {
		return TVec3<T>(x*v.x, y*v.y, z*v.z);
	}
	/*
	inline TVec3<T> operator*(const TMat3<T> &M) const {
		return TVec3<T>(x*M.xx + y*M.xy + z*M.xz,
		                x*M.yx + y*M.yy + z*M.yz,
		                x*M.zx + y*M.zy + z*M.zz);
	}
	*/
	inline TVec3<T> operator*(const TMat4<T> &M) const {
		return TVec3<T>(x*M.xx + y*M.xy + z*M.xz + M.xw,
		                x*M.yx + y*M.yy + z*M.yz + M.yw,
		                x*M.zx + y*M.zy + z*M.zz + M.zw);
	}

	inline TVec3<T> operator/(const T &f) const {
		T rcpf = 1.0f/f;

		return TVec3<T>(x*rcpf, y*rcpf, z*rcpf);
	}
	inline TVec3<T> operator/(const TVec3<T> &v) const {
		return TVec3<T>(x/v.x, y/v.y, z/v.z);
	}

	inline TVec3<T> &operator=(const T &f) {
		x = f;
		y = f;
		z = f;

		return *this;
	}
	inline TVec3<T> &operator=(const TVec3<T> &v) {
		x = v.x;
		y = v.y;
		z = v.z;

		return *this;
	}

	inline TVec3<T> &operator+=(const TVec3<T> &v) {
		*this = TVec3<T>(*this) + v;
		return *this;
	}
	inline TVec3<T> &operator-=(const TVec3<T> &v) {
		*this = TVec3<T>(*this) - v;
		return *this;
	}
	inline TVec3<T> &operator*=(const T &f) {
		*this = TVec3<T>(*this)*f;
		return *this;
	}
	inline TVec3<T> &operator*=(const TVec3<T> &v) {
		*this = TVec3<T>(*this)*v;
		return *this;
	}
	inline TVec3<T> &operator*=(const TMat3<T> &M) {
		*this = TVec3<T>(*this)*M;
		return *this;
	}
	inline TVec3<T> &operator*=(const TMat4<T> &M) {
		*this = TVec3<T>(*this)*M;
	}
	inline TVec3<T> &operator/=(const T &f) {
		*this = TVec3<T>(*this)/f;
		return *this;
	}
	inline TVec3<T> &operator/=(const TVec3<T> &v) {
		*this = TVec3<T>(*this)/v;
		return *this;
	}

	inline T &operator[](size_t i) { assert(i < 3); return (&x)[i]; }
};
typedef TVec3<float> Vec3f;
typedef TVec3<double> Vec3d;
typedef TVec3<Real> Vec3;

template<typename T>
inline TVec3<T> Cross(const TVec3<T> &a, const TVec3<T> &b) {
	return TVec3<T>(a.y*b.z - a.z*b.y,
	                a.z*b.x - a.x*b.z,
	                a.x*b.y - a.y*b.x);
}

template<typename T>
inline T Dot(const TVec3<T> &a, const TVec3<T> &b) {
	return a.x*b.x + a.y*b.y + a.z*b.z;
}

template<typename T>
inline TVec3<T> Normal(const TVec3<T> &v) {
	return TVec3<T>(v).Normalize();
}

template<typename T>
inline T Distance(const TVec3<T> &a, const TVec3<T> &b) {
	return (a - b).Magnitude();
}

//==============================================================================
//
//	MATRIX 4
//
//==============================================================================

template<typename T>
struct TMat4 {
	T xx, yx, zx, wx,
	  xy, yy, zy, wy,
	  xz, yz, zz, wz,
	  xw, yw, zw, ww;

	TMat4(): xx(1),xy(0),xz(0),xw(0), yx(0),yy(1),yz(0),yw(0),
	zx(0),zy(0),zz(1),zw(0), wx(0),wy(0),wz(0),ww(1) {
	}
	TMat4(Real xx, Real xy, Real xz, Real xw, Real yx, Real yy, Real yz,
	Real yw, Real zx, Real zy, Real zz, Real zw, Real wx, Real wy, Real wz,
	Real ww): xx(xx),xy(xy),xz(xz),xw(xw), yx(yx),yy(yy),yz(yz),yw(yw),
	zx(zx),zy(zy),zz(zz),zw(zw), wx(wx),wy(wy),wz(wz),ww(ww) {
	}
	TMat4(const TMat4 &M): xx(M.xx),xy(M.xy),xz(M.xz),xw(M.xw),
	yx(M.yx),yy(M.yy),yz(M.yz),yw(M.yw), zx(M.zx),zy(M.zy),zz(M.zz),zw(M.zw),
	wx(M.wx),wy(M.wy),wz(M.wz),ww(M.ww) {
	}
	~TMat4() {
	}

	void Transpose() {
		*this = TMat4<T>(xx,yx,zx,wx, xy,yy,zy,wy, xz,yz,zz,wz, xw,yw,zw,ww);
	}

	TMat4<T> &Translate(const Vec3<T> &v) {
		xw = v.x;
		yw = v.y;
		zw = v.z;

		return *this;
	}

	TMat4<T> &Rotate(TVec3<T> &euler) {
		// NASA rotation order: ZXY
		return RotateZ(euler.z).TurnX(euler.x).TurnY(euler.y);
	}
	TMat4<T> &RotateX(T angle) {
		float c, s;

		c = Cos(angle);
		s = Sin(angle);

		xx=1;  xy=0;  xz=0;
		yx=0;  yy=c;  yz=-s;
		zx=0;  zy=s;  zz=c;

		return *this;
	}
	TMat4<T> &RotateY(T angle) {
		float c, s;

		c = Cos(angle);
		s = Sin(angle);

		xx=c;  xy=0;  xz=s;
		yx=0;  yy=1;  yz=0;
		zx=-s; zy=0;  zz=c;

		return *this;
	}
	TMat4<T> &RotateZ(T angle) {
		float c, s;

		c = Cos(angle);
		s = Sin(angle);

		xx=c;  xy=-s; xz=0;
		yx=s;  yy=c;  yz=0;
		zx=0;  zy=0;  zz=1;

		return *this;
	}

	TMat4<T> &Move(const Vec3<T> &v) {
		xw += xx*v.x + xy*v.y + xz*v.z;
		yw += yx*v.x + yy*v.y + yz*v.z;
		zw += zx*v.x + zy*v.y + zz*v.z;

		return *this;
	}
	TMat4<T> &MoveX(T x) {
		xw += xx*x;
		yw += yx*x;
		zw += zx*x;

		return *this;
	}
	TMat4<T> &MoveY(T y) {
		xw += xy*y;
		yw += yy*y;
		zw += zy*y;

		return *this;
	}
	TMat4<T> &MoveZ(T z) {
		xw += xz*z;
		yw += yz*z;
		zw += zz*z;

		return *this;
	}

	TMat4<T> &Turn(const Vec3<T> &euler) {
		// NASA rotation order: ZXY
		return TurnZ(euler.z).TurnX(euler.x).TurnY(euler.y);
	}
	TMat4<T> &TurnX(const T angle) {
		T c, s, t;

		c = Cos(angle);
		s = Sin(angle);

		t = xy;
		xy = t*c  + xz*s;
		xz = t*-s + xz*c;

		t = yy;
		yy = t*c  + yz*s;
		yz = t*-s + yz*c;

		t = zy;
		zy = t*c  + zz*s;
		zz = t*-s + zz*c;

		return *this;
	}
	TMat4<T> &TurnY(const T angle) {
		T c, s, t;

		c = Cos(angle);
		s = Sin(angle);

		t = xx;
		xx = t*c  + xz*s;
		xz = t*-s + xz*c;

		t = yx;
		yx = t*c  + yz*s;
		yz = t*-s + yz*c;

		t = zx;
		zx = t*c  + zz*s;
		zz = t*-s + zz*c;

		return *this;
	}
	TMat4<T> &TurnZ(const T angle) {
		T c, s, t;

		c = Cos(angle);
		s = Sin(angle);

		t = xx;
		xx = t*c  + xy*s;
		xy = t*-s + xy*c;

		t = yx;
		yx = t*c  + yy*s;
		yy = t*-s + yy*c;

		t = zx;
		zx = t*c  + zy*s;
		zy = t*-s + zy*c;

		return *this;
	}

	TMat4<T> operator*(const TMat4<T> &M) const {
		return TMat4<T>(xx*M.xx + xy*M.yx + xz*M.zx + xw*M.wx,
		                xx*M.xy + xy*M.yy + xz*M.zy + xw*M.wy,
		                xx*M.xz + xy*M.yz + xz*M.zz + xw*M.wz,
		                xx*M.xw + xy*M.yw + xz*M.zw + xw*M.ww,

		                yx*M.xx + yy*M.yx + yz*M.zx + yw*M.wx,
		                yx*M.xy + yy*M.yy + yz*M.zy + yw*M.wy,
		                yx*M.xz + yy*M.yz + yz*M.zz + yw*M.wz,
		                yx*M.xw + yy*M.yw + yz*M.zw + yw*M.ww,

		                zx*M.xx + zy*M.yx + zz*M.zx + zw*M.wx,
		                zx*M.xy + zy*M.yy + zz*M.zy + zw*M.wy,
		                zx*M.xz + zy*M.yz + zz*M.zz + zw*M.wz,
		                zx*M.xw + zy*M.yw + zz*M.zw + zw*M.ww,

		                wx*M.xx + wy*M.yx + wz*M.zx + ww*M.wx,
		                wx*M.xy + wy*M.yy + wz*M.zy + ww*M.wy,
		                wx*M.xz + wy*M.yz + wz*M.zz + ww*M.wz,
		                wx*M.xw + wy*M.yw + wz*M.zw + ww*M.ww);
	}

	TMat4<T> &operator=(const TMat4<T> &M) {
		xx=M.xx; xy=M.xy; xz=M.xz; xw=M.xw;
		yx=M.yx; yy=M.yy; yz=M.yz; yw=M.yw;
		zx=M.zx; zy=M.zy; zz=M.zz; zw=M.zw;
		wx=M.wx; wy=M.wy; wz=M.wz; ww=M.ww;

		return *this;
	}

	TMat4<T> &operator*=(const TMat4<T> &M) {
		*this = TMat4<T>(*this) * M;
	}

	static inline TMat4<T> Identity() {
		return TMat4<T>();
	}

	static inline TMat4<T> Translation(const Vec3<T> &v) {
		return TMat4<T>().Translate(v);
	}
	static inline TMat4<T> TranslationX(const Real x) {
		return TMat4<T>().Translate(TVec3<T>(x,0,0));
	}
	static inline TMat4<T> TranslationY(const Real x) {
		return TMat4<T>().Translate(TVec3<T>(0,y,0));
	}
	static inline TMat4<T> TranslationZ(const Real x) {
		return TMat4<T>().Translate(TVec3<T>(0,0,z));
	}

	static inline TMat4<T> Rotation(const Vec3<T> &euler) {
		return TMat4<T>().Rotate(euler);
	}
	static inline TMat4<T> RotationX(const Real x) {
		return TMat4<T>().RotateX(x);
	}
	static inline TMat4<T> RotationY(const Real y) {
		return TMat4<T>().RotateY(y);
	}
	static inline TMat4<T> RotationZ(const Real z) {
		return TMat4<T>().RotateZ(z);
	}
};
typedef TMat4<float> Mat4f;
typedef TMat4<double> Mat4d;
typedef TMat4<Real> Mat4;

EE_NAMESPACE_END
