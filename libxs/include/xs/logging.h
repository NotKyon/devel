#ifndef XS_LOGGING_H
#define XS_LOGGING_H

#include "basetypes.h"

enum {
	kXsLog_Error,
	kXsLog_Warning,
	kXsLog_Note,
	kXsLog_Message,

	kXsLog_Text_Mask = 0x03,

	kXsLog_IgnoreLine_Bit = 0x04,
	kXsLog_IgnoreFunction_Bit = 0x08,
	kXsLog_IgnoreErrno_Bit = 0x10,

	kXsLog_Remark = kXsLog_Note /*alias*/
};

/*
 * Log a message (to stderr) using details about where the message is from. If
 * errno is set, the message will be appended with the error message
 * corresponding to the errno code, and the number of the errno code.
 *
 * >filename (const char *): Name of the file the message is from. Can be NULL.
 * >line (xs_uint_t): The line of the file the message is from. Must be 0 if
 *                    filename is NULL. If this value is 0, it is ignored.
 * >function (const char *): Name of the function this message is referring to.
 *                           If NULL this argument is ignored.
 * >type (int): The type of message to be logged + various settings. The type
 *              of message is (type & kXsLog_Text_Mask). If
 *              kXsLog_IgnoreLine_Bit is set, the line parameter will be ignored
 *              (regardless of whether it's zero or not). Similarly, if
 *              kXsLog_IgnoreFunction_Bit is set, the function parameter is
 *              ignored. If kXsLog_IgnoreErrno_Bit is set, the value of errno
 *              will be ignored in the generation of the message.
 * >format (const char *): Formatting string (same format as printf()).
 * >args (va_list): Arguments used to format the message.
 */
void XsLogMessageV(const char *filename, xs_uint_t line, const char *function,
int type, const char *format, va_list args);

/*
 * Invoke XsLogMessageV() with the arguments list automatically generated.
 *
 * >filename (const char *): Name of the file the message is from. Can be NULL.
 * >line (xs_uint_t): The line of the file the message is from. Must be 0 if
 *                    filename is NULL. If this value is 0, it is ignored.
 * >type (int): See XsLogMessageV().
 */
void XsLogMessage(const char *filename, xs_uint_t line, const char *function,
int type, const char *format, ...);

#endif
