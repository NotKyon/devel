#ifndef MK_LIB_H
#define MK_LIB_H

#include "mk_types.h"
#include "mk_project.h"

enum {
	kLib_Processed_Bit = BIT(0)
};

typedef struct lib_s {
	char *name;

	char *flags[kNumProjSys];

	bitfield_t config;

	struct lib_s *prev, *next;
} *lib_t;

extern lib_t g_lib_head;
extern lib_t g_lib_tail;

lib_t NewLib();
void DeleteLib(lib_t lib);
void DeleteAllLibs();

void SetLibName(lib_t lib, const char *name);
void SetLibFlags(lib_t lib, int sys, const char *flags);

const char *LibName(lib_t lib);
const char *LibFlags(lib_t lib, int sys);

lib_t LibBefore(lib_t lib);
lib_t LibAfter(lib_t lib);
lib_t FirstLib();
lib_t LastLib();
lib_t FindLib(const char *name);

void ClearLibProcessed(lib_t lib);
void SetLibProcessed(lib_t lib);
int TestLibProcessed(lib_t lib);

void ClearAllLibsProcessed();
void SetAllLibsProcessed();
int TestAnyLibsProcessed();

#endif
