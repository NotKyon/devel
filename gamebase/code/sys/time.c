#if _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif
#include <gamebase.h>

#include <time.h>
#ifndef _WIN32
# include <sched.h>
#endif

void gbYield() {
#if _WIN32
	Sleep(0);
#else
	sched_yield();
#endif
}
void gbDelay(gb_uint_t time) {
	if (!time)
		return;

#if _WIN32
	Sleep(time);
#else
	{
		struct timespec ts;

		ts.tv_sec  =  time/1000;
		ts.tv_nsec = (time%1000)*1000000; /*one-million nano per milli*/

		/*
		 * TODO: handle this more appropriately...
		 */
		nanosleep(&ts, (struct timespec *)NULL);
	}
#endif
}
void gbMicroDelay(gb_u64_t time) {
	if (!time)
		return;

#if _WIN32
	/* TODO: Investigate NtDelayExecution()...
		NTSYSAPI NTSTATUS NTAPI NtDelayExecution
		(
		  IN BOOLEAN              Alertable,
		  IN PLARGE_INTEGER       DelayInterval
		);
	*/
#else
	{
		struct timespec ts;

		ts.tv_sec  =  time/1000000;
		ts.tv_nsec = (time%1000000)*1000;

		nanosleep(&ts, (struct timespec *)NULL);
	}
#endif
}
gb_uint_t gbMillisecs() {
#if _WIN32
	return GetTickCount();
#else
	return clock();
#endif
}
gb_u64_t gbMicrosecs() {
#if _WIN32
	gb_u64_t f, t;

	/*
	 * TODO: Remove use of doubles when calculating
	 * NOTE: Be careful doing so; integer precision may cause highly incorrect
	 *       results...
	 */
	QueryPerformanceFrequency((LARGE_INTEGER *)&f);
	QueryPerformanceCounter((LARGE_INTEGER *)&t);

	return (gb_u64_t)((((double)t)/(double)f)*1000000.0);
#else
	struct timespec ts;

	/*
	 * NOTE: Avoiding the use of CLOCK_REALTIME and CLOCK_*_CPUTIME_ID. The
	 *       REALTIME variant is ignored because it may be slightly slower (by
	 *       applying an offset). The CPUTIME_ID variants are ignored because
	 *       they are unreliable in an SMP environment.
	 */
	clock_gettime(CLOCK_MONOTONIC, &ts);
	/*gettimeofday(&ts, NULL);*/

	return (gb_u64_t)(ts.tv_sec*1000000) + (gb_u64_t)(ts.tv_nsec/1000);
#endif
}
void gbQueryPerfTimer(gb_u64_t *seconds, gb_u64_t *nanoseconds) {
	gbAssert(seconds != (gb_u64_t *)NULL);
	gbAssert(nanoseconds != (gb_u64_t *)NULL);

	{
#if _WIN32
		gb_u64_t f, t;

		QueryPerformanceFrequency((LARGE_INTEGER *)&f);
		QueryPerformanceCounter((LARGE_INTEGER *)&t);

		t = ((double)t)/(((double)f)/1000000000.0);

		*seconds     = t/1000000000;
		*nanoseconds = t%1000000000;
#else
		struct timespec ts;

		clock_gettime(CLOCK_MONOTONIC, &ts);

		*seconds     = ts.tv_sec ;
		*nanoseconds = ts.tv_nsec;
#endif
	}
}
