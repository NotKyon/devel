float4x4    World,
            View,
            Projection,
            WorldInverse,
            ViewInverse,
            ProjectionInverse,
            WorldTranspose,
            ViewTranspose,
            ProjectionTranspose,
            WorldInverseTranspose,
            ViewInverseTranspose,
            ProjectionInverseTranspose,
            WorldView,
            WorldProjection,
            ViewProjection,
            WorldViewProjection,
            WorldViewInverse,
            WorldProjectionInverse,
            ViewProjectionInverse,
            WorldViewProjectionInverse,
            WorldViewTranspose,
            WorldProjectionTranspose,
            ViewProjectionTranspose,
            WorldViewProjectionTranspose,
            WorldViewInverseTranspose,
            WorldProjectionInverseTranspose,
            ViewProjectionInverseTranspose,
            WorldViewProjectionInverseTranspose;

{
float4x4    LightWorld,
            LightView,
            LightProjection,
            LightWorldInverse,
            LightViewInverse,
            LightProjectionInverse,
            LightWorldTranspose,
            LightViewTranspose,
            LightProjectionTranspose,
            LightWorldInverseTranspose,
            LightViewInverseTranspose,
            LightProjectionInverseTranspose,
            LightWorldView,
            LightWorldProjection,
            LightViewProjection,
            LightWorldViewProjection,
            LightWorldViewInverse,
            LightWorldProjectionInverse,
            LightViewProjectionInverse,
            LightWorldViewProjectionInverse,
            LightWorldViewTranspose,
            LightWorldProjectionTranspose,
            LightViewProjectionTranspose,
            LightWorldViewProjectionTranspose,
            LightWorldViewInverseTranspose,
            LightWorldProjectionInverseTranspose,
            LightViewProjectionInverseTranspose,
            LightWorldViewProjectionInverseTranspose;
float4      Diffuse,
            Specular,
            Ambient,
            Emissive;
float3      LightVecL | LightVec | Direction,
            LightVecW,
            LightPosL | LightPos | Position,
            LightPosW,
            Attenuation;
float       SpecularPower | Shininess;
} <
    string Object = "Light##n", //Lighting
                    "Sun",      //Lighting (select the sun-light)
                    "";         //Material (for some)
    string Space  = "World",    //World space ('W')
                    "Object",   //Object space ('L')
                    "";         //Default
  >;

{
texture     Diffuse,            //basic diffuse
            Normal,             //normal-map
            Height,             //height-map
            Emissive | Glow,    //emissive-map
            Specular,           //specular-map
            Thickness,          //thickness-map
            Virtual,            //virtual texturing indirection texture
            Physical,           //virtual texturing pages texture
            RenderColorTarget,
            RenderDepthStencilTarget;
} <
    string  ResourceName = (Filename),
            ResourceType = "1D",
                           "2D",
                           "3D",
                           "Cube",
            Format       = (PixelFormat);
    float2  Dimensions,
            ViewPortRatio;
    int     MipLevels;
  >;
float3      DirToSun;
float3      FogColor;
float2      FogRange;
float       FogNear, FogFar;

float       Time,
            LastTime,
            ElapsedTime;
float3      Accel;              //for particle systems

float3      EyePosW | EyePos | CameraPos,
            EyePosL | CameraPosL,
            CornerFrustum;

    /*
     * CornerFrustum
     *
     * cornerFrustum.y = tan(pi/3.0f/2.0f)*camera_FarClip;
     * cornerFrustum.x = cornerFrustum.y*camera_AspectRatio;
     * cornerFrustum.z = camera_FarClip;
     *
     * See: http://projectvanquish.wordpress.com/category/xna/ssao/
     * http://projectvanquish.wordpress.com/2012/02/16/screen-space-ambient-
     *  occlusion/
     */


floatMxN    Bones[Q];           //bone palette

float4      TextureSize;        //x=width,y=height,z=depth,w=mip-levels

texture     CastTexture;
float4x4    CastViewProjection;
float3      CastPosition;
bool        CastActive;
int         CastShadowActive;
int         CastShadowReverseLookup;

float4      Custom0,
            Custom1,
            Custom2,
            Custom3;
