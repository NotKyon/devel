#ifndef LOOKUP_H
#define LOOKUP_H

#include "local.h"

#define LOOKUP_LOWER "abcdefghijklmnopqrstuvwxyz"
#define LOOKUP_UPPER "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define LOOKUP_DIGIT "0123456789"
#define LOOKUP_UNDER "_"
#define LOOKUP_IDENT LOOKUP_LOWER LOOKUP_UPPER LOOKUP_DIGIT LOOKUP_UNDER

typedef struct entry_s {
	struct entry_s *entries;
	void *p;
} entry_t;
typedef struct lookup_s {
	int convmap[256];

	int numEntries;
	struct entry_s *entries;
} lookup_t;

int InitLookup(lookup_t *table, const char *allowed);
void DeleteLookupEntries(lookup_t *table, entry_t *entries);
void DeinitLookup(lookup_t *table);

entry_t *FindEntry(lookup_t *table, const char *str);
entry_t *LookupEntry(lookup_t *table, const char *str);

entry_t *NewEntry(lookup_t *table, const char *str, void *p);
entry_t *SetEntry(lookup_t *table, const char *str, void *p);
void *GetEntry(lookup_t *table, const char *str);

#endif
