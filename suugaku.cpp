﻿#include "suugaku.hpp"

#include <math.h>

namespace suugaku
{

	// Internal namespace
	namespace detail
	{

		/*
			M[ 0]=xx; M[ 1]=yx; M[ 2]=zx; M[ 3]=wx;
			M[ 4]=xy; M[ 5]=yy; M[ 6]=zy; M[ 7]=wy;
			M[ 8]=xz; M[ 9]=yz; M[10]=zz; M[11]=wz;
			M[12]=xw; M[13]=yw; M[14]=zw; M[15]=ww;
		*/

		template< typename tMat >
		Vec3f RowTransform( const tMat &M, const Vec3f &V )
		{
			return
				Vec3f
				(
					M.xx*V.x + M.xy*V.y + M.xz*V.z,
					M.yx*V.x + M.yy*V.y + M.yz*V.z,
					M.zx*V.x + M.zy*V.y + M.zz*V.z
				);
		}
		template< typename tMat >
		Vec3f ColumnTransform( const tMat &M, const Vec3f &V )
		{
			return
				Vec3f
				(
					M.xx*V.x + M.yx*V.y + M.zx*V.z,
					M.xy*V.x + M.yy*V.y + M.zy*V.z,
					M.xz*V.x + M.yz*V.y + M.zz*V.z
				);
		}
		
		template< typename tMat >
		inline tMat &LoadRotation( tMat &O, const Vec3f &I )
		{
			fp_t sx,sy,sz, cx,cy,cz;

			sx=Sin(I.x); sy=Sin(I.y); sz=Sin(I.z);
			cx=Cos(I.x); cy=Cos(I.y); cz=Cos(I.z);

			const fp_t cz_zx = cz*-sx;
			const fp_t zy = -sy;
			
			O.xx = cz*cy;            O.xy = sz*cy + cz_zx*sy; O.xz = cx*sy;
			O.yx =-sz*cx;            O.yy = cz*cx;            O.yz = sx;
			O.zx = cz*zy + sz*sx*cy; O.zy = sz*zy + cz_zx*cy; O.zz = cx*cy;
			
			return O;
		}

		template< typename tMat >
		inline tMat &ApplyXRotation( tMat &O, fp_t I )
		{
			const fp_t s = Sin( I );
			const fp_t c = Cos( I );
			const fp_t z = -s;

			fp_t t;

			t = O.yx;
			O.yx = t*c + O.zx*s;
			O.zx = t*z + O.zx*c;

			t = O.yy;
			O.yy = t*c + O.zy*s;
			O.zy = t*z + O.zy*c;

			t = O.yz;
			O.yz = t*c + O.zz*s;
			O.zz = t*z + O.zz*c;

			return O;
		}
		template< typename tMat >
		inline tMat &ApplyYRotation( tMat &O, fp_t I )
		{
			const fp_t s = Sin( I );
			const fp_t c = Cos( I );
			const fp_t z = -s;

			fp_t t;

			t = O.xx;
			O.xx = t*c + O.zx*s;
			O.zx = t*z + O.zx*c;

			t = O.xy;
			O.xy = t*c + O.zy*s;
			O.zy = t*z + O.zy*c;

			t = O.xz;
			O.xz = t*c + O.zz*s;
			O.zz = t*z + O.zz*c;

			return O;
		}
		template< typename tMat >
		inline tMat &ApplyZRotation( tMat &O, fp_t I )
		{
			const fp_t s = Sin( I );
			const fp_t c = Cos( I );
			const fp_t z = -s;

			fp_t t;

			t = O.xx;
			O.xx = t*c + O.yx*s;
			O.yx = t*z + O.yx*c;

			t = O.xy;
			O.xy = t*c + O.yy*s;
			O.yy = t*z + O.yy*c;

			t = O.xz;
			O.xz = t*c + O.yz*s;
			O.yz = t*z + O.yz*c;

			return O;
		}

		template< typename tMat >
		inline tMat &ApplyScaling( tMat &O, const Vec3f &Scaling )
		{
			O.xx *= Scaling.x; O.yx *= Scaling.x; O.zx *= Scaling.x;
			O.xy *= Scaling.y; O.yy *= Scaling.y; O.zy *= Scaling.y;
			O.xz *= Scaling.z; O.yz *= Scaling.z; O.zz *= Scaling.z;

			return O;
		}

		template< typename tMat >
		inline void Decompose( const tMat &I, Vec3f &OutEulerAngles, Vec3f &OutScaling )
		{
			const fp_t ssx = Sqrt( I.xx*I.xx + I.xy*I.xy + I.xz*I.xz );
			const fp_t ssy = Sqrt( I.yx*I.yx + I.yy*I.yy + I.yz*I.yz );
			const fp_t ssz = Sqrt( I.zx*I.zx + I.zy*I.zy + I.zz*I.zz );

			OutScaling = Vec3f( ssx, ssy, ssz );

			const fp_t sx = fp_t(1)/ssx;
			const fp_t sy = fp_t(1)/ssy;
			const fp_t sz = fp_t(1)/ssz;

			const Vec3f vx( I.RowX()*sx );
			const Vec3f vy( I.RowY()*sy );
			const Vec3f vz( I.RowZ()*sz );
			
			OutEulerAngles.x = Degrees( asin( vz.y ) );
			OutEulerAngles.y = Degrees( atan2( vz.x, vz.z ) );
			OutEulerAngles.z = Degrees( atan2( vx.y, vy.y ) );
		}

	}

	/*
	========================================================================
	
		FUNCTIONS
	
	========================================================================
	*/

	fp_t Cos( fp_t fAngleInDegrees )
	{
		return cos( Radians( fAngleInDegrees ) );
	}
	fp_t Sin( fp_t fAngleInDegrees )
	{
		return sin( Radians( fAngleInDegrees ) );
	}
	fp_t Tan( fp_t fAngleInDegrees )
	{
		return tan( Radians( fAngleInDegrees ) );
	}
	fp_t Sqrt( fp_t fValue )
	{
		return sqrt( fValue );
	}

	fp_t ClampSNorm( fp_t fValue )
	{
		return fValue < -1 ? -1 : ( fValue > 1 ? 1 : fValue );
	}
	fp_t ClampUNorm( fp_t fValue )
	{
		return fValue < 0 ? 0 : ( fValue > 1 ? 1 : fValue );
	}
	
	fp_t Dot( const Vec2f &A, const Vec2f &B )
	{
		return A.x*B.x + A.y*B.y;
	}
	fp_t Dot( const Vec3f &A, const Vec3f &B )
	{
		return A.x*B.x + A.y*B.y + A.z*B.z;
	}
	fp_t Dot( const Vec4f &A, const Vec4f &B )
	{
		return A.x*B.x + A.y*B.y + A.z*B.z + A.w*B.w;
	}

	Vec3f Cross( const Vec3f &A, const Vec3f &B )
	{
		return
			Vec3f
			(
				A.y*B.z - A.z*B.y,
				A.z*B.x - A.x*B.z,
				A.x*B.y - A.y*B.x
			);
	}

	Vec3f VectorLocalToGlobal( const Mat3f &M, const Vec3f &V )
	{
		return detail::RowTransform( M, V );
	}
	Vec3f VectorGlobalToLocal( const Mat3f &M, const Vec3f &V )
	{
		return detail::ColumnTransform( M, V );
	}

	Vec3f PointLocalToGlobal( const Mat4f &M, const Vec3f &P )
	{
		return detail::RowTransform( M, P ) + Vec3f( M.xw, M.yw, M.zw );
	}
	Vec3f PointGlobalToLocal( const Mat4f &M, const Vec3f &P )
	{
		const Vec3f Q( P.x - M.xw, P.y - M.yw, P.z - M.zw );
		return detail::ColumnTransform( M, Q );
	}
	Vec3f VectorLocalToGlobal( const Mat4f &M, const Vec3f &V )
	{
		return detail::RowTransform( M, V );
	}
	Vec3f VectorGlobalToLocal( const Mat4f &M, const Vec3f &V )
	{
		return detail::ColumnTransform( M, V );
	}

	Vec3f MoveVector( const Mat3f &ObjectLocalXf, const Vec3f &Distance )
	{
		return detail::RowTransform( ObjectLocalXf, Distance );
	}
	Vec3f MoveVector( const Mat4f &ObjectLocalXf, const Vec3f &Distance )
	{
		return
			detail::RowTransform( ObjectLocalXf, Distance ) +
			Vec3f( ObjectLocalXf.xw, ObjectLocalXf.yw, ObjectLocalXf.zw );
	}

	static inline fp_t NegateSigned( fp_t fValue, fp_t fCheckSign )
	{
		return fCheckSign < 0 ? -fValue : fValue;
	}
	static inline fp_t NegateUnsigned( fp_t fValue, fp_t fCheckSign )
	{
		return fCheckSign > 0 ? -fValue : fValue;
	}

	Vec3f LookAt( const Vec3f &Source, const Vec3f &Target )
	{
		const Vec3f P( ( Target - Source ).Normalized() );

		const fp_t y = atan2( P.x, P.z );
		const fp_t x = NegateSigned( atan2( P.y*cos(y), fabs( P.z ) ), P.z );

		return Vec3f( Degrees( x ), Degrees( y ), 0 );
	}




	/*
	========================================================================
	
		MATRIX 3x3
	
	========================================================================
	*/

	Mat3f &Mat3f::LoadIdentity()
	{
		*this = Mat3f();
		return *this;
	}
	Mat3f &Mat3f::LoadTranspose()
	{
		*this = Mat3f( RowX(), RowY(), RowZ() );
		return *this;
	}
	Mat3f &Mat3f::LoadTranspose( const Mat3f &Other )
	{
		*this = Mat3f( Other.RowX(), Other.RowY(), Other.RowZ() );
		return *this;
	}

	Mat3f &Mat3f::LoadRotation( const Vec3f &AnglesDegrees )
	{
		return detail::LoadRotation( *this, AnglesDegrees );
	}
	Mat3f &Mat3f::LoadScaling( const Vec3f &Scaling )
	{
		const fp_t a = Scaling.x;
		const fp_t b = Scaling.y;
		const fp_t c = Scaling.z;

		xx = a; yx = 0; zx = 0;
		xy = 0; yy = b; zy = 0;
		xz = 0; yz = 0; zz = c;

		return *this;
	}

	Mat3f &Mat3f::ApplyRotation( const Vec3f &AnglesDegrees )
	{
		return
			ApplyZRotation( AnglesDegrees.z ).
			ApplyXRotation( AnglesDegrees.x ).
			ApplyYRotation( AnglesDegrees.y );
	}
	Mat3f &Mat3f::ApplyXRotation( fp_t fXAngleDegrees )
	{
		return detail::ApplyXRotation( *this, fXAngleDegrees );
	}
	Mat3f &Mat3f::ApplyYRotation( fp_t fYAngleDegrees )
	{
		return detail::ApplyYRotation( *this, fYAngleDegrees );
	}
	Mat3f &Mat3f::ApplyZRotation( fp_t fZAngleDegrees )
	{
		return detail::ApplyZRotation( *this, fZAngleDegrees );
	}
	Mat3f &Mat3f::ApplyScaling( const Vec3f &Scaling )
	{
		return detail::ApplyScaling( *this, Scaling );
	}

	void Mat3f::Decompose( Vec3f &dstEulerAngles, Vec3f &dstScale ) const
	{
		detail::Decompose( *this, dstEulerAngles, dstScale );
	}

	Mat3f &Mat3f::LoadMultiply( const Mat3f &A, const Mat3f &B )
	{
		/*
			The following code is equivalent to:

				xx = Dot( A.ColumnX(), B.RowX() );
				xy = Dot( A.ColumnX(), B.RowY() );
				xz = Dot( A.ColumnX(), B.RowZ() );

				yx = Dot( A.ColumnY(), B.RowX() );
				yy = Dot( A.ColumnY(), B.RowY() );
				yz = Dot( A.ColumnY(), B.RowZ() );

				zx = Dot( A.ColumnZ(), B.RowX() );
				zy = Dot( A.ColumnZ(), B.RowY() );
				zw = Dot( A.ColumnZ(), B.RowZ() );
		*/

		xx = A.xx*B.xx + A.xy*B.yx + A.xz*B.zx;
		xy = A.xx*B.xy + A.xy*B.yy + A.xz*B.zy;
		xz = A.xx*B.xz + A.xy*B.yz + A.xz*B.zz;

		yx = A.yx*B.xx + A.yy*B.yx + A.yz*B.zx;
		yy = A.yx*B.xy + A.yy*B.yy + A.yz*B.zy;
		yz = A.yx*B.xz + A.yy*B.yz + A.yz*B.zz;

		zx = A.zx*B.xx + A.zy*B.yx + A.zz*B.zx;
		zy = A.zx*B.xy + A.zy*B.yy + A.zz*B.zy;
		zz = A.zx*B.xz + A.zy*B.yz + A.zz*B.zz;

		return *this;
	}

	fp_t Mat3f::Determinant() const
	{
		/*
			The following code is equivalent to:
			
				return Dot( RowX(), Cross( RowY(), RowZ() ) );
		*/
		return
			xx*( yy*zz - zy*yz ) +
			yx*( zy*xz - xy*zz ) +
			zx*( xy*yz - yy*xz );
	}




	/*
	========================================================================
	
		MATRIX 4x4
	
	========================================================================
	*/

	Mat4f &Mat4f::LoadIdentity()
	{
		*this = Mat4f();
		return *this;
	}
	Mat4f &Mat4f::LoadTranspose()
	{
		*this = Mat4f( RowX(), RowY(), RowZ(), RowW() );
		return *this;
	}
	Mat4f &Mat4f::LoadTranspose( const Mat4f &Other )
	{
		*this = Mat4f( Other.RowX(), Other.RowY(), Other.RowZ(), Other.RowW() );
		return *this;
	}

	Mat4f &Mat4f::LoadPerspProj( fp_t fFovDeg, fp_t fAspect, fp_t fZNear, fp_t fZFar )
	{
		const fp_t zn = fZNear;
		const fp_t zf = fZFar;

		fp_t a, b, c, d;

		b = 1.0f/Tan(0.5f*fFovDeg);
		a = b/fAspect;

		c =     zf/(zf - zn);
		d = -zn*zf/(zf - zn);

		xx=a; yx=0; zx=0; wx=0;
		xy=0; yy=b; zy=0; wy=0;
		xz=0; yz=0; zz=c; wz=1;
		xw=0; yw=0; zw=d; ww=0;

		return *this;
	}
	Mat4f &Mat4f::LoadOrthoProj( fp_t fLeft, fp_t fRight, fp_t fBottom, fp_t fTop, fp_t fZNear, fp_t fZFar )
	{
		const fp_t  l = fLeft;
		const fp_t  r = fRight;
		const fp_t  b = fBottom;
		const fp_t  t = fTop;
		const fp_t zn = fZNear;
		const fp_t zf = fZFar;

		fp_t A, B, C, D, E, F;

		A =     2.0/(r  -  l);
		B = (l + r)/(l  -  r);
		C =     2.0/(t  -  b);
		D = (t + b)/(b  -  t);
		E =     1.0/(zf - zn);
		F =      zn/(zn - zf);

		xx=A; yx=0; zx=0; wx=0;
		xy=0; yy=C; zy=0; wy=0;
		xz=0; yz=0; zz=E; wz=0;
		xw=B; yw=D; zw=F; ww=1;

		return *this;
	}

	bool Mat4f::IsProjPersp() const
	{
		return wz >= 1 - SUUGAKU_EPSILON && wz <= 1 + SUUGAKU_EPSILON;
	}
	bool Mat4f::IsProjOrtho() const
	{
		return wz >= 0 - SUUGAKU_EPSILON && wz <= 0 + SUUGAKU_EPSILON;
	}
	float Mat4f::GetPerspProjFov() const
	{
		// avoid divide-by-zero
		if( yy >= 0 - SUUGAKU_EPSILON && yy <= 0 + SUUGAKU_EPSILON ) {
			return 0;
		}

		return Degrees( atan( 1/yy ) )*2;
	}
	float Mat4f::GetPerspZNear() const
	{
		return -zw/zz;
	}
	float Mat4f::GetPerspZFar() const
	{
		return zw/( 1 - zz );
	}

	Mat4f &Mat4f::LoadTranslation( const Vec3f &Translation )
	{
		*this = Mat4f();
		return SetTranslation( Translation );
	}
	Mat4f &Mat4f::LoadRotation( const Vec3f &AnglesDegrees )
	{
		detail::LoadRotation( *this, AnglesDegrees );

		wx = 0;
		wy = 0;
		wz = 0;
		xw = 0;
		yw = 0;
		zw = 0;
		ww = 1;

		return *this;
	}
	Mat4f &Mat4f::LoadScaling( const Vec3f &Scaling )
	{
		const fp_t a = Scaling.x;
		const fp_t b = Scaling.y;
		const fp_t c = Scaling.z;

		xx = a; yx = 0; zx = 0;
		xy = 0; yy = b; zy = 0;
		xz = 0; yz = 0; zz = c;

		return *this;
	}

	Mat4f &Mat4f::LoadAffine( const Mat3f &Rotation, const Vec3f &Position )
	{
		xx = Rotation.xx; yx = Rotation.yx; zx = Rotation.zx; wx = 0;
		xy = Rotation.xy; yy = Rotation.yy; zy = Rotation.zy; wy = 0;
		xz = Rotation.xz; yz = Rotation.yz; zz = Rotation.zz; wz = 0;
		xw = Position.x ; yw = Position.y ; zw = Position.z ; ww = 1;

		return *this;
	}

	Mat4f &Mat4f::SetTranslation( const Vec3f &Translation )
	{
		xw = Translation.x;
		yw = Translation.y;
		zw = Translation.z;

		return *this;
	}

	Mat4f &Mat4f::ApplyTranslation( const Vec3f &Translation )
	{
		const Vec3f &V = Translation;

		xw += xx*V.x + yx*V.y + zx*V.z;
		yw += xy*V.x + yy*V.y + zy*V.z;
		zw += xz*V.x + yz*V.y + zz*V.z;

		return *this;
	}
	Mat4f &Mat4f::ApplyRotation( const Vec3f &AnglesDegrees )
	{
		return
			ApplyZRotation( AnglesDegrees.z ).
			ApplyXRotation( AnglesDegrees.x ).
			ApplyYRotation( AnglesDegrees.y );
	}
	Mat4f &Mat4f::ApplyXRotation( fp_t fXAngleDegrees )
	{
		return detail::ApplyXRotation( *this, fXAngleDegrees );
	}
	Mat4f &Mat4f::ApplyYRotation( fp_t fYAngleDegrees )
	{
		return detail::ApplyYRotation( *this, fYAngleDegrees );
	}
	Mat4f &Mat4f::ApplyZRotation( fp_t fZAngleDegrees )
	{
		return detail::ApplyZRotation( *this, fZAngleDegrees );
	}
	Mat4f &Mat4f::ApplyScaling( const Vec3f &Scaling )
	{
		return detail::ApplyScaling( *this, Scaling );
	}

	void Mat4f::Decompose( Vec3f &dstEulerAngles, Vec3f &dstScale ) const
	{
		detail::Decompose( *this, dstEulerAngles, dstScale );
	}
	void Mat4f::Decompose( Vec3f &dstTranslation, Vec3f &dstEulerAngles, Vec3f &dstScale ) const
	{
		Decompose( dstEulerAngles, dstScale );
		dstTranslation = Vec3f( xw, yw, zw );
	}

	Mat4f &Mat4f::LoadAffineInverse( const Mat4f &M )
	{
		const fp_t x = -( M.xw*M.xx + M.yw*M.yx + M.zw*M.zx );
		const fp_t y = -( M.xw*M.xy + M.yw*M.yy + M.zw*M.zy );
		const fp_t z = -( M.xw*M.xz + M.yw*M.yz + M.zw*M.zz );

		xx = M.xx; yx = M.xy; zx = M.xz; wx = 0;
		xy = M.yx; yy = M.yy; zy = M.yz; wy = 0;
		xz = M.zx; yz = M.zy; zz = M.zz; wz = 0;
		xw =    x; yw =    y; zw =    z; ww = 1;

		return *this;
	}
	Mat4f &Mat4f::LoadAffineMultiply( const Mat4f &A, const Mat4f &B )
	{
		xx = A.xx*B.xx + A.xy*B.yx + A.xz*B.zx;
		xy = A.xx*B.xy + A.xy*B.yy + A.xz*B.zy;
		xz = A.xx*B.xz + A.xy*B.yz + A.xz*B.zz;
		xw = A.xx*B.xw + A.xy*B.yw + A.xz*B.zw + A.xw;

		yx = A.yx*B.xx + A.yy*B.yx + A.yz*B.zx;
		yy = A.yx*B.xy + A.yy*B.yy + A.yz*B.zy;
		yz = A.yx*B.xz + A.yy*B.yz + A.yz*B.zz;
		yw = A.yx*B.xw + A.yy*B.yw + A.yz*B.zw + A.yw;

		zx = A.zx*B.xx + A.zy*B.yx + A.zz*B.zx;
		zy = A.zx*B.xy + A.zy*B.yy + A.zz*B.zy;
		zz = A.zx*B.xz + A.zy*B.yz + A.zz*B.zz;
		zw = A.zx*B.xw + A.zy*B.yw + A.zz*B.zw + A.zw;

		wx = 0;
		wy = 0;
		wz = 0;
		ww = 1;

		return *this;
	}
	Mat4f &Mat4f::LoadMultiply( const Mat4f &A, const Mat4f &B )
	{
		xx = A.xx*B.xx + A.xy*B.yx + A.xz*B.zx + A.xw*B.wx;
		xy = A.xx*B.xy + A.xy*B.yy + A.xz*B.zy + A.xw*B.wy;
		xz = A.xx*B.xz + A.xy*B.yz + A.xz*B.zz + A.xw*B.wz;
		xw = A.xx*B.xw + A.xy*B.yw + A.xz*B.zw + A.xw*B.ww;

		yx = A.yx*B.xx + A.yy*B.yx + A.yz*B.zx + A.yw*B.wx;
		yy = A.yx*B.xy + A.yy*B.yy + A.yz*B.zy + A.yw*B.wy;
		yz = A.yx*B.xz + A.yy*B.yz + A.yz*B.zz + A.yw*B.wz;
		yw = A.yx*B.xw + A.yy*B.yw + A.yz*B.zw + A.yw*B.ww;

		zx = A.zx*B.xx + A.zy*B.yx + A.zz*B.zx + A.zw*B.wx;
		zy = A.zx*B.xy + A.zy*B.yy + A.zz*B.zy + A.zw*B.wy;
		zz = A.zx*B.xz + A.zy*B.yz + A.zz*B.zz + A.zw*B.wz;
		zw = A.zx*B.xw + A.zy*B.yw + A.zz*B.zw + A.zw*B.ww;

		wx = A.wx*B.xx + A.wy*B.yx + A.wz*B.zx + A.ww*B.wx;
		wy = A.wx*B.xy + A.wy*B.yy + A.wz*B.zy + A.ww*B.wy;
		wz = A.wx*B.xz + A.wy*B.yz + A.wz*B.zz + A.ww*B.wz;
		ww = A.wx*B.xw + A.wy*B.yw + A.wz*B.zw + A.ww*B.ww;

		return *this;
	}

	fp_t Mat4f::Determinant() const
	{
		return
			wx*zy*yz*xw - zx*wy*yz*xw - wx*yy*zz*xw + yx*wy*zz*xw +
			zx*yy*wz*xw - yx*zy*wz*xw - wx*zy*xz*yw + zx*wy*xz*yw +
			wx*xy*zz*yw - xx*wy*zz*yw - zx*xy*wz*yw + xx*zy*wz*yw +
			wx*yy*xz*zw - yx*wy*xz*zw - wx*xy*yz*zw + xx*wy*yz*zw +
			yx*xy*wz*zw - xx*yy*wz*zw - zx*yy*xz*ww + yx*zy*xz*ww +
			zx*xy*yz*ww - xx*zy*yz*ww - yx*xy*zz*ww + xx*yy*zz*ww;
	}

}

#ifdef TEST_SUUGAKU

# include <stdio.h>
# include <stdlib.h>

// 試験 (test / examination)
namespace shiken
{
	
	using namespace suugaku;

#define expect(Expr_)\
	do {\
		if( !( Expr_ ) ) {\
			Fail( #Expr_, __FILE__, __LINE__ );\
			return false;\
		}\
	} while(0)

	void Fail( const char *pszExpr, const char *pszFile, unsigned uLine )
	{
		fprintf( stderr, "%s(%u): Error: Expression failed {%s}\n",
			pszFile, uLine, pszExpr );
	}

	const char *VecString( const Vec3f &V )
	{
		static const unsigned kBufSegSize = 64;
		static const unsigned kBufSegCount = 64;
		static const unsigned kBufSize = kBufSegSize*kBufSegCount;
		static char szBuf[ kBufSize ];
		static unsigned uBuf = 0;

		if( uBuf + kBufSegSize >= kBufSize ) {
			uBuf = 0;
		}

		snprintf( &szBuf[ uBuf ], kBufSegSize, "(x:%f,y:%f,z:%f)",
			V.x, V.y, V.z );
		szBuf[ uBuf + kBufSegSize - 1 ] = '\0';

		const char *const pszResult = &szBuf[ uBuf ];
		uBuf += kBufSegSize;

		return pszResult;
	}

	bool FloatEq( fp_t fA, fp_t fB )
	{
		const fp_t x = fB - fA;

		return x >= -SUUGAKU_EPSILON && x <= SUUGAKU_EPSILON;
	}

	bool TestTrig()
	{
		expect( FloatEq( Cos( 0 ), Cos( 360 ) ) );
		expect( FloatEq( Sin( 0 ), Sin( 360 ) ) );

		expect( FloatEq( Cos( 0 ), 1 ) );
		expect( FloatEq( Sin( 0 ), 0 ) );

		expect( FloatEq( Cos( 180 ), Sin( 270 ) ) );

		return true;
	}
	bool TestVectors()
	{
		Vec3f A( 1, 2, 3 );
		Vec3f B( 5, 3, 1 );

		expect( A + B == Vec3f( 6, 5, 4 ) );
		expect( B - A == Vec3f( 4, 1, -2 ) );
		expect( A * B == Vec3f( 5, 6, 3 ) );
		expect( A / B == Vec3f( 0.2, 0.666666666, 3 ) );

		expect( FloatEq( Dot( A, B ), 14 ) );

		expect( LookAt( Vec3f(0,0,0), Vec3f(0,1,0) ) == Vec3f(-90,0,0) );
		expect( LookAt( Vec3f(0,0,0), Vec3f(0,-1,0) ) == Vec3f(90,0,0) );

		return true;
	}

	bool Run()
	{
		if( !TestTrig() ) {
			return false;
		}

		if( !TestVectors() ) {
			return false;
		}

		return true;
	}

}

int main()
{
	return shiken::Run() ? EXIT_SUCCESS : EXIT_FAILURE;
}

#endif
