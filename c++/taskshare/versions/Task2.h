#ifndef TASK_H
#define TASK_H

#include "../TaskShare.h"

//------------------------------------------------------------------------------
// JOB QUEUE

__TS_NS_INIT__

typedef struct token_s {
	union { jobFunc_t fn; void *fnp; };
	void *p;
} token_t;
typedef struct queue_s {
	enum {
#if !TASK_CONF_BUFFER_SIZE
# undef  TASK_CONF_BUFFER_SIZE
# define TASK_CONF_BUFFER_SIZE 4096
#endif
		kBufferSize = TASK_CONF_BUFFER_SIZE
	};

	token_t buffer[kBufferSize];
	u32 numPendingJobs;
	u32 insertionIndex;
	u32 streamingIndex;
} queue_t;

inline void InitQueue(queue_t &q);
inline void FiniQueue(queue_t &q);

inline void EnqueueToken(queue_t &q, const token_t &t);
inline void Schedule(queue_t &q, jobFunc_t fn, void *p);
inline void ScheduleTermination(queue_t &q);

inline void Streamer(queue_t &q);

inline void SyncQueue(queue_t &q);
inline void SyncQueueActive(queue_t &q);

inline void InitQueue(queue_t &q) {
	memset((void *)q.buffer, 0, sizeof(q.buffer));
	q.numPendingJobs = 0;
	q.insertionIndex = 0;
	q.streamingIndex = 0;
}
inline void FiniQueue(queue_t &q) {
	ScheduleTermination(q);
	SyncQueue(q);

	AtomicExchange(&q.numPendingJobs, 0);
	AtomicExchange(&q.insertionIndex, 0);
	AtomicExchange(&q.streamingIndex, 0);
	memset((void *)q.buffer, 0, sizeof(q.buffer));
}

inline void EnqueueToken(queue_t &q, const token_t &t) {
	u32 index;

	if (t.fn==(jobFunc_t)0)
		return;

	AtomicIncrement(&q.numPendingJobs);

	index = AtomicIncrement(&q.insertionIndex)%queue_t::kBufferSize;
	AtomicSpinNotEqual(&q.buffer[index].fnp, (void *)0);

	AtomicExchange(&q.buffer[index].p  , t.p  );
		//atomic operation above completes prior to fnp being set
		//if not, issue a memory barrier (__sync_synchronize)
	AtomicExchange(&q.buffer[index].fnp, t.fnp);
}
inline void Schedule(queue_t &q, jobFunc_t fn, void *p) {
	token_t t;

	t.fn = fn;
	t.p  = p ;

	EnqueueToken(q, t);
}
inline void ScheduleTermination(queue_t &q) {
	token_t t;
	u32 i;

	t.fnp = (void *)1;
	t.p   = (void *)0;

	for(i=0; i<64; i++)
		EnqueueToken(q, t);
}

inline void Streamer(queue_t &q) {
	u32 index;

	do {
		index = AtomicIncrement(&q.streamingIndex)%queue_t::kBufferSize;
		AtomicSpinEqual(&q.buffer[index].fnp, (void *)0);

		switch((size_t)q.buffer[index].fnp) {
		case 1:
			return;

		default:
			q.buffer[index].fn(q.buffer[index].p);
			break;
		}

		//NOTE: no need to set 'p'
		q.buffer[index].fnp = (void *)0;
		AtomicDecrement(&q.numPendingJobs);
	} while(1);
}

inline void SyncQueue(queue_t &q) {
	while(AtomicRead(&q.numPendingJobs)!=0)
		Relinquish();
}
inline void SyncQueueActive(queue_t &q) {
	AtomicSpinNotEqual(&q.numPendingJobs, 0);
}

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
