#include "OpenGLFramebuffer.hpp"
#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <cstdio>
#include <cstdlib>
#ifdef _WIN32
# define WIN32_LEAN_AND_MEAN
# include <Windows.h>
# undef min
# undef max
#endif
#include <ctime>

#define expect(Expr_) \
	do {\
		if(!(Expr_)) {\
			fflush(nullptr);\
			fprintf( stderr, "\nERROR: Expectation failed `%s`\n  file: %s\n  line: %u\n  func: %s\n", \
				#Expr_, __FILE__, __LINE__, __func__);\
			exit(EXIT_FAILURE);\
		}\
	}while(false)

GLFWwindow *g_window = nullptr;

void onResize_f( GLFWwindow *, GLint w, GLint h ) {
	// Don't crash...
	if( h < 1 ) { h = 1; }

	glViewport( 0, 0, w, h );
}

void init() {
	glfwInit();
	atexit( &glfwTerminate );

	glfwWindowHint( GLFW_VISIBLE, GLFW_TRUE );
	glfwWindowHint( GLFW_RESIZABLE, GLFW_TRUE );

	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 2 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 1 );

	g_window = glfwCreateWindow( 640, 480, "OpenGLFramebuffer test", nullptr, nullptr );
	expect( g_window != nullptr );

	glfwSetFramebufferSizeCallback( g_window, &onResize_f );

	glfwMakeContextCurrent( g_window );

	printf( "%s\n", (const char*)glGetString( GL_VERSION ) );
	printf( "%s\n", (const char*)glGetString( GL_VENDOR ) );
	printf( "%s\n", (const char*)glGetString( GL_RENDERER ) );

	glViewport( 0, 0, 640, 480 );

	const GLenum glewInitResult = glewInit();
	expect( glewInitResult == GLEW_OK );
	expect( !!GLEW_VERSION_2_1 );

	expect( glewIsSupported( "GL_ARB_framebuffer_object" ) );

	srand( time(nullptr) );
}
bool step() {
	glfwSwapBuffers( g_window );
#ifdef _WIN32
	Sleep( 4 ); // back off CPU usage for a bit
#endif

	glfwPollEvents();

	return !glfwWindowShouldClose( g_window );
}

// -------------------------------------------------------------------------- //

float randomUNorm() {
	static const unsigned m = 1<<10;
	const unsigned n = rand() % m;
	return float( n )/float( m - 1 );
}

void clearFramebuffer( const OpenGLFramebuffer &fb ) {
	OpenGLFramebuffer::bind( fb );

	glClearColor( randomUNorm(), randomUNorm(), randomUNorm(), randomUNorm() );
	glClearDepth( 1.0f );
	glClearStencil( 0xFF );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	OpenGLFramebuffer::unbind();
}

int main() {
	init();

	OpenGLFramebuffer fb;

	fb.init();
	clearFramebuffer( fb );

	do {
		// Just in case the window resized
		if( fb.resize() ) {
			clearFramebuffer( fb );
		}

		OpenGLFramebuffer::bind( fb );

		glDisable( GL_TEXTURE_2D );

		glBegin( GL_TRIANGLES );
			glColor3f( 1.0f, 0.0f, 0.0f );
			glVertex2f( -0.5f, -0.5f );

			glColor3f( 0.0f, 1.0f, 0.0f );
			glVertex2f( 0.0f, 0.5f );

			glColor3f( 0.0f, 0.0f, 1.0f );
			glVertex2f( 0.5f, -0.5f );
		glEnd();

		OpenGLFramebuffer::unbind();

		// Clear the normal framebuffer
		glClearColor( 0.1f, 0.3f, 0.5f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT );

		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, fb.getResources().colorImages[ 0 ] );

		glBegin( GL_QUADS );
			glColor3f( 1.0f, 1.0f, 1.0f );

			glTexCoord2f( 0.0f, 0.0f );
			glVertex2f( -0.5f, -0.5f );

			glTexCoord2f( 0.0f, 1.0f );
			glVertex2f( -0.5f, 0.5f );

			glTexCoord2f( 1.0f, 1.0f );
			glVertex2f( 0.5f, 0.5f );

			glTexCoord2f( 1.0f, 0.0f );
			glVertex2f( 0.5f, -0.5f );
		glEnd();
	} while( step() );

	return EXIT_SUCCESS;
}
