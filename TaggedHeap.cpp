﻿/*

	Tagged Heap Allocator
	=====================

	Inspired by Naughty Dog's presentation "Parallelizing the Naughty Dog
	Engine Using Fibers," which in part described their high-performance high-
	volume multi-threading and memory efficient allocator.

	You have a memory pool that hosts a collection of fixed-size blocks. Blocks
	can be assigned tags. There is no "free(ptr)" interface. In its place is a
	"free(tag)" interface, which will bulk free all blocks associated with that
	tag. (e.g., "Game," or "Render," or "GPU," etc.)

	Sub-allocations will be made within a given block as the block size is quite
	large (2MB). The allocations will all be linear.

	To speed up allocations, and also make them thread-safe, threads will grab
	private blocks they will make allocations from. There will be at most one
	block per tag in the thread's local pool. This means only grabbing new
	blocks from the global pool is necessary.

	If an allocation is larger than one block then multiple contiguous blocks
	will need to be pulled from the global pool. This operation is somewhat more
	expensive, but is expected to be relatively rare for the use case.

	！！！ Only use this allocator if you require a fixed-size memory pool,
	`      and have explicit control over synchronization of allocations and
	`      object life-time. This is usually only the case for well-developed,
	`      mission-critical, and memory-hungry games.

	＊ Tags should be thought of as life-times rather than systems. From the
	`  Naughty Dog presentation, the following are listed:

		ⅰ Single Game Logic Stage (scratch memory)
		ⅱ Double Game Logic Stage (low-priority ray casts)
		ⅲ Game To Render Logic Stage (object instance arrays)
		ⅳ Game To GPU Stage (skinning matrices)
		ⅴ Render To GPU Stage (command buffers)

		(Those are doubled as the tags apply to both CPU and GPU memory.)




	Implementation Details
	----------------------

	A pool structure holds a pointer to the memory blocks, which can either be
	heap allocated or statically allocated. (We don't care. We only expect the
	memory to be valid for the duration we use it.)

	The pool structure additionally holds an array of indexes representing the
	used blocks and the allocated blocks. Another integer represents where in
	this array the free blocks exist at-and-after, implying where the used
	blocks exist. This integer is called the "current position."

	Indexes are encoded as 12-bits representing each 2MB block (giving 8GB of
	addressable memory per allocator), and 4-bits representing the tag (allowing
	16 unique tags, per allocator).

	The following is a basic set of operations for working with the data
	structure. Be aware that these aren't optimized and that the source-level
	implementation may differ. However the process is mostly the same.

	To allocate a block:
	
		① Atomic-fetch-then-add on the "current position" for the array.
		② If the current position has the high-bit set, then a lock is held and
		`  we must wait for it to be released. Spin-loop until the high-bit is
		`  not set, then try again from ①.
		③ If the current position is out of range, then handle "out of memory"
		`  in whichever way is appropriate.
		④ The value we retrieved from the atomic-fetch-and-add gives us the
		`  block index we can use. Change its tag to whatever tag we've
		`  allocated. Done.

	To deallocate a *tag*:

		① Acquire a lock. (Described in a below section.)
		② For each used block, remove it from that section of the array by
		`  swapping it with the last element in the section. Decrement the
		`  current position of the array. (This simultaneously marks the value
		`  as free.)
		③ Optionally sort the array to improve performance for allocating
		`  multiple blocks at once.
		④ Release the lock. (Described in a below section.)

		＊ Also note that thread-local caches will need to be wiped, or the
		`  thread needs to become aware that the blocks it had cached are no
		`  longer valid.
	
	To allocate multiple blocks:

		① Acquire a lock. (Described in a below section.)
		② Search for enough contiguous blocks, and remove them from that
		`  section of the array. (Again, order does not matter, but to speed up
		`  the algorithm it is preferred that these blocks are kept in order.)
		③ Adjust the current position (the allocated blocks should be in the
		`  "used" section rather than the "free" section now). 
		④ Release the lock. (Described in a below section.)

	To acquire/release a lock:

		① Read the current position. If it is currently locked (high-bit set),
		`  then spin until it is not.
		② Attempt to atomic-compare-and-set the value to the read current
		`  position with the high-bit newly set. If this fails, go back to ①.
		③ Do whatever you need to with the lock. Do not attempt to grab the
		`  lock again from the same thread -- it is not re-entrant.
		④ Release the lock with an atomic-set to the new value of "current
		`  position." (The high-bit must not be set; it must be cleared.)

	The thread has a local set of sixteen block pointers. The pointers
	correspond to the tags for the blocks. (The pointers can be indexes instead
	as long as the thread has access to the base address to resolve the
	appropriate pointer for sub-allocations.) The pointers should be null (or in
	the case of indexes, be set to a specific "invalid value") when a new block
	would be needed for the tag, and by default.
	
	When a thread has acquired a block it will make linear allocations from it.
	Linear allocations will always be a multiple of 16 bytes, whether requested
	or not. This ensures that as long as the base pointer is aligned that all
	subsequent allocations will also be aligned (to 16 bytes). The value
	tracking the current position of the linear allocation should exist within
	the last 16 bytes of the block. This is to allow it to be overridden once
	the block becomes full (allowing more memory to be squeezed out of
	individual blocks -- this is opposed to "at the start of the block" or "in
	the thread-local data"). The allocator will know it needs a new block after
	that, so it will set its internal pointer of that block to null. (Which
	signals it will need another block.)

	If an allocation is requested that would not fit within the current block
	then one of the blocks must be "wasted." (It will be unavailable for further
	sub-allocations.) The block that is wasted should be the block that has less
	available space. (For a multiple block allocation, check the last block
	rather than the first.) The block with more available space should be used
	as the thread-local block. If the available space is equal between the
	blocks, then do not change the thread-local block.




	Optimization Avenues and Other Alterations
	------------------------------------------

	【Dynamic Memory】
	It is not necessary to use a fixed-pool. For example, you can reserve a set
	of pages from the virtual address space (esp. if targeting 64-bit) without
	actually allocating the memory. When a block is "allocated" commit that
	block of memory then. Doing so is mostly useful for editors/tools or for
	development builds when you're testing to see what your memory budget needs
	to be. Note, however, that each block will need to be page-sized (or a
	multiple thereof) for this to make sense.




*/

#include <stdio.h>
#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#undef AX_KILOBYTES
#define AX_KILOBYTES(N_)			( (N_)*1024 )

#undef AX_MEGABYTES
#define AX_MEGABYTES(N_)			( (N_)*1024*1024 )

#undef AX_GIGABYTES
#define AX_GIGABYTES(N_)			( (N_)*1024*1024*1024 )

#ifndef AX_TAGGED_HEAP_CHUNK_SIZE
# define AX_TAGGED_HEAP_CHUNK_SIZE	AX_MEGABYTES(2)
#endif

#ifndef AX_TAGGED_HEAP_MAX_TAGS
# define AX_TAGGED_HEAP_MAX_TAGS	16
#endif

namespace Ax
{

	template< size_t n, size_t d >
	struct TIntDiv
	{
		static const size_t numerator = n;
		static const size_t denominator = d;
		static const size_t value = n/d + +( n%d != 0 );
	};

	namespace Detail
	{
		
		struct SMemChunk
		{
			union {
				unsigned char		Buf[ AX_TAGGED_HEAP_CHUNK_SIZE ];
				SMemChunk *			pNext;
			};
		};

		struct STagHeader
		{
			static const size_t		kPartSize = 16;
			unsigned				cParts;
			STagHeader *			pNext;

			inline size_t Bytes() const
			{
				return size_t( cParts )*kPartSize;
			}
		};

	}

	class CTaggedHeap
	{
	public:
		static const size_t kChunkSize = AX_TAGGED_HEAP_CHUNK_SIZE;
		static const size_t kMaxTags = AX_TAGGED_HEAP_MAX_TAGS;

		CTaggedHeap()
		: m_pFree( ( SMemChunk * )0 )
		{
			memset( &m_Chunks[0], 0, sizeof( m_Chunks ) );
		}
		~CTaggedHeap()
		{
		}

		void 

	private:
		Detail::SMemChunk *			m_pFree;

		Detail::SMemChunk *			m_pChunks;
		size_t						m_cChunks;

		size_t						m_cUsed[ kMaxTags ];
		Detail::SMemChunk *			m_pUsed[ kMaxTags ];
	};

}
