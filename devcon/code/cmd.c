#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cmd.h"
#include "cvar.h"
#include "memory.h"
#include "string.h"
#include "system.h"

/* command buffer */
cmdBuf_t g_cmdBuf = { 0, 0, (char **)0 };

/*
 * =============================================================================
 *
 *	COMMAND BUFFER FUNCTIONS
 *
 * =============================================================================
 */
size_t cmd_Capacity() {
	return g_cmdBuf.capacity;
}
size_t cmd_Length() {
	return g_cmdBuf.length;
}
const char *cmd_Get(size_t i) {
	if (i > g_cmdBuf.length)
		return (const char *)0;

	return g_cmdBuf.commands[i];
}

#define CMDBUF_CHUNK_SIZE 32
void cmd_Add(const char *command) {
	if (g_cmdBuf.length == g_cmdBuf.capacity) {
		g_cmdBuf.capacity += CMDBUF_CHUNK_SIZE;
		g_cmdBuf.commands = (char **)mem_Realloc((void *)g_cmdBuf.commands,
			sizeof(char *)*g_cmdBuf.capacity);
	}

	g_cmdBuf.commands[g_cmdBuf.length++] = str_Duplicate(command);
}

#if 0
static int GetDataType(const char *arg) {
	if (*arg>='0' && *arg<='9') {
		if (strchr(arg, '.'))
			return kCVar_Float;

		if (arg[1]!='x' && strchr(arg, 'f'))
			return kCVar_Float;

		return kCVar_Integer;
	}

	if (!strcmp(arg, "true") || !strcmp(arg, "false")
	|| !strcmp(arg, "yes") || !strcmp(arg, "no")
	|| !strcmp(arg, "on") || !strcmp(arg, "off"))
		return kCVar_Boolean;

	return kCVar_String;
}
static void *GetData(int dataType, const char *arg) {
	union { void *p; int i; float f; } v;

	v.p = (void *)0;

	switch(dataType) {
	case kCVar_String:
		v.p = (void *)arg;
		break;
	case kCVar_Integer:
		sscanf(arg, "%i", &v.i);
		break;
	case kCVar_Float:
		sscanf(arg, "%f", &v.f);
		break;
	case kCVar_Boolean:
		if(!strcmp(arg, "true")
		|| !strcmp(arg, "yes")
		|| !strcmp(arg, "on")
		|| !strcmp(arg, "1")) {
			v.i = 1;
			break;
		}

		if(!strcmp(arg, "false")
		|| !strcmp(arg, "no")
		|| !strcmp(arg, "off")
		|| !strcmp(arg, "0")) {
			v.i = 0;
			break;
		}

		sys_Warning("Unknown boolean value \"%s\"; assuming 0", arg);
		v.i = 0;
		break;
	default:
		sys_Warning("Unknown data type (%i)\n", dataType);
		break;
	}

	return v.p;
}
#endif

static int Execute(const char *command) {
	/*
	 * TODO: This WHOLE function and set up should *REALLY* be improved.
	 */
	union { void *p; int i; float f; } v;
	const char *p;
	cvar_t *cvar;
	size_t i, j;
	char args[3][256];
	int isString[sizeof(args)/sizeof(args[0])];
	/*int dataType;*/

	/*
	 * TODO: In place modification of commands instead of string copy?
	 *       Use buffers only for strings (due to escape sequences). Place
	 *       NULL characters at the first whitespace token.
	 *       ~aaron-20120330-1405
	 */

	for(i=0; i<sizeof(args)/sizeof(args[0]); i++) {
		args[i][0] = 0;
		isString[i] = 0;
	}

	for(i=0; i<sizeof(args)/sizeof(args[0]); i++) {
		while(*command <= ' ' && *command)
			command++;

		if (*command=='\0')
			break;

		p = command;
		if(*p=='\"') {
			p++;

			isString[i] = 1;

			j = 0;
			while(*p!='\"') {
				if (j==sizeof(args[i])) {
					sys_Error("String is too long");
					return 0;
				}

				if(*p=='\0') {
					sys_Error("Malformed string");
					return 0;
				}

				if(*p=='\\') {
					switch(*++p) {
					case '\'': args[i][j++] = '\''; break;
					case '\"': args[i][j++] = '\"'; break;
					case '\?': args[i][j++] = '\?'; break;
					case '\\': args[i][j++] = '\\'; break;
					case 'a' : args[i][j++] = '\a'; break;
					case 'b' : args[i][j++] = '\b'; break;
					case 'f' : args[i][j++] = '\f'; break;
					case 'n' : args[i][j++] = '\n'; break;
					case 'r' : args[i][j++] = '\r'; break;
					case 't' : args[i][j++] = '\t'; break;
					case 'v' : args[i][j++] = '\v'; break;
					case 'x' : sys_Warning("'\\x' not supported"); break;
					case '\0':
						sys_Error("Malformed string (escape sequence)");
						return 0;
					default:
						sys_Warning("'\\%c' is not recognized", *p);
						break;
					}

					p++;
					continue;
				} /*escape sequence*/

				args[i][j++] = *p++;
			} /*string loop*/

			args[i][j] = 0;
			command = ++p;
			continue;
		}

		while(*p > ' ')
			p++;

		if ((size_t)(p - command) >= sizeof(args[i]) - 1) {
			sys_Error("Command is too long.");
			return 0;
		}

		strncpy(args[i], command, p - command);
		args[i][p - command] = 0;

		command = p;
	}

	while(*command <= ' ' && *command)
		command++;

	if (*command)
		sys_Warning("This part of the command is ignored: %s\n", command);

	/*sys_Log("Command: \"%s\" \"%s\" \"%s\"\n", args[0], args[1], args[2]);*/

	if (!i) /*no arguments is a valid 'skip me'*/
		return 1;

	if (!strcmp(args[0], "set")) {
		if (i < 3) {
			sys_Error("'set' takes two arguments");
			return 0;
		}

		/*dataType = isString[2] ? kCVar_String : GetDataType(args[2]);*/

		/*
		 * TODO: Improve this to use a proper data type
		 */
		cvar = cvar_New(args[1], kCVar_String|kCVar_NoModify_Bit, args[2]);
		if (!cvar)
			return 0;
	} else {
		if (i >= 3) {
			sys_Warning("Too many arguments; extra arguments ignore");
			i = 2;
		}

		cvar = cvar_Find(args[0]);
		if (!cvar) {
			sys_Error("Did not find cvar \"%s\"", args[0]);
			return 0;
		}

		if (i == 2) {
#if 0
			dataType = isString[1] ? kCVar_String : GetDataType(args[1]);
			if (dataType != cvar_Type(cvar)) {
				sys_Error("Data types do not match");
				return 0;
			}

			v.p = GetData(cvar_Type(cvar), args[1]);

			switch(cvar_Type(cvar)) {
			case kCVar_String:
				cvar_SetString(cvar, args[1]);
				break;
			case kCVar_Integer:
				cvar_SetInteger(cvar, v.i);
				break;
			case kCVar_Boolean:
				cvar_SetBoolean(cvar, v.i);
				break;
			case kCVar_Float:
				cvar_SetFloat(cvar, v.f);
				break;
			default:
				sys_Error("[%s(%u)] Data type not handled!", __FILE__,
					__LINE__);
				return 0;
			}
#endif
			if (cvar->flags & kCVar_Const_Bit)
				sys_Error("\"%s\" is constant!", cvar_Name(cvar));
			else {
				cvar->value.s = str_Copy(cvar->value.s, args[1]);
				cvar->flags |= kCVar_Modified_Bit;
			}
		} else {
			switch(cvar_Type(cvar)) {
			case kCVar_String:
				sys_Log("%s: \"%s\"\n", args[0], cvar_String(cvar));
				break;
			case kCVar_Integer:
				sys_Log("%s: %i\n", args[0], cvar_Integer(cvar));
				break;
			case kCVar_Boolean:
				sys_Log("%s: %s\n", args[0], cvar_Boolean(cvar) ? "true" :
					"false");
				break;
			case kCVar_Float:
				sys_Log("%s: %.4f\n", args[0], cvar_Float(cvar));
				break;
			default:
				sys_Error("[%s(%u)] Data type not handled!", __FILE__,
					__LINE__);
				return 0;
			}
		}
	}

	return 1;
}
int cmd_Execute() {
	size_t i;
	int r;

	r = 1;

	for(i=0; i<g_cmdBuf.length; i++) {
		r &= Execute(g_cmdBuf.commands[i]);
		g_cmdBuf.commands[i] = mem_Free((void *)g_cmdBuf.commands[i]);
	}

	g_cmdBuf.length = 0;
	return r;
}
