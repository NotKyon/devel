#include <stdlib.h>
#include <ncurses.h>

int g_scrw=0, g_scrh=0;

void init() {
	/* initialize ncurses */
	initscr();

	/* disable character echoing (i.e., pressing a key doesn't get that key
	   displayed automatically) */
	noecho();

	/* ask for input from arrow keys, F1, F2, etc. */
	keypad(stdscr, 1);

	/* retrieve the size of the screen */
	getmaxyx(stdscr, g_scrh, g_scrw);
}
void deinit() {
	/* deinitialize ncurses */
	endwin();
}

void locate(int x, int y) {
	if (x < 0)
		x = 0;
	if (y < 0)
		y = 0;

	if (x >= g_scrw)
		x = g_scrw - 1;
	if (y >= g_scrh)
		y = g_scrh - 1;

	move(y, x);
}

int main() {
	int c;

	/* initialize the editor */
	init();
	atexit(deinit);

	/* display some text to the terminal */
	printw("Hello, ncurses world!\n");
	printw("Press 'F1' for a notification; or view a character in BOLD.\n");
	c = getch();
	if (c==KEY_F(1)) {
		printw("You pressed the F1 key!\n");
	} else {
		attron(A_BOLD); /*enable bold text*/
		printw("%c\n", c);
		attroff(A_BOLD); /*disable bold text*/
	}
	printw("\nAlso, the screen's resolution is (%i,%i)\n", g_scrw, g_scrh);
	refresh(); /*refresh the screen*/

	/* wait for a key to be pressed */
	getch();

	/* successful run */
	return 0;
}
