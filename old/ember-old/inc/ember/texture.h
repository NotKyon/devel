#ifndef EMBER_TEXTURE_H
#define EMBER_TEXTURE_H

#include "config.h"
#include "opengl.h"
#include "gc.h"

#ifndef __texture_t_defined__
#define __texture_t_defined__ 1
typedef struct stTexture *texture_t;
#endif

struct stTexture {
	char *name;

	GLuint tex;
	GLenum format, internal_format;
	unsigned int width, height, depth;

	struct stTexture *prev, *next;
};
__em_extrn gc_pool_t g_tex_pool __em_nil();
__em_extrn struct stTexture *g_tex_head __em_nil(), *g_tex_tail __em_nil();

extern int tex_init();
extern void tex_deinit();
extern texture_t tex_alloc( const char *name );
extern void tex_dealloc( texture_t tex );
extern void tex_set_name( texture_t tex, const char *name );
extern void tex_set_format( texture_t tex, GLenum format );
extern void tex_set_internal_format( texture_t tex, GLenum internal_format );
extern void tex_set_size( texture_t tex, unsigned int w, unsigned int h, unsigned int d );
extern const char *tex_name( texture_t tex );
extern GLuint tex_gl_id( texture_t tex );
extern GLenum tex_format( texture_t tex );
extern GLenum tex_internal_format( texture_t tex );
extern unsigned int tex_width( texture_t tex );
extern unsigned int tex_height( texture_t tex );
extern unsigned int tex_depth( texture_t tex );
extern texture_t tex_before( texture_t tex );
extern texture_t tex_after( texture_t tex );
extern texture_t tex_first();
extern texture_t tex_last();

#endif
