#if 0
# define AXLOG_PRINTOUTF      printf
# define AXLOG_PRINTERRF(...) fprintf(stderr, __VA_ARGS__)
# define AXLOG_SNPRINTF       snprintf
# define AXLOG_SNPRINTFV      vsnprintf
#endif

#define AXLOG_VS_STYLE 1

#define AXPRINTF_IMPLEMENTATION
#define AXSTR_IMPLEMENTATION
#define AXLOG_IMPLEMENTATION
#include "axlib/ax_printf.h"
#include "axlib/ax_string.h"
#include "axlib/ax_logger.h"

#include <stdio.h>
#include <stdlib.h>

enum
{
	kLF_MyProg,
	kLF_Render,
	kLF_Sound
};

int main()
{
	axlog_infof( nullptr, 0, nullptr, "infoLog0" );

	axlog_set_facility( kLF_MyProg, "myprog" );
	axlog_set_facility( kLF_Render, "render" );
	axlog_set_facility( kLF_Sound, "sound" );

	AX_DEBUG_LOG += "Hello, world!";
	Ax::debugf( "A", 1, "Hello..." );
	
	AX_TRACE( "enter" );

	Ax::infoLog += "infoLog1";
	Ax::infoLog[kLF_MyProg] += "infoLog2";
	Ax::debugLog += "debugLog1";
	Ax::debugLog << "debugLog2";

	Ax::infoLog[kLF_MyProg|axlogc_extfile]("somefile.txt",95) += "Something on a specific line of this file...";

	Ax::noteLog[kLF_Render] += "We don't actually have a renderer for this test. :(";

	Ax::warningLog[kLF_Sound|axlogc_devel]("bgm00.ogg") += "Someday we'll have tunes.";
	
	int x = 5;
	if( !AXLOG_CHECK( x == 7 ) ) {
		Ax::infoLog << "lalala";
	}
	
	AX_TRACE( "leave" );

	return EXIT_SUCCESS;
}
