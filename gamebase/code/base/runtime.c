#include <gamebase.h>

#include <stdio.h>
#include <stdlib.h>

#define CHUNK_SIZE 0x1000
#define NUM_CHUNKS 0x10

const char *gbFV(const char *format, va_list args) {
	static char buf[NUM_CHUNKS][CHUNK_SIZE];
	static size_t i = 0;
	size_t r;

#if __STDC_WANT_SECURE_LIB__
	vsprintf_s(buf[i], sizeof(buf[i]), format, args);
#else
	vsnprintf(buf[i], sizeof(buf[i]) - 1, format, args);
	buf[i][sizeof(buf[i]) - 1] = '\0';
#endif

	r = i;
	i = (i + 1) % NUM_CHUNKS;

	return buf[r];
}
const char *gbF(const char *format, ...) {
	const char *p;
	va_list args;

	va_start(args, format);
	p = gbFV(format, args);
	va_end(args);

	return p;
}

void gbRuntimeError(const char *message) {
	gb_exception_t except;

	except.kind = kGBExcept_RuntimeError;
	except.message = message;
	except.data.p = (void *)NULL;

	gbRaiseException(&except);
}

static size_t g_endPtrMax=0, g_endPtrNum=0;
static gb_fnPtr_t *g_endPtrs = (gb_fnPtr_t *)NULL;
static gb_bool_t g_endInit = GB_FALSE;
static void OnEnd() {
	size_t i;

	for(i=g_endPtrNum; i>0; i--)
		g_endPtrs[i - 1]();

	g_endPtrMax = 0;
	g_endPtrNum = 0;
	g_endPtrs = (gb_fnPtr_t *)gbMemFree((void *)g_endPtrs);
}
void gbOnEnd(gb_fnPtr_t fnOnEnd) {
	gbAssert(fnOnEnd != (gb_fnPtr_t)NULL);

	if (!g_endInit) {
		g_endInit = GB_TRUE;
		atexit(OnEnd);
	}

	if (g_endPtrNum + 1 > g_endPtrMax) {
		g_endPtrMax += 8;
		g_endPtrs = (gb_fnPtr_t *)gbMemExtend((void *)g_endPtrs,
			g_endPtrMax*sizeof(gb_fnPtr_t));
	}

	g_endPtrs[g_endPtrNum++] = fnOnEnd;
}

static int g_exitFlag = EXIT_SUCCESS;
void gbSetExitFlag(int flag) {
	g_exitFlag = flag;
}
int gbGetExitFlag() {
	return g_exitFlag;
}
void gbEnd() {
	exit(g_exitFlag);
}
