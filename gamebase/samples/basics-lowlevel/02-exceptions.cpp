//
//	TODO: This system needs to be explained in detail
//

#define GB_INIT 1
#include <gamebase.h>

static gb_exceptResult_t MyExceptionHandler(const gb_exception_t *except) {
	// display information about the exception
	gbWriteStderr(gbF("Caught exception: %i; %s; %p\n",
		except->kind, except->message, except->data));

	// break in debug mode
	gbDebugStop();

	// we have handled the exception
	return kGBExceptRes_Break;
}

void GBMain() {
	// select the exception handler
	gbSetExceptionHandler(MyExceptionHandler);

	// raise an error
	gbRuntimeError("Testing the exception system...");
}
