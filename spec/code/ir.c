#include "ir.h"

#include "report.h"
#include "memory.h"

/* -------------------------------------------------------------------------- */

static int g_ir_error = kIR_Error_None;

int GetIRError() {
	int e;

	e = g_ir_error;
	g_ir_error = kIR_Error_None;

	return e;
}
const char *GetIRErrorText(int error) {
	switch(error) {
	case kIR_Error_None:			return "No Error";
	case kIR_Error_StackOverflow:	return "Stack Overflow";
	case kIR_Error_StackUnderflow:	return "Stack Underflow";
	default:						break;
	}

	return (const char *)NULL;
}

static void SetIRError(int error) {
	if (g_ir_error != kIR_Error_None)
		return;

	g_ir_error = error;
}

/* -------------------------------------------------------------------------- */

static lib_t *g_lib_head = (lib_t *)NULL;
static lib_t *g_lib_tail = (lib_t *)NULL;
static lib_t *g_lib_actv = (lib_t *)NULL;

lib_t *NewLib() {
	lib_t *lib;

	lib = (lib_t *)Memory((void *)0, sizeof(*lib));

	lib->ns = (ns_t *)NULL;

	lib->next = (lib_t *)NULL;
	if ((lib->prev = g_lib_tail) != (lib_t *)NULL)
		g_lib_tail->next = lib;
	else
		g_lib_head = lib;
	g_lib_tail = lib;

	if (!g_lib_actv)
		g_lib_actv = lib;

	return lib;
}
lib_t *DeleteLib(lib_t *lib) {
	if (!lib)
		return (lib_t *)NULL;

	if (lib->prev)
		lib->prev->next = lib->next;
	if (lib->next)
		lib->next->prev = lib->prev;

	if (g_lib_head==lib)
		g_lib_head = lib->next;
	if (g_lib_tail==lib)
		g_lib_tail = lib->prev;

	if (g_lib_actv==lib)
		g_lib_actv = lib->next ? lib->next : lib->prev;

	lib->prev = (lib_t *)NULL;
	lib->next = (lib_t *)NULL;

	lib->ns = DeleteNamespace(lib->ns);

	return (lib_t *)Memory((void *)lib, 0);
}

void SetLibNamespace(lib_t *lib, ns_t *ns) {
	ASSERT(lib != (lib_t *)NULL);
	ASSERT(!lib->ns);

	lib->ns = ns;
}
ns_t *GetLibNamespace(lib_t *lib) {
	ASSERT(lib != (lib_t *)NULL);

	return lib->ns;
}

lib_t *LibBefore(lib_t *lib) {
	ASSERT(lib != (lib_t *)NULL);

	return lib->prev;
}
lib_t *LibAfter(lib_t *lib) {
	ASSERT(lib != (lib_t *)NULL);

	return lib->next;
}
lib_t *FirstLib() {
	return g_lib_head;
}
lib_t *LastLib() {
	return g_lib_tail;
}

void SetActiveLib(lib_t *lib) {
	g_lib_actv = lib;
}
lib_t *GetActiveLib() {
	return g_lib_actv;
}

/* -------------------------------------------------------------------------- */

#define NS_ACTV_MAX_STACK 128
static ns_t *g_ns_actv_stack[NS_ACTV_MAX_STACK] = { (ns_t *)NULL, };
static int g_ns_actv_sp = 0;

static void PushFirstActiveNamespace(ns_t *ns) {
	if (g_ns_actv_sp > 0)
		return;

	if (g_ns_actv_stack[0] != (ns_t *)NULL)
		return;

	g_ns_actv_sp = 0;
	g_ns_actv_stack[0] = ns;
}

ns_t *NewNamespace(ns_t *prnt) {
	ns_t *ns;

	ns = (ns_t *)Memory((void *)NULL, sizeof(*ns));

	ns->name = (char *)NULL;
	ns->abrv = (char *)NULL;

	ns->type_head = (type_t *)NULL;
	ns->type_tail = (type_t *)NULL;
	ns->enum_head = (enum_t *)NULL;
	ns->enum_tail = (enum_t *)NULL;
	ns->iface_head = (iface_t *)NULL;
	ns->iface_tail = (iface_t *)NULL;

	if (prnt)
		ns->lib = prnt->lib;
	else
		ns->lib = g_lib_actv;
	ASSERT(ns->lib != (lib_t *)NULL);

	ns->prnt = prnt;
	if (ns->lib->ns == (ns_t *)NULL)
		SetLibNamespace(ns->lib, ns);

	ns->head = (ns_t *)NULL;
	ns->tail = (ns_t *)NULL;

	ns->prev = (ns_t *)NULL;
	ns->next = (ns_t *)NULL;

	if (ns->prnt) {
		if ((ns->prev = ns->prnt->tail) != (ns_t *)NULL)
			ns->prnt->tail->next = ns;
		else
			ns->prnt->head = ns;
		ns->prnt->tail = ns;
	}

	PushFirstActiveNamespace(ns);

	return ns;
}
ns_t *DeleteNamespace(ns_t *ns) {
	if (!ns)
		return (ns_t *)NULL;

	ns->name = (char *)Memory((void *)ns->name, 0);
	ns->abrv = (char *)Memory((void *)ns->abrv, 0);

	while(ns->head)
		DeleteNamespace(ns->head);

	if (ns->prev)
		ns->prev->next = ns->next;
	if (ns->next)
		ns->next->prev = ns->prev;

	if (ns->prnt) {
		if (ns->prnt->head==ns)
			ns->prnt->head = ns->next;
		if (ns->prnt->tail==ns)
			ns->prnt->tail = ns->prev;
	}

	ns->prev = (ns_t *)NULL;
	ns->next = (ns_t *)NULL;

	if (ns->lib->ns==ns)
		ns->lib->ns = (ns_t *)NULL;
	ns->lib = (lib_t *)NULL;

	while(ns->type_head)
		DeleteTypedef(ns->type_head);
	while(ns->enum_head)
		DeleteEnum(ns->enum_head);
	while(ns->func_head)
		DeleteFunc(ns->func_head);
	while(ns->iface_head)
		DeleteInterface(ns->iface_head);

	return (ns_t *)Memory((void *)ns, 0);
}

void SetNamespaceName(ns_t *ns, const char *name) {
	ASSERT(ns != (ns_t *)NULL);

	ns->name = Copy(ns->name, name);
}
void SetNamespaceAbrv(ns_t *ns, const char *abrv) {
	ASSERT(ns != (ns_t *)NULL);

	ns->abrv = Copy(ns->abrv, abrv);
}
const char *GetNamespaceName(ns_t *ns) {
	ASSERT(ns != (ns_t *)NULL);

	return ns->name;
}
const char *GetNamespaceAbrv(ns_t *ns) {
	ASSERT(ns != (ns_t *)NULL);

	return ns->abrv;
}

lib_t *GetNamespaceLib(ns_t *ns) {
	ASSERT(ns != (ns_t *)NULL);

	return ns->lib;
}

ns_t *GetNamespaceParent(ns_t *ns) {
	ASSERT(ns != (ns_t *)NULL);

	return ns->prnt;
}

ns_t *FirstNamespace(ns_t *ns) {
	ASSERT(GetActiveLib() != (lib_t *)NULL);

	return ns ? ns->head : GetActiveLib()->ns;
}
ns_t *LastNamespace(ns_t *ns) {
	ASSERT(GetActiveLib() != (lib_t *)NULL);

	return ns ? ns->tail : GetActiveLib()->ns;
}
ns_t *NamespaceBefore(ns_t *ns) {
	ASSERT(ns != (ns_t *)NULL);

	return ns->prev;
}
ns_t *NamespaceAfter(ns_t *ns) {
	ASSERT(ns != (ns_t *)NULL);

	return ns->next;
}

void PushActiveNamespace() {
	size_t sp;

	sp = g_ns_actv_sp + 1;
	if (sp == NS_ACTV_MAX_STACK) {
		SetIRError(kIR_Error_StackOverflow);
		return;
	}

	g_ns_actv_stack[sp] = GetActiveNamespace();
	g_ns_actv_sp = sp;
}
void PopActiveNamespace() {
	if (g_ns_actv_sp == 0) {
		SetIRError(kIR_Error_StackUnderflow);
		return;
	}

	g_ns_actv_sp--;
}
void SetActiveNamespace(ns_t *ns) {
	g_ns_actv_stack[g_ns_actv_sp] = ns;
}
ns_t *GetActiveNamespace() {
	return g_ns_actv_stack[g_ns_actv_sp];
}

/* -------------------------------------------------------------------------- */

type_t *NewTypedef() {
	type_t *type;

	ASSERT(GetActiveNamespace() != (ns_t *)NULL);

	type = (type_t *)Memory((void *)0, sizeof(*type));

	type->name = (char *)NULL;
	type->from = (char *)NULL;

	type->prnt = GetActiveNamespace();
	type->next = (type_t *)NULL;
	if ((type->prev = type->prnt->type_tail) != (type_t *)NULL)
		type->prnt->type_tail->next = type;
	else
		type->prnt->type_head = type;
	type->prnt->type_tail = type;

	return type;
}
type_t *DeleteTypedef(type_t *type) {
	if (!type)
		return (type_t *)NULL;

	type->name = (char *)Memory((void *)type->name, 0);
	type->from = (char *)Memory((void *)type->from, 0);

	if (type->prev)
		type->prev->next = type->next;
	if (type->next)
		type->next->prev = type->prev;

	if (type->prnt->type_head==type)
		type->prnt->type_head = type->next;
	if (type->prnt->type_tail==type)
		type->prnt->type_tail = type->prev;

	type->prnt = (ns_t *)NULL;
	type->prev = (type_t *)NULL;
	type->next = (type_t *)NULL;

	return (type_t *)Memory((void *)type, 0);
}

void SetTypedefName(type_t *type, const char *name) {
	ASSERT(type != (type_t *)NULL);

	type->name = Copy(type->name, name);
}
void SetTypedefFrom(type_t *type, const char *from) {
	ASSERT(type != (type_t *)NULL);

	type->from = Copy(type->from, from);
}
const char *GetTypedefName(type_t *type) {
	ASSERT(type != (type_t *)NULL);

	return type->name;
}
const char *GetTypedefFrom(type_t *type) {
	ASSERT(type != (type_t *)NULL);

	return type->from;
}

ns_t *GetTypedefNamespace(type_t *type) {
	ASSERT(type != (type_t *)NULL);

	return type->prnt;
}

type_t *TypedefBefore(type_t *type) {
	ASSERT(type != (type_t *)NULL);

	return type->prev;
}
type_t *TypedefAfter(type_t *type) {
	ASSERT(type != (type_t *)NULL);

	return type->next;
}
type_t *FirstTypedef() {
	ASSERT(GetActiveNamespace() != (ns_t *)NULL);

	return GetActiveNamespace()->type_head;
}
type_t *LastTypedef() {
	ASSERT(GetActiveNamespace() != (ns_t *)NULL);

	return GetActiveNamespace()->type_tail;
}

/* -------------------------------------------------------------------------- */

enum_t *NewEnum() {
	enum_t *e;

	ASSERT(GetActiveNamespace() != (ns_t *)NULL);

	e = (enum_t *)Memory((void *)0, sizeof(*e));

	e->name = (char *)NULL;

	e->itemMax = 0;
	e->itemNum = 0;
	e->items = (item_t *)NULL;

	e->prnt = GetActiveNamespace();
	e->next = (enum_t *)NULL;
	if ((e->prev = e->prnt->enum_tail) != (enum_t *)NULL)
		e->prnt->enum_tail->next = e;
	else
		e->prnt->enum_head = e;
	e->prnt->enum_tail = e;

	return e;
}
enum_t *DeleteEnum(enum_t *e) {
	size_t i;

	if (!e)
		return (enum_t *)NULL;

	e->name = (char *)Memory((void *)e->name, 0);

	for(i=0; i<e->itemNum; i++)
		Memory((void *)e->items[i].name, 0);
	e->items = (item_t *)Memory((void *)e->items, 0);

	if (e->prev)
		e->prev->next = e->next;
	if (e->next)
		e->next->prev = e->prev;

	if (e->prnt->enum_head==e)
		e->prnt->enum_head = e->next;
	if (e->prnt->enum_tail==e)
		e->prnt->enum_tail = e->prev;

	e->prev = (enum_t *)NULL;
	e->next = (enum_t *)NULL;

	e->prnt = (ns_t *)NULL;

	return (enum_t *)Memory((void *)e, 0);
}

void SetEnumName(enum_t *e, const char *name) {
	ASSERT(e != (enum_t *)NULL);

	e->name = Copy(e->name, name);
}
const char *GetEnumName(enum_t *e) {
	ASSERT(e != (enum_t *)NULL);

	return e->name;
}

size_t CountEnumItems(enum_t *e) {
	ASSERT(e != (enum_t *)NULL);

	return e->itemNum;
}
size_t AddEnumItem(enum_t *e) {
	size_t n;

	ASSERT(e != (enum_t *)NULL);

	if (e->itemNum + 1 > e->itemMax) {
		n = e->itemMax + 8;

		e->items = (item_t *)Memory((void *)e->items, n*sizeof(item_t));
		e->itemMax = n;
	}

	e->items[e->itemNum].name = (char *)NULL;
	e->items[e->itemNum].value = 0;

	return e->itemNum++;
}
void SetEnumItemName(enum_t *e, size_t i, const char *name) {
	ASSERT(e != (enum_t *)NULL);
	ASSERT(i < e->itemNum);

	e->items[i].name = Copy(e->items[i].name, name);
}
void SetEnumItemValue(enum_t *e, size_t i, int v) {
	ASSERT(e != (enum_t *)NULL);
	ASSERT(i < e->itemNum);

	e->items[i].value = v;
}
const char *GetEnumItemName(enum_t *e, size_t i) {
	ASSERT(e != (enum_t *)NULL);
	ASSERT(i < e->itemNum);

	return e->items[i].name;
}
int GetEnumItemValue(enum_t *e, size_t i) {
	ASSERT(e != (enum_t *)NULL);
	ASSERT(i < e->itemNum);

	return e->items[i].value;
}

ns_t *GetEnumNamespace(enum_t *e) {
	ASSERT(e != (enum_t *)NULL);

	return e->prnt;
}
enum_t *EnumBefore(enum_t *e) {
	ASSERT(e != (enum_t *)NULL);

	return e->prev;
}
enum_t *EnumAfter(enum_t *e) {
	ASSERT(e != (enum_t *)NULL);

	return e->next;
}
enum_t *FirstEnum() {
	ASSERT(GetActiveNamespace() != (ns_t *)NULL);

	return GetActiveNamespace()->enum_head;
}
enum_t *LastEnum() {
	ASSERT(GetActiveNamespace() != (ns_t *)NULL);

	return GetActiveNamespace()->enum_tail;
}

/* -------------------------------------------------------------------------- */

func_t *NewFunc(iface_t *iface) {
	func_t *func;

	func = (func_t *)Memory((void *)0, sizeof(*func));

	func->name = (char *)NULL;
	func->returnType = (char *)NULL;

	func->parmMax = 0;
	func->parmNum = 0;
	func->parms = (parm_t *)NULL;

	func->ns = iface ? GetInterfaceNamespace(iface) : GetActiveNamespace();
	ASSERT(func->ns != (ns_t *)NULL);

	func->iface = iface;
	func->p_head = iface ? &iface->func_head : &func->ns->func_head;
	func->p_tail = iface ? &iface->func_tail : &func->ns->func_tail;
	func->next = (func_t *)NULL;
	if ((func->prev = *func->p_tail) != (func_t *)NULL)
		(*func->p_tail)->next = func;
	else
		*func->p_head = func;
	*func->p_tail = func;

	return func;
}
func_t *DeleteFunc(func_t *func) {
	size_t i;

	if (!func)
		return (func_t *)NULL;

	func->name = (char *)Memory((void *)func->name, 0);
	func->returnType = (char *)Memory((void *)func->returnType, 0);

	for(i=0; i<func->parmNum; i++) {
		Memory((void *)func->parms[i].name, 0);
		Memory((void *)func->parms[i].type, 0);
		Memory((void *)func->parms[i].def, 0);
	}
	func->parms = (parm_t *)Memory((void *)func->parms, 0);

	if (func->prev)
		func->prev->next = func->next;
	if (func->next)
		func->next->prev = func->prev;

	if (*func->p_head==func)
		*func->p_head = func->next;
	if (*func->p_tail==func)
		*func->p_tail = func->prev;

	func->prev = (func_t *)NULL;
	func->next = (func_t *)NULL;

	func->ns = (ns_t *)NULL;
	func->iface = (iface_t *)NULL;

	return (func_t *)Memory((void *)func, 0);
}

void SetFuncName(func_t *func, const char *name) {
	ASSERT(func != (func_t *)NULL);

	func->name = Copy(func->name, name);
}
void SetFuncReturnType(func_t *func, const char *returnType) {
	ASSERT(func != (func_t *)NULL);

	func->returnType = Copy(func->returnType, returnType);
}
const char *GetFuncName(func_t *func) {
	ASSERT(func != (func_t *)NULL);

	return func->name;
}
const char *GetFuncReturnType(func_t *func) {
	ASSERT(func != (func_t *)NULL);

	return func->returnType;
}

size_t CountFuncParms(func_t *func) {
	ASSERT(func != (func_t *)NULL);

	return func->parmNum;
}
size_t AddFuncParm(func_t *func) {
	size_t n;

	ASSERT(func != (func_t *)NULL);

	if (func->parmNum + 1 > func->parmMax) {
		n = func->parmMax + 4;

		func->parms = (parm_t *)Memory((void *)func->parms, n*sizeof(parm_t));
		func->parmMax = n;
	}

	func->parms[func->parmNum].name = (char *)NULL;
	func->parms[func->parmNum].type = (char *)NULL;
	func->parms[func->parmNum].def  = (char *)NULL;

	return func->parmNum++;
}
void SetFuncParmName(func_t *func, size_t i, const char *name) {
	ASSERT(func != (func_t *)NULL);
	ASSERT(i < func->parmNum);

	func->parms[i].name = Copy(func->parms[i].name, name);
}
void SetFuncParmType(func_t *func, size_t i, const char *type) {
	ASSERT(func != (func_t *)NULL);
	ASSERT(i < func->parmNum);

	func->parms[i].type = Copy(func->parms[i].type, type);
}
void SetFuncParmDef(func_t *func, size_t i, const char *def) {
	ASSERT(func != (func_t *)NULL);
	ASSERT(i < func->parmNum);

	func->parms[i].def = Copy(func->parms[i].def, def);
}
const char *GetFuncParmName(func_t *func, size_t i) {
	ASSERT(func != (func_t *)NULL);
	ASSERT(i < func->parmNum);

	return func->parms[i].name;
}
const char *GetFuncParmType(func_t *func, size_t i) {
	ASSERT(func != (func_t *)NULL);
	ASSERT(i < func->parmNum);

	return func->parms[i].type;
}
const char *GetFuncParmDef(func_t *func, size_t i) {
	ASSERT(func != (func_t *)NULL);
	ASSERT(i < func->parmNum);

	return func->parms[i].def;
}

ns_t *GetFuncNamespace(func_t *func) {
	ASSERT(func != (func_t *)NULL);

	return func->ns;
}
iface_t *GetFuncInterface(func_t *func) {
	ASSERT(func != (func_t *)NULL);

	return func->iface;
}

func_t *FuncBefore(func_t *func) {
	ASSERT(func != (func_t *)NULL);

	return func->prev;
}
func_t *FuncAfter(func_t *func) {
	ASSERT(func != (func_t *)NULL);

	return func->next;
}
func_t *FirstFunc() {
	ASSERT(GetActiveNamespace() != (ns_t *)NULL);

	return GetActiveNamespace()->func_head;
}
func_t *LastFunc() {
	ASSERT(GetActiveNamespace() != (ns_t *)NULL);

	return GetActiveNamespace()->func_tail;
}

/* -------------------------------------------------------------------------- */

iface_t *NewInterface() {
	iface_t *iface;

	iface = (iface_t *)Memory((void *)NULL, sizeof(*iface));

	iface->name = (char *)NULL;
	iface->base = (iface_t *)NULL;

	iface->func_head = (func_t *)NULL;
	iface->func_tail = (func_t *)NULL;

	iface->prnt = GetActiveNamespace();
	ASSERT(iface->prnt != (ns_t *)NULL);

	iface->next = (iface_t *)NULL;
	if ((iface->prev = iface->prnt->iface_tail) != (iface_t *)NULL)
		iface->prnt->iface_tail->next = iface->next;
	else
		iface->prnt->iface_head = iface;
	iface->prnt->iface_tail = iface;

	return iface;
}
iface_t *DeleteInterface(iface_t *iface) {
	if (!iface)
		return (iface_t *)NULL;

	iface->name = (char *)Memory((void *)iface->name, 0);

	while(iface->func_head)
		DeleteFunc(iface->func_head);

	if (iface->prev)
		iface->prev->next = iface->next;
	if (iface->next)
		iface->next->prev = iface->prev;

	if (iface->prnt->iface_head==iface)
		iface->prnt->iface_head = iface->next;
	if (iface->prnt->iface_tail==iface)
		iface->prnt->iface_tail = iface->prev;

	iface->prev = (iface_t *)NULL;
	iface->next = (iface_t *)NULL;

	return (iface_t *)Memory((void *)iface, 0);
}

void SetInterfaceName(iface_t *iface, const char *name) {
	ASSERT(iface != (iface_t *)NULL);

	iface->name = Copy(iface->name, name);
}
const char *GetInterfaceName(iface_t *iface) {
	ASSERT(iface != (iface_t *)NULL);

	return iface->name;
}

void SetInterfaceBase(iface_t *iface, iface_t *base) {
	ASSERT(iface != (iface_t *)NULL);

	iface->base = base;
}
iface_t *GetInterfaceBase(iface_t *iface) {
	ASSERT(iface != (iface_t *)NULL);

	return iface->base;
}

func_t *FirstInterfaceFunc(iface_t *iface) {
	ASSERT(iface != (iface_t *)NULL);

	return iface->func_head;
}
func_t *LastInterfaceFunc(iface_t *iface) {
	ASSERT(iface != (iface_t *)NULL);

	return iface->func_tail;
}

ns_t *GetInterfaceNamespace(iface_t *iface) {
	ASSERT(iface != (iface_t *)NULL);

	return iface->prnt;
}

iface_t *InterfaceBefore(iface_t *iface) {
	ASSERT(iface != (iface_t *)NULL);

	return iface->prev;
}
iface_t *InterfaceAfter(iface_t *iface) {
	ASSERT(iface != (iface_t *)NULL);

	return iface->next;
}
iface_t *FirstInterface() {
	ASSERT(GetActiveNamespace() != (ns_t *)NULL);

	return GetActiveNamespace()->iface_head;
}
iface_t *LastInterface() {
	ASSERT(GetActiveNamespace() != (ns_t *)NULL);

	return GetActiveNamespace()->iface_tail;
}

/* -------------------------------------------------------------------------- */

ir_t *NewIR() {
	ir_t *ir;

	ir = (ir_t *)Memory((void *)NULL, sizeof(*ir));

	ir->tokenMax = 0;
	ir->tokenNum = 0;
	ir->tokens = (token_t *)NULL;

	return ir;
}
ir_t *DeleteIR(ir_t *ir) {
	size_t i;

	if (!ir)
		return (ir_t *)NULL;

	for(i=0; i<ir->tokenNum; i++) {
		if (ir->tokens[i].tokenType==kIR_TokType_Emit
		||  ir->tokens[i].tokenType==kIR_TokType_EmitC
		||  ir->tokens[i].tokenType==kIR_TokType_EmitCPP)
			Memory((void *)ir->tokens[i].data.emit, 0);
	}

	ir->tokens = (token_t *)Memory((void *)ir->tokens, 0);

	return (ir_t *)Memory((void *)ir, 0);
}

size_t CountIRTokens(ir_t *ir) {
	ASSERT(ir != (ir_t *)NULL);

	return ir->tokenNum;
}
size_t AddIRToken(ir_t *ir) {
	ASSERT(ir != (ir_t *)NULL);

	if (ir->tokenNum + 1 > ir->tokenMax) {
		ir->tokenMax += 8;

		ir->tokens = (token_t *)Memory((void *)ir->tokens,
			ir->tokenMax*sizeof(token_t));
	}

	ir->tokens[ir->tokenNum].tokenType = kIR_TokType_Emit;
	ir->tokens[ir->tokenNum].data.emit = (char *)NULL;

	return ir->tokenNum++;
}

void SetIRTokenType(ir_t *ir, size_t i, tokenType_t type) {
	ASSERT(ir != (ir_t *)NULL);
	ASSERT(i < ir->tokenNum);

	ir->tokens[i].tokenType = type;
}
void SetIRTokenEmit(ir_t *ir, size_t i, const char *emit) {
	ASSERT(ir != (ir_t *)NULL);
	ASSERT(i < ir->tokenNum);

	ir->tokens[i].data.emit = Copy(ir->tokens[i].data.emit, emit);
}
void SetIRTokenNamespace(ir_t *ir, size_t i, ns_t *ns) {
	ASSERT(ir != (ir_t *)NULL);
	ASSERT(i < ir->tokenNum);

	ir->tokens[i].data.ns = ns;
}

tokenType_t GetIRTokenType(ir_t *ir, size_t i) {
	ASSERT(ir != (ir_t *)NULL);
	ASSERT(i < ir->tokenNum);

	return ir->tokens[i].tokenType;
}
const char *GetIRTokenEmit(ir_t *ir, size_t i) {
	ASSERT(ir != (ir_t *)NULL);
	ASSERT(i < ir->tokenNum);

	return ir->tokens[i].data.emit;
}
ns_t *GetIRTokenNamespace(ir_t *ir, size_t i) {
	ASSERT(ir != (ir_t *)NULL);
	ASSERT(i < ir->tokenNum);

	return ir->tokens[i].data.ns;
}

size_t AddIREmitToken(ir_t *ir, const char *emit) {
	size_t i;

	i = AddIRToken(ir);
	SetIRTokenType(ir, i, kIR_TokType_Emit);
	SetIRTokenEmit(ir, i, emit);

	return i;
}
size_t AddIREmitCToken(ir_t *ir, const char *emit) {
	size_t i;

	i = AddIRToken(ir);
	SetIRTokenType(ir, i, kIR_TokType_EmitC);
	SetIRTokenEmit(ir, i, emit);

	return i;
}
size_t AddIREmitCPPToken(ir_t *ir, const char *emit) {
	size_t i;

	i = AddIRToken(ir);
	SetIRTokenType(ir, i, kIR_TokType_EmitCPP);
	SetIRTokenEmit(ir, i, emit);

	return i;
}
size_t AddIRNamespaceToken(ir_t *ir, ns_t *ns) {
	size_t i;

	i = AddIRToken(ir);
	SetIRTokenType(ir, i, kIR_TokType_Namespace);
	SetIRTokenNamespace(ir, i, ns);

	return i;
}
size_t AddIRTokenWithType(ir_t *ir, tokenType_t type) {
	size_t i;

	ASSERT(type != kIR_TokType_Emit);
	ASSERT(type != kIR_TokType_EmitC);
	ASSERT(type != kIR_TokType_EmitCPP);
	ASSERT(type != kIR_TokType_Namespace);

	i = AddIRToken(ir);
	SetIRTokenType(ir, i, type);

	return i;
}
