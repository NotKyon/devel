#ifndef LOCAL_H
#define LOCAL_H

#include <nf.h>

#define EXPORT extern "C" __declspec(dllexport)

#if __GNUC__ || __clang__ || __INTEL_COMPILER
# define likely(x) __builtin_expect((x), 1)
# define unlikely(x) __builtin_expect((x), 0)
#else
# define likely(x) (x)
# define unlikely(x) (x)
#endif

struct GUI_Menu;
struct GUI_ToolBar;
struct GUI_StatusBar;
struct GUI_Window;

struct GUI_Menu {
    string name, tip;

    GUI_Window *window;
    GUI_Menu *prnt;
    GUI_Menu *prev, *next;
    GUI_Menu *head, *tail;

    inline GUI_Menu() {
        name.assign("");
        tip.assign("");

        window = (GUI_Window *)NULL;
        prnt = (GUI_Menu *)NULL;
        prev = (GUI_Menu *)NULL;
        next = (GUI_Menu *)NULL;
        head = (GUI_Menu *)NULL;
        tail = (GUI_Menu *)NULL;
    }
    inline ~GUI_Menu() {
        while(head)
            delete head;

        if (window) {
            GUI_Menu **p_head, **p_tail;

            p_head = prnt ? &prnt->head : &window->menu_head;
            p_tail = prnt ? &prnt->tail : &window->menu_tail;

            if (prev)
                prev->next = next;
            else
                *p_head = next;
            if (next)
                next->prev = prev;
            else
                *p_tail = prev;
        }
    }
};

#endif
