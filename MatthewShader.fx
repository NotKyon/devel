// transform object vertices to view space and project them in perspective:
float4x4 gWvpXf : WorldViewProjection < string UIWidget="None"; >;

struct appdata {
    float3 Position	: POSITION;
};

struct QuadVertexOutput {
    float4 Position	: POSITION;
};

QuadVertexOutput unlitVS(	appdata IN,
						uniform float4x4 WvpXf
) {
    QuadVertexOutput OUT = (QuadVertexOutput)0;

    float4 Po = float4(IN.Position.xyz,1);

    OUT.Position   = mul(Po,WvpXf);

    return OUT;
}

float4 useMRTPS(QuadVertexOutput IN) : COLOR
{
	//return(0,0,0,1);
	return float4( 0, 1, 0, 1 );
}

technique Main <> {
    pass deferred_lighting <> {        
        VertexShader = compile vs_3_0 unlitVS(gWvpXf);
	    ZEnable = true;
		ZWriteEnable = true;
		ZFunc = LessEqual;
		AlphaBlendEnable = false;
		CullMode = None;
        PixelShader = compile ps_3_0 useMRTPS();
    }
}
