#include <ember/buffer.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

static int __buf_init( struct stBuffer *self )
{
	self->name = ( char * )0;
	self->line = 0;

	self->source = ( char * )0;
	self->ptr    = ( const char * )0;

	self->cb = ( buffer_callback_t )0;

	return 1;
}
static void __buf_deinit( struct stBuffer *self )
{
	if( self->name )
		free( ( void * )self->name );

	if( self->source )
		free( ( void * )self->source );
}

int buf_init()
{
	if( !( g_buf_pool =
	            gc_register_pool( sizeof( struct stBuffer ), ( gc_init_t )__buf_init,
	                ( gc_deinit_t )__buf_deinit ) ) )
		return 0;

	return 1;
}
void buf_deinit()
{
	gc_unregister_pool( g_buf_pool );
}

buffer_t buf_alloc( const char *name, char *source, buffer_callback_t cb )
{
	buffer_t buf;

	if( !( buf = ( buffer_t )gc_alloc( g_buf_pool ) ) )
		return ( buffer_t )0;

	buf->name = name ? strdup( name ) : ( char * )0;
	buf->line = 1;

	buf->source = source ? strdup( source ) : ( char * )0;
	buf->ptr    = buf->source;

	buf->cb = cb;

	return buf;
}
void buf_dealloc( buffer_t buf )
{
	gc_release( ( void * )buf );
}

const char *buf_name( buffer_t buf )
{
	return buf->name;
}
unsigned int buf_line( buffer_t buf )
{
	return buf->line;
}

char buf_read( buffer_t buf )
{
	char c;

	if( ( c = *buf->ptr ) != 0 )
		buf->ptr++;

	if( c == '\n' )
		buf->line++;

	return c;
}

char buf_look( buffer_t buf )
{
	return *buf->ptr;
}

char buf_peek( buffer_t buf )
{
	if( *buf->ptr == 0 )
		return 0;

	return buf->ptr[ 1 ];
}

void buf_unget( buffer_t buf )
{
	if( buf->ptr != buf->source )
		buf->ptr--;
}

void buf_reset( buffer_t buf )
{
	buf->ptr  = buf->source;
	buf->line = 1;
}

void buf_report( buffer_t buf, int level, const char *format, ... )
{
	static char _text[ 4096 ];
	va_list args;

	/*__asm__ __volatile__("int $3");*/

	va_start( args, format );
#ifdef _WIN32
	_vsnprintf( _text, sizeof( _text ) - 1, format, args );
#else
	vsnprintf( _text, sizeof( _text ) - 1, format, args );
#endif
	va_end( args );

	_text[ sizeof( _text ) - 1 ] = 0;

	if( buf->cb )
		buf->cb( buf, level, _text );
	else
		fprintf( stderr, "error: %s:%u: %s\n", buf->name, buf->line, _text );
}

buffer_t buf_load( const char *filename, buffer_callback_t cb )
{
	buffer_t buf;
	size_t size;
	char *source;
	FILE *file;

	if( !( file = fopen( filename, "r" ) ) )
		return ( buffer_t )0;

	fseek( file, 0, SEEK_END );
	size = ( size_t )ftell( file );

	fseek( file, 0, SEEK_SET );
	if( !( source = ( char * )malloc( size + 1 ) ) ) {
		fclose( file );
		return ( buffer_t )0;
	}

	fread( ( void * )source, size, 1, file );
	fclose( file );

	source[ size ] = 0;

	if( !( buf = buf_alloc( filename, ( char * )0, cb ) ) ) {
		free( ( void * )source );
		return ( buffer_t )0;
	}

	buf->source = source;
	buf->ptr    = source;

	return buf;
}
