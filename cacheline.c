#include <stdio.h>

int DetectCacheLineSize() {
#define VENDOR_INTEL "GenuineIntel"
#define VENDOR_AMD "AuthenticAMD"
	char vendor[12];

	asm("cpuid" : "=b" (*(int *)&vendor[0]),
	              "=d" (*(int *)&vendor[4]),
	              "=c" (*(int *)&vendor[8])
	            : "a" (0x00000000));

	if (*(int *)&vendor[0] == *(int *)&VENDOR_INTEL[0]
	&&  *(int *)&vendor[4] == *(int *)&VENDOR_INTEL[4]
	&&  *(int *)&vendor[8] == *(int *)&VENDOR_INTEL[8]) {
		int ebx;

		printf("Intel!\n");fflush(stdout);

		asm("cpuid" : "=b" (ebx) : "a" (0x00000001));

		return ((ebx&0xFFFF0000)>>16)*8; /*bh*8 = cache line size*/
	}

	if (*(int *)&vendor[0] == *(int *)&VENDOR_AMD[0]
	&&  *(int *)&vendor[4] == *(int *)&VENDOR_AMD[4]
	&&  *(int *)&vendor[8] == *(int *)&VENDOR_AMD[8]) {
		int ecx;

		printf("AMD!\n");fflush(stdout);

		asm("cpuid" : "=c" (ecx) : "a" (0x80000005));

		return (ecx&0x000000FF); /*data cache line size in cl*/
	}

	printf("Could not detect vendor! \"%.12s\"\n", vendor);fflush(stdout);
	return 0;
}

int main() {
	int x;

	x = DetectCacheLineSize();
	printf("Cache Line Size: %i\n", x);fflush(stdout);

	return 0;
}
