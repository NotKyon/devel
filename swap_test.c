void xor_swap(register int *a, register int *b) {
	*a ^= *b;
	*b ^= *a;
	*a ^= *b;
}
void nrm_swap(register int *a, register int *b) {
	register int x;

	x = *a;
	*a = *b;
	*b = x;
}
