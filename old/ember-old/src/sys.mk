# Determine the operating system, and the number of CPUs available
ifdef OS
 ifeq ($(OS),Windows_NT)
  OS := Windows
 else
  ifdef SystemRoot
   OS := Windows
  endif
 endif
endif
ifeq ($(OS),Windows)
 ifdef NUMBER_OF_PROCESSORS
  CPUCOUNT := $(NUMBER_OF_PROCESSORS)
 endif
 ifeq ($(shell uname -o),Cygwin)
  OS := Cygwin
 endif
 ifeq ($(MAKE_MODE),unix)
  OS := Msys
  EXT := .exe
  DYLIB_PREFIX :=
  DYLIB_SUFFIX := .dll
 endif
 ifdef MSYSCON
  OS := Msys
  EXT := .exe
  DYLIB_PREFIX :=
  DYLIB_SUFFIX := .dll
 endif
else
 OS := $(shell uname -s)
 ifeq ($(OS),Linux)
  CPUCOUNT := $(shell grep -c ^processor /proc/cpuinfo)
 elifeq ($(OS),Darwin)
  CPUCOUNT := $(shell hwprefs cpu_count)
 endif
endif
ifndef CPUCOUNT
 CPUCOUNT := $J
endif

# Determine the shell utilities
ifeq ($(OS),Windows)
 RM := del /Q
else
 DYLIB_PREFIX := lib
 ifeq ($(OS),Darwin)
  DYLIB_SUFFIX := .dylib
 else
  DYLIB_SUFFIX := .so
 endif
 RM := rm -f
endif

# Determine how to refer to targets of the operating systems
WINDOWS_SUFFIX   = mswin
LINUX_SUFFIX     = linux
MACOS_SUFFIX     = macos
ifeq ($(OS),Windows)
 OS_SUFFIX       = $(WINDOWS_SUFFIX)
endif
ifeq ($(OS),Cygwin)
 OS_SUFFIX       = $(WINDOWS_SUFFIX)
endif
ifeq ($(OS),Msys)
 OS_SUFFIX       = $(WINDOWS_SUFFIX)
endif
ifeq ($(OS),Linux)
 OS_SUFFIX       = $(LINUX_SUFFIX)
endif
ifeq ($(OS),Darwin)
 OS_SUFFIX       = $(MACOS_SUFFIX)
endif

# Determine the prefix to install to
ifeq ($(OS_SUFFIX),$(WINDOWS_SUFFIX))
 PREFIX          ?= ./
endif
ifeq ($(OS_SUFFIX),$(LINUX_SUFFIX))
 PREFIX          ?= /usr/local
endif
ifeq ($(OS_SUFFIX),$(MACOS_SUFFIX))
 PREFIX          ?= /usr/local
endif

# Standard configuration options
CONFIG           = DEBUG

CFLAGS_ALL       = -W -Wall -pedantic
CFLAGS_RELEASE   = -O3 -fomit-frame-pointer -mtune=core2 -D_NDEBUG -DNDEBUG
CFLAGS_DEBUG     = -g -D_DEBUG -DDEBUG -D__DEBUG__ -D__debug__

CXXFLAGS_ALL     = $(CFLAGS_ALL) -Weffc++
CXXFLAGS_RELEASE = $(CFLAGS_RELEASE)
CXXFLAGS_DEBUG   = $(CFLAGS_DEBUG)

LFLAGS_ALL       =
LFLAGS_RELEASE   = -s
LFLAGS_DEBUG     =

LEXFLAGS_ALL     =
LEXFLAGS_RELEASE = -f
LEXFLAGS_DEBUG   = -d

YACFLAGS_ALL     =
YACFLAGS_RELEASE =
YACFLAGS_DEBUG   =

CFLAGS           = $(CFLAGS_ALL) $(CFLAGS_$(CONFIG))
CXXFLAGS         = $(CXXFLAGS_ALL) $(CXXFLAGS_$(CONFIG))
LFLAGS           = $(LFLAGS_ALL) $(LFLAGS_$(CONFIG))

# TODO: An "auto" target? (Use the CPUCOUNT variable and call make again, this
#       time passing in "all" instead of "auto"
