#include <stdio.h>
#include <math.h>

#define TB_RES (0x8000)//(0x8000)
static double g_sinTable[TB_RES];

#define PI 3.1415926535897932384626433832795
#define DEG_2_RAD (0.0174532925)
#define RAD_2_DEG (57.2957795)

void InitTables()
{
	int i;
	
	for(i = 0; i < TB_RES; ++i)
	{
		g_sinTable[i] = sin(((double)i/(double)TB_RES) * (PI * 2));
	}
}
double Sin(double deg)
{
	return g_sinTable[((unsigned int)(deg/360.0*(double)TB_RES))%TB_RES];
}
double Cos(double deg)
{
	return Sin(deg+90);
}

//+aaronmod (http://www.iquilezles.org/apps/soundtoy/index.html)
#define SINE 0
#define PIANO1 1
#define PIANO2 2
#define SPACE_PIANO 3
#define GUITAR 4
#define FLUTE 5
#define RHYTHM 6
#define AARON1 7
#define AARON2 8

#ifndef INSTRUMENT
# define INSTRUMENT GUITAR
#endif

double sign(double x) {
	return x < 0 ? -1 : x > 0 ? +1 : 0;
}
double saw(double x, double a) {
	double f;

	f = fmod(x, 1);

	return f < a ? f/a : 1.0 - (f - a)/(1.0 - a);
}
double grad(int n, double x) {
	n = (n << 13) ^ n;
	n = (n*(n*n*15731 + 789221) + 1376312589);

	return (n & 0x20000000) ? -x : x;
}
double noise(double x) {
	double i, f, w, a, b;

	i = floor(x);
	f = x - i;
	w = f*f*f*(f*(f*6.0 - 15.0) + 10.0);
	a = grad(i+0, f+0);
	b = grad(i+1, f-1);

	return a + (b - a)*w;
}

double clamp(double x, double a, double b)
{
	return x < a ? a : ( x > b ? b : x );
}

double SoundShader(double w, double t)
{
	double y;

	//var f = 440.0*Math.pow( 2.0, (sid-9-12)/12.0 );
	w = 3.14159265358979*2.0*w;

#if INSTRUMENT==SINE
	//y  = sin(w*t)*exp(-5*t);

	y  = clamp((sin(w*t/2)), -0.5f, 0.5f);
	y *= clamp((1.0f - (t - 0.01f))*16.0f, -1.0f, 1.0f)*(1.0f - t);

	y = 13*atan(0.00076f*y) + 3.5f*atan((y/7500)*(y/7500));
	y = y*70;
#elif INSTRUMENT==PIANO1
	y  = 0.6*sin(1.0*w*t)*exp(-0.0008*w*t);
	y += 0.3*sin(2.0*w*t)*exp(-0.0010*w*t);
	y += 0.1*sin(4.0*w*t)*exp(-0.0015*w*t);
	y += 0.2*y*y*y;
	y *= 0.9 + 0.1*cos(70.0*t);
	y += 2.0*y*exp(-22.0*t);
#elif INSTRUMENT==PIANO2
	{
		double rt, r, a, b, y2, y3;

		t   = t + .00015*noise(12*t);

		rt  = t;

		r   = t*w*.2;
		r   = fmod(r,1);
		a   = 0.15 + 0.6*(rt);
		b   = 0.65 - 0.5*(rt);
		y   = 50*r*(r-1)*(r-.2)*(r-a)*(r-b);


		r   = t*w*.401;
		r   = fmod(r,1);
		a   = 0.12 + 0.65*(rt);
		b   = 0.67 - 0.55*(rt);
		y2  = 50*r*(r-1)*(r-.4)*(r-a)*(r-b);
		r   = t*w*.399;
		r   = fmod(r,1);
		a   = 0.14 + 0.55*(rt);
		b   = 0.66 - 0.65*(rt);
		y3  = 50*r*(r-1)*(r-.8)*(r-a)*(r-b);

		y  += .02*noise(1000*t);

		y  /= (t*w*.0015+.1);
		y2 /= (t*w*.0020+.1);
		y3 /= (t*w*.0025+.1);

		y   = (y+y2+y3)/3;
	}
#elif INSTRUMENT==SPACE_PIANO
	{
		double tt, a, b, c;

		tt = 1-t;
		a  = sin(t*w*.5)*log(t+0.3)*tt;
		b  = sin(t*w)*t*.4;
		c  = fmod(tt,.075)*cos(pow(tt,3)*w)*t*2;
		y  = (a+b+c)*tt;
	}
#elif INSTRUMENT==GUITAR
	{
		double f;

		f  = cos(0.251*w*t);
		y  = 0.5*cos(1.0*w*t+3.14*f)*exp(-0.0007*w*t);
		y += 0.2*cos(2.0*w*t+3.14*f)*exp(-0.0009*w*t);
		y += 0.2*cos(4.0*w*t+3.14*f)*exp(-0.0016*w*t);
		y += 0.1*cos(8.0*w*t+3.14*f)*exp(-0.0020*w*t);
		y *= 0.9 + 0.1*cos(70.0*t);
		y  = 2.0*y*exp(-22.0*t) + y;
	}
#elif INSTRUMENT==FLUTE
	y  = 6.0*t*exp(-2*t)*sin(w*t);
	y *= 0.8 + 0.2*cos(16*t);
#elif INSTRUMENT==RHYTHM
	{
		double h, g, a, f, t2, s;

		h = fmod(t,.5);
		y  = 0.2*noise(32000*h)*exp(-32*h);
		y += 1.0*noise(3200*h)*exp(-32*h);
		y += 7.0*cos( 320-100*exp(-10*h))*exp(-4*h);
		//---------
		h = fmod(t+.15,1.0);
		y += 0.5*noise(32000*h)*exp(-64*h);
		//------------
		h = fmod(t+.25,1.0);
		y += 1.0*noise(32000*h)*exp(-32*h);
		//------------
		t += .25;
		s = sign(sin(.5*6.2831*t));
		h = fmod(t,.5);
		y += 2.0*cos(6.2831*(105+11*s)*t)*exp(-6*h);
		//---------
		h = fmod(t,.125)/.125;
		y += 1.4*noise(320*h)*exp(-32*h);
		//---------
		g = .018;
		t2 = t+ .05*cos(t*6.2831);
		f = fmod(t2,g)/g;
		a = .5+.4*cos(6.2831*t2);
		f = saw(f,a);
		f = -1.0+2*f;
		f = f*f*f;
		y += f*1.5;
		//---------
		y *= .6;
	}
#elif INSTRUMENT==AARON1
	y = (0.01*w*t*exp(1 - 0.01*w*t))*sin(w*t)*exp(-5*t);
#elif INSTRUMENT==AARON2
	y = (0.006*w*t*exp(1 - 0.006*w*t))*sin(w*t)*exp(-5*t);
	y = y + sin(w*t)*exp(-5*t);
	y = 13*atan(0.00076*y) + 3.5*atan((y/7500)*(y/7500));
	y = y*70;
#else
	y  = sin(w*t);
#endif

	     if(y < -1) y = -1;
	else if(y > +1) y = +1;

	return y;
}

//------------------------------------------------------------------------------

#define NOTE_A			0
#define NOTE_A_SHARP	1
#define NOTE_B			2
#define NOTE_C			3
#define NOTE_C_SHARP	4
#define NOTE_D			5
#define NOTE_D_SHARP	6
#define NOTE_E			7
#define NOTE_F			8
#define NOTE_F_SHARP	9
#define NOTE_G			10
#define NOTE_G_SHARP	11

#define DURATION_SIXTEENTH	{ 1, 16 }
#define DURATION_EIGHTH		{ 1, 8 }
#define DURATION_QUARTER	{ 1, 4 }
#define DURATION_HALF		{ 1, 2 }
#define DURATION_WHOLE		{ 1, 1 }
#define DURATION_DOUBLE		{ 2, 1 }

typedef struct
{
	unsigned int string_note[6];
	unsigned int string_octave[6];
} guitar_t;

typedef struct
{
	unsigned int fret;
	unsigned int string;
	unsigned int length[2]; // 0 = numerator, 1 = denominator
} tab_note_t;

typedef struct
{
	guitar_t *guitar;
	tab_note_t *notes;
	size_t num_notes;
	double tempo;
	double volume;
} gtr_track_t;

guitar_t gtr_eadgbe =
{
	{ NOTE_E, NOTE_B, NOTE_G, NOTE_D, NOTE_A, NOTE_E },
	{ 5,      4,      4,      4,      3,      3      }
};
#if 0
/* fur elise + test */
tab_note_t gtr_tabs[] =
{
	{  4, 1, DURATION_QUARTER },
	{  0, 0, DURATION_QUARTER },
	{  4, 1, DURATION_QUARTER },
	{  0, 0, DURATION_QUARTER },
	{  4, 1, DURATION_QUARTER },
	{  0, 1, DURATION_QUARTER },
	{  3, 1, DURATION_QUARTER },
	{  1, 1, DURATION_QUARTER },
	{  2, 2, DURATION_HALF },

	{ 14, 0, DURATION_HALF },
	{ 15, 0, DURATION_HALF },
	{ 12, 0, DURATION_WHOLE },
	{  0, 5, DURATION_DOUBLE },
	{  0, 4, DURATION_WHOLE },
	{  0, 3, DURATION_WHOLE },
	{  0, 2, DURATION_WHOLE },
	{  0, 1, DURATION_WHOLE },
	{  0, 0, DURATION_WHOLE },
	{  0, 5, DURATION_WHOLE },
	{  1, 5, DURATION_WHOLE },
	{  2, 5, DURATION_WHOLE },
	{  3, 5, DURATION_WHOLE }
};
#endif

/* notes for pitch practice (A B C D E F G) */
tab_note_t gtr_tabs[] =
{
	{ 1, 0, DURATION_QUARTER },
	{ 1, 0, DURATION_QUARTER },
	{ 1, 0, DURATION_QUARTER },
	{ 1, 0, DURATION_QUARTER },
	
	{ 1, 2, DURATION_QUARTER },
	{ 1, 2, DURATION_QUARTER },
	{ 1, 2, DURATION_QUARTER },
	{ 1, 2, DURATION_QUARTER },
	
	{ 1, 3, DURATION_QUARTER },
	{ 1, 3, DURATION_QUARTER },
	{ 1, 3, DURATION_QUARTER },
	{ 1, 3, DURATION_QUARTER },
	
	{ 2, 0, DURATION_QUARTER },
	{ 2, 0, DURATION_QUARTER },
	{ 2, 0, DURATION_QUARTER },
	{ 2, 0, DURATION_QUARTER },
	
	{ 2, 2, DURATION_QUARTER },
	{ 2, 2, DURATION_QUARTER },
	{ 2, 2, DURATION_QUARTER },
	{ 2, 2, DURATION_QUARTER },
	
	{ 2, 3, DURATION_QUARTER },
	{ 2, 3, DURATION_QUARTER },
	{ 2, 3, DURATION_QUARTER },
	{ 2, 3, DURATION_QUARTER },
	
	{ 3, 0, DURATION_QUARTER },
	{ 3, 0, DURATION_QUARTER },
	{ 3, 0, DURATION_QUARTER },
	{ 3, 0, DURATION_QUARTER },
	
	{ 3, 2, DURATION_QUARTER },
	{ 3, 2, DURATION_QUARTER },
	{ 3, 2, DURATION_QUARTER },
	{ 3, 2, DURATION_QUARTER },
	
	{ 4, 0, DURATION_QUARTER },
	{ 4, 0, DURATION_QUARTER },
	{ 4, 0, DURATION_QUARTER },
	{ 4, 0, DURATION_QUARTER },
	
	{ 4, 1, DURATION_QUARTER },
	{ 4, 1, DURATION_QUARTER },
	{ 4, 1, DURATION_QUARTER },
	{ 4, 1, DURATION_QUARTER },
	
	{ 4, 3, DURATION_QUARTER },
	{ 4, 3, DURATION_QUARTER },
	{ 4, 3, DURATION_QUARTER },
	{ 4, 3, DURATION_QUARTER },
	
	{ 5, 0, DURATION_QUARTER },
	{ 5, 0, DURATION_QUARTER },
	{ 5, 0, DURATION_QUARTER },
	{ 5, 0, DURATION_QUARTER },
	
	{ 5, 1, DURATION_QUARTER },
	{ 5, 1, DURATION_QUARTER },
	{ 5, 1, DURATION_QUARTER },
	{ 5, 1, DURATION_QUARTER },
	
	{ 5, 3, DURATION_QUARTER },
	{ 5, 3, DURATION_QUARTER },
	{ 5, 3, DURATION_QUARTER },
	{ 5, 3, DURATION_QUARTER }
};

gtr_track_t gtr_track =
{
	&gtr_eadgbe,
	gtr_tabs,
	sizeof(gtr_tabs)/sizeof(gtr_tabs[0]),
	/*60.0*/30.0,	//tempo (beats per minute)
	0.8		//volume (0.0 to 1.0)
};

double NoteFreq(unsigned int note, unsigned int octave)
{
	double f;

	f = pow(2.0, ((double)(note + (octave*12)) - 24.0)/12.0);
	f = f*440.0;

	return f;
}
double ConvertTabToFreq(const guitar_t *gtr, const tab_note_t *note)
{
	unsigned int n, o;
	double f;

	n  = gtr->string_note[note->string];
	o  = gtr->string_octave[note->string];
	n += note->fret;
	f  = NoteFreq(n, o);

	return f;
}
double NoteDuration(const tab_note_t *note, double tempo)
{
	const unsigned int *l;
	double t;

	l = &note->length[0];

	t  = ((double)l[0])/((double)l[1]);
	t *= 60.0/tempo;

	printf("NoteDuration: %f\n", t);

	return t;
}
void WriteNote(FILE *f, double freq, double duration, double volume,
unsigned int sampPerSec)
{
	unsigned short samp;
	unsigned int i;
	double t, total;

	volume *= 32767; //conversion to short
	total = duration*(double)sampPerSec;

	for(i=0; i<(unsigned int)total; i++) {
		//t = ((double)i)/total;
		t = ( ( double )i )/( double )sampPerSec;

		samp = ( short )( volume*SoundShader( freq, fmod( t, 1.0 ) ) );
		fwrite( &samp, sizeof( samp ), 1, f );
	}
}
void PlayGuitar(FILE *f, gtr_track_t *track, unsigned int sampPerSec)
{
	double fr, t;
	size_t i;

	for(i=0; i<track->num_notes; i++) {

		fr = ConvertTabToFreq(track->guitar, &track->notes[i]);
		t  = NoteDuration(&track->notes[i], track->tempo);

		printf("Freq: %f\n", fr);

		WriteNote(f, fr, t, track->volume, sampPerSec);
	}
}
unsigned int FindTotalSamples(gtr_track_t *track, unsigned int sampPerSec)
{
	unsigned int numSamples;
	double t;
	size_t i;

	numSamples = 0;

	for(i=0; i<track->num_notes; i++) {
		t = NoteDuration(&track->notes[i], track->tempo);
		numSamples += (unsigned int)(t*(double)sampPerSec);
	}

	return numSamples;
}
//-aaronmod
int main()
{
	InitTables();
	int i;
	unsigned int ui;
	unsigned short us;
	
	FILE *f = fopen("audio.wav", "wb");
	
	//+aaronmod
#if JIMS_VERSION
	unsigned int samples = 44100 * 6;
#else
	unsigned int samples = FindTotalSamples(&gtr_track, 44100);
	printf("samples: %u\n", samples);
#endif
	//-aaronmod
	
	fwrite("RIFF", 4, 1, f);
	unsigned int size = samples * sizeof(short) - 8;
	fwrite(&size, 4, 1, f);
	fwrite("WAVE", 4, 1, f);
	
	/*fmt*/
	fwrite("fmt ", 4, 1, f);
	ui = 16; /*Size*/
	fwrite(&ui, 4, 1, f);
	us = 1; /*Compression code. 0x0001 = PCM*/
	fwrite(&us, 2, 1, f);
	us = 1; /*Mono*/
	fwrite(&us, 2, 1, f);
	ui = 44100; /*Sample rate*/
	fwrite(&ui, 4, 1, f);
	ui = 44100 * sizeof(short); /*Avg Bytes per Second*/
	fwrite(&ui, 4, 1, f);
	us = 2; /*Block align*/
	fwrite(&us, 2, 1, f);
	us = 16; /*bits per sample*/
	fwrite(&us, 2, 1, f);
	
	/*data*/
	fwrite("data", 4, 1, f);
	ui = samples * sizeof(short);
	fwrite(&ui, 4, 1, f);
	
	
	//+aaronmod
#if JIMS_VERSION
	float freq1 = 261.6;
	
	float steps[] = {82.41, 110, 146.8, 196, 246.9, 329.6};
	
	short samp = 0;
	for(ui = 0; ui < samples; ++ui)
	{
		freq1 = steps[(unsigned int)((float)ui/(float)samples*6)];
	
		short vol = (32767 * .8);
		//+aaronmod
		//samp = (short)(vol * Sin((ui * 360.0 * freq1)/44100.0));
		//samp = (short)(vol * sinf((ui * 3.1415926535*2 * freq1)/44100.0));
		samp = (short)(vol*SoundShader(freq1, fmod(((double)ui)/44100.0, 1)));
		//-aaronmod
		fwrite(&samp, sizeof(samp), 1, f);
	}
#else
	PlayGuitar(f, &gtr_track, 44100);
#endif
	//-aaronmod
	
	fclose(f);
	
	return 0;
}
