#ifndef MAIN_H
#define MAIN_H

#include "local.h"

#include "report.h"
#include "memory.h"
#include "string.h"

#include "buffer.h"
#include "lookup.h"
#include "ast.h"

#include "utility.h"
#include "opts.h"

#include "ir.h"
#include "gen.h"

#endif
