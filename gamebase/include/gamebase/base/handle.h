#ifndef GB_BASE_HANDLE_H
#define GB_BASE_HANDLE_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Handle Functions (This is an *INTERNAL* API!) */
typedef void *gb_handleType_t;
typedef void *gb_handle_t;

typedef gb_bool_t(*gb_fnCtor_t)(void *);
typedef void(*gb_fnDtor_t)(void *);

gb_bool_t gbIsHandleTypeValid(gb_handleType_t type);
gb_bool_t gbIsHandleValid(gb_handle_t handle);

gb_handleType_t gbExtendHandleType(const char *name, size_t n, gb_fnCtor_t ctor,
	gb_fnDtor_t dtor, gb_handleType_t base);
gb_handleType_t gbAddHandleType(const char *name, size_t n, gb_fnCtor_t ctor,
	gb_fnDtor_t dtor);

void gbGrabHandleType(gb_handleType_t type);
gb_handleType_t gbDropHandleType(gb_handleType_t type);

size_t gbGetDeadObjectLimit(gb_handleType_t type);
gb_bool_t gbDeadObjectVacancy(gb_handleType_t type);

void gbSetGlobalDeadObjectLimit(size_t limit);
size_t gbGetGlobalDeadObjectLimit();

void gbSetLocalDeadObjectLimit(gb_handleType_t type, size_t limit);
size_t gbGetLocalDeadObjectLimit(gb_handleType_t type);

size_t gbCountLiveHandles(gb_handleType_t type);
size_t gbCountDeadHandles(gb_handleType_t type);

gb_handle_t gbNewHandle(gb_handleType_t type);
gb_handle_t gbPurgeHandle(gb_handle_t handle);

gb_handleType_t gbHandleType(gb_handle_t handle);
gb_handleType_t gbHandleSuperType(gb_handle_t handle);
gb_handleType_t gbHandleBaseType(gb_handle_t handle);
gb_bool_t gbIsHandleDerived(gb_handle_t handle, gb_handleType_t from);

gb_handle_t gbHandleEx(void *p, gb_handleType_t type);
void *gbObjectEx(gb_handle_t handle, gb_handleType_t type);

gb_handle_t gbHandle(void *p);
void *gbObject(gb_handle_t handle);

void gbGrabHandle(gb_handle_t handle);
gb_handle_t gbDropHandle(gb_handle_t handle);

void gbSetHandleDebugName(gb_handle_t handle, const char *name);
const char *gbGetHandleDebugName(gb_handle_t handle);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
