#ifndef LIBRAYCAST_HEADER_INCLUDED
#define LIBRAYCAST_HEADER_INCLUDED

/*

	PUBLIC DOMAIN RAY CASTING LIBRARY

*/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef RC_FUNC
# define RC_FUNC		extern
#endif
#ifndef RC_CALL
# ifdef _WIN32
#  define RC_CALL		__stdcall
# else
#  define RC_CALL
# endif
#endif

typedef void			RC_VOID;

#ifdef _MSC_VER
typedef unsigned __int8	RC_UINT8;
typedef unsigned __int16 RC_UINT16;
typedef unsigned __int32 RC_UINT32;
typedef unsigned __Int64 RC_UINT64;
typedef signed __int8	RC_INT8;
typedef signed __int16	RC_INT16;
typedef signed __int32	RC_INT32;
typedef signed __int64	RC_INT64;
#else
typedef unsigned char	RC_UINT8;
typedef unsigned short	RC_UINT16;
typedef unsigned int	RC_UINT32;
typedef unsigned long long RC_UINT64;
typedef signed char		RC_INT8;
typedef signed short	RC_INT16;
typedef signed int		RC_INT32;
typedef signed long long RC_INT64;
#endif
typedef float			RC_FLOAT;

typedef enum RC_BOOL_
{
	RC_FALSE			= 0UL,
	RC_TRUE				= 1UL
} RC_BOOL;

typedef enum RC_RESULT_
{
	RC_SUCCESS			= 1,
	RC_FAILURE			= 0,

	RC_OUT_OF_MEMORY	= -1,
	RC_INVALID_PARMS	= -2,

	RC_STEP_FINISHED	= RC_SUCCESS,
	RC_STEP_WORKING		= RC_FAILURE
} RC_RESULT;

typedef enum RC_NODE_TYPE_
{
	RC_NODE_TYPE_NONE	= 0,

	RC_NODE_TYPE_MESH	= 1,
	RC_NODE_TYPE_SPHERE	= 2
} RC_NODE_TYPE;

typedef struct RC_VECTOR2_
{
	RC_FLOAT			x;
	RC_FLOAT			y;
} RC_VECTOR2;
typedef struct RC_VECTOR3_
{
	RC_FLOAT			x;
	RC_FLOAT			y;
	RC_FLOAT			z;
} RC_VECTOR3;
typedef struct RC_VECTOR4_
{
	RC_FLOAT			x;
	RC_FLOAT			y;
	RC_FLOAT			z;
	RC_FLOAT			w;
} RC_VECTOR4;
typedef struct RC_MATRIX3_
{
	RC_VECTOR3			x;
	RC_VECTOR3			y;
	RC_VECTOR3			z;
} RC_MATRIX3;
typedef struct RC_MATRIX4_
{
	RC_VECTOR4			x;
	RC_VECTOR4			y;
	RC_VECTOR4			z;
	RC_VECTOR4			w;
} RC_MATRIX4;

typedef struct RC_RECTANGLE_
{
	RC_UINT32			x1;
	RC_UINT32			y1;
	RC_UINT32			x2;
	RC_UINT32			y2;
} RC_RECTANGLE;

typedef struct RC_BOUNDS_
{
	RC_VECTOR3			lo;
	RC_VECTOR3			hi;
} RC_BOUNDS;

typedef struct RC_MESH_
{
	const RC_FLOAT *	vertexData;
	RC_UINT16			perVertexSize;
	RC_UINT16			vertexCount;

	const RC_UINT16 *	indexData;
	RC_UINT32			indexCount;
} RC_MESH;
typedef struct RC_SPHERE_
{
	RC_VECTOR3			position;
	RC_FLOAT			radius;
} RC_SPHERE;

typedef struct RC_NODE_INFO_
{
	RC_NODE_TYPE		type;
	RC_MATRIX4			transform;
	RC_BOUNDS			bounds;
	union
	{
		RC_VERTEX_INFO	mesh;
		RC_SPHERE		sphere;
	}					data;
	RC_UINT32			categoryMask;
	void *				customData;
} RC_NODE_INFO;

typedef struct RC_SCENE_INFO_
{
	RC_NODE_INFO **		nodes;
	RC_UINT32			nodeCount;
} RC_SCENE_INFO;

typedef struct RC_HIT_INFO_
{
	/* Node that was hit */
	const RC_NODE_INFO *node;
	/* Local-space contact position between the ray and node */
	RC_VECTOR3			contactPoint;
	/* Normal vector of the contact point */
	RC_VECTOR3			contactNormal;
	/* Exact coordinates on the triangle that was hit (for
	 * RC_NODE_TYPE_MESH nodes) */
	RC_VECTOR3			barycentricCoords;
	/* Position in the index buffer of the start of the triangle for
	 * RC_NODE_TYPE_MESH nodes */
	RC_UINT32			indexBufferTriangleBase;
} RC_HIT_INFO;

typedef struct RC_SCRATCH_MEMORY_
{
	/* Set this to the memory buffer that can be used */
	void *				memory;
	/* Size of the memory buffer, in bytes */
	RC_UINT32			sizeInBytes;

	/* Set this to zero. It represents the low-memory (growing upward) allocated */
	RC_UINT32			offsetLow;
	/* Set this to zero. It represents the high-memory (growing downward) allocated */
	RC_UINT32			offsetHigh;

	/* Set this to zero. It's used for thread-safety */
	RC_UINT32			allocLock;
} RC_SCRATCH_MEMORY;

typedef struct RC_RAY_INFO_
{
	/* Category bit-mask used to limit the objects considered. ANDing this with
	 * a node's categoryMask must match this mask for it to be considered. (That
	 * is, all of the bits specified in this categoryMask must be on in the
	 * node's categoryMask.) */
	RC_UINT32			categoryMask;
	/* Scene containing the nodes to test */
	const RC_SCENE_INFO *scene;

	/* Origin of the ray, in the scene's space. */
	RC_VECTOR3			start;
	/* Ending point of the ray, in the scene's space. */
	RC_VECTOR3			end;

	/* Size of the outHits array, in elements. */
	RC_UINT32			maxHits;
	/* Hit results from every node intersecting the array. */
	RC_HIT_INFO *		outHits;
	/* Set this to zero initially. It is the number of hits found. */
	RC_UINT32			numHits;

	/* Scratch memory buffer used for this ray. The memory given will only be
	 * used for the ray test. The memory should only be used with ONE RAY AT A
	 * TIME. */
	RC_SCRATCH_MEMORY	scratch;
} RC_RAY_INFO;

typedef struct RC_SCREEN_PICK_INFO_
{
	/* Position of the ray in screen-space (window coordinates). */
	RC_VECTOR2			position;
	/* Viewport of the rendering API. This is used to convert the screen-space
	 * position into a clip-space position. */
	RC_RECTANGLE		viewport;
	/* View inverse (camera) matrix */
	RC_MATRIX4			viewInverse;
	/* Projection inverse matrix */
	RC_MATRIX4			projectionInverse;
} RC_SCREEN_PICK_INFO;

/*
 * Sort the nodes in a scene from front-to-back based on the view-inverse
 * (camera) matrix.
 *
 * You don't need to call this function but doing so may improve performance by
 * keeping relevant data closer together in cache.
 *
 * Return Value
 * - RC_SUCCESS         Completed successfully.
 * - RC_INVALID_PARMS   One or more of the parameters was invalid.
 */
RC_FUNC RC_RESULT RC_CALL
	rcSortNodes
	(
		/* Scene containing the nodes to be sorted */
		RC_SCENE_INFO *				pInScene,
		/* View-inverse matrix representing the transformation to sort from */
		const RC_MATRIX4 *			pInViewInverse
	);
/*
 * Calculate the appropriate vectors for performing a pick from screen-space to
 * world-space.
 *
 * Return Value
 * - RC_SUCCESS         Completed successfully.
 * - RC_INVALID_PARMS   One or more of the parameters were invalid.
 */
RC_FUNC RC_RESULT RC_CALL
	rcCalcRayScreenVectors
	(
		RC_VECTOR3 *				pOutStart,
		RC_VECTOR3 *				pOutEnd,
		const RC_SCREEN_PICK_INFO *	pInScreenPick
	);
/*
 * Perform one iteration of the ray test with the given information.
 *
 * Return Value
 * - RC_STEP_FINISHED   There are no more iterations for this ray.
 * - RC_STEP_WORKING    An iteration has completed successfully, but processing
 *                      has not yet been completed. Further iterations will be
 *                      necessary.
 * - RC_INVALID_PARMS   pInoutRay is invalid.
 */
RC_FUNC RC_RESULT RC_CALL
	rcStepRay
	(
		RC_RAY_INFO *				pInoutRay
	);
/*
 * Completely perform a ray test with the given information.
 *
 * Return Value
 * - RC_SUCCESS         Completed successfully.
 * - RC_INVALID_PARMS   pInoutRay is invalid.
 */
RC_FUNC RC_RESULT RC_CALL
	rcCastRay
	(
		RC_RAY_INFO *				pInoutRay
	);

#ifdef __cplusplus
} //extern "C"
#endif

#endif
