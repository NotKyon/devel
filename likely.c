#define NUM_CALCS 256000
#define NUM_TESTS 40000000

#if _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#else
# include <time.h>
# include <sys/time.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#if _WIN32
# if _MSC_VER && _MSC_VER<1600
typedef   signed __int64 i64_t;
typedef unsigned __int64 u64_t;
# else
#  include <stdint.h>
typedef  int64_t i64_t;
typedef uint64_t u64_t;
# endif
#endif

#if __GNUC__ || __INTEL_COMPILER || __clang__
# define likely(x) __builtin_expect((x), 1)
# define unlikely(x) __builtin_expect((x), 0)
#else
# define likely(x) x
# define unlikely(x) x
#endif
#define alone(x) x

double Time() {
#if _WIN32
	u64_t t, f;

	QueryPerformanceCounter((LARGE_INTEGER *)&t);
	QueryPerformanceFrequency((LARGE_INTEGER *)&f);

	return ((double)t)/(double)f;
#else
	struct timespec ts;

	return ((double)ts.tv_sec) + ((double)ts.tv_nsec)/1000000000.0;
#endif
}

void Warmup() {
	/* increase processor cycles on purpose */
	volatile int x = 0;
	volatile int i;

	for(i=0; i<4096; i++)
		x += i*(x*x) - i*(x*x*x) + i*x - i*(x*i)*(x*x*i) + i*x*(-x*i);
}

int Redundancy(int x) {
	int r;

	r = (int)sqrtf((float)x);

	return r;
}

int g_results[NUM_CALCS];

#define CODE(test)\
static int didRun = 0;\
if (test(didRun))\
	return;\
\
{\
	int i;\
	\
	for(i=0; i<NUM_CALCS; i++)\
		g_results[i] = Redundancy(1000000 + i);\
}\
\
didRun = 1

void Test_Likely() {
	CODE(likely);
}
void Test_Unlikely() {
	CODE(unlikely);
}
void Test_Alone() {
	CODE(alone);
}

#define Report(x) Report_(#x, Test_##x)
void Report_(const char *name, void(*test)()) {
	double s, e;
	int i;

	printf("Running: %s...\n", name);fflush(stdout);

	s = Time();
	for(i=0; i<NUM_TESTS; i++)
		test();
	e = Time();

	printf("%f\n\n", e - s);
}

int main(int argc, char **argv) {
	int i;

	for(i=1; i<argc; i++) {
		printf("%s%s", argv[i], i + 1 < argc ? " - " : "\n\n");
	}

	Warmup();

#if _WIN32
	/*SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);*/
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
	SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
#endif

	Report(Likely);
	Report(Unlikely);
	Report(Alone);

	return EXIT_SUCCESS;
}
