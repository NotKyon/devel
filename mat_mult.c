typedef float vec4_t __attribute__((vector_size(16)));
typedef float mat4_t __attribute__((vector_size(64)));

/*void mat_mult(mat4_t *d, const mat4_t *a, const mat4_t *b) {*/
void mat_mult(vec4_t *d, const vec4_t *a, const vec4_t *b) {
	d[0][0] = a[0][0]*b[0][0] + a[1][0]*b[0][1] + a[2][0]*b[0][2] + a[3][0]*b[0][3];
	d[1][0] = a[0][0]*b[1][0] + a[1][0]*b[1][1] + a[2][0]*b[1][2] + a[3][0]*b[1][3];
	d[2][0] = a[0][0]*b[2][0] + a[1][0]*b[2][1] + a[2][0]*b[2][2] + a[3][0]*b[2][3];
	d[3][0] = a[0][0]*b[3][0] + a[1][0]*b[3][1] + a[2][0]*b[3][2] + a[3][0]*b[3][3];

	d[0][1] = a[0][1]*b[0][0] + a[1][1]*b[0][1] + a[2][1]*b[0][2] + a[3][1]*b[0][3];
	d[1][1] = a[0][1]*b[1][0] + a[1][1]*b[1][1] + a[2][1]*b[1][2] + a[3][1]*b[1][3];
	d[2][1] = a[0][1]*b[2][0] + a[1][1]*b[2][1] + a[2][1]*b[2][2] + a[3][1]*b[2][3];
	d[3][1] = a[0][1]*b[3][0] + a[1][1]*b[3][1] + a[2][1]*b[3][2] + a[3][1]*b[3][3];

	d[0][2] = a[0][2]*b[0][0] + a[1][2]*b[0][1] + a[2][2]*b[0][2] + a[3][2]*b[0][3];
	d[1][2] = a[0][2]*b[1][0] + a[1][2]*b[1][1] + a[2][2]*b[1][2] + a[3][2]*b[1][3];
	d[2][2] = a[0][2]*b[2][0] + a[1][2]*b[2][1] + a[2][2]*b[2][2] + a[3][2]*b[2][3];
	d[3][2] = a[0][2]*b[3][0] + a[1][2]*b[3][1] + a[2][2]*b[3][2] + a[3][2]*b[3][3];

	d[0][3] = a[0][3]*b[0][0] + a[1][3]*b[0][1] + a[2][3]*b[0][2] + a[3][3]*b[0][3];
	d[1][3] = a[0][3]*b[1][0] + a[1][3]*b[1][1] + a[2][3]*b[1][2] + a[3][3]*b[1][3];
	d[2][3] = a[0][3]*b[2][0] + a[1][3]*b[2][1] + a[2][3]*b[2][2] + a[3][3]*b[2][3];
	d[3][3] = a[0][3]*b[3][0] + a[1][3]*b[3][1] + a[2][3]*b[3][2] + a[3][3]*b[3][3];
}

/*
 * GCC's output: -O3 -fomit-frame-pointer -msse -msse2 -masm=intel -S
	.file	"mat_mult.c"
	.intel_syntax noprefix
	.text
	.p2align 4,,15
	.globl	mat_mult
	.type	mat_mult, @function
mat_mult:
.LFB0:
	.cfi_startproc
	mov	edx, DWORD PTR [esp+8]
	mov	eax, DWORD PTR [esp+12]
	mov	ecx, DWORD PTR [esp+4]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+64]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+128]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+192]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+64]
	fld	DWORD PTR [edx+64]
	fmul	DWORD PTR [eax+68]
	faddp	st(1), st
	fld	DWORD PTR [edx+128]
	fmul	DWORD PTR [eax+72]
	faddp	st(1), st
	fld	DWORD PTR [edx+192]
	fmul	DWORD PTR [eax+76]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+64]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+128]
	fld	DWORD PTR [edx+64]
	fmul	DWORD PTR [eax+132]
	faddp	st(1), st
	fld	DWORD PTR [edx+128]
	fmul	DWORD PTR [eax+136]
	faddp	st(1), st
	fld	DWORD PTR [edx+192]
	fmul	DWORD PTR [eax+140]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+128]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+192]
	fld	DWORD PTR [edx+64]
	fmul	DWORD PTR [eax+196]
	faddp	st(1), st
	fld	DWORD PTR [edx+128]
	fmul	DWORD PTR [eax+200]
	faddp	st(1), st
	fld	DWORD PTR [edx+192]
	fmul	DWORD PTR [eax+204]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+192]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+68]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+132]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+196]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+4]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+64]
	fld	DWORD PTR [edx+68]
	fmul	DWORD PTR [eax+68]
	faddp	st(1), st
	fld	DWORD PTR [edx+132]
	fmul	DWORD PTR [eax+72]
	faddp	st(1), st
	fld	DWORD PTR [edx+196]
	fmul	DWORD PTR [eax+76]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+68]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+128]
	fld	DWORD PTR [edx+68]
	fmul	DWORD PTR [eax+132]
	faddp	st(1), st
	fld	DWORD PTR [edx+132]
	fmul	DWORD PTR [eax+136]
	faddp	st(1), st
	fld	DWORD PTR [edx+196]
	fmul	DWORD PTR [eax+140]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+132]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+192]
	fld	DWORD PTR [edx+68]
	fmul	DWORD PTR [eax+196]
	faddp	st(1), st
	fld	DWORD PTR [edx+132]
	fmul	DWORD PTR [eax+200]
	faddp	st(1), st
	fld	DWORD PTR [edx+196]
	fmul	DWORD PTR [eax+204]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+196]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+72]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+136]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+200]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+8]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+64]
	fld	DWORD PTR [edx+72]
	fmul	DWORD PTR [eax+68]
	faddp	st(1), st
	fld	DWORD PTR [edx+136]
	fmul	DWORD PTR [eax+72]
	faddp	st(1), st
	fld	DWORD PTR [edx+200]
	fmul	DWORD PTR [eax+76]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+72]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+128]
	fld	DWORD PTR [edx+72]
	fmul	DWORD PTR [eax+132]
	faddp	st(1), st
	fld	DWORD PTR [edx+136]
	fmul	DWORD PTR [eax+136]
	faddp	st(1), st
	fld	DWORD PTR [edx+200]
	fmul	DWORD PTR [eax+140]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+136]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+192]
	fld	DWORD PTR [edx+72]
	fmul	DWORD PTR [eax+196]
	faddp	st(1), st
	fld	DWORD PTR [edx+136]
	fmul	DWORD PTR [eax+200]
	faddp	st(1), st
	fld	DWORD PTR [edx+200]
	fmul	DWORD PTR [eax+204]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+200]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+76]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+140]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+204]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+12]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax+64]
	fld	DWORD PTR [edx+76]
	fmul	DWORD PTR [eax+68]
	faddp	st(1), st
	fld	DWORD PTR [edx+140]
	fmul	DWORD PTR [eax+72]
	faddp	st(1), st
	fld	DWORD PTR [edx+204]
	fmul	DWORD PTR [eax+76]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+76]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax+128]
	fld	DWORD PTR [edx+76]
	fmul	DWORD PTR [eax+132]
	faddp	st(1), st
	fld	DWORD PTR [edx+140]
	fmul	DWORD PTR [eax+136]
	faddp	st(1), st
	fld	DWORD PTR [edx+204]
	fmul	DWORD PTR [eax+140]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+140]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax+192]
	fld	DWORD PTR [edx+76]
	fmul	DWORD PTR [eax+196]
	faddp	st(1), st
	fld	DWORD PTR [edx+140]
	fmul	DWORD PTR [eax+200]
	faddp	st(1), st
	fld	DWORD PTR [edx+204]
	fmul	DWORD PTR [eax+204]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+204]
	ret
	.cfi_endproc
.LFE0:
	.size	mat_mult, .-mat_mult
	.ident	"GCC: (Ubuntu/Linaro 4.6.3-1ubuntu2) 4.6.3"
	.section	.note.GNU-stack,"",@progbits
*/



/* -------------------------------------------------------------------------- */



/*
 * Clang's output: -O3 -fomit-frame-pointer -msse -msse2
	.file	"mat_mult.c"
	.text
	.globl	mat_mult
	.align	16, 0x90
	.type	mat_mult,@function
mat_mult:                               # @mat_mult
# BB#0:
	subl	$204, %esp
	movl	216(%esp), %edx
	movdqa	(%edx), %xmm1
	movl	208(%esp), %eax
	movaps	32(%eax), %xmm2
	movaps	%xmm2, 160(%esp)        # 16-byte Spill
	movaps	48(%eax), %xmm4
	movaps	%xmm4, 176(%esp)        # 16-byte Spill
	movaps	(%eax), %xmm0
	movaps	16(%eax), %xmm3
	movaps	%xmm3, (%esp)           # 16-byte Spill
	movaps	%xmm3, 16(%eax)
	movaps	%xmm4, 48(%eax)
	movaps	%xmm2, 32(%eax)
	movl	212(%esp), %ecx
	movss	(%ecx), %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$1, %xmm1, %xmm3        # xmm3 = xmm1[1,0,0,0]
	mulss	64(%ecx), %xmm3
	addss	%xmm2, %xmm3
	pshufd	$3, %xmm1, %xmm2        # xmm2 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	128(%ecx), %xmm1
	addss	%xmm3, %xmm1
	mulss	192(%ecx), %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, %xmm0
	movaps	%xmm0, (%eax)
	movdqa	64(%edx), %xmm1
	movaps	96(%eax), %xmm2
	movaps	%xmm2, 112(%esp)        # 16-byte Spill
	movaps	112(%eax), %xmm4
	movaps	%xmm4, 128(%esp)        # 16-byte Spill
	movaps	64(%eax), %xmm6
	movaps	80(%eax), %xmm3
	movaps	%xmm3, 16(%esp)         # 16-byte Spill
	movaps	%xmm3, 80(%eax)
	movaps	%xmm4, 112(%eax)
	movaps	%xmm2, 96(%eax)
	movss	(%ecx), %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$1, %xmm1, %xmm3        # xmm3 = xmm1[1,0,0,0]
	mulss	64(%ecx), %xmm3
	addss	%xmm2, %xmm3
	pshufd	$3, %xmm1, %xmm2        # xmm2 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	128(%ecx), %xmm1
	addss	%xmm3, %xmm1
	mulss	192(%ecx), %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, %xmm6
	movaps	%xmm6, 64(%eax)
	movdqa	128(%edx), %xmm2
	movaps	160(%eax), %xmm3
	movaps	%xmm3, 64(%esp)         # 16-byte Spill
	movaps	176(%eax), %xmm5
	movaps	%xmm5, 80(%esp)         # 16-byte Spill
	movaps	128(%eax), %xmm1
	movaps	144(%eax), %xmm4
	movaps	%xmm4, 144(%esp)        # 16-byte Spill
	movaps	%xmm4, 144(%eax)
	movaps	%xmm5, 176(%eax)
	movaps	%xmm3, 160(%eax)
	movss	(%ecx), %xmm3
	mulss	%xmm2, %xmm3
	pshufd	$1, %xmm2, %xmm4        # xmm4 = xmm2[1,0,0,0]
	mulss	64(%ecx), %xmm4
	addss	%xmm3, %xmm4
	pshufd	$3, %xmm2, %xmm3        # xmm3 = xmm2[3,0,0,0]
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	mulss	128(%ecx), %xmm2
	addss	%xmm4, %xmm2
	mulss	192(%ecx), %xmm3
	addss	%xmm2, %xmm3
	movss	%xmm3, %xmm1
	movaps	%xmm1, 128(%eax)
	movdqa	192(%edx), %xmm4
	movaps	224(%eax), %xmm2
	movaps	%xmm2, 32(%esp)         # 16-byte Spill
	movaps	240(%eax), %xmm5
	movaps	%xmm5, 48(%esp)         # 16-byte Spill
	movaps	192(%eax), %xmm7
	movaps	208(%eax), %xmm3
	movaps	%xmm3, 96(%esp)         # 16-byte Spill
	movaps	%xmm3, 208(%eax)
	movaps	%xmm5, 240(%eax)
	movaps	%xmm2, 224(%eax)
	movss	(%ecx), %xmm5
	mulss	%xmm4, %xmm5
	pshufd	$1, %xmm4, %xmm2        # xmm2 = xmm4[1,0,0,0]
	mulss	64(%ecx), %xmm2
	addss	%xmm5, %xmm2
	pshufd	$3, %xmm4, %xmm5        # xmm5 = xmm4[3,0,0,0]
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	mulss	128(%ecx), %xmm4
	addss	%xmm2, %xmm4
	mulss	192(%ecx), %xmm5
	addss	%xmm4, %xmm5
	movss	%xmm5, %xmm7
	movaps	%xmm7, 192(%eax)
	movdqa	(%edx), %xmm5
	movaps	176(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 48(%eax)
	movaps	160(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 32(%eax)
	movaps	(%esp), %xmm3           # 16-byte Reload
	movaps	%xmm3, 16(%eax)
	movss	4(%ecx), %xmm4
	mulss	%xmm5, %xmm4
	pshufd	$1, %xmm5, %xmm2        # xmm2 = xmm5[1,0,0,0]
	mulss	68(%ecx), %xmm2
	addss	%xmm4, %xmm2
	pshufd	$3, %xmm5, %xmm4        # xmm4 = xmm5[3,0,0,0]
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	mulss	132(%ecx), %xmm5
	addss	%xmm2, %xmm5
	mulss	196(%ecx), %xmm4
	addss	%xmm5, %xmm4
	movlhps	%xmm0, %xmm4            # xmm4 = xmm4[0],xmm0[0]
	shufps	$-30, %xmm0, %xmm4      # xmm4 = xmm4[2,0],xmm0[2,3]
	movaps	%xmm4, (%eax)
	movdqa	64(%edx), %xmm0
	movaps	128(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 112(%eax)
	movaps	112(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 96(%eax)
	movaps	16(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 80(%eax)
	movss	4(%ecx), %xmm5
	mulss	%xmm0, %xmm5
	pshufd	$1, %xmm0, %xmm2        # xmm2 = xmm0[1,0,0,0]
	mulss	68(%ecx), %xmm2
	addss	%xmm5, %xmm2
	pshufd	$3, %xmm0, %xmm5        # xmm5 = xmm0[3,0,0,0]
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	mulss	132(%ecx), %xmm0
	addss	%xmm2, %xmm0
	mulss	196(%ecx), %xmm5
	addss	%xmm0, %xmm5
	movlhps	%xmm6, %xmm5            # xmm5 = xmm5[0],xmm6[0]
	shufps	$-30, %xmm6, %xmm5      # xmm5 = xmm5[2,0],xmm6[2,3]
	movaps	%xmm5, 64(%eax)
	movdqa	128(%edx), %xmm0
	movaps	80(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 176(%eax)
	movaps	64(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 160(%eax)
	movaps	144(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 144(%eax)
	movss	4(%ecx), %xmm6
	mulss	%xmm0, %xmm6
	pshufd	$1, %xmm0, %xmm2        # xmm2 = xmm0[1,0,0,0]
	mulss	68(%ecx), %xmm2
	addss	%xmm6, %xmm2
	pshufd	$3, %xmm0, %xmm6        # xmm6 = xmm0[3,0,0,0]
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	mulss	132(%ecx), %xmm0
	addss	%xmm2, %xmm0
	mulss	196(%ecx), %xmm6
	addss	%xmm0, %xmm6
	movlhps	%xmm1, %xmm6            # xmm6 = xmm6[0],xmm1[0]
	shufps	$-30, %xmm1, %xmm6      # xmm6 = xmm6[2,0],xmm1[2,3]
	movaps	%xmm6, 128(%eax)
	movdqa	192(%edx), %xmm1
	movaps	48(%esp), %xmm0         # 16-byte Reload
	movaps	%xmm0, 240(%eax)
	movaps	32(%esp), %xmm0         # 16-byte Reload
	movaps	%xmm0, 224(%eax)
	movaps	96(%esp), %xmm0         # 16-byte Reload
	movaps	%xmm0, 208(%eax)
	movss	4(%ecx), %xmm0
	mulss	%xmm1, %xmm0
	pshufd	$1, %xmm1, %xmm2        # xmm2 = xmm1[1,0,0,0]
	mulss	68(%ecx), %xmm2
	addss	%xmm0, %xmm2
	pshufd	$3, %xmm1, %xmm0        # xmm0 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	132(%ecx), %xmm1
	addss	%xmm2, %xmm1
	mulss	196(%ecx), %xmm0
	addss	%xmm1, %xmm0
	movlhps	%xmm7, %xmm0            # xmm0 = xmm0[0],xmm7[0]
	shufps	$-30, %xmm7, %xmm0      # xmm0 = xmm0[2,0],xmm7[2,3]
	movaps	%xmm0, 192(%eax)
	movdqa	(%edx), %xmm1
	movaps	176(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 48(%eax)
	movaps	160(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 32(%eax)
	movaps	%xmm3, 16(%eax)
	movss	8(%ecx), %xmm7
	mulss	%xmm1, %xmm7
	pshufd	$1, %xmm1, %xmm2        # xmm2 = xmm1[1,0,0,0]
	mulss	72(%ecx), %xmm2
	addss	%xmm7, %xmm2
	pshufd	$3, %xmm1, %xmm7        # xmm7 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	136(%ecx), %xmm1
	addss	%xmm2, %xmm1
	mulss	200(%ecx), %xmm7
	addss	%xmm1, %xmm7
	shufps	$48, %xmm4, %xmm7       # xmm7 = xmm7[0,0],xmm4[3,0]
	shufps	$-124, %xmm7, %xmm4     # xmm4 = xmm4[0,1],xmm7[0,2]
	movaps	%xmm4, (%eax)
	movdqa	64(%edx), %xmm1
	movaps	128(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 112(%eax)
	movaps	112(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 96(%eax)
	movaps	16(%esp), %xmm3         # 16-byte Reload
	movaps	%xmm3, 80(%eax)
	movss	8(%ecx), %xmm7
	mulss	%xmm1, %xmm7
	pshufd	$1, %xmm1, %xmm2        # xmm2 = xmm1[1,0,0,0]
	mulss	72(%ecx), %xmm2
	addss	%xmm7, %xmm2
	pshufd	$3, %xmm1, %xmm7        # xmm7 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	136(%ecx), %xmm1
	addss	%xmm2, %xmm1
	mulss	200(%ecx), %xmm7
	addss	%xmm1, %xmm7
	shufps	$48, %xmm5, %xmm7       # xmm7 = xmm7[0,0],xmm5[3,0]
	shufps	$-124, %xmm7, %xmm5     # xmm5 = xmm5[0,1],xmm7[0,2]
	movaps	%xmm5, 64(%eax)
	movdqa	128(%edx), %xmm1
	movaps	80(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 176(%eax)
	movaps	64(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 160(%eax)
	movaps	144(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 144(%eax)
	movss	8(%ecx), %xmm7
	mulss	%xmm1, %xmm7
	pshufd	$1, %xmm1, %xmm2        # xmm2 = xmm1[1,0,0,0]
	mulss	72(%ecx), %xmm2
	addss	%xmm7, %xmm2
	pshufd	$3, %xmm1, %xmm7        # xmm7 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	136(%ecx), %xmm1
	addss	%xmm2, %xmm1
	mulss	200(%ecx), %xmm7
	addss	%xmm1, %xmm7
	shufps	$48, %xmm6, %xmm7       # xmm7 = xmm7[0,0],xmm6[3,0]
	shufps	$-124, %xmm7, %xmm6     # xmm6 = xmm6[0,1],xmm7[0,2]
	movaps	%xmm6, 128(%eax)
	movdqa	192(%edx), %xmm1
	movaps	48(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 240(%eax)
	movaps	32(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 224(%eax)
	movaps	96(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 208(%eax)
	movss	8(%ecx), %xmm7
	mulss	%xmm1, %xmm7
	pshufd	$1, %xmm1, %xmm2        # xmm2 = xmm1[1,0,0,0]
	mulss	72(%ecx), %xmm2
	addss	%xmm7, %xmm2
	pshufd	$3, %xmm1, %xmm7        # xmm7 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	136(%ecx), %xmm1
	addss	%xmm2, %xmm1
	mulss	200(%ecx), %xmm7
	addss	%xmm1, %xmm7
	shufps	$48, %xmm0, %xmm7       # xmm7 = xmm7[0,0],xmm0[3,0]
	shufps	$-124, %xmm7, %xmm0     # xmm0 = xmm0[0,1],xmm7[0,2]
	movaps	%xmm0, 192(%eax)
	movdqa	(%edx), %xmm1
	movaps	176(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 48(%eax)
	movaps	160(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 32(%eax)
	movaps	(%esp), %xmm2           # 16-byte Reload
	movaps	%xmm2, 16(%eax)
	movss	12(%ecx), %xmm7
	mulss	%xmm1, %xmm7
	pshufd	$1, %xmm1, %xmm2        # xmm2 = xmm1[1,0,0,0]
	mulss	76(%ecx), %xmm2
	addss	%xmm7, %xmm2
	pshufd	$3, %xmm1, %xmm7        # xmm7 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	140(%ecx), %xmm1
	addss	%xmm2, %xmm1
	mulss	204(%ecx), %xmm7
	addss	%xmm1, %xmm7
	movaps	%xmm4, %xmm1
	movss	%xmm7, %xmm1
	shufps	$36, %xmm1, %xmm4       # xmm4 = xmm4[0,1],xmm1[2,0]
	movaps	%xmm4, (%eax)
	movdqa	64(%edx), %xmm1
	movaps	128(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 112(%eax)
	movaps	112(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 96(%eax)
	movaps	%xmm3, 80(%eax)
	movss	12(%ecx), %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$1, %xmm1, %xmm7        # xmm7 = xmm1[1,0,0,0]
	mulss	76(%ecx), %xmm7
	addss	%xmm2, %xmm7
	pshufd	$3, %xmm1, %xmm4        # xmm4 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	140(%ecx), %xmm1
	addss	%xmm7, %xmm1
	mulss	204(%ecx), %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm5, %xmm1
	movss	%xmm4, %xmm1
	shufps	$36, %xmm1, %xmm5       # xmm5 = xmm5[0,1],xmm1[2,0]
	movaps	%xmm5, 64(%eax)
	movdqa	128(%edx), %xmm1
	movaps	80(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 176(%eax)
	movaps	64(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 160(%eax)
	movaps	144(%esp), %xmm2        # 16-byte Reload
	movaps	%xmm2, 144(%eax)
	movss	12(%ecx), %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$1, %xmm1, %xmm5        # xmm5 = xmm1[1,0,0,0]
	mulss	76(%ecx), %xmm5
	addss	%xmm2, %xmm5
	pshufd	$3, %xmm1, %xmm4        # xmm4 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	140(%ecx), %xmm1
	addss	%xmm5, %xmm1
	mulss	204(%ecx), %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm6, %xmm1
	movss	%xmm4, %xmm1
	shufps	$36, %xmm1, %xmm6       # xmm6 = xmm6[0,1],xmm1[2,0]
	movaps	%xmm6, 128(%eax)
	movdqa	192(%edx), %xmm1
	movaps	48(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 240(%eax)
	movaps	32(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 224(%eax)
	movaps	96(%esp), %xmm2         # 16-byte Reload
	movaps	%xmm2, 208(%eax)
	movss	12(%ecx), %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$1, %xmm1, %xmm3        # xmm3 = xmm1[1,0,0,0]
	mulss	76(%ecx), %xmm3
	addss	%xmm2, %xmm3
	pshufd	$3, %xmm1, %xmm2        # xmm2 = xmm1[3,0,0,0]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulss	140(%ecx), %xmm1
	addss	%xmm3, %xmm1
	mulss	204(%ecx), %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm1
	movss	%xmm2, %xmm1
	shufps	$36, %xmm1, %xmm0       # xmm0 = xmm0[0,1],xmm1[2,0]
	movaps	%xmm0, 192(%eax)
	addl	$204, %esp
	ret
.Ltmp0:
	.size	mat_mult, .Ltmp0-mat_mult


	.section	".note.GNU-stack","",@progbits
*/
