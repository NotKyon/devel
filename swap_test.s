	.file	"swap_test.c"
	.intel_syntax noprefix
	.text
	.p2align 2,,3
	.globl	_xor_swap
	.def	_xor_swap;	.scl	2;	.type	32;	.endef
_xor_swap:
LFB0:
	.cfi_startproc
	mov	eax, DWORD PTR [esp+4]
	mov	ecx, DWORD PTR [esp+8]
	mov	edx, DWORD PTR [ecx]
	xor	edx, DWORD PTR [eax]
	mov	DWORD PTR [eax], edx
	xor	edx, DWORD PTR [ecx]
	mov	DWORD PTR [ecx], edx
	xor	DWORD PTR [eax], edx
	ret
	.cfi_endproc
LFE0:
	.p2align 2,,3
	.globl	_nrm_swap
	.def	_nrm_swap;	.scl	2;	.type	32;	.endef
_nrm_swap:
LFB1:
	.cfi_startproc
	push	ebx
	.cfi_def_cfa_offset 8
	.cfi_offset 3, -8
	mov	edx, DWORD PTR [esp+8]
	mov	eax, DWORD PTR [esp+12]
	mov	ecx, DWORD PTR [edx]
	mov	ebx, DWORD PTR [eax]
	mov	DWORD PTR [edx], ebx
	mov	DWORD PTR [eax], ecx
	pop	ebx
	.cfi_def_cfa_offset 4
	.cfi_restore 3
	ret
	.cfi_endproc
LFE1:
