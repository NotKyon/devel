﻿#if defined( _WIN32 )
# include <windows.h>
#else
# include <time.h>
#endif
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#if !defined( __clang__ )
# include <stdexcept>
#else
namespace std
{

	class string
	{
	public:
		string() { mBuffer[ 0 ] = '\0'; }
		~string() {}

		string( const char *p ) { assign( p ); }
		string( const string &s ) { assign( s.c_str() ); }

		const char *c_str() const { return mBuffer; }

		string &assign( const char *p )
		{
			const char *const base = p;
			while( *p != '\0' )
			{
				mBuffer[ p - base ] = *p;
				++p;

				if( ( size_t )( p - base ) == sizeof( mBuffer ) - 1 )
				{
					break;
				}
			}

			mBuffer[ p - base ] = '\0';
			return *this;
		}
		string &append( const char *p )
		{
			char *s = mBuffer;
			while( *s != '\0' )
			{
				++s;
			}

			size_t n = ( sizeof( mBuffer ) - ( s - mBuffer ) ) - 1;

			const char *const base = p;
			while( *p != '\0' && n > 0 )
			{
				s[ p - base ] = *p;
				++p;
				--n;
			}

			s[ p - base ] = '\0';
			return *this;
		}

		string &assign( const string &s ) { return assign( s.c_str() ); }
		string &append( const string &s ) { return append( s.c_str() ); }

		string &operator=( const char *p ) { return assign( p ); }
		string &operator=( const string &s ) { return assign( s ); }
		string &operator+=( const char *p ) { return append( p ); }
		string &operator+=( const string &s ) { return append( s ); }

		string operator+( const char *p )
		{
			return string( *this ).append( p );
		}
		string operator+( const string &s )
		{
			return string( *this ).append( s );
		}

	private:
		char mBuffer[ 256 ];
	};

	class exception
	{
	public:
		exception( const string &x ): mWhat( x ) {}
		exception( const char *x ): mWhat( x ) {}
		~exception() {}

		const char *what() const
		{
			return mWhat.c_str();
		}

	private:
		string mWhat;
	};
	class runtime_error: public exception
	{
	public:
		runtime_error( const string &x ): exception( x ) {}
		runtime_error( const char *x ): exception( x ) {}
		~runtime_error() {}
	};

}
#endif

typedef uint64_t u64;
typedef double f64;

#if defined( _WIN32 )
# include <Windows.h>

namespace sdf
{

	namespace detail
	{

		inline u64 ConvertResolution( u64 t, u64 f, u64 r )
		{
			return t*r/f;
		}
		inline u64 QueryFrequency()
		{
			LARGE_INTEGER f;

			if( !QueryPerformanceFrequency( &f ) )
			{
				f.QuadPart = 1;
			}

			return ( u64 )f.QuadPart;
		}

	}

	inline u64 query_microseconds()
	{
		static const u64 f = detail::QueryFrequency();
		u64 t;

		QueryPerformanceCounter( ( LARGE_INTEGER * )&t );

		return detail::ConvertResolution( t, f, 1000000 );
	}

}
#else
# error FIXME: Need timing stuff for this platform
#endif

namespace sdf
{

	inline f64 microseconds_to_seconds( u64 microsec )
	{
		return f64( microsec )/1000000.0;
	}

	class timer
	{
	public:
		inline timer()
		{
			reset();
		}
		inline ~timer()
		{
		}

		inline void reset()
		{
			mBase = query_microseconds();
		}

		inline f64 get_elapsed_seconds() const
		{
			return microseconds_to_seconds( get_elapsed() );
		}
		inline u64 get_elapsed() const
		{
			return query_microseconds() - mBase;
		}
		
		inline u64 operator()() const
		{
			return get_elapsed();
		}

	private:
		u64 mBase;
	};

}

bool prg_doSomething() {
	volatile int i = 0;
	return ( i == 0 );
}

void prg_test1() {
	if( !prg_doSomething() ) {
		fprintf( stderr, "ERROR\n" );
		return;
	}

	printf( "" );
}
void prg_test2() {
	try {
		if( !prg_doSomething() ) {
			throw std::runtime_error( "ERROR" );
		}

		printf( "" );
	} catch( std::exception &except ) {
		fprintf( stderr, "%s\n", except.what() );
		return;
	}
}
void prg_test3() {
	if( !prg_doSomething() ) {
		throw std::runtime_error( "ERROR" );
	}

	printf( "" );
}

f64 prg_sampleFunction( void(*func)() ) {
	static const unsigned int numSamples = 1<<28;

	sdf::timer t;
	for( unsigned int i = 0; i < numSamples; ++i ) {
		func();
	}

	return t.get_elapsed_seconds();
}

int main() {
	f64 method1 = prg_sampleFunction( &prg_test1 );
	f64 method2 = prg_sampleFunction( &prg_test2 );
	f64 method3 = 0;
	try {
		method3 = prg_sampleFunction( &prg_test3 );
	} catch( std::exception &except ) {
		fprintf( stderr, "%s\n", except.what() );
	}

	printf(
		"cerrors    : %.8f\n"
		"exceptions : %.8f\n"
		"exceptions2: %.8f\n",
		method1, method2, method3 );

	return EXIT_SUCCESS;
}
