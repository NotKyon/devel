%{

#include "declconv.h"

extern FILE *yyin;

struct Lib	*lib = (struct Lib *)0;
struct Arg	*arg_stack[MAX_ARGS];
int			arg_sp = 0;

void yyerror(const char *message)
{
	error("%s", message);
	exit(1);
}

int yywrap(void)
{
	return 1;
}

/*
static char *_stradd(char *dst, const char *src)
{
	size_t	n, i;

	n = 0;
	while(src[n])
		n++;
	for(i=0; i<n; i++)
		dst[i] = src[i];

	dst[i] = 0;
	return &dst[i];
}
*/
static const char *_cescape(const char *input)
{
	static char	buff[8192];
	char		*ptr;
	int			i;

	buff[0] = 0;
	ptr = &buff[0];
	for(i=0; input[i]; i++)
	{
		if (input[i]=='\\')
			*ptr++ = '\\';
		*ptr++ = input[i];
	}

	return &buff[0];
}
static const char *_cfuncname(const char *filename)
{
	static char	buff[8192];
	char		*ptr;
	int			i;
	int			capitalize;

	ptr = &buff[0];
	*ptr = 0;
	capitalize = 1;
	for(i=0; i<filename[i]; i++)
	{
		if (filename[i]=='.')
			break;
		switch(filename[i])
		{
			case '`':
			case '~':
			case '!':
			case '@':
			case '#':
			case '$':
			case '%':
			case '^':
			case '&':
			case '*':
			case '(':
			case ')':
			case '-':
			case '=':
			case '+':
			case '[':
			case ']':
			case '{':
			case '}':
			case '\\':
			case '|':
			case ';':
			case ':':
			case '\'':
			case '\"':
			case ',':
			case '/':
			case '<':
			case '>':
			case '?':
				*ptr++ = '_';
				break;
			case ' ':
			case '\t':
				capitalize = 1;
				break;
			default:
				if (capitalize)
				{
					if (filename[i]>='a' && filename[i]<='z')
						*ptr++ = filename[i]-'a'+'A';
					else
						*ptr++ = filename[i];
					capitalize = 0;
				}
				else
					*ptr++ = filename[i];
				break;
		}
	}
	*ptr = 0;

	return &buff[0];
}
static const char *_cincguardname(const char *filename)
{
	static char	buff[8192];
	char		*ptr;
	int			i;

	ptr = &buff[0];
	*ptr++ = '_';
	for(i=0; filename[i]; i++)
	{
		char	c;
		c = filename[i];
		if (c>='a'&&c<='z')
			c = c-'a'+'A';
		switch(c)
		{
			case '`':
			case '~':
			case '!':
			case '@':
			case '#':
			case '$':
			case '%':
			case '^':
			case '&':
			case '*':
			case '(':
			case ')':
			case '-':
			case '=':
			case '+':
			case '[':
			case ']':
			case '{':
			case '}':
			case '\\':
			case '|':
			case ';':
			case ':':
			case '\'':
			case '\"':
			case ',':
			case '.':
			case '/':
			case '<':
			case '>':
			case '?':
			case ' ':
			case '\t':
				*ptr++ = '_';
				break;
			default:
				*ptr++ = c;
				break;
		}
	}
	*ptr++ = '_';
	*ptr = 0;

	return &buff[0];
}

static void write_header_nb(FILE *outfile, const char *srcname)
{
	fprintf(outfile, "; Nuclear Basic Plugin Loader File\n");
	fprintf(outfile, "; Converted by declconv from \"%s\"\n", srcname);
	fprintf(outfile, "; declconv was written by Aaron J. Miller <nocannedmeat@gmail.com> on June 7th, 2011.\n\n");
}
static void write_consts_nb(FILE *outfile)
{
	struct Constant	*cnst;

	fprintf(outfile, ";constants\n");

	for(cnst=lib->first_cnst; cnst; cnst=cnst->next)
		fprintf(outfile, "Const %s=%s\n", cnst->name, cnst->value);

	fprintf(outfile, "\n");
}
static void write_args_nb(FILE *outfile, struct Func *func)
{
	struct Arg	*arg;

	for(arg=func->first_arg; arg; arg=arg->next)
	{
		if (arg != func->first_arg)
			fprintf(outfile, ", ");
		if (arg->flags&ARG_REF_BIT)
			fprintf(outfile, "Ref ");
		if (arg->type)
		{
			if (arg->type=='$')
				fprintf(outfile, "Strict %s%c", arg->name, arg->type);
			else if(arg->type=='*')
				fprintf(outfile, "%s%%", arg->name);
			else
				fprintf(outfile, "%s%c", arg->name, arg->type);
		}
		else
			fprintf(outfile, "%s", arg->name);
		if (arg->flags&ARG_DEF_BIT)
		{
			if (arg->type=='$')
				fprintf(outfile, "=\"%s\"", arg->def);
			else
				fprintf(outfile, "=%s", arg->def);
		}
	}
}

static void write_header_c(FILE *outfile, const char *srcname)
{
	const char	*incguard;

	incguard = _cincguardname(srcname);

	fprintf(outfile, "/*\n");
	fprintf(outfile, " * Nuclear Fusion Plugin Loader File\n");
	fprintf(outfile, " * Converted by declconv from \"%s\"\n", srcname);
	fprintf(outfile, " * declconv was written by Aaron J. Miller <nocannedmeat@gmail.com> on June 7th, 2011.\n");
	fprintf(outfile, " */\n\n");
	fprintf(outfile, "#ifndef %s\n", incguard);
	fprintf(outfile, "#define %s\n\n", incguard);
	fprintf(outfile, "/* function prototype macros */\n");
	fprintf(outfile, "#if defined(__cplusplus)||defined(cplusplus)\n");
	fprintf(outfile, "# define __D(v) =v\n");
	fprintf(outfile, "# ifdef NF_RUN_MODE\n");
	fprintf(outfile, "#  define __E extern \"C\"\n");
	fprintf(outfile, "# else\n");
	fprintf(outfile, "#  define __E extern \"C\" extern\n");
	fprintf(outfile, "# endif\n");
	fprintf(outfile, "#else\n");
	fprintf(outfile, "# define __D(v) /* empty */\n");
	fprintf(outfile, "# ifdef NF_RUN_MODE\n");
	fprintf(outfile, "#  define __E /* empty */\n");
	fprintf(outfile, "# else\n");
	fprintf(outfile, "#  define __E extern\n");
	fprintf(outfile, "# endif\n");
	fprintf(outfile, "#endif\n");
	fprintf(outfile, "#if defined(__GNUC__)\n");
	fprintf(outfile, "# define __Prototype(r,n) __E r __attribute__((stdcall)) (*__##n)\n");
	fprintf(outfile, "#else\n");
	fprintf(outfile, "# define __Prototype(r,n) __E r (__stdcall *__##n)\n");
	fprintf(outfile, "#endif\n\n");
}
static void write_tail_c(FILE *outfile)
{
	fprintf(outfile, "#undef __Prototype\n");
	fprintf(outfile, "#undef __E\n");
	fprintf(outfile, "#undef __D\n");
	fprintf(outfile, "\n#endif\n");
}
static void write_consts_c(FILE *outfile)
{
	struct Constant	*cnst;

	fprintf(outfile, "/* constants */\n");

	for(cnst=lib->first_cnst; cnst; cnst=cnst->next)
		fprintf(outfile, "#define %s %s\n", cnst->name, cnst->value);

	fprintf(outfile, "\n");
}
static void write_args_c(FILE *outfile, struct Func *func, int def)
{
	struct Arg	*arg;

	for(arg=func->first_arg; arg; arg=arg->next)
	{
		char type;
		if (arg != func->first_arg)
			fprintf(outfile, ", ");
		type = arg->type;
		if (!type) type = '%';
		switch(type)
		{
			case '%':
			{
				if (arg->flags&ARG_REF_BIT)
					fprintf(outfile, "int *%s", arg->name);
				else
					fprintf(outfile, "int %s", arg->name);
				if (arg->flags&ARG_DEF_BIT && def)
					fprintf(outfile, " __D(%s)", arg->def);
			}
			break;
			case '#':
			{
				if (arg->flags&ARG_REF_BIT)
					fprintf(outfile, "float *%s", arg->name);
				else
					fprintf(outfile, "float %s", arg->name);
				if (arg->flags&ARG_DEF_BIT && def)
					fprintf(outfile, " __D(%s)", arg->def);
			}
			break;
			case '$':
			{
				if (arg->flags&ARG_REF_BIT)
					fprintf(outfile, "char **%s", arg->name);
				else
					fprintf(outfile, "const char *%s", arg->name);
				if (arg->flags&ARG_DEF_BIT && def)
					fprintf(outfile, " __D(\"%s\")", _cescape(arg->def));
			}
			break;
			case '*':
			{
				if (arg->flags&ARG_REF_BIT)
					fprintf(outfile, "void **%s", arg->name);
				else
					fprintf(outfile, "void *%s", arg->name);
				if (arg->flags&ARG_DEF_BIT && def)
					fprintf(outfile, " __D(%s)", arg->def);
			}
			break;
			default:
				yyerror("unknown type");
		}
	}
}

void write_nb(FILE *outfile, const char *srcname)
{
	struct Arg	*arg;
	struct Func	*func;

	write_header_nb(outfile, srcname);
	write_consts_nb(outfile);

	fprintf(outfile, ";function declarations\n");
	for(func=lib->first_func; func; func=func->next)
	{
		if (func->type)
		{
			if (func->type=='$')
				fprintf(outfile, "FuncPtr __int_%s(", func->name);
			else if(func->type=='*')
				fprintf(outfile, "FuncPtr %s(", func->name);
			else
				fprintf(outfile, "FuncPtr %s%c(", func->name, func->type);
		}
		else
			fprintf(outfile, "FuncPtr %s(", func->name);
		write_args_nb(outfile, func);
		fprintf(outfile, ")\n");
		if (func->type=='$')
		{
			fprintf(outfile, "Function %s$(", func->name);
			write_args_nb(outfile, func);
			fprintf(outfile, ")\n");
			fprintf(outfile, "\tLocal r$\n");
			fprintf(outfile, "\tFuncPtr __conv_str(Ref lhs$, rhs) = nbAssignStrStrict\n");
			fprintf(outfile, "\t__conv_str r, __int_%s(", func->name);
			for(arg=func->first_arg; arg; arg=arg->next)
			{
				fprintf(outfile, "%s", arg->name);
				if (arg->next)
					fprintf(outfile, ", ");
			}
			fprintf(outfile, ")\n");
			fprintf(outfile, "\tReturn r\nEndFunction\n");
		}
	}

	fprintf(outfile, "\n;loader\n");
	fprintf(outfile, "__plugin = LoadPlugin(\"%s\")\n", lib->dll_name);
	fprintf(outfile, "If Not __plugin Then DebugError \"Could not load '%s'\"\n", lib->dll_name);
	for(func=lib->first_func; func; func=func->next)
	{
		if (func->type=='$')
		{
			fprintf(outfile, "__int_%s = GetPluginFunction(__plugin, \"%s\")\n",
				func->name, func->deco);
		}
		else
		{
			fprintf(outfile, "%s = GetPluginFunction(__plugin, \"%s\")\n",
				func->name, func->deco);
		}
	}

	fprintf(outfile, "\n;checker\n");
	for(func=lib->first_func; func; func=func->next)
	{
		if (func->type=='$')
		{
			fprintf(outfile, "If Not __int_%s Then DebugError \"Could not load '%s' from '%s'\"\n",
				func->name, func->deco, lib->dll_name);
		}
		else
		{
			fprintf(outfile, "If Not %s Then DebugError \"Could not load '%s' from '%s'\"\n",
				func->name, func->deco, lib->dll_name);
		}
	}

	fprintf(outfile, "\n");
}
void write_c(FILE *outfile, const char *srcname)
{
	struct Arg	*arg;
	struct Func	*func;
	const char	*funcname;

	funcname = _cfuncname(srcname);

	write_header_c(outfile, srcname);
	write_consts_c(outfile);

	fprintf(outfile, "/* function declarations */\n");
	for(func=lib->first_func; func; func=func->next)
	{
		switch(func->type)
		{
			case 0:
			case '%':
				fprintf(outfile, "__Prototype(int, %s)(", func->name);
				break;
			case '#':
				fprintf(outfile, "__Prototype(float, %s)(", func->name);
				break;
			case '$':
				fprintf(outfile, "__Prototype(const char *, %s)(", func->name);
				break;
			case '*':
				fprintf(outfile, "__Prototype(void *, %s)(", func->name);
				break;
			default:
				yyerror("unknown type");
		}

		write_args_c(outfile, func, 0);
		fprintf(outfile, ");\n");
	}

	fprintf(outfile, "#if defined(__cplusplus)||defined(cplusplus)\n");
	for(func=lib->first_func; func; func=func->next)
	{
		switch(func->type)
		{
			case 0:
			case '%':
				fprintf(outfile, "inline int %s(", func->name);
				break;
			case '#':
				fprintf(outfile, "inline float %s(", func->name);
				break;
			case '$':
				fprintf(outfile, "inline const char *%s(", func->name);
				break;
			case '*':
				fprintf(outfile, "inline void *%s(", func->name);
				break;
		}
		write_args_c(outfile, func, 1);
		fprintf(outfile, ") { return __%s(", func->name);
		for(arg=func->first_arg; arg; arg=arg->next)
		{
			if (arg!=func->first_arg)
				fprintf(outfile, ", ");
			fprintf(outfile, "%s", arg->name);
		}
		fprintf(outfile, "); }\n");
	}
	fprintf(outfile, "#else\n");
	fprintf(outfile, "# ifdef NF_RUN_MODE\n");
	fprintf(outfile, "#  define __F(r,n,p,c) r n p { return __##n(c); }\n");
	fprintf(outfile, "# else\n");
	fprintf(outfile, "#  define __F(r,n,p,c) extern r n p;\n");
	fprintf(outfile, "# endif\n");
	for(func=lib->first_func; func; func=func->next)
	{
		fprintf(outfile, "__F(");
		switch(func->type)
		{
			case 0:
			case '%':
				fprintf(outfile, "int,");
				break;
			case '#':
				fprintf(outfile, "float,");
				break;
			case '$':
				fprintf(outfile, "const char *,");
				break;
			case '*':
				fprintf(outfile, "void *,");
				break;
		}
		fprintf(outfile, "%s,(", func->name);
		write_args_c(outfile, func, 0);
		fprintf(outfile, "),(");
		for(arg=func->first_arg; arg; arg=arg->next)
		{
			if (arg!=func->first_arg)
				fprintf(outfile, ", ");
			fprintf(outfile, "%s", arg->name);
		}
		fprintf(outfile, "))\n");
	}
	fprintf(outfile, "#endif\n");

	fprintf(outfile, "\n/* loader */\n");
	fprintf(outfile, "__E void LoadPlugin_%s(void);\n", funcname);
	fprintf(outfile, "#ifdef NF_RUN_MODE\n");
	fprintf(outfile, "void LoadPlugin_%s(void)\n", funcname);
	fprintf(outfile, "{\n");
	fprintf(outfile, "\tNUKE p;\n");
	fprintf(outfile, "\n\tif ((p=LoadPlugin(\"%s\")) == (NUKE)0)\n", lib->dll_name);
	fprintf(outfile, "\t\tDebugError(\"Could not load '%s'\");\n", lib->dll_name);
	for(func=lib->first_func; func; func=func->next)
	{
		fprintf(outfile, "\tif ((*((void **)&__%s)=(void *)GetPluginFunction(p, \"%s\"))==(void *)0)\n",
			func->name, func->deco);
		fprintf(outfile, "\t\tDebugError(\"Could not load '%s' from '%s'\");\n",
			func->deco, lib->dll_name);
	}
	fprintf(outfile, "}\n#endif\n\n");

	write_tail_c(outfile);
}

int main(int argc, char **argv)
{
	const char	*outname;
	FILE		*outfile;

	if (argc<2)
	{
		error("no input file!");
		return 1;
	}

	yyin = fopen(argv[1], "r");
	if (!yyin)
	{
		error("failed to open \"%s\"", argv[1]);
		return 1;
	}

	set_current_file(argv[1]);
	reset_current_line();
	yyparse();

	if (!lib)
	{
		error("no \".lib\" declaration found!");
		return 1;
	}

	outname = (argc>2) ? argv[2] : "output.nb";
	outfile = fopen(outname, "wb");
	if (!outfile)
	{
		error("failed to open \"%s\" for writing", outname);
		return 1;
	}
	write_nb(outfile, argv[1]);
	fclose(outfile);

	outname = (argc>3) ? argv[3] : "output.h";
	outfile = fopen(outname, "wb");
	if (!outfile)
	{
		error("failed to open \"%s\" for writing", outname);
		return 1;
	}
	write_c(outfile, argv[1]);
	fclose(outfile);

	return 0;
}

struct Arg *alloc_arg(const char *name, char type)
{
	struct Arg	*arg;

	arg = (struct Arg *)malloc(sizeof(struct Arg));
	if (!arg)
	{
		error("failed to allocate memory");
		exit(1);
	}

	strncpy(arg->name, name, sizeof(arg->name)-1);
	arg->name[sizeof(arg->name)-1] = 0;

	arg->type = type;

	arg->next = (struct Arg *)0;

	return arg;
}

struct Func *alloc_func(const char *name, const char *deco, char type)
{
	struct Func	*func;
	int			i;

	if (!lib)
	{
		error("function declared without initial \".lib\" declaration");
		exit(1);
	}

	func = (struct Func *)malloc(sizeof(struct Func));
	if (!func)
	{
		error("failed to allocate memory");
		exit(1);
	}

	strncpy(func->name, name, sizeof(func->name)-1);
	func->name[sizeof(func->name)-1] = 0;

	if (!deco)
	{
		deco = name;
		sprintf(func->deco, "%s@%d", func->name, arg_sp*4);
	}
	else
	{
		strncpy(func->deco, deco, sizeof(func->deco)-1);
		func->deco[sizeof(func->deco)-1] = 0;
	}

	func->type = type;

	func->first_arg = (struct Arg *)0;
	func->last_arg = (struct Arg *)0;

	if (lib->last_func)
		lib->last_func->next = func;
	lib->last_func = func;
	if (!lib->first_func)
		lib->first_func = func;
	func->next = (struct Func *)0;

	if (arg_sp>0)
	{
		func->first_arg = arg_stack[0];
		func->last_arg = func->first_arg;
		for(i=1; i<arg_sp; i++)
		{
			func->last_arg->next = arg_stack[i];
			func->last_arg = arg_stack[i];
		}

		arg_sp = 0;
	}

	return func;
}

struct Constant *alloc_const(const char *name, const char *value)
{
	struct Constant	*cnst;

	if (!lib)
	{
		error("constant declared without initial \".lib\" declaration");
		exit(1);
	}

	cnst = (struct Constant *)malloc(sizeof(struct Constant));
	if (!cnst)
	{
		error("failed to allocate memory");
		exit(1);
	}

	strncpy(cnst->name, name, sizeof(cnst->name)-1);
	cnst->name[sizeof(cnst->name)-1] = 0;

	strncpy(cnst->value, value, sizeof(cnst->value)-1);
	cnst->value[sizeof(cnst->value)-1] = 0;

	if (lib->last_cnst)
		lib->last_cnst->next = cnst;
	lib->last_cnst = cnst;
	if (!lib->first_cnst)
		lib->first_cnst = cnst;
	cnst->next = (struct Constant *)0;

	return cnst;
}

%}

%token TOK_LIB TOK_STRING TOK_IDENT TOK_TYPE TOK_NUMBER TOK_REF TOK_CONST

%start decls

%%

decls			: decl
				| decls decl
				;

func			: TOK_IDENT '(' args ')'
				{
					alloc_func($1, (const char *)0, 0);
				}
				| TOK_IDENT TOK_TYPE '(' args ')'
				{
					alloc_func($1, (const char *)0, *$2);
				}
				| TOK_IDENT '(' args ')' ':' TOK_STRING
				{
					alloc_func($1, $6, 0);
				}
				| TOK_IDENT TOK_TYPE '(' args ')' ':' TOK_STRING
				{
					alloc_func($1, $7, *$2);
				}
				;
constant		: TOK_CONST TOK_IDENT '=' TOK_NUMBER
				{
					alloc_const($2, $4);
				}
				;
decl			: TOK_LIB TOK_STRING '\n'
				{
					if (lib != (struct Lib *)0)
					{
						error("library already specified");
						exit(1);
					}
					lib = (struct Lib *)malloc(sizeof(struct Lib));
					if (!lib)
					{
						error("failed to allocate memory");
						exit(1);
					}
					strncpy(lib->dll_name, $2, sizeof(lib->dll_name)-1);
					lib->dll_name[sizeof(lib->dll_name)-1] = 0;
					lib->first_cnst = (struct Constant *)0;
					lib->last_cnst = (struct Constant *)0;
					lib->first_func = (struct Func *)0;
					lib->last_func = (struct Func *)0;
				}
				| func '\n'
				| constant '\n'
				| '\n'
				;

args			: /* nothing */
				| arg
				| arg ',' args
				;

arg_name		: TOK_IDENT
				{
					arg_stack[arg_sp] = alloc_arg($1, 0);
				}
				| TOK_IDENT TOK_TYPE
				{
					arg_stack[arg_sp] = alloc_arg($1, *$2);
				}
				| TOK_REF TOK_IDENT
				{
					arg_stack[arg_sp] = alloc_arg($2, 0);
					arg_stack[arg_sp]->flags |= ARG_REF_BIT;
				}
				| TOK_REF TOK_IDENT TOK_TYPE
				{
					arg_stack[arg_sp] = alloc_arg($2, *$3);
					arg_stack[arg_sp]->flags |= ARG_REF_BIT;
				}
				;
arg				: arg_name
				{
					arg_sp++;
				}
				| arg_name '=' TOK_NUMBER
				{
					if (arg_stack[arg_sp]->flags & ARG_REF_BIT)
						yyerror("syntax error");
					arg_stack[arg_sp]->flags |= ARG_DEF_BIT;
					strcpy(arg_stack[arg_sp]->def, $3);
					arg_sp++;
				}
				| arg_name '=' TOK_STRING
				{
					if (arg_stack[arg_sp]->flags & ARG_REF_BIT)
						yyerror("syntax error");
					arg_stack[arg_sp]->flags |= ARG_DEF_BIT;
					strcpy(arg_stack[arg_sp]->def, $3);
					arg_sp++;
				}
				;
