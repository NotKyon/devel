#include <stdio.h>

int abs(int x) {
	return (x ^ (-((x&0x80000000)>>31))) & 0x7FFFFFFF;
}

void test(int x) {
	printf("abs(%i) = %i\n", x, abs(x));
}

int main() {
	test(-3);
	test(-2);
	test(-1);
	test(0);
	test(1);
	test(2);
	test(3);

	return 0;
}
