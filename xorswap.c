#include <stdio.h>

void xor_swap(int a, int b) {
	printf("xor_swap(%i, %i)", a, b);
	a ^= b;
	b ^= a;
	a ^= b;
	printf(" -> %i, %i\n", a, b);
}

int main() {
	xor_swap(2, 3);
	xor_swap(5, 6);
	xor_swap(19, 27);
	xor_swap(0, 1);
	xor_swap(53, -28);

	return 0;
}
