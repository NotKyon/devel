#include <ember/math.h>
#include <math.h>

void mat_identity( Matrix *dst )
{
	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;
	dst->rm._14 = 0;
	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;
	dst->rm._24 = 0;
	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
	dst->rm._34 = 0;
	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
const Matrix *mat_identity_ptr()
{
	static const float m[ 16 ] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

	return ( const Matrix * )&m[ 0 ];
}

void mat_copy( Matrix *dst, const Matrix *src )
{
	dst->rm._11 = src->rm._11;
	dst->rm._12 = src->rm._12;
	dst->rm._13 = src->rm._13;
	dst->rm._14 = src->rm._14;
	dst->rm._21 = src->rm._21;
	dst->rm._22 = src->rm._22;
	dst->rm._23 = src->rm._23;
	dst->rm._24 = src->rm._24;
	dst->rm._31 = src->rm._31;
	dst->rm._32 = src->rm._32;
	dst->rm._33 = src->rm._33;
	dst->rm._34 = src->rm._34;
	dst->rm._41 = src->rm._41;
	dst->rm._42 = src->rm._42;
	dst->rm._43 = src->rm._43;
	dst->rm._44 = src->rm._44;
}
void mat_copy3( Matrix *dst, const Matrix *src )
{
	dst->rm._11 = src->rm._11;
	dst->rm._12 = src->rm._12;
	dst->rm._13 = src->rm._13;
	dst->rm._21 = src->rm._21;
	dst->rm._22 = src->rm._22;
	dst->rm._23 = src->rm._23;
	dst->rm._31 = src->rm._31;
	dst->rm._32 = src->rm._32;
	dst->rm._33 = src->rm._33;
}

void mat_multiply3( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 =
	    a->rm._11 * b->rm._11 + a->rm._12 * b->rm._21 + a->rm._13 * b->rm._31;
	dst->rm._12 =
	    a->rm._11 * b->rm._12 + a->rm._12 * b->rm._22 + a->rm._13 * b->rm._32;
	dst->rm._13 =
	    a->rm._11 * b->rm._13 + a->rm._12 * b->rm._23 + a->rm._13 * b->rm._33;

	dst->rm._21 =
	    a->rm._21 * b->rm._11 + a->rm._22 * b->rm._21 + a->rm._23 * b->rm._31;
	dst->rm._22 =
	    a->rm._21 * b->rm._12 + a->rm._22 * b->rm._22 + a->rm._23 * b->rm._32;
	dst->rm._23 =
	    a->rm._21 * b->rm._13 + a->rm._22 * b->rm._23 + a->rm._23 * b->rm._33;

	dst->rm._31 =
	    a->rm._31 * b->rm._11 + a->rm._32 * b->rm._21 + a->rm._33 * b->rm._31;
	dst->rm._32 =
	    a->rm._31 * b->rm._12 + a->rm._32 * b->rm._22 + a->rm._33 * b->rm._32;
	dst->rm._33 =
	    a->rm._31 * b->rm._13 + a->rm._32 * b->rm._23 + a->rm._33 * b->rm._33;
}
void mat_multiply3_cc( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 =
	    a->cm._11 * b->cm._11 + a->cm._12 * b->cm._21 + a->cm._13 * b->cm._31;
	dst->rm._12 =
	    a->cm._11 * b->cm._12 + a->cm._12 * b->cm._22 + a->cm._13 * b->cm._32;
	dst->rm._13 =
	    a->cm._11 * b->cm._13 + a->cm._12 * b->cm._23 + a->cm._13 * b->cm._33;

	dst->rm._21 =
	    a->cm._21 * b->cm._11 + a->cm._22 * b->cm._21 + a->cm._23 * b->cm._31;
	dst->rm._22 =
	    a->cm._21 * b->cm._12 + a->cm._22 * b->cm._22 + a->cm._23 * b->cm._32;
	dst->rm._23 =
	    a->cm._21 * b->cm._13 + a->cm._22 * b->cm._23 + a->cm._23 * b->cm._33;

	dst->rm._31 =
	    a->cm._31 * b->cm._11 + a->cm._32 * b->cm._21 + a->cm._33 * b->cm._31;
	dst->rm._32 =
	    a->cm._31 * b->cm._12 + a->cm._32 * b->cm._22 + a->cm._33 * b->cm._32;
	dst->rm._33 =
	    a->cm._31 * b->cm._13 + a->cm._32 * b->cm._23 + a->cm._33 * b->cm._33;
}
void mat_multiply3_rc( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 =
	    a->rm._11 * b->cm._11 + a->rm._12 * b->cm._21 + a->rm._13 * b->cm._31;
	dst->rm._12 =
	    a->rm._11 * b->cm._12 + a->rm._12 * b->cm._22 + a->rm._13 * b->cm._32;
	dst->rm._13 =
	    a->rm._11 * b->cm._13 + a->rm._12 * b->cm._23 + a->rm._13 * b->cm._33;

	dst->rm._21 =
	    a->rm._21 * b->cm._11 + a->rm._22 * b->cm._21 + a->rm._23 * b->cm._31;
	dst->rm._22 =
	    a->rm._21 * b->cm._12 + a->rm._22 * b->cm._22 + a->rm._23 * b->cm._32;
	dst->rm._23 =
	    a->rm._21 * b->cm._13 + a->rm._22 * b->cm._23 + a->rm._23 * b->cm._33;

	dst->rm._31 =
	    a->rm._31 * b->cm._11 + a->rm._32 * b->cm._21 + a->rm._33 * b->cm._31;
	dst->rm._32 =
	    a->rm._31 * b->cm._12 + a->rm._32 * b->cm._22 + a->rm._33 * b->cm._32;
	dst->rm._33 =
	    a->rm._31 * b->cm._13 + a->rm._32 * b->cm._23 + a->rm._33 * b->cm._33;
}
void mat_multiply3_cr( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 =
	    a->cm._11 * b->rm._11 + a->cm._12 * b->rm._21 + a->cm._13 * b->rm._31;
	dst->rm._12 =
	    a->cm._11 * b->rm._12 + a->cm._12 * b->rm._22 + a->cm._13 * b->rm._32;
	dst->rm._13 =
	    a->cm._11 * b->rm._13 + a->cm._12 * b->rm._23 + a->cm._13 * b->rm._33;

	dst->rm._21 =
	    a->cm._21 * b->rm._11 + a->cm._22 * b->rm._21 + a->cm._23 * b->rm._31;
	dst->rm._22 =
	    a->cm._21 * b->rm._12 + a->cm._22 * b->rm._22 + a->cm._23 * b->rm._32;
	dst->rm._23 =
	    a->cm._21 * b->rm._13 + a->cm._22 * b->rm._23 + a->cm._23 * b->rm._33;

	dst->rm._31 =
	    a->cm._31 * b->rm._11 + a->cm._32 * b->rm._21 + a->cm._33 * b->rm._31;
	dst->rm._32 =
	    a->cm._31 * b->rm._12 + a->cm._32 * b->rm._22 + a->cm._33 * b->rm._32;
	dst->rm._33 =
	    a->cm._31 * b->rm._13 + a->cm._32 * b->rm._23 + a->cm._33 * b->rm._33;
}

void mat_multiply34( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 =
	    a->rm._11 * b->rm._11 + a->rm._12 * b->rm._21 + a->rm._13 * b->rm._31;
	dst->rm._12 =
	    a->rm._11 * b->rm._12 + a->rm._12 * b->rm._22 + a->rm._13 * b->rm._32;
	dst->rm._13 =
	    a->rm._11 * b->rm._13 + a->rm._12 * b->rm._23 + a->rm._13 * b->rm._33;

	dst->rm._21 =
	    a->rm._21 * b->rm._11 + a->rm._22 * b->rm._21 + a->rm._23 * b->rm._31;
	dst->rm._22 =
	    a->rm._21 * b->rm._12 + a->rm._22 * b->rm._22 + a->rm._23 * b->rm._32;
	dst->rm._23 =
	    a->rm._21 * b->rm._13 + a->rm._22 * b->rm._23 + a->rm._23 * b->rm._33;

	dst->rm._31 =
	    a->rm._31 * b->rm._11 + a->rm._32 * b->rm._21 + a->rm._33 * b->rm._31;
	dst->rm._32 =
	    a->rm._31 * b->rm._12 + a->rm._32 * b->rm._22 + a->rm._33 * b->rm._32;
	dst->rm._33 =
	    a->rm._31 * b->rm._13 + a->rm._32 * b->rm._23 + a->rm._33 * b->rm._33;

	dst->rm._41 = a->rm._41 * b->rm._11 + a->rm._42 * b->rm._21 +
	              a->rm._43 * b->rm._31 + b->rm._41;
	dst->rm._42 = a->rm._41 * b->rm._12 + a->rm._42 * b->rm._22 +
	              a->rm._43 * b->rm._32 + b->rm._42;
	dst->rm._43 = a->rm._41 * b->rm._13 + a->rm._42 * b->rm._23 +
	              a->rm._43 * b->rm._33 + b->rm._43;
}
void mat_multiply34_cc( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 =
	    a->cm._11 * b->cm._11 + a->cm._12 * b->cm._21 + a->cm._13 * b->cm._31;
	dst->rm._12 =
	    a->cm._11 * b->cm._12 + a->cm._12 * b->cm._22 + a->cm._13 * b->cm._32;
	dst->rm._13 =
	    a->cm._11 * b->cm._13 + a->cm._12 * b->cm._23 + a->cm._13 * b->cm._33;

	dst->rm._21 =
	    a->cm._21 * b->cm._11 + a->cm._22 * b->cm._21 + a->cm._23 * b->cm._31;
	dst->rm._22 =
	    a->cm._21 * b->cm._12 + a->cm._22 * b->cm._22 + a->cm._23 * b->cm._32;
	dst->rm._23 =
	    a->cm._21 * b->cm._13 + a->cm._22 * b->cm._23 + a->cm._23 * b->cm._33;

	dst->rm._31 =
	    a->cm._31 * b->cm._11 + a->cm._32 * b->cm._21 + a->cm._33 * b->cm._31;
	dst->rm._32 =
	    a->cm._31 * b->cm._12 + a->cm._32 * b->cm._22 + a->cm._33 * b->cm._32;
	dst->rm._33 =
	    a->cm._31 * b->cm._13 + a->cm._32 * b->cm._23 + a->cm._33 * b->cm._33;

	dst->rm._41 = a->cm._41 * b->cm._11 + a->cm._42 * b->cm._21 +
	              a->cm._43 * b->cm._31 + b->cm._41;
	dst->rm._42 = a->cm._41 * b->cm._12 + a->cm._42 * b->cm._22 +
	              a->cm._43 * b->cm._32 + b->cm._42;
	dst->rm._43 = a->cm._41 * b->cm._13 + a->cm._42 * b->cm._23 +
	              a->cm._43 * b->cm._33 + b->cm._43;
}
void mat_multiply34_rc( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 =
	    a->rm._11 * b->cm._11 + a->rm._12 * b->cm._21 + a->rm._13 * b->cm._31;
	dst->rm._12 =
	    a->rm._11 * b->cm._12 + a->rm._12 * b->cm._22 + a->rm._13 * b->cm._32;
	dst->rm._13 =
	    a->rm._11 * b->cm._13 + a->rm._12 * b->cm._23 + a->rm._13 * b->cm._33;

	dst->rm._21 =
	    a->rm._21 * b->cm._11 + a->rm._22 * b->cm._21 + a->rm._23 * b->cm._31;
	dst->rm._22 =
	    a->rm._21 * b->cm._12 + a->rm._22 * b->cm._22 + a->rm._23 * b->cm._32;
	dst->rm._23 =
	    a->rm._21 * b->cm._13 + a->rm._22 * b->cm._23 + a->rm._23 * b->cm._33;

	dst->rm._31 =
	    a->rm._31 * b->cm._11 + a->rm._32 * b->cm._21 + a->rm._33 * b->cm._31;
	dst->rm._32 =
	    a->rm._31 * b->cm._12 + a->rm._32 * b->cm._22 + a->rm._33 * b->cm._32;
	dst->rm._33 =
	    a->rm._31 * b->cm._13 + a->rm._32 * b->cm._23 + a->rm._33 * b->cm._33;

	dst->rm._41 = a->rm._41 * b->cm._11 + a->rm._42 * b->cm._21 +
	              a->rm._43 * b->cm._31 + b->cm._41;
	dst->rm._42 = a->rm._41 * b->cm._12 + a->rm._42 * b->cm._22 +
	              a->rm._43 * b->cm._32 + b->cm._42;
	dst->rm._43 = a->rm._41 * b->cm._13 + a->rm._42 * b->cm._23 +
	              a->rm._43 * b->cm._33 + b->cm._43;
}
void mat_multiply34_cr( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 =
	    a->cm._11 * b->rm._11 + a->cm._12 * b->rm._21 + a->cm._13 * b->rm._31;
	dst->rm._12 =
	    a->cm._11 * b->rm._12 + a->cm._12 * b->rm._22 + a->cm._13 * b->rm._32;
	dst->rm._13 =
	    a->cm._11 * b->rm._13 + a->cm._12 * b->rm._23 + a->cm._13 * b->rm._33;

	dst->rm._21 =
	    a->cm._21 * b->rm._11 + a->cm._22 * b->rm._21 + a->cm._23 * b->rm._31;
	dst->rm._22 =
	    a->cm._21 * b->rm._12 + a->cm._22 * b->rm._22 + a->cm._23 * b->rm._32;
	dst->rm._23 =
	    a->cm._21 * b->rm._13 + a->cm._22 * b->rm._23 + a->cm._23 * b->rm._33;

	dst->rm._31 =
	    a->cm._31 * b->rm._11 + a->cm._32 * b->rm._21 + a->cm._33 * b->rm._31;
	dst->rm._32 =
	    a->cm._31 * b->rm._12 + a->cm._32 * b->rm._22 + a->cm._33 * b->rm._32;
	dst->rm._33 =
	    a->cm._31 * b->rm._13 + a->cm._32 * b->rm._23 + a->cm._33 * b->rm._33;

	dst->rm._41 = a->cm._41 * b->rm._11 + a->cm._42 * b->rm._21 +
	              a->cm._43 * b->rm._31 + b->rm._41;
	dst->rm._42 = a->cm._41 * b->rm._12 + a->cm._42 * b->rm._22 +
	              a->cm._43 * b->rm._32 + b->rm._42;
	dst->rm._43 = a->cm._41 * b->rm._13 + a->cm._42 * b->rm._23 +
	              a->cm._43 * b->rm._33 + b->rm._43;
}

void mat_multiply( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 = a->rm._11 * b->rm._11 + a->rm._12 * b->rm._21 +
	              a->rm._13 * b->rm._31 + a->rm._14 * b->rm._41;
	dst->rm._12 = a->rm._11 * b->rm._12 + a->rm._12 * b->rm._22 +
	              a->rm._13 * b->rm._32 + a->rm._14 * b->rm._42;
	dst->rm._13 = a->rm._11 * b->rm._13 + a->rm._12 * b->rm._23 +
	              a->rm._13 * b->rm._33 + a->rm._14 * b->rm._43;
	dst->rm._14 = a->rm._11 * b->rm._14 + a->rm._12 * b->rm._24 +
	              a->rm._13 * b->rm._34 + a->rm._14 * b->rm._44;

	dst->rm._21 = a->rm._21 * b->rm._11 + a->rm._22 * b->rm._21 +
	              a->rm._23 * b->rm._31 + a->rm._24 * b->rm._41;
	dst->rm._22 = a->rm._21 * b->rm._12 + a->rm._22 * b->rm._22 +
	              a->rm._23 * b->rm._32 + a->rm._24 * b->rm._42;
	dst->rm._23 = a->rm._21 * b->rm._13 + a->rm._22 * b->rm._23 +
	              a->rm._23 * b->rm._33 + a->rm._24 * b->rm._43;
	dst->rm._24 = a->rm._21 * b->rm._14 + a->rm._22 * b->rm._24 +
	              a->rm._23 * b->rm._34 + a->rm._24 * b->rm._44;

	dst->rm._31 = a->rm._31 * b->rm._11 + a->rm._32 * b->rm._21 +
	              a->rm._33 * b->rm._31 + a->rm._34 * b->rm._41;
	dst->rm._32 = a->rm._31 * b->rm._12 + a->rm._32 * b->rm._22 +
	              a->rm._33 * b->rm._32 + a->rm._34 * b->rm._42;
	dst->rm._33 = a->rm._31 * b->rm._13 + a->rm._32 * b->rm._23 +
	              a->rm._33 * b->rm._33 + a->rm._34 * b->rm._43;
	dst->rm._34 = a->rm._31 * b->rm._14 + a->rm._32 * b->rm._24 +
	              a->rm._33 * b->rm._34 + a->rm._34 * b->rm._44;

	dst->rm._41 = a->rm._41 * b->rm._11 + a->rm._42 * b->rm._21 +
	              a->rm._43 * b->rm._31 + a->rm._44 * b->rm._41;
	dst->rm._42 = a->rm._41 * b->rm._12 + a->rm._42 * b->rm._22 +
	              a->rm._43 * b->rm._32 + a->rm._44 * b->rm._42;
	dst->rm._43 = a->rm._41 * b->rm._13 + a->rm._42 * b->rm._23 +
	              a->rm._43 * b->rm._33 + a->rm._44 * b->rm._43;
	dst->rm._44 = a->rm._41 * b->rm._14 + a->rm._42 * b->rm._24 +
	              a->rm._43 * b->rm._34 + a->rm._44 * b->rm._44;
}
void mat_multiply_cc( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 = a->cm._11 * b->cm._11 + a->cm._12 * b->cm._21 +
	              a->cm._13 * b->cm._31 + a->cm._14 * b->cm._41;
	dst->rm._12 = a->cm._11 * b->cm._12 + a->cm._12 * b->cm._22 +
	              a->cm._13 * b->cm._32 + a->cm._14 * b->cm._42;
	dst->rm._13 = a->cm._11 * b->cm._13 + a->cm._12 * b->cm._23 +
	              a->cm._13 * b->cm._33 + a->cm._14 * b->cm._43;
	dst->rm._14 = a->cm._11 * b->cm._14 + a->cm._12 * b->cm._24 +
	              a->cm._13 * b->cm._34 + a->cm._14 * b->cm._44;

	dst->rm._21 = a->cm._21 * b->cm._11 + a->cm._22 * b->cm._21 +
	              a->cm._23 * b->cm._31 + a->cm._24 * b->cm._41;
	dst->rm._22 = a->cm._21 * b->cm._12 + a->cm._22 * b->cm._22 +
	              a->cm._23 * b->cm._32 + a->cm._24 * b->cm._42;
	dst->rm._23 = a->cm._21 * b->cm._13 + a->cm._22 * b->cm._23 +
	              a->cm._23 * b->cm._33 + a->cm._24 * b->cm._43;
	dst->rm._24 = a->cm._21 * b->cm._14 + a->cm._22 * b->cm._24 +
	              a->cm._23 * b->cm._34 + a->cm._24 * b->cm._44;

	dst->rm._31 = a->cm._31 * b->cm._11 + a->cm._32 * b->cm._21 +
	              a->cm._33 * b->cm._31 + a->cm._34 * b->cm._41;
	dst->rm._32 = a->cm._31 * b->cm._12 + a->cm._32 * b->cm._22 +
	              a->cm._33 * b->cm._32 + a->cm._34 * b->cm._42;
	dst->rm._33 = a->cm._31 * b->cm._13 + a->cm._32 * b->cm._23 +
	              a->cm._33 * b->cm._33 + a->cm._34 * b->cm._43;
	dst->rm._34 = a->cm._31 * b->cm._14 + a->cm._32 * b->cm._24 +
	              a->cm._33 * b->cm._34 + a->cm._34 * b->cm._44;

	dst->rm._41 = a->cm._41 * b->cm._11 + a->cm._42 * b->cm._21 +
	              a->cm._43 * b->cm._31 + a->cm._44 * b->cm._41;
	dst->rm._42 = a->cm._41 * b->cm._12 + a->cm._42 * b->cm._22 +
	              a->cm._43 * b->cm._32 + a->cm._44 * b->cm._42;
	dst->rm._43 = a->cm._41 * b->cm._13 + a->cm._42 * b->cm._23 +
	              a->cm._43 * b->cm._33 + a->cm._44 * b->cm._43;
	dst->rm._44 = a->cm._41 * b->cm._14 + a->cm._42 * b->cm._24 +
	              a->cm._43 * b->cm._34 + a->cm._44 * b->cm._44;
}
void mat_multiply_rc( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 = a->rm._11 * b->cm._11 + a->rm._12 * b->cm._21 +
	              a->rm._13 * b->cm._31 + a->rm._14 * b->cm._41;
	dst->rm._12 = a->rm._11 * b->cm._12 + a->rm._12 * b->cm._22 +
	              a->rm._13 * b->cm._32 + a->rm._14 * b->cm._42;
	dst->rm._13 = a->rm._11 * b->cm._13 + a->rm._12 * b->cm._23 +
	              a->rm._13 * b->cm._33 + a->rm._14 * b->cm._43;
	dst->rm._14 = a->rm._11 * b->cm._14 + a->rm._12 * b->cm._24 +
	              a->rm._13 * b->cm._34 + a->rm._14 * b->cm._44;

	dst->rm._21 = a->rm._21 * b->cm._11 + a->rm._22 * b->cm._21 +
	              a->rm._23 * b->cm._31 + a->rm._24 * b->cm._41;
	dst->rm._22 = a->rm._21 * b->cm._12 + a->rm._22 * b->cm._22 +
	              a->rm._23 * b->cm._32 + a->rm._24 * b->cm._42;
	dst->rm._23 = a->rm._21 * b->cm._13 + a->rm._22 * b->cm._23 +
	              a->rm._23 * b->cm._33 + a->rm._24 * b->cm._43;
	dst->rm._24 = a->rm._21 * b->cm._14 + a->rm._22 * b->cm._24 +
	              a->rm._23 * b->cm._34 + a->rm._24 * b->cm._44;

	dst->rm._31 = a->rm._31 * b->cm._11 + a->rm._32 * b->cm._21 +
	              a->rm._33 * b->cm._31 + a->rm._34 * b->cm._41;
	dst->rm._32 = a->rm._31 * b->cm._12 + a->rm._32 * b->cm._22 +
	              a->rm._33 * b->cm._32 + a->rm._34 * b->cm._42;
	dst->rm._33 = a->rm._31 * b->cm._13 + a->rm._32 * b->cm._23 +
	              a->rm._33 * b->cm._33 + a->rm._34 * b->cm._43;
	dst->rm._34 = a->rm._31 * b->cm._14 + a->rm._32 * b->cm._24 +
	              a->rm._33 * b->cm._34 + a->rm._34 * b->cm._44;

	dst->rm._41 = a->rm._41 * b->cm._11 + a->rm._42 * b->cm._21 +
	              a->rm._43 * b->cm._31 + a->rm._44 * b->cm._41;
	dst->rm._42 = a->rm._41 * b->cm._12 + a->rm._42 * b->cm._22 +
	              a->rm._43 * b->cm._32 + a->rm._44 * b->cm._42;
	dst->rm._43 = a->rm._41 * b->cm._13 + a->rm._42 * b->cm._23 +
	              a->rm._43 * b->cm._33 + a->rm._44 * b->cm._43;
	dst->rm._44 = a->rm._41 * b->cm._14 + a->rm._42 * b->cm._24 +
	              a->rm._43 * b->cm._34 + a->rm._44 * b->cm._44;
}
void mat_multiply_cr( Matrix *dst, const Matrix *a, const Matrix *b )
{
	dst->rm._11 = a->cm._11 * b->rm._11 + a->cm._12 * b->rm._21 +
	              a->cm._13 * b->rm._31 + a->cm._14 * b->rm._41;
	dst->rm._12 = a->cm._11 * b->rm._12 + a->cm._12 * b->rm._22 +
	              a->cm._13 * b->rm._32 + a->cm._14 * b->rm._42;
	dst->rm._13 = a->cm._11 * b->rm._13 + a->cm._12 * b->rm._23 +
	              a->cm._13 * b->rm._33 + a->cm._14 * b->rm._43;
	dst->rm._14 = a->cm._11 * b->rm._14 + a->cm._12 * b->rm._24 +
	              a->cm._13 * b->rm._34 + a->cm._14 * b->rm._44;

	dst->rm._21 = a->cm._21 * b->rm._11 + a->cm._22 * b->rm._21 +
	              a->cm._23 * b->rm._31 + a->cm._24 * b->rm._41;
	dst->rm._22 = a->cm._21 * b->rm._12 + a->cm._22 * b->rm._22 +
	              a->cm._23 * b->rm._32 + a->cm._24 * b->rm._42;
	dst->rm._23 = a->cm._21 * b->rm._13 + a->cm._22 * b->rm._23 +
	              a->cm._23 * b->rm._33 + a->cm._24 * b->rm._43;
	dst->rm._24 = a->cm._21 * b->rm._14 + a->cm._22 * b->rm._24 +
	              a->cm._23 * b->rm._34 + a->cm._24 * b->rm._44;

	dst->rm._31 = a->cm._31 * b->rm._11 + a->cm._32 * b->rm._21 +
	              a->cm._33 * b->rm._31 + a->cm._34 * b->rm._41;
	dst->rm._32 = a->cm._31 * b->rm._12 + a->cm._32 * b->rm._22 +
	              a->cm._33 * b->rm._32 + a->cm._34 * b->rm._42;
	dst->rm._33 = a->cm._31 * b->rm._13 + a->cm._32 * b->rm._23 +
	              a->cm._33 * b->rm._33 + a->cm._34 * b->rm._43;
	dst->rm._34 = a->cm._31 * b->rm._14 + a->cm._32 * b->rm._24 +
	              a->cm._33 * b->rm._34 + a->cm._34 * b->rm._44;

	dst->rm._41 = a->cm._41 * b->rm._11 + a->cm._42 * b->rm._21 +
	              a->cm._43 * b->rm._31 + a->cm._44 * b->rm._41;
	dst->rm._42 = a->cm._41 * b->rm._12 + a->cm._42 * b->rm._22 +
	              a->cm._43 * b->rm._32 + a->cm._44 * b->rm._42;
	dst->rm._43 = a->cm._41 * b->rm._13 + a->cm._42 * b->rm._23 +
	              a->cm._43 * b->rm._33 + a->cm._44 * b->rm._43;
	dst->rm._44 = a->cm._41 * b->rm._14 + a->cm._42 * b->rm._24 +
	              a->cm._43 * b->rm._34 + a->cm._44 * b->rm._44;
}

void mat_transpose3( Matrix *dst, const Matrix *src )
{
	if( dst != src ) {
		dst->rm._11 = src->cm._11;
		dst->rm._12 = src->cm._12;
		dst->rm._13 = src->cm._13;
		dst->rm._21 = src->cm._21;
		dst->rm._22 = src->cm._22;
		dst->rm._23 = src->cm._23;
		dst->rm._31 = src->cm._31;
		dst->rm._32 = src->cm._32;
		dst->rm._33 = src->cm._33;
	} else {
		Matrix tmp;

		mat_transpose3( &tmp, src );
		mat_copy( dst, &tmp );
	}
}
void mat_transpose( Matrix *dst, const Matrix *src )
{
	if( dst != src ) {
		dst->rm._11 = src->cm._11;
		dst->rm._12 = src->cm._12;
		dst->rm._13 = src->cm._13;
		dst->rm._14 = src->cm._14;
		dst->rm._21 = src->cm._21;
		dst->rm._22 = src->cm._22;
		dst->rm._23 = src->cm._23;
		dst->rm._24 = src->cm._24;
		dst->rm._31 = src->cm._31;
		dst->rm._32 = src->cm._32;
		dst->rm._33 = src->cm._33;
		dst->rm._34 = src->cm._34;
	} else {
		Matrix tmp;

		mat_transpose( &tmp, src );
		mat_copy( dst, &tmp );
	}
}

void mat_quaternion( Matrix *dst, const Quaternion *src )
{
	/*
*     float	r;
*     float	qw, qx, qy, qz;
*     float	xx, yy, zz, xy, xz, yz, wx, wy, wz;
*
*     qw = src->w;
*     qx = src->x;
*     qy = src->y;
*     qz = src->z;
*
*     r = 1/sqrtf(qw*qw + qx*qx + qy*qy + qz*qz);
*     qw *= r;
*     qx *= r;
*     qy *= r;
*     qz *= r;
*
*     xx = qx*qx; yy = qy*qy; zz = qz*qz;
*     xy = qx*qy; xz = qx*qz; yz = qy*qz;
*     wx = qw*qx; wy = qw*qy; wz = qw*qz;
*
*     dst->rm._11 = 1-2*(yy+zz);
*     dst->rm._12 =   2*(xy+wz);
*     dst->rm._13 =   2*(xz-wy);
*     dst->rm._14 =   0        ;
*
*     dst->rm._21 =   2*(xy-wz);
*     dst->rm._22 = 1-2*(xx+zz);
*     dst->rm._23 =   2*(yz+wx);
*     dst->rm._24 =   0        ;
*
*     dst->rm._31 =   2*(xz+wy);
*     dst->rm._32 =   2*(yz-wx);
*     dst->rm._33 = 1-2*(xx+yy);
*     dst->rm._34 =   0        ;
*
*     dst->rm._41 =   0        ;
*     dst->rm._42 =   0        ;
*     dst->rm._43 =   0        ;
*     dst->rm._44 =   1        ;
*/
	float qw, qx, qy, qz;
	float xx, yy, zz, xy, xz, yz, wx, wy, wz;

	qw = src->w;
	qx = src->x;
	qy = src->y;
	qz = src->z;

	xx = qx * qx;
	yy = qy * qy;
	zz = qz * qz;
	xy = qx * qy;
	xz = qx * qz;
	yz = qy * qz;
	wx = qw * qx;
	wy = qw * qy;
	wz = qw * qz;

	dst->rm._11 = 1 - 2 * ( yy + zz );
	dst->rm._12 = 2 * ( xy - wz );
	dst->rm._13 = 2 * ( xz + wy );
	dst->rm._14 = 0;

	dst->rm._21 = 2 * ( xy + wz );
	dst->rm._22 = 1 - 2 * ( xx + zz );
	dst->rm._23 = 2 * ( yz - wx );
	dst->rm._24 = 0;

	dst->rm._31 = 2 * ( xz - wy );
	dst->rm._32 = 2 * ( yz + wx );
	dst->rm._33 = 1 - 2 * ( xx + yy );
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
void mat_quaternion_fast( Matrix *dst, const Quaternion *src )
{
	/*float	r;*/
	float qw, qx, qy, qz;
	float xx, yy, zz, xy, xz, yz, wx, wy, wz;

	qw = src->w;
	qx = src->x;
	qy = src->y;
	qz = src->z;

	/*
       * r = invsqrt_fast(qw*qw + qx*qx + qy*qy + qz*qz);
       * qw *= r;
       * qx *= r;
       * qy *= r;
       * qz *= r;
       */

	xx = qx * qx;
	yy = qy * qy;
	zz = qz * qz;
	xy = qx * qy;
	xz = qx * qz;
	yz = qy * qz;
	wx = qw * qx;
	wy = qw * qy;
	wz = qw * qz;

	dst->rm._11 = 1 - 2 * ( yy + zz );
	dst->rm._12 = 2 * ( xy + wz );
	dst->rm._13 = 2 * ( xz - wy );
	dst->rm._14 = 0;

	dst->rm._21 = 2 * ( xy - wz );
	dst->rm._22 = 1 - 2 * ( xx + zz );
	dst->rm._23 = 2 * ( yz + wx );
	dst->rm._24 = 0;

	dst->rm._31 = 2 * ( xz + wy );
	dst->rm._32 = 2 * ( yz - wx );
	dst->rm._33 = 1 - 2 * ( xx + yy );
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
void mat_quaternion3( Matrix *dst, const Quaternion *src )
{
	/*
       * float	r;
       * float	qw, qx, qy, qz;
       * float	xx, yy, zz, xy, xz, yz, wx, wy, wz;
       *
       * qw = src->w;
       * qx = src->x;
       * qy = src->y;
       * qz = src->z;
       *
       * r = 1/sqrtf(qw*qw + qx*qx + qy*qy + qz*qz);
       * qw *= r;
       * qx *= r;
       * qy *= r;
       * qz *= r;
       *
       * xx = qx*qx; yy = qy*qy; zz = qz*qz;
       * xy = qx*qy; xz = qx*qz; yz = qy*qz;
       * wx = qw*qx; wy = qw*qy; wz = qw*qz;
       *
       * dst->rm._11 = 1-2*(yy+zz);
       * dst->rm._12 =   2*(xy+wz);
       * dst->rm._13 =   2*(xz-wy);
       *
       * dst->rm._21 =   2*(xy-wz);
       * dst->rm._22 = 1-2*(xx+zz);
       * dst->rm._23 =   2*(yz+wx);
       *
       * dst->rm._31 =   2*(xz+wy);
       * dst->rm._32 =   2*(yz-wx);
       * dst->rm._33 = 1-2*(xx+yy);
       */
	float qw, qx, qy, qz;
	float xx, yy, zz, xy, xz, yz, wx, wy, wz;

	qw = src->w;
	qx = src->x;
	qy = src->y;
	qz = src->z;

	xx = qx * qx;
	yy = qy * qy;
	zz = qz * qz;
	xy = qx * qy;
	xz = qx * qz;
	yz = qy * qz;
	wx = qw * qx;
	wy = qw * qy;
	wz = qw * qz;

	dst->rm._11 = 1 - 2 * ( yy + zz );
	dst->rm._12 = 2 * ( xy - wz );
	dst->rm._13 = 2 * ( xz + wy );

	dst->rm._21 = 2 * ( xy + wz );
	dst->rm._22 = 1 - 2 * ( xx + zz );
	dst->rm._23 = 2 * ( yz - wx );

	dst->rm._31 = 2 * ( xz - wy );
	dst->rm._32 = 2 * ( yz + wx );
	dst->rm._33 = 1 - 2 * ( xx + yy );
}
void mat_quaternion3_fast( Matrix *dst, const Quaternion *src )
{
	/*
       * float	r;
       * float	qw, qx, qy, qz;
       * float	xx, yy, zz, xy, xz, yz, wx, wy, wz;
       *
       * qw = src->w;
       * qx = src->x;
       * qy = src->y;
       * qz = src->z;
       *
       * r = invsqrt_fast(qw*qw + qx*qx + qy*qy + qz*qz);
       * qw *= r;
       * qx *= r;
       * qy *= r;
       * qz *= r;
       *
       * xx = qx*qx; yy = qy*qy; zz = qz*qz;
       * xy = qx*qy; xz = qx*qz; yz = qy*qz;
       * wx = qw*qx; wy = qw*qy; wz = qw*qz;
       *
       * dst->rm._11 = 1-2*(yy+zz);
       * dst->rm._12 =   2*(xy+wz);
       * dst->rm._13 =   2*(xz-wy);
       *
       * dst->rm._21 =   2*(xy-wz);
       * dst->rm._22 = 1-2*(xx+zz);
       * dst->rm._23 =   2*(yz+wx);
       *
       * dst->rm._31 =   2*(xz+wy);
       * dst->rm._32 =   2*(yz-wx);
       * dst->rm._33 = 1-2*(xx+yy);
       */
	float qw, qx, qy, qz;
	float xx, yy, zz, xy, xz, yz, wx, wy, wz;

	qw = src->w;
	qx = src->x;
	qy = src->y;
	qz = src->z;

	xx = qx * qx;
	yy = qy * qy;
	zz = qz * qz;
	xy = qx * qy;
	xz = qx * qz;
	yz = qy * qz;
	wx = qw * qx;
	wy = qw * qy;
	wz = qw * qz;

	dst->rm._11 = 1 - 2 * ( yy + zz );
	dst->rm._12 = 2 * ( xy - wz );
	dst->rm._13 = 2 * ( xz + wy );

	dst->rm._21 = 2 * ( xy + wz );
	dst->rm._22 = 1 - 2 * ( xx + zz );
	dst->rm._23 = 2 * ( yz - wx );

	dst->rm._31 = 2 * ( xz - wy );
	dst->rm._32 = 2 * ( yz + wx );
	dst->rm._33 = 1 - 2 * ( xx + yy );
}

void mat_decompose3( const Matrix *src, Quaternion *rot, Vec3 *scale )
{
	Matrix normal;
	float sx, sy, sz;

	scale->x = vec3_length( ( const Vec3 * )&src->rm._11 );
	scale->y = vec3_length( ( const Vec3 * )&src->rm._21 );
	scale->z = vec3_length( ( const Vec3 * )&src->rm._31 );

	sx = 1.0f / scale->x;
	sy = 1.0f / scale->y;
	sz = 1.0f / scale->z;

	vec3_scale( ( Vec3 * )&normal.rm._11, ( const Vec3 * )&src->rm._11, sx );
	vec3_scale( ( Vec3 * )&normal.rm._21, ( const Vec3 * )&src->rm._21, sy );
	vec3_scale( ( Vec3 * )&normal.rm._31, ( const Vec3 * )&src->rm._31, sz );

	quat_rotation( rot, &normal );
}
void mat_decompose_quaternion( const Matrix *src, Quaternion *rot )
{
	Matrix normal;
	float sx, sy, sz;

	sx = vec3_invlength( ( const Vec3 * )&src->rm._11 );
	sy = vec3_invlength( ( const Vec3 * )&src->rm._21 );
	sz = vec3_invlength( ( const Vec3 * )&src->rm._31 );

	vec3_scale( ( Vec3 * )&normal.rm._11, ( const Vec3 * )&src->rm._11, sx );
	vec3_scale( ( Vec3 * )&normal.rm._21, ( const Vec3 * )&src->rm._21, sy );
	vec3_scale( ( Vec3 * )&normal.rm._31, ( const Vec3 * )&src->rm._31, sz );

	quat_rotation( rot, &normal );
}
void mat_decompose_quaternion_fast( const Matrix *src, Quaternion *rot )
{
	Matrix normal;
	float sx, sy, sz;

	sx = vec3_invlength_fast( ( const Vec3 * )&src->rm._11 );
	sy = vec3_invlength_fast( ( const Vec3 * )&src->rm._21 );
	sz = vec3_invlength_fast( ( const Vec3 * )&src->rm._31 );

	vec3_scale( ( Vec3 * )&normal.rm._11, ( const Vec3 * )&src->rm._11, sx );
	vec3_scale( ( Vec3 * )&normal.rm._21, ( const Vec3 * )&src->rm._21, sy );
	vec3_scale( ( Vec3 * )&normal.rm._31, ( const Vec3 * )&src->rm._31, sz );

	quat_rotation_fast( rot, &normal );
}
void mat_decompose( const Matrix *src, Vec3 *trans, Quaternion *rot,
    Vec3 *scale )
{
	Matrix normal;
	float sx, sy, sz;

	vec3_copy( trans, ( const Vec3 * )&src->rm._41 );

	scale->x = vec3_length( ( const Vec3 * )&src->rm._11 );
	scale->y = vec3_length( ( const Vec3 * )&src->rm._21 );
	scale->z = vec3_length( ( const Vec3 * )&src->rm._31 );

	sx = 1.0f / scale->x;
	sy = 1.0f / scale->y;
	sz = 1.0f / scale->z;

	vec3_scale( ( Vec3 * )&normal.rm._11, ( const Vec3 * )&src->rm._11, sx );
	vec3_scale( ( Vec3 * )&normal.rm._21, ( const Vec3 * )&src->rm._21, sy );
	vec3_scale( ( Vec3 * )&normal.rm._31, ( const Vec3 * )&src->rm._31, sz );

	quat_rotation( rot, &normal );
}
void mat_decompose_scale( const Matrix *src, Vec3 *scale )
{
	scale->x = vec3_length( ( const Vec3 * )&src->rm._11 );
	scale->y = vec3_length( ( const Vec3 * )&src->rm._21 );
	scale->z = vec3_length( ( const Vec3 * )&src->rm._31 );
}
void mat_decompose_scale_fast( const Matrix *src, Vec3 *scale )
{
	scale->x = vec3_length_fast( ( const Vec3 * )&src->rm._11 );
	scale->y = vec3_length_fast( ( const Vec3 * )&src->rm._21 );
	scale->z = vec3_length_fast( ( const Vec3 * )&src->rm._31 );
}

float mat_determinant3( const Matrix *src )
{
	float aei, bfg, cdh, afh, bdi, ceg;

	/*
       *      a b c	11 12 13
       *      d e f	21 22 23
       *      g h i	31 32 33
       */

	aei = src->rm._11 * src->rm._22 * src->rm._33;
	bfg = src->rm._12 * src->rm._23 * src->rm._31;
	cdh = src->rm._13 * src->rm._21 * src->rm._32;
	afh = src->rm._11 * src->rm._23 * src->rm._32;
	bdi = src->rm._12 * src->rm._21 * src->rm._33;
	ceg = src->rm._13 * src->rm._22 * src->rm._31;

	return aei + bfg + cdh - afh - bdi - ceg;
}
float mat_determinant( const Matrix *src )
{
	Vec4 vm, vx, vy, vz;

	vx.x = src->rm._11;
	vx.y = src->rm._21;
	vx.z = src->rm._31;
	vx.w = src->rm._41;

	vy.x = src->rm._12;
	vy.y = src->rm._22;
	vy.z = src->rm._32;
	vy.w = src->rm._42;

	vz.x = src->rm._13;
	vz.y = src->rm._32;
	vz.z = src->rm._33;
	vz.w = src->rm._43;

	vec4_cross( &vm, &vx, &vy, &vz );

	return -( src->rm._14 * vm.x + src->rm._24 * vm.y + src->rm._34 * vm.z +
	          src->rm._44 * vm.w );
}

void mat_inverse_world( Matrix *dst, const Matrix *src )
{
#if 0
	mat_transpose3(dst, src);
#else
	mat_inverse3( dst, src );
#endif
	dst->rm._14 = 0;
	dst->rm._24 = 0;
	dst->rm._34 = 0;
	dst->rm._41 = -( dst->rm._11 * src->rm._41 + dst->rm._21 * src->rm._42 +
	                 dst->rm._31 * src->rm._43 );
	dst->rm._42 = -( dst->rm._12 * src->rm._41 + dst->rm._22 * src->rm._42 +
	                 dst->rm._32 * src->rm._43 );
	dst->rm._43 = -( dst->rm._13 * src->rm._41 + dst->rm._23 * src->rm._42 +
	                 dst->rm._33 * src->rm._43 );
	dst->rm._44 = 1;
}
float mat_inverse3( Matrix *dst, const Matrix *src )
{
	Matrix tmp;
	float r, d;
	Vec3 vr, vi[ 3 ];
	int i, j, k;

	if( dst == src ) {
		r = mat_inverse3( &tmp, src );
		mat_copy( dst, &tmp );
		return r;
	}

	if( !( d = mat_determinant3( src ) ) )
		return 0.0f;

	for( i = 0; i < 3; i++ ) {
		for( j = 0; j < 3; j++ ) {
			k = j;
			if( k > i )
				k -= 1;

			vi[ k ].x = src->f[ j * 4 + 0 ];
			vi[ k ].y = src->f[ j * 4 + 1 ];
			vi[ k ].z = src->f[ j * 4 + 2 ];
		}

		vec3_cross( &vr, &vi[ 0 ], &vi[ 1 ] );
		dst->f[ 0 * 4 + i ] = pow( -1, i ) * vr.x / d;
		dst->f[ 1 * 4 + i ] = pow( -1, i ) * vr.y / d;
		dst->f[ 2 * 4 + i ] = pow( -1, i ) * vr.z / d;
	}

	return d;
}
float mat_inverse( Matrix *dst, const Matrix *src )
{
	float d;
	Vec4 vr, vi[ 4 ];
	int i, j, k;

	if( dst == src ) {
		Matrix tmp;
		float r;
		r = mat_inverse( &tmp, src );
		mat_copy( dst, &tmp );
		return r;
	}

	if( !( d = mat_determinant( src ) ) )
		return 0;

	for( i = 0; i < 4; i++ ) {
		for( j = 0; j < 4; j++ ) {
			k = j;
			if( k > i )
				k -= 1;

			vi[ k ].x = src->f[ j * 4 + 0 ];
			vi[ k ].y = src->f[ j * 4 + 1 ];
			vi[ k ].z = src->f[ j * 4 + 2 ];
			vi[ k ].w = src->f[ j * 4 + 3 ];
		}

		vec4_cross( &vr, &vi[ 0 ], &vi[ 1 ], &vi[ 2 ] );
		dst->f[ 0 * 4 + i ] = pow( -1, i ) * vr.x / d;
		dst->f[ 1 * 4 + i ] = pow( -1, i ) * vr.y / d;
		dst->f[ 2 * 4 + i ] = pow( -1, i ) * vr.z / d;
		dst->f[ 3 * 4 + i ] = pow( -1, i ) * vr.w / d;
	}

	return d;
}

void mat_translation_ex( Matrix *dst, const Vec3 *trans )
{
	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
	dst->rm._34 = 0;

	dst->rm._41 = trans->x;
	dst->rm._42 = trans->y;
	dst->rm._43 = trans->z;
	dst->rm._44 = 1;
}
void mat_translation( Matrix *dst, float x, float y, float z )
{
	Vec3 trans;

	trans.x = x;
	trans.y = y;
	trans.z = z;
	mat_translation_ex( dst, &trans );
}

void mat_scaling_ex( Matrix *dst, const Vec3 *scale )
{
	dst->rm._11 = scale->x;
	dst->rm._12 = 0;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = scale->y;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = scale->z;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
void mat_scaling( Matrix *dst, float x, float y, float z )
{
	Vec3 scale;

	scale.x = x;
	scale.y = y;
	scale.z = z;
	mat_scaling_ex( dst, &scale );
}

void mat_rotation_x( Matrix *dst, float x )
{
	float c, s;

	c = ( float )cos( x );
	s = ( float )sin( x );

	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = c;
	dst->rm._23 = -s;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = s;
	dst->rm._33 = c;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
void mat_rotation_x_fast( Matrix *dst, float x )
{
	float c, s;

	c = cos_fast( x );
	s = sin_fast( x );

	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = c;
	dst->rm._23 = -s;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = s;
	dst->rm._33 = c;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}

void mat_rotation_y( Matrix *dst, float y )
{
	float c, s;

	c = ( float )cos( y );
	s = ( float )sin( y );

	dst->rm._11 = c;
	dst->rm._12 = 0;
	dst->rm._13 = -s;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = s;
	dst->rm._32 = 0;
	dst->rm._33 = c;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
void mat_rotation_y_fast( Matrix *dst, float y )
{
	float c, s;

	c = cos_fast( y );
	s = sin_fast( y );

	dst->rm._11 = c;
	dst->rm._12 = 0;
	dst->rm._13 = -s;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = s;
	dst->rm._32 = 0;
	dst->rm._33 = c;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}

void mat_rotation_z( Matrix *dst, float z )
{
	float c, s;

	c = ( float )cos( z );
	s = ( float )sin( z );

	dst->rm._11 = c;
	dst->rm._12 = -s;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = s;
	dst->rm._22 = c;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
void mat_rotation_z_fast( Matrix *dst, float z )
{
	float c, s;

	c = cos_fast( z );
	s = sin_fast( z );

	dst->rm._11 = c;
	dst->rm._12 = -s;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = s;
	dst->rm._22 = c;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}

void mat_rotation3_x( Matrix *dst, float x )
{
	float c, s;

	c = ( float )cos( x );
	s = ( float )sin( x );

	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = c;
	dst->rm._23 = -s;

	dst->rm._31 = 0;
	dst->rm._32 = s;
	dst->rm._33 = c;
}
void mat_rotation3_x_fast( Matrix *dst, float x )
{
	float c, s;

	c = cos_fast( x );
	s = sin_fast( x );

	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = c;
	dst->rm._23 = -s;

	dst->rm._31 = 0;
	dst->rm._32 = s;
	dst->rm._33 = c;
}

void mat_rotation3_y( Matrix *dst, float y )
{
	float c, s;

	c = ( float )cos( y );
	s = ( float )sin( y );

	dst->rm._11 = c;
	dst->rm._12 = 0;
	dst->rm._13 = -s;

	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;

	dst->rm._31 = s;
	dst->rm._32 = 0;
	dst->rm._33 = c;
}
void mat_rotation3_y_fast( Matrix *dst, float y )
{
	float c, s;

	c = cos_fast( y );
	s = sin_fast( y );

	dst->rm._11 = c;
	dst->rm._12 = 0;
	dst->rm._13 = -s;

	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;

	dst->rm._31 = s;
	dst->rm._32 = 0;
	dst->rm._33 = c;
}

void mat_rotation3_z( Matrix *dst, float z )
{
	float c, s;

	c = ( float )cos( z );
	s = ( float )sin( z );

	dst->rm._11 = c;
	dst->rm._12 = -s;
	dst->rm._13 = 0;

	dst->rm._21 = s;
	dst->rm._22 = c;
	dst->rm._23 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
}
void mat_rotation3_z_fast( Matrix *dst, float z )
{
	float c, s;

	c = cos_fast( z );
	s = sin_fast( z );

	dst->rm._11 = c;
	dst->rm._12 = -s;
	dst->rm._13 = 0;

	dst->rm._21 = s;
	dst->rm._22 = c;
	dst->rm._23 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
}

void mat_translate_ex( Matrix *dst, const Vec3 *trans )
{
	dst->rm._41 = trans->x;
	dst->rm._42 = trans->y;
	dst->rm._43 = trans->z;
}
void mat_translate( Matrix *dst, float x, float y, float z )
{
	dst->rm._41 = x;
	dst->rm._42 = y;
	dst->rm._43 = z;
}

void mat_perspective( Matrix *dst, float fov, float aspect, float zn, float zf )
{
	float sx, sy, a, b;

	sy = 1 / tan( 0.5 * fov );
	sx = sy / aspect;

	a = zf / ( zf - zn );
	b = -zn * zf / ( zf - zn );

	dst->rm._11 = sx;
	dst->rm._12 = 0;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = sy;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = a;
	dst->rm._34 = 1;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = b;
	dst->rm._44 = 0;
}

void mat_scale3_ex( Matrix *dst, const Vec3 *scale )
{
	dst->rm._11 *= scale->x;
	dst->rm._12 *= scale->y;
	dst->rm._13 *= scale->z;

	dst->rm._21 *= scale->x;
	dst->rm._22 *= scale->y;
	dst->rm._23 *= scale->z;

	dst->rm._31 *= scale->x;
	dst->rm._32 *= scale->y;
	dst->rm._33 *= scale->z;
}
void mat_scale3( Matrix *dst, float x, float y, float z )
{
	dst->rm._11 *= x;
	dst->rm._12 *= y;
	dst->rm._13 *= z;

	dst->rm._21 *= x;
	dst->rm._22 *= y;
	dst->rm._23 *= z;

	dst->rm._31 *= x;
	dst->rm._32 *= y;
	dst->rm._33 *= z;
}

void mat_turn_x( Matrix *dst, float x )
{
	float C, S;
	float b, c, e, f, h, i, cS, fS, iS;

	C = cosf( x );
	S = sinf( x );

	b = dst->rm._12;
	c = dst->rm._13;
	e = dst->rm._22;
	f = dst->rm._23;
	h = dst->rm._32;
	i = dst->rm._33;

	cS = c * S;
	fS = f * S;
	iS = i * S;

	dst->rm._12 = b * C + cS;
	dst->rm._13 = b * -S + cS;
	dst->rm._22 = e * C + fS;
	dst->rm._23 = e * -S + fS;
	dst->rm._32 = h * C + iS;
	dst->rm._33 = h * -S + iS;
}

void mat_turn_x_fast( Matrix *dst, float x )
{
	float C, S;
	float b, c, e, f, h, i, cS, fS, iS;

	C = cos_fast( x );
	S = sin_fast( x );

	b = dst->rm._12;
	c = dst->rm._13;
	e = dst->rm._22;
	f = dst->rm._23;
	h = dst->rm._32;
	i = dst->rm._33;

	cS = c * S;
	fS = f * S;
	iS = i * S;

	dst->rm._12 = b * C + cS;
	dst->rm._13 = b * -S + cS;
	dst->rm._22 = e * C + fS;
	dst->rm._23 = e * -S + fS;
	dst->rm._32 = h * C + iS;
	dst->rm._33 = h * -S + iS;
}

void mat_turn_y( Matrix *dst, float y )
{
	float C, S;
	float a, c, d, f, g, i;

	C = cosf( y );
	S = sinf( y );

	a = dst->rm._11;
	c = dst->rm._13;
	d = dst->rm._21;
	f = dst->rm._23;
	g = dst->rm._31;
	i = dst->rm._33;

	dst->rm._11 = a * C + c * S;
	dst->rm._13 = a * -S + c * C;
	dst->rm._21 = d * C + f * S;
	dst->rm._23 = d * -S + f * C;
	dst->rm._31 = g * C + i * S;
	dst->rm._33 = g * -S + i * C;
}

void mat_turn_y_fast( Matrix *dst, float y )
{
	float C, S;
	float a, c, d, f, g, i;

	C = cos_fast( y );
	S = sin_fast( y );

	a = dst->rm._11;
	c = dst->rm._13;
	d = dst->rm._21;
	f = dst->rm._23;
	g = dst->rm._31;
	i = dst->rm._33;

	dst->rm._11 = a * C + c * S;
	dst->rm._13 = a * -S + c * C;
	dst->rm._21 = d * C + f * S;
	dst->rm._23 = d * -S + f * C;
	dst->rm._31 = g * C + i * S;
	dst->rm._33 = g * -S + i * C;
}

void mat_turn_z( Matrix *dst, float z )
{
	float C, S;
	float a, b, d, e, g, h;

	C = cosf( z );
	S = sinf( z );

	a = dst->rm._11;
	b = dst->rm._12;
	d = dst->rm._21;
	e = dst->rm._22;
	g = dst->rm._31;
	h = dst->rm._32;

	dst->rm._11 = a * C + b * S;
	dst->rm._12 = a * -S + b * C;
	dst->rm._21 = d * C + e * S;
	dst->rm._22 = d * -S + e * C;
	dst->rm._31 = g * C + h * S;
	dst->rm._32 = g * -S + h * C;
}

void mat_turn_z_fast( Matrix *dst, float z )
{
	float C, S;
	float a, b, d, e, g, h;

	C = cos_fast( z );
	S = sin_fast( z );

	a = dst->rm._11;
	b = dst->rm._12;
	d = dst->rm._21;
	e = dst->rm._22;
	g = dst->rm._31;
	h = dst->rm._32;

	dst->rm._11 = a * C + b * S;
	dst->rm._12 = a * -S + b * C;
	dst->rm._21 = d * C + e * S;
	dst->rm._22 = d * -S + e * C;
	dst->rm._31 = g * C + h * S;
	dst->rm._32 = g * -S + h * C;
}

void mat_turn_ex( Matrix *dst, const Vec3 *p )
{
	float a, b, c, d, e, f, g, h, i;
	float r, s, t, u, v, w, x, y, z;
	float cx, cy, cz, sx, sy, sz;

	cx = cosf( p->x );
	cy = cosf( p->y );
	cz = cosf( p->z );

	sx = sinf( p->x );
	sy = sinf( p->y );
	sz = sinf( p->z );

	a = cz * cy + sz * sx * sy;
	b = sz * cx;
	c = cz * -sy + sz * sx * sy;

	d = sz * cy + cz * -sx * sy;
	e = cz * cx;
	f = sz * -sy + cz * -sx * cy;

	g = sx * sy;
	h = sx;
	i = sx * cy;

	r = dst->rm._11;
	s = dst->rm._12;
	t = dst->rm._13;

	u = dst->rm._21;
	v = dst->rm._22;
	w = dst->rm._23;

	x = dst->rm._31;
	y = dst->rm._32;
	z = dst->rm._33;

	dst->rm._11 = a * r + b * u + c * x;
	dst->rm._12 = a * s + b * v + c * y;
	dst->rm._13 = a * t + b * w + c * z;

	dst->rm._21 = d * r + e * u + f * x;
	dst->rm._22 = d * s + e * v + f * y;
	dst->rm._23 = d * t + e * w + f * z;

	dst->rm._31 = g * r + h * u + i * x;
	dst->rm._32 = g * s + h * v + i * y;
	dst->rm._33 = g * t + h * w + i * z;
}
void mat_turn( Matrix *dst, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	mat_turn_ex( dst, &v );
}

void mat_turn_fast_ex( Matrix *dst, const Vec3 *p )
{
	float a, b, c, d, e, f, g, h, i;
	float r, s, t, u, v, w, x, y, z;
	float cx, cy, cz, sx, sy, sz;

	cx = cos_fast( p->x );
	cy = cos_fast( p->y );
	cz = cos_fast( p->z );

	sx = sin_fast( p->x );
	sy = sin_fast( p->y );
	sz = sin_fast( p->z );

	a = cz * cy + sz * sx * sy;
	b = sz * cx;
	c = cz * -sy + sz * sx * sy;

	d = sz * cy + cz * -sx * sy;
	e = cz * cx;
	f = sz * -sy + cz * -sx * cy;

	g = sx * sy;
	h = sx;
	i = sx * cy;

	r = dst->rm._11;
	s = dst->rm._12;
	t = dst->rm._13;

	u = dst->rm._21;
	v = dst->rm._22;
	w = dst->rm._23;

	x = dst->rm._31;
	y = dst->rm._32;
	z = dst->rm._33;

	dst->rm._11 = a * r + b * u + c * x;
	dst->rm._12 = a * s + b * v + c * y;
	dst->rm._13 = a * t + b * w + c * z;

	dst->rm._21 = d * r + e * u + f * x;
	dst->rm._22 = d * s + e * v + f * y;
	dst->rm._23 = d * t + e * w + f * z;

	dst->rm._31 = g * r + h * u + i * x;
	dst->rm._32 = g * s + h * v + i * y;
	dst->rm._33 = g * t + h * w + i * z;
}
void mat_turn_fast( Matrix *dst, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	mat_turn_fast_ex( dst, &v );
}

void mat_move_x( Matrix *dst, float x )
{
	dst->rm._41 += dst->rm._11 * x;
	dst->rm._42 += dst->rm._12 * x;
	dst->rm._43 += dst->rm._13 * x;
}
void mat_move_y( Matrix *dst, float y )
{
	dst->rm._41 += dst->rm._21 * y;
	dst->rm._42 += dst->rm._22 * y;
	dst->rm._43 += dst->rm._23 * y;
}
void mat_move_z( Matrix *dst, float z )
{
	dst->rm._41 += dst->rm._31 * z;
	dst->rm._42 += dst->rm._32 * z;
	dst->rm._43 += dst->rm._33 * z;
}

void mat_move_ex( Matrix *dst, const Vec3 *v )
{
	dst->rm._41 += dst->rm._11 * v->x + dst->rm._21 * v->y + dst->rm._31 * v->z;
	dst->rm._42 += dst->rm._12 * v->x + dst->rm._22 * v->y + dst->rm._32 * v->z;
	dst->rm._43 += dst->rm._13 * v->x + dst->rm._23 * v->y + dst->rm._33 * v->z;
}
void mat_move( Matrix *dst, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;

	mat_move_ex( dst, &v );
}

void mat_rotate_ex( Matrix *dst, const Vec3 *p )
{
	float cx, cy, cz, sx, sy, sz;

	cx = cosf( p->x );
	cy = cosf( p->y );
	cz = cosf( p->z );

	sx = sinf( p->x );
	sy = sinf( p->y );
	sz = sinf( p->z );

	dst->rm._11 = cz * cy + sz * sx * sy;
	dst->rm._12 = sz * cx;
	dst->rm._13 = cz * -sy + sz * sx * sy;

	dst->rm._21 = sz * cy + cz * -sx * sy;
	dst->rm._22 = cz * cx;
	dst->rm._23 = sz * -sy + cz * -sx * cy;

	dst->rm._31 = sx * sy;
	dst->rm._32 = sx;
	dst->rm._33 = sx * cy;
}
void mat_rotate( Matrix *dst, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	mat_rotate_ex( dst, &v );
}

void mat_rotate_fast_ex( Matrix *dst, const Vec3 *p )
{
	float cx, cy, cz, sx, sy, sz;

	cx = cosf( p->x );
	cy = cosf( p->y );
	cz = cosf( p->z );

	sx = sinf( p->x );
	sy = sinf( p->y );
	sz = sinf( p->z );

	dst->rm._11 = cz * cy + sz * sx * sy;
	dst->rm._12 = sz * cx;
	dst->rm._13 = cz * -sy + sz * sx * sy;

	dst->rm._21 = sz * cy + cz * -sx * sy;
	dst->rm._22 = cz * cx;
	dst->rm._23 = sz * -sy + cz * -sx * cy;

	dst->rm._31 = sx * sy;
	dst->rm._32 = sx;
	dst->rm._33 = sx * cy;
}
void mat_rotate_fast( Matrix *dst, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	mat_rotate_fast_ex( dst, &v );
}
