#ifndef GB_SYS_FS_H
#define GB_SYS_FS_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"
#include "../base/handle.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* File System */
typedef enum {
	kGBVFSDriver_System, /*native file system access*/
	kGBVFSDriver_Device, /*device file system*/

	kGBVFSDriver_Memory, /*in memory file system*/
	kGBVFSDriver_ExecPack, /*internal executable file system*/

	kGBVFSDriver_Archive, /*reading from an archive*/
	kGBVFSDriver_EMP, /*reading from a native archive*/

	kGBVFSDriver_Network /*network based*/
} gb_vfsDriverType_t;

enum {
	kGBFileType_File_Bit = (1<<0),
	kGBFileType_Dir_Bit = (1<<1),
	kGBFileType_SymLink_Bit = (1<<2),
	kGBFileType_Dev_Bit = (1<<3)
};

enum {
	kGBFileAcc_Read_Bit = (1<<0),
	kGBFileAcc_Write_Bit = (1<<1),

	kGBFileAcc_Query_Bit = (1<<2), /*only check to see if can open*/
	kGBFileAcc_DenySymlink_Bit = (1<<3), /*symbolic links not permitted*/
	kGBFileAcc_Append_Bit = (1<<4), /*append access requested*/

	kGBFileAcc_ReadWrite_Mask = kGBFileAcc_Read_Bit|kGBFileAcc_Write_Bit
};

typedef gb_handle_t gb_vfs_t, gb_file_t, gb_dir_t;

typedef struct gb_fileStat_s {
	char filename[256]; /* platform-independent file name (not OS filename) */
	gb_uint_t fileType; /* file, dir, symlink... kGBFileType_*_Bit(s) */

	gb_u64_t fileSize;

	gb_u64_t lastAccessTime;
	gb_u64_t lastModifyTime;
	gb_u64_t lastChangeTime;
} gb_fileStat_t;

#define GB_VFSFN(n,r,p) typedef r(*gb_fnVFS##n##_t)p
#define GB_VFSFN_LIST()\
	GB_VFSFN(PathPhysicalToVirtual, size_t,(char *,size_t,const char *));\
	GB_VFSFN(PathVirtualToPhysical, size_t,(char *,size_t,const char *));\
	\
	GB_VFSFN(CopyFile, gb_bool_t,(const char *,const char *));\
	GB_VFSFN(RenameFile, gb_bool_t,(const char *,const char *));\
	GB_VFSFN(DeleteFile, gb_bool_t,(const char *));\
	GB_VFSFN(StatFile, gb_bool_t,(const char *,gb_fileStat_t *));\
	\
	GB_VFSFN(OpenDir, gb_dir_t,(const char *));\
	GB_VFSFN(ReadDir, gb_bool_t,(gb_dir_t,gb_fileStat_t *));\
	\
	GB_VFSFN(OpenFile, gb_file_t,(const char *,gb_uint_t));\
	GB_VFSFN(ReadFile, size_t,(gb_file_t,void *,size_t,size_t));\
	GB_VFSFN(WriteFile, size_t,(gb_file_t,const void *,size_t,size_t));\
	GB_VFSFN(SetFilePos, gb_bool_t,(gb_file_t,gb_u64_t));\
	GB_VFSFN(GetFilePos, gb_u64_t,(gb_file_t));\
	\
	GB_VFSFN(GetNativeDirHandle, void *,(gb_dir_t));\
	GB_VFSFN(GetNativeFileHandle, void *,(gb_file_t));\
	\
	GB_VFSFN(GetExtensionCount, size_t,());\
	GB_VFSFN(GetExtensionName, const char *,(size_t));\
	GB_VFSFN(GetExtensionProc, gb_fnPtr_t,(const char *));\
	\
	GB_VFSFN(QueryString, size_t,(const char *,void *,size_t));\
	GB_VFSFN(QueryPName, size_t,(gb_uint_t,void *,size_t))
GB_VFSFN_LIST();
#undef GB_VFSFN

typedef struct gb_vfsDriverInfo_s {
	gb_vfsDriverType_t driverType;

	const char *proto; /* supported protocol */
	const char *ext; /* supported extension */
} gb_vfsDriverInfo_t;

typedef struct gb_vfsDriverDesc_s {
	gb_vfsDriverInfo_t info;

	gb_handleType_t fileHandleType;
	gb_handleType_t dirHandleType;

#define GB_VFSFN(n,r,p) gb_fnVFS##n##_t fn##n
	GB_VFSFN_LIST();
#undef GB_VFSFN
} gb_vfsDriverDesc_t;

/* -------------------------------------------------------------------------- */
/* DRIVER/PROTOCOL */

gb_handleType_t gbGetBaseFileHandleType();
gb_handleType_t gbGetBaseDirHandleType();

gb_vfs_t gbRegisterVFS(gb_vfsDriverDesc_t *drvDsc);
gb_vfs_t gbUnregisterVFS(gb_vfs_t vfs);

gb_vfs_t gbGetVFS(const char *proto);
gb_vfs_t gbFirstVFS();
gb_vfs_t gbLastVFS();
gb_vfs_t gbVFSBefore(gb_vfs_t vfs);
gb_vfs_t gbVFSAfter(gb_vfs_t vfs);

size_t gbGetVFSExtensionCount(gb_vfs_t vfs);
const char *gbGetVFSExtensionName(gb_vfs_t vfs, size_t i);
gb_fnPtr_t gbGetVFSExtensionProc(gb_vfs_t vfs, const char *sym);
gb_bool_t gbIsVFSExtensionSupported(gb_vfs_t vfs, const char *name);

size_t gbVFSQueryString(gb_vfs_t vfs, const char *qry, void *p, size_t n);
size_t gbVFSQueryPName(gb_vfs_t vfs, gb_uint_t pname, void *p, size_t n);

/* -------------------------------------------------------------------------- */
/* USER */

size_t gbPathPhysicalToVirtual(char *dst, size_t dstn, const char *path);
size_t gbPathVirtualToPhysical(char *dst, size_t dstn, const char *path);

gb_bool_t gbCopyFile(const char *src, const char *dst);
gb_bool_t gbRenameFile(const char *src, const char *dst);
gb_bool_t gbDeleteFile(const char *path);
gb_bool_t gbStatFile(const char *path, gb_fileStat_t *fileInfo);

gb_dir_t gbOpenDir(const char *path);
gb_dir_t gbCloseDir(gb_dir_t dir);
gb_bool_t gbReadDir(gb_dir_t dir, gb_fileStat_t *fileInfo);

gb_file_t gbOpenFile(const char *path, gb_uint_t flags);
gb_file_t gbCloseFile(gb_file_t file);
size_t gbReadFile(gb_file_t file, void *dst, size_t size, size_t num);
size_t gbWriteFile(gb_file_t file, const void *src, size_t size, size_t num);
gb_bool_t gbSetFilePos(gb_file_t file, gb_u64_t pos);
gb_u64_t gbGetFilePos(gb_file_t file);

void *gbGetNativeDirHandle(gb_dir_t dir);
void *gbGetNativeFileHandle(gb_file_t file);

size_t gbReadFileLE(gb_file_t file, void *dst, size_t size, size_t num);
size_t gbReadFileBE(gb_file_t file, void *dst, size_t size, size_t num);
size_t gbWriteFileLE(gb_file_t file, const void *src, size_t size, size_t num);
size_t gbWriteFileBE(gb_file_t file, const void *src, size_t size, size_t num);

gb_bool_t gbWrite8(gb_file_t file, gb_u8_t x);
gb_bool_t gbRead8(gb_file_t file, gb_u8_t *x);

gb_bool_t gbWrite16(gb_file_t file, gb_u16_t x);
gb_bool_t gbWrite32(gb_file_t file, gb_u32_t x);
gb_bool_t gbWrite64(gb_file_t file, gb_u64_t x);
gb_bool_t gbRead16(gb_file_t file, gb_u16_t *x);
gb_bool_t gbRead32(gb_file_t file, gb_u32_t *x);
gb_bool_t gbRead64(gb_file_t file, gb_u64_t *x);

gb_bool_t gbWriteL16(gb_file_t file, gb_u16_t x);
gb_bool_t gbWriteL32(gb_file_t file, gb_u32_t x);
gb_bool_t gbWriteL64(gb_file_t file, gb_u64_t x);
gb_bool_t gbReadL16(gb_file_t file, gb_u16_t *x);
gb_bool_t gbReadL32(gb_file_t file, gb_u32_t *x);
gb_bool_t gbReadL64(gb_file_t file, gb_u64_t *x);

gb_bool_t gbWriteB16(gb_file_t file, gb_u16_t x);
gb_bool_t gbWriteB32(gb_file_t file, gb_u32_t x);
gb_bool_t gbWriteB64(gb_file_t file, gb_u64_t x);
gb_bool_t gbReadB16(gb_file_t file, gb_u16_t *x);
gb_bool_t gbReadB32(gb_file_t file, gb_u32_t *x);
gb_bool_t gbReadB64(gb_file_t file, gb_u64_t *x);

gb_bool_t gbWriteString(gb_file_t file, const char *src);
gb_bool_t gbReadString(gb_file_t file, char *dst, size_t dstn);

gb_bool_t gbWriteFloat(gb_file_t file, float x);
gb_bool_t gbReadFloat(gb_file_t file, float *x);

gb_bool_t gbWriteDouble(gb_file_t file, double x);
gb_bool_t gbReadDouble(gb_file_t file, double *x);

gb_bool_t gbIsEOF(gb_file_t file);
gb_bool_t gbSkipFile(gb_file_t file, gb_i64_t amnt);

gb_dir_t gbFirstOpenDir();
gb_dir_t gbLastOpenDir();
gb_dir_t gbOpenDirBefore(gb_dir_t dir);
gb_dir_t gbOpenDirAfter(gb_dir_t dir);

gb_file_t gbFirstOpenFile();
gb_file_t gbLastOpenFile();
gb_file_t gbOpenFileBefore(gb_file_t file);
gb_file_t gbOpenFileAfter(gb_file_t file);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
