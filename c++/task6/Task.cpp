//
//	TASK STREAMING - VERSION 6
//

//------------------------------------------------------------------------------
// INCLUDES

#include "../taskshare/versions/Task6.h"

#include "../taskshare/test/IsPrime.cpp"

#ifndef NUM_TESTS
# if 1
#  define NUM_TESTS 1024
# else
#  define NUM_TESTS 4
# endif
#endif

//------------------------------------------------------------------------------
//	FRAMEWORK FUNCTIONS

void InitAppHangQuery();
void FiniAppHangQuery();
void InitThreads(task::threadFunc_t fn, void *p);
void FiniThreads();

//------------------------------------------------------------------------------
//	APP

int g_numbers[NUM_TESTS];
task::queue_t g_mainQueue;

//------------------------------------------------------------------------------
//	THREAD

task::threadResult_t THREADPROCAPI WorkerThread_f(void *) {
	task::Streamer(g_mainQueue);
	return (task::threadResult_t)NULL;
}

//------------------------------------------------------------------------------
//	MAIN TEST

// initialize task streaming
void Init() {
	// initialize the queue
	task::InitQueue(g_mainQueue);

	// initialize the threads
	InitThreads(WorkerThread_f, (void *)NULL);
}

// finish task streaming
void Fini() {
	// finished with the queue
	task::FiniQueue(g_mainQueue);
}

// perform the main code (this part is benchmarked)
void Run() {
	size_t i;

	// NOTE: threads before tasks if queue is not large enough
	for(i=0; i<NUM_TESTS; i++) {
		g_numbers[i] = 100000000 + i;
		task::Schedule(g_mainQueue, IsPrime_f, &g_numbers[i]);
	}

	// wait for the streamers to finish
	//InitAppHangQuery();
	task::SyncQueue(g_mainQueue);
	//FiniAppHangQuery();
}

// debugging test
static void TASK_CONF_JOBPROCAPI Job_f(void *p) {
	task::u32 x;

	x = task::AtomicIncrement((task::u32 *)p); 
	printf("%u;", x);
}
void Debug() {
	task::u32 _t;
	size_t i, j;

	_t = 0;
	for(i=0; i<NUM_TESTS*8; i++) {
		for(j=0; j<NUM_TESTS; j++)
			task::Schedule(g_mainQueue, Job_f, &_t);

		InitAppHangQuery();
		task::SyncQueue(g_mainQueue);
		FiniAppHangQuery();
	}
}
