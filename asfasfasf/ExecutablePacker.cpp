﻿#include "ExecutablePacker.hpp"

void help() {
	fprintf( stderr, "ExecutablePacker destName stubName [FILES...]\n" );
	exit( EXIT_FAILURE );
}

int main( int argc, char **argv ) {
	Ax::ExecutablePacker packer;

	if( argc < 3 ) {
		help();
	}
	
	const char *destName = argv[ 1 ];
	const char *stubName = argv[ 2 ];

	for( int i = 3; i < argc; ++i ) {
		packer.addFile( argv[ i ], argv[ i ] );
	}
	
	try {
		packer.pack< Ax::kExecPack_AppendExecutable >( stubName, destName );
	} catch( const Ax::Exception &except ) {
		fprintf( stderr, "ERROR: %s\n", except.getMessage() );
		exit( EXIT_FAILURE );
	} catch( ... ) {
		fprintf( stderr, "ERROR: (unknown)\n" );
		exit( EXIT_FAILURE );
	}
	
	return EXIT_SUCCESS;
}
