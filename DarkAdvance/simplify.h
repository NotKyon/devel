#ifndef SIMPLIFY_H
#define SIMPLIFY_H

#include "matrix.h"
#include "memory.h"
#include "entity.h"

// vectorsimplify.cpp
EXPORT vec3_t *NewVec3();
EXPORT vec4_t *NewVec4();
EXPORT void CopyVec3(vec3_t *dst, const vec3_t *src);
EXPORT void CopyVec4(vec4_t *dst, const vec4_t *src);
EXPORT vec3_t *DuplicateVec3(const vec3_t *src);
EXPORT vec4_t *DuplicateVec4(const vec4_t *src);
EXPORT void Vec_SetX(vec3_t *dst, float x);
EXPORT void Vec_SetY(vec3_t *dst, float y);
EXPORT void Vec_SetZ(vec3_t *dst, float z);
EXPORT void Vec_SetW(vec4_t *dst, float w);
EXPORT float Vec_X(const vec3_t *src);
EXPORT float Vec_Y(const vec3_t *src);
EXPORT float Vec_Z(const vec3_t *src);
EXPORT float Vec_W(const vec4_t *src);

// matrixsimplify.cpp
EXPORT matrix_t *NewMatrix();
EXPORT void CopyMatrix(matrix_t *dst, const matrix_t *src);
EXPORT matrix_t *DuplicateMatrix(const matrix_t *src);
EXPORT void Matrix_SetRow(matrix_t *dst, unsigned int rownum, const vec4_t *src);
EXPORT void Matrix_SetColumn(matrix_t *dst, unsigned int colnum, const vec4_t *src);
EXPORT void Matrix_SetElement(matrix_t *dst, unsigned int r, unsigned int c, float x);
EXPORT void Matrix_GetRow(vec4_t *dst, const matrix_t *src, unsigned int rownum);
EXPORT void Matrix_GetColumn(vec4_t *dst, const matrix_t *src, unsigned int colnum);
EXPORT float Matrix_GetElement(const matrix_t *src, unsigned int r, unsigned int c);
EXPORT float Matrix_11(const matrix_t *src);
EXPORT float Matrix_12(const matrix_t *src);
EXPORT float Matrix_13(const matrix_t *src);
EXPORT float Matrix_14(const matrix_t *src);
EXPORT float Matrix_21(const matrix_t *src);
EXPORT float Matrix_22(const matrix_t *src);
EXPORT float Matrix_23(const matrix_t *src);
EXPORT float Matrix_24(const matrix_t *src);
EXPORT float Matrix_31(const matrix_t *src);
EXPORT float Matrix_32(const matrix_t *src);
EXPORT float Matrix_33(const matrix_t *src);
EXPORT float Matrix_34(const matrix_t *src);
EXPORT float Matrix_41(const matrix_t *src);
EXPORT float Matrix_42(const matrix_t *src);
EXPORT float Matrix_43(const matrix_t *src);
EXPORT float Matrix_44(const matrix_t *src);

// quaternionsimplify.cpp
EXPORT quat_t *NewQuat();
EXPORT void CopyQuat(quat_t *dst, const quat_t *src);
EXPORT quat_t *DuplicateQuat(const quat_t *src);
EXPORT void Quat_Set(quat_t *dst, float w, float x, float y, float z);
EXPORT void Quat_SetW(quat_t *dst, float w);
EXPORT void Quat_SetX(quat_t *dst, float x);
EXPORT void Quat_SetY(quat_t *dst, float y);
EXPORT void Quat_SetZ(quat_t *dst, float z);
EXPORT float Quat_W(const quat_t *src);
EXPORT float Quat_X(const quat_t *src);
EXPORT float Quat_Y(const quat_t *src);
EXPORT float Quat_Z(const quat_t *src);

// entitysimplify.cpp
EXPORT entity_t NewEntity__NoParent();
EXPORT float EntityPositionX(entity_t ent);
EXPORT float EntityPositionY(entity_t ent);
EXPORT float EntityPositionZ(entity_t ent);
EXPORT float EntityOffsetPositionX(entity_t ent);
EXPORT float EntityOffsetPositionY(entity_t ent);
EXPORT float EntityOffsetPositionZ(entity_t ent);
EXPORT float EntityGlobalPositionX(entity_t ent);
EXPORT float EntityGlobalPositionY(entity_t ent);
EXPORT float EntityGlobalPositionZ(entity_t ent);
EXPORT float EntityPostPositionX(entity_t ent);
EXPORT float EntityPostPositionY(entity_t ent);
EXPORT float EntityPostPositionZ(entity_t ent);
EXPORT float CalculateEntityScaleX(entity_t ent);
EXPORT float CalculateEntityScaleY(entity_t ent);
EXPORT float CalculateEntityScaleZ(entity_t ent);
EXPORT float CalculateEntityOffsetScaleX(entity_t ent);
EXPORT float CalculateEntityOffsetScaleY(entity_t ent);
EXPORT float CalculateEntityOffsetScaleZ(entity_t ent);
EXPORT float CalculateEntityGlobalScaleX(entity_t ent);
EXPORT float CalculateEntityGlobalScaleY(entity_t ent);
EXPORT float CalculateEntityGlobalScaleZ(entity_t ent);
EXPORT float CalculateEntityPostScaleX(entity_t ent);
EXPORT float CalculateEntityPostScaleY(entity_t ent);
EXPORT float CalculateEntityPostScaleZ(entity_t ent);
EXPORT float EntityRotationX(entity_t ent);
EXPORT float EntityRotationY(entity_t ent);
EXPORT float EntityRotationZ(entity_t ent);
EXPORT float EntityOffsetRotationX(entity_t ent);
EXPORT float EntityOffsetRotationY(entity_t ent);
EXPORT float EntityOffsetRotationZ(entity_t ent);
EXPORT float EntityGlobalRotationX(entity_t ent);
EXPORT float EntityGlobalRotationY(entity_t ent);
EXPORT float EntityGlobalRotationZ(entity_t ent);
EXPORT float EntityPostRotationX(entity_t ent);
EXPORT float EntityPostRotationY(entity_t ent);
EXPORT float EntityPostRotationZ(entity_t ent);

#endif
