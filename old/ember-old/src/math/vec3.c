#include <ember/math.h>
#include <math.h>

const Vec3 *vec3_forward( void )
{
	static Vec3 v = { 0, 0, 1 };
	return &v;
}

const Vec3 *vec3_up( void )
{
	static Vec3 v = { 0, 1, 0 };
	return &v;
}

const Vec3 *vec3_right( void )
{
	static Vec3 v = { 1, 0, 0 };
	return &v;
}

float vec3_x( const Vec3 *src )
{
	return src->x;
}
float vec3_y( const Vec3 *src )
{
	return src->y;
}
float vec3_z( const Vec3 *src )
{
	return src->z;
}

void vec3_zero( Vec3 *dst )
{
	dst->x = dst->y = dst->z = 0;
}
void vec3_identity( Vec3 *dst )
{
	dst->x = dst->y = dst->z = 1;
}

void vec3_set( Vec3 *dst, float x, float y, float z )
{
	dst->x = x;
	dst->y = y;
	dst->z = z;
}

void vec3_copy( Vec3 *dst, const Vec3 *src )
{
	dst->x = src->x;
	dst->y = src->y;
	dst->z = src->z;
}

void vec3_add( Vec3 *dst, const Vec3 *a, const Vec3 *b )
{
	dst->x = a->x + b->x;
	dst->y = a->y + b->y;
	dst->z = a->z + b->z;
}
void vec3_subtract( Vec3 *dst, const Vec3 *a, const Vec3 *b )
{
	dst->x = a->x - b->x;
	dst->y = a->y - b->y;
	dst->z = a->z - b->z;
}
void vec3_multiply( Vec3 *dst, const Vec3 *a, const Vec3 *b )
{
	dst->x = a->x * b->x;
	dst->y = a->y * b->y;
	dst->z = a->z * b->z;
}
void vec3_divide( Vec3 *dst, const Vec3 *a, const Vec3 *b )
{
	dst->x = a->x / b->x;
	dst->y = a->y / b->y;
	dst->z = a->z / b->z;
}
void vec3_scale( Vec3 *dst, const Vec3 *a, float b )
{
	dst->x = a->x * b;
	dst->y = a->y * b;
	dst->z = a->z * b;
}

float vec3_dot( const Vec3 *a, const Vec3 *b )
{
	return a->x * b->x + a->y * b->y + a->z * b->z;
}
void vec3_cross( Vec3 *dst, const Vec3 *a, const Vec3 *b )
{
	dst->x = a->y * b->z - a->z * b->y;
	dst->y = a->z * b->x - a->x * b->z;
	dst->z = a->x * b->y - a->y * b->x;
}

void vec3_lerp_ex( Vec3 *dst, const Vec3 *a, const Vec3 *b, const Vec3 *w )
{
	dst->x = a->x + w->x * ( b->x - a->x );
	dst->y = a->y + w->y * ( b->y - a->y );
	dst->z = a->z + w->z * ( b->z - a->z );
}
void vec3_lerp( Vec3 *dst, const Vec3 *a, const Vec3 *b, float w )
{
	dst->x = a->x + w * ( b->x - a->x );
	dst->y = a->y + w * ( b->y - a->y );
	dst->z = a->z + w * ( b->z - a->z );
}

float vec3_length( const Vec3 *a )
{
	return sqrtf( a->x * a->x + a->y * a->y + a->z * a->z );
}
float vec3_length_fast( const Vec3 *a )
{
	return sqrt_fast( a->x * a->x + a->y * a->y + a->z * a->z );
}
float vec3_invlength( const Vec3 *a )
{
	return 1.0f / sqrtf( a->x * a->x + a->y * a->y + a->z * a->z );
}
float vec3_invlength_fast( const Vec3 *a )
{
	return invsqrt_fast( a->x * a->x + a->y * a->y + a->z * a->z );
}

int vec3_normalize( Vec3 *a )
{
	float r;

	r = sqrtf( a->x * a->x + a->y * a->y + a->z * a->z );
	if( !r )
		return 0;

	r = 1.0f / r;

	a->x *= r;
	a->y *= r;
	a->z *= r;

	return 1;
}
int vec3_normalize_fast( Vec3 *a )
{
	float r;

	r = invsqrt_fast( a->x * a->x + a->y * a->y + a->z * a->z );

	a->x *= r;
	a->y *= r;
	a->z *= r;

	return 1;
}

float vec3_distance( const Vec3 *a, const Vec3 *b )
{
	float x, y, z;

	x = a->x - b->x;
	y = a->y - b->y;
	z = a->z - b->z;

	return sqrtf( x * x + y * y + z * z );
}
float vec3_distance_fast( const Vec3 *a, const Vec3 *b )
{
	float x, y, z;

	x = a->x - b->x;
	y = a->y - b->y;
	z = a->z - b->z;

	return sqrt_fast( x * x + y * y + z * z );
}

void vec3_transform3_ex( Vec3 *dst, const Vec3 *a, const Matrix *b )
{
	dst->x = a->x * b->rm._11 + a->y * b->rm._21 + a->z * b->rm._31;
	dst->y = a->x * b->rm._12 + a->y * b->rm._22 + a->z * b->rm._32;
	dst->z = a->x * b->rm._13 + a->y * b->rm._23 + a->z * b->rm._33;
}
void vec3_transform_ex( Vec3 *dst, const Vec3 *a, const Matrix *b )
{
	dst->x = a->x * b->rm._11 + a->y * b->rm._21 + a->z * b->rm._31 + b->rm._41;
	dst->y = a->x * b->rm._12 + a->y * b->rm._22 + a->z * b->rm._32 + b->rm._42;
	dst->z = a->x * b->rm._13 + a->y * b->rm._23 + a->z * b->rm._33 + b->rm._43;
}
void vec3_transform3( Vec3 *dst, float x, float y, float z, const Matrix *b )
{
	Vec3 a;

	a.x = x;
	a.y = y;
	a.z = z;
	vec3_transform3_ex( dst, &a, b );
}
void vec3_transform( Vec3 *dst, float x, float y, float z, const Matrix *b )
{
	Vec3 a;

	a.x = x;
	a.y = y;
	a.z = z;
	vec3_transform_ex( dst, &a, b );
}

void vec3_translate( Vec3 *dst, const Matrix *src )
{
	dst->x = src->rm._41;
	dst->y = src->rm._42;
	dst->z = src->rm._43;
}

void vec3_point( Vec3 *dst, const Vec3 *a, const Vec3 *b )
{
	Matrix mat;
	Vec3 diff, vec, frontVec = { 0, 0, 1 };
	float x, y;

	dst->z = 0;

	vec3_subtract( &diff, b, a );

	vec.x = diff.x;
	vec.y = 0;
	vec.z = diff.z;

	if( vec3_normalize( &vec ) ) {
		y = vec3_dot( &vec, &frontVec );
		if( y > 1 )
			y = 1;
		if( y < -1 )
			y = -1;
		y = acos( y );
		if( vec.x < 0 )
			y = -y;
	} else {
		y = 0;
	}

	mat_rotation_y( &mat, -y );
	vec3_transform3_ex( &vec, &diff, &mat );

	if( vec3_normalize( &vec ) ) {
		x = vec3_dot( &vec, &frontVec );
		if( x > 1 )
			x = 1;
		if( x < -1 )
			x = -1;
		x = acos( x );
		if( vec.y > 0 )
			x = -x;
	} else {
		x = 0;
	}

	dst->x = x;
	dst->y = y;
}
void vec3_point_fast( Vec3 *dst, const Vec3 *a, const Vec3 *b )
{
	Matrix mat;
	Vec3 diff, vec, frontVec = { 0, 0, 1 };
	float x, y;

	dst->z = 0;

	vec3_subtract( &diff, b, a );

	vec.x = diff.x;
	vec.y = 0;
	vec.z = diff.z;

	vec3_normalize_fast( &vec );
	y = vec3_dot( &vec, &frontVec );
	if( y > 1 )
		y = 1;
	if( y < -1 )
		y = -1;
	y = acos_fast( y );
	if( vec.x < 0 )
		y = -y;

	mat_rotation_y( &mat, -y );
	vec3_transform3_ex( &vec, &diff, &mat );

	vec3_normalize_fast( &vec );
	x = vec3_dot( &vec, &frontVec );
	if( x > 1 )
		x = 1;
	if( x < -1 )
		x = -1;
	x = acos_fast( x );
	if( vec.y > 0 )
		x = -x;

	dst->x = x;
	dst->y = y;
}

void vec3_matrix_euler( Vec3 *dst, const Matrix *src )
{
	float v;

	v = -src->rm._32;

	if( v > 0.998 ) {
		dst->y = atan2f( src->rm._13, src->rm._11 );
		dst->x = 0.5 * M_PI;
		dst->z = 0;

		return;
	}

	if( v < -0.998 ) {
		dst->y = atan2f( src->rm._13, src->rm._11 );
		dst->x = -0.5 * M_PI;
		dst->z = 0;

		return;
	}

	dst->x = asinf( v );
	dst->y = atan2f( src->rm._31, src->rm._33 );
	dst->z = atan2f( src->rm._12, src->rm._22 );
}
void vec3_matrix_euler_fast( Vec3 *dst, const Matrix *src )
{
	float v;

	v = -src->rm._32;

	if( v > 0.998 ) {
		dst->y = atan2_fast( src->rm._13, src->rm._11 );
		dst->x = 0.5 * M_PI;
		dst->z = 0;

		return;
	}

	if( v < -0.998 ) {
		dst->y = atan2_fast( src->rm._13, src->rm._11 );
		dst->x = -0.5 * M_PI;
		dst->z = 0;

		return;
	}

	dst->x = asin_fast( v );
	dst->y = atan2_fast( src->rm._31, src->rm._33 );
	dst->z = atan2_fast( src->rm._12, src->rm._22 );
}
void vec3_quaternion_euler( Vec3 *dst, const Quaternion *src )
{
	Matrix mat;

	mat_quaternion( &mat, src );
	vec3_matrix_euler( dst, &mat );
}
void vec3_quaternion_euler_fast( Vec3 *dst, const Quaternion *src )
{
	Matrix mat;

	mat_quaternion( &mat, src );
	vec3_matrix_euler_fast( dst, &mat );
}
/* TODO: Handle it the proper way -- see below (unknown source) */
/*
 * const double w2 = q.w*q.w;
 * const double x2 = q.x*q.x;
 * const double y2 = q.y*q.y;
 * const double z2 = q.z*q.z;
 * const double unitLength = w2 + x2 + y2 + z2;    // Normalised == 1, otherwise
 * correction divisor.
 * const double abcd = q.w*q.x + q.y*q.z;
 * const double eps = 1e-7;    // TODO: pick from your math lib instead of
 * hardcoding.
 * const double pi = 3.14159265358979323846;   // TODO: pick from your math lib
 * instead of hardcoding.
 * if (abcd > (0.5-eps)*unitLength)
 * {
 * yaw = 2 * atan2(q.y, q.w);
 * pitch = pi;
 * roll = 0;
 * }
 * else if (abcd < (-0.5+eps)*unitLength)
 * {
 * yaw = -2 * ::atan2(q.y, q.w);
 * pitch = -pi;
 * roll = 0;
 * }
 * else
 * {
 * const double adbc = q.w*q.z - q.x*q.y;
 * const double acbd = q.w*q.y - q.x*q.z;
 * yaw = ::atan2(2*adbc, 1 - 2*(z2+x2));
 * pitch = ::asin(2*abcd/unitLength);
 * roll = ::atan2(2*acbd, 1 - 2*(y2+x2));
 * }
 */
void vec3_triangle_normal( Vec3 *dst, const Vec3 *vtxs )
{
	Vec3 a, b;

	vec3_subtract( &a, &vtxs[ 1 ], &vtxs[ 0 ] );
	vec3_subtract( &b, &vtxs[ 2 ], &vtxs[ 0 ] );

	vec3_cross( dst, &a, &b );
	vec3_normalize( dst );
}

void vec3_triangle_slide( Vec3 *dst, const Vec3 *src, const Vec3 *normal,
    float speed )
{
	static const Vec3 up = { 0, 1, 0 };

	Vec3 objdir, walldir;
	float d;

	vec3_copy( &objdir, src );
	vec3_normalize( &objdir );

	d = vec3_dot( normal, &objdir );
	vec3_cross( &walldir, normal, &up );
	vec3_normalize( &walldir );

	vec3_scale( dst, &walldir, speed * d );
}

void vec3_rotate_quaternion( Vec3 *dst, const Vec3 *src, const Quaternion *rot )
{
	Quaternion p, i, t;

	quat_set( &p, 0, src->x, src->y, src->z );

	quat_inverse( &i, rot );
	quat_multiply( &t, &i, &p );
	quat_multiply( &p, rot, &t );

	dst->x = p.x;
	dst->y = p.y;
	dst->z = p.z;
}

int vec3_in_triangle( const Vec3 *p, const Vec3 *verts, Vec2 *uv )
{
	Vec3 v[ 3 ];
	float d00, d01, d02, d11, d12;
	float r;

	vec3_subtract( &v[ 0 ], &verts[ 2 ], &verts[ 0 ] );
	vec3_subtract( &v[ 1 ], &verts[ 1 ], &verts[ 0 ] );
	vec3_subtract( &v[ 2 ], p, &verts[ 0 ] );

	d00 = vec3_dot( &v[ 0 ], &v[ 0 ] );
	d01 = vec3_dot( &v[ 0 ], &v[ 1 ] );
	d02 = vec3_dot( &v[ 0 ], &v[ 2 ] );
	d11 = vec3_dot( &v[ 1 ], &v[ 1 ] );
	d12 = vec3_dot( &v[ 1 ], &v[ 2 ] );

	r     = 1.0f / ( d00 * d11 - d01 * d01 );
	uv->x = ( d11 * d02 - d01 * d12 ) * r;
	uv->y = ( d00 * d12 - d01 * d02 ) * r;

	if( uv->x > 0 && uv->y > 0 && uv->x + uv->y < 1 )
		return 1;

	return 0;
}

void vec3_unproject( Vec3 *dst, const Vec3 *p, float width, float height,
    const Matrix *invwrldviewproj )
{
	Vec3 v;

	v.x = 2.0f * p->x / width - 1.0f;
	v.y = 1.0f - 2.0f * p->y / height;
	v.z = p->z;

	vec3_transform_ex( dst, &v, invwrldviewproj );
}
