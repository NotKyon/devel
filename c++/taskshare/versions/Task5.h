#ifndef TASK_H
#define TASK_H

#define TASK_CONF_NO_DEFAULT_TASK 1
#include "../TaskShare.h"

//------------------------------------------------------------------------------
// JOB QUEUE

#define TASK_JOBQUEUE_VERSION 5

#undef TASK_CONF_LOCKFREE
#define TASK_CONF_LOCKFREE 1

__TS_NS_INIT__

//----

typedef struct token_s {
	union { jobFunc_t fn; void *fnp; };
	void *p;
} token_t;
typedef struct queue_s {
#if !TASK_CONF_BUFFER_SIZE
# undef  TASK_CONF_BUFFER_SIZE
# define TASK_CONF_BUFFER_SIZE 4096
#elif TASK_CONF_BUFFER_SIZE > 65536
# error "JobQueue buffer size is too large."
#endif
	token_t cmdbuf[TASK_CONF_BUFFER_SIZE];
	bool terminate;

#if _MSC_VER || __INTEL_COMPILER
# define __align(x) __declspec(align(x))
#else
# define __align(x) __attribute__((aligned(x)))
#endif
	__align(32) volatile u32 numStreamers;
	__align(32) volatile u32 numPendingJobs;

	__align(32) volatile u32 insertionIndex;
	__align(32) volatile u32 streamingIndex;
} queue_t;

//----

inline void InitQueue(queue_t &q);
inline void FiniQueue(queue_t &q);

inline void EnqueueToken(queue_t &q, const token_t &t);
inline void Schedule(queue_t &q, jobFunc_t fn, void *p);
inline void ScheduleTermination(queue_t &q);

inline bool Stream(queue_t &q, token_t &t);
inline void Streamer(queue_t &q);

inline void SyncQueue(queue_t &q);
inline void SyncQueueActive(queue_t &q);

//----

inline void InitQueue(queue_t &q) {
	size_t i;

	for(i=0; i<TASK_CONF_BUFFER_SIZE; i++) {
		q.cmdbuf[i].fnp = (void *)NULL;
		q.cmdbuf[i].p   = (void *)NULL;
	}
	q.terminate = false;
	q.numStreamers = 0;
	q.numPendingJobs = 0;
	q.insertionIndex = 0;
	q.streamingIndex = 0;
}
inline void FiniQueue(queue_t &q) {
	ScheduleTermination(q);

	while(AtomicRead(&q.numStreamers) > 0)
		Relinquish();
}

inline void EnqueueToken(queue_t &q, const token_t &t) {
	u32 index;

	AtomicIncrement(&q.numPendingJobs);

	index = AtomicIncrement(&q.insertionIndex)%TASK_CONF_BUFFER_SIZE;

	while(AtomicRead(&q.cmdbuf[index].fnp) != (void *)NULL) {
		if (q.terminate)
			return;

		Relinquish();
	}

	AtomicExchange(&q.cmdbuf[index].p  , t.p  );
	AtomicExchange(&q.cmdbuf[index].fnp, t.fnp);
}
inline void Schedule(queue_t &q, jobFunc_t fn, void *p) {
	token_t t;

	t.fn = fn;
	t.p  = p ;

	EnqueueToken(q, t);
}
inline void ScheduleTermination(queue_t &q) {
	q.terminate = true;
}

inline bool Stream(queue_t &q, token_t &t) {
	u32 index;

	index = AtomicIncrement(&q.streamingIndex)%TASK_CONF_BUFFER_SIZE;

	while((t.fnp = AtomicRead(&q.cmdbuf[index].fnp))==(void *)NULL) {
		if (q.terminate)
			return false;

		Relinquish();
	}

	t.p = AtomicRead(&q.cmdbuf[index].p);
	AtomicExchange(&q.cmdbuf[index].fnp, (void *)NULL);

	return true;
}
inline void Streamer(queue_t &q) {
	token_t t;

	AtomicIncrement(&q.numStreamers);

	while(Stream(q, t)) {
		t.fn(t.p);
		AtomicDecrement(&q.numPendingJobs);
	}

	AtomicDecrement(&q.numStreamers);
}

inline void SyncQueue(queue_t &q) {
	while(AtomicRead(&q.numPendingJobs) > 0)
		Relinquish();
}
inline void SyncQueueActive(queue_t &q) {
	while(AtomicRead(&q.numPendingJobs) > 0) {
	}
}

__TS_NS_FINI__

#endif
