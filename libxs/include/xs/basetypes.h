#ifndef XS_BASETYPES_H
#define XS_BASETYPES_H

#include <stdarg.h>
#include <stddef.h>

#ifndef XS_DEBUG
# if DEBUG||_DEBUG||__debug__
#  define XS_DEBUG 1
# endif
#endif

typedef int xs_bool_t;
#define XS_TRUE (1)
#define XS_FALSE (0)

typedef unsigned int xs_uint_t;

typedef size_t xs_size_t; /*TODO: define to size of pointer*/

#endif
