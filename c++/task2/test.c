
unsigned int InterlockedOr(unsigned int *volatile x, unsigned int y) {
/*
	__asm__ __volatile__("lock; orl %1, %0"
		: "=m" (*x)
		: "r" (y), "m" (*x)
		: "memory");
*/
	return __sync_fetch_and_or(x, y);
}
