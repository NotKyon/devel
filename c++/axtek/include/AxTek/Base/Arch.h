#ifndef AXTEK_BASE_ARCH_H
#define AXTEK_BASE_ARCH_H

#if _MSC_VER > 1000
# pragma once
#endif

#if __amd64__ || __amd64 || __x86_64__ || __x86_64 || _M_X64 || _M_AMD64
# define AXTEK_ARCH_AMD64   1
# define AXTEK_ARCH_X86     1
#elif __arm__ || __thumb__ || __TARGET_ARCH_ARM || _ARM
# define AXTEK_ARCH_ARM     1
#elif __i386__ || __i386 || _M_IX86 || __x86__ || _x86_ || __I86__ || __INTEL__
# define AXTEK_ARCH_X86     1
#elif __mips__ || __mips || __MIPS__ || _MIPS_ISA
# define AXTEK_ARCH_MIPS    1
#elif __powerpc__ || __POWERPC__ || __ppc__ || __ppc || _M_PPC || _XENON
# define AXTEK_ARCH_PPC     1
#else
# define AXTEK_ARCH_UNKNOWN 1
#endif

#if _WIN64 || AXTEK_ARCH_AMD64 || __LP64__
# define AXTEK_ARCH_BITS    64
#else
# ifndef _WIN32
#  include <limits.h>
#  define AXTEK_ARCH_BITS   __WORDSIZE
# else
#  define AXTEK_ARCH_BITS    32
# endif
#endif

#endif
