#include "mk_dir.h"
#include "mk_utility.h"

const char *SystemSpecialDir(int sys) {
	switch(sys) {
	case kProjSys_MSWin          : return "mswin" ;
	case kProjSys_Linux          : return "linux" ;
	case kProjSys_MacOS          : return "macos" ;
	default:
		break;
	}

	return "";
}
const char *ArchitectureSpecialDir(int arch) {
	switch(arch) {
	case kProjArch_X86           : return "x86"   ;
	case kProjArch_X86_64        : return "x86_64";
	case kProjArch_ARM           : return "arm"   ;
	case kProjArch_PowerPC       : return "ppc"   ;
	case kProjArch_MIPS          : return "mips"  ;
	default:
		break;
	}

	return "";
}
const char *ProjectTypeSpecialDir(int type) {
	switch(type) {
	case kProjType_Executable    : return "exec"  ;
	case kProjType_Library       : return "lib"   ;
	case kProjType_DynamicLibrary: return "dylib" ;
	default:
		break;
	}

	return "";
}

int IsSpecialDir(project_t proj, const char *name) {
	assert(proj != (project_t)0);
	assert(name != (const char *)0);

	if (!strcmp(SystemSpecialDir(proj->sys), name))
		return 1;

	if (!strcmp(ArchitectureSpecialDir(proj->arch), name))
		return 1;

	if (!strcmp(ProjectTypeSpecialDir(proj->type), name))
		return 1;

	/*
	 * PLAN: Add a ".freestanding" (or perhaps ".native?") indicator file to
	 *       support OS/kernel development. Would be cool.
	 */

	return 0;
}

void EnumSpecialLibDirs(project_t proj) {
	const char *libdir, *spdirs[3];
	xs_path_t dir;
	size_t i, j, n;

	assert(proj != (project_t)0);

	n = XsArraySize(proj->libdirs);
	for(i=0; i<n; i++) {
		libdir = XsArrayElement(proj->libdirs, i);

		spdirs[0] = SystemSpecialDir(proj->sys);
		spdirs[1] = ArchitectureSpecialDir(proj->arch);
		spdirs[2] = ProjectTypeSpecialDir(proj->type);

		for(j=0; j<sizeof(spdirs)/sizeof(spdirs[0]); j++) {
			if (spdirs[j][0]=='\0')
				continue;

			XsStrCpy(dir, sizeof(dir), va("%s/%s", libdir, spdirs[j]));

			if (XsIsDir(dir))
				XsArrayPush(proj->libdirs, dir);
		}
	}

	errno = 0; /* XsIsDir() may fail (harmlessly) and set errno; ignore */
}

void EnumSources(project_t proj, const char *srcdir) {
	struct dirent *dp;
	xs_path_t path;
	char *p;
	DIR *d;
	int e, addSlash;

	assert(proj != (project_t)0);
	assert(srcdir != (const char *)0);

	if (!(d = opendir(srcdir))) {
		ErrorMessage(srcdir);
		return;
	}

	while((dp = readdir(d)) != (struct dirent *)0) {
		XsStrCpy(path, sizeof(path), srcdir);
		XsStrCat(path, sizeof(path), dp->d_name);

		p = getext(dp->d_name);

		if (isextc(p) || isextcpp(p)) {
			if (!XsIsFile(path))
				continue;

			AddProjectSource(proj, path);
			continue;
		}

		if (!XsIsDir(path)) {
			errno = 0; /*don't care about the errno from XsIsDir()*/
			continue;
		}

		if (IsSpecialDir(proj, dp->d_name)) {
			AddProjectSpecialDir(proj, dp->d_name);

			addSlash = 1;

			p = strrchr(path, '/');
			if (p) {
				if (*(p + 1)==0)
					addSlash = 0;
			}

			if (addSlash)
				XsStrCat(path, sizeof(path), "/"); /*ending '/' is necessary*/

			EnumSources(proj, path); /*recursive!*/
		}
	}

	/* check if any errors occurred while enumerating */
	e = errno;
	closedir(d);
	errno = e;

	if (errno)
		ErrorMessage(srcdir);
}

int CalcProjectName(project_t proj, const char *path, const char *file) {
	size_t i;
	FILE *f;
	xs_path_t buf, cwd;
	char *p;

	/*
	 * NOTE: 'file' is the "indicator file"; e.g., ".library"
	 */

	assert(proj != (project_t)0);
	assert(path != (const char *)0);

	/* read a single line from the file */
	if (file != (const char *)0) {
		if (!(f = fopen(file, "r"))) {
			ErrorMessage(file);
			return 0;
		}

		buf[0] = 0;
		if (!fgets(buf, sizeof(buf), f))
			buf[0] = 0; /*warning: unused result*/

		fclose(f);

		/* strip the line of whitespace on both ends */
		for(i=0; buf[i]<=' ' && buf[i]!=0; i++);

		if ((p = strchr(&buf[i], '\r')) != (char *)0)
			*p = '\0';
		if ((p = strchr(&buf[i], '\n')) != (char *)0)
			*p = '\0';

		while((p = strrchr(&buf[i], ' ')) != (char *)0) {
			if (*(p + 1)!='\0')
				break;

			*p = '\0';
		}
	} else {
		XsGetCWD(cwd, sizeof(cwd));

		if (!(p = strrchr(cwd, '/')))
			p = cwd;
		else if(*(p + 1)=='\0') {
			if (strcmp(cwd, "/") != 0) {
				*p = 0;
				p = strrchr(cwd, '/');
				if (!p)
					p = cwd;
			}
		}

		assert(p != (char *)0);

		if (p!=cwd && *p=='/')
			p++;

		XsStrCpy(buf, sizeof(buf), p);
		i = 0;
	}

	/* if no name was specified in the file, use the directory name */
	if (!buf[i]) {
		XsStrCpy(buf, sizeof(buf), path);

		/* fs_realpath() always adds a '/' at the end of a directory */
		p = strrchr(buf, '/');

		/* ensure this is the case (debug-mode only) */
		assert(p != (char *)0);
		assert(*(p + 1)==0);

		/* remove the ending '/' to avoid conflict with the following */
		*p = 0;

		/* if there's an ending '/' (always should be), mark the character
		   past that as the start of the directory name */
		i = ((p = strrchr(buf, '/')) != (char *)0) ? (p - buf) + 1 : 0;
	}

	/* set the project's name */
	SetProjectName(proj, &buf[i]);

	/* done */
	return 1;
}

project_t AddProject(project_t prnt, const char *path, const char *file,
int type) {
	project_t proj;

	assert(path != (const char *)0);
	assert(file != (const char *)0);

	/* create the project */
	proj = NewProject(prnt);

	/* calculate the appropriate name for the project */
	if (!CalcProjectName(proj, path, file)) {
		DeleteProject(proj);
		return (project_t)0;
	}

	/* prepare the passed settings of the project */
	SetProjectPath(proj, path);
	SetProjectType(proj, type);

	/* add the sources for the project */
	EnumSources(proj, path);

	/* return the project */
	return proj;
}

void FindProjects(project_t prnt, const char *srcdir) {
	struct { const char *name; int type; } tests[] = {
		{ ".library", kProjType_Library },
		{ ".dynamiclibrary", kProjType_DynamicLibrary },
		{ ".executable", kProjType_Executable }
	};
	struct dirent *dp;
	project_t proj;
	xs_path_t path, file;
	size_t i;
	DIR *d;
	int e;

	/* open the source directory to add projects */
	if (!(d = opendir(srcdir)))
		ErrorExit(srcdir);

	/* run through each entry in the directory */
	while((dp=readdir(d)) != (struct dirent *)0) {
		if (!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, ".."))
			continue;

		if (!XsRealPath(va("%s/%s/", srcdir, dp->d_name), path, sizeof(path)))
			continue;

		/* run through each test */
		for(i=0; i<sizeof(tests)/sizeof(tests[0]); i++) {
			XsStrCpy(file, sizeof(file), va("%s%s", path, tests[i].name));

			/* make sure this file exists */
			if (!XsIsFile(file)) {
				errno = 0;
				continue;
			}

			/* add the project file */
			proj = AddProject(prnt, path, file, tests[i].type);

			/* if successful, find sub-projects */
			if (proj)
				FindProjects(proj, path);
		}
	}

	/* close this directory */
	e = errno;
	closedir(d);
	errno = e;

	/* if an error occurred, display it */
	/*
	if (errno)
		ErrorExit(va("readdir(\"%s\") failed", srcdir));
	*/
}

void FindRootDirs(xs_array_t srcdirs, xs_array_t incdirs, xs_array_t libdirs) {
	struct { const char *name; xs_array_t dst; } tests[] = {
		{ "src", 0 }, { "source" , 0 }, { "code"     , 0 },
		{ "inc", 0 }, { "include", 0 }, { "headers"  , 0 },
		{ "lib", 0 }, { "library", 0 }, { "libraries", 0 }
	};
	struct stat s;
	project_t proj;
	xs_path_t path;
	size_t i, n;

	i = 0;

	tests[i++].dst = srcdirs;   /* src       */
	tests[i++].dst = srcdirs;   /* source    */
	tests[i++].dst = srcdirs;   /* code      */
	tests[i++].dst = incdirs;   /* inc       */
	tests[i++].dst = incdirs;   /* include   */
	tests[i++].dst = incdirs;   /* headers   */
	tests[i++].dst = libdirs;   /* lib       */
	tests[i++].dst = libdirs;   /* library   */
	tests[i++].dst = libdirs;   /* libraries */

	assert(srcdirs != (xs_array_t)0);
	assert(incdirs != (xs_array_t)0);
	assert(libdirs != (xs_array_t)0);

	for(i=0; i<sizeof(tests)/sizeof(tests[0]); i++) {
		assert(tests[i].name != (const char *)0);
		assert(tests[i].dst != (xs_array_t)0);

		if (XsArraySize(tests[i].dst) > 0)
			continue;

		if (stat(tests[i].name, &s)==-1)
			continue;
		if (S_ISDIR(~s.st_mode))
			continue;

		XsArrayPush(tests[i].dst, tests[i].name);
	}

	n = XsArraySize(srcdirs);
	for(i=0; i<n; i++)
		FindProjects((project_t)0, XsArrayElement(srcdirs, i));

	if (!FirstRootProject() && n>0) {
		if (!XsRealPath("./", path, sizeof(path)))
			return;

		proj = NewProject((project_t)0);

		if (!CalcProjectName(proj, path, (const char *)0)) {
			DeleteProject(proj);
			return;
		}

		XsStrCat(path, sizeof(path), va("%s/", XsArrayElement(srcdirs, 0)));

		SetProjectPath(proj, path);
		SetProjectType(proj, kProjType_Executable);

		EnumSources(proj, path);
	}
}

int MakeObjDirs(project_t proj) {
	const char *typename;
	xs_path_t objdir, cwd;
	size_t i, n;

	assert(proj != (project_t)0);

	if (XsGetCWD(cwd, sizeof(cwd))==(char *)0) {
		ErrorMessage("getcwd() failed");
		return 0;
	}

	n = strlen(cwd);

	typename = ProjectTypeSpecialDir(ProjectType(proj));

	snprintf(objdir, sizeof(objdir)-1, "obj/%s/%s/%s",
		g_flags & kFlag_Release_Bit ? "rel" : "dbg", typename,
		&ProjectPath(proj)[n]);
	objdir[sizeof(objdir)-1] = 0;

	XsMkDirs(objdir);

	n = ProjectSpecialDirCount(proj);
	for(i=0; i<n; i++)
		XsMkDirs(va("%s%s/", objdir, ProjectSpecialDir(proj, i)));

	return 1;
}
