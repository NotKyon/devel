#include "ast.h"

#include "memory.h"
#include "report.h"

ast_t *NewAST(ast_t *prnt, const char *lexan, int token, int line) {
	ast_t *ast;

	ast = (ast_t *)Memory((void *)0, sizeof(*ast));

	ast->root = prnt ? (prnt->root ? prnt->root : prnt) : (ast_t *)0;
	ast->prnt = prnt;
	ast->head = (ast_t *)0;
	ast->tail = (ast_t *)0;
	ast->prev = (ast_t *)0;
	ast->next = (ast_t *)0;

	if (prnt) {
		if ((ast->prev = prnt->tail) != (ast_t *)0)
			prnt->tail->next = ast;
		else
			prnt->head = ast;
		prnt->tail = ast;
	}

	ast->lexan = Duplicate(lexan);
	ast->token = token;
	ast->line = line;

	return ast;
}

ast_t *DeleteAST(ast_t *ast) {
	if (!ast)
		return (ast_t *)0;

	while(ast->head)
		DeleteAST(ast->head);

	if (ast->prev)
		ast->prev->next = ast->next;
	if (ast->next)
		ast->next->prev = ast->prev;

	if (ast->prnt) {
		if (ast->prnt->head==ast)
			ast->prnt->head = ast->next;
		if (ast->prnt->tail==ast)
			ast->prnt->tail = ast->prev;
	}

	Memory((void *)ast->lexan, 0);

	return (ast_t *)Memory((void *)ast, 0);
}

ast_t *ASTRoot(ast_t *ast) {
	ASSERT(ast != (ast_t *)0);

	return ast->root;
}
ast_t *ASTParent(ast_t *ast) {
	ASSERT(ast != (ast_t *)0);

	return ast->prnt;
}
ast_t *FirstAST(ast_t *ast) {
	ASSERT(ast != (ast_t *)0);

	return ast->head;
}
ast_t *LastAST(ast_t *ast) {
	ASSERT(ast != (ast_t *)0);

	return ast->tail;
}
ast_t *ASTBefore(ast_t *ast) {
	ASSERT(ast != (ast_t *)0);

	return ast->prev;
}
ast_t *ASTAfter(ast_t *ast) {
	ASSERT(ast != (ast_t *)0);

	return ast->next;
}

const char *ASTLexan(ast_t *ast) {
	ASSERT(ast != (ast_t *)0);

	return ast->lexan;
}
int ASTToken(ast_t *ast) {
	ASSERT(ast != (ast_t *)0);

	return ast->token;
}
const char *ASTFile(ast_t *ast) {
	ast_t *root;

	ASSERT(ast != (ast_t *)0);

	root = ASTRoot(ast);
	if (!root)
		root = ast;

	return ASTLexan(root);
}
int ASTLine(ast_t *ast) {
	ASSERT(ast != (ast_t *)0);

	return ast->line;
}

bool_t IsASTDescendant(ast_t *ast, ast_t *prnt) {
	ASSERT(ast != (ast_t *)0);
	ASSERT(ast != prnt);

	if (!prnt)
		return FALSE;

	while(ast) {
		ast = ast->prnt;

		if (ast==prnt)
			return TRUE;
	}

	return FALSE;
}
void SetASTParent(ast_t *ast, ast_t *prnt) {
	ASSERT(ast != (ast_t *)0);
	ASSERT(prnt != (ast_t *)0); /*can't change the root AST*/
	ASSERT(ast != prnt);

	if (ast->prnt==prnt)
		return;

	if (IsASTDescendant(prnt, ast))
		SetASTParent(prnt, ast->prnt);

	if (ast->prev)
		ast->prev->next = ast->next;
	if (ast->next)
		ast->next->prev = ast->prev;

	if (ast->prnt) {
		if (ast->prnt->head==ast)
			ast->prnt->head = ast->next;
		if (ast->prnt->tail==ast)
			ast->prnt->tail = ast->prev;
	}

	ast->prnt = prnt;
}

void ASTReportFuncV(ast_t *ast, const char *type, const char *func,
const char *format, va_list args) {
	ASSERT(ast != (ast_t *)0);
	ASSERT(type != (const char *)0);
	ASSERT(format != (const char *)0);

	ReportV(type, ASTFile(ast), ASTLine(ast), func, format, args);
}
void ASTReportFunc(ast_t *ast, const char *type, const char *func,
const char *format, ...) {
	va_list args;

	ASSERT(ast != (ast_t *)0);
	ASSERT(type != (const char *)0);
	ASSERT(format != (const char *)0);

	va_start(args, format);
	ASTReportFuncV(ast, type, func, format, args);
	va_end(args);
}

void ASTWarnFunc(ast_t *ast, const char *func, const char *format, ...) {
	va_list args;

	ASSERT(ast != (ast_t *)0);
	ASSERT(format != (const char *)0);

	va_start(args, format);
	ASTReportFuncV(ast, TYPE_WRN, func, format, args);
	va_end(args);
}
void ASTErrorFunc(ast_t *ast, const char *func, const char *format, ...) {
	va_list args;

	ASSERT(ast != (ast_t *)0);
	ASSERT(format != (const char *)0);

	va_start(args, format);
	ASTReportFuncV(ast, TYPE_ERR, func, format, args);
	va_end(args);
}

void ASTReportV(ast_t *ast, const char *type, const char *format,
va_list args) {
	ASSERT(ast != (ast_t *)0);
	ASSERT(type != (const char *)0);
	ASSERT(format != (const char *)0);

	ASTReportFuncV(ast, type, (const char *)0, format, args);
}
void ASTReport(ast_t *ast, const char *type, const char *format, ...) {
	va_list args;

	ASSERT(ast != (ast_t *)0);
	ASSERT(type != (const char *)0);
	ASSERT(format != (const char *)0);

	va_start(args, format);
	ASTReportV(ast, type, format, args);
	va_end(args);
}

void ASTWarn(ast_t *ast, const char *format, ...) {
	va_list args;

	ASSERT(ast != (ast_t *)0);
	ASSERT(format != (const char *)0);

	va_start(args, format);
	ASTReportV(ast, TYPE_WRN, format, args);
	va_end(args);
}
void ASTError(ast_t *ast, const char *format, ...) {
	va_list args;

	ASSERT(ast != (ast_t *)0);
	ASSERT(format != (const char *)0);

	va_start(args, format);
	ASTReportV(ast, TYPE_ERR, format, args);
	va_end(args);
}
