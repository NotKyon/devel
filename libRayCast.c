#include "libRayCast.h"

#ifndef RC_ARCH_X86
# if defined( __i386__ ) || defined( _M_IX86 )
#  define RC_ARCH_X86	1
# else
#  define RC_ARCH_X86	0
# endif
#endif
#ifndef RC_ARCH_X64
# if defined( __amd64__ ) || defined( __x86_64__ ) || defined( _M_X64 )
#  define RC_ARCH_X64	1
# else
#  define RC_ARCH_X64	0
# endif
#endif

#if RC_ARCH_X64 && RC_ARCH_X86
# undef RC_ARCH_X86
# define RC_ARCH_X86	0
#endif

#ifndef RC_SIMD_NONE	0
#define RC_SIMD_SSE		1

#ifndef RC_SIMD
# if RC_ARCH_X86 || RC_ARCH_X64
#  define RC_SIMD		RC_SIMD_SSE
# else
#  define RC_SIMD		RC_SIMD_NONE
# endif
#endif

#if RC_SIMD_SSE
# include <mmintrin.h>
# include <emmintrin.h>
# include <xmmintrin.h>
#endif




/*
===============================================================================

	MATRIX MATH

===============================================================================
*/

/*
 * Transpose a 4x4 or 3x3 matrix.
 * NOTE: dst cannot be the same address as src.
 */
static
void rcMat4Transpose( RC_MATRIX4 *pOutMatrix, const RC_MATRIX4 *pInMatrix )
{
#define dst				pOutMatrix
#define src				pInMatrix

	dst->m11 = src->m11;
	dst->m12 = src->m21;
	dst->m13 = src->m31;
	dst->m14 = src->m41;

	dst->m21 = src->m12;
	dst->m22 = src->m22;
	dst->m23 = src->m32;
	dst->m24 = src->m42;

	dst->m31 = src->m13;
	dst->m32 = src->m23;
	dst->m33 = src->m33;
	dst->m34 = src->m43;

	dst->m41 = src->m14;
	dst->m42 = src->m24;
	dst->m43 = src->m34;
	dst->m44 = src->m44;

#undef src
#undef dst
}
static
void rcMat4Transpose3( RC_MATRIX4 *pOutMatrix, const RC_MATRIX4 *pInMatrix )
{
#define dst				pOutMatrix
#define src				pInMatrix

	dst->m11 = src->m11;
	dst->m12 = src->m21;
	dst->m13 = src->m31;

	dst->m21 = src->m12;
	dst->m22 = src->m22;
	dst->m23 = src->m32;

	dst->m31 = src->m13;
	dst->m32 = src->m23;
	dst->m33 = src->m33;

#undef src
#undef dst
}

/*
 * Create an inverse matrix from a affine transform. (This expects an
 * orthonormal matrix; any matrix with uniform scaling and the bottom row as
 * [0,0,0,1].)
 */
static
void rcMat4AffineInverse( RC_MATRIX4 *pOutMatrix, const RC_MATRIX4 *pInMatrix )
{
#define dst				pOutMatrix
#define src				pInMatrix

	rcMat3Inverse(dst, src);

	dst->m14 = -(src->m14*dst->m11 + src->m24*dst->m12 + src->m34*dst->m13);
	dst->m24 = -(src->m14*dst->m21 + src->m24*dst->m22 + src->m34*dst->m23);
	dst->m34 = -(src->m14*dst->m31 + src->m24*dst->m32 + src->m34*dst->m33);

	dst->m41 = 0;
	dst->m42 = 0;
	dst->m43 = 0;
	dst->m44 = 1;

#undef src
#undef dst
}
static
void rcMat4Inverse( RC_MATRIX4 *pOutMatrix, const RC_MATRIX4 *pInMatrix )
{
	/*
	 *	!!! TODO !!!
	 */
}

/*
 * Multiply a 4x4 or 3x3 matrix with another, storing the result in a third.
 * NOTE: dst cannot be the same address as a or b.
 */
static
void rcMat4Multiply( RC_MATRIX4 *pOutMatrix, const RC_MATRIX4 *pInMatrixA, const RC_MATRIX4 *pInMatrixB )
{
#define dst				pOutMatrix
#define a				pInMatrixA
#define b				pInMatrixB

	dst->m11 = a->m11*b->m11 + a->m12*b->m21 + a->m13*b->m31 + a->m14*b->m41;
	dst->m12 = a->m11*b->m12 + a->m12*b->m22 + a->m13*b->m32 + a->m14*b->m42;
	dst->m13 = a->m11*b->m13 + a->m12*b->m23 + a->m13*b->m33 + a->m14*b->m43;
	dst->m14 = a->m11*b->m14 + a->m12*b->m24 + a->m13*b->m34 + a->m14*b->m44;

	dst->m21 = a->m21*b->m11 + a->m22*b->m21 + a->m23*b->m31 + a->m24*b->m41;
	dst->m22 = a->m21*b->m12 + a->m22*b->m22 + a->m23*b->m32 + a->m24*b->m42;
	dst->m23 = a->m21*b->m13 + a->m22*b->m23 + a->m23*b->m33 + a->m24*b->m43;
	dst->m24 = a->m21*b->m14 + a->m22*b->m24 + a->m23*b->m34 + a->m24*b->m44;

	dst->m31 = a->m31*b->m11 + a->m32*b->m21 + a->m33*b->m31 + a->m34*b->m41;
	dst->m32 = a->m31*b->m12 + a->m32*b->m22 + a->m33*b->m32 + a->m34*b->m42;
	dst->m33 = a->m31*b->m13 + a->m32*b->m23 + a->m33*b->m33 + a->m34*b->m43;
	dst->m34 = a->m31*b->m14 + a->m32*b->m24 + a->m33*b->m34 + a->m34*b->m44;

	dst->m41 = a->m41*b->m11 + a->m42*b->m21 + a->m43*b->m31 + a->m44*b->m41;
	dst->m42 = a->m41*b->m12 + a->m42*b->m22 + a->m43*b->m32 + a->m44*b->m42;
	dst->m43 = a->m41*b->m13 + a->m42*b->m23 + a->m43*b->m33 + a->m44*b->m43;
	dst->m44 = a->m41*b->m14 + a->m42*b->m24 + a->m43*b->m34 + a->m44*b->m44;

#undef b
#undef a
#undef dst
}
static
void rcMat4Multiply3( RC_MATRIX4 *pOutMatrix, const RC_MATRIX4 *pInMatrixA, const RC_MATRIX4 *pInMatrixB )
{
#define dst				pOutMatrix
#define a				pInMatrixA
#define b				pInMatrixB

	dst->m11 = a->m11*b->m11 + a->m12*b->m21 + a->m13*b->m31;
	dst->m12 = a->m11*b->m12 + a->m12*b->m22 + a->m13*b->m32;
	dst->m13 = a->m11*b->m13 + a->m12*b->m23 + a->m13*b->m33;

	dst->m21 = a->m21*b->m11 + a->m22*b->m21 + a->m23*b->m31;
	dst->m22 = a->m21*b->m12 + a->m22*b->m22 + a->m23*b->m32;
	dst->m23 = a->m21*b->m13 + a->m22*b->m23 + a->m23*b->m33;

	dst->m31 = a->m31*b->m11 + a->m32*b->m21 + a->m33*b->m31;
	dst->m32 = a->m31*b->m12 + a->m32*b->m22 + a->m33*b->m32;
	dst->m33 = a->m31*b->m13 + a->m32*b->m23 + a->m33*b->m33;

#undef b
#undef a
#undef dst
}
/*
 * Transform a 2D vector by a 4x4 matrix
 */
static
void rcVec2TransformMat4( RC_VECTOR2 *pOutVector, const RC_VECTOR2 *pInVector, const RC_MATRIX4 *pInMatrix )
{
	const float x = pInVector->x;
	const float y = pInVector->y;

	pOutVector->x = x*pInMatrix->m11 + x*pMatrix->m21 + x*pMatrix->m31 + x*pMatrix->m41;
	pOutVector->y = y*pInMatrix->m12 + y*pMatrix->m22 + y*pMatrix->m32 + y*pMatrix->m42;
}
/*
 * Transform a 3D vector by a 4x4 matrix
 */
static
void rcVec3TransformMat4( RC_VECTOR3 *pOutVector, const RC_VECTOR3 *pInVector, const RC_MATRIX4 *pInMatrix )
{
	const float x = pInVector->x;
	const float y = pInVector->y;
	const float z = pInVector->z;

	pOutVector->x = x*pInMatrix->m11 + x*pMatrix->m21 + x*pMatrix->m31 + x*pMatrix->m41;
	pOutVector->y = y*pInMatrix->m12 + y*pMatrix->m22 + y*pMatrix->m32 + y*pMatrix->m42;
	pOutVector->z = z*pInMatrix->m13 + z*pMatrix->m23 + z*pMatrix->m33 + z*pMatrix->m43;
}
/*
 * Transform a 4D vector by a 4x4 matrix
 */
static
void rcVec4TransformMat4( RC_VECTOR4 *pOutVector, const RC_VECTOR4 *pInVector, const RC_MATRIX4 *pInMatrix )
{
	const float x = pInVector->x;
	const float y = pInVector->y;
	const float z = pInVector->z;
	const float w = pInVector->w;

	pOutVector->x = x*pInMatrix->m11 + x*pMatrix->m21 + x*pMatrix->m31 + x*pMatrix->m41;
	pOutVector->y = y*pInMatrix->m12 + y*pMatrix->m22 + y*pMatrix->m32 + y*pMatrix->m42;
	pOutVector->z = z*pInMatrix->m13 + z*pMatrix->m23 + z*pMatrix->m33 + z*pMatrix->m43;
	pOutVector->w = w*pInMatrix->m14 + w*pMatrix->m24 + w*pMatrix->m34 + w*pMatrix->m44;
}
/*
 * Normalize a 3D vector
 */
static
void rcVec3Normalize( RC_VECTOR3 *pOutVector, const RC_VECTOR3 *pInVector )
{
	float invmag = 1.0f/sqrtf( pInVector->x*pInVector->x + pInVector->y*pInVector->y + pInVector->z*pInVector->z );
	
	pOutVector->x = invmag*pInVector->x;
	pOutVector->y = invmag*pInVector->y;
	pOutVector->z = invmag*pInVector->z;
}




/*
===============================================================================

	SCRATCH MEMORY

===============================================================================
*/

/*
 * Allocate from "low memory"
 */
static
void *rcAllocLow( RC_SCRATCH_MEMORY *pInoutMemory, RC_UINT32 cInBytes )
{
	unsigned char *memory;

	if( pInoutMemory->offsetLow + cInBytes > pInoutMemory->sizeInBytes - pInoutMemory->offsetHigh ) {
		return NULL;
	}

	memory = ( ( unsigned char * )pInoutMemory->memory ) + pInoutMemory->offsetLow;
	pInoutMemory->offsetLow += cInBytes;

	return ( void * )memory;
}
/*
 * Allocate from "high memory"
 */
static
void *rcAllocHigh( RC_SCRATCH_MEMORY *pInoutMemory, RC_UINT32 cInBytes )
{
	unsigned char *memory;

	if( pInoutMemory->offsetLow + cInBytes > pInoutMemory->sizeInBytes - pInoutMemory->offsetHigh ) {
		return NULL;
	}

	pInoutMemory->offsetHigh -= cInBytes;
	memory = ( ( unsigned char * )pInoutMemory->memory ) + ( pInoutMemory->sizeInBytes - pInoutMemory->offsetHigh );

	return ( void * )memory;
}




/*
===============================================================================

	FRONT-END

===============================================================================
*/

RC_FUNC RC_RESULT RC_CALL rcCalcRayScreenVectors( RC_VECTOR3 *pOutStart, RC_VECTOR3 *pOutEnd, const RC_SCREEN_PICK_INFO *pInScreenPick )
{
	RC_VECTOR2 screenSize;
	RC_VECTOR2 clipSpaceCoords;
	RC_VECTOR4 clipNear;
	RC_VECTOR4 clipFar;
	RC_MATRIX4 invProj;
	RC_VECTOR4 eyeNear;
	RC_VECTOR4 eyeFar;
	RC_VECTOR4 worldNear;
	RC_VECTOR4 worldFar;

	if( !pOutStart || !pOutEnd || !pInScreenPick ) {
		return RC_INVALID_PARMS;
	}

	screenSize.x = ( float )( pInScreenPick->viewport.x2 - pInScreenPick->viewport.x1 );
	screenSize.y = ( float )( pInScreenPick->viewport.y2 - pInScreenPick->viewport.y1 );

	clipSpaceCoords.x = 2.0f*pInScreenPick->position.x/screenSize.x - 1.0f;
	clipSpaceCoords.y = 1.0f - 2.0f*pInScreenPick->position.y/screenSize.y;

	clipNear.x = clipSpaceCoords.x;
	clipNear.y = clipSpaceCoords.y;
	clipNear.z = -1.0f;
	clipNear.w = 1.0f;

	clipFar.x = clipSpaceCoords.x;
	clipFar.y = clipSpaceCoords.y;
	clipFar.z = 1.0f;
	clipFar.w = 1.0f;

	rcVec4TransformMat4( &eyeNear, &clipNear, &pInScreenPick->projectionInverse );
	rcVec4TransformMat4( &eyeFar , &clipFar , &pInScreenPick->projectionInverse );

	rcVec4TransformMat4( &worldNear, &eyeNear, &pInScreenPick->viewInverse );
	rcVec4TransformMat4( &worldFar , &eyeFar , &pInScreenPick->viewInverse );

	rcVec3Normalize( pOutStart, ( const RC_VECTOR3 * )&worldNear );
	rcVec3Normalize( pOutEnd  , ( const RC_VECTOR3 * )&worldFar  );

	return RC_SUCCESS;
}
RC_FUNC RC_RESULT RC_CALL rcStepRay( RC_RAY_INFO *pInoutRay )
{
	if( !pInoutRay ) {
		return RC_INVALID_PARMS;
	}

	/*
	 *	TODO
	 */
}
RC_FUNC RC_RESULT RC_CALL rcCastRay( RC_RAY_INFO *pInoutRay )
{
	RC_RESULT result;

	do {
		result = rcStepRay( pInoutRay );
	} while( result == RC_STEP_WORKING );

	return result;
}
