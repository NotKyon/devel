#define GB_INIT 1
#include <gamebase.h>

//
//	The following code demonstrates how to use the reporting system. This system
//	outputs color coded text to the console and/or log files. (Although, log
//	files don't get colored output, obviously.)
//

void GBMain() {
	// Simplest way to output text
	gbPrint("This is the most basic way to output text to the console...");

	// Are you making a compiler?
	gbWarn(__FILE__,__LINE__,__func__, "This is a warning");
	gbError(__FILE__,__LINE__,__func__, "This is an error");

	// These are also helpful (they don't output file information)
	gbWarnMessage("This is a simplified warning; line number: %i", __LINE__);
	gbErrorMessage("Simplified error, too! date: %s", __DATE__);

	// You can do more advanced reporting
	gbReport(kGBLogType_Remark, (const char *)NULL, 0, (const char *)NULL,
		"This is a simple remark...");

	// And of course, the wonderful debug log
	gbDebugLog("This will only be output in debug mode.");
}
