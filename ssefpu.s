	.file	"ssefpu.c"
	.intel_syntax noprefix
	.text
	.p2align 5,,31
	.globl	_sse_dot
	.def	_sse_dot;	.scl	2;	.type	32;	.endef
_sse_dot:
LFB534:
	.cfi_startproc
	mulps	xmm0, xmm1
	sub	esp, 28
	.cfi_def_cfa_offset 32
	movaps	xmm2, xmm0
	shufps	xmm2, xmm0, 78
	addps	xmm2, xmm0
	movaps	xmm0, xmm2
	shufps	xmm0, xmm2, 1
	addss	xmm2, xmm0
	movss	DWORD PTR [esp+12], xmm2
	fld	DWORD PTR [esp+12]
	add	esp, 28
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE534:
	.p2align 5,,31
	.globl	_fpu_dot
	.def	_fpu_dot;	.scl	2;	.type	32;	.endef
_fpu_dot:
LFB535:
	.cfi_startproc
	mov	edx, DWORD PTR [esp+4]
	mov	eax, DWORD PTR [esp+8]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	ret
	.cfi_endproc
LFE535:
	.p2align 5,,31
	.globl	_sse_cross
	.def	_sse_cross;	.scl	2;	.type	32;	.endef
_sse_cross:
LFB536:
	.cfi_startproc
	movaps	xmm2, xmm0
	movaps	xmm3, xmm1
	shufps	xmm2, xmm0, 9
	shufps	xmm3, xmm1, 18
	shufps	xmm0, xmm0, 18
	mov	eax, DWORD PTR [esp+4]
	shufps	xmm1, xmm1, 9
	mulps	xmm2, xmm3
	mulps	xmm0, xmm1
	subps	xmm2, xmm0
	movaps	xmm0, xmm2
	movss	DWORD PTR [eax], xmm2
	shufps	xmm0, xmm2, 85
	unpckhps	xmm2, xmm2
	movss	DWORD PTR [eax+4], xmm0
	movss	DWORD PTR [eax+8], xmm2
	ret
	.cfi_endproc
LFE536:
	.p2align 5,,31
	.globl	_fpu_cross
	.def	_fpu_cross;	.scl	2;	.type	32;	.endef
_fpu_cross:
LFB537:
	.cfi_startproc
	mov	edx, DWORD PTR [esp+8]
	mov	eax, DWORD PTR [esp+12]
	mov	ecx, DWORD PTR [esp+4]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+8]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+4]
	fsubp	st(1), st
	fstp	DWORD PTR [ecx]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+8]
	fsubp	st(1), st
	fstp	DWORD PTR [ecx+4]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+4]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax]
	fsubp	st(1), st
	fstp	DWORD PTR [ecx+8]
	ret
	.cfi_endproc
LFE537:
	.p2align 5,,31
	.globl	_sse_cross4
	.def	_sse_cross4;	.scl	2;	.type	32;	.endef
_sse_cross4:
LFB538:
	.cfi_startproc
	movaps	xmm4, xmm1
	movaps	xmm7, xmm1
	shufps	xmm4, xmm1, 90
	shufps	xmm7, xmm1, 191
	movaps	xmm6, xmm2
	shufps	xmm6, xmm2, 90
	sub	esp, 44
	.cfi_def_cfa_offset 48
	movaps	xmm5, xmm4
	movaps	xmm3, xmm2
	movaps	XMMWORD PTR [esp], xmm4
	movaps	xmm4, xmm6
	mulps	xmm4, xmm7
	shufps	xmm3, xmm2, 191
	shufps	xmm1, xmm1, 1
	mov	eax, DWORD PTR [esp+48]
	mulps	xmm5, xmm3
	movaps	XMMWORD PTR [esp+16], xmm4
	movaps	xmm4, xmm2
	shufps	xmm4, xmm2, 1
	mulps	xmm3, xmm1
	shufps	xmm2, xmm2, 65
	mulps	xmm4, xmm7
	mulps	xmm2, XMMWORD PTR [esp]
	subps	xmm5, XMMWORD PTR [esp+16]
	mulps	xmm1, xmm6
	movaps	xmm6, xmm0
	shufps	xmm6, xmm0, 1
	subps	xmm3, xmm4
	movaps	xmm4, xmm0
	shufps	xmm4, xmm0, 90
	shufps	xmm0, xmm0, 255
	subps	xmm1, xmm2
	mulps	xmm6, xmm5
	mulps	xmm4, xmm3
	mulps	xmm0, xmm1
	subps	xmm6, xmm4
	addps	xmm6, xmm0
	mulps	xmm6, XMMWORD PTR LC1
	movaps	XMMWORD PTR [eax], xmm6
	add	esp, 44
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE538:
	.p2align 5,,31
	.globl	_fpu_cross4
	.def	_fpu_cross4;	.scl	2;	.type	32;	.endef
_fpu_cross4:
LFB539:
	.cfi_startproc
	push	ebx
	.cfi_def_cfa_offset 8
	.cfi_offset 3, -8
	sub	esp, 40
	.cfi_def_cfa_offset 48
	mov	ebx, DWORD PTR [esp+52]
	mov	ecx, DWORD PTR [esp+56]
	mov	edx, DWORD PTR [esp+60]
	mov	eax, DWORD PTR [esp+48]
	fld	DWORD PTR [ebx]
	fstp	DWORD PTR [esp+20]
	fld	DWORD PTR [ebx+4]
	fstp	DWORD PTR [esp+24]
	fld	DWORD PTR [ebx+8]
	fstp	DWORD PTR [esp+36]
	fld	DWORD PTR [ebx+12]
	fstp	DWORD PTR [esp+4]
	fld	DWORD PTR [ecx]
	fstp	DWORD PTR [esp]
	fld	DWORD PTR [ecx+4]
	fstp	DWORD PTR [esp+8]
	fld	DWORD PTR [ecx+8]
	fld	DWORD PTR [ecx+12]
	fld	DWORD PTR [edx]
	fstp	DWORD PTR [esp+28]
	fld	DWORD PTR [edx+4]
	fst	DWORD PTR [esp+12]
	fld	DWORD PTR [edx+8]
	fld	DWORD PTR [edx+12]
	fld	st(4)
	fmul	st, st(1)
	fld	st(2)
	fmul	st, st(5)
	fsubp	st(1), st
	fld	DWORD PTR [esp+8]
	fmul	st, st(2)
	fld	st(4)
	fmul	st, st(6)
	fsubp	st(1), st
	fld	DWORD PTR [esp+8]
	fmul	st, st(4)
	fstp	DWORD PTR [esp+16]
	fxch	st(4)
	fmul	st, st(6)
	fsubr	DWORD PTR [esp+16]
	fstp	DWORD PTR [esp+16]
	fld	DWORD PTR [esp+24]
	fmul	st, st(1)
	fld	DWORD PTR [esp+36]
	fmul	st, st(5)
	fsubp	st(1), st
	fld	DWORD PTR [esp+16]
	fmul	DWORD PTR [esp+4]
	faddp	st(1), st
	fstp	DWORD PTR [eax]
	fxch	st(1)
	fmul	DWORD PTR [esp]
	fxch	st(4)
	fmul	DWORD PTR [esp+28]
	fsubp	st(4), st
	fxch	st(1)
	fmul	DWORD PTR [esp]
	fxch	st(4)
	fmul	DWORD PTR [esp+28]
	fsubp	st(4), st
	fmul	DWORD PTR [esp+20]
	fld	DWORD PTR [esp+36]
	fmul	st, st(3)
	fsubp	st(1), st
	fld	DWORD PTR [esp+4]
	fmul	st, st(4)
	faddp	st(1), st
	fchs
	fstp	DWORD PTR [eax+4]
	fld	DWORD PTR [esp]
	fmul	DWORD PTR [esp+12]
	fld	DWORD PTR [esp+20]
	fmulp	st(2), st
	fxch	st(2)
	fmul	DWORD PTR [esp+24]
	fsubp	st(1), st
	fld	DWORD PTR [esp+28]
	fmul	DWORD PTR [esp+8]
	fsubr	st, st(2)
	fmul	DWORD PTR [esp+4]
	faddp	st(1), st
	fstp	DWORD PTR [eax+8]
	fld	DWORD PTR [esp+20]
	fmul	DWORD PTR [esp+16]
	fxch	st(2)
	fmul	DWORD PTR [esp+24]
	fsubp	st(2), st
	fld	DWORD PTR [esp+12]
	fmul	DWORD PTR [esp+8]
	fsubp	st(1), st
	fmul	DWORD PTR [esp+4]
	faddp	st(1), st
	fchs
	fstp	DWORD PTR [eax+12]
	add	esp, 40
	.cfi_def_cfa_offset 8
	pop	ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE539:
	.p2align 5,,31
	.globl	_init_tb
	.def	_init_tb;	.scl	2;	.type	32;	.endef
_init_tb:
LFB540:
	.cfi_startproc
	push	ebx
	.cfi_def_cfa_offset 8
	.cfi_offset 3, -8
	fldz
	xor	ebx, ebx
	sub	esp, 40
	.cfi_def_cfa_offset 48
	jmp	L12
	.p2align 5,,7
L15:
	mov	DWORD PTR [esp+24], ebx
	fild	DWORD PTR [esp+24]
	fmul	DWORD PTR LC3
	fmul	QWORD PTR LC4
	fadd	st, st(0)
	fstp	QWORD PTR [esp]
	call	_sin
L12:
	fstp	DWORD PTR _g_sin_tb[0+ebx*4]
	inc	ebx
	cmp	ebx, 65536
	jne	L15
	add	esp, 40
	.cfi_def_cfa_offset 8
	pop	ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE540:
	.p2align 5,,31
	.globl	_sse_sin
	.def	_sse_sin;	.scl	2;	.type	32;	.endef
_sse_sin:
LFB541:
	.cfi_startproc
	sub	esp, 12
	.cfi_def_cfa_offset 16
	fld	DWORD PTR LC5
	fdivr	DWORD PTR [esp+16]
	fmul	DWORD PTR LC6
	fisttp	QWORD PTR [esp]
	mov	eax, DWORD PTR [esp]
	movzx	edx, ax
	fld	DWORD PTR _g_sin_tb[0+edx*4]
	add	esp, 12
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE541:
	.p2align 5,,31
	.globl	_sse_cos
	.def	_sse_cos;	.scl	2;	.type	32;	.endef
_sse_cos:
LFB542:
	.cfi_startproc
	sub	esp, 12
	.cfi_def_cfa_offset 16
	fld	DWORD PTR LC7
	fadd	DWORD PTR [esp+16]
	fdiv	DWORD PTR LC5
	fmul	DWORD PTR LC6
	fisttp	QWORD PTR [esp]
	mov	eax, DWORD PTR [esp]
	movzx	edx, ax
	fld	DWORD PTR _g_sin_tb[0+edx*4]
	add	esp, 12
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE542:
	.p2align 5,,31
	.globl	_sse_sincos
	.def	_sse_sincos;	.scl	2;	.type	32;	.endef
_sse_sincos:
LFB543:
	.cfi_startproc
	sub	esp, 12
	.cfi_def_cfa_offset 16
	fld	DWORD PTR [esp+20]
	mov	ecx, DWORD PTR [esp+16]
	fld	DWORD PTR LC5
	fld	st(1)
	fdiv	st, st(1)
	fld	DWORD PTR LC6
	fmul	st(1), st
	fxch	st(1)
	fisttp	QWORD PTR [esp]
	mov	eax, DWORD PTR [esp]
	movzx	edx, ax
	fld	DWORD PTR _g_sin_tb[0+edx*4]
	fstp	DWORD PTR [ecx]
	fxch	st(2)
	fadd	DWORD PTR LC7
	fdivrp	st(1), st
	fmulp	st(1), st
	fisttp	QWORD PTR [esp]
	mov	eax, DWORD PTR [esp]
	movzx	edx, ax
	fld	DWORD PTR _g_sin_tb[0+edx*4]
	fstp	DWORD PTR [ecx+4]
	add	esp, 12
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE543:
	.p2align 5,,31
	.globl	_fpu_sin
	.def	_fpu_sin;	.scl	2;	.type	32;	.endef
_fpu_sin:
LFB544:
	.cfi_startproc
	fld	DWORD PTR LC8
	fdivr	DWORD PTR [esp+4]
	fmul	DWORD PTR LC9
	fstp	DWORD PTR [esp+4]
	jmp	_sinf
	.cfi_endproc
LFE544:
	.p2align 5,,31
	.globl	_fpu_cos
	.def	_fpu_cos;	.scl	2;	.type	32;	.endef
_fpu_cos:
LFB545:
	.cfi_startproc
	fld	DWORD PTR LC8
	fdivr	DWORD PTR [esp+4]
	fmul	DWORD PTR LC9
	fstp	DWORD PTR [esp+4]
	jmp	_sinf
	.cfi_endproc
LFE545:
	.p2align 5,,31
	.globl	_fpu_sincos
	.def	_fpu_sincos;	.scl	2;	.type	32;	.endef
_fpu_sincos:
LFB546:
	.cfi_startproc
	push	ebx
	.cfi_def_cfa_offset 8
	.cfi_offset 3, -8
	sub	esp, 40
	.cfi_def_cfa_offset 48
	fld	DWORD PTR LC8
	mov	ebx, DWORD PTR [esp+48]
	fdivr	DWORD PTR [esp+52]
	fmul	DWORD PTR LC9
	fst	DWORD PTR [esp]
	fstp	DWORD PTR [esp+16]
	call	_sinf
	fstp	DWORD PTR [ebx]
	fld	DWORD PTR [esp+16]
	fstp	DWORD PTR [esp]
	call	_cosf
	fstp	DWORD PTR [ebx+4]
	add	esp, 40
	.cfi_def_cfa_offset 8
	pop	ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE546:
	.p2align 5,,31
	.globl	_sse_matrow
	.def	_sse_matrow;	.scl	2;	.type	32;	.endef
_sse_matrow:
LFB547:
	.cfi_startproc
	movaps	xmm3, xmm0
	shufps	xmm3, xmm0, 0
	mulps	xmm3, xmm1
	movaps	xmm1, xmm0
	shufps	xmm1, xmm0, 85
	mulps	xmm1, xmm2
	addps	xmm3, xmm1
	movaps	xmm1, xmm0
	shufps	xmm1, xmm0, 170
	shufps	xmm0, xmm0, 255
	mulps	xmm1, XMMWORD PTR [esp+4]
	mulps	xmm0, XMMWORD PTR [esp+20]
	addps	xmm3, xmm1
	addps	xmm3, xmm0
	movaps	xmm0, xmm3
	ret
	.cfi_endproc
LFE547:
	.p2align 5,,31
	.globl	_sse_matmult
	.def	_sse_matmult;	.scl	2;	.type	32;	.endef
_sse_matmult:
LFB548:
	.cfi_startproc
	mov	ecx, DWORD PTR [esp+12]
	mov	eax, DWORD PTR [esp+8]
	mov	edx, DWORD PTR [esp+4]
	movaps	xmm3, XMMWORD PTR [ecx]
	movaps	xmm0, XMMWORD PTR [ecx]
	movaps	xmm1, XMMWORD PTR [ecx+48]
	movaps	xmm4, XMMWORD PTR [ecx+32]
	unpcklps	xmm3, XMMWORD PTR [ecx+16]
	unpckhps	xmm0, XMMWORD PTR [ecx+16]
	movaps	xmm2, xmm4
	unpckhps	xmm4, xmm1
	unpcklps	xmm2, xmm1
	movaps	xmm6, XMMWORD PTR [eax+16]
	movaps	xmm5, XMMWORD PTR [eax+32]
	movaps	xmm7, XMMWORD PTR [eax+48]
	movaps	xmm1, xmm3
	movlhps	xmm1, xmm2
	movhlps	xmm2, xmm3
	movaps	xmm3, xmm0
	movlhps	xmm3, xmm4
	movhlps	xmm4, xmm0
	movaps	xmm0, XMMWORD PTR [eax]
	shufps	xmm6, xmm6, 0
	shufps	xmm5, xmm5, 0
	shufps	xmm7, xmm7, 0
	shufps	xmm0, xmm0, 0
	mulps	xmm6, xmm2
	mulps	xmm7, xmm4
	mulps	xmm5, xmm3
	mulps	xmm0, xmm1
	addps	xmm5, xmm7
	addps	xmm0, xmm6
	addps	xmm0, xmm5
	movaps	XMMWORD PTR [edx], xmm0
	movaps	xmm0, XMMWORD PTR [eax]
	movaps	xmm6, XMMWORD PTR [eax+16]
	movaps	xmm5, XMMWORD PTR [eax+32]
	movaps	xmm7, XMMWORD PTR [eax+48]
	shufps	xmm0, xmm0, 85
	shufps	xmm6, xmm6, 85
	shufps	xmm5, xmm5, 85
	shufps	xmm7, xmm7, 85
	mulps	xmm6, xmm2
	mulps	xmm7, xmm4
	mulps	xmm5, xmm3
	mulps	xmm0, xmm1
	addps	xmm5, xmm7
	addps	xmm0, xmm6
	addps	xmm0, xmm5
	movaps	XMMWORD PTR [edx+16], xmm0
	movaps	xmm0, XMMWORD PTR [eax]
	movaps	xmm6, XMMWORD PTR [eax+16]
	movaps	xmm5, XMMWORD PTR [eax+32]
	movaps	xmm7, XMMWORD PTR [eax+48]
	shufps	xmm0, xmm0, 170
	shufps	xmm6, xmm6, 170
	shufps	xmm5, xmm5, 170
	shufps	xmm7, xmm7, 170
	mulps	xmm6, xmm2
	mulps	xmm7, xmm4
	mulps	xmm5, xmm3
	mulps	xmm0, xmm1
	addps	xmm5, xmm7
	addps	xmm0, xmm6
	addps	xmm0, xmm5
	movaps	XMMWORD PTR [edx+32], xmm0
	movaps	xmm0, XMMWORD PTR [eax]
	movaps	xmm6, XMMWORD PTR [eax+16]
	movaps	xmm5, XMMWORD PTR [eax+32]
	movaps	xmm7, XMMWORD PTR [eax+48]
	shufps	xmm0, xmm0, 255
	shufps	xmm6, xmm6, 255
	shufps	xmm5, xmm5, 255
	shufps	xmm7, xmm7, 255
	mulps	xmm6, xmm2
	mulps	xmm7, xmm4
	mulps	xmm5, xmm3
	mulps	xmm0, xmm1
	addps	xmm5, xmm7
	addps	xmm0, xmm6
	addps	xmm0, xmm5
	movaps	XMMWORD PTR [edx+48], xmm0
	ret
	.cfi_endproc
LFE548:
	.p2align 5,,31
	.globl	_fpu_matmult
	.def	_fpu_matmult;	.scl	2;	.type	32;	.endef
_fpu_matmult:
LFB549:
	.cfi_startproc
	mov	edx, DWORD PTR [esp+8]
	mov	eax, DWORD PTR [esp+12]
	mov	ecx, DWORD PTR [esp+4]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+16]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+32]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+48]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+16]
	fld	DWORD PTR [edx+16]
	fmul	DWORD PTR [eax+20]
	faddp	st(1), st
	fld	DWORD PTR [edx+32]
	fmul	DWORD PTR [eax+24]
	faddp	st(1), st
	fld	DWORD PTR [edx+48]
	fmul	DWORD PTR [eax+28]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+4]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+32]
	fld	DWORD PTR [edx+16]
	fmul	DWORD PTR [eax+36]
	faddp	st(1), st
	fld	DWORD PTR [edx+32]
	fmul	DWORD PTR [eax+40]
	faddp	st(1), st
	fld	DWORD PTR [edx+48]
	fmul	DWORD PTR [eax+44]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+8]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+48]
	fld	DWORD PTR [edx+16]
	fmul	DWORD PTR [eax+52]
	faddp	st(1), st
	fld	DWORD PTR [edx+32]
	fmul	DWORD PTR [eax+56]
	faddp	st(1), st
	fld	DWORD PTR [edx+48]
	fmul	DWORD PTR [eax+60]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+12]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+20]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+36]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+52]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+16]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+16]
	fld	DWORD PTR [edx+20]
	fmul	DWORD PTR [eax+20]
	faddp	st(1), st
	fld	DWORD PTR [edx+36]
	fmul	DWORD PTR [eax+24]
	faddp	st(1), st
	fld	DWORD PTR [edx+52]
	fmul	DWORD PTR [eax+28]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+20]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+32]
	fld	DWORD PTR [edx+20]
	fmul	DWORD PTR [eax+36]
	faddp	st(1), st
	fld	DWORD PTR [edx+36]
	fmul	DWORD PTR [eax+40]
	faddp	st(1), st
	fld	DWORD PTR [edx+52]
	fmul	DWORD PTR [eax+44]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+24]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+48]
	fld	DWORD PTR [edx+20]
	fmul	DWORD PTR [eax+52]
	faddp	st(1), st
	fld	DWORD PTR [edx+36]
	fmul	DWORD PTR [eax+56]
	faddp	st(1), st
	fld	DWORD PTR [edx+52]
	fmul	DWORD PTR [eax+60]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+28]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+24]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+40]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+56]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+32]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+16]
	fld	DWORD PTR [edx+24]
	fmul	DWORD PTR [eax+20]
	faddp	st(1), st
	fld	DWORD PTR [edx+40]
	fmul	DWORD PTR [eax+24]
	faddp	st(1), st
	fld	DWORD PTR [edx+56]
	fmul	DWORD PTR [eax+28]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+36]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+32]
	fld	DWORD PTR [edx+24]
	fmul	DWORD PTR [eax+36]
	faddp	st(1), st
	fld	DWORD PTR [edx+40]
	fmul	DWORD PTR [eax+40]
	faddp	st(1), st
	fld	DWORD PTR [edx+56]
	fmul	DWORD PTR [eax+44]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+40]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+48]
	fld	DWORD PTR [edx+24]
	fmul	DWORD PTR [eax+52]
	faddp	st(1), st
	fld	DWORD PTR [edx+40]
	fmul	DWORD PTR [eax+56]
	faddp	st(1), st
	fld	DWORD PTR [edx+56]
	fmul	DWORD PTR [eax+60]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+44]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+28]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+44]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+60]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+48]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax+16]
	fld	DWORD PTR [edx+28]
	fmul	DWORD PTR [eax+20]
	faddp	st(1), st
	fld	DWORD PTR [edx+44]
	fmul	DWORD PTR [eax+24]
	faddp	st(1), st
	fld	DWORD PTR [edx+60]
	fmul	DWORD PTR [eax+28]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+52]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax+32]
	fld	DWORD PTR [edx+28]
	fmul	DWORD PTR [eax+36]
	faddp	st(1), st
	fld	DWORD PTR [edx+44]
	fmul	DWORD PTR [eax+40]
	faddp	st(1), st
	fld	DWORD PTR [edx+60]
	fmul	DWORD PTR [eax+44]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+56]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax+48]
	fld	DWORD PTR [edx+28]
	fmul	DWORD PTR [eax+52]
	faddp	st(1), st
	fld	DWORD PTR [edx+44]
	fmul	DWORD PTR [eax+56]
	faddp	st(1), st
	fld	DWORD PTR [edx+60]
	fmul	DWORD PTR [eax+60]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+60]
	ret
	.cfi_endproc
LFE549:
	.p2align 5,,31
	.globl	_sse_rsqrt
	.def	_sse_rsqrt;	.scl	2;	.type	32;	.endef
_sse_rsqrt:
LFB550:
	.cfi_startproc
	sub	esp, 28
	.cfi_def_cfa_offset 32
	movss	xmm0, DWORD PTR [esp+32]
	rsqrtss	xmm0, xmm0
	movss	DWORD PTR [esp+12], xmm0
	fld	DWORD PTR [esp+12]
	add	esp, 28
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE550:
	.p2align 5,,31
	.globl	_fpu_rsqrt
	.def	_fpu_rsqrt;	.scl	2;	.type	32;	.endef
_fpu_rsqrt:
LFB551:
	.cfi_startproc
	sub	esp, 28
	.cfi_def_cfa_offset 32
	fld	DWORD PTR [esp+32]
	fld	st(0)
	fsqrt
	fucomi	st, st(0)
	jp	L35
	fstp	st(1)
L32:
	fdivr	DWORD PTR LC10
	add	esp, 28
	.cfi_remember_state
	.cfi_def_cfa_offset 4
	ret
L35:
	.cfi_restore_state
	ffreep	st(0)
	fstp	DWORD PTR [esp]
	call	_sqrtf
	jmp	L32
	.cfi_endproc
LFE551:
	.p2align 5,,31
	.globl	_sse_sqrt
	.def	_sse_sqrt;	.scl	2;	.type	32;	.endef
_sse_sqrt:
LFB552:
	.cfi_startproc
	sub	esp, 4
	.cfi_def_cfa_offset 8
	mov	eax, DWORD PTR [esp+8]
	sub	eax, 1065353216
	sar	eax
	add	eax, 1065353216
	mov	DWORD PTR [esp], eax
	fld	DWORD PTR [esp]
	add	esp, 4
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE552:
	.p2align 5,,31
	.globl	_fpu_sqrt
	.def	_fpu_sqrt;	.scl	2;	.type	32;	.endef
_fpu_sqrt:
LFB553:
	.cfi_startproc
	sub	esp, 28
	.cfi_def_cfa_offset 32
	fld	DWORD PTR [esp+32]
	fld	st(0)
	fsqrt
	fucomi	st, st(0)
	jp	L42
	fstp	st(1)
L39:
	add	esp, 28
	.cfi_remember_state
	.cfi_def_cfa_offset 4
	ret
L42:
	.cfi_restore_state
	ffreep	st(0)
	fstp	DWORD PTR [esp]
	call	_sqrtf
	jmp	L39
	.cfi_endproc
LFE553:
	.p2align 5,,31
	.globl	_sse_clamp
	.def	_sse_clamp;	.scl	2;	.type	32;	.endef
_sse_clamp:
LFB554:
	.cfi_startproc
	sub	esp, 28
	.cfi_def_cfa_offset 32
	movss	xmm0, DWORD PTR [esp+32]
	maxss	xmm0, XMMWORD PTR LC11
	minss	xmm0, XMMWORD PTR LC12
	movss	DWORD PTR [esp+12], xmm0
	fld	DWORD PTR [esp+12]
	add	esp, 28
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE554:
	.p2align 5,,31
	.globl	_fpu_clamp
	.def	_fpu_clamp;	.scl	2;	.type	32;	.endef
_fpu_clamp:
LFB555:
	.cfi_startproc
	fld	DWORD PTR [esp+4]
	fld1
	fchs
	fucomi	st, st(1)
	ja	L48
	ffreep	st(0)
	fld1
	fxch	st(1)
	fucomi	st, st(1)
	fcmovnbe	st, st(1)
	fstp	st(1)
	ret
	.p2align 5,,7
L48:
	fstp	st(1)
	ret
	.cfi_endproc
LFE555:
	.p2align 5,,31
	.globl	_sse_abs
	.def	_sse_abs;	.scl	2;	.type	32;	.endef
_sse_abs:
LFB556:
	.cfi_startproc
	mov	edx, DWORD PTR [esp+4]
	mov	eax, edx
	sar	eax, 31
	xor	eax, edx
	ret
	.cfi_endproc
LFE556:
	.p2align 5,,31
	.globl	_fpu_abs
	.def	_fpu_abs;	.scl	2;	.type	32;	.endef
_fpu_abs:
LFB557:
	.cfi_startproc
	mov	eax, DWORD PTR [esp+4]
	cdq
	xor	eax, edx
	sub	eax, edx
	ret
	.cfi_endproc
LFE557:
	.p2align 5,,31
	.globl	_sse_fabs
	.def	_sse_fabs;	.scl	2;	.type	32;	.endef
_sse_fabs:
LFB558:
	.cfi_startproc
	sub	esp, 4
	.cfi_def_cfa_offset 8
	mov	eax, DWORD PTR [esp+8]
	and	eax, 2147483647
	mov	DWORD PTR [esp], eax
	fld	DWORD PTR [esp]
	add	esp, 4
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE558:
	.p2align 5,,31
	.globl	_fpu_fabs
	.def	_fpu_fabs;	.scl	2;	.type	32;	.endef
_fpu_fabs:
LFB559:
	.cfi_startproc
	fld	DWORD PTR [esp+4]
	fabs
	ret
	.cfi_endproc
LFE559:
	.p2align 5,,31
	.globl	_sse_sel
	.def	_sse_sel;	.scl	2;	.type	32;	.endef
_sse_sel:
LFB560:
	.cfi_startproc
	sub	esp, 4
	.cfi_def_cfa_offset 8
	mov	eax, DWORD PTR [esp+8]
	cdq
	mov	eax, edx
	and	edx, DWORD PTR [esp+16]
	not	eax
	and	eax, DWORD PTR [esp+12]
	or	eax, edx
	mov	DWORD PTR [esp], eax
	fld	DWORD PTR [esp]
	add	esp, 4
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE560:
	.p2align 5,,31
	.globl	_fpu_sel
	.def	_fpu_sel;	.scl	2;	.type	32;	.endef
_fpu_sel:
LFB561:
	.cfi_startproc
	fldz
	fld	DWORD PTR [esp+4]
	fxch	st(1)
	fucomip	st, st(1)
	ffreep	st(0)
	fld	DWORD PTR [esp+8]
	fld	DWORD PTR [esp+12]
	fcmovbe	st, st(1)
	fstp	st(1)
	fst	DWORD PTR [esp+12]
	ret
	.cfi_endproc
LFE561:
	.p2align 5,,31
	.globl	_sse_diffabsaccum
	.def	_sse_diffabsaccum;	.scl	2;	.type	32;	.endef
_sse_diffabsaccum:
LFB562:
	.cfi_startproc
	sub	esp, 44
	.cfi_def_cfa_offset 48
	mov	eax, DWORD PTR [esp+48]
	mov	DWORD PTR [esp+24], eax
	mov	eax, DWORD PTR [esp+52]
	mov	DWORD PTR [esp+28], eax
	mov	eax, DWORD PTR [esp+56]
	movupd	xmm0, XMMWORD PTR [esp+24]
	mov	DWORD PTR [esp+16], eax
	mov	eax, DWORD PTR [esp+60]
	mov	DWORD PTR [esp+20], eax
	movupd	xmm1, XMMWORD PTR [esp+16]
	subpd	xmm0, xmm1
	andpd	xmm0, XMMWORD PTR _mask
	movsd	QWORD PTR [esp+8], xmm0
	fld	QWORD PTR [esp+8]
	add	esp, 44
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE562:
	.p2align 5,,31
	.globl	_fpu_diffabsaccum
	.def	_fpu_diffabsaccum;	.scl	2;	.type	32;	.endef
_fpu_diffabsaccum:
LFB563:
	.cfi_startproc
	fld	QWORD PTR [esp+12]
	fsubr	QWORD PTR [esp+4]
	fabs
	ret
	.cfi_endproc
LFE563:
	.p2align 5,,31
	.globl	_sse_muldiv
	.def	_sse_muldiv;	.scl	2;	.type	32;	.endef
_sse_muldiv:
LFB564:
	.cfi_startproc
	sub	esp, 12
	.cfi_def_cfa_offset 16
	fld	QWORD PTR [esp+16]
	fisttp	DWORD PTR [esp+4]
	mov	eax, DWORD PTR [esp+4]
	cdq
	fld	QWORD PTR [esp+24]
	idiv	DWORD PTR _A1
	fisttp	DWORD PTR [esp+4]
	cdq
	idiv	DWORD PTR _B1
	cdq
	idiv	DWORD PTR _C1
	mov	ecx, eax
	mov	eax, DWORD PTR [esp+4]
	cdq
	idiv	DWORD PTR _D1
	cdq
	idiv	DWORD PTR _E1
	cdq
	idiv	DWORD PTR _F1
	add	eax, ecx
	mov	DWORD PTR [esp+4], eax
	fild	DWORD PTR [esp+4]
	add	esp, 12
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE564:
	.p2align 5,,31
	.globl	_fpu_muldiv
	.def	_fpu_muldiv;	.scl	2;	.type	32;	.endef
_fpu_muldiv:
LFB565:
	.cfi_startproc
	fld	QWORD PTR [esp+4]
	fld	QWORD PTR [esp+12]
	fxch	st(1)
	fmul	QWORD PTR _A2
	fmul	QWORD PTR _B2
	fmul	QWORD PTR _C2
	fxch	st(1)
	fmul	QWORD PTR _D2
	fmul	QWORD PTR _E2
	fmul	QWORD PTR _F2
	faddp	st(1), st
	ret
	.cfi_endproc
LFE565:
	.p2align 5,,31
	.globl	_timer
	.def	_timer;	.scl	2;	.type	32;	.endef
_timer:
LFB566:
	.cfi_startproc
	sub	esp, 60
	.cfi_def_cfa_offset 64
	lea	eax, [esp+32]
	mov	DWORD PTR [esp], eax
	call	_QueryPerformanceCounter@4
	.cfi_def_cfa_offset 60
	sub	esp, 4
	.cfi_def_cfa_offset 64
	lea	eax, [esp+40]
	mov	DWORD PTR [esp], eax
	call	_QueryPerformanceFrequency@4
	.cfi_def_cfa_offset 60
	sub	esp, 4
	.cfi_def_cfa_offset 64
	mov	edx, DWORD PTR [esp+36]
	fild	QWORD PTR [esp+32]
	test	edx, edx
	js	L71
L68:
	fstp	QWORD PTR [esp+24]
	mov	eax, DWORD PTR [esp+44]
	fld	QWORD PTR [esp+24]
	test	eax, eax
	fild	QWORD PTR [esp+40]
	js	L72
	fstp	QWORD PTR [esp+24]
	fld	QWORD PTR [esp+24]
	add	esp, 60
	.cfi_remember_state
	.cfi_def_cfa_offset 4
	fdivp	st(1), st
	ret
	.p2align 5,,7
L71:
	.cfi_restore_state
	fadd	DWORD PTR LC14
	jmp	L68
	.p2align 5,,7
L72:
	fadd	DWORD PTR LC14
	fstp	QWORD PTR [esp+24]
	fld	QWORD PTR [esp+24]
	add	esp, 60
	.cfi_def_cfa_offset 4
	fdivp	st(1), st
	ret
	.cfi_endproc
LFE566:
	.p2align 5,,31
	.globl	_rnd
	.def	_rnd;	.scl	2;	.type	32;	.endef
_rnd:
LFB567:
	.cfi_startproc
	sub	esp, 28
	.cfi_def_cfa_offset 32
	call	_rand
	mov	edx, 274877907
	mov	ecx, eax
	imul	edx
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [esp+12], ecx
	fild	DWORD PTR [esp+12]
	fdiv	DWORD PTR LC15
	fld	DWORD PTR [esp+32]
	fsubr	DWORD PTR [esp+36]
	fmulp	st(1), st
	fadd	DWORD PTR [esp+32]
	add	esp, 28
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE567:
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
	.align 4
LC19:
	.ascii "double multiply versus integer divide\0"
LC20:
	.ascii "Running %s test...\12\0"
	.align 4
LC21:
	.ascii "SSE Total Time  : %f second(s)\12\0"
	.align 4
LC22:
	.ascii "FPU Total Time  : %f second(s)\12\0"
	.align 4
LC24:
	.ascii "SSE Average Time: %f second(s)\12\0"
	.align 4
LC25:
	.ascii "FPU Average Time: %f second(s)\12\0"
	.align 4
LC26:
	.ascii "Writing report to 'results.txt'...\0"
LC27:
	.ascii "wb\0"
LC28:
	.ascii "results.txt\0"
LC29:
	.ascii "sse: %f\12\0"
LC30:
	.ascii "fpu: %f\12\0"
	.align 4
LC31:
	.ascii "failed to write to 'results.txt'\0"
	.section	.text.startup,"x"
	.p2align 5,,31
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB568:
	.cfi_startproc
	lea	ecx, [esp+4]
	.cfi_def_cfa 1, 0
	and	esp, -16
	push	DWORD PTR [ecx-4]
	push	ebp
	.cfi_escape 0x10,0x5,0x2,0x75,0
	mov	ebp, esp
	push	edi
	push	esi
	.cfi_escape 0x10,0x7,0x2,0x75,0x7c
	.cfi_escape 0x10,0x6,0x2,0x75,0x78
	mov	esi, 274877907
	push	ebx
	.cfi_escape 0x10,0x3,0x2,0x75,0x74
	xor	ebx, ebx
	push	ecx
	.cfi_escape 0xf,0x3,0x75,0x70,0x6
	sub	esp, 136
	call	___main
L76:
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	lea	edi, [0+ebx*8]
	sar	eax, 31
	prefetchw	_g_vals1[edi+32]
	prefetchw	_g_vals2[edi+32]
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals1[0+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals2[0+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals1[8+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals2[8+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals1[16+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals2[16+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals1[24+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals2[24+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals1[32+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	edx, 14
	sar	eax, 31
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals2[32+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals1[40+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals2[40+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals1[48+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals2[48+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals1[56+ebx*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	mov	eax, 2040
	imul	edx, edx, 256000
	sub	eax, ebx
	mov	DWORD PTR [ebp-64], eax
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals2[56+ebx*8]
	add	ebx, 8
	cmp	ebx, 2040
	jne	L76
	xor	bx, bx
	mov	esi, 274877907
	.p2align 5,,24
L77:
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	lea	edi, [ebx+2040]
	sar	eax, 31
	inc	ebx
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals1[0+edi*8]
	call	_rand
	mov	ecx, eax
	imul	esi
	mov	eax, ecx
	sar	eax, 31
	sar	edx, 14
	sub	edx, eax
	imul	edx, edx, 256000
	sub	ecx, edx
	cmp	ebx, DWORD PTR [ebp-64]
	mov	DWORD PTR [ebp-52], ecx
	fild	DWORD PTR [ebp-52]
	fdiv	DWORD PTR LC15
	fmul	DWORD PTR LC16
	fsub	DWORD PTR LC17
	fstp	QWORD PTR _g_vals2[0+edi*8]
	jne	L77
	mov	DWORD PTR [esp+4], 0x447a0000
	mov	DWORD PTR [esp], 0x3f800000
	call	_rnd
	mov	DWORD PTR [esp+4], 0x447a0000
	mov	DWORD PTR [esp], 0x3f800000
	fstp	QWORD PTR _A2
	call	_rnd
	mov	DWORD PTR [esp+4], 0x447a0000
	fstp	QWORD PTR _B2
	mov	DWORD PTR [esp], 0x3f800000
	call	_rnd
	mov	DWORD PTR [esp+4], 0x447a0000
	fstp	QWORD PTR _C2
	mov	DWORD PTR [esp], 0x3f800000
	call	_rnd
	mov	DWORD PTR [esp+4], 0x447a0000
	fstp	QWORD PTR _D2
	mov	DWORD PTR [esp], 0x3f800000
	call	_rnd
	mov	DWORD PTR [esp+4], 0x447a0000
	fstp	QWORD PTR _E2
	mov	DWORD PTR [esp], 0x3f800000
	call	_rnd
	mov	DWORD PTR [esp+4], OFFSET FLAT:LC19
	fst	QWORD PTR _F2
	mov	DWORD PTR [esp], OFFSET FLAT:LC20
	fld	QWORD PTR _A2
	fisttp	DWORD PTR _A1
	fld	QWORD PTR _B2
	fisttp	DWORD PTR _B1
	fld	QWORD PTR _C2
	fisttp	DWORD PTR _C1
	fld	QWORD PTR _D2
	fisttp	DWORD PTR _D1
	fld	QWORD PTR _E2
	fisttp	DWORD PTR _E1
	fisttp	DWORD PTR _F1
	call	_printf
	mov	eax, DWORD PTR __imp___iob
	add	eax, 32
	mov	DWORD PTR [esp], eax
	call	_fflush
	fldz
	mov	DWORD PTR [ebp-84], 1024000
	fst	QWORD PTR [ebp-96]
	fstp	QWORD PTR [ebp-104]
	.p2align 5,,24
L90:
	fldz
	lea	eax, [ebp-40]
	mov	DWORD PTR [esp], eax
	fst	QWORD PTR _g_result+8
	fstp	QWORD PTR _g_result
	call	_QueryPerformanceCounter@4
	lea	eax, [ebp-32]
	sub	esp, 4
	mov	DWORD PTR [esp], eax
	call	_QueryPerformanceFrequency@4
	mov	eax, DWORD PTR [ebp-36]
	sub	esp, 4
	test	eax, eax
	fild	QWORD PTR [ebp-40]
	js	L103
L78:
	fstp	QWORD PTR [ebp-48]
	mov	eax, DWORD PTR [ebp-28]
	fld	QWORD PTR [ebp-48]
	test	eax, eax
	fild	QWORD PTR [ebp-32]
	js	L104
L79:
	fstp	QWORD PTR [ebp-48]
	mov	eax, DWORD PTR _B1
	mov	edi, DWORD PTR _A1
	mov	ebx, DWORD PTR _F1
	xor	esi, esi
	fld	QWORD PTR [ebp-48]
	mov	DWORD PTR [ebp-64], eax
	mov	eax, DWORD PTR _C1
	fdivp	st(1), st
	mov	DWORD PTR [ebp-72], eax
	mov	eax, DWORD PTR _D1
	mov	DWORD PTR [ebp-56], eax
	mov	eax, DWORD PTR _E1
	mov	DWORD PTR [ebp-76], eax
	fstp	QWORD PTR [ebp-112]
	fld	QWORD PTR _g_result
	.p2align 5,,24
L80:
	fld	QWORD PTR _g_vals1[0+esi*8]
	lea	eax, [0+esi*8]
	prefetcht0	_g_vals2[eax+24]
	prefetcht0	_g_vals1[eax+24]
	fisttp	DWORD PTR [ebp-52]
	mov	eax, DWORD PTR [ebp-52]
	cdq
	fld	QWORD PTR _g_vals2[0+esi*8]
	idiv	edi
	fisttp	DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-64]
	cdq
	idiv	DWORD PTR [ebp-72]
	mov	ecx, eax
	mov	eax, DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-56]
	cdq
	idiv	DWORD PTR [ebp-76]
	cdq
	idiv	ebx
	add	eax, ecx
	mov	DWORD PTR [ebp-52], eax
	fild	DWORD PTR [ebp-52]
	faddp	st(1), st
	fld	QWORD PTR _g_vals1[8+esi*8]
	fisttp	DWORD PTR [ebp-52]
	mov	eax, DWORD PTR [ebp-52]
	cdq
	fld	QWORD PTR _g_vals2[8+esi*8]
	idiv	edi
	fisttp	DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-64]
	cdq
	idiv	DWORD PTR [ebp-72]
	mov	ecx, eax
	mov	eax, DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-56]
	cdq
	idiv	DWORD PTR [ebp-76]
	cdq
	idiv	ebx
	add	eax, ecx
	mov	DWORD PTR [ebp-52], eax
	fild	DWORD PTR [ebp-52]
	faddp	st(1), st
	fld	QWORD PTR _g_vals1[16+esi*8]
	fisttp	DWORD PTR [ebp-52]
	mov	eax, DWORD PTR [ebp-52]
	cdq
	idiv	edi
	fld	QWORD PTR _g_vals2[16+esi*8]
	fisttp	DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-64]
	cdq
	idiv	DWORD PTR [ebp-72]
	mov	ecx, eax
	mov	eax, DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-56]
	cdq
	idiv	DWORD PTR [ebp-76]
	cdq
	idiv	ebx
	add	eax, ecx
	mov	DWORD PTR [ebp-52], eax
	fild	DWORD PTR [ebp-52]
	faddp	st(1), st
	fld	QWORD PTR _g_vals1[24+esi*8]
	fisttp	DWORD PTR [ebp-52]
	mov	eax, DWORD PTR [ebp-52]
	cdq
	fld	QWORD PTR _g_vals2[24+esi*8]
	idiv	edi
	fisttp	DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-64]
	cdq
	idiv	DWORD PTR [ebp-72]
	mov	ecx, eax
	mov	eax, DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-56]
	cdq
	idiv	DWORD PTR [ebp-76]
	cdq
	idiv	ebx
	add	eax, ecx
	mov	DWORD PTR [ebp-52], eax
	fild	DWORD PTR [ebp-52]
	faddp	st(1), st
	fld	QWORD PTR _g_vals1[32+esi*8]
	fisttp	DWORD PTR [ebp-52]
	mov	eax, DWORD PTR [ebp-52]
	cdq
	fld	QWORD PTR _g_vals2[32+esi*8]
	idiv	edi
	cdq
	idiv	DWORD PTR [ebp-64]
	cdq
	idiv	DWORD PTR [ebp-72]
	fisttp	DWORD PTR [ebp-52]
	mov	ecx, eax
	mov	eax, DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-56]
	cdq
	idiv	DWORD PTR [ebp-76]
	cdq
	idiv	ebx
	add	eax, ecx
	mov	DWORD PTR [ebp-52], eax
	fild	DWORD PTR [ebp-52]
	faddp	st(1), st
	fld	QWORD PTR _g_vals1[40+esi*8]
	fisttp	DWORD PTR [ebp-52]
	mov	eax, DWORD PTR [ebp-52]
	cdq
	fld	QWORD PTR _g_vals2[40+esi*8]
	idiv	edi
	fisttp	DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-64]
	cdq
	idiv	DWORD PTR [ebp-72]
	mov	ecx, eax
	mov	eax, DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-56]
	cdq
	idiv	DWORD PTR [ebp-76]
	cdq
	idiv	ebx
	add	eax, ecx
	mov	DWORD PTR [ebp-52], eax
	fild	DWORD PTR [ebp-52]
	faddp	st(1), st
	fld	QWORD PTR _g_vals1[48+esi*8]
	fisttp	DWORD PTR [ebp-52]
	mov	eax, DWORD PTR [ebp-52]
	cdq
	fld	QWORD PTR _g_vals2[48+esi*8]
	idiv	edi
	fisttp	DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-64]
	cdq
	idiv	DWORD PTR [ebp-72]
	mov	ecx, eax
	mov	eax, DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-56]
	cdq
	idiv	DWORD PTR [ebp-76]
	cdq
	idiv	ebx
	add	eax, ecx
	mov	DWORD PTR [ebp-52], eax
	fild	DWORD PTR [ebp-52]
	faddp	st(1), st
	fld	QWORD PTR _g_vals1[56+esi*8]
	fisttp	DWORD PTR [ebp-52]
	mov	eax, DWORD PTR [ebp-52]
	cdq
	fld	QWORD PTR _g_vals2[56+esi*8]
	idiv	edi
	fisttp	DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-64]
	cdq
	idiv	DWORD PTR [ebp-72]
	mov	ecx, eax
	mov	eax, DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-56]
	cdq
	idiv	DWORD PTR [ebp-76]
	cdq
	idiv	ebx
	add	eax, ecx
	lea	ecx, [esi+8]
	mov	DWORD PTR [ebp-52], eax
	mov	eax, 2040
	fild	DWORD PTR [ebp-52]
	sub	eax, esi
	cmp	ecx, 2040
	mov	esi, ecx
	faddp	st(1), st
	jne	L80
	add	eax, 2040
	mov	DWORD PTR [ebp-80], eax
	.p2align 5,,24
L81:
	fld	QWORD PTR _g_vals1[0+ecx*8]
	fisttp	DWORD PTR [ebp-52]
	mov	eax, DWORD PTR [ebp-52]
	cdq
	fld	QWORD PTR _g_vals2[0+ecx*8]
	inc	ecx
	idiv	edi
	fisttp	DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-64]
	cdq
	idiv	DWORD PTR [ebp-72]
	mov	esi, eax
	mov	eax, DWORD PTR [ebp-52]
	cdq
	idiv	DWORD PTR [ebp-56]
	cdq
	idiv	DWORD PTR [ebp-76]
	cdq
	idiv	ebx
	add	eax, esi
	cmp	ecx, DWORD PTR [ebp-80]
	mov	DWORD PTR [ebp-52], eax
	fild	DWORD PTR [ebp-52]
	faddp	st(1), st
	jne	L81
	lea	eax, [ebp-40]
	fstp	QWORD PTR _g_result
	mov	DWORD PTR [esp], eax
	call	_QueryPerformanceCounter@4
	lea	eax, [ebp-32]
	sub	esp, 4
	mov	DWORD PTR [esp], eax
	call	_QueryPerformanceFrequency@4
	mov	eax, DWORD PTR [ebp-36]
	sub	esp, 4
	fild	QWORD PTR [ebp-40]
	test	eax, eax
	js	L105
L82:
	fstp	QWORD PTR [ebp-48]
	mov	eax, DWORD PTR [ebp-28]
	fld	QWORD PTR [ebp-48]
	test	eax, eax
	fild	QWORD PTR [ebp-32]
	js	L106
L83:
	fstp	QWORD PTR [ebp-48]
	lea	eax, [ebp-40]
	mov	DWORD PTR [esp], eax
	fld	QWORD PTR [ebp-48]
	fdivp	st(1), st
	fsub	QWORD PTR [ebp-112]
	fadd	QWORD PTR [ebp-96]
	fstp	QWORD PTR [ebp-96]
	call	_QueryPerformanceCounter@4
	lea	eax, [ebp-32]
	sub	esp, 4
	mov	DWORD PTR [esp], eax
	call	_QueryPerformanceFrequency@4
	mov	edi, DWORD PTR [ebp-36]
	sub	esp, 4
	fild	QWORD PTR [ebp-40]
	test	edi, edi
	js	L107
L84:
	fstp	QWORD PTR [ebp-48]
	mov	esi, DWORD PTR [ebp-28]
	fld	QWORD PTR [ebp-48]
	test	esi, esi
	fild	QWORD PTR [ebp-32]
	js	L108
L85:
	fstp	QWORD PTR [ebp-48]
	xor	edx, edx
	fld	QWORD PTR [ebp-48]
	fdivp	st(1), st
	fstp	QWORD PTR [ebp-72]
	fld	QWORD PTR _g_result+8
	fld	QWORD PTR _A2
	fld	QWORD PTR _B2
	fstp	QWORD PTR [ebp-64]
	fld	QWORD PTR _C2
	fld	QWORD PTR _D2
	fld	QWORD PTR _E2
	fld	QWORD PTR _F2
	.p2align 5,,24
L86:
	fld	QWORD PTR _g_vals1[0+edx*8]
	lea	eax, [0+edx*8]
	mov	ecx, 2040
	prefetcht0	_g_vals2[eax+104]
	prefetcht0	_g_vals1[eax+104]
	lea	eax, [edx+8]
	fmul	st, st(5)
	sub	ecx, edx
	cmp	eax, 2040
	fmul	QWORD PTR [ebp-64]
	fmul	st, st(4)
	fld	QWORD PTR _g_vals2[0+edx*8]
	fmul	st, st(4)
	fmul	st, st(3)
	fmul	st, st(2)
	faddp	st(1), st
	faddp	st(6), st
	fld	QWORD PTR _g_vals1[8+edx*8]
	fmul	st, st(5)
	fmul	QWORD PTR [ebp-64]
	fmul	st, st(4)
	fld	QWORD PTR _g_vals2[8+edx*8]
	fmul	st, st(4)
	fmul	st, st(3)
	fmul	st, st(2)
	faddp	st(1), st
	faddp	st(6), st
	fld	QWORD PTR _g_vals1[16+edx*8]
	fmul	st, st(5)
	fmul	QWORD PTR [ebp-64]
	fmul	st, st(4)
	fld	QWORD PTR _g_vals2[16+edx*8]
	fmul	st, st(4)
	fmul	st, st(3)
	fmul	st, st(2)
	faddp	st(1), st
	faddp	st(6), st
	fld	QWORD PTR _g_vals1[24+edx*8]
	fmul	st, st(5)
	fmul	QWORD PTR [ebp-64]
	fmul	st, st(4)
	fld	QWORD PTR _g_vals2[24+edx*8]
	fmul	st, st(4)
	fmul	st, st(3)
	fmul	st, st(2)
	faddp	st(1), st
	faddp	st(6), st
	fld	QWORD PTR _g_vals1[32+edx*8]
	fmul	st, st(5)
	fmul	QWORD PTR [ebp-64]
	fmul	st, st(4)
	fld	QWORD PTR _g_vals2[32+edx*8]
	fmul	st, st(4)
	fmul	st, st(3)
	fmul	st, st(2)
	faddp	st(1), st
	faddp	st(6), st
	fld	QWORD PTR _g_vals1[40+edx*8]
	fmul	st, st(5)
	fmul	QWORD PTR [ebp-64]
	fmul	st, st(4)
	fld	QWORD PTR _g_vals2[40+edx*8]
	fmul	st, st(4)
	fmul	st, st(3)
	fmul	st, st(2)
	faddp	st(1), st
	faddp	st(6), st
	fld	QWORD PTR _g_vals1[48+edx*8]
	fmul	st, st(5)
	fmul	QWORD PTR [ebp-64]
	fmul	st, st(4)
	fld	QWORD PTR _g_vals2[48+edx*8]
	fmul	st, st(4)
	fmul	st, st(3)
	fmul	st, st(2)
	faddp	st(1), st
	faddp	st(6), st
	fld	QWORD PTR _g_vals1[56+edx*8]
	fmul	st, st(5)
	fmul	QWORD PTR [ebp-64]
	fmul	st, st(4)
	fld	QWORD PTR _g_vals2[56+edx*8]
	mov	edx, eax
	fmul	st, st(4)
	fmul	st, st(3)
	fmul	st, st(2)
	faddp	st(1), st
	faddp	st(6), st
	jne	L86
	lea	edx, [ecx+2040]
	.p2align 5,,24
L87:
	fld	QWORD PTR _g_vals1[0+eax*8]
	fmul	st, st(5)
	fmul	QWORD PTR [ebp-64]
	fmul	st, st(4)
	fld	QWORD PTR _g_vals2[0+eax*8]
	inc	eax
	cmp	eax, edx
	fmul	st, st(4)
	fmul	st, st(3)
	fmul	st, st(2)
	faddp	st(1), st
	faddp	st(6), st
	jne	L87
	ffreep	st(0)
	ffreep	st(0)
	ffreep	st(0)
	ffreep	st(0)
	ffreep	st(0)
	lea	eax, [ebp-40]
	fstp	QWORD PTR _g_result+8
	mov	DWORD PTR [esp], eax
	call	_QueryPerformanceCounter@4
	lea	eax, [ebp-32]
	sub	esp, 4
	mov	DWORD PTR [esp], eax
	call	_QueryPerformanceFrequency@4
	mov	ebx, DWORD PTR [ebp-36]
	sub	esp, 4
	fild	QWORD PTR [ebp-40]
	test	ebx, ebx
	js	L109
L88:
	fstp	QWORD PTR [ebp-48]
	mov	ecx, DWORD PTR [ebp-28]
	fld	QWORD PTR [ebp-48]
	test	ecx, ecx
	fild	QWORD PTR [ebp-32]
	js	L110
L89:
	fstp	QWORD PTR [ebp-48]
	dec	DWORD PTR [ebp-84]
	fld	QWORD PTR [ebp-48]
	fdivp	st(1), st
	fsub	QWORD PTR [ebp-72]
	fadd	QWORD PTR [ebp-104]
	fstp	QWORD PTR [ebp-104]
	jne	L90
	fld	QWORD PTR [ebp-96]
	mov	DWORD PTR [esp], OFFSET FLAT:LC21
	fstp	QWORD PTR [esp+4]
	call	_printf
	fld	QWORD PTR [ebp-104]
	mov	DWORD PTR [esp], OFFSET FLAT:LC22
	fstp	QWORD PTR [esp+4]
	call	_printf
	mov	DWORD PTR [esp], 10
	call	_putchar
	fld	DWORD PTR LC23
	mov	DWORD PTR [esp], OFFSET FLAT:LC24
	fld	QWORD PTR [ebp-96]
	fdiv	st, st(1)
	fst	QWORD PTR [ebp-64]
	fstp	QWORD PTR [esp+4]
	fstp	QWORD PTR [ebp-136]
	call	_printf
	mov	DWORD PTR [esp], OFFSET FLAT:LC25
	fld	QWORD PTR [ebp-136]
	fdivr	QWORD PTR [ebp-104]
	fst	QWORD PTR [ebp-72]
	fstp	QWORD PTR [esp+4]
	call	_printf
	mov	DWORD PTR [esp], OFFSET FLAT:LC26
	call	_puts
	mov	eax, DWORD PTR __imp___iob
	add	eax, 32
	mov	DWORD PTR [esp], eax
	call	_fflush
	mov	DWORD PTR [esp+4], OFFSET FLAT:LC27
	mov	DWORD PTR [esp], OFFSET FLAT:LC28
	call	_fopen
	test	eax, eax
	mov	ebx, eax
	je	L91
	fld	QWORD PTR [ebp-96]
	mov	DWORD PTR [esp+4], OFFSET FLAT:LC21
	mov	esi, 2048
	mov	DWORD PTR [esp], eax
	fstp	QWORD PTR [esp+8]
	call	_fprintf
	fld	QWORD PTR [ebp-104]
	mov	DWORD PTR [esp+4], OFFSET FLAT:LC22
	mov	DWORD PTR [esp], ebx
	fstp	QWORD PTR [esp+8]
	call	_fprintf
	mov	DWORD PTR [esp+4], ebx
	mov	DWORD PTR [esp], 10
	call	_fputc
	fld	QWORD PTR [ebp-64]
	mov	DWORD PTR [esp+4], OFFSET FLAT:LC24
	mov	DWORD PTR [esp], ebx
	fstp	QWORD PTR [esp+8]
	call	_fprintf
	fld	QWORD PTR [ebp-72]
	mov	DWORD PTR [esp+4], OFFSET FLAT:LC25
	mov	DWORD PTR [esp], ebx
	fstp	QWORD PTR [esp+8]
	call	_fprintf
	mov	DWORD PTR [esp+4], ebx
	mov	DWORD PTR [esp], 10
	call	_fputc
L92:
	fld	QWORD PTR _g_result
	mov	DWORD PTR [esp+4], OFFSET FLAT:LC29
	mov	DWORD PTR [esp], ebx
	fstp	QWORD PTR [esp+8]
	call	_fprintf
	fld	QWORD PTR _g_result+8
	mov	DWORD PTR [esp+4], OFFSET FLAT:LC30
	mov	DWORD PTR [esp], ebx
	fstp	QWORD PTR [esp+8]
	call	_fprintf
	mov	DWORD PTR [esp+4], ebx
	mov	DWORD PTR [esp], 10
	call	_fputc
	dec	esi
	jne	L92
	mov	DWORD PTR [esp], ebx
	call	_fclose
	lea	esp, [ebp-16]
	xor	eax, eax
	pop	ecx
	.cfi_remember_state
	.cfi_restore 1
	.cfi_def_cfa 1, 0
	pop	ebx
	.cfi_restore 3
	pop	esi
	.cfi_restore 6
	pop	edi
	.cfi_restore 7
	pop	ebp
	.cfi_restore 5
	lea	esp, [ecx-4]
	.cfi_def_cfa 4, 4
	ret
L110:
	.cfi_restore_state
	fadd	DWORD PTR LC14
	jmp	L89
L109:
	fadd	DWORD PTR LC14
	jmp	L88
L108:
	fadd	DWORD PTR LC14
	jmp	L85
L107:
	fadd	DWORD PTR LC14
	jmp	L84
L106:
	fadd	DWORD PTR LC14
	jmp	L83
L105:
	fadd	DWORD PTR LC14
	jmp	L82
L104:
	fadd	DWORD PTR LC14
	jmp	L79
L103:
	fadd	DWORD PTR LC14
	jmp	L78
L91:
	mov	DWORD PTR [esp], OFFSET FLAT:LC31
	call	_perror
	lea	esp, [ebp-16]
	xor	eax, eax
	pop	ecx
	.cfi_restore 1
	.cfi_def_cfa 1, 0
	pop	ebx
	.cfi_restore 3
	pop	esi
	.cfi_restore 6
	pop	edi
	.cfi_restore 7
	pop	ebp
	.cfi_restore 5
	lea	esp, [ecx-4]
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE568:
	.comm	_F2, 8, 4
	.comm	_E2, 8, 4
	.comm	_D2, 8, 4
	.comm	_C2, 8, 4
	.comm	_B2, 8, 4
	.comm	_A2, 8, 4
	.comm	_F1, 4, 4
	.comm	_E1, 4, 4
	.comm	_D1, 4, 4
	.comm	_C1, 4, 4
	.comm	_B1, 4, 4
	.comm	_A1, 4, 4
	.comm	_mask, 16, 4
.lcomm _g_sin_tb,262144,32
.lcomm _g_vals1,16384,32
.lcomm _g_vals2,16384,32
.lcomm _g_result,16,8
	.section .rdata,"dr"
	.align 16
LC1:
	.long	1065353216
	.long	-1082130432
	.long	1065353216
	.long	-1082130432
	.align 4
LC3:
	.long	931135488
	.align 8
LC4:
	.long	1413754136
	.long	1074340347
	.align 4
LC5:
	.long	1135869952
	.align 4
LC6:
	.long	1199570944
	.align 4
LC7:
	.long	1119092736
	.align 4
LC8:
	.long	1127481344
	.align 4
LC9:
	.long	1078530011
	.align 4
LC10:
	.long	1065353216
	.align 16
LC11:
	.long	-1082130432
	.long	0
	.long	0
	.long	0
	.align 16
LC12:
	.long	1065353216
	.long	0
	.long	0
	.long	0
	.align 4
LC14:
	.long	1602224128
	.align 4
LC15:
	.long	1215954944
	.align 4
LC16:
	.long	1169915904
	.align 4
LC17:
	.long	1161527296
	.align 4
LC23:
	.long	1232732160
	.def	_sin;	.scl	2;	.type	32;	.endef
	.def	_sinf;	.scl	2;	.type	32;	.endef
	.def	_cosf;	.scl	2;	.type	32;	.endef
	.def	_sqrtf;	.scl	2;	.type	32;	.endef
	.def	_QueryPerformanceCounter@4;	.scl	2;	.type	32;	.endef
	.def	_QueryPerformanceFrequency@4;	.scl	2;	.type	32;	.endef
	.def	_rand;	.scl	2;	.type	32;	.endef
	.def	_putchar;	.scl	2;	.type	32;	.endef
	.def	_puts;	.scl	2;	.type	32;	.endef
	.def	_fputc;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
	.def	_fflush;	.scl	2;	.type	32;	.endef
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_fprintf;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef
	.def	_perror;	.scl	2;	.type	32;	.endef
