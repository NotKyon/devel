// Headers
#include <stdio.h>
#include <stdlib.h>

// Program
int main() {

	// Greet
	printf( "Hello, world!\n" );

	// Leave
	return EXIT_SUCCESS;
	
}
