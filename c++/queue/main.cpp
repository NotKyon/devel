#include "Queue.h"

Queue<int> gQueue;

int main() {
	gQueue.SetGranularity(4096);

	gQueue.Push(1);
	gQueue.Push(2);
	gQueue.Push(3);
	gQueue.Push(4);
	gQueue.Push(5);
	gQueue.Push(6);
	gQueue.Push(7);
	gQueue.Push(8);

	while(!gQueue.IsEmpty())
		printf("%i\n", gQueue.Pull());

	return EXIT_SUCCESS;
}
