#include "matrix.h"

/*
 * Create an identity quaternion.
 */
void Quat_Identity(quat_t *dst) {
	dst->w = 1;
	dst->x = 0;
	dst->y = 0;
	dst->z = 0;
}

/*
 * Create a quaternion from an axis angle.
 */
void Quat_Axis(quat_t *dst, float angle, float x, float y, float z) {
	float c, s, t;

	t = 0.5f*angle;
	c = Cos(t);
	s = Sin(t);

	dst->w = c;
	dst->x = s*x;
	dst->y = s*y;
	dst->z = s*z;
}
void Quat_RotationX(quat_t *dst, float angle) {
	Quat_Axis(dst, angle, 1, 0, 0);
}
void Quat_RotationY(quat_t *dst, float angle) {
	Quat_Axis(dst, angle, 0, 1, 0);
}
void Quat_RotationZ(quat_t *dst, float angle) {
	Quat_Axis(dst, angle, 0, 0, 1);
}

/*
 * Multiply two quaternions.
 */
void Quat_Multiply(quat_t *dst, const quat_t *a, const quat_t *b) {
	dst->w = a->w*b->w - a->x*b->x - a->y*b->y - a->z*b->z;
	dst->x = a->w*b->x + a->x*b->w + a->y*b->z - a->z*b->y;
	dst->y = a->w*b->y - a->x*b->z + a->y*b->w + a->z*b->x;
	dst->z = a->w*b->z + a->x*b->y - a->y*b->x + a->z*b->w;
}

/*
 * Create a rotation quaternion.
 */
void Quat_Rotation(quat_t *dst, const vec3_t *src) {
	quat_t qx, qy, qz, qt;

	Quat_RotationX(&qx, src->x);
	Quat_RotationY(&qy, src->y);
	Quat_RotationZ(&qz, src->z);

	Quat_Multiply(&qt, &qz, &qx);
	Quat_Multiply(dst, &qt, &qy);

	Quat_Normalize(dst, dst);
}

/*
 * Quaternion dot product, length, normalize.
 */
float Quat_Dot(const quat_t *a, const quat_t *b) {
	return a->w*b->w + a->x*b->x + a->y*b->y + a->z*b->z;
}
float Quat_LengthSq(const quat_t *src) {
	return Quat_Dot(src, src);
}
float Quat_Length(const quat_t *src) {
	return sqrtf(Quat_LengthSq(src));
}
float Quat_LengthFast(const quat_t *src) {
	return SqrtFast(Quat_LengthSq(src));
}
float Quat_InvLengthFast(const quat_t *src) {
	return InvSqrtFast(Quat_LengthSq(src));
}
void Quat_Normalize(quat_t *dst, const quat_t *src) {
	float r;

	r = 1.0f/Quat_Length(src);

	dst->w = src->w*r;
	dst->x = src->x*r;
	dst->y = src->y*r;
	dst->z = src->z*r;
}
void Quat_NormalizeFast(quat_t *dst, const quat_t *src) {
	float r;

	r = Quat_InvLengthFast(src);

	dst->w = src->w*r;
	dst->x = src->x*r;
	dst->y = src->y*r;
	dst->z = src->z*r;
}

/*
 * Quaternion conjugate and inverse.
 */
void Quat_Conjugate(quat_t *dst, const quat_t *src) {
	dst->w =  src->w;
	dst->x = -src->x;
	dst->y = -src->y;
	dst->z = -src->z;
}
void Quat_Inverse(quat_t *dst, const quat_t *src) {
	Quat_Conjugate(dst, src);
	Quat_Normalize(dst, dst);
}
void Quat_InverseFast(quat_t *dst, const quat_t *src) {
	Quat_Conjugate(dst, src);
	Quat_NormalizeFast(dst, dst);
}

/*
 * Quaternion interpolations.
 */
void Quat_Slerp(quat_t *dst, const quat_t *a, const quat_t *b, float w) {
	float d, e, ew;

	w = 1 - w;

	d = Quat_Dot(a, b);
	/*e = (d < 0) ? -1 : 1;*/
	e = (float)(1 | (((int)d)>>31)); /* see: http://codepad.org/lpiYReQF */
	ew = e*w;

	dst->w = w*a->w + ew*b->w;
	dst->x = w*a->x + ew*b->x;
	dst->y = w*a->y + ew*b->y;
	dst->z = w*a->z + ew*b->z;
}
void Quat_Lerp(quat_t *dst, const quat_t *a, const quat_t *b, float w) {
	dst->w = a->w + (b->w-a->w)*w;
	dst->x = a->x + (b->x-a->x)*w;
	dst->y = a->y + (b->y-a->y)*w;
	dst->z = a->z + (b->z-a->z)*w;
}
