#ifndef EE_SCHEDULER_H
#define EE_SCHEDULER_H

#include <Ember/BaseTypes.h>

/* -------------------------------------------------------------------------- */

typedef void(*ee_JobFunc_t)(void *);

typedef struct Scheduler_s *ee_Scheduler_t;
typedef struct JobQueue_s *ee_JobQueue_t;
/*typedef ee_UInt_t ee_JobQueue_t;*/
typedef ee_UInt_t ee_Job_t;

/* -------------------------------------------------------------------------- */

/* Initialize the scheduling system */
EE_PUBLIC bool eeStartScheduler();
/* Deinitialize the scheduling system */
EE_PUBLIC void eeStopScheduler();

/* Retrieve the real number of logical cores in the system */
EE_PUBLIC unsigned int eeGetRealCPUCount();
/* Retrieve the number of CPUs in the system */
EE_PUBLIC unsigned int eeGetCPUCount();
/* Set the "virtual" CPU count */
EE_PUBLIC void eeSetCPUCount(unsigned int numCPUs);

/* Allocate a scheduler */
EE_PUBLIC ee_Scheduler_t eeNewScheduler();
/* Delete an allocated scheduler */
EE_PUBLIC ee_Scheduler_t eeDeleteScheduler(ee_Scheduler_t sched);
/* Grab a scheduler for a specific CPU */
EE_PUBLIC ee_Scheduler_t eeGetCPUScheduler(unsigned int cpu);

/* Create a job queue */
EE_PUBLIC ee_JobQueue_t eeNewJobQueue();
/* Destroy a job queue (does not wait for any jobs to complete) */
EE_PUBLIC ee_JobQueue_t eeDeleteJobQueue(ee_JobQueue_t q);
/* Set a job queue to a specific scheduler */
EE_PUBLIC void eeSetJobQueueScheduler(ee_JobQueue_t p, ee_Scheduler_t sched);
/* Retrieve the scheduler of a job queue */
EE_PUBLIC ee_Scheduler_t eeGetJobQueueScheduler(ee_JobQueue_t p);

/* Add a job to a queue (may be scheduled immediately) */
EE_PUBLIC void eeEnqueueJob(ee_JobQueue_t p, ee_JobFunc_t fn, void *arg);
/* Add a new phase to a queue */
EE_PUBLIC ee_UInt_t eeEnqueuePhase(ee_JobQueue_t p);

/* Wait for a phase to be reached */
EE_PUBLIC void eeWaitForPhase(ee_JobQueue_t p, ee_UInt_t phase);
/* Wait for an entire queue */
EE_PUBLIC void eeWaitForQueue(ee_JobQueue_t p);
/* Wait for all job queues to complete */
EE_PUBLIC void eeWaitForAllQueues();

#endif
