/*
 *	Mk
 *	Written by NotKyon (Aaron J. Miller) <nocannedmeat@gmail.com>, 2012-2016.
 *	Contributions from James P. O'Hagan <johagan12@gmail.com>, 2012-2016.
 *
 *	This tool is used to simplify the build process of simple interdependent
 *	programs/libraries that need to be (or would benefit from being) distributed
 *	without requiring make/scons/<other builder program>.
 *
 *	The entire program is written in one source file to make it easier to build.
 *	To build this program in debug mode, run the following in your terminal:
 *	$ gcc -g -D_DEBUG -W -Wall -pedantic -std=c99 -o mk mk.c
 *
 *	To build this program in release mode, run the following:
 *	$ gcc -O3 -fomit-frame-pointer -W -Wall -pedantic -std=c99 -o mk mk.c
 *
 *	Also, I apologize for the quality (or rather, the lack thereof) of the code.
 *	I intend to improve it over time, make some function names more consistent,
 *	and provide better internal source code documentation.
 *
 *	I did not attempt to compile with VC++. I do not believe "%zu" works in VC,
 *	so you may have to modify as necessary. Under GCC, I get absolutely no
 *	warnings for ISO C99; no extensions used.
 *
 *	LICENSE
 *	=======
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define MK_VERSION_MAJOR 0
#define MK_VERSION_MINOR 7
#define MK_VERSION_PATCH 1

#define MK_VERSION ( MK_VERSION_MAJOR*10000 + MK_VERSION_MINOR*100 + MK_VERSION_PATCH )

#define PPTOKSTR__IMPL(X_) #X_
#define PPTOKSTR(X_) PPTOKSTR__IMPL(X_)

#define MK_VERSION_STR PPTOKSTR(MK_VERSION_MAJOR) "." PPTOKSTR(MK_VERSION_MINOR) "." PPTOKSTR(MK_VERSION_PATCH)

#ifndef MK_WINDOWS_ENABLED
# ifdef _WIN32
#  define MK_WINDOWS_ENABLED 1
# else
#  define MK_WINDOWS_ENABLED 0
# endif
#endif

#ifndef MK_WINDOWS_COLORS_ENABLED
# if MK_WINDOWS_ENABLED
#  define MK_WINDOWS_COLORS_ENABLED 1
# else
#  define MK_WINDOWS_COLORS_ENABLED 0
# endif
#endif
#if !MK_WINDOWS_ENABLED
# undef MK_WINDOWS_COLORS_ENABLED
# define MK_WINDOWS_COLORS_ENABLED 0
#endif

#ifdef _MSC_VER
# define MK_VC_VER _MSC_VER
#else
# define MK_VC_VER 0
#endif

#ifndef MK_HAS_DIRENT
# if !MK_VC_VER
#  define MK_HAS_DIRENT 1
# else
#  define MK_HAS_DIRENT 0
# endif
#endif
#ifndef MK_HAS_UNISTD
# if !MK_VC_VER
#  define MK_HAS_UNISTD 1
# else
#  define MK_HAS_UNISTD 0
# endif
#endif

#ifndef MK_HAS_EXECINFO
# if !MK_WINDOWS_ENABLED
#  define MK_HAS_EXECINFO 1
# else
#  define MK_HAS_EXECINFO 0
# endif
#endif

#ifndef MK_SECLIB
# if defined( __STDC_WANT_SECURE_LIB__ ) && defined( _MSC_VER )
#  define MK_SECLIB 1
# else
#  define MK_SECLIB 0
# endif
#endif

#if MK_WINDOWS_ENABLED
# if MK_VC_VER
#  define _CRT_SECURE_NO_WARNINGS 1
# endif
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif
#include <time.h>
#include <errno.h>
#include <stdio.h>
#if MK_HAS_DIRENT
# include <dirent.h>
#else
# include "dirent.h" /* user-provided replacement */
#endif
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#if MK_HAS_UNISTD
# include <unistd.h>
#else
# include <io.h>
# include <direct.h>
#endif
#include <sys/stat.h>
#include <sys/types.h>
#if MK_HAS_EXECINFO
# include <execinfo.h>
#endif

#ifndef MK_DEBUG_ENABLED
# if defined(_DEBUG)||defined(DEBUG)||defined(__debug__)||defined(__DEBUG__)
#  define MK_DEBUG_ENABLED 1
# else
#  define MK_DEBUG_ENABLED 0
# endif
#endif

#ifndef S_IFREG
# define S_IFREG 0100000
#endif
#ifndef S_IFDIR
# define S_IFDIR 0040000
#endif

#ifndef PATH_MAX
# if defined( _MAX_PATH )
#  define PATH_MAX _MAX_PATH
# elif defined( MAX_PATH )
#  define PATH_MAX MAX_PATH
# else
#  define PATH_MAX 512
# endif
#endif

#ifndef bitfield_t_defined
# define bitfield_t_defined 1
typedef size_t bitfield_t;
#endif

#ifndef NORETURN
# if MK_VC_VER
#  define NORETURN _declspec(noreturn)
# else
#  define NORETURN __attribute__((noreturn))
# endif
#endif

#ifndef CURRENT_FUNCTION
# if MK_VC_VER
#  define CURRENT_FUNCTION __FUNCTION__
# else
#  define CURRENT_FUNCTION __func__
# endif
#endif

#define PPTOKCAT__IMPL(X,Y) X##Y
#define PPTOKCAT(X,Y) PPTOKCAT__IMPL(X,Y)

/*
================
PROCESS_NEWLINE_CONCAT

Define to 1 to enable newline concatenation through the backslash character.
Example:

	INPUT
	-----
	Test\
	Text

	OUTPUT
	------
	TestText

================
*/
#ifndef PROCESS_NEWLINE_CONCAT
# define PROCESS_NEWLINE_CONCAT 1
#endif

/*

	TODO: bin, bin64
	TODO: "deploy" build to build both 32-bit and 64-bit (bin and bin64)

*/



#define MK_HOST_OS_MSWIN  0
#define MK_HOST_OS_CYGWIN 0
#define MK_HOST_OS_MACOSX 0
#define MK_HOST_OS_LINUX  0
#define MK_HOST_OS_UNIX   0

#if MK_WINDOWS_ENABLED
# if defined( __CYGWIN__ )
#  undef  MK_HOST_OS_CYGWIN
#  define MK_HOST_OS_CYGWIN 1
# else
#  undef  MK_HOST_OS_MSWIN
#  define MK_HOST_OS_MSWIN 1
# endif
#elif defined( __linux__ ) || defined( __linux )
# undef  MK_HOST_OS_LINUX
# define MK_HOST_OS_LINUX 1
#elif defined( __APPLE__ ) && defined( __MACH__ )
# undef  MK_HOST_OS_MACOSX
# define MK_HOST_OS_MACOSX 1
#elif defined( __unix__ ) || defined( __unix )
# undef  MK_HOST_OS_UNIX
# define MK_HOST_OS_UNIX 1
#else
# error "Unrecognized host OS."
#endif

#define MK_HOST_CPU_X86     0
#define MK_HOST_CPU_X86_64  0
#define MK_HOST_CPU_ARM     0
#define MK_HOST_CPU_AARCH64 0
#define MK_HOST_CPU_PPC     0
#define MK_HOST_CPU_MIPS    0

#if defined( __amd64__ ) || defined( __x86_64__ ) || defined( _M_X64 )
# undef  MK_HOST_CPU_X86_64
# define MK_HOST_CPU_X86_64 1
#elif defined( __i386__ ) || defined( _M_IX86 )
# undef  MK_HOST_CPU_X86
# define MK_HOST_CPU_X86 1
#elif defined( __aarch64__ )
# undef  MK_HOST_CPU_AARCH64
# define MK_HOST_CPU_AARCH64 1
#elif defined( __arm__ ) || defined( _ARM )
# undef  MK_HOST_CPU_ARM
# define MK_HOST_CPU_ARM 1
#elif defined( __powerpc__ ) || defined( __POWERPC__ ) || defined( __ppc__ )
# undef  MK_HOST_CPU_PPC
# define MK_HOST_CPU_PPC 1
#elif defined( _M_PPC ) || defined( _ARCH_PPC )
# undef  MK_HOST_CPU_PPC
# define MK_HOST_CPU_PPC 1
#elif defined( __mips__ ) || defined( __MIPS__ )
# undef  MK_HOST_CPU_MIPS
# define MK_HOST_CPU_MIPS 1
#else
# error "Unrecognized host CPU."
#endif


/*
===============================================================================

	Configuration Settings

===============================================================================
*/
#ifndef MK_DEFAULT_OBJDIR_BASE
# define MK_DEFAULT_OBJDIR_BASE ".mk-obj"
#endif

#ifndef MK_DEFAULT_DEBUG_SUFFIX
# define MK_DEFAULT_DEBUG_SUFFIX "-dbg"
#endif

#ifndef MK_DEFAULT_COLOR_MODE
# if MK_WINDOWS_COLORS_ENABLED
#  define MK_DEFAULT_COLOR_MODE Windows
# else
#  define MK_DEFAULT_COLOR_MODE ANSI
# endif
#endif
#define MK__DEFAULT_COLOR_MODE_IMPL PPTOKCAT(kMkColorMode_, MK_DEFAULT_COLOR_MODE)

#ifndef MK_DEFAULT_RELEASE_CONFIG_NAME
# define MK_DEFAULT_RELEASE_CONFIG_NAME "release"
#endif
#ifndef MK_DEFAULT_DEBUG_CONFIG_NAME
# define MK_DEFAULT_DEBUG_CONFIG_NAME "debug"
#endif

/* Windows */
#ifndef MK_PLATFORM_OS_NAME_MSWIN
# define MK_PLATFORM_OS_NAME_MSWIN "mswin"
#endif
/* Universal Windows Program */
#ifndef MK_PLATFORM_OS_NAME_UWP
# define MK_PLATFORM_OS_NAME_UWP "uwp"
#endif
/* Windows (via Cygwin) */
#ifndef MK_PLATFORM_OS_NAME_CYGWIN
# define MK_PLATFORM_OS_NAME_CYGWIN "cygwin"
#endif
/* Mac OS X */
#ifndef MK_PLATFORM_OS_NAME_MACOSX
# define MK_PLATFORM_OS_NAME_MACOSX "apple"
#endif
/* (GNU/)Linux */
#ifndef MK_PLATFORM_OS_NAME_LINUX
# define MK_PLATFORM_OS_NAME_LINUX "linux"
#endif
/* Some UNIX variant (e.g., FreeBSD, Solaris) */
#ifndef MK_PLATFORM_OS_NAME_UNIX
# define MK_PLATFORM_OS_NAME_UNIX "unix"
#endif

/* x86 (32-bit) */
#ifndef MK_PLATFORM_CPU_NAME_X86
# define MK_PLATFORM_CPU_NAME_X86 "x86"
#endif
/* x86-64 (64-bit) */
#ifndef MK_PLATFORM_CPU_NAME_X64
# define MK_PLATFORM_CPU_NAME_X64 "x64"
#endif
/* ARM (32-bit) */
#ifndef MK_PLATFORM_CPU_NAME_ARM
# define MK_PLATFORM_CPU_NAME_ARM "arm"
#endif
/* ARM / AArch64 (64-bit) */
#ifndef MK_PLATFORM_CPU_NAME_AARCH64
# define MK_PLATFORM_CPU_NAME_AARCH64 "aarch64"
#endif
/* PowerPC (generic) */
#ifndef MK_PLATFORM_CPU_NAME_PPC
# define MK_PLATFORM_CPU_NAME_PPC "ppc"
#endif
/* MIPS (generic) */
#ifndef MK_PLATFORM_CPU_NAME_MIPS
# define MK_PLATFORM_CPU_NAME_MIPS "mips"
#endif
/* WebAssembly */
#ifndef MK_PLATFORM_CPU_NAME_WASM
# define MK_PLATFORM_CPU_NAME_WASM "wasm"
#endif

#ifndef MK_PLATFORM_OS_NAME
# if MK_HOST_OS_MSWIN
#  define MK_PLATFORM_OS_NAME MK_PLATFORM_OS_NAME_MSWIN
# elif MK_HOST_OS_CYGWIN
#  define MK_PLATFORM_OS_NAME MK_PLATFORM_OS_NAME_CYGWIN
# elif MK_HOST_OS_LINUX
#  define MK_PLATFORM_OS_NAME MK_PLATFORM_OS_NAME_LINUX
# elif MK_HOST_OS_MACOSX
#  define MK_PLATFORM_OS_NAME MK_PLATFORM_OS_NAME_MACOSX
# elif MK_HOST_OS_UNIX
#  define MK_PLATFORM_OS_NAME MK_PLATFORM_OS_NAME_UNIX
# else
#  error "MK_PLATFORM_OS_NAME: Unhandled OS."
# endif
#endif

#ifndef MK_PLATFORM_CPU_NAME
# if MK_HOST_CPU_X86
#  define MK_PLATFORM_CPU_NAME MK_PLATFORM_CPU_NAME_X86
# elif MK_HOST_CPU_X86_64
#  define MK_PLATFORM_CPU_NAME MK_PLATFORM_CPU_NAME_X64
# elif MK_HOST_CPU_ARM
#  define MK_PLATFORM_CPU_NAME MK_PLATFORM_CPU_NAME_ARM
# elif MK_HOST_CPU_AARCH64
#  define MK_PLATFORM_CPU_NAME MK_PLATFORM_CPU_NAME_AARCH64
# elif MK_HOST_CPU_PPC
#  define MK_PLATFORM_CPU_NAME MK_PLATFORM_CPU_NAME_PPC
# elif MK_HOST_CPU_MIPS
#  define MK_PLATFORM_CPU_NAME MK_PLATFORM_CPU_NAME_MIPS
# else
#  error "MK_PLATFORM_CPU_NAME: Unhandled CPU."
# endif
#endif

#ifndef MK_PLATFORM_DIR
# define MK_PLATFORM_DIR MK_PLATFORM_OS_NAME "-" MK_PLATFORM_CPU_NAME
#endif

/* -------------------------------------------------------------------------- */
#ifndef MK_DEFAULT_DEBUGLOG_FILENAME
# define MK_DEFAULT_DEBUGLOG_FILENAME "mk-debug.log"
#endif

const char *Opt_GetObjdirBase(void);
const char *Opt_GetDebugSuffix(void);
const char *Opt_GetDebugLogPath(void);

typedef enum
{
	kMkColorMode_None,
	kMkColorMode_ANSI,
#if MK_WINDOWS_COLORS_ENABLED
	kMkColorMode_Windows,
#endif

	kMkNumColorModes
} MkColorMode_t;

MkColorMode_t Opt_GetColorMode(void);

const char *Opt_GetConfigName(void);
const char *Opt_GetBuildGenPath(void);
const char *Opt_GetBuildGenIncDir(void);

/* -------------------------------------------------------------------------- */
void _Dbg_CloseLog();
void Dbg_Out(const char *str);
void Dbg_OutfV(const char *format, va_list args);
void Dbg_Outf(const char *format, ...);
void Dbg_Enter(const char *format, ...);
void Dbg_Leave(void);
void Dbg_BackTrace();

/* -------------------------------------------------------------------------- */
typedef enum {
	SIO_OUT,
	SIO_ERR,

	__SIO_NUM__
} sio_t;

#define S_COLOR_BLACK			"^0"
#define S_COLOR_BLUE			"^1"
#define S_COLOR_GREEN			"^2"
#define S_COLOR_CYAN			"^3"
#define S_COLOR_RED				"^4"
#define S_COLOR_MAGENTA			"^5"
#define S_COLOR_BROWN			"^6"
#define S_COLOR_LIGHT_GREY		"^7"
#define S_COLOR_DARK_GREY		"^8"
#define S_COLOR_LIGHT_BLUE		"^9"
#define S_COLOR_LIGHT_GREEN		"^A"
#define S_COLOR_LIGHT_CYAN		"^B"
#define S_COLOR_LIGHT_RED		"^C"
#define S_COLOR_LIGHT_MAGENTA	"^D"
#define S_COLOR_LIGHT_BROWN		"^E"
#define S_COLOR_WHITE			"^F"

#define S_COLOR_PURPLE			"^5"
#define S_COLOR_YELLOW			"^E"
#define S_COLOR_VIOLET			"^D"

#define S_COLOR_RESTORE			"^&"

#define COLOR_BLACK				0x0
#define COLOR_BLUE				0x1
#define COLOR_GREEN				0x2
#define COLOR_CYAN				0x3
#define COLOR_RED				0x4
#define COLOR_MAGENTA			0x5
#define COLOR_BROWN				0x6
#define COLOR_LIGHT_GREY		0x7
#define COLOR_DARK_GREY			0x8
#define COLOR_LIGHT_BLUE		0x9
#define COLOR_LIGHT_GREEN		0xA
#define COLOR_LIGHT_CYAN		0xB
#define COLOR_LIGHT_RED			0xC
#define COLOR_LIGHT_MAGENTA		0xD
#define COLOR_LIGHT_BROWN		0xE
#define COLOR_WHITE				0xF

#define COLOR_PURPLE			0x5
#define COLOR_YELLOW			0xE
#define COLOR_VIOLET			0xD

void Sys_InitColoredOutput();
unsigned char Sys_GetCurrColor(sio_t sio);
void Sys_SetCurrColor(sio_t sio, unsigned char color);
void Sys_UncoloredPuts(sio_t sio, const char *text, size_t len);
int Sys_CharToColorCode(char c);
void Sys_Puts(sio_t sio, const char *text);
void Sys_Printf(sio_t sio, const char *format, ...);
void Sys_PrintStr(sio_t sio, unsigned char color, const char *str);
void Sys_PrintUint(sio_t sio, unsigned char color, unsigned int val);
void Sys_PrintInt(sio_t sio, unsigned char color, int val);

/* -------------------------------------------------------------------------- */
void Log_ErrorMsg(const char *message);
NORETURN void Log_FatalError(const char *message);
void Log_Error(const char *file, unsigned int line, const char *func,
	const char *message);
void Log_ErrorAssert(const char *file, unsigned int line, const char *func,
	const char *message);

/* macros for ASSERT errors */
#if MK_DEBUG_ENABLED
# undef ASSERT
# undef ASSERT_MSG
# define ASSERT_MSG(x,m) if(!(x))Log_ErrorAssert(__FILE__,__LINE__,CURRENT_FUNCTION,m)
# define ASSERT( x) ASSERT_MSG((x),"ASSERT(" #x ") failed!" )
#else
# undef ASSERT
# undef ASSERT_MSG
# define ASSERT( x ) /*nothing*/
# define ASSERT_MSG(x,m) /*nothing*/
#endif

/* -------------------------------------------------------------------------- */
#ifndef MEM_LOCTRACE_ENABLED
# if MK_DEBUG_ENABLED
#  define MEM_LOCTRACE_ENABLED 1
# else
#  define MEM_LOCTRACE_ENABLED 0
# endif
#endif

typedef void( *Mem_Fini_fn_t )( void * );

enum
{
	/* do not zero out the memory (it WILL be initialized immediately) */
	kMemF_Uninitialized = 1<<0
};

#define Mem_MaybeAllocEx(cBytes_,uFlags_)	(Mem__MaybeAlloc((cBytes_),(uFlags_),__FILE__,__LINE__,CURRENT_FUNCTION))
#define Mem_MaybeAlloc(cBytes_)				(Mem__MaybeAlloc((cBytes_),0,__FILE__,__LINE__,CURRENT_FUNCTION))
#define Mem_AllocEx(cBytes_,uFlags_)		(Mem__Alloc((cBytes_),(uFlags_),__FILE__,__LINE__,CURRENT_FUNCTION))
#define Mem_Alloc(cBytes_)					(Mem__Alloc((cBytes_),0,__FILE__,__LINE__,CURRENT_FUNCTION))
#define Mem_Dealloc(pBlock_)				(Mem__Dealloc((void*)(pBlock_),__FILE__,__LINE__,CURRENT_FUNCTION))
#define Mem_AddRef(pBlock_)					(Mem__AddRef((void*)(pBlock_),__FILE__,__LINE__,CURRENT_FUNCTION))
#define Mem_Attach(pBlock_,pSuperBlock_)	(Mem__Attach((void*)(pBlock_),(void*)(pSuperBlock_),__FILE__,__LINE__,CURRENT_FUNCTION))
#define Mem_Detach(pBlock_)					(Mem__Detach((void*)(pBlock_),__FILE__,__LINE__,CURRENT_FUNCTION))
#define Mem_SetFini(pBlock_,pfnFini_)		(Mem__SetFini((void*)(pBlock_),(Mem_Fini_fn_t)(pfnFini_)))
#define Mem_Size(pBlock_)					(Mem__Size((const void*)(pBlock_)))

void *Mem__MaybeAlloc( size_t cBytes, bitfield_t uFlags, const char *pszFile, unsigned int uLine, const char *pszFunction );
void *Mem__Alloc( size_t cBytes, bitfield_t uFlags, const char *pszFile, unsigned int uLine, const char *pszFunction );
void *Mem__Dealloc( void *pBlock, const char *pszFile, unsigned int uLine, const char *pszFunction );
void *Mem__AddRef( void *pBlock, const char *pszFile, unsigned int uLine, const char *pszFunction );
void *Mem__Attach( void *pBlock, void *pSuperBlock, const char *pszFile, unsigned int uLine, const char *pszFunction );
void *Mem__Detach( void *pBlock, const char *pszFile, unsigned int uLine, const char *pszFunction );
void *Mem__SetFini( void *pBlock, Mem_Fini_fn_t pfnFini );
size_t Mem__Size( const void *pBlock );

/* -------------------------------------------------------------------------- */
#ifndef VA_MINIMUM_SIZE
# define VA_MINIMUM_SIZE 1024
#endif

#define Com_Memory(p_,n_) Com__Memory((void*)(p_),(size_t)(n_),__FILE__,__LINE__,CURRENT_FUNCTION)
#define Com_Strdup(cstr_) Com__Strdup((cstr_),__FILE__,__LINE__,CURRENT_FUNCTION)
void *Com__Memory( void *p, size_t n, const char *pszFile, unsigned int uLine, const char *pszFunction );
const char *Com_Va(const char *format, ...);
size_t Com_Strlen(const char *src);
size_t Com_Strcat(char *buf, size_t bufn, const char *src);
size_t Com_Strncat(char *buf, size_t bufn, const char *src, size_t srcn);
void Com_Strcpy(char *dst, size_t dstn, const char *src);
void Com_Strncpy(char *buf, size_t bufn, const char *src, size_t srcn);
char *Com__Strdup(const char *cstr,const char *file,unsigned int line,const char *func);

char *Com_Dup(char *dst, const char *src);
char *Com_DupN(char *dst, const char *src, size_t numchars);
char *Com_Append(char *dst, const char *src);
const char *Com_ExtractDir(char *buf, size_t n, const char *filename);
void Com_SubstExt(char *dst, size_t dstn, const char *src, const char *ext);
int Com_Shell(const char *format, ...);
int Com_MatchPath(const char *rpath, const char *apath);
int Com_GetIntDate();
const char *Com_FindArgEnd(const char *arg);
int Com_MatchArg(const char *a, const char *b);
const char *Com_SkipArg(const char *arg);
void Com_StripArgs(char *dst, size_t n, const char *src);
int Com_MatchPathChar(char a, char b);
int Com_RelPath(char *dst, size_t dstn, const char *curpath,
	const char *abspath);
int Com_RelPathCWD(char *dst, size_t dstn, const char *abspath);

/* -------------------------------------------------------------------------- */
typedef struct strList_s *strList_t;

strList_t SL_New();
size_t SL_Capacity(strList_t arr);
size_t SL_Size(strList_t arr);
char **SL_Data(strList_t arr);
char *SL_Get(strList_t arr, size_t i);
void SL_Set(strList_t arr, size_t i, const char *cstr);
void SL_Clear(strList_t arr);
void SL_Delete(strList_t arr);
void SL_DeleteAll();
void SL_Resize(strList_t arr, size_t n);
void SL_PushBack(strList_t arr, const char *cstr);
void SL_PopBack(strList_t arr);
void SL_Print(strList_t arr);
void SL_DbgPrint(strList_t arr);
static int _SL_Cmp(const void *a, const void *b);
void SL_Sort(strList_t arr);
void SL_OrderedSort(strList_t arr, size_t *const buffer, size_t maxBuffer);
void SL_IndexedSort(strList_t arr, const size_t *const buffer,
	size_t bufferLen);
void SL_PrintOrderedBuffer(const size_t *const buffer, size_t bufferLen);

void SL_Unique(strList_t arr);

/* -------------------------------------------------------------------------- */
typedef struct buffer_s *buffer_t;

buffer_t Buf_LoadMemoryN(const char *filename, const char *source, size_t len);
buffer_t Buf_LoadMemory(const char *filename, const char *source);
buffer_t Buf_LoadFile(const char *filename);
buffer_t Buf_Unload(buffer_t text);

int Buf_SetFileName(buffer_t text, const char *filename);
int Buf_SetFunction(buffer_t text, const char *func);

const char *Buf_GetFileName(const buffer_t text);
const char *Buf_GetFunction(const buffer_t text);
size_t Buf_GetLine(const buffer_t text);

size_t Buf_GetLength(const buffer_t text);

void Buf_Seek(buffer_t text, size_t pos);
size_t Buf_Tell(const buffer_t text);
char *Buf_Ptr(buffer_t text);

char Buf_Read(buffer_t text);
char Buf_Peek(buffer_t text);
char Buf_Look(buffer_t text, size_t offset);
int Buf_Check(buffer_t text, char ch);

void Buf_Skip(buffer_t text, size_t offset);
void Buf_SkipWhite(buffer_t text);
void Buf_SkipLine(buffer_t text);
int Buf_SkipComment(buffer_t text, const char *pszCommentDelim);

int Buf_ReadLine(buffer_t text, char *dst, size_t dstn);

void Buf_ErrorV(buffer_t text, const char *format, va_list args);
void Buf_Error(buffer_t text, const char *format, ...);

/* -------------------------------------------------------------------------- */
typedef struct stat fs_stat_t;

void FS_Init();
void FS_Fini();
char *FS_GetCWD(char *cwd, size_t n);
int FS_Enter(const char *path);
void FS_Leave();
int FS_IsFile(const char *path);
int FS_IsDir(const char *path);
void FS_MkDirs(const char *dirs);
char *FS_RealPath(const char *filename, char *resolvedname, size_t maxn);
DIR *FS_OpenDir(const char *path);
struct dirent *FS_ReadDir(DIR *d);
void FS_Delete(const char *path);

/* -------------------------------------------------------------------------- */
typedef struct gitinfo_s *gitinfo_t;

char *Git_FindPath( void );
char *Git_GetBranchPath( const char *pszGitDir );
char *Git_GetCommit( const char *pszBranchFile );
char *Git_GetCommitDate( const char *pszBranchFile, const fs_stat_t *pStat );
gitinfo_t Git_LoadInfo( void );
int Git_WriteHeader( const char *pszHFilename, gitinfo_t pGitInfo );

int Git_GenerateInfo( void );

/* -------------------------------------------------------------------------- */
#ifndef MK_DEBUG_DEPENDENCY_TRACKER_ENABLED
# define MK_DEBUG_DEPENDENCY_TRACKER_ENABLED 0
#endif

typedef struct dep_s *dep_t;

dep_t Dep_New(const char *name);
void Dep_Delete(dep_t dep);
void Dep_DeleteAll();
const char *Dep_GetFile(dep_t dep);
void Dep_Push(dep_t dep, const char *name);
size_t Dep_Size(dep_t dep);
const char *Dep_Get(dep_t dep, size_t i);
dep_t Dep_Find(const char *name);

/* -------------------------------------------------------------------------- */
char DepF_Read(buffer_t buf, char *cur, char *look);
int DepF_Lex(buffer_t buf, char *dst, size_t dstn, char *cur, char *look);
int Dep_Load(const char *filename);

/* -------------------------------------------------------------------------- */
typedef struct lib_s *lib_t;

lib_t Lib_New();
void Lib_Delete(lib_t lib);
void Lib_DeleteAll();
void Lib_SetName(lib_t lib, const char *name);
void Lib_SetFlags(lib_t lib, int sys, const char *flags);
const char *Lib_Name(lib_t lib);
const char *Lib_Flags(lib_t lib, int sys);
lib_t Lib_Prev(lib_t lib);
lib_t Lib_Next(lib_t lib);
lib_t Lib_Head();
lib_t Lib_Tail();
lib_t Lib_Find(const char *name);
lib_t Lib_Lookup(const char *name);
void Lib_ClearAllProcessed();
void Lib_ClearProcessed(lib_t lib);
void Lib_SetProcessed(lib_t lib);
int Lib_IsProcessed(lib_t lib);

/* -------------------------------------------------------------------------- */
#ifndef MK_DEBUG_AUTOLIBCONF_ENABLED
# define MK_DEBUG_AUTOLIBCONF_ENABLED 1
#endif

typedef struct autolink_s *autolink_t;

autolink_t AL_New();
void AL_Delete(autolink_t al);
void AL_DeleteAll();
void AL_SetHeader(autolink_t al, int sys, const char *header);
void AL_SetLib(autolink_t al, const char *libname);
const char *AL_GetHeader(autolink_t al, int sys);
const char *AL_GetLib(autolink_t al);
autolink_t AL_Find(int sys, const char *header);
const char *AL_Autolink(int sys, const char *header);
void AL_ManagePackage_r(const char *libname, int sys, const char *incdir);
int AL_LoadConfig( const char *pszFilename );

/* -------------------------------------------------------------------------- */
#ifndef MK_DEBUG_LIBDEPS_ENABLED
# define MK_DEBUG_LIBDEPS_ENABLED 1
#endif

typedef struct project_s *project_t;

enum {
	/* static library (lib<blah>.a) */
	kProjType_Library,
	/* dynamic library (<blah>.dll) */
	kProjType_DynamicLibrary,
	/* (console) executable (<blah>.exe) */
	kProjType_Executable,
	/* (gui) executable (<blah>.exe) -- gets console in debug mode */
	kProjType_Application
};

enum {
	kProjSys_MSWin,
	kProjSys_UWP,
	kProjSys_Cygwin,
	kProjSys_Linux,
	kProjSys_MacOSX,
	kProjSys_Unix,

	kNumProjSys
};

enum {
	kProjArch_X86,
	kProjArch_X86_64,
	kProjArch_ARM,
	kProjArch_AArch64,
	kProjArch_PowerPC,
	kProjArch_MIPS,
	kProjArch_WebAssembly,

	kNumProjArch
};

project_t Prj_New(project_t prnt);
void Prj_Delete(project_t proj);
void Prj_DeleteAll();
project_t Prj_RootHead();
project_t Prj_RootTail();
project_t Prj_Parent(project_t proj);
project_t Prj_Head(project_t proj);
project_t Prj_Tail(project_t proj);
project_t Prj_Prev(project_t proj);
project_t Prj_Next(project_t proj);
void Prj_SetName(project_t proj, const char *name);
void Prj_SetOutPath(project_t proj, const char *path);
void Prj_SetPath(project_t proj, const char *path);
void Prj_SetType(project_t proj, int type);
const char *Prj_Name(project_t proj);
const char *Prj_Path(project_t proj);
const char *Prj_OutPath(project_t proj);
int Prj_Type(project_t proj);
void Prj_AddLib(project_t proj, const char *libname);
size_t Prj_LibCount(project_t proj);
const char *Prj_Lib(project_t proj, size_t i);
void Prj_AddLinkFlag(project_t proj, const char *flags);
const char *Prj_LinkFlags(project_t proj);
void Prj_AppendExtraLib(project_t proj, const char *extras);
const char *Prj_ExtraLibs(project_t proj);
void Prj_CompleteExtraLibs(project_t proj, char *extras, size_t n);
void Prj_AddSource(project_t proj, const char *src);
size_t Prj_SourceCount(project_t proj);
const char *Prj_Source(project_t proj, size_t i);
void Prj_AddTestSource(project_t proj, const char *src);
size_t Prj_TestSourceCount(project_t proj);
const char *Prj_TestSource(project_t proj, size_t i);
void Prj_AddSpecDir(project_t proj, const char *dir);
size_t Prj_SpecDirCount(project_t proj);
const char *Prj_SpecDir(project_t proj, size_t i);
int Prj_IsTarget(project_t proj);
void Prj_PrintAll(project_t proj, const char *margin);
void Prj_CalcLibFlags(project_t proj);
project_t Prj_Find_r(project_t from, const char *name);
project_t Prj_Find(const char *name);

/* -------------------------------------------------------------------------- */
int Prj_IsSpecDir(project_t proj, const char *name);
int Prj_IsIncDir(project_t proj, const char *name);
int Prj_IsLibDir(project_t proj, const char *name);
int Prj_IsTestDir(project_t proj, const char *name);
int Prj_IsDirOwner(const char *path);
void Prj_EnumSources(project_t proj, const char *srcdir);
void Prj_EnumTests(project_t proj, const char *srcdir);
int Prj_CalcName(project_t proj, const char *path, const char *file);
project_t Prj_Add(project_t prnt, const char *path, const char *file, int type);
void Prj_FindPackages(const char *pkgdir);
void Prj_FindDLLs(const char *dllsdir);
void Prj_FindTools(const char *tooldir);
void Prj_FindProjects(project_t prnt, const char *srcdir);
void Prj_FindRootDirs(strList_t srcdirs, strList_t incdirs, strList_t libdirs,
	strList_t pkgdirs, strList_t tooldirs, strList_t dllsdirs);
int Prj_MakeObjDirs(project_t proj);

/* -------------------------------------------------------------------------- */
#ifndef MK_DEBUG_FIND_SOURCE_LIBS_ENABLED
# define MK_DEBUG_FIND_SOURCE_LIBS_ENABLED 0
#endif

void Bld_InitUnitTestArrays();
int Bld_FindSourceLibs(strList_t dst, int sys, const char *obj,
	const char *dep);
int Bld_ShouldCompile(const char *obj);
int Bld_ShouldLink(const char *bin, int numbuilds);
const char *Bld_GetCFlags(project_t proj, const char *obj, const char *src);
void Bld_GetDeps_r(project_t proj, strList_t deparray);
int Bld_DoesLibDependOnLib(lib_t mainlib, lib_t deplib);
int _Bld_CmpLibs(const void *a, const void *b);
void Bld_SortDeps(strList_t deparray);
const char *Bld_GetProjDepLinkFlags(project_t proj);
void Bld_GetLibs(project_t proj, char *dst, size_t n);
const char *Bld_GetLFlags(project_t proj, const char *bin, strList_t objs);
void Bld_GetObjName(project_t proj, char *obj, size_t n, const char *src);
void Bld_GetBinName(project_t proj, char *bin, size_t n);
void Bld_UnitTest(project_t proj, const char *src);
void Bld_RunTests();
void Bld_SortProjects(struct project_s *proj);
void Bld_RelinkDeps(project_t proj);
int Mk_Project(project_t proj);
int Mk_AllProjects();

/* -------------------------------------------------------------------------- */
void Mk_PushSrcDir(const char *srcdir);
void Mk_PushIncDir(const char *incdir);
void Mk_PushLibDir(const char *libdir);
void Mk_PushPkgDir(const char *pkgdir);
void Mk_PushToolDir(const char *tooldir);
void Mk_PushDLLsDir(const char *dllsdir);
void FS_UnwindDirs();
void Mk_Init(int argc, char **argv);

/* -------------------------------------------------------------------------- */
strList_t g_targets = (strList_t)0;
strList_t g_srcdirs = (strList_t)0;
strList_t g_incdirs = (strList_t)0;
strList_t g_libdirs = (strList_t)0;
strList_t g_pkgdirs = (strList_t)0;
strList_t g_tooldirs = (strList_t)0;
strList_t g_dllsdirs = (strList_t)0;

enum {
	kFlag_ShowHelp_Bit = 0x01,
	kFlag_ShowVersion_Bit = 0x02,
	kFlag_Verbose_Bit = 0x04,
	kFlag_Release_Bit = 0x08,
	kFlag_Rebuild_Bit = 0x10,
	kFlag_NoCompile_Bit = 0x20,
	kFlag_NoLink_Bit = 0x40,
	kFlag_LightClean_Bit = 0x80,
	kFlag_PrintHierarchy_Bit = 0x100,
	kFlag_Pedantic_Bit = 0x200,
	kFlag_Test_Bit = 0x400,
	kFlag_FullClean_Bit = 0x800
};
bitfield_t g_flags = 0;
MkColorMode_t g_flags_color = MK__DEFAULT_COLOR_MODE_IMPL;

/*
 *	========================================================================
 *	DEBUGGING CODE
 *	========================================================================
 *	Debug helpers.
 */

#if MK_DEBUG_ENABLED
FILE *g_debugLog = (FILE *)0;
#endif
unsigned g_debugIndentLevel = 0;

void _Dbg_CloseLog() {
#if MK_DEBUG_ENABLED
	if( !g_debugLog ) {
		return;
	}

	fprintf(g_debugLog, "\n\n[[== DEBUG LOG CLOSED ==]]\n\n");

	fclose(g_debugLog);
	g_debugLog = (FILE *)0;
#endif
}

/* write to the debug log, no formatting */
void Dbg_Out(const char *str) {
#if MK_DEBUG_ENABLED
	static const char szTabs[] = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
	static const unsigned cTabs = sizeof( szTabs ) - 1;
	unsigned cIndents;
	
	if( !g_debugLog ) {
		time_t rawtime;
		struct tm *timeinfo;
		char szTimeBuf[ 128 ];

		g_debugLog = fopen(Opt_GetDebugLogPath(), "a+");
		if( !g_debugLog ) {
			return;
		}
		
		szTimeBuf[ 0 ] = '\0';

		time( &rawtime );
		if( ( timeinfo = localtime( &rawtime ) ) != ( struct tm * )0 ) {
			strftime( szTimeBuf, sizeof( szTimeBuf ), " (%Y-%m-%d %H:%M:%S)", timeinfo );
		}

		fprintf(g_debugLog, "\n\n[[== DEBUG LOG OPENED%s ==]]\n\n", szTimeBuf);
		fprintf(g_debugLog, "#### Mk version: " MK_VERSION_STR " ****\n\n" );

		atexit(_Dbg_CloseLog);
	}
	
	cIndents = g_debugIndentLevel;
	while( cIndents > 0 ) {
		unsigned cIndentsToWrite;
		
		cIndentsToWrite = cIndents < cTabs ? cIndents : cTabs;
		fwrite(&szTabs[0], (size_t)cIndentsToWrite, 1, g_debugLog);
		
		cIndents -= cIndentsToWrite;
	}

	fwrite(str, strlen(str), 1, g_debugLog);
	fflush(g_debugLog);
#else
	(void)str;
#endif
}

/* write to the debug log (va_args) */
void Dbg_OutfV(const char *format, va_list args) {
#if MK_DEBUG_ENABLED
	static char buf[65536];

# if MK_SECLIB
	vsprintf_s(buf, sizeof(buf), format, args);
# else
	vsnprintf(buf, sizeof(buf), format, args);
	buf[sizeof(buf) - 1] = '\0';
# endif

	Dbg_Out(buf);
#else
	(void)format;
	(void)args;
#endif
}
/* write to the debug log */
void Dbg_Outf(const char *format, ...) {
	va_list args;

	va_start(args, format);
	Dbg_OutfV(format, args);
	va_end(args);
}

/* enter a debug layer */
void Dbg_Enter(const char *format, ...) {
	va_list args;
	
	Dbg_Out("\n");
	va_start(args, format);
	Dbg_OutfV(format, args);
	Dbg_Out("\n");
	Dbg_Out("{\n");
	va_end(args);
	
	++g_debugIndentLevel;
}
/* leave a debug layer */
void Dbg_Leave(void) {
	ASSERT( g_debugIndentLevel > 0 );
	
	if( g_debugIndentLevel > 0 ) {
		--g_debugIndentLevel;
	}
	
	Dbg_Out("}\n\n");
}

/* backtrace support... */
void Dbg_BackTrace() {
#if MK_WINDOWS_ENABLED
#else
	int i, n;
	void *buffer[PATH_MAX];
	char **strings;

	n = backtrace(buffer, sizeof(buffer)/sizeof(buffer[0]));
	if( n < 1 ) {
		return;
	}

	strings = backtrace_symbols(buffer, n);
	if( !strings ) {
		return;
	}

	Dbg_Enter("BACKTRACE(%i)", n);
	for( i=0; i<n; i++ ) {
		Dbg_Outf("%s\n", strings[i]);
	}
	Dbg_Leave();

	free((void *)strings);
#endif
}

/*
 *	========================================================================
 *	COLORED PRINTING CODE
 *	========================================================================
 *	Display colored output.
 */

#if MK_WINDOWS_ENABLED
HANDLE g_sioh[__SIO_NUM__];
#endif
FILE *g_siof[__SIO_NUM__];

int Sys_IsColoredOutputEnabled() {
	return g_flags_color != kMkColorMode_None;
}

void Sys_InitColoredOutput() {
#if MK_WINDOWS_ENABLED
	g_sioh[SIO_OUT] = GetStdHandle(STD_OUTPUT_HANDLE);
	g_sioh[SIO_ERR] = GetStdHandle(STD_ERROR_HANDLE);
#endif

	/*
	 * TODO: Make sure that colored output is possible on this terminal
	 */

	g_siof[SIO_OUT] = stdout;
	g_siof[SIO_ERR] = stderr;
}

unsigned char Sys_GetCurrColor(sio_t sio) {
#if MK_WINDOWS_ENABLED
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	if( g_sioh[sio] != INVALID_HANDLE_VALUE ) {
		if( g_sioh[sio] && GetConsoleScreenBufferInfo(g_sioh[sio], &csbi) ) {
			return csbi.wAttributes & 0xFF;
		}
	}
#else
	/*
	 * TODO: Support this on GNU/Linux...
	 */
	(void)sio;
#endif

	return 0x07;
}
void Sys_SetCurrColor(sio_t sio, unsigned char color) {
	/*
		* MAP ORDER:
		* Black, Blue, Green, Cyan, Red, Magenta, Yellow, Grey
		*
		* TERMINAL COLOR ORDER:
		* Black=0, Red=1, Green=2, Yellow=3, Blue=4, Magenta=5, Cyan=6, Grey=7
		*/
	static const char *const mapF[16] = {
		"\x1b[30;22m", "\x1b[34;22m", "\x1b[32;22m", "\x1b[36;22m",
		"\x1b[31;22m", "\x1b[35;22m", "\x1b[33;22m", "\x1b[37;22m",
		"\x1b[30;1m", "\x1b[34;1m", "\x1b[32;1m", "\x1b[36;1m",
		"\x1b[31;1m", "\x1b[35;1m", "\x1b[33;1m", "\x1b[37;1m"
	};
#if 0
	static const char *const mapB[16] = {
		"\x1b[40m", "\x1b[41m", "\x1b[42m", "\x1b[43m",
		"\x1b[44m", "\x1b[45m", "\x1b[46m", "\x1b[47m",
		"\x1b[40;1m", "\x1b[41;1m", "\x1b[42;1m", "\x1b[43;1m",
		"\x1b[44;1m", "\x1b[45;1m", "\x1b[46;1m", "\x1b[47;1m"
	};
#endif

	switch( Opt_GetColorMode() ) {
	case kMkColorMode_None:
		break;
		
	case kMkColorMode_ANSI:
		fwrite(mapF[color & 0x0F], strlen(mapF[color & 0x0F]), 1, g_siof[sio]);
		break;
		
#if MK_WINDOWS_COLORS_ENABLED
	case kMkColorMode_Windows:
		if( g_sioh[sio] != INVALID_HANDLE_VALUE ) {
			SetConsoleTextAttribute(g_sioh[sio], (WORD)color);
		}
		break;
#endif

	case kMkNumColorModes:
		ASSERT_MSG(0, "Invalid color mode!");
		break;
	}
}
void Sys_UncoloredPuts(sio_t sio, const char *text, size_t len) {
	if( !len ) {
		len = strlen(text);
	}

	Dbg_Outf("%.*s", len, text);

#if MK_WINDOWS_ENABLED && 0
	if( g_sioh[sio] != INVALID_HANDLE_VALUE ) {
		WriteFile(g_sioh[sio], text, len, NULL, NULL);
		return;
	}
#endif

	fwrite(text, len, 1, g_siof[sio]);
}
int Sys_CharToColorCode(char c) {
	if( c>='0' && c<='9' ) {
		return  0 + (int)(c - '0');
	}

	if( c>='A' && c<='F' ) {
		return 10 + (int)(c - 'A');
	}

	if( c>='a' && c<='f' ) {
		return 10 + (int)(c - 'a');
	}

	return -1;
}
void Sys_Puts(sio_t sio, const char *text) {
	unsigned char prevColor;
	const char *s, *e;
	int color;

	/* set due to potential bad input pulling garbage color */
	prevColor = Sys_GetCurrColor(sio);

	s = text;
	while( 1 ) {
		/* write normal characters in one chunk */
		if( *s!='^' ) {
			e = strchr(s, '^');
			if( !e ) {
				e = strchr(s, '\0');
			}

			Sys_UncoloredPuts(sio, s, e - s);
			if( *e=='\0' ) {
				break;
			}

			s = e;
			continue;
		}

		/* must be a special character, treat it as such */
		color = Sys_CharToColorCode(*++s);
		if( color != -1 ) {
			prevColor = Sys_GetCurrColor(sio);
			Sys_SetCurrColor(sio, (unsigned char)color);
		} else if(*s=='&') {
			color = (int)prevColor;
			prevColor = Sys_GetCurrColor(sio);
			Sys_SetCurrColor(sio, (unsigned char)color);
		} else if(*s=='^') {
			Sys_UncoloredPuts(sio, "^", 1);
		}

		s++;
	}
}
void Sys_Printf(sio_t sio, const char *format, ...) {
	static char buf[32768];
	va_list args;

	va_start(args, format);
#if __STDC_WANT_SECURE_LIB__
	vsprintf_s(buf, sizeof(buf), format, args);
#else
	vsnprintf(buf, sizeof(buf), format, args);
	buf[sizeof(buf) - 1] = '\0';
#endif
	va_end(args);

	Sys_Puts(sio, buf);
}

void Sys_PrintStr(sio_t sio, unsigned char color, const char *str) {
	unsigned char curColor;

	ASSERT( str != (const char *)0 );

	curColor = Sys_GetCurrColor(sio);
	Sys_SetCurrColor(sio, color);
	Sys_UncoloredPuts(sio, str, 0);
	Sys_SetCurrColor(sio, curColor);
}
void Sys_PrintUint(sio_t sio, unsigned char color, unsigned int val) {
	char buf[64];

#if __STDC_WANT_SECURE_LIB__
	sprintf_s(buf, sizeof(buf), "%u", val);
#else
	snprintf(buf, sizeof(buf), "%u", val);
	buf[sizeof(buf) - 1] = '\0';
#endif

	Sys_PrintStr(sio, color, buf);
}
void Sys_PrintInt(sio_t sio, unsigned char color, int val) {
	char buf[64];

#if __STDC_WANT_SECURE_LIB__
	sprintf_s(buf, sizeof(buf), "%i", val);
#else
	snprintf(buf, sizeof(buf), "%i", val);
	buf[sizeof(buf) - 1] = '\0';
#endif

	Sys_PrintStr(sio, color, buf);
}

/*
 *	========================================================================
 *	ERROR HANDLING CODE
 *	========================================================================
 *	Display errors in an appropriate manner.
 */

/* display a simple Log_Error message */
void Log_ErrorMsg(const char *message) {
	unsigned char curColor;

	curColor = Sys_GetCurrColor(SIO_ERR);
	Sys_Printf(SIO_ERR, S_COLOR_RED "ERROR" S_COLOR_RESTORE ": %s", message);
	Sys_SetCurrColor(SIO_ERR, curColor);
	if( errno ) {
		Sys_Printf(SIO_ERR, ": %s [" S_COLOR_MAGENTA "%d"
			S_COLOR_RESTORE "]", strerror(errno), errno);
	}
	Sys_UncoloredPuts(SIO_ERR, "\n", 1);

	Dbg_BackTrace();
}

/* exit with an Log_Error message; if errno is set, display its Log_Error */
NORETURN void Log_FatalError(const char *message) {
	Log_ErrorMsg(message);
	exit(EXIT_FAILURE);
}

/* provide a general purpose Log_Error, without terminating */
void Log_Error(const char *file, unsigned int line, const char *func,
const char *message) {
	if( file ) {
		Sys_PrintStr(SIO_ERR, COLOR_WHITE, file);
		Sys_UncoloredPuts(SIO_ERR, ":", 1);
		if( line ) {
			Sys_PrintUint(SIO_ERR, COLOR_BROWN, line);
			Sys_UncoloredPuts(SIO_ERR, ":", 1);
		}
		Sys_UncoloredPuts(SIO_ERR, " ", 1);
	}

	Sys_Puts(SIO_ERR, S_COLOR_RED "ERROR" S_COLOR_RESTORE ": ");

	if( func ) {
		unsigned char curColor;

		curColor = Sys_GetCurrColor(SIO_ERR);
		Sys_Puts(SIO_ERR, "in " S_COLOR_GREEN);
		Sys_UncoloredPuts(SIO_ERR, func, 0);
		Sys_SetCurrColor(SIO_ERR, curColor);
		Sys_UncoloredPuts(SIO_ERR, ": ", 2);
	}

	if( message ) {
		Sys_UncoloredPuts(SIO_ERR, message, 0);
	}

	if( (errno&&message) || !message ) {
		if( message ) {
			Sys_UncoloredPuts(SIO_ERR, ": ", 2);
		}
		Sys_UncoloredPuts(SIO_ERR, strerror(errno), 0);
		Sys_Printf(SIO_ERR, "[" S_COLOR_MAGENTA "%d" S_COLOR_RESTORE "]",
			errno);
	}

	Sys_UncoloredPuts(SIO_ERR, "\n", 1);
	Dbg_BackTrace();
}

/* provide an Log_Error for a failed ASSERT */
void Log_ErrorAssert(const char *file, unsigned int line, const char *func,
const char *message) {
	Log_Error(file, line, func, message);
#if MK_WINDOWS_ENABLED
	fflush( stdout );
	fflush( stderr );

	if( IsDebuggerPresent() ) {
# if defined( _MSC_VER )
		__debugbreak();
# elif defined( __GNUC__ ) || defined( __clang__ )
		__builtin_trap();
# else
		DebugBreak();
# endif
	}
#endif
	/*exit(EXIT_FAILURE);*/
	abort();
}


/*
 *	========================================================================
 *	MEMORY CODE
 *	========================================================================
 *	Utility functions for managing memory. Allocated objects can have
 *	hierarchies of memory, such that when a "super" node is deallocated, its
 *	sub nodes are also deallocated. Destructor functions can be assigned as
 *	well to do other clean-up tasks. Reference counting is supported when
 *	ownership isn't as simple. (Objects allocated default to a reference
 *	count of one.)
 */

struct Mem__Hdr_s
{
	struct Mem__Hdr_s *pPrnt;
	struct Mem__Hdr_s *pPrev, *pNext;
	struct Mem__Hdr_s *pHead, *pTail;
	Mem_Fini_fn_t pfnFini;
	size_t cRefs;
	size_t cBytes;
#if MEM_LOCTRACE_ENABLED
	const char *pszFile;
	unsigned int uLine;
	const char *pszFunction;
#endif
};

static void Mem__Unlink( struct Mem__Hdr_s *pHdr )
{
	if( pHdr->pPrev != NULL ) {
		pHdr->pPrev->pNext = pHdr->pNext;
	} else if( pHdr->pPrnt != NULL ) {
		pHdr->pPrnt->pHead = pHdr->pNext;
	}

	if( pHdr->pNext != NULL ) {
		pHdr->pNext->pPrev = pHdr->pPrev;
	} else if( pHdr->pPrnt != NULL ) {
		pHdr->pPrnt->pTail = pHdr->pPrev;
	}

	pHdr->pPrnt = NULL;
	pHdr->pPrev = NULL;
	pHdr->pNext = NULL;
}

/*
================
Mem__MaybeAlloc

Potentially allocate memory. (Returns NULL on failure.)
================
*/
void *Mem__MaybeAlloc( size_t cBytes, bitfield_t uFlags, const char *pszFile, unsigned int uLine, const char *pszFunction )
{
	struct Mem__Hdr_s *pHdr;
	void *p;

	pHdr = ( struct Mem__Hdr_s * )malloc( sizeof( *pHdr ) + cBytes );
	if( !pHdr ) {
		return NULL;
	}

	p = ( void * )( pHdr + 1 );

	pHdr->pPrnt = NULL;
	pHdr->pPrev = NULL;
	pHdr->pNext = NULL;
	pHdr->pHead = NULL;
	pHdr->pTail = NULL;
	pHdr->pfnFini = NULL;
	pHdr->cRefs = 1;
	pHdr->cBytes = cBytes;
#if MEM_LOCTRACE_ENABLED
	pHdr->pszFile = pszFile;
	pHdr->uLine = uLine;
	pHdr->pszFunction = pszFunction;
#else
	( void )pszFile;
	( void )uLine;
	( void )pszFunction;
#endif

	if( ( uFlags & kMemF_Uninitialized ) == 0 ) {
		memset( p, 0, cBytes );
	}

#if LOG_MEMORY_ALLOCS
	Dbg_Outf( "ALLOC: %s(%i) in %s: %p, %u;", pszFile, uLine, pszFunction, p,
		( unsigned int )cBytes );
#endif

	return p;
}
/*
================
Mem__Alloc

Allocate memory without returning NULL. If an allocation fails, provide an error
then exit the process.
================
*/
void *Mem__Alloc( size_t cBytes, bitfield_t uFlags, const char *pszFile, unsigned int uLine, const char *pszFunction )
{
	void *p;

	p = Mem__MaybeAlloc( cBytes, uFlags, pszFile, uLine, pszFunction );
	if( !p ) {
		Log_FatalError( "Out of memory" );
	}

	return p;
}
/*
================
Mem__Dealloc

Decrement the reference count of the memory block. If the reference count
reaches zero then deallocate all sub-blocks of memory. Throw an error if any of
the sub-blocks have a reference count greater than one.
================
*/
void *Mem__Dealloc( void *pBlock, const char *pszFile, unsigned int uLine, const char *pszFunction )
{
	struct Mem__Hdr_s *pHdr;

	if( !pBlock ) {
		return NULL;
	}

	ASSERT( ( size_t )pBlock > 4096 );

	pHdr = ( struct Mem__Hdr_s * )pBlock - 1;

#if LOG_MEMORY_ALLOCS
	Dbg_Outf( "DEALLOC: %s(%i) in %s: %s%p, %u (refcnt=%u);", pszFile, uLine,
		pszFunction, pHdr->pPrnt != NULL ? "[sub]" : "", pBlock,
		( unsigned int )pHdr->cBytes, ( unsigned int )pHdr->cRefs );
#endif

	ASSERT( pHdr->cRefs > 0 );
	if( --pHdr->cRefs != 0 ) {
		return NULL;
	}

	while( pHdr->pHead != NULL ) {
		ASSERT( pHdr->pHead->cRefs <= 1 );
		Mem__Dealloc( ( void * )( pHdr->pHead + 1 ), pszFile, uLine, pszFunction );
	}

	if( pHdr->pfnFini != NULL ) {
		pHdr->pfnFini( pBlock );
	}

	Mem__Unlink( pHdr );

	free( ( void * )pHdr );
	return NULL;
}
/*
================
Mem__AddRef

Increase the reference count of a memory block. Returns the address passed in.
================
*/
void *Mem__AddRef( void *pBlock, const char *pszFile, unsigned int uLine, const char *pszFunction )
{
	struct Mem__Hdr_s *pHdr;

	ASSERT( pBlock != NULL );

	pHdr = ( struct Mem__Hdr_s * )pBlock - 1;

	++pHdr->cRefs;

#if LOG_MEMORY_ALLOCS
	Dbg_Outf( "MEM-ADDREF: %s(%i) in %s: %p, %u (refcnt=%u);", pszFile, uLine,
		pszFunction, pBlock, ( unsigned int )cBytes, ( unsigned int )pHdr->cRefs );
#else
	( void )pszFile;
	( void )uLine;
	( void )pszFunction;
#endif

	return pBlock;
}
/*
================
Mem__Attach

Set a block to be the sub-block of some super-block. The sub-block will be
deallocated when the super-block is deallocated. The sub-block does NOT have its
reference count increased upon being attached to the super-block.

The super-block (pSuperBlock) cannot be NULL. To remove a block from an existing
super-block, call Mem__Detach().

Returns the address of the sub-block (pBlock) passed in.
================
*/
void *Mem__Attach( void *pBlock, void *pSuperBlock, const char *pszFile, unsigned int uLine, const char *pszFunction )
{
	struct Mem__Hdr_s *pHdr;
	struct Mem__Hdr_s *pSuperHdr;

	ASSERT( pBlock != NULL );
	ASSERT( pSuperBlock != NULL );

	pHdr = ( struct Mem__Hdr_s * )pBlock - 1;
	pSuperHdr = ( struct Mem__Hdr_s * )pSuperBlock - 1;

#if LOG_MEMORY_ALLOCS
	Dbg_Outf( "MEM-ATTACH: %s(%i) in %s: %p(%u)->%p(%u);",
		pszFile, uLine, pszFunction,
		pBlock, ( unsigned int )pBlock->cBytes,
		pSuperBlock, ( unsigned int )pSuperBlock->cBytes );
#else
	( void )pszFile;
	( void )uLine;
	( void )pszFunction;
#endif

	Mem__Unlink( pHdr );

	pHdr->pPrnt = pSuperHdr;
	pHdr->pPrev = NULL;
	pHdr->pNext = pSuperHdr->pHead;
	if( pSuperHdr->pHead != NULL ) {
		pSuperHdr->pHead->pPrev = pHdr;
	} else {
		pSuperHdr->pTail = pHdr;
	}
	pSuperHdr->pHead = pHdr;

	return pBlock;
}
/*
================
Mem__Detach

Remove a block from the super-block it is attached to, if any. This ensures the
block will not be deallocated when its super-block is allocated. (Useful when
new ownership is necessary.)

Only call this function if absolutely necessary. Changing or removing ownership
is discouraged.

Returns the address passed in (pBlock).
================
*/
void *Mem__Detach( void *pBlock, const char *pszFile, unsigned int uLine, const char *pszFunction )
{
	struct Mem__Hdr_s *pHdr;

	ASSERT( pBlock != NULL );

	pHdr = ( struct Mem__Hdr_s * )pBlock - 1;

#if LOG_MEMORY_ALLOCS
	Dbg_Outf( "MEM-DETACH: %s(%i) in %s: %p(%u);",
		pszFile, uLine, pszFunction,
		pBlock, ( unsigned int )pBlock->cBytes );
#else
	( void )pszFile;
	( void )uLine;
	( void )pszFunction;
#endif

	Mem__Unlink( pHdr );
	return pBlock;
}
/*
================
Mem__SetFini

Set the function to call when the program is finished with the block of memory.
(i.e., the clean-up / destructor function.)

Returns the address passed in (pBlock).
================
*/
void *Mem__SetFini( void *pBlock, Mem_Fini_fn_t pfnFini )
{
	struct Mem__Hdr_s *pHdr;

	ASSERT( pBlock != NULL );

	pHdr = ( struct Mem__Hdr_s * )pBlock - 1;
	pHdr->pfnFini = pfnFini;

	return pBlock;
}
/*
================
Mem__Size

Retrieve the size of a block of memory, in bytes.
================
*/
size_t Mem__Size( const void *pBlock )
{
	const struct Mem__Hdr_s *pHdr;

	if( !pBlock ) {
		return 0;
	}

	pHdr = ( const struct Mem__Hdr_s * )pBlock - 1;

	return pHdr->cBytes;
}


/*
 *	========================================================================
 *	UTILITY CODE
 *	========================================================================
 *	A set of utility functions for making the rest of this easier.
 */

/*
================
Com__Memory

manage memory; never worry about errors
================
*/
void *Com__Memory( void *p, size_t n, const char *pszFile, unsigned int uLine, const char *pszFunction )
{
	errno = 0;

	if( n > 0 ) {
		void *q;

		q = Mem__Alloc( n, 0, pszFile, uLine, pszFunction );

		if( p != NULL ) {
			size_t c;
#if MK_DEBUG_ENABLED
			const struct Mem__Hdr_s *pHdr;

			pHdr = ( const struct Mem__Hdr_s * )p - 1;
			ASSERT_MSG( pHdr->cRefs == 1, "Cannot reallocate object: there are multiple references" );
			ASSERT_MSG( pHdr->pHead == NULL, "Cannot reallocate object: it has associated objects" );
			ASSERT_MSG( pHdr->pPrnt == NULL, "Cannot reallocate object: it is owned by another object" );
#endif

			c = Mem__Size( p );
			memcpy( q, p, c < n ? c : n );
			Mem__Dealloc( p, pszFile, uLine, pszFunction );
		}

		return q;
	}

	Mem__Dealloc( p, pszFile, uLine, pszFunction );
	return NULL;
}

/*
================
Com_Va

provide printf-style formatting on the fly
================
*/
const char *Com_Va(const char *format, ...) {
	static char buf[65536];
	static size_t index = 0;
	va_list args;
	size_t n;
	char *p;

	p = &buf[index];

	va_start(args, format);
#if _STDC_WANT_SECURE_LIB_
	n = vsprintf_s(&buf[index], sizeof(buf) - index, format, args);
#else
	n = vsnprintf(&buf[index], sizeof(buf) - index, format, args);
	buf[sizeof(buf) - 1] = '\0';
#endif
	va_end(args);

	index += n;
	if( index + VA_MINIMUM_SIZE > sizeof(buf) ) {
		index = 0;
	}

	return p;
}

/* secure strlen */
size_t Com_Strlen(const char *src)
{
	ASSERT( src != (const char *)0 );
	return strlen(src);
}

/* secure strcat */
size_t Com_Strcat(char *buf, size_t bufn, const char *src) {
	size_t index, len;

	index = Com_Strlen(buf);
	len = Com_Strlen(src);

	if( index+len >= bufn ) {
		Log_FatalError("detected overflow");
	}

	memcpy((void *)&buf[index], (const void *)src, len+1);
	return index+len;
}

/* secure strncat */
size_t Com_Strncat(char *buf, size_t bufn, const char *src, size_t srcn) {
	size_t index, len;

	index = Com_Strlen(buf);
	len = srcn ? srcn : Com_Strlen(src);

	if( index+len >= bufn ) {
		Log_FatalError("detected overflow");
	}

	memcpy((void *)&buf[index], (const void *)src, len);
	buf[index + len] = '\0';

	return index+len;
}

/* secure strcpy */
void Com_Strcpy(char *dst, size_t dstn, const char *src) {
	size_t i;

	for( i=0; src[i]; i++ ) {
		dst[i] = src[i];
		if( i==dstn-1 ) {
			Log_FatalError("detected overflow");
		}
	}

	dst[i] = 0;
}

/* secure strncpy */
void Com_Strncpy(char *buf, size_t bufn, const char *src, size_t srcn) {
	if( srcn >= bufn ) {
		Log_FatalError("detected overflow");
	}

	strncpy(buf, src, srcn);
	buf[srcn] = 0;
}

/* secure strdup; uses Com_Memory() */
char *Com__Strdup(const char *cstr,const char *file,unsigned int line,const char *func) {
	size_t n;
	char *p;

	ASSERT( cstr != (const char *)0 );

	n = strlen(cstr) + 1;

	p = (char *)Mem__Alloc(n, kMemF_Uninitialized, file, line, func);
	memcpy((void *)p, (const void *)cstr, n);

	return p;
}

/* strdup() in-place alternative */
char *Com_Dup(char *dst, const char *src) {
	size_t n;

	if( src ) {
		n = Com_Strlen(src) + 1;

		dst = (char *)Com_Memory((void *)dst, n);
		memcpy((void *)dst, (const void *)src, n);
	} else {
		dst = (char *)Com_Memory((void *)dst, 0);
	}

	return dst;
}
char *Com_DupN(char *dst, const char *src, size_t numchars) {
	if( !numchars ) {
		numchars = Com_Strlen(src);
	}

	if( src ) {
		dst = (char *)Com_Memory((void *)dst, numchars + 1);
		memcpy((void *)dst, (const void *)src, numchars);
		dst[ numchars ] = '\0';
	} else {
		dst = (char *)Com_Memory((void *)dst, 0);
	}

	return dst;
}

/* dynamic version of strcat() */
char *Com_Append(char *dst, const char *src) {
	size_t l, n;

	if( !src ) {
		return dst;
	}

	l = dst ? Com_Strlen(dst) : 0;
	n = Com_Strlen(src) + 1;

	dst = (char *)Com_Memory((void *)dst, l + n);
	memcpy((void *)&dst[l], (const void *)src, n);

	return dst;
}

/* extract the directory part of a filename */
const char *Com_ExtractDir(char *buf, size_t n, const char *filename) {
	const char *p;

	if( (p = strrchr(filename, '/')) != (const char *)0 ) {
		Com_Strncpy(buf, n, filename, (p-filename) + 1);
		return &p[1];
	}

	*buf = 0;
	return filename;
}

/* copy the source string into the destination string, overwriting the
   extension (if present, otherwise appending) with 'ext' */
void Com_SubstExt(char *dst, size_t dstn, const char *src, const char *ext) {
	const char *p;

	ASSERT( dst != (char *)0 );
	ASSERT( dstn > 1 );
	ASSERT( src != (const char *)0 );

	p = strrchr(src, '.');
	if( !p ) {
		p = strchr(src, 0);
	}

	ASSERT( p != (const char *)0 );

	Com_Strncpy(dst, dstn, src, p - src);
	Com_Strcpy(&dst[p - src], dstn - (size_t)(p - src), ext);
}

/* run a command in the Com_Shell */
int Com_Shell(const char *format, ...) {
	static char cmd[16384];
	va_list args;

	va_start(args, format);
#if __STDC_WANT_SECURE_LIB__
	vsprintf_s(cmd, sizeof(cmd), format, args);
#else
	vsnprintf(cmd, sizeof(cmd), format, args);
	cmd[sizeof(cmd)-1] = 0;
#endif
	va_end(args);

#ifdef _WIN32
	{
		char *p, *e;

		e = strchr(cmd, ' ');
		if( !e ) {
			e = strchr(cmd, '\0');
		}

		for( p=&cmd[0]; p && p<e; p=strchr(p, '/') ) {
			if( *p=='/' ) {
				*p = '\\';
			}
		}
	}
#endif

	if( g_flags & kFlag_Verbose_Bit ) {
		Sys_PrintStr(SIO_ERR, COLOR_LIGHT_CYAN, "> ");
		Sys_PrintStr(SIO_ERR, COLOR_CYAN, cmd);
		Sys_UncoloredPuts(SIO_ERR, "\n", 1);
	}

	fflush(g_siof[SIO_ERR]);

	return system(cmd);
}

/* determine whether a given relative path is part of an absolute path */
int Com_MatchPath(const char *rpath, const char *apath) {
	size_t rl, al;

	ASSERT( rpath != (const char *)0 );
	ASSERT( apath != (const char *)0 );

	rl = Com_Strlen(rpath);
	al = Com_Strlen(apath);

	if( rl > al ) {
		return 0;
	}

#if MK_WINDOWS_ENABLED
	if( _stricmp(&apath[al - rl], rpath) != 0 ) {
		return 0;
	}
#else
	if( strcmp(&apath[al - rl], rpath) != 0 ) {
		return 0;
	}
#endif

	if( al - rl > 0 ) {
		if( apath[al -rl - 1] != '/' ) {
			return 0;
		}
	}

	return 1;
}

/* retrieve the current date as an integral date -- YYYYMMDD, e.g., 20120223 */
int Com_GetIntDate() {
	struct tm *p;
	time_t t;

	t = time(0);
	p = localtime(&t);

	return (p->tm_year + 1900)*10000 + (p->tm_mon + 1)*100 + p->tm_mday;
}

/* find the end of an argument within a string */
const char *Com_FindArgEnd(const char *arg) {
	const char *p;

	if( *arg!='\"' ) {
		p = strchr(arg, ' ');
		if( !p ) {
			p = strchr(arg, '\0');
		}

		return p;
	}

	for( p=arg; *p!='\0'; p++ ) {
		if( *p=='\\' ) {
			p++;
			continue;
		}

		if( *p=='\"' ) {
			return p + 1;
		}
	}

	return p;
}

/* determine whether one argument in a string is equal to another */
int Com_MatchArg(const char *a, const char *b) {
	const char *x, *y;

	x = Com_FindArgEnd(a);
	y = Com_FindArgEnd(b);

	if( x - a != y - b ) {
		return 0;
	}

	/*
	 * TODO: compare content of each argument, not the raw input
	 */

	return (int)(strncmp(a, b, x - a) == 0);
}

/* skip to next argument within a string */
const char *Com_SkipArg(const char *arg) {
	const char *p;

	p = Com_FindArgEnd(arg);
	while( *p<=' ' && *p!='\0' ) {
		p++;
	}

	if( *p=='\0' ) {
		return NULL;
	}

	return p;
}

/* puts the lotion on its skin; else it gets the hose again */
void Com_StripArgs(char *dst, size_t n, const char *src) {
	const char *p, *q, *next;

	Com_Strcpy(dst, n, "");

	for( p=src; p!=NULL; p=next ) {
		next = Com_SkipArg(p);

		for( q=src; q<p; q=Com_SkipArg(q) ) {
			if( Com_MatchArg(q, p) ) {
				break;
			}
		}

		if( q < p ) {
			continue;
		}

		q = next ? next : strchr(p, '\0');
		Com_Strncat(dst, n, p, q - p);
	}
}

/* determine whether two characters of a path match */
int Com_MatchPathChar(char a, char b) {
#if MK_WINDOWS_ENABLED
	if( a>='A' && a<='Z' ) {
		a = a - 'A' + 'a';
	}
	if( b>='A' && b<='Z' ) {
		b = b - 'A' + 'a';
	}
	if( a=='\\' ) {
		a = '/';
	}
	if( b=='\\' ) {
		b = '/';
	}
#endif

	return a == b ? 1 : 0;
}

/* retrieve a relative path */
int Com_RelPath(char *dst, size_t dstn, const char *curpath,
const char *abspath) {
	size_t i;

	ASSERT( dst != (char *)0 );
	ASSERT( dstn > 1 );
	ASSERT( curpath != (const char *)0 );
	ASSERT( abspath != (const char *)0 );

	i = 0;
	while( Com_MatchPathChar(curpath[i], abspath[i]) && curpath[i]!='\0' ) {
		i++;
	}

	if( curpath[i]=='\0' && abspath[i]=='\0' ) {
		Com_Strcpy(dst, dstn, "");
	} else {
		Com_Strcpy(dst, dstn, &abspath[i]);
	}

	return 1;
}

/* retrieve a relative path based on the current working directory */
int Com_RelPathCWD(char *dst, size_t dstn, const char *abspath) {
	char cwd[PATH_MAX];

	if( !FS_GetCWD(cwd, sizeof(cwd)) ) {
		return 0;
	}

	return Com_RelPath(dst, dstn, cwd, abspath);
}

/*
 *	========================================================================
 *	OPTIONS SYSTEM
 *	========================================================================
 *	Retrieve several options. In the future these are to be made more
 *	configurable.
 */

/* retrieve the object directory base (where intermediate files go) */
const char *Opt_GetObjdirBase(void)
{
	return MK_DEFAULT_OBJDIR_BASE "/" MK_PLATFORM_DIR;
}
/* retrieve the path to the debug log */
const char *Opt_GetDebugLogPath(void)
{
	return MK_DEFAULT_OBJDIR_BASE "/" MK_DEFAULT_DEBUGLOG_FILENAME;
}
/* retrieve the debug suffix used when naming debug target output */
const char *Opt_GetDebugSuffix(void)
{
	return MK_DEFAULT_DEBUG_SUFFIX;
}
/* retrieve the color mode used when writing colored output */
MkColorMode_t Opt_GetColorMode(void)
{
	return g_flags_color;
}
/* retrieve the name of this configuration (debug, release) */
const char *Opt_GetConfigName(void)
{
	return
		( g_flags & kFlag_Release_Bit ) != 0
		? MK_DEFAULT_RELEASE_CONFIG_NAME
		: MK_DEFAULT_DEBUG_CONFIG_NAME;
}

/* retrieve the path to the "gen" directory */
const char *Opt_GetBuildGenPath(void)
{
	return MK_DEFAULT_OBJDIR_BASE "/gen";
}
/* retrieve the path to the "build-generated" include directory */
const char *Opt_GetBuildGenIncDir(void)
{
	return MK_DEFAULT_OBJDIR_BASE "/gen/include/build-generated";
}

/*
 *	========================================================================
 *	DYNAMIC STRING ARRAY SYSTEM
 *	========================================================================
 *	Manages a dynamic array of strings. Functions similarly to the C++ STL's
 *	std::vector<std::string> class.
 */

struct strList_s {
	size_t capacity;
	size_t size;
	char **data;

	struct strList_s *prev, *next;
};
struct strList_s *g_arr_head = (struct strList_s *)0;
struct strList_s *g_arr_tail = (struct strList_s *)0;

/* create a new (empty) array */
strList_t SL_New() {
	strList_t arr;

	arr = (strList_t)Com_Memory((void *)0, sizeof(*arr));

	arr->capacity = 0;
	arr->size = 0;
	arr->data = (char **)0;

	arr->next = (struct strList_s *)0;
	if( (arr->prev = g_arr_tail) != (struct strList_s *)0 ) {
		g_arr_tail->next = arr;
	} else {
		g_arr_head = arr;
	}
	g_arr_tail = arr;

	return arr;
}

/* retrieve the current amount of Com_Memory allocated for the array */
size_t SL_Capacity(strList_t arr) {
	ASSERT( arr != (strList_t)0 );

	return arr->capacity;
}

/* retrieve how many elements are in the array */
size_t SL_Size(strList_t arr) {
	if( !arr ) {
		return 0;
	}

	return arr->size;
}

/* retrieve the data of the array */
char **SL_Data(strList_t arr) {
	ASSERT( arr != (strList_t)0 );

	return arr->data;
}

/* retrieve a single element of the array */
char *SL_Get(strList_t arr, size_t i) {
	ASSERT( arr != (strList_t)0 );
	ASSERT( i < arr->size );

	return arr->data[i];
}

/* set a single element of the array */
void SL_Set(strList_t arr, size_t i, const char *cstr) {
	ASSERT( arr != (strList_t)0 );
	ASSERT( i < arr->size );

	if( !cstr ) {
		arr->data[i] = (char *)Com_Memory((void *)arr->data[i], 0);
	} else {
		arr->data[i] = Com_Strdup(cstr);
	}
}

/* deallocate the internal Com_Memory used by the array */
void SL_Clear(strList_t arr) {
	size_t i;

	ASSERT( arr != (strList_t)0 );

	for( i=0; i<arr->size; i++ ) {
		arr->data[i] = (char *)Com_Memory((void *)arr->data[i], 0);
	}

	arr->data = (char **)Com_Memory((void *)arr->data, 0);
	arr->capacity = 0;
	arr->size = 0;
}

/* delete an array */
void SL_Delete(strList_t arr) {
	if( !arr ) {
		return;
	}

	SL_Clear(arr);

	if( arr->prev ) {
		arr->prev->next = arr->next;
	}
	if( arr->next ) {
		arr->next->prev = arr->prev;
	}

	if( g_arr_head==arr ) {
		g_arr_head = arr->next;
	}
	if( g_arr_tail==arr ) {
		g_arr_tail = arr->prev;
	}

	Com_Memory((void *)arr, 0);
}

/* delete all arrays */
void SL_DeleteAll() {
	while( g_arr_head ) {
		SL_Delete(g_arr_head);
	}
}

/* set the new size of an array */
void SL_Resize(strList_t arr, size_t n) {
	size_t i;

	ASSERT( arr != (strList_t)0 );

	if( n > arr->capacity ) {
		i = arr->capacity;
		arr->capacity += 4096/sizeof(void *);
		/*arr->capacity += 32;*/

		arr->data = (char **)Com_Memory((void *)arr->data,
			arr->capacity*sizeof(char *));

		memset((void *)&arr->data[i], 0, (arr->capacity - i)*sizeof(char *));
	}

	arr->size = n;
}

/* add an element to the array, resizing if necessary */
void SL_PushBack(strList_t arr, const char *cstr) {
	size_t i;

	ASSERT( arr != (strList_t)0 );

	i = SL_Size(arr);
	SL_Resize(arr, i+1);
	SL_Set(arr, i, cstr);
}
/* remove the last element in the array */
void SL_PopBack(strList_t arr) {
	ASSERT( arr != (strList_t)0 );

	if( !arr->size ) {
		return;
	}

	SL_Set(arr, arr->size - 1, (const char *)0);
	--arr->size;
}

/* display the contents of an array */
void SL_Print(strList_t arr) {
	size_t i, n;

	ASSERT( arr != (strList_t)0 );

	n = SL_Size(arr);
	for( i=0; i<n; i++ ) {
#if MK_WINDOWS_ENABLED
		printf("%2u. \"%s\"\n", (unsigned int)i, SL_Get(arr, i));
#else
		/* ISO C90 does not support the 'z' modifier; ignore... */
		printf("%2zu. \"%s\"\n", i, SL_Get(arr, i));
#endif
	}

	printf("\n");
}
void SL_DbgPrint(strList_t arr) {
	size_t i, n;

	ASSERT( arr != (strList_t)0 );

	n = SL_Size(arr);
	for( i=0; i<n; i++ ) {
#if MK_WINDOWS_ENABLED
		Dbg_Outf("%2u. \"%s\"\n", (unsigned int)i, SL_Get(arr, i));
#else
		/* ISO C90 does not support the 'z' modifier; ignore... */
		Dbg_Outf("%2zu. \"%s\"\n", i, SL_Get(arr, i));
#endif
	}

	Dbg_Outf("\n");
}

/* alphabetically sort an array */
static int _SL_Cmp(const void *a, const void *b) {
	return strcmp(*(char *const *)a, *(char *const *)b);
}

void SL_Sort(strList_t arr) {
	if( arr->size < 2 ) {
		return;
	}

	qsort((void *)arr->data, arr->size, sizeof(char *), _SL_Cmp);
}

void SL_OrderedSort(strList_t arr, size_t *const buffer, size_t maxBuffer) {
	size_t i, j;
	size_t n;

	ASSERT( buffer != NULL );
	ASSERT( maxBuffer >= arr->size );

	n = arr->size < maxBuffer ? arr->size : maxBuffer;

	for( i=0; i<n; i++ ) {
		buffer[i] = (size_t)arr->data[i];
	}

	SL_Sort(arr);

	for( i=0; i<n; i++ ) {
		for( j=0; j<n; j++ ) {
			if( buffer[i]==(size_t)arr->data[j] ) {
				buffer[i] = j;
				break;
			}
		}

		ASSERT( j<n ); /* did exit from loop? */
	}
}
void SL_IndexedSort(strList_t arr, const size_t *const buffer, size_t bufferLen) {
#if 1
	strList_t backup;
	size_t i, n;

	/*
	 *	XXX: This is ugly.
	 *	FIXME: Ugly.
	 *	XXX: You can't fix ugly.
	 *	TODO: Buy a makeup kit for the beast.
	 *
	 *	In all seriousness, an in-place implementation should be written. This works for now,
	 *	though.
	 */

	ASSERT( buffer != NULL );
	ASSERT( bufferLen > 0 );

	backup = SL_New();
	n = bufferLen < arr->size ? bufferLen : arr->size;
	for( i=0; i<n; i++ ) {
		SL_PushBack(backup, SL_Get(arr, i));
	}

	for( i=0; i<n; i++ ) {
		SL_Set(arr, buffer[i], SL_Get(backup, i));
	}

	SL_Delete(backup);
#else
	char *tmp;
	size_t i;

	/*
	 *	FIXME: This partially works, but produces bad results.
	 */

	for( i=0; i<bufferLen; i++ ) {
		if( i > buffer[i] )
			continue;

		tmp = arr->data[buffer[i]];
		arr->data[buffer[i]] = arr->data[i];
		arr->data[i] = tmp;
	}
#endif
}
void SL_PrintOrderedBuffer(const size_t *const buffer, size_t bufferLen) {
	size_t i;

	printf("array buffer %p (%u element%s):\n", (void *)buffer,
		(unsigned int)bufferLen, bufferLen==1?"":"s");
	for( i=0; i<bufferLen; i++ ) {
		printf("  %u\n", (unsigned int)buffer[i]);
	}
	printf("\n");
}

/* remove duplicate entries from an array */
void SL_Unique(strList_t arr) {
	const char *a, *b;
	size_t i, j, k, n;

	n = SL_Size(arr);
	for( i=0; i<n; i++ ) {
		if( !(a = SL_Get(arr, i)) ) {
			continue;
		}

		for( j=i+1; j<n; j++ ) {
			if( !(b = SL_Get(arr, j)) ) {
				continue;
			}

			if( strcmp(a, b)==0 ) {
				SL_Set(arr, j, (const char *)0);
			}
		}
	}

	arr->size = 0;
	for( i=0; i<n; ++i ) {
		if( arr->data[i]!=(char *)0 ) {
			arr->size = i + 1;
			continue;
		}

		j = i + 1;
		while( j < n && arr->data[ j ] == ( char * )0 ) {
			++j;
		}

		if( j == n ) {
			break;
		}

		for( k = 0; j + k < n; ++k ) {
			arr->data[i+k] = arr->data[j+k];
		}

		--i;
	}
}

/*
 *	========================================================================
 *	BUFFER MANAGEMENT CODE
 *	========================================================================
 *	Manage input source file buffers (similar in nature to flex's buffer).
 *	Keeps track of the file's name and the current line.
 */
struct buffer_s {
	char *file;
	char *func;

	char *text;
	char *endPtr;
	char *ptr;

	size_t numLines;
	char **linePtrs;
};

static int _Buf_CopyN(char **dst, const char *src, size_t len) {
	size_t n;
	char *p;

	if( !src ) {
		if( *dst ) {
			Com_Memory((void *)*dst, 0);
			*dst = NULL;
		}

		return 1;
	}

	len = len ? len : strlen(src);
	n = len + 1;

	p = (char *)Com_Memory((void *)*dst, n);
	if( !p ) {
		return 0;
	}

	*dst = p;

	memcpy(p, src, len);
	p[len] = '\0';

	return 1;
}
static int _Buf_Copy(char **dst, const char *src) {
	return _Buf_CopyN(dst, src, 0);
}
#if 0
static int _Buf_Append(char **dst, const char *src, size_t *offset) {
	char *p;
	size_t len;

	if( (!src) || (!*dst && *offset>0) ) {
		return 0;
	}

	if( !*dst ) {
		return _Buf_Copy(dst, src);
	}

	len = strlen(src) + 1;

	p = (char *)(Com_Memory((void *)*dst, *offset + len));
	if( !p ) {
		return 0;
	}

	*dst = p;

	memcpy(&((*dst)[*offset]), src, len);
	*offset += len - 1;

	return 1;
}
#endif

int Buf_SetFileName(buffer_t text, const char *filename) {
	return _Buf_Copy(&text->file, filename);
}
int Buf_SetFunction(buffer_t text, const char *func) {
	return _Buf_Copy(&text->func, func);
}

static int _Buf_AddLinePtr(buffer_t text, char *ptr) {
	char **arr;
	size_t amt;

	if( !ptr ) {
		if( text->linePtrs ) {
			Com_Memory((void *)(text->linePtrs), 0);
			text->linePtrs = NULL;
			text->numLines = 0;
		}

		return 1;
	}

	amt = sizeof(char *)*(text->numLines + 1);
	arr = (char **)Com_Memory((void *)text->linePtrs, amt);
	if( !arr ) {
		return 0;
	}

	text->linePtrs = arr;
	text->linePtrs[text->numLines++] = ptr;

	return 1;
}

static buffer_t _Buf_Alloc() {
	buffer_t text;

	text = (buffer_t )Com_Memory(0, sizeof(*text));
	if( !text ) {
		return NULL;
	}

	text->file		= NULL;
	text->func		= NULL;

	text->text		= NULL;
	text->endPtr	= NULL;
	text->ptr		= NULL;

	text->numLines	= 0;
	text->linePtrs	= NULL;

	return text;
}
static void _Buf_Fini(buffer_t text) {
	_Buf_AddLinePtr(text, NULL);

	_Buf_Copy(&text->file, NULL);
	_Buf_Copy(&text->func, NULL);
	text->ptr = NULL;
}

#if PROCESS_NEWLINE_CONCAT
static const char *_Buf_SkipNewline(const char *p) {
	const char *e;

	e = NULL;
	if( *p=='\r' ) {
		e = ++p;
	}

	if( *p=='\n' ) {
		e = ++p;
	}

	if( *p=='\0' ) {
		e = p;
	}

	return e;
}
#endif
static int _Buf_InitFromMemory(buffer_t text, const char *filename,
const char *source, size_t len) {
#if PROCESS_NEWLINE_CONCAT
	const char *s, *e, *next;
	const char *add;
#else
	int add;
#endif

	ASSERT( !!text );
	ASSERT( !!filename );
	ASSERT( !!source );

	if( !Buf_SetFileName(text, filename) ) {
		return 0;
	}

#if PROCESS_NEWLINE_CONCAT
	if( !len ) {
		len = strlen( source );
	}

	text->text = Com_Memory(0, len + 1);
	if( !text->text ) {
		return 0;
	}

	*text->text = '\0';
#else
	if( !_Buf_CopyN(&text->text, source, len) ) {
		return 0;
	}
#endif

	_Buf_AddLinePtr(text, NULL);
	_Buf_AddLinePtr(text, text->text);

#if PROCESS_NEWLINE_CONCAT
	text->ptr = text->text;
	s = source;
	for( e=s; *e; e=next ) {
		next = e + 1;
		add = NULL; /*this will point to AFTER the newline or AT the EOF*/

		if( *e=='\\' ) {
			add = _Buf_SkipNewline(e + 1);
			if( !add ) {
				continue;
			}
		} else {
			add = _Buf_SkipNewline(e);
			if( !add ) {
				continue;
			}

			e = add; /*want newlines in source when not escaped!*/
		}

		memcpy(text->ptr, s, e - s);
		text->ptr += e - s;

		s = add;
		next = add;

		if( !_Buf_AddLinePtr(text, text->ptr) ) {
			_Buf_Fini(text);
			return 0;
		}
	}

	*text->ptr = '\0';
#else
	for( text->ptr=text->text; *text->ptr!='\0'; text->ptr++ ) {
		add = 0;

		if( *text->ptr=='\r' ) {
			if( text->ptr[1]=='\n' ) {
				text->ptr++;
			}
			add = 1;
		}
		if( *text->ptr=='\n' ) {
			add = 1;
		}

		if( add && !_Buf_AddLinePtr(text, &text->ptr[1]) ) {
			_Buf_Fini(text);
			return 0;
		}
	}
#endif

	text->endPtr = text->ptr;
	text->ptr = text->text;

#if 0
	printf("******************************************************\n");
	printf("#### ORIGINAL ####\n");
	printf("%s\n", source);
	printf("******************************************************\n");
	printf("#### READ-IN ####\n");
	printf("%s\n", text->text);
	printf("******************************************************\n");
#endif

	return 1;
}
static int _Buf_InitFromFile(buffer_t text, const char *filename) {
#if 0
		FILE *f;
		char *p, buf[8192];
		size_t offset;
		int r;

	#if __STDC_WANT_SECURE_LIB__
		if( fopen_s(&f, filename, "rb")!=0 ) {
			f = NULL;
		}
	#else
		f = fopen(filename, "rb");
	#endif
		if( !f ) {
			return 0;
		}

		p = NULL;
		offset = 0;
		r = 1;

		while( !feof(f) ) {
			int n;

			n = fread(buf, 1, sizeof(buf) - 1, f);
			if( n <= 0 ) {
				r = !ferror(f);
				break;
			}

			buf[ n ] = '\0';

			if( !_Buf_Append(&p, buf, &offset) ) {
				r = 0;
				break;
			}
		}

		fclose(f);

		if( !r || !_Buf_InitFromMemory(text, filename, p, 0) ) {
			_Buf_Copy(&p, NULL);
			return 0;
		}

		return 1;
#else
	FILE *f;
	char *p;
	size_t n;

# if MK_SECLIB
	if( fopen_s(&f, filename, "rb")!=0 ) {
		f = NULL;
	}
# else
	f = fopen(filename, "rb");
# endif
	if( !f ) {
		return 0;
	}

	fseek( f, 0, SEEK_END );
	n = ( size_t )ftell( f );

	fseek( f, 0, SEEK_SET );

	p = ( char * )Com_Memory( NULL, n + 1 );
	if( !fread( ( void * )p, n, 1, f ) ) {
		Com_Memory( ( void * )p, 0 );
		fclose( f );
		return 0;
	}

	fclose( f );
	f = NULL;

	p[ n ] = '\0';
	if( !_Buf_InitFromMemory(text, filename, p, 0) ) {
		Com_Memory( ( void * )p, 0 );
		return 0;
	}

	Com_Memory( ( void * )p, 0 );
	return 1;
#endif
}

buffer_t Buf_LoadMemoryN(const char *filename, const char *source, size_t len) {
	buffer_t text;

	text = _Buf_Alloc();
	if( !text ) {
		return NULL;
	}

	if( !_Buf_InitFromMemory(text, filename, source, len) ) {
		Com_Memory((void *)text, 0);
		return NULL;
	}

	return text;
}
buffer_t Buf_LoadMemory(const char *filename, const char *source) {
	return Buf_LoadMemoryN(filename, source, 0);
}
buffer_t Buf_LoadFile(const char *filename) {
	buffer_t text;

	text = _Buf_Alloc();
	if( !text ) {
		return NULL;
	}

	if( !_Buf_InitFromFile(text, filename) ) {
		Com_Memory((void *)text, 0);
		return NULL;
	}

	return text;
}
buffer_t Buf_Unload(buffer_t text) {
	if( !text ) {
		return NULL;
	}

	_Buf_Fini(text);
	Com_Memory((void *)text, 0);

	return NULL;
}

const char *Buf_GetFileName(const buffer_t text) {
	return text->file;
}
const char *Buf_GetFunction(const buffer_t text) {
	return text->func;
}
size_t Buf_GetLine(const buffer_t text) {
	size_t i;

	for( i=0; i<text->numLines; i++ ) {
		if( text->ptr < text->linePtrs[i] ) {
			break;
		}
	}

	return i + 1;
}

size_t Buf_GetLength(const buffer_t text) {
	return text->endPtr - text->text;
}

void Buf_Seek(buffer_t text, size_t pos) {
	text->ptr = &text->text[pos];

	if( text->ptr > text->endPtr ) {
		text->ptr = text->endPtr;
	}
}
size_t Buf_Tell(const buffer_t text) {
	return text->ptr - text->text;
}
char *Buf_Ptr(buffer_t text) {
	return text->ptr;
}

char Buf_Read(buffer_t text) {
	char c;

	c = *text->ptr++;
	if( text->ptr > text->endPtr ) {
		text->ptr = text->endPtr;
	}

	return c;
}
char Buf_Peek(buffer_t text) {
	return *text->ptr;
}
char Buf_Look(buffer_t text, size_t offset) {
	if( text->ptr + offset >= text->endPtr ) {
		return '\0';
	}

	return text->ptr[offset];
}
int Buf_Check(buffer_t text, char ch) {
	if( *text->ptr != ch ) {
		return 0;
	}

	if( ++text->ptr > text->endPtr ) {
		text->ptr = text->endPtr;
	}

	return 1;
}

void Buf_Skip(buffer_t text, size_t offset) {
	Buf_Seek(text, text->ptr - text->text + offset);
}
void Buf_SkipWhite(buffer_t text) {
	while( (unsigned char)(*text->ptr)<=' ' && text->ptr<text->endPtr ) {
		text->ptr++;
	}
}
void Buf_SkipLine(buffer_t text) {
	int r;

	r = 0;
	while( text->ptr < text->endPtr ) {
		if( *text->ptr=='\r' ) {
			r = 1;
			text->ptr++;
		}
		if( *text->ptr=='\n' ) {
			r = 1;
			text->ptr++;
		}

		if( r ) {
			return;
		}

		text->ptr++;
	}
}

int Buf_SkipComment(buffer_t text, const char *pszCommentDelim) {
	size_t n;
	
	ASSERT( pszCommentDelim != ( const char * )0 );
	
	n = strlen( pszCommentDelim );
	if( text->ptr + n > text->endPtr ) {
		return 0;
	}
	
	if( strncmp( text->ptr, pszCommentDelim, ( int )n ) != 0 ) {
		return 0;
	}

	text->ptr += n;
	Buf_SkipLine( text );

	return 1;
}

int Buf_ReadLine(buffer_t text, char *dst, size_t dstn) {
	const char *s, *e;

	s = text->ptr;
	if( s==text->endPtr ) {
		return -1;
	}

	Buf_SkipLine( text );
	e = text->ptr;
	
	if( e > s && *( e - 1 ) == '\n' ) {
		--e;
	}
	
	if( e > s && *( e - 1 ) == '\r' ) {
		--e;
	}

	Com_Strncpy( dst, dstn, s, e - s );

	return (int)(e - s);
}

void Buf_ErrorV(buffer_t text, const char *format, va_list args) {
	char buf[ 4096 ];

#if __STDC_WANT_SECURE_LIB__
	vsprintf_s(buf, sizeof(buf), format, args);
#else
	vsnprintf(buf, sizeof(buf), format, args);
	buf[sizeof(buf) - 1] = '\0';
#endif

	Log_Error(text->file, Buf_GetLine(text), text->func, buf);
}
void Buf_Error(buffer_t text, const char *format, ...) {
	va_list args;

	va_start(args, format);
	Buf_ErrorV(text, format, args);
	va_end(args);
}

/*
 *	========================================================================
 *	CONFIGURATION MANAGEMENT CODE
 *	========================================================================
 *	Manages property sheets and configurations.
 *
 *	Properties define individual options to apply per project. e.g., include
 *	directories, library directories, user defined macros, etc.
 *
 *	Property sheets are files with collections of properties.
 *
 *	Configurations are per-project collections of all the individual
 *	property sheets used for that specific project.
 *
 *	Workspace configurations are used to select the configurations used for
 *	each project. The global workspace configuration can be selected through
 *	the command-line. Legacy command-line options point to specific built-in
 *	or override files. If "-r" is specified then "release" will be assumed,
 *	otherwise "debug" will be assumed.
 *
 *	Individual property sheets are located in cfg/#.mk-prop. Default project
 *	configurations are at cfg/#.mk-proj. (e.g., cfg/debug.mk-proj.) Projects
 *	will use any .mk-proj files in their root directories. Unit tests will
 *	reference cfg/test.mk-proj or default.mk-proj within their directory.
 *
 *	Configuration files list the filenames of each .mk-prop the project
 *	uses. (Settings compound in the order they're specified.):
 *
 *		configuration/file/path.mk-proj
 *
 *	Workspace configuration files list the configurations each project will
 *	use. (The path to the configuration is not specified.) For a default
 *	configuration to be applied to all projects after that line, a
 *	configuration can be specified by itself. Any project which has that
 *	configuration will use it, otherwise if a "default.mk-proj" is available
 *	then that will be used. Barring that a warning will be generated and the
 *	system will continue using no special configuration.
 *
 *		project-name:configuration
 *
 *	It's desirable to also specify properties that are by default always
 *	included. (e.g., autolink-only properties.) These should be placed in
 *	cfg/include/#.mk-prop.
 *
 *	NOTE: There can be multiple configuration directories and they can use
 *	      different names. By default these are:
 *
 *				cfg/			props/
 *				config/			properties/
 *
 *		################################################################
 *		# TODO: Integrate into Mk...                                   #
 *		################################################################
 */
typedef struct property_s *property_t;
typedef struct propertySheet_s *propertySheet_t;
typedef struct configuration_s *configuration_t;

typedef struct propertyVar_s *propertyVar_t;
typedef struct propertyExpr_s *propertyExpr_t;
typedef struct propertyStmt_s *propertyStmt_t;
typedef struct propertyTagGroup_s *propertyTagGroup_t;

typedef enum {
	kPropTagMode_AllOf,
	kPropTagMode_AnyOf,
	kPropTagMode_NoneOf
} propertyTagMode_t;
typedef enum {
	kPropTag_Config_Debug,					/* debug */
	kPropTag_Config_Release,				/* release */

	kPropTag_ProjType_Executable,			/* exec */
	kPropTag_ProjType_Library,				/* lib */
	kPropTag_ProjType_StaticLibrary,		/* staticlib */
	kPropTag_ProjType_DynamicLibrary,		/* dynamiclib */
	kPropTag_ProjType_UnitTest,				/* test */

	kPropTag_ProjOrg_Normal,				/* projorg_project */
	kPropTag_ProjOrg_Package,				/* projorg_package */

	kPropTag_PropInc_Owned,					/* propinc_owned */
	kPropTag_PropInc_Super,					/* propinc_super */
	kPropTag_PropInc_Broad,					/* propinc_broad */

	kPropTag_TargetOS_Windows,				/* mswin */
	kPropTag_TargetOS_WindowsStore,			/* winrt */
	kPropTag_TargetOS_Linux,				/* linux */
	kPropTag_TargetOS_MacOSX,				/* macos */
	kPropTag_TargetOS_Haiku,				/* haiku */
	kPropTag_TargetOS_Unix,					/* unix */
	kPropTag_TargetOS_WebOS,				/* webos */
	kPropTag_TargetOS_Android,				/* droid */
	kPropTag_TargetOS_IOS,					/* ios */

	kPropTag_TargetArch_X86,				/* x86 */
	kPropTag_TargetArch_X64,				/* x64 */
	kPropTag_TargetArch_MIPS,				/* mips */
	kPropTag_TargetArch_MIPS64,				/* mips64 */
	kPropTag_TargetArch_ARM,				/* arm */
	kPropTag_TargetArch_AArch64,			/* aarch64 */
	kPropTag_TargetArch_PowerPC,			/* ppc */

	kPropTag_TargetUI_None,					/* ui_none */
	kPropTag_TargetUI_Win32,				/* ui_win32 */
	kPropTag_TargetUI_Metro,				/* ui_metro */
	kPropTag_TargetUI_X11,					/* ui_x11 */
	kPropTag_TargetUI_Cocoa,				/* ui_cocoa */
	kPropTag_TargetUI_Android,				/* ui_android */
	kPropTag_TargetUI_CocoaToach,			/* ui_cocoatoach */

	kPropTag_TargetCPU_Intel,				/* cpu_intel */
	kPropTag_TargetCPU_MIPS,				/* cpu_mips */
	kPropTag_TargetCPU_ARM,					/* cpu_arm */
	kPropTag_TargetCPU_PowerPC,				/* cpu_powerpc */

	kPropTag_TargetBits_32,					/* bits_32 */
	kPropTag_TargetBits_64,					/* bits_64 */

	kPropTag_TargetDevice_PC,				/* pc */
	kPropTag_TargetDevice_Mobile			/* mobile */
} propertyTag_t;
typedef enum {
	kPropVar_Bool
} propertyVarType_t;
typedef enum {
	kPropExpr_Literal,
	kPropExpr_Var,

	kPropExpr_Not,

	kPropExpr_And,
	kPropExpr_Or
} propertyExprType_t;
typedef enum {
	kPropStmt_Root,

	kPropStmt_Property,

	kPropStmt_If,
	kPropStmt_ElseIf,
	kPropStmt_EndIf
} propertyStmtType_t;
typedef enum {
	kProp_Unset,

	kProp_IncludeDir,			/* incdir path */
	kProp_LibraryDir,			/* libdir path */
	kProp_AutoHeader,			/* autoinc libname header */
	kProp_AutoLibrary,			/* autolib libname library */
	kProp_Define,				/* define name[=value] */
	kProp_CompileFlags,			/* compileflags flags */
	kProp_CFlags,				/* cflags flags -- for C only */
	kProp_CxxFlags,				/* c++flags flags -- for C++ only */
	kProp_LinkerInput,			/* link file */
	kProp_LinkerFlags			/* linkflags flags */
} propertyType_t;

struct property_s {
	propertyType_t type;
	char *value1;
	char *value2;
	struct property_s *prev, *next;
	struct propertySheet_s *prnt;
};
struct propertyVar_s {
	propertyVarType_t type;
	char *name;
	int ivalue;

	struct propertyVar_s *prev, *next;
};
struct propertyExpr_s {
	propertyExprType_t type;

	union
	{
		int literal;
		propertyVar_t *var;
	} value;

	struct propertyExpr_s *lhs, *rhs;
};
struct propertyStmt_s {
	propertyStmtType_t type;

	property_t *prop;
	propertyExpr_t *condexpr;

	struct propertyStmt_s *prnt;
	struct propertyStmt_s *prev, *next;
	struct propertyStmt_s *head, *tail;
};
struct propertyTagGroup_s {
	propertyTagMode_t mode;

	propertyTag_t *tags;
	size_t ntags;

	struct propertyTagGroup_s *tg_prev, *tg_next;
};
struct propertySheet_s {
	char *name;

	struct propertyTagGroup_s *tg_head, *tg_tail;
	struct propertyStmt_s *root;

	struct propertySheet_s *prnt;
	struct propertySheet_s *prev, *next;
	struct propertySheet_s *head, *tail;
};
struct configuration_s {
	struct configuration_s *cfg_prnt;
	struct configuration_s *cfg_prev, *cfg_next;
	struct configuration_s *cfg_head, *cfg_tail;

	struct propertyVar_s *var_head, *var_tail;

	struct property_s *props;
	size_t nprops;
};


/*
 *	========================================================================
 *	FILE SYSTEM MANAGEMENT CODE
 *	========================================================================
 *	This code deals with various file system related subjects. This includes
 *	making directories and finding where the executable is, etc.
 */

strList_t g_fs_dirstack = (strList_t)0;

/* initialize file system */
void FS_Init() {
	atexit(FS_Fini);
}

/* deinitialize file system */
void FS_Fini() {
	SL_Delete(g_fs_dirstack);
	g_fs_dirstack = (strList_t)0;
}

/* retrieve the current directory */
char *FS_GetCWD(char *cwd, size_t n) {
	char *p;

	ASSERT( cwd != (char *)0 );
	ASSERT( n > 1 );

#if MK_VC_VER
	if( !(p = _getcwd(cwd, n)) ) {
		Log_FatalError("getcwd() failed");
	}
#else
	if( !(p = getcwd(cwd, n)) ) {
		Log_FatalError("getcwd() failed");
	}
#endif

#if MK_WINDOWS_ENABLED
	while( (p=strchr(p,'\\')) != (char *)0 ) {
		*p = '/';
	}
#endif

	p = strrchr(cwd, '/');
	if( (p && *(p + 1) != '\0') || !p ) {
		Com_Strcat(cwd, n, "/");
	}

	return cwd;
}

/* enter a directory (uses directory stack) */
int FS_Enter(const char *path) {
	char cwd[PATH_MAX];
	
	ASSERT( path != ( const char * )0 );
	
	Dbg_Enter("FS_Enter(\"%s\")", path);

	FS_GetCWD(cwd, sizeof(cwd));

	if( !g_fs_dirstack ) {
		g_fs_dirstack = SL_New();
	}

	SL_PushBack(g_fs_dirstack, cwd);

	if( chdir(path)==-1 ) {
		Log_ErrorMsg(Com_Va("chdir(^F\"%s\"^&) failed", path));
		if( chdir(cwd)==-1 ) {
			Log_FatalError("failed to restore current directory");
		}

		return 0;
	}

	return 1;
}

/* exit the current directory (uses directory stack) */
void FS_Leave() {
	size_t i;

	ASSERT( g_fs_dirstack != (strList_t)0 );
	ASSERT( SL_Size(g_fs_dirstack) > 0 );
	
	Dbg_Leave();

	i = SL_Size(g_fs_dirstack)-1;

	if( chdir(SL_Get(g_fs_dirstack, i))==-1 ) {
		Log_FatalError(Com_Va("chdir(\"%s\") failed",
			SL_Get(g_fs_dirstack, i)));
	}

	SL_Set(g_fs_dirstack, i, (const char *)0);
	SL_Resize(g_fs_dirstack, i);
}

/* determine whether the path specified is a file. */
int FS_IsFile(const char *path) {
	fs_stat_t s;

	if( stat(path, &s)==-1 ) {
		return 0;
	}

	if( ~s.st_mode & S_IFREG ) {
		errno = s.st_mode & S_IFDIR ? EISDIR : EBADF;
		return 0;
	}

	return 1;
}

/* determine whether the path specified is a directory. */
int FS_IsDir(const char *path) {
	static char temp[PATH_MAX];
	fs_stat_t s;
	const char *p;

	p = strrchr(path, '/');
	if( p && *(p + 1)=='\0' ) {
		Com_Strncpy(temp, sizeof(temp), path, p - path);
		temp[p - path] = '\0';
		p = &temp[0];
	} else {
		p = path;
	}

	if( stat(p, &s)==-1 ) {
		return 0;
	}

	if( ~s.st_mode & S_IFDIR ) {
		errno = ENOTDIR;
		return 0;
	}

	return 1;
}

/* create a series of directories (e.g., a/b/c/d/...) */
void FS_MkDirs(const char *dirs) {
	/*
	 *	! This is old code !
	 *	Just ignore bad practices, mmkay?
	 */
	const char *p;
	char buf[PATH_MAX], *path;
	int ishidden;

	/* ignore the root directory */
	if( dirs[0]=='/' ) {
		buf[0] = dirs[0];
		path = &buf[1];
		p = &dirs[1];
	} else if(dirs[1]==':' && dirs[2]=='/') {
		buf[0] = dirs[0];
		buf[1] = dirs[1];
		buf[2] = dirs[2];
		path = &buf[3];
		p = &dirs[3];
	} else {
		path = &buf[0];
		p = &dirs[0];
	}
	
	/* not hidden unless path component begins with '.' */
	ishidden = ( int )( dirs[0] == '.' );

	/* make each directory, one by one */
	while( 1 ) {
		if(*p=='/' || *p==0) {
			*path = 0;

			errno = 0;
#ifdef _WIN32
			mkdir(buf);
#else
			mkdir(buf, 0740);
#endif
			if( errno && errno!=EEXIST ) {
				Log_FatalError(Com_Va("couldn't create directory \"%s\"", buf));
			}
			
			if( ishidden ) {
				SetFileAttributesA(buf, FILE_ATTRIBUTE_HIDDEN);
			}
			
			if( p[0] == '/' && p[1] == '.' ) {
				ishidden = 1;
			} else {
				ishidden = 0;
			}

			if( !(*path++ = *p++) ) {
				return;
			} else if(*p=='\0') { /* handle a '/' ending */
				return;
			}
		} else {
			*path++ = *p++;
		}

		if( path==&buf[sizeof(buf)-1] )
			Log_FatalError("path is too long");
	}
}

/* find the real path to a file */
#ifdef _WIN32
char *FS_RealPath(const char *filename, char *resolvedname, size_t maxn) {
	static char buf[PATH_MAX];
	size_t i;
	DWORD r;

	if( !(r = GetFullPathNameA(filename, sizeof(buf), buf, (char **)0)) ) {
		errno = ENOSYS;
		return (char *)0;
	} else if(r >= (DWORD)maxn) {
		errno = ERANGE;
		return (char *)0;
	}

	for( i=0; i<sizeof(buf); i++ ) {
		if( !buf[i] ) {
			break;
		}

		if( buf[i]=='\\' ) {
			buf[i] = '/';
		}
	}

	if( buf[1]==':' ) {
		if( buf[0]>='A' && buf[0]<='Z' ) {
			buf[0] = buf[0] - 'A' + 'a';
		}
	}

	strncpy(resolvedname, buf, maxn-1);
	resolvedname[maxn-1] = 0;

	return resolvedname;
}
#else /*#elif __linux__||__linux||linux*/
char *FS_RealPath(const char *filename, char *resolvedname, size_t maxn) {
	static char buf[PATH_MAX + 1];

	if( maxn > PATH_MAX ) {
		if( !realpath(filename, resolvedname) ) {
			return (char *)0;
		}

		resolvedname[PATH_MAX] = 0;
		return resolvedname;
	}

	if( !realpath(filename, buf) ) {
		return (char *)0;
	}

	buf[PATH_MAX] = 0;
	strncpy(resolvedname, buf, maxn);

	if( FS_IsDir(resolvedname) ) {
		Com_Strcat(resolvedname, maxn, "/");
	}

	resolvedname[maxn-1] = 0;
	return resolvedname;
}
#endif

DIR *FS_OpenDir(const char *path) {
	static char buf[PATH_MAX];
	const char *p, *usepath;
	DIR *d;

	Dbg_Outf("FS_OpenDir(\"%s\")\n", path);

	p = strrchr(path, '/');
	if( p && *(p + 1) == '\0' ) {
		Com_Strncpy(buf, sizeof(buf), path, (size_t)(p - path));
		usepath = buf;
	} else {
		usepath = path;
	}

	if( !( d = opendir(usepath) ) ) {
		const char *es;
		int e;
		
		e = errno;
		es = strerror( e );

		Dbg_Outf( "\tKO: %i (%s)\n", e, es );
		return ( DIR * )0;
	}
	
	Dbg_Outf( "\tOK: #%x\n", (unsigned)(size_t)d );
	return d;
}
DIR *FS_CloseDir(DIR *p) {
	int e;
	
	e = errno;
	Dbg_Outf( "FS_CloseDir(#%x)\n", (unsigned)(size_t)p );
	if( p != ( DIR * )0 ) {
		closedir( p );
	}
	errno = e;
	
	return ( DIR * )0;
}
struct dirent *FS_ReadDir(DIR *d) {
	struct dirent *dp;

	/* Okay... how is errno getting set? From what? */
	if( errno ) {
		Log_Error(NULL, 0, "FS_ReadDir", NULL);
	}

	/* TEMP-FIX: buggy version of readdir() sets errno? */
	dp = readdir(d);
	if( dp != ( struct dirent * )0 || errno == ENOTDIR ) {
		errno = 0;
	}

	return dp;
}

void FS_Delete(const char *path) {
	if( FS_IsDir( path ) ) {
		DIR *d;
		struct dirent *dp;
		char *filepath;
		char *filepath_namepart;

		filepath = ( char * )Com_Memory( NULL, PATH_MAX );
		Com_Strcpy( filepath, PATH_MAX, path );

		filepath_namepart = strchr( filepath, '\0' );
		if( filepath_namepart > filepath ) {
			char dirsep;
			
			dirsep = *( filepath_namepart - 1 );
			if( dirsep != '/' && dirsep != '\\' ) {
				*filepath_namepart++ = '/';
			}
		}

		d = FS_OpenDir( path );
		while( ( dp = FS_ReadDir( d ) ) != NULL ) {
			if( strcmp( dp->d_name, "." ) == 0 ) {
				continue;
			}
			if( strcmp( dp->d_name, ".." ) == 0 ) {
				continue;
			}

			Com_Strcpy( filepath_namepart,
				PATH_MAX - ( size_t )filepath_namepart, dp->d_name );
			FS_Delete( filepath );
		}
		FS_CloseDir(d);
		
		filepath = ( char * )Com_Memory( ( void * )filepath, 0 );
		
		Dbg_Outf( "Deleting directory \"%s\"...\n", path );
		rmdir( path );
	} else {
		errno = 0;
		
		Dbg_Outf( "Deleting file \"%s\"...\n", path );
		remove( path );
	}
}

/*
 *	=======================================================================
 *	GIT VERSION CONTROL INFO
 *	=======================================================================
 *	Retrieve information about the git repo from the perspective of the
 *	current directory. Header information can then be written to a
 *	gitinfo.h file that the project can include.
 *
 *	e.g., a project might include the gitinfo.h in one of their version
 *	`     source files (such as for an "about box" or debug logging) as
 *	`     follows:
 *
 *		#include <build-generated/gitinfo.h>
 *
 *	The file will define the following macros, if git is used:
 *
 *		BUILDGEN_GITINFO_BRANCH (e.g., "master")
 *		BUILDGEN_GITINFO_COMMIT (e.g., "9d4bf863ed1ea878fde62756eea2c6e85fc7a6de")
 *		BUILDGEN_GITINFO_TSTAMP (e.g., "2016-01-10 16:47:11")
 *
 *	If git is used, BUILDGEN_GITINFO_AVAILABLE will be defined to 1,
 *	otherwise it will be defined to 0.
 *
 *	It is planned to expand this in the future to also grab the tag of the
 *	branch, if any, which may be used for versioning. (As Swift's packages
 *	use.) Then, should zip/tar file generation be supported, said files can
 *	also use the version they're defined against.
 */
 
struct gitinfo_s
{
	char *pszBranchFile;
	char *pszCommit;
	char *pszTimestamp;
};

/* find the nearest ".git" directory at or below the current path */
char *Git_FindPath( void )
{
	/* szDir always ends with '/'; szGitDir is szDir + ".git" */
	char szDir[ PATH_MAX ], szGitDir[ PATH_MAX + 8 ];
	/* pszDirPos always points to the last '/' in szDir */
	char *pszDirPos;

	/* start searching from the current directory */
	if( !FS_GetCWD( szDir, ( int )sizeof( szDir ) - 1 ) ) {
		return ( char * )0;
	}

	/* find the nul terminator */
	pszDirPos = strchr( szDir, '\0' );

	/* quick sanity check */
	if( pszDirPos == &szDir[ 0 ] || !pszDirPos ) {
		Dbg_Outf( "Git_FindPath: FS_GetCWD returned an empty directory path\n" );
		return ( char * )0;
	}

	/* `szDir` must end with '/' and `pszDirPos` must point to the last '/' */
	if( *( pszDirPos - 1 ) != '/' ) {
		pszDirPos[ 0 ] = '/';
		pszDirPos[ 1 ] = '\0';
	} else {
		--pszDirPos;
	}
	
	Dbg_Outf( "Searching for .git directory, starting from: <%s>\n", szDir );

	/* find the .git directory (if successful, this part returns) */
	do {
		/* construct the .git path name */
		Com_Strcpy( szGitDir, sizeof( szGitDir ), szDir );
		Com_Strcat( szGitDir, sizeof( szGitDir ), ".git" );
		Dbg_Outf( "\tTrying: <%s>\n", szGitDir );
		
		/* if this is a valid .git directory, then done */
		if( FS_IsDir( szGitDir ) ) {
			Dbg_Outf( "\t\tFound!\n" );
			return Com_Dup( ( char * )0, szGitDir );
		}
		
		/* find the beginning of the next directory up */
		while( pszDirPos-- > &szDir[ 0 ] ) {
			if( *pszDirPos == '/' ) {
				pszDirPos[ 1 ] = '\0';
				break;
			}
		}
	} while( pszDirPos != &szDir[ 0 ] );
	
	/* no .git directory found */
	Dbg_Outf( "\tNo .git found\n" );
	return ( char * )0;
}
/* find the file belonging to the current branch of the given .git directory */
char *Git_GetBranchPath( const char *pszGitDir )
{
	static const char szRefPrefix[] = "ref: ";
	static const size_t cRefPrefix = sizeof( szRefPrefix ) - 1;
	buffer_t buf;
	char szHeadFile[ PATH_MAX ];
	char szRefLine[ 512 ];
	
	ASSERT( pszGitDir != ( const char * )0 && *pszGitDir != '\0' );
	
	Com_Strcpy( szHeadFile, sizeof( szHeadFile ), pszGitDir );
	Com_Strcat( szHeadFile, sizeof( szHeadFile ), "/HEAD" );

	Dbg_Outf( "Git_GetBranchPath: %s: <%s>\n", pszGitDir, szHeadFile );	
	if( !( buf = Buf_LoadFile( szHeadFile ) ) ) {
		return ( char * )0;
	}
	
	Buf_ReadLine( buf, szRefLine, sizeof( szRefLine ) );
	Dbg_Outf( "Git_GetBranchPath: %s: <%s>\n", szHeadFile, szRefLine );
	Buf_Unload( buf );
	
	if( strncmp( szRefLine, szRefPrefix, ( int )( unsigned )cRefPrefix ) != 0 ) {
		Dbg_Outf( "%s: first line does not begin with '%s'\n", szHeadFile, szRefPrefix );
		return ( char * )0;
	}
	
	return Com_Dup( ( char * )0, Com_Va( "%s/%s", pszGitDir, &szRefLine[ cRefPrefix ] ) );
}
/* get the name of the latest commit from the given branch file */
char *Git_GetCommit( const char *pszBranchFile )
{
	buffer_t buf;
	char szCommitLine[ 512 ];
	
	ASSERT( pszBranchFile != ( const char * )0 && *pszBranchFile != '\0' );
	
	if( !( buf = Buf_LoadFile( pszBranchFile ) ) ) {
		return ( char * )0;
	}
	
	Buf_ReadLine( buf, szCommitLine, sizeof( szCommitLine ) );
	Dbg_Outf( "Git_GetCommit(\"%s\"): <%s>\n", pszBranchFile, szCommitLine );
	Buf_Unload( buf );
	
	return Com_Dup( ( char * )0, szCommitLine );
}
/* get the timestamp of the latest commit from the given branch file */
char *Git_GetCommitDate( const char *pszBranchFile, const fs_stat_t *pStat )
{
	fs_stat_t s;
	struct tm *t;
	char szBuf[ 128 ];
	
	ASSERT( pszBranchFile != ( const char * )0 && *pszBranchFile != '\0' );
	
	if( pStat != ( const fs_stat_t * )0 ) {
		s = *pStat;
	} else if( stat( pszBranchFile, &s ) != 0 ) {
		Log_Error( pszBranchFile, 0, ( const char * )0, "Stat failed" );
		return ( char * )0;
	}

	if( !( t = localtime( &s.st_mtime ) ) ) {
		Log_Error( pszBranchFile, 0, ( const char * )0, "Could not determine time from stat" );
		return ( char * )0;
	}

	strftime( szBuf, sizeof( szBuf ), "%Y-%m-%d %H:%M:%S", t );
	Dbg_Outf( "Git_GetCommitDate(\"%s\"): %s\n", pszBranchFile, szBuf );
	return Com_Dup( ( char * )0, szBuf );
}

/* load gitinfo from the cache or generate it (NOTE: expects to be in project's source directory) */
gitinfo_t Git_LoadInfo( void )
{
	static const char *const pszHeader = "mk-gitinfo-cache::v1";
	static const char *const pszHeaderUnavailable = "mk-gitinfo-cache::unavailable";
	static const char szPrefixGitBranchFile[] = "git-branch-file:";
	static const size_t cPrefixGitBranchFile = sizeof( szPrefixGitBranchFile ) - 1;
	static const char *const pszTailer = "mk-gitinfo-cache::end";
			
	gitinfo_t pGitInfo;
	buffer_t buf;
	FILE *pCacheFile;
	char *pszGitPath;
	char szGitCacheFile[ PATH_MAX ];
	char szLine[ 512 ];
	
	if( !( pGitInfo = ( gitinfo_t )Mem_Alloc( sizeof( *pGitInfo ) ) ) ) {
		return ( gitinfo_t )0;
	}
	
	/* construct the name of the cache file */
	Com_Strcpy( szGitCacheFile, sizeof( szGitCacheFile ), Com_Va( "%s/cache.gitinfo", Opt_GetBuildGenPath() ) );
	Dbg_Outf( "Git_LoadInfo: cache-file: %s\n", szGitCacheFile );
	
	/* load the cache file */
	if( ( buf = Buf_LoadFile( szGitCacheFile ) ) != ( buffer_t )0 ) {
		/* using `do/while(0)` for ability to `break` on error */
		do {
			char *pszGitBranchFile;
			
			/* check the header for the proper version (if not then we need to regenerate) */
			Buf_ReadLine( buf, szLine, sizeof( szLine ) );
			Dbg_Outf( "\t%s\n", szLine );
			if( strcmp( szLine, pszHeader ) != 0 ) {
				if( strcmp( szLine, pszHeaderUnavailable ) == 0 ) {
					Mem_Dealloc( ( void * )pGitInfo );
					return ( gitinfo_t )0;
				}
				Dbg_Outf( "%s: expected \"%s\" on line 1; regenerating\n", szGitCacheFile, pszHeader );
				break;
			}
			
			/* grab the branch file name */
			Buf_ReadLine( buf, szLine, sizeof( szLine ) );
			Dbg_Outf( "\t%s\n", szLine );
			if( strncmp( szLine, szPrefixGitBranchFile, cPrefixGitBranchFile ) != 0 ) {
				Dbg_Outf( "%s: expected \"%s\" on line 2; regenerating\n", szGitCacheFile, szPrefixGitBranchFile );
				break;
			}
			
			/* pszGitBranchFile guaranteed to be not null from `Com_Dup` */
			pszGitBranchFile = Com_Dup( ( char * )0, &szLine[ cPrefixGitBranchFile ] );
			Dbg_Outf( "\t\t<%s>\n", pszGitBranchFile );
			
			/* find the commit name */
			if( !( pGitInfo->pszCommit = Git_GetCommit( pszGitBranchFile ) ) ) {
				pszGitBranchFile = ( char * )Mem_Dealloc( ( void * )pszGitBranchFile );
				break;
			}
			
			/* grab the timestamp for the commit */
			if( !( pGitInfo->pszTimestamp = Git_GetCommitDate( pszGitBranchFile, ( const fs_stat_t * )0 ) ) ) {
				pGitInfo->pszCommit = ( char * )Mem_Dealloc( ( void * )pGitInfo->pszCommit );
				pszGitBranchFile = ( char * )Mem_Dealloc( ( void * )pszGitBranchFile );
				break;
			}
			
			/* store the branch file to indicate success */
			pGitInfo->pszBranchFile = pszGitBranchFile;
		} while( 0 );
		Buf_Unload( buf );
		
		/* return if we successfully filled the structure (if pszBranchFile is set, assume filled) */
		if( pGitInfo->pszBranchFile != ( char * )0 ) {
			return pGitInfo;
		}
	} else {
		Dbg_Outf( "\tCould not load cache file\n" );
	}
	
	if( !( pCacheFile = fopen( szGitCacheFile, "wb" ) ) ) {
		Log_Error( szGitCacheFile, 0, ( const char * )0, "Failed to open gitinfo cache file for writing" );
	}

	/* using `do/while(0)` for easy `break` (to avoid code duplication) */
	do {
		/* find the git path */
		if( !( pszGitPath = Git_FindPath() ) ) {
			break;
		}
		
		/* find the git branch file */
		if( !( pGitInfo->pszBranchFile = Git_GetBranchPath( pszGitPath ) ) ) {
			pszGitPath = ( char * )Mem_Dealloc( ( void * )pszGitPath );
			break;
		}
		
		/* get the commit for the branch */
		if( !( pGitInfo->pszCommit = Git_GetCommit( pGitInfo->pszBranchFile ) ) ) {
			pGitInfo->pszBranchFile = ( char * )Mem_Dealloc( ( void * )pGitInfo->pszBranchFile );
			pszGitPath = ( char * )Mem_Dealloc( ( void * )pszGitPath );
			break;
		}
		
		/* get the timestamp for the branch */
		if( !( pGitInfo->pszTimestamp = Git_GetCommitDate( pGitInfo->pszBranchFile, ( const fs_stat_t * )0 ) ) ) {
			pGitInfo->pszCommit = ( char * )Mem_Dealloc( ( void * )pGitInfo->pszCommit );
			pGitInfo->pszBranchFile = ( char * )Mem_Dealloc( ( void * )pGitInfo->pszBranchFile );
			pszGitPath = ( char * )Mem_Dealloc( ( void * )pszGitPath );
			break;
		}
		
		/* write the results to the debug log */
		Dbg_Outf( "Caching new gitinfo ***\n" );
		Dbg_Outf( "\tbranch: %s\n", pGitInfo->pszBranchFile );
		Dbg_Outf( "\tcommit: %s\n", pGitInfo->pszCommit );
		Dbg_Outf( "\ttstamp: %s\n", pGitInfo->pszTimestamp );
		Dbg_Outf( "\n" );
		
		/* cache the branch file */
		if( pCacheFile != ( FILE * )0 ) {
			fprintf( pCacheFile, "%s\r\n", pszHeader );
			fprintf( pCacheFile, "%s%s\r\n", szPrefixGitBranchFile, pGitInfo->pszBranchFile );
			fprintf( pCacheFile, "%s\r\n", pszTailer );

			pCacheFile = ( fclose( pCacheFile ), ( FILE * )0 );
		}
		
		/* done */
		return pGitInfo;
	} while( 0 );

	/* if this point is reached, we failed; free the gitinfo */
	Mem_Dealloc( ( void * )pGitInfo );
	
	/* write the "unavailable" cache file */
	if( pCacheFile != ( FILE * )0 ) {
		fprintf( pCacheFile, "%s\r\n", pszHeaderUnavailable );
		pCacheFile = ( fclose( pCacheFile ), ( FILE * )0 );
	}
	
	/* done */
	return ( gitinfo_t )0;
}
/* write out the header containing git info of the given commit and timestamp */
int Git_WriteHeader( const char *pszHFilename, gitinfo_t pGitInfo )
{
	fs_stat_t branchStat, headerStat;
	const char *pszBranchFile;
	const char *pszCommit;
	const char *pszTimestamp;
	
	const char *pszBranchName;
	FILE *fp;
	
	ASSERT( pszHFilename != ( const char * )0 && *pszHFilename != '\0' );

	if( pGitInfo != ( gitinfo_t )0 ) {
		pszBranchFile = pGitInfo->pszBranchFile;
		ASSERT( pszBranchFile != ( const char * )0 && *pszBranchFile != '\0' );
	
		pszCommit = pGitInfo->pszCommit;
		ASSERT( pszCommit != ( const char * )0 && *pszCommit != '\0' );
		
		pszTimestamp = pGitInfo->pszTimestamp;
		ASSERT( pszTimestamp != ( const char * )0 && *pszTimestamp != '\0' );
		
		if( !( pszBranchName = strrchr( pszBranchFile, '/' ) ) ) {
			Dbg_Outf( "Git_WriteHeader: Invalid branch filename \"%s\"", pszBranchFile );
			return 0;
		}

		/* eat the leading '/' (instead of "/master" we want "master") */
		++pszBranchName;
		
		/* early exit if we don't need a new file */
		if( stat( pszBranchFile, &branchStat ) == 0 && stat( pszHFilename, &headerStat ) == 0 && branchStat.st_mtime < headerStat.st_mtime ) {
			/* file already made; don't modify */
			return 1;
		}
		
		if( !( fp = fopen( pszHFilename, "wb" ) ) ) {
			Log_Error( pszHFilename, 0, ( const char * )0, "Failed to open file for writing" );
			return 0;
		}
		
		fprintf( fp, "/* This file is generated automatically; edit at your own risk! */\r\n" );
		fprintf( fp, "#ifndef BUILDGEN_GITINFO_H__%s\r\n", pszCommit );
		fprintf( fp, "#define BUILDGEN_GITINFO_H__%s\r\n", pszCommit );
		fprintf( fp, "\r\n" );
		fprintf( fp, "#if defined( _MSC_VER ) || defined( __GNUC__ ) || defined( __clang__ )\r\n" );
		fprintf( fp, "# pragma once\r\n" );
		fprintf( fp, "#endif\r\n" );
		fprintf( fp, "\r\n" );
		fprintf( fp, "#define BUILDGEN_GITINFO_AVAILABLE 1\r\n" );
		fprintf( fp, "\r\n" );
		fprintf( fp, "#define BUILDGEN_GITINFO_BRANCH \"%s\"\r\n", pszBranchName );
		fprintf( fp, "#define BUILDGEN_GITINFO_COMMIT \"%s\"\r\n", pszCommit );
		fprintf( fp, "#define BUILDGEN_GITINFO_TSTAMP \"%s\"\r\n", pszTimestamp );
		fprintf( fp, "\r\n" );
		fprintf( fp, "#endif /* BUILDGEN_GITINFO_H */\r\n" );
		
		fclose( fp );
		return 1;
	}
	
	/* if the file exists then just return now, assuming it's already set to "unavailable" */
	if( FS_IsFile( pszHFilename ) ) {
		/* FIXME: Would be better to compare against the gitinfo cache file */
		return 1;
	}
	
	if( !( fp = fopen( pszHFilename, "wb" ) ) ) {
		Log_Error( pszHFilename, 0, ( const char * )0, "Failed to open file for writing" );
		return 0;
	}
	
	fprintf( fp, "/* This file is generated automatically; edit at your own risk! */\r\n" );
	fprintf( fp, "#ifndef BUILDGEN_GITINFO_H__\r\n" );
	fprintf( fp, "#define BUILDGEN_GITINFO_H__\r\n" );
	fprintf( fp, "\r\n" );
	fprintf( fp, "#if defined( _MSC_VER ) || defined( __GNUC__ ) || defined( __clang__ )\r\n" );
	fprintf( fp, "# pragma once\r\n" );
	fprintf( fp, "#endif\r\n" );
	fprintf( fp, "\r\n" );
	fprintf( fp, "#define BUILDGEN_GITINFO_AVAILABLE 0\r\n" );
	fprintf( fp, "\r\n" );
	fprintf( fp, "#endif\r\n" );
	
	fclose( fp );
	return 1;
}

/* generate the gitinfo header if necessary */
int Git_GenerateInfo( void )
{
	gitinfo_t pGitInfo;
	char szHeaderDir[ PATH_MAX ];
	char szHeaderFile[ PATH_MAX ];
	int r;
	
	Com_Strcpy( szHeaderDir, sizeof( szHeaderDir ), Opt_GetBuildGenIncDir() );
	Com_Strcpy( szHeaderFile, sizeof( szHeaderFile ), Com_Va( "%s/gitinfo.h", szHeaderDir ) );
	
	FS_MkDirs( szHeaderDir );
	
	pGitInfo = Git_LoadInfo();
	r = Git_WriteHeader( szHeaderFile, pGitInfo );
	pGitInfo = ( gitinfo_t )Mem_Dealloc( ( void * )pGitInfo );
	
	return r;
}

/*
 *	========================================================================
 *	DEPENDENCY TRACKER
 *	========================================================================
 *	Track dependencies and manage the general structure.
 */

struct dep_s {
	char *name;
	strList_t deps;

	struct dep_s *prev, *next;
};
dep_t g_dep_head = (dep_t)0;
dep_t g_dep_tail = (dep_t)0;

#if !MK_DEBUG_ENABLED
# undef  MK_DEBUG_DEPENDENCY_TRACKER_ENABLED
# define MK_DEBUG_DEPENDENCY_TRACKER_ENABLED 0
#endif

/* create a new dependency list */
dep_t Dep_New(const char *name) {
	dep_t dep;

	ASSERT( name != (const char *)0 );

	dep = (dep_t)Com_Memory((void *)0, sizeof(*dep));

	dep->name = Com_Strdup(name);
	dep->deps = SL_New();

	dep->next = (dep_t)0;
	if( (dep->prev = g_dep_tail) != (dep_t)0 ) {
		g_dep_tail->next = dep;
	} else {
		g_dep_head = dep;
	}
	g_dep_tail = dep;

	return dep;
}

/* delete a dependency list */
void Dep_Delete(dep_t dep) {
	if( !dep ) {
		return;
	}

	dep->name = (char *)Com_Memory((void *)dep->name, 0);
	SL_Delete(dep->deps);
	dep->deps = (strList_t)0;

	if( dep->prev ) {
		dep->prev->next = dep->next;
	}
	if( dep->next ) {
		dep->next->prev = dep->prev;
	}

	if( g_dep_head==dep ) {
		g_dep_head = dep->next;
	}
	if( g_dep_tail==dep ) {
		g_dep_tail = dep->prev;
	}

	Com_Memory((void *)dep, 0);
}

/* delete all dependency lists */
void Dep_DeleteAll() {
	while( g_dep_head ) {
		Dep_Delete(g_dep_head);
	}
}

/* retrieve the name of the file a dependency list is tracking */
const char *Dep_GetFile(dep_t dep) {
	ASSERT( dep != (dep_t)0 );

	return dep->name;
}

/* add a dependency to a list */
void Dep_Push(dep_t dep, const char *name) {
	ASSERT( dep != (dep_t)0 );
	ASSERT( name != (const char *)0 );

	SL_PushBack(dep->deps, name);
#if MK_DEBUG_DEPENDENCY_TRACKER_ENABLED
	Dbg_Outf("~ Dep_Push \"%s\": \"%s\";\n", dep->name, name);
#endif
}

/* retrieve the number of dependencies in a list */
size_t Dep_Size(dep_t dep) {
	ASSERT( dep != (dep_t)0 );

	return SL_Size(dep->deps);
}

/* retrieve a dependency from a list */
const char *Dep_Get(dep_t dep, size_t i) {
	ASSERT( dep != (dep_t)0 );
	ASSERT( i < Dep_Size(dep) );

	return SL_Get(dep->deps, i);
}

/* find a dependency */
dep_t Dep_Find(const char *name) {
	dep_t dep;

	for( dep=g_dep_head; dep; dep=dep->next ) {
		if( !strcmp(dep->name, name) ) {
			return dep;
		}
	}

	return (dep_t)0;
}

/* print all known dependencies */
void Dep_DebugPrintAll(void) {
#if MK_DEBUG_DEPENDENCY_TRACKER_ENABLED
	dep_t dep;

	for( dep=g_dep_head; dep; dep=dep->next ) {
		Dbg_Outf( " ** dep: \"%s\"\n", dep->name );
	}
#endif
}

/*
 *	========================================================================
 *	DEPENDENCY READER
 *	========================================================================
 *	Read dependencies, as produced by GCC, and put them into an array.
 */

enum {
	kDepTok_EOF = 0,
	kDepTok_Colon = ':',

	__kDepTok_Start__ = 256,
	kDepTok_Ident
};

/* internal function ported from previous version of 'Dep_Load' */
char DepF_Read(buffer_t buf, char *cur, char *look) {
	/*
	 *	! This is based on old code.
	 */

	ASSERT( buf != (buffer_t)0 );
	ASSERT( cur != (char *)0 );
	ASSERT( look != (char *)0 );

	*cur = Buf_Read(buf);
	*look = Buf_Peek(buf);

	return *cur;
}

/* read a token from the buffer */
int DepF_Lex(buffer_t buf, char *dst, size_t dstn, char *cur, char *look) {
	size_t i;

	ASSERT( buf != (buffer_t)0 );
	ASSERT( dst != (char *)0 );
	ASSERT( dstn > 1 );

	i = 0;
	dst[0] = 0;

	if( !*cur ) {
		do {
			if( !DepF_Read(buf, cur, look) ) {
				return kDepTok_EOF;
			}
		} while( *cur <= ' ' );
	}

	if( *cur==':' ) {
		dst[0] = ':';
		dst[1] = 0;

		*cur = 0; /* force 'cur' to be retrieved again, else infinite loop */

		return kDepTok_Colon;
	}

	while( 1 ) {
		if( *cur=='\\' ) {
			/*
			 *	! Original code expected that '\r' was possible, hence this
			 *	  setup was needed to make this portion easier. Needs to be
			 *	  fixed later.
			 */
			if( *look=='\n' ) {
				do {
					if( !DepF_Read(buf, cur, look) ) {
						break;
					}
				} while( *cur <= ' ' );

				continue;
			}

			if( *look==' ' ) {
				dst[i++] = DepF_Read(buf, cur, look);
				if( i==dstn ) {
					Buf_Error(buf, "overflow detected");
					exit(EXIT_FAILURE);
				}
				continue;
			}

			*cur = '/'; /* correct path reads */
		} else if(*cur==':') {
			if( *look<=' ' ) {
				dst[i] = 0;
				return kDepTok_Ident; /* leave 'cur' for the next call */
			}
		} else if(*cur<=' ') {
			dst[i] = 0;
			*cur = 0; /* force read on next call */

			return kDepTok_Ident;
		}

		dst[i++] = *cur;
		if( i==dstn ) {
			Buf_Error(buf, "overflow detected");
			exit(EXIT_FAILURE);
		}

		DepF_Read(buf, cur, look);
	}
}

/* read dependencies from a file */
int Dep_Load(const char *filename) {
	buffer_t buf;
	dep_t dep;
	char lexan[PATH_MAX], ident[PATH_MAX], cur, look;
	int tok;

	ident[0] = '\0';
	lexan[0] = '\0';
	cur = 0;
	look = 0;

	buf = Buf_LoadFile(filename);
	if( !buf ) {
		return 0;
	}

	DepF_Read(buf, &cur, &look);

	dep = (dep_t)0;

	do {
		tok = DepF_Lex(buf, lexan, sizeof(lexan), &cur, &look);
#if 0
		Dbg_Outf( "%s(%i): %i: \"%s\"\n", Buf_GetFileName(buf),
			(int)Buf_GetLine(buf), tok, lexan );
#endif

		if( tok==kDepTok_Colon ) {
			if( ident[0] ) {
				dep = Dep_New(ident);
				ident[0] = 0;
			}

			continue;
		}

		if( dep && ident[0] ) {
			Dep_Push(dep, ident);
			ident[0] = 0;
		}

		if( tok==kDepTok_Ident ) {
			Com_Strcpy(ident, sizeof(ident), lexan);
		}
	} while( tok != kDepTok_EOF );

	Buf_Unload(buf);
	return 1;
}

/*
 *	========================================================================
 *	LIBRARIAN
 *	========================================================================
 *	Manages individual libraries. Each library is abstracted with a platform
 *	independent name (e.g., "opengl") which is then mapped to platform
 *	dependent flags. (e.g., on Windows the above would be "-lopengl32," but on
 *	GNU/Linux it would be "-lGL.")
 */

enum {
	kLib_Processed_Bit = 0x01 /* indicates a library has been "processed" */
};

struct lib_s {
	size_t name_n;
	char *name;
	char *flags[kNumProjSys];

	project_t proj;

	bitfield_t config;

	struct lib_s *prev, *next;
};

struct lib_s *g_lib_head = (struct lib_s *)0;
struct lib_s *g_lib_tail = (struct lib_s *)0;

/* create a new library */
lib_t Lib_New() {
	size_t i;
	lib_t lib;

	lib = (lib_t)Com_Memory((void *)0, sizeof(*lib));

	lib->name = (char *)0;
	for( i=0; i<sizeof(lib->flags)/sizeof(lib->flags[0]); i++ ) {
		lib->flags[i] = (char *)0;
	}

	lib->proj = (project_t)0;

	lib->config = 0;

	lib->next = (struct lib_s *)0;
	if( (lib->prev = g_lib_tail) != (struct lib_s *)0 ) {
		g_lib_tail->next = lib;
	} else {
		g_lib_head = lib;
	}
	g_lib_tail = lib;

	return lib;
}

/* delete an existing library */
void Lib_Delete(lib_t lib) {
	size_t i;

	if( !lib ) {
		return;
	}

	lib->name_n = 0;
	lib->name = (char *)Com_Memory((void *)lib->name, 0);
	for( i=0; i<sizeof(lib->flags)/sizeof(lib->flags[0]); i++ ) {
		lib->flags[i] = (char *)Com_Memory((void *)lib->flags[i], 0);
	}

	if( lib->prev ) {
		lib->prev->next = lib->next;
	}
	if( lib->next ) {
		lib->next->prev = lib->prev;
	}

	if( g_lib_head==lib ) {
		g_lib_head = lib->next;
	}
	if( g_lib_tail==lib ) {
		g_lib_tail = lib->prev;
	}

	Com_Memory((void *)lib, 0);
}

/* delete all existing libraries */
void Lib_DeleteAll() {
	while( g_lib_head )
		Lib_Delete(g_lib_head);
}

/* set the name of a library */
void Lib_SetName(lib_t lib, const char *name) {
	ASSERT( lib != (lib_t)0 );

	lib->name_n = name!=(const char *)0 ? strlen(name) : 0;
	lib->name = Com_Dup(lib->name, name);
}

/* set the flags of a library */
void Lib_SetFlags(lib_t lib, int sys, const char *flags) {
	ASSERT( lib != (lib_t)0 );
	ASSERT( sys>=0 && sys<kNumProjSys );

	lib->flags[sys] = Com_Dup(lib->flags[sys], flags);
}

/* retrieve the name of a library */
const char *Lib_Name(lib_t lib) {
	ASSERT( lib != (lib_t)0 );

	return lib->name;
}

/* retrieve the flags of a library */
const char *Lib_Flags(lib_t lib, int sys) {
	ASSERT( lib != (lib_t)0 );
	ASSERT( sys>=0 && sys<kNumProjSys );

	return lib->flags[sys];
}

/* retrieve the library before another */
lib_t Lib_Prev(lib_t lib) {
	ASSERT( lib != (lib_t)0 );

	return lib->prev;
}

/* retrieve the library after another */
lib_t Lib_Next(lib_t lib) {
	ASSERT( lib != (lib_t)0 );

	return lib->next;
}

/* retrieve the first library */
lib_t Lib_Head() {
	return g_lib_head;
}

/* retrieve the last library */
lib_t Lib_Tail() {
	return g_lib_tail;
}

/* find a library by its name */
lib_t Lib_Find(const char *name) {
	size_t name_n;
	lib_t lib;

	ASSERT( name != (const char *)0 );

	name_n = strlen( name );

	for( lib=g_lib_head; lib; lib=lib->next ) {
		if( !lib->name || name_n != lib->name_n ) {
			continue;
		}

		if( strcmp(lib->name, name)==0 ) {
			return lib;
		}
	}

	return (lib_t)0;
}

/* find or create a library by name */
lib_t Lib_Lookup(const char *name)
{
	lib_t lib;
	
	ASSERT( name != ( const char * )0 );
	
	if( ( lib = Lib_Find( name ) ) != ( lib_t )0 ) {
		return lib;
	}
	
	lib = Lib_New();
	Lib_SetName( lib, name );
	
	return lib;
}

/* mark all libraries as "not processed" */
void Lib_ClearAllProcessed() {
	lib_t lib;

	for( lib=g_lib_head; lib; lib=lib->next ) {
		lib->config &= ~kLib_Processed_Bit;
	}
}

/* mark a library as "not processed" */
void Lib_ClearProcessed(lib_t lib) {
	ASSERT( lib != (lib_t)0 );

	lib->config &= ~kLib_Processed_Bit;
}

/* mark a library as "processed" */
void Lib_SetProcessed(lib_t lib) {
	ASSERT( lib != (lib_t)0 );

	lib->config |= kLib_Processed_Bit;
}

/* determine whether a library was marked as "processed" */
int Lib_IsProcessed(lib_t lib) {
	ASSERT( lib != (lib_t)0 );

	return lib->config & kLib_Processed_Bit ? 1 : 0;
}

/*
 *	========================================================================
 *	AUTOLINK SYSTEM
 *	========================================================================
 *	This system maps which header files are used to access which libraries. This
 *	allows the dependency system to be exploited to reveal what flags need to be
 *	passed to the linker for the system to "just work."
 */

struct autolink_s {
	char *header[kNumProjSys];
	char *lib;

	struct autolink_s *prev, *next;
};
struct autolink_s *g_al_head = (struct autolink_s *)0;
struct autolink_s *g_al_tail = (struct autolink_s *)0;

/* allocate a new auto-link entry */
autolink_t AL_New() {
	autolink_t al;
	size_t i;

	al = (autolink_t)Com_Memory((void *)0, sizeof(*al));

	for( i=0; i<kNumProjSys; i++ ) {
		al->header[i] = (char *)0;
	}
	al->lib = (char *)0;

	al->next = (struct autolink_s *)0;
	if( (al->prev = g_al_tail) != (struct autolink_s *)0 ) {
		g_al_tail->next = al;
	} else {
		g_al_head = al;
	}
	g_al_tail = al;

	return al;
}

/* deallocate an existing auto-link entry */
void AL_Delete(autolink_t al) {
	size_t i;

	if( !al ) {
		return;
	}

	for( i=0; i<kNumProjSys; i++ ) {
		al->header[i] = (char *)Com_Memory((void *)al->header[i], 0);
	}
	al->lib = (char *)Com_Memory((void *)al->lib, 0);

	if( al->prev ) {
		al->prev->next = al->next;
	}
	if( al->next ) {
		al->next->prev = al->prev;
	}

	if( g_al_head==al ) {
		g_al_head = al->next;
	}
	if( g_al_tail==al ) {
		g_al_tail = al->prev;
	}

	Com_Memory((void *)al, 0);
}

/* deallocate all existing auto-link entries */
void AL_DeleteAll() {
	while( g_al_head ) {
		AL_Delete(g_al_head);
	}
}

/* set a header for an auto-link entry (determines whether to auto-link) */
void AL_SetHeader(autolink_t al, int sys, const char *header) {
	static char rel[PATH_MAX];
	const char *p;

	ASSERT( al != (autolink_t)0 );
	ASSERT( sys>=0 && sys<kNumProjSys );

	if( header != (const char *)0 ) {
		Com_RelPathCWD(rel, sizeof(rel), header);
		p = rel;
	} else {
		p = (const char *)0;
	}

	al->header[sys] = Com_Dup(al->header[sys], p);
}

/* set the library an auto-link entry refers to */
void AL_SetLib(autolink_t al, const char *libname) {
	ASSERT( al != (autolink_t)0 );

	al->lib = Com_Dup(al->lib, libname);
}

/* retrieve a header of an auto-link entry */
const char *AL_GetHeader(autolink_t al, int sys) {
	ASSERT( al != (autolink_t)0 );
	ASSERT( sys>=0 && sys<kNumProjSys );

	return al->header[sys];
}

/* retrieve the library an auto-link entry refers to */
const char *AL_GetLib(autolink_t al) {
	ASSERT( al != (autolink_t)0 );

	return al->lib;
}

/* find an auto-link entry by header and system */
autolink_t AL_Find(int sys, const char *header) {
	autolink_t al;

	ASSERT( sys>=0 && sys<kNumProjSys );
	ASSERT( header != (const char *)0 );

	for( al=g_al_head; al; al=al->next ) {
		if( !al->header[sys] ) {
			continue;
		}

		if( Com_MatchPath(al->header[sys], header) ) {
			return al;
		}
	}

	return (autolink_t)0;
}

/* find or create an auto-link entry by header and system */
autolink_t AL_Lookup(int sys, const char *header) {
	autolink_t al;
	
	ASSERT( sys>=0 && sys<kNumProjSys );
	ASSERT( header != ( const char * )0 );
	
	if( !( al = AL_Find( sys, header ) ) ) {
		al = AL_New();
		AL_SetHeader( al, sys, header );
	}
	
	return al;
}

/* retrieve the flags needed for linking to a library based on its header */
const char *AL_Autolink(int sys, const char *header) {
	autolink_t al;
	lib_t lib;

	ASSERT( sys>=0 && sys<kNumProjSys );
	ASSERT( header != (const char *)0 );

	if( !(al = AL_Find(sys, header)) ) {
		return (const char *)0;
	}

	if( !(lib = Lib_Find(AL_GetLib(al))) ) {
		return (const char *)0;
	}

	return Lib_Flags(lib, sys);
}

/* recursively add autolinks for a lib from an include directory */
void AL_ManagePackage_r(const char *libname, int sys, const char *incdir) {
	struct dirent *dp;
	autolink_t al;
	DIR *d;

	Dbg_Outf("*** PKGAUTOLINK dir:\"%s\" lib:\"%s\" ***\n",
		incdir, libname);

	FS_Enter(incdir);
	d = FS_OpenDir("./");
	if( !d ) {
		FS_Leave();
		Dbg_Outf("    AL_ManagePackage_r failed to enter directory\n");
		return;
	}
	while( (dp = readdir(d)) != (struct dirent *)0 ) {
		if( !strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..") ) {
			continue;
		}

		if( FS_IsDir(dp->d_name) ) {
			AL_ManagePackage_r(libname, sys, dp->d_name);
			continue;
		}

		{
			char realpath[PATH_MAX];

			if( !FS_RealPath(dp->d_name, realpath, sizeof(realpath)) ) {
				continue;
			}

			al = AL_New();
			AL_SetLib(al, libname);
			AL_SetHeader(al, sys, realpath);

			Dbg_Outf("    autolinking path: \"%s\"\n", realpath);
		}
	}
	FS_CloseDir(d);
	FS_Leave();
}

/*
===============================================================================

	AUTOLINK CONFIG FILE FORMAT
	===========================

	This allows you to configure a set of libraries/autolink headers. It's
	pretty simple:
	
		libname "linkflags" plat:"linkflags" plat:"linkflags" {
			"header/file/name.h"
			plat:"header/file/name.h"
			plat:"header/file/name.h"
		}

	e.g., for OpenGL:
	
		opengl "-lGL" mswin:"-lopengl32" apple:"-framework OpenGL" {
			"GL/gl.h"
			apple:"OpenGL/OpenGL.h"
		}

	New entries override old entries.

===============================================================================
*/

#if !MK_DEBUG_ENABLED
# undef  MK_DEBUG_AUTOLIBCONF_ENABLED
# define MK_DEBUG_AUTOLIBCONF_ENABLED 0
#endif

/* skip past whitespace, including comments */
static void AL__LexWhite( buffer_t buf ) {
	int r;
	do {
		r = 0;
		
		Buf_SkipWhite( buf );

		r |= Buf_SkipComment( buf, "#" );
		r |= Buf_SkipComment( buf, "//" );
	} while( r );
}
/* check whether a given character is part of a "name" token */
static int AL__IsLexName( char c ) {
	if( ( c >= 'a' && c <= 'z' ) || ( c >= 'A' && c <= 'Z' ) ) {
		return 1;
	}
	
	if( c >= '0' && c <= '9' ) {
		return 1;
	}
	
	if( c == '-' || c == '_' || c == '.' ) {
		return 1;
	}
	
	return 0;
}
/* check whether a given character is considered a valid delimiter (can break a name token) */
static int AL__IsLexDelim( char c ) {
	if( ( unsigned char )( c <= ' ' ) ) {
		return 1;
	}
	
	return c == '{' || c == '}' || c == ':' || c == ',';
}
/* lex in a name token; returns 1 on success or if not a name token; 0 on error */
static int AL__LexName( char *pszDst, size_t cDstMax, buffer_t buf ) {
	char *p, *e;
	
	ASSERT( pszDst != ( char * )0 );
	ASSERT( cDstMax > 1 );
	
	p = pszDst;
	e = pszDst + cDstMax - 1;
	
	for(;;) {
		char c;
		
		c = Buf_Peek( buf );

		if( AL__IsLexName( c ) ) {
			if( p == e ) {
				Buf_Error( buf, "Length of libary name is too long; must be under %u bytes", ( unsigned )cDstMax );
				pszDst[ 0 ] = '\0';
				return 0;
			}

			*p++ = c;
			( void )Buf_Read( buf );
			continue;
		} else if( p == pszDst ) {
			break;
		}
		
		if( AL__IsLexDelim( c ) ) {
			*p = '\0';
			return 1;
		}
		
		Buf_Error( buf, "Unexpected character in library name: 0x%.2X (%c)", ( unsigned )c, c );
		break;
	}
	
	pszDst[ 0 ] = '\0';
	return 0;
}
/* lex in a quote (string); returns 0 on error or if not a string (CONSISTENCY) */
static int AL__LexQuote( char *pszDst, size_t cDstMax, buffer_t buf ) {
	char *p, *e;
	
	ASSERT( pszDst != ( char * )0 );
	ASSERT( cDstMax > 3 );
	
	p = pszDst;
	e = pszDst + cDstMax - 1;
	
	if( !Buf_Check( buf, '\"' ) ) {
		return 0;
	}
	
	for(;;) {
		char c;
		
		if( buf->ptr == buf->endPtr ) {
			Buf_Error( buf, "Unexpected end of file while reading string" );
			pszDst[ 0 ] = '\0';
			return 0;
		}
		
		if( ( c = Buf_Read( buf ) ) == '\"' ) {
			break;
		}
		
		if( c == '\\' ) {
			c = Buf_Read( buf );

			switch( c ) {
			case 'a': c = '\a'; break;
			case 'b': c = '\b'; break;
			case 'f': c = '\f'; break;
			case 'n': c = '\n'; break;
			case 'r': c = '\r'; break;
			case 't': c = '\t'; break;
			case 'v': c = '\v'; break;
			case '\'': c = '\''; break;
			case '\"': c = '\"'; break;
			case '\?': c = '\?'; break;
			default:
				Buf_Error( buf, "Unknown escape sequence char 0x%.2X ('%c'); ignoring", ( unsigned )c, c );
				break;
			}
		}
		
		if( p == e ) {
			Buf_Error( buf, "String is too long; keep under %u bytes", ( unsigned )cDstMax );
			*p = '\0';
			return 0;
		}
		
		*p++ = c;
	}
	
	*p = '\0';
	return 1;
}

typedef enum AL_Token_e {
	kALTok_Error = -1,
	kALTok_EOF = 0,
	
	kALTok_LBrace = '{',
	kALTok_RBrace = '}',
	kALTok_Colon = ':',
	kALTok_Comma = ',',

	kALTok_Name = 128,
	kALTok_Quote,
	
	kALTok_Null
} AL_Token_t;

/* lex a token from the buffer */
AL_Token_t AL__Lex( char *pszDst, size_t cDstMax, buffer_t buf ) {
	char *bufPos;
	
	ASSERT( pszDst != ( char * )0 );
	ASSERT( cDstMax > 7 ); /* "(error)" == 7 bytes */
	ASSERT( buf != ( buffer_t )0 );
	
	AL__LexWhite( buf );
	
	if( ( bufPos = buf->ptr ) == buf->endPtr ) {
		*pszDst = '\0';
		return kALTok_EOF;
	}
	
	if( AL__LexName( pszDst, cDstMax, buf ) && pszDst[ 0 ] != '\0' ) {
		if( *pszDst == 'n' && strcmp( pszDst, "null" ) == 0 ) {
			return kALTok_Null;
		}
		
		return kALTok_Name;
	}

	if( Buf_Peek( buf ) == '\"' ) {
		if( !AL__LexQuote( pszDst, cDstMax, buf ) ) {
			buf->ptr = bufPos;
			Buf_Error( buf, "Incomplete quote starts here" );
			buf->ptr = buf->endPtr;
			return kALTok_Error;
		}
		
		return kALTok_Quote;
	}
	
	if( Buf_Check( buf, '{' ) ) {
		Com_Strcpy( pszDst, cDstMax, "{" );
		return kALTok_LBrace;
	}
	
	if( Buf_Check( buf, '}' ) ) {
		Com_Strcpy( pszDst, cDstMax, "}" );
		return kALTok_RBrace;
	}
	
	if( Buf_Check( buf, ':' ) ) {
		Com_Strcpy( pszDst, cDstMax, ":" );
		return kALTok_Colon;
	}
	
	if( Buf_Check( buf, ',' ) ) {
		Com_Strcpy( pszDst, cDstMax, "," );
		return kALTok_Comma;
	}
	
	Com_Strcpy( pszDst, cDstMax, "(error)" );
	
	Buf_Error( buf, "Unrecognized character 0x%.2X ('%c')", ( unsigned )Buf_Peek( buf ), Buf_Peek( buf ) );
	buf->ptr = buf->endPtr;
	
	return kALTok_Error;
}

/* retrieve the name of a token */
const char *AL__GetTokenName( AL_Token_t tok ) {
	switch( tok ) {
	case kALTok_Error:  return "(error)";
	case kALTok_EOF:    return "EOF";
	
	case kALTok_LBrace: return "'{'";
	case kALTok_RBrace: return "'}'";
	case kALTok_Colon:  return "':'";
	case kALTok_Comma:  return "','";
	
	case kALTok_Name:   return "name";
	case kALTok_Quote:  return "quote";

	case kALTok_Null:   return "'null'";
	}
	
	return "(invalid)";
}

/* expect any of the given token types (returns index of the token in the array, or -1 on error/unmatched) */
int AL__ExpectAny( char *pszDst, size_t cDstMax, buffer_t buf, const AL_Token_t *pToks, size_t cToks ) {
	char szErrorBuf[ 256 ];
	AL_Token_t tok;
	size_t i;
	
	if( ( tok = AL__Lex( pszDst, cDstMax, buf ) ) == kALTok_Error ) {
		return -1;
	}
	
	for( i = 0; i < cToks; ++i ) {
		if( pToks[ i ] == tok ) {
			return ( int )( unsigned )i;
		}
	}
	
	Com_Strcpy( szErrorBuf, sizeof( szErrorBuf ), "Expected " );
	for( i = 0; i < cToks; ++i ) {
		Com_Strcat( szErrorBuf, sizeof( szErrorBuf ), AL__GetTokenName( pToks[ i ] ) );
		if( i + 1 < cToks ) {
			Com_Strcat( szErrorBuf, sizeof( szErrorBuf ), ( i + 2 == cToks ) ? ", or " : ", " );
		}
	}
	Com_Strcat( szErrorBuf, sizeof( szErrorBuf ), ", but got " );
	Com_Strcat( szErrorBuf, sizeof( szErrorBuf ), AL__GetTokenName( tok ) );
	
	Buf_Error( buf, "%s", szErrorBuf );
	return -1;
}
/* expect any of the given three token types (returns -1 on failure, or 0, 1, or 2 on success) */
int AL__Expect3( char *pszDst, size_t cDstMax, buffer_t buf, AL_Token_t tok1, AL_Token_t tok2, AL_Token_t tok3 ) {
	AL_Token_t toks[ 3 ];
	
	toks[ 0 ] = tok1;
	toks[ 1 ] = tok2;
	toks[ 2 ] = tok3;
	
	return AL__ExpectAny( pszDst, cDstMax, buf, toks, sizeof( toks )/sizeof( toks[0] ) );
}
/* expect any of the given two token types (returns -1 on failure, or 0 or 1 on success) */
int AL__Expect2( char *pszDst, size_t cDstMax, buffer_t buf, AL_Token_t tok1, AL_Token_t tok2 ) {
	AL_Token_t toks[ 2 ];
	
	toks[ 0 ] = tok1;
	toks[ 1 ] = tok2;
	
	return AL__ExpectAny( pszDst, cDstMax, buf, toks, sizeof( toks )/sizeof( toks[0] ) );
}
/* expect the given token type (returns 0 on failure, 1 on success) */
int AL__Expect( char *pszDst, size_t cDstMax, buffer_t buf, AL_Token_t tok ) {
	return AL__ExpectAny( pszDst, cDstMax, buf, &tok, 1 ) + 1;
}

/* retrieve the platform (kProjSys_*) from the string, or fail with -1 */
int AL__GetProjPlatform( const char *pszPlat ) {
	if( strcmp( pszPlat, MK_PLATFORM_OS_NAME_MSWIN ) == 0 ) {
		return kProjSys_MSWin;
	}

	if( strcmp( pszPlat, MK_PLATFORM_OS_NAME_UWP ) == 0 ) {
		return kProjSys_UWP;
	}

	if( strcmp( pszPlat, MK_PLATFORM_OS_NAME_CYGWIN ) == 0 ) {
		return kProjSys_Cygwin;
	}

	if( strcmp( pszPlat, MK_PLATFORM_OS_NAME_LINUX ) == 0 ) {
		return kProjSys_Linux;
	}

	if( strcmp( pszPlat, MK_PLATFORM_OS_NAME_MACOSX ) == 0 ) {
		return kProjSys_MacOSX;
	}

	if( strcmp( pszPlat, MK_PLATFORM_OS_NAME_UNIX ) == 0 ) {
		return kProjSys_Unix;
	}

	return -1;
}

/* parse a series of comma-delimited platform names until a ':' is reached; expects pszDst to already have a name */
int AL__ReadPlatformNames( int *pDstPlats, size_t cDstPlatsMax, size_t *pcDstPlats, char *pszDst, size_t cDstMax, buffer_t buf ) {
	size_t cPlats = 0;
	int r;
	
	ASSERT( pDstPlats != ( int * )0 );
	ASSERT( cDstPlatsMax >= kNumProjSys );
	ASSERT( pcDstPlats != ( size_t * )0 );
	ASSERT( pszDst != ( char * )0 );
	ASSERT( cDstMax > 0 );
	ASSERT( buf != ( buffer_t )0 );
	
	*pcDstPlats = 0;
	
	for(;;) {
#if MK_DEBUG_AUTOLIBCONF_ENABLED
		Dbg_Outf( "Got platform: \"%s\"\n", pszDst );
#endif
		if( ( pDstPlats[ cPlats ] = AL__GetProjPlatform( pszDst ) ) == -1 ) {
			Buf_Error( buf, "Unknown platform \"%s\"", pszDst );
			return 0;
		}
		
		ASSERT( pDstPlats[ cPlats ] >= 0 && pDstPlats[ cPlats ] < kNumProjSys );
		
		++cPlats;
		
		if( ( r = AL__Expect2( pszDst, cDstMax, buf, kALTok_Comma, kALTok_Colon ) ) == -1 ) {
			return 0;
		}
		
		if( r == 0 /* comma */ ) {
			if( cPlats == cDstPlatsMax ) {
				Buf_Error( buf, "Too many platforms specified; keep under %u", ( unsigned )cDstPlatsMax );
				return 0;
			}

			continue;
		}
		
		ASSERT( r == 1 /* colon */ );
		break;
	}
	
	*pcDstPlats = cPlats;
	return 1;
}

/* parse the library's flags and the left-brace; returns 0 on fail, or 1 on success */
int AL__ReadLibFlagsAndLBrace( char *pszDst, size_t cDstMax, buffer_t buf, lib_t lib ) {
	size_t cPlats, i;
	int plat[ kNumProjSys ];
	int r;
	
	ASSERT( pszDst != ( char * )0 );
	ASSERT( cDstMax > 0 );
	ASSERT( buf != ( buffer_t )0 );
	ASSERT( lib != ( lib_t )0 );
	
#if MK_DEBUG_AUTOLIBCONF_ENABLED
	Dbg_Outf( "AL__ReadLibFlagsAndLBrace...\n" );
#endif
	
	/* keep going until we get an error or a left-brace */
	for(;;) {
		if( ( r = AL__Expect3( pszDst, cDstMax, buf, kALTok_Name, kALTok_Quote, kALTok_LBrace ) ) == -1 ) {
			break;
		}
		
		cPlats = 0;
		switch( r ) {
		case 0: /* name */
			/* read the sequence of platform names (e.g., mswin,uwp,cygwin: etc...) */
			if( !AL__ReadPlatformNames( plat, sizeof( plat )/sizeof( plat[ 0 ] ), &cPlats, pszDst, cDstMax, buf ) ) {
				return 0;
			}
				
			/* immediately following the platform names must be a quote or "null" */
			if( ( r = AL__Expect2( pszDst, cDstMax, buf, kALTok_Quote, kALTok_Null ) ) == -1 ) {
				return 0;
			}
			
			/* if we got null, then we can't share the code */
			if( r == 1 /* null */ ) {
#if MK_DEBUG_AUTOLIBCONF_ENABLED
				Dbg_Outf( "Setting platform linker flags to null.\n" );
#endif
				for( i = 0; i < cPlats; ++i ) {
					Lib_SetFlags( lib, plat[ i ], ( const char * )0 );
				}
				
				break;
			}
			
			/* fallthrough */
		case 1: /* quote */
			/* if we got a string and no platforms were specified then assume all */
			if( !cPlats ) {
#if MK_DEBUG_AUTOLIBCONF_ENABLED
				Dbg_Outf( "No platforms given; assuming all for this flag.\n" );
#endif

				while( cPlats < ( size_t )kNumProjSys ) {
					plat[ cPlats ] = ( int )( unsigned )cPlats;
					++cPlats;
				}
			}

#if MK_DEBUG_AUTOLIBCONF_ENABLED
			Dbg_Outf( "Setting platforms to linker flag: \"%s\"\n", pszDst );
#endif
			
			/* set each platform's library flags */
			for( i = 0; i < cPlats; ++i ) {
				Lib_SetFlags( lib, plat[ i ], pszDst );
			}
			
			break;
			
		case 2: /* left-brace */
#if MK_DEBUG_AUTOLIBCONF_ENABLED
			Dbg_Outf( "Got left-brace.\n\n" );
#endif
			return 1;
		}
	}
	
	/* we got an error */
	return 0;
}
/* parse the library's headers and the right-brace; returns 0 on fail, or 1 on success */
int AL__ReadLibHeadersAndRBrace( char *pszDst, size_t cDstMax, buffer_t buf, lib_t lib ) {
	size_t cPlats, i;
	int plat[ kNumProjSys ];
	int r;
	
	ASSERT( pszDst != ( char * )0 );
	ASSERT( cDstMax > 0 );
	ASSERT( buf != ( buffer_t )0 );
	ASSERT( lib != ( lib_t )0 );

#if MK_DEBUG_AUTOLIBCONF_ENABLED
	Dbg_Outf( "AL__ReadLibHeadersAndRBrace...\n" );
#endif
	
	/* keep going until we get an error or a right-brace */
	for(;;) {
		if( ( r = AL__Expect3( pszDst, cDstMax, buf, kALTok_Name, kALTok_Quote, kALTok_RBrace ) ) == -1 ) {
			break;
		}
		
		cPlats = 0;
		switch( r ) {
		case 0: /* name */
			/* read the sequence of platform names (e.g., mswin,uwp,cygwin: etc...) */
			if( !AL__ReadPlatformNames( plat, sizeof( plat )/sizeof( plat[ 0 ] ), &cPlats, pszDst, cDstMax, buf ) ) {
				return 0;
			}
				
			/* immediately following the platform names must be a quote */
			if( !AL__Expect( pszDst, cDstMax, buf, kALTok_Quote ) ) {
				return 0;
			}
			
			/* fallthrough */
		case 1: /* quote */
			/* cannot have an empty string for autolinks */
			if( *pszDst == '\0' ) {
				Buf_Error( buf, "Empty strings cannot be used for autolinks" );
				return 0;
			}

			/* if we got a string and no platforms were specified then assume all */
			if( !cPlats ) {
#if MK_DEBUG_AUTOLIBCONF_ENABLED
				Dbg_Outf( "No platforms given; assuming all for this header.\n" );
#endif
				while( cPlats < ( size_t )kNumProjSys ) {
					plat[ cPlats ] = ( int )( unsigned )cPlats;
					++cPlats;
				}
			}

#if MK_DEBUG_AUTOLIBCONF_ENABLED
			Dbg_Outf( "Adding autolink entries to %s for header: \"%s\"\n",
				cPlats == 1 ? "this platform" : "these platforms",
				pszDst );
#endif
			
			/* create the autolinks (pszDst has the header name) */
			for( i = 0; i < cPlats; ++i ) {
				autolink_t al;
				
				al = AL_Lookup( plat[ i ], pszDst );
				ASSERT( al != ( autolink_t )0 && "AL_Lookup() should never return NULL" );
				
				/* FIXME: This could be a bit higher performance */
				AL_SetLib( al, Lib_Name( lib ) );
			}
			
			break;
			
		case 2: /* right-brace */
#if MK_DEBUG_AUTOLIBCONF_ENABLED
			Dbg_Outf( "Got right-brace.\n" );
#endif
			return 1;
		}
	}
	
	/* we got an error */
	return 0;
}

/* parse a top-level library, returning 0 on failure or 1 on success */
int AL__ReadLib( char *pszDst, size_t cDstMax, buffer_t buf ) {
	lib_t lib;
	int r;
	
	r = AL__Expect2( pszDst, cDstMax, buf, kALTok_Name, kALTok_EOF );
	if( r != 0 /* not kALTok_Name */ ) {
		return r == 1; /* EOF is success */
	}
	
#if MK_DEBUG_AUTOLIBCONF_ENABLED
	Dbg_Enter( "AL__ReadLib(\"%s\")", pszDst );
#endif
	
	/* find or create the library; if found we overwrite some stuff */
	lib = Lib_Lookup( pszDst );
	ASSERT( lib != ( lib_t )0 );
	
	/* read the lib flags and the left-brace */
	if( !AL__ReadLibFlagsAndLBrace( pszDst, cDstMax, buf, lib ) ) {
#if MK_DEBUG_AUTOLIBCONF_ENABLED
		Dbg_Leave();
#endif
		return 0;
	}
	
	/* read headers */
	if( !AL__ReadLibHeadersAndRBrace( pszDst, cDstMax, buf, lib ) ) {
#if MK_DEBUG_AUTOLIBCONF_ENABLED
		Dbg_Leave();
#endif
		return 0;
	}

#if MK_DEBUG_AUTOLIBCONF_ENABLED
	Dbg_Leave();
#endif
	
	/* done */
	return 1;
}

/* parse a file for autolinks and libraries */
int AL_LoadConfig( const char *filename ) {
	buffer_t buf;
	char szLexan[ 512 ];
	int r;
	
	ASSERT( filename != ( const char * )0 );
	
	if( !( buf = Buf_LoadFile( filename ) ) ) {
		Dbg_Outf( "%s: Failed to load autolink config\n", filename );
		return 0;
	}
	
	Dbg_Enter( "AL_LoadConfig(\"%s\")", filename );
	
	r = 1;
	do {
		if( !AL__ReadLib( szLexan, sizeof( szLexan ), buf ) ) {
			r = 0;
			break;
		}
	} while( buf->ptr != buf->endPtr );
	
	Dbg_Leave();
	
	Buf_Unload( buf );
	return r;
}

/*
 *	========================================================================
 *	PROJECT MANAGEMENT
 *	========================================================================
 */

#if !MK_DEBUG_ENABLED
# undef  MK_DEBUG_LIBDEPS_ENABLED
# define MK_DEBUG_LIBDEPS_ENABLED 0
#endif

const int g_host_sys =
#if MK_HOST_OS_MSWIN
	kProjSys_MSWin;
#elif MK_HOST_OS_CYGWIN
	kProjSys_Cygwin;
#elif MK_HOST_OS_LINUX
	kProjSys_Linux;
#elif MK_HOST_OS_MACOSX
	kProjSys_MacOSX;
#elif MK_HOST_OS_UNIX
	kProjSys_Unix;
#else
# error "OS not recognized."
#endif

const int g_host_arch =
#if MK_HOST_CPU_X86
	kProjArch_X86;
#elif MK_HOST_CPU_X86_64
	kProjArch_X86_64;
#elif MK_HOST_CPU_ARM
	kProjArch_ARM;
#elif MK_HOST_CPU_AARCH64
	kProjArch_AArch64;
#elif MK_HOST_CPU_PPC
	kProjArch_PowerPC;
#elif MK_HOST_CPU_MIPS
	kProjArch_MIPS;
#else
/* TODO: kProjArch_AArch64 */
# error "Architecture not recognized."
#endif

enum {
	kProjCfg_UsesCxx_Bit = 0x01,
	kProjCfg_NeedRelink_Bit = 0x02,
	kProjCfg_Linking_Bit = 0x04,
	kProjCfg_Package_Bit = 0x08
};
enum {
	kProjStat_LibFlags_Bit = 0x01,
	kProjStat_CalcDeps_Bit = 0x02
};

struct project_s {
	char *name;
	char *path;
	char *outpath;
	char *binname;
	int type;
	int sys;
	int arch;

	strList_t defs;

	strList_t sources;
	strList_t specialdirs;
	strList_t libs;

	strList_t testsources;

	strList_t srcdirs; /* needed for determining object paths */

	char *linkerflags;
	char *extralibs;
	
	char *deplibsfilename;

	bitfield_t config;
	bitfield_t status;

	lib_t lib;

	struct project_s *prnt, **p_head, **p_tail;
	struct project_s *head, *tail;
	struct project_s *prev, *next;

	struct project_s *lib_prev, *lib_next;
};
struct project_s *g_proj_head = (struct project_s *)0;
struct project_s *g_proj_tail = (struct project_s *)0;
struct project_s *g_lib_proj_head = (struct project_s *)0;
struct project_s *g_lib_proj_tail = (struct project_s *)0;

static const struct { const char *const name; const int type; } g_ifiles[] = {
	{ ".library"          , kProjType_Library        },
	{ ".mk-library"       , kProjType_Library        },
	{ ".dynamiclibrary"   , kProjType_DynamicLibrary },
	{ ".mk-dynamiclibrary", kProjType_DynamicLibrary },
	{ ".executable"       , kProjType_Executable     },
	{ ".mk-executable"    , kProjType_Executable     },
	{ ".application"      , kProjType_Application    },
	{ ".mk-application"   , kProjType_Application    },
	
	/* ".txt" indicator files for easier creation on Windows - notkyon:160204 */
	{ "mk-staticlib.txt"  , kProjType_Library        },
	{ "mk-dynamiclib.txt" , kProjType_DynamicLibrary },
	{ "mk-executable.txt" , kProjType_Executable     },
	{ "mk-application.txt", kProjType_Application    }
};

/* create a new (empty) project */
project_t Prj_New(project_t prnt) {
	project_t proj;

	proj = (project_t)Com_Memory((void *)0, sizeof(*proj));

	proj->name = (char *)0;
	proj->path = (char *)0;
	proj->outpath = (char *)0;
	proj->binname = (char *)0;

	proj->type = kProjType_Executable;
	proj->sys = g_host_sys;
	proj->arch = g_host_arch;

	proj->defs = SL_New();

	proj->sources = SL_New();
	proj->specialdirs = SL_New();
	proj->libs = SL_New();

	proj->testsources = SL_New();

	proj->srcdirs = SL_New();

	proj->linkerflags = (char *)0;
	proj->extralibs = (char *)0;
	
	proj->deplibsfilename = (char *)0;

	proj->config = 0;
	proj->status = 0;

	proj->lib = (lib_t)0;

	proj->prnt = prnt;
	proj->p_head = prnt ? &prnt->head : &g_proj_head;
	proj->p_tail = prnt ? &prnt->tail : &g_proj_tail;
	proj->head = (struct project_s *)0;
	proj->tail = (struct project_s *)0;
	proj->next = (struct project_s *)0;
	if( (proj->prev = *proj->p_tail) != (struct project_s *)0 ) {
		(*proj->p_tail)->next = proj;
	} else {
		*proj->p_head = proj;
	}
	*proj->p_tail = proj;

	proj->lib_prev = (struct project_s *)0;
	proj->lib_next = (struct project_s *)0;

	return proj;
}

/* delete an existing project */
void Prj_Delete(project_t proj) {
	if( !proj )
		return;

	while( proj->head ) {
		Prj_Delete(proj->head);
	}

	proj->name = (char *)Com_Memory((void *)proj->name, 0);
	proj->path = (char *)Com_Memory((void *)proj->path, 0);
	proj->outpath = (char *)Com_Memory((void *)proj->outpath, 0);
	proj->binname = (char *)Com_Memory((void *)proj->binname, 0);

	SL_Delete(proj->defs);

	SL_Delete(proj->sources);
	SL_Delete(proj->specialdirs);
	SL_Delete(proj->libs);

	SL_Delete(proj->testsources);

	SL_Delete(proj->srcdirs);

	proj->linkerflags = (char *)Com_Memory((void *)proj->linkerflags, 0);
	proj->extralibs = (char *)Com_Memory((void *)proj->extralibs, 0);

	proj->deplibsfilename = (char *)Com_Memory((void*)proj->deplibsfilename, 0);

	if( proj->lib ) {
		Lib_Delete(proj->lib);
	}

	if( proj->prev ) {
		proj->prev->next = proj->next;
	}
	if( proj->next ) {
		proj->next->prev = proj->prev;
	}

	ASSERT( proj->p_head != (struct project_s **)0 );
	ASSERT( proj->p_tail != (struct project_s **)0 );

	if( *proj->p_head==proj ) {
		*proj->p_head = proj->next;
	}
	if( *proj->p_tail==proj ) {
		*proj->p_tail = proj->prev;
	}

	if( proj->type == kProjType_Library ) {
		if( proj->lib_prev ) {
			proj->lib_prev->lib_next = proj->lib_next;
		} else {
			g_lib_proj_head = proj->lib_next;
		}

		if( proj->lib_next ) {
			proj->lib_next->lib_prev = proj->lib_prev;
		} else {
			g_lib_proj_tail = proj->lib_prev;
		}
	}

	Com_Memory((void *)proj, 0);
}

/* delete all projects */
void Prj_DeleteAll() {
	while( g_proj_head )
		Prj_Delete(g_proj_head);
}

/* retrieve the first root project */
project_t Prj_RootHead() {
	return g_proj_head;
}

/* retrieve the last root project */
project_t Prj_RootTail() {
	return g_proj_tail;
}

/* retrieve the parent of a project */
project_t Prj_Parent(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return proj->prnt;
}

/* retrieve the first child project of the given project */
project_t Prj_Head(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return proj->head;
}

/* retrieve the last child project of the given project */
project_t Prj_Tail(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return proj->tail;
}

/* retrieve the sibling project before the given project */
project_t Prj_Prev(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return proj->prev;
}

/* retrieve the sibling project after the given project */
project_t Prj_Next(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return proj->next;
}

/* set the name of a project */
void Prj_SetName(project_t proj, const char *name) {
	const char *p;
	char tmp[PATH_MAX];

	ASSERT( proj != (project_t)0 );

	tmp[0] = 0;
	p = (const char *)0;

	if( name ) {
		if( !(p = strrchr(name, '/')) )
			p = strrchr(name, '\\');

		if( p ) {
			/* can't end in "/" */
			if( *(p+1)==0 ) {
				Log_FatalError(Com_Va("invalid project name \"%s\"", name));
			}

			Com_Strncpy(tmp, sizeof(tmp), name, (p-name) + 1);
			p++;
		} else {
			p = name;
		}
	}

	ASSERT( p != (const char *)0 );

	proj->outpath = Com_Dup(proj->outpath, tmp[0] ? tmp : (const char *)0);
	proj->name = Com_Dup(proj->name, p);

	proj->deplibsfilename = Com_Dup(proj->deplibsfilename,
		Com_Va("%s/%s/%s.deplibs",
			Opt_GetObjdirBase(),
			Opt_GetConfigName(),
			proj->name));
}

/* set the output path of a project */
void Prj_SetOutPath(project_t proj, const char *path) {
	ASSERT( proj != (project_t)0 );
	ASSERT( path != (const char *)0 );

	proj->outpath = Com_Dup(proj->outpath, path);
}

/* set the path of a project */
void Prj_SetPath(project_t proj, const char *path) {
	ASSERT( proj != (project_t)0 );

	proj->path = Com_Dup(proj->path, path);
}

/* set the type of a project (e.g., kProjType_Library) */
void Prj_SetType(project_t proj, int type) {
	ASSERT( proj != (project_t)0 );

	if( !proj->outpath ) {
		static const char *const pszlibdir = "lib/" MK_PLATFORM_DIR;
		static const char *const pszbindir = "bin/" MK_PLATFORM_DIR;
		
		proj->outpath = Com_Dup(proj->outpath, Com_Va( "%s/",
			type==kProjType_Library ? pszlibdir : pszbindir) );
		if( type==kProjType_Library && !SL_Size(g_libdirs) ) {
			Mk_PushLibDir(pszlibdir);
		}
	}

	if( proj->type == kProjType_Library ) {
		if( proj->lib_prev ) {
			proj->lib_prev->lib_next = proj->lib_next;
		} else {
			g_lib_proj_head = proj->lib_next;
		}

		if( proj->lib_next ) {
			proj->lib_next->lib_prev = proj->lib_prev;
		} else {
			g_lib_proj_tail = proj->lib_prev;
		}
	}

	proj->type = type;

	if( proj->type == kProjType_Library ) {
		proj->lib_next = (struct project_s *)0;
		if( (proj->lib_prev = g_lib_proj_tail) != (struct project_s *)0 ) {
			g_lib_proj_tail->lib_next = proj;
		} else {
			g_lib_proj_head = proj;
		}
		g_lib_proj_tail = proj;
	}
}

/* retrieve the (non-modifyable) name of a project */
const char *Prj_Name(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return proj->name;
}

/* retrieve the (non-modifyable) path of a project */
const char *Prj_Path(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return proj->path;
}

/* retrieve the output path of a project */
const char *Prj_OutPath(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return proj->outpath;
}

/* retrieve the type of a project (e.g., kProjType_Executable) */
int Prj_Type(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return proj->type;
}

/* add a library to the project */
void Prj_AddLib(project_t proj, const char *libname) {
	ASSERT( proj != (project_t)0 );
	ASSERT( libname != (const char *)0 );

	SL_PushBack(proj->libs, libname);
}

/* retrieve the number of libraries in a project */
size_t Prj_LibCount(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return SL_Size(proj->libs);
}

/* retrieve a library of a project */
const char *Prj_Lib(project_t proj, size_t i) {
	ASSERT( proj != (project_t)0 );
	ASSERT( i < SL_Size(proj->libs) );

	return SL_Get(proj->libs, i);
}

/* append a linker flag to the project */
void Prj_AddLinkFlag(project_t proj, const char *flags) {
	size_t l;

	ASSERT( proj != (project_t)0 );
	/* ASSERT( flags != (const char *)0 ); :: this happens due to built-in libs */
	if( !flags ) {
		return;
	}

	l = proj->linkerflags ? proj->linkerflags[0]!=0 : 0;

	if( l ) {
		proj->linkerflags = Com_Append(proj->linkerflags, " ");
	}

	proj->linkerflags = Com_Append(proj->linkerflags, flags);
}

/* retrieve the current linker flags of a project */
const char *Prj_LinkFlags(project_t proj) {
	ASSERT( proj != (project_t)0 );

	return proj->linkerflags ? proj->linkerflags : "";
}

/* append "extra libs" to the project (found in ".libs" files) */
void Prj_AppendExtraLib(project_t proj, const char *extras)
{
	size_t l;

	ASSERT( proj != (project_t)0 );
	ASSERT( extras != (const char *)0 );

	l = proj->extralibs ? proj->extralibs[0]!=0 : 0;

	if( l ) {
		proj->extralibs = Com_Append(proj->extralibs, " ");
	}

	proj->extralibs = Com_Append(proj->extralibs, extras);
}

/* retrieve the "extra libs" of a project */
const char *Prj_ExtraLibs(project_t proj)
{
	ASSERT( proj != (project_t)0 );

	return proj->extralibs ? proj->extralibs : "";
}

/* retrieve all "extra libs" of a project (includes project children) */
void Prj_CompleteExtraLibs(project_t proj, char *extras, size_t n) {
	project_t x;

	for( x=Prj_Head(proj); x; x=Prj_Next(x) ) {
		Prj_CompleteExtraLibs(x, extras, n);
	}

	Com_Strcat(extras, n, Prj_ExtraLibs(proj));
	Com_Strcat(extras, n, " ");
}

/* add a source file to a project */
void Prj_AddSource(project_t proj, const char *src) {
	const char *p;

	ASSERT( proj != (project_t)0 );
	ASSERT( src != (const char *)0 );

	ASSERT( proj->sources != (strList_t)0 );

	if( (p = strrchr(src, '.')) != (const char *)0 ) {
		if( !strcmp(p, ".cc") || !strcmp(p, ".cxx") || !strcmp(p, ".cpp" )
		 || !strcmp(p, ".c++")) {
			proj->config |= kProjCfg_UsesCxx_Bit;
		} else if(!strcmp(p, ".mm")) {
			proj->config |= kProjCfg_UsesCxx_Bit;
		}
	}

	SL_PushBack(proj->sources, src);

	p = strrchr(src, '/');
	if( !p ) {
		p = strrchr(src, '\\');
	}

	if( p ) {
		char temp[PATH_MAX], rel[PATH_MAX];

		Com_Strncpy(temp, sizeof(temp), src, p - src);
		temp[p - src] = '\0';

		Com_RelPath(rel, sizeof(rel), proj->path, temp);

		SL_PushBack(proj->srcdirs, rel);
	}
}

/* retrieve the number of source files within a project */
size_t Prj_SourceCount(project_t proj) {
	ASSERT( proj != (project_t)0 );

	ASSERT( proj->sources != (strList_t)0 );

	return SL_Size(proj->sources);
}

/* retrieve a source file of a project */
const char *Prj_Source(project_t proj, size_t i) {
	ASSERT( proj != (project_t)0 );

	ASSERT( proj->sources != (strList_t)0 );

	return SL_Get(proj->sources, i);
}

/* add a test source file to a project */
void Prj_AddTestSource(project_t proj, const char *src) {
	ASSERT( proj != (project_t)0 );
	ASSERT( src != (const char *)0 );

	ASSERT( proj->testsources != (strList_t)0 );

	/*
	if( (p = strrchr(src, '.')) != (const char *)0 ) {
		if( !strcmp(p, ".cc") || !strcmp(p, ".cxx") || !strcmp(p, ".cpp" )
		 || !strcmp(p, ".c++"))
			proj->config |= kProjCfg_UsesCxx_Bit;
		else if(!strcmp(p, ".mm"))
			proj->config |= kProjCfg_UsesCxx_Bit;
	}
	*/

	SL_PushBack(proj->testsources, src);
}

/* retrieve the number of test source files within a project */
size_t Prj_TestSourceCount(project_t proj) {
	ASSERT( proj != (project_t)0 );

	ASSERT( proj->sources != (strList_t)0 );

	return SL_Size(proj->testsources);
}

/* retrieve a test source file of a project */
const char *Prj_TestSource(project_t proj, size_t i) {
	ASSERT( proj != (project_t)0 );

	ASSERT( proj->sources != (strList_t)0 );

	return SL_Get(proj->testsources, i);
}

/* add a "special directory" to a project */
void Prj_AddSpecDir(project_t proj, const char *dir) {
	ASSERT( proj != (project_t)0 );
	ASSERT( dir != (const char *)0 );

	ASSERT( proj->specialdirs != (strList_t)0 );

	SL_PushBack(proj->specialdirs, dir);
}

/* retrieve the number of "special directories" within a project */
size_t Prj_SpecDirCount(project_t proj) {
	ASSERT( proj != (project_t)0 );

	ASSERT( proj->specialdirs != (strList_t)0 );

	return SL_Size(proj->specialdirs);
}

/* retrieve a "special directory" of a project */
const char *Prj_SpecDir(project_t proj, size_t i) {
	ASSERT( proj != (project_t)0 );

	ASSERT( proj->specialdirs != (strList_t)0 );

	return SL_Get(proj->specialdirs, i);
}

/* determine if a given project is targetted */
int Prj_IsTarget(project_t proj) {
	size_t i, n;

	if( !(n = SL_Size(g_targets)) ) {
		return 1;
	}

	for( i=0; i<n; i++ ) {
		if( !strcmp(Prj_Name(proj), SL_Get(g_targets, i)) ) {
			return 1;
		}
	}

	return 0;
}

/* display a tree of projects to stdout */
void Prj_PrintAll(project_t proj, const char *margin) {
	size_t i, n;
	char marginbuf[256];
	char bin[256];

	ASSERT( margin != (const char *)0 );

	if( !proj ) {
		for( proj=Prj_RootHead(); proj; proj=Prj_Next(proj) ) {
			Prj_PrintAll(proj, margin);
		}

		return;
	}

	if( Prj_IsTarget(proj) ) {
		Bld_GetBinName(proj, bin, sizeof(bin));

		printf("%s%s; \"%s\"\n", margin, Prj_Name(proj), bin);

		n = Prj_SourceCount(proj);
		for( i=0; i<n; i++ ) {
			printf("%s * %s\n", margin, Prj_Source(proj, i));
		}

		n = Prj_TestSourceCount(proj);
		for( i=0; i<n; i++ ) {
			printf("%s # %s\n", margin, Prj_TestSource(proj, i));
		}
	}

	Com_Strcpy(marginbuf, sizeof(marginbuf), margin);
	Com_Strcat(marginbuf, sizeof(marginbuf), "  ");
	for( proj=Prj_Head(proj); proj; proj=Prj_Next(proj) ) {
		Prj_PrintAll(proj, marginbuf);
	}
}

/* expand library dependencies */
static void Prj__ExpandLibDeps_r( project_t proj )
{
	const char *libname;
	project_t libproj;
	size_t i, j, n, m;
	lib_t lib;

	do {
		n = SL_Size(proj->libs);
		for( i=0; i<n; ++i ) {
			if( !( libname = SL_Get( proj->libs, i ) ) ) {
				continue;
			}

			if( !( lib = Lib_Find( libname ) ) ) {
				Log_ErrorMsg( Com_Va( "couldn't find library ^E\"%s\"^&",
					libname ) );
				continue;
			}

			if( !lib->proj ) {
				continue;
			}

			libproj = lib->proj;
			lib->proj = ( project_t )0; /*prevent infinite recursion*/
			Prj__ExpandLibDeps_r( libproj );
			lib->proj = libproj; /*restore*/

			m = SL_Size( libproj->libs );
			for( j=0; j<m; ++j ) {
				SL_PushBack( proj->libs, SL_Get( libproj->libs, j ) );
			}
		}

		/* optimization -- if there were no changes to the working set then
		 * don't spend time looking for unique entries */
		if( SL_Size( proj->libs ) == n ) {
			break;
		}

		SL_Unique( proj->libs );
	} while( SL_Size( proj->libs ) != n ); /* SL_Unique can alter count */
}
static void Prj__SaveLibDeps( project_t proj ) {
	const char *p;
	size_t i, n, len;
	FILE *fp;

	ASSERT( proj->deplibsfilename != ( char * )0 );

	fp = fopen( proj->deplibsfilename, "wb" );
	if( !fp ) {
		Log_ErrorMsg( Com_Va( "failed to write ^E\"%s\"^&",
			proj->deplibsfilename ) );
	}

	fprintf( fp, "DEPS1.00" );

	n = SL_Size( proj->libs );
	fwrite( &n, sizeof( n ), 1, fp );
	for( i=0; i<n; ++i ) {
		p = SL_Get( proj->libs, i );
		if( !p ) {
			p = "";
		}

		len = strlen( p );
		
		fwrite( &len, sizeof( len ), 1, fp );
		fprintf( fp, "%s", p );
	}

	fclose( fp );

#if MK_DEBUG_LIBDEPS_ENABLED
	Dbg_Outf( "libdeps: saved \"%s\"\n", proj->deplibsfilename );
#endif
}
static void Prj__LoadLibDeps( project_t proj ) {
	size_t i, n, temp;
	FILE *fp;
	char buf[ 512 ];

	ASSERT( proj->deplibsfilename != ( char * )0 );

#if MK_DEBUG_LIBDEPS_ENABLED
	Dbg_Outf( "libdeps: opening \"%s\"...\n", proj->deplibsfilename );
#endif

	fp = fopen( proj->deplibsfilename, "rb" );
	if( !fp ) {
#if MK_DEBUG_LIBDEPS_ENABLED
		Dbg_Outf( "libdeps: failed to open\n" );
#endif
		return;
	}

	if( !fread( &buf[0], 8, 1, fp ) ) {
		fclose( fp );
#if MK_DEBUG_LIBDEPS_ENABLED
		Dbg_Outf( "libdeps: failed; no header\n" );
#endif
		return;
	}
	buf[8] = '\0';
	if( strcmp( buf, "DEPS1.00" ) != 0 ) {
		fclose( fp );
#if MK_DEBUG_LIBDEPS_ENABLED
		Dbg_Outf( "libdeps: failed; invalid header "
			"[%.2X,%.2X,%.2X,%.2X,%.2X,%.2X,%.2X,%.2X]{\"%.8s\"}\n",
			buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[6],buf[7],
			buf );
#endif
		return;
	}

	if( !fread( &n, sizeof( n ), 1, fp ) ) {
		fclose( fp );
#if MK_DEBUG_LIBDEPS_ENABLED
		Dbg_Outf( "libdeps: failed; no item count\n" );
#endif
		return;
	}

	for( i=0; i<n; ++i ) {
		if( !fread( &temp, sizeof( temp ), 1, fp ) ) {
			fclose( fp );
#if MK_DEBUG_LIBDEPS_ENABLED
			Dbg_Outf( "libdeps: failed; item %u -- missing length\n",
				( unsigned int )i );
#endif
			return;
		}

		if( temp >= sizeof( buf ) ) {
			fclose( fp );
#if MK_DEBUG_LIBDEPS_ENABLED
			Dbg_Outf( "libdeps: failed; item %u is too long (%u >= %u)\n",
				( unsigned int )i, ( unsigned int )temp,
				( unsigned int )sizeof( buf ) );
#endif
			while( i > 0 ) {
				SL_PopBack( proj->libs );
				--i;
			}
			return;
		}

		if( !fread( &buf[0], temp, 1, fp ) ) {
			fclose( fp );
#if MK_DEBUG_LIBDEPS_ENABLED
			Dbg_Outf( "libdeps: failed; item %u -- missing data\n",
				( unsigned int )i );
#endif
			while( i > 0 ) {
				SL_PopBack( proj->libs );
				--i;
			}
			return;
		}
		buf[ temp ] = '\0';

		SL_PushBack( proj->libs, buf );
	}

	fclose( fp );
	
#if MK_DEBUG_LIBDEPS_ENABLED
	Dbg_Outf( "libdeps: succeeded\n" );
#endif
		
	SL_Unique( proj->libs );

#if MK_DEBUG_LIBDEPS_ENABLED
	Dbg_Enter( "libdeps-project(\"%s\")", proj->name );
	n = SL_Size( proj->libs );
	for( i = 0; i < n; ++i ) {
		Dbg_Outf( "\"%s\"\n", SL_Get( proj->libs, i ) );
	}
	Dbg_Leave();
#endif
}

void Prj_CalcDeps(project_t proj) {
	if( proj->status & kProjStat_CalcDeps_Bit ) {
		return;
	}
	proj->status |= kProjStat_CalcDeps_Bit;

	/* <BUG> requires rebuild if a library is removed */
	Prj__LoadLibDeps( proj );
	/* </BUG> */

	Prj__ExpandLibDeps_r( proj );
	Prj__SaveLibDeps( proj );
}

/* given an array of library names, return a string of flags */
void Prj_CalcLibFlags(project_t proj) {
	static char flags[32768];
	const char *libname;
	size_t i, n;
	lib_t lib;

	if( proj->status & kProjStat_LibFlags_Bit ) {
		return;
	}

	proj->status |= kProjStat_LibFlags_Bit;

	/*
	 * TODO: This would be a good place to parse the extra libs of the project
	 *       to remove any "!blah" linker flags. Also, it would be good to check
	 *       if the extra libs is referring to a Mk-known library (e.g.,
	 *       "opengl"), or a command-line setting as well (e.g., "-lGL") as the
	 *       latter is not very platform friendly. Elsewhere, conditionals could
	 *       be checked as well. (e.g., "mswin:-lopengl32 linux:-lGL" etc...)
	 *       For now, the current system (just extra linker flags) will work.
	 */

	Prj_CalcDeps(proj);

	/* find libraries */
	n = SL_Size(proj->libs);
	for( i=0; i<n; i++ ) {
		if( !(libname = SL_Get(proj->libs, i)) ) {
			continue;
		}

		if( !(lib = Lib_Find(libname)) ) {
			/*
			Log_ErrorMsg(Com_Va("couldn't find library ^E\"%s\"^&", libname));
			*/
			continue;
		}

#if MK_DEBUG_LIBDEPS_ENABLED
		Dbg_Outf( "libdeps: \"%s\" <- \"%s\"\n", proj->name, libname );
#endif

		/*
		 * NOTE: Projects are managed as dependencies; not as linker flags
		 */
		if( lib->proj ) {
			continue;
		}

		Prj_AddLinkFlag(proj, Lib_Flags(lib, proj->sys));
	}

	/* add these flags */
	Prj_CompleteExtraLibs(proj, flags, sizeof(flags));
	Prj_AddLinkFlag(proj, flags);
}

/* find a project by name */
project_t Prj_Find_r(project_t from, const char *name) {
	project_t p, r;

	ASSERT( from != (project_t)0 );
	ASSERT( name != (const char *)0 );

	if( !strcmp(Prj_Name(from), name) ) {
		return from;
	}

	for( p=Prj_Head(from); p; p=Prj_Next(p) ) {
		r = Prj_Find_r(p, name);
		if( r != (project_t)0 ) {
			return r;
		}
	}

	return (project_t)0;
}
project_t Prj_Find(const char *name) {
	project_t p, r;

	ASSERT( name != (const char *)0 );

	for( p=Prj_RootHead(); p; p=Prj_Next(p) ) {
		r = Prj_Find_r(p, name);
		if( r != (project_t)0 ) {
			return r;
		}
	}

	return (project_t)0;
}

/*
 *	========================================================================
 *	DIRECTORY MANAGEMENT
 *	========================================================================
 */

/* determine if the directory name specified is special */
int Prj_IsSpecDir(project_t proj, const char *name) {
	static const char *specialdirs[] = {
		MK_PLATFORM_OS_NAME_MSWIN,
		MK_PLATFORM_OS_NAME_UWP,
		MK_PLATFORM_OS_NAME_CYGWIN,
		MK_PLATFORM_OS_NAME_LINUX,
		MK_PLATFORM_OS_NAME_MACOSX,
		MK_PLATFORM_OS_NAME_UNIX,
		
		MK_PLATFORM_CPU_NAME_X86,
		MK_PLATFORM_CPU_NAME_X64,
		MK_PLATFORM_CPU_NAME_ARM,
		MK_PLATFORM_CPU_NAME_AARCH64,
		MK_PLATFORM_CPU_NAME_MIPS,
		MK_PLATFORM_CPU_NAME_PPC,
		MK_PLATFORM_CPU_NAME_WASM,

		"appexec", "exec", "lib", "dylib"
	};
	size_t i;

	/* check the target system */
	switch(proj->sys) {
#define MK__TRY_PLAT(ProjSys_,Plat_) \
	case ProjSys_: \
		if( !strcmp(name, Plat_) ) { \
			return 1; \
		} \
		break
		
	MK__TRY_PLAT(kProjSys_MSWin , MK_PLATFORM_OS_NAME_MSWIN );
	MK__TRY_PLAT(kProjSys_UWP   , MK_PLATFORM_OS_NAME_UWP   );
	MK__TRY_PLAT(kProjSys_Cygwin, MK_PLATFORM_OS_NAME_CYGWIN);
	MK__TRY_PLAT(kProjSys_Linux , MK_PLATFORM_OS_NAME_LINUX );
	MK__TRY_PLAT(kProjSys_MacOSX, MK_PLATFORM_OS_NAME_MACOSX);
	MK__TRY_PLAT(kProjSys_Unix  , MK_PLATFORM_OS_NAME_UNIX  );

#undef MK__TRY_PLAT

	default:
		/*
		 *	NOTE: If you get this message, it's likely because you failed to add
		 *	      the appropriate case statement to this switch. Make sure you
		 *	      port this function too, when adding a new target system.
		 */
		Log_ErrorMsg(Com_Va("unknown project system ^E'%d'^&", proj->sys));
		break;
	}

	/* check the target architecture */
	switch(proj->arch) {
#define MK__TRY_PLAT(ProjArch_,Plat_) \
	case ProjArch_: \
		if( !strcmp(name, Plat_) ) { \
			return 1; \
		} \
		break

	MK__TRY_PLAT(kProjArch_X86        , MK_PLATFORM_CPU_NAME_X86    );
	MK__TRY_PLAT(kProjArch_X86_64     , MK_PLATFORM_CPU_NAME_X64    );
	MK__TRY_PLAT(kProjArch_ARM        , MK_PLATFORM_CPU_NAME_ARM    );
	MK__TRY_PLAT(kProjArch_AArch64    , MK_PLATFORM_CPU_NAME_AARCH64);
	MK__TRY_PLAT(kProjArch_PowerPC    , MK_PLATFORM_CPU_NAME_PPC    );
	MK__TRY_PLAT(kProjArch_MIPS       , MK_PLATFORM_CPU_NAME_MIPS   );
	MK__TRY_PLAT(kProjArch_WebAssembly, MK_PLATFORM_CPU_NAME_WASM   );		

#undef MK__TRY_PLAT

	default:
		/*
		 *	NOTE: If you get this message, it's likely because you failed to add
		 *	      the appropriate case statement to this switch. Make sure you
		 *	      port this function too, when adding a new target architecture.
		 */
		Log_ErrorMsg(Com_Va("unknown project architecture ^E'%d'^&", proj->arch));
		break;
	}

	/* check the project type */
	switch(Prj_Type(proj)) {
	case kProjType_Application:
		if( !strcmp(name, "appexec") ) {
			return 1;
		}
		break;
	case kProjType_Executable:
		if( !strcmp(name, "exec") ) {
			return 1;
		}
		break;
	case kProjType_Library:
		if( !strcmp(name, "lib") ) {
			return 1;
		}
		break;
	case kProjType_DynamicLibrary:
		if( !strcmp(name, "dylib") ) {
			return 1;
		}
		break;
	default:
		/*
		 *	NOTE: If you get this message, it's likely because you failed to add
		 *	      the appropriate case statement to this switch. Make sure you
		 *	      port this function too, when adding a new binary target.
		 */
		Log_ErrorMsg(Com_Va("unknown project type ^E'%d'^&", proj->type));
		break;
	}

	/*
	 * XXX: STUPID HACK! Special directories cannot be included if their
	 *      condition does not match. We're testing for all of the special
	 *      directories again here to ensure this doesn't happen now that any
	 *      non-project-owning directory gets scavanged for source files.
	 */
	for( i=0; i<sizeof(specialdirs)/sizeof(specialdirs[0]); i++ ) {
		if( !strcmp(name, specialdirs[i]) ) {
			return -1;
		}
	}

	/*
	 *	PLAN: Add a ".freestanding" (or perhaps ".native?") indicator file to
	 *        support OS/kernel development. Would be cool.
	 */

	return 0;
}

/* determine whether the directory is an include directory */
int Prj_IsIncDir(project_t proj, const char *name) {
	static const char *const names[] = {
		"inc",
		"incs",
		"include",
		"includes",
		"headers"
	};
	size_t i;

	(void)proj;

	for( i=0; i<sizeof(names)/sizeof(names[0]); i++ ) {
		if( !strcmp(name, names[i]) ) {
			return 1;
		}
	}

	return 0;
}

/* determine whether the directory is a library directory */
int Prj_IsLibDir(project_t proj, const char *name) {
	static const char *const names[] = {
		"lib",
		"libs",
		"library",
		"libraries"
	};
	size_t i;

	(void)proj;

	for( i=0; i<sizeof(names)/sizeof(names[0]); i++ ) {
		if( !strcmp(name, names[i]) ) {
			return 1;
		}
	}

	return 0;
}

/* determine if the directory is a unit testing directory */
int Prj_IsTestDir(project_t proj, const char *name) {
	if( proj ) {}

	if( !strcmp(name, "uts" )
	 || !strcmp(name, "tests")
	 || !strcmp(name, "units")) {
		return 1;
	}

	return 0;
}

/* determine whether a directory owns a project indicator file or not */
int Prj_IsDirOwner(const char *path) {
	fs_stat_t s;
	size_t i;

	for( i=0; i<sizeof(g_ifiles)/sizeof(g_ifiles[0]); i++ ) {
		if( stat(Com_Va("%s%s", path, g_ifiles[i]), &s) != -1 ) {
			return 1;
		}
	}

	return 0;
}

/* enumerate all source files in a directory */
static void Prj_EnumSources__Impl(project_t proj, const char *srcdir) {
	struct dirent *dp;
	char path[PATH_MAX], *p;
	DIR *d;
	int r;

	ASSERT( proj != (project_t)0 );
	ASSERT( srcdir != (const char *)0 );

	if( !(d = FS_OpenDir(srcdir)) ) {
		Log_Error(srcdir, 0, 0, "FS_OpenDir() call in Prj_EnumSources() failed");
		return;
	}

	while( (dp = FS_ReadDir(d)) != (struct dirent *)0 ) {
		Com_Strcpy(path, sizeof(path), srcdir);
		Com_Strcat(path, sizeof(path), dp->d_name);

		p = strrchr(dp->d_name, '.');
		if( p != ( char * )0 ) {
			if( *(p+1)==0 ) {
				continue;
			}

			if( !strcmp(p, ".c" )
			 || !strcmp(p, ".cc")
			 || !strcmp(p, ".cpp")
			 || !strcmp(p, ".cxx")
			 || !strcmp(p, ".c++")
			 || !strcmp(p, ".m")
			 || !strcmp(p, ".mm")) {
				if( !FS_IsFile(path) ) {
					continue;
				}

				Prj_AddSource(proj, path);
				continue;
			}
		}

		if( !FS_IsDir(path) ) {
			errno = 0;
			continue;
		}

		/* the following tests all require an ending '/' */
		Com_Strcat(path, sizeof(path), "/");

		r = Prj_IsSpecDir(proj, dp->d_name); /*can now return -1*/
		if( r!=0 ) {
			if( r==1 ) {
				Prj_AddSpecDir(proj, dp->d_name);
				Prj_EnumSources(proj, path);
			}
			continue;
		}

		if( Prj_IsIncDir(proj, dp->d_name) ) {
			Mk_PushIncDir(path);
			AL_ManagePackage_r(proj->name, proj->sys, path);
			continue;
		}

		if( Prj_IsLibDir(proj, dp->d_name) ) {
			if( proj->config & kProjCfg_Package_Bit ) {
				Prj_SetOutPath(proj, path);
			}
			Mk_PushLibDir(path);
			continue;
		}

		if( Prj_IsTestDir(proj, dp->d_name) ) {
			Prj_EnumTests(proj, path);
			continue;
		}

		if( !Prj_IsDirOwner(path) ) {
			Prj_EnumSources(proj, path);
			continue;
		}
	}

	FS_CloseDir(d);

	if( errno ) {
		Log_Error(srcdir, 0, 0, "Prj_EnumSources() failed");
	}
}
void Prj_EnumSources( project_t proj, const char *srcdir ) {
	Dbg_Enter( "Prj_EnumSources(project:\"%s\", srcdir:\"%s\")", proj->name, srcdir );
	Prj_EnumSources__Impl( proj, srcdir );
	Dbg_Leave();
}

/* enumerate all unit tests in a directory */
static void Prj_EnumTests__Impl(project_t proj, const char *srcdir) {
	struct dirent *dp;
	char path[PATH_MAX], *p;
	DIR *d;

	ASSERT( proj != (project_t)0 );
	ASSERT( srcdir != (const char *)0 );

	if( !(d = FS_OpenDir(srcdir)) ) {
		Log_Error(srcdir, 0, 0, "FS_OpenDir() call in Prj_EnumTests() failed");
		return;
	}

	while( (dp = FS_ReadDir(d)) != (struct dirent *)0 ) {
		Com_Strcpy(path, sizeof(path), srcdir);
		Com_Strcat(path, sizeof(path), dp->d_name);

		p = strrchr(dp->d_name, '.');
		if( p ) {
			if( *(p+1)==0 ) {
				continue;
			}

			if( !strcmp(p, ".c" )
			 || !strcmp(p, ".cc")
			 || !strcmp(p, ".cpp")
			 || !strcmp(p, ".cxx")
			 || !strcmp(p, ".c++")
			 || !strcmp(p, ".m")
			 || !strcmp(p, ".mm")) {
				if( !FS_IsFile(path) ) {
					continue;
				}

				Prj_AddTestSource(proj, path);
				continue;
			}
		}

		if( !FS_IsDir(path) ) {
			errno = 0;
			continue;
		}

		if( Prj_IsSpecDir(proj, dp->d_name) ) {
			Prj_AddSpecDir(proj, dp->d_name);
			Com_Strcat(path, sizeof(path), "/"); /* ending '/' is necessary */
			Prj_EnumTests(proj, path);
		}
	}

	FS_CloseDir(d);

	if( errno ) {
		Log_Error(srcdir, 0, 0, "Prj_EnumTests() failed");
	}
}
void Prj_EnumTests( project_t proj, const char *srcdir ) {
	ASSERT( proj != ( project_t )0 );
	ASSERT( proj->name != ( const char * )0 );
	
	Dbg_Enter( "Prj_EnumTests(project:\"%s\", srcdir:\"%s\")", proj->name, srcdir );
	Prj_EnumTests__Impl( proj, srcdir );
	Dbg_Leave();
}

/* calculate the name of a project based on a directory or file */
int Prj_CalcName(project_t proj, const char *path, const char *file) {
	size_t i;
	FILE *f;
	char buf[256], cwd[256], *p;

	ASSERT( proj != (project_t)0 );
	ASSERT( path != (const char *)0 );

	/* read a single line from the file */
	if( file != (const char *)0 ) {
		if( !(f = fopen(file, "r")) ) {
			Log_Error(file, 0, 0, "fopen() call in Prj_CalcName() failed");
			return 0;
		}

		buf[0] = 0;
		fgets(buf, sizeof(buf), f);

		fclose(f);

		/* strip the line of whitespace on both ends */
		i = 0;
		while( buf[i]<=' ' && buf[i]!=0 ) {
			i++;
		}

		for( p=&buf[i]; *p>' '; p++ );
		if( *p!=0 ) {
			*p = 0;
		}
	} else {
#if 0
		/*
		 * XXX: Why did I use 'cwd' instead of path? ...
		 */
		FS_GetCWD(cwd, sizeof(cwd));
#else
		Com_Strcpy(cwd, sizeof(cwd), path);
#endif

		if( !(p = strrchr(cwd, '/')) ) {
			p = cwd;
		} else if(*(p+1)==0) {
			if( strcmp(cwd, "/") != 0 ) {
				*p = 0;
				p = strrchr(cwd, '/');
				if( !p ) {
					p = cwd;
				}
			}
		}

		ASSERT( p != (char *)0 );

		if( p!=cwd && *p=='/' ) {
			p++;
		}

		Com_Strcpy(buf, sizeof(buf), p);
		i = 0;
	}

	/* if no name was specified in the file, use the directory name */
	if( !buf[i] ) {
		Com_Strcpy(buf, sizeof(buf), path);

		/* FS_RealPath() always adds a '/' at the end of a directory */
		p = strrchr(buf, '/');

		/* ensure this is the case (debug-mode only) */
		ASSERT( p != (char *)0 );
		ASSERT( *(p+1)==0 );
		*p = 0; /* remove this ending '/' to not conflict with the following */

		/* if there's an ending '/' (always should be), mark the character
		   past that as the start of the directory name */
		i = ((p = strrchr(buf, '/')) != (char *)0) ? (p - buf) + 1 : 0;
	}

	/* set the project's name */
	Prj_SetName(proj, &buf[i]);

	/* done */
	return 1;
}

/* add a single project using 'file' for information */
project_t Prj_Add(project_t prnt, const char *path, const char *file,
int type) {
	project_t proj;

	ASSERT( path != (const char *)0 );

	/* create the project */
	proj = Prj_New(prnt);

	/* calculate the appropriate name for the project */
	if( !Prj_CalcName(proj, path, file) ) {
		Prj_Delete(proj);
		return (project_t)0;
	}

	/* prepare the passed settings of the project */
	Prj_SetPath(proj, path);
	Prj_SetType(proj, type);
	Bld_GetBinName(proj, (char *)0, 0);

	if( proj->type==kProjType_Library || proj->type==kProjType_DynamicLibrary ) {
		proj->lib = Lib_New();
		proj->lib->proj = proj;
		Lib_SetName(proj->lib, proj->name);
		Lib_SetFlags(proj->lib, proj->sys, Com_Va("\"%s\"", proj->binname));
	}

	/* add the sources for the project */
	Prj_EnumSources(proj, path);

	/* return the project */
	return proj;
}

/* find all projects within a specific packages directory */
void Prj_FindPackages(const char *pkgdir) {
	struct dirent *dp;
	project_t proj;
	char path[PATH_MAX];
	DIR *d;

	/* opne the directory to find packages */
	if( !(d = FS_OpenDir(pkgdir)) ) {
		Log_FatalError(pkgdir);
	}

	/* run through each entry in the directory */
	while( (dp=FS_ReadDir(d)) != (struct dirent *)0 ) {
		if( !strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..") ) {
			continue;
		}

		/* validate the directory */
		if( !FS_RealPath(Com_Va("%s/%s/", pkgdir, dp->d_name ), path,
		sizeof(path))) {
			errno = 0;
			continue;
		}

		/* this is a project directory; treat as a static library */
		proj = Prj_Add((project_t)0, path, (const char *)0,
		kProjType_Library);
		if( !proj ) {
			continue;
		}
	}

	/* close this directory */
	FS_CloseDir(d);

	/* if an error occurred, display it */
	if( errno ) {
		Log_FatalError(Com_Va("readdir(\"%s\") failed", pkgdir));
	}
}

/* find all dlls */
void Prj_FindDLLs(const char *dllsdir) {
	struct dirent *dp;
	project_t proj;
	char path[PATH_MAX];
	DIR *d;

	/* opne the directory to find packages */
	if( !(d = FS_OpenDir(dllsdir)) ) {
		Log_FatalError(dllsdir);
	}

	/* run through each entry in the directory */
	while( (dp=FS_ReadDir(d)) != (struct dirent *)0 ) {
		if( !strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..") ) {
			continue;
		}

		/* validate the directory */
		if( !FS_RealPath(Com_Va("%s/%s/", dllsdir, dp->d_name ), path,
		sizeof(path))) {
			errno = 0;
			continue;
		}

		/* this is a project directory; treat as a static library */
		proj = Prj_Add((project_t)0, path, (const char *)0,
		kProjType_DynamicLibrary);
		if( !proj ) {
			continue;
		}
	}

	/* close this directory */
	FS_CloseDir(d);

	/* if an error occurred, display it */
	if( errno ) {
		Log_FatalError(Com_Va("readdir(\"%s\") failed", dllsdir));
	}
}

/* find all tools */
void Prj_FindTools(const char *tooldir) {
	struct dirent *dp;
	project_t proj;
	char path[PATH_MAX];
	DIR *d;

	/* opne the directory to find packages */
	if( !(d = FS_OpenDir(tooldir)) ) {
		Log_FatalError(tooldir);
	}

	/* run through each entry in the directory */
	while( (dp=FS_ReadDir(d)) != (struct dirent *)0 ) {
		if( !strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..") ) {
			continue;
		}

		/* validate the directory */
		if( !FS_RealPath(Com_Va("%s/%s/", tooldir, dp->d_name ), path,
		sizeof(path))) {
			errno = 0;
			continue;
		}

		/* this is a project directory; treat as a static library */
		proj = Prj_Add((project_t)0, path, (const char *)0,
		kProjType_Executable);
		if( !proj ) {
			continue;
		}
	}

	/* close this directory */
	FS_CloseDir(d);

	/* if an error occurred, display it */
	if( errno ) {
		Log_FatalError(Com_Va("readdir(\"%s\") failed", tooldir));
	}
}

/* find all projects within a specific source directory, adding them as children
   to the 'prnt' project. */
void Prj_FindProjects(project_t prnt, const char *srcdir) {
	static const char *libs[] = { ".libs", ".user.libs", ".mk-libs",
		".user.mk-libs" };
	static char buf[32768];
	struct dirent *dp;
	fs_stat_t s;
	project_t proj;
	size_t i, j;
	FILE *f;
	char path[PATH_MAX], file[PATH_MAX], *p;
	DIR *d;

	proj = (project_t)0;

	/* open the source directory to add projects */
	if( !(d = FS_OpenDir(srcdir)) ) {
		Log_FatalError(srcdir);
	}

	/* run through each entry in the directory */
	while( (dp=FS_ReadDir(d)) != (struct dirent *)0 ) {
		if( !strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..") ) {
			continue;
		}

		/* make sure this is a directory; get the real (absolute) path */
		if( !FS_RealPath(Com_Va("%s/%s/", srcdir, dp->d_name ), path,
		sizeof(path))) {
			errno = 0;
			continue;
		}

		/* run through each indicator file test (.executable, ...) */
		for( i=0; i<sizeof(g_ifiles)/sizeof(g_ifiles[0]); i++ ) {
			Com_Strcpy(file, sizeof(file), Com_Va("%s%s", path, g_ifiles[i].name));

			/* make sure this file exists */
			if( !FS_IsFile(file) ) {
				errno = 0;
				continue;
			}

			/* add the project file */
			proj = Prj_Add(prnt, path, file, g_ifiles[i].type);
			if( !proj ) {
				continue;
			}

			/* if this entry contains a '.libs' file, then add to the project */
			for( j=0; j<sizeof(libs)/sizeof(libs[0]); j++ ) {
				Com_Strcpy(file, sizeof(file), Com_Va("%s/%s", srcdir, libs[j]));

				if( stat(file, &s)!=0 ) {
					continue;
				}

				f = fopen(file, "r");
				if( f ) {
					if( fgets(buf, sizeof(buf), f)!=(char *)0 ) {
						p = strchr(buf, '\r');
						if( p ) {
							*p = '\0';
						}

						p = strchr(buf, '\n');
						if( p ) {
							*p = '\0';
						}

						Prj_AppendExtraLib(proj, buf);
					} else {
						Log_Error(file, 1, 0, "failed to read line");
					}

					fclose(f);
					f = (FILE *)0;
				} else {
					Log_Error(file, 0, 0, "failed to open for reading");
				}
			}

			/* find sub-projects */
			Prj_FindProjects(proj, path);
		}
	}

	/* close this directory */
	FS_CloseDir(d);

	/* if an error occurred, display it */
	if( errno ) {
		Log_FatalError(Com_Va("readdir(\"%s\") failed", srcdir));
	}
}

/* find the root directories, adding projects for any of the 'srcdirs' */
void Prj_FindRootDirs(strList_t srcdirs, strList_t incdirs, strList_t libdirs,
strList_t pkgdirs, strList_t tooldirs, strList_t dllsdirs) {
	struct { const char *name; strList_t dst; } tests[] = {
		{ "src", (strList_t)0 }, { "source"    , (strList_t)0 },
		                       { "code"      , (strList_t)0 },
		                       { "projects"  , (strList_t)0 },
		{ "inc", (strList_t)1 }, { "include"   , (strList_t)1 },
		                       { "headers"   , (strList_t)1 },
		                       { "includes"  , (strList_t)1 },
		{ "lib", (strList_t)2 }, { "library"   , (strList_t)2 },
		                       { "libraries" , (strList_t)2 },
		                       { "libs"      , (strList_t)2 },
		{ "pkg", (strList_t)3 }, { "packages"  , (strList_t)3 },
		                       { "repository", (strList_t)3 },
		                       { "repo"      , (strList_t)3 },
		{ "tools", (strList_t)4 }, { "games", (strList_t)4 },
		{ "apps", (strList_t)4 }, { "dlls", (strList_t)5 },
		{ "sdk/include" , (strList_t)1 },
		{ "sdk/lib"     , (strList_t)2 },
		{ "sdk/packages", (strList_t)3 },
		{ "sdk/tools"   , (strList_t)4 },
		{ "sdk/dlls"    , (strList_t)5 }
	};
	static const char *workspaces[] = { ".workspace", ".user.workspace",
		".mk-workspace", ".user.mk-workspace" };
	static char buf[32768];
	fs_stat_t s;
	project_t proj;
	size_t i, n;
	FILE *f;
	char *p;
	char path[256];

	ASSERT( srcdirs  != (strList_t)0 );
	ASSERT( incdirs  != (strList_t)0 );
	ASSERT( libdirs  != (strList_t)0 );
	ASSERT( pkgdirs  != (strList_t)0 );
	ASSERT( tooldirs != (strList_t)0 );
	ASSERT( dllsdirs != (strList_t)0 );

	/* "initializer element is not computable at load time" warning -- fix */
	for( i=0; i<sizeof(tests)/sizeof(tests[0]); i++ ) {
		if( tests[i].dst==(strList_t)0 ) {
			tests[i].dst = srcdirs;
		} else if(tests[i].dst==(strList_t)1) {
			tests[i].dst = incdirs;
		} else if(tests[i].dst==(strList_t)2) {
			tests[i].dst = libdirs;
		} else if(tests[i].dst==(strList_t)4) {
			tests[i].dst = tooldirs;
		} else if(tests[i].dst==(strList_t)5) {
			tests[i].dst = dllsdirs;
		} else {
			ASSERT( tests[i].dst==(strList_t)3 );
			tests[i].dst = pkgdirs;
		}
	}

	/* run through each of the tests in the 'tests' array to see which
	   directories will be used in the build... */
	for( i=0; i<sizeof(tests)/sizeof(tests[0]); i++ ) {
		ASSERT( tests[i].name != (const char *)0 );
		ASSERT( tests[i].dst != (strList_t)0 );

		if( SL_Size(tests[i].dst) > 0 ) {
			continue;
		}

		if( stat(tests[i].name, &s)==-1 ) {
			continue;
		}
		if( ~s.st_mode & S_IFDIR ) {
			continue;
		}

		SL_PushBack(tests[i].dst, tests[i].name);
	}

	/* use the .workspace file to add extra directories */
	for( i=0; i<sizeof(workspaces)/sizeof(workspaces[0]); i++ ) {
		f = fopen(workspaces[i], "r");
		if( f ) {
			/* every line of the file specifies another directory */
			while( fgets(buf, sizeof(buf), f) != (char *)0 ) {
				p = strchr(buf, '\r');
				if( p ) {
					*p = '\0';
				}

				p = strchr(buf, '\n');
				if( p ) {
					*p = '\0';
				}

				if( !strncmp(buf, "src:", 4) ) {
					Mk_PushSrcDir(&buf[4]);
					continue;
				}
				if( !strncmp(buf, "inc:", 4) ) {
					Mk_PushIncDir(&buf[4]);
					continue;
				}
				if( !strncmp(buf, "lib:", 4) ) {
					Mk_PushLibDir(&buf[4]);
					continue;
				}
				if( !strncmp(buf, "pkg:", 4) ) {
					Mk_PushPkgDir(&buf[4]);
					continue;
				}
				if( !strncmp(buf, "tool:", 5) ) {
					Mk_PushToolDir(&buf[5]);
					continue;
				}
				if( !strncmp(buf, "dlls:", 5) ) {
					Mk_PushDLLsDir(&buf[5]);
					continue;
				}
			}

			fclose(f);
		}
	}

	errno = 0;

	/* find packages first */
	n = SL_Size(pkgdirs);
	for( i=0; i<n; i++ ) {
		Prj_FindPackages(SL_Get(pkgdirs, i));
	}

	/* find dlls */
	n = SL_Size(dllsdirs);
	for( i=0; i<n; i++ ) {
		Prj_FindDLLs(SL_Get(dllsdirs, i));
	}

	/* find tools next */
	n = SL_Size(tooldirs);
	for( i=0; i<n; i++ ) {
		Prj_FindTools(SL_Get(tooldirs, i));
	}

	/* we'll try to find projects in the source directories */
	n = SL_Size(srcdirs);
	for( i=0; i<n; i++ ) {
		Prj_FindProjects((project_t)0, SL_Get(srcdirs, i));
	}

	/* if we found no projects in srcdirs then treat the current directory as a
	   project */
	if( !Prj_RootHead() && n>0 ) {
		if( !FS_RealPath("./", path, sizeof(path)) ) {
			return;
		}

		proj = Prj_New((project_t)0);

		if( !Prj_CalcName(proj, path, (const char *)0) ) {
			Prj_Delete(proj);
			return;
		}

		Com_Strcat(path, sizeof(path), Com_Va("%s/", SL_Get(srcdirs, 0)));

		Prj_SetPath(proj, path);
		Prj_SetType(proj, kProjType_Executable);

		Prj_EnumSources(proj, path);
	}
}

/* create the object directories for a particular project */
int Prj_MakeObjDirs(project_t proj) {
	const char *typename = "";
	size_t i, n;
	char objdir[PATH_MAX], cwd[PATH_MAX];

	ASSERT( proj != (project_t)0 );

	if( getcwd(cwd, sizeof(cwd))==(char *)0 ) {
		Log_ErrorMsg("getcwd() failed");
		return 0;
	}

	n = Com_Strlen(cwd);

	switch(Prj_Type(proj)) {
	case kProjType_Application:
		typename = "appexec";
		break;
	case kProjType_Executable:
		typename = "exec";
		break;
	case kProjType_Library:
		typename = "lib";
		break;
	case kProjType_DynamicLibrary:
		typename = "dylib";
		break;
	default:
		ASSERT_MSG(0, "project type is invalid");
		break;
	}

#if __STDC_WANT_SECURE_LIB__
# define snprintf sprintf_s
#endif
	snprintf(objdir, sizeof(objdir)-1, "%s/%s/%s/%s",
		Opt_GetObjdirBase(),
		Opt_GetConfigName(), typename,
		&Prj_Path(proj)[n]);
	objdir[sizeof(objdir)-1] = 0;
#if __STDC_WANT_SECURE_LIB__
# undef snprintf
#endif

	if( ~g_flags & kFlag_NoCompile_Bit ) {
		FS_MkDirs(objdir);
	}

#if 0
	n = Prj_SpecDirCount(proj);
	for( i=0; i<n; i++ )
		FS_MkDirs(Com_Va("%s%s/", objdir, Prj_SpecDir(proj, i)));
#else
	n = SL_Size(proj->sources);
	for( i=0; i<n; i++ ) {
		char rel[PATH_MAX];
		char *p;
# ifdef _WIN32
		char *q;
# endif

		Com_RelPath(rel, sizeof(rel), proj->path, SL_Get(proj->sources, i));
		p = strrchr( rel, '/' );
# ifdef _WIN32
		q = strrchr( rel, '\\' );
		if( q != NULL && q > p ) {
			p = q;
		}
# endif
		if( p != NULL ) {
			*p = '\0';
		}
		SL_PushBack(proj->srcdirs, rel);
	}
#endif

	SL_Unique(proj->srcdirs);
	if( ~g_flags & kFlag_NoCompile_Bit ) {
		n = SL_Size(proj->srcdirs);
		for( i=0; i<n; i++ ) {
			FS_MkDirs(Com_Va("%s%s/", objdir, SL_Get(proj->srcdirs, i)));
		}
	}

	return 1;
}

/*
 *	========================================================================
 *	COMPILATION MANAGEMENT
 *	========================================================================
 */

#if !MK_DEBUG_ENABLED
# undef  MK_DEBUG_FIND_SOURCE_LIBS_ENABLED
# define MK_DEBUG_FIND_SOURCE_LIBS_ENABLED 0
#endif

strList_t g_unitTestCompiles = (strList_t)0;
strList_t g_unitTestRuns = (strList_t)0;
strList_t g_unitTestNames = (strList_t)0;

/* initialize unit test arrays */
void Bld_InitUnitTestArrays() {
	g_unitTestCompiles = SL_New();
	g_unitTestRuns = SL_New();
	g_unitTestNames = SL_New();
}

/* find which libraries are to be autolinked from a source file */
int Bld_FindSourceLibs(strList_t dst, int sys, const char *obj, const char *dep) {
	autolink_t al;
	size_t i, n;
	dep_t d;
	lib_t l;

#if MK_DEBUG_FIND_SOURCE_LIBS_ENABLED
	Dbg_Outf("Bld_FindSourceLibs: \"%s\", \"%s\"\n", obj, dep);
#endif

	d = Dep_Find(obj);
	if( !d ) {
		if( !Dep_Load(dep) ) {
			Log_ErrorMsg(Com_Va("failed to read dependency ^F\"%s\"^&", dep));
			return 0;
		}

		d = Dep_Find(obj);
		if( !d ) {
			Log_ErrorMsg(Com_Va("Dep_Find(^F\"%s\"^&) failed", obj));
			Dep_DebugPrintAll();
			return 0;
		}
	}

	n = Dep_Size(d);
	for( i=0; i<n; i++ ) {
		if( !(al = AL_Find(sys, Dep_Get(d, i))) ) {
			continue;
		}

#if MK_DEBUG_FIND_SOURCE_LIBS_ENABLED
		Dbg_Outf("  found dependency on \"%s\"; investigating\n", al->lib);
#endif

		l = Lib_Find(al->lib);
		if( !l ) {
#if MK_DEBUG_FIND_SOURCE_LIBS_ENABLED
			Dbg_Outf("   -ignoring because did not find associated lib\n");
#endif
			continue;
		}

		/* a project does not have a dependency on itself (for linking
		 * purposes); ignore */
		if( l->proj && l->proj->libs==dst ) {
#if MK_DEBUG_FIND_SOURCE_LIBS_ENABLED
			Dbg_Outf("   -ignoring because is current project\n");
#endif
			continue;
		}

		SL_PushBack(dst, AL_GetLib(al));
#if MK_DEBUG_FIND_SOURCE_LIBS_ENABLED
		Dbg_Outf("   +keeping\n", AL_GetLib(al));
#endif
	}

	SL_Unique(dst);
	return 1;
}

/* determine whether a source file should be built */
int Bld_ShouldCompile(const char *obj) {
	fs_stat_t s, obj_s;
	size_t i, n;
	dep_t d;
	char dep[PATH_MAX];

	ASSERT( obj != (const char *)0 );

	if( g_flags & kFlag_Rebuild_Bit ) {
		return 1;
	}
	if( g_flags & kFlag_NoCompile_Bit ) {
		return 0;
	}

	if( stat(obj, &obj_s)==-1 ) {
		return 1;
	}

	Com_SubstExt(dep, sizeof(dep), obj, ".d");

	if( stat(dep, &s)==-1 ) {
		return 1;
	}

	d = Dep_Find(obj);
	if( !d ) {
		if( !Dep_Load(dep) ) {
			return 1;
		}

		d = Dep_Find(obj);
		if( !d ) {
			return 1;
		}
	}

	n = Dep_Size(d);
	for( i=0; i<n; i++ ) {
		if( stat(Dep_Get(d, i), &s)==-1 ) {
			return 1; /* need recompile for new dependency list; this file is
			             (potentially) missing */
		}

		if( obj_s.st_mtime <= s.st_mtime ) {
			return 1;
		}
	}

	return 0; /* no reason to rebuild */
}

/* determine whether a project should be linked */
int Bld_ShouldLink(const char *bin, int numbuilds) {
	fs_stat_t bin_s;

	ASSERT( bin != (const char *)0 );

	if( g_flags & kFlag_Rebuild_Bit ) {
		return 1;
	}
	if( g_flags & kFlag_NoLink_Bit ) {
		return 0;
	}

	if( stat(bin, &bin_s)==-1 ) {
		return 1;
	}

	if( numbuilds != 0 ) {
		return 1;
	}

	return 0;
}

/* retrieve the compiler to use */
const char *Bld_GetCompiler(int iscxx) {
	static char cc[128], cxx[128];
	static int didinit = 0;

	/*
	 *	TODO: Check command-line option
	 */

	if( !didinit ) {
		const char *p;
		didinit = 1;

		p = getenv("CC");
		if( p != ( const char * )0 ) {
			Com_Strcpy(cc,sizeof(cc),p);
		} else {
#ifdef __clang__
			Com_Strcpy(cc,sizeof(cc),"clang");
#else
			Com_Strcpy(cc,sizeof(cc),"gcc");
#endif
		}

		p = getenv("CXX");
		if( p != ( const char * )0 ) {
			Com_Strcpy(cxx,sizeof(cxx),p);
		} else {
			if( strcmp(cc,"clang")==0) {
				Com_Strcpy(cxx,sizeof(cxx),"clang++");
			} else {
				char *q;
				Com_Strcpy(cxx,sizeof(cxx),cc);
				q = strstr(cxx,"gcc");
				if( q != ( char * )0 ) {
					q[1]='+';
					q[2]='+';
				}
			}
		}
	}

	return iscxx ? cxx : cc;
}

/* retrieve the warning flags for compilation */
void Bld_GetCFlags_Warnings(char *flags, size_t nflags) {
	/*
	 *	TODO: Allow the front-end to override the warning level
	 */

	/* cl: /Wall */
	Com_Strcat(flags, nflags, "-W -Wall -Wextra -Warray-bounds -pedantic ");
}
/* figure out the standard :: returns 1 if c++ file */
int Bld_GetCFlags_Standard(char *flags, size_t nflags, const char *filename) {
	const char *p;
	int iscplusplus;

	iscplusplus = 0;

	p = strrchr(filename, '.');
	if( !p ) {
		p = filename;
	}
	
	if( !strcmp(p, ".cc") || !strcmp(p, ".cxx") || !strcmp(p, ".cpp" )
	|| !strcmp(p, ".c++")) {
		if( g_flags & kFlag_Pedantic_Bit ) {
			Com_Strcat(flags, nflags, "-Weffc++ ");
		}

		Com_Strcat(flags, nflags, "-std=gnu++11 ");
		iscplusplus = 1;
	} else {
#if 0
# ifdef __clang__
		Com_Strcat(flags, nflags, "-std=gnu99 ");
# else
		Com_Strcat(flags, nflags, "-std=gnu11 ");
# endif
#else
		Com_Strcat(flags, nflags, "-std=gnu11 ");
#endif
	}

	return iscplusplus;
}
/* get configuration specific flags */
void Bld_GetCFlags_ReleaseDebug(char *flags, size_t nflags, int projarch) {
	/* optimization/debugging */
	if( g_flags & kFlag_Release_Bit ) {
		/* cl: /DNDEBUG /Og /Ox /Oy /GL /QIfist; /Qpar? (parallel code gen.) */
		Com_Strcat(flags, nflags, "-DNDEBUG -s -O3 -fno-strict-aliasing ");
		switch(projarch) {
		case kProjArch_X86:
			/* cl: /arch:SSE */
			Com_Strcat(flags, nflags, "-fomit-frame-pointer ");
			break;
		case kProjArch_X86_64:
			/* cl: /arch:SSE2 */
			Com_Strcat(flags, nflags, "-fomit-frame-pointer ");
			break;
		default:
			/*
			 *	NOTE: If you have architecture specific optimizations to apply
			 *        to release code, add them here.
			 */
			break;
		}
	} else {
		/* cl: /Zi /D_DEBUG /DDEBUG /D__debug__ */
		Com_Strcat(flags, nflags, "-g3 -D_DEBUG ");
	}
}
/* get platform specific flags */
void Bld_GetCFlags_Platform(char *flags, size_t nflags, int projarch,
int projsys, int usenative) {
	switch(projarch) {
	case kProjArch_X86:
		if( usenative ) {
			Com_Strcat(flags, nflags, "-m32 ");
		} else {
			Com_Strcat(flags, nflags, "-m32 -march=pentium -mtune=core2 ");
		}
		break;
	case kProjArch_X86_64:
		if( usenative ) {
			Com_Strcat(flags, nflags, "-m64 ");
		} else {
			Com_Strcat(flags, nflags, "-m64 -march=core2 -mtune=core2 ");
		}
		break;
	default:
		break;
	}

	if( usenative ) {
#if 0
		Com_Strcat(flags, nflags, "-march=native -mtune=native ");
#endif
	}

	/* add a macro for the target system (some systems don't provide their own,
	   or aren't consistent/useful */
	switch(projsys) {
	case kProjSys_MSWin:
		/* cl: /DMK_MSWIN */
		Com_Strcat(flags, nflags, "-DMK_MSWIN ");
		break;
	case kProjSys_UWP:
		/* cl: /DMK_UWP */
		Com_Strcat(flags, nflags, "-DMK_UWP ");
		break;
	case kProjSys_Cygwin:
		/* cl: /DMK_CYGWIN */
		Com_Strcat(flags, nflags, "-DMK_CYGWIN ");
		break;
	case kProjSys_Linux:
		/* cl: /DMK_LINUX */
		Com_Strcat(flags, nflags, "-DMK_LINUX ");
		break;
	case kProjSys_MacOSX:
		/* cl: /DMK_MACOS */
		Com_Strcat(flags, nflags, "-DMK_MACOSX ");
		break;
	case kProjSys_Unix:
		/* cl: /DMK_UNIX */
		Com_Strcat(flags, nflags, "-DMK_UNIX ");
		break;
	default:
		/*
		 *	PLAN: Add Android/iOS support here too.
		 */
		ASSERT_MSG(0, "project system not handled");
		break;
	}
}
/* get project type (executable, dll, ...) specific flags */
void Bld_GetCFlags_ProjectType(char *flags, size_t nflags, int projtype) {
	/* add a macro for the target build type */
	switch(projtype) {
	case kProjType_Application:
		/* cl: /DAPPLICATION */
		Com_Strcat(flags, nflags, "-DAPPLICATION -DMK_APPLICATION ");
		break;
	case kProjType_Executable:
		/* cl: /DEXECUTABLE */
		Com_Strcat(flags, nflags, "-DEXECUTABLE -DMK_EXECUTABLE ");
		break;
	case kProjType_Library:
		/* cl: /DLIBRARY */
		Com_Strcat(flags, nflags, "-DLIBRARY -DMK_LIBRARY "
			"-DLIB -DMK_LIB ");
		break;
	case kProjType_DynamicLibrary:
		/* cl: /DDYNAMICLIBRARY */
		Com_Strcat(flags, nflags, "-DDYNAMICLIBRARY -DMK_DYNAMICLIBRARY "
			"-DDLL -DMK_DLL ");
			/* " -fPIC" */
		break;
	default:
		ASSERT_MSG(0, "project type not handled");
		break;
	}
}
/* add all include paths */
void Bld_GetCFlags_Includes(char *flags, size_t nflags) {
	size_t i, n;
	
	Com_Strcat(flags, nflags, Com_Va("-I \"%s/..\" ", Opt_GetBuildGenIncDir()));

	/* add the include search paths */
	n = SL_Size(g_incdirs);
	for( i=0; i<n; i++ ) {
		/* cl: "/I \"%s\" " */
		Com_Strcat(flags, nflags, Com_Va("-I \"%s\" ", SL_Get(g_incdirs, i)));
	}
}
/* add all preprocessor definitions */
void Bld_GetCFlags_Definitions(char *flags, size_t nflags, strList_t defs) {
	size_t i, n;

	/* add project definitions */
	n = SL_Size(defs);
	for( i=0; i<n; i++ ) {
		/* cl: "\"/D%s\" " */
		Com_Strcat(flags, nflags, Com_Va("\"-D%s\" ", SL_Get(defs, i)));
	}
}
/* add the input/output flags */
void Bld_GetCFlags_UnitIO(char *flags, size_t nflags, const char *obj,
const char *src) {
	/*
	 *	TODO: Visual C++ and dependencies. How?
	 *	-     We can use /allincludes (or whatevertf it's called) to get them...
	 */

	/* add the remaining flags (e.g., dependencies, compile-only, etc) */
	Com_Strcat(flags, nflags, "-MD -MP -c ");

	/* add the appropriate compilation flags */
	Com_Strcat(flags, nflags, Com_Va("-o \"%s\" \"%s\"", obj, src));
}

/* retrieve the flags for compiling a particular source file */
const char *Bld_GetCFlags(project_t proj, const char *obj, const char *src) {
	static char flags[16384];

	ASSERT( proj != (project_t)0 );
	ASSERT( obj != (const char *)0 );
	ASSERT( src != (const char *)0 );

	flags[0] = '\0';

	Bld_GetCFlags_Warnings(flags, sizeof(flags));
	Bld_GetCFlags_Standard(flags, sizeof(flags), src);
	Bld_GetCFlags_ReleaseDebug(flags, sizeof(flags), proj->arch);
	Bld_GetCFlags_Platform(flags, sizeof(flags), proj->arch, proj->sys, 0);
	Bld_GetCFlags_ProjectType(flags, sizeof(flags), proj->type);
	Bld_GetCFlags_Includes(flags, sizeof(flags));
	Bld_GetCFlags_Definitions(flags, sizeof(flags), proj->defs);
	Bld_GetCFlags_UnitIO(flags, sizeof(flags), obj, src);

	return flags;
}

/* retrieve the dependencies of a project and its subprojects */
void Bld_GetDeps_r(project_t proj, strList_t deparray) {
	static size_t nest = 0;
	static char spaces[512] = { '\0', };
	size_t i, n, j, m;
	const char *libname;
	lib_t lib;
	char src[PATH_MAX], obj[PATH_MAX], dep[PATH_MAX];

	if( !spaces[0] ) {
		for( i=0; i<sizeof(spaces) - 1; i++ ) {
			spaces[i] = ' ';
		}

		spaces[i] = '\0';
	}

	nest++;

	ASSERT( proj != (project_t)0 );
	ASSERT( deparray != (strList_t)0 );

	/* ensure the project is up to date */
	Dbg_Outf(" **** Bld_GetDeps_r \"%s\" ****\n", proj->name);
	n = Prj_SourceCount(proj);
	for( i=0; i<n; i++ ) {
		Com_RelPathCWD(src, sizeof(src), Prj_Source(proj, i));
		Bld_GetObjName(proj, obj, sizeof(obj), src);
		Com_SubstExt(dep, sizeof(dep), obj, ".d");
		Dbg_Outf(" **** src:\"%s\" obj:\"%s\" dep:\"%s\" ****\n",
			src, obj, dep);
		if( !Bld_FindSourceLibs(proj->libs, proj->sys, obj, dep) ) {
			Dbg_Outf("Bld_FindSourceLibs(\"%s\"->libs, %i, \"%s\", \"%s\") "
				"call failed\n", proj->name, proj->sys, obj, dep);
			continue;
		}
	}

	/* check the libraries */
	n = SL_Size(proj->libs);
	for( i=0; i<n; i++ ) {
		libname = SL_Get(proj->libs, i);
		if( !libname ) {
			continue;
		}

		Dbg_Outf("%.*s[dep] %s\n", (nest - 1)*2, spaces, libname);

		lib = Lib_Find(libname);
		if( !lib || !lib->proj || lib->proj==proj ) {
			continue;
		}

		m = SL_Size(deparray);
		for( j=0; j<m; j++ ) {
			if( !strcmp(SL_Get(deparray, j), libname) ) {
				break;
			}
		}

		if( j < m ) {
			continue;
		}

		SL_PushBack(deparray, SL_Get(proj->libs, i));
		Bld_GetDeps_r(lib->proj, deparray);
	}

	nest--;
}

/* determine whether a library depends upon another library */
int Bld_DoesLibDependOnLib(lib_t mainlib, lib_t deplib) {
	static size_t nest = 0;
	static char spaces[512] = { '\0', };
	size_t i, n;
	lib_t libs[1024];

	ASSERT( mainlib != (lib_t)0 );
	ASSERT( deplib != (lib_t)0 );

	if( !spaces[0] ) {
		for( i=0; i<sizeof(spaces) - 1; i++ )
			spaces[i] = ' ';

		spaces[i] = '\0';
	}

	nest++;

	Dbg_Outf("%.*sdoeslibdependonlib_r(\"%s\",\"%s\")\n",
		(nest - 1)*2, spaces, mainlib->name, deplib->name);

	if( nest > 10 ) {
		nest--;
		return 0;
	}

#if 0
	printf("%s%p %s%p\n", _WIN32 ? "0x" : "",
		(void *)mainlib, _WIN32 ? "0x" : "", (void *)deplib);
	printf("%s %s\n", mainlib->name, deplib->name);
#endif

	if( !mainlib->proj || mainlib==deplib ) {
		nest--;
		return 0;
	}

	SL_Unique(mainlib->proj->libs);
	n = SL_Size(mainlib->proj->libs);
	if( n > sizeof(libs)/sizeof(libs[0]) ) {
		Log_Error(__FILE__,__LINE__,__func__, "n > 1024; too many libraries");
		exit(EXIT_FAILURE);
	}

	/* first check; any immediate dependencies? */
	for( i=0; i<n; i++ ) {
		Dbg_Outf("%.*s%.2u of %u \"%s\"\n", (nest - 1)*2, spaces,
			i + 1, n, SL_Get(mainlib->proj->libs, i));
		libs[i] = Lib_Find(SL_Get(mainlib->proj->libs, i));

		if( libs[i]==deplib ) {
			Dbg_Outf("%.*s\"%s\" depends on \"%s\"\n",
				(nest - 1)*2, spaces, libs[i]->name, deplib->name);
			nest--;
			return 1;
		}
	}

	/* second check; recursive check */
	for( i=0; i<n; i++ ) {
		if( !Bld_DoesLibDependOnLib(libs[i], deplib) ) {
			continue;
		}

		nest--;
		return 1;
	}

	/* nope; this lib does not depend on that lib */
	nest--;
	return 0;
}

/* determine which library should come first */
int _Bld_CmpLibs(const void *a, const void *b) {
#if 0
	int r;
	printf("About to compare \"%s\" to \"%s\"\n",
		(*(lib_t *)a)->name, (*(lib_t *)b)->name);
	fflush(stdout);
	r = Bld_DoesLibDependOnLib(*(lib_t *)a, *(lib_t *)b);
	printf("_Bld_CmpLibs \"%s\" \"%s\" -> %i\n",
		(*(lib_t *)a)->name, (*(lib_t *)b)->name, r);
	fflush(stdout);
	return 1 - r;
#else
	return 1 - Bld_DoesLibDependOnLib(*(lib_t *)a, *(lib_t *)b);
#endif
}
void Bld_SortDeps(strList_t deparray) {
	size_t i, n;
	lib_t libs[1024];

	ASSERT( deparray != (strList_t)0 );

	/* remove duplicates from the array */
	Dbg_Outf("Dependency Array (Before Unique):\n");
	SL_Unique(deparray);

	Dbg_Outf("Dependency Array:\n");
	SL_DbgPrint(deparray);

	/* too many libraries? */
	n = SL_Size(deparray);
	if( n > sizeof(libs)/sizeof(libs[0]) ) {
		Log_Error(__FILE__,__LINE__,__func__, "Too many libraries");
		exit(EXIT_FAILURE);
	}

	/* fill the array, then sort it */
	for( i=0; i<n; i++ ) {
		libs[i] = Lib_Find(SL_Get(deparray, i));
		if( !libs[i] ) {
			Log_FatalError(Com_Va("Couldn't find library ^E\"%s\"^&",
				SL_Get(deparray, i)));
		}
	}

	Dbg_Outf("About to sort...\n");
	qsort((void *)libs, n, sizeof(lib_t), _Bld_CmpLibs);
	Dbg_Outf("Sorted!\n");

	/* set the array elements, sorted */
	for( i=0; i<n; i++ ) {
		SL_Set(deparray, i, Lib_Name(libs[i]));
	}

	Dbg_Outf("Sorted Dependency Array:\n");
	SL_DbgPrint(deparray);
}

/* get the linker flags for linking dependencies of a project */
const char *Bld_GetProjDepLinkFlags(project_t proj) {
	static char buf[65536];
	strList_t deps;
	size_t i, n;
	lib_t lib;

	deps = SL_New();

	Bld_GetDeps_r(proj, deps);
	Dbg_Outf("Project: \"%s\"\n", proj->name);
	Bld_SortDeps(deps);

	buf[0] = '\0';

	n = SL_Size(deps);
	for( i=0; i<n; i++ ) {
		lib = Lib_Find(SL_Get(deps, i));
		if( !lib ) {
			continue;
		}

		if( !lib->flags[proj->sys] ) {
			continue;
		}

		if( !lib->flags[proj->sys][0] ) {
			continue;
		}

		Com_Strcat(buf, sizeof(buf), Com_Va("%s ", lib->flags[proj->sys]));
	}

	SL_Delete(deps);
	return buf;
}

/* construct a list of dependent libraries */
/*
 *	???: Dependent on what? Libraries the project passed in relies on? Libraries
 *	     other projects would need if they were using this?
 *
 *	NOTE: It appears this function is used by Bld_GetLFlags() to determine how to
 *	      link with a given project.
 */
void Bld_GetLibs(project_t proj, char *dst, size_t n) {
	const char *name, *suffix = "";
	project_t p;
	char bin[PATH_MAX];
	int type;

	ASSERT( proj != (project_t)0 );
	ASSERT( dst != (char *)0 );
	ASSERT( n > 1 );

	/*
	 *	HACK: "cannot find -lblah" is really annoying when you don't want the
	 *	      project
	 */
	if( !Prj_IsTarget(proj) ) {
		return;
	}

	type = Prj_Type(proj);
	name = Prj_Name(proj);

	suffix = g_flags & kFlag_Release_Bit ? "" : Opt_GetDebugSuffix();

	if( type==kProjType_Library ) {
		/*
		 *	???: Why is this here? What is it doing?
		 *	NOTE: This code did fix a bug... but which bug? And why?
		 *
		 *	???: Was this to make sure that a dependent project with no source
		 *	     files wouldn't be linked in?
		 */
		if( (~proj->config & kProjCfg_Linking_Bit )
		&& Prj_SourceCount(proj) > 0) {
			Com_Strcat(dst, n, Com_Va("-l%s%s ", name, suffix));
		}

		/* NOTE: these should be linked after the library itself is linked in */
		for( p=Prj_Head(proj); p; p=Prj_Next(p) )
			Bld_GetLibs(p, dst, n);
	} else if(type==kProjType_DynamicLibrary) {
		if( (~proj->config & kProjCfg_Linking_Bit )
		&& Prj_SourceCount(proj) > 0) {
			Bld_GetBinName(proj, bin, sizeof(bin));
			Com_Strcat(dst, n, Com_Va("\"%s\" ", bin));
			/*
			Com_Strcat(dst, n, Com_Va("-L \"%s\" ", Prj_OutPath(proj)));
			Com_Strcat(dst, n, Com_Va("-l%s%s.dll ", name, suffix));
			*/
		}
	}
}

/* retrieve the flags for linking a project */
const char *Bld_GetLFlags(project_t proj, const char *bin, strList_t objs) {
	static char flags[32768];
	static char libs[16384], libs_stripped[16384];
	project_t p;
	size_t i, n;

	(void)p;
	(void)libs_stripped;

	ASSERT( proj != (project_t)0 );
	ASSERT( bin != (const char *)0 );
	ASSERT( objs != (strList_t)0 );

	flags[0] = '\0';
	libs[0] = '\0';

#if MK_WINDOWS_ENABLED
# define STATIC_FLAGS "-static -static-libgcc "
#else
# define STATIC_FLAGS " "
#endif
	switch(Prj_Type(proj)) {
	case kProjType_Application:
		if( g_flags & kFlag_Release_Bit ) {
			Com_Strcat(flags, sizeof(flags), "-s ");
		}
		if( proj->sys == kProjSys_MSWin && ( g_flags & kFlag_Release_Bit ) ) {
			Com_Strcat(flags, sizeof(flags),
				Com_Va(STATIC_FLAGS "-Wl,subsystem,windows -o \"%s\" ", bin));
		} else {
			Com_Strcat(flags, sizeof(flags),
				Com_Va(STATIC_FLAGS "-o \"%s\" ", bin));
		}
		break;
	case kProjType_Executable:
		if( g_flags & kFlag_Release_Bit ) {
			Com_Strcat(flags, sizeof(flags), "-s ");
		}
		Com_Strcat(flags, sizeof(flags), Com_Va(STATIC_FLAGS "-o \"%s\" ",
			bin));
		break;
	case kProjType_Library:
		Com_Strcat(flags, sizeof(flags), Com_Va("cr \"%s\" ", bin));
		break;
	case kProjType_DynamicLibrary:
		Com_Strcat(flags, sizeof(flags),
			Com_Va(STATIC_FLAGS "-shared -o \"%s\" ", bin));
		break;
	default:
		ASSERT_MSG(0, "unhandled project type");
		break;
	}

	if( Prj_Type(proj)!=kProjType_Library ) {
		n = SL_Size(g_libdirs);
		for( i=0; i<n; i++ ) {
			Com_Strcat(flags, sizeof(flags), Com_Va("-L \"%s\" ",
				SL_Get(g_libdirs, i)));
		}
	}

	n = SL_Size(objs);
	for( i=0; i<n; i++ ) {
		Com_Strcat(flags, sizeof(flags), Com_Va("\"%s\" ", SL_Get(objs, i)));
	}

	if( Prj_Type(proj)!=kProjType_Library ) {
		proj->config |= kProjCfg_Linking_Bit;

#if 0
		/*
		 *	NOTE: This should no longer be necessary. The "lib" system used now
		 *	      should take care of this. (Dependencies found by the header
		 *	      files of each project.)
		 */
		for( p=g_lib_proj_head; p; p=p->lib_next )
			Bld_GetLibs(p, libs, sizeof(libs));

		p = Prj_Parent(proj) ? Prj_Head(Prj_Parent(proj))
			: Prj_RootHead();
		while( p != (project_t)0 ) {
			ASSERT( p != (project_t)0 );
			Bld_GetLibs(p, libs, sizeof(libs));
			p = Prj_Next(p);
		}
#else
		Com_Strcpy(libs, sizeof(libs), Bld_GetProjDepLinkFlags(proj));
#endif

		proj->config &= ~kProjCfg_Linking_Bit;

#if 0
		Com_StripArgs(libs_stripped, sizeof(libs_stripped), libs);
		Com_Strcat(flags, sizeof(flags), libs_stripped);
#else
		Com_Strcat(flags, sizeof(flags), libs);
#endif

		switch(proj->sys) {
		case kProjSys_MSWin:
			if( Prj_Type(proj)==kProjType_DynamicLibrary ) {
				Com_Strcat(flags, sizeof(flags),
					/* NOTE: we don't use the import library; it's pointless */
					Com_Va(/*"\"-Wl,--out-implib=%s%s.a\" "*/
					   "-Wl,--export-all-symbols "
					   "-Wl,--enable-auto-import "/*, bin,
					   ~g_flags & kFlag_Release_Bit ? Opt_GetDebugSuffix() : ""*/));
			}

			/*Com_Strcat(flags, sizeof(flags), "-lkernel32 -luser32 -lgdi32 "
				"-lshell32 -lole32 -lopengl32 -lmsimg32");*/
			break;
		case kProjSys_Linux:
			/*Com_Strcat(flags, sizeof(flags), "-lGL");*/
			break;
		case kProjSys_MacOSX:
			/*Com_Strcat(flags, sizeof(flags), "-lobjc -framework Cocoa "
				"-framework OpenGL");*/
			break;
		default:
			/*
			 *	NOTE: Add OS specific link flags here.
			 */
			break;
		}

		Com_Strcat(flags, sizeof(flags), Prj_LinkFlags(proj));
	}
#if 0
	else {
		static char extras[32768];

		proj->config |= kProjCfg_Linking_Bit;

		p = Prj_Parent(proj) ? Prj_Head(Prj_Parent(proj))
			: Prj_RootHead();
		for( p=p; p!=(project_t)0; p=Prj_Next(p) ) {
			if( p==proj )
				continue;

			extras[0] = '\0';
			Prj_CompleteExtraLibs(p, extras, sizeof(extras));
			Prj_AppendExtraLib(proj, extras);
		}
		while( p != (project_t)0 ) {
			ASSERT( p != (project_t)0 );
			Bld_GetLibs(p, libs, sizeof(libs));
			p = Prj_Next(p);
		}
	}
#endif

	return flags;
}

/* find the name of an object file for a given source file */
void Bld_GetObjName(project_t proj, char *obj, size_t n, const char *src) {
	const char *p = "";

	ASSERT( proj != (project_t)0 );
	ASSERT( obj != (char *)0 );
	ASSERT( n > 1 );
	ASSERT( src != (const char *)0 );

	switch(Prj_Type(proj)) {
	case kProjType_Application:
		p = "appexec";
		break;
	case kProjType_Executable:
		p = "exec";
		break;
	case kProjType_Library:
		p = "lib";
		break;
	case kProjType_DynamicLibrary:
		p = "dylib";
		break;
	default:
		ASSERT_MSG(0, "project type is invalid");
		break;
	}

	Com_SubstExt(obj, n, Com_Va("%s/%s/%s/%s",
		Opt_GetObjdirBase(),
		Opt_GetConfigName(), p, src), ".o");
}

/* find the binary name of a target */
void Bld_GetBinName(project_t proj, char *bin, size_t n) {
	const char *dir, *prefix, *name, *symbol, *suffix = "";
	int type;
	int sys;

	ASSERT( proj != (project_t)0 );
	ASSERT( bin != (char *)0 ? n > 1 : 1 );

	if( !proj->binname || bin!=(char *)0 ) {
		type = Prj_Type(proj);
		sys = proj->sys;

		/*dir = type==kProjType_Library ? "lib/" : "bin/";*/
		dir = Prj_OutPath(proj);
		ASSERT( dir != (const char *)0 );
		prefix = "";
		name = Prj_Name(proj);
		symbol = g_flags & kFlag_Release_Bit ? "" : Opt_GetDebugSuffix();

		switch(type) {
		case kProjType_Application:
		case kProjType_Executable:
			if( sys==kProjSys_MSWin || sys==kProjSys_UWP || sys==kProjSys_Cygwin ) {
				suffix = ".exe";
			}
			break;
		case kProjType_Library:
			prefix = "lib";
			suffix = ".a";
			break;
		case kProjType_DynamicLibrary:
			if( sys!=kProjSys_MSWin && sys!=kProjSys_UWP ) {
				prefix = "lib";
			}
			if( sys==kProjSys_MSWin || sys==kProjSys_UWP || sys==kProjSys_Cygwin ) {
				suffix = ".dll";
			} else if(sys==kProjSys_Linux || sys==kProjSys_Unix) {
				suffix = ".so";
			} else if(sys==kProjSys_MacOSX) {
				suffix = ".dylib";
			}
			break;
		default:
			ASSERT_MSG(0, "unhandled project type");
			break;
		}

		proj->binname = Com_Dup(proj->binname, Com_Va("%s%s%s%s%s",
			dir, prefix, name, symbol, suffix));
	}

	if( bin != (char *)0 ) {
		Com_Strcpy(bin, n, proj->binname);
	}
}

/* compile and run a unit test */
void Bld_UnitTest(project_t proj, const char *src)
{
	static char flags[32768];
	static char libs[16384], libs_stripped[16384];
	const char *tool, *libname, *libf;
	const char *cc, *cxx;
	project_t chld;
	size_t i, j, n;
	lib_t lib;
	char out[PATH_MAX], dep[PATH_MAX], projbin[PATH_MAX];

	ASSERT( proj != (project_t)0 );
	ASSERT( src != (const char *)0 );

	(void)chld;
	(void)libs_stripped;
	
	cc = Bld_GetCompiler(0);
	cxx = Bld_GetCompiler(1);

	/* determine the name for the unit test's executable */
	Com_Strcpy(out, sizeof(out), Com_Va("%s/%s/%s/test/",
		Opt_GetObjdirBase(),
		Opt_GetConfigName(),
		Prj_Name(proj)));
	FS_MkDirs(out);
	Com_SubstExt(out, sizeof(out), Com_Va("%s%s", out, strrchr(src, '/') + 1),
		".test");

#if MK_WINDOWS_ENABLED
	Com_Strcat(out, sizeof(out), ".exe");
	Com_SubstExt(dep, sizeof(dep), out, ".d");
#else
	Com_Strcpy(dep, sizeof(dep), out);
	Com_Strcat(dep, sizeof(dep), ".d");
#endif

	flags[0] = '\0';

	Bld_GetCFlags_Warnings(flags, sizeof(flags));
	if( Bld_GetCFlags_Standard(flags, sizeof(flags), src) ) {
		tool = cxx;
	} else {
		tool = (proj->config & kProjCfg_UsesCxx_Bit) ? cxx : cc;
	}
	Bld_GetCFlags_ReleaseDebug(flags, sizeof(flags), proj->arch);
	Bld_GetCFlags_Platform(flags, sizeof(flags), proj->arch, proj->sys, 1);
	Com_Strcat(flags, sizeof(flags), "-DTEST -DMK_TEST ");
	Com_Strcat(flags, sizeof(flags), "-DEXECUTABLE -DMK_EXECUTABLE ");
	Bld_GetCFlags_Includes(flags, sizeof(flags));
	Bld_GetCFlags_Definitions(flags, sizeof(flags), proj->defs);

	/* retrieve all of the library directories */
	n = SL_Size(g_libdirs);
	for( i=0; i<n; i++ ) {
		Com_Strcat(flags, sizeof(flags), Com_Va("-L \"%s\" ",
			SL_Get(g_libdirs, i)));
	}

	/* determine compilation flags: output and source */
	Com_Strcat(flags, sizeof(flags), Com_Va("-o \"%s\" \"%s\" ", out, src));

	/* link to the project directly if it's a library (static or dynamic) */
	switch(Prj_Type(proj))
	{
	case kProjType_Library:
	case kProjType_DynamicLibrary:
		Bld_GetBinName(proj, projbin, sizeof(projbin));
		Com_Strcat(flags, sizeof(flags), Com_Va("\"%s\" ", projbin));
		break;
	}

	/* now include all of the necessary libs we depend on (we're assuming these
	   are the same libs the project itself depends on)

	   FIXME: this won't work if we try using a lib that the project doesn't
	          depend on here... perhaps the solution is to make unit tests their
			  own projects? */
	libs[0] = '\0';

#if 0
	/* first pass: grab all sibling projects */
	chld = Prj_Parent(proj) ? Prj_Head(Prj_Parent(proj))
	       : Prj_RootHead();

	for( chld=chld; chld != (project_t)0; chld=Prj_Next(chld) ) {
		ASSERT( chld != (project_t)0 );

		if( chld==proj )
			continue;

		Bld_GetLibs(chld, libs, sizeof(libs));
		chld = Prj_Next(chld);
	}

	/* second pass: grab all of our direct dependencies */
	for( chld=Prj_Head(proj); chld != (project_t)0; chld=Prj_Next(chld) )
	{
		ASSERT( chld != (project_t)0 );
		Bld_GetLibs(chld, libs, sizeof(libs));
		chld = Prj_Next(chld);
	}
#endif

	/*
	 *	TODO: Use the proper dependency checking method.
	 */

	/* grab all of the directly dependent libraries */
	n = SL_Size(proj->libs);
	for( i=0; i<n; i++ ) {
		/* if we have a null pointer in the array, skip it */
		if( !(libname = SL_Get(proj->libs, i)) ) {
			continue;
		}

		/* check for a Com_Dup library */
		for( j=0; j<i; j++ ) {
			/* skip null pointers... */
			if( !SL_Get(proj->libs, j) ) {
				continue;
			}

			/* is there a match? (did we already include it, that is) */
			if( !strcmp(SL_Get(proj->libs, j), libname) ) {
				break;
			}
		}

		/* found a duplicate, don't include again */
		if( j != i ) {
			continue;
		}

		/* try to find the library handle from the name in the array */
		if( !(lib = Lib_Find(libname)) ) {
			Sys_PrintStr(SIO_ERR, COLOR_YELLOW, "WARN");
			Sys_UncoloredPuts(SIO_ERR, ": ", 2);
			Sys_PrintStr(SIO_ERR, COLOR_PURPLE, src);
			Sys_UncoloredPuts(SIO_ERR, ": couldn't find library \"", 0);
			Sys_PrintStr(SIO_ERR, COLOR_BROWN, libname);
			Sys_UncoloredPuts(SIO_ERR, "\"\n", 2);
			continue;
		}

		/* retrieve the flags... if we have a non-empty string, add them to the
		   linker flags */
		if( (libf=Lib_Flags(lib, proj->sys))!=(const char *)0 && *libf!='\0' ) {
			Com_Strcat(libs, sizeof(libs), Com_Va("%s ", libf));
		}
	}

	/* link in all libraries */
#if 0
	/*
	 *	NOTE: This should no longer be necessary. Using the AL_Autolink system.
	 */
	for( chld=g_lib_proj_head; chld; chld=chld->lib_next )
		Bld_GetLibs(chld, libs, sizeof(libs));

# if 0
	Com_StripArgs(libs_stripped, sizeof(libs_stripped), libs);
	Com_Strcat(flags, sizeof(flags), libs_stripped);
# else
	Com_Strcat(flags, sizeof(flags), libs);
# endif
#endif

	/* queue unit tests */
	SL_PushBack(g_unitTestCompiles, Com_Va("%s %s", tool, flags));
	SL_PushBack(g_unitTestRuns, out);

	Com_RelPathCWD(out, sizeof(out), src);
	SL_PushBack(g_unitTestNames, out);
}

/* perform each unit test */
void Bld_RunTests() {
	static size_t buffer[65536];
	strList_t failedtests;
	size_t i, n;
	int e;

	ASSERT( SL_Size(g_unitTestCompiles)==SL_Size(g_unitTestRuns) );
	ASSERT( SL_Size(g_unitTestRuns)==SL_Size(g_unitTestNames) );

	n = SL_Size(g_unitTestCompiles);
	if( !n ) {
		return;
	}
	ASSERT( n <= sizeof(buffer)/sizeof(buffer[0]) );
	SL_OrderedSort(g_unitTestNames, buffer, sizeof(buffer)/sizeof(buffer[0]));
	SL_IndexedSort(g_unitTestCompiles, buffer, n);
	SL_IndexedSort(g_unitTestRuns, buffer, n);

	failedtests = SL_New();

	for( i=0; i<n; i++ ) {
		/* compile the unit test */
		if( Com_Shell("%s", SL_Get(g_unitTestCompiles, i)) != 0 )
		{
			Sys_PrintStr(SIO_ERR, COLOR_LIGHT_RED, "KO");
			Sys_UncoloredPuts(SIO_ERR, ": ", 2);
			Sys_PrintStr(SIO_ERR, COLOR_PURPLE, SL_Get(g_unitTestNames, i));
			Sys_UncoloredPuts(SIO_ERR, " (did not build)\n", 0);
			SL_PushBack(failedtests, SL_Get(g_unitTestNames, i));
			continue;
		}

		/* run the unit test */
		e = Com_Shell("%s", SL_Get(g_unitTestRuns, i));
		if( e != 0 ) {
			Sys_PrintStr(SIO_ERR, COLOR_LIGHT_RED, "KO");
			Sys_UncoloredPuts(SIO_ERR, ": ", 2);
			Sys_PrintStr(SIO_ERR, COLOR_RED, SL_Get(g_unitTestNames, i));
			Sys_Printf(SIO_ERR, " (returned " S_COLOR_WHITE "%i"
				S_COLOR_RESTORE ")\n", e);
			SL_PushBack(failedtests, SL_Get(g_unitTestRuns, i));
		} else {
			Sys_PrintStr(SIO_ERR, COLOR_LIGHT_GREEN, "OK");
			Sys_UncoloredPuts(SIO_ERR, ": ", 2);
			Sys_PrintStr(SIO_ERR, COLOR_WHITE, SL_Get(g_unitTestNames, i));
			Sys_UncoloredPuts(SIO_ERR, "\n", 1);
		}
	}

	n = SL_Size(failedtests);
	if( n > 0 ) {
		Sys_PrintStr(SIO_ERR, COLOR_RED, "\n  *** ");
		Sys_PrintStr(SIO_ERR, COLOR_WHITE, Com_Va("%u", (unsigned int)n));
		Sys_PrintStr(SIO_ERR, COLOR_LIGHT_RED, n==1 ? " FAILURE" : " FAILURES");
		Sys_PrintStr(SIO_ERR, COLOR_RED, " ***\n  ");
		for( i=0; i<n; i++ ) {
			Sys_PrintStr(SIO_ERR, COLOR_YELLOW, SL_Get(failedtests, i));
			Sys_UncoloredPuts(SIO_ERR, "\n  ", 3);
		}
		Sys_PrintStr(SIO_ERR, COLOR_RED, "\n");
	}

	SL_Delete(failedtests);
}

/* sort the projects in a list */
void Bld_SortProjects(struct project_s *proj) {
	struct project_s **head, **tail;
	struct project_s *p, *next;
	int numsorts;

	head = proj ? &proj->head : &g_proj_head;
	tail = proj ? &proj->tail : &g_proj_tail;

	do {
		numsorts = 0;

		for( p=*head; p; p=next ) {
			if( !(next = p->next) ) {
				break;
			}

			if( p->type > next->type ) {
				numsorts++;

				if( p==*head ) {
					*head = next;
				}
				if( next==*tail ) {
					*tail = p;
				}

				if( p->prev ) {
					p->prev->next = next;
				}
				if( next->next ) {
					next->next->prev = p;
				}

				next->prev = p->prev;
				p->prev = next;
				p->next = next->next;
				next->next = p;

				next = p;
			}
		}
	} while( numsorts > 0 );
}

/* set a projects dependents to be relinked */
void Bld_RelinkDeps(project_t proj) {
	project_t prnt, next;

	ASSERT( proj != (project_t)0 );

	for( prnt=proj->prnt; prnt; prnt=prnt->prnt ) {
		prnt->config |= kProjCfg_NeedRelink_Bit;
	}

	if( proj->type==kProjType_Executable
	|| proj->type==kProjType_Application ) {
		return;
	}

	for( next=proj->next; next; next=next->next ) {
		if( proj->type > next->type ) {
			break;
		}

		next->config |= kProjCfg_NeedRelink_Bit;
		Bld_RelinkDeps(next);
	}
}

/* build a project */
int Mk_Project(project_t proj) {
	const char *src, *lnk, *tool, *cxx, *cc;
	project_t chld;
	strList_t objs;
	size_t cwd_l;
	size_t i, n;
	char cwd[PATH_MAX], obj[PATH_MAX], bin[PATH_MAX];
	int numbuilds;

	/* build the child projects */
	Bld_SortProjects(proj);
	for( chld=Prj_Head(proj); chld; chld=Prj_Next(chld) ) {
		if( !Mk_Project(chld) ) {
			return 0;
		}
	}

	/* if this project isn't targetted, just return now */
	if( !Prj_IsTarget(proj) )
		return 1;

	/* retrieve the current working directory */
	if( getcwd(cwd, sizeof(cwd))==(char *)0 ) {
		Log_FatalError("getcwd() failed");
	}

	cwd_l = Com_Strlen(cwd);

	/*
	 *	NOTE: This appears to be a hack.
	 */
	if( !Prj_SourceCount(proj )
	 && Prj_Type(proj)!=kProjType_DynamicLibrary) {
		Log_ErrorMsg(Com_Va("project ^E'%s'^& has no source files!",
			Prj_Name(proj)));
		return 1;
	}

	cc = Bld_GetCompiler(0);
	cxx = Bld_GetCompiler(1);
	tool = proj->config & kProjCfg_UsesCxx_Bit ? cxx : cc;

	/* make the object directories */
	Prj_MakeObjDirs(proj);

	/* store each object file */
	objs = SL_New();

	/* run through each source file */
	numbuilds = 0;
	n = Prj_SourceCount(proj);
	for( i=0; i<n; i++ ) {
		src = Prj_Source(proj, i);
		Bld_GetObjName(proj, obj, sizeof(obj), &src[cwd_l+1]);
		SL_PushBack(objs, obj);

		if( Bld_ShouldCompile(obj) ) {
			if (Com_Shell("%s %s", tool,
			Bld_GetCFlags(proj, obj, &src[cwd_l+1]))) {
				SL_Delete(objs);
				return 0;
			}
			numbuilds++;
		}

		Com_SubstExt(bin, sizeof(bin), obj, ".d");
		if( (~g_flags & kFlag_NoLink_Bit)
		 && !Bld_FindSourceLibs(proj->libs, proj->sys, obj, bin ) ) {
		 	Log_ErrorMsg("call to Bld_FindSourceLibs() failed");
			return 0;
		}
	}

	/* link the project's object files together */
	Bld_GetBinName(proj, bin, sizeof(bin));
	/*printf("bin: %s\n", bin);*/
	if( ( proj->config & kProjCfg_NeedRelink_Bit )
	 || Bld_ShouldLink(bin, numbuilds) ) {
		FS_MkDirs(Prj_OutPath(proj));
		SL_Unique(proj->libs);
		Prj_CalcLibFlags(proj);

		/* find the libraries this project needs */
		lnk = Prj_Type(proj)==kProjType_Library ? "ar" : tool;
		if( Com_Shell("%s %s", lnk, Bld_GetLFlags(proj, bin, objs)) ) {
			SL_Delete(objs);
			return 0;
		}

		/* dependent projects need to be rebuilt */
		Bld_RelinkDeps(proj);
	} else if( ~g_flags & kFlag_FullClean_Bit ) {
		Prj_CalcDeps(proj);
	}

	/* unit testing */
	if( g_flags & kFlag_Test_Bit )
	{
		n = Prj_TestSourceCount(proj);
		for( i=0; i<n; i++ ) {
			Bld_UnitTest(proj, Prj_TestSource(proj, i));
		}
	}

	/* clean (removes temporaries) -- only if not rebuilding */
	if( g_flags & kFlag_LightClean_Bit ) {
		n = SL_Size(objs);
		for( i=0; i<n; i++ ) {
			Com_SubstExt(obj, sizeof(obj), SL_Get(objs, i), ".d");
			FS_Delete(SL_Get(objs, i)); /* object (.o) */
			FS_Delete(obj); /* dependency (.d) */
		}

		if( g_flags & kFlag_NoLink_Bit ) {
			FS_Delete(bin);
		}
	}

	SL_Delete(objs);

	return 1;
}

/* build all the projects */
int Mk_AllProjects() {
	project_t proj;
	
	if( g_flags & kFlag_FullClean_Bit ) {
		FS_Delete( Opt_GetObjdirBase() );
	}

	Bld_SortProjects((struct project_s *)0);
	
	Git_GenerateInfo();

	for( proj=Prj_RootHead(); proj; proj=Prj_Next(proj) ) {
		if( !Mk_Project(proj) ) {
			return 0;
		}
	}

	Bld_RunTests();

	return 1;
}

/*
 *	========================================================================
 *	MAIN
 *	========================================================================
 */

void Mk_PushSrcDir(const char *srcdir) {
	if( !FS_IsDir(srcdir) ) {
		Log_ErrorMsg(Com_Va("^F%s^&", srcdir));
		return;
	}

	SL_PushBack(g_srcdirs, srcdir);
}
void Mk_PushIncDir(const char *incdir) {
	char inc[PATH_MAX];

	if( !FS_IsDir(incdir) ) {
		Log_ErrorMsg(Com_Va("^F%s^&", incdir));
		return;
	}

	Com_RelPathCWD(inc, sizeof(inc), incdir);
	SL_PushBack(g_incdirs, inc);
}
void Mk_PushLibDir(const char *libdir) {
#if 0
	/*
	 *	XXX: If project has not yet been built, the lib dir might not exist.
	 */
	if( !FS_IsDir(libdir) ) {
		Log_ErrorMsg(Com_Va("^F%s^&", libdir));
		return;
	}
#endif

	SL_PushBack(g_libdirs, libdir);
}
void Mk_PushPkgDir(const char *pkgdir) {
	if( !FS_IsDir(pkgdir) ) {
		Log_ErrorMsg(Com_Va("^F%s^&", pkgdir));
		return;
	}

	SL_PushBack(g_pkgdirs, pkgdir);
}
void Mk_PushToolDir(const char *tooldir) {
	if( !FS_IsDir(tooldir) ) {
		Log_ErrorMsg(Com_Va("^F%s^&", tooldir));
		return;
	}

	SL_PushBack(g_tooldirs, tooldir);
}
void Mk_PushDLLsDir(const char *dllsdir) {
	if( !FS_IsDir(dllsdir) ) {
		Log_ErrorMsg(Com_Va("^F%s^&", dllsdir));
		return;
	}

	SL_PushBack(g_dllsdirs, dllsdir);
}

void FS_UnwindDirs() {
	while( SL_Size(g_fs_dirstack) > 0 ) {
		FS_Leave();
	}
}

void Mk_Init(int argc, char **argv) {
	static const struct { const char *const header[3], *const lib; } autolinks[] = {
		/* OpenGL */
		{ { "GL/gl.h","GL/gl.h","OpenGL/OpenGL.h" },
		  "opengl" },
		{ { (const char *)0,(const char *)0,"GL/gl.h" },
		  "opengl" },
		{ { (const char *)0,(const char *)0,"OpenGL/gl.h" },
		  "opengl" },

		/* OpenGL - GLU */
		{ { "GL/glu.h","GL/glu.h","OpenGL/glu.h" },
		  "glu" },

		/* OpenGL - GLFW/GLEW */
		{ { "GL/glfw.h","GL/glfw.h","GL/glfw.h" },
		  "glfw" },
		{ { "GL/glew.h","GL/glew.h","GL/glew.h" },
		  "glew" },

		/* SDL */
		{ { "SDL/sdl.h","SDL/sdl.h","SDL/sdl.h" },
		  "sdl" },
		{ { "SDL/sdl_mixer.h","SDL/sdl_mixer.h","SDL/sdl_mixer.h" },
		  "sdl_mixer" },
		{ { "SDL/sdl_main.h","SDL/sdl_main.h","SDL/sdl_main.h" },
		  "sdl_main" },

		/* SDL2 - EXPERIMENTAL */
		{ { "SDL2/SDL.h","SDL2/SDL.h","SDL2/SDL.h" },
		  "sdl2" },

		 /* SFML */
		{ { "SFML/Config.hpp","SFML/Config.hpp","SFML/Config.hpp" },
		  "sfml" },

		/* Ogg/Vorbis */
		{ { "ogg/ogg.h","ogg/ogg.h","ogg/ogg.h" },
		  "ogg" },
		{ { "vorbis/codec.h","vorbis/codec.h","vorbis/codec.h" },
		  "vorbis" },
		{ { "vorbis/vorbisenc.h","vorbis/vorbisenc.h","vorbis/vorbisenc.h" },
		  "vorbisenc" },
		{ { "vorbis/vorbisfile.h","vorbis/vorbisfile.h","vorbis/vorbisfile.h" },
		  "vorbisfile" },

		/* Windows */
		{ { "winuser.h",(const char *)0,(const char *)0 },
		  "user32" },
		{ { "winbase.h",(const char *)0,(const char *)0 },
		  "kernel32" },
		{ { "shellapi.h",(const char *)0,(const char *)0 },
		  "shell32" },
		{ { "ole.h",(const char *)0,(const char *)0 },
		  "ole32" },
		{ { "commctrl.h",(const char *)0,(const char *)0 },
		  "comctl32" },
		{ { "commdlg.h",(const char *)0,(const char *)0 },
		  "comdlg32" },
		{ { "wininet.h",(const char *)0,(const char *)0 },
		  "wininet" },
		{ { "mmsystem.h",(const char *)0,(const char *)0 },
		  "winmm" },
		{ { "uxtheme.h",(const char *)0,(const char *)0 },
		  "uxtheme" },
		{ { "wingdi.h",(const char *)0,(const char *)0 },
		  "gdi32" },
		{ { "winsock2.h",(const char *)0,(const char *)0 },
		  "winsock2" },

		/* POSIX */
		{ { (const char *)0,"time.h","time.h" },
		  "realtime" },
		{ { (const char *)0,"math.h","math.h" },
		  "math" },

		/* PNG */
		{ { "png.h","png.h","png.h" },
		  "png" },

		/* BZip2 */
		{ { "bzlib.h","bzlib.h","bzlib.h" },
		  "bzip2" },

		/* ZLib */
		{ { "zlib.h","zlib.h","zlib.h" },
		  "z" },

		/* PThread */
		{ { "pthread.h","pthread.h","pthread.h" },
		  "pthread" },

		/* Curses */
		{ { "curses.h","curses.h","curses.h" },
		  "curses" },
	};
	static const struct { const char *const lib, *const flags[3]; } libs[] = {
		/* OpenGL (and friends) */
		{ "opengl",
		  { "-lopengl32","-lGL","-framework OpenGL" } },
		{ "glu",
		  { "-lglu32","-lGLU","-lGLU" } },
		{ "glfw",
		  { "-lglfw","-lglfw","-lglfw" } },
		{ "glew",
		  { "-lglew32","-lGLEW","-lGLEW" } },

		/* SDL */
		{ "sdl",
		  { "-lSDL","-lSDL","-lSDL" } },
		{ "sdl_mixer",
		  { "-lSDL_mixer","-lSDL_mixer","-lSDL_mixer" } },
		{ "sdl_main",
		  { "-lSDLmain","-lSDLmain","-lSDLmain" } },

		/* SDL2 - EXPERIMENTAL */
		{ "sdl2",
		  {  "-lSDL2 -luser32 -lkernel32 -lshell32 -lole32 -lwininet -lwinmm"
			 " -limm32 -lgdi32 -loleaut32 -lversion -luuid",
		     "-lSDL2", "-lSDL2" } },

		/* SFML */
		{ "sfml",
		  { "-lsfml-window-s -lsfml-graphics-s -lsfml-audio-s"
		    " -lsfml-network-s -lsfml-main -lsfml-system-s",
			"-lsfml-window-s -lsfml-graphics-s -lsfml-audio-s"
		    " -lsfml-network-s -lsfml-main -lsfml-system-s",
			"-lsfml-window-s -lsfml-graphics-s -lsfml-audio-s"
		    " -lsfml-network-s -lsfml-main -lsfml-system-s" } },

		/* Ogg/Vorbis */
		{ "ogg",
		  { "-logg","-logg","-logg" } },
		{ "vorbis",
		  { "-lvorbis","-lvorbis","-lvorbis" } },
		{ "vorbisenc",
		  { "-lvorbisenc","-lvorbisenc","-lvorbisenc" } },
		{ "vorbisfile",
		  { "-lvorbisfile","-lvorbisfile","-lvorbisfile" } },

		/* Windows */
		{ "user32",
		  { "-luser32",(const char *)0,(const char *)0 } },
		{ "kernel32",
		  { "-lkernel32",(const char *)0,(const char *)0 } },
		{ "shell32",
		  { "-lshell32",(const char *)0,(const char *)0 } },
		{ "ole32",
		  { "-lole32",(const char *)0,(const char *)0 } },
		{ "comctl32",
		  { "-lcomctl32",(const char *)0,(const char *)0 } },
		{ "comdlg32",
		  { "-lcomdlg32",(const char *)0,(const char *)0 } },
		{ "wininet",
		  { "-lwininet",(const char *)0,(const char *)0 } },
		{ "winmm",
		  { "-lwinmm",(const char *)0,(const char *)0 } },
		{ "uxtheme",
		  { "-luxtheme",(const char *)0,(const char *)0 } },
		{ "gdi32",
		  { "-lgdi32",(const char *)0,(const char *)0 } },
		{ "winsock2",
		  { "-lws2_32",(const char *)0,(const char *)0 } },

		/* POSIX */
		{ "realtime",
		  { (const char *)0,"-lrt","-lrt" } },
		{ "math",
		  { (const char *)0,"-lm","-lm" } },

		/* PNG */
		{ "png",
		  { "-lpng","-lpng","-lpng" } },

		/* BZip2 */
		{ "bzip2",
		  { "-lbzip2","-lbzip2","-lbzip2" } },

		/* ZLib */
		{ "z",
		  { "-lz","-lz","-lz" } },

		/* PThread */
		{ "pthread",
		  { "-lpthread","-lpthread","-lpthread" } },

		/* Curses */
		{ "curses",
		  { "-lncurses","-lncurses","-lncurses" } }
	};
	const char *optlinks[256], *p;
	bitfield_t bit;
	autolink_t al;
	size_t j;
	lib_t lib;
	char temp[PATH_MAX];
	int builtinautolinks = 1, userautolinks = 1;
	int i, op;

	/* core initialization */
	Sys_InitColoredOutput();
	atexit(SL_DeleteAll);
	FS_Init();
	atexit(FS_UnwindDirs);
	atexit(AL_DeleteAll);
	atexit(Dep_DeleteAll);
	atexit(Prj_DeleteAll);
	Bld_InitUnitTestArrays();

	/* set single-character options here */
	memset((void *)optlinks, 0, sizeof(optlinks));
	optlinks['h'] = "help";
	optlinks['v'] = "version";
	optlinks['V'] = "verbose";
	optlinks['r'] = "release";
	optlinks['R'] = "rebuild";
	optlinks['c'] = "compile-only";
	optlinks['b'] = "brush";
	optlinks['C'] = "clean";
	optlinks['H'] = "print-hierarchy";
	optlinks['p'] = "pedantic";
	optlinks['T'] = "test";
	optlinks['D'] = "dir";

	/* arrays need to be initialized */
	g_targets = SL_New();
	g_srcdirs = SL_New();
	g_incdirs = SL_New();
	g_libdirs = SL_New();
	g_pkgdirs = SL_New();
	g_tooldirs = SL_New();
	g_dllsdirs = SL_New();

	/* process command line arguments */
	for( i=1; i<argc; i++ ) {
		const char *opt;

		opt = argv[i];
		if( *opt=='-' ) {
			op = 0;
			p = (const char *)0;

			if( *(opt+1)=='-' ) {
				opt = &opt[2];

				if( (op = !strncmp(opt, "no-", 3) ? 1 : 0)==1 ) {
					opt = &opt[3];
				}

				if( (p = strchr(opt, '=')) != (const char *)0 ) {
					Com_Strncpy(temp, sizeof(temp), opt, p-opt);
					p++;
					opt = temp;
				}
			} else {
				if( *(opt+1)=='I' ) {
					if( *(opt+2)==0 ) {
						p = argv[++i];
					} else {
						p = &opt[2];
					}

					opt = "incdir";
				} else if(*(opt+1)=='L') {
					if( *(opt+2)==0 ) {
						p = argv[++i];
					} else {
						p = &opt[2];
					}

					opt = "libdir";
				} else if(*(opt+1)=='S') {
					if( *(opt+2)==0 ) {
						p = argv[++i];
					} else {
						p = &opt[2];
					}

					opt = "srcdir";
				} else if(*(opt+1)=='P') {
					if( *(opt+2)==0 ) {
						p = argv[++i];
					} else {
						p = &opt[2];
					}
				} else {
					/* not allowing repeats (e.g., -hv) yet */
					if( *(opt+2)!=0 ) {
						Log_ErrorMsg(Com_Va("^E'%s'^& is a malformed argument",
							argv[i]));
						continue;
					}

					if( !(opt = optlinks[(unsigned char)*(opt+1)]) ) {
						Log_ErrorMsg(Com_Va("unknown option ^E'%s'^&; ignoring",
							argv[i]));
						continue;
					}
				}
			}

			ASSERT( opt != (const char *)0 );

			bit = 0;
			if( !strcmp(opt, "help") ) {
				bit = kFlag_ShowHelp_Bit;
			} else if(!strcmp(opt, "version")) {
				bit = kFlag_ShowVersion_Bit;
			} else if(!strcmp(opt, "verbose")) {
				bit = kFlag_Verbose_Bit;
			} else if(!strcmp(opt, "release")) {
				bit = kFlag_Release_Bit;
			} else if(!strcmp(opt, "rebuild")) {
				bit = kFlag_Rebuild_Bit;
			} else if(!strcmp(opt, "compile-only")) {
				bit = kFlag_NoLink_Bit;
			} else if(!strcmp(opt, "brush")) {
				bit = kFlag_NoCompile_Bit|kFlag_NoLink_Bit|kFlag_LightClean_Bit;
			} else if(!strcmp(opt, "clean")) {
				bit = kFlag_NoCompile_Bit|kFlag_NoLink_Bit|kFlag_FullClean_Bit;
			} else if(!strcmp(opt, "print-hierarchy")) {
				bit = kFlag_PrintHierarchy_Bit;
			} else if(!strcmp(opt, "pedantic")) {
				bit = kFlag_Pedantic_Bit;
			} else if(!strcmp(opt, "test")) {
				bit = kFlag_Test_Bit;
			} else if(!strcmp(opt, "color")) {
				if( op ) {
					g_flags_color = kMkColorMode_None;
				} else {
					g_flags_color = MK__DEFAULT_COLOR_MODE_IMPL;
				}
				continue;
			} else if(!strcmp(opt, "ansi-colors")) {
				if( op ) {
					g_flags_color = kMkColorMode_None;
				} else {
					g_flags_color = kMkColorMode_ANSI;
				}
				continue;
			} else if(!strcmp(opt, "win32-colors")) {
#if MK_WINDOWS_COLORS_ENABLED
				if( op ) {
					g_flags_color = kMkColorMode_None;
				} else {
					g_flags_color = kMkColorMode_Windows;
				}
#else
				Log_ErrorMsg("option \"--[no-]win32-colors\" is disabled in this build");
#endif
				continue;
			} else if(!strcmp(opt, "builtin-autolinks")) {
				builtinautolinks = (int)!op;
				continue;
			} else if(!strcmp(opt, "user-autolinks")) {
				userautolinks = (int)!op;
				continue;
			} else if(!strcmp(opt, "srcdir")) {
				if( i+1==argc&&!p ) {
					Log_ErrorMsg(Com_Va("expected argument to ^E'%s'^&", argv[i]));
					continue;
				}

				Mk_PushSrcDir(p ? p : argv[++i]);
				continue;
			} else if(!strcmp(opt, "incdir")) {
				if( i+1==argc&&!p ) {
					Log_ErrorMsg(Com_Va("expected argument to ^E'%s'^&", argv[i]));
					continue;
				}

				Mk_PushIncDir(p ? p : argv[++i]);
				continue;
			} else if(!strcmp(opt, "libdir")) {
				if( i+1==argc&&!p ) {
					Log_ErrorMsg(Com_Va("expected argument to ^E'%s'^&", argv[i]));
					continue;
				}

				Mk_PushLibDir(p ? p : argv[++i]);
				continue;
			} else if(!strcmp(opt, "pkgdir")) {
				if( i+1==argc&&!p ) {
					Log_ErrorMsg(Com_Va("expected argument to ^E'%s'^&", argv[i]));
					continue;
				}

				Mk_PushPkgDir(p ? p : argv[++i]);
				continue;
			} else if(!strcmp(opt, "toolsdir")) {
				if( i+1==argc&&!p ) {
					Log_ErrorMsg(Com_Va("expected argument to ^E'%s'^&", argv[i]));
					continue;
				}

				Mk_PushToolDir(p ? p : argv[++i]);
				continue;
			} else if(!strcmp(opt, "dllsdir")) {
				if( i+1==argc&&!p ) {
					Log_ErrorMsg(Com_Va("expected argument to ^E'%s'^&", argv[i]));
					continue;
				}

				Mk_PushDLLsDir(p ? p : argv[++i]);
				continue;
			} else if(!strcmp(opt, "dir")) {
				if( i+1==argc&&!p ) {
					Log_ErrorMsg(Com_Va("expected argument to ^E'%s'^&", argv[i]));
					continue;
				}

				FS_Enter(p ? p : argv[++i]);
				continue;
			} else {
				Log_ErrorMsg(Com_Va("unknown option ^E'%s'^&; ignoring", argv[i]));
				continue;
			}

			ASSERT( bit != 0 );
			ASSERT( op==0 || op==1 );

			if( op ) {
				g_flags &= ~bit;
			} else {
				g_flags |= bit;
			}
		} else {
			SL_PushBack(g_targets, opt);
		}
	}

	/* support "clean rebuilds" */
	if( ( g_flags & kFlag_Rebuild_Bit ) && ( g_flags &
	( kFlag_LightClean_Bit | kFlag_FullClean_Bit ) ) ) {
		g_flags &= ~( kFlag_NoCompile_Bit | kFlag_NoLink_Bit );
	}

	/* show the version */
	if( g_flags & kFlag_ShowVersion_Bit ) {
		printf(
			"mk " MK_VERSION_STR " - compiled %s\nCopyright (c) 2012-2016 NotKyon\n\n"
			"This software contains ABSOLUTELY NO WARRANTY.\n\n", __DATE__);
	}

	/* show the help */
	if( g_flags & kFlag_ShowHelp_Bit ) {
		printf("Usage: mk [options...] [targets...]\n");
		printf("Options:\n");
		printf("  -h,--help                Show this help message.\n");
		printf("  -v,--version             Show the version.\n");
		printf("  -V,--verbose             Show the commands invoked.\n");
		printf("  -b,--brush               Remove intermediate files after "
			"building.\n");
		printf("  -C,--clean               Remove \"%s\"\n", Opt_GetObjdirBase());
		printf("  -r,--release             Build in release mode.\n");
		printf("  -R,--rebuild             "
			"Force a rebuild, without cleaning.\n");
		printf("  -T,--test                Run unit tests.\n");
		printf("  -c,--compile-only        Just compile; do not link.\n");
		printf("  -p,--pedantic            Enable pedantic warnings.\n");
		printf("  -H,--print-hierarchy     Display the project hierarchy.\n");
		printf("  -S,--srcdir=<dir>        Add a source directory.\n");
		printf("  -I,--incdir=<dir>        Add an include directory.\n");
		printf("  -L,--libdir=<dir>        Add a library directory.\n");
		printf("  -P,--pkgdir=<dir>        Add a package directory.\n");
		printf("  --[no-]color             Enable or disable colored output.\n");
		printf("    --ansi-colors          Enable ANSI-based coloring.\n");
#if MK_WINDOWS_COLORS_ENABLED
		printf("    --win32-colors         Enable Windows-based coloring.\n");
#endif
		printf("  --[no-]builtin-autolinks Enable built-in autolinks (default).\n");
		printf("  --[no-]user-autolinks    Enable loading of mk-autolinks.txt (default).\n");
		printf("\n");
		printf("See the documentation (or source code) for more details.\n");
	}

	/* exit if no targets were specified and a message was requested */
	if( g_flags&(kFlag_ShowVersion_Bit|kFlag_ShowHelp_Bit )
	 && !SL_Size(g_targets)) {
		exit(EXIT_SUCCESS);
	}

	/* add builtin autolinks / libraries */
	if( builtinautolinks ) {
		/* add the autolinks */
		for( j=0; j<sizeof(autolinks)/sizeof(autolinks[0]); j++ ) {
			al = AL_New();
	
			AL_SetLib(al, autolinks[j].lib);
	
			AL_SetHeader(al, kProjSys_MSWin , autolinks[j].header[0]);
			AL_SetHeader(al, kProjSys_UWP   , autolinks[j].header[0]);
			AL_SetHeader(al, kProjSys_Cygwin, autolinks[j].header[1]);
			AL_SetHeader(al, kProjSys_Linux , autolinks[j].header[1]);
			AL_SetHeader(al, kProjSys_MacOSX, autolinks[j].header[2]);
			AL_SetHeader(al, kProjSys_Unix  , autolinks[j].header[1]);
		}
	
		/* add the libraries */
		for( j=0; j<sizeof(libs)/sizeof(libs[0]); j++ ) {
			lib = Lib_New();
	
			Lib_SetName(lib, libs[j].lib);
	
			Lib_SetFlags(lib, kProjSys_MSWin , libs[j].flags[0]);
			Lib_SetFlags(lib, kProjSys_UWP   , libs[j].flags[0]);
			Lib_SetFlags(lib, kProjSys_Cygwin, libs[j].flags[1]);
			Lib_SetFlags(lib, kProjSys_Linux , libs[j].flags[1]);
			Lib_SetFlags(lib, kProjSys_MacOSX, libs[j].flags[2]);
			Lib_SetFlags(lib, kProjSys_Unix  , libs[j].flags[1]);
		}
	}
	
	/* load user autolinks / libraries */
	if( userautolinks ) {
		/* FIXME: Load up ~/.mk/mk-autolinks.txt first if available */
		
		/* load from the current directory */
		( void )AL_LoadConfig( "mk-autolinks.txt" );
	}

	/* grab the available directories */
	Prj_FindRootDirs(g_srcdirs, g_incdirs, g_libdirs, g_pkgdirs, g_tooldirs,
		g_dllsdirs);

	/* if there aren't any source directories, complain */
	if( !SL_Size(g_srcdirs) && !SL_Size(g_pkgdirs) ) {
		Log_FatalError("no source ('src' or 'source') or package ('pkg') directories");
	}
}

int main(int argc, char **argv) {
	Mk_Init(argc, argv);

	if( g_flags & kFlag_PrintHierarchy_Bit ) {
		printf("Source Directories:\n");
		SL_Print(g_srcdirs);

		printf("Include Directories:\n");
		SL_Print(g_incdirs);

		printf("Library Directories:\n");
		SL_Print(g_libdirs);

		printf("Targets:\n");
		SL_Print(g_targets);

		printf("Projects:\n");
		Prj_PrintAll((project_t)0, "  ");
		printf("\n");
		fflush(stdout);
	}

	if( !Mk_AllProjects() ) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
