﻿#include "ConstructBuffer.hpp"

#include <stdio.h>
#include <stdlib.h>

class CTest
{
public:
	CTest()
	: m_A( 0 )
	, m_X( 0 )
	, m_Y( 0 )
	{
		printf( "+CTest\n" );
	}
	~CTest()
	{
		printf( "-CTest\n" );
	}

	void LdX( int x ) { m_X = x; }
	void LdY( int y ) { m_Y = y; }
	void Add() { m_A = m_X + m_Y; }
	void Sub() { m_A = m_X - m_Y; }
	void TAX() { m_X = m_A; }
	void TAY() { m_Y = m_A; }
	void TXA() { m_A = m_X; }
	void TYA() { m_A = m_Y; }

	void Show() { printf( "A:%i X:%i Y:%i\n", m_A, m_X, m_Y ); }

private:
	int m_A;
	int m_X;
	int m_Y;
};

struct SGlob
{
	Ax::TConstructBuffer<CTest> Test;

	void Init()
	{
		printf( "<Init>\n" );
		Test.Construct();
		printf( "</Init>\n" );
	}
	void Fini()
	{
		printf( "<Fini>\n" );
		Test.Destruct();
		printf( "</Fini>\n" );
	}
};

SGlob G;

int main()
{
	G.Init();

	G.Test->Show();

	G.Test->LdX( 3 );
	G.Test->LdY( 5 );

	G.Test->Add();

	G.Test->Show();

	G.Fini();

	return EXIT_SUCCESS;
}
