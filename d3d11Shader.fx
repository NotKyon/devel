/*
===============================================================================

	COMMON CONSTANTS

===============================================================================
*/

#define PI 3.1415926535897932384626433832795
#define INV_PI 0.31830988618379067153776752674503

/*
===============================================================================

	LAYOUT STRUCTURES

	Notation:
	- C = clip space
	- M = model space (AKA local space, object space)
	- T = tangent space (AKA texture space)
	- V = view space
	- W = world space

===============================================================================
*/

// ==== ** Vertex shader input ** ====
struct vertex_t {

	float3 positionM : POSITION;
	float3 normalM : NORMAL;
	float3 tangentM : TANGENT;
	float3 bitangentM : BINORMAL;
	float2 texcoord0 : TEXCOORD0;

};
// ==== ** Fragment shader input / vertex shader output ** ====
struct fragment_t {

	float4 positionC : SV_Position;
	float3 positionW : POSITIONWS;
	float3 texcoord0 : TEXCOORD0; //z contains depthV
	float3 normalW : NORMALWS;
	float3 tangentW : TANGENTWS;
	float3 bitangentW : BITANGENTWS;

};
// ==== ** Rasterizer input / fragment shader output ** ====
struct rasterizer_t {

	float4 color : SV_Target0;

};


/*
===============================================================================

	RESOURCES

===============================================================================
*/

// ==== ** Albedo (diffuse) ** ====
texture albedoTexture : register( t0 ) <
	string ResourceName = "stones.png";
	string ResourceType = "2D";
>;
sampler2D albedoSampler : register( s0 ) = sampler_state {
	Texture = <albedoTexture>;
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

// ==== ** Normals ** ====
texture normalTexture : register( t1 ) <
	string ResourceName = "stones_NM_height.png";
	string ResourceType = "2D";
>;
sampler2D normalSampler : register( s1 ) = sampler_state {
	Texture = <normalTexture>;
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};


/*
===============================================================================

	UNIFORM INPUTS

===============================================================================
*/

// ==== ** Transformation matrices ** ====
float4x4 modelToWorld : World;
//float4x4 modelToView : WorldView;
float4x4 worldToView : View;
float4x4 modelToClip : WorldViewProjection;
//float4x4 worldToModelTranspose : WorldInverseTranspose;
//float4x4 worldToModel : WorldInverse;
float4x4 viewToWorld : ViewInverse; //needed for position of camera because NF doesn't have a "EYEPOS" semantic

// ==== ** Lighting ** ====
float3 lightDirW = { 0.2, 0.9, -0.4 };
float3 lightColor = { 4, 2.5, 1 };

#define MAX_POINT_LIGHTS 2

float pointLightFalloff = 2.0;
static const float pointLightIntensity = 15.0;

float3 pointLightColor[ MAX_POINT_LIGHTS ] = {
	//float3( 0.5, 0.5, 0.5 )*pointLightIntensity,
	float3( 0.2, 0.5, 0.8 )*pointLightIntensity,
	float3( 0.7, 0.3, 0.4 )*pointLightIntensity
};
float3 pointLightPosW[ MAX_POINT_LIGHTS ] = {
	float3( 0.0, 2.5, 0.0 ),
	float3( 0.0, 0.5, 2.5 )
};

// ==== ** Material ** ====
float roughness = 0.1;


/*
===============================================================================

	LIGHTING UTILITY FUNCTIONS

===============================================================================
*/

// Schlick's fresnel approximation
float3 schlick_fresnel( in float3 specularAlbedo, in float LdotH ) {

	const bool isSpecularZero = dot( specularAlbedo, 1.0 ) > 0.0;
	return float3( specularAlbedo + ( 1.0 - specularAlbedo )*pow( ( 1.0 - LdotH ), 5.0 ) )*isSpecularZero;

}

// Helper for beckmann_g()
float beckmann_g1( in float m, in float NdotX ) {

	const float NdotX2 = NdotX*NdotX;
	const float a = 1.0/( m*sqrt( ( 1 - NdotX2 )/NdotX2 ) );
	const float a2 = a*a;

	return ( a < 1.6 ) ? ( 3.535*a + 2.181*a2 )/( 1.0 + 2.276*a + 2.577*a2 ) : 1.0;

}

// Calculate Beckmann specular
//
// m: Roughness
// N: Normal
// H: Half vector
// V: View
// L: Light direction
float beckmann_g( in float m, in float3 N, in float3 H, in float3 V, in float3 L ) {

	const float NdotH = max( dot( N, H ), 0.0001 );
	const float NdotL = saturate( dot( N, L ) );
	const float NdotV = max( dot( N, V ), 0.0001 );

	const float NdotH2 = NdotH*NdotH;
	const float NdotH4 = NdotH2*NdotH2;
	const float m2 = m*m;

	const float d = exp( -( ( 1 - NdotH2 )/NdotH2 )/m2 )/( PI*m2*NdotH4 );

	const float g = beckmann_g1( m, NdotL )*beckmann_g1( m, NdotV );

	return d*g*( 1.0/( 4.0*NdotL*NdotV ) );

}

float3 lighting_directional( in float m, in float3 N, in float3 L, in float3 V,
in float3 lightColor, in float3 diffuseAlbedo, in float3 specularAlbedo ) {

	float3 irradiance = 0.0;
	float NdotL = saturate( dot( N, L ) );

	if( NdotL > 0.0 ) {
		const float3 H = normalize( L + V );
		const float specular = beckmann_g( m, N, H, V, L );
		const float3 fresnel = schlick_fresnel( specularAlbedo, dot( H, L ) );

		irradiance = ( diffuseAlbedo*INV_PI + specular*fresnel )*lightColor*NdotL;
	}

	return max( irradiance, 0.0 );

}

float3 lighting_point( in float m, in float3 N, in float3 lightPosW,
in float3 lightColor, in float lightFalloff, in float3 diffuseAlbedo,
in float3 specularAlbedo, in float3 positionW, in float3 V ) {

	const float3 lightPosM = lightPosW - positionW;
	const float lightDist = length( lightPosM );
	const float3 L = lightPosM/lightDist;

	const float attenuation = 1.0/pow( lightDist, lightFalloff );

	return lighting_directional( m, N, L, V, lightColor, diffuseAlbedo, specularAlbedo )*attenuation;

}

/*
===============================================================================

	VERTEX PROGRAM

===============================================================================
*/

// ==== ** Primary program ** ====
fragment_t main_vp( in vertex_t vertex ) {

	fragment_t result;

	result.positionC = mul( float4( vertex.positionM, 1 ), modelToClip );
	result.positionW = mul( float4( vertex.positionM, 0 ), modelToWorld ).xyz;
	result.texcoord0 =
		float3(
			vertex.texcoord0,
			mul( float4( result.positionW, 1 ), worldToView ).z
		);
	result.normalW = normalize( mul( vertex.normalM, ( float3x3 )modelToWorld ) );
	result.tangentW = normalize( mul( vertex.tangentM, ( float3x3 )modelToWorld ) );
	result.bitangentW = normalize( mul( vertex.bitangentM, ( float3x3 )modelToWorld ) );

	return result;

}


/*
===============================================================================

	FRAGMENT PROGRAM

===============================================================================
*/

// ==== ** Primary program ** ====
rasterizer_t main_fp( in fragment_t fragment ) {

	rasterizer_t result;

	const float3x3 tangentToWorld =
		float3x3(
			normalize( fragment.tangentW ),
			normalize( fragment.bitangentW ),
			normalize( fragment.normalW )
		);

	const float3 normalT = tex2D( normalSampler, fragment.texcoord0.xy ).xyz*2 - 1;
	const float3 normalW = normalize( mul( normalT, tangentToWorld ) );
	const float normalLen = length( normalT );

	const float3 diffuseAlbedo = tex2D( albedoSampler, fragment.texcoord0.xy ).xyz/PI;
	const float specularAlbedo = 0.05;

	const float toksvigShininess = 2.0/( roughness*roughness ) - 2.0;
	const float toksvigFt = max( normalLen/lerp( toksvigShininess, 1.0, normalLen ), 0.01 );
	const float toksvigRoughness = sqrt( 2.0/( toksvigShininess*toksvigFt + 2.0 ) );

	const float3 V = normalize( viewToWorld[ 3 ].xyz - fragment.positionW );

	float3 irradiance = lighting_directional( toksvigRoughness, normalW,
		lightDirW, V, lightColor, diffuseAlbedo, specularAlbedo );

	[unroll]
	for( uint i = 0; i < MAX_POINT_LIGHTS; ++i ) {
		irradiance +=
			lighting_point(
				toksvigRoughness, normalW, pointLightPosW[ i ],
				pointLightColor[ i ], pointLightFalloff, diffuseAlbedo,
				specularAlbedo, fragment.positionW, V
			);
	}

	result.color = float4( irradiance, 1.0 );

	return result;

}


/*
===============================================================================

	RENDER PROGRAM

===============================================================================
*/

// ==== ** State ** ====
DepthStencilState DepthEnabled { DepthEnable = TRUE; };

// ==== ** Primary program ** ====
technique11 main_rp {

	pass main_pass {

		SetVertexShader( CompileShader( vs_5_0, main_vp() ) );
		SetHullShader( NULL );
		SetDomainShader( NULL );
		SetGeometryShader( NULL );
		SetPixelShader( CompileShader( ps_5_0, main_fp() ) );

		SetDepthStencilState( DepthEnabled, 0 );

	}

}
