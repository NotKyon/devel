/* http://lab.polygonal.de/?p=205 <- original
   wrap*, sign, abs_bit, and sin_highopt were added */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifndef ENABLE_HIGHOPT_DOUBLE
# define ENABLE_HIGHOPT_DOUBLE 1
#endif

#if ENABLE_HIGHOPT_DOUBLE
# define wraprad_ho wraprad_d
# define HO_UINT(x) (x##ULL)
typedef double real_t;
typedef signed long long int sintr_t;
typedef unsigned long long int uintr_t;
#else
# define wraprad_ho wraprad_f
# define HO_UINT(x) (x##UL)
typedef float real_t;
typedef signed int sintr_t;
typedef unsigned int uintr_t;
#endif
#define wraprad wraprad_f
static const float PI = 3.14159265358979f;

float wrap360(float angle) {
	if ((*(unsigned int *)&angle) > 0x43B40000)
		angle -= ((float)((int)(angle/360.0f)))*360.0f;

	return angle;
}
float wrap180(float angle) {
	angle = wrap360(angle);
	return angle > 180.0f ? angle - 360.0f : angle;
}

double wraprad_d(double angle) {
	if (angle < 0.0f || angle > PI*2)
		angle -= floorf(angle/PI)*PI;

	return angle;
}
float wraprad_f(float angle) {
	if (angle < 0.0f || angle > PI*2)
		angle -= floorf(angle/PI)*PI;

	return angle;
}

real_t sign(real_t x) {
	return (*(sintr_t *)&x)>>(sizeof(sintr_t)*8 - 1);
}
real_t abs_bit(real_t x) {
	uintr_t i;

	i = *(uintr_t *)&x;
	i &= ~(HO_UINT(1)<<(sizeof(uintr_t)*8 - 1));

	return *(real_t *)&i;
}

float sin_high(float x) {
	float y;

	x = wraprad(x);
	y = 0.0f;

	if (x < 0)
	{
		y = 1.27323954f * x + 0.405284735f * x * x;
		
		if (y < 0.0f)
			y = 0.225f * (y *-y - y) + y;
		else
			y = 0.225f * (y * y - y) + y;
	}
	else
	{
		y = 1.27323954f * x - 0.405284735f * x * x;
		
		if (y < 0.0f)
			y = 0.225f * (y *-y - y) + y;
		else
			y = 0.225f * (y * y - y) + y;
	}

	return y;
}
real_t sin_highopt(real_t x) {
	x = wraprad(x);

	x = 1.27323954*x + sign(-x)*0.405284735*x*x;
	x = 0.225*(x*abs_bit(x)*x - x) + x;

	return x;
}

void test(float angle) {
#if 0
	printf("wraprad(%g) -> %g\n", angle, wraprad(angle));
	printf("sign(%g) -> %g\n", angle, sign(angle));
	printf("abs_bit(%g) -> %g\n", angle, abs_bit(angle));
#else
	printf("/* ============================================ */\n");
	printf("sin(%g) -> %g\n", angle, sin(angle));
	printf("sin_high(%g) -> %g\n", angle, sin_high(angle));
	printf("sin_highopt(%g) -> %g\n", angle, sin_highopt(angle));
	printf("\n");
	printf("%g <- sin versus sin_high\n",
		abs_bit(sin(angle) - sin_high(angle)));
	printf("%g <- sin versus sin_highopt\n",
		abs_bit(sin(angle) - sin_highopt(angle)));
	printf("%g <- sin_high versus sin_highopt\n",
		abs_bit(sin_high(angle) - sin_highopt(angle)));
	printf("\n\n");
#endif
}

int main() {
	test(3.0f);
	test(3.2f);
	test(99.9f);
	test(-3.0f);
	test(-3.2f);
	test(-99.9f);

	return EXIT_SUCCESS;
}
