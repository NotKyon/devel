#ifndef AXTEK_THREADING_TASK_H
#define AXTEK_THREADING_TASK_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

#include "CCommandBuffer.h"

__AX_TASK_NS_INIT__

/*
 * TODO: scheduler, dependencies, etc (uses CCommandBuffer)
 */

__AX_TASK_NS_FINI__

#endif
