#define GB_INIT 1
#include <gamebase.h>

//
//	Handles are used as the main interface to engine-level features. They
//	provide a way to encapsulate standard functionality, such as reference
//	counting, as well as a way to improve debugging, via handle validation.
//
//	Handles are partitioned by their types. For example, one handle might
//	represent a player, while another might represent an item. You can add
//	handle types to suit your needs. Then, once you have a handle type made,
//	you can start making handles!
//
//	This sort of functionality is mostly useful for plugin-writers or people
//	extending the engine itself. As mentioned above, handles are the main
//	interface to the engine.
//

//------------------------------------------------------------------------------

// We'll start out by describing the data our handle encapsulates
struct Item {
	gb_string_t name;
	int mod_health, mod_magic;
};

// We'll add a typedef for good measure
typedef gb_handle_t ItemHandle_t;

//------------------------------------------------------------------------------

// Now we'll define a constructor for the data
// Constructors return zero (GB_FALSE) on failure, to cancel the process
static gb_bool_t ItemCtor_f(Item *p) {
	// we'll just do some basic initialization here...
	gbInitString(&p->name);

	p->mod_health = 0;
	p->mod_magic  = 0;

	// done! We didn't fail, so return GB_TRUE
	return GB_TRUE;
}
// And here's the corresponding destructor
// Destructors don't return a value since it would be unnecessary
static void ItemDtor_f(Item *p) {
	// deinitialize
	gbFiniString(&p->name);
}

//------------------------------------------------------------------------------

//
//	We need to perform basic management of the "plugin" now. First comes
//	initialization of the handle type.
//

static gb_handleType_t g_itemHandleType = (gb_handleType_t)NULL;

// Initialize the handle type
void InitItemSystem() {
	// return if we're already initialized
	if (g_itemHandleType)
		return;

	// let's initialize
	g_itemHandleType = gbAddHandleType(
		"Item", sizeof(Item),
		(gb_fnCtor_t)ItemCtor_f,
		(gb_fnDtor_t)ItemDtor_f);
}
// Now we provide a way to deinitialize the whole system
void FiniItemSystem() {
	// return if we haven't been initialized
	if (!g_itemHandleType)
		return;

	// remove the handle type
	g_itemHandleType = gbDropHandleType(g_itemHandleType);
}

//------------------------------------------------------------------------------

//
//	Now we'll provide the actual interface to the system! This should be the
//	best *PUBLIC* interface you can think of; especially if you actually intend
//	to expose the API.
//

// Handles aren't direct pointers to the objects, so we'll define an internal
// function for retrieving the direct pointer to the object.
static Item *ItemFromHandle(ItemHandle_t itemHandle) {
	// Most of the work is done for you; this is just a wrapper/convenience
	// function
	return (Item *)gbObject(itemHandle);
}

// Allocate an item from the system
ItemHandle_t NewItem() {
	// This part is pretty easy
	return gbNewHandle(g_itemHandleType);
}
// Destroy an existing item from the system
ItemHandle_t DeleteItem(ItemHandle_t item) {
	// You should just decrement the reference count of the item rather than
	// deleting it without regard to the reference count.
	return gbDropHandle(item);
}

// Set the name of an item
void SetItemName(ItemHandle_t item, const char *name) {
	// You should add an assertion (only used in debug mode!) to check the
	// validity of handles that are passed in
	gbAssert(gbIsHandleValid(item));

	// You can also add one in to ensure the type you expected is what you got
	// NOTE: This actually calls gbIsHandleValid() so the above is unnecessary.
	gbAssert(gbHandleType(item)==g_itemHandleType);

	// Let's set the name
	gbSetString(&ItemFromHandle(item)->name, name);
}
// Set the health modifier of an item
void SetItemHealthMod(ItemHandle_t item, int modifier) {
	// An example of the shortening mentioned above
	gbAssert(gbHandleType(item)==g_itemHandleType);

	// Alter the modifier
	ItemFromHandle(item)->mod_health = modifier;
}
// Set the magic modifier of an item
void SetItemMagicMod(ItemHandle_t item, int modifier) {
	gbAssert(gbHandleType(item)==g_itemHandleType);

	ItemFromHandle(item)->mod_magic = modifier;
}

// Retrieve the name of an item
const char *GetItemName(ItemHandle_t item) {
	gbAssert(gbHandleType(item)==g_itemHandleType);

	return gbStringBuffer(&ItemFromHandle(item)->name);
}
// Retrieve the health modifier of an item
int GetItemHealthMod(ItemHandle_t item) {
	gbAssert(gbHandleType(item)==g_itemHandleType);

	return ItemFromHandle(item)->mod_health;
}
// Retrieve the magic modifier of an item
int GetItemMagicMod(ItemHandle_t item) {
	gbAssert(gbHandleType(item)==g_itemHandleType);

	return ItemFromHandle(item)->mod_magic;
}

//------------------------------------------------------------------------------

//
//	Now let's test out our system!
//

void PrintItem(ItemHandle_t item) {
	const char *name;
	int healthMod, magicMod;

	name = GetItemName(item);
	healthMod = GetItemHealthMod(item);
	magicMod = GetItemMagicMod(item);

	gbPrint(gbF("Item %s { health-mod: %i; magic-mod: %i; }",
		name, healthMod, magicMod));
}

void GBMain() {
	ItemHandle_t a, b;

	InitItemSystem();

	a = NewItem();
	b = NewItem();

	SetItemName(a, "Potion");
	SetItemHealthMod(a, +25);

	SetItemName(b, "Splash Potion");
	SetItemHealthMod(b, -50);

	PrintItem(a);
	PrintItem(b);

	// we'll see some output here because we did not delete our items 'a' and
	// 'b' as we should have
	FiniItemSystem();
}
