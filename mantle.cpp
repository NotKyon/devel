// Originally from here: http://www.gpucompute.org/test/mantle/mantle.cpp

// Modified to include some information I found elsewhere.

/* mantle api sample */
#include <Windows.h>
#include <stdio.h>
#include <conio.h>

#if 0
# define GR_STDCALL     __stdcall
#else
# define GR_STDCALL
#endif

#ifdef _MSC_VER
typedef signed __int8   GR_INT8;
typedef signed __int16  GR_INT16;
typedef signed __int32  GR_INT32;
typedef signed __int64  GR_INT64;
typedef unsigned __int8 GR_UINT8;
typedef unsigned __int16 GR_UINT16;
typedef unsigned __int32 GR_UINT32;
typedef unsigned __int64 GR_UINT64;
#else
# include <stdint.h>
typedef int8_t          GR_INT8;
typedef int16_t         GR_INT16;
typedef int32_t         GR_INT32;
typedef int64_t         GR_INT64;
typedef uint8_t         GR_UINT8;
typedef uint16_t        GR_UINT16;
typedef uint32_t        GR_UINT32;
typedef uint64_t        GR_UINT64;
#endif

typedef void            GR_VOID;
typedef float           GR_FLOAT;

typedef GR_UINT32       GR_BOOL;
typedef GR_INT32        GR_RESULT;
typedef GR_UINT32       GR_ENUM;
typedef GR_UINT64       GR_SIZE;

typedef GR_UINT32       GR_DEPTH_STENCIL_OP; // TODO: Figure out members (probably matches D3D11 codes)

typedef struct _GR_DEVICE
{
} *GR_DEVICE;
typedef struct _GR_PIPELINE
{
} *GR_PIPELINE;
typedef struct _GR_DESCRIPTOR_SET
{
} *GR_DESCRIPTOR_SET;

enum _GR_BOOL
{
    GR_FALSE            = 0L,
    GR_TRUE             = 1L
};

typedef struct _GR_APPLICATION_INFO
{
    const char *        pszAppName;
} GR_APPLICATION_INFO;
typedef struct _GR_GPU_INFO
{ 
    int                 arg1; 
    int                 arg2; 
    int                 arg3; 
    int                 arg4; 
    int                 arg5;
    char                szName              [ 128 ]; 
    int                 stride              [ 128 ]; 
} GR_GPU_INFO;

/*
		GR_WSI_WIN_PRESENT_INFO presentInfo =
		{
			g_hWnd, g_MasterResourceList.Images[ DR_BACKBUFFER ],
			GR_WSI_WIN_PRESENT_MODE_BLT, 0, PresentFlags
		};
*/
typedef struct _GR_WSI_WIN_PRESENT_INFO
{
	HWND                window;
	void *              unknown__Image;
	GR_ENUM             blitMode;          // "GR_WSI_WIN_PRESENT_MODE_BLT"
	void *              unknown__Zero;
	// GR_WSI_WIN_PRESENT_FLIP_DONOTWAIT
	GR_UINT32           presentFlags;
} GR_WSI_WIN_PRESENT_INFO;

typedef struct _GR_DEPTH_STENCIL_STATE_CREATE_INFO
{
	GR_BOOL             depthEnable;
	GR_BOOL             depthWriteEnable;
	GR_ENUM             depthFunc;          // GR_COMPARE_FUNC
	GR_BOOL             depthBoundsEnable;
	GR_FLOAT            minDepth;
	GR_FLOAT            maxDepth;
	GR_BOOL             stencilEnable;
	GR_UINT8            stencilReadMask;
	GR_UINT8            stencilWriteMask;
	GR_DEPTH_STENCIL_OP front;
	GR_DEPTH_STENCIL_OP back;
} GR_DEPTH_STENCIL_STATE_CREATE_INFO;

// mantle64.dll typedefs and exported functions
typedef void (GR_STDCALL*_IcdDbgMessage)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5, void *arg6);
_IcdDbgMessage IcdDbgMessage;
typedef int (GR_STDCALL*_grAllocMemory)(void *arg1, void *arg2, void *arg3);
_grAllocMemory grAllocMemory;
typedef void (GR_STDCALL*_grAttachImageViewDescriptors)( GR_DESCRIPTOR_SET descriptorSet, int imageSlotStart, GR_SIZE imageCount, void *imageViews );
_grAttachImageViewDescriptors grAttachImageViewDescriptors;
typedef void (GR_STDCALL*_grAttachMemoryViewDescriptors)( GR_DESCRIPTOR_SET descriptorSet, int bufferSlotStart, GR_SIZE bufferCount, void *memoryViews );
_grAttachMemoryViewDescriptors grAttachMemoryViewDescriptors;
typedef void (GR_STDCALL*_grAttachNestedDescriptors)(void *arg1, void *arg2, void *arg3, void *arg4);
_grAttachNestedDescriptors grAttachNestedDescriptors;
typedef void (GR_STDCALL*_grAttachSamplerDescriptors)( GR_DESCRIPTOR_SET descriptorSet, int samplerSlotStart, GR_SIZE samplerCount, void *samplers );
_grAttachSamplerDescriptors grAttachSamplerDescriptors;
typedef int (GR_STDCALL*_grBeginCommandBuffer)(void *arg1, void *arg2);
_grBeginCommandBuffer grBeginCommandBuffer;
typedef int (GR_STDCALL*_grBeginDescriptorSetUpdate)( GR_DESCRIPTOR_SET descriptorSet );
_grBeginDescriptorSetUpdate grBeginDescriptorSetUpdate;
typedef int (GR_STDCALL*_grBindObjectMemory)(void *arg1, void *arg2, void *arg3, void *arg4);
_grBindObjectMemory grBindObjectMemory;
typedef void (GR_STDCALL*_grClearDescriptorSetSlots)(void *arg1, void *arg2, void *arg3);
_grClearDescriptorSetSlots grClearDescriptorSetSlots;
typedef void (GR_STDCALL*_grCmdBeginQuery)(void *arg1, void *arg2, void *arg3, void *arg4);
_grCmdBeginQuery grCmdBeginQuery;
typedef void (GR_STDCALL*_grCmdBindDescriptorSet)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdBindDescriptorSet grCmdBindDescriptorSet;
typedef void (GR_STDCALL*_grCmdBindDynamicMemoryView)(void *arg1, void *arg2, void *arg3);
_grCmdBindDynamicMemoryView grCmdBindDynamicMemoryView;
typedef void (GR_STDCALL*_grCmdBindIndexData)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdBindIndexData grCmdBindIndexData;
typedef void (GR_STDCALL*_grCmdBindPipeline)(void *arg1, void *arg2, void *arg3);
_grCmdBindPipeline grCmdBindPipeline;
typedef void (GR_STDCALL*_grCmdBindStateObject)(void *arg1, void *arg2, void *arg3);
_grCmdBindStateObject grCmdBindStateObject;
typedef void (GR_STDCALL*_grCmdBindTargets)(void *arg1, void *arg2, void *arg3, void *arg4);
_grCmdBindTargets grCmdBindTargets;
typedef void (GR_STDCALL*_grCmdClearColorImage)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdClearColorImage grCmdClearColorImage;
typedef void (GR_STDCALL*_grCmdClearColorImageRaw)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdClearColorImageRaw grCmdClearColorImageRaw;
typedef void (GR_STDCALL*_grCmdClearDepthStencil)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5, void *arg6);
_grCmdClearDepthStencil grCmdClearDepthStencil;
typedef void (GR_STDCALL*_grCmdCloneImageData)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdCloneImageData grCmdCloneImageData;
typedef void (GR_STDCALL*_grCmdCopyImage)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdCopyImage grCmdCopyImage;
typedef void (GR_STDCALL*_grCmdCopyImageToMemory)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdCopyImageToMemory grCmdCopyImageToMemory;
typedef void (GR_STDCALL*_grCmdCopyMemory)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdCopyMemory grCmdCopyMemory;
typedef void (GR_STDCALL*_grCmdCopyMemoryToImage)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdCopyMemoryToImage grCmdCopyMemoryToImage;
typedef void (GR_STDCALL*_grCmdDbgMarkerBegin)(void *arg1, void *arg2);
_grCmdDbgMarkerBegin grCmdDbgMarkerBegin;
typedef void (GR_STDCALL*_grCmdDbgMarkerEnd)(void *arg1);
_grCmdDbgMarkerEnd grCmdDbgMarkerEnd;
typedef void (GR_STDCALL*_grCmdDispatch)(void *arg1, void *arg2, void *arg3, void *arg4);
_grCmdDispatch grCmdDispatch;
typedef void (GR_STDCALL*_grCmdDispatchIndirect)(void *arg1, void *arg2, void *arg3, void *arg4);
_grCmdDispatchIndirect grCmdDispatchIndirect;
typedef void (GR_STDCALL*_grCmdDraw)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdDraw grCmdDraw;
typedef void (GR_STDCALL*_grCmdDrawIndexed)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5, void *arg6);
_grCmdDrawIndexed grCmdDrawIndexed;
typedef void (GR_STDCALL*_grCmdDrawIndexedIndirect)(void *arg1, void *arg2, void *arg3, void *arg4);
_grCmdDrawIndexedIndirect grCmdDrawIndexedIndirect;
typedef void (GR_STDCALL*_grCmdDrawIndirect)(void *arg1, void *arg2, void *arg3, void *arg4);
_grCmdDrawIndirect grCmdDrawIndirect;
typedef void (GR_STDCALL*_grCmdEndQuery)(void *arg1, void *arg2, void *arg3);
_grCmdEndQuery grCmdEndQuery;
typedef void (GR_STDCALL*_grCmdFillMemory)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5, void *arg6, void *arg7);
_grCmdFillMemory grCmdFillMemory;
typedef void (GR_STDCALL*_grCmdInitAtomicCounters)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdInitAtomicCounters grCmdInitAtomicCounters;
typedef void (GR_STDCALL*_grCmdLoadAtomicCounters)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5, void *arg6, void *arg7);
_grCmdLoadAtomicCounters grCmdLoadAtomicCounters;
typedef void (GR_STDCALL*_grCmdMemoryAtomic)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5, void *arg6, void *arg7);
_grCmdMemoryAtomic grCmdMemoryAtomic;
typedef void (GR_STDCALL*_grCmdPrepareImages)(void *arg1, void *arg2, void *arg3);
_grCmdPrepareImages grCmdPrepareImages;
typedef void (GR_STDCALL*_grCmdPrepareMemoryRegions)(void *arg1, void *arg2, void *arg3);
_grCmdPrepareMemoryRegions grCmdPrepareMemoryRegions;
typedef void (GR_STDCALL*_grCmdResetEvent)(void *arg1, void *arg2);
_grCmdResetEvent grCmdResetEvent;
typedef void (GR_STDCALL*_grCmdResetQueryPool)(void *arg1, void *arg2, void *arg3, void *arg4);
_grCmdResetQueryPool grCmdResetQueryPool;
typedef void (GR_STDCALL*_grCmdResolveImage)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdResolveImage grCmdResolveImage;
typedef void (GR_STDCALL*_grCmdSaveAtomicCounters)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5, void *arg6, void *arg7);
_grCmdSaveAtomicCounters grCmdSaveAtomicCounters;
typedef void (GR_STDCALL*_grCmdSetEvent)(void *arg1, void *arg2);
_grCmdSetEvent grCmdSetEvent;
typedef void (GR_STDCALL*_grCmdUpdateMemory)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5, void *arg6, void *arg7);
_grCmdUpdateMemory grCmdUpdateMemory;
typedef void (GR_STDCALL*_grCmdWriteTimestamp)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grCmdWriteTimestamp grCmdWriteTimestamp;
typedef int (GR_STDCALL*_grCreateColorBlendState)(void *arg1, void *arg2, void *arg3);
_grCreateColorBlendState grCreateColorBlendState;
typedef int (GR_STDCALL*_grCreateColorTargetView)(void *arg1, void *arg2, void *arg3);
_grCreateColorTargetView grCreateColorTargetView;
typedef int (GR_STDCALL*_grCreateCommandBuffer)(void *arg1, void *arg2, void *arg3);
_grCreateCommandBuffer grCreateCommandBuffer;
typedef int (GR_STDCALL*_grCreateComputePipeline)(void *arg1, void *arg2, void *arg3);
_grCreateComputePipeline grCreateComputePipeline;
typedef int (GR_STDCALL*_grCreateDepthStencilState)(void *arg1, void *arg2, void *arg3);
_grCreateDepthStencilState grCreateDepthStencilState;
typedef int (GR_STDCALL*_grCreateDepthStencilView)(void *arg1, void *arg2, void *arg3);
_grCreateDepthStencilView grCreateDepthStencilView;
typedef int (GR_STDCALL*_grCreateDescriptorSet)(void *arg1, void *arg2, void *arg3);
_grCreateDescriptorSet grCreateDescriptorSet;
typedef int (GR_STDCALL*_grCreateDevice)(void *arg1, void *arg2, void *arg3);
_grCreateDevice grCreateDevice;
typedef int (GR_STDCALL*_grCreateEvent)(void *arg1, void *arg2, void *arg3);
_grCreateEvent grCreateEvent;
typedef int (GR_STDCALL*_grCreateFence)(void *arg1, void *arg2, void *arg3);
_grCreateFence grCreateFence;
typedef int (GR_STDCALL*_grCreateGraphicsPipeline)(void *arg1, void *arg2, void *arg3);
_grCreateGraphicsPipeline grCreateGraphicsPipeline;
typedef int (GR_STDCALL*_grCreateImage)(void *arg1, void *arg2, void *arg3);
_grCreateImage grCreateImage;
typedef int (GR_STDCALL*_grCreateImageView)(void *arg1, void *arg2, void *arg3);
_grCreateImageView grCreateImageView;
typedef int (GR_STDCALL*_grCreateMsaaState)(void *arg1, void *arg2, void *arg3);
_grCreateMsaaState grCreateMsaaState;
typedef int (GR_STDCALL*_grCreateQueryPool)(void *arg1, void *arg2, void *arg3);
_grCreateQueryPool grCreateQueryPool;
typedef int (GR_STDCALL*_grCreateQueueSemaphore)(void *arg1, void *arg2, void *arg3);
_grCreateQueueSemaphore grCreateQueueSemaphore;
typedef int (GR_STDCALL*_grCreateRasterState)(void *arg1, void *arg2, void *arg3);
_grCreateRasterState grCreateRasterState;
typedef int (GR_STDCALL*_grCreateSampler)(void *arg1, void *arg2, void *arg3);
_grCreateSampler grCreateSampler;
typedef int (GR_STDCALL*_grCreateShader)(void *arg1, void *arg2, void *arg3);
_grCreateShader grCreateShader;
typedef int (GR_STDCALL*_grCreateViewportState)(void *arg1, void *arg2, void *arg3);
_grCreateViewportState grCreateViewportState;
typedef signed int (GR_STDCALL*_grDbgRegisterMsgCallback)(void *arg1, void *arg2);
_grDbgRegisterMsgCallback grDbgRegisterMsgCallback;
typedef int (GR_STDCALL*_grDbgSetDeviceOption)(void *arg1, void *arg2, void *arg3, void *arg4);
_grDbgSetDeviceOption grDbgSetDeviceOption;
typedef signed int (GR_STDCALL*_grDbgSetGlobalOption)(void *arg1, void *arg2, void *arg3);
_grDbgSetGlobalOption grDbgSetGlobalOption;
typedef int (GR_STDCALL*_grDbgSetMessageFilter)(void *arg1, void *arg2, void *arg3);
_grDbgSetMessageFilter grDbgSetMessageFilter;
typedef int (GR_STDCALL*_grDbgSetObjectTag)(void *arg1, void *arg2, void *arg3);
_grDbgSetObjectTag grDbgSetObjectTag;
typedef int (GR_STDCALL*_grDbgSetValidationLevel)(void *arg1, void *arg2);
_grDbgSetValidationLevel grDbgSetValidationLevel;
typedef signed int (GR_STDCALL*_grDbgUnregisterMsgCallback)(void *arg1);
_grDbgUnregisterMsgCallback grDbgUnregisterMsgCallback;
typedef int (GR_STDCALL*_grDestroyDevice)(void *arg1);
_grDestroyDevice grDestroyDevice;
typedef int (GR_STDCALL*_grDestroyObject)(void *arg1);
_grDestroyObject grDestroyObject;
typedef int (GR_STDCALL*_grDeviceWaitIdle)(void *arg1);
_grDeviceWaitIdle grDeviceWaitIdle;
typedef int (GR_STDCALL*_grEndCommandBuffer)(void *arg1);
_grEndCommandBuffer grEndCommandBuffer;
typedef void (GR_STDCALL*_grEndDescriptorSetUpdate)( GR_DESCRIPTOR_SET descriptorSet );
_grEndDescriptorSetUpdate grEndDescriptorSetUpdate;
typedef int (GR_STDCALL*_grFreeMemory)(void *arg1);
_grFreeMemory grFreeMemory;
typedef int (GR_STDCALL*_grGetDeviceQueue)(void *arg1, void *arg2, void *arg3, void *arg4);
_grGetDeviceQueue grGetDeviceQueue;
typedef int (GR_STDCALL*_grGetEventStatus)(void *arg1);
_grGetEventStatus grGetEventStatus;
typedef int (GR_STDCALL*_grGetExtensionSupport)(void *arg1, void *arg2);
_grGetExtensionSupport grGetExtensionSupport;
typedef int (GR_STDCALL*_grGetFenceStatus)(void *arg1);
_grGetFenceStatus grGetFenceStatus;
typedef int (GR_STDCALL*_grGetFormatInfo)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grGetFormatInfo grGetFormatInfo;
typedef int (GR_STDCALL*_grGetGpuInfo)(void *arg1, unsigned int arg2, void *arg3, void *arg4);
_grGetGpuInfo grGetGpuInfo;
typedef int (GR_STDCALL*_grGetImageSubresourceInfo)(void *arg1, void *arg2);
_grGetImageSubresourceInfo grGetImageSubresourceInfo;
typedef int (GR_STDCALL*_grGetMemoryHeapCount)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grGetMemoryHeapCount grGetMemoryHeapCount;
typedef int (GR_STDCALL*_grGetMemoryHeapInfo)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grGetMemoryHeapInfo grGetMemoryHeapInfo;
typedef int (GR_STDCALL*_grGetMultiGpuCompatibility)(void *arg1, void *arg2, void *arg3);
_grGetMultiGpuCompatibility grGetMultiGpuCompatibility;
typedef int (GR_STDCALL*_grGetObjectInfo)(void *arg1, void *arg2, void *arg3, void *arg4);
_grGetObjectInfo grGetObjectInfo;
typedef int (GR_STDCALL*_grGetQueryPoolResults)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grGetQueryPoolResults grGetQueryPoolResults;
typedef signed int (GR_STDCALL*_grInitAndEnumerateGpus)(void *arg1, void *arg2, void *arg3, void *arg4);
_grInitAndEnumerateGpus grInitAndEnumerateGpus;
typedef GR_RESULT(GR_STDCALL*_grLoadPipeline)(GR_DEVICE device, GR_SIZE dataSize, const GR_VOID *pData, GR_PIPELINE *pPipeline);
_grLoadPipeline grLoadPipeline;
typedef int (GR_STDCALL*_grMapMemory)(void *arg1, void *arg2, void *arg3);
_grMapMemory grMapMemory;
typedef int (GR_STDCALL*_grOpenPeerImage)(void *arg1, void *arg2, void *arg3, void *arg4);
_grOpenPeerImage grOpenPeerImage;
typedef int (GR_STDCALL*_grOpenPeerMemory)(void *arg1, void *arg2, void *arg3);
_grOpenPeerMemory grOpenPeerMemory;
typedef int (GR_STDCALL*_grOpenSharedMemory)(void *arg1, void *arg2, void *arg3);
_grOpenSharedMemory grOpenSharedMemory;
typedef int (GR_STDCALL*_grOpenSharedQueueSemaphore)(void *arg1, void *arg2, void *arg3);
_grOpenSharedQueueSemaphore grOpenSharedQueueSemaphore;
typedef int (GR_STDCALL*_grPinSystemMemory)(void *arg1, void *arg2, void *arg3, void *arg4);
_grPinSystemMemory grPinSystemMemory;
typedef int (GR_STDCALL*_grQueueSetGlobalMemReferences)(void *arg1, void *arg2, void *arg3);
_grQueueSetGlobalMemReferences grQueueSetGlobalMemReferences;
typedef int (GR_STDCALL*_grQueueSubmit)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5, void *arg6);
_grQueueSubmit grQueueSubmit;
typedef int (GR_STDCALL*_grQueueWaitIdle)(void *arg1);
_grQueueWaitIdle grQueueWaitIdle;
typedef int (GR_STDCALL*_grRemapVirtualMemoryPages)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5, void *arg6, void *arg7);
_grRemapVirtualMemoryPages grRemapVirtualMemoryPages;
typedef int (GR_STDCALL*_grResetCommandBuffer)(void *arg1);
_grResetCommandBuffer grResetCommandBuffer;
typedef int (GR_STDCALL*_grResetEvent)(void *arg1);
_grResetEvent grResetEvent;
typedef int (GR_STDCALL*_grSetEvent)(void *arg1);
_grSetEvent grSetEvent;
typedef int (GR_STDCALL*_grSetMemoryPriority)(void *arg1, void *arg2);
_grSetMemoryPriority grSetMemoryPriority;
typedef int (GR_STDCALL*_grSignalQueueSemaphore)(void *arg1, void *arg2);
_grSignalQueueSemaphore grSignalQueueSemaphore;
typedef int (GR_STDCALL*_grStorePipeline)(GR_PIPELINE pipeline, GR_SIZE *pDataSize, GR_VOID *pData);
_grStorePipeline grStorePipeline;
typedef int (GR_STDCALL*_grUnmapMemory)(void *arg1);
_grUnmapMemory grUnmapMemory;
typedef int (GR_STDCALL*_grWaitForFences)(void *arg1, void *arg2, void *arg3, void *arg4, void *arg5);
_grWaitForFences grWaitForFences;
typedef int (GR_STDCALL*_grWaitQueueSemaphore)(void *arg1, void *arg2);
_grWaitQueueSemaphore grWaitQueueSemaphore;
typedef int (GR_STDCALL*_grWsiWinCreatePresentableImage)(void *arg1, void *arg2, void *arg3, void *arg4);
_grWsiWinCreatePresentableImage grWsiWinCreatePresentableImage;
typedef int (GR_STDCALL*_grWsiWinGetDisplayModeList)(void *arg1, void *arg2, void *arg3);
_grWsiWinGetDisplayModeList grWsiWinGetDisplayModeList;
typedef int (GR_STDCALL*_grWsiWinGetDisplays)(void *arg1, void *arg2, void *arg3);
_grWsiWinGetDisplays grWsiWinGetDisplays;
typedef int (GR_STDCALL*_grWsiWinGetScanLine)(void *arg1, void *arg2);
_grWsiWinGetScanLine grWsiWinGetScanLine;
typedef int (GR_STDCALL*_grWsiWinQueuePresent)(void *arg1, void *arg2);
_grWsiWinQueuePresent grWsiWinQueuePresent;
typedef int (GR_STDCALL*_grWsiWinReleaseFullscreenOwnership)(void *arg1);
_grWsiWinReleaseFullscreenOwnership grWsiWinReleaseFullscreenOwnership;
typedef int (GR_STDCALL*_grWsiWinSetGammaRamp)(void *arg1, void *arg2);
_grWsiWinSetGammaRamp grWsiWinSetGammaRamp;
typedef int (GR_STDCALL*_grWsiWinSetMaxQueuedFrames)(void *arg1, void *arg2);
_grWsiWinSetMaxQueuedFrames grWsiWinSetMaxQueuedFrames;
typedef int (GR_STDCALL*_grWsiWinTakeFullscreenOwnership)(void *arg1, void *arg2);
_grWsiWinTakeFullscreenOwnership grWsiWinTakeFullscreenOwnership;
typedef int (GR_STDCALL*_grWsiWinWaitForVerticalBlank)(void *arg1);
_grWsiWinWaitForVerticalBlank grWsiWinWaitForVerticalBlank;

GR_APPLICATION_INFO appInfo;

GR_GPU_INFO gpuInfo;

int main(int argc, char* argv[])
{
	( void )argc;
	( void )argv;

	// load mantle64.dll module
	HMODULE mantle64dll = LoadLibraryA( "mantle64.dll" );
	if( !mantle64dll ) {
		fprintf( stderr, "ERROR: Failed to load 'mantle64.dll'" );
		fflush( stderr );

		_getch();
		return EXIT_FAILURE;
	}

	// get addresses of an exported functions
	IcdDbgMessage = (_IcdDbgMessage)GetProcAddress(mantle64dll, "IcdDbgMessage");
	grAllocMemory = (_grAllocMemory)GetProcAddress(mantle64dll, "grAllocMemory");
	grAttachImageViewDescriptors = (_grAttachImageViewDescriptors)GetProcAddress(mantle64dll, "grAttachImageViewDescriptors");
	grAttachMemoryViewDescriptors = (_grAttachMemoryViewDescriptors)GetProcAddress(mantle64dll, "grAttachMemoryViewDescriptors");
	grAttachNestedDescriptors = (_grAttachNestedDescriptors)GetProcAddress(mantle64dll, "grAttachNestedDescriptors");
	grAttachSamplerDescriptors = (_grAttachSamplerDescriptors)GetProcAddress(mantle64dll, "grAttachSamplerDescriptors");
	grBeginCommandBuffer = (_grBeginCommandBuffer)GetProcAddress(mantle64dll, "grBeginCommandBuffer");
	grBeginDescriptorSetUpdate = (_grBeginDescriptorSetUpdate)GetProcAddress(mantle64dll, "grBeginDescriptorSetUpdate");
	grBindObjectMemory = (_grBindObjectMemory)GetProcAddress(mantle64dll, "grBindObjectMemory");
	grClearDescriptorSetSlots = (_grClearDescriptorSetSlots)GetProcAddress(mantle64dll, "grClearDescriptorSetSlots");
	grCmdBeginQuery = (_grCmdBeginQuery)GetProcAddress(mantle64dll, "grCmdBeginQuery");
	grCmdBindDescriptorSet = (_grCmdBindDescriptorSet)GetProcAddress(mantle64dll, "grCmdBindDescriptorSet");
	grCmdBindDynamicMemoryView = (_grCmdBindDynamicMemoryView)GetProcAddress(mantle64dll, "grCmdBindDynamicMemoryView");
	grCmdBindIndexData = (_grCmdBindIndexData)GetProcAddress(mantle64dll, "grCmdBindIndexData");
	grCmdBindPipeline = (_grCmdBindPipeline)GetProcAddress(mantle64dll, "grCmdBindPipeline");
	grCmdBindStateObject = (_grCmdBindStateObject)GetProcAddress(mantle64dll, "grCmdBindStateObject");
	grCmdBindTargets = (_grCmdBindTargets)GetProcAddress(mantle64dll, "grCmdBindTargets");
	grCmdClearColorImage = (_grCmdClearColorImage)GetProcAddress(mantle64dll, "grCmdClearColorImage");
	grCmdClearColorImageRaw = (_grCmdClearColorImageRaw)GetProcAddress(mantle64dll, "grCmdClearColorImageRaw");
	grCmdClearDepthStencil = (_grCmdClearDepthStencil)GetProcAddress(mantle64dll, "grCmdClearDepthStencil");
	grCmdCloneImageData = (_grCmdCloneImageData)GetProcAddress(mantle64dll, "grCmdCloneImageData");
	grCmdCopyImage = (_grCmdCopyImage)GetProcAddress(mantle64dll, "grCmdCopyImage");
	grCmdCopyImageToMemory = (_grCmdCopyImageToMemory)GetProcAddress(mantle64dll, "grCmdCopyImageToMemory");
	grCmdCopyMemory = (_grCmdCopyMemory)GetProcAddress(mantle64dll, "grCmdCopyMemory");
	grCmdCopyMemoryToImage = (_grCmdCopyMemoryToImage)GetProcAddress(mantle64dll, "grCmdCopyMemoryToImage");
	grCmdDbgMarkerBegin = (_grCmdDbgMarkerBegin)GetProcAddress(mantle64dll, "grCmdDbgMarkerBegin");
	grCmdDbgMarkerEnd = (_grCmdDbgMarkerEnd)GetProcAddress(mantle64dll, "grCmdDbgMarkerEnd");
	grCmdDispatch = (_grCmdDispatch)GetProcAddress(mantle64dll, "grCmdDispatch");
	grCmdDispatchIndirect = (_grCmdDispatchIndirect)GetProcAddress(mantle64dll, "grCmdDispatchIndirect");
	grCmdDraw = (_grCmdDraw)GetProcAddress(mantle64dll, "grCmdDraw");
	grCmdDrawIndexed = (_grCmdDrawIndexed)GetProcAddress(mantle64dll, "grCmdDrawIndexed");
	grCmdDrawIndexedIndirect = (_grCmdDrawIndexedIndirect)GetProcAddress(mantle64dll, "grCmdDrawIndexedIndirect");
	grCmdDrawIndirect = (_grCmdDrawIndirect)GetProcAddress(mantle64dll, "grCmdDrawIndirect");
	grCmdEndQuery = (_grCmdEndQuery)GetProcAddress(mantle64dll, "grCmdEndQuery");
	grCmdFillMemory = (_grCmdFillMemory)GetProcAddress(mantle64dll, "grCmdFillMemory");
	grCmdInitAtomicCounters = (_grCmdInitAtomicCounters)GetProcAddress(mantle64dll, "grCmdInitAtomicCounters");
	grCmdLoadAtomicCounters = (_grCmdLoadAtomicCounters)GetProcAddress(mantle64dll, "grCmdLoadAtomicCounters");
	grCmdMemoryAtomic = (_grCmdMemoryAtomic)GetProcAddress(mantle64dll, "grCmdMemoryAtomic");
	grCmdPrepareImages = (_grCmdPrepareImages)GetProcAddress(mantle64dll, "grCmdPrepareImages");
	grCmdPrepareMemoryRegions = (_grCmdPrepareMemoryRegions)GetProcAddress(mantle64dll, "grCmdPrepareMemoryRegions");
	grCmdResetEvent = (_grCmdResetEvent)GetProcAddress(mantle64dll, "grCmdResetEvent");
	grCmdResetQueryPool = (_grCmdResetQueryPool)GetProcAddress(mantle64dll, "grCmdResetQueryPool");
	grCmdResolveImage = (_grCmdResolveImage)GetProcAddress(mantle64dll, "grCmdResolveImage");
	grCmdSaveAtomicCounters = (_grCmdSaveAtomicCounters)GetProcAddress(mantle64dll, "grCmdSaveAtomicCounters");
	grCmdSetEvent = (_grCmdSetEvent)GetProcAddress(mantle64dll, "grCmdSetEvent");
	grCmdUpdateMemory = (_grCmdUpdateMemory)GetProcAddress(mantle64dll, "grCmdUpdateMemory");
	grCmdWriteTimestamp = (_grCmdWriteTimestamp)GetProcAddress(mantle64dll, "grCmdWriteTimestamp");
	grCreateColorBlendState = (_grCreateColorBlendState)GetProcAddress(mantle64dll, "grCreateColorBlendState");
	grCreateColorTargetView = (_grCreateColorTargetView)GetProcAddress(mantle64dll, "grCreateColorTargetView");
	grCreateCommandBuffer = (_grCreateCommandBuffer)GetProcAddress(mantle64dll, "grCreateCommandBuffer");
	grCreateComputePipeline = (_grCreateComputePipeline)GetProcAddress(mantle64dll, "grCreateComputePipeline");
	grCreateDepthStencilState = (_grCreateDepthStencilState)GetProcAddress(mantle64dll, "grCreateDepthStencilState");
	grCreateDepthStencilView = (_grCreateDepthStencilView)GetProcAddress(mantle64dll, "grCreateDepthStencilView");
	grCreateDescriptorSet = (_grCreateDescriptorSet)GetProcAddress(mantle64dll, "grCreateDescriptorSet");
	grCreateDevice = (_grCreateDevice)GetProcAddress(mantle64dll, "grCreateDevice");
	grCreateEvent = (_grCreateEvent)GetProcAddress(mantle64dll, "grCreateEvent");
	grCreateFence = (_grCreateFence)GetProcAddress(mantle64dll, "grCreateFence");
	grCreateGraphicsPipeline = (_grCreateGraphicsPipeline)GetProcAddress(mantle64dll, "grCreateGraphicsPipeline");
	grCreateImage = (_grCreateImage)GetProcAddress(mantle64dll, "grCreateImage");
	grCreateImageView = (_grCreateImageView)GetProcAddress(mantle64dll, "grCreateImageView");
	grCreateMsaaState = (_grCreateMsaaState)GetProcAddress(mantle64dll, "grCreateMsaaState");
	grCreateQueryPool = (_grCreateQueryPool)GetProcAddress(mantle64dll, "grCreateQueryPool");
	grCreateQueueSemaphore = (_grCreateQueueSemaphore)GetProcAddress(mantle64dll, "grCreateQueueSemaphore");
	grCreateRasterState = (_grCreateRasterState)GetProcAddress(mantle64dll, "grCreateRasterState");
	grCreateSampler = (_grCreateSampler)GetProcAddress(mantle64dll, "grCreateSampler");
	grCreateShader = (_grCreateShader)GetProcAddress(mantle64dll, "grCreateShader");
	grCreateViewportState = (_grCreateViewportState)GetProcAddress(mantle64dll, "grCreateViewportState");
	grDbgRegisterMsgCallback = (_grDbgRegisterMsgCallback)GetProcAddress(mantle64dll, "grDbgRegisterMsgCallback");
	grDbgSetDeviceOption = (_grDbgSetDeviceOption)GetProcAddress(mantle64dll, "grDbgSetDeviceOption");
	grDbgSetGlobalOption = (_grDbgSetGlobalOption)GetProcAddress(mantle64dll, "grDbgSetGlobalOption");
	grDbgSetMessageFilter = (_grDbgSetMessageFilter)GetProcAddress(mantle64dll, "grDbgSetMessageFilter");
	grDbgSetObjectTag = (_grDbgSetObjectTag)GetProcAddress(mantle64dll, "grDbgSetObjectTag");
	grDbgSetValidationLevel = (_grDbgSetValidationLevel)GetProcAddress(mantle64dll, "grDbgSetValidationLevel");
	grDbgUnregisterMsgCallback = (_grDbgUnregisterMsgCallback)GetProcAddress(mantle64dll, "grDbgUnregisterMsgCallback");
	grDestroyDevice = (_grDestroyDevice)GetProcAddress(mantle64dll, "grDestroyDevice");
	grDestroyObject = (_grDestroyObject)GetProcAddress(mantle64dll, "grDestroyObject");
	grDeviceWaitIdle = (_grDeviceWaitIdle)GetProcAddress(mantle64dll, "grDeviceWaitIdle");
	grEndCommandBuffer = (_grEndCommandBuffer)GetProcAddress(mantle64dll, "grEndCommandBuffer");
	grEndDescriptorSetUpdate = (_grEndDescriptorSetUpdate)GetProcAddress(mantle64dll, "grEndDescriptorSetUpdate");
	grFreeMemory = (_grFreeMemory)GetProcAddress(mantle64dll, "grFreeMemory");
	grGetDeviceQueue = (_grGetDeviceQueue)GetProcAddress(mantle64dll, "grGetDeviceQueue");
	grGetEventStatus = (_grGetEventStatus)GetProcAddress(mantle64dll, "grGetEventStatus");
	grGetExtensionSupport = (_grGetExtensionSupport)GetProcAddress(mantle64dll, "grGetExtensionSupport");
	grGetFenceStatus = (_grGetFenceStatus)GetProcAddress(mantle64dll, "grGetFenceStatus");
	grGetFormatInfo = (_grGetFormatInfo)GetProcAddress(mantle64dll, "grGetFormatInfo");
	grGetGpuInfo = (_grGetGpuInfo)GetProcAddress(mantle64dll, "grGetGpuInfo");
	grGetImageSubresourceInfo = (_grGetImageSubresourceInfo)GetProcAddress(mantle64dll, "grGetImageSubresourceInfo");
	grGetMemoryHeapCount = (_grGetMemoryHeapCount)GetProcAddress(mantle64dll, "grGetMemoryHeapCount");
	grGetMemoryHeapInfo = (_grGetMemoryHeapInfo)GetProcAddress(mantle64dll, "grGetMemoryHeapInfo");
	grGetMultiGpuCompatibility = (_grGetMultiGpuCompatibility)GetProcAddress(mantle64dll, "grGetMultiGpuCompatibility");
	grGetObjectInfo = (_grGetObjectInfo)GetProcAddress(mantle64dll, "grGetObjectInfo");
	grGetQueryPoolResults = (_grGetQueryPoolResults)GetProcAddress(mantle64dll, "grGetQueryPoolResults");
	grInitAndEnumerateGpus = (_grInitAndEnumerateGpus)GetProcAddress(mantle64dll, "grInitAndEnumerateGpus");
	grLoadPipeline = (_grLoadPipeline)GetProcAddress(mantle64dll, "grLoadPipeline");
	grMapMemory = (_grMapMemory)GetProcAddress(mantle64dll, "grMapMemory");
	grOpenPeerImage = (_grOpenPeerImage)GetProcAddress(mantle64dll, "grOpenPeerImage");
	grOpenPeerMemory = (_grOpenPeerMemory)GetProcAddress(mantle64dll, "grOpenPeerMemory");
	grOpenSharedMemory = (_grOpenSharedMemory)GetProcAddress(mantle64dll, "grOpenSharedMemory");
	grOpenSharedQueueSemaphore = (_grOpenSharedQueueSemaphore)GetProcAddress(mantle64dll, "grOpenSharedQueueSemaphore");
	grPinSystemMemory = (_grPinSystemMemory)GetProcAddress(mantle64dll, "grPinSystemMemory");
	grQueueSetGlobalMemReferences = (_grQueueSetGlobalMemReferences)GetProcAddress(mantle64dll, "grQueueSetGlobalMemReferences");
	grQueueSubmit = (_grQueueSubmit)GetProcAddress(mantle64dll, "grQueueSubmit");
	grQueueWaitIdle = (_grQueueWaitIdle)GetProcAddress(mantle64dll, "grQueueWaitIdle");
	grRemapVirtualMemoryPages = (_grRemapVirtualMemoryPages)GetProcAddress(mantle64dll, "grRemapVirtualMemoryPages");
	grResetCommandBuffer = (_grResetCommandBuffer)GetProcAddress(mantle64dll, "grResetCommandBuffer");
	grResetEvent = (_grResetEvent)GetProcAddress(mantle64dll, "grResetEvent");
	grSetEvent = (_grSetEvent)GetProcAddress(mantle64dll, "grSetEvent");
	grSetMemoryPriority = (_grSetMemoryPriority)GetProcAddress(mantle64dll, "grSetMemoryPriority");
	grSignalQueueSemaphore = (_grSignalQueueSemaphore)GetProcAddress(mantle64dll, "grSignalQueueSemaphore");
	grStorePipeline = (_grStorePipeline)GetProcAddress(mantle64dll, "grStorePipeline");
	grUnmapMemory = (_grUnmapMemory)GetProcAddress(mantle64dll, "grUnmapMemory");
	grWaitQueueSemaphore = (_grWaitQueueSemaphore)GetProcAddress(mantle64dll, "grWaitQueueSemaphore");
	grWsiWinCreatePresentableImage = (_grWsiWinCreatePresentableImage)GetProcAddress(mantle64dll, "grWsiWinCreatePresentableImage");
	grWsiWinGetDisplayModeList = (_grWsiWinGetDisplayModeList)GetProcAddress(mantle64dll, "grWsiWinGetDisplayModeList");
	grWsiWinGetDisplays = (_grWsiWinGetDisplays)GetProcAddress(mantle64dll, "grWsiWinGetDisplays");
	grWsiWinGetScanLine = (_grWsiWinGetScanLine)GetProcAddress(mantle64dll, "grWsiWinGetScanLine");
	grWsiWinQueuePresent = (_grWsiWinQueuePresent)GetProcAddress(mantle64dll, "grWsiWinQueuePresent");
	grWsiWinReleaseFullscreenOwnership = (_grWsiWinReleaseFullscreenOwnership)GetProcAddress(mantle64dll, "grWsiWinReleaseFullscreenOwnership");
	grWsiWinSetGammaRamp = (_grWsiWinSetGammaRamp)GetProcAddress(mantle64dll, "grWsiWinSetGammaRamp");
	grWsiWinSetMaxQueuedFrames = (_grWsiWinSetMaxQueuedFrames)GetProcAddress(mantle64dll, "grWsiWinSetMaxQueuedFrames");
	grWsiWinTakeFullscreenOwnership = (_grWsiWinTakeFullscreenOwnership)GetProcAddress(mantle64dll, "grWsiWinTakeFullscreenOwnership");
	grWsiWinWaitForVerticalBlank = (_grWsiWinWaitForVerticalBlank)GetProcAddress(mantle64dll, "grWsiWinWaitForVerticalBlank");

	// test app
	appInfo.pszAppName = "mantle test";

	// get number of gpus
	int gpuCount = 0;
	void *gpus[ 8 ] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	if( grInitAndEnumerateGpus( &appInfo, NULL, &gpuCount, gpus ) ) {
		printf("number of gpus: %d\n", gpuCount);
	}

	// get gpu info
	int *arg;
	for( int i = 0; i < gpuCount; i++ ) {
		if( grGetGpuInfo( gpus[ i ], 0x6100, &arg, &gpuInfo ) ) {
			printf("gpu name: %s\n", gpuInfo.szName);
		}
	}

	// free mantle64.dll module
	FreeLibrary( mantle64dll );

	_getch();
	return 0;
}
