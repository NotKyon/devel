#include <ember/ember.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

struct ItemL {
	int number;
	ListLink link;
};
struct ItemB {
	int number;
	BTreeNode node;
};

void list_test()
{
	struct ItemL a, b, c, *i;
	ListBase base;

	printf( "list test\n" );

	list_init( &base );
	list_link( &a.link, &a );
	list_link( &b.link, &b );
	list_link( &c.link, &c );
	list_add( &base, &a.link );
	list_add( &base, &b.link );
	list_add( &base, &c.link );
	a.number = 2;
	b.number = 17;
	c.number = 9;

	for( i = ( struct ItemL * )list_first( &base ); i;
	     i = ( struct ItemL * )list_next( &i->link ) )
		printf( "%d\n", i->number );

	list_remove_all( &base );

	printf( "\n" );
}

void btree_test()
{
	static const int keys[ 6 ] = { 25, 30, 10, 15, 57, 43 };
	struct ItemB a, b, c, d, e, f;
	BTreeBase base;
	BTreeNode *i;
	size_t j;

	printf( "btree test\n" );

	btree_init( &base );

	btree_find( &base, keys[ 0 ], &a.node );
	btree_find( &base, keys[ 1 ], &b.node );
	btree_find( &base, keys[ 2 ], &c.node );
	btree_find( &base, keys[ 3 ], &d.node );
	btree_find( &base, keys[ 4 ], &e.node );
	btree_find( &base, keys[ 5 ], &f.node );

	btree_set( &a.node, &a );
	a.number = 1;
	btree_set( &b.node, &b );
	b.number = 2;
	btree_set( &c.node, &c );
	c.number = 3;
	btree_set( &d.node, &d );
	d.number = 4;
	btree_set( &e.node, &e );
	e.number = 5;
	btree_set( &f.node, &f );
	f.number = 6;

	btree_remove( &a.node );
	btree_remove( &d.node );

	for( j = 0; j < sizeof( keys ) / sizeof( keys[ 0 ] ); j++ ) {
		printf( "{key=%d}: ", keys[ j ] );
		if( ( i = btree_find( &base, keys[ j ], ( BTreeNode * )0 ) ) != ( BTreeNode * )0 )
			printf( "%p: %d\n", ( void * )i, ( ( struct ItemB * )i->obj )->number );
		else
			printf( "<not found>\n" );
	}

	btree_remove_all( &base );

	printf( "\n" );
}

void __domathtest( const char *name, float x, float ( *f )( float ),
    float ( *f2 )( float ), float ( *af )( float ), float ( *af2 )( float ) )
{
	printf( "%s(%f) = %f; %f => %f; %f\n", name, x, f( x ), f2( x ), af( f( x ) ),
	    af2( f( x ) ) );
}
#define dotest( n, x ) __domathtest( #n, ( x ), n##f, n##_fast, a##n##f, a##n##_fast )
void math_test()
{
	float x;

	printf( "math test\n" );

	for( x = 0.0f; x < 3.14159265358979f * 2.0f; x += 0.125f ) {
		dotest( sin, x );
		dotest( cos, x );
		dotest( tan, x );
	}

	printf( "\n" );
}
#undef dotest

void print_matrix( const char *name, const Matrix *m )
{
	printf( "%s {\n\t%.3f,%.3f,%.3f,%.3f,\n"
	        "\t%.3f,%.3f,%.3f,%.3f,\n"
	        "\t%.3f,%.3f,%.3f,%.3f,\n"
	        "\t%.3f,%.3f,%.3f,%.3f\n}\n",
	    name, m->rm._11, m->rm._12, m->rm._13, m->rm._14, m->rm._21, m->rm._22,
	    m->rm._23, m->rm._24, m->rm._31, m->rm._32, m->rm._33, m->rm._34,
	    m->rm._41, m->rm._42, m->rm._43, m->rm._44 );
}
void matrix_test()
{
	Matrix mident, mtrans, minv;

	printf( "matrix test\n" );

	mat_translation( &mtrans, 5, 25, 30 );
	mat_inverse( &minv, &mtrans );
	mat_multiply( &mident, &minv, &mtrans ); /* m*inv(m) = I */
	print_matrix( "mtrans", &mtrans );
	print_matrix( "minv", &minv );
	print_matrix( "mident", &mident );

	printf( "\n" );
}

void ray_test()
{
	float dist;
	Vec3 tri[ 3 ];
	Vec2 uv;
	Ray ray;
	int r;

	printf( "ray test\n" );

	vec3_set( &tri[ 0 ], -5, -5, 2 );
	vec3_set( &tri[ 1 ], 0, 5, 2 );
	vec3_set( &tri[ 2 ], 5, -5, 2 );

	ray_set( &ray, 0, 0, -2, 0, 0, 1 );

	vec2_set( &uv, 0, 0 );
	dist = 0;

	r = ray_intersects_tri_ex( &ray, &tri[ 0 ], &uv, &dist );
	printf( "INPUTS\n" );
	printf( "tris = { (%.2f,%.2f,%.2f), (%.2f,%.2f,%2.f), (%.2f,%.2f,%.2f) }\n",
	    tri[ 0 ].x, tri[ 0 ].y, tri[ 0 ].z, tri[ 1 ].x, tri[ 1 ].y, tri[ 1 ].z, tri[ 2 ].x,
	    tri[ 2 ].y, tri[ 2 ].z );
	printf( "ray = { (%.2f,%.2f,%.2f), (%.2f,%.2f,%.2f) }\n", ray.org.x, ray.org.y,
	    ray.org.z, ray.dir.x, ray.dir.y, ray.dir.z );
	printf( "\nRESULTS\n" );
	printf( "uv = { (%.2f,%.2f) }\n", uv.x, uv.y );
	printf( "distance = %.2f\n", dist );
	printf( "r = %s\n", ( r ) ? "true" : "false" );

	printf( "\n" );
}

void time_test()
{
	unsigned int i;

	printf( "time test\n" );

	time_init();
	for( i = 0; i < 12; i++ ) {
		time_yield();
		time_update();
		time_yield();
		printf( "%f\n", time_tween() );
		time_yield();
	}

	printf( "\n" );
}

void ent_test()
{
	scene_t s;
	entity_t a, b, c, d;

	printf( "entity test\n" );

	sce_init();
	ent_init();

	if( !( s = sce_alloc() ) )
		fprintf( stderr, "failed to allocate scene\n" );

	a = ent_alloc( 0, 0 );
	b = ent_alloc( a, 0 );
	c = ent_alloc( b, 0 );
	d = ent_alloc( b, 0 );

	ent_set_parent( b, c );
	ent_set_parent( d, a );

	if( ent_parent( a ) != ( entity_t )0 )
		printf( "a.prnt is incorrect.\n" );
	if( ent_parent( b ) != c )
		printf( "b.prnt is incorrect.\n" );
	if( ent_parent( c ) != a )
		printf( "c.prnt is incorrect.\n" );
	if( ent_parent( d ) != a )
		printf( "d.prnt is incorrect.\n" );

	ent_deinit();
	sce_deinit();

	printf( "\n" );
}
void tform_test()
{
	Matrix m;
	scene_t s;
	entity_t a, b, c, d;

	printf( "transform test\n" );

	sce_init();
	ent_init();

	if( !( s = sce_alloc() ) ) {
		fprintf( stderr, "failed to allocate scene\n" );

		ent_deinit();
		sce_deinit();
		return;
	}

	a = ent_alloc( 0, 0 );
	b = ent_alloc( a, 0 );
	c = ent_alloc( b, 0 );
	d = ent_alloc( b, 0 );

	ent_move( a, 2, 2, 2 );

	ent_move( b, 5, 2, 0 );

	ent_turn_y( c, 3.1415926535 );
	ent_move( c, -1, -1, -1 );

	ent_turn_x( d, 3.1415926535 / 2 );
	ent_turn_y( d, 3.1415926535 / 2 );
	ent_move( d, 2, 6, 2 );

#if 1
	mat_identity( &m );
	ent_set_global_matrix( d, &m );
/*ent_translate(d, 0, 0, 0);*/
/*ent_move(d, 0, 0, 5);*/
#else
	mat_inverse_world( &m, ent_local_matrix( d ) );
	print_matrix( "ent_local", ent_local_matrix( d ) );
	print_matrix( "inv_local", &m );
#endif

	print_matrix( "ent_a", ent_global_matrix( a ) );
	print_matrix( "ent_b", ent_global_matrix( b ) );
	print_matrix( "ent_c", ent_global_matrix( c ) );
	print_matrix( "ent_d", ent_global_matrix( d ) );

	ent_deinit();
	sce_deinit();

	printf( "\n" );
}

const char *fx_property_kind_name( property_t pr )
{
	EPropertyKind kind;

	kind = fx_property_kind( pr );

	switch( kind ) {
	case EPropertyKind_Invalid:
		return "(invalid)";
	case EPropertyKind_Range:
		return "Range";
	case EPropertyKind_Color:
		return "Color";
	case EPropertyKind_Texture2D:
		return "Texture2D";
	case EPropertyKind_TextureRect:
		return "TextureRect";
	case EPropertyKind_TextureCube:
		return "TextureCube";
	case EPropertyKind_Float:
		return "Float";
	case EPropertyKind_Vector:
		return "Vector";
	default:
		return "(unknown)";
	}
}
const char *fx_property_texgen_name( property_t pr )
{
	EPropertyTexGen texgen;

	texgen = fx_property_texgen( pr );

	switch( texgen ) {
	case EPropertyTexGen_None:
		return "(none)";

	case EPropertyTexGen_ObjectLinear:
		return "ObjectLinear";
	case EPropertyTexGen_EyeLinear:
		return "EyeLinear";
	case EPropertyTexGen_SphereMap:
		return "SphereMap";
	case EPropertyTexGen_CubeReflect:
		return "CubeReflect";
	case EPropertyTexGen_CubeNormal:
		return "CubeNormal";

	default:
		return "(unknown)";
	}
}
void fx_print_tags( tagowner_t owner )
{
	tag_t tg;

	for( tg = fx_tag_first( owner ); tg; tg = fx_tag_after( tg ) ) {
		printf( "  tag: \"%s\"=\"%s\"\n", fx_tag_name( tg ), fx_tag_value( tg ) );
	}
}
void fx_print( effect_t effect )
{
	EPropertyKind kind;
	technique_t tech;
	property_t property;
	pass_t pass;

	printf( "effect: %s\n", fx_name( effect ) );
	for( property = fx_property_first( effect ); property;
	     property = fx_property_after( property ) ) {
		printf( "property: %s\n", fx_property_name( property ) );
		printf( "  description: %s\n", fx_property_desc( property ) );
		printf( "  kind       : %s\n", fx_property_kind_name( property ) );
		kind = fx_property_kind( property );
		switch( kind ) {
		case EPropertyKind_Range:
			printf( "  range      : %.4f, %.4f\n", fx_property_range( property, 0 ),
			    fx_property_range( property, 1 ) );
		case EPropertyKind_Float:
			printf( "  scalar     : %.4f\n", fx_property_scalar( property ) );
			break;
		case EPropertyKind_Color:
		case EPropertyKind_Vector:
			printf( "  value      : (%.4f, %.4f, %.4f, %.4f)\n",
			    fx_property_vec_x( property ), fx_property_vec_y( property ),
			    fx_property_vec_z( property ), fx_property_vec_w( property ) );
			break;
		case EPropertyKind_Texture2D:
		case EPropertyKind_TextureRect:
		case EPropertyKind_TextureCube:
			printf( "  texture    : \"%s\"\n", fx_property_string( property ) );
			printf( "  texgen mode: %s\n", fx_property_texgen_name( property ) );
			break;
		default:
			printf( " ...?\n" );
			break;
		}
	}

	for( tech = fx_technique_first( effect ); tech;
	     tech = fx_technique_after( tech ) ) {
		printf( "technique\n" );
		fx_print_tags( fx_technique_tagowner( tech ) );

		for( pass = fx_pass_first( tech ); pass; pass = fx_pass_after( pass ) ) {
			printf( "pass: %s\n", fx_pass_name( pass ) );
			fx_print_tags( fx_pass_tagowner( pass ) );
		}
	}

	printf( "<end of effect>\n" );
	fflush( stdout );
}
void fx_test()
{
	effect_t effect;

	buf_init();
	fx_init();

	printf( "effect test\n" );

	if( !( effect = fx_sl_load( "media/shaders/test.sl" ) ) ) {
		fprintf( stderr, "failed to load effect\n" );
		printf( "\n" );
		return;
	}

	fx_print( effect );

	fx_dealloc( effect );

	fx_deinit();
	buf_deinit();

	printf( "\n" );
}

void gl_test()
{
	printf( "gl test\n" );

	egl_set_param( EGLParam_WindowVisible, 1 );
	if( !egl_init() ) {
		fprintf( stderr, "ERROR: failed to initialize EmberGL\n" );
		printf( "\n" );
		return;
	}

	printf( "GL_VENDOR: %s\n", glGetString( GL_VENDOR ) );
	printf( "GL_RENDERER: %s\n", glGetString( GL_RENDERER ) );
	printf( "GL_VERSION: %s\n", glGetString( GL_VERSION ) );
	printf( "GL_SHADING_LANGUAGE_VERSION: %s\n",
	    glGetString( GL_SHADING_LANGUAGE_VERSION ) );
	printf( "GL_EXTENSIONS: %s\n", glGetString( GL_EXTENSIONS ) );
	printf( "\n" );
	printf( "Retrieved version: %d.%d\n", egl_get_param( EGLParam_MajorVersion ),
	    egl_get_param( EGLParam_MinorVersion ) );

	while( egl_running() ) {
		egl_update();
		egl_flip();
	}

	egl_deinit();
	printf( "\n" );
}

void all_tests()
{
	printf( "**Running all tests...\n" );

	list_test();
	btree_test();
	math_test();
	matrix_test();
	ray_test();
	time_test();
	ent_test();
	tform_test();
	fx_test();
	gl_test();

	printf( "**Done running all tests!\n\n" );
}
int main( int argc, char **argv )
{
	int i;

	for( i = 1; i < argc; i++ ) {
		if( !strcmp( argv[ i ], "all" ) )
			all_tests();
		else if( !strcmp( argv[ i ], "list" ) )
			list_test();
		else if( !strcmp( argv[ i ], "btree" ) )
			btree_test();
		else if( !strcmp( argv[ i ], "math" ) )
			math_test();
		else if( !strcmp( argv[ i ], "matrix" ) )
			matrix_test();
		else if( !strcmp( argv[ i ], "ray" ) )
			ray_test();
		else if( !strcmp( argv[ i ], "time" ) )
			time_test();
		else if( !strcmp( argv[ i ], "entity" ) )
			ent_test();
		else if( !strcmp( argv[ i ], "tform" ) )
			tform_test();
		else if( !strcmp( argv[ i ], "effect" ) )
			fx_test();
		else if( !strcmp( argv[ i ], "gl" ) )
			gl_test();
		else
			fprintf( stderr, "unknown test \"%s\"\n", argv[ i ] );
	}

	if( argc <= 1 )
		all_tests();

	return 0;
}
