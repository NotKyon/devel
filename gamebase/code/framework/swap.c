#include <gamebase.h>

#define BCW(c,x,i,s) ((((c)(((gb_u8_t *)&(x))[(i)]))<<(s)))
#define B16(x,i,s) BCW(gb_u16_t,(x),(i),(s))
#define B32(x,i,s) BCW(gb_u32_t,(x),(i),(s))
#define B64(x,i,s) BCW(gb_u64_t,(x),(i),(s))
gb_u16_t gbBigEndian16(gb_u16_t x) {
	return B16(x,0,8)|B16(x,1,0);
}
gb_u32_t gbBigEndian32(gb_u32_t x) {
	return B32(x,0,24)|B32(x,1,16)|B32(x,2,8)|B32(x,3,0);
}
gb_u64_t gbBigEndian64(gb_u64_t x) {
	return B64(x,0,56)|B64(x,1,48)|B64(x,2,40)|B64(x,3,32)|
	       B64(x,4,24)|B64(x,5,16)|B64(x,6, 8)|B64(x,7, 0);
}

gb_u16_t gbLittleEndian16(gb_u16_t x) {
	return B16(x,0,0)|B16(x,1,8);
}
gb_u32_t gbLittleEndian32(gb_u32_t x) {
	return B32(x,0,0)|B32(x,1,8)|B32(x,2,16)|B32(x,3,24);
}
gb_u64_t gbLittleEndian64(gb_u64_t x) {
	return B64(x,0,0) |B64(x,1,8) |B64(x,2,16)|B64(x,3,24)|
	       B64(x,4,32)|B64(x,5,40)|B64(x,6,48)|B64(x,7,56);
}
#undef B64
#undef B32
#undef B16
#undef BCW

float gbBigEndianFloat(float x) {
	gb_u32_t r;

	r = gbBigEndian32(*(gb_u32_t *)&x);

	return *(float *)&r;
}
double gbBigEndianDouble(double x) {
	gb_u64_t r;

	r = gbBigEndian64(*(gb_u64_t *)&x);

	return *(double *)&r;
}

float gbLittleEndianFloat(float x) {
	gb_u32_t r;

	r = gbLittleEndian32(*(gb_u32_t *)&x);

	return *(float *)&r;
}
double gbLittleEndianDouble(double x) {
	gb_u64_t r;

	r = gbLittleEndian64(*(gb_u64_t *)&x);

	return *(double *)&r;
}
