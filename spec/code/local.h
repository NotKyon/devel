#ifndef LOCAL_H
#define LOCAL_H

#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "assert.h"

#ifndef TRUE
# define TRUE 1
# define FALSE 0
#endif
typedef int bool_t;

typedef void(*fnptr_t)();

#endif
