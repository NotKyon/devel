/*
 * Software Renderer Test -- Slight Revision
 * Written by Aaron J. Miller <nocannedmeat@gmail.com>; 20120309
 *  !!! MAY BE BUGGY; IS DEFINITELY INCOMPLETE !!!
 *
 * Compile like so:
 * $ gcc -O3 -fomit-frame-pointer -o test main.c -lglfw -lGL
 *
 * On Windows, replace "-lGL" with "-lopengl32"
 * On Mac OS X, replace with "-lobjc -framework Cocoa -framework OpenGL"
 *
 * Some platforms might require the "-lm" switch to include the math library.
 *
 * This sample only uses OpenGL to put the pixels on the screen, the pixels are
 * manually created through the software renderer.
 *
 * This is mostly here just for testing and technical curiosity/exploration.
 */

#include <GL/glfw.h>
#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>

typedef struct matrix_s {
	float m11, m21, m31, m41;
	float m12, m22, m32, m42;
	float m13, m23, m33, m43;
	float m14, m24, m34, m44;
} matrix_t;

typedef struct surface_s {
	void *pixels;

	unsigned int bytes_per_pixel;
	unsigned int width, height;
	unsigned int pitch;
} *surface_t;

surface_t g_screen;

#define MATRIXMODE_MODELVIEW 0
#define MATRIXMODE_PROJECTION 1
struct {
	matrix_t matrix[2];
	matrix_t mvp;
	int changed;
} g_mstack[64];
int g_mstackp=0;
int g_mmode=MATRIXMODE_MODELVIEW;

struct {
	union {
		const void *p;
		const float *f;
		const unsigned char *b;
	} ptr;
	unsigned int element_type;
	unsigned int num_elements;
	size_t stride;
} g_attribs[1];
#define ELEMENT_FLOAT 0
#define ELEMENT_BYTE 1
#define ATTRIB_POSITION 0

const char *va(const char *fmt, ...) {
	static char buf[8192];
	va_list args;

	va_start(args, fmt);
	vsnprintf(buf, sizeof(buf)-1, fmt, args);
	buf[sizeof(buf)-1] = 0;
	va_end(args);

	return buf;
}
void error(const char *message) {
	fprintf(stderr, "error: %s\n", message);
	fflush(stderr);

	exit(-1);
}

void *memory(void *p, size_t n) {
	if (!p&&n) {
		if (!(p=malloc(n)))
			error(va("malloc() failed: %s [errno:%d]", strerror(errno), errno));
	} else if(p&&n) {
		if (!(p=realloc(p, n)))
			error(va("realloc() failed: %s [errno:%d]", strerror(errno), errno));
	} else {
		assert(p&&!n);

		free(p);
		p = (void *)0;
	}

	return p;
}

surface_t newsurface(unsigned int width, unsigned int height) {
	surface_t surf;
	size_t n;

	assert(width>0 && width<4096);
	assert(height>0 && height<4096);

	surf = (surface_t)memory((void *)0, sizeof(*surf));

	surf->width = width;
	surf->height = height;
	surf->bytes_per_pixel = 3;
	surf->pitch = surf->width*surf->bytes_per_pixel;

	n = surf->width*surf->height*surf->bytes_per_pixel;
	surf->pixels = memory((void *)0, n);

	memset(surf->pixels, 0, n);

	return surf;
}
void deletesurface(surface_t surf) {
	assert(surf != (surface_t)0);
	assert(surf->pixels != (void *)0);

	memory(surf->pixels, 0);
	memory((void *)surf, 0);
}

unsigned char *getpixelptr(int x, int y) {
	unsigned char *pixels;

	if (x<0||y<0||x>=(int)g_screen->width||y>=(int)g_screen->height)
		return (unsigned char *)0;

	pixels = (unsigned char *)g_screen->pixels;
	y = g_screen->height-y;

	return &pixels[y*g_screen->pitch + x*g_screen->bytes_per_pixel];
}

void(*setpixel)(int, int, unsigned int);
void(*drawtriangle)(int,int, int,int, int,int, unsigned int);

void cls(unsigned int color) {
	unsigned char *pixels, r, g, b;
	unsigned int x, y, index;

	r = ((color&0x000000FF)>>0);
	g = ((color&0x0000FF00)>>8);
	b = ((color&0x00FF0000)>>16);

	pixels = (unsigned char *)g_screen->pixels;
	index = 0;

	for(y=0; y<g_screen->height; y++) {
		for(x=0; x<g_screen->width; x++) {
			pixels[index+0] = r;
			pixels[index+1] = g;
			pixels[index+2] = b;

			index += 3;
		}
	}
}

void setpixel_pure(int x, int y, unsigned int pixel) {
	unsigned char *pixels;

	if (!(pixels = getpixelptr(x, y)))
		return;

	pixels[0] = (pixel&0x000000FF)>>0;
	pixels[1] = (pixel&0x0000FF00)>>8;
	pixels[2] = (pixel&0x00FF0000)>>16;
}

void drawline(int x1, int y1, int x2, int y2, unsigned int color) {
	int deltax,deltay, signx,signy;
	int pointx,pointy, amountx,amounty;
	int x, y;

	deltax = x2-x1;
	deltay = y2-y1;

	signx = 1 | (deltax>>31);
	signy = 1 | (deltay>>31);

	deltax *= signx;
	deltay *= signy;

	amountx = deltay==0?0:((deltax<<16)/deltay)*signx;
	amounty = deltax==0?0:((deltay<<16)/deltax)*signy;

	signx = (1<<16)*signx;
	signy = (1<<16)*signy;

	pointx = x1<<16;
	pointy = y1<<16;

	if (deltax>deltay) {
		for(x=0; x<deltax; x++) {
			setpixel(pointx>>16, pointy>>16, color);
			pointx += signx;
			pointy += amounty;
		}
	} else {
		for(y=0; y<deltay; y++) {
			setpixel(pointx>>16, pointy>>16, color);
			pointx += amountx;
			pointy += signy;
		}
	}
}

void drawtriangle_wire(int x1, int y1, int x2, int y2, int x3, int y3, unsigned int color) {
	drawline(x1, y1, x2, y2, color);
	drawline(x2, y2, x3, y3, color);
	drawline(x3, y3, x1, y1, color);
}
#if 0
#define SWAP(a,b)\
	a^=b, b^=a, a^=b
#define SORTSWAP(a,b)\
	if(y##a > y##b) SWAP(y##a, y##b); if(x##a > x##b) SWAP(x##a, x##b)
#define FP_MULT 65536
#define MAKEFP(x) ((x)*(FP_MULT))
void drawtriangle_flat(int x1, int y1, int x2, int y2, int x3, int y3, unsigned int color) {
	int fp_startDelta;

	SORTSWAP(2, 3);
	SORTSWAP(1, 2);

	fp_startDelta = x2 - x1
}
#endif

void vertexpointer2f(const float *ptr, size_t stride) {
	g_attribs[ATTRIB_POSITION].ptr.f = ptr;
	g_attribs[ATTRIB_POSITION].element_type = ELEMENT_FLOAT;
	g_attribs[ATTRIB_POSITION].num_elements = 2;
	g_attribs[ATTRIB_POSITION].stride = !stride?sizeof(float)*2:stride;
}
void vertexpointer3f(const float *ptr, size_t stride) {
	g_attribs[ATTRIB_POSITION].ptr.f = ptr;
	g_attribs[ATTRIB_POSITION].element_type = ELEMENT_FLOAT;
	g_attribs[ATTRIB_POSITION].num_elements = 3;
	g_attribs[ATTRIB_POSITION].stride = !stride?sizeof(float)*3:stride;
}

static void internalmatrixmult(matrix_t *d, const matrix_t *a, const matrix_t *b) {
	d->m11 = a->m11*b->m11 + a->m12*b->m21 + a->m13*b->m31 + a->m14*b->m41;
	d->m12 = a->m11*b->m12 + a->m12*b->m22 + a->m13*b->m32 + a->m14*b->m42;
	d->m13 = a->m11*b->m13 + a->m12*b->m23 + a->m13*b->m33 + a->m14*b->m43;
	d->m14 = a->m11*b->m14 + a->m12*b->m24 + a->m13*b->m34 + a->m14*b->m44;

	d->m21 = a->m21*b->m11 + a->m22*b->m21 + a->m23*b->m31 + a->m24*b->m41;
	d->m22 = a->m21*b->m12 + a->m22*b->m22 + a->m23*b->m32 + a->m24*b->m42;
	d->m23 = a->m21*b->m13 + a->m22*b->m23 + a->m23*b->m33 + a->m24*b->m43;
	d->m24 = a->m21*b->m14 + a->m22*b->m24 + a->m23*b->m34 + a->m24*b->m44;

	d->m31 = a->m31*b->m11 + a->m32*b->m21 + a->m33*b->m31 + a->m34*b->m41;
	d->m32 = a->m31*b->m12 + a->m32*b->m22 + a->m33*b->m32 + a->m34*b->m42;
	d->m33 = a->m31*b->m13 + a->m32*b->m23 + a->m33*b->m33 + a->m34*b->m43;
	d->m34 = a->m31*b->m14 + a->m32*b->m24 + a->m33*b->m34 + a->m34*b->m44;

	d->m41 = a->m41*b->m11 + a->m42*b->m21 + a->m43*b->m31 + a->m44*b->m41;
	d->m42 = a->m41*b->m12 + a->m42*b->m22 + a->m43*b->m32 + a->m44*b->m42;
	d->m43 = a->m41*b->m13 + a->m42*b->m23 + a->m43*b->m33 + a->m44*b->m43;
	d->m44 = a->m41*b->m14 + a->m42*b->m24 + a->m43*b->m34 + a->m44*b->m44;
}
static void internalmultpersp(matrix_t *d, const matrix_t *a, const matrix_t *b) {
	d->m11 = a->m11*b->m11;
	d->m12 = a->m12*b->m22;
	d->m13 = a->m13*b->m33 + a->m14;
	d->m14 = a->m13*b->m34;

	d->m21 = a->m21*b->m11;
	d->m22 = a->m22*b->m22;
	d->m23 = a->m23*b->m33 + a->m24;
	d->m24 = a->m23*b->m34;

	d->m31 = a->m31*b->m11;
	d->m32 = a->m32*b->m22;
	d->m33 = a->m33*b->m33 + a->m34;
	d->m34 = a->m33*b->m34;

	d->m41 = 0;
	d->m42 = 0;
	d->m43 = 1;
	d->m44 = 0;
}
static void internalmultortho(matrix_t *d, const matrix_t *a, const matrix_t *b) {
	d->m11 = a->m11*b->m11;
	d->m12 = a->m12*b->m22;
	d->m13 = a->m13*b->m33;
	d->m14 = a->m11*b->m14 + a->m12*b->m24 + a->m13*b->m34 + a->m14;

	d->m21 = a->m21*b->m11;
	d->m22 = a->m22*b->m22;
	d->m23 = a->m23*b->m33;
	d->m24 = a->m21*b->m14 + a->m22*b->m24 + a->m23*b->m34 + a->m24;

	d->m31 = a->m31*b->m11;
	d->m32 = a->m32*b->m22;
	d->m33 = a->m33*b->m33;
	d->m34 = a->m31*b->m14 + a->m32*b->m24 + a->m33*b->m34 + a->m34;

	d->m41 = 0;
	d->m42 = 0;
	d->m43 = 0;
	d->m44 = 1;
}

void calcmvp() {
	matrix_t *mvp;
	const matrix_t *mv, *p;

	if (!g_mstack[g_mstackp].changed)
		return;

	mvp = &g_mstack[g_mstackp].mvp;
	mv  = &g_mstack[g_mstackp].matrix[MATRIXMODE_MODELVIEW];
	p   = &g_mstack[g_mstackp].matrix[MATRIXMODE_PROJECTION];

	/*internalmatrixmult(mvp, mv, p);*/

	if (!g_mstack[g_mstackp].matrix[MATRIXMODE_PROJECTION].m44)
		internalmultpersp(mvp, mv, p);
	else
		internalmultortho(mvp, mv, p);

	g_mstack[g_mstackp].changed = 0;
}
void invalidatemvp() {
	g_mstack[g_mstackp].changed = 1;
}

void matrixmode(int mode) {
	g_mmode = mode&1;
}
void pushmatrix() {
	g_mstack[g_mstackp+1] = g_mstack[g_mstackp];
	g_mstackp++;
}
void popmatrix() {
	g_mstackp--;
}
matrix_t *curmatrixptr() {
	return &g_mstack[g_mstackp].matrix[g_mmode];
}

void loadidentity() {
	matrix_t *p;

	p = curmatrixptr();

	p->m11=1; p->m12=0; p->m13=0; p->m14=0;
	p->m21=0; p->m22=1; p->m23=0; p->m24=0;
	p->m31=0; p->m32=0; p->m33=1; p->m34=0;
	p->m41=0; p->m42=0; p->m43=0; p->m44=1;

	invalidatemvp();
}
void loadmatrix(const matrix_t *s) {
	memcpy((void *)curmatrixptr(), (const void *)s, sizeof(matrix_t));
	invalidatemvp();
}
void getmatrix(matrix_t *d) {
	memcpy((void *)d, (const void *)curmatrixptr(), sizeof(matrix_t));
}
void matrixmult(const matrix_t *s) {
	matrix_t *p, d;

	getmatrix(&d);
	p = curmatrixptr();

	internalmatrixmult(p, &d, s);
	invalidatemvp();
}

void matrixtranslate(float x, float y, float z) {
	matrix_t *p;

	p = curmatrixptr();

	p->m14 += p->m11*x + p->m12*y + p->m13*z;
	p->m24 += p->m21*x + p->m22*y + p->m23*z;
	p->m34 += p->m31*x + p->m32*y + p->m33*z;

	invalidatemvp();
}
void matrixrotatex(float angle) {
	matrix_t *p;
	float c, s, t;

	p = curmatrixptr();

	c = cosf(angle/180.0f*3.1415926535f);
	s = sinf(angle/180.0f*3.1415926535f);

	t = p->m12;
	p->m12 = t*c  + p->m13*s;
	p->m13 = t*-s + p->m13*c;

	t = p->m22;
	p->m22 = t*c  + p->m23*s;
	p->m23 = t*-s + p->m23*c;

	t = p->m32;
	p->m32 = t*c  + p->m33*s;
	p->m33 = t*-s + p->m33*c;

	invalidatemvp();
}
void matrixrotatey(float angle) {
	matrix_t *p;
	float c, s, t;

	p = curmatrixptr();

	c = cosf(angle/180.0f*3.1415926535f);
	s = sinf(angle/180.0f*3.1415926535f);

	t = p->m11;
	p->m11 = t*c + p->m13*-s;
	p->m13 = t*s + p->m13*c;

	t = p->m21;
	p->m21 = t*c + p->m23*-s;
	p->m23 = t*s + p->m23*c;

	t = p->m31;
	p->m31 = t*c + p->m33*-s;
	p->m33 = t*s + p->m33*c;

	invalidatemvp();
}
void matrixrotatez(float angle) {
	matrix_t *p;
	float c, s, t;

	p = curmatrixptr();

	c = cosf(angle/180.0f*3.1415926535f);
	s = sinf(angle/180.0f*3.1415926535f);

	t = p->m11;
	p->m11 = t*c  + p->m12*s;
	p->m12 = t*-s + p->m12*c;

	t = p->m21;
	p->m21 = t*c  + p->m22*s;
	p->m22 = t*-s + p->m22*c;

	t = p->m31;
	p->m31 = t*c  + p->m32*s;
	p->m32 = t*-s + p->m32*c;

	invalidatemvp();
}
void matrixscale(float x, float y, float z) {
	matrix_t *p;

	p = curmatrixptr();

	p->m11 = p->m11*x;
	p->m21 = p->m21*x;
	p->m31 = p->m31*x;

	p->m12 = p->m12*y;
	p->m22 = p->m22*y;
	p->m32 = p->m32*y;

	p->m13 = p->m13*z;
	p->m23 = p->m23*z;
	p->m33 = p->m33*z;

	invalidatemvp();
}
void matrixpersp(float fov, float aspect, float zn, float zf) {
	matrix_t *p;
	float x, y, a, b;

	p = curmatrixptr();

	y = 1 / tanf((fov/2)/180.0f*3.1415926535f);
	x = y / aspect;

	a =     zf/(zf - zn);
	b = -zn*zf/(zf - zn);

	p->m11=x; p->m12=0; p->m13=0; p->m14=0;
	p->m21=0; p->m22=y; p->m23=0; p->m24=0;
	p->m31=0; p->m32=0; p->m33=a; p->m34=b;
	p->m41=0; p->m42=0; p->m43=1; p->m44=0;

	invalidatemvp();
}
void matrixortho(float l, float r, float b, float t, float zn, float zf) {
	matrix_t *p;
	float A, B, C, D, E, F;

	p = curmatrixptr();

	A = 2/(r-l);
	B = (l+r)/(l-r);
	C = 2/(t-b);
	D = (t+b)/(b-t);
	E = 1/(zf-zn);
	F = zn/(zn-zf);

	p->m11=A; p->m12=0; p->m13=0; p->m14=B;
	p->m21=0; p->m22=C; p->m23=0; p->m24=D;
	p->m31=0; p->m32=0; p->m33=E; p->m34=F;
	p->m41=0; p->m42=0; p->m43=0; p->m44=1;

	invalidatemvp();
}

void vertextransform(float *x, float *y, float *z) {
	matrix_t *mvp;
	float tx, ty, tz, n;
	float vpx, vpy, vpw, vph;

	vpx = 0.0f;
	vpy = 0.0f;
	vpw = (float)g_screen->width;
	vph = (float)g_screen->height;

	calcmvp();
	mvp = &g_mstack[g_mstackp].mvp;

#if 0
	tx = *x;
	ty = *y;
	tz = *z;

	*x = tx*mvp->m11 + ty*mvp->m12 + tz*mvp->m13 + mvp->m14;
	*y = tx*mvp->m21 + ty*mvp->m22 + tz*mvp->m23 + mvp->m24;
	*z = tx*mvp->m31 + ty*mvp->m32 + tz*mvp->m33 + mvp->m34;
#else
	n = (*x)*mvp->m41 + (*y)*mvp->m42 + (*z)*mvp->m43 + mvp->m44;

	tx = ((*x)*mvp->m11 + (*y)*mvp->m12 + (*z)*mvp->m13 + mvp->m14) / n;
	ty = ((*x)*mvp->m21 + (*y)*mvp->m22 + (*z)*mvp->m23 + mvp->m24) / n;
	tz = ((*x)*mvp->m31 + (*y)*mvp->m32 + (*z)*mvp->m33 + mvp->m34) / n;

	*x = vpx + (1.0f + tx)*vpw/2.0f;
	*y = vpy + (1.0f - ty)*vph/2.0f;
	*z = 0.0f/*minz*/ + tz*(1.0f/*maxz*/ - 0.0f/*minz*/);
#endif
}

void *addptr(void *p, size_t n) {
	union { void *p; size_t n; } v;

	v.p  = p;
	v.n += n;

	return v.p;
}
void drawtriangles(unsigned int n) {
	unsigned int stride;
	const float *p;
	float fx[3],fy[3];
	float x, y, z;
	int i;
	int rx[3], ry[3];

	if (!g_attribs[ATTRIB_POSITION].ptr.p || !n)
		return;

	p = g_attribs[ATTRIB_POSITION].ptr.f;
	stride = g_attribs[ATTRIB_POSITION].stride;

	if (g_attribs[ATTRIB_POSITION].element_type!=ELEMENT_FLOAT)
		return;

			/*static int __hi__=0;*/
	switch(g_attribs[ATTRIB_POSITION].num_elements) {
		case 2:
			while(n) {
				for(i=0; i<3; i++) {
					x = p[0];
					y = p[1];
					z = 0;

					fx[i] = x;
					fy[i] = y;
#if 0
					x = x * (float)g_screen->width;
					y = y * (float)g_screen->height;
#endif
					vertextransform(&x, &y, &z);
					rx[i] = (int)x;
					ry[i] = (int)y;

					p = (float *)addptr((void *)p, stride);
				}
				drawtriangle(rx[0],ry[0], rx[1],ry[1], rx[2],ry[2], 0xFF00);
				/*
				if (!__hi__) {
					__hi__ = 1;
					fprintf(stdout, "i:%f,%f; %f,%f; %f,%f;\n", fx[0],fy[0], fx[1],fy[1], fx[2],fy[2]);
					fprintf(stdout, "o:%d,%d; %d,%d; %d,%d;\n", rx[0],ry[0], rx[1],ry[1], rx[2],ry[2]);
					fflush(stdout);
				}
				*/
				n--;
			}
			break;
		case 3:
			while(n) {
				for(i=0; i<3; i++) {
					x = p[0];
					y = p[1];
					z = p[2];

					vertextransform(&x, &y, &z);
					rx[i] = (int)x;
					ry[i] = (int)y;

					p = (float *)addptr((void *)p, stride);
				}
				drawtriangle(rx[0],ry[0], rx[1],ry[1], rx[2],ry[2], 0xFF00);
				/*if (!__hi__) {
					__hi__ = 1;
					fprintf(stdout, "o:%d,%d; %d,%d; %d,%d;\n", rx[0],ry[0], rx[1],ry[1], rx[2],ry[2]);
					fflush(stdout);
				}*/
				n--;
			}
			break;

		default:
			break;
	}
}

void resetrenderer() {
	int w, h;

	if (!g_screen) {
		glfwGetWindowSize(&w, &h);
		g_screen = newsurface(w, h);
	}

	setpixel = setpixel_pure;
	drawtriangle = drawtriangle_wire;

	matrixmode(MATRIXMODE_PROJECTION);
	loadidentity();

	matrixmode(MATRIXMODE_MODELVIEW);
	loadidentity();

	vertexpointer3f((float *)0, 0);
}

void gl_window_size(float *w, float *h) {
	int x, y;

	glfwGetWindowSize(&x, &y);
	*w = (float)x;
	*h = (float)y;
}

void flip() {
	struct { float x, y; } w, s, p, c;

	gl_window_size(&w.x, &w.y);
	s.x = (float)g_screen->width;
	s.y = (float)g_screen->height;

	p.x = (w.x-s.x)/2;
	p.y = (w.y-s.y)/2;

	c.x = (p.x/w.x)*2 - 1;
	c.y = (p.y/w.y)*2 - 1;

	glRasterPos2f(c.x, c.y);
	glDrawPixels
	(
		g_screen->width,	/* width */
		g_screen->height,	/* height */
		GL_RGB,				/* format */
		GL_UNSIGNED_BYTE,	/* type */
		g_screen->pixels	/* data */
	);

	glfwPollEvents();
	glfwSwapBuffers();
}

void resize_func(int w, int h) {
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
const char *gl_string(GLenum pname) {
	return (const char *)glGetString(pname);
}
void gl_info() {
	FILE *f;

	fprintf(stdout, "gl version : %s\n", gl_string(GL_VERSION));
	fprintf(stdout, "gl vendor  : %s\n", gl_string(GL_VENDOR));
	fprintf(stdout, "gl renderer: %s\n", gl_string(GL_RENDERER));
	fprintf(stdout, "gl ext     : %s\n", gl_string(GL_EXTENSIONS));
	fflush(stdout);

	if ((f=fopen("debug.log", "a+")) != (FILE *)0) {
		const char *src;
		char buf[512], *dst;

		fprintf(f, "gl version : %s\n", gl_string(GL_VERSION));
		fprintf(f, "gl vendor  : %s\n", gl_string(GL_VENDOR));
		fprintf(f, "gl renderer: %s\n", gl_string(GL_RENDERER));
		fprintf(f, "\ngl extensions {\n");

		for(src=gl_string(GL_EXTENSIONS),dst=&buf[0]; *src; src++) {
			if (*src==' ') {
				*dst = 0;

				fprintf(f, "\t%s\n", buf);
				dst = &buf[0];

				continue;
			}

			*dst++ = *src;
		}

		fprintf(f, "}\n\n");
		fclose(f);
	}
}
int main(int argc, char **argv) {
	static float triangle_vertices[3*3] = {
		-0.5f, -0.5f, 1,
		 0.0f, +0.5f, 1,
		+0.5f, -0.5f, 1
	};
	float time_p, time_c, dt, time_e;
	int frames;
	int w, h;

	if (argc||argv) {/*nothing*/}

	if (!glfwInit())
		error(va("failed to initialize GLFW"));

	atexit(glfwTerminate);

	w = 800;
	h = 480;

	glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, 1);
	if (!glfwOpenWindow(w, h, 8, 8, 8, 0, 0, 0, GLFW_WINDOW))
		error(va("failed to set video mode"));

	glfwSetWindowTitle("Aaron's Renderer Test");
	glfwSetWindowSizeCallback(resize_func);

	gl_info();

	resetrenderer();

	{
		int vp[4];

		glGetIntegerv(GL_VIEWPORT, &vp[0]);
		printf("%i,%i,%i,%i\n", vp[0],vp[1],vp[2],vp[3]);
		fflush(stdout);
	}

	/*matrixtranslate((float)(w/2), (float)(h/2), 15);*/
	matrixtranslate(0, 0, 15);
	/*matrixscale(150, 150, 150);*/

	time_c = (float)glfwGetTime();
	time_e = 0.0f;
	frames = 0;
	while(glfwGetWindowParam(GLFW_OPENED)) {
		time_p = time_c;
		time_c = (float)glfwGetTime();
		dt = time_c-time_p;
		time_e += dt;

		if (time_e >= 1.0) {
			printf("%i\n", frames);
			fflush(stdout);
			frames = 0;
			time_e -= 1.0;
		}

		cls(0x00222222);

		/*
		drawline(50, 50, 120, 140, 0x00FF0044);
		drawtriangle(200,300, 300,50, 400,300, 0x00FFFFFF);
		*/

		matrixmode(MATRIXMODE_PROJECTION);
		/*matrixortho(-1,1,1,-1, 1,1000);*/
		matrixpersp(60.0f, (float)w/(float)h, 1, 1000);

		matrixmode(MATRIXMODE_MODELVIEW);
		matrixrotatez(1.25f*35*dt);
		matrixrotatex(0.75f*35*dt);
		matrixrotatey(1.00f*35*dt);

		vertexpointer3f(triangle_vertices, 0);
		drawtriangles(1);

		flip();
		frames++;
	}

	return 0;
}
