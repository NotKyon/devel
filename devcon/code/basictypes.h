#ifndef BASICTYPES_H
#define BASICTYPES_H

#include <stddef.h>

#undef BIT
#define BIT(x) (1<<(x))

#endif
