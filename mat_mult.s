	.file	"mat_mult.c"
	.intel_syntax noprefix
	.text
	.p2align 4,,15
	.globl	mat_mult
	.type	mat_mult, @function
mat_mult:
.LFB0:
	.cfi_startproc
	mov	edx, DWORD PTR [esp+8]
	mov	eax, DWORD PTR [esp+12]
	mov	ecx, DWORD PTR [esp+4]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+16]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+32]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+48]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+16]
	fld	DWORD PTR [edx+16]
	fmul	DWORD PTR [eax+20]
	faddp	st(1), st
	fld	DWORD PTR [edx+32]
	fmul	DWORD PTR [eax+24]
	faddp	st(1), st
	fld	DWORD PTR [edx+48]
	fmul	DWORD PTR [eax+28]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+16]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+32]
	fld	DWORD PTR [edx+16]
	fmul	DWORD PTR [eax+36]
	faddp	st(1), st
	fld	DWORD PTR [edx+32]
	fmul	DWORD PTR [eax+40]
	faddp	st(1), st
	fld	DWORD PTR [edx+48]
	fmul	DWORD PTR [eax+44]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+32]
	fld	DWORD PTR [edx]
	fmul	DWORD PTR [eax+48]
	fld	DWORD PTR [edx+16]
	fmul	DWORD PTR [eax+52]
	faddp	st(1), st
	fld	DWORD PTR [edx+32]
	fmul	DWORD PTR [eax+56]
	faddp	st(1), st
	fld	DWORD PTR [edx+48]
	fmul	DWORD PTR [eax+60]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+48]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+20]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+36]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+52]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+4]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+16]
	fld	DWORD PTR [edx+20]
	fmul	DWORD PTR [eax+20]
	faddp	st(1), st
	fld	DWORD PTR [edx+36]
	fmul	DWORD PTR [eax+24]
	faddp	st(1), st
	fld	DWORD PTR [edx+52]
	fmul	DWORD PTR [eax+28]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+20]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+32]
	fld	DWORD PTR [edx+20]
	fmul	DWORD PTR [eax+36]
	faddp	st(1), st
	fld	DWORD PTR [edx+36]
	fmul	DWORD PTR [eax+40]
	faddp	st(1), st
	fld	DWORD PTR [edx+52]
	fmul	DWORD PTR [eax+44]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+36]
	fld	DWORD PTR [edx+4]
	fmul	DWORD PTR [eax+48]
	fld	DWORD PTR [edx+20]
	fmul	DWORD PTR [eax+52]
	faddp	st(1), st
	fld	DWORD PTR [edx+36]
	fmul	DWORD PTR [eax+56]
	faddp	st(1), st
	fld	DWORD PTR [edx+52]
	fmul	DWORD PTR [eax+60]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+52]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+24]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+40]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+56]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+8]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+16]
	fld	DWORD PTR [edx+24]
	fmul	DWORD PTR [eax+20]
	faddp	st(1), st
	fld	DWORD PTR [edx+40]
	fmul	DWORD PTR [eax+24]
	faddp	st(1), st
	fld	DWORD PTR [edx+56]
	fmul	DWORD PTR [eax+28]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+24]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+32]
	fld	DWORD PTR [edx+24]
	fmul	DWORD PTR [eax+36]
	faddp	st(1), st
	fld	DWORD PTR [edx+40]
	fmul	DWORD PTR [eax+40]
	faddp	st(1), st
	fld	DWORD PTR [edx+56]
	fmul	DWORD PTR [eax+44]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+40]
	fld	DWORD PTR [edx+8]
	fmul	DWORD PTR [eax+48]
	fld	DWORD PTR [edx+24]
	fmul	DWORD PTR [eax+52]
	faddp	st(1), st
	fld	DWORD PTR [edx+40]
	fmul	DWORD PTR [eax+56]
	faddp	st(1), st
	fld	DWORD PTR [edx+56]
	fmul	DWORD PTR [eax+60]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+56]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax]
	fld	DWORD PTR [edx+28]
	fmul	DWORD PTR [eax+4]
	faddp	st(1), st
	fld	DWORD PTR [edx+44]
	fmul	DWORD PTR [eax+8]
	faddp	st(1), st
	fld	DWORD PTR [edx+60]
	fmul	DWORD PTR [eax+12]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+12]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax+16]
	fld	DWORD PTR [edx+28]
	fmul	DWORD PTR [eax+20]
	faddp	st(1), st
	fld	DWORD PTR [edx+44]
	fmul	DWORD PTR [eax+24]
	faddp	st(1), st
	fld	DWORD PTR [edx+60]
	fmul	DWORD PTR [eax+28]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+28]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax+32]
	fld	DWORD PTR [edx+28]
	fmul	DWORD PTR [eax+36]
	faddp	st(1), st
	fld	DWORD PTR [edx+44]
	fmul	DWORD PTR [eax+40]
	faddp	st(1), st
	fld	DWORD PTR [edx+60]
	fmul	DWORD PTR [eax+44]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+44]
	fld	DWORD PTR [edx+12]
	fmul	DWORD PTR [eax+48]
	fld	DWORD PTR [edx+28]
	fmul	DWORD PTR [eax+52]
	faddp	st(1), st
	fld	DWORD PTR [edx+44]
	fmul	DWORD PTR [eax+56]
	faddp	st(1), st
	fld	DWORD PTR [edx+60]
	fmul	DWORD PTR [eax+60]
	faddp	st(1), st
	fstp	DWORD PTR [ecx+60]
	ret
	.cfi_endproc
.LFE0:
	.size	mat_mult, .-mat_mult
	.ident	"GCC: (Ubuntu/Linaro 4.6.3-1ubuntu2) 4.6.3"
	.section	.note.GNU-stack,"",@progbits
