﻿#ifdef _MSC_VER
# define VC_VERSION _MSC_VER
#else
# define VC_VERSION 0
#endif

#ifndef HAS_STDINT
# if !VC_VERSION || VC_VERSION >= 1600
#  define HAS_STDINT 1
# else
#  define HAS_STDINT 0
# endif
#endif

#if HAS_STDINT
# include <stdint.h>
typedef int8_t   Int8;
typedef int16_t  Int16;
typedef int32_t  Int32;
typedef int64_t  Int64;
typedef uint8_t  UInt8;
typedef uint16_t UInt16;
typedef uint32_t UInt32;
typedef uint64_t UInt64;
#elif VC_VERSION != 0
typedef   signed __int8  Int8;
typedef   signed __int16 Int16;
typedef   signed __int32 Int32;
typedef   signed __int64 Int64;
typedef unsigned __int8  UInt8;
typedef unsigned __int16 UInt16;
typedef unsigned __int32 UInt32;
typedef unsigned __int64 UInt64;
#else
typedef   signed char          Int8;
typedef   signed short         Int16;
typedef   signed int           Int32;
typedef   signed long long int Int64;
typedef unsigned char          UInt8;
typedef unsigned short         UInt16;
typedef unsigned int           UInt32;
typedef unsigned long long int UInt64;
#endif

#include <string.h>
#include <stdlib.h> // malloc

#ifndef CONSTEXPR14
# if !VC_VERSION || VC_VERSION >= 1800
#  define CONSTEXPR14 constexpr
# else
#  define CONSTEXPR14 inline
# endif
#endif
#ifndef CONSTEXPR11
# if !VC_VERSION || VC_VERSION >= 1700
#  define CONSTEXPR11 constexpr
# else
#  define CONSTEXPR11 inline
# endif
#endif

#ifndef HAS_CXX11_MOVE
# if !VC_VERSION || VC_VERSION >= 1600
#  define HAS_CXX11_MOVE 1
# else
#  define HAS_CXX11_MOVE 0
# endif
#endif

#ifndef NOEXCEPT11
# if !VC_VERSION || VC_VERSION >= 1700
#  define NOEXCEPT11 noexcept
# else
#  define NOEXCEPT11 throw()
# endif
#endif

#ifndef NOEXCEPT11_FALSE
# if !VC_VERSION || VC_VERSION >= 1800
#  define NOEXCEPT11_FALSE noexcept(false)
# else
#  define NOEXCEPT11_FALSE
# endif
#endif

#ifndef FASTPATH
# if defined( __GNUC__ ) || defined( __clang__ )
#  define FASTPATH(Expr_) __builtin_expect(!!(Expr_), 1)
# else
#  define FASTPATH(Expr_) (Expr_)
# endif
#endif
#ifndef SLOWPATH
# if defined( __GNUC__ ) || defined( __clang__ )
#  define SLOWPATH(Expr_) __builtin_expect(!!(Expr_), 0)
# else
#  define SLOWPATH(Expr_) (Expr_)
# endif
#endif

template<unsigned tBits>
struct SizedInteger
{
};
template<>
struct SizedInteger<8>
{
	typedef Int8  Int;
	typedef UInt8 UInt;
};
template<>
struct SizedInteger<16>
{
	typedef Int16  Int;
	typedef UInt16 UInt;
};
template<>
struct SizedInteger<32>
{
	typedef Int32  Int;
	typedef UInt32 UInt;
};
template<>
struct SizedInteger<64>
{
	typedef Int64  Int;
	typedef UInt64 UInt;
};

typedef SizedInteger<sizeof(void*)*8> PointerSizedInteger;

typedef PointerSizedInteger::Int  IntPtr;
typedef PointerSizedInteger::UInt UIntPtr;

typedef IntPtr  DiffType;
typedef UIntPtr SizeType;

typedef Int64  Int;
typedef UInt64 UInt;

typedef UInt32 UTF32Char;

namespace utility
{
	
#undef min
#undef max

	template< typename T >
	CONSTEXPR14 T min( T a, T b ) NOEXCEPT11
	{
		return a < b ? a : b;
	}
	template< typename T >
	CONSTEXPR14 T max( T a, T b ) NOEXCEPT11
	{
		return a < b ? b : a;
	}
	
}

class StringBase;

namespace policy { class DefaultStringAllocator; }
namespace policy { class DefaultStringGrowth; }

template< typename Allocator = policy::DefaultStringAllocator,
typename Growth = policy::DefaultStringGrowth >
class TString;

typedef TString<> String;

struct DomainCode
{
	UInt32 domain;
	Int32  code;
	
	DomainCode()
	: domain( 0 )
	, code( 0 )
	{
	}
	DomainCode( UInt32 domain, Int32 code )
	: domain( domain )
	, code( code )
	{
	}
};

class Error
{
public:
	UInt32      domain;
	Int32       code;
	StringBase *description;
	
	Error()
	NOEXCEPT11
	: domain( 0 )
	, code( 0 )
	, description( ( StringBase * )0 )
	{
	}
	Error( UInt32 domain, Int32 code,
	StringBase *description = ( StringBase * )0 )
	NOEXCEPT11
	: domain( domain )
	, code( code )
	, description( description )
	{
	}
	
	bool isFailure() const NOEXCEPT11 { return code < 0; }
	bool isSuccess() const NOEXCEPT11 { return code >= 0; }
	
	static inline bool generate( Error *outError, UInt32 domain, Int32 code,
	StringBase *description = ( StringBase * )0 ) NOEXCEPT11_FALSE
	{
		if( !outError ) {
			if( code < 0 ) {
				throw Error( domain, code, description );
			}
			
			return true;
		}
		
		outError->domain      = domain;
		outError->code        = code;
		outError->description = description;
		
		return code >= 0;
	}
	static inline bool generate( Error *outError, const DomainCode &info,
	StringBase *description = ( StringBase * )0 ) NOEXCEPT11_FALSE
	{
		return generate( outError, info.domain, info.code, description );
	}
};

#define AXLIB_DOMAIN__BEGIN      0xD897C000
#define AXLIB_DOMAIN__END        0xD89804A7
#define AXLIB_DOMAIN__OFFSET_STR 0x3446

#define AXLIB__MAKE_DOMAIN(Offset_) \
	( AXLIB_DOMAIN__BEGIN + ( Offset_ ) )

#define AXSTRING_DOMAIN          AXLIB__MAKE_DOMAIN(AXLIB_DOMAIN__OFFSET_STR)

struct StringCode: public DomainCode
{
	StringCode( Int32 code )
	: DomainCode( AXSTRING_DOMAIN, code )
	{
	}
};

enum EStringResult
{
	/// Operation succeeded
	kStr_Success = 0,
	/// Operation succeeded, but the UTF-8 being decoded was incomplete
	kStr_TruncatedUTF8 = 1,
	/// Operation succeeded, but happened to decode an invalid codepoint
	kStr_InvalidCodepoint = 2,
	
	/// Operation failed for an unknown reason
	kStr_Error = -1,
	/// Operation failed because we were unable to allocate memory
	kStr_Error_OutOfMemory = -2,
	/// Error occurred while decoding UTF-8 stream
	kStr_Error_DecodeOverflow = -3,
};

class StringBase
{
	template< typename Allocator, typename GrowthPolicy >
	friend class TString;
	
	struct BaseObject
	{
		UInt32 referenceCount;
		UInt32 capacity;

		char *data() NOEXCEPT11
		{
			return reinterpret_cast< char * >( this + 1 );
		}
		const char *data() const NOEXCEPT11
		{
			return reinterpret_cast< const char * >( this + 1 );
		}

		static BaseObject *fromData( char *p ) NOEXCEPT11
		{
			return reinterpret_cast< BaseObject * >( p ) - 1;
		}
		static const BaseObject *fromData( const char *p ) NOEXCEPT11
		{
			return reinterpret_cast< const BaseObject * >( p ) - 1;
		}
	};

	char * m_data;
	UInt32 m_offset;
	UInt32 m_length;
	
	BaseObject *baseObject() NOEXCEPT11
	{
		return BaseObject::fromData( m_data );
	}
	const BaseObject *baseObject() const NOEXCEPT11
	{
		return BaseObject::fromData( m_data );
	}

public:
	StringBase() NOEXCEPT11
	: m_data( ( char * )0 )
	, m_offset( 0 )
	, m_length( 0 )
	{
	}
	StringBase( const StringBase &other ) NOEXCEPT11
	: m_data( other.m_data )
	, m_offset( other.m_offset )
	, m_length( other.m_length )
	{
		if( m_data != ( char * )0 ) {
			++baseObject()->referenceCount;
		}
	}
#if HAS_CXX11_MOVE
	StringBase( StringBase &&other ) NOEXCEPT11
	: m_data( other.m_data )
	, m_offset( other.m_offset )
	, m_length( other.m_length )
	{
		other.m_data = nullptr;
		other.m_offset = 0;
		other.m_length = 0;
	}
#endif
	
	const char *get() const NOEXCEPT11 { return m_data + m_offset; }
	const char *getEnd() const NOEXCEPT11 { return get() + m_length; }
	
	bool isEmpty() const NOEXCEPT11 { return m_length == 0; }
	bool isUsed() const NOEXCEPT11 { return m_length != 0; }

	SizeType len() const NOEXCEPT11 { return SizeType( m_length ); }
	int lenInt() const NOEXCEPT11 { return int( DiffType( m_length ) ); }
	
	SizeType lengthInBytes() const NOEXCEPT11 { return m_length; }
	SizeType lengthInCodepoints() const NOEXCEPT11 {
		// Starting position in string
		const char *const s = m_data + m_offset;
		// Ending position in string
		const char *const e = s + m_length;
		
		// Check for unlikely overflow
		if( SLOWPATH( e + 3 < e ) ) {
			return 0;
		}

		// Number of characters found so far
		SizeType n = 0;
		// Current pointer
		const char *p = s;
		while( p < e ) {
			// 4-byte encoding
			if( ( *p & 0xF0 ) == 0xF0 ) {
				p += 4;
			// 3-byte encoding
			} else if( ( *p & 0xE0 ) == 0xE0 ) {
				p += 3;
			// 2-byte encoding
			} else if( ( *p & 0xC0 ) == 0xC0 ) {
				p += 2;
			// 1-byte (or invalid) encoding
			} else {
				++p;
			}
			
			// Found a codepoint (even if invalid)
			++n;
		}
		
		// Done
		return n;
	}
	
	SizeType clampByteOffset( SizeType offsetInBytes ) const NOEXCEPT11
	{
		return offsetInBytes < m_length ? offsetInBytes : m_length;
	}
	
	bool decodeUTF8Codepoint( UTF32Char &outChar, SizeType offsetInBytes = 0,
	SizeType *outOffsetInBytes = ( SizeType * )0,
	Error *outError = ( Error * )0 ) const
	{
		// Replacement character to write
		static const UTF32Char kReplaceChar = 0xFFFD;
		// Dummy address if `outOffsetInBytes` is `nullptr`
		SizeType dummyAddr;

		// Keep the offset in range
		offsetInBytes = clampByteOffset( offsetInBytes );
		// Adjust the out-pointer so that it's never null (use `dummyAddr`)
		outOffsetInBytes =
			outOffsetInBytes != ( SizeType * )0 ?
				outOffsetInBytes :
				&dummyAddr;
		
		// Base pointer of this string
		const char *const q = m_data + m_offset;

		// Check for overflow possibility
		if( SLOWPATH( q + offsetInBytes < q ) ) {
			return Error::generate( outError,
				StringCode( kStr_Error_DecodeOverflow ) );
		}
		
		// Get to the start of the subsection of the string we're interested in
		const char *const p = q + offsetInBytes;
		// End of the string
		const char *const e = p + m_length;
		
		// If we're at the end of the string, write the NUL character
		if( p == e ) {
			outChar = 0;
			*outOffsetInBytes = offsetInBytes;

			return Error::generate( outError, StringCode( kStr_Success ) );
		}
		
		// 1-byte decode fast-path
		if( FASTPATH( ( p[0] & 0x80 ) == 0x00 ) ) {
			outChar = UTF32Char( p[0] );
			*outOffsetInBytes = offsetInBytes + 1;

			return Error::generate( outError, StringCode( kStr_Success ) );
		}
		
		// 4-byte decode
		if( ( p[0] & 0xF0 ) == 0xF0 ) {
			if( p + 4 > e ) {
				outChar = kReplaceChar;
				*outOffsetInBytes = m_length;
				
				return Error::generate( outError,
					StringCode( kStr_TruncatedUTF8 ) );
			}
			
			outChar =
				(UTF32Char(p[0]&0x07)<<18) |
				(UTF32Char(p[1]&0x3F)<<12) |
				(UTF32Char(p[2]&0x3F)<< 6) |
				(UTF32Char(p[3]&0x3F)<< 0);
			*outOffsetInBytes = offsetInBytes + 4;

			return Error::generate( outError, StringCode( kStr_Success ) );
		}
		
		// 3-byte decode
		if( ( p[0] & 0xE0 ) == 0xE0 ) {
			if( p + 3 > e ) {
				outChar = kReplaceChar;
				*outOffsetInBytes = m_length;
				
				return Error::generate( outError,
					StringCode( kStr_TruncatedUTF8 ) );
			}

			outChar =
				(UTF32Char(p[0]&0x0F)<<12) |
				(UTF32Char(p[1]&0x3F)<< 6) |
				(UTF32Char(p[2]&0x3F)<< 0);
			*outOffsetInBytes = offsetInBytes + 3;

			return Error::generate( outError, StringCode( kStr_Success ) );
		}

		// 2-byte decode
		if( ( p[0] & 0xC0 ) == 0xC0 ) {
			if( p + 2 > e ) {
				outChar = kReplaceChar;
				*outOffsetInBytes = m_length;

				return Error::generate( outError,
					StringCode( kStr_TruncatedUTF8 ) );
			}

			outChar =
				(UTF32Char(p[0]&0x1F)<<6) |
				(UTF32Char(p[1]&0x3F)<<0);
			*outOffsetInBytes = offsetInBytes + 2;

			return Error::generate( outError, StringCode( kStr_Success ) );
		}
		
		// Invalid character
		outChar = kReplaceChar;
		*outOffsetInBytes = offsetInBytes + 1;
		
		return Error::generate( outError, StringCode( kStr_InvalidCodepoint ) );
	}
	
	SizeType convertCodepointOffsetToByteIndex( DiffType offsetInCodepoints )
	const NOEXCEPT11
	{
		const char *const s = get();
		const char *const e = getEnd();
		const char *p;

		// Search backward in the string until we've reached our offset
		if( offsetInCodepoints < 0 ) {
			// Begin at the end of the string
			p = e;
			
			// Search until we've either found the codepoint of interest, or
			// we've hit the end of the string (this serves as an incidental
			// clamp)
			while( p > s && offsetInCodepoints < -1 ) {
				// Decrement first, because `p` is initially at `e` which is
				// "one past the end" of the string
				--p;
				
				// Fast path for ASCII strings
				if( FASTPATH( ( *p & 0x80 ) == 0x00 ) ) {
					// This counts as a codepoint
					++offsetInCodepoints;
					continue;
				}
				
				// Simple check for a lead byte of a UTF-8 codepoint
				if( ( *p & 0xF0 ) == 0xF0 || ( *p & 0xE0 ) == 0xE0 ||
				( *p & 0xC0 ) == 0xC0 ) {
					// The lead byte designates the start of the codepoint, so
					// it's safe to increment the offset now
					++offsetInCodepoints;
					continue;
				}
			}

			// The byte index is the difference between where the point is at
			// now and where the base of the string is
			return SizeType( DiffType( p - s ) );
		}

		// Begin the search from the start of the string
		p = s;

		// Search forward in the string until we've reached our offset
		while( p < e && offsetInCodepoints > 0 ) {
			// Quick increment if we're just ASCII
			if( FASTPATH( ( *p & 0x80 ) == 0x00 ) ) {
				--offsetInCodepoints;
				++p;
				continue;
				
				// TODO: Perhaps this could be sped up by doing a test like
				//       `( *(UInt64*)p & 0x8080808080808080 ) == 0`, or
				//       `( *(UInt32*)p & 0x80808080 ) == 0` to skip several
				//       characters at once.
			}
			
			// NOTE: In the following it's okay to skip checking for truncated
			//       codepoint encodings. (e.g., only two bytes left in the
			//       string but the current byte indicates there should be
			//       another three bytes.) It's okay to skip that check because
			//       it's performed on the next iteration of the while loop, and
			//       this does not attempt to dereference from outside of the
			//       string's range.
			//
			//       The only way this might fail would be if the string were
			//       located at the end of the address space, which would
			//       cause it to be wrapped if its last codepoint were invalid.
			//
			//       To my knowledge that's impossible, or something you would
			//       have to go out of your way to make happen, on the only
			//       platforms I care about supporting.
			
			// 4-byte encoding
			if( ( *p & 0xF0 ) == 0xF0 ) {
				--offsetInCodepoints;
				p += 4;
				continue;
			}
			
			// 3-byte encoding
			if( ( *p & 0xE0 ) == 0xE0 ) {
				--offsetInCodepoints;
				p += 3;
				continue;
			}
			
			// 2-byte encoding
			if( ( *p & 0xC0 ) == 0xC0 ) {
				--offsetInCodepoints;
				p += 2;
				continue;
			}
			
			// Invalid encoding (we already managed ASCII earlier)
			//
			// Each byte of an invalid encoding counts as a codepoint
			--offsetInCodepoints;
			++p;
		}
		
		// Account for the possibility of truncated encodings
		if( SLOWPATH( p > e ) ) {
			p = e;
		}

		// The byte index is the difference between the current pointer and the
		// start pointer.
		return SizeType( DiffType( p - s ) );
	}

	bool cmp( const char *s, const char *e ) const NOEXCEPT11
	{
		const UInt32 length = m_length;
		const UInt32 otherLength = UInt32( SizeType( DiffType( e - s ) ) );
		
		if( length != otherLength ) {
			return false;
		}
		
		if( !length ) {
			return true;
		}
		
		return strncmp( m_data + m_offset, s, length ) == 0;
	}
	bool cmp( const char *cstring ) const NOEXCEPT11
	{
		return cmp( cstring, strchr( cstring, '\0' ) );
	}
	bool cmp( const StringBase &other ) const NOEXCEPT11
	{
		return cmp( other.get(), other.getEnd() );
	}
	bool caseCmp( const char *s, const char *e ) const NOEXCEPT11
	{
		const UInt32 length = m_length;
		const UInt32 otherLength = UInt32( SizeType( DiffType( e - s ) ) );
		
		if( length != otherLength ) {
			return false;
		}
		
		if( !length ) {
			return true;
		}
		
#if defined( _WIN32 ) && !defined( __CYGWIN__ )
		return _strnicmp( m_data + m_offset, s, length ) == 0;
#else
		return strncasecmp( m_data + m_offset, s, length ) == 0;
#endif
	}
	bool caseCmp( const char *cstring ) const NOEXCEPT11
	{
		return caseCmp( cstring, strchr( cstring, '\0' ) );
	}
	bool caseCmp( const StringBase &other ) const NOEXCEPT11
	{
		return caseCmp( other.get(), other.getEnd() );
	}

	int sortCmp( const char *s, const char *e ) const NOEXCEPT11
	{
		const UInt32 otherLength = UInt32( SizeType( DiffType( e - s ) ) );
		const UInt32 length = utility::min( m_length, otherLength );
		return strncmp( m_data + m_offset, s, length );
	}
	int sortCmp( const char *cstring ) const NOEXCEPT11
	{
		return sortCmp( cstring, strchr( cstring, '\0' ) );
	}
	int sortCmp( const StringBase &other ) const NOEXCEPT11
	{
		return sortCmp( other.get(), other.getEnd() );
	}
	int sortCaseCmp( const char *s, const char *e ) const NOEXCEPT11
	{
		const UInt32 otherLength = UInt32( SizeType( DiffType( e - s ) ) );
		const UInt32 length = utility::min( m_length, otherLength );
#if defined( _WIN32 ) && !defined( __CYGWIN__ )
		return _strnicmp( m_data + m_offset, s, length );
#else
		return strncasecmp( m_data + m_offset, s, length );
#endif
	}
	int sortCaseCmp( const char *cstring ) const NOEXCEPT11
	{
		return sortCaseCmp( cstring, strchr( cstring, '\0' ) );
	}
	int sortCaseCmp( const StringBase &other ) const NOEXCEPT11
	{
		return sortCaseCmp( other.get(), other.getEnd() );
	}

	bool operator==( const StringBase &other ) const NOEXCEPT11
	{
		return cmp( other );
	}
	bool operator==( const char *cstring ) const NOEXCEPT11
	{
		return cmp( cstring );
	}

	bool operator<( const StringBase &other ) const NOEXCEPT11
	{
		return sortCmp( other ) < 0;
	}
	bool operator<( const char *cstring ) const NOEXCEPT11
	{
		return sortCmp( cstring ) < 0;
	}

	bool operator!=( const StringBase &other ) const NOEXCEPT11
	{
		return !cmp( other );
	}
	bool operator!=( const char *cstring ) const NOEXCEPT11
	{
		return !cmp( cstring );
	}

	bool operator<=( const StringBase &other ) const NOEXCEPT11
	{
		return sortCmp( other ) <= 0;
	}
	bool operator<=( const char *cstring ) const NOEXCEPT11
	{
		return sortCmp( cstring ) <= 0;
	}
	bool operator>=( const StringBase &other ) const NOEXCEPT11
	{
		return sortCmp( other ) >= 0;
	}
	bool operator>=( const char *cstring ) const NOEXCEPT11
	{
		return sortCmp( cstring ) >= 0;
	}
	bool operator>( const StringBase &other ) const NOEXCEPT11
	{
		return sortCmp( other ) > 0;
	}
	bool operator>( const char *cstring ) const NOEXCEPT11
	{
		return sortCmp( cstring ) > 0;
	}
};

namespace policy
{
	
	class DefaultStringAllocator
	{
	public:
		void *allocate( SizeType sizeInBytes, SizeType &outCapacityInBytes )
		NOEXCEPT11
		{
			void *const p =
				sizeInBytes != 0 ? malloc( sizeInBytes ) : ( void * )0;
			if( !p ) {
				outCapacityInBytes = 0;
				return ( void * )0;
			}

			outCapacityInBytes = sizeInBytes;
			return p;
		}
		void deallocate( void *p, SizeType capacityInBytes ) NOEXCEPT11
		{
			((void)capacityInBytes);
			free( p );
		}
		bool reallocate( void *&p, SizeType desiredSizeInBytes,
		SizeType &capacityInBytes ) NOEXCEPT11
		{
			if( !p ) {
				return
					( p = allocate( desiredSizeInBytes, capacityInBytes ) )
					!= ( void * )0;
			}

			if( !desiredSizeInBytes ) {
				deallocate( p, capacityInBytes );
				p = ( void * )0;
				return true;
			}

			void *const q = realloc( p, desiredSizeInBytes );
			if( !q ) {
				return false;
			}
			
			capacityInBytes = desiredSizeInBytes;
			p = q;
			
			return true;
		}
	};
	
	struct ExponentialStringGrowth
	{
		static CONSTEXPR11
		SizeType calculateGrowth
		(
			SizeType currentCapacityInBytes,
			SizeType desiredSizeInBytes
		) NOEXCEPT11
		{
			return
				currentCapacityInBytes >= desiredSizeInBytes ?
					currentCapacityInBytes :
				desiredSizeInBytes*desiredSizeInBytes >= desiredSizeInBytes ?
					desiredSizeInBytes*desiredSizeInBytes :
				desiredSizeInBytes*2 >= desiredSizeInBytes*2 ?
					desiredSizeInBytes*2 :
				desiredSizeInBytes + 32 < desiredSizeInBytes ?
					desiredSizeInBytes + 32 :
				desiredSizeInBytes;
		}
	};
	template< SizeType tGranularity >
	struct AlignedStringGrowth
	{
		static CONSTEXPR11
		SizeType calculateGrowth
		(
			SizeType currentCapacityInBytes,
			SizeType desiredSizeInBytes
		) NOEXCEPT11
		{
			return
				currentCapacityInBytes >= desiredSizeInBytes ?
					currentCapacityInBytes :
				(
					desiredSizeInBytes +
					(
						desiredSizeInBytes%tGranularity == 0 ?
							0 :
							tGranularity - desiredSizeInBytes%tGranularity
					)
				);
		}
	};
	
	struct DefaultStringGrowth: public ExponentialStringGrowth
	{
		using ExponentialStringGrowth::calculateGrowth;
	};
	
}

namespace detail
{
	
	template< typename T >
	struct HasRealloc
	{
		// ... TODO ...
		static const bool value = false;
	};
	
	template< typename Allocator, bool tHasRealloc = HasRealloc< Allocator >::value >
	class ReallocAdapter: public Allocator
	{
	public:
		using Allocator::allocate;
		using Allocator::deallocate;
		
		bool reallocate( void *&p, SizeType desiredSizeInBytes,
		SizeType &capacityInBytes ) NOEXCEPT11
		{
			SizeType newCapacityInBytes = 0;
			void *q = ( void * )0;
			if( desiredSizeInBytes > 0 ) {
				q = allocate( desiredSizeInBytes, newCapacityInBytes );
				if( !q ) {
					return false;
				}
			}
			
			if( p != ( void * )0 ) {
				const SizeType copySizeInBytes =
					utility::min( desiredSizeInBytes, capacityInBytes );
				memcpy( q, p, copySizeInBytes );
				deallocate( p, capacityInBytes );
			}

			p = q;
			capacityInBytes = newCapacityInBytes;
			
			return true;
		}
	};
	template< typename Allocator >
	class ReallocAdapter< Allocator, true >: public Allocator
	{
	public:
		using Allocator::allocate;
		using Allocator::deallocate;
		using Allocator::reallocate;
	};
	
}

/*

	- How to apply copy-semantics to the allocator? (Hope a simple copy
	  constructor will work out?)

*/

template< typename InAllocator, typename InGrowthPolicy >
class TString
: public detail::ReallocAdapter< InAllocator >
, public StringBase
{
public:
	typedef detail::ReallocAdapter< InAllocator > Allocator;
	typedef InGrowthPolicy                        GrowthPolicy;
	typedef TString<InAllocator,InGrowthPolicy>   Self;

	TString()
	: Allocator()
	, StringBase()
	{
	}
	TString( const TString &other )
	: Allocator( other )
	, StringBase( other )
	{
		if( m_data != ( const char * )0 ) {
			++baseObject()->referenceCount;
		}
	}
#if HAS_CXX11_MOVE
	TString( TString &&other )
	: Allocator( other )
	, StringBase( other )
	{
	}
#endif
	TString( const char *cstring )
	: Allocator()
	, StringBase()
	{
		assign( cstring );
	}
	~TString()
	{
		purge();
	}

	/// Free the memory this string owns.
	///
	/// This is implemented by decrementing an internal reference count on the
	/// string.
	void purge() NOEXCEPT11
	{
		if( !m_data ) {
			return;
		}

		BaseObject *const baseObj = StringBase::baseObject();

		m_data = ( char * )0;
		m_offset = 0;
		m_length = 0;

		if( --baseObj->referenceCount != 0 ) {
			return;
		}
		
		Allocator::deallocate( ( void * )baseObj, baseObj->capacity );
	}

	Self left( DiffType offsetInCodepoints ) const NOEXCEPT11
	{
		const SizeType offsetInBytes =
			convertCodepointOffsetToByteIndex( offsetInCodepoints );

		Self result( *this );
		result.m_length = offsetInBytes;

		return result;
	}
	Self right( DiffType offsetInCodepoints ) const NOEXCEPT11
	{
		const SizeType offsetInBytes =
			// Have to mirror offset since this is "from the right side"
			convertCodepointOffsetToByteIndex
			(
				offsetInCodepoints < 0 ?
					-offsetInCodepoints + 1 :
					-offsetInCodepoints - 1
			);
			
		Self result( *this );
		result.m_length  = m_length - offsetInBytes;
		result.m_offset += offsetInBytes;

		return result;
	}
	Self mid( DiffType offsetInCodepoints, SizeType lengthInCodepoints = 1 )
	const NOEXCEPT11
	{
		const SizeType offsetInBytes =
			convertCodepointOffsetToByteIndex( offsetInCodepoints );
		
		Self result( *this );
		result.m_offset += offsetInBytes;
		result.m_length =
			result.convertCodepointOffsetToByteIndex(
				DiffType( lengthInCodepoints ) );

		return result;
	}
	
	Self substr( DiffType startInCodepoints, DiffType endInCodepoints )
	const NOEXCEPT11
	{
		const SizeType offsetInBytes[ 2 ] = {
			convertCodepointOffsetToByteIndex( startInCodepoints ),
			convertCodepointOffsetToByteIndex( endInCodepoints )
		};
		
		const SizeType startInBytes =
			utility::min( offsetInBytes[ 0 ], offsetInBytes[ 1 ] );
		const SizeType endInBytes =
			utility::max( offsetInBytes[ 0 ], offsetInBytes[ 1 ] );

		const SizeType rangeLengthInBytes = endInBytes - startInBytes;

		Self result( *this );
		result.m_offset += startInBytes;
		result.m_length = rangeLengthInBytes;

		return result;
	}
	
	bool reserve( SizeType desiredCapacityInBytes,
	Error *outError = ( Error * )0 )
	{
		// Early exit if nothing to do
		if( SLOWPATH( !m_data && !desiredCapacityInBytes ) ) {
			return Error::generate( outError, StringCode( kStr_Success ) );
		}
		
		// Old base object
		BaseObject *const baseObj =
			m_data != ( char * )0 ? baseObject() : ( BaseObject * )0;

		// Will hold the capacity in bytes after allocation
		SizeType capacityInBytes = 0;

		// Check if we have to make a new object
		//
		// We either don't have a base object already, or there are multiple
		// references to it
		if( !baseObj || baseObj->referenceCount != 1 ) {
			// Allocate the new base object
			BaseObject *const newBaseObj =
				( BaseObject * )
				Allocator::allocate
				(
					// Figure out our true desired capacity in bytes
					//
					// We use a growth policy here to allow user management
					GrowthPolicy::calculateGrowth
					(
						baseObj != ( BaseObject * )0 ? baseObj->capacity : 0,
						desiredCapacityInBytes + sizeof( BaseObject )
					),
					// The actual capacity is stored out here
					capacityInBytes
				);
			// Of course, if we couldn't allocate the memory then we should
			// complain
			if( SLOWPATH( !newBaseObj ) ) {
				return Error::generate( outError,
					StringCode( kStr_Error_OutOfMemory ) );
			}
			
			// Where we should copy to (or the base of the string)
			char *const target = newBaseObj->data();
			// Perform a copy if there was an old base object
			if( baseObj != ( BaseObject *)0 ) {
				// Source to copy from
				const char *const source = m_data + m_offset;
				// Number of bytes to copy from `source` to `target`
				const SizeType copySizeInBytes = lengthInBytes();

				// Copy between the targets
				memcpy( target, source, copySizeInBytes );
				
				// Mark that we're no longer using the old base object.
				//
				// We know `baseObj` is not `nullptr`
				//
				// We know that `baseObj`'s reference count is at least greater
				// than `1` as well
				//
				// Therefore, it's safe to just decrement here without trying to
				// check for `0` because we know the result **cannot** be zero.
				--baseObj->referenceCount;
			}

			m_data                     = target;
			m_offset                   = 0;
			newBaseObj->referenceCount = 1;
			newBaseObj->capacity       = capacityInBytes - sizeof( BaseObject );

			return Error::generate( outError, StringCode( kStr_Success ) );
		}
		
		// FIXME: If `m_offset` is not `0` then rather than reallocating this
		//        memory, it would be better to just allocate new memory.
		//        However, if we move the `m_offset` call into the above
		//        "allocate only" path, then we have to properly decrement
		//        `baseObj->referenceCount`, complete with follow-up deletion if
		//        it does reach `0`. (We don't do that currently because it's
		//        guaranteed to be greater than `1` initially.)

		// Check for an overflow
		if( SLOWPATH( desiredCapacityInBytes + m_offset <
		desiredCapacityInBytes || desiredCapacityInBytes + m_offset +
		sizeof( BaseObject ) < desiredCapacityInBytes + m_offset ) ) {
			// Treat as an out-of-memory condition
			return Error::generate( outError,
				StringCode( kStr_Error_OutOfMemory ) );
		}
		
		// Don't bother trying to reallocate if we've got enough space
		//
		// We can't do this earlier because a `reserve()` denotes an imminent
		// "write" operation, which will thus make a copy. So even if we have
		// enough space prior to calling `reserve()` we need to be the sole
		// owner of this memory to actually change its content.
		if( FASTPATH( desiredCapacityInBytes <= baseObj->capacity ) ) {
			// Don't need to do anything here
			return Error::generate( outError, StringCode( kStr_Success ) );
		}
		
		// Base object to reallocate (converted to `void*` for realloc)
		void *baseObjPtr = ( void * )baseObj;
		
		// Reallocate our base object (`baseObj` is not `nullptr` and its
		// `referenceCount` is exactly equal to `1`)
		if( SLOWPATH( !Allocator::reallocate(
			// Base pointer to modify
			baseObjPtr,
			// Figure out our true desired capacity in bytes
			//
			// We use a growth policy here to allow user management
			GrowthPolicy::calculateGrowth
			(
				baseObj != ( BaseObject * )0 ? baseObj->capacity : 0,
				desiredCapacityInBytes + m_offset + sizeof( BaseObject )
			),
			// The actual capacity is stored out here
			capacityInBytes
		) ) ) {
			// Something went wrong, so complain
			return Error::generate( outError,
				StringCode( kStr_Error_OutOfMemory ) );
		}

		// Update our stored pointer
		//
		// Our existing offset and length don't need to be updated since they're
		// relative to our pointer
		//
		// Likewise, we don't need to alter the base object's reference count
		// since this operation only took place *because* we own the only
		// reference
		m_data = ( ( BaseObject * )baseObjPtr )->data();
		
		// We've successfully resized
		return Error::generate( outError, StringCode( kStr_Success ) );
	}

	void clear() NOEXCEPT11
	{
		// Nothing to do if we've got nothing to clear
		//
		// This check needs to happen before we call `baseObject()` as that
		// expects `m_data` to not be `nullptr`.
		if( !m_data ) {
			return;
		}

		// Just zero the offset and length
		m_offset = 0;
		m_length = 0;

		// Base object (for managing the reference count)
		BaseObject *const baseObj = baseObject();

		// If we're the sole owner of this, then it's okay to keep it around
		// for memory reserve purposes to reduce allocations
		if( baseObj->referenceCount == 1 ) {
			return;
		}
		
		// Otherwise, we need to release our reference to it since the next time
		// we get something assigned we'd likely have to create a new copy
		// anyway, so it's better to return memory now.
		--baseObj->referenceCount;
		m_data = ( char * )0;
	}
	
	bool assign( const Self &other, Error *outError = ( Error * )0 ) NOEXCEPT11
	{
		if( !other.m_length ) {
			clear();
			return Error::generate( outError, StringCode( kStr_Success ) );
		}

		if( m_data != other.m_data ) {
			purge();
			if( ( m_data = other.m_data ) != ( char * )0 ) {
				++baseObject()->referenceCount;
			}
		}

		m_offset = other.m_offset;
		m_length = other.m_length;

		return Error::generate( outError, StringCode( kStr_Success ) );
	}

	bool assign( const char *s, const char *e, Error *outError = ( Error * )0 )
	{
		// Early exit on assign to null
		if( !s || e <= s ) {
			clear();
			return Error::generate( outError, StringCode( kStr_Success ) );
		}
		
		// Helper class to manage early scope exit cleanup
		//
		// Necessary in case an exception is thrown
		struct ResetSize
		{
			UInt32 &ref;
			UInt32  old;
			bool    unnecessary;
			
			ResetSize( UInt32 &ref ) NOEXCEPT11
			: ref( ref )
			, old( ref )
			, unnecessary( false )
			{
				ref = 0;
			}
			~ResetSize() NOEXCEPT11
			{
				if( !unnecessary ) {
					ref = old;
				}
			}
		};
		
		// Reset the length on early scope exit (potentially due to exception)
		//
		// This sets the length to zero, which is a performance optimization for
		// our `reserve()` call in certain cases
		ResetSize lengthReset( m_length );
		// Reset the offset on early scope exit as well
		//
		// This sets the offset to zero, which is a storage optimization for the
		// `reserve()` call. It's also a potential performance optimization as
		// it may allow us to avoid making a new reallocation if we happen to be
		// the sole owner of this reference.
		ResetSize offsetReset( m_offset );
		
		// Length of the other string in bytes
		const SizeType otherLengthInBytes = SizeType( DiffType( e - s ) );

		// Attempt to reserve enough memory to hold this new string
		//
		// (Have to force a copy here since we don't own the other allocator)
		if( !reserve( otherLengthInBytes, outError ) ) {
			return false;
		}
		
		// We no longer need to restore the length or offset, so a reset is now
		// unnecessary
		lengthReset.unnecessary = true;
		offsetReset.unnecessary = true;

		// Copy the new string in
		memcpy( m_data, s, otherLengthInBytes );
		
		// Specify our own length
		// NOTE: We want to leave `m_offset` at `0`
		m_length = otherLengthInBytes;

		// Success
		return Error::generate( outError, StringCode( kStr_Success ) );
	}
	bool assign( const char *cstring, Error *outError = ( Error * )0 )
	{
		return assign( cstring, strchr( cstring, '\0' ), outError );
	}
	
	template< typename OtherAllocator, typename OtherGrowthPolicy >
	bool assign( const TString< OtherAllocator, OtherGrowthPolicy > &other,
	Error *outError = ( Error * )0 )
	{
		return assign( other.get(), other.getEnd(), outError );
	}

	String &operator=( const Self &other )
	{
		assign( other );
		return *this;
	}
	String &operator=( const char *cstring )
	{
		assign( cstring );
		return *this;
	}
	template< typename OtherAllocator, typename OtherGrowthPolicy >
	String &operator=(
	const TString< OtherAllocator, OtherGrowthPolicy > &other )
	{
		return assign( other );
		return *this;
	}

	bool append( const char *s, const char *e, Error *outError = ( Error * )0 )
	{
		// Check for invalidate input or empty length
		if( e <= s || !s ) {
			return Error::generate( outError, StringCode( kStr_Success ) );
		}

		// Length of the input
		const SizeType lengthInBytes = SizeType( DiffType( e - s ) );

		// Avoid overflow
		if( m_length + lengthInBytes < m_length ) {
			return Error::generate( outError,
				StringCode( kStr_Error_OutOfMemory ) );
		}

		// Prepare enough space for the current text plus the new text
		if( !reserve( m_length + lengthInBytes, outError ) ) {
			return false;
		}

		// Copy the new text in
		memcpy( m_data + m_offset + m_length, s, lengthInBytes );

		// Reflect the new length of this string
		m_length += lengthInBytes;

		// Done
		return true;
	}
	bool append( const char *cstring, Error *outError = ( Error * )0 )
	{
		return append( cstring, strchr( cstring, '\0' ), outError );
	}
	bool append( const Self &other, Error *outError = ( Error * )0 )
	{
		return append( other.get(), other.getEnd(), outError );
	}
	
	template< typename OtherAllocator, typename OtherGrowthPolicy >
	bool append( const TString< OtherAllocator, OtherGrowthPolicy > &other,
	Error *outError = ( Error * )0 )
	{
		return append( other.get(), other.getEnd(), outError );
	}

	Self &operator+=( const Self &other )
	{
		append( other );
		return *this;
	}
	Self &operator+=( const char *cstring )
	{
		append( cstring );
		return *this;
	}
	template< typename OtherAllocator, typename OtherGrowthPolicy >
	Self &operator+=(
	const TString< OtherAllocator, OtherGrowthPolicy > &other )
	{
		return append( other );
		return *this;
	}

	Self &operator<<( const Self &other )
	{
		append( other );
		return *this;
	}
	Self &operator<<( const char *cstring )
	{
		append( cstring );
		return *this;
	}
	template< typename OtherAllocator, typename OtherGrowthPolicy >
	Self &operator<<(
	const TString< OtherAllocator, OtherGrowthPolicy > &other )
	{
		return append( other );
		return *this;
	}

	static inline Self concatenate( const char *as, const char *ae,
	const char *bs, const char *be, Error *outError = ( Error * )0 )
	{
		const SizeType aLength = SizeType( DiffType( ae - as ) );
		const SizeType bLength = SizeType( DiffType( be - bs ) );
		
		if( aLength + bLength < aLength ) {
			return Self();
		}

		Self result;
		bool r = true;

		r = r && result.reserve( aLength + bLength, outError );
		r = r && result.assign( as, ae, outError );
		r = r && result.append( as, ae, outError );

		if( !r ) {
			return Self();
		}

		return result;
	}
	static inline Self concatenate( const StringBase &a, const StringBase &b,
	Error *outError = ( Error * )0 )
	{
		return concatenate( a.get(), a.getEnd(), b.get(), b.getEnd(),
			outError );
	}
	static inline Self concatenate( const char *as, const char *ae,
	const StringBase &b, Error *outError = ( Error * )0 )
	{
		return concatenate( as, ae, b.get(), b.getEnd(), outError );
	}
	static inline Self concatenate( const StringBase &a, const char *bs,
	const char *be, Error *outError = ( Error * )0 )
	{
		return concatenate( a.get(), a.getEnd(), bs, be, outError );
	}
	static inline Self concatenate( const char *cstringA, const char *cstringB,
	Error *outError = ( Error * )0 )
	{
		return concatenate( cstringA, strchr( cstringA, '\0' ), cstringB,
			strchr( cstringB, '\0' ), outError );
	}
	static inline Self concatenate( const char *cstringA, const StringBase &b,
	Error *outError = ( Error * )0 )
	{
		return concatenate( cstringA, strchr( cstringA, '\0' ), b.get(),
			b.getEnd(), outError );
	}
	static inline Self concatenate( const StringBase &a, const char *cstringB,
	Error *outError = ( Error * )0 )
	{
		return concatenate( a.get(), a.getEnd(), cstringB,
			strchr( cstringB, '\0' ), outError );
	}
};

inline String operator+( const StringBase &a, const StringBase &b )
{
	return String::concatenate( a, b );
}
inline String operator+( const StringBase &a, const char *cstringB )
{
	return String::concatenate( a, cstringB );
}
inline String operator+( const char *cstringA, const StringBase &b )
{
	return String::concatenate( cstringA, b );
}

// -------------------------------------------------------------------------- //

String trySomething( const String &in )
{
	String result;

	result.assign( "Hello, " );
	result.append( in );
	result.append( "!" );

	return result;
}

#include <stdio.h>
int main()
{
	String test;

	test = "Hello, world!";
	printf( "%.*s\n", test.lenInt(), test.get() );

	test = "Walter";
	test = trySomething( test );
	printf( "%.*s\n", test.lenInt(), test.get() );

	if( test != "Hello, Walter!" ) {
		fprintf( stderr, "ERROR: test (\"%.*s\") != \"Hello, Walter!\"\n",
			test.lenInt(), test.get() );
		return EXIT_FAILURE;
	}

	String other( test.left( 5 ) );
	printf( "<%.*s>\n", other.lenInt(), other.get() );

	other += ", Johnson!";
	printf( "Test1: <%.*s>\n", test.lenInt(), test.get() );
	printf( "Test2: <%.*s>\n", other.lenInt(), other.get() );

	test.clear();
	printf( "Test1: <%.*s>\n", test.lenInt(), test.get() );
	printf( "Test2: <%.*s>\n", other.lenInt(), other.get() );

	other = other.right( 8 );
	printf( "Other: <%.*s>\n", other.lenInt(), other.get() );
	
	test = "Timmy";
	printf( "Test1: <%.*s>\n", test.lenInt(), test.get() );
	printf( "Test2: <%.*s>\n", other.lenInt(), other.get() );

	return EXIT_SUCCESS;
}
