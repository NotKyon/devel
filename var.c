#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

/* Boolean */
typedef int mk_bool_t;
#define MK_TRUE 1
#define MK_FALSE 0

/* Assert */
#if _MSC_VER||__INTEL_COMPILER
# define __func__ __FUNCTION__
#endif

#ifndef BREAKPOINT
# if MK_ARCH_X86||MK_ARCH_X86_64 || __i386__||__x86_64__ || _M_IX86||_M_IX64
#  if __GNUC__ || __clang__
#   define BREAKPOINT() __asm__("int $3")
#  else
#   define BREAKPOINT() __asm int 3
#  endif
# elif __GNUC__ || __clang__
#  define BREAKPOINT() __builtin_trap()
# else
#  error "Please provide a BREAKPOINT() macro for your platform."
# endif
#endif

#if DEBUG||_DEBUG||__debug__ || MK_DEBUG
# define ASSERT(x) if(!(x)){Error(__FILE__,__LINE__,__func__,\
	"ASSERT(%s) failed!", #x);BREAKPOINT();}
#else
# define ASSERT(x) /*do nothing!*/
#endif

/* Report */
#define TYPE_ERR "ERROR"
#define TYPE_WRN "WARNING"

void ReportV(const char *type, const char *file, int line, const char *func,
const char *message, va_list args);
void Report(const char *type, const char *file, int line, const char *func,
const char *message, ...);

void Warn(const char *file, int line, const char *func, const char *message,
...);
void Error(const char *file, int line, const char *func, const char *message,
...);

void WarnMessage(const char *message, ...);
void ErrorMessage(const char *message, ...);

void WarnFile(const char *file, int line, const char *message, ...);
void ErrorFile(const char *file, int line, const char *message, ...);

void ErrorFileExit(const char *file, int line, const char *message, ...);
void ErrorExit(const char *message, ...);

/* Memory */
void *Alloc(size_t n);
void *AllocZero(size_t n);
void *Free(void *p);

void *Memory(void *p, size_t n);

char *DuplicateN(const char *src, size_t srcn);
char *Duplicate(const char *src);

char *CopyN(char *dst, const char *src, size_t srcn);
char *Copy(char *dst, const char *src);

char *AppendNChar(char *dst, const char *src, size_t srcn, char ch);
char *AppendN(char *dst, const char *src, size_t srcn);
char *Append(char *dst, const char *src);

char *TrimAppendChar(char *dst, const char *src, char ch);
char *TrimAppend(char *dst, const char *src);

char *StrCpy(char *dst, size_t dstn, const char *src);
char *StrCpyN(char *dst, size_t dstn, const char *src, size_t srcn);

char *StrCat(char *dst, size_t dstn, const char *src);
char *StrCatN(char *dst, size_t dstn, const char *src, size_t srcn);

/* String */
typedef struct string_s {
	size_t capacity, length;
	char *p;
} string_t;

#define STR_CHUNK 64

void InitString(string_t *str);
void DeinitString(string_t *str);

void ResizeString(string_t *str, size_t len);

void PushChar(string_t *str, char ch);
void PushStringN(string_t *str, const char *s, size_t n);
void PushString(string_t *str, const char *s);

size_t StringCapacity(const string_t *str);
size_t StringLength(const string_t *str);
const char *StringBuffer(const string_t *str);
char *StringData(string_t *str);

void SetStringN(string_t *str, const char *s, size_t n);
void SetString(string_t *str, const char *s);

/* Array */
typedef struct array_s {
	size_t capacity, size;
	char **data;

	struct array_s *prev, *next;
} array_t;

array_t *NewArray();

size_t ArrayCapacity(array_t *arr);
size_t ArraySize(array_t *arr);

char **ArrayData(array_t *arr);

char *ArrayElement(array_t *arr, size_t i);

void SetArrayElementN(array_t *arr, size_t i, const char *cstr, size_t cstrn);
void SetArrayElement(array_t *arr, size_t i, const char *cstr);

void ClearArray(array_t *arr);
void DeleteArray(array_t *arr);
void DeleteAllArrays();

void ResizeArray(array_t *arr, size_t n);

size_t ArrayPushN(array_t *arr, const char *cstr, size_t cstrn);
size_t ArrayPush(array_t *arr, const char *cstr);

void RemoveArrayDups(array_t *arr);
void Explode(array_t *arr, const char *str);
char *Implode(array_t *arr);

/* Variables */
struct var_s;

typedef mk_bool_t(*commandfn_t)(struct var_s *var, array_t *args);

typedef struct var_s {
	char *name;
	char *value;

	size_t minArgs, maxArgs;
	commandfn_t cmd;

	struct var_s *prev, *next;
} var_t;

var_t *FindVar(const char *name);
var_t *FindCreateVar(const char *name);
var_t *AddCmdVar(const char *name, size_t numArgs, commandfn_t cmd);
var_t *SetVar(const char *name, const char *value);
const char *GetVar(const char *name);
mk_bool_t CallVar(var_t *var, array_t *args);

const char *FindRParen(const char *from);
const char *FindComma(const char *from);

void VarExpand(string_t *str, const char *src, array_t *args);

void ExpandTo(string_t *dst, const char *src);
char *Expand(const char *src);

/*
 * ==========================================================================
 *
 *	REPORT FUNCTIONS
 *
 * ==========================================================================
 */
void ReportV(const char *type, const char *file, int line, const char *func,
const char *message, va_list args) {
	int e;

	e = errno;

	fflush(stdout);

	if (file) {
		fprintf(stderr, "[%s", file);
		if (line)
			fprintf(stderr, "(%i)", line);

		if (func)
			fprintf(stderr, " %s", func);

		fprintf(stderr, "] ");
	} else if(func)
		fprintf(stderr, "%s: ", func);

	fprintf(stderr, "%s: ", type);

	vfprintf(stderr, message, args);

	if (e)
		fprintf(stderr, ": %s (%i)", strerror(e), e);

	fprintf(stderr, "\n");
	fflush(stderr);
}
void Report(const char *type, const char *file, int line, const char *func,
const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(type, file, line, func, message, args);
	va_end(args);
}

void Warn(const char *file, int line, const char *func, const char *message,
...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_WRN, file, line, func, message, args);
	va_end(args);
}
void Error(const char *file, int line, const char *func, const char *message,
...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_ERR, file, line, func, message, args);
	va_end(args);
}

void WarnMessage(const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_WRN, (const char *)0, 0, (const char *)0, message, args);
	va_end(args);
}
void ErrorMessage(const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_ERR, (const char *)0, 0, (const char *)0, message, args);
	va_end(args);
}

void WarnFile(const char *file, int line, const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_WRN, file, line, (const char *)0, message, args);
	va_end(args);
}
void ErrorFile(const char *file, int line, const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_ERR, file, line, (const char *)0, message, args);
	va_end(args);
}

void ErrorFileExit(const char *file, int line, const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_ERR, file, line, (const char *)0, message, args);
	va_end(args);

	exit(EXIT_FAILURE);
}
void ErrorExit(const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_ERR, (const char *)0, 0, (const char *)0, message, args);
	va_end(args);

	exit(EXIT_FAILURE);
}

/*
 * ==========================================================================
 *
 *	MEMORY FUNCTIONS
 *
 * ==========================================================================
 */
void *Alloc(size_t n) {
	void *p;

	if (!n)
		return (void *)0;

	p = malloc(n);
	if (!p) {
		BREAKPOINT();
		ErrorExit("Failed to allocate memory");
	}

	return p;
}
void *AllocZero(size_t n) {
	void *p;

	p = Alloc(n);
	if (!p)
		return (void *)0;

	memset(p, 0, n);

	return p;
}
void *Free(void *p) {
	if (!p)
		return (void *)0;

	free(p);
	return (void *)0;
}

void *Memory(void *p, size_t n) {
	if (!p)
		return Alloc(n);

	if (!n)
		return Free(p);

	p = realloc(p, n);
	if (!p)
		ErrorExit("Failed to reallocate memory");

	return p;
}

char *DuplicateN(const char *src, size_t srcn) {
	size_t l;
	char *p;

	if (!src)
		return (char *)0;

	l = srcn ? srcn : strlen(src);

	p = (char *)Alloc(l + 1);
	memcpy((void *)p, (const void *)src, l);
	p[l] = 0;

	return p;
}
char *Duplicate(const char *src) {
	return DuplicateN(src, 0);
}

char *CopyN(char *dst, const char *src, size_t srcn) {
	size_t len;

	if (!src)
		return (char *)Free((void *)dst);

	len = srcn ? srcn : strlen(src);

	dst = (char *)Memory((void *)dst, len + 1);
	memcpy((void *)dst, (const char *)src, len);
	dst[len] = 0;

	return dst;
}
char *Copy(char *dst, const char *src) {
	return CopyN(dst, src, 0);
}

char *AppendNChar(char *dst, const char *src, size_t srcn, char ch) {
	size_t l1, l2, l3;

	if (!dst)
		return DuplicateN(src, srcn);

	l1 = strlen(dst);
	l2 = srcn ? srcn : strlen(src);
	l3 = ch!='\0' ? 1 : 0;

	dst = (char *)Memory((void *)dst, l1 + l2 + l3 + 1);
	memcpy(&dst[l1], (const void *)src, l2);
	dst[l1 + l2] = ch;
	if (ch != '\0')
		dst[l1 + l2 + l3] = '\0';

	return dst;
}
char *AppendN(char *dst, const char *src, size_t srcn) {
	return AppendNChar(dst, src, srcn, '\0');
}
char *Append(char *dst, const char *src) {
	return AppendN(dst, src, 0);
}

char *TrimAppendChar(char *dst, const char *src, char ch) {
	const char *p, *q;

	for(p=src; *p<=' ' && *p!='\0'; p++);

	q = strchr(p, '\0');

	while(q > p) {
		if (*q++ > ' ')
			break;
	}

	if (!(q - p))
		return dst;

	return AppendNChar(dst, p, q - p, ch);
}
char *TrimAppend(char *dst, const char *src) {
	return TrimAppendChar(dst, src, '\0');
}

char *StrCpy(char *dst, size_t dstn, const char *src) {
	return StrCpyN(dst, dstn, src, 0);
}
char *StrCpyN(char *dst, size_t dstn, const char *src, size_t srcn) {
	size_t l;
	char *r;

	if (!dst) {
		Error(__FILE__,__LINE__,__func__, "NULL destination string passed.");
		return (char *)0;
	}

	if (!src) {
		Error(__FILE__,__LINE__,__func__, "NULL source string passed.");
		return (char *)0;
	}

	if (dstn < 2) {
		Error(__FILE__,__LINE__,__func__, "No room in destination string.");
		return (char *)0;
	}

	l = srcn ? srcn : strlen(src);

	if (l + 1 > dstn) {
		l = dstn - 1;
		r = (char *)0;

		Warn(__FILE__,__LINE__,__func__, "Overflow prevented.");
	} else
		r = dst;

	if (l)
		memcpy((void *)dst, (const void *)src, l);

	dst[l] = 0;

	return r;
}

char *StrCat(char *dst, size_t dstn, const char *src) {
	return StrCatN(dst, dstn, src, 0);
}
char *StrCatN(char *dst, size_t dstn, const char *src, size_t srcn) {
	char *p;

	p = strchr(dst, '\0');

	p = StrCpyN(p, dstn - (p - dst), src, srcn);
	if (p != (char *)0)
		p = dst;

	return p;
}


/*
 * ==========================================================================
 *
 *	STRING FUNCTIONS
 *
 * ==========================================================================
 */
void InitString(string_t *str) {
	ASSERT(str != (string_t *)0);

	str->capacity = 0;
	str->length = 0;
	str->p = (char *)0;
}
void DeinitString(string_t *str) {
	ASSERT(str != (string_t *)0);

	str->capacity = 0;
	str->length = 0;
	str->p = (char *)Memory((void *)str->p, 0);
}

void ResizeString(string_t *str, size_t len) {
	ASSERT(str != (string_t *)0);

	if (len + 1 > str->capacity) {
		str->capacity  = len + 1;
		str->capacity -= str->capacity%STR_CHUNK;
		str->capacity += STR_CHUNK;

		str->p = (char *)Memory((void *)str->p, str->capacity);
	}

	str->length = len;
}

void PushChar(string_t *str, char ch) {
	size_t len;

	ASSERT(str != (string_t *)0);

	len = str->length;
	ResizeString(str, len + 1);

	str->p[len + 0] = ch;
	str->p[len + 1] = '\0';
}
void PushStringN(string_t *str, const char *s, size_t n) {
	size_t l1, l2;

	ASSERT(str != (string_t *)0);
	ASSERT(s != (const char *)0);

	l1 = str->length;
	l2 = n ? n : strlen(s);

	ResizeString(str, l1 + l2);

	memcpy(&str->p[l1], (const void *)s, l2);
	str->p[l1 + l2] = '\0';
}
void PushString(string_t *str, const char *s) {
	ASSERT(str != (string_t *)0);
	ASSERT(s != (const char *)0);

	PushStringN(str, s, 0);
}

size_t StringCapacity(const string_t *str) {
	ASSERT(str != (const string_t *)0);

	return str->capacity;
}
size_t StringLength(const string_t *str) {
	ASSERT(str != (const string_t *)0);

	return str->length;
}
const char *StringBuffer(const string_t *str) {
	ASSERT(str != (const string_t *)0);

	return str->p;
}
char *StringData(string_t *str) {
	ASSERT(str != (string_t *)0);

	return str->p;
}

void SetStringN(string_t *str, const char *s, size_t n) {
	size_t len;

	ASSERT(str != (string_t *)0);
	ASSERT(s != (const char *)0);

	len = n ? n : strlen(s);
	ResizeString(str, len);

	memcpy(&str->p[0], (const void *)s, len);
	str->p[len] = 0;
}
void SetString(string_t *str, const char *s) {
	ASSERT(str != (string_t *)0);
	ASSERT(s != (const char *)0);

	SetStringN(str, s, 0);
}

/*
 * ==========================================================================
 *
 *	ARRAY FUNCTIONS
 *
 * ==========================================================================
 */
static array_t *g_arr_head = (array_t *)0;
static array_t *g_arr_tail = (array_t *)0;

array_t *NewArray() {
	array_t *arr;

	arr = (array_t *)Memory((void *)0, sizeof(*arr));

	arr->capacity = 0;
	arr->size = 0;
	arr->data = (char **)0;

	arr->next = (array_t *)0;
	if ((arr->prev = g_arr_tail) != (array_t *)0)
		g_arr_tail->next = arr;
	else
		g_arr_head = arr;
	g_arr_tail = arr;

	return arr;
}

size_t ArrayCapacity(array_t *arr) {
	ASSERT(arr != (array_t *)0);

	return arr->capacity;
}
size_t ArraySize(array_t *arr) {
	ASSERT(arr != (array_t *)0);

	return arr->size;
}

char **ArrayData(array_t *arr) {
	ASSERT(arr != (array_t *)0);

	return arr->data;
}

char *ArrayElement(array_t *arr, size_t i) {
	ASSERT(arr != (array_t *)0);
	ASSERT(i < arr->size);

	return arr->data[i];
}

void SetArrayElementN(array_t *arr, size_t i, const char *cstr, size_t cstrn) {
	ASSERT(arr != (array_t *)0);
	ASSERT(i < arr->size);

	arr->data[i] = CopyN(arr->data[i], cstr, cstrn);
}
void SetArrayElement(array_t *arr, size_t i, const char *cstr) {
	SetArrayElementN(arr, i, cstr, 0);
}

void ClearArray(array_t *arr) {
	size_t i;

	ASSERT(arr != (array_t *)0);

	for(i=0; i<arr->size; i++)
		arr->data[i] = (char *)Memory((void *)arr->data[i], 0);

	arr->data = (char **)Memory((void *)arr->data, 0);

	arr->capacity = 0;
	arr->size = 0;
}

void DeleteArray(array_t *arr) {
	if (!arr)
		return;

	ClearArray(arr);

	if (arr->prev)
		arr->prev->next = arr->next;
	if (arr->next)
		arr->next->prev = arr->prev;

	if (g_arr_head==arr)
		g_arr_head = arr->next;
	if (g_arr_tail==arr)
		g_arr_tail = arr->prev;

	Memory((void *)arr, 0);
}

void DeleteAllArrays() {
	while(g_arr_head)
		DeleteArray(g_arr_head);
}

void ResizeArray(array_t *arr, size_t n) {
	size_t i;

	ASSERT(arr != (array_t *)0);

	if (n > arr->capacity) {
		i = arr->capacity;
		arr->capacity += 4096/sizeof(void *);

		arr->data = (char **)Memory((void *)arr->data,
			arr->capacity*sizeof(char *));

		memset((void *)&arr->data[i], 0, (arr->capacity - i)*sizeof(char *));
	}

	arr->size = n;
}

size_t ArrayPushN(array_t *arr, const char *cstr, size_t cstrn) {
	size_t i;

	ASSERT(arr != (array_t *)0);

	i = ArraySize(arr);

	ResizeArray(arr, i + 1);
	SetArrayElementN(arr, i, cstr, cstrn);

	return i;
}
size_t ArrayPush(array_t *arr, const char *cstr) {
	return ArrayPushN(arr, cstr, 0);
}

void RemoveArrayDups(array_t *arr) {
	const char *a, *b;
	size_t i, j, n;

	n = ArraySize(arr);
	for(i=0; i<n; i++) {
		if (!(a = ArrayElement(arr, i)))
			continue;

		for(j=i + 1; j<n; j++) {
			if (!(b = ArrayElement(arr, j)))
				continue;

			if (!strcmp(a, b))
				SetArrayElement(arr, j, (const char *)0);
		}
	}
}
void Explode(array_t *arr, const char *str) {
	const char *p;
	char tmp[1024];

	p = str;
	while((p=strchr(p, ';')) != (const char *)0) {
		StrCpyN(tmp, sizeof(tmp), str, p - str);
		ArrayPush(arr, tmp);
		p++;
	}

	p = strchr(p, '\0');
	if (p - str > 0) {
		StrCpyN(tmp, sizeof(tmp), str, p - str);
		ArrayPush(arr, tmp);
	}
}
char *Implode(array_t *arr) {
	size_t i, n;
	char *p;

	ASSERT(arr != (array_t *)0);

	p = Duplicate("");

	n = ArraySize(arr);
	for(i=0; i<n; i++) {
		if (i > 0)
			Append(p, ";");

		/*
		 * FIXME: escape ';' in the source string
		 */
		p = Append(p, ArrayElement(arr, i));
	}

	return p;
}

/*
 * ==========================================================================
 *
 *	CRUDE VARIABLE SYSTEM
 *
 * ==========================================================================
 */
static var_t *g_var_head = (var_t *)0;
static var_t *g_var_tail = (var_t *)0;

static mk_bool_t NormalVar_f(var_t *cmd, array_t *args) {
	if (cmd||args) {/*nothing*/}

	return MK_TRUE;
}
static mk_bool_t ForeachCmd_f(var_t *cmd, array_t *args) {
	const char *s, *e, *itemExpr;
	mk_bool_t addSpace;
	string_t temp, value;
	var_t *iteratorVar;

	cmd->value = (char *)Memory((void *)cmd->value, 0);

	InitString(&temp);

	ExpandTo(&temp, ArrayElement(args, 0));
	iteratorVar = FindCreateVar(temp.p);
	if (!iteratorVar)
		return MK_FALSE;

	ResizeString(&temp, 0);
	ExpandTo(&temp, ArrayElement(args, 1));

	if (!temp.p) {
		DeinitString(&temp);
		return MK_FALSE;
	}

	InitString(&value);
	addSpace = MK_FALSE;

	itemExpr = ArrayElement(args, 2);

	s = temp.p;
	while(1) {
		while(*s<=' ' && *s!='\0')
			s++;

		if (*s=='\0')
			break;

		e = s;
		while(*e>' ')
			e++;

		iteratorVar->value = CopyN(iteratorVar->value, s, e - s);
		s = e;

		if (addSpace)
			PushChar(&value, ' ');

		ExpandTo(&value, itemExpr);

		addSpace = MK_TRUE;
	}

	cmd->value = value.p; /*we don't call DeinitString; take ownership*/

	DeinitString(&temp);
	return MK_TRUE;
}

var_t *FindVar(const char *name) {
	var_t *v;

	for(v=g_var_head; v; v=v->next) {
		if (!strcmp(v->name, name))
			return v;
	}

	return (var_t *)0;
}
var_t *FindCreateVar(const char *name) {
	var_t *v;

	v = FindVar(name);
	if (v)
		return v;

	v = (var_t *)Memory((void *)0, sizeof(*v));

	v->name = Duplicate(name);
	v->value = (char *)0;

	v->minArgs = 0;
	v->maxArgs = 0;
	v->cmd = NormalVar_f;

	v->next = (var_t *)0;
	if ((v->prev = g_var_tail) != (var_t *)0)
		g_var_tail->next = v;
	else
		g_var_head = v;
	g_var_tail = v;

	return v;
}
var_t *AddCmdVar(const char *name, size_t numArgs, commandfn_t cmd) {
	var_t *v;

	v = FindVar(name);
	if (v)
		return (var_t *)0;

	v = (var_t *)Memory((void *)0, sizeof(*v));

	v->name = Duplicate(name);
	v->value = (char *)0;

	v->minArgs = numArgs;
	v->maxArgs = numArgs;
	v->cmd = cmd;

	v->next = (var_t *)0;
	if ((v->prev = g_var_tail) != (var_t *)0)
		g_var_tail->next = v;
	else
		g_var_head = v;
	g_var_tail = v;

	return v;
}
var_t *SetVar(const char *name, const char *value) {
	var_t *v;

	v = FindCreateVar(name);
	if (!v)
		return (var_t *)0;

	v->value = Copy(v->value, value);
	return v;
}
const char *GetVar(const char *name) {
	var_t *v;

	v = FindCreateVar(name);
	if (!v)
		return "";

	return v->value ? v->value : "";
}
mk_bool_t CallVar(var_t *var, array_t *args) {
	size_t n;

	n = ArraySize(args);
	if (n<var->minArgs || n>var->maxArgs)
		return MK_FALSE;

	if (!var->cmd(var, args))
		return MK_FALSE;

	if (!var->value)
		var->value = Copy(var->value, "");

	return MK_TRUE;
}

const char *FindRParen(const char *from) {
	/*mk_bool_t inQuote, ignoreQuote;*/
	int nestLevel;

	/*inQuote = MK_FALSE;
	ignoreQuote = MK_FALSE;*/
	nestLevel = 0;

	while(*from!='\0') {
		/*
		if (*from=='\"' && !ignoreQuote) {
			inQuote ^= MK_TRUE;
			from++;
			continue;
		}

		if (inQuote && *from=='\\' && !ignoreQuote) {
			ignoreQuote = MK_TRUE;
			from++;
			continue;
		} else
			ignoreQuote = MK_FALSE;
		*/

		if (*from=='(' /*&& !inQuote*/) {
			nestLevel++;
			from++;
			continue;
		}

		if (*from==')' /*&& !inQuote*/) {
			if (nestLevel-- == 0)
				break;

			from++;
			continue;
		}

		from++;
	}

	return from;
}
const char *FindComma(const char *from) {
	mk_bool_t inQuote, ignoreQuote;

	inQuote = MK_FALSE;
	ignoreQuote = MK_FALSE;

	while(*from!='\0') {
		if (*from=='\"' && !ignoreQuote) {
			inQuote ^= MK_TRUE;
			from++;
			continue;
		}

		if (*from=='\\' && inQuote && !ignoreQuote) {
			ignoreQuote = MK_TRUE;
			from++;
			continue;
		} else
			ignoreQuote = MK_FALSE;

		if (*from==',')
			break;

		from++;
	}

	return from;
}

/*
//	The flags are "$(C$(XX)FLAGS)"
//	                 ^
//	                    ^
//
//	The flags are "$(CXXFLAGS)"
//	The flags are "-W -Wall -Weffc++ -pedantic"
//
//	A$(B$(C))
//	^                 dst='A'
//	 ^                check '$'
//	  ^               expression '('
//	   ^              B (not yet analyzed)
//	    ^             check '$'
//	     ^            expression '('
//	      ^           C (not yet analyzed)
//	       ^          end expression ')'; "<value of C>"
//	        ^         end expression ')'; "<value of B<value of C>>"
//
//	Start out expanding the initial string.
//	When a '$(' is encountered, find the matching ')'.
//		Take the string within those parentheses and run the expansion on that.
//		Run variable analysis on the expansion.
//
//	So, given "A$(B$(C))" assuming $(C)="X" and $(BX)="Y", the steps that occur
//	will be:
//
//		Place 'A' in dst. [dst="A"]
//		"$(" encountered:
//			Place 'B' in dst2. [dst2="B"]
//			"$(" encountered.
//				Place 'C' in dst3. [dst3="C"]
//				")" encountered. ("$(C)")
//				dst3 ("C") expanded ("X").
//				Expansion placed in dst2. [dst2="BX"]
//			")" encountered. ("$(B$(C))")
//			dst2 ("BX") expanded ("Y").
//			Expansion placed in dst. [dst="AY"]
//		End of string.
*/

/*
 * NOTE: This function shouldn't have to check for a ')'
 */
void VarExpand(string_t *str, const char *src, array_t *args) {
	const char *s, *e;
	var_t *var;
	char *work, *varName;

	/* nullify the working string for later use (see below) */
	work = (char *)0;

	/* skip leading whitespace */
	while(*src<=' ' && *src!='\0')
		src++;

	/* variable name starts here */
	s = src;

	/* find where the variable name ends */
	while(*src>' ' /*&& *src!=')'*/)
		src++;

	/* variable name has come to an end */
	e = src;

	/* record the variable's name */
	work = CopyN(work, s, e - s);
	varName = Expand(work);

	/* find the variable; if not found then early terminate */
	var = FindVar(varName);
	if (!var)
		return;

	/* we're afraid of ghosts */
	ResizeArray(args, 0);

	/* enumerate each argument */
	while(1) {
		/* skip spaces between arguments */
		while(*src<=' ' && *src!='\0')
			src++;

		/* a ')' and '\0' are the same to us */
		if (*src=='\0' /*|| *src==')'*/)
			break;

		/* we've found the start of the argument */
		s = src;

		/*
		 * TODO: Account for strings when checking the comma
		 */

		/* search for the next delimiter (whitespace doesn't count) */
#if 0
		while(*src!=',' && *src!='\0' /*&& *src!=')'*/)
			src++;
#else
		src = FindComma(src);
#endif

		/* end of argument has been located */
		e = src;

		/* break on NULL arguments */
		if ((e - s)==0)
			break;

		/*
		 * NOTE: Array arguments aren't expanded by us. They're expanded by the
		 *       function that uses them. An example of why this method is used
		 *       is 'foreach'.
		 */

		/* push the argument */
		ArrayPushN(args, s, e - s);

		/* no more arguments? */
		if (*e!=',')
			break;

		/* skip the comma */
		src = e + 1;
	}

	/* perform the call */
	if (!CallVar(var, args))
		return;

	if (var->value && var->value[0]!='\0')
		PushString(str, var->value);
}

void ExpandTo(string_t *dst, const char *src) {
	const char *e;
	array_t *args;
	char *p;

	args = NewArray();

	while(*src!='\0') {
		if (*src=='$') {
			src++;

			if (*src!='(') {
				PushChar(dst, *src++);
				continue;
			}

			src++;

			e = FindRParen(src); /*NOTE: null char instead of rparen is fine*/

			if ((size_t)(e - src) > 0) {
				p = DuplicateN(src, e - src);
				VarExpand(dst, p, args);
				p = Memory((void *)p, 0);
			}

			src = e + 1;
			continue;
		}

		PushChar(dst, *src++);
	}

	DeleteArray(args);
}
char *Expand(const char *src) {
	string_t str;

	InitString(&str);
	SetString(&str, "");

	ExpandTo(&str, src);

	return str.p;
}

/*
 * ==========================================================================
 *
 *	MAIN PROGRAM
 *
 * ==========================================================================
 */

void TestExpand(const char *src) {
	char *dst;

	dst = Expand(src);
	printf("\"%s\" -> \"%s\"\n", src, dst);
	dst = (char *)Memory((void *)dst, 0);
}

int main() {
	SetVar("B", "Z");
	SetVar("BX", "Y");
	SetVar("C", "X");

	SetVar("CFLAGS", "-W -Wall -pedantic -std=c99");
	SetVar("CXXFLAGS", "-W -Wall -Weffc++ -pedantic -ansi");
	SetVar("XX", "");

	AddCmdVar("foreach", 3, ForeachCmd_f);

	SetVar("INCDIRS", "/usr/include /usr/local/include ~/ember/include");

	TestExpand("A$(B$(C))");
	TestExpand("Flags: \"$(C$(XX)FLAGS)\"");
	TestExpand("Dirs: $(foreach DIR,$(INCDIRS),\"-I$(DIR)\")");

	return 0;
}
