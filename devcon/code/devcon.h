#ifndef DEVCON_H
#define DEVCON_H

#include <GL/glfw.h>
#include <GL/gl.h>

#include <stdarg.h>
#include <stddef.h>

#include "renderer.h"
#include "lookup.h"

/*
 * -----------------------------------------------------------------------------
 *
 *	DEVELOPER CONSOLE SYSTEM
 *
 * -----------------------------------------------------------------------------
 */
#define DEVCON_KEY '`' /*96*/

enum {
	kConColor_Background, /*background color of the console*/
	kConColor_Foreground, /*normal foreground text*/
	kConColor_Error, /*error color*/
	kConColor_Warning, /*warning color*/
	kConColor_Prompt, /*color of the prompt in the edit field*/
	kConColor_EditField, /*what you type into...*/
	kConColor_Autocomplete, /*within edit-field*/

	kNumConColors
};

#define DEVCON_COLUMNS 80
#define DEVCON_ROWS 0x4000
#define DEVCON_TEXT (DEVCON_COLUMNS*DEVCON_ROWS)
#define DEVCON_COMMAND 0x100
#define DEVCON_PROMPT 0x20

typedef struct devcon_s {
	color_t palette[kNumConColors];
	int curPalette; /*current palette color index*/

	int visible; /*is the console visible at all?*/
	int height; /*height of the console in pixels*/

	int curRow; /*which row are we currently viewing? (bottom)*/
	int visRows; /*how many rows are visible?*/
	int numRows; /*how many rows have been written?*/

	int curText; /*index into the text buffer*/

	unsigned short text[DEVCON_TEXT]; /*text buffer*/

	char prompt[DEVCON_PROMPT]; /*prompt*/
	char command[DEVCON_COMMAND]; /*command buffer*/
	char autocomplete[DEVCON_COMMAND]; /*autocomplete*/

	int curCommand; /*current index in the command buffer*/

	GLuint fontImage; /*built-in font*/
} devcon_t;

extern int g_devconAlive;
extern devcon_t g_devcon;

extern unsigned char g_devconFont[128*128]; /*GL_LUMINANCE*/

/*
 * Initialize the developer console system. Call this BEFORE any other developer
 * console calls!
 * NOTE: Initializing the developer console does not automatically show it. The
 *       developer console is hidden by default.
 */
void con_Init();

/*
 * Deinitialize the developer console system.
 */
void con_Deinit();

/*
 * Determine whether the developer console has been initialized.
 *
 * return: 1 if the developer console has been initialized; 0 otherwise.
 */
int con_IsAlive();

/*
 * Show the developer console.
 */
void con_Show();

/*
 * Hide the developer console.
 */
void con_Hide();

/*
 * Determine whether the developer console is visible.
 *
 * return: 1 if the developer console is visible; 0 otherwise.
 */
int con_IsVisible();

/*
 * Render the developer console using OpenGL.
 */
void con_Render();

/*
 * Print to the developer console using printf style formatting. Special
 * character codes exist in the string to indicate which color to use for the
 * text. (If no character code is specified, the default foreground color is
 * assumed.)
 *
 * format: Formatting string. This has the same attributes as printf.
 * args: Argument list for the format.
 *
 * Color Codes
 * -----------
 * "^0" = Foreground
 * "^1" = Error
 * "^2" = Warning
 * "^3" = Prompt
 * "^4" = Edit Field
 * "^5" = Autocomplete
 */
void con_PrintV(const char *format, va_list args);

/*
 * Print to the developer console using con_PrintV() without an explicit args
 * parameter.
 */
void con_Print(const char *format, ...);

/*
 * Key handling
 *
 * Uses the current event. (see event.h)
 */
void con_KeyEvent();

/*
 * Mouse event handling
 *
 * Uses the current event. (see event.h)
 */
void con_MouseEvent();

#endif
