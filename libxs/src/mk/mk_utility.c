#include <time.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>

#include "mk_utility.h"
#include "mk_assert.h"

const char *va(const char *format, ...) {
	static char buf[0x10000];
	static size_t i = 0;
	va_list args;
	size_t j;
	int r;

	j = i;

	va_start(args, format);
	r = vsnprintf(&buf[j], sizeof(buf)-1-j, format, args);
	if (r==-1) {
		/* overflow prevented? */
		buf[sizeof(buf)-1] = 0;
		i = 0;
	} else {
		/* advance to the next position in the buffer */
		buf[j + r] = 0;
		i += r + 1;
	}
	va_end(args);

	return &buf[j];
}

const char *getdir(char *buf, size_t n, const char *filename) {
	const char *p;

	if ((p = strrchr(filename, '/')) != (const char *)0) {
		XsStrCpyN(buf, n, filename, (p - filename) + 1);
		return &p[1];
	}

	*buf = 0;
	return filename;
}

void extsubst(char *dst, size_t dstn, const char *src, const char *ext) {
	const char *p;

	assert(dst != (char *)0);
	assert(dstn > 1);
	assert(src != (const char *)0);

	p = strrchr(src, '.');
	if (!p)
		p = strchr(src, '\0');

	assert(p != (const char *)0);

	XsStrCpyN(dst, dstn, src, p - src);
	XsStrCpy(&dst[p - src], dstn - (size_t)(p - src), ext);
}

int shell(const char *format, ...) {
	static char cmd[16384];
	va_list args;

	va_start(args, format);
	vsnprintf(cmd, sizeof(cmd) - 1, format, args);
	cmd[sizeof(cmd) - 1] = 0;
	va_end(args);

	if (g_flags & kFlag_Verbose_Bit) {
		fprintf(stdout, "%s\n", cmd);
		fflush(stdout);
	}

	return system(cmd);
}

int matchpath(const char *rpath, const char *apath) {
	size_t rl, al;

	assert(rpath != (const char *)0);
	assert(apath != (const char *)0);

	rl = strlen(rpath);
	al = strlen(apath);

	if (rl > al)
		return 0;

	/*
	 * TODO: Case-insensitive compare on Windows hosts.
	 */
	if (strcmp(&apath[al-rl], rpath) != 0)
		return 0;

	if (al - rl > 0) {
		if (apath[al - rl - 1] != '/')
			return 0;
	}

	return 1;
}

int getintegraldate() {
	struct tm *p;
	time_t t;

	t = time(0);
	p = localtime(&t);

	return (p->tm_year + 1900)*10000 + (p->tm_mon + 1)*100 + p->tm_mday;
}

const char *getext(const char *filename) {
	const char *p;

	p = strrchr(filename, '.');

	return p ? p : "";
}
int isextcpp(const char *ext) {
	/*
	 * TODO: Use a look-up table of acceptable characters.
	 *
	 * int acceptable[256], r;
	 *
	 * memset(acceptable, 0, sizeof(acceptable));
	 *
	 * acceptable['.'] = 1;
	 * acceptable['c'] = 2;
	 * acceptable['C'] = 2;
	 * acceptable['x'] = 3;
	 * acceptable['X'] = 3;
	 * acceptable['p'] = 4;
	 * acceptable['P'] = 4;
	 * acceptable['+'] = 5;
	 *
	 * if (acceptable[*(unsigned char *)ext++]!=1) return 0;
	 * if (acceptable[*(unsigned char *)ext++]!=2) return 0;
	 *
	 * r = acceptable[*(unsigned char *)ext++];
	 * if (r < 3) return 0;
	 *
	 * if (acceptable[*(unsigned char *)ext++]!=r) return 0;
	 * if (*ext!='\0') return 0;
	 *
	 * return 1;
	 */
	if (*ext++ != '.')
		return 0;

	if (*ext != 'c' && *ext != 'C') {
		/* Objective-C++ counts as C++ */
		if (*ext=='m' || *ext=='M') {
			ext++;

			if (*ext=='m' || *ext=='M') {
				ext++;

				return *ext=='\0' ? 1 : 0;
			}
		}

		return 0;
	}

	ext++;

	if (*ext=='c' || *ext=='C') {
		ext++;

		return *ext=='\0' ? 1 : 0;
	}

	if (*ext=='x' || *ext=='X') {
		ext++;

		if (*ext=='x' || *ext=='X') {
			ext++;

			return *ext=='\0' ? 1 : 0;
		}

		return 0;
	}

	if (*ext=='p' || *ext=='P') {
		ext++;

		if (*ext=='p' || *ext=='P') {
			ext++;

			return *ext=='\0' ? 1 : 0;
		}

		return 0;
	}

	if (*ext=='+') {
		ext++;

		if (*ext=='+') {
			ext++;

			return *ext=='\0' ? 1 : 0;
		}

		return 0;
	}

	return 0;
}
int isextc(const char *ext) {
	if (*ext++ != '.')
		return 0;

	if (*ext=='c' || *ext=='C') {
		ext++;

		return *ext=='\0' ? 1 : 0;
	}

	return 0;
}
