#ifndef GB_BASE_H
#define GB_BASE_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "config.h"

#include "base/app.h"
#include "base/engine.h"
#include "base/except.h"
#include "base/memory.h"
#include "base/report.h"
#include "base/console.h"
#include "base/runtime.h"
#include "base/handle.h"

#endif
