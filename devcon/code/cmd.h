#ifndef CMD_H
#define CMD_H

#include "basictypes.h"

/*
 * =============================================================================
 *
 *	COMMAND BUFFER
 *
 * =============================================================================
 */
typedef struct cmdBuf_s {
	size_t capacity, length;
	char **commands;
} cmdBuf_t;

extern cmdBuf_t g_cmdBuf;

/*
 * Retrieve the capacity of the command buffer.
 *
 * return: Number of elements the command buffer can fit without needing to be
 *         reallocated.
 */
size_t cmd_Capacity();

/*
 * Retrieve the length of the command buffer.
 *
 * return: Number of elements currently in the command buffer.
 */
size_t cmd_Length();

/*
 * Retrieve a command in the command buffer.
 *
 * i: Zero-based index into the command buffer.
 * return: The command in the command buffer or NULL if 'i' exceeds the number
 *         of commands in the buffer.
 */
const char *cmd_Get(size_t i);

/*
 * Queue a command for execution into the command buffer. These commands can be
 * executed with a call to cmd_Execute().
 *
 * command: String describing the command. Read below.
 *
 * COMMAND FORMAT
 * --------------
 * Commands are relatively simple. There are three types of commands currently:
 *
 * set <cvar> <value>: Sets <cvar> to <value>, but if <cvar> doesn't exist, it
 *                     will be created.
 * <cvar> <value>: Sets <cvar> to <value>, but complains if <cvar> doesn't
 *                 exist.
 * <cvar>: Display the contents of <cvar>.
 */
void cmd_Add(const char *command);

/*
 * Execute the commands currently in the command buffer, then empty the command
 * buffer. See cmd_Add() for the details of each command.
 *
 * return: 1 if all of the commands executed successfully; 0 otherwise.
 */
int cmd_Execute();

#endif
