	.file	"clamp.c"
	.text
	.globl	clamp
	.align	16, 0x90
	.type	clamp,@function
clamp:                                  # @clamp
# BB#0:
	pushl	%eax
	movl	8(%esp), %eax
	movl	%eax, %ecx
	andl	$-2147483648, %ecx      # imm = 0xFFFFFFFF80000000
	orl	$1065353216, %ecx       # imm = 0x3F800000
	movl	$1065353216, %edx       # imm = 0x3F800000
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %edx
	shrl $31, %edx
	neg  %edx
	and  %edx, %ecx
	mov  %ecx, %eax
	xorl $0xFFFFFFFF, %edx
	and  %eax, %edx
	or   %edx, %eax
	
	#NO_APP
	movd	%eax, %xmm0
	movss	%xmm0, (%esp)
	flds	(%esp)
	popl	%eax
	ret
.Ltmp0:
	.size	clamp, .Ltmp0-clamp

	.globl	test
	.align	16, 0x90
	.type	test,@function
test:                                   # @test
# BB#0:
	subl	$28, %esp
	movss	32(%esp), %xmm0
	cvtss2sd	%xmm0, %xmm1
	movsd	%xmm1, 4(%esp)
	movd	%xmm0, %eax
	movl	%eax, %ecx
	andl	$-2147483648, %ecx      # imm = 0xFFFFFFFF80000000
	orl	$1065353216, %ecx       # imm = 0x3F800000
	movl	$1065353216, %edx       # imm = 0x3F800000
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %edx
	shrl $31, %edx
	neg  %edx
	and  %edx, %ecx
	mov  %ecx, %eax
	xorl $0xFFFFFFFF, %edx
	and  %eax, %edx
	or   %edx, %eax
	
	#NO_APP
	movl	%eax, 20(%esp)
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 12(%esp)
	movl	$.L.str, (%esp)
	calll	printf
	addl	$28, %esp
	ret
.Ltmp1:
	.size	test, .Ltmp1-test

	.globl	main
	.align	16, 0x90
	.type	main,@function
main:                                   # @main
# BB#0:
	pushl	%edi
	pushl	%esi
	subl	$36, %esp
	movl	$-1069547520, %eax      # imm = 0xFFFFFFFFC0400000
	movl	$1065353216, %esi       # imm = 0x3F800000
	movl	$-1082130432, %edi      # imm = 0xFFFFFFFFBF800000
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %esi
	shrl $31, %esi
	neg  %esi
	and  %esi, %edi
	mov  %edi, %eax
	xorl $0xFFFFFFFF, %esi
	and  %eax, %esi
	or   %esi, %eax
	
	#NO_APP
	movl	%eax, 20(%esp)
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 12(%esp)
	movl	$-1073217536, 8(%esp)   # imm = 0xFFFFFFFFC0080000
	movl	$0, 4(%esp)
	movl	$.L.str, (%esp)
	calll	printf
	movl	$-1077936128, %eax      # imm = 0xFFFFFFFFBFC00000
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %esi
	shrl $31, %esi
	neg  %esi
	and  %esi, %edi
	mov  %edi, %eax
	xorl $0xFFFFFFFF, %esi
	and  %eax, %esi
	or   %esi, %eax
	
	#NO_APP
	movl	%eax, 20(%esp)
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 12(%esp)
	movl	$-1074266112, 8(%esp)   # imm = 0xFFFFFFFFBFF80000
	movl	$0, 4(%esp)
	movl	$.L.str, (%esp)
	calll	printf
	movl	$-1098907648, %eax      # imm = 0xFFFFFFFFBE800000
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %esi
	shrl $31, %esi
	neg  %esi
	and  %esi, %edi
	mov  %edi, %eax
	xorl $0xFFFFFFFF, %esi
	and  %eax, %esi
	or   %esi, %eax
	
	#NO_APP
	movl	%eax, 20(%esp)
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 12(%esp)
	movl	$-1076887552, 8(%esp)   # imm = 0xFFFFFFFFBFD00000
	movl	$0, 4(%esp)
	movl	$.L.str, (%esp)
	calll	printf
	movl	$1051394572, %eax       # imm = 0x3EAB020C
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %esi
	shrl $31, %esi
	neg  %esi
	and  %esi, %esi
	mov  %esi, %eax
	xorl $0xFFFFFFFF, %esi
	and  %eax, %esi
	or   %esi, %eax
	
	#NO_APP
	movl	%eax, 20(%esp)
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 12(%esp)
	movl	$1070948417, 8(%esp)    # imm = 0x3FD56041
	movl	$-2147483648, 4(%esp)   # imm = 0xFFFFFFFF80000000
	movl	$.L.str, (%esp)
	calll	printf
	movl	$1063675494, %eax       # imm = 0x3F666666
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %esi
	shrl $31, %esi
	neg  %esi
	and  %esi, %esi
	mov  %esi, %eax
	xorl $0xFFFFFFFF, %esi
	and  %eax, %esi
	or   %esi, %eax
	
	#NO_APP
	movl	%eax, 20(%esp)
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 12(%esp)
	movl	$1072483532, 8(%esp)    # imm = 0x3FECCCCC
	movl	$-1073741824, 4(%esp)   # imm = 0xFFFFFFFFC0000000
	movl	$.L.str, (%esp)
	calll	printf
	movl	$1071560786, %eax       # imm = 0x3FDEB852
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %esi
	shrl $31, %esi
	neg  %esi
	and  %esi, %esi
	mov  %esi, %eax
	xorl $0xFFFFFFFF, %esi
	and  %eax, %esi
	or   %esi, %eax
	
	#NO_APP
	movl	%eax, 20(%esp)
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 12(%esp)
	movl	$1073469194, 8(%esp)    # imm = 0x3FFBD70A
	movl	$1073741824, 4(%esp)    # imm = 0x40000000
	movl	$.L.str, (%esp)
	calll	printf
	movl	$1176256512, %eax       # imm = 0x461C4000
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %esi
	shrl $31, %esi
	neg  %esi
	and  %esi, %esi
	mov  %esi, %eax
	xorl $0xFFFFFFFF, %esi
	and  %eax, %esi
	or   %esi, %eax
	
	#NO_APP
	movl	%eax, 20(%esp)
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 12(%esp)
	movl	$1086556160, 8(%esp)    # imm = 0x40C38800
	movl	$0, 4(%esp)
	movl	$.L.str, (%esp)
	calll	printf
	movl	$-971227136, %eax       # imm = 0xFFFFFFFFC61C4000
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %esi
	shrl $31, %esi
	neg  %esi
	and  %esi, %edi
	mov  %edi, %eax
	xorl $0xFFFFFFFF, %esi
	and  %eax, %esi
	or   %esi, %eax
	
	#NO_APP
	movl	%eax, 20(%esp)
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 12(%esp)
	movl	$-1060927488, 8(%esp)   # imm = 0xFFFFFFFFC0C38800
	movl	$0, 4(%esp)
	movl	$.L.str, (%esp)
	calll	printf
	xorl	%eax, %eax
	#APP
	andl $0x7FFFFFFF, %eax
	sub  %eax, %esi
	shrl $31, %esi
	neg  %esi
	and  %esi, %esi
	mov  %esi, %eax
	xorl $0xFFFFFFFF, %esi
	and  %eax, %esi
	or   %esi, %eax
	
	#NO_APP
	movl	%eax, 20(%esp)
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$.L.str, (%esp)
	calll	printf
	xorl	%eax, %eax
	addl	$36, %esp
	popl	%esi
	popl	%edi
	ret
.Ltmp2:
	.size	main, .Ltmp2-main

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	 "clamp(%f) = %f (0x%.8X)\n"
	.size	.L.str, 25


	.section	".note.GNU-stack","",@progbits
