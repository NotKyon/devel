#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <vector> //want std::move<>

#define __TOSTRING(x) #x
#define TOSTRING(x) __TOSTRING(x)

#define CONSTANT 256

inline void print(const char *x, ...)
{
	va_list args;
	char buf[8192];

	va_start(args, x);
	vsnprintf(buf, sizeof(buf), x, args);
	buf[sizeof(buf) - 1] = '\0';
	va_end(args);

	fprintf(stdout, "%s\n", buf);
	fflush(stdout);
}

template<class _Functor>
class scope_guard
{
protected:
	enum class state
	{
		Valid,
		Committed
	};

	_Functor m_Func;
	state m_State;

public:
	inline scope_guard(const _Functor &func): m_Func(func), m_State(state::Valid)
	{ print("normal_constructor(%p)", (void *)this); }
	inline scope_guard(scope_guard<_Functor> &&sg): m_Func(sg.m_Func), m_State(sg.m_State)
	{ sg.commit();print("move_constructor(%p)", (void *)this); }
	inline ~scope_guard() { print("destructor(%p)", (void *)this); if (m_State==state::Valid) m_Func(); }

	inline void commit() { print("commit(%p)", (void *)this); m_State = state::Committed; }
};

template<class _Functor>
inline scope_guard<_Functor> make_scope_guard(const _Functor &func)
{
	return std::forward<scope_guard<_Functor>>(func);
}
#define ADD_SCOPE_GUARD(lambda) auto __scopeGuard_##__COUNTER__ = make_scope_guard([&]()lambda)
#define __CONCAT(x,y) x##y
#define CONCAT(x,y) __CONCAT(x,y)

int main()
{
	ADD_SCOPE_GUARD({print("scope_guard::b");});
	{
		ADD_SCOPE_GUARD({print("scope_guard::a");});
	}
    printf("CONSTANT: %s\n", TOSTRING(CONSTANT));
	printf("CONCAT(x,__COUNTER__): %s\n", TOSTRING(CONCAT(x,__COUNTER__)));
	printf("CONCAT(x,__COUNTER__): %s\n", TOSTRING(CONCAT(x,__COUNTER__)));
	printf("CONCAT(x,__COUNTER__): %s\n", TOSTRING(CONCAT(x,__COUNTER__)));
    return EXIT_SUCCESS;
}
