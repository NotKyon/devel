#include "matrix.h"

/*
 * Create an identity matrix.
 */
void Matrix_Identity(matrix_t *dst) {
	dst->m11=1; dst->m12=0; dst->m13=0; dst->m14=0;
	dst->m21=0; dst->m22=1; dst->m23=0; dst->m24=0;
	dst->m31=0; dst->m32=0; dst->m33=1; dst->m34=0;
	dst->m41=0; dst->m42=0; dst->m43=0; dst->m44=1;
}

/*
 * Transpose a 4x4 or 3x3 matrix.
 * NOTE: dst cannot be the same address as src.
 */
void Matrix_Transpose(matrix_t *dst, const matrix_t *src) {
	dst->m11 = src->m11;
	dst->m12 = src->m21;
	dst->m13 = src->m31;
	dst->m14 = src->m41;

	dst->m21 = src->m12;
	dst->m22 = src->m22;
	dst->m23 = src->m32;
	dst->m24 = src->m42;

	dst->m31 = src->m13;
	dst->m32 = src->m23;
	dst->m33 = src->m33;
	dst->m34 = src->m43;

	dst->m41 = src->m14;
	dst->m42 = src->m24;
	dst->m43 = src->m34;
	dst->m44 = src->m44;
}
void Matrix_Transpose3x3(matrix_t *dst, const matrix_t *src) {
	dst->m11 = src->m11;
	dst->m12 = src->m21;
	dst->m13 = src->m31;

	dst->m21 = src->m12;
	dst->m22 = src->m22;
	dst->m23 = src->m32;

	dst->m31 = src->m13;
	dst->m32 = src->m23;
	dst->m33 = src->m33;
}

/*
 * Create an inverse matrix from a rigid-body transform. (This expects an
 * orthonormal matrix; any matrix with uniform scaling and the bottom row as
 * [0,0,0,1].)
 */
void Matrix_RigidInverse(matrix_t *dst, const matrix_t *src) {
	Matrix_Transpose3x3(dst, src);

	dst->m14 = -(src->m14*dst->m11 + src->m24*dst->m12 + src->m34*dst->m13);
	dst->m24 = -(src->m14*dst->m21 + src->m24*dst->m22 + src->m34*dst->m23);
	dst->m34 = -(src->m14*dst->m31 + src->m24*dst->m32 + src->m34*dst->m33);

	dst->m41 = 0;
	dst->m42 = 0;
	dst->m43 = 0;
	dst->m44 = 1;
}

/*
 * Multiply a 4x4 or 3x3 matrix with another, storing the result in a third.
 * NOTE: dst cannot be the same address as a or b.
 */
void Matrix_Multiply(matrix_t *dst, const matrix_t *a, const matrix_t *b) {
	dst->m11 = a->m11*b->m11 + a->m12*b->m21 + a->m13*b->m31 + a->m14*b->m41;
	dst->m12 = a->m11*b->m12 + a->m12*b->m22 + a->m13*b->m32 + a->m14*b->m42;
	dst->m13 = a->m11*b->m13 + a->m12*b->m23 + a->m13*b->m33 + a->m14*b->m43;
	dst->m14 = a->m11*b->m14 + a->m12*b->m24 + a->m13*b->m34 + a->m14*b->m44;

	dst->m21 = a->m21*b->m11 + a->m22*b->m21 + a->m23*b->m31 + a->m24*b->m41;
	dst->m22 = a->m21*b->m12 + a->m22*b->m22 + a->m23*b->m32 + a->m24*b->m42;
	dst->m23 = a->m21*b->m13 + a->m22*b->m23 + a->m23*b->m33 + a->m24*b->m43;
	dst->m24 = a->m21*b->m14 + a->m22*b->m24 + a->m23*b->m34 + a->m24*b->m44;

	dst->m31 = a->m31*b->m11 + a->m32*b->m21 + a->m33*b->m31 + a->m34*b->m41;
	dst->m32 = a->m31*b->m12 + a->m32*b->m22 + a->m33*b->m32 + a->m34*b->m42;
	dst->m33 = a->m31*b->m13 + a->m32*b->m23 + a->m33*b->m33 + a->m34*b->m43;
	dst->m34 = a->m31*b->m14 + a->m32*b->m24 + a->m33*b->m34 + a->m34*b->m44;

	dst->m41 = a->m41*b->m11 + a->m42*b->m21 + a->m43*b->m31 + a->m44*b->m41;
	dst->m42 = a->m41*b->m12 + a->m42*b->m22 + a->m43*b->m32 + a->m44*b->m42;
	dst->m43 = a->m41*b->m13 + a->m42*b->m23 + a->m43*b->m33 + a->m44*b->m43;
	dst->m44 = a->m41*b->m14 + a->m42*b->m24 + a->m43*b->m34 + a->m44*b->m44;
}
void Matrix_Multiply3x3(matrix_t *dst, const matrix_t *a, const matrix_t *b) {
	dst->m11 = a->m11*b->m11 + a->m12*b->m21 + a->m13*b->m31;
	dst->m12 = a->m11*b->m12 + a->m12*b->m22 + a->m13*b->m32;
	dst->m13 = a->m11*b->m13 + a->m12*b->m23 + a->m13*b->m33;

	dst->m21 = a->m21*b->m11 + a->m22*b->m21 + a->m23*b->m31;
	dst->m22 = a->m21*b->m12 + a->m22*b->m22 + a->m23*b->m32;
	dst->m23 = a->m21*b->m13 + a->m22*b->m23 + a->m23*b->m33;

	dst->m31 = a->m31*b->m11 + a->m32*b->m21 + a->m33*b->m31;
	dst->m32 = a->m31*b->m12 + a->m32*b->m22 + a->m33*b->m32;
	dst->m33 = a->m31*b->m13 + a->m32*b->m23 + a->m33*b->m33;
}

/*
 * Perform an optimized rigid-transform matrix multiplication.
 * NOTE: dst cannot be the same address as a or b.
 */
void Matrix_RigidMultiply(matrix_t *dst, const matrix_t *a, const matrix_t *b) {
	dst->m11 = a->m11*b->m11 + a->m12*b->m21 + a->m13*b->m31;
	dst->m12 = a->m11*b->m12 + a->m12*b->m22 + a->m13*b->m32;
	dst->m13 = a->m11*b->m13 + a->m12*b->m23 + a->m13*b->m33;
	dst->m14 = a->m11*b->m14 + a->m12*b->m24 + a->m13*b->m34 + a->m14;

	dst->m21 = a->m21*b->m11 + a->m22*b->m21 + a->m23*b->m31;
	dst->m22 = a->m21*b->m12 + a->m22*b->m22 + a->m23*b->m32;
	dst->m23 = a->m21*b->m13 + a->m22*b->m23 + a->m23*b->m33;
	dst->m24 = a->m21*b->m14 + a->m22*b->m24 + a->m23*b->m34 + a->m24;

	dst->m31 = a->m31*b->m11 + a->m32*b->m21 + a->m33*b->m31;
	dst->m32 = a->m31*b->m12 + a->m32*b->m22 + a->m33*b->m32;
	dst->m33 = a->m31*b->m13 + a->m32*b->m23 + a->m33*b->m33;
	dst->m34 = a->m31*b->m14 + a->m32*b->m24 + a->m33*b->m34 + a->m34;

	dst->m41 = 0;
	dst->m42 = 0;
	dst->m43 = 0;
	dst->m44 = 1;
}

/*
 * Create a translation matrix, or set the translation of an existing matrix.
 */
void Matrix_Translation(matrix_t *dst, const vec3_t *v) {
	float x, y, z;

	x = v->x;
	y = v->y;
	z = v->z;

	dst->m11=1; dst->m12=0; dst->m13=0; dst->m14=x;
	dst->m21=0; dst->m22=1; dst->m23=0; dst->m24=y;
	dst->m31=0; dst->m32=0; dst->m33=1; dst->m34=z;
	dst->m41=0; dst->m42=0; dst->m43=0; dst->m44=1;
}
void Matrix_SetTranslation(matrix_t *dst, const vec3_t *v) {
	dst->m14 = v->x;
	dst->m24 = v->y;
	dst->m34 = v->z;
}

/*
 * Apply translation to all axes at once or individually.
 */
void Matrix_Move(matrix_t *dst, const vec3_t *v) {
	dst->m14 += dst->m11*v->x + dst->m12*v->y + dst->m13*v->z;
	dst->m24 += dst->m21*v->x + dst->m22*v->y + dst->m23*v->z;
	dst->m34 += dst->m31*v->x + dst->m32*v->y + dst->m33*v->z;
}
void Matrix_MoveX(matrix_t *dst, float x) {
	dst->m14 += dst->m11*x;
	dst->m24 += dst->m21*x;
	dst->m34 += dst->m31*x;
}
void Matrix_MoveY(matrix_t *dst, float y) {
	dst->m14 += dst->m12*y;
	dst->m24 += dst->m22*y;
	dst->m34 += dst->m32*y;
}
void Matrix_MoveZ(matrix_t *dst, float z) {
	dst->m14 += dst->m13*z;
	dst->m24 += dst->m23*z;
	dst->m34 += dst->m33*z;
}

/*
 * Create a rotation matrix for the individual axes, or a concatenated rotation
 * matrix in ZXY (Unity/Nuclear Fusion/NASA) rotation order.
 */
void Matrix_RotationX(matrix_t *dst, float angle) {
	float c, s;

	c = Cos(angle);
	s = Sin(angle);

	dst->m11=1;  dst->m12=0;  dst->m13=0;  dst->m14=0;
	dst->m21=0;  dst->m22=c;  dst->m23=-s; dst->m24=0;
	dst->m31=0;  dst->m32=s;  dst->m33=c;  dst->m34=0;
	dst->m41=0;  dst->m42=0;  dst->m43=0;  dst->m44=1;
}
void Matrix_RotationY(matrix_t *dst, float angle) {
	float c, s;

	c = Cos(angle);
	s = Sin(angle);

	dst->m11=c;  dst->m12=0;  dst->m13=s;  dst->m14=0;
	dst->m21=0;  dst->m22=1;  dst->m23=0;  dst->m24=0;
	dst->m31=-s; dst->m32=0;  dst->m33=c;  dst->m34=0;
	dst->m41=0;  dst->m42=0;  dst->m43=0;  dst->m44=1;
}
void Matrix_RotationZ(matrix_t *dst, float angle) {
	float c, s;

	c = Cos(angle);
	s = Sin(angle);

	dst->m11=c;  dst->m12=-s; dst->m13=0;  dst->m14=0;
	dst->m21=s;  dst->m22=c;  dst->m23=0;  dst->m24=0;
	dst->m31=0;  dst->m32=0;  dst->m33=1;  dst->m34=0;
	dst->m41=0;  dst->m42=0;  dst->m43=0;  dst->m44=1;
}
void Matrix_Rotation(matrix_t *dst, const vec3_t *v) {
	matrix_t rx, ry, rz, t;

	Matrix_RotationX(&rx, v->x);
	Matrix_RotationY(&ry, v->y);
	Matrix_RotationZ(&rz, v->z);

	Matrix_Multiply3x3(&t, &rz, &rx);
	Matrix_Multiply3x3(dst, &t, &ry);

	dst->m14 = 0;
	dst->m24 = 0;
	dst->m34 = 0;
	dst->m41 = 0;
	dst->m42 = 0;
	dst->m43 = 0;
	dst->m44 = 1;
}

/*
 * Apply rotation to all axes or individual axes. (i.e., turn relative to the
 * current angle.)
 */
void Matrix_TurnX(matrix_t *dst, float angle) {
	float c, s, t;

	c = Cos(angle);
	s = Sin(angle);

	t = dst->m12;
	dst->m12 = t*c  + dst->m13*s;
	dst->m13 = t*-s + dst->m13*c;

	t = dst->m22;
	dst->m22 = t*c  + dst->m23*s;
	dst->m23 = t*-s + dst->m23*c;

	t = dst->m32;
	dst->m32 = t*c  + dst->m33*s;
	dst->m33 = t*-s + dst->m33*c;
}
void Matrix_TurnY(matrix_t *dst, float angle) {
	float c, s, t;

	c = Cos(angle);
	s = Sin(angle);

	t = dst->m11;
	dst->m11 = t*c + dst->m13*-s;
	dst->m13 = t*s + dst->m13*c;

	t = dst->m21;
	dst->m21 = t*c + dst->m23*-s;
	dst->m23 = t*s + dst->m23*c;

	t = dst->m31;
	dst->m31 = t*c + dst->m33*-s;
	dst->m33 = t*s + dst->m33*c;
}
void Matrix_TurnZ(matrix_t *dst, float angle) {
	float c, s, t;

	c = Cos(angle);
	s = Sin(angle);

	t = dst->m11;
	dst->m11 = t*c  + dst->m12*s;
	dst->m12 = t*-s + dst->m12*c;

	t = dst->m21;
	dst->m21 = t*c  + dst->m22*s;
	dst->m22 = t*-s + dst->m22*c;

	t = dst->m31;
	dst->m31 = t*c  + dst->m32*s;
	dst->m32 = t*-s + dst->m32*c;
}
void Matrix_Turn(matrix_t *dst, const vec3_t *v) {
	/* Unity/NF/NASA rotation order: ZXY */
	Matrix_TurnZ(dst, v->z);
	Matrix_TurnX(dst, v->x);
	Matrix_TurnY(dst, v->y);
}

/*
 * Create a scaling matrix.
 */
void Matrix_Scaling(matrix_t *dst, const vec3_t *v) {
	float x, y, z;

	x = v->x;
	y = v->y;
	z = v->z;

	dst->m11=x; dst->m12=0; dst->m13=0; dst->m14=0;
	dst->m21=0; dst->m22=y; dst->m23=0; dst->m24=0;
	dst->m31=0; dst->m32=0; dst->m33=z; dst->m34=0;
	dst->m41=0; dst->m42=0; dst->m43=0; dst->m44=1;
}

/*
 * Scale an existing matrix by all axes or individual axes.
 */
void Matrix_ScaleEx(matrix_t *dst, const matrix_t *src, const vec3_t *v) {
	dst->m11 = src->m11*v->x;
	dst->m21 = src->m21*v->x;
	dst->m31 = src->m31*v->x;

	dst->m12 = src->m12*v->y;
	dst->m22 = src->m22*v->y;
	dst->m32 = src->m32*v->y;

	dst->m13 = src->m13*v->z;
	dst->m23 = src->m23*v->z;
	dst->m33 = src->m33*v->z;
}
void Matrix_Scale(matrix_t *dst, const vec3_t *v) {
	Matrix_ScaleEx(dst, dst, v);
}
void Matrix_ScaleX(matrix_t *dst, float factor) {
	dst->m11 *= factor;
	dst->m21 *= factor;
	dst->m31 *= factor;
}
void Matrix_ScaleY(matrix_t *dst, float factor) {
	dst->m12 *= factor;
	dst->m22 *= factor;
	dst->m32 *= factor;
}
void Matrix_ScaleZ(matrix_t *dst, float factor) {
	dst->m13 *= factor;
	dst->m23 *= factor;
	dst->m33 *= factor;
}

/*
 * Decompose a matrix
 */
void Matrix_GetTranslation(vec3_t *dst, const matrix_t *src) {
	dst->x = src->m14;
	dst->y = src->m24;
	dst->z = src->m34;
}
void Matrix_GetScaling(vec3_t *dst, const matrix_t *src) {
	dst->x = sqrtf(Vec3_Length(Vec3(src->m11, src->m21, src->m31)));
	dst->y = sqrtf(Vec3_Length(Vec3(src->m12, src->m22, src->m32)));
	dst->z = sqrtf(Vec3_Length(Vec3(src->m13, src->m23, src->m33)));
}
void Matrix_GetScalingFast(vec3_t *dst, const matrix_t *src) {
	dst->x = SqrtFast(Vec3_Length(Vec3(src->m11, src->m21, src->m31)));
	dst->y = SqrtFast(Vec3_Length(Vec3(src->m12, src->m22, src->m32)));
	dst->z = SqrtFast(Vec3_Length(Vec3(src->m13, src->m23, src->m33)));
}
void Matrix_GetInvScalingF(vec3_t *dst, const matrix_t *src) {
	dst->x = InvSqrtFast(Vec3_Length(Vec3(src->m11, src->m21, src->m31)));
	dst->y = InvSqrtFast(Vec3_Length(Vec3(src->m12, src->m22, src->m32)));
	dst->z = InvSqrtFast(Vec3_Length(Vec3(src->m13, src->m23, src->m33)));
}
void Matrix_GetRotationExFast(vec3_t *dst, const matrix_t *src, const vec3_t *s) {
	matrix_t norm;
	float v;

	Matrix_ScaleEx(&norm, src, s);

	v = -src->m23;

	if (v > 0.998) {
		dst->y = ATan2(src->m31, src->m11);
		dst->x = 0.5f*3.1415926535f;
		dst->z = 0;

		return;
	}

	if (v < -0.998) {
		dst->y = ATan2(src->m31, src->m11);
		dst->x = -0.5f*3.1415926535f;
		dst->z = 0;

		return;
	}

	dst->x = ASin(v);
	dst->y = ATan2(src->m13, src->m33);
	dst->z = ATan2(src->m21, src->m22);
}
void Matrix_GetRotationEx(vec3_t *dst, const matrix_t *src, const vec3_t *s) {
	Matrix_GetRotationExFast(dst, src, Vec3(1.0f/s->x, 1.0f/s->y, 1.0f/s->z));
}
void Matrix_GetQuaternionExFast(quat_t *dst, const matrix_t *src, const vec3_t *s) {
#ifndef MAX
# define MAX_NOT_DEFINED
# define MAX(a,b) ((a)>(b)?(a):(b))
#endif
	matrix_t norm;

	Matrix_ScaleEx(&norm, src, s);

	dst->w = sqrtf(MAX(0, 1 + norm.m11 + norm.m22 + norm.m33))/2.0f;
	dst->x = sqrtf(MAX(0, 1 + norm.m11 - norm.m22 - norm.m33))/2.0f;
	dst->y = sqrtf(MAX(0, 1 - norm.m11 + norm.m22 - norm.m33))/2.0f;
	dst->z = sqrtf(MAX(0, 1 - norm.m11 - norm.m22 + norm.m33))/2.0f;

	dst->x = CopySign(dst->x, norm.m23 - norm.m32);
	dst->y = CopySign(dst->y, norm.m31 - norm.m13);
	dst->z = CopySign(dst->z, norm.m12 - norm.m21);
#ifdef MAX_NOT_DEFINED
# undef MAX
#endif
}
void Matrix_GetQuaternionEx(quat_t *dst, const matrix_t *src, const vec3_t *s) {
	Matrix_GetQuaternionExFast(dst, src, Vec3(1.0f/s->x, 1.0f/s->y, 1.0f/s->z));
}
void Matrix_GetRotation(vec3_t *dst, const matrix_t *src) {
	vec3_t s;

	Matrix_GetScaling(&s, src);
	Matrix_GetRotationEx(dst, src, &s);
}
void Matrix_GetRotationFast(vec3_t *dst, const matrix_t *src) {
	vec3_t s;

	Matrix_GetInvScalingF(&s, src);
	Matrix_GetRotationExFast(dst, src, &s);
}
void Matrix_GetQuaternion(quat_t *dst, const matrix_t *src) {
	vec3_t s;

	Matrix_GetScaling(&s, src);
	Matrix_GetQuaternionEx(dst, src, &s);
}
void Matrix_GetQuaternionFast(quat_t *dst, const matrix_t *src) {
	vec3_t s;

	Matrix_GetInvScalingF(&s, src);
	Matrix_GetQuaternionExFast(dst, src, &s);
}

/*
 * Create a projection matrix.
 */
void Matrix_Perspective(matrix_t *dst, float fov, float aspect, float zn, float zf) {
	float x, y, a, b;

	y = 1 / Tan(0.5f * fov);
	x = y / aspect;

	a =     zf/(zf - zn);
	b = -zn*zf/(zf - zn);

	dst->m11=x; dst->m12=0; dst->m13=0; dst->m14=0;
	dst->m21=0; dst->m22=y; dst->m23=0; dst->m24=0;
	dst->m31=0; dst->m32=0; dst->m33=a; dst->m34=b;
	dst->m41=0; dst->m42=0; dst->m43=1; dst->m44=0;
}
