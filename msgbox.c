#include <windows.h>
#include <stdio.h>
#include <string.h>

int main( int argc, char **argv ) {
	DWORD iconType;
	DWORD buttonType;
	const char *title;
	int i;
	int r;

	title = "";
	iconType = 0;
	buttonType = MB_OK;
	
	for( i = 1; i < argc; ++i ) {
		if( strcmp( argv[ i ], "-info" ) == 0 ) {
			iconType = MB_ICONINFORMATION;
			continue;
		}
		
		if( strcmp( argv[ i ], "-warning" ) == 0 ) {
			iconType = MB_ICONWARNING;
			continue;
		}
		
		if( strcmp( argv[ i ], "-error" ) == 0 ) {
			iconType = MB_ICONERROR;
			continue;
		}
		
		if( strcmp( argv[ i ], "-ask" ) == 0 ) {
			iconType = MB_ICONQUESTION;
			continue;
		}
		
		if( strcmp( argv[ i ], "-yesno" ) == 0 ) {
			buttonType = MB_YESNO;
			continue;
		}
		
		if( strcmp( argv[ i ], "-abortretryignore" ) == 0 ) {
			buttonType = MB_ABORTRETRYIGNORE;
			continue;
		}
		
		if( strcmp( argv[ i ], "-title" ) ) {
			++i;
			title = ( i < argc ) ? argv[ i ] : "";
			continue;
		}

		r = MessageBoxA( GetDesktopWindow(), argv[ i ], title,
			iconType | buttonType );
		Sleep( 1000 );
	}
	
	return r;
}
