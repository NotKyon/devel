#ifndef IOBJECT_H
#define IOBJECT_H

#include "IBase.h"

class IObject: public virtual IBase {
public:
	virtual void DoSomething() = 0;
};

#endif
