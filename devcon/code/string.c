#include "string.h"
#include "memory.h"
#include <string.h>

char *str_Duplicate(const char *src) {
	size_t n;
	char *p;

	if (!src)
		return (char *)0;

	n = strlen(src) + 1;

	p = (char *)mem_Alloc(n);
	memcpy((void *)p, (const void *)src, n);

	return p;
}

char *str_Copy(char *dst, const char *src) {
	size_t n;
	char *p;

	if (!dst)
		return str_Duplicate(src);

	if (!src) {
		mem_Free((void *)dst);
		return (char *)0;
	}

	n = strlen(src) + 1;

	p = (char *)mem_Realloc((void *)dst, n);
	memcpy((void *)p, (const void *)src, n);

	return p;
}
