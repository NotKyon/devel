#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#if __APPLE__
# include <OpenGL/gl.h>
#else
# include <GL/gl.h>
#endif

#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#if _WIN32
# if _MSC_VER
#  include "DXSDK_2010_06/Include/XAudio2.h"
# else
#  include <xaudio2.h>
# endif
#endif

// placement new
template<class _type_>
inline void *operator new(size_t n, _type_ *p) {
	(void)n;
	return p;
}

// window title
#ifndef APPTITLE
# define APPTITLE "Platformer"
#endif

// base directory searched for resources
#ifndef BASEDIR
# define BASEDIR "res/main"
#endif

//============================================================================//
// * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * //
////////////////////////////////////////////////////////////////////////////////
//############################################################################//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//----------------------------------------------------------------------------//
//............................................................................//
//                                                                            //
//                 /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\                 //
//              //============================================\\              //
//          //====================================================\\          //
//      //============================================================\\      //
//  //====================================================================\\  //
////////////////////////////////////////////////////////////////////////////////
//****************************************************************************//
//*                                                                          *//
//* ======================================================================== *//
//*     ------------              ------------              ------------     *//
//*     LIBRARY CODE              LIBRARY CODE              LIBRARY CODE     *//
//*     ------------              ------------              ------------     *//
//* ======================================================================== *//
//*                                                                          *//
//****************************************************************************//
////////////////////////////////////////////////////////////////////////////////
//  \\====================================================================//  //
//      \\============================================================//      //
//          \\====================================================//          //
//              \\============================================//              //
//                 \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/                 //
//                                                                            //
//............................................................................//
//----------------------------------------------------------------------------//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//############################################################################//
////////////////////////////////////////////////////////////////////////////////
// * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * //
//============================================================================//

/*
===============================================================================

	Memory Tags
	Add new memory tags here

===============================================================================
*/
#define MEMORY_TAGS_LIST()\
	MEMTAG( TAG_NONE,				"Generic" )\
	\
	MEMTAG( TAG_AXLIB_ARR,			"AxLib Array" )\
	\
	MEMTAG( TAG_FW_LOADTGA,			"Framework LoadTGA" )\
	MEMTAG( TAG_FW_IMAGE,			"Framework Image" )\
	MEMTAG( TAG_FW_AUDIO_BUFFER,	"Framework Audio Buffer" )

/*
===============================================================================

	Configuration

===============================================================================
*/
#ifndef AX_DEBUG_ENABLED
# ifdef _DEBUG
#  define AX_DEBUG_ENABLED 1
# else
#  define AX_DEBUG_ENABLED 0
# endif
#endif

#ifndef AX_TEST_ENABLED
# if defined(TEST)||defined(_TEST)||defined(__test__)
#  define AX_TEST_ENABLED 1
# else
#  define AX_TEST_ENABLED 0
# endif
#endif

#ifndef AX_DEBUG_OR_TEST_ENABLED
# if AX_DEBUG_ENABLED || AX_TEST_ENABLED
#  define AX_DEBUG_OR_TEST_ENABLED 1
# else
#  define AX_DEBUG_OR_TEST_ENABLED 0
# endif
#endif

/*
===============================================================================

	C++11 Support

===============================================================================
*/
#define _MSC_VER__MAJOR (_MSC_VER/100 - 6)
#define _MSC_VER__MINOR (_MSC_VER%100)
#define __ICC_MAJOR (__INTEL_COMPILER/100)
#define __ICC_MINOR (__INTEL_COMPILER/10%10)

#define AX_CHECK_COMPILER_VERSION(inMajor,inMinor,cmpMajor,cmpMinor)\
	((inMajor==cmpMajor && inMinor>=cmpMinor) || (inMajor>cmpMajor))
#define AX_CHECK_GCC_VER(major,minor)\
	AX_CHECK_COMPILER_VERSION(__GNUC__,__GNUC_MINOR__,major,minor)
#define AX_CHECK_ICC_VER(major,minor)\
	AX_CHECK_COMPILER_VERSION(__ICC_MAJOR,__ICC_MINOR,major,minor)
#define AX_CHECK_MSC_VER(major,minor)\
	AX_CHECK_COMPILER_VERSION(_MSC_VER__MAJOR,_MSC_VER__MINOR,major,minor)
#define AX_CHECK_CLANG_VER(major,minor)\
	AX_CHECK_COMPILER_VERSION(__clang_major__,__clang_minor__, major,minor)
#define AX_CHECK_GCC_ICC_MSC_CLANG_VER(gccA,gccB,iccA,iccB,mscA,mscB,claA,claB)\
	(AX_CHECK_GCC_VER(gccA,gccB)||\
	AX_CHECK_ICC_VER(iccA,iccB)||\
	AX_CHECK_MSC_VER(mscA,mscB)||\
	AX_CHECK_CLANG_VER(claA,claB))
#define AX_CHECK_GCC_VERSION AX_CHECK_GCC_VER
#define AX_CHECK_ICC_VERSION AX_CHECK_ICC_VER
#define AX_CHECK_MSC_VERSION AX_CHECK_MSC_VER
#define AX_CHECK_CLANG_VERSION AX_CHECK_CLANG_VER

#if _MSC_VER
# pragma warning(disable:4351) //array initialization via constructor
#endif


//
//	C++11 (Partially) Available?
//	http://wiki.apache.org/stdcxx/C%2B%2B0xCompilerSupport
//
#ifndef AX_CXX11_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,6, 11,0, 10,0, 2,9)
#  define AX_CXX11_ENABLED 1
# else
#  define AX_CXX11_ENABLED 0
# endif
#endif

//
//	alignas [N2341]
//
#ifndef AX_CXX_ALIGNAS_ENABLED
# if AX_CHECK_GCC_VERSION(4,8) || AX_CHECK_CLANG_VERSION(3,0)
#  define AX_CXX_ALIGNAS_ENABLED 1
# else
#  define AX_CXX_ALIGNAS_ENABLED 0
# endif
#endif

//
//	alignof [N2341]
//
#ifndef AX_CXX_ALIGNOF_ENABLED
# if AX_CHECK_GCC_VERSION(4,5) || AX_CHECK_CLANG_VERSION(2,9)
#  define AX_CXX_ALIGNOF_ENABLED 1
# else
#  define AX_CXX_ALIGNOF_ENABLED 0
# endif
#endif

//
//	C++11 Atomics [N2427]
//
#ifndef AX_CXX_ATOMICS_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,4, 13,0, 11,0, 3,1)
#  define AX_CXX_ATOMICS_ENABLED 1
# else
#  define AX_CXX_ATOMICS_ENABLED 0
# endif
#endif

//
//	auto [N1984,N2546]
//
#ifndef AX_CXX_AUTO_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,4, 11,0, 10,0, 0,0)
#  define AX_CXX_AUTO_ENABLED 1
# else
#  define AX_CXX_AUTO_ENABLED 0
# endif
#endif

//
//	C99 preprocessor [N1653]
//
#ifndef AX_C99_PREPROCESSOR_ENABLED
# if AX_CHECK_GCC_VERSION(4,3) || AX_CHECK_ICC_VERSION(11,1) || __clang__
#  define AX_C99_PREPROCESSOR_ENABLED 1
# else
#  define AX_C99_PREPROCESSOR_ENABLED 0
# endif
#endif

//
//	constexpr [N2235]
//
#ifndef AX_CXX_CONSTEXPR_ENABLED
# if AX_CHECK_GCC_VERSION(4,6) || AX_CHECK_ICC_VERSION(13,0)\
|| AX_CHECK_CLANG_VERSION(3,1)
#  define AX_CXX_CONSTEXPR_ENABLED 1
# else
#  define AX_CXX_CONSTEXPR_ENABLED 0
# endif
#endif

//
//	decltype [N2343,N3276]
//
#ifndef AX_CXX_DECLTYPE_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,3, 11,0, 10,0, 2,9)
#  define AX_CXX_DECLTYPE_ENABLED 1
# else
#  define AX_CXX_DECLTYPE_ENABLED 0
# endif
#endif

//
//	Defaulted and Deleted Functions [N2346]
//
#ifndef AX_CXX_DEFAULT_DELETE_FUNCTIONS_ENABLED
# if AX_CHECK_GCC_VERSION(4,4) || AX_CHECK_ICC_VERSION(12,0)\
|| AX_CHECK_CLANG_VERSION(3,0)
#  define AX_CXX_DEFAULT_DELETE_FUNCTIONS_ENABLED 1
# else
#  define AX_CXX_DEFAULT_DELETE_FUNCTIONS_ENABLED 0
# endif
#endif

//
//	Delegating Constructors [N1986]
//
#ifndef AX_CXX_DELEGATING_CTORS_ENABLED
# if AX_CHECK_GCC_VERSION(4,7) || AX_CHECK_MSC_VERSION(11,0)\
|| AX_CHECK_CLANG_VERSION(3,0)
#  define AX_CXX_DELEGATING_CTORS_ENABLED 1
# else
#  define AX_CXX_DELEGATING_CTORS_ENABLED 0
# endif
#endif

//
//	Explicit Conversion Operators [N2437]
//
#ifndef AX_CXX_EXPLICIT_CONVERSION_OPERATORS_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,5, 13,0, 11,0, 3,0)
#  define AX_CXX_EXPLICIT_CONVERSION_OPERATORS_ENABLED 1
# else
#  define AX_CXX_EXPLICIT_CONVERSION_OPERATORS_ENABLED 0
# endif
#endif

//
//	Extended 'friend' Declarations [N1791]
//
#ifndef AX_CXX_EXTENDED_FRIEND_DECLARATIONS_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,7, 11,0, 10,0, 2,9)
#  define AX_CXX_EXTENDED_FRIEND_DECLARATIONS_ENABLED 1
# else
#  define AX_CXX_EXTENDED_FRIEND_DECLARATIONS_ENABLED 0
# endif
#endif

//
//	extern template [N1987]
//
#ifndef AX_CXX_EXTERN_TEMPLATE_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(3,3, 9,0, 6,0, 1,0)
#  define AX_CXX_EXTERN_TEMPLATE_ENABLED 1
# else
#  define AX_CXX_EXTERN_TEMPLATE_ENABLED 0
# endif
#endif

//
//	Forward Declarations For Enums [N2764]
//
#ifndef AX_CXX_FORWARD_ENUMS_ENABLED
# if AX_CHECK_GCC_VERSION(4,6) || AX_CHECK_MSC_VERSION(11,0)\
|| AX_CHECK_CLANG_VERSION(3,1)
#  define AX_CXX_FORWARD_ENUMS_ENABLED 1
# else
#  define AX_CXX_FORWARD_ENUMS_ENABLED 0
# endif
#endif

//
//	Inheriting Constructors [N2540]
//
#ifndef AX_CXX_INHERITING_CONSTRUCTORS_ENABLED
# if AX_CHECK_GCC_VERSION(4,8)
#  define AX_CXX_INHERITING_CONSTRUCTORS_ENABLED 1
# else
#  define AX_CXX_INHERITING_CONSTRUCTORS_ENABLED 0
# endif
#endif

//
//	Initializer Lists [N2672]
//
#ifndef AX_CXX_INIT_LISTS_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,4, 13,0, 11,0, 3,1)
#  define AX_CXX_INIT_LISTS_ENABLED 1
# else
#  define AX_CXX_INIT_LISTS_ENABLED 0
# endif
#endif

//
//	Lambda Expressions and Closures [N2550,N2658,N2927]
//
#ifndef AX_CXX_LAMBDA_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,5, 11,0, 10,0, 3,1)
#  define AX_CXX_LAMBDA_ENABLED 1
# else
#  define AX_CXX_LAMBDA_ENABLED 0
# endif
#endif

//
//	Local/Anonymous Types as Template Arguments [N2657]
//
#ifndef AX_CXX_LOCAL_TYPE_TEMPLATE_ARGS_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,5, 12,0, 10,0, 2,9)
#  define AX_CXX_LOCAL_TYPE_TEMPLATE_ARGS_ENABLED 1
# else
#  define AX_CXX_LOCAL_TYPE_TEMPLATE_ARGS_ENABLED 0
# endif
#endif

//
//	Long Long [N1811]
//
#ifndef AX_CXX_LONG_LONG_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(1,0, 1,0, 10,0, 1,0)
#  define AX_CXX_LONG_LONG_ENABLED 1
# else
#  define AX_CXX_LONG_LONG_ENABLED 0
# endif
#endif

//
//	New Function Declaration Syntax For Deduced Return Types [N2541]
//
#ifndef AX_CXX_AUTO_FUNCTIONS_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,4, 12,1, 10,0, 2,9)
#  define AX_CXX_AUTO_FUNCTIONS_ENABLED 1
# else
#  define AX_CXX_AUTO_FUNCTIONS_ENABLED 0
# endif
#endif

//
//	nullptr [N2431]
//
#ifndef AX_CXX_NULLPTR_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,6, 12,1, 10,0, 2,9)
#  define AX_CXX_NULLPTR_ENABLED 1
# else
#  define AX_CXX_NULLPTR_ENABLED 0
# endif
#endif

//
//	R-Value References / std::move [N2118,N2844,N2844+,N3053]
//
#ifndef AX_CXX_RVALUE_REFS_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,3, 11,1, 10,0, 1,0)
#  define AX_CXX_RVALUE_REFS_ENABLED 1
# else
#  define AX_CXX_RVALUE_REFS_ENABLED 0
# endif
#endif

//
//	static_assert [N1720]
//
#ifndef AX_CXX_STATIC_ASSERT_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,3, 11,0, 10,0, 2,9)
#  define AX_CXX_STATIC_ASSERT_ENABLED 1
# else
#  define AX_CXX_STATIC_ASSERT_ENABLED 0
# endif
#endif

//
//	Strongly-typed Enums [N2347]
//
#ifndef AX_CXX_STRONG_ENUMS_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,4, 12,0, 11,0, 2,9)
#  define AX_CXX_STRONG_ENUMS_ENABLED 1
# else
#  define AX_CXX_STRONG_ENUMS_ENABLED 0
# endif
#endif

//
//	Template Aliases [N2258]
//
#ifndef AX_CXX_TEMPLATE_ALIASES_ENABLED
# if AX_CHECK_GCC_VERSION(4,4) || AX_CHECK_ICC_VERSION(12,1)\
|| AX_CHECK_CLANG_VERSION(3,0)
#  define AX_CXX_TEMPLATE_ALIASES_ENABLED 1
# else
#  define AX_CXX_TEMPLATE_ALIASES_ENABLED 0
# endif
#endif

//
//	Thread Local Storage (REAL) [N2659]
//
#ifndef AX_CXX_TLS_ENABLED
# if AX_CHECK_GCC_VERSION(4,8)
#  define AX_CXX_TLS_ENABLED 1
# else
#  define AX_CXX_TLS_ENABLED 0
# endif
#endif

//
//	Thread Local Storage (CRAP)
//
#ifndef AX_CXX_FAKE_TLS_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,4, 11,1, 10,0, 2,9)
#  define AX_CXX_FAKE_TLS_ENABLED 1
# else
#  define AX_CXX_FAKE_TLS_ENABLED 0
# endif
#endif

//
//	Built-in Type Traits [N1836]
//
#ifndef AX_CXX_TYPE_TRAITS_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,3, 10,0, 8,0, 3,0)
#  define AX_CXX_TYPE_TRAITS_ENABLED 1
# else
#  define AX_CXX_TYPE_TRAITS_ENABLED 0
# endif
#endif

//
//	Variadic Templates [N2242,N2555]
//
#ifndef AX_CXX_VARIADIC_TEMPLATES_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,3, 12,1, 11,0, 2,9)
#  define AX_CXX_VARIADIC_TEMPLATES_ENABLED 1
# else
#  define AX_CXX_VARIADIC_TEMPLATES_ENABLED 0
# endif
#endif

//
//	Range-based For-loops [N2930]
//
#ifndef AX_CXX_RANGE_FOR_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,6, 13,0, 11,0, 3,0)
#  define AX_CXX_RANGE_FOR_ENABLED 1
# else
#  define AX_CXX_RANGE_FOR_ENABLED 0
# endif
#endif

//
//	override and final [N2928,N3206,N3272]
//
#ifndef AX_CXX_OVERRIDE_FINAL_ENABLED
# if AX_CHECK_GCC_ICC_MSC_CLANG_VER(4,7, 12,0, 8,0, 2,9)
#  define AX_CXX_OVERRIDE_FINAL_ENABLED 1
# else
#  define AX_CXX_OVERRIDE_FINAL_ENABLED 0
# endif
#endif

//
//	Attributes [N2761]
//
#ifndef AX_CXX_ATTRIBUTES_ENABLED
# if AX_CHECK_GCC_VERSION(4,8) || AX_CHECK_ICC_VERSION(12,1)
#  define AX_CXX_ATTRIBUTES_ENABLED 1
# else
#  define AX_CXX_ATTRIBUTES_ENABLED 0
# endif
#endif

//
//	Ref-Qualifiers [N2439]
//
#ifndef AX_CXX_REF_QUALIFIERS_ENABLED
# if AX_CHECK_CLANG_VERSION(2,9)
#  define AX_CXX_REF_QUALIFIERS_ENABLED 1
# else
#  define AX_CXX_REF_QUALIFIERS_ENABLED 0
# endif
#endif

//
//	Non-static Data Member Initializers [N2756]
//
#ifndef AX_CXX_NONSTATIC_INIT_ENABLED
# if AX_CHECK_GCC_VERSION(4,7) || AX_CHECK_CLANG_VERSION(3,0)
#  define AX_CXX_NONSTATIC_INIT_ENABLED 1
# else
#  define AX_CXX_NONSTATIC_INIT_ENABLED 0
# endif
#endif


//
//	Nullptr
//
#ifndef AX_NULL
# if AX_CXX_NULLPTR_ENABLED
#  define AX_NULL nullptr
# else
#  define AX_NULL 0
# endif
#endif

//
//	= default, and = delete
//
#if AX_CXX_DEFAULT_DELETE_FUNCTIONS_ENABLED
# ifndef AX_DECL_DELETE
#  define AX_DECL_DELETE = delete
# endif
#else
# ifndef AX_DECL_DELETE
#  define AX_DECL_DELETE
# endif
#endif

//
//	Scoped Enumeration
//
#if AX_CXX_STRONG_ENUMS_ENABLED
# ifndef AX_ENUM_ENTER
#  define AX_ENUM_ENTER(x) enum class x {
# endif
# ifndef AX_ENUM_LEAVE
#  define AX_ENUM_LEAVE(x) };
# endif
#else
# ifndef AX_ENUM_ENTER
#  define ENUM_ENTER(n)\
	class n {\
	public:\
		enum _##n {
# endif
# ifndef AX_ENUM_LEAVE
#  define ENUM_LEAVE(n)\
		};\
	\
	protected:\
		_##n m_value;\
	\
	public:\
		inline n(_##n v=(_##n)0): m_value(v) {}\
		inline n(const n &x): m_value(x.m_value) {}\
		inline n &operator=(const n &x) { m_value = x.m_value; return *this; }\
		inline operator _##n() { return m_value; }\
	};
# endif
#endif

//
//	Static Assertions
//
#ifndef AX_STATIC_ASSERT_MSG
# if AX_CXX_STATIC_ASSERT_ENABLED
#  define AX_STATIC_ASSERT_MSG(x,m) static_assert((x),m)
# else
#  ifdef __COUNTER__
#   define AX_STATIC_ASSERT_MSG(x,m) enum { _Assert__##__COUNTER__ = 1/!!(x) }
#  else
#   define AX_STATIC_ASSERT_MSG(x,m) enum { _Assert__##__LINE__ = 1/!!(x) }
#  endif
# endif
#endif
#ifndef AX_STATIC_ASSERT
# define AX_STATIC_ASSERT(x) AX_STATIC_ASSERT_MSG((x),"Assert failed")
#endif


/*
===============================================================================

	Macros

===============================================================================
*/

//
//	Current Function (prettified)
//
#ifndef AX_CURRENT_FUNCTION
# if defined(__GNUC__) || __ICC >= 600
#  define AX_CURRENT_FUNCTION __PRETTY_FUNCTION__
# elif defined(__FUNCSIG__)
#  define AX_CURRENT_FUNCTION __FUNCSIG__
# elif __INTEL_COMPILER >= 600 || __IBMCPP__ >= 500 || __CWCC__ >= 0x4200
#  define AX_CURRENT_FUNCTION __FUNCTION__
# elif __BORLANDC__ >= 0x550
#  define AX_CURRENT_FUNCTION __FUNC__
# elif __STDC_VERSION__ >= 19901
#  define AX_CURRENT_FUNCTION __func__
# elif _MSC_VER >= 100 //TODO: double check this
#  define AX_CURRENT_FUNCTION __FUNCTION__
# else
#  define AX_CURRENT_FUNCTION AX_NULL
# endif
#endif

//
//	Miscellaneous Macros
//
#define AX_CONCAT__(x,y) x##y
#define AX_CONCAT(x,y) AX_CONCAT__(x,y)

#define AX_TOSTRING__(x) #x
#define AX_TOSTRING(x) AX_TOSTRING__(x)

#define AX_STR_CONCAT(x,y) AX_TOSTRING(x##y)

#ifdef __COUNTER__
# define AX_ANONVAR(prefix) AX_CONCAT(prefix,__COUNTER__)
#else
# define AX_ANONVAR(prefix) AX_CONCAT(prefix,__LINE__)
#endif

//
//	Branch Prediction Macros
//
#if defined(__GNUC__) || defined(__clang__)
# define AX_LIKELY(x) (__builtin_expect(!!(x), 1))
# define AX_UNLIKELY(x) (__builtin_expect(!!(x), 0))
#else
# define AX_LIKELY(x) (x)
# define AX_UNLIKELY(x) (x)
#endif

//
//	Forced Inlining
//
#ifndef AX_INLINE
# if _MSC_VER || __INTEL_COMPILER
#  define AX_INLINE __forceinline
# elif __GNUC__ || __clang__
#  define AX_INLINE inline __attribute__((always_inline))
# else
#  define AX_INLINE inline
# endif
#endif

//
//	Uncopyable Class
//
#ifndef AX_UNCOPYABLE
# define AX_UNCOPYABLE(T)\
	private: T(const T &) AX_DECL_DELETE;\
	private: T &operator=(const T &) AX_DECL_DELETE
#endif

//
//	Format printf Strings
//
#ifndef AX_PRINTF_PARM
# if _MSC_VER >= 1600
#  include <CodeAnalysis/SourceAnnotations.h>
#  define AX_PRINTF_PARM [SA_FormatString(Style="printf")]
# else
#  define AX_PRINTF_PARM
# endif
#endif

#ifndef AX_PRINTF_ATTR
# if __GNUC__ || __clang__ || __INTEL_COMPILER
#  define AX_PRINTF_ATTR(x,y) __attribute__((format(printf,x,y)))
# else
#  define AX_PRINTF_ATTR(x,y)
# endif
#endif

/*
===============================================================================

	Base Types

===============================================================================
*/
#if _MSC_VER
typedef   signed __int8			 s8_t;
typedef   signed __int16		s16_t;
typedef   signed __int32		s32_t;
typedef   signed __int64		s64_t;
typedef unsigned __int8			 u8_t;
typedef unsigned __int16		u16_t;
typedef unsigned __int32		u32_t;
typedef unsigned __int64		u64_t;
#else
# include <stdint.h>
typedef   signed char			 s8_t;
typedef   signed short			s16_t;
typedef   signed int			s32_t;
typedef   signed long long int	s64_t;
typedef unsigned char			 u8_t;
typedef unsigned short			u16_t;
typedef unsigned int			u32_t;
typedef unsigned long long int	u64_t;
#endif
#if AX_CXX_DECLTYPE_ENABLED && AX_CXX_NULLPTR_ENABLED
typedef decltype(nullptr)		nullptr_t;
#else
typedef void *					nullptr_t;
#endif

template<int _Size> struct __ptr_size {};
template<> struct __ptr_size<2> { typedef s16_t sint_t; typedef u16_t uint_t; };
template<> struct __ptr_size<4> { typedef s32_t sint_t; typedef u32_t uint_t; };
template<> struct __ptr_size<8> { typedef s64_t sint_t; typedef u64_t uint_t; };

typedef __ptr_size<sizeof(void *)>::sint_t sint_t;
typedef __ptr_size<sizeof(void *)>::uint_t uint_t;

#define __AX_ASSERT_SIZE(t,n)\
	AX_STATIC_ASSERT_MSG(sizeof(t##n##_t)==n/8,#t #n "_t is not " #n "-bit!")
__AX_ASSERT_SIZE(s, 8);		__AX_ASSERT_SIZE(u, 8);
__AX_ASSERT_SIZE(s,16);		__AX_ASSERT_SIZE(u,16);
__AX_ASSERT_SIZE(s,32);		__AX_ASSERT_SIZE(u,32);
__AX_ASSERT_SIZE(s,64);		__AX_ASSERT_SIZE(u,64);
#undef __AX_ASSERT_SIZE
AX_STATIC_ASSERT_MSG(sizeof(sint_t)==sizeof(void *), "Not pointer-sized!");
AX_STATIC_ASSERT_MSG(sizeof(uint_t)==sizeof(void *), "Not pointer-sized!");


/*
===============================================================================

	Asserts

===============================================================================
*/

//
//	Assert Macros
//	AX_ASSERT_ENABLED is whether AX_ASSERT and AX_ASSERT_MSG is called
//	AX_ASSERT and AX_ASSERT_MSG can be defined by you, or you can let it be
//	defined separately.
//
#ifndef AX_ASSERT_ENABLED
# if AX_DEBUG_OR_TEST_ENABLED
#  define AX_ASSERT_ENABLED 1
# else
#  define AX_ASSERT_ENABLED 0
# endif
#endif

#ifndef AX_ASSERT_MSG
# if AX_ASSERT_ENABLED
#  define AX_ASSERT_MSG(x,m)\
	if AX_UNLIKELY(!(x))\
		_Sys_Assert(__FILE__,__LINE__,AX_CURRENT_FUNCTION,#x,(m))
# else
#  define AX_ASSERT_MSG(x,m) (void)0
# endif
#endif

#ifndef AX_ASSERT
# if AX_ASSERT_ENABLED
#  define AX_ASSERT(x) AX_ASSERT_MSG((x),"Assert failed")
# else
#  define AX_ASSERT(x) (void)0
# endif
#endif

//
//	Constant Assert Macros
//	These aren't configurable; they will call AX_ASSERT*, or AX_STATIC_ASSERT
//
//	AX_ASSERT_ALIGN[number](ptr) will check to be sure ptr is aligned to the
//	boundary specified by number, in bytes.
//
//	AX_ASSERT_ALIGNED(p,n) is like AX_ASSERT_ALIGN[number](ptr) but supports
//	arbitrary alignments.
//
//	AX_ASSERT_ALIGNED_TYPE(p,t) will check to be sure the pointer 'p' is aligned
//	to the size of the type 't'.
//
#define AX_ASSERT_ALIGNED(p,n)		AX_ASSERT((uint_t)(p)%(n)==0)
#define AX_ASSERT_ALIGN2(ptr)		AX_ASSERT_ALIGNED((ptr),2)
#define AX_ASSERT_ALIGN4(ptr)		AX_ASSERT_ALIGNED((ptr),4)
#define AX_ASSERT_ALIGN8(ptr)		AX_ASSERT_ALIGNED((ptr),8)
#define AX_ASSERT_ALIGN16(ptr)		AX_ASSERT_ALIGNED((ptr),16)
#define AX_ASSERT_ALIGN32(ptr)		AX_ASSERT_ALIGNED((ptr),32)
#define AX_ASSERT_ALIGN64(ptr)		AX_ASSERT_ALIGNED((ptr),64)
#define AX_ASSERT_ALIGN128(ptr)		AX_ASSERT_ALIGNED((ptr),128)
#define AX_ASSERT_ALIGN256(ptr)		AX_ASSERT_ALIGNED((ptr),256)
#define AX_ASSERT_ALIGN512(ptr)		AX_ASSERT_ALIGNED((ptr),512)
#define AX_ASSERT_ALIGNED_TYPE(p,t)	AX_ASSERT_ALIGNED((p),sizeof(t))

#define AX_ASSERT_SIZEOF(t,n)		AX_STATIC_ASSERT(sizeof(t)==(n))
#define AX_ASSERT_SIZEOF_ALIGNED(t)	AX_STATIC_ASSERT(sizeof(t)%(n)==0)
#define AX_ASSERT_SIZEOF_ALIGN8(t)	AX_STATIC_ASSERT_SIZEOF_ALIGNED((t),8)
#define AX_ASSERT_SIZEOF_ALIGN16(t)	AX_STATIC_ASSERT_SIZEOF_ALIGNED((t),16)
#define AX_ASSERT_SIZEOF_ALIGN32(t)	AX_STATIC_ASSERT_SIZEOF_ALIGNED((t),32)

//
//	Assert functions
//
typedef void(*assertFunc_t)(const char *file, int line, const char *func,
	const char *expr, const char *msg);

static assertFunc_t g_assertFunc = AX_NULL;

static void _Sys_Catf(char *buf, size_t len, const char *fmt, ...) {
	va_list args;
	char *p;

	p = strchr(buf, '\0');

	len -= p - buf;

	va_start(args, fmt);
#if __STDC_WANT_SECURE_LIB__
	vsprintf_s(p, len, fmt, args);
#else
	vsnprintf(p, len, fmt, args);
#endif
	va_end(args);
}

/*
================
_Sys_Assert

Handle an assert triggered by AX_ASSERT_MSG or AX_ASSERT

DO NOT CALL DIRECTLY
================
*/
void _Sys_Assert(const char *file, int line, const char *func, const char *expr,
const char *msg) {
	size_t len;
	char buf[4096];

	if( g_assertFunc ) {
		g_assertFunc(file, line, func, expr, msg);
		return;
	}

	buf[0] = '\0';
	_Sys_Catf(buf,sizeof(buf), "\n[[ +RUNTIME ERROR+ ]]\n\n");

	if( file ) {
		_Sys_Catf(buf,sizeof(buf), "<< %s%s", file, line ? "" : "\n");
		if( line ) {
			_Sys_Catf(buf,sizeof(buf), "(%i)\n", line);
		}
	}

	if( func ) {
		_Sys_Catf(buf,sizeof(buf), "{} %s\n", func);
	}

	_Sys_Catf(buf,sizeof(buf), ">> %s\n// %s\n\n", msg, expr);
	_Sys_Catf(buf,sizeof(buf), "[[ -RUNTIME ERROR- ]]\n\n");

	len = strlen(buf);

	fwrite(buf, len, 1, stderr);

#if _WIN32
	if( OpenClipboard( AX_NULL ) ) {
		HGLOBAL mem = GlobalAlloc( GMEM_MOVEABLE, len + 1 );
		if( mem ) {
			void *ptr = GlobalLock( mem );
			if( ptr ) {
				memcpy( ptr, buf, len + 1 );
			}
			GlobalUnlock( mem );

			EmptyClipboard();
			SetClipboardData( CF_TEXT, mem );
		}

		CloseClipboard();
	}

	if( IsDebuggerPresent() ) {
		DebugBreak();
		return;
	}
#endif

	exit(EXIT_FAILURE);
}

/*
================
Sys_SetAssertHandler

Set the current assert handler; allows more customization for asserts
================
*/
void Sys_SetAssertHandler(assertFunc_t func) {
	g_assertFunc = func;
}
/*
================
Sys_GetAssertHandler

Retrieve the current assert handler
================
*/
assertFunc_t Sys_GetAssertHandler() {
	return g_assertFunc;
}


/*
===============================================================================

	Exceptions

===============================================================================
*/
static const uint_t MAX_EXCEPTION_ERROR = 4096;

class Exception {
public:
					Exception(const char *error) {
						uint_t i;

						for(i=0; i<sizeof(errorText) - 1; i++) {
							if( *error=='\0' )
								break;

							errorText[i] = *error++;
						}

						errorText[i] = '\0';
					}
	virtual			~Exception() {}

	const char *	What() const { return errorText; }

protected:
	static char		errorText[ MAX_EXCEPTION_ERROR ];

protected:
	friend class Exception_file;
					Exception() {}
};

class Exception_file: public virtual Exception {
public:
					Exception_file(const char *file, int line, const char *msg):
					Exception(msg), file(file), line(line) {
					}
					Exception_file(const Exception_file &e): Exception(),
					file(e.file), line(e.line) {
					}
	virtual			~Exception_file() {}

	const char *	File() const { return file; }
	int				Line() const { return line; }

	Exception_file &operator=(const Exception_file &e) {
						file = e.file;
						line = e.line;
						return *this;
					}

protected:
	const char *	file;
	int				line;
};

char				Exception::errorText[ MAX_EXCEPTION_ERROR ];

AX_INLINE void RuntimeError(const char *error) {
	throw Exception( error );
}
AX_INLINE void RuntimeError(const char *file, int line, const char *msg) {
	throw Exception_file( file,line, msg );
}
#define RuntimeError_file( msg ) RuntimeError( __FILE__,__LINE__, (msg) )


/*
===============================================================================

	Pointer Arithmetic
	Add or subtract n-bytes to or from a pointer

===============================================================================
*/
template<typename T>
static inline T *AddPtr(T *p, uint_t n) {
	union { T *p; uint_t n; } x;

	x.p  = p;
	x.n += n;

	return x.p;
}
template<typename T>
static inline T *SubPtr(T *p, uint_t n) {
	union { T *p; uint_t n; } x;

	x.p  = p;
	x.n -= n;

	return x.p;
}

template<typename T>
static inline T *AlignPtr(T *p, uint_t alignment, uint_t offset=0,
uint_t padding=0) {
	union { T *p; uint_t n; } x;

	x.p  = p;
	x.n += alignment + padding + offset;
	x.n &= ~(alignment - 1);
	x.n -= offset;

	return x.p;
}


/*
===============================================================================

	Memory Management
	Provides system-level memory access and usage statistics

===============================================================================
*/
enum __memTag_t {
	TAG_INVALID			= -1,

#define MEMTAG( name, text ) name,
	MEMORY_TAGS_LIST()
#undef MEMTAG

	__TAG_USER__
};

class Mem {
public:
	typedef int			tag_t;
	static const int	MAX_TAGS = 256;

	struct tagInfo_t {
		const char *	name;

		uint_t			numAllocs;
		uint_t			numDeallocs;

		uint_t			totalAlloced;
		uint_t			totalDealloced;

		uint_t			numAllocsFrame;
		uint_t			numDeallocsFrame;

		uint_t			totalAllocedFrame;
		uint_t			totalDeallocedFrame;
	};

	static void			Init();
	static void			Fini();

	static void *		Alloc(uint_t n, tag_t tag);
	static nullptr_t	Dealloc(void *p);

	static void *		AllocDbg(uint_t n, tag_t tag, const char *file,
							int line);
	static nullptr_t	DeallocDbg(void *p);

	static uint_t		NumTags();

	static void			SetTag(tag_t tag, const char *name);
	static tag_t		AddTag(const char *name);
	static tagInfo_t	GetTag(tag_t tag);

	static void			NotifyFrame();

	static tagInfo_t	GetTotal();

protected:
	struct memRelStub_t {
		tag_t			mtag;
		uint_t			size;
	};
	struct memDbgStub_t {
		void *			self;
		const char *	file;
		int				line;
		memDbgStub_t *	prev;
		memDbgStub_t *	next;
		tag_t			mtag;
		uint_t			size;
	};

	static tagInfo_t	tags[ MAX_TAGS ];
	static uint_t		numTags;
	static memDbgStub_t *dbgStubHead;

	static void			TagAlloc(tag_t tag, uint_t n);
	static void			TagDealloc(tag_t tag, uint_t n);
};

Mem::tagInfo_t			Mem::tags[ Mem::MAX_TAGS ];
uint_t					Mem::numTags = 0;
Mem::memDbgStub_t *		Mem::dbgStubHead = AX_NULL;

#ifdef _DEBUG
# define Mem_Alloc(n, tag)\
	Mem::AllocDbg(n, tag, __FILE__,__LINE__)
# define Mem_Dealloc(p)\
	Mem::DeallocDbg(p)
#else
# define Mem_Alloc(n, tag)\
	Mem::Alloc(n, tag)
# define Mem_Dealloc(p)\
	Mem::Dealloc(p)
#endif

/*
================
Mem::Init

Initialize the memory subsystem
================
*/
void Mem::Init() {
	tag_t tag;

	if( !!numTags ) {
		return;
	}

	numTags		= 0;
	dbgStubHead	= AX_NULL;

#define MEMTAG( name,text ) tag = AddTag( text );AX_ASSERT( tag==name );
	MEMORY_TAGS_LIST()(void)tag;
#undef MEMTAG

	atexit( &Mem::Fini );
}

/*
================
Mem::Fini

Finish using the memory subsystem (registered for atexit by Mem::Init)
================
*/
void Mem::Fini() {
	memDbgStub_t *stub, *next;
	uint_t num;

	num = 0;
	for(stub=dbgStubHead; stub; stub=next) {
		next = stub->next;
		num++;

		fprintf(stderr, "MEMLEAK: ");
		if( stub->file ) {
			fprintf(stderr, "%s(%i): ", stub->file,stub->line);
		}
		fprintf(stderr, "from %s(%i): %u byte%s\n",
			(uint_t)stub->mtag < numTags ? tags[stub->mtag].name : "(invalid)",
			stub->mtag, (unsigned int)stub->size, stub->size!=1 ? "s" : "");

		if( stub->prev ) {
			stub->prev->next = AX_NULL;
		}
		if( stub->next ) {
			stub->next->prev = AX_NULL;
		}

		stub->prev = AX_NULL;
		stub->next = AX_NULL;

		free( (void *)stub );
	}

	if( num > 0 ) {
		fprintf(stderr, "\n ** %u total memory leak%s **\n",
			(unsigned int)num, num!=1 ? "s" : "");
	}
	fflush(stderr);

	numTags = 0;
	dbgStubHead = AX_NULL;
}

/*
================
Mem::Alloc

Allocate n-bytes of memory if n > 0; else return NULL
The memory MUST be free'd with Mem::Dealloc

NOTE: Call Mem_Alloc, not this
================
*/
void *Mem::Alloc(uint_t n, tag_t tag) {
	memRelStub_t *stub;

	if( !n ) {
		return AX_NULL;
	}

	stub = (memRelStub_t *)(new char[ n + sizeof(*stub) ]);
	if( !stub ) {
		RuntimeError_file( "Bad alloc" );
		return AX_NULL;
	}

	TagAlloc( tag, n );

	stub->mtag = tag;
	stub->size = n;

	return AddPtr( stub, sizeof(*stub) );
}

/*
================
Mem::Dealloc

Deallocates memory previously allocated with Mem::Alloc()

NOTE: Call Mem_Dealloc, not this
================
*/
nullptr_t Mem::Dealloc(void *p) {
	memRelStub_t *stub;

	if( !p ) {
		return AX_NULL;
	}

	stub = (memRelStub_t *)SubPtr( p, sizeof(*stub) );
	TagDealloc( stub->mtag, stub->size );

	delete [] (char *)p;
	return AX_NULL;
}

/*
================
Mem::AllocDbg

Debug-mode memory allocation; includes file and line of alloc

NOTE: Call Mem_Alloc, not this
================
*/
void *Mem::AllocDbg(uint_t n, tag_t tag, const char *file, int line) {
	memDbgStub_t *stub;
	uint_t i;
	u32_t *p;

	if( !n ) {
		return AX_NULL;
	}

	stub = (memDbgStub_t *)(new char[ n + sizeof(*stub) ]);
	if( !stub ) {
		RuntimeError( file,line, "Bad alloc" );
		return AX_NULL;
	}

	TagAlloc( tag, n );

	stub->self	= (void *)stub;
	stub->file	= file;
	stub->line	= line;
	stub->prev	= AX_NULL;
	stub->next	= dbgStubHead;
	stub->mtag	= tag;
	stub->size	= n;

	if( dbgStubHead ) {
		dbgStubHead->prev = stub;
	}

	p = (u32_t *)AddPtr( stub, sizeof(*stub) );
	for(i=0; (i + 1)*sizeof(u32_t)<=stub->size; i++) {
		p[ i ] = 0x1BADC0DE;
	}

	if( (i + 1)*sizeof(u32_t)!=stub->size ) {
		memset( &p[ i ], 0xCD, stub->size - i*sizeof(u32_t) );
	}

	return p;
}
/*
================
Mem::DeallocDbg

Debug-mode memory deallocation; does extensive checking

NOTE: Call Mem_Dealloc, not this
================
*/
nullptr_t Mem::DeallocDbg(void *p) {
	memDbgStub_t *stub;

	if( !p ) {
		return AX_NULL;
	}

	AX_ASSERT((uint_t)((char *)p - sizeof(*stub)) < (uint_t)p);

	stub = (memDbgStub_t *)SubPtr( p, sizeof(*stub) );
	AX_ASSERT(stub->self==(void *)stub);

	if( stub->prev ) {
		stub->prev->next = stub->next;
	} else {
		dbgStubHead = stub->next;
	}

	if( stub->next ) {
		stub->next->prev = stub->prev;
	}

	TagDealloc( stub->mtag, stub->size );

	delete [] (char *)stub;
	return AX_NULL;
}

/*
================
Mem::NumTags

Retrieve the number of valid tags
================
*/
uint_t Mem::NumTags() {
	return numTags;
}

/*
================
Mem::SetTag

Set the name for a memory tag
================
*/
void Mem::SetTag(tag_t tag, const char *name) {
	AX_ASSERT(tag>TAG_NONE && tag<(tag_t)numTags);

	tags[tag].name = name;
}
/*
================
Mem::AddTag

Add an extra memory tag, returning the tag on success or TAG_INVALID on failure
================
*/
Mem::tag_t Mem::AddTag(const char *name) {
	tag_t tag;

	if( numTags == MAX_TAGS ) {
		return TAG_INVALID;
	}

	tag = numTags++;

	tags[tag].name					= name;

	tags[tag].numAllocs				= 0;
	tags[tag].numDeallocs			= 0;

	tags[tag].totalAlloced			= 0;
	tags[tag].totalDealloced		= 0;

	tags[tag].numAllocsFrame		= 0;
	tags[tag].numDeallocsFrame		= 0;

	tags[tag].totalAllocedFrame		= 0;
	tags[tag].totalDeallocedFrame	= 0;

	return tag;
}
/*
================
Mem::GetTag

Retrieve information about a particular tag
================
*/
Mem::tagInfo_t Mem::GetTag(tag_t tag) {
	AX_ASSERT(tag>TAG_NONE && tag<MAX_TAGS);

	return tags[tag];
}

/*
================
Mem::NotifyFrame

Reset statistics for a new frame (call at beginning of frame!)
================
*/
void Mem::NotifyFrame() {
	for(uint_t i=0; i<numTags; i++) {
		tags[i].numAllocsFrame		= 0;
		tags[i].numDeallocsFrame	= 0;

		tags[i].totalAllocedFrame	= 0;
		tags[i].totalDeallocedFrame	= 0;
	}
}

/*
================
Mem::GetTotal

Retrieve the total statistics so far (including frame statistics)
NOTE: Although this returns a tagInfo_t, there is no real corresponding tag for
this
================
*/
Mem::tagInfo_t Mem::GetTotal() {
	tagInfo_t total;

	total.name						= "Total";

	total.numAllocs					= 0;
	total.numDeallocs				= 0;

	total.totalAlloced				= 0;
	total.totalDealloced			= 0;

	total.numAllocsFrame			= 0;
	total.numDeallocsFrame			= 0;

	total.totalAllocedFrame			= 0;
	total.totalDeallocedFrame		= 0;

	for(uint_t i=0; i<numTags; i++) {
		total.numAllocs				+= tags[i].numAllocs;
		total.numDeallocs			+= tags[i].numDeallocs;

		total.totalAlloced			+= tags[i].totalAlloced;
		total.totalDealloced		+= tags[i].totalDealloced;

		total.numAllocsFrame		+= tags[i].numAllocsFrame;
		total.numDeallocsFrame		+= tags[i].numDeallocsFrame;

		total.totalAllocedFrame		+= tags[i].totalAllocedFrame;
		total.totalDeallocedFrame	+= tags[i].totalDeallocedFrame;
	}

	return total;
}

/*
================
Mem::TagAlloc

Tag an allocation
================
*/
void Mem::TagAlloc(tag_t tag, uint_t n) {
	AX_ASSERT(tag>TAG_NONE && tag<(tag_t)numTags);
	AX_ASSERT(!!n);

	tags[tag].numAllocs++;
	tags[tag].numAllocsFrame++;

	tags[tag].totalAlloced += n;
	tags[tag].totalAllocedFrame += n;
}

/*
================
Mem::TagDealloc

Tag a deallocation
================
*/
void Mem::TagDealloc(tag_t tag, uint_t n) {
	AX_ASSERT(tag>TAG_NONE && tag<(tag_t)numTags);
	AX_ASSERT(!!n);

	tags[tag].numDeallocs++;
	tags[tag].numDeallocsFrame++;

	tags[tag].totalDealloced += n;
	tags[tag].totalDeallocedFrame += n;
}


/*
===============================================================================

	Callback System
	These define objects which when 'Call()'d will defer execution to the
	function pointer they store, passing along extra parameters along the way.

	Normally you would need several overloaded functions for everything that
	wants to call a function pointer. With this you can just pass a class along
	and don't have to worry about casting.

	This system is pretty easy to use:

		void f() { printf("Hello, world!\n"); }
		int main() { MakeCallback(f).Call(); return EXIT_SUCCESS; }

	And for objects:

		class A {
		public:
			void F() { printf("Hello!\n"); }
		};

		int main() { A x; MakeCallback(&x, x::F).Call(); return EXIT_SUCCESS; }

===============================================================================
*/
class Callback {
public:
	virtual			~Callback() {}
	virtual void	Call() = 0;
};

template<typename _ArgT=void>
class Callback_static: public virtual Callback {
public:
	typedef void(*fn_t)(_ArgT p);

					Callback_static(fn_t fn, _ArgT p): func(fn), parm(p) {
						AX_ASSERT(!!fn);
					}
	virtual			~Callback_static() {}

	virtual void	Call() { func(parm); }

protected:
	fn_t			func;
	_ArgT			parm;
};

template<> class Callback_static<void>: public virtual Callback {
public:
	typedef void(*fn_t)();

					Callback_static(fn_t fn): func(fn) { AX_ASSERT(!!fn); }
	virtual			~Callback_static() {}

	virtual void	Call() { func(); }

protected:
	fn_t			func;
};

template<typename T, typename _ArgT>
class Callback_object: public virtual Callback {
public:
	typedef void(T::*fn_t)(_ArgT p);

					Callback_object(T *t, fn_t fn, _ArgT p): type(t),
					func(fn), parm(p) {
						AX_ASSERT(!!t);
						AX_ASSERT(!!fn);
					}
	virtual			~Callback_object() {}

	virtual void	Call() { (type->*func)(parm); }

protected:
	T *				type;
	fn_t			func;
	_ArgT			parm;
};
template<typename T> class Callback_object<T, void>: public virtual Callback {
public:
	typedef void(T::*fn_t)();

					Callback_object(T *t, fn_t fn): type(t), func(fn) {
						AX_ASSERT(!!t);
						AX_ASSERT(!!fn);
					}
	virtual			~Callback_object() {}

	virtual void	Call() { (type->*func)(); }

protected:
	T *				type;
	fn_t			func;
};

/*
================
MakeCallback

Create a callback object from a function/method pointer
================
*/
template<typename _ArgT>
Callback_static<_ArgT> MakeCallback(typename Callback_static<_ArgT>::fn_t fn,
_ArgT parm) {
	return Callback_static<_ArgT>(fn, parm);
}
Callback_static<void> MakeCallback(Callback_static<void>::fn_t fn) {
	return Callback_static<void>(fn);
}

template<typename T, typename _ArgT>
Callback_object<T,_ArgT> MakeCallback(T *t,
typename Callback_object<T,_ArgT>::fn_t fn, _ArgT parm) {
	return Callback_object<T,_ArgT>(t, fn, parm);
}
template<typename T>
Callback_object<T,void> MakeCallback(T *t,
typename Callback_object<T,void>::fn_t fn) {
	return Callback_object<T,void>(t, fn);
}


/*
===============================================================================

	Array Class
	Stores a list of items effectively

===============================================================================
*/
template<typename _type_, Mem::tag_t _tag_ = TAG_AXLIB_ARR>
class Arr {
public:
					Arr(uint_t granularity=16);
					Arr(const Arr &arr);
					Arr(Arr &&arr);
					~Arr();

	_type_ *		Ptr();
	const _type_ *	Ptr() const;

	uint_t			Len() const;
	uint_t			Cap() const;

	bool			Reserve(uint_t num);
	void			Condense();

	void			DeleteContents();
	void			Clear();
	void			Purge();

	bool			Append(const _type_ &obj);
	bool			Append(const Arr &arr);

	_type_ *		Get(uint_t index);
	const _type_ *	Get(uint_t index) const;

	Arr &			operator=(const Arr &arr);

	_type_ &		operator[](uint_t index);
	const _type_ &	operator[](uint_t index) const;

protected:
	_type_ *		data;
	uint_t			size;
	uint_t			capacity;
	uint_t			granularity;
};

/*
================
Arr<_type_,_tag_>::Arr( uint_t )

Construct the array with a default granularity
================
*/
template<typename _type_, Mem::tag_t _tag_>
Arr<_type_,_tag_>::Arr(uint_t granularity): data(AX_NULL),size(0),capacity(0),
granularity(granularity ? granularity : 1) {
}
/*
================
Arr<_type_,_tag_>::Arr( const Arr & )

Construct the array from another array
================
*/
template<typename _type_, Mem::tag_t _tag_>
Arr<_type_,_tag_>::Arr(const Arr &arr): data(AX_NULL),size(0),capacity(0),
granularity(arr.granularity) {
	*this = arr;
}
/*
================
Arr<_type_,_tag_>::Arr( Arr && )

Swap-construct the array from an r-value array
================
*/
template<typename _type_, Mem::tag_t _tag_>
Arr<_type_,_tag_>::Arr(Arr &&arr): data(arr.data),size(arr.size),
capacity(arr.capacity), granularity(arr.granularity) {
	arr.data		= AX_NULL;
	arr.size		= 0;
	arr.capacity	= 0;
	arr.granularity	= 0;
}
/*
================
Arr<_type_,_tag_>::~Arr

Destruct the array
================
*/
template<typename _type_, Mem::tag_t _tag_>
Arr<_type_,_tag_>::~Arr() {
	Purge();
}

/*
================
Arr<_type_,_tag_>::Ptr

Retrieve a pointer to the array's data
================
*/
template<typename _type_, Mem::tag_t _tag_>
_type_ *Arr<_type_,_tag_>::Ptr() {
	return data;
}
template<typename _type_, Mem::tag_t _tag_>
const _type_ *Arr<_type_,_tag_>::Ptr() const {
	return data;
}

/*
================
Arr<_type_,_tag_>::Len

Retrieve the number of elements in the array
================
*/
template<typename _type_, Mem::tag_t _tag_>
uint_t Arr<_type_,_tag_>::Len() const {
	return size;
}
/*
================
Arr<_type_,_tag_>::Cap

Retrieve the number of allocated (used or otherwise) elements in the array
================
*/
template<typename _type_, Mem::tag_t _tag_>
uint_t Arr<_type_,_tag_>::Cap() const {
	return capacity;
}

/*
================
Arr<_type_,_tag_>::Reserve

Ensure there are at least 'num' elements allocated (not initialized) in the
array
================
*/
template<typename _type_, Mem::tag_t _tag_>
bool Arr<_type_,_tag_>::Reserve(uint_t num) {
	if( num < capacity ) {
		return true;
	}

	if( num%granularity != 0 )
		num += num%granularity;

	_type_ *p = (_type_ *)Mem_Alloc( sizeof(_type_)*num, _tag_ );
	if( !p ) {
		return false;
	}

	for(uint_t i=0; i<size; i++) {
		p[ i ] = data[ i ];
	}

	Mem_Dealloc( (void *)data );
	data = p;

	capacity = num;
	return true;
}
/*
================
Arr<_type_,_tag_>::Condense

Fit the array's allocated elements to the used elements (downsize)
================
*/
template<typename _type_, Mem::tag_t _tag_>
void Arr<_type_,_tag_>::Condense() {
	if( size==capacity ) {
		return;
	}

	if( !size ) {
		Purge();
		return;
	}

	_type_ *p = (_type_ *)Mem_Alloc( sizeof(_type_)*size, _tag_ );
	if( !p ) {
		return;
	}

	for(uint_t i=0; i<size; i++) {
		p[ i ] = data[ i ];
	}

	Mem_Dealloc( (void *)data );
	data = p;

	capacity = size;
}

/*
================
Arr<_type_,_tag_>::DeleteContents

Delete all pointers in the array
WARNING: Do not call unless you are storing POINTERS in the array; this will not
         compile otherwise.
================
*/
template<typename _type_, Mem::tag_t _tag_>
void Arr<_type_,_tag_>::DeleteContents() {
	for(uint_t i=0; i<size; i++) {
		delete data[ i ];
		data[ i ] = AX_NULL;
	}
}
/*
================
Arr<_type_,_tag_>::Clear

Destruct all used elements within the array and reset the count
================
*/
template<typename _type_, Mem::tag_t _tag_>
void Arr<_type_,_tag_>::Clear() {
	for(uint_t i=0; i<size; i++) {
		data[ i ].~_type_();
	}

	size = 0;
}
/*
================
Arr<_type_,_tag_>::Purge

Completely free all allocated memory within the array, destructing the used
items
================
*/
template<typename _type_, Mem::tag_t _tag_>
void Arr<_type_,_tag_>::Purge() {
	Clear();

	data = (_type_ *)Mem_Dealloc( (void *)data );
	capacity = 0;
}

/*
================
Arr<_type_,_tag_>::Append( const _type_ & )

Append a single object to the array, returning false if there was an error
================
*/
template<typename _type_, Mem::tag_t _tag_>
bool Arr<_type_,_tag_>::Append(const _type_ &obj) {
	if( !Reserve( size + 1 ) ) {
		return false;
	}

	data[ size++ ] = obj;
	return true;
}
/*
================
Arr<_type_,_tag_>::Append( const Arr & )

Append another array to this array, returning false if there was an error
================
*/
template<typename _type_, Mem::tag_t _tag_>
bool Arr<_type_,_tag_>::Append(const Arr &arr) {
	if( !arr.size ) {
		return true;
	}

	if( !Reserve( size + arr.size ) ) {
		return false;
	}

	for(uint_t i=0; i<arr.size; i++) {
		data[ size++ ] = arr.data[ i ];
	}

	return true;
}
/*
================
Arr<_type_,_tag_>::Get

Retrieve a pointer to an element within the array
================
*/
template<typename _type_, Mem::tag_t _tag_>
_type_ *Arr<_type_,_tag_>::Get(uint_t index) {
	if( index >= size ) {
		return AX_NULL;
	}

	return &data[ index ];
}
template<typename _type_, Mem::tag_t _tag_>
const _type_ *Arr<_type_,_tag_>::Get(uint_t index) const {
	if( index >= size ) {
		return AX_NULL;
	}

	return &data[ index ];
}

/*
================
Arr<_type_,_tag_>::operator=

Assign another array to this one
================
*/
template<typename _type_, Mem::tag_t _tag_>
Arr<_type_,_tag_> &Arr<_type_,_tag_>::operator=(const Arr &arr) {
	if( !Reserve( arr.size ) ) {
		return *this;
	}

	uint_t i;
	for(i=0; i<arr.size; i++) {
		data[ i ] = arr.data[ i ];
	}

	while( i<size ) {
		data[ i++ ].~_type();
	}
	size = arr.size;

	return *this;
}

/*
================
Arr<_type_,_tag_>::operator[]

Access this array like a native array
================
*/
template<typename _type_, Mem::tag_t _tag_>
_type_ &Arr<_type_,_tag_>::operator[](uint_t index) {
	return *Get( index );
}
template<typename _type_, Mem::tag_t _tag_>
const _type_ &Arr<_type_,_tag_>::operator[](uint_t index) const {
	return *Get( index );
}

/*
===============================================================================

	Circular Linked List

===============================================================================
*/
template<class _type_>
class List {
public:
	typedef List<_type_> link_t;

					List(_type_ *owner=AX_NULL);
					~List();

	_type_ *		Owner();
	const _type_ *	Owner() const;
	void			SetOwner(_type_ *newOwner);

	void			AddHead(link_t &link);
	void			AddTail(link_t &link);
	void			InsertBefore(link_t &link);
	void			InsertAfter(link_t &link);

	uint_t			Len() const;

	bool			Empty() const;
	bool			IsList() const;
	bool			IsLink() const;

	void			Clear();
	void			Remove();

	link_t *		HeadLink();
	const link_t *	HeadLink() const;
	link_t *		PrevLink();
	const link_t *	PrevLink() const;
	link_t *		NextLink();
	const link_t *	NextLink() const;

	_type_ *		Prev();
	const _type_ *	Prev() const;
	_type_ *		Next();
	const _type_ *	Next() const;

protected:
	List *			head;
	List *			prev;
	List *			next;

	_type_ *		owner;
};

/*
================
List<_type_>::List

Construct the list or link
================
*/
template<class _type_>
List<_type_>::List(_type_ *owner): head(this), prev(this), next(this),
owner(owner) {
}
/*
================
List<_type_>::~List

Destruct the list (remove all items from it) or link (remove self from list)
================
*/
template<class _type_>
List<_type_>::~List() {
	Clear();
}

/*
================
List<_type_>::Owner

Retrieve the current owner of the list
================
*/
template<class _type_>
_type_ *List<_type_>::Owner() {
	return owner;
}
template<class _type_>
const _type_ *List<_type_>::Owner() const {
	return owner;
}
/*
================
List<_type_>::SetOwner

Set the current owner of the list
================
*/
template<class _type_>
void List<_type_>::SetOwner(_type_ *newOwner) {
	owner = newOwner;
}

/*
================
List<_type_>::AddHead

Add a link to the beginning of the list (so that link is first)
================
*/
template<class _type_>
void List<_type_>::AddHead(link_t &link) {
	link.InsertAfter( *head );
}
/*
================
List<_type_>::AddTail

Add a link to the end of the list (so that link is last)
================
*/
template<class _type_>
void List<_type_>::AddTail(link_t &link) {
	link.InsertBefore( *head );
}
/*
================
List<_type_>::InsertBefore

Insert this link before 'link'
================
*/
template<class _type_>
void List<_type_>::InsertBefore(link_t &link) {
	Remove();

	prev = link.prev;
	next = &link;

	link.prev->next = this;
	link.prev = this;

	head = link.head;
}
/*
================
List<_type_>::InsertAfter

Insert this link after 'link'
================
*/
template<class _type_>
void List<_type_>::InsertAfter(link_t &link) {
	Remove();

	next = link.next;
	prev = &link;

	link.next->prev = this;
	link.next = this;

	head = link.head;
}

/*
================
List<_type_>::Len

COUNT the number of items in the list
PERFORMANCE WARNING: This scales linearly with the number of items in the list
================
*/
template<class _type_>
uint_t List<_type_>::Len() const {
	uint_t num;

	num = 0;
	for(link_t *p=next; p!=this; p++)
		num++;

	return num;
}

/*
================
List<_type_>::Empty

Determine whether the list is empty
================
*/
template<class _type_>
bool List<_type_>::Empty() const {
	return next!=this;
}
/*
================
List<_type_>::IsList

Determine whether this is the list itself (true) or a link within a list (false)
================
*/
template<class _type_>
bool List<_type_>::IsList() const {
	return head==this;
}
/*
================
List<_type_>::IsLink

Determine whether this is a link within a list (true) or the list itself (false)
================
*/
template<class _type_>
bool List<_type_>::IsLink() const {
	return head!=this;
}

/*
================
List<_type_>::Clear

Remove all items within the list if IsList() or unlink self (IsLink())
================
*/
template<class _type_>
void List<_type_>::Clear() {
	while( next!=this ) {
		next->Remove();
	}

	Remove();
}
/*
================
List<_type_>::Remove

Unlink self from list
================
*/
template<class _type_>
void List<_type_>::Remove() {
	prev->next = next;
	next->prev = prev;

	prev = this;
	next = this;
	head = this;
}

/*
================
List<_type_>::HeadLink

Retrieve the head link
================
*/
template<class _type_>
typename List<_type_>::link_t *List<_type_>::HeadLink() {
	return head;
}
template<class _type_>
const typename List<_type_>::link_t *List<_type_>::HeadLink() const {
	return head;
}
/*
================
List<_type_>::PrevLink

Retrieve the link to the previous item in the list
================
*/
template<class _type_>
typename List<_type_>::link_t *List<_type_>::PrevLink() {
	return prev!=this ? prev : AX_NULL;
}
template<class _type_>
const typename List<_type_>::link_t *List<_type_>::PrevLink() const {
	return prev!=this ? prev : AX_NULL;
}
/*
================
List<_type_>::NextLink

Retrieve the link to the next item in the list
================
*/
template<class _type_>
typename List<_type_>::link_t *List<_type_>::NextLink() {
	return next!=this ? next : AX_NULL;
}
template<class _type_>
const typename List<_type_>::link_t *List<_type_>::NextLink() const {
	return next!=this ? next : AX_NULL;
}

/*
================
List<_type_>::Prev

Retrieve the owner of the previous item in the list
================
*/
template<class _type_>
_type_ *List<_type_>::Prev() {
	return prev!=this ? prev->owner : AX_NULL;
}
template<class _type_>
const _type_ *List<_type_>::Prev() const {
	return prev!=this ? prev->owner : AX_NULL;
}
/*
================
List<_type_>::Next

Retrieve the owner of the next item in the list
================
*/
template<class _type_>
_type_ *List<_type_>::Next() {
	return next!=this ? next->owner : AX_NULL;
}
template<class _type_>
const _type_ *List<_type_>::Next() const {
	return next!=this ? next->owner : AX_NULL;
}

/*
===============================================================================

	Logging

===============================================================================
*/
class Lib {
public:
	static void		VPrintf(AX_PRINTF_PARM const char *fmt, va_list args);
	static void		Printf(AX_PRINTF_PARM const char *fmt, ...)
						AX_PRINTF_ATTR(2, 3);
	static void		Error(AX_PRINTF_PARM const char *fmt, ...)
						AX_PRINTF_ATTR(2, 3);
	static void		Warn(AX_PRINTF_PARM const char *fmt, ...)
						AX_PRINTF_ATTR(2, 3);
};

/*
================
Lib::VPrintf
================
*/
void Lib::VPrintf(const char *fmt, va_list args) {
	vfprintf(stderr, fmt, args);
}
/*
================
Lib::Printf
================
*/
void Lib::Printf(const char *fmt, ...) {
	va_list args;

	va_start(args, fmt);
	VPrintf(fmt, args);
	va_end(args);
}
/*
================
Lib::Error
================
*/
void Lib::Error(const char *fmt, ...) {
	va_list args;

	va_start(args, fmt);
	Printf("ERROR: ");
	VPrintf(fmt, args);
	Printf("\n");
	va_end(args);

#if _DEBUG
	if( IsDebuggerPresent() ) {
		DebugBreak();
	}
#endif
}
/*
================
Lib::Warn
================
*/
void Lib::Warn(const char *fmt, ...) {
	va_list args;

	va_start(args, fmt);
	Printf("WARNING: ");
	VPrintf(fmt, args);
	Printf("\n");
	va_end(args);
}

//============================================================================//
// * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * //
////////////////////////////////////////////////////////////////////////////////
//############################################################################//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//----------------------------------------------------------------------------//
//............................................................................//
//                                                                            //
//                 /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\                 //
//              //============================================\\              //
//          //====================================================\\          //
//      //============================================================\\      //
//  //====================================================================\\  //
////////////////////////////////////////////////////////////////////////////////
//****************************************************************************//
//*                                                                          *//
//* ======================================================================== *//
//*     --------------           --------------           --------------     *//
//*     FRAMEWORK CODE           FRAMEWORK CODE           FRAMEWORK CODE     *//
//*     --------------           --------------           --------------     *//
//* ======================================================================== *//
//*                                                                          *//
//****************************************************************************//
////////////////////////////////////////////////////////////////////////////////
//  \\====================================================================//  //
//      \\============================================================//      //
//          \\====================================================//          //
//              \\============================================//              //
//                 \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/                 //
//                                                                            //
//............................................................................//
//----------------------------------------------------------------------------//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//############################################################################//
////////////////////////////////////////////////////////////////////////////////
// * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * //
//============================================================================//

/*
===============================================================================

	AudioEngine
	Handle the entire audio system

===============================================================================
*/
class AudioMixer;

struct audioPerfData_t {
	// CPU cycles spent on processing audio since last query
	u64_t	audioCycles;
	// Total CPU cycles since last query
	u64_t	totalCycles;
	// Lowest amount of CPU cycles spent processing any audio quantum
	u32_t	minCycles;
	// Highest amount of CPU cycles spent processing any audio quantum
	u32_t	maxCycles;
	// Total memory currently in use (bytes)
	u32_t	memory;
	// Current delay (in samples) between submit and playback
	u32_t	latency;
	// Total audio dropouts
	u32_t	glitches;
	// Current number of audio sources being mixed
	u32_t	activeVoices;
	// Current number of audio mixers working
	u32_t	activeMixers;
};

class AudioEngine {
friend class AudioMixer;
friend class AudioTrack;
public:
						AudioEngine();
						~AudioEngine();

	bool				Init();
	void				Fini();
	bool				IsInitialized() const;

	void				Start();
	void				Stop();

	void				SetVolume(double volume);
	double				GetVolume() const;

	AudioMixer *		BGM();
	const AudioMixer *	BGM() const;

	AudioMixer *		SFX();
	const AudioMixer *	SFX() const;

	audioPerfData_t		QueryPerformance() const;

protected:
	IXAudio2 *			engine;
	IXAudio2MasteringVoice *master;

	AudioMixer *		bgmMixer;
	AudioMixer *		sfxMixer;
};

/*
===============================================================================

	AudioMixer
	Handle a mixer (collection of voices) for the audio engine; there can be
	multiple mixers

===============================================================================
*/
class AudioTrack;

class AudioMixer {
friend class AudioEngine;
friend class AudioTrack;
public:
	void				SetVolume(double volume);
	double				GetVolume() const;

	bool				AddVoice(AudioTrack *voice);
	void				RemoveVoice(AudioTrack *voice);
	void				RemoveAllVoices();

protected:
	AudioEngine *		engine;
	IXAudio2SubmixVoice *submixer;
	List<AudioTrack>	voices;

						AudioMixer(AudioEngine *engine);
						~AudioMixer();

	bool				Init();
	void				Fini();
};

/*
===============================================================================

	AudioTrack
	Basic sound track (base class for Music and Sound)

===============================================================================
*/
enum audioFormat_t {
	SNDF_UNINITIALIZED,

	SNDF_MONO_8,
	SNDF_MONO_16,
	SNDF_STEREO_8,
	SNDF_STEREO_16
};
enum bufferSound_t {
	SNDBUF_CONTINUE,	//there's more to be queued
	SNDBUF_BREAK		//end of the queue
};
class AudioTrack {
friend class AudioMixer;
public:
	virtual				~AudioTrack();

	void				SetVolume(double volume);
	double				GetVolume() const;

	audioFormat_t		Format() const;
	bool				IsMono() const;
	bool				IsStereo() const;
	bool				IsEightBit() const;
	bool				IsSixteenBit() const;

	u32_t				NumChannels() const;
	u32_t				SampleSize() const;

	const char *		File() const;
	bool				IsStreaming() const;

	bool				Buffer(u32_t numSamples, const void *samples,
							bufferSound_t opt);

protected:
	AudioMixer *		mixer;
	List<AudioTrack>	link;
	IXAudio2SourceVoice *source;

	char				filename[ 256 ];
	FILE *				fptr; //if set, this is streaming

	audioFormat_t		format;
	u32_t				numSamples;
	u16_t				sampleRate;
	void *				samples;

						AudioTrack();
						
	bool				Init(AudioMixer *newmixer, audioFormat_t sfmt);
	bool				InitMemory(AudioMixer *newmixer, audioFormat_t format,
							u32_t numSamples, u16_t rate, const void *samples);
	bool				InitStream(AudioMixer *newmixer, const char *filename);
	void				Fini();

	void				Update();
};

/*
===============================================================================

	Music
	Background music (streamed in or automatically synthesized)

===============================================================================
*/
class Music: public virtual AudioTrack {
public:
	static Music *		StreamWAV(const char *filename);

	static Music *		Mono8(u32_t numSamples, u16_t rate, u8_t *samples);
	static Music *		Mono16(u32_t numSamples, u16_t rate, u16_t *samples);
	static Music *		Stereo8(u32_t numSamples, u16_t rate, u8_t *samples);
	static Music *		Stereo16(u32_t numSamples, u16_t rate, u16_t *samples);

	virtual				~Music();

protected:
						Music();
};
/*
===============================================================================

	Sound
	Sound effect

===============================================================================
*/
class Sound: public virtual AudioTrack {
public:
	static Sound *		StreamWAV(const char *filename);
	static Sound *		LoadWAV(const char *filename);

	static Sound *		Mono8(u32_t numSamples, u16_t rate, u8_t *samples);
	static Sound *		Mono16(u32_t numSamples, u16_t rate, u16_t *samples);
	static Sound *		Stereo8(u32_t numSamples, u16_t rate, u8_t *samples);
	static Sound *		Stereo16(u32_t numSamples, u16_t rate, u16_t *samples);

	virtual				~Sound();

protected:
						Sound();
};


/*
================
AudioEngine::AudioEngine

Construct the audio engine; does not initialize XAudio2
================
*/
AudioEngine::AudioEngine(): engine(AX_NULL),master(AX_NULL), bgmMixer(AX_NULL),
sfxMixer(AX_NULL) {
}
/*
================
AudioEngine::~AudioEngine

Destruct the audio engine; will automatically shut down open resources
================
*/
AudioEngine::~AudioEngine() {
	if( !IsInitialized() ) {
		return;
	}

	Fini();
}

/*
================
AudioEngine::Init

Initialize the audio engine (call before using!)
================
*/
bool AudioEngine::Init() {
	if( !!engine ) {
		return true;
	}

	HRESULT hr;

	hr = XAudio2Create( &engine );
	if( FAILED(hr) ) {
		Lib::Error( "XAudio2Create() failed" );
		return false;
	}

#ifdef _DEBUG
	XAUDIO2_DEBUG_CONFIGURATION xdc;

	xdc.TraceMask = 0xFFFFFFFF;
	xdc.BreakMask = 0x00000000;
	xdc.LogThreadID = TRUE;
	xdc.LogFileline = TRUE;
	xdc.LogFunctionName = TRUE;
	xdc.LogTiming = TRUE;

	engine->SetDebugConfiguration( &xdc );
#endif

	hr = engine->CreateMasteringVoice( &master );
	if( FAILED(hr) ) {
		engine->Release(); engine = AX_NULL;
		Lib::Error( "IXAudio2::CreateMasteringVoice() failed" );
		return false;
	}

	bgmMixer = new AudioMixer( this );
	if( !bgmMixer ) {
		master->DestroyVoice(); master = AX_NULL;
		engine->Release(); engine = AX_NULL;
		Lib::Error( "Failed to allocate AudioMixer (BGM)" );
		return false;
	}

	sfxMixer = new AudioMixer( this );
	if( !sfxMixer ) {
		delete bgmMixer;
		master->DestroyVoice(); master = AX_NULL;
		engine->Release(); engine = AX_NULL;
		Lib::Error( "Failed to allocate AudioMixer (SFX)" );
		return false;
	}

	return true;
}
/*
================
AudioEngine::Fini

Finish using the audio engine
================
*/
void AudioEngine::Fini() {
	if( !engine ) {
		return;
	}

	delete sfxMixer; sfxMixer = AX_NULL;
	delete bgmMixer; bgmMixer = AX_NULL;

	master->DestroyVoice(); master = AX_NULL;
	engine->Release(); engine = AX_NULL;
}
/*
================
AudioEngine::IsInitialized

Determine whether the audio engine has been initialized
================
*/
bool AudioEngine::IsInitialized() const {
	return !!engine;
}

/*
================
AudioEngine::Start

Start the audio engine processing thread
================
*/
void AudioEngine::Start() {
	AX_ASSERT( !!engine );
	engine->StartEngine();
}
/*
================
AudioEngine::Stop

Stop the audio engine processing thread
================
*/
void AudioEngine::Stop() {
	AX_ASSERT( !!engine );
	engine->StopEngine();
}

/*
================
AudioEngine::SetVolume

Set the master volume (-1 .. 1)
================
*/
void AudioEngine::SetVolume(double volume) {
	AX_ASSERT( !!engine );
	AX_ASSERT( !!master );

	//clampificate
	if( volume<-1 )
		volume=-1;
	if( volume>+1 )
		volume=+1;

	master->SetVolume( (float)(volume*XAUDIO2_MAX_VOLUME_LEVEL) );
}
/*
================
AudioEngine::GetVolume

Get the master volume (-1 .. 1)
================
*/
double AudioEngine::GetVolume() const {
	AX_ASSERT( !!engine );
	AX_ASSERT( !!master );

	float f = 0.0f;
	master->GetVolume( &f );

	return ((double)f)/(double)XAUDIO2_MAX_VOLUME_LEVEL;
}
/*
================
AudioEngine::BGM

Retrieve the background music audio mixer
================
*/
AudioMixer *AudioEngine::BGM() {
	return bgmMixer;
}
const AudioMixer *AudioEngine::BGM() const {
	return bgmMixer;
}

/*
================
AudioEngine::SFX

Retrieve the sound effects audio mixer
================
*/
AudioMixer *AudioEngine::SFX() {
	return sfxMixer;
}
const AudioMixer *AudioEngine::SFX() const {
	return sfxMixer;
}

/*
================
AudioEngine::QueryPerformance

Grab performance information from the audio engine
================
*/
audioPerfData_t AudioEngine::QueryPerformance() const {
	XAUDIO2_PERFORMANCE_DATA xpd;
	audioPerfData_t apd;

	engine->GetPerformanceData( &xpd );

	apd.audioCycles = xpd.AudioCyclesSinceLastQuery;
	apd.totalCycles = xpd.TotalCyclesSinceLastQuery;
	apd.minCycles = xpd.MinimumCyclesPerQuantum;
	apd.maxCycles = xpd.MaximumCyclesPerQuantum;
	apd.memory = xpd.MemoryUsageInBytes;
	apd.latency = xpd.CurrentLatencyInSamples;
	apd.glitches = xpd.GlitchesSinceEngineStarted;
	apd.activeVoices = xpd.ActiveSourceVoiceCount;
	apd.activeMixers = xpd.ActiveSubmixVoiceCount;

	return apd;
}

/*
================
AudioMixer::AudioMixer

Constructor
================
*/
AudioMixer::AudioMixer(AudioEngine *engine): engine(engine), submixer(AX_NULL),
voices() {
	AX_ASSERT( !!engine );
}
/*
================
AudioMixer::~AudioMixer

Destructor
================
*/
AudioMixer::~AudioMixer() {
	Fini();
}

/*
================
AudioMixer::Init

Initialize the submixer voice
================
*/
bool AudioMixer::Init() {
	if( !!submixer ) {
		return true;
	}

	HRESULT hr;

	// stereo, 44.1khz
	hr = engine->engine->CreateSubmixVoice( &submixer, 2, 44100 );
	if( FAILED(hr) ) {
		Lib::Error( "IXAudio2::CreateSubmixVoice() failed" );
		return false;
	}

	return true;
}
/*
================
AudioMixer::Fini

Finish using the submixer voice
================
*/
void AudioMixer::Fini() {
	if( !submixer ) {
		return;
	}

	RemoveAllVoices();

	submixer->DestroyVoice();
	submixer = AX_NULL;
}

/*
================
AudioMixer::SetVolume

Adjust the volume for this mixer
================
*/
void AudioMixer::SetVolume(double volume) {
	AX_ASSERT( !!submixer );

	//clampificate
	if( volume<-1 )
		volume=-1;
	if( volume>+1 )
		volume=+1;

	submixer->SetVolume( (float)(volume*XAUDIO2_MAX_VOLUME_LEVEL) );
}
/*
================
AudioMixer::GetVolume

Retrieve the volume for this mixer
================
*/
double AudioMixer::GetVolume() const {
	AX_ASSERT( !!submixer );

	float f = 0.0f;
	submixer->GetVolume( &f );

	return ((double)f)/(double)XAUDIO2_MAX_VOLUME_LEVEL;
}

/*
================
AudioMixer::AddVoice

Add a voice to the mixer
================
*/
bool AudioMixer::AddVoice(AudioTrack *voice) {
	AX_ASSERT( !!submixer );

	voices.AddTail( voice->link );

	XAUDIO2_SEND_DESCRIPTOR sendDesc = {
		1, submixer
	};
	XAUDIO2_VOICE_SENDS voiceSends = {
		1, &sendDesc
	};

	HRESULT hr = voice->source->SetOutputVoices( &voiceSends );
	if( FAILED(hr) ) {
		Lib::Error( "voice->source->SetOutputVoices() failed" );
		return false;
	}

	return true;
}
/*
================
AudioMixer::RemoveVoice

Remove a voice from the mixer
================
*/
void AudioMixer::RemoveVoice(AudioTrack *voice) {
	static XAUDIO2_SEND_DESCRIPTOR nullSendDesc = {
		0, AX_NULL
	};
	static const XAUDIO2_VOICE_SENDS nullVoiceSends = {
		1, &nullSendDesc
	};

	AX_ASSERT( !!submixer );

	if( voice->link.HeadLink()!=&voices ) {
		Lib::Warn( "Tried to remove unattached voice" );
		return;
	}

	voice->source->SetOutputVoices( &nullVoiceSends );

	voice->link.Remove();
}
/*
================
AudioMixer::RemoveAllVoices

Remove all voices from the mixer
================
*/
void AudioMixer::RemoveAllVoices() {
	AX_ASSERT( !!submixer );

	AudioTrack *voice;
	while( !!(voice = voices.Next()) ) {
		RemoveVoice( voice );
	}
}

/*
================
AudioTrack::AudioTrack

Constructor
================
*/
AudioTrack::AudioTrack(): mixer(AX_NULL), link(this), source(AX_NULL),
fptr(AX_NULL), format(SNDF_UNINITIALIZED), numSamples(0), sampleRate(0),
samples(AX_NULL) {
	filename[ 0 ] = '\0';
}
/*
================
AudioTrack::~AudioTrack

Destructor
================
*/
AudioTrack::~AudioTrack() {
	Fini();
}

/*
================
AudioTrack::Init

Initialize this track
================
*/
bool AudioTrack::Init(AudioMixer *newmixer, audioFormat_t sfmt) {
	AX_ASSERT( !!newmixer );

	if( !!mixer ) {
		Lib::Warn( "AudioTrack::Init() already initialized" );
		return true;
	}

	WAVEFORMATEX fmt;

	fmt.wFormatTag = WAVE_FORMAT_PCM;
	fmt.nChannels = sfmt==SNDF_STEREO_16 || sfmt==SNDF_STEREO_8 ? 2 : 1;
	fmt.nSamplesPerSec = 44100;
	fmt.wBitsPerSample = sfmt==SNDF_MONO_8 || sfmt==SNDF_STEREO_8 ? 8 : 16;
	fmt.nBlockAlign = (fmt.nChannels*fmt.wBitsPerSample)/8;
	fmt.nAvgBytesPerSec = fmt.nSamplesPerSec*fmt.nBlockAlign;
	fmt.cbSize = 0;

	HRESULT hr;
	hr = newmixer->engine->engine->CreateSourceVoice( &source, &fmt );
	if( FAILED(hr) ) {
		Lib::Error( "AudioTrack::Init() -> CreateSourceVoice() failed" );
		return false;
	}

	mixer = newmixer;
	if( !mixer->AddVoice( this ) ) {
		source->DestroyVoice(); source = AX_NULL;
		Lib::Error( "AudioTrack::Init() -> AudioMixer::AddVoice() failed" );
		return false;
	}

	return true;
}
/*
================
AudioTrack::InitMemory

Initialize the track from the memory
================
*/
bool AudioTrack::InitMemory(AudioMixer *newmixer, audioFormat_t format,
u32_t numSamples, u16_t rate, const void *samples) {
	u32_t len = numSamples*(1 + (int)IsSixteenBit());

	if( len > XAUDIO2_MAX_BUFFER_BYTES ) {
		Lib::Error( "AudioTrack::InitMemory(): Audio buffer too large" );
		return false;
	}

	if( !Init( newmixer, format ) ) {
		Lib::Error( "AudioTrack::InitMemory(): AudioTrack::Init() failed" );
		return false;
	}

	if( !Buffer( numSamples, samples, SNDBUF_BREAK ) ) {
		Lib::Error( "AudioTrack::InitMemory(): AudioTrack::Buffer() failed" );
		return false;
	}

	return true;
}
/*
================
AudioTrack::InitStream

Initialize the track from a file stream
================
*/
bool AudioTrack::InitStream(AudioMixer *newmixer, const char *filename) {
	(void)newmixer;
	(void)filename;
	Lib::Warn( "AudioTrack::InitStream(): not implemented" );
	return false;
}

/*
================
AudioTrack::Fini

Finish using this track
================
*/
void AudioTrack::Fini() {
	if( !source ) {
		return;
	}

	source->DestroyVoice();
	link.Remove();
}

/*
================
AudioTrack::Update

Update the streams
================
*/
void AudioTrack::Update() {
	// TODO
}

/*
================
AudioTrack::SetVolume

Adjust the current output volume for this track
================
*/
void AudioTrack::SetVolume(double volume) {
	AX_ASSERT( !!source );

	//clampificate
	if( volume<-1 )
		volume=-1;
	if( volume>+1 )
		volume=+1;

	source->SetVolume( (float)(volume*XAUDIO2_MAX_VOLUME_LEVEL) );
}
/*
================
AudioTrack::GetVolume

Retrieve the current output volume for this track
================
*/
double AudioTrack::GetVolume() const {
	AX_ASSERT( !!source );

	float f = 0.0f;
	source->GetVolume( &f );

	return ((double)f)/(double)XAUDIO2_MAX_VOLUME_LEVEL;
}

/*
================
AudioTrack::Format

Retrieve the format of this track
================
*/
audioFormat_t AudioTrack::Format() const {
	return format;
}
/*
================
AudioTrack::IsMono

Determine whether the format is a mono (one-channel) format
================
*/
bool AudioTrack::IsMono() const {
	return format==SNDF_MONO_8 || format==SNDF_MONO_16;
}
/*
================
AudioTrack::IsStereo

Determine whether the format is a stereo (two-channel) format
================
*/
bool AudioTrack::IsStereo() const {
	return format==SNDF_STEREO_8 || format==SNDF_STEREO_16;
}
/*
================
AudioTrack::IsEightBit

Determine whether the format is an 8-bit format
================
*/
bool AudioTrack::IsEightBit() const {
	return format==SNDF_MONO_8 || format==SNDF_STEREO_8;
}
/*
================
AudioTrack::IsSixteenBit

Determine whether the format is a 16-bit format
================
*/
bool AudioTrack::IsSixteenBit() const {
	return format==SNDF_MONO_16 || format==SNDF_STEREO_16;
}

/*
================
AudioTrack::NumChannels

Retrieve the number of channels used by the format
================
*/
u32_t AudioTrack::NumChannels() const {
	return format==SNDF_STEREO_8 || format==SNDF_STEREO_16 ? 2 : 1;
}

/*
================
AudioTrack::SampleSize

Retrieve the byte size of each individual sample
================
*/
u32_t AudioTrack::SampleSize() const {
	switch( format ) {
	case SNDF_MONO_8:		return 1;
	case SNDF_MONO_16:		return 2;
	case SNDF_STEREO_8:		return 2;
	case SNDF_STEREO_16:	return 4;
	default:
		break;
	}

	return 0;
}

/*
================
AudioTrack::File

Retrieve the name of the backing file for this track
================
*/
const char *AudioTrack::File() const {
	return filename;
}
/*
================
AudioTrack::IsStreaming

Determine whether this file is being streamed
================
*/
bool AudioTrack::IsStreaming() const {
	return !!fptr;
}

/*
================
AudioTrack::Buffer

Buffer audio to be played back
================
*/
bool AudioTrack::Buffer(u32_t numSamples, const void *samples,
bufferSound_t opt) {
	AX_ASSERT( numSamples>0 );
	AX_ASSERT( !!samples );
	AX_ASSERT( opt==SNDBUF_BREAK || opt==SNDBUF_CONTINUE );

	XAUDIO2_BUFFER xb;

	xb.Flags		= opt==SNDBUF_BREAK ? XAUDIO2_END_OF_STREAM : 0;
	xb.AudioBytes	= SampleSize()*numSamples;
	xb.pAudioData	= (const BYTE *)samples;
	xb.PlayBegin	= 0;
	xb.PlayLength	= 0;
	xb.LoopBegin	= 0;
	xb.LoopLength	= 0;
	xb.LoopCount	= 0;

	HRESULT hr = source->SubmitSourceBuffer( &xb );
	if( FAILED(hr) ) {
		Lib::Error( "AudioTrack::Buffer(): SubmitSourceBuffer failed" );
		return false;
	}

	return true;
}

/*
===============================================================================

	Image

===============================================================================
*/
class Image {
public:
					~Image();

	const char *	Name() const;

	u16_t			ResX() const;
	u16_t			ResY() const;
	const u8_t *	Data() const;

	bool			HasAlpha() const;

	bool			LoadGL();
	void			UnloadGL();
	GLuint			GLImage() const;

	static Image *	LoadTGA(const char *filename);

protected:
	char			name[ 256 ];

	u16_t			resX;
	u16_t			resY;
	u8_t *			data;

	GLuint			handle;
	bool			alpha;

	List<Image>		link;
	static List<Image>	list;

					Image();
};
List<Image>			Image::list;

/*
================
Image::Image

Constructor
================
*/
Image::Image(): resX(0),resY(0), data(AX_NULL), handle(0),alpha(false),
link(this) {
	name[ 0 ] = '\0';
	list.AddTail( link );
}
/*
================
Image::~Image

Destructor
================
*/
Image::~Image() {
	UnloadGL();

	if( !data ) {
		return;
	}

	data = (u8_t *)Mem_Dealloc( (void *)data );
}

/*
================
Image::Name

Retrieve the filename (as referenced during load) of the image
================
*/
const char *Image::Name() const {
	return name;
}

/*
================
Image::ResX

Retrieve the width of the image
================
*/
u16_t Image::ResX() const {
	return resX;
}
/*
================
Image::ResY

Retrieve the height of the image
================
*/
u16_t Image::ResY() const {
	return resY;
}
/*
================
Image::Data

Read the data of the image directly
================
*/
const u8_t *Image::Data() const {
	return data;
}

/*
================
Image::LoadGL

Load the image into OpenGL
================
*/
bool Image::LoadGL() {
	AX_ASSERT( resX>0 && resY>0 );
	AX_ASSERT( !!data );

	if( !!handle ) {
		return true;
	}

	if( resX>8192 || resY>8192 ) {
		Lib::Warn( "Image::LoadGL(\"%s\"): resolution is very large (%i,%i)",
			name, (int)resX, (int)resY );
	}
	if( resX!=resY ) {
		Lib::Warn( "Image::LoadGL(\"%s\"): not square (%i,%i)", name,
			(int)resX, (int)resY );
	}
	if( (resX>0 && (resX & (resX - 1))) || (resY>0 && (resY & (resY - 1))) ) {
		Lib::Warn( "Image::LoadGL(\"%s\"): not power-of-two (%i,%i)", name,
			(int)resX, (int)resY );
	}

	//clear the last error if set
	int err = glGetError();
#if _DEBUG
	if( err ) {
		Lib::Error( "Image::LoadGL(\"%s\"): glGetError() -> %i", name, err );
	}
#else
	(void)err;
#endif

	glGenTextures(1, &handle);
	if( !handle ) {
		Lib::Error( "Image::LoadGL(\"%s\"): glGenTextures() failed", name);
		return false;
	}

	glTexImage2D(GL_TEXTURE_2D, 0, alpha ? GL_RGBA : GL_RGB, resX, resY, 0,
		alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, (const GLvoid *)data);
	if( !!(err = glGetError()) ) {
		Lib::Error( "Image::LoadGL(\"%s\"): glTexImage2D() -> %i",
			name, err );
		glDeleteTextures(1, &handle);
		handle = 0;
		return false;
	}

	return true;
}
/*
================
Image::UnloadGL

Unload the image from OpenGL
================
*/
void Image::UnloadGL() {
	if( !handle ) {
		return;
	}

	glDeleteTextures(1, &handle);
	handle = 0;
}

/*
================
Image::GLImage

Retrieve the OpenGL handle to the image
================
*/
GLuint Image::GLImage() const {
	return handle;
}

/*
================
Image::LoadTGA

Load the image from a TGA (does not load to OpenGL)
================
*/
Image *Image::LoadTGA(const char *filename) {
	// TODO: ensure everything is little endian
	static const int INDEXED = 1;
	static const int RGB = 2;
	static const int GRAY = 3;

#pragma pack(1)
	struct header_t {
		u8_t	infoLen;
		u8_t	colormap;
		union {
			struct {
				u8_t	type:3;
				bool	rle:1;
			}	f;
			u8_t		bits;
		}		type;

		u16_t	cmapOff;
		u16_t	cmapLen;
		u8_t	cmapBits;

		u16_t	originX;
		u16_t	originY;

		u16_t	resX;
		u16_t	resY;

		u8_t	bpp;
		u8_t	bits;

		inline bool IsDescTypeValid() const {
			if( type.f.type!=INDEXED && type.f.type!=RGB
			&& type.f.type!=GRAY ) {
				return false;
			}

			return true;
		}
	};
#pragma pack()

#if __STDC_WANT_SECURE_LIB__
	FILE *f = AX_NULL;
	if( fopen_s( &f, filename, "rb" ) != 0 ) {
		f = AX_NULL;
	}
#else
	FILE *f = fopen( filename, "rb" );
#endif
	if( !f ) {
		Lib::Error("\"%s\": fopen() failed", filename);
		return AX_NULL;
	}

	fseek(f, 0, SEEK_END);
	u32_t len = ftell(f);
	fseek(f, 0, SEEK_SET);

	u8_t *buffer = (u8_t *)Mem_Alloc( len, TAG_FW_LOADTGA );
	if( !buffer ) {
		fclose(f);
		Lib::Error("\"%s\": Mem_Alloc() failed", filename);
		return AX_NULL;
	}

	if( !fread( buffer,len,1,f ) ) {
		fclose(f);
		Mem_Dealloc( (void *)buffer );
		Lib::Error("\"%s\": fread() failed", filename);
		return AX_NULL;
	}

	fclose(f);
	f = AX_NULL;

	header_t hdr;
	u8_t *src = buffer;

	hdr.infoLen = *src++;
	hdr.colormap = *src++;
	hdr.type.bits = *src++;

	hdr.cmapOff = *(u16_t *)src; src += 2;
	hdr.cmapLen = *(u16_t *)src; src += 2;
	hdr.cmapBits = *src++;

	hdr.originX = *(u16_t *)src; src += 2;
	hdr.originY = *(u16_t *)src; src += 2;

	hdr.resX = *(u16_t *)src; src += 2;
	hdr.resY = *(u16_t *)src; src += 2;

	hdr.bpp = *src++;
	hdr.bits = *src++;

	src += hdr.infoLen;

	if( !hdr.IsDescTypeValid() ) {
		Mem_Dealloc( (void *)buffer );
		Lib::Error("\"%s\": Invalid Targa header", filename);
		return AX_NULL;
	}

	if( hdr.type.f.type==INDEXED ) {
		Mem_Dealloc( (void *)buffer );
		Lib::Error("\"%s\": Indexed images are not supported", filename);
		return AX_NULL;
	}

	if( hdr.colormap!=0 ) {
		Mem_Dealloc( (void *)buffer );
		Lib::Error("\"%s\": Colormaps are not supported", filename);
		return AX_NULL;
	}

	if( hdr.type.f.type==GRAY ) {
		if( hdr.bpp!=8 ) {
			Mem_Dealloc( (void *)buffer );
			Lib::Error("\"%s\": Grayscale images must be 8-bit", filename);
			return AX_NULL;
		}
	} else if( hdr.bpp!=32 && hdr.bpp!=24 ) {
		Mem_Dealloc( (void *)buffer );
		Lib::Error("\"%s\": Only RGBA, RGB, or grayscale images are supported",
			filename);
		return AX_NULL;
	}

	if( !hdr.type.f.rle ) {
		if( hdr.resX*hdr.resY*(hdr.bpp/8U) > len - (hdr.infoLen + 18U) ) {
			Mem_Dealloc( (void *)buffer );
			Lib::Error("\"%s\": Incomplete file (uncompressed)", filename);
			return AX_NULL;
		}
	}

	Image *img = new Image();
	if( !img ) {
		Mem_Dealloc( (void *)buffer );
		Lib::Error("\"%s\": Failed to allocate image", filename);
		return AX_NULL;
	}

#if __STDC_WANT_SECURE_LIB__
	strcpy_s( img->name,sizeof(img->name), filename );
#else
	strncpy( img->name, filename, sizeof(img->name) );
	img->name[ sizeof(img->name) - 1 ] = '\0';
#endif

	img->alpha = hdr.bpp==32 ? true : false;

	img->resX = hdr.resX;
	img->resY = hdr.resY;

	img->data = (u8_t *)Mem_Alloc( img->resX*img->resY*(img->alpha ? 4 : 3),
		TAG_FW_IMAGE );
	if( !img->data ) {
		delete img;
		Mem_Dealloc( (void *)buffer );
		Lib::Error("\"%s\": Failed to allocate image buffer", filename);
		return AX_NULL;
	}

	u16_t x, y;
	u8_t *dst = img->data;

	if( !hdr.type.f.rle ) {
		switch( hdr.bpp ) {
		case 8:
			for(y=0; y<hdr.resY; y++) {
				for(x=0; x<hdr.resX; x++) {
					*dst++ = *src;
					*dst++ = *src;
					*dst++ = *src;
					if( img->alpha ) {
						*dst++ = 0xFF;
					}
					src++;
				}
			}
			break;

		case 24:
			for(y=0; y<hdr.resY; y++) {
				for(x=0; x<hdr.resX; x++) {
					*dst++ = src[ 2 ];
					*dst++ = src[ 1 ];
					*dst++ = src[ 0 ];
					if( img->alpha ) {
						*dst++ = 0xFF;
					}
					src += 3;
				}
			}
			break;

		case 32:
			for(y=0; y<hdr.resY; y++) {
				for(x=0; x<hdr.resX; x++) {
					*dst++ = src[ 2 ];
					*dst++ = src[ 1 ];
					*dst++ = src[ 0 ];
					if( img->alpha ) {
						*dst++ = src[ 3 ];
					}
					src += 4;
				}
			}
			break;
		}
	} else {
		y = 0;
		x = 0;
		while(x<hdr.resX && y<hdr.resY) {
			u8_t r,g,b,a;
			u8_t pkthdr = *src++;
			u8_t pktlen = 1 + (pkthdr & 0x7F);
			u8_t srclen;

			if( ~pkthdr & 0x80 ) {
				srclen = pktlen;
				pktlen = 1;
			} else {
				srclen = 1;
			}

#if 0
			if( src + pktlen*(hdr.bpp/8) > buffer + len ) {
				delete img;
				Mem_Dealloc( (void *)buffer );
				Lib::Error("\"%s\": Incomplete file (RLE)", filename);
				return AX_NULL;
			}
#endif

			while(srclen > 0) {
				switch( hdr.bpp ) {
				case 8:
					r = *src;
					g = *src;
					b = *src;
					a = 0xFF;
					src++;
					break;

				case 24:
					r = src[ 2 ];
					g = src[ 1 ];
					b = src[ 0 ];
					a = 0xFF;
					src += 3;
					break;

				case 32:
					r = src[ 2 ];
					g = src[ 1 ];
					b = src[ 0 ];
					a = src[ 3 ];
					src += 4;
					break;
				}

				while( pktlen > 0 ) {
					*dst++ = r;
					*dst++ = g;
					*dst++ = b;
					if( img->alpha ) {
						*dst++ = a;
					}

					pktlen--;

					x++;
					if( x==hdr.resX ) {
						x = 0;
						if( y==hdr.resY ) {
							goto __exitLoop;
						} else {
							y++;
						}
					}
				}

				srclen--;
				pktlen = 1;
			}
		}
__exitLoop:;
	}

	if( hdr.bits & 0x10 ) {
		// TODO: flip horizontal
		Lib::Warn("\"%s\": Horizontal flipping not implemented", filename);
	}
	if( ~hdr.bits & 0x20 ) {
		// TODO: flip the image (vertical)
		Lib::Warn("\"%s\": Vertical flipping not implemented", filename);
	}

	Mem_Dealloc( (void *)buffer );
	return img;
}

/*
===============================================================================

	EventQueue
	Queue events for later use

===============================================================================
*/

enum key_t {
	K_UNKNOWN = 0,

    K_ESCAPE = 1,
    K_A = 30,
    K_B = 48,
    K_C = 46,
    K_D = 32,
    K_E = 18,
    K_F = 33,
    K_G = 34,
    K_H = 35,
    K_I = 23,
    K_J = 36,
    K_K = 37,
    K_L = 38,
    K_M = 50,
    K_N = 49,
    K_O = 24,
    K_P = 25,
    K_Q = 16,
    K_R = 19,
    K_S = 31,
    K_T = 20,
    K_U = 22,
    K_V = 47,
    K_W = 17,
    K_X = 45,
    K_Y = 21,
    K_Z = 44,
    K_F1 = 59,
    K_F2 = 60,
    K_F3 = 61,
    K_F4 = 62,
    K_F5 = 63,
    K_F6 = 64,
    K_F7 = 65,
    K_F8 = 66,
    K_F9 = 67,
    K_F10 = 68,
    K_F11 = 87,
    K_F12 = 88,
    K_GRAVE = 41,
    K_1 = 2,
    K_2 = 3,
    K_3 = 4,
    K_4 = 5,
    K_5 = 6,
    K_6 = 7,
    K_7 = 8,
    K_8 = 9,
    K_9 = 10,
    K_0 = 11,
    K_MINUS = 12,
    K_EQUAL = 13,
    K_BACKSPACE = 14,
    K_TAB = 15,
    K_LBRACKET = 26,
    K_RBRACKET = 27,
    K_BACKSLASH = 43,
    K_ENTER = 28,
    K_CAPSLOCK = 58,
    K_LSHIFT = 42,
    K_RSHIFT = 54,
    K_SEMICOLON = 39,
    K_QUOTE = 40,
    K_COMMA = 51,
    K_PERIOD = 52,
    K_SLASH = 53,
    K_LSUPER = 219,
    K_LALT = 56,
    K_RALT = 184,
    K_RSUPER = 220,
    K_MENU = 221,
    K_LCTRL = 29,
    K_RCTRL = 157,
    K_PRNTSCRN = 183,
    K_SCROLLLOCK = 70,
    K_BREAK = 197,
    K_INS = 210,
    K_DEL = 211,
    K_HOME = 199,
    K_END = 207,
    K_PGUP = 201,
    K_PGDN = 209,
    K_UP = 200,
    K_DOWN = 208,
    K_LEFT = 203,
    K_RIGHT = 205,
    K_NUMLOCK = 69,
    K_NUMDIV = 181,
    K_NUMMUL = 55,
    K_NUMSUB = 74,
    K_NUMADD = 78,
    K_NUMENTER = 156,
    K_NUMDOT = 83,
    K_NUM1 = 79,
    K_NUM2 = 80,
    K_NUM3 = 81,
    K_NUM4 = 75,
    K_NUM5 = 76,
    K_NUM6 = 77,
    K_NUM7 = 71,
    K_NUM8 = 72,
    K_NUM9 = 73,
    K_NUM0 = 82,
    K_SPACE = 57
};
enum buttonID_t {
	// generic
	BUTTON_MLEFT,		//left mouse button
	BUTTON_MRIGHT,		//right mouse button
	BUTTON_MWHEEL,		//mouse wheel (middle mouse button)
	BUTTON_MX1,			//extra mouse button (1)
	BUTTON_MX2,			//extra mouse button (2)

	BUTTON_MWHEELDOWN,	//mouse wheel down (toward you)
	BUTTON_MWHEELUP,	//mouse wheel up (away from you)

	BUTTON_UP,			//W or up arrow
	BUTTON_LEFT,		//A or left arrow
	BUTTON_DOWN,		//S or down arrow
	BUTTON_RIGHT,		//D or right arrow

	// action specific
	BUTTON_PREVWEAPON,	//BUTTON_MWHEELUP
	BUTTON_NEXTWEAPON,	//BUTTON_MWHEELDOWN
	BUTTON_WEAPON1,		//1
	BUTTON_WEAPON2,		//2
	BUTTON_WEAPON3,		//3
	BUTTON_WEAPON4,		//4
	BUTTON_WEAPON5,		//5
	BUTTON_WEAPON6,		//6
	BUTTON_WEAPON7,		//7
	BUTTON_WEAPON8,		//8
	BUTTON_WEAPON9,		//9
	BUTTON_WEAPON0,		//0
	BUTTON_USE1,		//E or BUTTON_MLEFT
	BUTTON_USE2,		//Q or BUTTON_MRIGHT
	BUTTON_SPEED,		//Shift
	BUTTON_PREVTARGET,	//G
	BUTTON_NEXTTARGET,	//F

	// networking specific
	BUTTON_NET_TALK,	//T
	BUTTON_NET_VOICE,	//V

	// quit
	BUTTON_QUIT			//Esc or window close button
};
enum eventKind_t {
	EV_NULL,

	EV_KEYPRESS,
	EV_KEYRELEASE,

	EV_PRESS,
	EV_RELEASE,

	EV_MOTION
};
struct keyEvent_t {
	key_t				ident;
};
struct buttonEvent_t {
	buttonID_t			ident;
};
struct motionEvent_t {
	union {
		float			abs[ 2 ];
		struct {
			float		x;
			float		y;
		}				a;
	};
	union {
		float			rel[ 2 ];
		struct {
			float		x;
			float		y;
		}				r;
	};
};
struct event_t {
	eventKind_t			kind;
	uint_t				frame;

	union {
		keyEvent_t		key;
		buttonEvent_t	button;
		motionEvent_t	motion;
	};

	static event_t KeyPress(uint_t frameID, key_t ident);
	static event_t KeyRelease(uint_t frameID, key_t ident);
	static event_t Press(uint_t frameID, buttonID_t ident);
	static event_t Release(uint_t frameID, buttonID_t ident);
	static event_t Motion(uint_t frameID, int absX,int absY, int relX,int relY);

	static event_t Translate(const event_t &ev);
};

/*
================
Ev_NameFromKey

Retrieve the name of a key from the key
================
*/
inline const char *Ev_NameFromKey(key_t key) {
	switch( key ) {
#define HANDLE(x) case x: return #x
	HANDLE(K_UNKNOWN);

	HANDLE(K_ESCAPE);
	HANDLE(K_A);
	HANDLE(K_B);
	HANDLE(K_C);
	HANDLE(K_D);
	HANDLE(K_E);
	HANDLE(K_F);
	HANDLE(K_G);
	HANDLE(K_H);
	HANDLE(K_I);
	HANDLE(K_J);
	HANDLE(K_K);
	HANDLE(K_L);
	HANDLE(K_M);
	HANDLE(K_N);
	HANDLE(K_O);
	HANDLE(K_P);
	HANDLE(K_Q);
	HANDLE(K_R);
	HANDLE(K_S);
	HANDLE(K_T);
	HANDLE(K_U);
	HANDLE(K_V);
	HANDLE(K_W);
	HANDLE(K_X);
	HANDLE(K_Y);
	HANDLE(K_Z);
	HANDLE(K_F1);
	HANDLE(K_F2);
	HANDLE(K_F3);
	HANDLE(K_F4);
	HANDLE(K_F5);
	HANDLE(K_F6);
	HANDLE(K_F7);
	HANDLE(K_F8);
	HANDLE(K_F9);
	HANDLE(K_F10);
	HANDLE(K_F11);
	HANDLE(K_F12);
	HANDLE(K_GRAVE);
	HANDLE(K_1);
	HANDLE(K_2);
	HANDLE(K_3);
	HANDLE(K_4);
	HANDLE(K_5);
	HANDLE(K_6);
	HANDLE(K_7);
	HANDLE(K_8);
	HANDLE(K_9);
	HANDLE(K_0);
	HANDLE(K_MINUS);
	HANDLE(K_EQUAL);
	HANDLE(K_BACKSPACE);
	HANDLE(K_TAB);
	HANDLE(K_LBRACKET);
	HANDLE(K_RBRACKET);
	HANDLE(K_BACKSLASH);
	HANDLE(K_ENTER);
	HANDLE(K_CAPSLOCK);
	HANDLE(K_LSHIFT);
	HANDLE(K_RSHIFT);
	HANDLE(K_SEMICOLON);
	HANDLE(K_QUOTE);
	HANDLE(K_COMMA);
	HANDLE(K_PERIOD);
	HANDLE(K_SLASH);
	HANDLE(K_LSUPER);
	HANDLE(K_LALT);
	HANDLE(K_RALT);
	HANDLE(K_RSUPER);
	HANDLE(K_MENU);
	HANDLE(K_LCTRL);
	HANDLE(K_RCTRL);
	HANDLE(K_PRNTSCRN);
	HANDLE(K_SCROLLLOCK);
	HANDLE(K_BREAK);
	HANDLE(K_INS);
	HANDLE(K_DEL);
	HANDLE(K_HOME);
	HANDLE(K_END);
	HANDLE(K_PGUP);
	HANDLE(K_PGDN);
	HANDLE(K_UP);
	HANDLE(K_DOWN);
	HANDLE(K_LEFT);
	HANDLE(K_RIGHT);
	HANDLE(K_NUMLOCK);
	HANDLE(K_NUMDIV);
	HANDLE(K_NUMMUL);
	HANDLE(K_NUMSUB);
	HANDLE(K_NUMADD);
	HANDLE(K_NUMENTER);
	HANDLE(K_NUMDOT);
	HANDLE(K_NUM1);
	HANDLE(K_NUM2);
	HANDLE(K_NUM3);
	HANDLE(K_NUM4);
	HANDLE(K_NUM5);
	HANDLE(K_NUM6);
	HANDLE(K_NUM7);
	HANDLE(K_NUM8);
	HANDLE(K_NUM9);
	HANDLE(K_NUM0);
	HANDLE(K_SPACE);
#undef HANDLE
	}

	return "(unknown)";
}
/*
================
Ev_NameFromButton

Retrieve the name of a button
================
*/
inline const char *Ev_NameFromButton(buttonID_t button) {
	switch( button ) {
#define HANDLE(x) case x: return #x
	HANDLE(BUTTON_MLEFT);
	HANDLE(BUTTON_MRIGHT);
	HANDLE(BUTTON_MWHEEL);
	HANDLE(BUTTON_MX1);
	HANDLE(BUTTON_MX2);
	HANDLE(BUTTON_MWHEELDOWN);
	HANDLE(BUTTON_MWHEELUP);
	HANDLE(BUTTON_UP);
	HANDLE(BUTTON_LEFT);
	HANDLE(BUTTON_DOWN);
	HANDLE(BUTTON_RIGHT);

	HANDLE(BUTTON_PREVWEAPON);
	HANDLE(BUTTON_NEXTWEAPON);
	HANDLE(BUTTON_WEAPON1);
	HANDLE(BUTTON_WEAPON2);
	HANDLE(BUTTON_WEAPON3);
	HANDLE(BUTTON_WEAPON4);
	HANDLE(BUTTON_WEAPON5);
	HANDLE(BUTTON_WEAPON6);
	HANDLE(BUTTON_WEAPON7);
	HANDLE(BUTTON_WEAPON8);
	HANDLE(BUTTON_WEAPON9);
	HANDLE(BUTTON_WEAPON0);
	HANDLE(BUTTON_USE1);
	HANDLE(BUTTON_USE2);
	HANDLE(BUTTON_SPEED);
	HANDLE(BUTTON_PREVTARGET);
	HANDLE(BUTTON_NEXTTARGET);

	HANDLE(BUTTON_NET_TALK);
	HANDLE(BUTTON_NET_VOICE);

	HANDLE(BUTTON_QUIT);
#undef HANDLE
	}

	return "(unknown)";
}
/*
================
Ev_NameFromEvent

Retrieve the name of an event
================
*/
inline const char *Ev_NameFromEvent(eventKind_t kind) {
	switch( kind ) {
#define HANDLE(x) case x: return #x
	HANDLE(EV_NULL);

	HANDLE(EV_KEYPRESS);
	HANDLE(EV_KEYRELEASE);

	HANDLE(EV_PRESS);
	HANDLE(EV_RELEASE);

	HANDLE(EV_MOTION);
#undef HANDLE
	}

	return "(unknown)";
}

/*
================
event_t::KeyPress

Create a key press event
================
*/
event_t event_t::KeyPress(uint_t frameID, key_t ident) {
	event_t ev;

	ev.kind = EV_KEYPRESS;
	ev.frame = frameID;
	ev.key.ident = ident;

	return ev;
}
/*
================
event_t::KeyRelease

Create a key release event
================
*/
event_t event_t::KeyRelease(uint_t frameID, key_t ident) {
	event_t ev;

	ev.kind = EV_KEYRELEASE;
	ev.frame = frameID;
	ev.key.ident = ident;

	return ev;
}
/*
================
event_t::Press

Create a press event
================
*/
event_t event_t::Press(uint_t frameID, buttonID_t ident) {
	event_t ev;

	ev.kind = EV_PRESS;
	ev.frame = frameID;
	ev.button.ident = ident;

	return ev;
}
/*
================
event_t::Release

Create a release event
================
*/
event_t event_t::Release(uint_t frameID, buttonID_t ident) {
	event_t ev;

	ev.kind = EV_RELEASE;
	ev.frame = frameID;
	ev.button.ident = ident;

	return ev;
}
/*
================
event_t::Motion

Create a motion event
================
*/
event_t event_t::Motion(uint_t frameID, int absX,int absY, int relX,int relY) {
	event_t ev;

	ev.kind = EV_MOTION;
	ev.frame = frameID;

	ev.motion.abs[ 0 ] = (float)absX;
	ev.motion.abs[ 1 ] = (float)absY;

	ev.motion.rel[ 0 ] = (float)relX;
	ev.motion.rel[ 1 ] = (float)relY;

	return ev;
}

/*
================
event_t::Translate

Translate a generic event into a game-specific event
NOTE: Do not use within level editor
================
*/
inline event_t event_t::Translate(const event_t &ev) {
	event_t nev;

	nev.kind = ev.kind;
	nev.frame = ev.frame;

	switch( ev.kind ) {
	case EV_NULL:
		break;

	case EV_KEYPRESS:
	case EV_KEYRELEASE:
		//
		//	TODO: Handle binds here
		//
#define TRANSLATE( x )\
	{nev.kind = ev.kind==EV_KEYPRESS ? EV_PRESS : EV_RELEASE;\
	nev.button.ident = (buttonID_t)(x);}
		switch( ev.key.ident ) {
		case K_W:
		case K_UP:
			TRANSLATE( BUTTON_UP );
			break;
		case K_A:
		case K_LEFT:
			TRANSLATE( BUTTON_LEFT );
			break;
		case K_S:
		case K_DOWN:
			TRANSLATE( BUTTON_DOWN );
			break;
		case K_D:
		case K_RIGHT:
			TRANSLATE( BUTTON_RIGHT );
			break;

		case K_1:
		case K_2:
		case K_3:
		case K_4:
		case K_5:
		case K_6:
		case K_7:
		case K_8:
		case K_9:
		case K_0:
			TRANSLATE( BUTTON_WEAPON1 + (ev.key.ident - K_1) );
			break;

		case K_E:
			TRANSLATE( BUTTON_USE1 );
			break;
		case K_Q:
			TRANSLATE( BUTTON_USE2 );
			break;

		case K_LSHIFT:
		case K_RSHIFT:
			TRANSLATE( BUTTON_SPEED );
			break;

		case K_G:
			TRANSLATE( BUTTON_PREVTARGET );
			break;
		case K_F:
			TRANSLATE( BUTTON_NEXTTARGET );
			break;

		case K_T:
			TRANSLATE( BUTTON_NET_TALK );
			break;
		case K_V:
			TRANSLATE( BUTTON_NET_VOICE );
			break;

		case K_ESCAPE:
			TRANSLATE( BUTTON_QUIT );
			break;

		default:
			nev.key.ident = ev.key.ident;
			break;
#undef TRANSLATE
		}
		break;

	case EV_PRESS:
	case EV_RELEASE:
		if( ev.button.ident==BUTTON_MWHEELUP ) {
			nev.button.ident = BUTTON_PREVWEAPON;
			break;
		}
		if( ev.button.ident==BUTTON_MWHEELDOWN ) {
			nev.button.ident = BUTTON_NEXTWEAPON;
			break;
		}

		if( ev.button.ident==BUTTON_MLEFT ) {
			nev.button.ident = BUTTON_USE1;
			break;
		}
		if( ev.button.ident==BUTTON_MRIGHT ) {
			nev.button.ident = BUTTON_USE2;
			break;
		}

		nev.button.ident = ev.button.ident;
		break;

	case EV_MOTION:
		nev.motion.abs[ 0 ] = ev.motion.abs[ 0 ];
		nev.motion.abs[ 1 ] = ev.motion.abs[ 1 ];

		nev.motion.rel[ 0 ] = ev.motion.rel[ 0 ];
		nev.motion.rel[ 1 ] = ev.motion.rel[ 1 ];
		break;
	}

	return nev;
}

class EventQueue {
public:
	static const uint_t MAX_EVENTS = 128;

					EventQueue();
					~EventQueue();

	void			Push(const event_t &ev);

	bool			Empty() const;

	event_t			Peek() const;
	event_t			Pull();

	void			Erase();
	void			Clear();

protected:
	event_t			events[ MAX_EVENTS ];
	uint_t			eventPtr, numEvents;
};

/*
================
EventQueue::EventQueue

Construct the event queue
================
*/
EventQueue::EventQueue(): events(), eventPtr(0), numEvents(0) {
}
/*
================
EventQueue::~EventQueue

Destruct the event queue
================
*/
EventQueue::~EventQueue() {
}

/*
================
EventQueue::Push

Add an event to the queue
NOTE: If the queue is full this will silently fail
================
*/
void EventQueue::Push(const event_t &ev) {
	if( numEvents==MAX_EVENTS ) {
		return;
	}

	events[ (eventPtr + numEvents++)%MAX_EVENTS ] = ev;
}

/*
================
EventQueue::Empty

Determine whether the event queue is empty
================
*/
bool EventQueue::Empty() const {
	return !numEvents;
}

/*
================
EventQueue::Peek

Retrieve the next event in the queue without removing it
================
*/
event_t EventQueue::Peek() const {
	static const event_t nullEvent = { EV_NULL, 0, {} };

	return numEvents ? events[ eventPtr%MAX_EVENTS ] : nullEvent;
}
/*
================
EventQueue::Pull

Retrieve the next event in the queue and remove it
================
*/
event_t EventQueue::Pull() {
	event_t ev;

	ev = Peek();
	Erase();

	return ev;
}

/*
================
EventQueue::Erase

Discard the first event in the queue
================
*/
void EventQueue::Erase() {
	if( !numEvents ) {
		return;
	}

	eventPtr++;
	numEvents--;
}
/*
================
EventQueue::Clear

Remove all events from the queue
================
*/
void EventQueue::Clear() {
	numEvents = 0;
	eventPtr = 0;
}

/*
===============================================================================

	Engine

===============================================================================
*/
#ifndef MAINWINDOW_CLASSNAME
# define MAINWINDOW_CLASSNAME "MainWindow_GL"
#endif

class Engine {
public:
	static const uint_t MAX_FRAME_CALLBACKS = 8;

					Engine();
					~Engine();

	bool			InitWindow();
	void			FiniWindow();
	bool			IsWindowInitialized() const;

	bool			InitOpenGL();
	void			FiniOpenGL();
	bool			IsOpenGLInitialized() const;

	void			ShowWindow();
	void			HideWindow();

	void			Quit();
	bool			IsRunning() const;

	bool			AddFrameCallback(Callback *cb);
	bool			RemoveFrameCallback(Callback *cb);
	void			Frame();

	uint_t			FrameID() const;
	static bool		IsFrameNewer(uint_t frst, uint_t scnd);

	void			QueEvent(const event_t &ev);
	bool			EventReady();
	event_t			GetEvent();

	void			EnterMenu();
	void			LeaveMenu();
	bool			InMenu() const;

	void			VPrintf(AX_PRINTF_PARM const char *fmt, va_list args);
	void			Printf(AX_PRINTF_PARM const char *fmt, ...)
						AX_PRINTF_ATTR(2, 3);
	void			DPrintf(AX_PRINTF_PARM const char *fmt, ...)
						AX_PRINTF_ATTR(2, 3);
	void			Error(AX_PRINTF_PARM const char *fmt, ...)
						AX_PRINTF_ATTR(2, 3);
	void			Warn(AX_PRINTF_PARM const char *fmt, ...)
						AX_PRINTF_ATTR(2, 3);

	void			DPrintEvent(const event_t &ev);

protected:
#if _WIN32
	HWND			window;
	HDC				dc;
	HGLRC			rc;
#else
# error "TODO: Implement this platform"
#endif

	bool			running;
	uint_t			frameID;

	Callback *		frameCallbacks[ MAX_FRAME_CALLBACKS ];
	uint_t			numFrameCallbacks;

	EventQueue		events;

	uint_t			menuNest;
	int				lastMouseX;
	int				lastMouseY;

	static LRESULT CALLBACK MsgProc_f(HWND,UINT,WPARAM,LPARAM);
};

Engine *			engine = AX_NULL;

/*
================
Engine::Engine

Construct the common subsystem
================
*/
Engine::Engine()
#if _WIN32
: window(0), dc(0), rc(0),
#else
# error "TODO: Implement this platform"
#endif
running(true), frameID(0), frameCallbacks(), numFrameCallbacks(0), events(),
menuNest(0), lastMouseX(0),lastMouseY(0) {
}
/*
================
Engine::~Engine

Destruct the common subsystem
================
*/
Engine::~Engine() {
	if( IsOpenGLInitialized() ) {
		FiniOpenGL();
	}

	if( IsWindowInitialized() ) {
		FiniWindow();
	}
}

/*
================
Engine::InitWindow

Initialize the window (make it)
================
*/
bool Engine::InitWindow() {
#if _WIN32
	static const DWORD exstyle=0;
	static const DWORD style=WS_OVERLAPPEDWINDOW;
	static const int w=640, h=480;
	WNDCLASSEXA wc;
	RECT rc;

	AX_ASSERT( engine!=AX_NULL ); //needed for window

	if( !!window ) {
		Warn( "InitWindow() already called" );
		return true;
	}

	wc.cbSize			= sizeof(wc);
	wc.style			= 0;
	wc.lpfnWndProc		= (WNDPROC)MsgProc_f;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= GetModuleHandleA( AX_NULL );
	wc.hIcon			= LoadIconA( NULL, IDI_APPLICATION );
	wc.hCursor			= LoadCursorA( NULL, IDC_ARROW );
	wc.hbrBackground	= AX_NULL;
	wc.lpszMenuName		= AX_NULL;
	wc.lpszClassName	= MAINWINDOW_CLASSNAME;
	wc.hIconSm			= LoadIconA( NULL, IDI_APPLICATION );

	if( !RegisterClassExA( &wc ) ) {
		RuntimeError_file( "Failed to RegisterClassExA()" );
		return false;
	}

	rc.left		= GetSystemMetrics(SM_CXSCREEN)/2 - w/2;
	rc.top		= GetSystemMetrics(SM_CYSCREEN)/2 - h/2;
	rc.right	= rc.left + w;
	rc.bottom	= rc.top  + h;

	if( !AdjustWindowRectEx( &rc, style, FALSE, exstyle ) ) {
		UnregisterClassA( MAINWINDOW_CLASSNAME, wc.hInstance );
		RuntimeError_file( "Failed to AdjustWindowRectEx()" );
		return false;
	}

	lastMouseX = -1;
	lastMouseY = -1;

	window = CreateWindowExA( exstyle, MAINWINDOW_CLASSNAME, APPTITLE, style,
		rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, (HWND)0,
		(HMENU)0, wc.hInstance, AX_NULL );
	if( !window ) {
		UnregisterClassA( MAINWINDOW_CLASSNAME, wc.hInstance );
		RuntimeError_file( "Failed to CreateWindowExA()" );
		return false;
	}

	return true;
#else
# error "TODO: Implement this platform"
#endif
}
/*
================
Engine::FiniWindow

Finish using the window (destroy it)
================
*/
void Engine::FiniWindow() {
#if _WIN32
	if( !window ) {
		Warn( "FiniWindow() already called or InitWindow() not called" );
		return;
	}

	DestroyWindow( window );
	window = (HWND)0;

	UnregisterClassA( MAINWINDOW_CLASSNAME, GetModuleHandleA( AX_NULL ) );
#else
# error "TODO: Implement this platform"
#endif
}
/*
================
Engine::IsWindowInitialized

Determine whether the window is initialized
================
*/
bool Engine::IsWindowInitialized() const {
#if _WIN32
	return !!window;
#else
# error "TODO: Implement this platform"
#endif
}

/*
================
Engine::InitOpenGL

Initialize OpenGL in the window
================
*/
bool Engine::InitOpenGL() {
#if _WIN32
	if( !!dc ) {
		return true;
	}

	dc = GetDC( window );
	if( !dc ) {
		RuntimeError_file( "GetDC() failed" );
		return false;
	}

	PIXELFORMATDESCRIPTOR pfd;

	pfd.nSize			= sizeof(pfd);
	pfd.nVersion		= 1;
	pfd.dwFlags			= PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|
						PFD_DOUBLEBUFFER;
	pfd.iPixelType		= PFD_TYPE_RGBA;
	pfd.cColorBits		= 32;
	pfd.cRedBits		= 0;
	pfd.cRedShift		= 0;
	pfd.cGreenBits		= 0;
	pfd.cGreenShift		= 0;
	pfd.cBlueBits		= 0;
	pfd.cBlueShift		= 0;
	pfd.cAlphaBits		= 8;
	pfd.cAlphaShift		= 0;
	pfd.cAccumBits		= 0;
	pfd.cAccumRedBits	= 0;
	pfd.cAccumGreenBits	= 0;
	pfd.cAccumBlueBits	= 0;
	pfd.cAccumAlphaBits	= 0;
	pfd.cDepthBits		= 24;
	pfd.cStencilBits	= 8;
	pfd.cAuxBuffers		= 0;
	pfd.iLayerType		= PFD_MAIN_PLANE;
	pfd.bReserved		= 0;
	pfd.dwLayerMask		= 0;
	pfd.dwVisibleMask	= 0;
	pfd.dwDamageMask	= 0;

	int pf = ChoosePixelFormat( dc, &pfd );
	if( !pf ) {
		ReleaseDC( window, dc ); dc = AX_NULL;
		RuntimeError_file( "ChoosePixelFormat() failed" );
		return false;
	}

	DescribePixelFormat( dc, pf, sizeof(pfd), &pfd );
	// NOTE: Windows XP sets this incorrectly
	if( !pfd.cStencilBits ) {
		pfd.cStencilBits = 8;
	}
	Printf( "Chose pixel format with RGBA %i, ZS %i:%i\n",
		(int)pfd.cColorBits, (int)pfd.cDepthBits, (int)pfd.cStencilBits );

	if( !SetPixelFormat( dc, pf, &pfd ) ) {
		ReleaseDC( window, dc ); dc = AX_NULL;
		RuntimeError_file( "SetPixelFormat() failed" );
		return false;
	}

	rc = wglCreateContext( dc );
	if( !rc ) {
		ReleaseDC( window, dc ); dc = AX_NULL;
		RuntimeError_file( "wglCreateContext() failed" );
		return false;
	}

	wglMakeCurrent( dc, rc );
	return true;
#else
# error "TODO: Implement this platform"
#endif
}

/*
================
Engine::FiniOpenGL

Finish using OpenGL
================
*/
void Engine::FiniOpenGL() {
#if _WIN32
	if( !rc ) {
		return;
	}

	wglMakeCurrent( AX_NULL, AX_NULL );
	wglDeleteContext( rc ); rc = AX_NULL;
	ReleaseDC( window, dc ); dc = AX_NULL;
#else
# error "TODO: Implement this platform"
#endif
}
/*
================
Engine::IsOpenGLInitialized

Determine whether OpenGL has been initialized
================
*/
bool Engine::IsOpenGLInitialized() const {
#if _WIN32
	return !!rc;
#else
# error "TODO: Implement this platform"
#endif
}

/*
================
Engine::ShowWindow

Makes the window visible
================
*/
void Engine::ShowWindow() {
#if _WIN32
	if( !window ) {
		Error( "Window is not initialized" );
		return;
	}

	::ShowWindow( window, SW_SHOW );
	::UpdateWindow( window );
#else
# error "TODO: Implement this platform"
#endif
}
/*
================
Engine::HideWindow

Makes the window invisible
================
*/
void Engine::HideWindow() {
#if _WIN32
	if( !window ) {
		Error( "Window is not initialized" );
		return;
	}

	::ShowWindow( window, SW_HIDE );
#else
# error "TODO: Implement this platform"
#endif
}

/*
================
Engine::Quit

Set the internal running state to false
================
*/
void Engine::Quit() {
	running = false;
}
/*
================
Engine::IsRunning

Determine whether the system is running or not
================
*/
bool Engine::IsRunning() const {
	return running;
}

/*
================
Engine::AddFrameCallback

Add a callback to be performed on Frame()
================
*/
bool Engine::AddFrameCallback(Callback *cb) {
	if( numFrameCallbacks==MAX_FRAME_CALLBACKS ) {
		Error( "Engine::AddFrameCallback(): Max callbacks reached" );
		return false;
	}

	frameCallbacks[ numFrameCallbacks++ ] = cb;
	return true;
}
/*
================
Engine::RemoveFrameCallback

Remove a frame callback previously added with AddFrameCallback()
================
*/
bool Engine::RemoveFrameCallback(Callback *cb) {
	uint_t i;

	for(i=0; i<numFrameCallbacks; i++) {
		if( frameCallbacks[ i ]!=cb ) {
			continue;
		}

		while(i + 1 < numFrameCallbacks) {
			frameCallbacks[ i ] = frameCallbacks[ i + 1 ];
			i++;
		}

		numFrameCallbacks--;
		return true;
	}

	Error( "Engine::RemoveFrameCallback(): Callback not found" );
	return false;
}

/*
================
Engine::Frame

Do game and render logic
================
*/
void Engine::Frame() {
#if _WIN32
	MSG msg;
	while( PeekMessageA( &msg, 0, 0, 0, PM_REMOVE ) ) {
		TranslateMessage( &msg );
		DispatchMessageA( &msg );
	}
#endif

	for(uint_t i=0; i<numFrameCallbacks; i++) {
		AX_ASSERT(!!frameCallbacks[i]);
		frameCallbacks[ i ]->Call();
	}

	SwapBuffers( dc );
	frameID++;
}

/*
================
Engine::FrameID

Retrieve the ID of the current frame
================
*/
uint_t Engine::FrameID() const {
	return frameID;
}
/*
================
Engine::IsFrameNewer

Determine whether the first frame is newer than the second frame
================
*/
bool Engine::IsFrameNewer(uint_t frst, uint_t scnd) {
	if( frst>scnd ) {
		//account for wrap-around
		return ( frst - scnd < (1<<(sizeof(uint_t)*4)) );
	}

	return false;
}

/*
================
Engine::QueEvent

Queue an event
================
*/
void Engine::QueEvent(const event_t &ev) {
	events.Push( ev );
}
/*
================
Engine::EventReady

Determines whether there's a waiting event in the queue
================
*/
bool Engine::EventReady() {
	return !events.Empty();
}
/*
================
Engine::GetEvent

Retrieve the next event in the queue, removing it in the process
================
*/
event_t Engine::GetEvent() {
	return events.Pull();
}

/*
================
Engine::EnterMenu

Increment the internal menu nesting level to indicate a menu has been entered.
A corresponding LeaveMenu() call must be made if the menu is left.
================
*/
void Engine::EnterMenu() {
	menuNest++;
}
/*
================
Engine::LeaveMenu

Decrement the internal menu nesting level to indicate a menu has been left.
Calling this does not automatically cause InMenu() to return false. If the menu
nesting level is greater than zero after this leaves InMenu() will still return
true.
================
*/
void Engine::LeaveMenu() {
	if( !menuNest ) {
		Error( "Excessive LeaveMenu()" );
		return;
	}

	menuNest--;
}
/*
================
Engine::InMenu

Determine whether any menu is open. Calling EnterMenu() will increment the
internal nesting level while LeaveMenu() will decrement it. This will return
true if the level is greater than zero.
================
*/
bool Engine::InMenu() const {
	return menuNest > 0;
}

/*
================
Engine::VPrintf

Perform printf-styled printing (with va_list); called internally
================
*/
void Engine::VPrintf(const char *fmt, va_list args) {
	vfprintf(stderr, fmt, args);
}
/*
================
Engine::Printf

Perform printf-styled printing
================
*/
void Engine::Printf(const char *fmt, ...) {
	va_list args;

	va_start(args, fmt);
	VPrintf(fmt, args);
	va_end(args);
}
/*
================
Engine::DPrintf

Perform printf-styled printing IF in developer mode
================
*/
void Engine::DPrintf(const char *fmt, ...) {
#if _DEBUG
	va_list args;

	va_start(args, fmt);
	VPrintf(fmt, args);
	va_end(args);
#else
	(void)fmt;
#endif
}
/*
================
Engine::Error

Write out an error with printf-styled formatting
================
*/
void Engine::Error(const char *fmt, ...) {
	va_list args;

	Printf("ERROR: ");
	va_start(args, fmt);
	VPrintf(fmt, args);
	va_end(args);
	Printf("\n");
}
/*
================
Engine::Warn

Write out a warning with printf-styled formatting
================
*/
void Engine::Warn(const char *fmt, ...) {
	va_list args;

	Printf("WARNING: ");
	va_start(args, fmt);
	VPrintf(fmt, args);
	va_end(args);
	Printf("\n");
}
/*
================
Engine::DPrintEvent

Write out an event in developer mode
================
*/
void Engine::DPrintEvent(const event_t &ev) {
	DPrintf( "Event :\n" );
	DPrintf( "  kind: %s\n", Ev_NameFromEvent( ev.kind ) );

	switch( ev.kind ) {
	case EV_NULL:
		break;

	case EV_KEYPRESS:
	case EV_KEYRELEASE:
		DPrintf( "    key.ident: %s\n", Ev_NameFromKey( ev.key.ident ) );
		break;

	case EV_PRESS:
	case EV_RELEASE:
		DPrintf( "    button.ident: %s\n",
			Ev_NameFromButton( ev.button.ident ) );
		break;

	case EV_MOTION:
		DPrintf( "    abs: %g, %g\n", ev.motion.abs[ 0 ],ev.motion.abs[ 1 ] );
		DPrintf( "    rel: %g, %g\n", ev.motion.rel[ 0 ],ev.motion.rel[ 1 ] );
		break;
	}

	DPrintf( "\n" );
}

/*
================
Engine::MsgProc_f

Main window procedure
================
*/
static key_t __KeyScancode_(LPARAM lparm) {
	return (key_t)(((lparm&0x00FF0000)>>16) | ((lparm&0x01000000)>>17));
}
LRESULT CALLBACK Engine::MsgProc_f(HWND window, UINT msg, WPARAM wparm,
LPARAM lparm) {
	AX_ASSERT( engine!=AX_NULL );

	uint_t frame = engine->FrameID();
	key_t key;
	int x, y;
	int lx, ly;

	switch( msg ) {
	case WM_CLOSE:
		engine->QueEvent( event_t::Press( frame, BUTTON_QUIT ) );
		engine->QueEvent( event_t::Release( frame, BUTTON_QUIT ) );
		return 0;
	case WM_DESTROY:
		engine->Quit();
		return 0;

	case WM_SYSKEYDOWN:
	case WM_KEYDOWN:
		key = __KeyScancode_( lparm );
		engine->QueEvent( event_t::KeyPress( frame, key ) );

		return 0;
	case WM_SYSKEYUP:
	case WM_KEYUP:
		key = __KeyScancode_( lparm );
		engine->QueEvent( event_t::KeyRelease( frame, key ) );

		return 0;

	case WM_LBUTTONDOWN:
		engine->QueEvent( event_t::Press( frame, BUTTON_MLEFT ) );
		return 0;
	case WM_LBUTTONUP:
		engine->QueEvent( event_t::Release( frame, BUTTON_MLEFT ) );
		return 0;

	case WM_RBUTTONDOWN:
		engine->QueEvent( event_t::Press( frame, BUTTON_MRIGHT ) );
		return 0;
	case WM_RBUTTONUP:
		engine->QueEvent( event_t::Release( frame, BUTTON_MRIGHT ) );
		return 0;

	case WM_MBUTTONDOWN:
		engine->QueEvent( event_t::Press( frame, BUTTON_MWHEEL ) );
		return 0;
	case WM_MBUTTONUP:
		engine->QueEvent( event_t::Release( frame, BUTTON_MWHEEL ) );
		return 0;

	case WM_XBUTTONDOWN:
		switch( GET_XBUTTON_WPARAM( wparm ) ) {
		case XBUTTON1:
			engine->QueEvent( event_t::Press( frame, BUTTON_MX1 ) );
			break;
		case XBUTTON2:
			engine->QueEvent( event_t::Press( frame, BUTTON_MX2 ) );
			break;

		default:
			break;
		}

		return 0;
	case WM_XBUTTONUP:
		switch( GET_XBUTTON_WPARAM( wparm ) ) {
		case XBUTTON1:
			engine->QueEvent( event_t::Release( frame, BUTTON_MX1 ) );
			break;
		case XBUTTON2:
			engine->QueEvent( event_t::Release( frame, BUTTON_MX2 ) );
			break;

		default:
			break;
		}

		return 0;

	case WM_MOUSEMOVE:
		x = (int)(short)LOWORD( lparm );
		y = (int)(short)HIWORD( lparm );

		lx = engine->lastMouseX==-1 ? x : engine->lastMouseX;
		ly = engine->lastMouseY==-1 ? y : engine->lastMouseY;

		engine->lastMouseX = x;
		engine->lastMouseY = y;

		// seems some excess queuing occurs
		if( x - lx == 0 && y - ly == 0 ) {
			break;
		}

		engine->QueEvent( event_t::Motion( frame, x,y, x - lx,y - ly ) );
		break;

	case WM_MOUSEWHEEL:
		y = ((int)(short)HIWORD( wparm ))/WHEEL_DELTA;

		while( y > 0 ) {
			y--;

			engine->QueEvent( event_t::Press(frame, BUTTON_MWHEELUP) );
			engine->QueEvent( event_t::Release(frame, BUTTON_MWHEELUP) );
		}

		while( y < 0 ) {
			y++;

			engine->QueEvent( event_t::Press(frame, BUTTON_MWHEELDOWN) );
			engine->QueEvent( event_t::Release(frame, BUTTON_MWHEELDOWN) );
		}

		break;

	default:
		break;
	}

	return DefWindowProc( window, msg, wparm, lparm );
}

//============================================================================//
// * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * //
////////////////////////////////////////////////////////////////////////////////
//############################################################################//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//----------------------------------------------------------------------------//
//............................................................................//
//                                                                            //
//                 /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\                 //
//              //============================================\\              //
//          //====================================================\\          //
//      //============================================================\\      //
//  //====================================================================\\  //
////////////////////////////////////////////////////////////////////////////////
//****************************************************************************//
//*                                                                          *//
//* ======================================================================== *//
//*     --------------           --------------           --------------     *//
//*     MAIN GAME CODE           MAIN GAME CODE           MAIN GAME CODE     *//
//*     --------------           --------------           --------------     *//
//* ======================================================================== *//
//*                                                                          *//
//****************************************************************************//
////////////////////////////////////////////////////////////////////////////////
//  \\====================================================================//  //
//      \\============================================================//      //
//          \\====================================================//          //
//              \\============================================//              //
//                 \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/                 //
//                                                                            //
//............................................................................//
//----------------------------------------------------------------------------//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//############################################################################//
////////////////////////////////////////////////////////////////////////////////
// * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * //
//============================================================================//

/*
===============================================================================

	Game
	Do the main game tasks

===============================================================================
*/
class Game {
public:
				Game();
				~Game();

	bool		Init();
	void		Fini();
	bool		IsInitialized() const;

	void		Frame();

	static void	Main();

protected:
	Image *		tileset;
};

Game *game = AX_NULL;

/*
================
Game::Game

Constructor
================
*/
Game::Game() {
}
/*
================
Game::~Game

Destructor
================
*/
Game::~Game() {
}

/*
================
Game::Init

Initialize the game
================
*/
bool Game::Init() {
	engine->ShowWindow();

	tileset = Image::LoadTGA( BASEDIR "/tileset.tga" );
	if( !tileset ) {
		engine->Error( "Failed to load tileset" );
		return false;
	}

	if( !tileset->LoadGL() ) {
		delete tileset;
		tileset = AX_NULL;
		engine->Error( "Failed to tileset->LoadGL()" );
		return false;
	}

	return true;
}
/*
================
Game::Fini

Finish the game
================
*/
void Game::Fini() {
	if( !tileset ) {
		return;
	}

	delete tileset;
}
/*
================
Game::IsInitialized

Determine whether the game is initialized
================
*/
bool Game::IsInitialized() const {
	return !!tileset;
}

/*
================
Game::Frame

Run game logic for one frame
================
*/
void Game::Frame() {
	while( engine->EventReady() ) {
		event_t ev = event_t::Translate( engine->GetEvent() );
		//engine->DPrintEvent( ev ); //enable to see what's coming through

		switch(ev.kind) {
		case EV_RELEASE:
			if( ev.button.ident == BUTTON_QUIT ) {
				engine->Quit();
				return;
			}
			break;

		case EV_KEYRELEASE:
			if( ev.key.ident == K_F12 ) {
				RuntimeError_file( "F12" );
				return;
			}
			if( ev.key.ident == K_F11 ) {
				RuntimeError( "F11" );
				return;
			}
			break;

		default:
			break;
		}
	}

	glClearColor( 0.1f,0.3f,0.5f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );

	glBindTexture( GL_TEXTURE_2D, tileset->GLImage() );
	glDrawPixels(tileset->ResX(),tileset->ResY(), GL_RGBA, GL_UNSIGNED_BYTE, tileset->Data());
}

/*
================
GameMain

Main game entry point
================
*/
void Game::Main() {
	Game gameLocal;

	game = &gameLocal;

	if( !game->Init() ) {
		engine->Error( "Failed to initialize game" );
		return;
	}

	auto gameFrameCallback = MakeCallback( game, &Game::Frame );
	engine->AddFrameCallback( &gameFrameCallback );

	while( engine->IsRunning() ) {
		engine->Frame();
	}

	game->Fini();

	game = AX_NULL;
}

/*
================
AppMain

Main entry point before game running
================
*/
int AppMain(int argc, char **argv) {
	char buf[4096];

	buf[0] = '\0';

	Mem::Init();

	try {
		Engine engineLocal;

		engine = &engineLocal;

		if( !engine->InitWindow() ) {
			RuntimeError( "engine->InitWindow() failed" );
			return EXIT_FAILURE;
		}

		if( !engine->InitOpenGL() ) {
			RuntimeError( "engine->InitOpenGL() failed" );
			return EXIT_FAILURE;
		}

		//
		//	TODO: Check for "-editor" in args
		//
		(void)argc;
		(void)argv;

		// run the game code
		Game::Main();
		//Editor::Main();

		engine->FiniOpenGL();
		engine->FiniWindow();

		engine = AX_NULL;
		return EXIT_SUCCESS;
	} catch(Exception_file &except) {
#ifndef __STDC_WANT_SECURE_LIB__
# define sprintf_s snprintf
#endif
		sprintf_s(buf,sizeof(buf), "ERROR: %s(%i): %s",
			except.File(),except.Line(), except.What());
	} catch(Exception &except) {
		sprintf_s(buf,sizeof(buf), "ERROR: %s", except.What());
	} catch(...) {
		sprintf_s(buf,sizeof(buf), "ERROR: Unhandled exception");
#undef sprintf_s
	}

	fprintf(stderr, "%s\n", buf);
	fflush(stderr);

#if _WIN32
	MessageBoxA( GetActiveWindow(), buf, "ERROR", MB_ICONERROR|MB_OK );
# if _DEBUG
	if( IsDebuggerPresent() ) {
		DebugBreak();
	}
# endif
#endif

	return EXIT_FAILURE;
}

int main(int argc, char **argv) {
	return AppMain( argc, argv );
}
#if _WIN32
# if _MSC_VER
#  pragma warning(push)
#  pragma warning(disable:28251) //inconsistent annotations
# endif
int WINAPI WinMain(HINSTANCE,HINSTANCE,LPSTR,int) {
	return AppMain( __argc, __argv );
}
# if _MSC_VER
#  pragma warning(pop)
# endif
#endif
