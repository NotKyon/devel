#ifndef SYSTEM_H
#define SYSTEM_H

#include "memory.h"

#include <stdarg.h>

void sys_LogV(const char *format, va_list args);
void sys_Log(const char *format, ...);
void sys_Error(const char *format, ...);
void sys_Warning(const char *format, ...);

#if MK_X86 || MK_X86_64 || __i386__ || __x86_64__ || _M_IX86
# if __GNUC__ || __clang__
#  define BREAKPOINT() asm("int $3")
# else
#  define BREAKPOINT() __asm int 3
# endif
#else
# if __GNUC__ || __clang__
#  define BREAKPOINT() __builtin_trap()
# else
#  error "Define BREAKPOINT()"
# endif
#endif

#define ASSERT(x)\
	if (!(x)){\
		sys_Error("%s(%u): ASSERT(%s) failed!", __FILE__, __LINE__, #x);\
		BREAKPOINT();\
	}

#endif
