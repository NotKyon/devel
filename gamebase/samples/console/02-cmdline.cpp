#define GB_INIT 1
#include <gamebase.h>

//
//	Many apps need to monitor the arguments they're given on the command-line.
//	You can evaluate the arguments yourself using gbCountAppArgs() and
//	gbAppArg()!
//

void GBMain() {
	size_t i;

	// The app's name is not included in the arguments
	for(i=0; i<gbCountAppArgs(); i++)
		gbPrint(gbAppArg(i));
}
