/*
 *	Build Tool
 *	Written by Aaron J. Miller <nocannedmeat@gmail.com>, 2012
 *
 *	This tool is used to simplify the build process of simple interdependent
 *	programs/libraries that need to be (or would benefit from being) distributed
 *	without requiring make/scons/<other builder program>.
 *
 *	The entire program is written in one source file to make it easier to build.
 *	To build this program in debug mode, run the following in your terminal:
 *	$ gcc -g -DDEBUG -W -Wall -pedantic -std=c99 -o mk mk.c
 *
 *	To build this program in release mode, run the following:
 *	$ gcc -O3 -fomit-frame-pointer -W -Wall -pedantic -std=c99 -o mk mk.c
 *
 *	Also, I apologize for the quality (or rather, the lack thereof) of the code.
 *	I intend to improve it over time, make some function names more consistent,
 *	and provide better internal source code documentation.
 *
 *	I did not attempt to compile with VC++. I do not believe "%zu" works in VC,
 *	so you may have to modify as necessary. Under GCC, I get absolutely no
 *	warnings for ISO C99; no extensions used.
 *
 *	LICENSE
 *	=======
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if _WIN32
# if _MSC_VER
#  define _CRT_SECURE_NO_WARNINGS 1
# endif
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#else
# include <limits.h>
#endif
#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <dirent.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#ifndef PATH_MAX
# define PATH_MAX 256
#endif

#ifndef bitfield_t_defined
# define bitfield_t_defined 1
typedef size_t bitfield_t;
#endif

/* -------------------------------------------------------------------------- */
void errormessage(const char *message);
void errorexit(const char *message);
void error(const char *file, unsigned int line, const char *func,
const char *message);
void errorassert(const char *file, unsigned int line, const char *func,
const char *message);

/* macros for assert errors */
#if DEBUG||_DEBUG
# undef assert
# undef assertmsg
# if __i386__||__x86_64__
#  define breakpoint() __asm("int $3")
# else
#  define breakpoint()
# endif
# define assertmsg(x,m) if(!(x)){errorassert(__FILE__,__LINE__,__func__,m);\
	breakpoint();}
# define assert(x) assertmsg((x),"assert(" #x ") failed!")
#else
# undef assert
# undef assertmsg
# define assert(x) /*nothing*/
# define assertmsg(x,m) /*nothing*/
# define breakpoint() /*nothing*/
#endif

/* -------------------------------------------------------------------------- */
void *memory(void *p, size_t n);
const char *va(const char *format, ...);
size_t i_strcat(char *buf, size_t bufn, const char *src);
void i_strcpy(char *dst, size_t dstn, const char *src);
void i_strncpy(char *buf, size_t bufn, const char *src, size_t srcn);
char *i_strdup(const char *cstr);
char *duplicate(char *dst, const char *src);
char *append(char *dst, const char *src);
const char *getdir(char *buf, size_t n, const char *filename);
void extsubst(char *dst, size_t dstn, const char *src, const char *ext);
int shell(const char *format, ...);
int matchpath(const char *rpath, const char *apath);
int getintegraldate();

/* -------------------------------------------------------------------------- */
typedef struct array_s *array_t;

array_t newarray();
size_t arraycapacity(array_t arr);
size_t arraysize(array_t arr);
char **arraydata(array_t arr);
char *arrayelement(array_t arr, size_t i);
void setarrayelement(array_t arr, size_t i, const char *cstr);
void cleararray(array_t arr);
void deletearray(array_t arr);
void deleteallarrays();
void resizearray(array_t arr, size_t n);
void arraypush(array_t arr, const char *cstr);
void printarray(array_t arr);

/* -------------------------------------------------------------------------- */
typedef struct buffer_s *buffer_t;

buffer_t newbuffer();
void deletebuffer(buffer_t buf);
void buffererror(buffer_t buf, const char *message);
int loadbuffer(buffer_t buf, const char *path);
char readbuffer(buffer_t buf);
char peekbuffer(buffer_t buf);
char *getbufferdata(buffer_t buf);
char *getbufferptr(buffer_t buf);
const char *getbuffername(buffer_t buf);
unsigned int getbufferline(buffer_t buf);

/* -------------------------------------------------------------------------- */
void fs_init();
void fs_deinit();
char *fs_getcwd(char *cwd, size_t n);
int fs_enter(const char *path);
void fs_exit();
int fs_isfile(const char *path);
int fs_isdir(const char *path);
void fs_mkdirs(const char *dirs);
char *fs_realpath(const char *filename, char *resolvedname, size_t maxn);

/* -------------------------------------------------------------------------- */
typedef struct dep_s *dep_t;

dep_t newdependency(const char *name);
void deletedependency(dep_t dep);
void deletealldependencies();
const char *getdependencyfile(dep_t dep);
void pushdependency(dep_t dep, const char *name);
size_t dependencycount(dep_t dep);
const char *getdependency(dep_t dep, size_t i);
dep_t finddependency(const char *name);

/* -------------------------------------------------------------------------- */
char dep_read(buffer_t buf, char *cur, char *look);
int dep_lex(buffer_t buf, char *dst, size_t dstn, char *cur, char *look);
int readdeps(const char *filename);

/* -------------------------------------------------------------------------- */
typedef struct lib_s *lib_t;

lib_t newlib();
void deletelib(lib_t lib);
void deletealllibs();
void setlibname(lib_t lib, const char *name);
void setlibflags(lib_t lib, int sys, const char *flags);
const char *libname(lib_t lib);
const char *libflags(lib_t lib, int sys);
lib_t libbefore(lib_t lib);
lib_t libafter(lib_t lib);
lib_t firstlib();
lib_t lastlib();
lib_t findlib(const char *name);
void clearalllibsprocessed();
void clearlibprocessed(lib_t lib);
void setlibprocessed(lib_t lib);
int testlibprocessed(lib_t lib);

/* -------------------------------------------------------------------------- */
typedef struct project_s *project_t;

enum {
	kProjType_Library,
	kProjType_DynamicLibrary,
	kProjType_Executable
};

enum {
	kProjSys_MSWin,
	kProjSys_Linux,
	kProjSys_MacOS,

	kNumProjSys
};

enum {
	kProjArch_X86,
	kProjArch_X86_64,
	kProjArch_ARM,
	kProjArch_PowerPC,
	kProjArch_MIPS
};

project_t newproject(project_t prnt);
void deleteproject(project_t proj);
void deleteallprojects();
project_t firstrootproject();
project_t lastrootproject();
project_t projectparent(project_t proj);
project_t firstproject(project_t proj);
project_t lastproject(project_t proj);
project_t projectbefore(project_t proj);
project_t projectafter(project_t proj);
void setprojectname(project_t proj, const char *name);
void setprojectpath(project_t proj, const char *path);
void setprojecttype(project_t proj, int type);
const char *projectname(project_t proj);
const char *projectpath(project_t proj);
const char *projectoutpath(project_t proj);
int projecttype(project_t proj);
void addprojectlib(project_t proj, const char *libname);
size_t projectlibcount(project_t proj);
const char *projectlib(project_t proj, size_t i);
void appendprojectlinkerflag(project_t proj, const char *flags);
const char *projectlinkerflags(project_t proj);
void addprojectsource(project_t proj, const char *src);
size_t projectsourcecount(project_t proj);
const char *projectsource(project_t proj, size_t i);
void addprojectspecialdir(project_t proj, const char *dir);
size_t projectspecialdircount(project_t proj);
const char *projectspecialdir(project_t proj, size_t i);
int isprojecttargetted(project_t proj);
void printprojects(project_t proj, const char *margin);
void calcprojectlibflags(project_t proj);

/* -------------------------------------------------------------------------- */
int isspecialdir(project_t proj, const char *name);
void enumspeciallibdirs(project_t proj);
void enumsources(project_t proj, const char *srcdir);
int calcprojectname(project_t proj, const char *path, const char *file);
project_t addproject(project_t prnt, const char *path, const char *file,
int type);
void findprojects(project_t prnt, const char *srcdir);
void findrootdirs(array_t srcdirs, array_t incdirs, array_t libdirs);
int makeobjdirs(project_t proj);

/* -------------------------------------------------------------------------- */
typedef struct autolink_s *autolink_t;

autolink_t newautolink();
void deleteautolink(autolink_t al);
void deleteallautolinks();
void setautolinkheader(autolink_t al, int sys, const char *header);
void setautolinkflags(autolink_t al, int sys, const char *flags);
const char *autolinkheader(autolink_t al, int sys);
const char *autolinkflags(autolink_t al, int sys);
autolink_t findautolink(int sys, const char *header);

/* -------------------------------------------------------------------------- */
int shouldcompile(const char *obj);
int shouldlink(const char *bin, int numbuilds);
const char *getcompileflags(project_t proj, const char *obj, const char *src);
void getprojectlibs(project_t proj, char *dst, size_t n);
const char *getlinkflags(project_t proj, const char *bin, array_t objs);
void getobjname(project_t proj, char *obj, size_t n, const char *src);
void getbinname(project_t proj, char *bin, size_t n);
int buildproject(project_t proj);
int buildprojects();

/* -------------------------------------------------------------------------- */
void pushsrcdir(const char *srcdir);
void pushincdir(const char *incdir);
void pushlibdir(const char *libdir);
void init(int argc, char **argv);

/* -------------------------------------------------------------------------- */
array_t g_targets = (array_t)0;
array_t g_srcdirs = (array_t)0;
array_t g_incdirs = (array_t)0;
array_t g_libdirs = (array_t)0;

enum {
	kFlag_ShowHelp_Bit = 0x01,
	kFlag_ShowVersion_Bit = 0x02,
	kFlag_Verbose_Bit = 0x04,
	kFlag_Release_Bit = 0x08,
	kFlag_Rebuild_Bit = 0x10,
	kFlag_NoCompile_Bit = 0x20,
	kFlag_NoLink_Bit = 0x40,
	kFlag_Clean_Bit = 0x80,
	kFlag_PrintHierarchy_Bit = 0x100,
	kFlag_Pedantic_Bit = 0x200,
	kFlag_Profile_Bit = 0x400
};
bitfield_t g_flags = 0;

char g_cc[256];
char g_cxx[256];

/*
 *	========================================================================
 *	ERROR HANDLING CODE
 *	========================================================================
 *	Display errors in an appropriate manner.
 */

/* display a simple error message */
void errormessage(const char *message) {
	fprintf(stderr, "error: %s", message);
	if (errno)
		fprintf(stderr, ": %s [%d]", strerror(errno), errno);
	fprintf(stderr, "\n");
}

/* exit with an error message; if errno is set, display its error */
void errorexit(const char *message) {
	errormessage(message);
	exit(EXIT_FAILURE);
}

/* provide a general purpose error, without terminating */
void error(const char *file, unsigned int line, const char *func,
const char *message) {
	fprintf(stderr, "error: ");

	if (file) {
		fprintf(stderr, "%s:", file);
		if (line)
			fprintf(stderr, "%u:", line);
		fprintf(stderr, " ");
	}

	if (func)
		fprintf(stderr, "in %s: ", func);

	if (message)
		fprintf(stderr, "%s", message);

	if ((errno&&message) || !message)
		fprintf(stderr, "%s%s [%d]", message?": ":"", strerror(errno), errno);

	fprintf(stderr, "\n");
}

/* provide an error for a failed assert */
void errorassert(const char *file, unsigned int line, const char *func,
const char *message) {
	error(file, line, func, message);
	fflush(stderr);
	/*exit(EXIT_FAILURE);*/
}

/*
 *	========================================================================
 *	UTILITY CODE
 *	========================================================================
 *	A set of utility functions for making the rest of this easier.
 */

/* manage memory; never worry about errors */
void *memory(void *p, size_t n) {
	errno = 0;

	if (!p&&n) {
		if (!(p = malloc(n)))
			errorexit("failed to allocate memory");
	} else if(p&&n) {
		void *q;

		if (!(q = realloc(p, n))) {
			free(p);
			errorexit("failed to reallocate memory");
		}

		p = q;
	} else {
		if(p&&!n)
			free(p);

		p = (void *)0;
	}

	return p;
}

/* provide printf-style formating on the fly; not recursive */
const char *va(const char *format, ...) {
	static char buf[65536];
	static size_t i = 0;
	va_list args;
	size_t j;
	int r;

	j = i;

	va_start(args, format);
	r = vsnprintf(&buf[j], sizeof(buf)-1-j, format, args);
	if (r==-1) {
		buf[sizeof(buf)-1] = 0;
		i = 0;
	} else {
		buf[j + r] = 0;
		i += r + 1;
	}
	va_end(args);

	return &buf[j];
}

/* secure strcat */
size_t i_strcat(char *buf, size_t bufn, const char *src) {
	size_t index, len;

	index = strlen(buf);
	len = strlen(src);

	if (index+len >= bufn)
		errorexit("detected overflow");

	memcpy((void *)&buf[index], (const void *)src, len+1);
	return index+len;
}

/* secure strcpy */
void i_strcpy(char *dst, size_t dstn, const char *src) {
	size_t i;

	for(i=0; src[i]; i++) {
		dst[i] = src[i];
		if (i==dstn-1)
			errorexit("detected overflow");
	}

	dst[i] = 0;
}

/* secure strncpy */
void i_strncpy(char *buf, size_t bufn, const char *src, size_t srcn) {
	if (srcn >= bufn)
		errorexit("detected overflow");

	strncpy(buf, src, srcn);
	buf[srcn] = 0;
}

/* secure strdup; uses memory() */
char *i_strdup(const char *cstr) {
	size_t n;
	char *p;

	assert(cstr != (const char *)0);

	n = strlen(cstr) + 1;

	p = (char *)memory((void *)0, n);
	memcpy((void *)p, (const void *)cstr, n);

	return p;
}

/* strdup() in-place alternative */
char *duplicate(char *dst, const char *src) {
	size_t n;

	if (src) {
		n = strlen(src) + 1;

		dst = (char *)memory((void *)dst, n);
		memcpy((void *)dst, (const void *)src, n);
	} else
		dst = (char *)memory((void *)dst, 0);

	return dst;
}

/* dynamic version of strcat() */
char *append(char *dst, const char *src) {
	size_t l, n;

	if (!src)
		return dst;

	l = dst ? strlen(dst) : 0;
	n = strlen(src) + 1;

	dst = (char *)memory((void *)dst, l + n);
	memcpy((void *)&dst[l], (const void *)src, n);

	return dst;
}

/* extract the directory part of a filename */
const char *getdir(char *buf, size_t n, const char *filename) {
	const char *p;

	if ((p = strrchr(filename, '/')) != (const char *)0) {
		i_strncpy(buf, n, filename, (p-filename) + 1);
		return &p[1];
	}

	*buf = 0;
	return filename;
}

/* copy the source string into the destination string, overwriting the
   extension (if present, otherwise appending) with 'ext' */
void extsubst(char *dst, size_t dstn, const char *src, const char *ext) {
	const char *p;

	assert(dst != (char *)0);
	assert(dstn > 1);
	assert(src != (const char *)0);

	p = strrchr(src, '.');
	if (!p)
		p = strchr(src, 0);

	assert(p != (const char *)0);

	i_strncpy(dst, dstn, src, p - src);
	i_strcpy(&dst[p - src], dstn - (size_t)(p - src), ext);
}

/* run a command in the shell */
int shell(const char *format, ...) {
	static char cmd[16384];
	va_list args;

	va_start(args, format);
	vsnprintf(cmd, sizeof(cmd)-1, format, args);
	cmd[sizeof(cmd)-1] = 0;
	va_end(args);

	if (g_flags & kFlag_Verbose_Bit) {
		fprintf(stdout, "%s\n", cmd);
		fflush(stdout);
	}

	return system(cmd);
}

/* determine whether a given relative path is part of an absolute path */
int matchpath(const char *rpath, const char *apath) {
	size_t rl, al;

	assert(rpath != (const char *)0);
	assert(apath != (const char *)0);

	rl = strlen(rpath);
	al = strlen(apath);

	if (rl > al)
		return 0;

	/*
	 *	TODO: Case-insensitive compare on Windows hosts.
	 */
	if (strcmp(&apath[al-rl], rpath) != 0)
		return 0;

	if (al-rl > 0) {
		if (apath[al-rl-1] != '/')
			return 0;
	}

	return 1;
}

/* retrieve the current date as an integral date -- YYYYMMDD, e.g., 20120223 */
int getintegraldate() {
	struct tm *p;
	time_t t;

	t = time(0);
	p = localtime(&t);

	return (p->tm_year + 1900)*10000 + (p->tm_mon + 1)*100 + p->tm_mday;
}

/*
 *	========================================================================
 *	DYNAMIC STRING ARRAY SYSTEM
 *	========================================================================
 *	Manages a dynamic array of strings. Functions similarly to the C++ STL's
 *	std::vector<std::string> class.
 */

struct array_s {
	size_t capacity;
	size_t size;
	char **data;

	struct array_s *prev, *next;
};
struct array_s *g_arr_head = (struct array_s *)0;
struct array_s *g_arr_tail = (struct array_s *)0;

/* create a new (empty) array */
array_t newarray() {
	array_t arr;

	arr = (array_t)memory((void *)0, sizeof(*arr));

	arr->capacity = 0;
	arr->size = 0;
	arr->data = (char **)0;

	arr->next = (struct array_s *)0;
	if ((arr->prev = g_arr_tail) != (struct array_s *)0)
		g_arr_tail->next = arr;
	else
		g_arr_head = arr;
	g_arr_tail = arr;

	return arr;
}

/* retrieve the current amount of memory allocated for the array */
size_t arraycapacity(array_t arr) {
	assert(arr != (array_t)0);

	return arr->capacity;
}

/* retrieve how many elements are in the array */
size_t arraysize(array_t arr) {
	assert(arr != (array_t)0);

	return arr->size;
}

/* retrieve the data of the array */
char **arraydata(array_t arr) {
	assert(arr != (array_t)0);

	return arr->data;
}

/* retrieve a single element of the array */
char *arrayelement(array_t arr, size_t i) {
	assert(arr != (array_t)0);
	assert(i < arr->size);

	return arr->data[i];
}

/* set a single element of the array */
void setarrayelement(array_t arr, size_t i, const char *cstr) {
	assert(arr != (array_t)0);
	assert(i < arr->size);

	if (!cstr)
		arr->data[i] = (char *)memory((void *)arr->data[i], 0);
	else {
		arr->data[i] = i_strdup(cstr);
	}
}

/* deallocate the internal memory used by the array */
void cleararray(array_t arr) {
	size_t i;

	assert(arr != (array_t)0);

	for(i=0; i<arr->size; i++)
		arr->data[i] = (char *)memory((void *)arr->data[i], 0);

	arr->data = (char **)memory((void *)arr->data, 0);
	arr->capacity = 0;
	arr->size = 0;
}

/* delete an array */
void deletearray(array_t arr) {
	if (!arr)
		return;

	cleararray(arr);

	if (arr->prev)
		arr->prev->next = arr->next;
	if (arr->next)
		arr->next->prev = arr->prev;

	if (g_arr_head==arr)
		g_arr_head = arr->next;
	if (g_arr_tail==arr)
		g_arr_tail = arr->prev;

	memory((void *)arr, 0);
}

/* delete all arrays */
void deleteallarrays() {
	while(g_arr_head)
		deletearray(g_arr_head);
}

/* set the new size of an array */
void resizearray(array_t arr, size_t n) {
	size_t i;

	assert(arr != (array_t)0);

	if (n > arr->capacity) {
		i = arr->capacity;
		arr->capacity += 4096/sizeof(void *);
		/*arr->capacity += 32;*/

		arr->data = (char **)memory((void *)arr->data,
			arr->capacity*sizeof(char *));

		memset((void *)&arr->data[i], 0, (arr->capacity - i)*sizeof(char *));
	}

	arr->size = n;
}

/* add an element to the array, resizing if necessary */
void arraypush(array_t arr, const char *cstr) {
	size_t i;

	assert(arr != (array_t)0);

	i = arraysize(arr);
	resizearray(arr, i+1);
	setarrayelement(arr, i, cstr);
}

/* display the contents of an array */
void printarray(array_t arr) {
	size_t i, n;

	assert(arr != (array_t)0);

	n = arraysize(arr);
	for(i=0; i<n; i++)
		printf("%2u. \"%s\"\n", (unsigned int)i, arrayelement(arr, i));

	printf("\n");
}

/* remove duplicate entries from an array */
void removearraydups(array_t arr) {
	const char *a, *b;
	size_t i, j, n;

	n = arraysize(arr);
	for(i=0; i<n; i++) {
		if (!(a = arrayelement(arr, i)))
			continue;

		for(j=i+1; j<n; j++) {
			if (!(b = arrayelement(arr, j)))
				continue;

			if (!strcmp(a, b))
				setarrayelement(arr, j, (const char *)0);
		}
	}
}

/*
 *	========================================================================
 *	BUFFER MANAGEMENT CODE
 *	========================================================================
 *	Manage input source file buffers (similar in nature to flex's buffer).
 *	Keeps track of the file's name and the current line.
 */

struct buffer_s {
	char *name;
	unsigned int line;

	char *buf;
	char *p;
};

/* create a new buffer */
buffer_t newbuffer() {
	buffer_t buf;

	buf = (buffer_t)memory((void *)0, sizeof(*buf));

	buf->name = (char *)0;
	buf->line = 0;
	buf->buf = (char *)0;
	buf->p = (char *)0;

	return buf;
}

/* delete an existing buffer */
void deletebuffer(buffer_t buf) {
	if (!buf)
		return;

	buf->name = (char *)memory((void *)buf->name, 0);
	buf->buf = (char *)memory((void *)buf->buf, 0);

	memory((void *)buf, 0);
}

/* generate an error for the buffer */
void buffererror(buffer_t buf, const char *message) {
	assert(buf != (buffer_t)0);
	assert(message != (const char *)0);

	error(buf->name, buf->line, (const char *)0, message);
}

/* load a file into a buffer */
int loadbuffer(buffer_t buf, const char *path) {
	size_t n, i, j, k;
	char tmp[4096];
	FILE *f;
	int tries;

	assert(buf != (buffer_t)0);
	assert(path != (const char *)0);

	buf->name = i_strdup(path);

	f = fopen(path, "rb");
	if (!f) {
		buffererror(buf, "failed to open file");
		return 0;
	}

	fseek(f, 0, SEEK_END);
	n = ftell(f);
	fseek(f, 0, SEEK_SET);

	buf->buf = (char *)memory((void *)0, n+1);

	j = 0;
	tries = 32;
	while(n > 0) {
		k = n < sizeof(tmp)-1 ? n : sizeof(tmp)-1;
		n -= k;
		if (!fread((void *)tmp, k, 1, f)) {
			if (--tries==0) {
				fclose(f);
				buffererror(buf, "failed to read file");
				return 0;
			}

			continue;
		}
		tmp[k] = 0;
		for(i=0; i<k; i++) {
			if (tmp[i]=='\r') {
				if (tmp[i+1]=='\n')
					i++;
				buf->buf[j++] = '\n';
			} else
				buf->buf[j++] = tmp[i];
		}
	}
	buf->buf[j] = 0;

	fclose(f);

	buf->line = 1;
	buf->p = buf->buf;
	return 1;
}

/* read a single character from the buffer */
char readbuffer(buffer_t buf) {
	char cur;

	assert(buf != (buffer_t)0);

	if (!buf->p)
		return 0;

	if (!*buf->p)
		return 0;

	cur = *buf->p++;

	return cur;
}

/* read a single character form the buffer without incrementing the position */
char peekbuffer(buffer_t buf) {
	assert(buf != (buffer_t)0);

	if (!buf->p)
		return 0;

	return *buf->p;
}

/* retrieve a pointer to the entire buffer */
char *getbufferdata(buffer_t buf) {
	assert(buf != (buffer_t)0);

	return buf->buf;
}

/* retrieve a pointer to the current position in the buffer */
char *getbufferptr(buffer_t buf) {
	assert(buf != (buffer_t)0);

	return buf->p;
}

/* retrieve the filename of a buffer */
const char *getbuffername(buffer_t buf) {
	assert(buf != (buffer_t)0);

	return buf->name;
}

/* retrieve the current line of a buffer */
unsigned int getbufferline(buffer_t buf) {
	assert(buf != (buffer_t)0);

	return buf->line;
}

/*
 *	========================================================================
 *	FILE SYSTEM MANAGEMENT CODE
 *	========================================================================
 *	This code deals with various file system related subjects. This includes
 *	making directories and finding where the executable is, etc.
 */

array_t g_fs_dirstack = (array_t)0;

/* initialize file system */
void fs_init() {
	atexit(fs_deinit);
}

/* deinitialize file system */
void fs_deinit() {
	deletearray(g_fs_dirstack);
	g_fs_dirstack = (array_t)0;
}

/* retrieve the current directory */
char *fs_getcwd(char *cwd, size_t n) {
	char *p;

	assert(cwd != (char *)0);
	assert(n > 1);

	if (!(p = getcwd(cwd, n)))
		errorexit("getcwd() failed");

#if _WIN32
	while((p=strchr(p,'\\')) != (char *)0)
		*p = '/';
#endif

	/*
	if ((p = strrchr(p, '/')) != (char *)0)
		if (*(p+1)==0)
			*p = 0;
	*/

	return cwd;
}

/* enter a directory (uses directory stack) */
int fs_enterex(const char *path, int showchdirerror) {
	char cwd[512];

	fs_getcwd(cwd, sizeof(cwd));

	if (!g_fs_dirstack)
		g_fs_dirstack = newarray();

	arraypush(g_fs_dirstack, cwd);

	if (chdir(path)==-1) {
		if (showchdirerror)
			errormessage(va("chdir(\"%s\") failed", path));
		if (chdir(cwd)==-1)
			errorexit("failed to restore current directory");

		return 0;
	}

	return 1;
}
int fs_enter(const char *path) {
	return fs_enterex(path, 1);
}

/* exit the current directory (uses directory stack) */
void fs_exit() {
	size_t i;

	assert(g_fs_dirstack != (array_t)0);
	assert(arraysize(g_fs_dirstack) > 0);

	i = arraysize(g_fs_dirstack)-1;

	if (chdir(arrayelement(g_fs_dirstack, i))==-1)
		errorexit(va("chdir(\"%s\") failed", arrayelement(g_fs_dirstack, i)));

	setarrayelement(g_fs_dirstack, i, (const char *)0);
	resizearray(g_fs_dirstack, i);
}

/* determine whether the path specified is a file. */
int fs_isfile(const char *path) {
	struct stat s;

	if (stat(path, &s)==-1)
		return 0;

	if (!S_ISREG(s.st_mode)) {
		errno = S_ISDIR(s.st_mode) ? EISDIR : EBADF;
		return 0;
	}

	return 1;
}

/* determine whether the path specified is a directory. */
int fs_isdir(const char *path) {
	struct stat s;

	if (stat(path, &s)==-1)
		return 0;

	if (!S_ISDIR(s.st_mode)) {
		errno = ENOTDIR;
		return 0;
	}

	return 1;
}

/* create a series of directories (e.g., a/b/c/d/...) */
void fs_mkdirs(const char *dirs) {
	/*
	 *	! This is old code !
	 *	Just ignore bad practices, mmkay?
	 */
	const char *p;
	char buf[512], *path;

	/* ignore the root directory */
	if (dirs[0]=='/') {
		buf[0] = dirs[0];
		path = &buf[1];
		p = &dirs[1];
	} else if(dirs[1]==':' && dirs[2]=='/') {
		buf[0] = dirs[0];
		buf[1] = dirs[1];
		buf[2] = dirs[2];
		path = &buf[3];
		p = &dirs[3];
	} else {
		path = &buf[0];
		p = &dirs[0];
	}

	/* make each directory, one by one */
	while(1) {
		if(*p=='/' || *p==0) {
			*path = 0;

			errno = 0;
#ifdef _WIN32
			mkdir(buf);
#else
			mkdir(buf, S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH);
#endif
			if (errno && errno!=EEXIST)
				errorexit(va("couldn't create directory \"%s\"", buf));

			if (!(*path++ = *p++))
				return;
			else if(*p==0) /* handle a '/' ending */
				return;
		} else
			*path++ = *p++;

		if (path==&buf[sizeof(buf)-1])
			errorexit("path is too long");
	}
}

/* find the real path to a file */
#ifdef _WIN32
char *fs_realpath(const char *filename, char *resolvedname, size_t maxn) {
	static char buf[512];
	size_t i;
	DWORD r;

	if (!(r = GetFullPathNameA(filename, sizeof(buf), buf, (char **)0))) {
		errno = ENOSYS;
		return (char *)0;
	} else if(r >= (DWORD)maxn) {
		errno = ERANGE;
		return (char *)0;
	}

	for(i=0; i<sizeof(buf); i++) {
		if (!buf[i])
			break;

		if (buf[i]=='\\')
			buf[i] = '/';
	}

	if (buf[1]==':') {
		if (buf[0]>='A' && buf[0]<='Z')
			buf[0] = buf[0] - 'A' + 'a';
	}

	i_strcpy(resolvedname, maxn, buf);

	return resolvedname;
}
#else /*#elif __linux__||__linux||linux*/
/*
char *fs_realpath(const char *filename, char *resolvedname, size_t maxn) {
	const char *p;
	char dir[512], *q;

	if ((p = getdir(dir, sizeof(dir), filename))==filename) {
		fs_getcwd(dir, sizeof(dir));
	} else {
		fs_enter(dir);
		fs_getcwd(dir, sizeof(dir));
		fs_exit();
	}

	assert(p != (const char *)0);

	snprintf(resolvedname, maxn-1, "%s%s", dir, p);
	resolvedname[maxn-1] = 0;

	if (fs_isdir(resolvedname)) {
		if ((q = strrchr(resolvedname, '/')) != (const char *)0) {
			if (q[1] != 0) {
				q = strchr(q, 0);
				i_strcat(q, maxn-(q-resolvedname), "/");
			}
		}
	}

	return resolvedname;
}
*/
/*
char *fs_realpath(const char *filename, char *resolvedname, size_t maxn) {
	static char buf[PATH_MAX + 1];
	char *q;

	if (maxn > PATH_MAX) {
		if (!realpath(filename, resolvedname))
			return (char *)0;

		resolvedname[PATH_MAX] = 0;
	} else if(!realpath(filename, buf)) {
		return (char *)0;

		buf[PATH_MAX] = 0;
		strncpy(resolvedname, buf, maxn);
		resolvedname[maxn-1] = 0;
	}

	printf("DEBUG: resolvedname(%i): %s\n", (int)PATH_MAX, resolvedname);
	fflush(stdout);

	if (fs_isdir(resolvedname)) {
		if ((q = strrchr(resolvedname, '/')) != (const char *)0) {
			if (q[1] != 0) {
				q = strchr(q, 0);
				i_strcat(q, maxn-(q-resolvedname), "/");
			}
		}
	}

	return resolvedname;
}
*/
char *fs_realpath(const char *filename, char *resolvedname, size_t maxn) {
	size_t namepart;
	char *p;
	char dir[PATH_MAX + 1];

	i_strcpy(dir, sizeof(dir), filename);
	if ((p = strrchr(dir, '/')) != (char *)0) {
		namepart = (p + 1) - dir;
		if (*(p + 1)==0)
			*p = 0;
		else
			*(p + 1) = 0;
	} else {
		namepart = 0;
		p = "./";
	}

	if (!fs_enterex(dir, 0))
		return (char *)0;
	if (!fs_getcwd(dir, sizeof(dir)))
	    return (char *)0;
	fs_exit();

	i_strcat(dir, sizeof(dir), &filename[namepart]);
	if (fs_isdir(dir))
		i_strcat(dir, sizeof(dir), "/");

	i_strcpy(resolvedname, maxn, dir);

	/*printf("DEBUG: fs_realpath(\"%s\"): \"%s\"\n", filename, resolvedname);
	  fflush(stdout);*/

	return resolvedname;
}
#endif

/*
 *	========================================================================
 *	DEPENDENCY TRACKER
 *	========================================================================
 *	Track dependencies and manage the general structure.
 */

struct dep_s {
	char *name;
	array_t deps;

	struct dep_s *prev, *next;
};
dep_t g_dep_head = (dep_t)0;
dep_t g_dep_tail = (dep_t)0;

/* create a new dependency list */
dep_t newdependency(const char *name) {
	dep_t dep;

	assert(name != (const char *)0);

	dep = (dep_t)memory((void *)0, sizeof(*dep));

	dep->name = i_strdup(name);
	dep->deps = newarray();

	dep->next = (dep_t)0;
	if ((dep->prev = g_dep_tail) != (dep_t)0)
		g_dep_tail->next = dep;
	else
		g_dep_head = dep;
	g_dep_tail = dep;

	return dep;
}

/* delete a dependency list */
void deletedependency(dep_t dep) {
	if (!dep)
		return;

	dep->name = (char *)memory((void *)dep->name, 0);
	deletearray(dep->deps);
	dep->deps = (array_t)0;

	if (dep->prev)
		dep->prev->next = dep->next;
	if (dep->next)
		dep->next->prev = dep->prev;

	if (g_dep_head==dep)
		g_dep_head = dep->next;
	if (g_dep_tail==dep)
		g_dep_tail = dep->prev;

	memory((void *)dep, 0);
}

/* delete all dependency lists */
void deletealldependencies() {
	while(g_dep_head)
		deletedependency(g_dep_head);
}

/* retrieve the name of the file a dependency list is tracking */
const char *getdependencyfile(dep_t dep) {
	assert(dep != (dep_t)0);

	return dep->name;
}

/* add a dependency to a list */
void pushdependency(dep_t dep, const char *name) {
	assert(dep != (dep_t)0);
	assert(name != (const char *)0);

	arraypush(dep->deps, name);
}

/* retrieve the number of dependencies in a list */
size_t dependencycount(dep_t dep) {
	assert(dep != (dep_t)0);

	return arraysize(dep->deps);
}

/* retrieve a dependency from a list */
const char *getdependency(dep_t dep, size_t i) {
	assert(dep != (dep_t)0);
	assert(i < dependencycount(dep));

	return arrayelement(dep->deps, i);
}

/* find a dependency */
dep_t finddependency(const char *name) {
	dep_t dep;

	for(dep=g_dep_head; dep; dep=dep->next) {
		if (!strcmp(dep->name, name))
			return dep;
	}

	return (dep_t)0;
}

/*
 *	========================================================================
 *	DEPENDENCY READER
 *	========================================================================
 *	Read dependencies, as produced by GCC, and put them into an array.
 */

enum {
	kDepTok_EOF = 0,
	kDepTok_Colon = ':',

	__kDepTok_Start__ = 256,
	kDepTok_Ident
};

/* internal function ported from previous version of 'readdeps' */
char dep_read(buffer_t buf, char *cur, char *look) {
	/*
	 *	! This is based on old code.
	 */

	assert(buf != (buffer_t)0);
	assert(cur != (char *)0);
	assert(look != (char *)0);

	*cur = readbuffer(buf);
	*look = peekbuffer(buf);

	return *cur;
}

/* read a token from the buffer */
int dep_lex(buffer_t buf, char *dst, size_t dstn, char *cur, char *look) {
	size_t i;

	assert(buf != (buffer_t)0);
	assert(dst != (char *)0);
	assert(dstn > 1);

	dst[i=0] = 0;

	if (!*cur) {
		do {
			if (!dep_read(buf, cur, look))
				return kDepTok_EOF;
		} while(*cur <= ' ');
	}

	if (*cur==':') {
		dst[0] = ':';
		dst[1] = 0;

		*cur = 0; /* force 'cur' to be retrieved again, else infinite loop */

		return kDepTok_Colon;
	}

	while(1) {
		if (*cur=='\\') {
			/*
			 *	! Original code expected that '\r' was possible, hence this
			 *	  setup was needed to make this portion easier. This needs to be
			 *	  fixed later.
			 */
			if (*look=='\n') {
				do {
					if (!dep_read(buf, cur, look))
						break;
				} while(*cur <= ' ');

				continue;
			}

			if (*look==' ') {
				dst[i++] = dep_read(buf, cur, look);
				dep_read(buf, cur, look); /*eat the space*/
				continue;
			}

			*cur = '/'; /* correct path reads */
		} else if(*cur==':') {
			if (*look<=' ') {
				dst[i] = 0;
				return kDepTok_Ident; /* leave 'cur' for the next call */
			}
		} else if(*cur<=' ') {
			dst[i] = 0;
			*cur = 0; /* force read on next call */
			return kDepTok_Ident;
		}

		dst[i++] = *cur;
		if (i==dstn) {
			buffererror(buf, "overflow detected");
			exit(EXIT_FAILURE);
		}

		dep_read(buf, cur, look);
	}
}

/* read dependencies from a file */
int readdeps(const char *filename) {
	buffer_t buf;
	dep_t dep;
	char lexan[512], ident[512], cur, look;
	int tok;

	buf = newbuffer();
	if (!loadbuffer(buf, filename)) {
		deletebuffer(buf);
		return 0;
	}

	lexan[0] = 0;
	cur = 0;
	look = 0;

	dep_read(buf, &cur, &look);

	dep = (dep_t)0;

	do {
		tok = dep_lex(buf, lexan, sizeof(lexan), &cur, &look);

		if (tok==kDepTok_Colon) {
			if (ident[0]) {
				dep = newdependency(ident);
				ident[0] = 0;
			}

			continue;
		}

		if (dep && ident[0]) {
			pushdependency(dep, ident);
			ident[0] = 0;
		}

		if (tok==kDepTok_Ident)
			i_strcpy(ident, sizeof(ident), lexan);
	} while(tok != kDepTok_EOF);

	deletebuffer(buf);
	return 1;
}

/*
 *	========================================================================
 *	LIBRARIAN
 *	========================================================================
 *	Manages individual libraries. Each library is abstracted with a platform
 *	independent name (e.g., "opengl") which is then mapped to platform
 *	dependent flags. (e.g., on Windows the above would be "-lopengl32," but on
 *	GNU/Linux it would be "-lGL.")
 */

enum {
	kLib_Processed_Bit = 0x01 /* indicates a library has been "processed" */
};

struct lib_s {
	char *name;
	char *flags[kNumProjSys];

	bitfield_t config;

	struct lib_s *prev, *next;
};

struct lib_s *g_lib_head = (struct lib_s *)0;
struct lib_s *g_lib_tail = (struct lib_s *)0;

/* create a new library */
lib_t newlib() {
	size_t i;
	lib_t lib;

	lib = (lib_t)memory((void *)0, sizeof(*lib));

	lib->name = (char *)0;
	for(i=0; i<sizeof(lib->flags)/sizeof(lib->flags[0]); i++)
		lib->flags[i] = (char *)0;

	lib->config = 0;

	lib->next = (struct lib_s *)0;
	if ((lib->prev = g_lib_tail) != (struct lib_s *)0)
		g_lib_tail->next = lib;
	else
		g_lib_head = lib;
	g_lib_tail = lib;

	return lib;
}

/* delete an existing library */
void deletelib(lib_t lib) {
	size_t i;

	if (!lib)
		return;

	lib->name = (char *)memory((void *)lib->name, 0);
	for(i=0; i<sizeof(lib->flags)/sizeof(lib->flags[0]); i++)
		lib->flags[i] = (char *)memory((void *)lib->flags[i], 0);

	if (lib->prev)
		lib->prev->next = lib->next;
	if (lib->next)
		lib->next->prev = lib->prev;

	if (g_lib_head==lib)
		g_lib_head = lib->next;
	if (g_lib_tail==lib)
		g_lib_tail = lib->prev;

	memory((void *)lib, 0);
}

/* delete all existing libraries */
void deletealllibs() {
	while(g_lib_head)
		deletelib(g_lib_head);
}

/* set the name of a library */
void setlibname(lib_t lib, const char *name) {
	assert(lib != (lib_t)0);

	lib->name = duplicate(lib->name, name);
}

/* set the flags of a library */
void setlibflags(lib_t lib, int sys, const char *flags) {
	assert(lib != (lib_t)0);
	assert(sys>=0 && sys<kNumProjSys);

	lib->flags[sys] = duplicate(lib->flags[sys], flags);
}

/* retrieve the name of a library */
const char *libname(lib_t lib) {
	assert(lib != (lib_t)0);

	return lib->name;
}

/* retrieve the flags of a library */
const char *libflags(lib_t lib, int sys) {
	assert(lib != (lib_t)0);
	assert(sys>=0 && sys<kNumProjSys);

	return lib->flags[sys];
}

/* retrieve the library before another */
lib_t libbefore(lib_t lib) {
	assert(lib != (lib_t)0);

	return lib->prev;
}

/* retrieve the library after another */
lib_t libafter(lib_t lib) {
	assert(lib != (lib_t)0);

	return lib->next;
}

/* retrieve the first library */
lib_t firstlib() {
	return g_lib_head;
}

/* retrieve the last library */
lib_t lastlib() {
	return g_lib_tail;
}

/* find a library by its name */
lib_t findlib(const char *name) {
	lib_t lib;

	assert(name != (const char *)0);

	for(lib=g_lib_head; lib; lib=lib->next) {
		if (!lib->name)
			continue;

		if (!strcmp(lib->name, name))
			return lib;
	}

	return (lib_t)0;
}

/* mark all libraries as "not processed" */
void clearalllibsprocessed() {
	lib_t lib;

	for(lib=g_lib_head; lib; lib=lib->next)
		lib->config &= ~kLib_Processed_Bit;
}

/* mark a library as "not processed" */
void clearlibprocessed(lib_t lib) {
	assert(lib != (lib_t)0);

	lib->config &= ~kLib_Processed_Bit;
}

/* mark a library as "processed" */
void setlibprocessed(lib_t lib) {
	assert(lib != (lib_t)0);

	lib->config |= kLib_Processed_Bit;
}

/* determine whether a library was marked as "processed" */
int testlibprocessed(lib_t lib) {
	assert(lib != (lib_t)0);

	return lib->config & kLib_Processed_Bit ? 1 : 0;
}

/*
 *	========================================================================
 *	AUTOLINK SYSTEM
 *	========================================================================
 *	This system maps which header files are used to access which libraries. This
 *	allows the dependency system to be exploited to reveal what flags need to be
 *	passed to the linker for the system to "just work."
 */

struct autolink_s {
	char *header[kNumProjSys];
	char *lib;

	struct autolink_s *prev, *next;
};
struct autolink_s *g_al_head = (struct autolink_s *)0;
struct autolink_s *g_al_tail = (struct autolink_s *)0;

/* allocate a new auto-link entry */
autolink_t newautolink() {
	autolink_t al;
	size_t i;

	al = (autolink_t)memory((void *)0, sizeof(*al));

	for(i=0; i<kNumProjSys; i++)
		al->header[i] = (char *)0;
	al->lib = (char *)0;

	al->next = (struct autolink_s *)0;
	if ((al->prev = g_al_tail) != (struct autolink_s *)0)
		g_al_tail->next = al;
	else
		g_al_head = al;
	g_al_tail = al;

	return al;
}

/* deallocate an existing auto-link entry */
void deleteautolink(autolink_t al) {
	size_t i;

	if (!al)
		return;

	for(i=0; i<kNumProjSys; i++)
		al->header[i] = (char *)memory((void *)al->header[i], 0);
	al->lib = (char *)memory((void *)al->lib, 0);

	if (al->prev)
		al->prev->next = al->next;
	if (al->next)
		al->next->prev = al->prev;

	if (g_al_head==al)
		g_al_head = al->next;
	if (g_al_tail==al)
		g_al_tail = al->prev;

	memory((void *)al, 0);
}

/* deallocate all existing auto-link entries */
void deleteallautolinks() {
	while(g_al_head)
		deleteautolink(g_al_head);
}

/* set a header for an auto-link entry (determines whether to auto-link) */
void setautolinkheader(autolink_t al, int sys, const char *header) {
	assert(al != (autolink_t)0);
	assert(sys>=0 && sys<kNumProjSys);

	al->header[sys] = duplicate(al->header[sys], header);
}

/* set the library an auto-link entry refers to */
void setautolinklib(autolink_t al, const char *libname) {
	assert(al != (autolink_t)0);

	al->lib = duplicate(al->lib, libname);
}

/* retrieve a header of an auto-link entry */
const char *autolinkheader(autolink_t al, int sys) {
	assert(al != (autolink_t)0);
	assert(sys>=0 && sys<kNumProjSys);

	return al->header[sys];
}

/* retrieve the library an auto-link entry refers to */
const char *autolinklib(autolink_t al) {
	assert(al != (autolink_t)0);

	return al->lib;
}

/* find an auto-link entry by header and system */
autolink_t findautolink(int sys, const char *header) {
	autolink_t al;

	assert(sys>=0 && sys<kNumProjSys);
	assert(header != (const char *)0);

	for(al=g_al_head; al; al=al->next) {
		if (!al->header[sys])
			continue;

		if (matchpath(al->header[sys], header))
			return al;
	}

	return (autolink_t)0;
}

/* retrieve the flags needed for linking to a library based on its header */
const char *autolink(int sys, const char *header) {
	autolink_t al;
	lib_t lib;

	assert(sys>=0 && sys<kNumProjSys);
	assert(header != (const char *)0);

	if (!(al = findautolink(sys, header)))
		return (const char *)0;

	if (!(lib = findlib(autolinklib(al))))
		return (const char *)0;

	return libflags(lib, sys);
}

/*
 *	========================================================================
 *	PROJECT MANAGEMENT
 *	========================================================================
 */

const int g_host_sys =
#if _WIN32 && !__CYGWIN__
	kProjSys_MSWin;
#elif __linux__||__linux||linux || __CYGWIN__
	kProjSys_Linux;
#elif __APPLE__
	kProjSys_MacOS;
#elif __unix__||__unix
	kProjSys_Linux; /* add just a kProjSys_Unix? */
#else
# error "OS not recognized."
#endif

const int g_host_arch =
#if __amd64__||__amd64||__x86_64__||x86_64||_M_X64
	kProjArch_X86_64;
#elif __i386__||__i386||_M_IX86
	kProjArch_X86;
#elif __arm__||_ARM
	kProjArch_ARM;
#elif __powerpc__||__powerpc||__POWERPC__||__ppc__||_M_PPC||_ARCH_PPC
	kProjArch_PowerPC;
#elif __mips__||__mips||mips||__MIPS__
	kProjArch_MIPS;
#else
# error "Architecture not recognized."
#endif

enum {
	kProjCfg_UsesCxx_Bit = 0x01,
	kProjCfg_NeedRelink_Bit = 0x02,
	kProjCfg_Linking_Bit = 0x04
};

struct project_s {
	char *name;
	char *path;
	char *outpath;
	int type;
	int sys;
	int arch;

	array_t sources;
	array_t specialdirs;
	array_t libs;
	array_t libdirs;

	char *linkerflags;

	bitfield_t config;

	struct project_s *prnt, **p_head, **p_tail;
	struct project_s *head, *tail;
	struct project_s *prev, *next;
};
struct project_s *g_proj_head = (struct project_s *)0;
struct project_s *g_proj_tail = (struct project_s *)0;

/* create a new (empty) project */
project_t newproject(project_t prnt) {
	project_t proj;
	size_t i, n;

	proj = (project_t)memory((void *)0, sizeof(*proj));

	proj->name = (char *)0;
	proj->path = (char *)0;
	proj->outpath = (char *)0;

	proj->type = kProjType_Executable;
	proj->sys = g_host_sys;
	proj->arch = g_host_arch;

	proj->sources = newarray();
	proj->specialdirs = newarray();
	proj->libs = newarray();
	proj->libdirs = newarray();

	n = arraysize(g_libdirs);
	for(i=0; i<n; i++)
		arraypush(proj->libdirs, arrayelement(g_libdirs, i));

	proj->linkerflags = (char *)0;

	proj->config = 0;

	proj->prnt = prnt;
	proj->p_head = prnt ? &prnt->head : &g_proj_head;
	proj->p_tail = prnt ? &prnt->tail : &g_proj_tail;
	proj->head = (struct project_s *)0;
	proj->tail = (struct project_s *)0;
	proj->next = (struct project_s *)0;
	if ((proj->prev = *proj->p_tail) != (struct project_s *)0)
		(*proj->p_tail)->next = proj;
	else
		*proj->p_head = proj;
	*proj->p_tail = proj;

	return proj;
}

/* delete an existing project */
void deleteproject(project_t proj) {
	if (!proj)
		return;

	while(proj->head)
		deleteproject(proj->head);

	proj->name = (char *)memory((void *)proj->name, 0);
	proj->path = (char *)memory((void *)proj->path, 0);
	proj->outpath = (char *)memory((void *)proj->outpath, 0);

	deletearray(proj->sources);
	deletearray(proj->specialdirs);
	deletearray(proj->libs);

	proj->linkerflags = (char *)memory((void *)proj->linkerflags, 0);

	if (proj->prev)
		proj->prev->next = proj->next;
	if (proj->next)
		proj->next->prev = proj->prev;

	assert(proj->p_head != (struct project_s **)0);
	assert(proj->p_tail != (struct project_s **)0);

	if (*proj->p_head==proj)
		*proj->p_head = proj->next;
	if (*proj->p_tail==proj)
		*proj->p_tail = proj->prev;

	memory((void *)proj, 0);
}

/* delete all projects */
void deleteallprojects() {
	while(g_proj_head)
		deleteproject(g_proj_head);
}

/* retrieve the first root project */
project_t firstrootproject() {
	return g_proj_head;
}

/* retrieve the last root project */
project_t lastrootproject() {
	return g_proj_tail;
}

/* retrieve the parent of a project */
project_t projectparent(project_t proj) {
	assert(proj != (project_t)0);

	return proj->prnt;
}

/* retrieve the first child project of the given project */
project_t firstproject(project_t proj) {
	assert(proj != (project_t)0);

	return proj->head;
}

/* retrieve the last child project of the given project */
project_t lastproject(project_t proj) {
	assert(proj != (project_t)0);

	return proj->tail;
}

/* retrieve the sibling project before the given project */
project_t projectbefore(project_t proj) {
	assert(proj != (project_t)0);

	return proj->prev;
}

/* retrieve the sibling project after the given project */
project_t projectafter(project_t proj) {
	assert(proj != (project_t)0);

	return proj->next;
}

/* set the name of a project */
void setprojectname(project_t proj, const char *name) {
	const char *p;
	char tmp[512];

	assert(proj != (project_t)0);

	tmp[0] = 0;
	p = (const char *)0;

	if (name) {
		if (!(p = strrchr(name, '/')))
			p = strrchr(name, '\\');

		if (p) {
			/* can't end in "/" */
			if (*(p+1)==0)
				errorexit(va("invalid project name \"%s\"", name));

			i_strncpy(tmp, sizeof(tmp), name, (p-name) + 1);
			p++;
		} else
			p = name;
	}

	assert(p != (const char *)0);

	proj->outpath = duplicate(proj->outpath, tmp[0] ? tmp : (const char *)0);
	proj->name = duplicate(proj->name, p);
}

/* set the path of a project */
void setprojectpath(project_t proj, const char *path) {
	assert(proj != (project_t)0);

	proj->path = duplicate(proj->path, path);
}

/* set the type of a project (e.g., kProjType_Library) */
void setprojecttype(project_t proj, int type) {
	assert(proj != (project_t)0);

	if (!proj->outpath)
		proj->outpath = duplicate(proj->outpath,
			type==kProjType_Library ? "lib/" : "bin/");

	proj->type = type;
}

/* retrieve the (non-modifyable) name of a project */
const char *projectname(project_t proj) {
	assert(proj != (project_t)0);

	return proj->name;
}

/* retrieve the (non-modifyable) path of a project */
const char *projectpath(project_t proj) {
	assert(proj != (project_t)0);

	return proj->path;
}

/* retrieve the output path of a project */
const char *projectoutpath(project_t proj) {
	assert(proj != (project_t)0);

	return proj->outpath;
}

/* retrieve the type of a project (e.g., kProjType_Executable) */
int projecttype(project_t proj) {
	assert(proj != (project_t)0);

	return proj->type;
}

/* add a library to the project */
void addprojectlib(project_t proj, const char *libname) {
	assert(proj != (project_t)0);
	assert(libname != (const char *)0);

	arraypush(proj->libs, libname);
}

/* retrieve the number of libraries in a project */
size_t projectlibcount(project_t proj) {
	assert(proj != (project_t)0);

	return arraysize(proj->libs);
}

/* retrieve a library of a project */
const char *projectlib(project_t proj, size_t i) {
	assert(proj != (project_t)0);
	assert(i < arraysize(proj->libs));

	return arrayelement(proj->libs, i);
}

/* append a linker flag to the project */
void appendprojectlinkerflag(project_t proj, const char *flags) {
	size_t l;

	assert(proj != (project_t)0);
	assert(flags != (const char *)0);

	l = proj->linkerflags ? proj->linkerflags[0]!=0 : 0;

	if (l)
		proj->linkerflags = append(proj->linkerflags, " ");

	proj->linkerflags = append(proj->linkerflags, flags);
}

/* retrieve the current linker flags of a project */
const char *projectlinkerflags(project_t proj) {
	assert(proj != (project_t)0);

	return proj->linkerflags ? proj->linkerflags : "";
}

/* add a source file to a project */
void addprojectsource(project_t proj, const char *src) {
	const char *p;

	assert(proj != (project_t)0);
	assert(src != (const char *)0);

	assert(proj->sources != (array_t)0);

	if ((p = strrchr(src, '.')) != (const char *)0) {
		if (!strcmp(p, ".cc") || !strcmp(p, ".cxx") || !strcmp(p, ".cpp")
		|| !strcmp(p, ".c++"))
			proj->config |= kProjCfg_UsesCxx_Bit;
		else if(!strcmp(p, ".mm"))
			proj->config |= kProjCfg_UsesCxx_Bit;
	}

	arraypush(proj->sources, src);
}

/* retrieve the number of source files within a project */
size_t projectsourcecount(project_t proj) {
	assert(proj != (project_t)0);

	assert(proj->sources != (array_t)0);

	return arraysize(proj->sources);
}

/* retrieve a source file of a project */
const char *projectsource(project_t proj, size_t i) {
	assert(proj != (project_t)0);

	assert(proj->sources != (array_t)0);

	return arrayelement(proj->sources, i);
}

/* add a "special directory" to a project */
void addprojectspecialdir(project_t proj, const char *dir) {
	assert(proj != (project_t)0);
	assert(dir != (const char *)0);

	assert(proj->specialdirs != (array_t)0);

	arraypush(proj->specialdirs, dir);
}

/* retrieve the number of "special directories" within a project */
size_t projectspecialdircount(project_t proj) {
	assert(proj != (project_t)0);

	assert(proj->specialdirs != (array_t)0);

	return arraysize(proj->specialdirs);
}

/* retrieve a "special directory" of a project */
const char *projectspecialdir(project_t proj, size_t i) {
	assert(proj != (project_t)0);

	assert(proj->specialdirs != (array_t)0);

	return arrayelement(proj->specialdirs, i);
}

/* determine if a given project is targetted */
int isprojecttargetted(project_t proj) {
	size_t i, n;

	if (!(n = arraysize(g_targets)))
		return 1;

	for(i=0; i<n; i++) {
		if (!strcmp(projectname(proj), arrayelement(g_targets, i)))
			return 1;
	}

	return 0;
}

/* display a tree of projects to stdout */
void printprojects(project_t proj, const char *margin) {
	size_t i, n;
	char marginbuf[256];
	char bin[256];

	assert(margin != (const char *)0);

	if (!proj) {
		for(proj=firstrootproject(); proj; proj=projectafter(proj))
			printprojects(proj, margin);

		return;
	}

	if (isprojecttargetted(proj)) {
		getbinname(proj, bin, sizeof(bin));

		printf("%s%s; \"%s\"\n", margin, projectname(proj), bin);

		n = projectsourcecount(proj);
		for(i=0; i<n; i++)
			printf("%s * %s\n", margin, projectsource(proj, i));
	}

	i_strcpy(marginbuf, sizeof(marginbuf), margin);
	i_strcat(marginbuf, sizeof(marginbuf), "  ");
	for(proj=firstproject(proj); proj; proj=projectafter(proj))
		printprojects(proj, marginbuf);
}

/* given an array of library names, return a string of flags */
void calcprojectlibflags(project_t proj) {
	const char *libname;
	size_t i, n;
	lib_t lib;

	n = arraysize(proj->libs);
	for(i=0; i<n; i++) {
		if (!(libname = arrayelement(proj->libs, i)))
			continue;

		if (!(lib = findlib(libname))) {
			errormessage(va("couldn't find library \"%s\"", libname));
			continue;
		}

		appendprojectlinkerflag(proj, libflags(lib, proj->sys));
	}
}

/*
 *	========================================================================
 *	DIRECTORY MANAGEMENT
 *	========================================================================
 */

/* determine if the directory name specified is special */
int isspecialdir(project_t proj, const char *name) {
	/* check the target system */
	switch(proj->sys) {
	case kProjSys_MSWin:
		if (!strcmp(name, "mswin"))
			return 1;
		break;
	case kProjSys_Linux:
		if (!strcmp(name, "linux"))
			return 1;
		break;
	case kProjSys_MacOS:
		if (!strcmp(name, "macos"))
			return 1;
		break;
	default:
		/*
		 *	NOTE: If you get this message, it's likely because you failed to add
		 *	      the appropriate case statement to this switch. Make sure you
		 *	      port this function too, when adding a new target system.
		 */
		errormessage(va("unknown project system '%d'", proj->sys));
		break;
	}

	/* check the target architecture */
	switch(proj->arch) {
	case kProjArch_X86:
		if (!strcmp(name, "x86"))
			return 1;
		break;
	case kProjArch_X86_64:
		if (!strcmp(name, "x86_64"))
			return 1;
		break;
	case kProjArch_ARM:
		if (!strcmp(name, "arm"))
			return 1;
		break;
	case kProjArch_PowerPC:
		if (!strcmp(name, "ppc"))
			return 1;
		break;
	case kProjArch_MIPS:
		if (!strcmp(name, "mips"))
			return 1;
		break;
	default:
		/*
		 *	NOTE: If you get this message, it's likely because you failed to add
		 *	      the appropriate case statement to this switch. Make sure you
		 *	      port this function too, when adding a new target architecture.
		 */
		errormessage(va("unknown project architecture '%d'", proj->arch));
		break;
	}

	/* check the project type */
	switch(projecttype(proj)) {
	case kProjType_Executable:
		if (!strcmp(name, "exec"))
			return 1;
		break;
	case kProjType_Library:
		if (!strcmp(name, "lib"))
			return 1;
		break;
	case kProjType_DynamicLibrary:
		if (!strcmp(name, "dylib"))
			return 1;
		break;
	default:
		/*
		 *	NOTE: If you get this message, it's likely because you failed to add
		 *	      the appropriate case statement to this switch. Make sure you
		 *	      port this function too, when adding a new binary target.
		 */
		errormessage(va("unknown project type '%d'", proj->type));
		break;
	}

	/*
	 *	PLAN: Add a ".freestanding" (or perhaps ".native?") indicator file to
	 *        support OS/kernel development. Would be cool.
	 */

	return 0;
}

/* enumerate all the special library directories in a project */
void enumspeciallibdirs(project_t proj) {
	const char *libdir;
	size_t i, n;
	char dir[PATH_MAX + 1];

	n = arraysize(proj->libdirs);
	for(i=0; i<n; i++) {
		libdir = arrayelement(proj->libdirs, i);

		switch(proj->sys) {
		case kProjSys_MSWin:
			i_strcpy(dir, sizeof(dir), va("%s/mswin", libdir));
			break;
		case kProjSys_Linux:
			i_strcpy(dir, sizeof(dir), va("%s/linux", libdir));
			break;
		case kProjSys_MacOS:
			i_strcpy(dir, sizeof(dir), va("%s/macos", libdir));
			break;
		default:
			/*
			 * TODO: Other systems here
			 */
			break;
		}

		if (fs_isdir(dir))
			arraypush(proj->libdirs, dir);

		switch(proj->arch) {
		case kProjArch_X86:
			i_strcpy(dir, sizeof(dir), va("%s/x86", libdir));
			break;
		case kProjArch_X86_64:
			i_strcpy(dir, sizeof(dir), va("%s/x86_64", libdir));
			break;
		case kProjArch_ARM:
			i_strcpy(dir, sizeof(dir), va("%s/arm", libdir));
			break;
		case kProjArch_PowerPC:
			i_strcpy(dir, sizeof(dir), va("%s/ppc", libdir));
			break;
		case kProjArch_MIPS:
			i_strcpy(dir, sizeof(dir), va("%s/mips", libdir));
			break;
		default:
			/*
			 * TODO: Other architectures here
			 */
			break;
		}

		if (fs_isdir(dir))
			arraypush(proj->libdirs, dir);
	}

	errno = 0;
}

/* enumerate all source files in a directory */
void enumsources(project_t proj, const char *srcdir) {
	struct dirent *dp;
	char path[512], *p;
	DIR *d;
	int e;

	assert(proj != (project_t)0);
	assert(srcdir != (const char *)0);

	if (!(d = opendir(srcdir))) {
		errormessage(srcdir);
		return;
	}

	while((dp = readdir(d)) != (struct dirent *)0) {
		i_strcpy(path, sizeof(path), srcdir);
		i_strcat(path, sizeof(path), dp->d_name);

		p = strrchr(dp->d_name, '.');
		if (p) {
			if (*(p+1)==0)
				continue;

			if (!strcmp(p, ".c")
			 || !strcmp(p, ".cc")
			 || !strcmp(p, ".cpp")
			 || !strcmp(p, ".cxx")
			 || !strcmp(p, ".c++")
			 || !strcmp(p, ".m")
			 || !strcmp(p, ".mm")) {
				if (!fs_isfile(path))
					continue;

				addprojectsource(proj, path);
				continue;
			}
		}

		if (!fs_isdir(path)) {
			errno = 0;
			continue;
		}

		if (isspecialdir(proj, dp->d_name)) {
			addprojectspecialdir(proj, dp->d_name);
			i_strcat(path, sizeof(path), "/"); /* ending '/' is necessary */
			enumsources(proj, path);
		}
	}

	e = errno;
	closedir(d);
	errno = e;

	if (errno)
		errormessage(srcdir);
}

/* calculate the name of a project based on a directory or file */
int calcprojectname(project_t proj, const char *path, const char *file) {
	size_t i;
	FILE *f;
	char buf[PATH_MAX + 1], cwd[PATH_MAX + 1], *p;

	assert(proj != (project_t)0);
	assert(path != (const char *)0);

	/* read a single line from the file */
	if (file != (const char *)0) {
		if (!(f = fopen(file, "r"))) {
			errormessage(file);
			return 0;
		}

		buf[0] = 0;
		if (!fgets(buf, sizeof(buf), f))
			buf[0] = 0; /*warning: unused result*/

		fclose(f);

		/* strip the line of whitespace on both ends */
		for(i=0; buf[i]<=' ' && buf[i]!=0; i++);

		if ((p = strchr(&buf[i], '\r')) != (char *)0)
			*p = 0;
		if ((p = strchr(&buf[i], '\n')) != (char *)0)
			*p = 0;

		while((p = strrchr(&buf[i], ' ')) != (char *)0) {
			if (*(p + 1)!=0)
				break;

			*p = 0;
		}

		/*
		for(p=&buf[i]; *p>' '; p++);
		if (*p!=0)
			*p = 0;
		*/
	} else {
		fs_getcwd(cwd, sizeof(cwd));

		if (!(p = strrchr(cwd, '/')))
			p = cwd;
		else if(*(p+1)==0) {
			if (strcmp(cwd, "/") != 0) {
				*p = 0;
				p = strrchr(cwd, '/');
				if (!p)
					p = cwd;
			}
		}

		assert(p != (char *)0);

		if (p!=cwd && *p=='/')
			p++;

		i_strcpy(buf, sizeof(buf), p);
		i = 0;
	}

	/* if no name was specified in the file, use the directory name */
	if (!buf[i]) {
		i_strcpy(buf, sizeof(buf), path);

		/* fs_realpath() always adds a '/' at the end of a directory */
		p = strrchr(buf, '/');

		/* ensure this is the case (debug-mode only) */
		assert(p != (char *)0);
		assert(*(p+1)==0);
		*p = 0; /* remove this ending '/' to not conflict with the following */

		/* if there's an ending '/' (always should be), mark the character
		   past that as the start of the directory name */
		i = ((p = strrchr(buf, '/')) != (char *)0) ? (p - buf) + 1 : 0;
	}

	/* set the project's name */
	setprojectname(proj, &buf[i]);

	/* done */
	return 1;
}

/* add a single project using 'file' for information */
project_t addproject(project_t prnt, const char *path, const char *file,
int type) {
	project_t proj;

	assert(path != (const char *)0);
	assert(file != (const char *)0);

	/* create the project */
	proj = newproject(prnt);

	/* calculate the appropriate name for the project */
	if (!calcprojectname(proj, path, file)) {
		deleteproject(proj);
		return (project_t)0;
	}

	/* prepare the passed settings of the project */
	setprojectpath(proj, path);
	setprojecttype(proj, type);

	/* add the sources for the project */
	enumsources(proj, path);

	/* return the project */
	return proj;
}

/* find all projects within a specific source directory, adding them as children
   to the 'prnt' project. */
void findprojects(project_t prnt, const char *srcdir) {
	struct { const char *name; int type; } tests[] = {
		{ ".library", kProjType_Library },
		{ ".dynamiclibrary", kProjType_DynamicLibrary },
		{ ".executable", kProjType_Executable }
	};
	struct dirent *dp;
	project_t proj;
	size_t i;
	char path[512], file[512];
	DIR *d;
	int e;

	/* open the source directory to add projects */
	if (!(d = opendir(srcdir)))
		errorexit(srcdir);

	/* run through each entry in the directory */
	while((dp=readdir(d)) != (struct dirent *)0) {
		if (!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, ".."))
			continue;

		if (!fs_realpath(va("%s/%s/", srcdir, dp->d_name), path, sizeof(path)))
			continue;

		/* run through each test */
		for(i=0; i<sizeof(tests)/sizeof(tests[0]); i++) {
			i_strcpy(file, sizeof(file), va("%s%s", path, tests[i].name));

			/* make sure this file exists */
			if (!fs_isfile(file)) {
				errno = 0;
				continue;
			}

			/* add the project file */
			proj = addproject(prnt, path, file, tests[i].type);

			/* if successful, find sub-projects */
			if (proj)
				findprojects(proj, path);
		}
	}

	/* close this directory */
	e = errno;
	closedir(d);
	errno = e;

	/* if an error occurred, display it */
	/*
	if (errno)
		errorexit(va("readdir(\"%s\") failed", srcdir));
	*/
}

/* find the root directories, adding projects for any of the 'srcdirs' */
void findrootdirs(array_t srcdirs, array_t incdirs, array_t libdirs) {
	struct { const char *name; array_t dst; } tests[] = {
		{ "src", 0 }, { "source", 0 }, { "code", 0 },
		{ "inc", 0 }, { "include", 0 }, { "headers", 0 },
		{ "lib", 0 }, { "library", 0 }, { "libraries", 0 }
	};
	struct stat s;
	project_t proj;
	size_t i, n;
	char path[256];

	i = 0;

	tests[i++].dst = srcdirs;	/* src */
	tests[i++].dst = srcdirs;	/* source */
	tests[i++].dst = srcdirs;	/* code */
	tests[i++].dst = incdirs;	/* inc */
	tests[i++].dst = incdirs;	/* include */
	tests[i++].dst = incdirs;	/* headers */
	tests[i++].dst = libdirs;	/* lib */
	tests[i++].dst = libdirs;	/* library */
	tests[i++].dst = libdirs;	/* libraries */

	assert(srcdirs != (array_t)0);
	assert(incdirs != (array_t)0);
	assert(libdirs != (array_t)0);

	for(i=0; i<sizeof(tests)/sizeof(tests[0]); i++) {
		assert(tests[i].name != (const char *)0);
		assert(tests[i].dst != (array_t)0);

		if (arraysize(tests[i].dst) > 0)
			continue;

		if (stat(tests[i].name, &s)==-1)
			continue;
		if (S_ISDIR(~s.st_mode))
			continue;

		arraypush(tests[i].dst, tests[i].name);
	}

	n = arraysize(srcdirs);
	for(i=0; i<n; i++)
		findprojects((project_t)0, arrayelement(srcdirs, i));

	if (!firstrootproject() && n>0) {
		if (!fs_realpath("./", path, sizeof(path)))
			return;

		proj = newproject((project_t)0);

		if (!calcprojectname(proj, path, (const char *)0)) {
			deleteproject(proj);
			return;
		}

		i_strcat(path, sizeof(path), va("%s/", arrayelement(srcdirs, 0)));

		setprojectpath(proj, path);
		setprojecttype(proj, kProjType_Executable);

		enumsources(proj, path);
	}
}

/* create the object directories for a particular project */
int makeobjdirs(project_t proj) {
	const char *typename;
	size_t i, n;
	char objdir[512], cwd[512];

	assert(proj != (project_t)0);

	if (getcwd(cwd, sizeof(cwd))==(char *)0) {
		errormessage("getcwd() failed");
		return 0;
	}

	n = strlen(cwd);

	switch(projecttype(proj)) {
	case kProjType_Executable:
		typename = "exec";
		break;
	case kProjType_Library:
		typename = "lib";
		break;
	case kProjType_DynamicLibrary:
		typename = "dylib";
		break;
	default:
		assertmsg(0, "project type is invalid");
		break;
	}

	snprintf(objdir, sizeof(objdir)-1, "obj/%s/%s/%s",
		g_flags & kFlag_Release_Bit ? "release" : "debug", typename,
		&projectpath(proj)[n]);
	objdir[sizeof(objdir)-1] = 0;

	fs_mkdirs(objdir);

	n = projectspecialdircount(proj);
	for(i=0; i<n; i++)
		fs_mkdirs(va("%s%s/", objdir, projectspecialdir(proj, i)));

	return 1;
}

/*
 *	========================================================================
 *	COMPILATION MANAGEMENT
 *	========================================================================
 */

/* find which libraries are to be autolinked from a source file */
int findsourcelibs(array_t dst, int sys, const char *obj, const char *dep) {
	autolink_t al;
	size_t i, n;
	dep_t d;

	if ((d = finddependency(obj)) != (dep_t)0)
		deletedependency(d);

	if (!readdeps(dep)) {
		errormessage(va("failed to read dependency \"%s\"", dep));
		return 0;
	}

	if (!(d = finddependency(obj))) {
		errormessage(va("finddependency(\"%s\") failed", obj));
		return 0;
	}

	n = dependencycount(d);
	for(i=0; i<n; i++) {
		if (!(al = findautolink(sys, getdependency(d, i))))
			continue;

		arraypush(dst, autolinklib(al));
	}

	return 1;
}

/* determine whether a source file should be built */
int shouldcompile(const char *obj) {
	struct stat s, obj_s;
	size_t i, n;
	dep_t d;
	char dep[512];

	assert(obj != (const char *)0);

	if (g_flags & kFlag_Rebuild_Bit)
		return 1;
	if (g_flags & kFlag_NoCompile_Bit)
		return 0;

	if (stat(obj, &obj_s)==-1)
		return 1;

	extsubst(dep, sizeof(dep), obj, ".d");

	if (stat(dep, &s)==-1)
		return 1;

	d = finddependency(obj);
	if (!d)
		if (!readdeps(dep))
			return 1;

	d = finddependency(obj);
	if (!d)
		return 1;

	n = dependencycount(d);
	for(i=0; i<n; i++) {
		if (stat(getdependency(d, i), &s)==-1)
			return 1; /* need recompile for new dependency list; this file is
			             (potentially) missing */

		if (obj_s.st_mtime <= s.st_mtime)
			return 1;
	}

	return 0; /* no reason to rebuild */
}

/* determine whether a project should be linked */
int shouldlink(const char *bin, int numbuilds) {
	struct stat bin_s;

	assert(bin != (const char *)0);

	if (g_flags & kFlag_Rebuild_Bit)
		return 1;
	if (g_flags & kFlag_NoLink_Bit)
		return 0;

	if (stat(bin, &bin_s)==-1)
		return 1;

	if (numbuilds != 0)
		return 1;

	return 0;
}

/* retrieve the flags for compiling a particular source file */
const char *getcompileflags(project_t proj, const char *obj, const char *src) {
	/*
	 *	TODO: Visual C++ and dependencies. How?
	 */
	static char flags[16384];
	const char *p, *g;
	size_t i, n;

	assert(proj != (project_t)0);
	assert(obj != (const char *)0);
	assert(src != (const char *)0);

	flags[0] = 0;
	g = "";
	if (g_flags & kFlag_Profile_Bit)
		g = "-pg ";
	else if(~g_flags & kFlag_Release_Bit)
		g = "-g ";

	/*
	 * TODO: Allow the front-end to override the warning level
	 */

	/* cl: /Wall */
	i_strcat(flags, sizeof(flags), "-W -Wall ");
	if (g_flags & kFlag_Pedantic_Bit)
		i_strcat(flags, sizeof(flags), "-pedantic ");

	p = strrchr(src, '.');
	if (!p)
		p = src;

	if (!strcmp(p, ".cc") || !strcmp(p, ".cxx") || !strcmp(p, ".cpp")
	|| !strcmp(p, ".c++"))
		i_strcat(flags, sizeof(flags), "-Weffc++ ");

	/* architecture specific flags */
	switch(proj->arch) {
	case kProjArch_X86:
		i_strcat(flags, sizeof(flags), "-DMK_ARCH_X86 -DMK_BITS=32 -msse ");
		break;
	case kProjArch_X86_64:
		i_strcat(flags, sizeof(flags), "-DMK_ARCH_X86_64 -DMK_BITS=64 -msse ");
		break;
	case kProjArch_ARM:
		/* TODO: Add ARM64 support */
		i_strcat(flags, sizeof(flags), "-DMK_ARCH_ARM -DMK_BITS=32 ");
		break;
	case kProjArch_PowerPC:
		/* TODO: PowerPC bits = 64? */
		i_strcat(flags, sizeof(flags), "-DMK_ARCH_POWERPC -DMK_BITS=32 ");
		break;
	case kProjArch_MIPS:
		i_strcat(flags, sizeof(flags), "-DMK_ARCH_MIPS -DMK_BITS=32 ");
		break;
	default:
		break;
	}

	/* optimization/debugging */
	i_strcat(flags, sizeof(flags), g);
	if (g_flags & kFlag_Release_Bit) {
		/* cl: /DNDEBUG /Og /Ox /Oy /GL /QIfist; /Qpar? (parallel code gen.) */
		i_strcat(flags, sizeof(flags), "-DNDEBUG -s -O3 ");
		switch(proj->arch) {
		case kProjArch_X86:
			/* cl: /arch:SSE */
			i_strcat(flags, sizeof(flags), "-fomit-frame-pointer -m32 ");
			break;
		case kProjArch_X86_64:
			/* cl: /arch:SSE2 */
			i_strcat(flags, sizeof(flags), "-fomit-frame-pointer -m64 ");
			break;
		default:
			/*
			 *	NOTE: If you have architecture specific optimizations to apply
			 *        to release code, add them here.
			 */
			break;
		}
	} else {
		/* cl: /Zi /D_DEBUG /DDEBUG /D__debug__ */
		i_strcat(flags, sizeof(flags), "-D_DEBUG -DDEBUG -D__debug__ ");
	}

	i_strcat(flags, sizeof(flags), va("-DMK_INTEGRALDATE=%i ",
		getintegraldate()));

	/* add a macro for the target system (some systems don't provide their own,
	   or aren't consistent/useful */
	switch(proj->sys) {
	case kProjSys_MSWin:
		/* cl: /DMK_MSWIN */
		i_strcat(flags, sizeof(flags), "-DMK_MSWIN ");
		break;
	case kProjSys_Linux:
		/* cl: /DMK_LINUX */
		i_strcat(flags, sizeof(flags), "-DMK_LINUX ");
		break;
	case kProjSys_MacOS:
		/* cl: /DMK_MACOS */
		i_strcat(flags, sizeof(flags), "-DMK_MACOS ");
		break;
	default:
		/*
		 *	PLAN: Add Android/iOS support here too.
		 */
		assertmsg(0, "project system not handled");
		break;
	}

	/* add a macro for the target build type */
	switch(proj->type) {
	case kProjType_Executable:
		/* cl: /DEXECUTABLE */
		i_strcat(flags, sizeof(flags), "-DEXECUTABLE ");
		break;
	case kProjType_Library:
		/* cl: /DLIBRARY */
		i_strcat(flags, sizeof(flags), "-DLIBRARY ");
		break;
	case kProjType_DynamicLibrary:
		/* cl: /DDYNAMICLIBRARY */
		i_strcat(flags, sizeof(flags), "-DDYNAMICLIBRARY ");
			/* " -fPIC" */
		break;
	default:
		assertmsg(0, "project type not handled");
		break;
	}

	/* add the include search paths */
	n = arraysize(g_incdirs);
	for(i=0; i<n; i++)
		/* cl: "/I \"%s\" " */
		i_strcat(flags, sizeof(flags), va("-I \"%s\" ",
			arrayelement(g_incdirs, i)));

	/* add the remaining flags (e.g., dependencies, compile-only, etc) */
	i_strcat(flags, sizeof(flags), "-MD -MP -c ");

	/* add the appropriate compilation flags */
	i_strcat(flags, sizeof(flags), va("-o \"%s\" \"%s\"", obj, src));

	/* done */
	return flags;
}

/* construct a list of dependent libraries */
void getprojectlibs(project_t proj, char *dst, size_t n) {
	const char *name, *suffix;
	project_t node;
	char bin[512];
	int type;

	assert(proj != (project_t)0);
	assert(dst != (char *)0);
	assert(n > 1);

	type = projecttype(proj);
	name = projectname(proj);

	suffix = g_flags & kFlag_Release_Bit ? "" : "d";

	if (type==kProjType_Library) {
		for(node=firstproject(proj); node; node=projectafter(node))
			getprojectlibs(node, dst, n);

		if (~proj->config & kProjCfg_Linking_Bit)
			i_strcat(dst, n, va("-l%s%s ", name, suffix));
	} else if(type==kProjType_DynamicLibrary) {
		if (~proj->config & kProjCfg_Linking_Bit) {
			getbinname(proj, bin, sizeof(bin));
			i_strcat(dst, n, va("\"%s\" ", bin));
			/*
			i_strcat(dst, n, va("-L \"%s\" ", projectoutpath(proj)));
			i_strcat(dst, n, va("-l%s%s.dll ", name, suffix));
			*/
		}
	}
}

/* retrieve the flags for linking a project */
const char *getlinkflags(project_t proj, const char *bin, array_t objs) {
	static char flags[32768];
	static char libs[16384];
	project_t p;
	size_t i, n;

	assert(proj != (project_t)0);
	assert(bin != (const char *)0);
	assert(objs != (array_t)0);

	flags[0] = 0;
	libs[0] = 0;

	if (projecttype(proj)!=kProjType_Library) {
		if (g_flags & kFlag_Profile_Bit)
			i_strcat(flags, sizeof(flags), "-pg ");
	}

	switch(projecttype(proj)) {
	case kProjType_Executable:
		if (g_flags & kFlag_Release_Bit)
			i_strcat(flags, sizeof(flags), "-s ");
		i_strcat(flags, sizeof(flags), va("-o \"%s\" ", bin));
		break;
	case kProjType_Library:
		i_strcat(flags, sizeof(flags), va("cr \"%s\" ", bin));
		break;
	case kProjType_DynamicLibrary:
		i_strcat(flags, sizeof(flags), va("-shared -o \"%s\" ", bin));
		break;
	default:
		assertmsg(0, "unhandled project type");
		break;
	}

	if (projecttype(proj)!=kProjType_Library) {
		n = arraysize(proj->libdirs);
		for(i=0; i<n; i++)
			i_strcat(flags, sizeof(flags), va("-L \"%s\" ",
				arrayelement(proj->libdirs, i)));
	}

	n = arraysize(objs);
	for(i=0; i<n; i++)
		i_strcat(flags, sizeof(flags), va("\"%s\" ", arrayelement(objs, i)));

	if (projecttype(proj)!=kProjType_Library) {
		proj->config |= kProjCfg_Linking_Bit;

		p = projectparent(proj) ? firstproject(projectparent(proj))
			: firstrootproject();
		while(p) {
			getprojectlibs(p, libs, sizeof(libs));
			p = projectafter(p);
		}

		proj->config &= ~kProjCfg_Linking_Bit;

		i_strcat(flags, sizeof(flags), libs);

		switch(proj->sys) {
		case kProjSys_MSWin:
#if 0
			if (projecttype(proj)==kProjType_DynamicLibrary)
				i_strcat(flags, sizeof(flags),
					/* NOTE: we don't use the import library; it's pointless */
					va(/*"\"-Wl,--out-implib=%s%s.a\" "*/
					   "-Wl,--export-all-symbols "
					   "-Wl,--enable-auto-import "/*, bin,
					   ~g_flags & kFlag_Release_Bit ? "d" : ""*/));

			/*i_strcat(flags, sizeof(flags), "-lkernel32 -luser32 -lgdi32 "
				"-lshell32 -lole32 -lopengl32 -lmsimg32");*/
#endif
			break;
		case kProjSys_Linux:
			/*i_strcat(flags, sizeof(flags), "-lGL");*/
			break;
		case kProjSys_MacOS:
			/*i_strcat(flags, sizeof(flags), "-lobjc -framework Cocoa "
				"-framework OpenGL");*/
			break;
		default:
			/*
			 *	NOTE: Add OS specific link flags here.
			 */
			break;
		}

		i_strcat(flags, sizeof(flags), projectlinkerflags(proj));
		i_strcat(flags, sizeof(flags), " -lm");
	}

	return flags;
}

/* find the name of an object file for a given source file */
void getobjname(project_t proj, char *obj, size_t n, const char *src) {
	const char *p;

	assert(proj != (project_t)0);
	assert(obj != (char *)0);
	assert(n > 1);
	assert(src != (const char *)0);

	p = ""; /* 'p' may be used uninitialized */

	switch(projecttype(proj)) {
	case kProjType_Executable:
		p = "exec";
		break;
	case kProjType_Library:
		p = "lib";
		break;
	case kProjType_DynamicLibrary:
		p = "dylib";
		break;
	default:
		assertmsg(0, "project type is invalid");
		break;
	}

	extsubst(obj, n, va("obj/%s/%s/%s",
		g_flags & kFlag_Release_Bit ? "release" : "debug", p, src), ".o");
}

/* find the binary name of a target */
void getbinname(project_t proj, char *bin, size_t n) {
	const char *dir, *prefix, *name, *symbol, *suffix;
	int type;
	int sys;

	assert(proj != (project_t)0);
	assert(bin != (char *)0);
	assert(n > 1);

	type = projecttype(proj);
	sys = proj->sys;

	/*dir = type==kProjType_Library ? "lib/" : "bin/";*/
	dir = projectoutpath(proj);
	assert(dir != (const char *)0);
	prefix = "";
	suffix = "";
	name = projectname(proj);
	symbol = g_flags & kFlag_Release_Bit ? "" : "d";

	switch(type) {
	case kProjType_Executable:
		if (sys==kProjSys_MSWin)
			suffix = ".exe";
		break;
	case kProjType_Library:
		prefix = "lib";
		suffix = ".a";
		break;
	case kProjType_DynamicLibrary:
		if (sys!=kProjSys_MSWin)
			prefix = "lib";
		if (sys==kProjSys_MSWin)
			suffix = ".dll";
		else if(sys==kProjSys_Linux)
			suffix = ".so";
		else if(sys==kProjSys_MacOS)
			suffix = ".dylib";
		break;
	default:
		assertmsg(0, "unhandled project type");
		break;
	}

	i_strcpy(bin, n, va("%s%s%s%s%s", dir, prefix, name, symbol, suffix));
}

/* sort the projects in a list */
void sortprojects(struct project_s *proj) {
	struct project_s **head, **tail;
	struct project_s *p, *next;
	int numsorts;

	head = proj ? &proj->head : &g_proj_head;
	tail = proj ? &proj->tail : &g_proj_tail;

	do {
		numsorts = 0;

		for(p=*head; p; p=next) {
			if (!(next = p->next))
				break;

			if (p->type > next->type) {
				numsorts++;

				if (p==*head)
					*head = next;
				if (next==*tail)
					*tail = p;

				if (p->prev)
					p->prev->next = next;
				if (next->next)
					next->next->prev = p;

				next->prev = p->prev;
				p->prev = next;
				p->next = next->next;
				next->next = p;

				next = p;
			}
		}
	} while(numsorts > 0);
}

/* set a projects dependents to be relinked */
void relinkdependents(project_t proj) {
	project_t prnt, next;

	assert(proj != (project_t)0);

	for(prnt=proj->prnt; prnt; prnt=prnt->prnt)
		prnt->config |= kProjCfg_NeedRelink_Bit;

	if (proj->type==kProjType_Executable)
		return;

	for(next=proj->next; next; next=next->next) {
		if (proj->type > next->type)
			break;

		next->config |= kProjCfg_NeedRelink_Bit;
		relinkdependents(next);
	}
}

/* build a project */
int buildproject(project_t proj) {
	const char *src, *lnk, *tool;
	project_t chld;
	array_t objs;
	size_t cwd_l;
	size_t i, n;
	char cwd[512], obj[512], bin[512];
	int numbuilds;

	/* build the child projects */
	sortprojects(proj);
	for(chld=firstproject(proj); chld; chld=projectafter(chld)) {
		if (!buildproject(chld))
			return 0;
	}

	/* retrieve the current working directory */
	if (getcwd(cwd, sizeof(cwd))==(char *)0)
		errorexit("getcwd() failed");

	cwd_l = strlen(cwd);

	if (!projectsourcecount(proj)
	&&  projecttype(proj)!=kProjType_DynamicLibrary) {
		errormessage(va("project '%s' has no source files!",
			projectname(proj)));
		return 1;
	}

	tool = proj->config & kProjCfg_UsesCxx_Bit ? g_cxx : g_cc;

	/* make the object directories */
	makeobjdirs(proj);

	/* store each object file */
	objs = newarray();

	/* run through each source file */
	numbuilds = 0;
	n = projectsourcecount(proj);
	for(i=0; i<n; i++) {
		src = projectsource(proj, i);
		getobjname(proj, obj, sizeof(obj), &src[cwd_l+1]);
		arraypush(objs, obj);

		if (shouldcompile(obj)) {
			if (shell("%s %s", tool,
			getcompileflags(proj, obj, &src[cwd_l+1]))) {
				deletearray(objs);
				return 0;
			}
			numbuilds++;
		}

		if (~g_flags & kFlag_NoLink_Bit) {
			extsubst(bin, sizeof(bin), obj, ".d");
			if (!findsourcelibs(proj->libs, proj->sys, obj, bin)) {
				errormessage("call to findsourcelibs() failed");
				return 0;
			}
		}
	}

	/* link the project's object files together */
	getbinname(proj, bin, sizeof(bin));
	fs_mkdirs(projectoutpath(proj));
	/*printf("bin: %s\n", bin);*/
	if (shouldlink(bin, numbuilds) || proj->config & kProjCfg_NeedRelink_Bit) {
		removearraydups(proj->libs);
		calcprojectlibflags(proj);
		enumspeciallibdirs(proj);

		/* find the libraries this project needs */
		lnk = projecttype(proj)==kProjType_Library ? "ar" : tool;
		if (shell("%s %s", lnk, getlinkflags(proj, bin, objs))) {
			deletearray(objs);
			return 0;
		}

		/* dependent projects need to be rebuilt */
		relinkdependents(proj);
	}

	/* clean */
	if (g_flags & kFlag_Clean_Bit) {
		n = arraysize(objs);
		for(i=0; i<n; i++) {
			extsubst(obj, sizeof(obj), arrayelement(objs, i), ".d");
			remove(arrayelement(objs, i)); /* object (.o) */
			remove(obj); /* dependency (.d) */
		}

		if (g_flags & kFlag_NoLink_Bit)
			remove(bin);
	}

	deletearray(objs);

	return 1;
}

/* build all the projects */
int buildprojects() {
	project_t proj;

	sortprojects((struct project_s *)0);

	for(proj=firstrootproject(); proj; proj=projectafter(proj)) {
		if (!buildproject(proj))
			return 0;
	}

	return 1;
}

/*
 *	========================================================================
 *	MAIN
 *	========================================================================
 */

void pushsrcdir(const char *srcdir) {
	if (!fs_isdir(srcdir)) {
		errormessage(srcdir);
		return;
	}

	arraypush(g_srcdirs, srcdir);
}
void pushincdir(const char *incdir) {
	if (!fs_isdir(incdir)) {
		errormessage(incdir);
		return;
	}

	arraypush(g_incdirs, incdir);
}
void pushlibdir(const char *libdir) {
	if (!fs_isdir(libdir)) {
		errormessage(libdir);
		return;
	}

	arraypush(g_libdirs, libdir);
}

void init(int argc, char **argv) {
	struct { const char *header[kNumProjSys], *lib; } autolinks[] = {
		/* OpenGL */
		{ { "GL/gl.h","GL/gl.h","OpenGL/OpenGL.h" },
		  "opengl" },
		{ { (const char *)0,(const char *)0,"GL/gl.h" },
		  "opengl" },
		{ { (const char *)0,(const char *)0,"OpenGL/gl.h" },
		  "opengl" },

		/* OpenGL - GLU */
		{ { "GL/glu.h","GL/glu.h","OpenGL/glu.h" },
		  "glu" },

		/* OpenGL - GLFW/GLEW */
		{ { "GL/glfw.h","GL/glfw.h","GL/glfw.h" },
		  "glfw" },
		{ { "GL/glew.h","GL/glew.h","GL/glew.h" },
		  "glew" },

		/* SDL */
		{ { "SDL/sdl.h","SDL/sdl.h","SDL/sdl.h" },
		  "sdl" },
		{ { "SDL/sdl_mixer.h","SDL/sdl_mixer.h","SDL/sdl_mixer.h" },
		  "sdl_mixer" },
		{ { "SDL/sdl_main.h","SDL/sdl_main.h","SDL/sdl_main.h" },
		  "sdl_main" },

		/* Ogg/Vorbis */
		{ { "ogg/ogg.h","ogg/ogg.h","ogg/ogg.h" },
		  "ogg" },
		{ { "vorbis/codec.h","vorbis/codec.h","vorbis/codec.h" },
		  "vorbis" },
		{ { "vorbis/vorbisenc.h","vorbis/vorbisenc.h","vorbis/vorbisenc.h" },
		  "vorbisenc" },
		{ { "vorbis/vorbisfile.h","vorbis/vorbisfile.h","vorbis/vorbisfile.h" },
		  "vorbisfile" },

		/* Windows */
		{ { "winuser.h",(const char *)0,(const char *)0 },
		  "user32" },
		{ { "winbase.h",(const char *)0,(const char *)0 },
		  "kernel32" },
		{ { "shellapi.h",(const char *)0,(const char *)0 },
		  "shell32" },
		{ { "ole.h",(const char *)0,(const char *)0 },
		  "ole32" },
		{ { "commctrl.h",(const char *)0,(const char *)0 },
		  "comctl32" },
		{ { "commdlg.h",(const char *)0,(const char *)0 },
		  "comdlg32" },
		{ { "wininet.h",(const char *)0,(const char *)0 },
		  "wininet" },
		{ { "mmsystem.h",(const char *)0,(const char *)0 },
		  "winmm" },
		{ { "uxtheme.h",(const char *)0,(const char *)0 },
		  "uxtheme" },

		/* PNG */
		{ { "png.h","png.h","png.h" },
		  "png" },

		/* BZip2 */
		{ { "bzlib.h","bzlib.h","bzlib.h" },
		  "bzip2" },

		/* ZLib */
		{ { "zlib.h","zlib.h","zlib.h" },
		  "z" },

		/* PThread */
		{ { "pthread.h","pthread.h","pthread.h" },
		  "pthread" },

		/* dlfcn */
		{ { (const char *)0,"dlfcn.h",(const char *)0 },
		  "dlfcn" },

		/* ncurses */
		{ { (const char *)0,"ncurses.h","ncurses.h" },
		  "ncurses" },

		/* SOIL */
		{ { "SOIL.h","SOIL.h","SOIL.h" },
		  "soil" }
	};
	struct { const char *lib, *flags[kNumProjSys]; } libs[] = {
		/* OpenGL (and friends) */
		{ "opengl",
		  { "-lopengl32","-lGL","-framework OpenGL" } },
		{ "glu",
		  { "-lglu32","-lGLU","-lGLU" } },
		{ "glfw",
		  { "-lglfw","-lglfw","-lglfw" } },
		{ "glew",
		  { "-lglew32","-lGLEW","-lGLEW" } },

		/* SDL */
		{ "sdl",
		  { "-lSDL","-lSDL","-lSDL" } },
		{ "sdl_mixer",
		  { "-lSDL_mixer","-lSDL_mixer","-lSDL_mixer" } },
		{ "sdl_main",
		  { "-lSDLmain","-lSDLmain","-lSDLmain" } },

		/* Ogg/Vorbis */
		{ "ogg",
		  { "-logg","-logg","-logg" } },
		{ "vorbis",
		  { "-lvorbis","-lvorbis","-lvorbis" } },
		{ "vorbisenc",
		  { "-lvorbisenc","-lvorbisenc","-lvorbisenc" } },
		{ "vorbisfile",
		  { "-lvorbisfile","-lvorbisfile","-lvorbisfile" } },

		/* Windows */
		{ "user32",
		  { "-luser32",(const char *)0,(const char *)0 } },
		{ "kernel32",
		  { "-lkernel32",(const char *)0,(const char *)0 } },
		{ "shell32",
		  { "-lshell32",(const char *)0,(const char *)0 } },
		{ "ole32",
		  { "-lole32",(const char *)0,(const char *)0 } },
		{ "comctl32",
		  { "-lcomctl32",(const char *)0,(const char *)0 } },
		{ "comdlg32",
		  { "-lcomdlg32",(const char *)0,(const char *)0 } },
		{ "wininet",
		  { "-lwininet",(const char *)0,(const char *)0 } },
		{ "winmm",
		  { "-lwinmm",(const char *)0,(const char *)0 } },
		{ "uxtheme",
		  { "-luxtheme",(const char *)0,(const char *)0 } },

		/* PNG */
		{ "png",
		  { "-lpng","-lpng","-lpng" } },

		/* BZip2 */
		{ "bzip2",
		  { "-lbzip2","-lbzip2","-lbzip2" } },

		/* ZLib */
		{ "z",
		  { "-lz","-lz","-lz" } },

		/* PThread */
		{ "pthread",
		  { "-lpthread","-lpthread","-lpthread" } },

		/* dlfcn */
		{ "dlfcn",
		  { (const char *)0,"-ldl",(const char *)0 } },

		/* ncurses */
		{ "ncurses",
		  { (const char *)0,"-lncurses","-lncurses" } },

		/* SOIL */
		{ "soil",
		  { "-lSOIL","-lSOIL","-lSOIL" } }
	};
	const char *optlinks[256], *p;
	bitfield_t bit;
	autolink_t al;
	size_t j, k;
	lib_t lib;
	char temp[512];
	int i, op;

	/* core initialization */
	atexit(deleteallarrays);
	fs_init();
	atexit(deleteallautolinks);
	atexit(deletealldependencies);
	atexit(deleteallprojects);

	/* add the autolinks */
	for(j=0; j<sizeof(autolinks)/sizeof(autolinks[0]); j++) {
		al = newautolink();

		setautolinklib(al, autolinks[j].lib);

		for(k=0; k<kNumProjSys; k++)
			setautolinkheader(al, k, autolinks[j].header[k]);
	}

	/* add the libraries */
	for(j=0; j<sizeof(libs)/sizeof(libs[0]); j++) {
		lib = newlib();

		setlibname(lib, libs[j].lib);

		for(k=0; k<kNumProjSys; k++)
			setlibflags(lib, k, libs[j].flags[k]);
	}

	/* set single-character options here */
	memset((void *)optlinks, 0, sizeof(optlinks));
	optlinks['h'] = "help";
	optlinks['v'] = "version";
	optlinks['V'] = "verbose";
	optlinks['r'] = "release";
	optlinks['R'] = "rebuild";
	optlinks['c'] = "compile-only";
	optlinks['C'] = "clean";
	optlinks['p'] = "print-hierarchy";
	optlinks['P'] = "pedantic";
	optlinks['i'] = "profile"; /* i=instrument */

	/* arrays need to be initialized */
	g_targets = newarray();
	g_srcdirs = newarray();
	g_incdirs = newarray();
	g_libdirs = newarray();

	/* process command line arguments */
	for(i=1; i<argc; i++) {
		const char *opt;

		opt = argv[i];
		if (*opt=='-') {
			op = 0;
			p = (const char *)0;

			if (*(opt+1)=='-') {
				opt = &opt[2];

				if ((op = !strncmp(opt, "no-", 3) ? 1 : 0)==1)
					opt = &opt[3];

				if ((p = strchr(opt, '=')) != (const char *)0) {
					i_strncpy(temp, sizeof(temp), opt, p-opt);
					p++;
					opt = temp;
				}
			} else {
				if (*(opt+1)=='I') {
					if (*(opt+2)==0)
						p = argv[++i];
					else
						p = &opt[2];

					opt = "incdir";
				} else if(*(opt+1)=='L') {
					if (*(opt+2)==0)
						p = argv[++i];
					else
						p = &opt[2];

					opt = "libdir";
				} else if(*(opt+1)=='S') {
					if (*(opt+2)==0)
						p = argv[++i];
					else
						p = &opt[2];

					opt = "srcdir";
				} else {
					/* not allowing repeats (e.g., -hv) yet */
					if (*(opt+2)!=0) {
						errormessage(va("'%s' is a malformed argument", argv[i]));
						continue;
					}

					if (!(opt = optlinks[(unsigned char)*(opt+1)])) {
						errormessage(va("unknown option '%s'; ignoring",
							argv[i]));
						continue;
					}
				}
			}

			assert(opt != (const char *)0);

			bit = 0;
			if (!strcmp(opt, "help"))
				bit = kFlag_ShowHelp_Bit;
			else if(!strcmp(opt, "version"))
				bit = kFlag_ShowVersion_Bit;
			else if(!strcmp(opt, "verbose"))
				bit = kFlag_Verbose_Bit;
			else if(!strcmp(opt, "release"))
				bit = kFlag_Release_Bit;
			else if(!strcmp(opt, "rebuild"))
				bit = kFlag_Rebuild_Bit;
			else if(!strcmp(opt, "compile-only"))
				bit = kFlag_NoLink_Bit;
			else if(!strcmp(opt, "clean"))
				bit = kFlag_NoCompile_Bit|kFlag_NoLink_Bit|kFlag_Clean_Bit;
			else if(!strcmp(opt, "print-hierarchy"))
				bit = kFlag_PrintHierarchy_Bit;
			else if(!strcmp(opt, "pedantic"))
				bit = kFlag_Pedantic_Bit;
			else if(!strcmp(opt, "profile"))
				bit = kFlag_Profile_Bit;
			else if(!strcmp(opt, "srcdir")) {
				if (i+1==argc&&!p) {
					errormessage(va("expected argument to '%s'", argv[i]));
					continue;
				}

				pushsrcdir(p ? p : argv[++i]);
				continue;
			} else if(!strcmp(opt, "incdir")) {
				if (i+1==argc&&!p) {
					errormessage(va("expected argument to '%s'", argv[i]));
					continue;
				}

				pushincdir(p ? p : argv[++i]);
				continue;
			} else if(!strcmp(opt, "libdir")) {
				if (i+1==argc&&!p) {
					errormessage(va("expected argument to '%s'", argv[i]));
					continue;
				}

				pushlibdir(p ? p : argv[++i]);
				continue;
			} else {
				errormessage(va("unknown option '%s'; ignoring", argv[i]));
				continue;
			}

			assert(bit != 0);
			assert(op==0 || op==1);

			if (op)
				g_flags &= ~bit;
			else
				g_flags |= bit;
		} else
			arraypush(g_targets, opt);
	}

	/* show the version */
	if (g_flags & kFlag_ShowVersion_Bit)
		printf("mk 0.3 - compiled %s\ncopyright (c) 2012 Aaron J. Miller\n\n"
			"This software contains ABSOLUTELY NO WARRANTY.\n\n",
			__DATE__);

	/* show the help */
	if (g_flags & kFlag_ShowHelp_Bit) {
		printf("Usage: mk [options...] [targets...]\n");
		printf("Options:\n");
		printf("  -h,--help                Show this help message.\n");
		printf("  -v,--version             Show the version.\n");
		printf("  -V,--verbose             Show the commands invoked.\n");
		printf("  -C,--clean               Remove intermediate files and "
			"binaries.\n");
		printf("  -r,--release             Build in release mode.\n");
		printf("  -P,--pedantic            Enable pedantic warnings.\n");
		printf("  -i,--profile             Enable instrumented programs for "
			"profiling.\n");
		printf("  -R,--rebuild             "
			"Force a rebuild, even if unnecessary.\n");
		printf("  -c,--compile-only        Just compile; do not link.\n");
		printf("  -p,--print-hierarchy     Display the project hierarchy.\n");
		printf("  -S,--srcdir=<dir>        Add a source directory.\n");
		printf("  -I,--incdir=<dir>        Add an include directory.\n");
		printf("  -L,--libdir=<dir>        Add a library directory.\n");
		printf("\n");
		printf("See the documentation for more details.\n");
	}

	/* exit if no targets were specified and a message was requested */
	if (g_flags&(kFlag_ShowVersion_Bit|kFlag_ShowHelp_Bit)
	 && !arraysize(g_targets))
		exit(EXIT_SUCCESS);

	/* grab the available directories */
	findrootdirs(g_srcdirs, g_incdirs, g_libdirs);

	/* if there aren't any source directories, complain */
	if (!arraysize(g_srcdirs))
		errorexit("no source directories ('src' or 'source')");

	/* select the compiler based on environment variables */
	i_strcpy(g_cc, sizeof(g_cc), (p=getenv("CC")) ? p : "gcc");
	i_strcpy(g_cxx, sizeof(g_cxx), p ? p : (p=getenv("CXX")) ? p : "g++");
}

int main(int argc, char **argv) {
	init(argc, argv);

	if (g_flags & kFlag_PrintHierarchy_Bit) {
		printf("Source Directories:\n");
		printarray(g_srcdirs);

		printf("Include Directories:\n");
		printarray(g_incdirs);

		printf("Library Directories:\n");
		printarray(g_libdirs);

		printf("Targets:\n");
		printarray(g_targets);

		printf("Projects:\n");
		printprojects((project_t)0, "  ");
		printf("\n");
		fflush(stdout);
	}

	buildprojects();

	return 0;
}
