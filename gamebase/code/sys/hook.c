#include <gamebase.h>

/*
 * TODO: Improve the data-structures used here. A lot can improve from being a
 *       bit more logical about storing these things.
 *
 *       I'm thinking binary tree for 'id' sort, then link list for everything
 *       else. (Maybe an additional binary tree for each context?) That means a
 *       binary tree structure will be needed.
 */

struct Hook {
	gb_hookID_t id;
	gb_fnHook_t fnHook;
	void *context;

	gb_node_t link;
};
static gb_list_t g_list;
static gb_bool_t g_didInit = GB_FALSE;

static void Init() {
	if (GB_LIKELY(g_didInit))
		return;

	gbInitList(&g_list);
	g_didInit = GB_TRUE;
}

gb_hookID_t gbAllocHookID() {
	static gb_hookID_t start = 32768;

	return start++;
}

void gbAddHook(gb_hookID_t id, gb_fnHook_t fnHook, void *context) {
	struct Hook *h;

	gbAssert(id != (gb_hookID_t)0);
	gbAssert(fnHook != (gb_fnHook_t)NULL);

	Init();

	h = (struct Hook *)gbMemAlloc(sizeof(*h));

	h->id = id;
	h->fnHook = fnHook;
	h->context = context;

	gbInitNode(&h->link, (void *)h);
	gbAddLast(&g_list, &h->link);
}
int gbRunHooks(gb_hookID_t id, void *data, void *context) {
	struct Hook *h;
	int r;

	Init();

	for(h=(struct Hook *)gbFirst(&g_list); h;
	h=(struct Hook *)gbAfter(&h->link)) {
		if (h->id != id)
			continue;

		if (context && h->context!=context)
			continue;

		if (!(r = h->fnHook(id, data, context)))
			return 0;
	}

	return r;
}
void gbReplaceHook(gb_hookID_t id, gb_fnHook_t fnOld, gb_fnHook_t fnNew,
void *context) {
	struct Hook *h;

	Init();

	for(h=(struct Hook *)gbFirst(&g_list); h;
	h=(struct Hook *)gbAfter(&h->link)) {
		if (h->id != id)
			continue;

		if (h->context != context)
			continue;

		if (h->fnHook != fnOld)
			continue;

		h->fnHook = fnNew;
		break;
	}
}
void gbRemoveHook(gb_hookID_t id, gb_fnHook_t fnHook, void *context) {
	struct Hook *h;

	Init();

	for(h=(struct Hook *)gbFirst(&g_list); h;
	h=(struct Hook *)gbAfter(&h->link)) {
		if (h->id != id)
			continue;

		if (h->context != context)
			continue;

		if (h->fnHook != fnHook)
			continue;

		gbUnlinkNode(&h->link);
		gbMemFree((void *)h);

		break;
	}
}
void gbRemoveAllHooks(gb_hookID_t id, void *context) {
	struct Hook *h, *n;

	Init();

	for(h=(struct Hook *)gbFirst(&g_list); h; h=n) {
		n = (struct Hook *)gbAfter(&h->link);

		if (h->id != id)
			continue;

		if (h->context != context)
			continue;

		gbUnlinkNode(&h->link);
		gbMemFree((void *)h);
	}
}
size_t gbCountHooks(gb_hookID_t id, void *context) {
	struct Hook *h;
	size_t i;

	Init();

	i = 0;
	for(h=(struct Hook *)gbFirst(&g_list); h;
	h=(struct Hook *)gbAfter(&h->link)) {
		if (h->id != id)
			continue;

		if (h->context != context)
			continue;

		i++;
	}

	return i;
}
gb_fnHook_t gbGetHook(gb_hookID_t id, size_t index, void *context) {
	struct Hook *h;
	size_t i;

	Init();

	i = 0;
	for(h=(struct Hook *)gbFirst(&g_list); h;
	h=(struct Hook *)gbAfter(&h->link)) {
		if (h->id != id)
			continue;

		if (h->context != context)
			continue;

		if (i==index)
			return h->fnHook;

		i++;
	}

	return (gb_fnHook_t)NULL;
}
