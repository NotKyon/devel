#ifndef TASKSHARE_ATOMICS_H
#define TASKSHARE_ATOMICS_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

#if _MSC_VER
# include <intrin.h>

# pragma intrinsic(_InterlockedIncrement)
# pragma intrinsic(_InterlockedDecrement)
# pragma intrinsic(_InterlockedExchange)
# pragma intrinsic(_InterlockedCompareExchange)
# pragma intrinsic(_InterlockedAnd)
# pragma intrinsic(_InterlockedOr)
# pragma intrinsic(_InterlockedXor)

# if _WIN64
#  pragma intrinsic(_InterlockedIncrement64)
#  pragma intrinsic(_InterlockedDecrement64)
#  pragma intrinsic(_InterlockedExchange64)
#  pragma intrinsic(_InterlockedCompareExchange64)
#  pragma intrinsic(_InterlockedAnd64)
#  pragma intrinsic(_InterlockedOr64)
#  pragma intrinsic(_InterlockedXor64)
# else
#  define _InterlockedIncrement64 InterlockedIncrement64
#  define _InterlockedDecrement64 InterlockedDecrement64
#  define _InterlockedExchange64 InterlockedExchange64
#  define _InterlockedCompareExchange64 InterlockedCompareExchange64
#  define _InterlockedAnd64 InterlockedAnd64
#  define _InterlockedOr64 InterlockedOr64
#  define _InterlockedXor64 InterlockedXor64
# endif

# if 0
#  pragma intrinsic(_InterlockedExchangePointer)
#  pragma intrinsic(_InterlockedCompareExchangePointer)
# else
#  define _InterlockedExchangePointer InterlockedExchangePointer
#  define _InterlockedCompareExchangePointer InterlockedCompareExchangePointer
# endif
#endif

#if __GNUC__ || __clang__
# include <winbase.h>

# ifndef __forceinline
#  define __forceinline inline __attribute__((always_inline))
# endif

# define _InterlockedIncrement InterlockedIncrement
# define _InterlockedDecrement InterlockedDecrement
# define _InterlockedExchange InterlockedExchange
# define _InterlockedCompareExchange InterlockedCompareExchange
# if 0
#  define _InterlockedAnd InterlockedAnd
#  define _InterlockedOr InterlockedOr
#  define _InterlockedXor InterlockedXor
# else
#  define _InterlockedAnd(x,y) __sync_fetch_and_and((x), (y))
#  define _InterlockedOr(x,y) __sync_fetch_and_or((x), (y))
#  define _InterlockedXor(x,y) __sync_fetch_and_xor((x), (y))
# endif

# if 0
#  define _InterlockedIncrement64 InterlockedIncrement64
#  define _InterlockedDecrement64 InterlockedDecrement64
#  define _InterlockedExchange64 InterlockedExchange64
#  define _InterlockedCompareExchange64 InterlockedCompareExchange64
#  define _InterlockedAnd64 InterlockedAnd64
#  define _InterlockedOr64 InterlockedOr64
#  define _InterlockedXor64 InterlockedXor64
# else
#  define _InterlockedIncrement64(x) __sync_fetch_and_add((x), 1)
#  define _InterlockedDecrement64(x) __sync_fetch_and_sub((x), 1)
#  define _InterlockedExchange64(x,y) __sync_lock_test_and_set((x), (y))
#  define _InterlockedCompareExchange64(x,y,z)\
	__sync_val_compare_and_swap((x), (z), (y))
#  define _InterlockedAnd64(x,y) __sync_fetch_and_and((x), (y))
#  define _InterlockedOr64(x,y) __sync_fetch_and_or((x), (y))
#  define _InterlockedXor64(x,y) __sync_fetch_and_xor((x), (y))
# endif

# if 1
#  define _InterlockedExchangePointer(x,y) __sync_lock_test_and_set((x), (y))
#  define _InterlockedCompareExchangePointer(x,y,z)\
	__sync_val_compare_and_swap((x), (z), (y))
# endif
#endif

#if TASK_CONF_INTERLOCKED_ATOMICS
# if !_WIN32
#  undef TASK_CONF_INTERLOCKED_ATOMICS
#  define TASK_CONF_INTERLOCKED_ATOMICS 0
# endif
#elif !defined(TASK_CONF_INTERLOCKED_ATOMICS)
# if _MSC_VER || __INTEL_COMPILER
#  define TASK_CONF_INTERLOCKED_ATOMICS 1
# endif
#endif

//------------------------------------------------------------------------------
// ATOMICS

__TS_NS_INIT__

// http://gcc.gnu.org/onlinedocs/gcc-4.4.2/gcc/Atomic-Builtins.html

//------------------------------------------------------------------------------
// --32

__forceinline u32 AtomicIncrement(volatile u32 *x) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	return _InterlockedIncrement((volatile LONG *)x);
#else
	return __sync_fetch_and_add(x, 1);
#endif
}
__forceinline u32 AtomicDecrement(volatile u32 *x) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	return _InterlockedDecrement((volatile LONG *)x);
#else
	return __sync_fetch_and_sub(x, 1);
#endif
}
__forceinline u32 AtomicExchange(volatile u32 *x, u32 y) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	return _InterlockedExchange((volatile LONG *)x, y);
#else
	return __sync_lock_test_and_set(x, y);
#endif
}
__forceinline u32 AtomicExchangeIfEqual(volatile u32 *x, u32 y, u32 z) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	//dst, new, old
	return _InterlockedCompareExchange((volatile LONG *)x, y, z);
#else
	//dst, old, new
	return __sync_val_compare_and_swap(x, z, y);
#endif
}

__forceinline u32 AtomicAnd(volatile u32 *x, u32 y) {
#if TASK_CONF_INTERLOCKED_ATOMICS
# if 0
	u32 w, s;

	w = *x;
	s = w&y;

	do {
	} while(_InterlockedExchange((volatile LONG *)x, s)!=s);

	return w;
# else
	return _InterlockedAnd((volatile LONG *)x, y);
# endif
#else
	return __sync_fetch_and_and(x, y);
#endif
}
__forceinline u32 AtomicOr(volatile u32 *x, u32 y) {
#if TASK_CONF_INTERLOCKED_ATOMICS
# if 0
	u32 w, s;

	w = *x;
	s = w|y;

	do {
	} while(_InterlockedExchange((volatile LONG *)x, s)!=s);

	return w;
# else
	return _InterlockedOr((volatile LONG *)x, y);
# endif
#else
	return __sync_fetch_and_or(x, y);
#endif
}
__forceinline u32 AtomicXor(volatile u32 *x, u32 y) {
#if TASK_CONF_INTERLOCKED_ATOMICS
# if 0
	u32 w, s;

	w = *x;
	s = w^y;

	do {
	} while(_InterlockedExchange((volatile LONG *)x, s)!=s);

	return w;
# else
	return _InterlockedXor((volatile LONG *)x, y);
# endif
#else
	return __sync_fetch_and_xor(x, y);
#endif
}

__forceinline u32 AtomicRead(volatile u32 *x) {
	return AtomicOr(x, 0);
	//return AtomicExchangeIfEqual(x, 0, 0);
}

__forceinline u32 AtomicSpinEqual(volatile u32 *x, u32 y) {
	u32 r;

	do {
		r = AtomicRead(x);
	} while(r==y);

	return r;
}
__forceinline u32 AtomicSpinNotEqual(volatile u32 *x, u32 y) {
	u32 r;

	do {
		r = AtomicRead(x);
	} while(r!=y);

	return r;
}

//------------------------------------------------------------------------------
// --64

__forceinline u64 AtomicIncrement(volatile u64 *x) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	return _InterlockedIncrement64((volatile LONGLONG *)x);
#else
	return __sync_fetch_and_add(x, 1);
#endif
}
__forceinline u64 AtomicDecrement(volatile u64 *x) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	return _InterlockedDecrement64((volatile LONGLONG *)x);
#else
	return __sync_fetch_and_sub(x, 1);
#endif
}
__forceinline u64 AtomicExchange(volatile u64 *x, u64 y) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	return _InterlockedExchange64((volatile LONGLONG *)x, y);
#else
	return __sync_lock_test_and_set(x, y);
#endif
}
__forceinline u64 AtomicExchangeIfEqual(volatile u64 *x, u64 y, u64 z) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	//dst, new, old
	return _InterlockedCompareExchange64((volatile LONGLONG *)x, y, z);
#else
	//dst, old, new
	return __sync_val_compare_and_swap(x, z, y);
#endif
}

__forceinline u64 AtomicAnd(volatile u64 *x, u64 y) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	return _InterlockedAnd64((volatile LONGLONG *)x, y);
#else
	return __sync_fetch_and_and(x, y);
#endif
}
__forceinline u64 AtomicOr(volatile u64 *x, u64 y) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	return _InterlockedOr64((volatile LONGLONG *)x, y);
#else
	return __sync_fetch_and_or(x, y);
#endif
}
__forceinline u64 AtomicXor(volatile u64 *x, u64 y) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	return _InterlockedXor64((volatile LONGLONG *)x, y);
#else
	return __sync_fetch_and_xor(x, y);
#endif
}

__forceinline u64 AtomicRead(volatile u64 *x) {
	return AtomicOr(x, 0);
	//return AtomicExchangeIfEqual(x, 0, 0);
}

__forceinline u64 AtomicSpinEqual(volatile u64 *x, u64 y) {
	u64 r;

	do {
		r = AtomicRead(x);
	} while(r==y);

	return r;
}
__forceinline u64 AtomicSpinNotEqual(volatile u64 *x, u64 y) {
	u64 r;

	do {
		r = AtomicRead(x);
	} while(r!=y);

	return r;
}

//------------------------------------------------------------------------------
// --ptr

template<typename T>
__forceinline T *AtomicExchange(T *volatile *x, T *y) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	return _InterlockedExchangePointer((void *volatile *)x, (void *)y);
#else
	return __sync_lock_test_and_set(x, y);
#endif
}
template<typename T>
__forceinline T *AtomicExchangeIfEqual(T *volatile *x, T *y, T *z) {
#if TASK_CONF_INTERLOCKED_ATOMICS
	//dst, new, old
	return _InterlockedCompareExchangePointer((void *volatile *)x,
		(void *)y, (void *)z);
#else
	//dst, old, new
	return __sync_val_compare_and_swap(x, z, y);
#endif
}

template<typename T>
__forceinline T *AtomicRead(T *volatile *x) {
#if _WIN64 || __x86_64__
	return (T *)AtomicRead((volatile u64 *)x);
#elif _WIN32 || __x86__
	return (T *)AtomicRead((volatile u32 *)x);
#else
	if (sizeof(T *)==8)
		return (T *)AtomicRead((volatile u64 *)x);

	return (T *)AtomicRead((volatile u32 *)x);
#endif
}

template<typename T>
__forceinline T *AtomicSpinEqual(T *volatile *x, T *y) {
	T *r;

	do {
		r = AtomicRead(x);
	} while(r==y);

	return r;
}
template<typename T>
__forceinline T *AtomicSpinNotEqual(T *volatile *x, T *y) {
	T *r;

	do {
		r = AtomicRead(x);
	} while(r!=y);

	return r;
}

//------------------------------------------------------------------------------

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
