﻿/*
===============================================================================

	SDF ALLOCATORS AND LIST REDESIGN EXPERIMENTATION (2014-03-06)

	This file is for R&D purposes only.

	Currently investigating the use of type-based memory tags, state-full and
	stateless allocators (and their effect on containers), and a number of
	helper functions for containers.

	Also implementing a list<> container since none exist in SDF at the time of
	this writing.

	$ g++ -g -D_DEBUG -std=gnu++11 -W -Wall -pedantic -Wfatal-errors \
	  -o sdf_allocators_and_list sdf_allocators_and_list.cpp

===============================================================================
*/

#ifndef SDF_DEBUG_ENABLED
# if defined( DEBUG ) || defined( _DEBUG ) || defined( __DEBUG__ )
#  define SDF_DEBUG_ENABLED 1
# elif defined( debug ) || defined( _debug ) || defined( __debug__ )
#  define SDF_DEBUG_ENABLED 1
# else
#  define SDF_DEBUG_ENABLED 0
# endif
#endif
#ifndef SDF_TEST_ENABLED
# if defined( TEST ) || defined( _TEST ) || defined( __TEST__ )
#  define SDF_TEST_ENABLED 1
# elif defined( test ) || defined( _test ) || defined( __test__ )
#  define SDF_TEST_ENABLED 1
# else
#  define SDF_TEST_ENABLED 0
# endif
#endif

#define SDF_PLATFORM_WINDOWS 0x01
#define SDF_PLATFORM_LINUX 0x02
#define SDF_PLATFORM_MAC 0x04

#ifndef SDF_PLATFORM
# if defined( _WIN32 )
#  define SDF_PLATFORM ( SDF_PLATFORM_WINDOWS )
# elif defined( linux ) || defined( __linux ) || defined( __linux__ )
#  define SDF_PLATFORM ( SDF_PLATFORM_LINUX )
# elif defined( __APPLE__ )
#  define SDF_PLATFORM ( SDF_PLATFORM_MAC )
# else
#  define SDF_PLATFORM 0 //wtf is this?
# endif
#endif

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define SDF_THREAD_MODEL_WINDOWS 1
#define SDF_THREAD_MODEL_PTHREAD 2

#ifndef SDF_THREAD_MODEL
# if SDF_PLATFORM & SDF_PLATFORM_WINDOWS
#  define SDF_THREAD_MODEL SDF_THREAD_MODEL_WINDOWS
# else
#  define SDF_THREAD_MODEL SDF_THREAD_MODEL_PTHREAD
# endif
#endif

#if SDF_THREAD_MODEL == SDF_THREAD_MODEL_WINDOWS
# undef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN 1
# include <Windows.h>
# undef min
# undef max
#elif SDF_THREAD_MODEL == SDF_THREAD_MODEL_PTHREAD
# include <pthread.h>
#else
# error SDF: Unhandled SDF_THREAD_MODEL
#endif

#if SDF_THREAD_MODEL == SDF_THREAD_MODEL_WINDOWS
# define SDF_THREAD_CALL __stdcall
namespace sdf
{

	typedef DWORD threadResult_t;

}
#elif SDF_THREAD_MODEL == SDF_THREAD_MODEL_PTHREAD
# define SDF_THREAD_CALL
namespace sdf
{

	typedef void *threadResult_t;

}
#else
# error SDF: SDF_THREAD_CALL, sdf::threadResult_t: Unhandled SDF_THREAD_MODEL
#endif

//#include <new>

inline void *operator new( size_t, void *p )
{
	return p;
}

#define SDF_PURE
#define SDF_IF_UNLIKELY( x ) if( x )

// <testing> //
namespace sdf
{

	typedef ::int8_t s8;
	typedef ::int16_t s16;
	typedef ::int32_t s32;
	typedef ::int64_t s64;
	typedef ::uint8_t u8;
	typedef ::uint16_t u16;
	typedef ::uint32_t u32;
	typedef ::uint64_t u64;
	typedef ::size_t size_t;
	typedef ::ptrdiff_t diff_t;
	
	typedef u32 uint;
	typedef s32 sint;

	typedef size_t uptr;
	typedef diff_t sptr;

	template< typename TElement, TElement TValue >
	struct integral_constant
	{
		static constexpr TElement value = TValue;
		//static const TElement value = TValue;
	};

	struct false_type: public integral_constant< bool, false > {};
	struct true_type: public integral_constant< bool, true > {};

	template< typename TFirst, typename TSecond >
	struct is_same: public false_type {};

	template< typename TType >
	struct is_same< TType, TType >: public true_type {};
	
	template< typename TBase, typename TDerived >
	struct is_base_of
	: public integral_constant< bool, __is_base_of( TBase, TDerived ) >
	{
	};

	template< typename TBase, typename TDerived >
	struct is_equal
	: public integral_constant
	<
		bool,
		is_same< TBase, TDerived >::value ||
		is_base_of< TBase, TDerived >::value
	>
	{
	};

	/// Align a value to the nearest alignment granularity.
	inline uptr align_nearest( uptr x, uptr alignment ) SDF_PURE
	{
		return x + ( alignment - ( x%alignment ) )%alignment;
	}
	/// Align a value to the next alignment granularity.
	inline uptr align( uptr x, uptr alignment ) SDF_PURE
	{
		return x + ( alignment - x%alignment );
	}

	/*
	===========================================================================
	
		ATOMICS - INT
	
	===========================================================================
	*/
	inline int atomic_inc( volatile int *dst )
	{
#if defined( _MSC_VER )
		return _InterlockedIncrement( ( volatile LONG * )dst );
#else
		return __sync_fetch_and_add( dst, 1 );
#endif
	}
	inline int atomic_dec( volatile int *dst )
	{
#if defined( _MSC_VER )
		return _InterlockedDecrement( ( volatile LONG * )dst );
#else
		return __sync_fetch_and_sub( dst, 1 );
#endif
	}
	inline int atomic_add( volatile int *dst, int src )
	{
#if defined( _MSC_VER )
		return _InterlockedExchangeAdd( ( volatile LONG * )dst, src );
#else
		return __sync_fetch_and_add( dst, src );
#endif
	}
	inline int atomic_sub( volatile int *dst, int src )
	{
#if defined( _MSC_VER )
		return _InterlockedExchangeAdd( ( volatile LONG * )dst, -( s32 )src );
#else
		return __sync_fetch_and_sub( dst, src );
#endif
	}
	inline int atomic_set( volatile int *dst, int src )
	{
#if defined( _MSC_VER )
		return _InterlockedExchange( ( volatile LONG * )dst, src );
#else
		return __sync_lock_test_and_set( dst, src );
#endif
	}
	inline int atomic_set_eq( volatile int *dst, int src, int cmp )
	{
#if defined( _MSC_VER )
		return _InterlockedCompareExchange( ( volatile LONG * )dst, src, cmp );
#else
		return __sync_val_compare_and_swap( dst, cmp, src );
#endif
	}
	inline int atomic_and( volatile int *dst, int src )
	{
#if defined( _MSC_VER )
		return _InterlockedAnd( ( volatile LONG * )dst, src );
#else
		return __sync_fetch_and_and( dst, src );
#endif
	}
	inline int atomic_or( volatile int *dst, int src )
	{
#if defined( _MSC_VER )
		return _InterlockedOr( ( volatile LONG * )dst, src );
#else
		return __sync_fetch_and_or( dst, src );
#endif
	}
	inline int atomic_xor( volatile int *dst, int src )
	{
#if defined( _MSC_VER )
		return _InterlockedXor( ( volatile LONG * )dst, src );
#else
		return __sync_fetch_and_xor( dst, src );
#endif
	}
	
	/*
	===========================================================================

		ATOMICS - 32-BIT

	===========================================================================
	*/
	inline u32 atomic_inc( volatile u32 *dst )
	{
#if defined( _MSC_VER )
		return _InterlockedIncrement( ( volatile LONG * )dst );
#else
		return __sync_fetch_and_add( dst, 1 );
#endif
	}
	inline u32 atomic_dec( volatile u32 *dst )
	{
#if defined( _MSC_VER )
		return _InterlockedDecrement( ( volatile LONG * )dst );
#else
		return __sync_fetch_and_sub( dst, 1 );
#endif
	}
	inline u32 atomic_add( volatile u32 *dst, u32 src )
	{
#if defined( _MSC_VER )
		return _InterlockedExchangeAdd( ( volatile LONG * )dst, src );
#else
		return __sync_fetch_and_add( dst, src );
#endif
	}
	inline u32 atomic_sub( volatile u32 *dst, u32 src )
	{
#if defined( _MSC_VER )
		return _InterlockedExchangeAdd( ( volatile LONG * )dst, -( s32 )src );
#else
		return __sync_fetch_and_sub( dst, src );
#endif
	}
	inline u32 atomic_set( volatile u32 *dst, u32 src )
	{
#if defined( _MSC_VER )
		return _InterlockedExchange( ( volatile LONG * )dst, src );
#else
		return __sync_lock_test_and_set( dst, src );
#endif
	}
	inline u32 atomic_set_eq( volatile u32 *dst, u32 src, u32 cmp )
	{
#if defined( _MSC_VER )
		return _InterlockedCompareExchange( ( volatile LONG * )dst, src, cmp );
#else
		return __sync_val_compare_and_swap( dst, cmp, src );
#endif
	}
	inline u32 atomic_and( volatile u32 *dst, u32 src )
	{
#if defined( _MSC_VER )
		return _InterlockedAnd( ( volatile LONG * )dst, src );
#else
		return __sync_fetch_and_and( dst, src );
#endif
	}
	inline u32 atomic_or( volatile u32 *dst, u32 src )
	{
#if defined( _MSC_VER )
		return _InterlockedOr( ( volatile LONG * )dst, src );
#else
		return __sync_fetch_and_or( dst, src );
#endif
	}
	inline u32 atomic_xor( volatile u32 *dst, u32 src )
	{
#if defined( _MSC_VER )
		return _InterlockedXor( ( volatile LONG * )dst, src );
#else
		return __sync_fetch_and_xor( dst, src );
#endif
	}


	/*
	===========================================================================

		ATOMICS - 64-BIT

	===========================================================================
	*/
	inline u64 atomic_inc( volatile u64 *dst )
	{
#if defined( _MSC_VER )
		return _InterlockedIncrement64( ( volatile LONGLONG * )dst );
#else
		return __sync_fetch_and_add( dst, 1 );
#endif
	}
	inline u64 atomic_dec( volatile u64 *dst )
	{
#if defined( _MSC_VER )
		return _InterlockedDecrement64( ( volatile LONGLONG * )dst );
#else
		return __sync_fetch_and_sub( dst, 1 );
#endif
	}
	inline u64 atomic_add( volatile u64 *dst, u64 src )
	{
#if defined( _MSC_VER )
		return _InterlockedExchangeAdd64( ( volatile LONGLONG * )dst, src );
#else
		return __sync_fetch_and_add( dst, src );
#endif
	}
	inline u64 atomic_sub( volatile u64 *dst, u64 src )
	{
#if defined( _MSC_VER )
		return _InterlockedExchangeAdd64( ( volatile LONGLONG * )dst,
			-( s64 )src );
#else
		return __sync_fetch_and_sub( dst, src );
#endif
	}
	inline u64 atomic_set( volatile u64 *dst, u64 src )
	{
#if defined( _MSC_VER )
		return _InterlockedExchange64( ( volatile LONGLONG * )dst, src );
#else
		return __sync_lock_test_and_set( dst, src );
#endif
	}
	inline u64 atomic_set_eq( volatile u64 *dst, u64 src, u64 cmp )
	{
#if defined( _MSC_VER )
		return _InterlockedCompareExchange64( ( volatile LONGLONG * )dst, src,
			cmp );
#else
		return __sync_val_compare_and_swap( dst, cmp, src );
#endif
	}
	inline u64 atomic_and( volatile u64 *dst, u64 src )
	{
#if defined( _MSC_VER )
		return _InterlockedAnd64( ( volatile LONGLONG * )dst, src );
#else
		return __sync_fetch_and_and( dst, src );
#endif
	}
	inline u64 atomic_or( volatile u64 *dst, u64 src )
	{
#if defined( _MSC_VER )
		return _InterlockedOr64( ( volatile LONGLONG * )dst, src );
#else
		return __sync_fetch_and_or( dst, src );
#endif
	}
	inline u64 atomic_xor( volatile u64 *dst, u64 src )
	{
#if defined( _MSC_VER )
		return _InterlockedXor64( ( volatile LONGLONG * )dst, src );
#else
		return __sync_fetch_and_xor( dst, src );
#endif
	}

	/*
	===========================================================================

		ATOMICS - POINTERS

	===========================================================================
	*/
	template< typename _type1_, typename _type2_ >
	inline _type1_ *atomic_set_ptr( _type1_ *volatile *dst, _type2_ *src )
	{
#if defined( _MSC_VER )
		return ( _type1_ * )_InterlockedExchangePointer(
			( void *volatile * )dst, ( void * )src );
#else
		return __sync_lock_test_and_set( dst, src );
#endif
	}
	template< typename _type_ >
	inline _type_ *atomic_set_ptr( _type_ *volatile *dst, decltype( nullptr ) )
	{
		return atomic_set_ptr( dst, ( _type_ * )0 );
	}
	template< typename _type1_, typename _type2_, typename _type3_ >
	inline _type1_ *atomic_set_ptr_eq( _type1_ *volatile *dst, _type2_ *src,
	_type3_ *cmp )
	{
#if defined( _MSC_VER )
		return ( _type1 * )_InterlockedCompareExchangePointer(
			( void *volatile * )dst, ( void * )src, ( void * )cmp );
#else
		return __sync_val_compare_and_swap( dst, cmp, src );
#endif
	}

	/*
	===========================================================================

		ATOMICS - WRAPPER CLASS

	===========================================================================
	*/
	typedef uint atomic_t;

	struct atomic_integer {
	public:
		inline atomic_integer( atomic_t value = 0 ): mValue( value )
		{
		}
		inline ~atomic_integer()
		{
		}

		inline atomic_t increment()
		{
			return atomic_inc( &mValue );
		}
		inline atomic_t decrement()
		{
			return atomic_dec( &mValue );
		}

		inline atomic_t set( uint x )
		{
			return atomic_set( &mValue, x );
		}
		inline atomic_t get() const
		{
			return mValue;
		}

		inline atomic_integer &operator++()
		{
			atomic_inc( &mValue );
			return *this;
		}
		inline atomic_integer operator++( int )
		{
			return atomic_integer( atomic_inc( &mValue ) );
		}

		inline atomic_integer &operator--()
		{
			atomic_dec( &mValue );
			return *this;
		}
		inline atomic_integer operator--( int )
		{
			return atomic_integer( atomic_dec( &mValue ) );
		}

	private:
		atomic_t mValue;
	};

	/// \brief Mutual-exclusion (mutex) synchronization primitive
	class mutex
	{
	public:
		inline mutex(): mMutex()
		{
#if SDF_THREAD_MODEL == SDF_THREAD_MODEL_WINDOWS
			( void )InitializeCriticalSectionAndSpinCount( &mMutex, 4096 );
#elif SDF_THREAD_MODEL == SDF_THREAD_MODEL_PTHREAD
			static pthread_mutexattr_t attr;
			static bool didInit = false;

			SDF_IF_UNLIKELY( !didInit )
			{
				pthread_mutexattr_init( &attr );
				pthread_mutexattr_settype( &attr, PTHREAD_MUTEX_RECURSIVE );

				didInit = true;
			}

			pthread_mutex_init( &mMutex, &attr );
#else
# error SDF: SDF_THREAD_MODEL( mutex::mutex() ): Unhandled thread model
#endif
		}
		inline ~mutex()
		{
#if SDF_THREAD_MODEL == SDF_THREAD_MODEL_WINDOWS
			DeleteCriticalSection( &mMutex );
#elif SDF_THREAD_MODEL == SDF_THREAD_MODEL_PTHREAD
			pthread_mutex_destroy( &mMutex );
#else
# error SDF: SDF_THREAD_MODEL( mutex::~mutex() ): Unhandled thread model
#endif
		}

		inline void lock()
		{
#if SDF_THREAD_MODEL == SDF_THREAD_MODEL_WINDOWS
			EnterCriticalSection( &mMutex );
#elif SDF_THREAD_MODEL == SDF_THREAD_MODEL_PTHREAD
			pthread_mutex_lock( &mMutex );
#else
# error SDF: SDF_THREAD_MODEL( mutex::lock() ): Unhandled thread model
#endif
		}
		inline bool try_lock()
		{
#if SDF_THREAD_MODEL == SDF_THREAD_MODEL_WINDOWS
			return TryEnterCriticalSection( &mMutex ) != false;
#elif SDF_THREAD_MODEL == SDF_THREAD_MODEL_PTHREAD
			return pthread_mutex_trylock( &mMutex ) == false;
#else
# error SDF: SDF_THREAD_MODEL( mutex::try_lock() ): Unhandled thread model
#endif
		}
		inline void unlock()
		{
#if SDF_THREAD_MODEL == SDF_THREAD_MODEL_WINDOWS
			LeaveCriticalSection( &mMutex );
#elif SDF_THREAD_MODEL == SDF_THREAD_MODEL_PTHREAD
			pthread_mutex_unlock( &mMutex );
#else
# error SDF: SDF_THREAD_MODEL( mutex::unlock() ): Unhandled thread model
#endif
		}

	private:
#if SDF_THREAD_MODEL == SDF_THREAD_MODEL_WINDOWS
		CRITICAL_SECTION mMutex;
#elif SDF_THREAD_MODEL == SDF_THREAD_MODEL_PTHREAD
		pthread_mutex_t mMutex;
#else
# error SDF: SDF_THREAD_MODEL( mutex ): Unhandled thread model
#endif
	};

	/// \brief Abstract scope-based locking class
	template< class Mutex >
	class lock_guard
	{
	public:
		typedef Mutex mutex_type;

		inline lock_guard( Mutex &m ): mMutex( m )
		{
			mMutex.lock();
		}
		inline ~lock_guard()
		{
			mMutex.unlock();
		}

#if SDF_CXX_DEFAULT_DELETE_FUNCTIONS_ENABLED
		lock_guard( const lock_guard & ) = delete;
		lock_guard &operator=( const lock_guard & ) = delete;
#endif

	private:
		mutex_type mMutex;

#if !SDF_CXX_DEFAULT_DELETE_FUNCTIONS_ENABLED
		lock_guard( const lock_guard & ) {}
		lock_guard &operator=( const lock_guard & ) { return *this; }
#endif
	};
	
	/// \brief Locks a mutex on construct and unlocks on destruct
	typedef lock_guard< mutex > mutex_guard;

}
// </testing> //

/// \def SDF_DEFAULT_MEMTAG_NAME
/// \brief The default name given to each allocator instance.
#ifndef SDF_DEFAULT_MEMTAG_NAME
# define SDF_DEFAULT_MEMTAG_NAME "Default"
#endif

/// \def SDF_MEMTAG_NAME_PREFIX
/// \brief Prefix to use for internal SDF allocators
#ifndef SDF_MEMTAG_NAME_PREFIX
# define SDF_MEMTAG_NAME_PREFIX "SDF "
#endif

/// \def SDF_ALLOC_DEVMODE
/// \brief Specifies whether development-mode should be "on" for allocators
#ifndef SDF_ALLOC_DEVMODE
# define SDF_ALLOC_DEVMODE ( SDF_DEBUG_ENABLED || SDF_TEST_ENABLED )
#endif

/// \def SDF_ALLOC_DEBUG_ENABLED
/// \brief Specifies whether extra debug information is stored per allocation
#ifndef SDF_ALLOC_DEBUG_ENABLED
# define SDF_ALLOC_DEBUG_ENABLED SDF_ALLOC_DEVMODE
#endif
/// \def SDF_ALLOC_STATS_ENABLED
/// \brief Specifies whether statistics are tracked for allocs/deallocs
#ifndef SDF_ALLOC_STATS_ENABLED
# define SDF_ALLOC_STATS_ENABLED SDF_ALLOC_DEVMODE
#endif
/// \def SDF_ALLOC_FILE_LINE_ENABLED
/// \brief Specifies whether file/line information is stored per allocation.
#ifndef SDF_ALLOC_FILE_LINE_ENABLED
# define SDF_ALLOC_FILE_LINE_ENABLED SDF_ALLOC_DEVMODE
#endif
/// \def SDF_ALLOC_HEADER_LIST_ENABLED
/// \brief Specifies whether each allocation header should be tracked in a giant
///        list or not.
#ifndef SDF_ALLOC_HEADER_LIST_ENABLED
# define SDF_ALLOC_HEADER_LIST_ENABLED SDF_ALLOC_DEVMODE
#endif
/// \def SDF_ALLOC_CALL_STACK_ENABLED
/// \brief Specifies whether the call stack should be captured on each
///        allocation. (Off by default.)
#ifndef SDF_ALLOC_CALL_STACK_ENABLED
# define SDF_ALLOC_CALL_STACK_ENABLED 0
#endif

namespace sdf
{

	/// \brief Information about memory allocations.
	struct alloc_stats
	{
		/// Current total allocated size, in bytes.
		size_t size;
		/// Highest total allocated size so far, in bytes.
		size_t peak_size;
		/// Preferred maximum size that can be allocated, in bytes.
		size_t budget_size;

		/// Total number of allocate() calls that have occurred.
		uint alloc_count;
		/// Total number of deallocate() calls that have occurred.
		uint dealloc_count;
		/// Total number of bytes requested.
		u64 alloc_bytes;
		/// Total time (in microseconds) spent allocating.
		u64 alloc_time;
		/// Total time (in microseconds) spent deallocating.
		u64 dealloc_time;

		/// Change in alloc_count since last query.
		uint delta_alloc_count;
		/// Change in dealloc_count since last query.
		uint delta_dealloc_count;
		/// Change in the total number of bytes requested since last query.
		u64 delta_alloc_bytes;
		/// Change in alloc_time since last query.
		u64 delta_alloc_time;
		/// Change in dealloc_time since last query.
		u64 delta_dealloc_time;
	};

	class allocator;

	namespace detail
	{
	
		/// \brief Allocation header
		///
		/// This header exists at the start of every allocation. Portions of the
		/// header can be removed entirely based on a few preprocessor defines,
		/// even to the point of making the header take no space. Likewise,
		/// additional features (for example, a call-stack per allocation) can
		/// be enabled which will increase the size of the header.
		///
		/// In release mode, there shouldn't be any header at all (but this is
		/// just the default; if some header data is required even in release
		/// mode it can be configured that way).
		struct alloc_header
		{
#define SDF_ALLOC_HEADER_IS_EMPTY__ 1

#if SDF_ALLOC_DEBUG_ENABLED
# undef SDF_ALLOC_HEADER_IS_EMPTY__
			/// Start of the data
			void *data;
			/// Magic data
			u32 magic;
#endif
#if SDF_ALLOC_STATS_ENABLED
# undef SDF_ALLOC_HEADER_IS_EMPTY__
			/// Size of this allocation, in bytes
			size_t size;
			/// Allocator tag
			int tag;
#endif
#if SDF_ALLOC_FILE_LINE_ENABLED
# undef SDF_ALLOC_HEADER_IS_EMPTY__
			/// File the allocation occurred from
			const char *file;
			/// Line of the file the allocation occurred from
			int line;
#endif
#if SDF_ALLOC_HEADER_LIST_ENABLED
# undef SDF_ALLOC_HEADER_IS_EMPTY__
			/// Previous header in list
			alloc_header *prev;
			/// Next header in list
			alloc_header *next;
#endif
#if SDF_ALLOC_CALL_STACK_ENABLED
# undef SDF_ALLOC_HEADER_IS_EMPTY__
			static const u16 kMaxStack = 24;

			/// Number of stack pointers found
			size_t callstack_size;
			/// Stack pointers
			void *callstack[ kMaxStack ];
#endif

#ifndef SDF_ALLOC_HEADER_IS_EMPTY__
# define SDF_ALLOC_HEADER_IS_EMPTY__ 0
#endif

			static const bool is_empty = !!( SDF_ALLOC_HEADER_IS_EMPTY__ );
			static const u32 magic_value = 0x1C0FFEE2;
		};

	}

	struct memtag_default {};
	struct memtag_temp {};

	struct memtag_vector {};
	struct memtag_string {};
	struct memtag_list {};
	struct memtag_map {};

	class heap_allocator;

	template< typename TMemTag >
	struct default_allocator
	{
		typedef heap_allocator allocator_type;
		static allocator_type &get();
	};

	template< typename TMemTag >
	struct named_allocation
	{
		static inline const char *get_name()
		{
			return SDF_DEFAULT_MEMTAG_NAME;
		}
	};

#define SDF_DECLARE_MEMTAG_NAME(memtag,name)\
	template<> struct sdf::named_allocation< memtag > {\
		static inline const char *get_name() { return name; }\
	}

#define SDF_INTERNAL_DECLARE_MEMTAG_NAME(tag)\
	template<> struct named_allocation< memtag_##tag > {\
		static inline const char *get_name() {\
			return SDF_MEMTAG_NAME_PREFIX #tag;\
		}\
	}

	SDF_INTERNAL_DECLARE_MEMTAG_NAME( default );
	SDF_INTERNAL_DECLARE_MEMTAG_NAME( temp );
	SDF_INTERNAL_DECLARE_MEMTAG_NAME( vector );
	SDF_INTERNAL_DECLARE_MEMTAG_NAME( string );
	SDF_INTERNAL_DECLARE_MEMTAG_NAME( list );
	SDF_INTERNAL_DECLARE_MEMTAG_NAME( map );

	namespace detail
	{
	
		struct memtag_db
		{
			static const size_t max_tags = 512;

			struct object
			{
				int base_id;
				const char *names[ max_tags ];
				alloc_stats stats[ max_tags ];

				static object &get_instance();

			private:
				inline object(): base_id( 1 )
				{
					memset( &names[ 0 ], 0, sizeof( names ) );
					memset( &stats[ 0 ], 0, sizeof( stats ) );

					names[ 0 ] = "Default";
				}
			};

			static inline int get_count()
			{
				const int n = object::get_instance().base_id;

				if( static_cast< size_t >( n ) > max_tags )
				{
					return static_cast< int >( max_tags );
				}

				return n;
			}

			static inline int get_id()
			{
				return atomic_inc( &object::get_instance().base_id );
			}

			static inline void insert_name( int id, const char *name )
			{
				if( static_cast< size_t >( id ) >= max_tags )
				{
					return;
				}

				object::get_instance().names[ id ] = name;
			}
			static inline const char *get_name( int id )
			{
				if( static_cast< size_t >( id ) >= max_tags )
				{
					id = 0;
				}

				return object::get_instance().names[ id ];
			}
			static inline alloc_stats &get_stats( int id )
			{
				if( static_cast< size_t >( id ) >= max_tags )
				{
					id = 0;
				}

				return object::get_instance().stats[ id ];
			}
		};

		template< typename TMemTag >
		struct memtag_id
		{
			const int id;

			static inline int get()
			{
				static memtag_id instance;
				return instance;
			}

			inline operator int() const
			{
				return id;
			}
			inline operator const char *() const
			{
				return memtag_db::get_name( id );
			}

		private:
			inline memtag_id(): id( memtag_db::get_id() )
			{
				memtag_db::insert_name
				(
					id,
					named_allocation< TMemTag >::get_name()
				);
			}
		};
	
	}

	static inline int get_memtag_count()
	{
		return detail::memtag_db::get_count();
	}

	template< typename TMemTag >
	static inline int get_memtag_id()
	{
		return detail::memtag_id< TMemTag >::get();
	}
	template< typename TMemTag >
	static inline int get_memtag_id( const TMemTag & )
	{
		return get_memtag_id< TMemTag >();
	}

	template< typename TMemTag >
	static inline const char *get_memtag_name()
	{
		return named_allocation< TMemTag >::get_name();
	}
	template< typename TMemTag >
	static inline const char *get_memtag_name( const TMemTag & )
	{
		return get_memtag_name< TMemTag >();
	}
	static inline const char *get_memtag_name( int memtagId )
	{
		return detail::memtag_db::get_name( memtagId );
	}

	template< typename TMemTag >
	static inline const alloc_stats &get_memtag_stats()
	{
		return detail::memtag_db::get_stats( get_memtag_id< TMemTag >() );
	}
	template< typename TMemTag >
	static inline const alloc_stats &get_memtag_stats( const TMemTag & )
	{
		return get_memtag_stats< TMemTag >();
	}
	static inline const alloc_stats &get_memtag_stats( int memtagId )
	{
		return detail::memtag_db::get_stats( memtagId );
	}

	/// \brief Utility structure to determine whether an allocator is stateless
	///
	/// Stateless allocators can be constructed and destructed without affecting
	/// their allocations and do not need to be stored within their containers.
	///
	/// By default, unknown allocators are assumed to contain state. When an
	/// allocator contains state it will have a reference to it stored within
	/// the container it is initialized in.
	///
	/// When an allocator is marked as a stateless allocator, containers will
	/// not store a reference to the allocator, which reduces the sizes of those
	/// containers.
	template< typename TAlloc >
	struct is_stateless_allocator: public false_type {};

#define SDF_DECLARE_STATELESS_ALLOCATOR(alloc)\
	template<> struct sdf::is_stateless_allocator< alloc >: public true_type {}

#define SDF_INTERNAL_DECLARE_STATELESS_ALLOCATOR(alloc)\
	template<> struct is_stateless_allocator< alloc >: public true_type {}
	
	/// \brief Denotes a node allocator
	///
	/// Node allocators can allocate and deallocate data of a specific size.
	/// This size will likely have been selected for the allocator during its
	/// construction or initialization.
	struct allocator_tag_node {};
	template< typename TAlloc >
	struct is_node_allocator
	: public is_equal< allocator_tag_node, typename TAlloc::category >
	{
	};

	/// \brief Denotes a basic allocator
	///
	/// Basic allocators are node allocators that support variable-sized
	/// allocations.
	struct allocator_tag_basic: public allocator_tag_node {};
	template< typename TAlloc >
	struct is_basic_allocator
	: public is_equal< allocator_tag_basic, typename TAlloc::category >
	{
	};

	/// \brief Denotes a linear allocator
	///
	/// Linear allocators can allocate data but cannot deallocate. It is often
	/// the case that this allocator will be managing a fixed section of memory.
	///
	/// \note This is derived from \ref allocator_tag_basic to enable support
	///       for them in most containers. Their only flaw is that they can't
	///       deallocate individual items which means the user is directly
	///       responsible for reclaiming memory. This doesn't lack functionality
	///       compared to a basic allocator from the point of view of a
	///       container class.
	struct allocator_tag_linear: public allocator_tag_basic {};
	template< typename TAlloc >
	struct is_linear_allocator
	: public is_equal< allocator_tag_linear, typename TAlloc::category >
	{
	};
	
	/// \brief Denotes a resizeable allocator
	///
	/// Resizeable allocators are basic allocators with enhanced functionality
	/// to support memory resizes with the potential of remaining at the same
	/// base address.
	struct allocator_tag_resizeable: public allocator_tag_basic {};
	template< typename TAlloc >
	struct is_resizeable_allocator
	: public is_equal< allocator_tag_resizeable, typename TAlloc::category >
	{
	};

	/// \brief Utility structure to determine whether an allocator is capable of
	///        performing deallocations
	///
	/// If an allocator is unable to perform deallocations then some memory
	/// footprint optimizations can be made for aligned allocations. (Useful for
	/// static/fixed memory allocators.)
	template< typename TAlloc >
	struct is_deallocatable_allocator
	: public integral_constant< bool, !is_linear_allocator< TAlloc >::value >
	{
	};

	namespace detail
	{
	
		class allocator_wrapper_base
		{
		public:
			// TODO: This could be a constexpr
			static inline size_t calculate_allocation_size( size_t n,
			size_t alignment, bool canDealloc = true )
			{
				static const size_t ptrsize = sizeof( void * );

				const size_t effptrsize = ptrsize*+canDealloc;

				return
					n +
					( alignment > 1 ? alignment - 1 + effptrsize : 0 ) +
					( alloc_header::is_empty ? 0 : sizeof( alloc_header ) );
			}

			static inline void *align( void *base, size_t alignment,
			size_t offset )
			{
				if( alignment <= 1 )
				{
					return base;
				}

				const size_t addr = reinterpret_cast< size_t >( base ) + offset;
				const size_t aadr = sdf::align( addr, alignment ) - offset;

				return reinterpret_cast< void * >( aadr );
			}

			static inline void *track_alloc( void *base, size_t size,
			const char *file, int line, int tag )
			{
				// All of these parameters are potentially unused
				( void )size;
				( void )file;
				( void )line;
				( void )tag;

#if !SDF_ALLOC_HEADER_IS_EMPTY__
				alloc_header *const hdr =
					reinterpret_cast< alloc_header * >( base );
				void *const rptr = reinterpret_cast< void * >( hdr + 1 );

# if SDF_ALLOC_DEBUG_ENABLED
				hdr->data = rptr;
				hdr->magic = alloc_header::magic_value;
# endif
# if SDF_ALLOC_STATS_ENABLED
				hdr->size = size;
				hdr->tag = tag;

				alloc_stats &stats = memtag_db::get_stats( hdr->tag );

				const size_t n = atomic_add( &stats.size, size );
				atomic_add( &stats.alloc_bytes, size );
				atomic_inc( &stats.alloc_count );

				atomic_add( &stats.delta_alloc_bytes, size );
				atomic_inc( &stats.delta_alloc_count );

				//
				//	'n + size' here is to avoid the situation where a
				//	deallocation occurs right after the add above. 'n' is the
				//	previous size of the allocation.
				//
				//	The size of the allocation is *added* to the peak size to
				//	allow for multiple allocations to go through. Effectively,
				//	each allocation submits itself to the peak size if it was
				//	done at a time when it would've raised the peak size.
				//
				//	This is under the assumption that 'n' is never unique across
				//	threads.
				//
				//		TODO: Verify the above assumption.
				//
				if( n + size > stats.peak_size )
				{
					atomic_add( &stats.peak_size, size );
				}

				//
				//	TODO: Warn if going over budget size
				//	-     Keep track of how long since the last warning (maybe
				//	-     five seconds or so) and don't submit another warning
				//	-     until after that time has passed.
				//
# endif
# if SDF_ALLOC_FILE_LINE_ENABLED
				hdr->file = file;
				hdr->line = line;
# endif
# if SDF_ALLOC_CALL_STACK_ENABLED
				hdr->callstack_size = backtrace( hdr->callstack );
# endif
# if SDF_ALLOC_HEADER_LIST_ENABLED
				g().lock();
				hdr->prev = g().tail;
				hdr->next = nullptr;
				if( g().tail != nullptr )
				{
					g().tail->next = hdr;
				}
				else
				{
					g().head = hdr;
				}
				g().tail = hdr;
				g().unlock();
# endif

				return rptr;
#else
				return base;
#endif
			}
			static inline void *track_dealloc( void *base )
			{
#if !SDF_ALLOC_HEADER_IS_EMPTY__
				alloc_header *const hdr =
					reinterpret_cast< alloc_header * >( base ) - 1;
				void *const rptr = reinterpret_cast< void * >( hdr );

# if SDF_ALLOC_STATS_ENABLED
				alloc_stats &stats = memtag_db::get_stats( hdr->tag );

				atomic_sub( &stats.size, hdr->size );
				atomic_inc( &stats.dealloc_count );

				atomic_inc( &stats.delta_alloc_count );
# endif
# if SDF_ALLOC_HEADER_LIST_ENABLED
				g().lock();

				if( hdr->prev != nullptr )
				{
					hdr->prev->next = hdr->next;
				}
				else
				{
					g().head = hdr->next;
				}

				if( hdr->next != nullptr )
				{
					hdr->next->prev = hdr->prev;
				}
				else
				{
					g().tail = hdr->prev;
				}

				g().unlock();
# endif

				return rptr;
#else
				return base;
#endif
			}

		private:
#if SDF_ALLOC_HEADER_LIST_ENABLED
			struct state
			{
				alloc_header *head;
				alloc_header *tail;
				mutex listLock;

				static state &get_instance();

				inline void lock()
				{
					listLock.lock();
				}
				inline void unlock()
				{
					listLock.unlock();
				}

			private:
				inline state()
				: head( nullptr )
				, tail( nullptr )
				, listLock()
				{
				}
			};

			static inline state &g()
			{
				return state::get_instance();
			}
#endif
		};
	
	}

	class basic_virtual_allocator
	{
	public:
		typedef allocator_tag_basic category;

		virtual void *alloc( size_t ) = 0;
		virtual void dealloc( void * ) = 0;
	};
	class node_virtual_allocator
	{
	public:
		typedef allocator_tag_node category;

		virtual void *alloc( size_t ) = 0;
		virtual void dealloc( void * ) = 0;
	};
	class resizeable_virtual_allocator
	{
	public:
		typedef allocator_tag_resizeable category;
		
		virtual void *alloc( size_t ) = 0;
		virtual void dealloc( void * ) = 0;
		
		virtual void *realloc( void *, size_t ) = 0;
	};

	class heap_allocator
	{
	public:
		typedef allocator_tag_resizeable category;
		//typedef allocator_tag_node category;
		
		static heap_allocator &get_instance();

		void *alloc( size_t );
		void dealloc( void * );

		// resizeable, so can realloc
		void *realloc( void *, size_t );
	};
	SDF_INTERNAL_DECLARE_STATELESS_ALLOCATOR( heap_allocator );

	class null_allocator
	{
	public:
		typedef allocator_tag_resizeable category;
		
		static null_allocator &get_instance();

		inline void *alloc( size_t ) { return nullptr; }
		inline void dealloc( void * ) {}
		
		inline void *realloc( void *, size_t ) { return nullptr; }
	};
	SDF_INTERNAL_DECLARE_STATELESS_ALLOCATOR( null_allocator );
	
	template< typename TMemTag >
	inline typename default_allocator< TMemTag >::allocator_type &
	default_allocator< TMemTag >::get()
	{
		return allocator_type::get_instance();
	}

	template< typename TMemTag >
	struct memtag_traits
	{
		typedef typename default_allocator< TMemTag >::allocator_type
			allocator_type;
		typedef typename allocator_type::category category;
		static const bool stateless_allocator =
			is_stateless_allocator< allocator_type >::value;
	};

#define SDF_MEMTAG_ALLOCATOR( memtag )\
	typename sdf::default_allocator< TMemTag >::allocator_type

	template< typename TMemTag >
	inline void *tagged_alloc( SDF_MEMTAG_ALLOCATOR( TMemTag ) &allocator,
	size_t size, size_t alignment = 0, size_t offset = 0,
	const char *file = nullptr, int line = 0 )
	{
		typedef SDF_MEMTAG_ALLOCATOR( TMemTag ) allocator_type;

		const bool canDealloc =
			is_deallocatable_allocator< allocator_type >::value;

		const size_t realSize =
			detail::allocator_wrapper_base::calculate_allocation_size
			(
				size, alignment, canDealloc
			);

		void *const p = allocator.alloc( realSize );
		if( !p )
		{
			return nullptr;
		}

		const int memtagId = get_memtag_id< TMemTag >();

		void *q =
			detail::allocator_wrapper_base::track_alloc
			(
				p, size, file, line, memtagId
			);

		if( alignment > 1 )
		{
			void *const a =
				detail::allocator_wrapper_base::align( q, alignment, offset );
			*reinterpret_cast< void ** >( reinterpret_cast< uptr >( a ) -
				sizeof( void * ) ) = q;
			q = a;
		}

		return q;
	}

	enum class dealloc_flag
	{
		unaligned,
		aligned
	};
	
	template< typename TMemTag >
	inline void *tagged_dealloc( SDF_MEMTAG_ALLOCATOR( TMemTag ) &allocator,
	void *p, dealloc_flag flag = dealloc_flag::unaligned )
	{
		typedef SDF_MEMTAG_ALLOCATOR( TMemTag ) allocator_type;
		using detail::allocator_wrapper_base;

		const bool canDealloc =
			is_deallocatable_allocator< allocator_type >::value;

		if( !p || !canDealloc )
		{
			return nullptr;
		}

		void *q = p;
		if( flag == dealloc_flag::aligned )
		{
			q = *reinterpret_cast< void ** >( reinterpret_cast< uptr >( q ) -
				sizeof( void * ) );
		}

		q = detail::allocator_wrapper_base::track_dealloc( q );
		allocator.dealloc( q );

		return nullptr;
	}
	template< typename TMemTag >
	inline void *tagged_aligned_dealloc(
	SDF_MEMTAG_ALLOCATOR( TMemTag ) &allocator, void *p )
	{
		return tagged_dealloc< TMemTag >( allocator, p, dealloc_flag::aligned );
	}
	template< typename TMemTag >
	inline void *tagged_maybe_aligned_dealloc(
	SDF_MEMTAG_ALLOCATOR( TMemTag ) &allocator, void *p, size_t alignment )
	{
		return
			tagged_dealloc< TMemTag >
			(
				allocator,
				p,
				alignment > 1 ? dealloc_flag::aligned : dealloc_flag::unaligned
			);
	}

#define SDF_TAGGED_ALLOC(MemTag,Allocator,Size)\
	sdf::tagged_alloc< MemTag >( Allocator, Size, 0, 0, __FILE__, __LINE__ )
#define SDF_TAGGED_ALIGNED_ALLOC(MemTag,Allocator,Size,Alignment,Offset)\
	sdf::tagged_alloc< MemTag >\
	(\
		Allocator, Size, Alignment, Offset, __FILE__, __LINE__\
	)
#define SDF_TAGGED_DEALLOC(MemTag,Allocator,Ptr)\
	sdf::tagged_dealloc< MemTag >( Allocator, Ptr )
#define SDF_TAGGED_ALIGNED_DEALLOC(MemTag,Allocator,Ptr)\
	sdf::tagged_aligned_dealloc< MemTag >( Allocator, Ptr )
#define SDF_TAGGED_MAYBE_ALIGNED_DEALLOC(MemTag,Allocator,Ptr,Alignment)\
	sdf::tagged_maybe_aligned_dealloc< MemTag >( Allocator, Ptr, Alignment )

	namespace detail
	{

		template< typename TAlloc, typename TCategory >
		class check_allocator
		{
		};
		template< typename TAlloc >
		class check_allocator< TAlloc, allocator_tag_node >
		{
		public:
			typedef TAlloc allocator_type;
			static_assert( is_node_allocator< TAlloc >::value,
				"Unsuitable allocator" );
		};
		template< typename TAlloc >
		class check_allocator< TAlloc, allocator_tag_basic >
		{
		public:
			typedef TAlloc allocator_type;
			static_assert( is_basic_allocator< TAlloc >::value,
				"Unsuitable allocator" );
		};
		template< typename TAlloc >
		class check_allocator< TAlloc, allocator_tag_resizeable >
		{
		public:
			typedef TAlloc allocator_type;
			static_assert( is_resizeable_allocator< TAlloc >::value,
				"Unsuitable allocator" );
		};

		template
		<
			typename TAlloc,
			typename TCategory,
			bool TIsStateless = false
		>
		class container_base
		: public check_allocator< TAlloc, TCategory >
		{
		public:
			typedef typename TAlloc::category allocator_category;
			static const bool stateless_allocator = false;

			TAlloc &mAllocator;

			inline container_base( TAlloc &a )
			: mAllocator( a )
			{
			}

			inline TAlloc &get_allocator() const
			{
				return mAllocator;
			}
		};

		template< typename TAlloc, typename TCategory >
		class container_base< TAlloc, TCategory, true >
		: public check_allocator< TAlloc, TCategory >
		{
		public:
			typedef typename TAlloc::category allocator_category;
			static const bool stateless_allocator = true;

			inline container_base( TAlloc & )
			{
			}

			inline TAlloc &get_allocator() const
			{
				return TAlloc::get_instance();
			}
		};

		template< typename TMemTag >
		using node_container_base =
			container_base
			<
				typename default_allocator< TMemTag >::allocator_type,
				allocator_tag_node,
				is_stateless_allocator
				<
					typename default_allocator< TMemTag >::allocator_type
				>::value
			>;
		template< typename TMemTag >
		using basic_container_base =
			container_base
			<
				typename default_allocator< TMemTag >::allocator_type,
				allocator_tag_basic,
				is_stateless_allocator
				<
					typename default_allocator< TMemTag >::allocator_type
				>::value
			>;
		template< typename TMemTag >
		using resizeable_container_base =
			container_base
			<
				typename default_allocator< TMemTag >::allocator_type,
				allocator_tag_resizeable,
				is_stateless_allocator
				<
					typename default_allocator< TMemTag >::allocator_type
				>::value
			>;

	}

	namespace detail
	{

		struct list_node_base
		{
			list_node_base *prev;
			list_node_base *next;
		};

		template< typename TElement >
		struct list_node: public list_node_base
		{
			unsigned char buffer[ sizeof( TElement ) ];

			inline const TElement *c_pointer() const
			{
				return reinterpret_cast< const TElement * >( &buffer[ 0 ] );
			}
			inline const TElement *pointer() const
			{
				return reinterpret_cast< const TElement * >( &buffer[ 0 ] );
			}
			inline TElement *pointer()
			{
				return reinterpret_cast< TElement * >( &buffer[ 0] );
			}

			inline TElement *operator->()
			{
				return pointer();
			}
			inline const TElement *operator->() const
			{
				return c_pointer();
			}

			inline TElement &operator*()
			{
				return *pointer();
			}
			inline const TElement &operator*() const
			{
				return *c_pointer();
			}
		};

		struct list_base
		{
			list_node_base *head;
			list_node_base *tail;

			inline list_base(): head( nullptr ), tail( nullptr )
			{
			}
			inline ~list_base()
			{
				unlink_all();
			}

			inline void unlink( list_node_base *node )
			{
				if( node->prev != nullptr )
				{
					node->prev->next = node->next;
				}
				else
				{
					head = node->next;
				}

				if( node->next != nullptr )
				{
					node->next->prev = node->prev;
				}
				else
				{
					tail = node->prev;
				}

				node->prev = nullptr;
				node->next = nullptr;
			}
			inline void unlink_all()
			{
				while( head != nullptr )
				{
					unlink( head );
				}
			}

			inline void link_head( list_node_base *node )
			{
				node->prev = nullptr;
				node->next = head;
				if( head != nullptr )
				{
					head->prev = node;
				}
				else
				{
					tail = node;
				}
				head = node;
			}
			inline void link_tail( list_node_base *node )
			{
				node->next = nullptr;
				node->prev = tail;
				if( tail != nullptr )
				{
					tail->next = node;
				}
				else
				{
					head = node;
				}
				tail = node;
			}
			inline void link_before( list_node_base *node,
			list_node_base *before )
			{
				if( !before )
				{
					link_head( node );
					return;
				}

				node->prev = before->prev;
				node->next = before;

				if( before->prev != nullptr )
				{
					before->prev->next = node;
				}
				else
				{
					head = node;
				}
				before->prev = node;
			}
			inline void link_after( list_node_base *node,
			list_node_base *after )
			{
				if( !after )
				{
					link_tail( node );
					return;
				}

				node->prev = after;
				node->next = after->next;

				if( after->next != nullptr )
				{
					after->next->prev = node;
				}
				else
				{
					tail = node;
				}
				after->next = node;
			}
		};
		
		template< typename TElement >
		class list_iterator
		{
		public:
			inline list_iterator(): mNode( nullptr )
			{
			}
			inline list_iterator( const list_iterator &iter )
			: mNode( iter.mNode )
			{
			}
			inline list_iterator( list_node< TElement > &node )
			: mNode( &mNode )
			{
			}
			inline list_iterator( list_node_base *node )
			: mNode( reinterpret_cast< list_node< TElement > * >( node ) )
			{
			}
			inline ~list_iterator()
			{
			}
			
			inline TElement *operator->()
			{
				return mNode->pointer();
			}
			inline const TElement *operator->() const
			{
				return mNode->c_pointer();
			}

			inline TElement &operator*()
			{
				return *mNode->pointer();
			}
			inline const TElement &operator*() const
			{
				return *mNode->c_pointer();
			}

			inline list_iterator &operator++()
			{
				mNode =
					reinterpret_cast< list_node< TElement > * >( mNode->next );
				return *this;
			}
			inline list_iterator operator++( int ) const
			{
				return list_iterator( mNode->next );
			}

			inline list_iterator &operator--()
			{
				mNode =
					reinterpret_cast< list_node< TElement > * >( mNode->prev );
				return *this;
			}
			inline list_iterator operator--( int ) const
			{
				return list_iterator( mNode->prev );
			}
			
			inline bool operator==( const list_iterator &x ) const
			{
				return mNode == x.mNode;
			}
			inline bool operator!=( const list_iterator &x ) const
			{
				return mNode != x.mNode;
			}

		private:
			list_node< TElement > *mNode;
		};

	}

	template< typename TElement, typename TMemTag = memtag_list >
	class list: public detail::node_container_base< TMemTag >
	{
	public:
		typedef detail::list_node< TElement > node_type;
		typedef TMemTag memtag;
		typedef TElement value_type;
		
		typedef detail::list_iterator< TElement > iterator;
		typedef const detail::list_iterator< TElement > const_iterator;

		static const size_t kNodeSize = sizeof( node_type );
		static const size_t kNodeAlignment = 1;
		typedef
			typename detail::node_container_base< TMemTag >::allocator_type
			allocator_type;

		using detail::node_container_base< TMemTag >::get_allocator;

		inline list( allocator_type &a = default_allocator< TMemTag >::get() )
		: detail::node_container_base< TMemTag >( a ), mBase()
		{
		}
		inline ~list()
		{
			purge();
		}

		inline void clear()
		{
			while( !empty() )
			{
				pop_front();
			}
		}
		inline void purge()
		{
			clear();
		}

		inline bool empty() const
		{
			return mBase.head == nullptr;
		}
		inline void pop_front()
		{
			delete_node( mBase.head );
		}
		inline void pop_back()
		{
			delete_node( mBase.tail );
		}

		inline TElement *push_front( const TElement &x )
		{
			auto *node = alloc_node();
			if( !node )
			{
				return nullptr;
			}

			construct_node( node, x );
			mBase.link_head( node );

			return node->pointer();
		}
		inline TElement *push_back( const TElement &x )
		{
			auto *node = alloc_node();
			if( !node )
			{
				return nullptr;
			}

			construct_node( node, x );
			mBase.link_tail( node );

			return node->pointer();
		}

		inline const TElement &c_front() const
		{
			return
				*reinterpret_cast
				<
					const node_type *
				>( mBase.head )->c_pointer();
		}
		inline const TElement &c_back() const
		{
			return
				*reinterpret_cast
				<
					const node_type *
				>( mBase.tail )->c_pointer();
		}

		inline const TElement &front() const
		{
			return c_front();
		}
		inline const TElement &back() const
		{
			return c_back();
		}

		inline TElement &front()
		{
			return const_cast< TElement & >( c_front() );
		}
		inline TElement &back()
		{
			return const_cast< TElement & >( c_back() );
		}
		
		inline iterator begin()
		{
			return iterator( mBase.head );
		}
		inline iterator end()
		{
			return iterator( nullptr );
		}

		inline const_iterator cbegin() const
		{
			return const_iterator( mBase.head );
		}
		inline const_iterator cend() const
		{
			return const_iterator( mBase.tail );
		}

		inline const_iterator begin() const
		{
			return cbegin();
		}
		inline const_iterator end() const
		{
			return cend();
		}

	protected:
		detail::list_base mBase;

		inline node_type *alloc_node()
		{
			return
				reinterpret_cast< node_type * >
				(
					SDF_TAGGED_ALIGNED_ALLOC
					(
						TMemTag, get_allocator(), kNodeSize, kNodeAlignment,
						sizeof( detail::list_node_base )
					)
				);
		}
		inline void dealloc_node( node_type *node )
		{
			SDF_TAGGED_MAYBE_ALIGNED_DEALLOC
			(
				TMemTag,
				get_allocator(),
				reinterpret_cast< void * >( node ),
				kNodeAlignment
			);
		}
		
		inline static void construct_node( node_type *node )
		{
			new( node->pointer() ) TElement();
		}
		inline static void construct_node( node_type *node, const TElement &x )
		{
			new( node->pointer() ) TElement( x );
		}
		inline static void destroy_node( node_type *node )
		{
			node->pointer()->~TElement();
		}

		inline void delete_node( detail::list_node_base *node )
		{
			auto *const p = reinterpret_cast< node_type * >( node );

			mBase.unlink( p );
			destroy_node( p );

			dealloc_node( p );
		}
	};

}

sdf::heap_allocator &sdf::heap_allocator::get_instance()
{
	static sdf::heap_allocator instance;
	return instance;
}

void *sdf::heap_allocator::alloc( size_t n )
{
	return malloc( n );
}
void sdf::heap_allocator::dealloc( void *p )
{
	free( p );
}

void *sdf::heap_allocator::realloc( void *p, size_t n )
{
	if( !n )
	{
		free( p );
		return nullptr;
	}

	if( !p )
	{
		return realloc( p, n );
	}

	return malloc( n );
}

sdf::null_allocator &sdf::null_allocator::get_instance()
{
	static sdf::null_allocator instance;
	return instance;
}

sdf::detail::memtag_db::object &sdf::detail::memtag_db::object::get_instance()
{
	static object instance;
	return instance;
}
#if SDF_ALLOC_HEADER_LIST_ENABLED
sdf::detail::allocator_wrapper_base::state &
sdf::detail::allocator_wrapper_base::state::get_instance()
{
	static state instance;
	return instance;
}
#endif

int main( int argc, char **argv )
{
	( void )argc;
	( void )argv;

	sdf::list< int > x;

	printf( "empty? %s\n", x.empty() ? "true" : "false" );
	
	x.push_front( 2 );	printf( "f:%i\n", x.front() );
	x.push_back( 3 );	printf( "b:%i\n", x.back() );
	x.push_front( 1 );	printf( "f:%i\n", x.front() );
	x.push_back( 4 );	printf( "b:%i\n", x.back() );

	printf( "empty? %s\n", x.empty() ? "true" : "false" );

	for( int &n : x )
	{
		printf( "  %i\n", n );
	}

	x.clear();

	printf( "empty? %s\n", x.empty() ? "true" : "false" );

	x.push_front( 42 );	printf( "f:%i\n", x.front() );
	x.push_back( 23 );	printf( "b:%i\n", x.back() );

	printf( "empty? %s\n", x.empty() ? "true" : "false" );

	for( int &n : x )
	{
		printf( "  %i\n", n );
	}

	int num = sdf::get_memtag_count();
	printf( "\n==== MEMTAGS( %i ) ====\n", num );
	for( int i = 0; i < num; ++i )
	{
		printf( "\n" );
		printf( "           name = \"%s\"\n", sdf::get_memtag_name( i ) );

		const sdf::alloc_stats &s = sdf::get_memtag_stats( i );
		printf( "           size = %u\n", ( unsigned int )s.size );
		printf( "     peak  size = %u\n", ( unsigned int )s.peak_size );
		printf( "   budget  size = %u\n", ( unsigned int )s.budget_size );
		printf( "    alloc bytes = %u\n", ( unsigned int )s.alloc_bytes );
		printf( "    alloc count = %u\n", ( unsigned int )s.alloc_count );
		printf( "  dealloc count = %u\n", ( unsigned int )s.dealloc_count );
		printf( "    alloc  time = %u\n", ( unsigned int )s.alloc_time );
		printf( "  dealloc  time = %u\n", ( unsigned int )s.dealloc_time );
	}

	return EXIT_SUCCESS;
}
