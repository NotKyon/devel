#include <meshio/config.h>
#include <meshio/core.h>
#include <meshio/texture.h>

static size_t g_tex_count          = 0;
static meshio_texture_t g_tex_head = ( meshio_texture_t )0,
                        g_tex_tail = ( meshio_texture_t )0;

meshio_texture_t meshio_alloc_texture()
{
	meshio_texture_t tex;

	if( !( tex = ( meshio_texture_t )meshio_memory( 0, sizeof( *meshio_texture_t ) ) ) )
		return ( meshio_texture_t )0;

	tex->refcnt = 1;

	tex->name.n = 0;
	tex->name.p = ( char * )0;

	tex->flags = 0x00000000;

	tex->coord_count      = 2;
	tex->translation[ 0 ] = tex->translation[ 1 ] = tex->translation[ 2 ] = 0;
	tex->scale[ 0 ] = tex->scale[ 1 ] = tex->scale[ 2 ] = 1;
	tex->rotation = 0;

	tex->next = ( meshio_texture_t )0;
	if( ( tex->prev = g_tex_tail ) != ( meshio_texture_t )0 )
		g_tex_tail->next = tex;
	else
		g_tex_head = tex;
	g_tex_tail     = tex;

	g_tex_count++;
	return tex;
}
meshio_texture_t meshio_dealloc_texture( meshio_texture_t tex )
{
	if( tex->name.p )
		free( ( void * )tex->name.p );

	if( tex->prev )
		tex->prev->next = tex->next;
	if( tex->next )
		tex->next->prev = tex->prev;

	if( g_tex_head == tex )
		g_tex_head = tex->next;
	if( g_tex_tail == tex )
		g_tex_tail = tex->prev;

	g_tex_count--;
	return ( meshio_texture_t )meshio_memory( ( void * )tex, 0 );
}
void meshio_retain_texture( meshio_texture_t tex )
{
	tex->refcnt++;
}
meshio_texture_t meshio_release_texture( meshio_texture_t tex )
{
	return --tex->refcnt == 0 ? meshio_dealloc_texture( tex ) : ( meshio_texture_t )0;
}

size_t meshio_texture_count()
{
	return g_tex_count;
}
meshio_texture_t meshio_first_texture()
{
	return g_tex_head;
}
meshio_texture_t meshio_last_texture()
{
	return g_tex_tail;
}
meshio_texture_t meshio_texture_before( meshio_texture_t tex )
{
	return tex->prev;
}
meshio_texture_t meshio_texture_after( meshio_texture_t tex )
{
	return tex->next;
}

size_t meshio_texture_name_length( meshio_texture_t tex )
{
	return tex->name.n;
}
const char *meshio_texture_name( meshio_texture_t tex )
{
	return tex->name.p ? tex->name.p : "";
}

void meshio_set_texture_name( meshio_texture_t tex, const char *name )
{
	size_t i, n;

	n           = name ? strlen( name ) : 0;
	tex->name.p = ( char * )meshio_memory( ( void * )tex->name.p, n + 1 );

	for( i = 0; i < n; i++ )
		tex->name.p[ i ] = name[ i ];
	tex->name.p[ n ]     = 0;
	tex->name.n          = n;
}

bitfield_t meshio_texture_flags( meshio_texture_t tex )
{
	return tex->flags;
}
void meshio_set_texture_flags( meshio_texture_t tex, bitfield_t flags )
{
	tex->flags = flags;
}

unsigned int meshio_texture_coord_count( meshio_texture_t tex )
{
	return tex->coord_count;
}
float *meshio_texture_translation( meshio_texture_t tex )
{
	return &tex->translation[ 0 ];
}
float *meshio_texture_scale( meshio_texture_t tex )
{
	return &tex->scale[ 0 ];
}
float meshio_texture_rotation( meshio_texture_t tex )
{
	return tex->rotation;
}

meshio_texture_t meshio_find_texture( const char *name )
{
	meshio_texture_t tex;

	for( tex = g_tex_head; tex; tex = tex->next ) {
		if( !strcmp( tex->name, name ) )
			return tex;
	}

	return ( meshio_texture_t )0;
}

meshio_texture_t meshio_add_texture( const char *name )
{
	meshio_texture_t tex;

	if( ( tex = meshio_find_texture( name ) ) != ( meshio_texture_t )0 ) {
		meshio_retain_texture( tex );
		return ( meshio_texture_t )0;
	}

	if( !( tex = meshio_alloc_texture() ) )
		return ( meshio_texture_t )0;

	meshio_set_texture_name( tex, name );
	return tex;
}
