bits 16

;IDEA: Linked list filesystem; files take up space sequentially in memory
;and list their sizes. New files are placed directly after the previous file, at
;the closest base 2 boundary and previous files are checked for extra space, and
;if found will be placed in that free space and inserted between previous and
;next file. Files can be resized, however will be moved if they exceed free
;space in their area. Programs can request a spot of a certain size. No
;fragmentation at all! Maybe write to a fast cache area on the drive and then
;move when needed?

;Setup stack and data segment
mov ax, 0x07C0 ;We're loaded to this location in memory
mov ds, ax ;Set data segment
add ax, (4096 + 512) / 16 ;Stack start; stack expands downward
mov ss, ax
mov sp, 4096 ;Stack pointer at beginning of stack; end of stack's avail. memory

mov ah, 0x0E
mov al, 1
int 0x10

cli
hlt

times 510 - ($-$$) db 0
db 0x55, 0xAA

times 2880 * 512 - ($-$$) db 0 ;2880 sectors @ 512 bytes each = 1440Kb, 1.44Mb
