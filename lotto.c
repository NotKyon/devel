#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

enum {
	kMaxWhite=56,
	kMaxBlack=46
};

struct lottery_s {
	int num[5]; int meg;
} g_numbers[] = {
	{{3 ,16,23,35,36},20},
	{{28,34,39,45,53},34},
	{{3 ,4 ,24,36,52},45},
	{{20,23,28,35,39},21},
	{{3 ,11,19,23,36},21},
	{{6 ,7 ,13,24,46},34},
	{{1 ,13,21,49,55},17},
	{{2 ,44,48,50,52},3 },
	{{5 ,9 ,38,46,51},5 },
	{{2 ,3 ,4 ,8 ,43},26},
	{{5 ,18,21,29,41},37},
	{{2 ,10,13,38,46},2 },
	{{30,32,33,42,48},7 },
	{{10,18,36,38,44},8 },
	{{15,23,34,39,55},32},
	{{8 ,20,24,35,56},24},
	{{5 ,13,20,23,33},30},
	{{25,34,45,46,49},34},
	{{4 ,9 ,40,45,50},39},
	{{31,40,41,47,48},45},
	{{16,32,39,41,53},16},
	{{15,32,39,41,53},16},
	{{5 ,11,20,33,36},11},
	{{16,17,21,40,51},20},
	{{5 ,9 ,22,36,49},36}
};

struct slot_s {
	int *num_avg;
	int num[kMaxWhite];
	int num_gt_10, num_gt_20, num_gt_30, num_gt_40, num_gt_50;
} g_norm[5], g_mega;
int g_norm_avg[kMaxWhite], g_mega_avg[kMaxBlack];

/*
struct prize_s {
	int norm_match;
	int mega_match;
	int prize;
} g_prizes[] = {
	{ 0,1, 2        },
	{ 1,1, 3        },
	{ 3,0, 7        },
	{ 2,1, 10       },
	{ 3,1, 150      },
	{ 4,0, 150      },
	{ 4,1, 10000    },
	{ 5,0, 250000   },
	{ 5,1, 12000000 }
};

struct prize_s *match_prize(int norm_match, int mega_match) {
	struct prize_s *curr;
	size_t i, n;

	curr = (struct prize_s *)NULL;

	n = sizeof(g_prizes)/sizeof(g_prizes[0]);
	for(i=0; i<n; i++) {
		if(norm_match>=g_prizes[i].norm_match
		&& mega_match>=g_prizes[i].mega_match)
			curr = &g_prizes[i];
	}

	return curr;
}
*/

void process_slot(struct slot_s *d, const int n) {
	d->num[n]++;

	d->num_avg[n]++;

	if (n > 50)
		d->num_gt_50++;
	else if(n > 40)
		d->num_gt_40++;
	else if(n > 30)
		d->num_gt_30++;
	else if(n > 20)
		d->num_gt_20++;
	else if(n > 10)
		d->num_gt_10++;
}
void process_lotto(const struct lottery_s *s) {
	size_t i;

	for(i=0; i<5; i++)
		process_slot(&g_norm[i], s->num[i]);

	process_slot(&g_mega, s->meg);
}

void print_average() {
	
}

int main() {
	size_t i, n;

	memset(g_norm, 0, sizeof(g_norm));
	memset(&g_mega, 0, sizeof(g_mega));

	for(i=0; i<5; i++)
		g_norm[i].num_avg = &g_norm_avg[0];
	g_mega[i].num_avg = &g_mega_avg[0];

	n = sizeof(g_numbers)/sizeof(g_numbers[0]);
	for(i=0; i<n; i++)
		process_lotto(&g_numbers[i]);

	return EXIT_SUCCESS;
}
