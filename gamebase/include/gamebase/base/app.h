#ifndef GB_BASE_APP_H
#define GB_BASE_APP_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Low-Level Application Management Routines */
enum {
	kGBOptF_Switch_Bit = (1<<0),
	kGBOptF_RunOnce_Bit = (1<<1),
	kGBOptF_Disabled_Bit = (1<<2),
	kGBOptF_Param_Bit = (1<<3),
	kGBOptF_Optional_Bit = (1<<4)
};
typedef void(*gb_optfn_t)(const char *parm);
typedef struct gb_opt_s {
	const char *name;	/* name of argument (e.g., "include-dir") */
	int flags;			/* details of how to process this argument */
	gb_optfn_t fn;		/* callback function */
	int *bitfield;		/* pointer to bitfield to apply operation to */
	int bitmask;		/* mask of bits to apply operation with */
} gb_opt_t;
#define GB_OPT_SWITCH(n, bf,bm)\
	{ (n), kGBOptF_Switch_Bit, (gb_optfn_t)NULL, (bf), (bm) }
#define GB_OPT_MP(n, fn)\
	{ (n), 0, (fn), (int *)NULL, 0 }
#define GB_OPT_SP(n, fn)\
	{ (n), kGBOptF_RunOnce_Bit, (fn), (int *)NULL, 0 }
#define GB_OPT_MP_PARM(n, fn)\
	{ (n), kGBOptF_Param_Bit, (fn), (int *)NULL, 0 }
#define GB_OPT_SP_PARM(n, fn)\
	{ (n), kGBOptF_Param_Bit|kGBOptF_RunOnce_Bit, (fn), (int *)NULL, 0 }
#define GB_OPT_MP_PARM_LOOSE(n, fn)\
	{ (n), kGBOptF_Param_Bit|kGBOptF_Optional_Bit, (fn), (int *)NULL, 0 }
#define GB_OPT_SP_PARM_LOOSE(n, fn)\
	{ (n), kGBOptF_Param_Bit|kGBOptF_RunOnce_Bit|kGBOptF_Optional_Bit, (fn),\
	  (int *)NULL, 0 }
#define GB_OPT_SWITCH_PARM_LOOSE(n, fn, bf,bm)\
	{ (n), kGBOptF_Switch_Bit|kGBOptF_Param_Bit|\
	       kGBOptF_RunOnce_Bit|kGBOptF_Optional_Bit, (fn), (bf), (bm) }

void gbSetOptTable(gb_opt_t *p, size_t n);
void gbSetOptAlias(const char *alias[256]);

void gbAppInit(int argc, char **argv);
void gbAppFini();

void gbPushArgFile(const char *f);
size_t gbCountArgFiles();
const char *gbGetArgFile(size_t i);

void gbProcessArgs();

/* Basic Application Information */
const char *gbAppDir();
const char *gbAppFile();

const char *gbAppTitle();
void gbSetAppTitle(const char *title);

size_t gbCountAppArgs();
const char *gbAppArg(size_t i);

const char *gbLaunchDir();

	/* internal */
	void gbSetAppDir(const char *appdir);
	void gbSetAppFile(const char *appfile);
	void gbSetLaunchDir(const char *launchdir);

	void gbResetAppArgs();
	void gbAddAppArgs(int argc, char **argv);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
