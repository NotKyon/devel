#ifndef VEC3_H
#define VEC3_H

#include <cmath>
#include <cassert>
#include <cstddef>

typedef float vec_t;

template<typename T=vec_t>
struct vec3 {
	union { T x, r, s; };
	union { T y, g, t; };
	union { T z, b, p; };
	//union { T w, a, q; };

	inline vec3(T x=0, T y=0, T z=0): x(x), y(y), z(z) {}
	inline vec3(const vec3<T> &v): x(v.x), y(v.y), z(v.z) {}
	inline ~vec3() {}

	inline void zero() { x=0; y=0; z=0; }

	inline T length() const {
		return dot(*this, *this);
	}
	inline T magnitude() const {
		return sqrt(length());
	}

	inline void normalize() {
		float rcpmag;

		rcpmag = 1.0f/magnitude();

		x *= rcpmag;
		y *= rcpmag;
		z *= rcpmag;
	}

	inline vec3<T> operator+(const vec3<T> &v) const {
		return vec3<T>(x + v.x, y + v.y, z + v.z);
	}
	inline vec3<T> operator-(const vec3<T> &v) const {
		return vec3<T>(x - v.x, y - v.y, z - v.z);
	}
	inline vec3<T> operator*(const T &f) const {
		return vec3<T>(x*f, y*f, z*f);
	}
	inline vec3<T> operator/(const T &f) const {
		T rcpf = 1.0f/f;

		return vec3<T>(x*rcpf, y*rcpf, z*rcpf);
	}

	inline vec3<T> &operator=(const T &f) {
		x = f;
		y = f;
		z = f;

		return *this;
	}
	inline vec3<T> &operator=(const vec3<T> &v) {
		x = v.x;
		y = v.y;
		z = v.z;

		return *this;
	}

	inline vec3<T> &operator+=(const vec3<T> &v) {
		x += v.x;
		y += v.y;
		z += v.z;

		return *this;
	}
	inline vec3<T> &operator-=(const vec3<T> &v) {
		x -= v.x;
		y -= v.y;
		z -= v.z;

		return *this;
	}
	inline vec3<T> &operator*=(const T &f) {
		x *= f;
		y *= f;
		z *= f;

		return *this;
	}
	inline vec3<T> &operator/=(const T &f) {
		T rcpf = 1.0f/f;

		x *= rcpf;
		y *= rcpf;
		z *= rcpf;

		return *this;
	}

	inline T &operator[](size_t i) { assert(i < 3); return (&x)[i]; }
};
typedef vec3<float> fvec3;
typedef vec3<double> dvec3;

template<typename T>
inline vec3<T> cross(const vec3<T> &a, const vec3<T> &b) {
	return vec3<T>(a.y*b.z - a.z*b.y,
	               a.z*b.x - a.x*b.z,
	               a.x*b.y - a.y*b.x);
}

template<typename T>
inline T dot(const vec3<T> &a, const vec3<T> &b) {
	return a.x*b.x + a.y*b.y + a.z*b.z;
}

template<typename T>
inline vec3<T> normal(const vec3<T> &v) {
	vec3<T> r = v;

	r.normalize();
	return r;
}

template<typename T>
inline T distance(const vec3<T> &a, const vec3<T> &b) {
	return (a - b).magnitude();
}

#endif
