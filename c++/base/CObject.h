#ifndef COBJECT_H
#define COBJECT_H

#include "IObject.h"

class CObject: public IObject {
public:
	CObject();
	virtual ~CObject();

	virtual void DoSomething();
};

#endif
