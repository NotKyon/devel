#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "mk_error.h"

void ErrorMessage(const char *message) {
	Error(0, 0, 0, message);
}
void ErrorExit(const char *message) {
	ErrorMessage(message);
	exit(EXIT_FAILURE);
}
void Error(const char *file, unsigned int line, const char *func,
const char *message) {
	int e;

	e = errno;

	fflush(stdout);

	if (file) {
		fprintf(stderr, "[%s", file);
		if (line)
			fprintf(stderr, "(%u)", line);
		if (func)
			fprintf(stderr, " %s", func);
		fprintf(stderr, "] ");
	} else if(func)
		fprintf(stderr, "%s: ", func);

	if (message)
		fprintf(stderr, "ERROR: %s", message);

	if (e) {
		if (message)
			fprintf(stderr, ": ");

		fprintf(stderr, "%s (%i)", strerror(e), e);
	}

	fprintf(stderr, "\n");
	fflush(stderr);
}
