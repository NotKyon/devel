﻿#define THREADAPI_WINDOWS 1
#define THREADAPI_PTHREAD 2

#ifndef THREADAPI
# if defined( _WIN32 )
#  define THREADAPI THREADAPI_WINDOWS
# elif defined( __APPLE__ )
#  define THREADAPI THREADAPI_PTHREAD
# elif defined( __linux__ )
#  define THREADAPI THREADAPI_PTHREAD
# elif defined( __BSD__ ) || defined( __FreeBSD__ )
#  define THREADAPI THREADAPI_PTHREAD
# else
#  error Unhandled platform for THREADAPI
# endif
#endif

#if THREADAPI == THREADAPI_WINDOWS
# undef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN 1
# include <Windows.h>
#elif THREADAPI == THREADAPI_PTHREAD
# include <pthread.h>
#else
# error Unhandled THREADAPI
#endif

#ifndef SECURE_LIB_ENABLED
# if defined( _WIN32 ) && defined( _MSC_VER )
#  define SECURE_LIB_ENABLED 1
# else
#  define SECURE_LIB_ENABLED 0
# endif
#endif

#ifndef DEBUG_ENABLED
# if defined( _DEBUG ) || defined( DEBUG ) || defined( __debug__ )
#  define DEBUG_ENABLED 1
# else
#  define DEBUG_ENABLED 0
# endif
#endif

#ifndef IF_LIKELY
# undef IF_UNLIKELY
# if defined( __GNUC__ ) || defined( __clang__ )
#  define IF_LIKELY( expr ) if( __builtin_expect( !!( expr ), 1 ) )
#  define IF_UNLIKELY( expr ) if( __builtin_expect( !!( expr ), 0 ) )
# else
#  define IF_LIKELY( expr ) if( expr )
#  define IF_UNLIKELY( expr ) if( expr )
# endif
#endif

#include "rawkjob.h"

/*
===============================================================================

	PLATFORM THREADING

===============================================================================
*/

/*
----------------
	Atomics
----------------
*/
#if defined( _MSC_VER )
# include <intrin.h>

# pragma intrinsic( _InterlockedIncrement )
# pragma intrinsic( _InterlockedDecrement )
# pragma intrinsic( _InterlockedExchange )
# pragma intrinsic( _InterlockedCompareExchange )
# pragma intrinsic( _InterlockedAnd )
# pragma intrinsic( _InterlockedOr )
# pragma intrinsic( _InterlockedXor )
# pragma intrinsic( _InterlockedExchangeAdd )

# if defined( _WIN64 )
#  pragma intrinsic( _InterlockedIncrement64 )
#  pragma intrinsic( _InterlockedDecrement64 )
#  pragma intrinsic( _InterlockedExchange64 )
#  pragma intrinsic( _InterlockedCompareExchange64 )
#  pragma intrinsic( _InterlockedAnd64 )
#  pragma intrinsic( _InterlockedOr64 )
#  pragma intrinsic( _InterlockedXor64 )
#  pragma intrinsic( _InterlockedExchangeAdd64 )
# else
#  define _InterlockedIncrement64 InterlockedIncrement64
#  define _InterlockedDecrement64 InterlockedDecrement64
#  define _InterlockedExchange64 InterlockedExchange64
#  define _InterlockedCompareExchange64 InterlockedCompareExchange64
#  define _InterlockedAnd64 InterlockedAnd64
#  define _InterlockedOr64 InterlockedOr64
#  define _InterlockedXor64 InterlockedXor64
#  define _InterlockedExchangeAdd64 InterlockedExchangeAdd64
# endif

# define _InterlockedExchangePointer InterlockedExchangePointer
# define _InterlockedCompareExchangePointer InterlockedCompareExchangePointer
#endif

static rjob_uint_t atomic_inc( volatile rjob_uint_t *dst ) {
#if defined( _MSC_VER )
	return _InterlockedIncrement( ( volatile LONG * )dst );
#else
	return __sync_fetch_and_add( dst, 1 );
#endif
}
static rjob_uint_t atomic_dec( volatile rjob_uint_t *dst ) {
#if defined( _MSC_VER )
	return _InterlockedDecrement( ( volatile LONG * )dst );
#else
	return __sync_fetch_and_sub( dst, 1 );
#endif
}
static rjob_uint_t atomic_add( volatile rjob_uint_t *dst, rjob_uint_t src ) {
#if defined( _MSC_VER )
	return _InterlockedExchangeAdd( ( volatile LONG * )dst, src );
#else
	return __sync_fetch_and_add( dst, src );
#endif
}
static rjob_uint_t atomic_sub( volatile rjob_uint_t *dst, rjob_uint_t src ) {
#if defined( _MSC_VER )
	return _InterlockedExchangeAdd( ( volatile LONG * )dst, -( LONG )src );
#else
	return __sync_fetch_and_sub( dst, src );
#endif
}
static rjob_uint_t atomic_set( volatile rjob_uint_t *dst, rjob_uint_t src ) {
#if defined( _MSC_VER )
	return _InterlockedExchange( ( volatile LONG * )dst, src );
#else
	return __sync_lock_test_and_set( dst, src );
#endif
}
static rjob_uint_t atomic_setEq( volatile rjob_uint_t *dst, rjob_uint_t src,
rjob_uint_t cmp ) {
#if defined( _MSC_VER )
	return _InterlockedCompareExchange( ( volatile LONG * )dst, src, cmp );
#else
	return __sync_val_compare_and_swap( dst, cmp, src );
#endif
}
static rjob_uint_t atomic_and( volatile rjob_uint_t *dst, rjob_uint_t src ) {
#if defined( _MSC_VER )
	return _InterlockedAdd( ( volatile LONG * )dst, src );
#else
	return __sync_fetch_and_and( dst, src );
#endif
}
static rjob_uint_t atomic_or( volatile rjob_uint_t *dst, rjob_uint_t src ) {
#if defined( _MSC_VER )
	return _InterlockedOr( ( volatile LONG * )dst, src );
#else
	return __sync_fetch_and_or( dst, src );
#endif
}
static rjob_uint_t atomic_xor( volatile rjob_uint_t *dst, rjob_uint_t src ) {
#if defined( _MSC_VER )
	return _InterlockedXor( ( volatile LONG * )dst, src );
#else
	return __sync_fetch_and_xor( dst, src );
#endif
}

/*
----------------
	Mutex
----------------
*/
#if THREADAPI == THREADAPI_WINDOWS
typedef CRITICAL_SECTION mutex_t;
#elif THREADAPI == THREADAPI_PTHREAD
typedef pthread_mutex_t mutex_t;
#else
# error THREADAPI unhandled for mutex_t
#endif

static void mutex_init( mutex_t *mutex ) {
#if THREADAPI == THREADAPI_WINDOWS
	InitializeCriticalSectionAndSpinCount( mutex, 4096 );
#elif THREADAPI == THREADAPI_PTHREAD
	static pthread_mutexattr_t attr;
	static int didInit = 0;

	IF_UNLIKELY( !didInit ) {
		pthread_mutexattr_init( &attr );
		pthread_mutexattr_settype( &attr, PTHREAD_MUTEX_RECURSIVE );

		didInit = 1;
	}

	( void )pthread_mutex_init( mutex, &attr );
#else
# error THREADAPI unhandled for mutex_init
#endif
}
static void mutex_fini( mutex_t *mutex ) {
#if THREADAPI == THREADAPI_WINDOWS
	DeleteCriticalSection( mutex );
#elif THREADAPI == THREADAPI_PTHREAD
	pthread_mutex_destroy( mutex );
#else
# error THREADAPI unhandled for mutex_fini
#endif
}

static void mutex_lock( mutex_t *mutex ) {
#if THREADAPI == THREADAPI_WINDOWS
	EnterCriticalSection( mutex );
#elif THREADAPI == THREADAPI_PTHREAD
	pthread_mutex_lock( mutex );
#else
# error THREADAPI unhandled for mutex_lock
#endif
}
static int mutex_tryLock( mutex_t *mutex ) {
#if THREADAPI == THREADAPI_WINDOWS
	return ( int )( TryEnterCriticalSection( mutex ) != 0 );
#elif THREADAPI == THREADAPI_PTHREAD
	return ( int )( pthread_mutex_trylock( mutex ) == 0 );
#else
# error THREADAPI unhandled for mutex_tryLock
#endif
}
static void mutex_unlock( mutex_t *mutex ) {
#if THREADAPI == THREADAPI_WINDOWS
	LeaveCriticalSection( mutex );
#elif THREADAPI == THREADAPI_PTHREAD
	pthread_mutex_unlock( mutex );
#else
# error THREADAPI unhandled for mutex_unlock
#endif
}

/*
----------------
	R/W Lock
----------------
*/
static const rjob_uint_t kRWLock_MaxReaders = 64;
static const rjob_uint_t kRWLock_SpinCount = 2048;
typedef struct rwlock_s rwlock_t;

struct rwlock_s {
	mutex_t writerMutex;
	rjob_uint_t readerCount;
};

static void rwlock_init( rwlock_t *rwlock ) {
	mutex_init( &rwlock->writerMutex );
	atomic_set( &rwlock->readerCount, 0 );
}
static void rwlock_fini( rwlock_t *rwlock ) {
	mutex_fini( &rwlock->writerMutex );
}

static void rwlock_lockReader( rwlock_t *rwlock ) {
	rjob_uint_t curSpins = 0;
	rjob_uint_t didLock = 0;

	while( !didLock ) {
		/* try to gain access */
		if( atomic_inc( &rwlock->readerCount ) == kRWLock_MaxReaders ) {
			atomic_dec( &rwlock->readerCount );

			if( curSpins == kRWLock_SpinCount ) {
				thread_yield();
			} else {
				++curSpins;
			}

			continue;
		}

		/* access gained */
		didLock = 1;
	}
}
static void rwlock_unlockReader( rwlock_t *rwlock ) {
	atomic_dec( &rwlock->readerCount );
}

static void rwlock_lockWriter( rwlock_t *rwlock ) {
	rjob_uint_t i = 0;

	/* only one writer can acquire access at a time; avoids deadlock */
	mutex_lock( &rwlock->writerMutex );
	for( i = 0; i < kRWLock_MaxReaders; ++i ) {
		rwlock_lockReader( rwlock );
	}
	mutex_unlock( &rwlock->writerMutex );
}
static void rwlock_unlockWriter( rwlock_t *rwlock ) {
	atomic_set( &rwlock->readerCount, 0 );
}

/*
----------------
	Signal
----------------
*/
typedef struct signal_s signal_t;

struct signal_s {
#if THREADAPI == THREADAPI_WINDOWS
	HANDLE _event;
#elif THREADAPI == THREADAPI_PTHREAD
	pthread_mutex_t _mutex;
	pthread_cond_t _cond;
#else
# error THREADAPI unhandled for signal_t
#endif
};

static void signal_init( signal_t *sig ) {
#if THREADAPI == THREADAPI_WINDOWS
	sig->_event = CreateEvent( 0, TRUE, FALSE, 0 );
#elif THREADAPI == THREADAPI_PTHREAD
	( void )pthread_mutex_init( &sig->_mutex, NULL );
	( void )pthread_cond_init( &sig->_cond, NULL );
#else
# error THREADAPI unhandled for signal_init
#endif
}
static void signal_fini( signal_t *sig ) {
#if THREADAPI == THREADAPI_WINDOWS
	CloseHandle( sig->_event );
	sig->_event = NUll;
#elif THREADAPI == THREADAPI_PTHREAD
	pthread_cond_destroy( &sig->_cond );
	pthread_mutex_destroy( &sig->_mutex );
#else
# error THREADAPI unhandled for signal_fini
#endif
}

static void signal_trigger( signal_t *sig ) {
#if THREADAPI == THREADAPI_WINDOWS
	SetEvent( sig->_event );
#elif THREADAPI == THREADAPI_PTHREAD
	pthread_cond_signal( &sig->_cond );
#else
# error THREADAPI unhandled for signal_trigger
#endif
}
static void signal_reset( signal_t *sig ) {
#if THREADAPI == THREADAPI_WINDOWS
	ResetEvent( sig->_event );
#elif THREADAPI == THREADAPI_PTHREAD
# error TODO: Implement this
#else
# error THREADAPI unhandled for signal_reset
#endif
}
static void signal_wait( signal_t *sig ) {
#if THREADAPI == THREADAPI_WINDOWS
	( void )WaitForSingleObject( sig->_event, INFINITE );
#elif THREADAPI == THREADAPI_PTHREAD
	pthread_mutex_lock( &sig->_mutex );
	pthread_cond_wait( &sig->_cond, &sig->_mutex );
	pthread_mutex_unlock( &sig->_mutex );
#else
# error THREADAPI unhandled for signal_wait
#endif
}
static int signal_timedWait( signal_t *sig, rjob_uint_t milliseconds ) {
#if THREADAPI == THREADAPI_WINDOWS
	return ( int )( WaitForSingleObject( sig->_event, milliseconds ) !=
		WAIT_OBJECT_0 );
#elif THREADAPI == THREADAPI_PTHREAD
	struct timespec ts;
	int r;

	ts.tv_sec = milliseconds/1000;
	ts.tv_nsec = ( milliseconds%1000 )*1000000;

	r = 0;

	pthread_mutex_lock( &sig->_mutex );
	r = (( int ) pthread_cond_timedwait( &sig->_cond, &sig->_mutex, &ts ) ==
		0 );
	pthread_mutex_unlock( &sig->_mutex );

	return r;
#else
# error THREADAPI unhandled for signal_timedWait
#endif
}
static int signal_tryWait( signal_t *sig ) {
	return signal_timedWait( sig, 0 );
}

/*
----------------
	Barrier
----------------
*/
typedef struct barrier_s barrier_t;

struct barrier_s {
	rjob_uint_t cur;
	rjob_uint_t exp;
	signal_t sig;
};

static void barrier_init( barrier_t *barrier, rjob_uint_t exp ) {
	barrier->cur = 0;
	barrier->exp = exp;
	signal_init( &barrier->sig );
}
static void barrier_fini( barrier_t *barrier ) {
	signal_fini( &barrier->sig );
}

static void barrier_raise( barrier_t *barrier ) {
	if( atomic_inc( &barrier->cur ) >= barrier->exp - 1 ) {
		signal_trigger( &barrier->sig );
	}
}
static void barrier_lower( barrier_t *barrier ) {
	if( atomic_dec( &barrier->cur ) < barrier->exp ) {
		signal_reset( &barrier->sig );
	}
}

static void barrier_clear( barrier_t *barrier ) {
	atomic_set( &barrier->cur, 0 );
	signal_reset( &barrier->sig );
}
static void barrier_reset( barrier_t *barrier, rjob_uint_t exp ) {
	atomic_set( &barrier->exp, exp );
	atomic_set( &barrier->cur, 0 );
	signal_reset( &barrier->sig );
}

static void barrier_increaseTarget( barrier_t *barrier ) {
	( void )atomic_inc( &barrier->exp );
	if( barrier->cur < barrier->exp ) {
		signal_reset( &barrier->sig );
	}
}

static void barrier_wait( barrier_t *barrier ) {
	signal_wait( &barrier->sig );
}
static int barrier_timedWait( barrier_t *barrier, rjob_uint_t milliseconds ) {
	return signal_timedWait( &barrier->sig, milliseconds );
}
static int barrier_tryWait( barrier_t *barrier ) {
	return signal_tryWait( &barrier->sig );
}

/*
===============================================================================

	BUILT-IN JOBS

===============================================================================
*/
static void RJOB_CALL __job_signal_f( void *barrierPointer ) {
	barrier_t *barrier = ( barrier_t * )barrierPointer;

	( void )barrier;
}
static void RJOB_CALL __job_sync_f( void *barrierPointer ) {
	barrier_t *barrier = ( barrier_t * )barrierPointer;

	barrier_wait( barrier );
}

/*
===============================================================================

	BLOCKS

===============================================================================
*/
static const rjob_uint_t kBlockJobGranularity = 16;

void rjob_block_init( rjob_block_t *block ) {
	IF_UNLIKELY( !block ) {
		return;
	}

	block->alloc = &allocator_alloc_f;
	block->dealloc = &allocator_dealloc_f;

	block->jobIndex = 0;
	block->jobCount = 0;
	block->jobCapacity = 0;
	block->jobs = NULL;

	block->parallelism = kRJob_Parallelism_Default;
	block->rt_currentParallelism = 0;
	block->rt_affinity = 0;

	block->ct_lastSignal = 0;
	block->ct_lastSync = 0;

	block->rt_status = kRJob_RunStatus_Creating;
	block->rt_workTime = 0;
	block->rt_stallTime = 0;
	block->rt_peakTime = 0;

	block->rt_stallCount = 0;

	block->rt_barrierCount = 0;
	block->rt_barriers = NULL;

	block->queue = NULL;
}
void rjob_block_fini( rjob_block_t *block ) {
	IF_UNLIKELY( !block || !block->dealloc ) {
		return;
	}

	if( block->queue != NULL ) {
		rjob_block_abort( block );
	}

	if( block->jobs != NULL ) {
		block->dealloc( ( void * )block->jobs );
		block->jobs = NULL;
	}

	if( block->rt_barriers != NULL ) {
		barrier_t *barriers = ( barrier_t * )block->rt_barriers;
		unsigned short i;

		for( i = 0; i < block->rt_barrierCount; ++i ) {
			barrier_fini( &barriers[ i ] );
		}

		block->dealloc( ( void * )block->rt_barriers );
		block->rt_barriers = NULL;
	}
}

rjob_error_t rjob_block_setAllocator( rjob_block_t *block, rjob_alloc_t alloc,
rjob_dealloc_t dealloc ) {
	IF_UNLIKELY( block == NULL || alloc == NULL || dealloc == NULL ) {
		return kRJob_Error_InvalidArgument;
	}

	IF_UNLIKELY( block->queue != NULL || block->jobs != NULL ||
	block->rt_barriers != NULL ) {
		return kRJob_Error_InvalidOperation;
	}

	block->alloc = alloc;
	block->dealloc = dealloc;

	return kRJob_Success;
}
void rjob_block_getAllocator( const rjob_block_t *block, rjob_alloc_t *alloc,
rjob_dealloc_t *dealloc ) {
	IF_UNLIKELY( !block ) {
		return;
	}

	if( alloc != NULL ) {
		*alloc = block->alloc;
	}
	if( dealloc != NULL ) {
		*dealloc = block->dealloc;
	}
}

rjob_error_t rjob_block_addJob( rjob_block_t *block, rjob_run_t run,
void *cookie ) {
	rjob_uint_t n;
	void *p;

	IF_UNLIKELY( !block || !run ) {
		return kRJob_Error_InvalidArgument;
	}

	IF_UNLIKELY( rjob_block_isSubmitted( block ) ) {
		/* cannot add jobs to block object while being executed */
		return kRJob_Error_InvalidOperation;
	}

	if( block->jobCount == block->jobCapacity ) {
		n = block->jobCapacity + kBlockJobGranularity;
		p = block->alloc( n*sizeof( rjob_item_t ) );
		if( !p ) {
			return kRJob_Error_OutOfMemory;
		}

		memcpy( p, ( const void * )block->jobs,
			block->jobCapacity*sizeof( rjob_item_t ) );

		block->dealloc( ( void * )block->jobs );
		block->jobs = ( rjob_item_t * )p;
		block->jobCapacity = n;
	}

	block->jobs[ block->jobCount ].run = run;
	block->jobs[ block->jobCount ].cookie = cookie;

	/*
	 * rt_barrierCount is used here because it will be the index of the next
	 * barrier in the block. There are initially no barriers, but there needs
	 * to be one at the very end of the block for synchronization purposes. So
	 * a barrier will be created automatically at the end (if one is not there
	 * already).
	 *
	 * The initial state of a block is the absence of any jobs or barriers.
	 * When the first job is added, it will use the barrier count, which will
	 * be zero. When the first barrier is finally created, it will have the
	 * index value of zero as well.
	 */
	block->jobs[ block->jobCount ].barrierIndex = block->rt_barrierCount;
}
rjob_error_t rjob_block_addSignal( rjob_block_t *block ) {
	rjob_error_t r;

	IF_UNLIKELY( !block ) {
		return kRJob_Error_InvalidArgument;
	}
	/*
	 * If the last sync index is less than the last signal index then there is
	 * a currently outstanding signal (because a sync would occur after the
	 * signal if that weren't the case).
	 */
	IF_UNLIKELY( block->ct_lastSync < block->ct_lastSignal ) {
		/* each signal needs a sync; a sync needs to be placed */
		return kRJob_Error_InvalidOperation;
	}

	r = rjob_block_addJob( block, &job__signal_f, ( void * )block );
	if( r != kRJob_Success ) {
		return r;
	}

	block->ct_lastSignal = block->jobCount - 1;
	return kRJob_Success;
}
rjob_error_t rjob_block_addSync( rjob_block_t *block ) {
	rjob_error_t r;

	IF_UNLIKELY( !block ) {
		return kRJob_Error_InvalidArgument;
	}
	/*
	 * If the last signal index is less than the last sync index then the last
	 * thing we have in the list is a sync, not a signal. We need to use the
	 * last signal.
	 */
	IF_UNLIKELY( block->ct_lastSignal < block->ct_lastSync ) {
		/* each sync needs a matching signal; a signal needs to be placed */
		return kRJob_Error_InvalidOperation;
	}

	r = rjob_block_addJob( block, &job__sync_f, ( void * )block );
	IF_UNLIKELY( r != kRJob_Success ) {
		return r;
	}

	block->ct_lastSync = block->jobCount - 1;
	return kRJob_Success;
}

int rjob_block_isSubmitted( const rjob_block_t *block ) {
	IF_UNLIKELY( !block ) {
		return 0;
	}

	return +( block->queue != NULL );
}
void rjob_block_abort( rjob_block_t *block ) {
	volatile rjob_uint_t *numThreads;

	IF_UNLIKELY( !block ) {
		return;
	}
	IF_UNLIKELY( !block->queue ) {
		return;
	}

	( void )atomic_set( &block->rt_abort, 1 );

	numThreads = &block->rt_currentParallelism;
	while( *numThreads != 0 ) {
		thread_yield();
	}
}

rjob_uint_t rjob_block_getJobCount( const rjob_block_t *block ) {
	return block->jobCount;
}
rjob_runStatus_t rjob_block_getRunStatus( const rjob_block_t *block ) {
	return block->rt_status;
}
rjob_microsecs_t rjob_block_getWorkTime( const rjob_block_t *block ) {
	return block->rt_workTime;
}
rjob_microsecs_t rjob_block_getStallTime( const rjob_block_t *block ) {
	return block->rt_stallTime;
}
rjob_microsecs_t rjob_block_getPeakTime( const rjob_block_t *block ) {
	return block->rt_peakTime;
}
rjob_uint_t rjob_block_getStallCount( const rjob_block_t *block ) {
	return block->rt_stallCount;
}

/*
===============================================================================

	QUEUES

===============================================================================
*/

struct rjob_queue_s {
	rjob_alloc_t alloc;
	rjob_dealloc_t dealloc;

	rjob_uint_t priority;

	rjob_uint_t blockIndex;
	rjob_uint_t blockCount;
	rjob_uint_t blockCapacity;
	rjob_block_t **blocks;

	rjob_runStatus_t rt_status;
	rjob_microsecs_t rt_workTime;
	rjob_microsecs_t rt_stallTime;
	rjob_microsecs_t rt_peakTime;

	rjob_uint_t rt_stallCount;
	rjob_uint_t rt_lastStalledBlock;
};

void rjob_queue_init( rjob_queue_t *queue ) {
	IF_UNLIKELY( !queue ) {
		return;
	}

	queue->alloc = &allocator_alloc_f;
	queue->dealloc = &allocator_dealloc_f;

	queue->priority = 3;

	queue->blockCount = 0;
	queue->blockCapacity = 0;
	queue->blocks = NULL;

	queue->rt_status = kRJob_RunStatus_Creating;
	queue->rt_workTime = 0;
	queue->rt_stallTime = 0;
	queue->rt_peakTime = 0;

	queue->rt_stallCount = 0;
	queue->rt_blockIndex = 0;
	queue->rt_lastStalledBlock = 0;

	queue->rt_abort = 0;
	queue->rt_poolCount = 0;
}
void rjob_queue_fini( rjob_queue_t *queue ) {
	if( !queue ) {
		return;
	}

	rjob_queue_abort( queue );
}

rjob_error_t rjob_queue_setAllocator( rjob_queue_t *queue, rjob_alloc_t alloc,
rjob_dealloc_t dealloc ) {
	if( !queue || !alloc || !dealloc ) {
		return kRJob_Error_InvalidArgument;
	}

	if( queue->rt_poolCount > 0 || queue->blocks != NULL ) {
		return kRJob_Error_InvalidOperation;
	}

	queue->alloc = alloc;
	queue->dealloc = dealloc;

	return kRJob_Success;
}
void rjob_queue_getAllocator( const rjob_queue_t *queue, rjob_alloc_t *alloc,
rjob_dealloc_t *dealloc ) {
	if( !queue ) {
		return;
	}

	if( alloc != NULL ) {
		*alloc = queue->alloc;
	}
	if( dealloc != NULL ) {
		*dealloc = queue->dealloc;
	}
}

void rjob_queue_setPriority( rjob_queue_t *queue, rjob_uint_t priority ) {
	if( !queue ) {
		return;
	}

	( void )atomic_set( &queue->priority, priority );
}
rjob_uint_t rjob_queue_getPriority( const rjob_queue_t *queue ) {
	if( !queue ) {
		return 0;
	}

	return queue->priority;
}

rjob_error_t rjob_queue_submitBlock( rjob_queue_t *queue,
rjob_block_t *block ) {
}
void rjob_queue_abort( rjob_queue_t *queue ) {
}

rjob_uint_t rjob_queue_getBlockCount( const rjob_queue_t *queue ) {
}
const rjob_block_t *rjob_queue_getBlock( const rjob_queue_t *queue,
rjob_uint_t index ) {
}
rjob_runStatus_t rjob_queue_getRunStatus( const rjob_queue_t *queue ) {
}
rjob_microsecs_t rjob_queue_getWorkTime( const rjob_queue_t *queue ) {
}
rjob_microsecs_t rjob_queue_getStallTime( const rjob_queue_t *queue ) {
}
rjob_microsecs_t rjob_queue_getPeakTime( const rjob_queue_t *queue ) {
}
rjob_uint_t rjob_queue_getStallCount( const rjob_queue_t *queue ) {
}
