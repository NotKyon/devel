#ifndef LOOKUP_H
#define LOOKUP_H

#define LOOKUP_LOWER "abcdefghijklmnopqrstuvwxyz"
#define LOOKUP_UPPER "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define LOOKUP_DIGIT "0123456789"
#define LOOKUP_UNDER "_"
#define LOOKUP_IDENT LOOKUP_LOWER LOOKUP_UPPER LOOKUP_DIGIT LOOKUP_UNDER

typedef struct entry_s {
	struct entry_s *entries;
	void *p;
} entry_t;
typedef struct lookup_s {
	int convmap[256];

	int numEntries;
	struct entry_s *entries;
} lookup_t;

/*
 * Initialize a lookup table.
 *
 * table: Address of the table to initialize.
 * allowed: String containing each character that's allowed to be represented in
 *          the table. You can use LOOKUP_IDENT for the default (all lowercase
 *          and uppercase letters, all digits, and an underscore).
 * return: 1 if successful; 0 otherwise.
 */
int InitLookup(lookup_t *table, const char *allowed);

/*
 * Delete a set of entries within a lookup table.
 *
 * table: Address of the table containing the entries.
 * entries: Address of the entries to delete.
 */
void DeleteLookupEntries(lookup_t *table, entry_t *entries);

/*
 * Deinitialize a lookup table.
 *
 * table: Address of the table to deinitialize.
 */
void DeinitLookup(lookup_t *table);

/*
 * Find an entry within a lookup table.
 *
 * table: Address of the table with the entries to search.
 * str: String to use to search the table.
 * return: Address of the entry that matches, or NULL if it could not be found
 *         or 'str' is illegal.
 */
entry_t *FindEntry(lookup_t *table, const char *str);

/*
 * Find an entry within a lookup table, creating it if it does not exist.
 *
 * table: Address of the table with the entries to search.
 * str: String to use to search the table.
 * return: Address of the entry that matches or a new entry, or NULL if 'str' is
 *         illegal.
 */
entry_t *LookupEntry(lookup_t *table, const char *str);

/*
 * Create a new entry in the lookup table.
 *
 * table: Address of the table to create the entry within.
 * str: Name of the entry.
 * p: Value to associate with the entry.
 * return: Address of the entry or NULL if the entry already exists with a
 *         non-zero value.
 */
entry_t *NewEntry(lookup_t *table, const char *str, void *p);

/*
 * Set an existing entry within the lookup table.
 *
 * table: Address of the table containing the entry to set.
 * str: Name of the entry.
 * p: Value to associate with the entry.
 * return: Address of the entry or NULL if the entry already exists with a
 *         non-zero value.
 */
entry_t *SetEntry(lookup_t *table, const char *str, void *p);

/*
 * Retrieve the value of an entry within a lookup table.
 *
 * table: Address of the table containing the entry to set.
 * str: Name of the entry.
 * return: Value of the entry found or NULL if the entry was not found.
 */
void *GetEntry(lookup_t *table, const char *str);

#endif
