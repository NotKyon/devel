#include "mk_autolink.h"

autolink_t g_al_head = (autolink_t)0;
autolink_t g_al_tail = (autolink_t)0;

autolink_t NewAutolink() {
	autolink_t al;
	size_t i;

	al = (autolink_t)XsMemory((void *)0, sizeof(*al));

	for(i=0; i<kNumProjSys; i++)
		al->header[i] = (char *)0;
	al->lib = (char *)0;

	al->next = (autolink_t)0;
	if ((al->prev = g_al_tail) != (autolink_t)0)
		g_al_tail->next = al;
	else
		g_al_head = al;
	g_al_tail = al;

	return al;
}
void DeleteAutolink(autolink_t al) {
	size_t i;

	if (!al)
		return;

	for(i=0; i<kNumProjSys; i++)
		al->header[i] = (char *)XsMemory((void *)al->header[i], 0);
	al->lib = (char *)XsMemory((void *)al->lib, 0);

	if (al->prev)
		al->prev->next = al->next;
	if (al->next)
		al->next->prev = al->prev;

	if (g_al_head==al)
		g_al_head = al->next;
	if (g_al_tail==al)
		g_al_tail = al->prev;

	XsMemory((void *)al, 0);
}
void DeleteAllAutolinks() {
	while(g_al_head)
		DeleteAutolink(g_al_head);
}

void SetAutolinkHeader(autolink_t al, int sys, const char *header) {
	assert(al != (autolink_t)0);
	assert((unsigned int)sys < kNumProjSys);

	al->header[sys] = XsReduplicate(al->header[sys], header);
}
void SetAutolinkLib(autolink_t al, const char *libname) {
	assert(al != (autolink_t)0);

	al->lib = XsReduplicate(al->lib, libname);
}

const char *AutolinkHeader(autolink_t al, int sys) {
	assert(al != (autolink_t)0);
	assert((unsigned int)sys < kNumProjSys);

	return al->header[sys];
}
const char *AutolinkLib(autolink_t al) {
	assert(al != (autolink_t)0);

	return al->lib;
}

autolink_t AutolinkBefore(autolink_t al) {
	assert(al != (autolink_t)0);

	return al->prev;
}
autolink_t AutolinkAfter(autolink_t al) {
	assert(al != (autolink_t)0);

	return al->next;
}
autolink_t FirstAutolink() {
	return g_al_head;
}
autolink_t LastAutolink() {
	return g_al_tail;
}

autolink_t FindAutolink(int sys, const char *header) {
	autolink_t al;

	assert((unsigned int)sys < kNumProjSys);
	assert(header != (const char *)0);

	for(al=FirstAutolink(); al; al=AutolinkAfter(al)) {
		if (!al->header[sys])
			continue;

		if (matchpath(al->header[sys], header))
			return al;
	}

	return (autolink_t)0;
}

const char *Autolink(int sys, const char *header) {
	autolink_t al;
	lib_t lib;

	assert((unsigned int)sys < kNumProjSys);
	assert(header != (const char *)0);

	al = FindAutolink(sys, header);
	if (!al)
		return (const char *)0;

	lib = FindLib(AutolinkLib(al));
	if (!lib)
		return (const char *)0;

	return LibFlags(lib, sys);
}
