﻿#include <stdio.h>
#include <stdlib.h>

const char *k_utoa( unsigned n, unsigned radix )
{
	static const char *const upper = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static char buf[ 16 ] = { 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 };
	char *p = &buf[ sizeof( buf ) - 1 ];

	do {
		*--p = upper[ n%radix ];
		n /= radix;
	} while( n != 0 );

	return p;
}
void k_print_utoa( unsigned n, unsigned radix )
{
	printf( "%s\n", k_utoa( n, radix ) );
}

int main()
{
	k_print_utoa(  123, 10 );
	k_print_utoa( 0xFF, 16 );
	k_print_utoa(    0, 10 );

	return EXIT_SUCCESS;
}


/*

;  in: ax=n, cx=radix
; out: ax=address of text
;      clobbers di, bx
k_utoa:
	mov di, .buf + 15
	.top:
		dec di
		div cl
		mov bl, [.upper + ah]
		mov [di], bl
		test ax
		jnz .top
	mov ax, di
	retn
.buf: dd 0, 0, 0, 0
.upper: db "0123456789ABCDEF"

*/


