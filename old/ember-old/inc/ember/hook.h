#ifndef EMBER_HOOK_H
#define EMBER_HOOK_H

#include "config.h"

typedef struct HookCollection *hook_collection_t;

typedef int ( *hookcall_t )( void *dat, void *ctx );

struct Hook {
	hookcall_t func;
	void *ctx;

	struct HookCollection *hc;
	struct Hook *prev,
	    *next;
};
struct HookCollection {
	struct Hook *head,
	    *tail;
};
__em_extrn gc_pool_t g_hook_collection_pool __em_nil(),
    g_hook_pool __em_nil();

extern int hook_init();
extern void hook_deinit();
extern hook_collection_t hook_alloc_collection();
extern void hook_dealloc_collection( hook_collection_t hc );
extern void hook_add( hook_collection_t hc, hookcall_t f, void *ctx );
extern void hook_remove( hook_collection_t hc, hookcall_t f, void *ctx );
extern int hook_run_all( hook_collection_t hc, void *dat );
extern int hook_run( hook_collection_t hc, void *dat, void *ctx );

#endif
