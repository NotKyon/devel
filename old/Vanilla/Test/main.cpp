// includes
#include <windows.h>
#include <stdio.h>
#include <Axia/Vanilla/Vanilla.h>

// draw callback
void MainFrame_DrawRect(AXWidget *pWidget, int left, int top, int right, int bottom)
{
	//AXFillRectVertGL(left, top, right, bottom, 0xFFFFFFFF, 0x00000000);
	//AXDrawTextGL(pWidget, "This is my message!", AXDrawTextSingleLineBit|AXDrawTextCenterBit|AXDrawTextVCenterBit|AXDrawTextNoPrefixBit, left, top, right, bottom);
	
	AXFillRectVertGL(0, 0, AXWidth(pWidget), AXHeight(pWidget), 0, 0xFFFFFFFF);
	AXDrawTextGL(pWidget, "Hello, world!", AXDrawTextSingleLineBit|AXDrawTextCenterBit|AXDrawTextVCenterBit|AXDrawTextNoPrefixBit, 0, 0, AXWidth(pWidget), AXHeight(pWidget));
}
void ChildFrame_DrawRect(AXWidget *pWidget, int left, int top, int right, int bottom)
{
	AXMatrixModeToProjectionGL();
	AXLoadIdentityGL();
	AXOrthoGL(0, 1, 0, 1);
	
	AXMatrixModeToModelViewGL();
	AXLoadIdentityGL();
	
	AXFillRectGL(0, 0, 1, 1, 0xFF7F0000);
	
	AXMatrixModeToModelViewGL();
	AXLoadIdentityGL();
	
	AXBeginTrianglesGL();
		AXColor3fGL(1, 0, 0);
		AXVertex2fGL(0, 0);
		//AXVertex2fGL(-1, -1);
		//AXVertex2fGL(0, 0);
		
		AXColor3fGL(0, 1, 0);
		AXVertex2fGL(0.5, 1);
		//AXVertex2fGL(0, 1);
		//AXVertex2fGL(AXWidth(pWidget)/2, AXHeight(pWidget));
		
		AXColor3fGL(0, 0, 1);
		AXVertex2fGL(1, 0);
		//AXVertex2fGL(1, -1);
		//AXVertex2fGL(AXWidth(pWidget), 0);
	AXEndGL();
	AXBeginLineStripGL();
		AXColor3fGL(1, 0, 0);
		AXVertex2fGL(0.5, 0.5);
		//AXVertex2fGL(-1, -1);
		//AXVertex2fGL(0, 0);
		
		AXColor3fGL(0, 1, 0);
		AXVertex2fGL(0.75, 1);
		//AXVertex2fGL(0, 1);
		//AXVertex2fGL(AXWidth(pWidget)/2, AXHeight(pWidget));
		
		AXColor3fGL(0, 0, 1);
		AXVertex2fGL(1, 0.5);
		//AXVertex2fGL(1, -1);
		//AXVertex2fGL(AXWidth(pWidget), 0);
	AXEndGL();
}

// main
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	// declarations
	AXWidget	*pMainWnd;
	AXWidget	*pChildWnd;
	AXWidget	*pDlgWnd;
	AXWidget	*pButtonWnd;
	AXWidget	*pTextFieldWnd;
	FILE		*pFile;
	
	// initialize
	AXInitVanilla();
	
	if ((pFile=fopen("debug.log", "a+")) != (FILE *)0)
	{
		fprintf(pFile, "Init\n\n");
		
		fclose(pFile);
	}
	
	// main frame
	pMainWnd = AXMakeWindow("Main Frame", 640, 480);
	AXSetPosition(pMainWnd, 50, 50);
	AXSetSize(pMainWnd, 800, 600);
	//AXOnDrawRect(pMainWnd, MainFrame_DrawRect);
	AXOnDrawRect(pMainWnd, ChildFrame_DrawRect);
	AXMakeKeyAndOrderFront(pMainWnd);
	
	// child frame
	pChildWnd = AXMakeWindow("Child Frame", 400, 250);
	AXSetParent(pChildWnd, pMainWnd);
	AXMakeKeyAndOrderFront(pChildWnd);
	
	// test recursive issue
	AXAddChild(pChildWnd, pMainWnd);
	AXOnDrawRect(pChildWnd, ChildFrame_DrawRect);
	AXZoom(pChildWnd);
	
	pDlgWnd = AXMakeWindow("Dialog Frame", 260, 200);
	AXOnDrawRect(pDlgWnd, ChildFrame_DrawRect);
	AXSetParent(pDlgWnd, pMainWnd);
	AXSetPosition(pDlgWnd, 25, 25);
	pButtonWnd = AXMakeButton(pDlgWnd, "Button", 5, 5, 250, 25);
	pTextFieldWnd = AXMakeTextField(pDlgWnd, "Text Field", 5, 35, 250, 25);
	AXMakeKeyAndOrderFront(pDlgWnd);
	
	if ((pFile=fopen("debug.log", "a+")) != (FILE *)0)
	{
		fprintf(pFile, "Main Frame:\n");
		fprintf(pFile, "position [local]: %d, %d\n", AXPositionX(pMainWnd), AXPositionY(pMainWnd));
		fprintf(pFile, "position [global]: %d, %d\n", AXPositionX(pMainWnd, YES), AXPositionY(pMainWnd, YES));
		fprintf(pFile, "size [local]: %d, %d\n", AXWidth(pMainWnd), AXHeight(pMainWnd));
		fprintf(pFile, "size [global]: %d, %d\n", AXWidth(pMainWnd, YES), AXHeight(pMainWnd, YES));
		fprintf(pFile, "title: \"%s\"\n", AXTitle(pMainWnd));
		fprintf(pFile, "\n");

		fprintf(pFile, "Child Frame:\n");
		fprintf(pFile, "position [local]: %d, %d\n", AXPositionX(pChildWnd), AXPositionY(pChildWnd));
		fprintf(pFile, "position [global]: %d, %d\n", AXPositionX(pChildWnd, YES), AXPositionY(pChildWnd, YES));
		fprintf(pFile, "size [local]: %d, %d\n", AXWidth(pChildWnd), AXHeight(pChildWnd));
		fprintf(pFile, "size [global]: %d, %d\n", AXWidth(pChildWnd, YES), AXHeight(pChildWnd, YES));
		fprintf(pFile, "title: \"%s\"\n", AXTitle(pChildWnd));
		fprintf(pFile, "\n");
		
		fclose(pFile);
	}
	
	// main loop
	while(AXIsRunning())
	{
		// determine whether to quit
		if (!AXFirstHandle())
			AXQuit();
		
		// synchronize
		AXSync();
	}
	
	// deinitialize
	if ((pFile=fopen("debug.log", "a+")) != (FILE *)0)
	{
		fprintf(pFile, "\n-Deinit\n");
		
		fclose(pFile);
	}

	AXDeinitVanilla();

	if ((pFile=fopen("debug.log", "a+")) != (FILE *)0)
	{
		fprintf(pFile, "+Deinit\n\n");
		
		fclose(pFile);
	}
	
	// done
	return 0;
}
