/*
 * This file is not meant to be included directly...
 */

#if GB_INIT
extern void GBMain();

int main(int argc, char **argv) {
	gbAppInit(argc, argv);

	gbSetConsoleTextColor(kGBConCol_BrightWhite);
	gbWriteStdout(gbEngineName());
	gbSetConsoleTextColor(kGBConCol_White);
	gbWriteStdout(" built on ");
	gbSetConsoleTextColor(kGBConCol_BrightWhite);
	gbWriteStdout(gbEngineBuildDate());
	gbSetConsoleTextColor(kGBConCol_White);
	gbWriteStdout(" at ");
	gbSetConsoleTextColor(kGBConCol_BrightWhite);
	gbWriteStdout(gbEngineBuildTime());
	gbSetConsoleTextColor(kGBConCol_White);
	gbWriteStdout("\n");

	if (gbEngineDebugBuild()) {
		gbSetConsoleTextColor(kGBConCol_Cyan);
		gbWriteStdout("This is a ");
		gbSetConsoleTextColor(kGBConCol_BrightCyan);
		gbWriteStdout("DEBUG BUILD");
		gbSetConsoleTextColor(kGBConCol_Cyan);
		gbWriteStdout(". Performance will be ");
		gbSetConsoleTextColor(kGBConCol_BrightRed);
		gbWriteStdout("SLOW");
		gbSetConsoleTextColor(kGBConCol_Cyan);
		gbWriteStdout("!");
		gbSetConsoleTextColor(kGBConCol_White);
		gbWriteStdout("\n");
	}

	gbWriteStdout("\n");

	GBMain();
	return gbGetExitFlag();
}

# if _WIN32
int WINAPI WinMain(HINSTANCE a, HINSTANCE b, LPSTR c, int d) {
	if (a||b||c||d) {/*unused*/}

#  if _MSC_VER||__INTEL_COMPILER
	return main(__argc, __argv);
#  else
	return main(0, (char **)NULL);
#  endif
}
# endif
#endif
