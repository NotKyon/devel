#include "threadconf.h"

#include "lock.h"
#include "signal.h"
#include "jobstream.h"

typedef struct threadData_s {
	thread_t sysHandle;
	gb_fnThread_t fnThread;
	void *p;

	gb_bool_t running;
	int exitValue;
} threadData_t;

static threadResult_t THREADPROCAPI WrapperThread_f(void *p) {
	threadData_t *td;
	union { int i; void *p; } r;

	td = (threadData_t *)p;
	gbAssert(td != (threadData_t *)NULL);
	gbAssert(td->fnThread != (gb_fnThread_t)NULL);

	td->running = GB_TRUE;

	r.p  = (void *)NULL;
	r.i |= td->fnThread(td->p);

	td->exitValue = r.i;
	td->running = GB_FALSE;

	return *(threadResult_t *)&r.i;
}

/* -------------------------------------------------------------------------- */

thread_t gblocalThreadSelf() {
#if __USE_WIN32_THREADS__
	return GetCurrentThread();
#else
	return pthread_self();
#endif
}
thread_t gblocalNewThread(threadFunc_t func, void *p) {
	thread_t h;

#if __USE_WIN32_THREADS__
	h = CreateThread(0, 0, func, p, 0, 0);
#else
	memset(&h, 0, sizeof(h));
	pthread_create(&h, (const pthread_attr_t *)0, func, p);
#endif

	return h;
}
void gblocalSetThreadCPU(thread_t thread, gb_u32_t cpu) {
#if __USE_WIN32_THREADS__
	DWORD_PTR mask, cpuMask, stride;

	stride = gbGetCPUThreadCount()/gbGetCPUCoreCount();

	cpuMask  = 1;
	cpuMask |= stride&(1<<1);

	mask = cpuMask<<(cpu*stride);

	SetThreadAffinityMask(thread, mask);
#else
# if __CYGWIN__
	/* can't set affinity of thread via pthreads... big performance sink */
# else
	/* HELPFUL: http://bit.ly/yw7R3W */
	cpu_set_t cpuset;

	cpuset = cpu;
	pthread_setaffinity_np(thread, sizeof(cpuset), &cpuset);
# endif
#endif
}

void gblocalJoinThread(thread_t thread) {
#if __USE_WIN32_THREADS__
	WaitForSingleObject(thread, INFINITE);
#else
	pthread_join(thread, (void **)0);
#endif
}
void gblocalKillThread(thread_t thread) {
#if __USE_WIN32_THREADS__
	/*TerminateThread(thread, 0);*/
	CloseHandle(thread);
#else
	/* no pthread_destroy()? there's a pthread_kill() */
	memset((void *)&thread, 0, sizeof(thread));
#endif
}

void gblocalSetThreadPriority(thread_t thread, gb_threadPriority_t priority) {
/*
 *	kGBThreadPriority_Idle
 *	kGBThreadPriority_VeryLow
 *	kGBThreadPriority_Low
 *	kGBThreadPriority_BelowNormal
 *	kGBThreadPriority_Normal
 *	kGBThreadPriority_AboveNormal
 *	kGBThreadPriority_High
 *	kGBThreadPriority_VeryHigh
 *	kGBThreadPriority_Critical
 */
#if __USE_WIN32_THREADS__
	static const DWORD LUT[kNumGBThreadPriorities] = {
		THREAD_PRIORITY_IDLE, THREAD_PRIORITY_LOWEST, THREAD_PRIORITY_LOWEST,
		THREAD_PRIORITY_BELOW_NORMAL,
		THREAD_PRIORITY_NORMAL,
		THREAD_PRIORITY_ABOVE_NORMAL,
		THREAD_PRIORITY_HIGHEST, THREAD_PRIORITY_HIGHEST,
		THREAD_PRIORITY_TIME_CRITICAL
	};

	SetThreadPriority(thread, LUT[priority]);
#else
	static const int LUT[kNumGBThreadPriorities] = {
		-16, -12, -8, -4,
		0,
		+4, +8, +12, +16
	};

	pthread_setschedprio(thread, LUT[priority]);
#endif
}

/* -------------------------------------------------------------------------- */

static gb_bool_t ThreadCtor_f(threadData_t *p) {
	memset(&p->sysHandle, 0, sizeof(p->sysHandle));
	p->fnThread = (gb_fnThread_t)NULL;
	p->p = (void *)NULL;

	p->running = GB_FALSE;
	p->exitValue = -1;

	return GB_TRUE;
}
static void ThreadDtor_f(threadData_t *p) {
	gblocalJoinThread(p->sysHandle);
	gblocalKillThread(p->sysHandle);
}

static gb_bool_t LockCtor_f(lock_t *p) {
	InitLock(p);
	return GB_TRUE;
}
static void LockDtor_f(lock_t *p) {
	FiniLock(p);
}

static gb_bool_t SignalCtor_f(signal_t *p) {
	InitSignal(p, 0);
	return GB_TRUE;
}
static void SignalDtor_f(signal_t *p) {
	FiniSignal(p);
}

/* -------------------------------------------------------------------------- */

static gb_handleType_t g_threadHandleType = (gb_handleType_t)NULL;
static gb_handleType_t g_lockHandleType = (gb_handleType_t)NULL;
static gb_handleType_t g_signalHandleType = (gb_handleType_t)NULL;

static jobStream_t g_jobStream;

#define MAX_STREAM_THREADS 64
static thread_t g_streamThreads[MAX_STREAM_THREADS];
static gb_uint_t g_numStreamThreads = 0;

static threadResult_t THREADPROCAPI StreamerThread_f(void *p) {
	Streamer((jobStream_t *)p);
	return (threadResult_t)0;
}

void gblocalInitThreading() {
	if (g_threadHandleType)
		return;

	g_threadHandleType = gbAddHandleType(
		"Thread", sizeof(threadData_t),
		(gb_fnCtor_t)ThreadCtor_f,
		(gb_fnDtor_t)ThreadDtor_f);

	g_lockHandleType = gbAddHandleType(
		"Lock", sizeof(lock_t),
		(gb_fnCtor_t)LockCtor_f,
		(gb_fnDtor_t)LockDtor_f);

	g_signalHandleType = gbAddHandleType(
		"Signal", sizeof(signal_t),
		(gb_fnCtor_t)SignalCtor_f,
		(gb_fnDtor_t)SignalDtor_f);
}
void gblocalFiniThreading() {
	if (!g_threadHandleType)
		return;

	if (g_numStreamThreads > 0) {
		FiniStream(&g_jobStream);
		g_numStreamThreads = 0;
	}

	g_signalHandleType = gbDropHandleType(g_signalHandleType);
	g_lockHandleType = gbDropHandleType(g_lockHandleType);
	g_threadHandleType = gbDropHandleType(g_threadHandleType);
}
struct jobStream_s *gblocalJobStream() {
	return &g_jobStream;
}

/* -------------------------------------------------------------------------- */

static threadData_t *GetThreadData(gb_thread_t thread) {
	gbAssert(gbHandleType(thread) == g_threadHandleType);

	return (threadData_t *)gbObject((gb_handle_t)thread);
}

gb_thread_t gbNewThread(gb_fnThread_t fnThread, void *p) {
	threadData_t *td;
	gb_thread_t t;

	gbAssert(g_threadHandleType != (gb_handleType_t)NULL);

	t = gbNewHandle(g_threadHandleType);
	td = (threadData_t *)gbObject(t);

	td->fnThread = fnThread;
	td->p = p;

	td->sysHandle = gblocalNewThread(WrapperThread_f, (void *)td);
	gbAssert(td->sysHandle != (thread_t)NULL);

	return t;
}
gb_thread_t gbDeleteThread(gb_thread_t thread) {
	return (gb_thread_t)gbDropHandle((gb_handle_t)thread);
}

gb_bool_t gbIsThreadRunning(gb_thread_t thread) {
	return GetThreadData(thread)->running;
}
int gbGetThreadExitValue(gb_thread_t thread) {
	threadData_t *td;

	td = GetThreadData(thread);

	return td->running ? 0 : td->exitValue;
}

void gbSetThreadCPU(gb_thread_t thread, gb_u32_t cpu) {
	gblocalSetThreadCPU(GetThreadData(thread)->sysHandle, cpu);
}

void gbJoinThread(gb_thread_t thread) {
	gblocalJoinThread(GetThreadData(thread)->sysHandle);
}

void gbSetThreadPriority(gb_thread_t thread, gb_threadPriority_t priority) {
	gbAssert(priority < kNumGBThreadPriorities);

	gblocalSetThreadPriority(GetThreadData(thread)->sysHandle, priority);
}

/* -------------------------------------------------------------------------- */

static lock_t *GetLock(gb_lock_t lock) {
	gbAssert(gbHandleType(lock) == g_lockHandleType);

	return (lock_t *)gbObject((gb_handle_t)lock);
}

gb_lock_t gbNewLock() {
	return gbNewHandle(g_lockHandleType);
}
gb_lock_t gbDeleteLock(gb_lock_t lock) {
	return (gb_lock_t)gbDropHandle((gb_handle_t)lock);
}

void gbLock(gb_lock_t lock) {
	Lock(GetLock(lock));
}
gb_bool_t gbTryLock(gb_lock_t lock) {
	return TryLock(GetLock(lock));
}
void gbUnlock(gb_lock_t lock) {
	Unlock(GetLock(lock));
}

/* -------------------------------------------------------------------------- */

static signal_t *GetSignal(gb_signal_t sig) {
	gbAssert(gbHandleType(sig) == g_signalHandleType);

	return (signal_t *)gbObject((gb_handle_t)sig);
}

gb_signal_t gbNewSignal(gb_uint_t expCnt) {
	gb_signal_t h;
	signal_t *p;

	h = gbNewHandle(g_signalHandleType);
	p = (signal_t *)gbObject(h);

	if (expCnt)
		ResetSignal(p, expCnt);

	return h;
}
gb_signal_t gbDeleteSignal(gb_signal_t sig) {
	return (gb_signal_t)gbDropHandle((gb_handle_t)sig);
}

void gbResetSignal(gb_signal_t sig, gb_uint_t expCnt) {
	ResetSignal(GetSignal(sig), expCnt);
}
void gbAllocSignalSlot(gb_signal_t sig) {
	AllocSignalSlot(GetSignal(sig));
}
void gbRaiseSignal(gb_signal_t sig) {
	RaiseSignal(GetSignal(sig));
}

void gbSyncSignal(gb_signal_t sig) {
	SyncSignal(GetSignal(sig));
}
gb_bool_t gbTrySyncSignal(gb_signal_t sig) {
	return TrySyncSignal(GetSignal(sig));
}

void gbSpinSyncSignal(gb_signal_t sig, size_t spinCount) {
	SpinSyncSignal(GetSignal(sig), spinCount);
}

/* -------------------------------------------------------------------------- */

void gbRequireDefStream() {
	gb_uint_t i, count;

	if (g_numStreamThreads)
		return;

	InitStream(&g_jobStream);

	count = gbGetCPUThreadCount();
	if (count > MAX_STREAM_THREADS)
		count = MAX_STREAM_THREADS;

	for(i=0; i<count; i++) {
		g_streamThreads[i] = gblocalNewThread(StreamerThread_f, &g_jobStream);
		gblocalSetThreadCPU(g_streamThreads[i], i%64);
	}

	g_numStreamThreads = count;
}

gb_uint_t gbCountDefStreamThreads() {
	return g_numStreamThreads;
}
gb_uint_t gbGetJobStreamBufferSize() {
	return TASK_CONF_BUFFER_SIZE;
}

void gbSchedule(gb_jobFunc_t fn, void *p) {
	Schedule(&g_jobStream, fn, p);
}

void gbSyncDefStream() {
	SyncStream(&g_jobStream);
}
void gbSyncDefStreamActive() {
	SyncStreamActive(&g_jobStream);
}
