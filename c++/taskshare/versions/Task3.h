#ifndef TASK_H
#define TASK_H

#include "../TaskShare.h"

//------------------------------------------------------------------------------
// JOB QUEUE

__TS_NS_INIT__

typedef struct token_s {
	union { jobFunc_t fn; void *fnp; };
	void *p;
} token_t;
typedef struct queue_s {
	enum {
#if !TASK_CONF_BUFFER_SIZE
# undef  TASK_CONF_BUFFER_SIZE
# define TASK_CONF_BUFFER_SIZE 4096
#endif
		kBufferSize = TASK_CONF_BUFFER_SIZE
	};

	token_t buffer[kBufferSize];
	u32 numPendingJobs;
	u32 insertionIndex;
	u32 streamingIndex;

	u32 numStreamers;

	lock_t lock;
} queue_t;

inline void InitQueue(queue_t &q);
inline void FiniQueue(queue_t &q);

inline void EnqueueToken(queue_t &q, const token_t &t);
inline void Schedule(queue_t &q, jobFunc_t fn, void *p);
inline void ScheduleTermination(queue_t &q);

inline void Streamer(queue_t &q);

inline void SyncQueue(queue_t &q);
inline void SyncQueueActive(queue_t &q);

inline void InitQueue(queue_t &q) {
	memset((void *)q.buffer, 0, sizeof(q.buffer));
	q.numPendingJobs = 0;
	q.insertionIndex = 0;
	q.streamingIndex = 0;
	q.numStreamers   = 0;
	InitLock(q.lock);
}
inline void FiniQueue(queue_t &q) {
	ScheduleTermination(q);
	while(q.numStreamers > 0)
		Relinquish();
	FiniLock(q.lock);
}

inline void EnqueueToken(queue_t &q, const token_t &t) {
	u32 index;

	if (t.fn==(jobFunc_t)0)
		return;

	Lock(q.lock);
		q.numPendingJobs++;

		index = q.insertionIndex++ % queue_t::kBufferSize;
	Unlock(q.lock);

	while(q.buffer[index].fnp != (void *)0)
		Relinquish();

	Lock(q.lock);
		q.buffer[index].p  = t.p ;
		q.buffer[index].fn = t.fn;
	Unlock(q.lock);
}
inline void Schedule(queue_t &q, jobFunc_t fn, void *p) {
	token_t t;

	t.fn = fn;
	t.p  = p ;

	EnqueueToken(q, t);
}
inline void ScheduleTermination(queue_t &q) {
	token_t t;
	u32 i;

	t.fnp = (void *)1;
	t.p   = (void *)0;

	Lock(q.lock);
	for(i=0; i<queue_t::kBufferSize; i++)
		q.buffer[i] = t;
	Unlock(q.lock);
}

inline void Streamer(queue_t &q) {
	u32 index;

	Lock(q.lock);
	q.numStreamers++;
	//Unlock(q.lock);

	do {
		//Lock(q.lock);
			index = q.streamingIndex++ % queue_t::kBufferSize;
			while(q.buffer[index].fnp == (void *)0)
				Relinquish();
		Unlock(q.lock);

		switch((size_t)q.buffer[index].fnp) {
		case 1:
			goto __Streamer_return;

		default:
			q.buffer[index].fn(q.buffer[index].p);
			break;
		}

		//NOTE: no need to set 'p'
		Lock(q.lock);
			q.buffer[index].fnp = (void *)0;
			q.numPendingJobs--;
		//Unlock(q.lock);
	} while(1);

__Streamer_return:
	Lock(q.lock);
	q.numStreamers--;
	Unlock(q.lock);
}

inline void SyncQueue(queue_t &q) {
	while(q.numPendingJobs)
		Relinquish();
}
inline void SyncQueueActive(queue_t &q) {
	while(q.numPendingJobs) {
	}
}

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
