#ifndef AXTEK_CONTAINERS_CONFIG_H
#define AXTEK_CONTAINERS_CONFIG_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../Base/Config.h"

#define __AX_LIB_NS_INIT__ __AX_NS_INIT__ namespace lib {
#define __AX_LIB_NS_FINI__ } __AX_NS_FINI__

#endif
