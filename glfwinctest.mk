all: glfwinctest

clean:
	-@rm -f $(wildcard glfwinctest glfwinctest.exe) 2>/dev/null

glfwinctest: glfwinctest.o
	@g++ -o $@ $<

glfwinctest.o: glfwinctest.cpp glfwinctest.h
	@g++ -c -o $@ $<
