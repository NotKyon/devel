#include <xs/buffer.h>
#include <xs/assert.h>
#include <xs/memory.h>
#include <xs/string.h>
#include <xs/logging.h>
#include <stdio.h>
#include <string.h>

struct xs_buffer_s {
	char *filename;
	xs_size_t numlines;
	xs_size_t *lines; /* indices corresponding to each line */
	xs_size_t length;
	xs_size_t index;
	char *source;

	struct xs_buffer_s *prev, *next;
};
static struct xs_buffer_s *g_buf_head = (struct xs_buffer_s *)0;
static struct xs_buffer_s *g_buf_tail = (struct xs_buffer_s *)0;

static void AnalyzeBufferLines(xs_buffer_t buf) {
	xs_size_t i;

	buf->numlines = 0;
	buf->lines = (xs_size_t *)XsMemory((void *)buf->lines, 0);

	for(i=0; i<buf->length; i++) {
		if (buf->source[i] != '\n')
			continue;

		buf->lines = (xs_size_t *)XsMemory((void *)buf->lines,
			sizeof(xs_size_t)*(buf->numlines + 1));
		buf->lines[buf->numlines++] = i;
	}
}

xs_buffer_t XsNewBuffer(const char *filename, const char *source) {
	xs_buffer_t buf;

	buf = (xs_buffer_t)XsMemory((void *)0, sizeof(*buf));

	buf->filename = XsDuplicate(filename);
	buf->numlines = 0;
	buf->lines = (xs_size_t *)0;
	buf->index = 0;
	buf->source = XsDuplicate(source);
	buf->length = source ? strlen(source) : 0;

	buf->next = (struct xs_buffer_s *)0;
	if ((buf->prev = g_buf_tail) != (struct xs_buffer_s *)0)
		g_buf_tail->next = buf;
	else
		g_buf_head = buf;
	g_buf_tail = buf;

	AnalyzeBufferLines(buf);

	return buf;
}

xs_buffer_t XsLoadBuffer(const char *filename) {
	xs_buffer_t buf;
	FILE *f;
	char curline[8192], *p;

	if (!(f = fopen(filename, "rb"))) {
		XsLogMessage(filename, 0, 0, kXsLog_Error, "Failed to open file");
		return (xs_buffer_t)0;
	}

	buf = XsNewBuffer(filename, (const char *)0);

	while(fgets(curline, sizeof(curline) - 2, f) != 0) {
		curline[sizeof(curline) - 2] = 0;

		if ((p = strchr(curline, '\r')) != (char *)0) {
			*(p + 0) = '\n';
			*(p + 1) = 0;
		}

		buf->source = XsAppend(buf->source, curline);
		buf->numlines++;
	}

	if (ferror(f)) {
		fclose(f);
		XsLogMessage(filename, 0, 0, kXsLog_Error, "Failed to read from file");
		return XsDeleteBuffer(buf);
	}

	fclose(f);

	buf->length = buf->source ? strlen(buf->source) : 0;
	AnalyzeBufferLines(buf);

	return buf;
}

xs_buffer_t XsDeleteBuffer(xs_buffer_t buf) {
	if (!buf)
		return (xs_buffer_t)0;

	if (buf->prev)
		buf->prev->next = buf->next;
	if (buf->next)
		buf->next->prev = buf->prev;

	if (g_buf_head==buf)
		g_buf_head = buf->next;
	if (g_buf_tail==buf)
		g_buf_tail = buf->prev;

	XsMemory((void *)buf->filename, 0);
	XsMemory((void *)buf->source, 0);
	XsMemory((void *)buf->lines, 0);

	return (xs_buffer_t)XsMemory((void *)buf, 0);
}

const char *XsBufferFilename(xs_buffer_t buf) {
	return buf->filename;
}
const char *XsBufferSource(xs_buffer_t buf) {
	return buf->source;
}
xs_uint_t XsBufferLine(xs_buffer_t buf) {
	xs_uint_t i, line;

	line = 0;

	for(i=0; i<buf->numlines; i++) {
		line++;

		if (buf->lines[i] >= buf->index)
			break;
	}

	return line;
}
xs_size_t XsBufferIndex(xs_buffer_t buf) {
	return buf->index;
}
xs_size_t XsBufferLength(xs_buffer_t buf) {
	return buf->length;
}

char XsPeekBuffer(xs_buffer_t buf) {
	return buf->source[buf->index];
}
char XsPullBuffer(xs_buffer_t buf) {
	char c;

	c = buf->source[buf->index];
	if (buf->index != buf->length)
		buf->index++;


	return c;
}
char XsEatBuffer(xs_buffer_t buf) {
	XsPullBuffer(buf);
	return XsPeekBuffer(buf);
}
char XsUnpullbuffer(xs_buffer_t buf) {
	if (buf->index > 0)
		buf->index--;

	return buf->source[buf->index];
}
xs_size_t XsSetBufferIndex(xs_buffer_t buf, xs_size_t index) {
	if (index > buf->length)
		index = buf->length;

	buf->index = index;
	return buf->index;
}
xs_bool_t XsBufferEnd(xs_buffer_t buf) {
	if (buf->index==buf->length)
		return XS_TRUE;

	return XS_FALSE;
	/*return buf->index==buf->length ? XS_TRUE : XS_FALSE;*/
}

void XsSkipBufferWhitespace(xs_buffer_t buf) {
	while(XsPeekBuffer(buf) <= ' ' && !XsBufferEnd(buf))
		XsPullBuffer(buf);
}
void XsSkipBufferLine(xs_buffer_t buf) {
	while(XsPeekBuffer(buf) != '\n' && !XsBufferEnd(buf))
		XsPullBuffer(buf);

	XsPullBuffer(buf); /* eat the line ending */
}
xs_bool_t XsReadBufferLine(xs_buffer_t buf, char *line, xs_size_t n) {
	xs_size_t i;

	XS_ASSERT(n > 1);

	if (XsBufferEnd(buf))
		return XS_FALSE;

	i = 0;

	while(XsPeekBuffer(buf) != '\n' && !XsBufferEnd(buf)) {
		if (i + 2 >= n)
			break;

		line[i++] = XsPullBuffer(buf);
	}

	line[i++] = XsPullBuffer(buf); /* eat the linefeed */
	line[i] = 0;

	return XS_TRUE;
}

xs_buffer_t XsFirstBuffer() {
	return g_buf_head;
}
xs_buffer_t XsLastBuffer() {
	return g_buf_tail;
}
xs_buffer_t XsBufferBefore(xs_buffer_t buf) {
	return buf->prev;
}
xs_buffer_t XsBufferAfter(xs_buffer_t buf) {
	return buf->next;
}

void XsLogBufferV(xs_buffer_t buf, int type, const char *format, va_list args) {
	XsLogMessageV(XsBufferFilename(buf), XsBufferLine(buf), 0, type, format, args);
}
void XsLogBuffer(xs_buffer_t buf, int type, const char *format, ...) {
	va_list args;

	va_start(args, format);
	XsLogBufferV(buf, type, format, args);
	va_end(args);
}
void XsErrorBuffer(xs_buffer_t buf, const char *format, ...) {
	va_list args;

	va_start(args, format);
	XsLogBufferV(buf, kXsLog_Error, format, args);
	va_end(args);
}
void XsWarnBuffer(xs_buffer_t buf, const char *format, ...) {
	va_list args;

	va_start(args, format);
	XsLogBufferV(buf, kXsLog_Warning, format, args);
	va_end(args);
}
void XsRemarkBuffer(xs_buffer_t buf, const char *format, ...) {
	va_list args;

	va_start(args, format);
	XsLogBufferV(buf, kXsLog_Remark, format, args);
	va_end(args);
}
