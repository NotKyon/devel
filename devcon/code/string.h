#ifndef STRING_H
#define STRING_H

char *str_Duplicate(const char *src);
char *str_Copy(char *dst, const char *src);

#endif
