#include "mk_dep.h"

dep_t g_dep_head = (dep_t)0;
dep_t g_dep_tail = (dep_t)0;

dep_t NewDependency(const char *name) {
	dep_t dep;

	assert(name != (const char *)0);

	dep = (dep_t)XsMemory((void *)0, sizeof(*dep));

	dep->name = XsDuplicate(name);
	dep->deps = XsNewArray();

	dep->next = (dep_t)0;
	if ((dep->prev = g_dep_tail) != (dep_t)0)
		g_dep_tail->next = dep;
	else
		g_dep_head = dep;
	g_dep_tail = dep;

	return dep;
}
void DeleteDependency(dep_t dep) {
	if (!dep)
		return;

	dep->name = (char *)XsMemory((void *)dep->name, 0);

	XsDeleteArray(dep->deps);
	dep->deps = (xs_array_t)0;

	if (dep->prev)
		dep->prev->next = dep->next;
	if (dep->next)
		dep->next->prev = dep->prev;

	if (g_dep_head==dep)
		g_dep_head = dep->next;
	if (g_dep_tail==dep)
		g_dep_tail = dep->prev;

	XsMemory((void *)dep, 0);
}
void DeleteAllDependencies() {
	while(g_dep_head)
		DeleteDependency(g_dep_head);
}

const char *GetDependencyFile(dep_t dep) {
	assert(dep != (dep_t)0);

	return dep->name;
}
void PushDependency(dep_t dep, const char *name) {
	assert(dep != (dep_t)0);
	assert(name != (const char *)0);

	XsArrayPush(dep->deps, name);
}
size_t DependencyCount(dep_t dep) {
	assert(dep != (dep_t)0);

	return XsArraySize(dep->deps);
}
const char *GetDependency(dep_t dep, size_t i) {
	assert(dep != (dep_t)0);
	assert(i < DependencyCount(dep));

	return XsArrayElement(dep->deps, i);
}

dep_t FindDependency(const char *name) {
	dep_t dep;

	for(dep=g_dep_head; dep; dep=dep->next) {
		if (!strcmp(dep->name, name))
			return dep;
	}

	return (dep_t)0;
}
