#ifndef GB_FRAMEWORK_BYTEORDER_H
#define GB_FRAMEWORK_BYTEORDER_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Byte Order */
gb_u16_t gbBigEndian16(gb_u16_t x);
gb_u32_t gbBigEndian32(gb_u32_t x);
gb_u64_t gbBigEndian64(gb_u64_t x);

gb_u16_t gbLittleEndian16(gb_u16_t x);
gb_u32_t gbLittleEndian32(gb_u32_t x);
gb_u64_t gbLittleEndian64(gb_u64_t x);

float gbBigEndianFloat(float x);
double gbBigEndianDouble(double x);

float gbLittleEndianFloat(float x);
double gbLittleEndianDouble(double x);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
