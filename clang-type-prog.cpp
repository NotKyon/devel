// Generated by llvm2cpp - DO NOT MODIFY!

#include <llvm/Pass.h>
#include <llvm/PassManager.h>
#include <llvm/ADT/SmallVector.h>
#include <llvm/Analysis/Verifier.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/CallingConv.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/IRPrintingPasses.h>
#include <llvm/IR/InlineAsm.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/FormattedStream.h>
#include <llvm/Support/MathExtras.h>
#include <algorithm>
using namespace llvm;

Module* makeLLVMModule();

int main(int argc, char**argv) {
		Module* Mod = makeLLVMModule();
		verifyModule(*Mod, PrintMessageAction);
		PassManager PM;
		PM.add(createPrintModulePass(&outs()));
		PM.run(*Mod);
		return 0;
}


Module* makeLLVMModule() {
	// Module Construction
	Module* mod = new Module("clang-type.bc", getGlobalContext());
	mod->setDataLayout("0xb7e530");
	mod->setTargetTriple("x86_64-pc-windows-msvc");
	
	// Type Definitions
	ArrayType* ArrayTy_0 = ArrayType::get(IntegerType::get(mod->getContext(), 8), 7);
	
	PointerType* PointerTy_1 = PointerType::get(ArrayTy_0, 0);
	
	ArrayType* ArrayTy_2 = ArrayType::get(IntegerType::get(mod->getContext(), 8), 2);
	
	PointerType* PointerTy_3 = PointerType::get(ArrayTy_2, 0);
	
	ArrayType* ArrayTy_4 = ArrayType::get(IntegerType::get(mod->getContext(), 8), 10);
	
	PointerType* PointerTy_5 = PointerType::get(ArrayTy_4, 0);
	
	ArrayType* ArrayTy_6 = ArrayType::get(IntegerType::get(mod->getContext(), 8), 5);
	
	PointerType* PointerTy_7 = PointerType::get(ArrayTy_6, 0);
	
	StructType *StructTy_struct_Type_s = mod->getTypeByName("struct.Type_s");
	if (!StructTy_struct_Type_s) {
	StructTy_struct_Type_s = StructType::create(mod->getContext(), "struct.Type_s");
	}
	std::vector<Type*>StructTy_struct_Type_s_fields;
	StructTy_struct_Type_s_fields.push_back(IntegerType::get(mod->getContext(), 32));
	StructTy_struct_Type_s_fields.push_back(IntegerType::get(mod->getContext(), 32));
	PointerType* PointerTy_9 = PointerType::get(IntegerType::get(mod->getContext(), 8), 0);
	
	StructTy_struct_Type_s_fields.push_back(PointerTy_9);
	StructTy_struct_Type_s_fields.push_back(PointerTy_9);
	std::vector<Type*>FuncTy_11_args;
	FuncTy_11_args.push_back(PointerTy_9);
	FunctionType* FuncTy_11 = FunctionType::get(
		/*Result=*/Type::getVoidTy(mod->getContext()),
		/*Params=*/FuncTy_11_args,
		/*isVarArg=*/false);
	
	PointerType* PointerTy_10 = PointerType::get(FuncTy_11, 0);
	
	StructTy_struct_Type_s_fields.push_back(PointerTy_10);
	StructTy_struct_Type_s_fields.push_back(PointerTy_10);
	StructTy_struct_Type_s_fields.push_back(PointerTy_10);
	StructTy_struct_Type_s_fields.push_back(PointerTy_10);
	if (StructTy_struct_Type_s->isOpaque()) {
	StructTy_struct_Type_s->setBody(StructTy_struct_Type_s_fields, /*isPacked=*/false);
	}
	
	ArrayType* ArrayTy_8 = ArrayType::get(StructTy_struct_Type_s, 2);
	
	PointerType* PointerTy_12 = PointerType::get(ArrayTy_8, 0);
	
	PointerType* PointerTy_13 = PointerType::get(IntegerType::get(mod->getContext(), 32), 0);
	
	
	// Function Declarations
	
	Function* func_StrInstance_Init_f = mod->getFunction("StrInstance_Init_f");
	if (!func_StrInstance_Init_f) {
	func_StrInstance_Init_f = Function::Create(
		/*Type=*/FuncTy_11,
		/*Linkage=*/GlobalValue::ExternalLinkage,
		/*Name=*/"StrInstance_Init_f", mod); // (external, no body)
	func_StrInstance_Init_f->setCallingConv(CallingConv::C);
	}
	AttributeSet func_StrInstance_Init_f_PAL;
	{
		SmallVector<AttributeSet, 4> Attrs;
		AttributeSet PAS;
			{
				AttrBuilder B;
				PAS = AttributeSet::get(mod->getContext(), ~0U, B);
			}
		
		Attrs.push_back(PAS);
		func_StrInstance_Init_f_PAL = AttributeSet::get(mod->getContext(), Attrs);
		
	}
	func_StrInstance_Init_f->setAttributes(func_StrInstance_Init_f_PAL);
	
	Function* func_StrInstance_Fini_f = mod->getFunction("StrInstance_Fini_f");
	if (!func_StrInstance_Fini_f) {
	func_StrInstance_Fini_f = Function::Create(
		/*Type=*/FuncTy_11,
		/*Linkage=*/GlobalValue::ExternalLinkage,
		/*Name=*/"StrInstance_Fini_f", mod); // (external, no body)
	func_StrInstance_Fini_f->setCallingConv(CallingConv::C);
	}
	AttributeSet func_StrInstance_Fini_f_PAL;
	{
		SmallVector<AttributeSet, 4> Attrs;
		AttributeSet PAS;
			{
				AttrBuilder B;
				PAS = AttributeSet::get(mod->getContext(), ~0U, B);
			}
		
		Attrs.push_back(PAS);
		func_StrInstance_Fini_f_PAL = AttributeSet::get(mod->getContext(), Attrs);
		
	}
	func_StrInstance_Fini_f->setAttributes(func_StrInstance_Fini_f_PAL);
	
	Function* func_StrInstance_Copy_f = mod->getFunction("StrInstance_Copy_f");
	if (!func_StrInstance_Copy_f) {
	func_StrInstance_Copy_f = Function::Create(
		/*Type=*/FuncTy_11,
		/*Linkage=*/GlobalValue::ExternalLinkage,
		/*Name=*/"StrInstance_Copy_f", mod); // (external, no body)
	func_StrInstance_Copy_f->setCallingConv(CallingConv::C);
	}
	AttributeSet func_StrInstance_Copy_f_PAL;
	{
		SmallVector<AttributeSet, 4> Attrs;
		AttributeSet PAS;
			{
				AttrBuilder B;
				PAS = AttributeSet::get(mod->getContext(), ~0U, B);
			}
		
		Attrs.push_back(PAS);
		func_StrInstance_Copy_f_PAL = AttributeSet::get(mod->getContext(), Attrs);
		
	}
	func_StrInstance_Copy_f->setAttributes(func_StrInstance_Copy_f_PAL);
	
	Function* func_StrInstance_Move_f = mod->getFunction("StrInstance_Move_f");
	if (!func_StrInstance_Move_f) {
	func_StrInstance_Move_f = Function::Create(
		/*Type=*/FuncTy_11,
		/*Linkage=*/GlobalValue::ExternalLinkage,
		/*Name=*/"StrInstance_Move_f", mod); // (external, no body)
	func_StrInstance_Move_f->setCallingConv(CallingConv::C);
	}
	AttributeSet func_StrInstance_Move_f_PAL;
	{
		SmallVector<AttributeSet, 4> Attrs;
		AttributeSet PAS;
			{
				AttrBuilder B;
				PAS = AttributeSet::get(mod->getContext(), ~0U, B);
			}
		
		Attrs.push_back(PAS);
		func_StrInstance_Move_f_PAL = AttributeSet::get(mod->getContext(), Attrs);
		
	}
	func_StrInstance_Move_f->setAttributes(func_StrInstance_Move_f_PAL);
	
	// Global Variable Declarations

	
	GlobalVariable* gvar_array_____C__06ENNEIMBA_String__AA_ = new GlobalVariable(/*Module=*/*mod, 
	/*Type=*/ArrayTy_0,
	/*isConstant=*/true,
	/*Linkage=*/GlobalValue::LinkOnceODRLinkage ,
	/*Initializer=*/0, // has initializer, specified below
	/*Name=*/"\x01??_C@_06ENNEIMBA@String?$AA@");
	gvar_array_____C__06ENNEIMBA_String__AA_->setAlignment(1);
	
	GlobalVariable* gvar_array_____C__01CPLAODJH_S__AA_ = new GlobalVariable(/*Module=*/*mod, 
	/*Type=*/ArrayTy_2,
	/*isConstant=*/true,
	/*Linkage=*/GlobalValue::LinkOnceODRLinkage ,
	/*Initializer=*/0, // has initializer, specified below
	/*Name=*/"\x01??_C@_01CPLAODJH@S?$AA@");
	gvar_array_____C__01CPLAODJH_S__AA_->setAlignment(1);
	
	GlobalVariable* gvar_array_____C__09BGBBGAHE_StringRef__AA_ = new GlobalVariable(/*Module=*/*mod, 
	/*Type=*/ArrayTy_4,
	/*isConstant=*/true,
	/*Linkage=*/GlobalValue::LinkOnceODRLinkage ,
	/*Initializer=*/0, // has initializer, specified below
	/*Name=*/"\x01??_C@_09BGBBGAHE@StringRef?$AA@");
	gvar_array_____C__09BGBBGAHE_StringRef__AA_->setAlignment(1);
	
	GlobalVariable* gvar_array_____C__04LJGNGNMK_ESRF__AA_ = new GlobalVariable(/*Module=*/*mod, 
	/*Type=*/ArrayTy_6,
	/*isConstant=*/true,
	/*Linkage=*/GlobalValue::LinkOnceODRLinkage ,
	/*Initializer=*/0, // has initializer, specified below
	/*Name=*/"\x01??_C@_04LJGNGNMK@ESRF?$AA@");
	gvar_array_____C__04LJGNGNMK_ESRF__AA_->setAlignment(1);
	
	GlobalVariable* gvar_array_g_TypeReflection = new GlobalVariable(/*Module=*/*mod, 
	/*Type=*/ArrayTy_8,
	/*isConstant=*/false,
	/*Linkage=*/GlobalValue::ExternalLinkage,
	/*Initializer=*/0, // has initializer, specified below
	/*Name=*/"g_TypeReflection");
	gvar_array_g_TypeReflection->setAlignment(16);
	
	GlobalVariable* gvar_int32_g_cTypeReflection = new GlobalVariable(/*Module=*/*mod, 
	/*Type=*/IntegerType::get(mod->getContext(), 32),
	/*isConstant=*/false,
	/*Linkage=*/GlobalValue::ExternalLinkage,
	/*Initializer=*/0, // has initializer, specified below
	/*Name=*/"g_cTypeReflection");
	gvar_int32_g_cTypeReflection->setAlignment(4);
	
	// Constant Definitions
	Constant *const_array_14 = ConstantDataArray::getString(mod->getContext(), "String", true);
	Constant *const_array_15 = ConstantDataArray::getString(mod->getContext(), "S", true);
	Constant *const_array_16 = ConstantDataArray::getString(mod->getContext(), "StringRef", true);
	Constant *const_array_17 = ConstantDataArray::getString(mod->getContext(), "ESRF", true);
	std::vector<Constant*> const_array_18_elems;
	std::vector<Constant*> const_struct_19_fields;
	ConstantInt* const_int32_20 = ConstantInt::get(mod->getContext(), APInt(32, StringRef("0"), 10));
	const_struct_19_fields.push_back(const_int32_20);
	ConstantInt* const_int32_21 = ConstantInt::get(mod->getContext(), APInt(32, StringRef("8"), 10));
	const_struct_19_fields.push_back(const_int32_21);
	std::vector<Constant*> const_ptr_22_indices;
	const_ptr_22_indices.push_back(const_int32_20);
	const_ptr_22_indices.push_back(const_int32_20);
	Constant* const_ptr_22 = ConstantExpr::getGetElementPtr(gvar_array_____C__06ENNEIMBA_String__AA_, const_ptr_22_indices);
	const_struct_19_fields.push_back(const_ptr_22);
	std::vector<Constant*> const_ptr_23_indices;
	const_ptr_23_indices.push_back(const_int32_20);
	const_ptr_23_indices.push_back(const_int32_20);
	Constant* const_ptr_23 = ConstantExpr::getGetElementPtr(gvar_array_____C__01CPLAODJH_S__AA_, const_ptr_23_indices);
	const_struct_19_fields.push_back(const_ptr_23);
	const_struct_19_fields.push_back(func_StrInstance_Init_f);
	const_struct_19_fields.push_back(func_StrInstance_Fini_f);
	const_struct_19_fields.push_back(func_StrInstance_Copy_f);
	const_struct_19_fields.push_back(func_StrInstance_Move_f);
	Constant* const_struct_19 = ConstantStruct::get(StructTy_struct_Type_s, const_struct_19_fields);
	const_array_18_elems.push_back(const_struct_19);
	std::vector<Constant*> const_struct_24_fields;
	const_struct_24_fields.push_back(const_int32_20);
	ConstantInt* const_int32_25 = ConstantInt::get(mod->getContext(), APInt(32, StringRef("16"), 10));
	const_struct_24_fields.push_back(const_int32_25);
	std::vector<Constant*> const_ptr_26_indices;
	const_ptr_26_indices.push_back(const_int32_20);
	const_ptr_26_indices.push_back(const_int32_20);
	Constant* const_ptr_26 = ConstantExpr::getGetElementPtr(gvar_array_____C__09BGBBGAHE_StringRef__AA_, const_ptr_26_indices);
	const_struct_24_fields.push_back(const_ptr_26);
	std::vector<Constant*> const_ptr_27_indices;
	const_ptr_27_indices.push_back(const_int32_20);
	const_ptr_27_indices.push_back(const_int32_20);
	Constant* const_ptr_27 = ConstantExpr::getGetElementPtr(gvar_array_____C__04LJGNGNMK_ESRF__AA_, const_ptr_27_indices);
	const_struct_24_fields.push_back(const_ptr_27);
	ConstantPointerNull* const_ptr_28 = ConstantPointerNull::get(PointerTy_10);
	const_struct_24_fields.push_back(const_ptr_28);
	const_struct_24_fields.push_back(const_ptr_28);
	const_struct_24_fields.push_back(const_ptr_28);
	const_struct_24_fields.push_back(const_ptr_28);
	Constant* const_struct_24 = ConstantStruct::get(StructTy_struct_Type_s, const_struct_24_fields);
	const_array_18_elems.push_back(const_struct_24);
	Constant* const_array_18 = ConstantArray::get(ArrayTy_8, const_array_18_elems);
	ConstantInt* const_int32_29 = ConstantInt::get(mod->getContext(), APInt(32, StringRef("2"), 10));
	
	// Global Variable Definitions
	gvar_array_____C__06ENNEIMBA_String__AA_->setInitializer(const_array_14);
	gvar_array_____C__01CPLAODJH_S__AA_->setInitializer(const_array_15);
	gvar_array_____C__09BGBBGAHE_StringRef__AA_->setInitializer(const_array_16);
	gvar_array_____C__04LJGNGNMK_ESRF__AA_->setInitializer(const_array_17);
	gvar_array_g_TypeReflection->setInitializer(const_array_18);
	gvar_int32_g_cTypeReflection->setInitializer(const_int32_29);
	
	// Function Definitions
	
	return mod;
}
