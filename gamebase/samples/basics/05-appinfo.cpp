#define GB_INIT 1
#include <gamebase.h>

/*
 * This example demonstrates what AppDir(), AppFile(), and LaunchDir() do.
 */
void GBMain() {
	gbPrint(gbF("App Dir   : %s", gbAppDir()));
	gbPrint(gbF("App File  : %s", gbAppFile()));
	gbPrint(gbF("Launch Dir: %s", gbLaunchDir()));
}
