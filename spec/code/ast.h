#ifndef AST_H
#define AST_H

#include "local.h"

typedef struct ast_s {
	struct ast_s *root;
	struct ast_s *prnt, *head, *tail, *prev, *next;

	char *lexan;
	int token;
	int line;
} ast_t;

ast_t *NewAST(ast_t *prnt, const char *lexan, int token, int line);

ast_t *DeleteAST(ast_t *ast);

ast_t *ASTRoot(ast_t *ast);
ast_t *ASTParent(ast_t *ast);
ast_t *FirstAST(ast_t *ast);
ast_t *LastAST(ast_t *ast);
ast_t *ASTBefore(ast_t *ast);
ast_t *ASTAfter(ast_t *ast);

const char *ASTLexan(ast_t *ast);
int ASTToken(ast_t *ast);
const char *ASTFile(ast_t *ast);
int ASTLine(ast_t *ast);

bool_t IsASTDescendant(ast_t *ast, ast_t *prnt);
void SetASTParent(ast_t *ast, ast_t *prnt);

void ASTReportFuncV(ast_t *ast, const char *type, const char *func,
const char *format, va_list args);
void ASTReportFunc(ast_t *ast, const char *type, const char *func,
const char *format, ...);

void ASTWarnFunc(ast_t *ast, const char *func, const char *format, ...);
void ASTErrorFunc(ast_t *ast, const char *func, const char *format, ...);

void ASTReportV(ast_t *ast, const char *type, const char *format, va_list args);
void ASTReport(ast_t *ast, const char *type, const char *format, ...);

void ASTWarn(ast_t *ast, const char *format, ...);
void ASTError(ast_t *ast, const char *format, ...);

#endif
