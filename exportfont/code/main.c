#include <SOIL/SOIL.h>
#include <GL/gl.h>
#include <stdio.h>
#include <stdlib.h>

void LoadFont(const char *targa) {
	unsigned char *data;
	const char *s;
	int width, height, channels;
	int x, y;

	width = 0;
	height = 0;
	channels = 0;

	data = SOIL_load_image(targa, &width, &height, &channels, SOIL_LOAD_L);
	if (!data) {
		fprintf(stderr, "ERROR: Failed to load \"%s\"\n", targa);
		return;
	}

	printf("/*\n * \"%s\"\n * width=%i\n * height=%i\n * channels=%i\n */\n\n",
		   targa, width, height, channels);

	printf("unsigned char imagedata[] = {\n\t");
	for(y=0; y<height; y++) {
		for(x=0; x<width; x++) {
			if (y==height - 1 && x==width - 1)
				s = "\n";
			else if((x + 1) % 8 != 0)
				s = ", ";
			else
				s = ",\n\t";

			printf("0x%.2X%s", (unsigned int)data[y*width + x], s);
		}
	}
	printf("};\n");

	SOIL_free_image_data(data);
}

int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "exportfont \"targa name\"\n");
		return EXIT_FAILURE;
	}

	LoadFont(argv[1]);
	return 0;
}
