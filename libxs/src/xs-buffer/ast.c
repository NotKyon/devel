#include <xs/ast.h>
#include <xs/memory.h>
#include <xs/string.h>

struct xs_ast_s {
	struct xs_ast_s *root;
	struct xs_ast_s *prnt, *head, *tail, *prev, *next;

	char *lexan;
	int token;
};

xs_ast_t XsNewAST(xs_ast_t prnt, const char *lexan, int token) {
	xs_ast_t ast;

	ast = (xs_ast_t)XsMemory((void *)0, sizeof(*ast));

	ast->root = prnt ? (prnt->root ? prnt->root : prnt) : (xs_ast_t)0;
	ast->prnt = prnt;
	ast->head = (xs_ast_t)0;
	ast->tail = (xs_ast_t)0;
	ast->prev = (xs_ast_t)0;
	ast->next = (xs_ast_t)0;

	if (prnt) {
		if ((ast->prev = prnt->tail) != (xs_ast_t)0)
			prnt->tail->next = ast;
		else
			prnt->head = ast;
		prnt->tail = ast;
	}

	ast->lexan = XsDuplicate(lexan);
	ast->token = token;

	return ast;
}

xs_ast_t XsDeleteAST(xs_ast_t ast) {
	if (!ast)
		return (xs_ast_t)0;

	while(ast->head)
		XsDeleteAST(ast->head);

	if (ast->prev)
		ast->prev->next = ast->next;
	if (ast->next)
		ast->next->prev = ast->prev;

	if (ast->prnt) {
		if (ast->prnt->head==ast)
			ast->prnt->head = ast->next;
		if (ast->prnt->tail==ast)
			ast->prnt->tail = ast->prev;
	}

	XsMemory((void *)ast->lexan, 0);

	return (xs_ast_t)XsMemory((void *)ast, 0);
}

xs_ast_t XsASTRoot(xs_ast_t ast) {
	return ast->root;
}
xs_ast_t XsASTParent(xs_ast_t ast) {
	return ast->prnt;
}
xs_ast_t XsFirstAST(xs_ast_t ast) {
	return ast->head;
}
xs_ast_t XsLastAST(xs_ast_t ast) {
	return ast->tail;
}
xs_ast_t XsASTBefore(xs_ast_t ast) {
	return ast->prev;
}
xs_ast_t XsASTAfter(xs_ast_t ast) {
	return ast->next;
}

const char *XsASTLexan(xs_ast_t ast) {
	return ast->lexan;
}
int XsASTToken(xs_ast_t ast) {
	return ast->token;
}
const char *XsASTFile(xs_ast_t ast) {
	xs_ast_t root;

	root = XsASTRoot(ast);
	if (!root)
		root = ast;

	return XsASTLexan(root);
}
