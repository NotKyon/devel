#include "header.h"

#include <stdio.h>
#include <stdlib.h>

extern int   GetValue1I();
extern float GetValue1F();
extern int   GetValue2I();
extern float GetValue2F();

int main() {
	TMyClass<int> a, b;
	TMyClass<float> c, d;

	printf("GetValue::int     = %i, %i\n", GetValue1I(), GetValue2I());
	printf("GetValue::float   = %g, %g\n", GetValue1F(), GetValue2F());

	printf("MyClassI::g_count = %i\n", TMyClass<int>::g_count); //should be 3
	printf("MyClassF::g_count = %i\n", TMyClass<float>::g_count); //should be 4

	return EXIT_SUCCESS;
}
