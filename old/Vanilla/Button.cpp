// includes
#include <Axia/Vanilla/Vanilla.h>
#include <Axia/Vanilla/Internal.h>

// button state
enum EButtonState
{
	EButtonState_Idle = 0,
	EButtonState_Hover = 1,
	EButtonState_Pressed = 2,
	EButtonState_Held = 3
};

// button type
enum EButtonType
{
	EButtonType_Push = 0,
	EButtonType_DefPush = 1,
	EButtonType_CheckBox = 2,
	EButtonType_RadioBox = 3
};

// button structure
struct BUTTON
{
	struct
	{
		EButtonState						state:2;
		EButtonState						prevState:2;
		EButtonState						realState:2;
		EButtonType							type:2;
	}										flags;
};

// button procedure
LRESULT CALLBACK ButtonMsgProc(HWND hwndRef, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	// declarations
	TRACKMOUSEEVENT	tme;
	//PAINTSTRUCT		ps;
	BUTTON			*pButton;
	HWND			hwndParentRef;
	//HDC				hdcRef;

	// core create
	if (uiMsg==WM_NCCREATE)
	{
		if (!(pButton = (BUTTON *)malloc(sizeof(BUTTON))))
			return 0;

		pButton->flags.state = EButtonState_Idle;
		pButton->flags.prevState = EButtonState_Idle;
		pButton->flags.realState = EButtonState_Idle;
		pButton->flags.type = EButtonType_Push;

		SetWindowLongPtrA(hwndRef, 0, (LONG_PTR)pButton);

		return DefWindowProcA(hwndRef, uiMsg, wParam, lParam);
	}

	// get the button
	pButton = (BUTTON *)GetWindowLongPtrA(hwndRef, 0);

	// check the message
	switch(uiMsg)
	{
		case WM_NCDESTROY:
			if (pButton)
			{
				SetWindowLongPtrA(hwndRef, 0, 0);
				free((void *)pButton);
			}
			break;

		case WM_LBUTTONDOWN:
			pButton->flags.prevState = pButton->flags.state;
			pButton->flags.state = EButtonState_Pressed;
			InvalidateRect(hwndRef, 0, 0);
			break;

		case WM_LBUTTONUP:
			pButton->flags.prevState = pButton->flags.state;
			pButton->flags.state = EButtonState_Hover;
			hwndParentRef = GetParent(hwndRef);
			break;

		case WM_MOUSEMOVE:
			tme.cbSize		= sizeof(tme);
			tme.dwFlags		= TME_LEAVE;
			tme.hwndTrack	= hwndRef;
			tme.dwHoverTime	= 0;
			TrackMouseEvent(&tme);
			break;
	}

	// done
	return DefWindowProc(hwndRef, uiMsg, wParam, lParam);
}

// initialize
int InitButton(void)
{
	return 0;
}

// deinitialize
void DeinitButton(void)
{
}
