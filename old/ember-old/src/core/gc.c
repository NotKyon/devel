#include <string.h>
#include <stdlib.h>
#include <ember/gc.h>

struct stGCPool {
	/* properties */
	size_t element_size, byte_size;
	gc_init_t init;
	gc_deinit_t deinit;

	/* lists */
	struct stGCObj *alloc_head, *alloc_tail;
	struct stGCObj *avail_head, *avail_tail;

	/* link */
	struct stGCPool *prev, *next;
};
struct stGCObj {
	size_t reference_count;
	void *self;

	struct stGCPool *pool;

	struct stGCObj *prev, *next;
};

static struct stGCPool *g_gc_head = ( struct stGCPool * )0,
                       *g_gc_tail = ( struct stGCPool * )0;

static void *gc_self( void *p )
{
	union {
		void *p;
		size_t l;
	} h;

	h.p = p;
	h.l += sizeof( struct stGCObj );

	return h.p;
}
static struct stGCObj *gc_obj( void *self )
{
	union {
		void *p;
		size_t l;
	} h;

	h.p = self;
	h.l -= sizeof( struct stGCObj );

	return h.p;
}

gc_pool_t gc_register_pool( size_t element_size, gc_init_t init,
    gc_deinit_t deinit )
{
	struct stGCPool *pool;

	if( !( pool = ( struct stGCPool * )malloc( sizeof( struct stGCPool ) ) ) )
		return ( gc_pool_t )0;

	pool->element_size = element_size;
	pool->byte_size    = element_size + sizeof( struct stGCObj );

	pool->init   = init;
	pool->deinit = deinit;

	pool->alloc_head = ( struct stGCObj * )0;
	pool->alloc_tail = ( struct stGCObj * )0;
	pool->avail_head = ( struct stGCObj * )0;
	pool->avail_tail = ( struct stGCObj * )0;

	pool->next = ( struct stGCPool * )0;
	if( ( pool->prev = g_gc_tail ) != ( struct stGCPool * )0 )
		g_gc_tail->next = pool;
	else
		g_gc_head = pool;
	g_gc_tail     = pool;

	return ( gc_pool_t )pool;
}

void gc_unregister_pool( gc_pool_t pool )
{
	while( pool->alloc_head )
		gc_release( pool->alloc_head->self );

	gc_collect_ex( pool );

	if( pool->prev )
		pool->prev->next = pool->next;
	if( pool->next )
		pool->next->prev = pool->prev;

	if( g_gc_head == pool )
		g_gc_head = pool->next;
	if( g_gc_tail == pool )
		g_gc_tail = pool->prev;

	free( ( void * )pool );
}

void gc_collect_ex( gc_pool_t pool )
{
	struct stGCObj *obj, *next;

	obj = pool->avail_head;
	while( obj ) {
		next = obj->next;

		free( ( void * )obj );
		obj = next;
	}

	pool->avail_head = ( struct stGCObj * )0;
	pool->avail_tail = ( struct stGCObj * )0;
}

void gc_collect()
{
	struct stGCPool *pool;

	for( pool = g_gc_head; pool; pool = pool->next )
		gc_collect_ex( pool );
}

void *gc_alloc( gc_pool_t pool )
{
	struct stGCObj *obj;

	if( ( obj = pool->avail_head ) != ( struct stGCObj * )0 ) {
		if( obj->prev )
			obj->prev->next = obj->next;
		if( obj->next )
			obj->next->prev = obj->prev;

		if( pool->avail_head == obj )
			pool->avail_head = obj->next;
		if( pool->avail_tail == obj )
			pool->avail_tail = obj->prev;
	} else {
		if( !( obj = ( struct stGCObj * )malloc( pool->byte_size ) ) )
			return ( void * )0;
	}

	obj->reference_count = 1;
	obj->self            = gc_self( ( void * )obj );
	obj->pool            = pool;

	obj->next = ( struct stGCObj * )0;
	if( ( obj->prev = pool->alloc_tail ) != ( struct stGCObj * )0 )
		pool->alloc_tail->next = obj;
	else
		pool->alloc_head = obj;
	pool->alloc_tail     = obj;

	if( pool->init )
		if( !pool->init( obj->self ) ) {
			gc_dealloc( obj->self );
			return ( void * )0;
		}

	return obj->self;
}

void gc_retain( void *obj )
{
	struct stGCObj *p;

	p = gc_obj( obj );
	p->reference_count++;
}
void gc_release( void *obj )
{
	struct stGCObj *p;

	p = gc_obj( obj );
	if( --p->reference_count == 0 )
		gc_dealloc( obj );
}

void gc_dealloc( void *obj )
{
	struct stGCObj *p;

	p = gc_obj( obj );

	if( p->pool->deinit )
		p->pool->deinit( obj );

	if( p->prev )
		p->prev->next = p->next;
	if( p->next )
		p->next->prev = p->prev;

	if( p->pool->alloc_head == p )
		p->pool->alloc_head = p->next;
	if( p->pool->alloc_tail == p )
		p->pool->alloc_tail = p->prev;

	p->next = ( struct stGCObj * )0;
	if( ( p->prev = p->pool->avail_tail ) != ( struct stGCObj * )0 )
		p->pool->avail_tail->next = p;
	else
		p->pool->avail_head = p;
	p->pool->avail_tail     = p;
}
