#ifndef AXTEK_BASE_MACROS_H
#define AXTEK_BASE_MACROS_H

#if _MSC_VER > 1000
# pragma once
#endif

#define __AX_NS_INIT__ namespace ax {
#define __AX_NS_FINI__ }

//
//	The compiler doesn't always choose the best inlining mechanism for a
//	particular branch of code. In these cases, you want to tell it a function
//	absolutely needs to be inlined.
//
#if __GNUC__ || __clang__
# ifndef __forceinline
#  define __forceinline inline __attribute__((always_inline))
# endif
#endif

//
//	Data sometimes needs to be aligned to a specific boundary, either for
//	performance reasons or to keep the processor from faulting. This macro
//	allows you to accomplish that.
//
#ifndef __align
# if _MSC_VER || __INTEL_COMPILER
#  define __align(x) __declspec(align(x))
# else
#  define __align(x) __attribute__((aligned(x)))
# endif
#endif

//
//	Some processors have branch prediction for prefetching the appropriate code
//	path. For some operations (e.g., automatic initialization) having specific
//	branch prediction macros in there can help.
//
//		if unlikely (!didInit)
//			do_init();
//
//		do_func();
//
//	Likewise, error checking should be considered unlikely to fault. (Even if
//	there's a misprediction here, its latency won't be noticed at all due to the
//	larger amount of latency errors tend to introduce.)
//
//		int safe_divide(int numerator, int denominator) {
//			if unlikely (!denominator)
//				return handle_error(DIVIDE_BY_ZERO);
//
//			return numerator/denominator;
//		}
//
#ifndef __likely
# if __GNUC__ || __clang__ || __INTEL_COMPILER
#  define __likely(x) (__builtin_expect(!!(x), 1))
#  define __unlikely(x) (__builtin_expect(!!(x), 0))
# else
#  define __likely(x) (x)
#  define __unlikely(x) (x)
# endif
#endif

#endif
