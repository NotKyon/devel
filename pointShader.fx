// EFFECT STATES
// http://msdn.microsoft.com/en-us/library/windows/desktop/bb173347(v=vs.85).aspx

struct VertIn {
	float3 pos:POSITION;
	float4 col:COLOR0;
	//float2 uv:TEXCOORD0;
};

struct VertOut_FragIn {
	float4 pos:POSITION;
	float4 col:COLOR0;
};

float4x4 modelViewProj : WORLDVIEWPROJECTION;

VertOut_FragIn vertexMain(in VertIn vi) {
	VertOut_FragIn vofi;

	vofi.pos = mul( float4( vi.pos.xyz, 1.0 ), modelViewProj );
	vofi.col = vi.col;

	return vofi;
}
float4 fragmentMain(in VertOut_FragIn vofi): COLOR0 {
	return vofi.col;
}

technique mainTech {
	pass mainPass {
		FillMode = Point;
		PointSpriteEnable = true;
		PointSize = 8.0;
		VertexShader = compile vs_2_0 vertexMain();
		PixelShader = compile ps_2_0 fragmentMain();
	}
}
