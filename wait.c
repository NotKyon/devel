#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

void wait(unsigned int numSeconds) {
	Sleep( numSeconds*1000 );
}
unsigned int getMultiplier(char code) {
	switch( code ) {
	case 's':
	case 'S':
		return 1;
	case 'm':
	case 'M':
		return 60;
	case 'h':
	case 'H':
		return 60*60;
	case 'd':
	case 'D':
		return 24*60*60;
	case 'w':
	case 'W':
		return 7*24*60*60;
	default:
		break;
	}

	return 0;
}

int main(int argc, char **argv) {
	int i;

	unsigned int totalTime = 0;

	for( i=1; i<argc; i++ ) {
		char *s;
		int num = 0;

		for( s=argv[ i ]; *s!='\0'; ++s ) {
			if( *s<'0' || *s>'9' ) {
				break;
			}

			num = num*10 + ( (+*s) - '0' );
		}

		totalTime += num*getMultiplier( *s );
	}

	printf( "Waiting for %u second%s\n", totalTime, totalTime==1 ? "" : "s" );
	wait( totalTime );

	for( i=1; i<10; i++ ) {
		printf( "********************************\n" );
		printf( "********************************\n" );
		printf( "********************************\n" );
		printf( "********************************\n" );
		printf( "********************************\n" );
		printf( "================================\n" );
		printf( "--- HEY!! HEY!!  HEY!! HEY!! ---\n" );
		printf( " - Your timer's time is over! -\n" );
		printf( "--- HEY!! HEY!!  HEY!! HEY!! ---\n" );
		printf( "================================\n" );
		printf( "********************************\n" );
		printf( "********************************\n" );
		printf( "********************************\n" );
		printf( "********************************\n" );
		printf( "********************************\n" );
		printf( "\n\n\n" );
		fflush( stdout );
	}

	printf( "Timer is over.\n" );

	{
		time_t rawtime;
		struct tm *timeinfo;

		time( &rawtime );
		timeinfo = localtime( &rawtime );
		printf( "Current time: %s", asctime( timeinfo ) );
	}

	return EXIT_SUCCESS;
}
