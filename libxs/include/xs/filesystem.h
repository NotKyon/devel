#ifndef XS_FILESYSTEM_H
#define XS_FILESYSTEM_H

#include "basetypes.h"

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#ifndef PATH_MAX
# define PATH_MAX 256
#endif

typedef char xs_path_t[PATH_MAX + 1];

/*
 * TODO: Document these
 */
char *XsGetCWD(char *cwd, xs_size_t n);
int XsEnterDir(const char *path);
int XsExitDir();
int XsIsFile(const char *path);
int XsIsDir(const char *path);
int XsMakeDirs(const char *dirs);
char *XsRealPath(const char *filename, char *resolvedname, xs_size_t maxn);
const char *XsGetDir(char *buf, xs_size_t n, const char *filename);
void XsExtSubst(char *dst, xs_size_t dstn, const char *src, const char *ext);

#endif
