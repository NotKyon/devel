#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
	int w = GetSystemMetrics( SM_CXSCREEN );
	int h = GetSystemMetrics( SM_CYSCREEN );

	printf( "%i, %i\n", w, h );

	return EXIT_SUCCESS;
}
