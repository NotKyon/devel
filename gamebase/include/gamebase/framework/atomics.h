#ifndef GB_FRAMEWORK_ATOMICS_H
#define GB_FRAMEWORK_ATOMICS_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

/* -------------------------------------------------------------------------- */
/* --intrinsics */

#if _MSC_VER
# include <intrin.h>

# pragma intrinsic(_InterlockedIncrement)
# pragma intrinsic(_InterlockedDecrement)
# pragma intrinsic(_InterlockedExchange)
# pragma intrinsic(_InterlockedCompareExchange)
# pragma intrinsic(_InterlockedAnd)
# pragma intrinsic(_InterlockedOr)
# pragma intrinsic(_InterlockedXor)

# if _WIN64
#  pragma intrinsic(_InterlockedIncrement64)
#  pragma intrinsic(_InterlockedDecrement64)
#  pragma intrinsic(_InterlockedExchange64)
#  pragma intrinsic(_InterlockedCompareExchange64)
#  pragma intrinsic(_InterlockedAnd64)
#  pragma intrinsic(_InterlockedOr64)
#  pragma intrinsic(_InterlockedXor64)
# else
#  define _InterlockedIncrement64 InterlockedIncrement64
#  define _InterlockedDecrement64 InterlockedDecrement64
#  define _InterlockedExchange64 InterlockedExchange64
#  define _InterlockedCompareExchange64 InterlockedCompareExchange64
#  define _InterlockedAnd64 InterlockedAnd64
#  define _InterlockedOr64 InterlockedOr64
#  define _InterlockedXor64 InterlockedXor64
# endif

# if 0
#  pragma intrinsic(_InterlockedExchangePointer)
#  pragma intrinsic(_InterlockedCompareExchangePointer)
# else
#  define _InterlockedExchangePointer InterlockedExchangePointer
#  define _InterlockedCompareExchangePointer InterlockedCompareExchangePointer
# endif
#endif

/* -------------------------------------------------------------------------- */
/* --32 */

GB_INLINE gb_u32_t gbAtomicIncrement32(volatile gb_u32_t *x) {
#if _MSC_VER
	return _InterlockedIncrement((volatile LONG *)x);
#else
	return __sync_fetch_and_add(x, 1);
#endif
}
GB_INLINE gb_u32_t gbAtomicDecrement32(volatile gb_u32_t *x) {
#if _MSC_VER
	return _InterlockedDecrement((volatile LONG *)x);
#else
	return __sync_fetch_and_sub(x, 1);
#endif
}
GB_INLINE gb_u32_t gbAtomicExchange32(volatile gb_u32_t *x, gb_u32_t y) {
#if _MSC_VER
	return _InterlockedExchange((volatile LONG *)x, y);
#else
	return __sync_lock_test_and_set(x, y);
#endif
}
GB_INLINE gb_u32_t gbAtomicExchangeIfEqual32(volatile gb_u32_t *x, gb_u32_t y,
gb_u32_t z) {
#if _MSC_VER
	/* dst, new, old */
	return _InterlockedCompareExchange((volatile LONG *)x, y, z);
#else
	/* dst, old, new */
	return __sync_val_compare_and_swap(x, z, y);
#endif
}

GB_INLINE gb_u32_t gbAtomicAnd32(volatile gb_u32_t *x, gb_u32_t y) {
#if _MSC_VER
# if 0
	gb_u32_t w, s;

	w = *x;
	s = w&y;

	do {
	} while(_InterlockedExchange32((volatile LONG *)x, s)!=s);

	return w;
# else
	return _InterlockedAnd((volatile LONG *)x, y);
# endif
#else
	return __sync_fetch_and_and(x, y);
#endif
}
GB_INLINE gb_u32_t gbAtomicOr32(volatile gb_u32_t *x, gb_u32_t y) {
#if _MSC_VER
# if 0
	gb_u32_t w, s;

	w = *x;
	s = w|y;

	do {
	} while(_InterlockedExchange((volatile LONG *)x, s)!=s);

	return w;
# else
	return _InterlockedOr((volatile LONG *)x, y);
# endif
#else
	return __sync_fetch_and_or(x, y);
#endif
}
GB_INLINE gb_u32_t gbAtomicXor32(volatile gb_u32_t *x, gb_u32_t y) {
#if _MSC_VER
# if 0
	gb_u32_t w, s;

	w = *x;
	s = w^y;

	do {
	} while(_InterlockedExchange((volatile LONG *)x, s)!=s);

	return w;
# else
	return _InterlockedXor((volatile LONG *)x, y);
# endif
#else
	return __sync_fetch_and_xor(x, y);
#endif
}

GB_INLINE gb_u32_t gbAtomicRead32(volatile gb_u32_t *x) {
	/* return gbAtomicOr(x, 0); */
	return gbAtomicExchangeIfEqual32(x, 0, 0);
}

GB_INLINE gb_u32_t gbAtomicSpinEqual32(volatile gb_u32_t *x, gb_u32_t y) {
	gb_u32_t r;

	do {
		r = gbAtomicRead32(x);
	} while(r==y);

	return r;
}
GB_INLINE gb_u32_t gbAtomicSpinNotEqual32(volatile gb_u32_t *x, gb_u32_t y) {
	gb_u32_t r;

	do {
		r = gbAtomicRead32(x);
	} while(r!=y);

	return r;
}

/* -------------------------------------------------------------------------- */
/* --64 */

GB_INLINE gb_u64_t gbAtomicIncrement64(volatile gb_u64_t *x) {
#if _MSC_VER
	return _InterlockedIncrement64((volatile LONGLONG *)x);
#else
	return __sync_fetch_and_add(x, 1);
#endif
}
GB_INLINE gb_u64_t gbAtomicDecrement64(volatile gb_u64_t *x) {
#if _MSC_VER
	return _InterlockedDecrement64((volatile LONGLONG *)x);
#else
	return __sync_fetch_and_sub(x, 1);
#endif
}
GB_INLINE gb_u64_t gbAtomicExchange64(volatile gb_u64_t *x, gb_u64_t y) {
#if _MSC_VER
	return _InterlockedExchange64((volatile LONGLONG *)x, y);
#else
	return __sync_lock_test_and_set(x, y);
#endif
}
GB_INLINE gb_u64_t gbAtomicExchangeIfEqual64(volatile gb_u64_t *x, gb_u64_t y,
gb_u64_t z) {
#if _MSC_VER
	/* dst, new, old */
	return _InterlockedCompareExchange64((volatile LONGLONG *)x, y, z);
#else
	/* dst, old, new */
	return __sync_val_compare_and_swap(x, z, y);
#endif
}

GB_INLINE gb_u64_t gbAtomicAnd64(volatile gb_u64_t *x, gb_u64_t y) {
#if _MSC_VER
	return _InterlockedAnd64((volatile LONGLONG *)x, y);
#else
	return __sync_fetch_and_and(x, y);
#endif
}
GB_INLINE gb_u64_t gbAtomicOr64(volatile gb_u64_t *x, gb_u64_t y) {
#if _MSC_VER
	return _InterlockedOr64((volatile LONGLONG *)x, y);
#else
	return __sync_fetch_and_or(x, y);
#endif
}
GB_INLINE gb_u64_t gbAtomicXor64(volatile gb_u64_t *x, gb_u64_t y) {
#if _MSC_VER
	return _InterlockedXor64((volatile LONGLONG *)x, y);
#else
	return __sync_fetch_and_xor(x, y);
#endif
}

GB_INLINE gb_u64_t gbAtomicRead64(volatile gb_u64_t *x) {
	/* return gbAtomicOr(x, 0); */
	return gbAtomicExchangeIfEqual64(x, 0, 0);
}

GB_INLINE gb_u64_t gbAtomicSpinEqual64(volatile gb_u64_t *x, gb_u64_t y) {
	gb_u64_t r;

	do {
		r = gbAtomicRead64(x);
	} while(r==y);

	return r;
}
GB_INLINE gb_u64_t gbAtomicSpinNotEqual64(volatile gb_u64_t *x, gb_u64_t y) {
	gb_u64_t r;

	do {
		r = gbAtomicRead64(x);
	} while(r!=y);

	return r;
}

/* -------------------------------------------------------------------------- */
/* --ptr */

#if __cplusplus
template<typename T>
GB_INLINE T *gbAtomicExchangePtr(T *volatile *x, T *y) {
#if _MSC_VER
	return _InterlockedExchangePointer((void *volatile *)x, (void *)y);
#else
	return __sync_lock_test_and_set(x, y);
#endif
}
template<typename T>
GB_INLINE T *gbAtomicExchangeIfEqualPtr(T *volatile *x, T *y, T *z) {
#if _MSC_VER
	/* dst, new, old */
	return _InterlockedCompareExchangePointer((void *volatile *)x,
		(void *)y, (void *)z);
#else
	/* dst, old, new */
	return __sync_val_compare_and_swap(x, z, y);
#endif
}

template<typename T>
GB_INLINE T *gbAtomicReadPtr(T *volatile *x) {
#if _WIN64 || __x86_64__
	return (T *)gbAtomicRead64((volatile gb_u64_t *)x);
#elif _WIN32 || __x86__
	return (T *)gbAtomicRead32((volatile gb_u32_t *)x);
#else
	if (sizeof(T *)==8)
		return (T *)gbAtomicRead64((volatile gb_u64_t *)x);

	return (T *)gbAtomicRead32((volatile gb_u32_t *)x);
#endif
}

template<typename T>
GB_INLINE T *gbAtomicSpinEqualPtr(T *volatile *x, T *y) {
	T *r;

	do {
		r = gbAtomicReadPtr(x);
	} while(r==y);

	return r;
}
template<typename T>
GB_INLINE T *gbAtomicSpinNotEqualPtr(T *volatile *x, T *y) {
	T *r;

	do {
		r = gbAtomicReadPtr(x);
	} while(r!=y);

	return r;
}
#else
GB_INLINE void *gbAtomicExchangePtr(void *volatile *x, void *y) {
#if _MSC_VER
	return _InterlockedExchangePointer((void *volatile *)x, (void *)y);
#else
	return __sync_lock_test_and_set(x, y);
#endif
}
GB_INLINE void *gbAtomicExchangeIfEqualPtr(void *volatile *x, void *y, void *z) {
#if _MSC_VER
	/* dst, new, old */
	return _InterlockedCompareExchangePointer((void *volatile *)x,
		(void *)y, (void *)z);
#else
	/* dst, old, new */
	return __sync_val_compare_and_swap(x, z, y);
#endif
}

GB_INLINE void *gbAtomicReadPtr(void *volatile *x) {
#if _WIN64 || __x86_64__
	return (void *)gbAtomicRead64((volatile gb_u64_t *)x);
#elif _WIN32 || __x86__
	return (void *)gbAtomicRead32((volatile gb_u32_t *)x);
#else
	if (sizeof(void *)==8)
		return (void *)gbAtomicRead64((volatile gb_u64_t *)x);

	return (void *)gbAtomicRead32((volatile gb_u32_t *)x);
#endif
}

GB_INLINE void *gbAtomicSpinEqualPtr(void *volatile *x, void *y) {
	void *r;

	do {
		r = gbAtomicReadPtr(x);
	} while(r==y);

	return r;
}
GB_INLINE void *gbAtomicSpinNotEqualPtr(void *volatile *x, void *y) {
	void *r;

	do {
		r = gbAtomicReadPtr(x);
	} while(r!=y);

	return r;
}
#endif

#endif
