﻿#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

typedef uint64_t uint64;

static uint64 GetMSD( uint64 x )
{
	while( x >= 10 ) {
		x /= 10;
	}

	return x;
}
static uint64 RemoveMSD( uint64 x )
{
	uint64 v = 1;
	uint64 y = x;

	do {
		v *= 10;
		y /= 10;
	} while( y >= 10 );

	return x - v*y;
}
static uint64 AddMSD( uint64 x, unsigned msd )
{
	uint64 y = x;
	
	uint64 n = 1;
	while( y > 0 ) {
		y /= 10;
		n *= 10;
	}

	y = x + n*msd;
	if( y < x ) {
		return x/10 + ( n/10 )*msd;
	}

	return y;
}

bool SciNot( uint64 &n, uint64 &ndot, int e )
{
	if( e > 0 ) {
		while( e-- > 0 ) {
			// overflow
			if( n*10 + 9 < n ) {
				return false;
			}

			n = n*10 + GetMSD( ndot );
			ndot = RemoveMSD( ndot );
		}
	} else {
		while( e++ < 0 ) {
			ndot = AddMSD( ndot, n%10 );
			n /= 10;
		}
	}

	return true;
}

void Print( uint64 n, uint64 ndot )
{
	printf( "%u.%u\n", ( unsigned )n, ( unsigned )ndot );
}

void Test( uint64 n, uint64 ndot, int e )
{
	Print( n, ndot );
	if( !SciNot( n, ndot, e ) ) {
		printf( "(Overflow)\n" );
	}
	Print( n, ndot );
	printf( "\n" );
}

void TestGetMSD( uint64 x )
{
	printf( "GetMSD( %u ) -> %u\n", ( unsigned )x, ( unsigned )GetMSD( x ) );
}
void TestRemoveMSD( uint64 x )
{
	printf( "RemoveMSD( %u ) -> %u\n", ( unsigned )x, ( unsigned )RemoveMSD( x ) );
}
void TestAddMSD( uint64 x, unsigned msd )
{
	printf( "AddMSD( %u, %u ) -> %u\n", ( unsigned )x, msd, ( unsigned )AddMSD( x, msd ) );
}

int main()
{
	TestGetMSD( 1234 );
	TestGetMSD( 4321 );
	TestGetMSD( 9876 );
	TestRemoveMSD( 1234 );
	TestRemoveMSD( 4321 );
	TestRemoveMSD( 9876 );
	TestAddMSD( 1234, 5 );
	TestAddMSD( 4321, 5 );
	TestAddMSD( 9876, 5 );

	printf( "\n" );
	
	Test( 123, 456, -1 );
	Test( 123, 456,  1 );

	Test( 0, 123, -1 ); // Problem: Leading zeros not possible
	Test( 0, 123,  1 );

	return EXIT_SUCCESS;
}
