#include <gamebase.h>

#if _WIN32
# include <stdio.h>
# include <malloc.h>

# define ALIGNMENT 16

# if __GNUC__
extern void *_aligned_malloc(size_t n, size_t a);
extern void *_aligned_realloc(void *p, size_t n, size_t a);
extern void _aligned_free(void *p);
# endif

# define x_malloc(n) _aligned_malloc((n),ALIGNMENT)
# define x_realloc(p,n) _aligned_realloc((p),(n),ALIGNMENT)
# define x_free(p) _aligned_free((p))
#else
# define x_malloc(n) malloc((n))
# define x_realloc(p,n) realloc((p),(n))
# define x_free(p) free((p))
#endif

#include <stdlib.h>
#include <string.h>

void *gbMemAlloc(size_t n) {
	void *p;

	if (!n)
		return (void *)0;

	p = x_malloc(n);
	if (!p)
		gbErrorExit("Failed to allocate memory");

	return p;
}
void *gbMemFree(void *p) {
	if (!p)
		return (void *)0;

	x_free(p);
	return (void *)0;
}
void *gbMemExtend(void *p, size_t n) {
	if (!p)
		return gbMemAlloc(n);

	if (!n)
		return gbMemFree(p);

	p = x_realloc(p, n);
	if (!p)
		gbErrorExit("Failed to reallocate memory");

	return p;
}

void gbMemClear(void *p, size_t n) {
	memset(p, 0, n);
}
void gbMemFill(void *p, char s, size_t n) {
	memset(p, s, n);
}
void gbMemCopy(void *dst, const void *src, size_t n) {
	memcpy(dst, src, n);
}
void gbMemMove(void *dst, const void *src, size_t n) {
	memmove(dst, src, n);
}

void *gbMemAllocZero(size_t n) {
	void *p;

	p = gbMemAlloc(n);
	if (!p)
		return (void *)0;

	memset(p, 0, n);

	return p;
}

/* -------------------------------------------------------------------------- */

char *gbDuplicateN(const char *src, size_t srcn) {
	size_t l;
	char *p;

	if (!src)
		return (char *)0;

	l = srcn ? srcn : strlen(src);

	p = (char *)gbMemAlloc(l + 1);
	memcpy((void *)p, (const void *)src, l);
	p[l] = 0;

	return p;
}
char *gbDuplicate(const char *src) {
	return gbDuplicateN(src, 0);
}
char *gbCopyN(char *dst, const char *src, size_t srcn) {
	size_t len;

	if (!src)
		return (char *)gbMemFree((void *)dst);

	len = srcn ? srcn : strlen(src);

	dst = (char *)gbMemExtend((void *)dst, len + 1);
	memcpy((void *)dst, (const char *)src, len);
	dst[len] = 0;

	return dst;
}
char *gbCopy(char *dst, const char *src) {
	return gbCopyN(dst, src, 0);
}
char *gbAppendNChar(char *dst, const char *src, size_t srcn, char ch) {
	size_t l1, l2, l3;

	if (!dst)
		return gbDuplicateN(src, srcn);

	l1 = strlen(dst);
	l2 = srcn ? srcn : strlen(src);
	l3 = ch!='\0' ? 1 : 0;

	dst = (char *)gbMemExtend((void *)dst, l1 + l2 + l3 + 1);
	memcpy(&dst[l1], (const void *)src, l2);
	dst[l1 + l2] = ch;
	if (ch != '\0')
		dst[l1 + l2 + l3] = '\0';

	return dst;
}
char *gbAppendN(char *dst, const char *src, size_t srcn) {
	return gbAppendNChar(dst, src, srcn, '\0');
}
char *gbAppend(char *dst, const char *src) {
	return gbAppendN(dst, src, 0);
}

char *gbTrimAppendChar(char *dst, const char *src, char ch) {
	const char *p, *q;

	for(p=src; *p<=' ' && *p!='\0'; p++);

	q = strchr(p, '\0');

	while(q > p) {
		if (*q++ > ' ')
			break;
	}

	if (!(q - p))
		return dst;

	return gbAppendNChar(dst, p, q - p, ch);
}
char *gbTrimAppend(char *dst, const char *src) {
	return gbTrimAppendChar(dst, src, '\0');
}

char *gbStrCpy(char *dst, size_t dstn, const char *src) {
	return gbStrCpyN(dst, dstn, src, 0);
}
char *gbStrCpyN(char *dst, size_t dstn, const char *src, size_t srcn) {
	size_t l;
	char *r;

	if (GB_UNLIKELY(!dst)) {
		gbError(__FILE__,__LINE__,__func__, "NULL destination string passed.");
		return (char *)0;
	}

	if (GB_UNLIKELY(!src)) {
		gbError(__FILE__,__LINE__,__func__, "NULL source string passed.");
		return (char *)0;
	}

	if (GB_UNLIKELY(dstn < 2)) {
		gbError(__FILE__,__LINE__,__func__, "No room in destination string.");
		return (char *)0;
	}

	l = srcn ? srcn : strlen(src);

	if (GB_UNLIKELY(l + 1 > dstn)) {
		l = dstn - 1;
		r = (char *)0;

		gbWarn(__FILE__,__LINE__,__func__, "Overflow prevented.");
	} else
		r = dst;

	if (l)
		memcpy((void *)dst, (const void *)src, l);

	dst[l] = 0;

	return r;
}

char *gbStrCat(char *dst, size_t dstn, const char *src) {
	return gbStrCatN(dst, dstn, src, 0);
}
char *gbStrCatN(char *dst, size_t dstn, const char *src, size_t srcn) {
	char *p;

	p = strchr(dst, '\0');

	p = gbStrCpyN(p, dstn - (p - dst), src, srcn);
	if (p != (char *)0)
		p = dst;

	return p;
}
