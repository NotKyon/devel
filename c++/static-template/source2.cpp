#include "header.h"

#include <stdio.h>

int GetValue2I() {
	MyClassI c(23);

	new MyClassI;

	return c.GetValue();
}
float GetValue2F() {
	MyClassF c(23);

	new MyClassF;
	new MyClassF;

	return c.GetValue();
}
