//
//	This example demonstrates one way you might use threading to do actual work.
//	See the note at the bottom, which describes an error of this sample (which
//	is there by design).
//
#define GB_INIT 1
#include <gamebase.h>

#include <stdlib.h> //for qsort

#define MAX_PRIMES 200
gb_u32_t g_primes[MAX_PRIMES];
gb_u32_t g_curPrimeIndex = 0;
gb_u32_t g_testNumber = 3;

//
//	Simple primality test, good for our function
//
gb_bool_t IsPrime(gb_u32_t number) {
	gb_u32_t i;

	// check a small sample set of known non-primes
	if (number <= 2)
		return GB_FALSE;

	// even numbers aren't primes
	if (~number & 1)
		return GB_FALSE;

	// enumerate the numbers and check for even divisibility
	// NOTE: doing this the long way for demonstration purposes
	for(i=2; i<number; i++) {
		if (number%i == 0)
			return GB_FALSE;
	}

	// appears to be a prime
	return GB_TRUE;
}

//
//	Utility function to find the next prime.
//
gb_u32_t FindNextPrime() {
	gb_u32_t number;

	// grab a number to test
__grab_next:
	number = gbAtomicIncrement32(&g_testNumber);

	// run the test
	if (!IsPrime(number))
		goto __grab_next;

	// found the number!
	return number;
}

//
//	Thread for checking primes
//
int PrimeThread_f(void *p) {
	gb_u32_t index;

	if (p) {} //unused parameter

	// main loop (grab index beforehand)
	while((index = gbAtomicIncrement32(&g_curPrimeIndex)) < MAX_PRIMES)
		// grab and store the next prime
		g_primes[index] = FindNextPrime();

	// done!
	return 0;
}

// <sorter comparison function>
int UIntCmp_f(const void *a, const void *b) {
	return *(int *)a - *(int *)b;
}

//
//	Main program logic
//
void GBMain() {
	gb_thread_t thread1, thread2;
	gb_uint_t i;

	// let the user know we're about to calculate
	gbPrint("Calculating primes...");

	// add threads
	thread1 = gbNewThread(PrimeThread_f, (void *)NULL);
	thread2 = gbNewThread(PrimeThread_f, (void *)NULL);

	// wait for each thread to complete
	gbJoinThread(thread1);
	gbJoinThread(thread2);

	// we're done with the threads
	thread1 = gbDeleteThread(thread1);
	thread2 = gbDeleteThread(thread2);

	// tell the user we're done
	gbPrint("Done calculating primes!");

	// the list probably isn't sorted (parallelism); sort it now
	qsort(&g_primes[0], MAX_PRIMES, sizeof(g_primes[0]), UIntCmp_f);

	// now display each prime
	for(i=0; i<MAX_PRIMES; i++)
		gbWriteStdout(gbF("%u%s", g_primes[i], (i + 1)%8==0 ? "\n" : "\t"));
}
