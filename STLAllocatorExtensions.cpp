#include <stdio.h>
#include <stdlib.h>

#include <new>
#include <iterator>

#include <stdint.h>
#include <stddef.h>
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef size_t uptr;
typedef ptrdiff_t sptr;
typedef unsigned int uint;
typedef signed int sint;

#ifndef SDF_NOEXCEPT
# define SDF_NOEXCEPT noexcept
#endif

// intrusive_list is used for the allocation statistics (see the actual
// allocator interface) -- no equivalent exists in the Standard C++ library
namespace sdf
{

	// Default node-type for intrusive_list
	//
	// This can be replaced with a user-supplied node-type in the template
	// parameter for intrusive_list. (Alternatively, a user-supplied node-type
	// could inherit from this.)
	//
	// The prev and next members must not be nullptr.
	//
	// This class should be lightweight because intrusive_list stores a private
	// instance of this as the base of the list. (The list is implemented as a
	// circular list for efficiency purposes: less branching due to less null
	// pointer checks; simpler implementation; etc.)
	struct intrusive_list_node
	{
		// Non-null pointer to the previous node in the list.
		intrusive_list_node *prev;
		// Non-null pointer to the next node in the list.
		intrusive_list_node *next;

		// Constructor for the node.
		inline intrusive_list_node(): prev( this ), next( this )
		{
		}
	};

	// Iterator used by intrusive_list.
	template< typename T, typename Pointer, typename Reference >
	class intrusive_list_iterator
	{
	public:
		typedef intrusive_list_iterator< T, Pointer, Reference > this_type;
		typedef intrusive_list_iterator< T, T *, T & > iterator;
		typedef intrusive_list_iterator< T, const T *, const T & >
			const_iterator;
		typedef T value_type;
		typedef T node_type;
		typedef ptrdiff_t difference_type;
		typedef Pointer pointer;
		typedef Reference reference;
		typedef std::bidirectional_iterator_tag iterator_category;

	public:
		inline intrusive_list_iterator( pointer x = nullptr ): mNode( x )
		{
		}
		inline intrusive_list_iterator( const iterator &x ): mNode( x.mNode )
		{
		}

		inline pointer get_pointer() const
		{
			return mNode;
		}

		inline reference operator*() const
		{
			//SDF_ASSERT_NOT_NULL( mNode );
			return *mNode;
		}
		inline pointer operator->() const
		{
			//SDF_ASSERT_NOT_NULL( mNode );
			return mNode;
		}

		inline intrusive_list_iterator &operator++()
		{
			//SDF_ASSERT_NOT_NULL( mNode );
			mNode = mNode->next;
			return *this;
		}
		inline intrusive_list_iterator &operator--()
		{
			//SDF_ASSERT_NOT_NULL( mNode );
			mNode = mNode->prev;
			return *this;
		}
		inline intrusive_list_iterator operator++( int ) const
		{
			return intrusive_list_iterator( mNode->next );
		}
		inline intrusive_list_iterator operator--( int ) const
		{
			return intrusive_list_iterator( mNode->prev );
		}

		inline bool operator==( const intrusive_list_iterator &x ) const
		{
			return mNode == x.mNode;
		}
		inline bool operator!=( const intrusive_list_iterator &x ) const
		{
			return mNode != x.mNode;
		}

	private:
		pointer mNode;
	};

	// List container where the user supplies the memory.
	//
	// In a normal list container the links are stored in an internal node
	// structure that is separate from the value stored in the list. In this
	// sort-of system objects can only be stored in one list at a time. For
	// them to be stored in separate lists a pointer would have to be used.
	// That is usually acceptable, but has some performance drawbacks. That
	// sort-of design is not usually cache-coherent due to the separate
	// locations of the node and the object.
	//
	// The intrusive list, which this class implements, solves that issue by
	// forcing the user to provide the memory for the node. By doing so, no
	// extra indirection is necessary as would be the case otherwise.
	template< typename T = intrusive_list_node >
	class intrusive_list
	{
	public:
		typedef intrusive_list< T > this_type;
		typedef T node_type;
		typedef T value_type;
		typedef size_t size_type;
		typedef diff_t difference_type;
		typedef T &reference;
		typedef T *pointer;
		typedef const T &const_reference;
		typedef const T *const_pointer;
		typedef intrusive_list_iterator< T, T *, T & > iterator;
		typedef typename iterator::const_iterator const_iterator;
		typedef std::reverse_iterator< iterator > reverse_iterator;
		typedef std::reverse_iterator< const_iterator > const_reverse_iterator;

	public:
		/// Basic constructor of the list
		inline intrusive_list(): mBase()
		{
		}
		/// Copy constructor of the list
		inline intrusive_list( const this_type &x ): mBase()
		{
			*this = x;
		}

		/// Assignment operator
		inline this_type &operator=( const this_type &x )
		{
			clear();
			for( auto &item : x )
			{
				push_back( item );
			}

			return *this;
		}

		/// Retrieve an iterator to the front of the list
		inline iterator begin() SDF_NOEXCEPT
		{
			return iterator( mBase.next );
		}
		/// Const version of begin()
		inline const_iterator begin() const SDF_NOEXCEPT
		{
			return const_iterator( mBase.next );
		}
		/// Retrieve an iterator the item just after the back of the list
		inline iterator end() SDF_NOEXCEPT
		{
			return iterator( &mBase );
		}
		/// Const version of end()
		inline const_iterator end() const SDF_NOEXCEPT
		{
			return const_iterator( &mBase );
		}

		/// Same as reverse_iterator( end() )
		inline reverse_iterator rbegin() SDF_NOEXCEPT
		{
			return reverse_iterator( &mBase );
		}
		/// Const version of rbegin()
		inline const_reverse_iterator rbegin() const SDF_NOEXCEPT
		{
			return const_reverse_iterator( &mBase );
		}
		/// Same as reverse_iterator( begin() )
		inline reverse_iterator rend() SDF_NOEXCEPT
		{
			return reverse_iterator( mBase.next );
		}
		/// Const version of rend()
		inline const_reverse_iterator rend() const SDF_NOEXCEPT
		{
			return const_reverse_iterator( mBase.next );
		}

		/// Retrieve a reference to the first item in the list
		inline reference front() SDF_NOEXCEPT
		{
			return *mBase.next;
		}
		/// Const version of front()
		inline const_reference front() const SDF_NOEXCEPT
		{
			return *mBase.next;
		}
		/// Retrieve a reference to the last item in the list
		inline reference back() SDF_NOEXCEPT
		{
			return *mBase.prev;
		}
		/// Const version of back()
		inline const_reference back() const SDF_NOEXCEPT
		{
			return *mBase.prev;
		}

		/// Determine whether the list is empty or not
		inline bool empty() const SDF_NOEXCEPT
		{
			return mBase.next == &mBase;
		}
		/// Count the number of items in the list. (This is an O(N) operation.)
		inline size_type size() const SDF_NOEXCEPT
		{
			size_type count = 0;

			for( auto *p = mBase.next; p != &mBase; p = p->next )
			{
				++count;
			}

			return count;
		}
		/// Remove all items from the list.
		inline void clear() SDF_NOEXCEPT
		{
			while( mBase.next != &mBase )
			{
				remove( *mBase.next );
			}
		}

		/// Insert an item at the beginning of the list
		inline void push_front( T &x )
		{
			insert( begin(), x );
		}
		/// Remove the item at the beginning of the list
		inline void pop_front() SDF_NOEXCEPT
		{
			remove( mBase.next );
		}
		/// Insert an item at the end of the list
		inline void push_back( T &x )
		{
			insert( end(), x );
		}
		/// Remove the item at the end of the list
		inline void pop_back() SDF_NOEXCEPT
		{
			remove( mBase.prev );
		}

		/// Find the iterator for the given item in the list
		inline iterator locate( T &x ) SDF_NOEXCEPT
		{
			return const_cast< iterator >( SDF_CONST_THIS->locate( x ) );
		}
		/// Const version of locate()
		inline const_iterator locate( const T &x ) const SDF_NOEXCEPT
		{
			// NOTE: Could just do const_iterator( &x ) instead of checking
			// -     through the list, but that would violate the rule that if
			// -     the item is not in the list then end() should be returned.

			for( const auto *p = mBase.next; p != &mBase; p = p->next )
			{
				if( *p == x )
				{
					return const_iterator( p );
				}
			}

			return end();
		}

		/// Insert the item before the given iterator in the list
		inline iterator insert( iterator pos, T &x ) SDF_NOEXCEPT
		{
			x.prev = pos->prev;
			x.next = pos.get_pointer();

			pos->prev->next = &x;
			pos->prev = &x;

			return iterator( &x );
		}
		
		/// \todo erase( pos )
		inline iterator erase( iterator pos ) SDF_NOEXCEPT
		{
			iterator r = pos;
			++r;

			remove( *pos );

			return r;
		}
		/// \todo erase( first, last )
		inline iterator erase( iterator first, iterator last ) SDF_NOEXCEPT
		{
			iterator j;
			for( iterator i = first; i != last; i = j )
			{
				j = i;
				++j;

				remove( *i );
			}

			return last;
		}

		/// \brief Remove an item from whatever list it's in
		///
		/// This does not require the list (hence it's static)
		static void remove( T &x ) SDF_NOEXCEPT
		{
			x.prev->next = x.next;
			x.next->prev = x.prev;

			x.prev = &x;
			x.next = &x;
		}

	protected:
		node_type mBase;
	};

	/// \def SDF_VALUE_FROM_NODE( NodePtr, ValueType, ValueField )
	/// \brief Retrieve a pointer to an instance of a structure from a pointer
	///        to a field within that structure.
	/// \param NodePtr Pointer to the field within the structure.
	/// \param ValueType Name of the structure.
	/// \param ValueField Name of the field pointed to by NodePtr.
	/// \return The base of the structure as a ValueType pointer.
#define SDF_VALUE_FROM_NODE( NodePtr, ValueType, ValueField )\
	( ( ValueType * )( ( size_t )( NodePtr ) -\
	offsetof( ValueType, ValueField ) ) )

}

/*

	GOALS
	- Configurable from external system
	- Filters through a central interface for memory debugging and profiling
	  purposes
	- Works as an Alloc template parameter for containers

*/

// The default name given to each allocator instance.
#ifndef SDF_DEFAULT_ALLOCATOR_NAME
# define SDF_DEFAULT_ALLOCATOR_NAME "Default"
#endif

// Specifies whether development-mode should be "on" for allocators
#ifndef SDF_ALLOC_DEVMODE
# define SDF_ALLOC_DEVMODE ( SDF_DEBUG_ENABLED || SDF_TEST_ENABLED )
#endif

// Specifies whether extra debug information is stored per allocation
#ifndef SDF_ALLOC_DEBUG_ENABLED
# define SDF_ALLOC_DEBUG_ENABLED SDF_ALLOC_DEVMODE
#endif
// Specifies whether statistics are tracked for allocs/deallocs
#ifndef SDF_ALLOC_STATS_ENABLED
# define SDF_ALLOC_STATS_ENABLED SDF_ALLOC_DEVMODE
# include <mutex>
#endif
// Specifies whether file/line information is stored per allocation.
#ifndef SDF_ALLOC_FILE_LINE_ENABLED
# define SDF_ALLOC_FILE_LINE_ENABLED SDF_ALLOC_DEVMODE
#endif
// Specifies whether each allocation header should be tracked in a giant list or
// not.
#ifndef SDF_ALLOC_HEADER_LIST_ENABLED
# define SDF_ALLOC_HEADER_LIST_ENABLED SDF_ALLOC_DEVMODE
# include <mutex>
#endif
// Specifies whether the call stack should be captured on each allocation. (Off
// by default.)
#ifndef SDF_ALLOC_CALL_STACK_ENABLED
# define SDF_ALLOC_CALL_STACK_ENABLED 0
#endif


namespace sdf
{

#if SDF_ALLOC_STATS_ENABLED
	// Information about memory allocations.
	struct alloc_stats
	{
		// Name of the allocator. (Same as allocator::get_name().)
		const char *name;
		// Current total allocated size, in bytes.
		size_t size;
		// Preferred maximum size that can be allocated, in bytes.
		size_t budget_size;

		// Total number of allocate() calls that have occurred.
		uint alloc_count;
		// Total number of deallocate() calls that have occurred.
		uint dealloc_count;
		// Total time (in microseconds) spent allocating.
		u64 alloc_time;
		// Total time (in microseconds) spent deallocating.
		u64 dealloc_time;

		// Change in alloc_count since last query.
		uint delta_alloc_count;
		// Change in dealloc_count since last query.
		uint delta_dealloc_count;
		// Change in alloc_time since last query.
		u64 delta_alloc_time;
		// Change in dealloc_time since last query.
		u64 delta_dealloc_time;

		// Link to the previous set of statistics
		alloc_stats *prev;
		// Link to the next set of statistics
		alloc_stats *next;
		// Global list
		typedef intrusive_list< alloc_stats > container_type;
		static container_type container;
		// Lock for the list
		static std::mutex access;
	};
#endif

	namespace detail
	{
	
		// Allocation header
		//
		// This header exists at the start of every allocation. Portions of the
		// header can be removed entirely based on a few preprocessor defines,
		// even to the point of making the header take no space. Likewise,
		// additional features (for example, a call-stack per allocation) can be
		// enabled which will increase the size of the header.
		//
		// In release mode, there shouldn't be any header at all (but this is
		// just the default; if some header data is required even in release
		// mode it can be configured that way).
		struct alloc_header
		{
#if SDF_ALLOC_DEBUG_ENABLED
			// Start of the data
			void *data;
			// Magic data
			size_t magic;
#endif
#if SDF_ALLOC_STATS_ENABLED
			// Size of this allocation, in bytes
			size_t size;
			// Allocator name
			const char *name;
#endif
#if SDF_ALLOC_FILE_LINE_ENABLED
			// File the allocation occurred from
			const char *file;
			// Line of the file the allocation occurred from
			int line;
#endif
#if SDF_ALLOC_HEADER_LIST_ENABLED
			// Previous header in list
			alloc_header *prev;
			// Next header in list
			alloc_header *next;
#endif
#if SDF_ALLOC_CALL_STACK_ENABLED
			static const u16 kMaxStack = 24;

			// Stack pointers
			void *callstack[ kMaxStack ];
#endif
		};

		// Empty structure
		struct empty_struct
		{
		};

		// Calculate the effective size of an empty structure
		template< size_t Size >
		struct struct_size
		{
			// This is the effective size of the structure
			static const size_t value = Size;
		};
		// Specialization of struct_size for an empty structure
		template<>
		struct struct_size< sizeof( empty_struct ) >
		{
			// The usable size of an empty structure is zero
			static const size_t value = 0;
		};

		// Store the real size of alloc_header
		static const size_t kAllocHeaderSize =
			struct_size< sizeof( alloc_header ) >::value;
	
	}


	// Base allocator interface
	//
	// Adapts other allocators
	template< typename T, typename Alloc >
	class allocator: public Alloc
	{
	public:
		typedef T value_type, *pointer, &reference;
		typedef const T *const_pointer, &const_reference;
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;

		template< typename U >
		struct rebind
		{
			typedef allocator< U, typename Alloc::rebind< U >::other > other;
		};

		template< typename U >
		inline U *address( U &x ) const
		{
			return reinterpret_cast< U * >( &reinterpret_cast< char & >( x ) );
		}

		template< typename U >
		inline void construct( U *p, const U & )
		{
			new( reinterpret_cast< void * >( p ) ) U();
		}
		template< typename U, typename... Args >
		inline void construct( U *p, Args &&...args )
		{
			new( reinterpret_cast< void * >( p ) )
				U( std::forward< Args >( args ) );
		}
		template< typename U >
		inline void destroy( U *p )
		{
			if( !p )
			{
				return;
			}

			p->~U();
		}

		inline size_type max_size() const SDF_NOEXCEPT
		{
			return Alloc::max_size();
		}

		inline pointer allocate( size_type n, const void * = nullptr )
		{
		}
		inline void deallocate( pointer p, size_type n )
		{
		}

	private:
#if SDF_ALLOC_STATS_ENABLED
		// Allocation statistics for this allocator.
		alloc_stats mStats;
#endif
#if SDF_ALLOC_HEADER_LIST_ENABLED
		// Thread-safety for valid-list access
		std::mutex mValidListAccess;

		// Valid allocations list
		intrusive_list< detail::alloc_header > mValidList;
#endif

#if SDF_ALLOC_DEBUG_ENABLED
		/// Determine whether an allocation header is valid for this allocator
		bool validate_header( detail::alloc_header *hdr );
#endif
	};

}

#if SDF_ALLOC_STATS_ENABLED
sdf::alloc_stats::container_type sdf::alloc_stats::container;
std::mutex sdf::alloc_stats::access;
#endif
