﻿#include "suugaku_demo_window.hpp"

const unsigned kDefaultWindowResX = 1280;
const unsigned kDefaultWindowResY = 720;

struct WM_s
{
	HWND     hWindow;
	ATOM     WindowClass;
	unsigned uResX, uResY;
	HDC      hDevCtx;
	HGLRC    hGfxCtx;
	unsigned uKeyState[ 256/(sizeof(unsigned)*8) ];

	static WM_s &GetInstance();

	inline void SetKey( unsigned uIndex )
	{
		const unsigned i = uIndex/( sizeof( unsigned )*8 );
		const unsigned j = uIndex%( sizeof( unsigned )*8 );

		if( i >= sizeof( uKeyState )/sizeof( uKeyState[0] ) ) {
			return;
		}

		uKeyState[ i ] |= 1U<<j;
	}
	inline void ClearKey( unsigned uIndex )
	{
		const unsigned i = uIndex/( sizeof( unsigned )*8 );
		const unsigned j = uIndex%( sizeof( unsigned )*8 );

		if( i >= sizeof( uKeyState )/sizeof( uKeyState[0] ) ) {
			return;
		}

		uKeyState[ i ] &= ~( 1U<<j );
	}
	inline bool GetKey( unsigned uIndex ) const
	{
		const unsigned i = uIndex/( sizeof( unsigned )*8 );
		const unsigned j = uIndex%( sizeof( unsigned )*8 );

		if( i >= sizeof( uKeyState )/sizeof( uKeyState[0] ) ) {
			return false;
		}

		return ( uKeyState[ i ] & ( 1U<<j ) ) != 0;
	}

private:
	WM_s()
	: hWindow( (HWND)0 )
	, WindowClass( (ATOM)0 )
	, uResX( 0 )
	, uResY( 0 )
	, hDevCtx( NULL )
	, hGfxCtx( NULL )
	{
	}
	~WM_s()
	{
		FiniWindow();
	}
};

WM_s &WM_s::GetInstance()
{
	static WM_s instance;
	return instance;
}

#define WM WM_s::GetInstance()

static unsigned GetScancode_Win32( LPARAM lParm )
{
	return ( ( lParm & 0x00FF0000 )>>16 ) + ( ( lParm & 0x01000000 )>>17 );
}
static LRESULT CALLBACK MsgProc_f( HWND hWnd, UINT uMsg, WPARAM wParm, LPARAM lParm )
{
	switch( uMsg )
	{
	case WM_CLOSE:
		DestroyWindow( hWnd );
		return 0;

	case WM_DESTROY:
		PostQuitMessage( 0 );
		return 0;

	case WM_SIZE:
		WM.uResX = LOWORD(lParm);
		WM.uResY = HIWORD(lParm);
		return 0;

	case WM_ERASEBKGND:
		return 0;

	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
		WM.SetKey( GetScancode_Win32( lParm ) );
		return 0;

	case WM_KEYUP:
	case WM_SYSKEYUP:
		WM.ClearKey( GetScancode_Win32( lParm ) );
		return 0;

	default:
		break;
	}

	return DefWindowProcW( hWnd, uMsg, wParm, lParm );
}

static bool InitWindowClass_Win32()
{
	WNDCLASSEXW wc;

	if( WM.WindowClass != (ATOM)0 ) {
		return true;
	}

	wc.cbSize = sizeof(wc);
	wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = &MsgProc_f;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = GetModuleHandleW((LPCWSTR)0);
	wc.hIcon = LoadIconW(wc.hInstance, (LPCWSTR)1);
	wc.hCursor = LoadCursorW((HINSTANCE)0, (LPCWSTR)IDC_ARROW);
	wc.hbrBackground = (HBRUSH)0;
	wc.lpszMenuName = (LPCWSTR)0;
	wc.lpszClassName = L"Window";
	wc.hIconSm = LoadIconW(wc.hInstance, (LPCWSTR)1);

	WM.WindowClass = RegisterClassExW(&wc);
	if (!WM.WindowClass) {
		return false;
	}

	return true;
}
static void FiniWindowClass_Win32()
{
	if( !WM.WindowClass ) {
		return;
	}

	UnregisterClassW( (LPCWSTR)(size_t)WM.WindowClass, GetModuleHandleW( (LPCWSTR)0 ) );
	WM.WindowClass = (ATOM)0;
}

static bool InitWindow_Win32()
{
	if( WM.hWindow != (HWND)0 ) {
		return true;
	}

	DWORD dwExstyle = 0;
	DWORD dwStyle = WS_OVERLAPPEDWINDOW;

	const LONG uWResX = (LONG)kDefaultWindowResX;
	const LONG uWResY = (LONG)kDefaultWindowResY;

	const LONG uSResX = GetSystemMetrics( SM_CXSCREEN );
	const LONG uSResY = GetSystemMetrics( SM_CYSCREEN );

	RECT rcArea = {
		uSResX/2 - uWResX/2,
		uSResY/2 - uWResY/2,
		uSResX/2 + uWResX/2 + uWResX%2,
		uSResY/2 + uWResY/2 + uWResY%2
	};

	if( !AdjustWindowRectEx( &rcArea, dwStyle, FALSE, dwExstyle ) ) {
		return false;
	}

	WM.hWindow =
		CreateWindowExW
		(
			dwExstyle,
			L"Window",
			L"Test Window",
			dwStyle,
			rcArea.left,
			rcArea.top,
			rcArea.right - rcArea.left,
			rcArea.bottom - rcArea.top,
			(HWND)0,
			(HMENU)0,
			GetModuleHandleW((LPCWSTR)0),
			(LPVOID)0
		);
	if( !WM.hWindow ) {
		return false;
	}

	UpdateWindow( WM.hWindow );
	ShowWindow( WM.hWindow, SW_SHOW );

	RECT rcClient;
	if( GetClientRect( WM.hWindow, &rcClient ) ) {
		WM.uResX = rcClient.right;
		WM.uResY = rcClient.bottom;
	}

	return true;
}
static void FiniWindow_Win32()
{
	if( !WM.hWindow ) {
		return;
	}

	DestroyWindow( WM.hWindow );
	WM.hWindow = (HWND)0;
}

static bool InitOpenGL_Win32()
{
	if( WM.hGfxCtx != NULL ) {
		return true;
	}

	WM.hDevCtx = GetDC( WM.hWindow );
	if( !WM.hDevCtx ) {
		return false;
	}

	PIXELFORMATDESCRIPTOR pfd;

	pfd.nSize           = sizeof( pfd );
	pfd.nVersion        = 1;
	pfd.dwFlags         = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType      = PFD_TYPE_RGBA;
	pfd.cColorBits      = 32;
	pfd.cRedBits        = 0;
	pfd.cRedShift       = 0;
	pfd.cGreenBits      = 0;
	pfd.cGreenShift     = 0;
	pfd.cBlueBits       = 0;
	pfd.cBlueShift      = 0;
	pfd.cAlphaBits      = 0;
	pfd.cAlphaShift     = 0;
	pfd.cAccumBits      = 0;
	pfd.cAccumRedBits   = 0;
	pfd.cAccumGreenBits = 0;
	pfd.cAccumBlueBits  = 0;
	pfd.cAccumAlphaBits = 0;
	pfd.cDepthBits      = 24;
	pfd.cStencilBits    = 8;
	pfd.cAuxBuffers     = 0;
	pfd.iLayerType      = PFD_MAIN_PLANE;
	pfd.bReserved       = 0;
	pfd.dwLayerMask     = 0;
	pfd.dwVisibleMask   = 0;
	pfd.dwDamageMask    = 0;

	const int iPixelFmt = ChoosePixelFormat( WM.hDevCtx, &pfd );
	if( !iPixelFmt ) {
		ReleaseDC( WM.hWindow, WM.hDevCtx );
		return false;
	}

	if( !SetPixelFormat( WM.hDevCtx, iPixelFmt, &pfd ) ) {
		ReleaseDC( WM.hWindow, WM.hDevCtx );
		return false;
	}

	WM.hGfxCtx = wglCreateContext( WM.hDevCtx );
	if( !WM.hGfxCtx ) {
		ReleaseDC( WM.hWindow, WM.hDevCtx );
		return false;
	}

	if( !wglMakeCurrent( WM.hDevCtx, WM.hGfxCtx ) ) {
		wglDeleteContext( WM.hGfxCtx );
		WM.hGfxCtx = NULL;

		ReleaseDC( WM.hWindow, WM.hDevCtx );
		WM.hDevCtx = NULL;

		return false;
	}

	return true;
}
static void FiniOpenGL_Win32()
{
	if( !WM.hGfxCtx ) {
		return;
	}

	wglMakeCurrent( NULL, NULL );

	wglDeleteContext( WM.hGfxCtx );
	WM.hGfxCtx = NULL;

	ReleaseDC( WM.hWindow, WM.hDevCtx );
	WM.hDevCtx = NULL;
}

bool InitWindow()
{
	bool r = true;

	r = r && InitWindowClass_Win32();
	r = r && InitWindow_Win32();
	r = r && InitOpenGL_Win32();

	return r;
}
void FiniWindow()
{
	FiniOpenGL_Win32();
	FiniWindow_Win32();
	FiniWindowClass_Win32();
}

unsigned GetWindowResX()
{
	return WM.uResX;
}
unsigned GetWindowResY()
{
	return WM.uResY;
}

bool ProcessWindowEvents()
{
	bool bRunning = true;
	MSG Msg;

	while( PeekMessageW( &Msg, (HWND)0, 0, 0, PM_REMOVE ) ) {
		if( Msg.message == WM_QUIT ) {
			bRunning = false;
		}

		TranslateMessage( &Msg );
		DispatchMessageW( &Msg );
	}

	return bRunning && WM.hWindow != (HWND)0;
}

HWND GetWindowHandle_Win32()
{
	return WM.hWindow;
}

void SwapBuffers()
{
	SwapBuffers( WM.hDevCtx );
}

bool KeyDown( EKey Key )
{
	return WM.GetKey( ( unsigned )Key );
}
