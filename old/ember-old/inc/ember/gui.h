#ifndef EMBER_GUI_H
#define EMBER_GUI_H

#include "config.h"
#include "opengl.h"
#include "gc.h"

#ifndef __responder_t_defined__
#define __responder_t_defined__ 1
typedef struct stResponderGL *responder_t;
#endif

#ifndef __window_t_defined__
#define __window_t_defined__ 1
typedef struct stWindowGL *window_t;
#endif

#define GUI_CAPTION_BIT 0x01
#define GUI_ICONIFIABLE_BIT 0x02
#define GUI_RESIZABLE_BIT 0x04
#define GUI_CLOSABLE_BIT 0x08
#define GUI_ICONIFIED_BIT 0x10
#define GUI_ZOOMED_BIT 0x20

typedef enum {
	EGUIWindowLevel_BottomMost,
	EGUIWindowLevel_Bottom,
	EGUIWindowLevel_Normal,
	EGUIWindowLevel_Top,
	EGUIWindowLevel_TopMost,

	EGUINumWindowLevels
} EGUIWindowLevel;

typedef enum {
	EGUIMouseButton_Left,
	EGUIMouseButton_Right,
	EGUIMouseButton_Middle
} EGUIMouseButton;

struct stResponderGL {
	void ( *on_init )( struct stWindowGL *, void * );
	void ( *on_deinit )( struct stWindowGL * );
	void ( *on_close )( struct stWindowGL * );

	void ( *on_move )( struct stWindowGL *, int, int );
	void ( *on_size )( struct stWindowGL *, int, int );

	void ( *pre_draw )( struct stWindowGL * );
	void ( *on_draw )( struct stWindowGL * );
	void ( *post_draw )( struct stWindowGL * );

	void ( *on_mouse_down )( struct stWindowGL *, EGUIMouseButton, int, int );
	void ( *on_mouse_up )( struct stWindowGL *, EGUIMouseButton, int, int );
	void ( *on_mouse_move )( struct stWindowGL *, int, int );
	void ( *on_mouse_wheel )( struct stWindowGL *, int );

	void ( *on_key_char )( struct stWindowGL *, char );
	void ( *on_key_down )( struct stWindowGL *, int );
	void ( *on_key_up )( struct stWindowGL *, int );
};

struct stWindowGL {
	char *title;
	char *placeholder;
	char *tip;

	bitfield_t style;
	EGUIWindowLevel level;

	struct {
		int x, y;
		int w, h;
	} normal_rect,
	    restore_rect;
	struct {
		int w, h;
	} min_extent,
	    max_extent;
	int caption_height;

	struct {
		float r, g, b, a;
	} bg_color;

	struct stResponderGL *responder;

	struct stWindowGL *w_prnt,
	    *w_head, *w_tail,
	    *w_prev, *w_next;

	struct stWindowGL *l_prev, *l_next;
};
__em_extrn gc_pool_t g_gui_w_pool __em_nil();
__em_extrn window_t g_gui_l_head[ EGUINumWindowLevels ],
    g_gui_l_tail[ EGUINumWindowLevels ];

extern int gui_init();
extern void gui_deinit();
extern void gui_resize_workspace( int w, int h );
extern int gui_workspace_width();
extern int gui_workspace_height();
extern window_t gui_create( responder_t resp, const char *title, bitfield_t style, int x, int y, int w, int h );
extern void gui_destroy( window_t self );
extern void gui_close( window_t self );
extern void gui_set_title( window_t self, const char *title );
extern void gui_set_placeholder( window_t self, const char *placeholder );
extern void gui_set_tip( window_t self, const char *tip );
extern void gui_set_style( window_t self, bitfield_t style );
extern void gui_set_level( window_t self, EGUIWindowLevel level );
extern void gui_move_window( window_t self, int x, int y );
extern void gui_resize_window( window_t self, int w, int h );
extern void gui_iconify( window_t self );
extern void gui_zoom( window_t self );
extern void gui_restore( window_t self );
extern void gui_set_min_extent( window_t self, int min_w, int min_h );
extern void gui_set_max_extent( window_t self, int max_w, int max_h );
extern void gui_set_caption_height( window_t self, int caption_height );
extern void gui_set_background_color( window_t self, float r, float g, float b );
extern void gui_set_transparency( window_t self, float a );
extern responder_t gui_set_responder( window_t self, responder_t responder );
extern int gui_is_descendant( window_t self, window_t prnt );
extern void gui_set_parent( window_t self, window_t prnt );
extern const char *gui_title( window_t self );
extern const char *gui_placeholder( window_t self );
extern const char *gui_tip( window_t self );
extern bitfield_t gui_style( window_t self );
extern EGUIWindowLevel gui_level( window_t self );
extern int gui_position_x( window_t self );
extern int gui_position_y( window_t self );
extern int gui_width( window_t self );
extern int gui_height( window_t self );
extern int gui_restore_position_x( window_t self );
extern int gui_restore_position_y( window_t self );
extern int gui_restore_width( window_t self );
extern int gui_restore_height( window_t self );
extern int gui_min_width( window_t self );
extern int gui_min_height( window_t self );
extern int gui_max_width( window_t self );
extern int gui_max_height( window_t self );
extern int gui_caption_height( window_t self );
extern void gui_get_background_color( window_t self, float *out );
extern float gui_background_color_red( window_t self );
extern float gui_background_color_green( window_t self );
extern float gui_background_color_blue( window_t self );
extern float gui_transparency( window_t self );
extern responder_t gui_responder( window_t self );
extern window_t gui_super( window_t self );
extern window_t gui_first_sub( window_t self );
extern window_t gui_last_sub( window_t self );
extern window_t gui_sub_before( window_t self );
extern window_t gui_sub_after( window_t self );
extern window_t gui_first( EGUIWindowLevel level );
extern window_t gui_last( EGUIWindowLevel level );
extern int gui_client_width( window_t self );
extern int gui_client_height( window_t self );

#endif
