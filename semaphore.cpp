#include <windows.h>
#include <cstdio>
#include <cstdlib>

int main() {
	HANDLE semaphore;
	int initCount, maxiCount;
	int numSigs;
	int i;

	numSigs = 5;

	//initCount = 1 - numSigs;
	//maxiCount = 1;
	initCount = 0;
	maxiCount = numSigs;
	semaphore = CreateSemaphore(0, initCount, maxiCount, 0);
	if (!semaphore) {
		fprintf(stderr, "Failed to allocate semaphore\n");
		return EXIT_FAILURE;
	}

	for(i=0; i<numSigs; i++) {
		LONG x;

		if (!ReleaseSemaphore(semaphore, 1, &x)) {
			fprintf(stderr, "Failed to release semaphore\n");
			return EXIT_FAILURE;
		}

		printf("Previous count: %i\n", x);
	}

	printf("Waiting...\n");
	if (!WaitForSingleObject(semaphore, 0)) {
		fprintf(stderr, "Failed to acquire access!\n");
		return EXIT_FAILURE;
	}

	CloseHandle(semaphore); semaphore = (HANDLE)0;
	return EXIT_SUCCESS;
}
