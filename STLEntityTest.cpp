#define ENTSTRUC_VECTOR_POINTER 1
#define ENTSTRUC_VECTOR_ITERATOR 2
#define ENTSTRUC_INTRUSIVE_LIST 3
#define ENTSTRUC_STANDARD_LIST 4
#define ENTSTRUC_CUSTOM_VECTOR 5
#define ENTSTRUC_SOA 6

#ifndef ENTITY_STRUCTURE
# define ENTITY_STRUCTURE ENTSTRUC_VECTOR_POINTER
//# define ENTITY_STRUCTURE ENTSTRUC_VECTOR_ITERATOR
//# define ENTITY_STRUCTURE ENTSTRUC_INTRUSIVE_LIST
//# define ENTITY_STRUCTURE ENTSTRUC_STANDARD_LIST
//# define ENTITY_STRUCTURE ENTSTRUC_CUSTOM_VECTOR
//# define ENTITY_STRUCTURE ENTSTRUC_SOA
#endif

#ifndef THREADING_ENABLED
# define THREADING_ENABLED 0
#endif

#if THREADING_ENABLED
# include "c++/taskshare/versions/Task5.h"
#endif

#include <assert.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include <list>
#include <vector>
#include <string>
#include <utility>
#include <type_traits>

typedef uint64_t u64;
typedef double f64;

/*
===============================================================================

	ARCHITECTURE / PLATFORM

===============================================================================
*/
#ifndef X86
# if defined( __i386__ ) || defined( _M_IX86 )
#  define X86 1
# else
#  define X86 0
# endif
#endif
#ifndef X64
# if defined( __amd64__ ) || defined( __x86_64__ ) || defined( _M_X64 )
#  undef X86
#  define X86 1
#  define X64 1
# else
#  define X64 0
# endif
#endif

#ifndef SIMD_ENABLED
# define SIMD_ENABLED 0
# if ENTITY_STRUCTURE == ENTSTRUC_SOA
#  if X86
#   undef SIMD_ENABLED
#   define SIMD_ENABLED 1
#   include <mmintrin.h>
#   include <emmintrin.h>
#   include <xmmintrin.h>
#  endif
# endif
#endif

#ifndef ALIGN
# if defined( __GNUC__ ) || defined( __clang__ )
#  define ALIGN( x ) __attribute__( ( aligned( x ) ) )
# elif defined( _MSC_VER )
#  define ALIGN( x ) __declspec( align( x ) )
# else
#  define ALIGN( x )
# endif
#endif

#ifndef CACHELINE
# define CACHELINE 64
#endif

#ifndef CACHELINE_ALIGN
# define CACHELINE_ALIGN ALIGN( CACHELINE )
#endif

/*
===============================================================================

	DEVELOPMENT / TESTING CONFIGURATION

===============================================================================
*/
#ifndef DEBUG_ENABLED
# if defined( DEBUG ) || defined( _DEBUG ) || defined( __debug__ )
#  define DEBUG_ENABLED 1
# else
#  define DEBUG_ENABLED 0
# endif
#endif
#ifndef TEST_ENABLED
# if defined( TEST ) || defined( _TEST ) || defined( __test__ )
#  define TEST_ENABLED 1
# else
#  define TEST_ENABLED 0
# endif
#endif

#ifndef ASSERT_ENABLED
# define ASSERT_ENABLED ( DEBUG_ENABLED || TEST_ENABLED )
#endif

#ifndef ASSERT
# if ASSERT_ENABLED
#  define ASSERT(x) assert(x)
# else
#  define ASSERT(x)
# endif
#endif

/*
===============================================================================

	TIMING / PROFILING

===============================================================================
*/
#if defined( _WIN32 )
# include <Windows.h>
namespace sdf
{

	namespace detail
	{

		inline u64 ConvertResolution( u64 t, u64 f, u64 r )
		{
			return t*r/f;
		}
		inline u64 QueryFrequency()
		{
			LARGE_INTEGER f;

			if( !QueryPerformanceFrequency( &f ) )
			{
				f.QuadPart = 1;
			}

			return ( u64 )f.QuadPart;
		}

	}

	inline u64 query_microseconds()
	{
		static const u64 f = detail::QueryFrequency();
		u64 t;

		QueryPerformanceCounter( ( LARGE_INTEGER * )&t );

		return detail::ConvertResolution( t, f, 1000000 );
	}

}
#else
# error FIXME: Need timing stuff for this platform
#endif

namespace sdf
{

	inline f64 microseconds_to_seconds( u64 microsec )
	{
		return f64( microsec )/1000000.0;
	}

	class timer
	{
	public:
		inline timer()
		{
		}
		inline ~timer()
		{
		}

		inline void reset()
		{
			mBase = query_microseconds();
		}

		inline f64 get_elapsed_seconds() const
		{
			return microseconds_to_seconds( get_elapsed() );
		}
		inline u64 get_elapsed() const
		{
			return query_microseconds() - mBase;
		}
		
		inline u64 operator()() const
		{
			return get_elapsed();
		}

	private:
		u64 mBase = query_microseconds();
	};

}

/*
===============================================================================

	MEMORY MANAGEMENT

===============================================================================
*/
namespace detail
{

	struct construct_token
	{
	};

}

inline void *operator new( size_t, void *p, detail::construct_token )
{
	return p;
}

template< typename T >
inline void construct( T &x )
{
	new( ( void * )&reinterpret_cast< char & >( x ), detail::construct_token() )
		T();
}
template< typename T >
inline void destroy( T &x )
{
	x.~T();
}

/*
===============================================================================

	CUSTOM VECTOR<> IMPLEMENTATION

===============================================================================
*/
template< typename T >
class custom_vector
{
public:
	typedef T value_type;
	typedef T *pointer;
	typedef const T *const_pointer;
	typedef T &reference;
	typedef const T &const_reference;
	typedef T *iterator;
	typedef const T *const_iterator;

	inline custom_vector()
	{
	}
	inline custom_vector( custom_vector &&x )
	{
		*this = std::move( x );
	}
	inline ~custom_vector()
	{
		purge();
	}

	inline custom_vector &operator=( custom_vector &&x )
	{
		mSize = x.mSize;
		mCapacity = x.mCapacity;
		mBuffer = x.mBuffer;

		x.mBuffer = nullptr;
		x.mSize = 0;
		x.mCapacity = 0;

		return *this;
	}

	inline void clear()
	{
		for( size_t i = mSize; i > 0; --i )
		{
			destroy( mBuffer[ i - 1 ] );
		}

		mSize = 0;
	}
	inline void purge()
	{
		clear();
		( void )set_capacity( 0 );
	}

	inline bool empty() const
	{
		return mSize == 0;
	}

	inline reference front()
	{
		return *mBuffer;
	}
	inline reference back()
	{
		return mBuffer[ mSize - 1 ];
	}

	inline void push_back( const T &x )
	{
		const size_t index = mSize;
		if( !resize( index + 1 ) )
		{
			return;
		}

		mBuffer[ index ] = x;
	}
	inline void emplace_back( T &&x )
	{
		const size_t index = mSize;
		if( !resize( index + 1 ) )
		{
			return;
		}

		mBuffer[ index ] = std::move( x );
	}
	inline void pop_back( size_t count = 1 )
	{
		if( empty() || !count )
		{
			return;
		}

		if( count > mSize )
		{
			count = mSize;
		}

		const size_t newSize = mSize - count;
		for( size_t i = mSize; i > newSize; --i )
		{
			destroy( mBuffer[ i ] );
		}

		mSize = newSize;
	}

	inline bool set_capacity( size_t n )
	{
		// destroy old elements
		for( size_t i = mSize; i > n; --i )
		{
			destroy( mBuffer[ i - 1 ] );
		}

		// prepare a new buffer
		T *p = nullptr;
		if( n > 0 )
		{
			// allocate the new buffer
			p = ( T * )malloc( sizeof( T )*n );
			if( !p )
			{
				return false;
			}

			// copy existing elements over
			const size_t lowerBound = n < mSize ? n : mSize;
			for( size_t i = 0; i < lowerBound; ++i )
			{
				// doing a direct copy will be SUPER EXPENSIVE
				construct( p[ i ] );
				p[ i ] = std::move( mBuffer[ i ] );
			}
		}

		// deallocate the old buffer
		free( ( void * )mBuffer );
		mBuffer = p;

		// truncate the size
		if( n < mSize )
		{
			mSize = n;
		}

		// make note of the new capacity and return news of the success
		mCapacity = n;
		return true;
	}
	inline bool reserve( size_t n )
	{
		if( n <= mCapacity )
		{
			return true;
		}

		return set_capacity( n );
	}
	inline bool resize( size_t n )
	{
		// reserve space
		if( !reserve( n ) )
		{
			return false;
		}

		// destroy old elements
		for( size_t i = mSize; i > n; --i )
		{
			destroy( mBuffer[ i - 1 ] );
		}

		// construct new elements
		for( size_t i = mSize; i < n; ++i )
		{
			construct( mBuffer[ i ] );
		}

		// specify the new size
		mSize = n;

		// tell the caller about the victory
		return true;
	}

	inline size_t size() const
	{
		return mSize;
	}
	inline size_t capacity() const
	{
		return mCapacity;
	}

	inline iterator begin()
	{
		return const_cast< iterator >( cbegin() );
	}
	inline iterator end()
	{
		return const_cast< iterator >( cend() );
	}

	inline const_iterator begin() const
	{
		return cbegin();
	}
	inline const_iterator end() const
	{
		return cend();
	}

	inline const_iterator cbegin() const
	{
		return mBuffer;
	}
	inline const_iterator cend() const
	{
		return mBuffer + mSize;
	}

	inline reference operator[]( size_t i )
	{
		return mBuffer[ i ];
	}

private:
	size_t mSize = 0;
	size_t mCapacity = 0;
	T *mBuffer = nullptr;
};

/*
===============================================================================

	TYPES AND MATH

===============================================================================
*/
inline float degrees( float x ) { return x/180.0f*3.1415926535f; }

#if SIMD_ENABLED
# if X86
struct v128
{
	union
	{
		__m128 v;
		float f[ 4 ];
	};

	inline v128(): v( _mm_setzero_ps() )
	{
	}
	inline v128( float x ): v( _mm_set_ps1( x ) )
	{
	}
	inline v128( float x, float y, float z, float w ):
	v( _mm_set_ps( w, z, y, x ) )
	{
	}
	inline v128( const v128 &x ): v( x.v )
	{
	}
	inline v128( const __m128 x ): v( x )
	{
	}

	inline v128 operator+( const v128 &x ) const
	{
		return v128( _mm_add_ps( v, x.v ) );
	}
	inline v128 operator-( const v128 &x ) const
	{
		return v128( _mm_sub_ps( v, x.v ) );
	}
	inline v128 operator*( const v128 &x ) const
	{
		return v128( _mm_mul_ps( v, x.v ) );
	}
	inline v128 operator/( const v128 &x ) const
	{
		return v128( _mm_div_ps( v, x.v ) );
	}
	inline v128 operator-() const
	{
		return v128( _mm_sub_ps( _mm_setzero_ps(), v ) );
	}

	inline v128 &operator=( const v128 &x )
	{
		v = x.v;
		return *this;
	}
	inline v128 &operator+=( const v128 &x )
	{
		v = _mm_add_ps( v, x.v );
		return *this;
	}
	inline v128 &operator-=( const v128 &x )
	{
		v = _mm_sub_ps( v, x.v );
		return *this;
	}
	inline v128 &operator*=( const v128 &x )
	{
		v = _mm_mul_ps( v, x.v );
		return *this;
	}
	inline v128 &operator/=( const v128 &x )
	{
		v = _mm_div_ps( v, x.v );
		return *this;
	}
};
inline v128 operator*( float a, const v128 &b )
{
	return b*a;
}
inline v128 operator/( float a, const v128 &b )
{
	return v128( a )/b;
}
# else
#  error FIXME: Implement v128 for this architecture
# endif
#else
struct v128
{
	union
	{
		float f[ 4 ];
	};

	inline v128()
	{
		f[ 0 ] = 0;
		f[ 1 ] = 0;
		f[ 2 ] = 0;
		f[ 3 ] = 0;
	}
	inline v128( float x )
	{
		f[ 0 ] = x;
		f[ 1 ] = x;
		f[ 2 ] = x;
		f[ 3 ] = x;
	}
	inline v128( float x, float y, float z, float w )
	{
		f[ 0 ] = x;
		f[ 1 ] = y;
		f[ 2 ] = z;
		f[ 3 ] = w;
	}
	inline v128( const v128 &x )
	{
		f[ 0 ] = x.f[ 0 ];
		f[ 1 ] = x.f[ 1 ];
		f[ 2 ] = x.f[ 2 ];
		f[ 3 ] = x.f[ 3 ];
	}

	inline v128 operator+( const v128 &x ) const
	{
		return v128
		(
			f[ 0 ] + x.f[ 0 ], f[ 1 ] + x.f[ 1 ],
			f[ 2 ] + x.f[ 2 ], f[ 3 ] + x.f[ 3 ]
		);
	}
	inline v128 operator-( const v128 &x ) const
	{
		return v128
		(
			f[ 0 ] - x.f[ 0 ], f[ 1 ] - x.f[ 1 ],
			f[ 2 ] - x.f[ 2 ], f[ 3 ] - x.f[ 3 ]
		);
	}
	inline v128 operator*( const v128 &x ) const
	{
		return v128
		(
			f[ 0 ]*x.f[ 0 ], f[ 1 ]*x.f[ 1 ],
			f[ 2 ]*x.f[ 2 ], f[ 3 ]*x.f[ 3 ]
		);
	}
	inline v128 operator/( const v128 &x ) const
	{
		return v128
		(
			f[ 0 ]/x.f[ 0 ], f[ 1 ]/x.f[ 1 ],
			f[ 2 ]/x.f[ 2 ], f[ 3 ]/x.f[ 3 ]
		);
	}
	inline v128 operator-() const
	{
		return v128( -f[ 0 ], -f[ 1 ], -f[ 2 ], -f[ 3 ] );
	}

	inline v128 &operator=( const v128 &x )
	{
		f[ 0 ] = x.f[ 0 ];
		f[ 1 ] = x.f[ 1 ];
		f[ 2 ] = x.f[ 2 ];
		f[ 3 ] = x.f[ 3 ];
		return *this;
	}
	inline v128 &operator+=( const v128 &x )
	{
		f[ 0 ] += x.f[ 0 ];
		f[ 1 ] += x.f[ 1 ];
		f[ 2 ] += x.f[ 2 ];
		f[ 3 ] += x.f[ 3 ];
		return *this;
	}
	inline v128 &operator-=( const v128 &x )
	{
		f[ 0 ] -= x.f[ 0 ];
		f[ 1 ] -= x.f[ 1 ];
		f[ 2 ] -= x.f[ 2 ];
		f[ 3 ] -= x.f[ 3 ];
		return *this;
	}
	inline v128 &operator*=( const v128 &x )
	{
		f[ 0 ] *= x.f[ 0 ];
		f[ 1 ] *= x.f[ 1 ];
		f[ 2 ] *= x.f[ 2 ];
		f[ 3 ] *= x.f[ 3 ];
		return *this;
	}
	inline v128 &operator/=( const v128 &x )
	{
		f[ 0 ] /= x.f[ 0 ];
		f[ 1 ] /= x.f[ 1 ];
		f[ 2 ] /= x.f[ 2 ];
		f[ 3 ] /= x.f[ 3 ];
		return *this;
	}
};
inline v128 operator*( float a, const v128 &b )
{
	return b*a;
}
inline v128 operator/( float a, const v128 &b )
{
	return v128( a )/b;
}
#endif
static_assert( sizeof( v128 ) == 16, "v128 is not 128-bit" );

typedef v128 PackedScalar;

/*
===============================================================================

	BASIC 3D VECTOR

===============================================================================
*/
template< typename T >
struct TVector3
{
	T x = 0.0f, y = 0.0f, z = 0.0f;

	inline TVector3()
	{
	}
	explicit inline TVector3( T v ): x( v ), y( v ), z( v )
	{
	}
	inline TVector3( T x, T y, T z ): x( x ), y( y ), z( z )
	{
	}
	inline TVector3( const TVector3 &v ): x( v.x ), y( v.y ), z( v.z )
	{
	}
	inline ~TVector3()
	{
	}

	inline TVector3 &operator=( const TVector3 &v )
	{
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}
	inline TVector3 &operator+=( const TVector3 &v )
	{
		*this = *this + v;
		return *this;
	}
	inline TVector3 &operator-=( const TVector3 &v )
	{
		*this = *this - v;
		return *this;
	}
	inline TVector3 &operator*=( const TVector3 &v )
	{
		*this = *this*v;
		return *this;
	}
	inline TVector3 &operator/=( const TVector3 &v )
	{
		*this = *this/v;
		return *this;
	}
	inline TVector3 &operator*=( float s )
	{
		*this = *this*s;
		return *this;
	}
	inline TVector3 &operator/=( float s )
	{
		*this = *this/s;
		return *this;
	}

	inline TVector3 operator+( const TVector3 &v ) const
	{
		return TVector3( x + v.x, y + v.y, z + v.z );
	}
	inline TVector3 operator-( const TVector3 &v ) const
	{
		return TVector3( x - v.x, y - v.y, z - v.z );
	}
	inline TVector3 operator*( const TVector3 &v ) const
	{
		return TVector3( x*v.x, y*v.y, z*v.z );
	}
	inline TVector3 operator/( const TVector3 &v ) const
	{
		return TVector3( x/v.x, y/v.y, z/v.z );
	}
	inline TVector3 operator*( T s ) const
	{
		return TVector3( x*s, y*s, z*s );
	}
	inline TVector3 operator/( T s ) const
	{
		const T r = 1.0f/s;
		return TVector3( x*r, y*r, z*r );
	}
};

typedef TVector3< float > Vector3;
typedef TVector3< v128 > PackedVector3;

template< typename T >
inline TVector3< T > operator*( T a, const TVector3< T > &b )
{
	return b*a;
}

template< typename T >
inline TVector3< T > Cross( const TVector3< T > &a, const TVector3< T > &b )
{
	return
		TVector3< T >
		(
			a.y*b.z - b.y*a.z,
			a.z*b.x - b.z*a.x,
			a.x*b.y - b.x*a.y
		);
}

/*
===============================================================================

	BASIC QUATERNION

===============================================================================
*/
template< typename T >
struct TQuaternion
{
	T x = 0.0f, y = 0.0f, z = 0.0f;
	T w = 1.0f;

	inline TQuaternion()
	{
	}
	inline TQuaternion( T x, T y, T z, T w ): x( x ), y( y ),
	z( z ), w( w )
	{
	}
	inline TQuaternion( const TQuaternion &v ): x( v.x ), y( v.y ), z( v.z ),
	w( v.w )
	{
	}

	inline TQuaternion &operator=( const TQuaternion &v )
	{
		x = v.x;
		y = v.y;
		z = v.z;
		w = v.w;

		return *this;
	}

	inline TQuaternion &SetAxisAngle( T angle, const TVector3< T > &axis )
	{
		const T sha = sin( degrees( angle/2 ) );
		const T cha = cos( degrees( angle/2 ) );
		*this = TQuaternion( axis.x*sha, axis.y*sha, axis.z*sha, cha );
		return *this;
	}
	inline TQuaternion &SetAxisAngle( T angle, T x, T y, T z )
	{
		return SetAxisAngle( angle, TVector3< T >( x, y, z ) );
	}
	inline TQuaternion &SetEuler( const TVector3< T > &euler )
	{
		TQuaternion qx, qy, qz;

		qx.SetAxisAngle( euler.x, 1, 0, 0 );
		qy.SetAxisAngle( euler.y, 0, 1, 0 );
		qz.SetAxisAngle( euler.z, 0, 0, 1 );

		*this = qz*qx*qy;
		return *this;
	}
	inline TQuaternion &SetEuler( T pitch, T yaw, T roll )
	{
		return SetEuler( TVector3< T >( pitch, yaw, roll ) );
	}

	inline TQuaternion operator*( const TQuaternion &q ) const
	{
		return
			TQuaternion
			(
				 x*q.w + y*q.z - z*q.y + w*q.x,
				-x*q.z + y*q.w + z*q.x + w*q.y,
				 x*q.y - y*q.x + z*q.w + w*q.z,
				-x*q.x - y*q.y - z*q.z + w*q.w
			);
	}
	inline TVector3< T > operator*( const TVector3< T > &v ) const
	{
		const TVector3< T > t = Cross( TVector3< T >( x, y, z ), v )*2.0f;
		return v + w*t + Cross( TVector3< T >( x, y, z ), t ); 
	}
};

typedef TQuaternion< float > Quaternion;
typedef TQuaternion< v128 > PackedQuaternion;

template< typename T >
inline TQuaternion< T > Conjugate( const TQuaternion< T > &q )
{
	return TQuaternion< T >( -q.x, -q.y, -q.z, q.w );
}

/*
===============================================================================

	BASIC TRANSFORMATION

===============================================================================
*/
namespace Detail
{

	template< typename T >
	struct ScalarTraits
	{
		static const size_t kCount = 1;
	};

	template<>
	struct ScalarTraits< v128 >
	{
		static const size_t kCount = 4;
	};

}

template< typename T >
struct TTransform
{
	typedef T scalar_type;
	typedef TVector3< T > vector_type;
	typedef TQuaternion< T > quaternion_type;

	static const size_t kTransformCount = Detail::ScalarTraits< T >::kCount;

	vector_type pos;
	scalar_type scale;
	quaternion_type rot;
	
	inline TTransform(): pos(), scale(), rot()
	{
	}
	inline ~TTransform()
	{
	}

	inline TTransform &SetEuler( T pitch, T yaw, T roll )
	{
		rot.SetEuler( pitch, yaw, roll );
		return *this;
	}
	inline TTransform &SetEuler( const TVector3< T > &v )
	{
		rot.SetEuler( v );
		return *this;
	}

	inline TTransform &SetAxisAngle( T angle, T x_, T y_, T z_ )
	{
		rot.SetAxisAngle( angle, x_, y_, z_ );
		return *this;
	}
	inline TTransform &SetAxisAngle( T angle, const TVector3< T > &axis )
	{
		rot.SetAxisAngle( angle, axis );
		return *this;
	}

	inline TTransform &Teleport( T x_, T y_, T z_ )
	{
		pos.x = x_;
		pos.y = y_;
		pos.z = z_;
		
		return *this;
	}
	inline TTransform &Teleport( const TVector3< T > &v )
	{
		return Teleport( v.x, v.y, v.z );
	}

	inline TTransform &Move( const TVector3< T > &v )
	{
		const TVector3< T > u = rot*v;
		pos.x = u.x;
		pos.y = u.y;
		pos.z = u.z;

		return *this;
	}
	inline TTransform &Move( T x_, T y_, T z_ )
	{
		return Move( TVector3< T >( x_, y_, z_ ) );
	}

	inline TTransform &Turn( T x_, T y_, T z_ )
	{
		rot = rot*TQuaternion< T >().SetEuler( x_, y_, z_ );
		return *this;
	}
	inline TTransform &Turn( const TQuaternion< T > &q )
	{
		rot = rot*q;
		return *this;
	}

	inline TTransform &Multiply( TTransform &dst, const TTransform &src ) const
	{
		dst = *this;

		dst.Move( src.pos );
		dst.Turn( src.rot );
		dst.scale = src.scale*scale;

		return dst;
	}
};

typedef TTransform< float > Transform;
typedef TTransform< PackedScalar > PackedTransform;

/*
===============================================================================

	SOA TRANSFORMATION SYSTEM

===============================================================================
*/

/*
// Packed transformations (collection of four transforms)
struct PackedTransform
{
	static size_t kTransformCount = 4;
	typedef v128 vector_type;

	PackedVector3 pos;
	PackedScalar scale;
	PackedQuaternion rot;
};
*/
static_assert( sizeof( PackedTransform ) == 8*4*4, "Unexpected size" );
static_assert( sizeof( PackedTransform )%64 == 0, "Not cache-line aligned" );

template< typename Dst, typename Src >
struct CopyConst
{
	typedef Dst type; //no attribute to copy
};
template< typename Dst, typename Src >
struct CopyConst< const Dst, Src >
{
	typedef Dst type; //removes const
};

template< typename Dst, typename Src >
struct CopyConst< Dst, const Src >
{
	typedef const Dst type; //adds const
};
template< typename Dst, typename Src >
struct CopyConst< const Dst, const Src > //is this even necessary?
{
	typedef const Dst type;
};

// Collection of transformations
struct TransformArrays
{
	// Type of the packed transform
	typedef PackedTransform transform_type;
	// Number of transforms stored in the transform_type
	static const size_t kTransformCount = transform_type::kTransformCount;

	// Resize granularity (used to improve successive resize performance)
	static const size_t kResizeGranularity = 32;

	// Current number of valid transforms stored
	size_t size = 0;
	// Maximum number of transforms available without re-allocation
	size_t capacity = 0;

	// Collection of packed transforms
	transform_type *transforms = nullptr;
	// Cached derived transforms
	transform_type *derivedTransformsCache = nullptr;
	// List of free indexes
	unsigned int *freeList = nullptr;
	// Current pointer into the free list stack
	unsigned int freeListIndex = 0;

	// Retrieve the number of packed transforms required for the given transform
	// count
	inline static size_t GetPackedTransformCount( size_t n )
	{
		static const size_t A = kTransformCount;
		return ( n + ( A - 1 ) )/A;
	}
	// Retrieve the number of details required for the given transform count
	// (this will be aligned to match the number of packed transforms)
	inline static size_t GetDetailsCount( size_t n )
	{
		static const size_t A = kTransformCount;
		return n + ( A - ( n%A ) )%A;
	}

	// Create a packed transform where each stored transform matches the
	// transform at the index specified
	inline PackedTransform &ExtractPackedTransform( PackedTransform &dst,
	size_t index, bool global = false ) const
	{
		static const size_t A = kTransformCount;

		ASSERT( index < size );

		// Store the relevant indexes
		const size_t offset = ( capacity/A )*( +global );
		const size_t i = index/A + offset; //index into transforms array
		const size_t j = index%A; //component index into vector

#if SIMD_ENABLED && X86
		// A different intrinsic needs to be used for N-wide SIMD
		static_assert( A == 4, "This code is for 4-component SIMD" );

		// SSE lets us replicate a single float across all components of a
		// vector in one go
		dst.pos.x.v = _mm_set_ps1( transforms[ i ].pos.x.f[ j ] );
		dst.pos.y.v = _mm_set_ps1( transforms[ i ].pos.y.f[ j ] );
		dst.pos.z.v = _mm_set_ps1( transforms[ i ].pos.z.f[ j ] );
		dst.scale.v = _mm_set_ps1( transforms[ i ].scale.f[ j ] );
		dst.rot.x.v = _mm_set_ps1( transforms[ i ].rot.x.f[ j ] );
		dst.rot.y.v = _mm_set_ps1( transforms[ i ].rot.y.f[ j ] );
		dst.rot.z.v = _mm_set_ps1( transforms[ i ].rot.z.f[ j ] );
		dst.rot.w.v = _mm_set_ps1( transforms[ i ].rot.w.f[ j ] );
#else
		// Generic method

		// 1. Store the relevant component of the source to the first component
		//    of the destination
		dst.pos.x.f[ 0 ] = transforms[ i ].pos.x.f[ j ];
		dst.pos.y.f[ 0 ] = transforms[ i ].pos.y.f[ j ];
		dst.pos.z.f[ 0 ] = transforms[ i ].pos.z.f[ j ];
		dst.scale.f[ 0 ] = transforms[ i ].scale.f[ j ];
		dst.rot.x.f[ 0 ] = transforms[ i ].rot.x.f[ j ];
		dst.rot.y.f[ 0 ] = transforms[ i ].rot.y.f[ j ];
		dst.rot.z.f[ 0 ] = transforms[ i ].rot.z.f[ j ];
		dst.rot.w.f[ 0 ] = transforms[ i ].rot.w.f[ j ];

		// 2. Replicate the stored component across all remaining components
		for( size_t k = 1; k < A; ++k )
		{
			dst.pos.x.f[ k ] = dst.pos.x.f[ 0 ];
			dst.pos.y.f[ k ] = dst.pos.y.f[ 0 ];
			dst.pos.z.f[ k ] = dst.pos.z.f[ 0 ];
			dst.scale.f[ k ] = dst.scale.f[ 0 ];
			dst.rot.x.f[ k ] = dst.rot.x.f[ 0 ];
			dst.rot.y.f[ k ] = dst.rot.y.f[ 0 ];
			dst.rot.z.f[ k ] = dst.rot.z.f[ 0 ];
			dst.rot.w.f[ k ] = dst.rot.w.f[ 0 ];
		}
#endif

		// Done
		return dst;
	}
	// Update the derived cache
	inline void UpdateTransforms( const PackedTransform &prnt )
	{
		if( !size )
		{
			return;
		}

		const PackedTransform *src = transforms;
		PackedTransform *dst = derivedTransformsCache;

		ASSERT( src != nullptr );
		ASSERT( dst != nullptr );

		// Run through each packed transform
		size_t n = GetPackedTransformCount( size );
		while( n-- > 0 )
		{
			// TODO: Prefetch the next dst and src here

			// Perform the update (updates four at once)
			prnt.Multiply( *dst, *src );

			// TODO: Schedule an UpdateTransforms() job for all sub-transforms
			// -     of src (or a list of jobs for better CPU utilization; one
			// -     job per 'Multiply')

			// Go to the next one
			++src;
			++dst;
		}
	}

	// Retrieve the position from the given transform
	inline Vector3 GetPosition( size_t index ) const
	{
		ASSERT( index < size );

		return
			Vector3
			(
				transforms[ index/4 ].pos.x.f[ index%4 ],
				transforms[ index/4 ].pos.y.f[ index%4 ],
				transforms[ index/4 ].pos.z.f[ index%4 ]
			);
	}
	// Retrieve the scale from the given transform
	inline float GetScale( size_t index ) const
	{
		ASSERT( index < size );

		return transforms[ index/4 ].scale.f[ index%4 ];
	}
	// Retrieve the rotation from the given transform
	inline Quaternion GetRotation( size_t index ) const
	{
		ASSERT( index < size );

		return
			Quaternion
			(
				transforms[ index/4 ].rot.x.f[ index%4 ],
				transforms[ index/4 ].rot.y.f[ index%4 ],
				transforms[ index/4 ].rot.z.f[ index%4 ],
				transforms[ index/4 ].rot.w.f[ index%4 ]
			);
	}

	// Set the position of the given transform
	inline void SetPosition( size_t index, const Vector3 &v )
	{
		ASSERT( index < size );

		transforms[ index/4 ].pos.x.f[ index%4 ] = v.x;
		transforms[ index/4 ].pos.y.f[ index%4 ] = v.y;
		transforms[ index/4 ].pos.z.f[ index%4 ] = v.z;
	}
	// Set the scale of the given transform
	inline void SetScale( size_t index, float s )
	{
		ASSERT( index < size );

		transforms[ index/4 ].scale.f[ index%4 ] = s;
	}
	// Set the rotation of the given transform
	inline void SetRotation( size_t index, const Quaternion &q )
	{
		ASSERT( index < size );

		transforms[ index/4 ].rot.x.f[ index%4 ] = q.x;
		transforms[ index/4 ].rot.y.f[ index%4 ] = q.y;
		transforms[ index/4 ].rot.z.f[ index%4 ] = q.z;
		transforms[ index/4 ].rot.w.f[ index%4 ] = q.w;
	}

	// Set the capacity for the buffers (in number of transforms)
	inline bool SetCapacity( size_t newCapacity )
	{
		const size_t oldPCount = GetPackedTransformCount( capacity );
		const size_t oldDCount = GetDetailsCount( capacity );
		const size_t newPCount = GetPackedTransformCount( newCapacity );
		const size_t newDCount = GetDetailsCount( newCapacity );

		if( oldPCount == newPCount || oldDCount == newDCount )
		{
			return true;
		}

		auto *const oldTransforms = transforms;
		auto *const oldFreeList = freeList;

		if( !SetMemory( transforms, newPCount*2 ) )
		{
			return false;
		}
		if( !SetMemory( freeList, newDCount ) )
		{
			SetMemory( transforms, 0 );
			transforms = oldTransforms;

			return false;
		}

		derivedTransformsCache = transforms + newPCount;

		const size_t copyCount = size < newCapacity ? size : newCapacity;
		const size_t copyPCount = GetPackedTransformCount( copyCount );
		const size_t copyDCount = GetDetailsCount( copyCount );

		Copy( transforms, oldTransforms, copyPCount );
		Copy( freeList, oldFreeList, copyDCount );

		SetMemory( oldTransforms, 0 );
		SetMemory( oldFreeList, 0 );

		return true;
	}
	// Adjust the capacity for at least 'newCapacity' elements, or do nothing if
	// there is already enough space for them
	inline bool Reserve( size_t newCapacity )
	{
		if( newCapacity > capacity )
		{
			return SetCapacity( newCapacity );
		}

		return true;
	}
	// Adjust the number of stored transforms
	inline bool Resize( size_t newSize )
	{
		static const size_t A = kResizeGranularity;

		// Reserve space for the items that are about to be added
		if( !Reserve( newSize + ( A - newSize%A ) ) )
		{
			// Was unable to reserve extra space, so return failure
			return false;
		}

		// Determine whether the free list may need to be fixed
		if( newSize < size )
		{
			// There's a chance the current free list may have invalid entries
			for( size_t i = 0; i < freeListIndex; ++i )
			{
				// Check if the item is valid
				if( freeList[ i ] < newSize )
				{
					// This item is valid so skip it
					continue;
				}

				// This item is not valid, so remove it
				for( size_t j = i + 1; j < freeListIndex; ++j )
				{
					// Shift all items after it down
					freeList[ j - 1 ] = freeList[ j ];
				}

				// Record the loss of an item
				--freeListIndex; //impossible for this to be zero
			}
		}

		// The current free list index should be less than the size of the old
		// free list
		ASSERT( freeListIndex <= size );

		// We have some free indexes to spare
		for( size_t i = size; i < newSize; ++i )
		{
			// Add the free index
			freeList[ freeListIndex++ ] = i;
		}

		// Record the new size
		size = newSize;

		// Successfully resized
		return true;
	}

private:
	template< typename T >
	inline static void Copy( T *dst, const T *src, size_t n )
	{
		if( !n )
		{
			return;
		}

		ASSERT( dst != nullptr );
		ASSERT( src != nullptr );

		memcpy( ( void * )dst, ( const void * )src, n*sizeof( T ) );
	}

	template< typename T >
	inline static bool SetMemory( T *&ptr, size_t n )
	{
		if( !n )
		{
			Deallocate( ( void * )ptr );
			ptr = nullptr;
			return true;
		}

		T *x = ( T * )Allocate( n*sizeof( T ) );
		if( !x )
		{
			return false;
		}

		ptr = x;
		return true;
	}
	template< typename T >
	inline static bool SetMemory( T *const &ptr, size_t n )
	{
		T *newPtr = ptr;
		return SetMemory( newPtr, n );
	}

	inline static void *Allocate( size_t n )
	{
		if( !n )
		{
			return nullptr;
		}

#if SIMD_ENABLED
# if defined( _WIN32 )
		return _aligned_malloc( n, 64 ); //let's do cache-line on Windows
# elif defined( __APPLE__ )
		// Mac OS X's malloc() always returns 16-byte aligned memory
		return malloc( n );
# else
#  error FIXME: Implement Allocate() for this platform
# endif
#else
		return malloc( n );
#endif
	}
	inline static void Deallocate( void *p )
	{
		if( !p )
		{
			return;
		}

#if SIMD_ENABLED
# if defined( _WIN32 )
		_aligned_free( p );
# elif defined( __APPLE__ )
		free( p );
# else
#  error FIXME: Implement Deallocate() for this platform
# endif
#else
		free( p );
#endif
	}
};

#if ENTITY_STRUCTURE == ENTSTRUC_SOA
# undef ENTITY_STRUCTURE
# define ENTITY_STRUCTURE ENTSTRUC_INTRUSIVE_LIST
# define SOA 1
#else
# define SOA 0
#endif

#if ENTITY_STRUCTURE == ENTSTRUC_VECTOR_POINTER
# define ENTITY_VECTOR_ENABLED 1
#elif ENTITY_STRUCTURE == ENTSTRUC_VECTOR_ITERATOR
# define ENTITY_VECTOR_ENABLED 1
#elif ENTITY_STRUCTURE == ENTSTRUC_CUSTOM_VECTOR
# define ENTITY_VECTOR_ENABLED 1
#else
# define ENTITY_VECTOR_ENABLED 0
#endif

#if ENTITY_STRUCTURE == ENTSTRUC_VECTOR_POINTER
# define ENTITY_CONTAINER_ENABLED 1
#elif ENTITY_STRUCTURE == ENTSTRUC_VECTOR_ITERATOR
# define ENTITY_CONTAINER_ENABLED 1
#elif ENTITY_STRUCTURE == ENTSTRUC_STANDARD_LIST
# define ENTITY_CONTAINER_ENABLED 1
#elif ENTITY_STRUCTURE == ENTSTRUC_CUSTOM_VECTOR
# define ENTITY_CONTAINER_ENABLED 1
#else
# define ENTITY_CONTAINER_ENABLED 0
#endif

#if SOA
# define ENTITY_HAS_TRANSFORM 0
#else
# define ENTITY_HAS_TRANSFORM 1
#endif
#if ENTITY_VECTOR_ENABLED
# define ENTITY_HAS_PARENT_INDEX 1
#else
# define ENTITY_HAS_PARENT_INDEX 0
#endif

struct Entity
{
	std::string name;
#if ENTITY_HAS_PARENT_INDEX
	size_t parentIndex = ( size_t )-1;
#endif
	Entity *parent = nullptr;

	size_t globalsIndex = ( size_t )-1;
#if ENTITY_HAS_TRANSFORM
	Transform transform;
#else
	TransformArrays childTransforms;
	size_t transformIndex = ( size_t )-1;
#endif

#if ENTITY_VECTOR_ENABLED
# if ENTITY_STRUCTURE == ENTSTRUC_VECTOR_POINTER
	typedef Entity *iterator;
	std::vector< Entity > children;
# elif ENTITY_STRUCTURE == ENTSTRUC_VECTOR_ITERATOR
	typedef std::vector< Entity >::iterator iterator;
	std::vector< Entity > children;
# elif ENTITY_STRUCTURE == ENTSTRUC_CUSTOM_VECTOR
	typedef custom_vector< Entity >::iterator iterator;
	custom_vector< Entity > children;
# endif
#elif ENTITY_STRUCTURE == ENTSTRUC_INTRUSIVE_LIST
	Entity *head = nullptr;
	Entity *tail = nullptr;
	Entity *prev = nullptr;
	Entity *next = nullptr;

	struct iterator
	{
		Entity *p;

		inline iterator( Entity *ptr = nullptr ): p( ptr ) {}

		inline iterator &operator--() { p = p->prev; return *this; }
		inline iterator &operator++() { p = p->next; return *this; }
		inline iterator operator--( int ) const { return iterator( p->prev ); }
		inline iterator operator++( int ) const { return iterator( p->next ); }

		inline Entity *operator->() { return p; }
		inline Entity &operator*() { return *p; }

		inline bool operator==( const iterator &x ) const { return p == x.p; }
		inline bool operator!=( const iterator &x ) const { return p != x.p; }
	};
#elif ENTITY_STRUCTURE == ENTSTRUC_STANDARD_LIST
	typedef std::list< Entity >::iterator iterator;
	std::list< Entity > children;
#endif

#if ENTITY_HAS_TRANSFORM
	static std::vector< Transform > globalTransforms;
#endif
	static Entity root;

	inline Entity()
	{
	}
	inline ~Entity()
	{
		FiniTransformIndex();

#if ENTITY_STRUCTURE == ENTSTRUC_INTRUSIVE_LIST
		Remove();
#endif
	}

	Entity( const Entity &x ) = delete; // NO!
	Entity &operator=( const Entity &x ) = delete; // NO!

	inline Entity &operator=( Entity &&x )
	{
		name = std::move( x.name );
		globalsIndex = x.globalsIndex;
#if ENTITY_VECTOR_ENABLED
		parentIndex = x.parentIndex;
#endif
		parent = x.parent;

#if ENTITY_CONTAINER_ENABLED
		children = std::move( x.children );

		for( Entity &child : children )
		{
			child.parent = this;
		}
#elif ENTITY_STRUCTURE == ENTSTRUC_INTRUSIVE_LIST
		head = x.head;
		tail = x.tail;

		x.head = nullptr;
		x.tail = nullptr;

		for( Entity *p = head; p != nullptr; p = p->next )
		{
			p->parent = this;
		}
#endif

#if ENTITY_HAS_TRANSFORM
		transform = x.transform;
#else
		transformIndex = x.transformIndex;
#endif
		
#if ENTITY_VECTOR_ENABLED
		x.parentIndex = ( size_t )-1;
#endif
		x.parent = nullptr;

		return *this;
	}
	inline Entity( Entity &&x )
	{
		*this = std::move( x );
	}

	inline iterator begin()
	{
#if ENTITY_CONTAINER_ENABLED
		if( children.empty() )
		{
# if ENTITY_STRUCTURE == ENTSTRUC_VECTOR_POINTER
			return nullptr;
# else
			return children.end();
# endif
		}
#endif

#if ENTITY_STRUCTURE == ENTSTRUC_VECTOR_POINTER
		return &children.front();
#elif ENTITY_STRUCTURE == ENTSTRUC_VECTOR_ITERATOR
		return children.begin();
#elif ENTITY_STRUCTURE == ENTSTRUC_INTRUSIVE_LIST
		return iterator( head );
#elif ENTITY_STRUCTURE == ENTSTRUC_STANDARD_LIST
		return children.begin();
#elif ENTITY_STRUCTURE == ENTSTRUC_CUSTOM_VECTOR
		return children.begin();
#endif
	}
	inline iterator end()
	{
#if ENTITY_CONTAINER_ENABLED
		if( children.empty() )
		{
# if ENTITY_STRUCTURE == ENTSTRUC_VECTOR_POINTER
			return nullptr;
# else
			return children.end();
# endif
		}
#endif

#if ENTITY_STRUCTURE == ENTSTRUC_VECTOR_POINTER
		return &children.front() + children.size();
#elif ENTITY_STRUCTURE == ENTSTRUC_VECTOR_ITERATOR
		return children.end();
#elif ENTITY_STRUCTURE == ENTSTRUC_INTRUSIVE_LIST
		return iterator( tail );
#elif ENTITY_STRUCTURE == ENTSTRUC_STANDARD_LIST
		return children.end();
#elif ENTITY_STRUCTURE == ENTSTRUC_CUSTOM_VECTOR
		return children.end();
#endif
	}
	
	inline Entity *AddChild()
	{
#if ENTITY_CONTAINER_ENABLED
# if ENTITY_VECTOR_ENABLED
		const size_t index = children.size();
		children.resize( index + 1 );
# else
		children.emplace_back( std::move( Entity() ) );
# endif
		Entity &child = children.back();
# if ENTITY_VECTOR_ENABLED
		child.parentIndex = index;
# endif
		child.parent = this;
		child.InitTransformIndex();
		return &child;
#elif ENTITY_STRUCTURE == ENTSTRUC_INTRUSIVE_LIST
		Entity *p = new Entity();
		p->parent = this;
		p->prev = tail;
		p->next = nullptr;
		if( p->prev != nullptr )
		{
			p->prev->next = p;
		}
		else
		{
			head = p;
		}
		tail = p;
		if( !p->InitTransformIndex() )
		{
			delete p;
			return nullptr;
		}
		return p;
#endif
	}

	inline void SetEuler( float pitch, float yaw, float roll )
	{
#if ENTITY_HAS_TRANSFORM
		transform.SetEuler( pitch, yaw, roll );
#else
		parent->childTransforms.SetRotation(
			transformIndex,
			Quaternion().SetEuler( pitch, yaw, roll )
		);
#endif
	}
	inline void Teleport( float x, float y, float z )
	{
#if ENTITY_HAS_TRANSFORM
		transform.Teleport( x, y, z );
#else
		parent->childTransforms.SetPosition(
			transformIndex,
			Vector3( x, y, z )
		);
#endif
	}
	inline void Move( float x, float y, float z )
	{
#if ENTITY_HAS_TRANSFORM
		transform.Move( x, y, z );
#else
		const Vector3 v( x, y, z );

		parent->childTransforms.SetPosition(
			transformIndex,
			parent->childTransforms.GetRotation( transformIndex )*v
		);
#endif
	}
	inline void Turn( float x, float y, float z )
	{
#if ENTITY_HAS_TRANSFORM
		transform.Turn( x, y, z );
#else
		const Quaternion &q = Quaternion().SetEuler( x, y, z );

		parent->childTransforms.SetRotation(
			transformIndex,
			parent->childTransforms.GetRotation( transformIndex )*q
		);
#endif
	}

private:
	inline void Remove()
	{
#if ENTITY_VECTOR_ENABLED
		Entity *const prnt = parent;
		parent = nullptr;

		if( !prnt )
		{
			return;
		}
		if( parentIndex == ( size_t )-1 )
		{
			return;
		}
		
		const size_t count = prnt->children.size();
		if( parentIndex + 1 >= count )
		{
			return;
		}

		while( begin() != end() )
		{
			begin()->Remove();
		}

		const size_t lastChild = count - 1;
		prnt->children[ parentIndex ] =
			std::move( prnt->children[ lastChild ] );
#elif ENTITY_STRUCTURE == ENTSTRUC_INTRUSIVE_LIST
		if( !parent ) { return; }
		if( prev != nullptr )
		{
			prev->next = next;
		}
		else
		{
			parent->head = next;
		}
		if( next != nullptr )
		{
			next->prev = prev;
		}
		else
		{
			parent->tail = prev;
		}
#endif
	}

	inline bool InitTransformIndex()
	{
#if !ENTITY_HAS_TRANSFORM
		if( !parent )
		{
			return true;
		}

		if( parent->childTransforms.freeListIndex == 0 )
		{
			const size_t newSize = parent->childTransforms.size + 1;

			if( !parent->childTransforms.Resize( newSize ) )
			{
				return false;
			}

			ASSERT( parent->childTransforms.freeListIndex > 0 );
		}

		// pop an index from the free index stack
		ASSERT( parent->childTransforms.freeList != nullptr );
		transformIndex = parent->childTransforms.freeList
		[
			--parent->childTransforms.freeListIndex
		];
		ASSERT( transformIndex < parent->childTransforms.size );

		return true;
#else
		return true;
#endif
	}
	inline void FiniTransformIndex()
	{
#if !ENTITY_HAS_TRANSFORM
		if( !parent )
		{
			return;
		}

		ASSERT( transformIndex != ( size_t )-1 );

		size_t &freeListIndex = parent->childTransforms.freeListIndex;

		ASSERT( freeListIndex < parent->childTransforms.size );
		ASSERT( parent->childTransforms.freeList != nullptr );

		// push the index to the free index stack
		parent->childTransforms.freeList[ freeListIndex++ ] = transformIndex;
#endif
	}
};
#if !SOA
std::vector< Transform > Entity::globalTransforms;
#endif
Entity Entity::root;

static const unsigned int i_count = 20;
static const unsigned int j_count = 20;
static const unsigned int k_count = 20;
static const unsigned int l_count = 20;

static const unsigned int N = i_count*j_count*k_count*l_count;

#if ENTITY_HAS_TRANSFORM
static void TransformEntitiesOf_r( Entity &parent, const Transform &base )
{
	static unsigned int n = 0;
	static unsigned int nest_level = 0;
	unsigned int child_index = 0;

	++nest_level;

	auto &globals = Entity::globalTransforms;

#if ENTITY_VECTOR_ENABLED
	globals.reserve( globals.size() + parent.children.size() );
#endif
	for( Entity &child : parent )
	{
		//printf( "%u\t", n++ );
		++n;
		++child_index;

		if( n > N )
		{
			printf( "Enumerated too many: %u[%u] (cur:%u, max:%u)\n",
				nest_level, child_index, n, N );
			return; // HACK!!! THIS IS A HACK!!! *****************************
		}

		const size_t index = globals.size();
		globals.resize( index + 1 );

		Transform &xform = globals[ index ];
		child.transform.Multiply( xform, base );
		child.globalsIndex = index;

		Transform temp = xform;
		TransformEntitiesOf_r( child, temp );
	}

	--nest_level;
}
static void TransformEntities()
{
	Entity::globalTransforms.clear();
	TransformEntitiesOf_r( Entity::root, Transform() );
}
#else
static void TransformEntitiesOf_r( Entity &parent, PackedTransform &xf )
{
	parent.childTransforms.UpdateTransforms( xf );

	PackedTransform packedLocal;
	for( Entity &child : parent )
	{
		if( child.childTransforms.size == 0 )
		{
			continue;
		}

		parent.childTransforms.ExtractPackedTransform
		(
			packedLocal, child.transformIndex, true
		);

		TransformEntitiesOf_r( child, packedLocal );
	}
}
static void TransformEntities()
{
	static unsigned int n = 0;

	PackedTransform packedLocal;

	for( Entity &child : Entity::root )
	{
		if( n++ >= i_count )
		{
			printf( "Enumerated too many: cur:%u, max:%u\n", n, i_count );
			return; // HACK!!! THIS IS A HACK!!! *****************************
		}

		if( child.childTransforms.size == 0 )
		{
			continue;
		}

		Entity::root.childTransforms.ExtractPackedTransform
		(
			packedLocal, child.transformIndex, false
		);

		for( Entity &grandchild : child )
		{
			TransformEntitiesOf_r( grandchild, packedLocal );
		}
	}
}
#endif

#ifndef CONFIG_NAME
# if defined( DEBUG ) || defined( _DEBUG ) || defined( __debug__ )
#  define CONFIG_NAME "DEBUG"
# else
#  define CONFIG_NAME "RELEASE"
# endif
#endif

#ifndef METHOD_NAME
# if ENTITY_STRUCTURE == ENTSTRUC_VECTOR_POINTER
#  define METHOD_NAME "vector<> pointer"
# elif ENTITY_STRUCTURE == ENTSTRUC_VECTOR_ITERATOR
#  define METHOD_NAME "vector<> iterator"
# elif ENTITY_STRUCTURE == ENTSTRUC_INTRUSIVE_LIST
#  define METHOD_NAME "intrusive_list"
# elif ENTITY_STRUCTURE == ENTSTRUC_STANDARD_LIST
#  define METHOD_NAME "list<>"
# elif ENTITY_STRUCTURE == ENTSTRUC_CUSTOM_VECTOR
#  define METHOD_NAME "custom_vector<>"
# else
#  error XXX: Unknown ENTITY_STRUCTURE
# endif
#endif

int main()
{
	sdf::timer timer;

	printf( "Entity Test (%s using %s; %s %s)\n",
		CONFIG_NAME, METHOD_NAME, __DATE__, __TIME__ );
	printf( "sizeof( Entity ) = %u; Max entities = %u\n", sizeof( Entity ), N );

	// construct the scene
	printf( "Constructing scene...\n" );
	timer.reset();
	for( unsigned int i = 0; i < i_count; ++i )
	{
		Entity *a = Entity::root.AddChild();
		a->SetEuler( 20, 80, 40 );
		a->Teleport( -5.0f + ( float )i, 0.0f, -5.0f + ( float )i );
		for( unsigned int j = 0; j < j_count; ++j )
		{
			Entity *b = a->AddChild();
			b->SetEuler( 30, 60, 45 );
			b->Teleport( ( float )j, ( float )j, ( float )j );
			for( unsigned int k = 0; k < k_count; ++k )
			{
				Entity *c = b->AddChild();
				c->SetEuler
				(
					36.0f*( float )i, 36.0f*( float )j, 36.0f*( float )k
				);
				c->Teleport( ( float )i, ( float )j, ( float )k );
				for( unsigned int l = 0; l < l_count; ++l )
				{
					Entity *d = c->AddChild();
					d->SetEuler( 0.0f, -180.0f + 18.0f*( float )l, 0.0f );
					d->Teleport( 1, 2, 3 );
				}
			}
		}
	}
	const u64 constructionTime = timer();

	// transform the scene
	printf( "Transforming scene...\n" );

#if ENTITY_HAS_TRANSFORM
	Entity::globalTransforms.reserve( N );
#endif

	timer.reset();
	TransformEntities();
	const u64 transformationTime = timer();

	// time!
	printf( "Total construction time: %g seconds (%g milliseconds)\n",
		sdf::microseconds_to_seconds( constructionTime ),
		( ( f64 )constructionTime )/1000.0 );
	printf( "Total transformation time: %g seconds (%g milliseconds)\n",
		sdf::microseconds_to_seconds( transformationTime ),
		( ( f64 )transformationTime )/1000.0 );

	// done
	return EXIT_SUCCESS;
}
