#include "../taskshare/versions/Task4.h"

#include <ctime>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>

//------------------------------------------------------------------------------
// TASK IMPLEMENTATIONS

#include "../taskshare/test/Alloc.cpp"

//------------------------------------------------------------------------------
// USER TEST

#if 0
# define NUM_TESTS 1024
#else
# define NUM_TESTS 16
#endif

#include "../taskshare/test/IsPrime.cpp"

task::queue_t gMainQueue;
int gNumbers[NUM_TESTS];

task::threadResult_t THREADPROCAPI WorkerThread(void *) {
	task::Streamer(gMainQueue);
	return (task::threadResult_t)0;
}

task::threadResult_t THREADPROCAPI CatchAppHang(void *) {
	static const int hangSeconds = 2;

#if _WIN32
	Sleep(1000*hangSeconds);
#else
	task::f80 s, e;

	s = task::SampleTimer();
	do {
		task::Relinquish();
		e = task::SampleTimer();
	} while((e - s) < (task::f80)hangSeconds);
#endif

#if __GNUC__
	__asm__ __volatile__("int $3");
#else
	__asm { int 3 };
#endif
}

int main(int argc, char **argv) {
	task::perfQuery_t pq;
	task::thread_t threads[32];
	task::thread_t appHangThread;
	size_t i, n;
	bool info;
	bool justcores;
	bool dontrun;

	n = 0;
	info = false;
	justcores = false;
	dontrun = false;

	for(i=1; i<(size_t)argc; i++) {
		if (!strcmp(argv[i], "--info"))
			info = true;
		else if(!strcmp(argv[i], "--just-cores"))
			justcores = true;
		else if(!strcmp(argv[i], "--dont-run"))
			dontrun = true;
		else if(!strcmp(argv[i], "--no-info"))
			info = false;
		else if(!strcmp(argv[i], "--no-just-cores"))
			justcores = false;
		else if(!strcmp(argv[i], "--no-dont-run"))
			dontrun = false;
		else
			fprintf(stderr, "error: unknown option '%s'\n", argv[i]);
	}

	if (!n) {
		if (justcores)
			n = task::GetCPUCoreCount();
		else
			n = task::GetCPUThreadCount();
	}

	if (n > (sizeof(threads)/sizeof(threads[0])))
		n = (sizeof(threads)/sizeof(threads[0]));

	if (info) {
		printf("implementation: ");

#if defined(TASK_CONF_LOCKFREE)
# if TASK_CONF_LOCKFREE
		printf("lockfree");
# else
		printf("mutex");
# endif
#else
		printf("(undetermined)");
#endif

		printf(" (%i)\n", TASK_JOBQUEUE_VERSION);

		printf("compiled with: ");
#if __GNUC__
		printf("gcc (%i)", __GNUC__);
#elif __clang__
		printf("clang (%i)", __clang__);
#elif __INTEL_COMPILER
		printf("icc (%i)", __INTEL_COMPILER);
#elif _MSC_VER
		printf("msc (%i)", _MSC_VER);
#else
		printf("(unknown)");
#endif
		printf("\n");

		printf("built: %s %s\n", __DATE__, __TIME__);

		printf("\n");
		printf("cpu-core-count  : %u\n", task::GetCPUCoreCount());
		printf("cpu-thread-count: %u\n", task::GetCPUThreadCount());
		printf("usr-thread-count: %u\n", (unsigned int)n);
	}

	if (dontrun)
		return EXIT_SUCCESS;

	task::InitQueue(gMainQueue);

	for(i=0; i<n; i++) {
		threads[i] = task::NewThread(WorkerThread, (void *)0);
		task::SetThreadCPU(threads[i],
			i/(task::IsHyperthreadingAvailable() + 1));
	}

	task::InitPerfQuery(pq);
	// NOTE: threads before tasks if queue is not large enough
	for(i=0; i<NUM_TESTS; i++) {
		gNumbers[i] = 100000000 + i;
		task::Schedule(gMainQueue, IsPrime_f, &gNumbers[i]);
	}

	task::SyncQueue(gMainQueue);
	task::FiniPerfQuery(pq);

	appHangThread = task::NewThread(CatchAppHang, NULL);
	task::FiniQueue(gMainQueue);
	task::KillThread(appHangThread);

	for(i=0; i<n; i++) {
		task::JoinThread(threads[i]);
		task::KillThread(threads[i]);
	}

	return 0;
}
