#include <gamebase.h>

void gbBTreeInit(gb_btree_t *base) {
	base->root = (gb_btreeNode_t *)NULL;
	base->head = (gb_btreeNode_t *)NULL;
	base->tail = (gb_btreeNode_t *)NULL;
}

void gbBTreeRemoveAll(gb_btree_t *base) {
	gb_btreeNode_t *node, *next;

	for(node=base->head; node; node=next) {
		next = node->next;
		node->prev = node->next = (gb_btreeNode_t *)NULL;
		node->prnt = (gb_btreeNode_t *)NULL;
		node->left = node->rght = (gb_btreeNode_t *)NULL;
		node->base = (gb_btree_t *)NULL;
	}

	base->root = (gb_btreeNode_t *)NULL;
	base->head = (gb_btreeNode_t *)NULL;
	base->tail = (gb_btreeNode_t *)NULL;
}

static void BTreeListNode(gb_btree_t *base, gb_btreeNode_t *node, int key) {
	node->base = base;

	node->key = key;
	node->obj = (void *)NULL;

	node->prnt = (gb_btreeNode_t *)NULL;
	node->left = (gb_btreeNode_t *)NULL;
	node->rght = (gb_btreeNode_t *)NULL;

	node->next = (gb_btreeNode_t *)NULL;
	if ((node->prev = base->tail) != (gb_btreeNode_t *)NULL)
		base->tail->next = node;
	else
		base->head = node;
	base->tail = node;
}

gb_btreeNode_t *gbBTreeFind(gb_btree_t *base, int key, gb_btreeNode_t *create) {
	gb_btreeNode_t *node, *next, *top, **spot;

	if (!base->root && create) {
		BTreeListNode(base, create, key);

		base->root = create;
		return create;
	}

	top = (gb_btreeNode_t *)NULL;
	for(node=base->root; node; node=next) {
		if (node->key==key)
			return node;

		spot = (key < node->key) ? &node->left : &node->rght;
		top = node;

		if (!(next = *spot)) {
			if (create) {
				BTreeListNode(base, create, key);

				create->prnt = top;
				return *spot=create;
			}

			return (gb_btreeNode_t *)NULL;
		}
	}

	return (gb_btreeNode_t *)NULL;
}

void gbBTreeRemove(gb_btreeNode_t *node) {
	gb_btree_t *base;
	gb_btreeNode_t *r, *s;

	if (GB_UNLIKELY(!(base = node->base)))
		return;

	if (!(r = node->rght)) {
		if (node->prnt) {
			if (node==node->prnt->left)
				node->prnt->left = node->left;
			else
				node->prnt->rght = node->left;
		} else
			base->root = node->left;

		if (node->left)
			node->left->prnt = node->prnt;
	} else if(!r->left) {
		r->prnt = node->prnt;

		if ((r->left = node->left) != (gb_btreeNode_t *)NULL)
			node->left->prnt = r;

		if (node->prnt)
			node->prnt->left = r;
		else
			base->root = r;
	} else {
		s = r->left;

		while(s->left)
			s = s->left;
		s->left = node->left;

		if ((s->prnt->left = s->rght) != (gb_btreeNode_t *)NULL)
			s->rght->prnt = s->prnt;
		s->rght = r;

		if ((s->prnt = node->prnt) != (gb_btreeNode_t *)NULL) {
			if (node->prnt->left==node)
				node->prnt->left = s;
			else
				node->prnt->rght = s;
		} else
			base->root = s;
	}

	if (node->prev)
		node->prev->next = node->next;
	else
		node->base->head = node->next;

	if (node->next)
		node->next->prev = node->prev;
	else
		node->base->tail = node->prev;
}

void gbBTreeSet(gb_btreeNode_t *node, void *obj) {
	node->obj = obj;
}
void *gbBTreeGet(gb_btreeNode_t *node) {
	return node->obj;
}
