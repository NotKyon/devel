#include "simplify.h"

vec3_t *NewVec3() {
	vec3_t *p;

	p = (vec3_t *)NewMemory(sizeof(*p));
	if (!p)
		return (vec3_t *)0;

	p->x = 0;
	p->y = 0;
	p->z = 0;

	return p;
}
vec4_t *NewVec4() {
	vec4_t *p;

	p = (vec4_t *)NewMemory(sizeof(*p));
	if (!p)
		return (vec4_t *)0;

	p->x = 0;
	p->y = 0;
	p->z = 0;
	p->w = 0;

	return p;
}

void CopyVec3(vec3_t *dst, const vec3_t *src) {
	dst->x = src->x;
	dst->y = src->y;
	dst->z = src->z;
}
void CopyVec4(vec4_t *dst, const vec4_t *src) {
	dst->x = src->x;
	dst->y = src->y;
	dst->z = src->z;
	dst->w = src->w;
}
vec3_t *DuplicateVec3(const vec3_t *src) {
	vec3_t *dst;

	dst = NewVec3();
	if (!dst)
		return (vec3_t *)0;

	CopyVec3(dst, src);
	return dst;
}
vec4_t *DuplicateVec4(const vec4_t *src) {
	vec4_t *dst;

	dst = NewVec4();
	if (!dst)
		return (vec4_t *)0;

	CopyVec4(dst, src);
	return dst;
}

void Vec_SetX(vec3_t *dst, float x) {
	dst->x = x;
}
void Vec_SetY(vec3_t *dst, float y) {
	dst->y = y;
}
void Vec_SetZ(vec3_t *dst, float z) {
	dst->z = z;
}
void Vec_SetW(vec4_t *dst, float w) {
	dst->w = w;
}

float Vec_X(const vec3_t *src) {
	return src->x;
}
float Vec_Y(const vec3_t *src) {
	return src->y;
}
float Vec_Z(const vec3_t *src) {
	return src->z;
}
float Vec_W(const vec4_t *src) {
	return src->w;
}
