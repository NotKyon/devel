#ifndef M_PI
# define M_PI		(3.141592653589793238462643383279502884197169)
#endif

#ifndef degrees
# define degrees(x)	((M_PI/180.0)*(x))	/* degrees to radians */
#endif

#ifndef radians
# define radians(x)	((180.0/M_PI)*(x))	/* radians to degrees */
#endif

#if 0
#ifdef __GNUC__
float absf(float x) {
	union { unsigned int i; float f; } v;

	v.f = x;
	v.i &= 0x7FFFFFFF;

	return v.f;
}
float sinf(float x) {
	float r;
	__asm__ __volatile__
	(
		"fsinx %[x], %[r]"
		: [r] "=f" (r)
		: [x] "f" (x)
	);
	return r;
}
float cosf(float x) {
	float r;
	__asm__ __volatile__
	(
		"fcos %[x], %[r]"
		: [r] "=f" (r)
		: [x] "f" (x)
	);
	return r;
}
float acosf(float x) {
	float r;
	__asm__ __volatile__
	(
		"facos %[x], %[r]"
		: [r] "=f" (r)
		: [x] "f" (x)
	);
	return r;
}
float asinf(float x) {
	float r;
	__asm__ __volatile__
	(
		"fasin %[x], %[r]"
		: [r] "=f" (r)
		: [x] "f" (x)
	);
	return r;
}
float atanf(float x) {
	float r;
	__asm__ __volatile__
	(
		"fatan %[x], %[r]"
		: [r] "=f" (r)
		: [x] "f" (x)
	);
	return r;
}
float atan2f(float y, float x) {
	float ax, ay, v;

	if (!x && !y)
		return 0.0f;

	ay = absf(y);
	ax = absf(x);

	if (ay-ax==ay)
		return y<0?-(M_PI*2.0f):(M_PI*2.0f);

	v = ax-ay==ax?0.0f:atanf(y/x);

	if (x>0)
		return v;

	if (y<0)
		return v-M_PI;

	return v+M_PI;
}
float sqrtf(float x) {
	float r;
	__asm__ __volatile__
	(
		"fsqrt %[x], %[r]"
		: [r] "=f" (r)
		: [x] "f" (x)
	);
	return r;
}
float powf(float x, float y) {
	float r;
	__asm__ __volatile__
	(
		"fetox %[x], %[y], %[r]"
		: [r] "=f" (r)
		: [x] "f" (x),
		  [y] "f" (y)
	);
	return r;
}
#else
# include <math.h>
#endif
#else
# include <math.h>
#endif

typedef struct Vec3 {
	float x, y, z;
} Vec3;

typedef struct Quaternion {
	float w, x, y, z;
} Quaternion;

typedef union Matrix {
	struct {
		float _11, _21, _31, _41;
		float _12, _22, _32, _42;
		float _13, _23, _33, _43;
		float _14, _24, _34, _44;
	} cm;
	struct {
		float _11, _12, _13, _14;
		float _21, _22, _23, _24;
		float _31, _32, _33, _34;
		float _41, _42, _43, _44;
	} rm;
	float f[16];
	float m[4][4];
} Matrix;

extern float fabs_fast(float x);
extern int ftoi_fast(float x);
extern float sqrt_fast(float x);
extern float invsqrt_fast(float x);
extern float sin_fast(float x);
extern float cos_fast(float x);
extern float acos_fast(float x);

extern const Vec3 *vec3_forward(void);
extern const Vec3 *vec3_up(void);
extern const Vec3 *vec3_right(void);
extern float vec3_x(const Vec3 *src);
extern float vec3_y(const Vec3 *src);
extern float vec3_z(const Vec3 *src);
extern void vec3_zero(Vec3 *dst);
extern void vec3_identity(Vec3 *dst);
extern void vec3_set(Vec3 *dst, float x, float y, float z);
extern void vec3_copy(Vec3 *dst, const Vec3 *src);
extern void vec3_add(Vec3 *dst, const Vec3 *a, const Vec3 *b);
extern void vec3_subtract(Vec3 *dst, const Vec3 *a, const Vec3 *b);
extern void vec3_multiply(Vec3 *dst, const Vec3 *a, const Vec3 *b);
extern void vec3_divide(Vec3 *dst, const Vec3 *a, const Vec3 *b);
extern void vec3_scale(Vec3 *dst, const Vec3 *a, float b);
extern float vec3_dot(const Vec3 *a, const Vec3 *b);
extern void vec3_cross(Vec3 *dst, const Vec3 *a, const Vec3 *b);
extern void vec3_lerp_ex(Vec3 *dst, const Vec3 *a, const Vec3 *b, const Vec3 *w);
extern void vec3_lerp(Vec3 *dst, const Vec3 *a, const Vec3 *b, float w);
extern float vec3_length(const Vec3 *a);
extern float vec3_length_fast(const Vec3 *a);
extern float vec3_invlength(const Vec3 *a);
extern float vec3_invlength_fast(const Vec3 *a);
extern int vec3_normalize(Vec3 *a);
extern int vec3_normalize_fast(Vec3 *a);
extern float vec3_distance(const Vec3 *a, const Vec3 *b);
extern float vec3_distance_fast(const Vec3 *a, const Vec3 *b);
extern void vec3_transform3_ex(Vec3 *dst, const Vec3 *a, const Matrix *b);
extern void vec3_transform_ex(Vec3 *dst, const Vec3 *a, const Matrix *b);
extern void vec3_transform3(Vec3 *dst, float x, float y, float z, const Matrix *b);
extern void vec3_transform(Vec3 *dst, float x, float y, float z, const Matrix *b);
extern void vec3_translate(Vec3 *dst, const Matrix *src);
extern void vec3_point(Vec3 *dst, const Vec3 *a, const Vec3 *b);
extern void vec3_point_fast(Vec3 *dst, const Vec3 *a, const Vec3 *b);
extern void vec3_matrix_euler(Vec3 *dst, const Matrix *src);
extern void vec3_quaternion_euler(Vec3 *dst, const Quaternion *src);
extern void vec3_rotate_quaternion(Vec3 *dst, const Vec3 *src, const Quaternion *rot);

extern void mat_identity(Matrix *dst);
extern void mat_copy(Matrix *dst, const Matrix *src);
extern void mat_copy3(Matrix *dst, const Matrix *src);
extern void mat_multiply3(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply3_cc(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply3_rc(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply3_cr(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply34(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply34_cc(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply34_rc(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply34_cr(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply_cc(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply_rc(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_multiply_cr(Matrix *dst, const Matrix *a, const Matrix *b);
extern void mat_transpose3(Matrix *dst, const Matrix *src);
extern void mat_transpose(Matrix *dst, const Matrix *src);
extern void mat_quaternion(Matrix *dst, const Quaternion *src);
extern void mat_quaternion_fast(Matrix *dst, const Quaternion *src);
extern void mat_quaternion3(Matrix *dst, const Quaternion *src);
extern void mat_quaternion3_fast(Matrix *dst, const Quaternion *src);
extern void mat_decompose3(const Matrix *src, Quaternion *rot, Vec3 *scale);
extern void mat_decompose_quaternion(const Matrix *src, Quaternion *rot);
extern void mat_decompose_quaternion_fast(const Matrix *src, Quaternion *rot);
extern void mat_decompose(const Matrix *src, Vec3 *trans, Quaternion *rot, Vec3 *scale);
extern float mat_determinant3(const Matrix *src);
extern void mat_inverse_world(Matrix *dst, const Matrix *src);
extern float mat_inverse3(Matrix *dst, const Matrix *src);
extern float mat_inverse(Matrix *dst, const Matrix *src);
extern void mat_translation_ex(Matrix *dst, const Vec3 *trans);
extern void mat_translation(Matrix *dst, float x, float y, float z);
extern void mat_scaling_ex(Matrix *dst, const Vec3 *scale);
extern void mat_scaling(Matrix *dst, float x, float y, float z);
extern void mat_rotation_x(Matrix *dst, float x);
extern void mat_rotation_x_fast(Matrix *dst, float x);
extern void mat_rotation_y(Matrix *dst, float y);
extern void mat_rotation_y_fast(Matrix *dst, float y);
extern void mat_rotation_z(Matrix *dst, float z);
extern void mat_rotation_z_fast(Matrix *dst, float z);
extern void mat_rotation3_x(Matrix *dst, float x);
extern void mat_rotation3_x_fast(Matrix *dst, float x);
extern void mat_rotation3_y(Matrix *dst, float y);
extern void mat_rotation3_y_fast(Matrix *dst, float y);
extern void mat_rotation3_z(Matrix *dst, float z);
extern void mat_rotation3_z_fast(Matrix *dst, float z);
extern void mat_translate_ex(Matrix *dst, const Vec3 *trans);
extern void mat_translate(Matrix *dst, float x, float y, float z);

extern float quat_w(const Quaternion *src);
extern float quat_x(const Quaternion *src);
extern float quat_y(const Quaternion *src);
extern float quat_z(const Quaternion *src);
extern void quat_set(Quaternion *dst, float w, float x, float y, float z);
extern void quat_identity(Quaternion *dst);
extern void quat_copy(Quaternion *dst, const Quaternion *src);
extern void quat_multiply(Quaternion *dst, const Quaternion *a, const Quaternion *b);
extern float quat_dot(const Quaternion *a, const Quaternion *b);
extern void quat_axis(Quaternion *dst, float angle, float x, float y, float z);
extern void quat_axis_fast(Quaternion *dst, float angle, float x, float y, float z);
extern void quat_rotation(Quaternion *dst, const Matrix *src);
extern void quat_rotation_fast(Quaternion *dst, const Matrix *src);
extern void quat_slerp(Quaternion *dst, const Quaternion *a, const Quaternion *b, float w);
extern void quat_rotate_ex(Quaternion *dst, const Vec3 *src);
extern void quat_rotate(Quaternion *dst, float x, float y, float z);
extern void quat_rotate_fast_ex(Quaternion *dst, const Vec3 *src);
extern void quat_rotate_fast(Quaternion *dst, float x, float y, float z);
extern void quat_conjugate(Quaternion *dst, const Quaternion *src);
extern float quat_length_sqr(const Quaternion *src);
extern float quat_length(const Quaternion *src);
extern float quat_length_fast(const Quaternion *src);
extern void quat_normalize(Quaternion *dst, const Quaternion *src);
extern void quat_normalize_fast(Quaternion *dst, const Quaternion *src);
extern void quat_inverse(Quaternion *dst, const Quaternion *src);
extern void quat_inverse_fast(Quaternion *dst, const Quaternion *src);

#define __max__(a,b) ((a)>(b)?(a):(b))

#ifdef _MSC_VER
# define __copysign__ _copysign
#else
# ifdef __GNUC__
#  define __copysign_api static __inline __attribute__((always_inline))
# else
#  define __copysign_api static
# endif
__copysign_api float __copysign__(float a, float b) {
	union { unsigned int u; float f; } _a, _b;
	_a.f = a; _b.f = b;
	_a.u = (_a.u&0x7FFFFFFF)|(_b.u&0x80000000);
	return _a.f;
}
#endif

float quat_w(const Quaternion *src) { return src->w; }
float quat_x(const Quaternion *src) { return src->x; }
float quat_y(const Quaternion *src) { return src->y; }
float quat_z(const Quaternion *src) { return src->z; }

void quat_set(Quaternion *dst, float w, float x, float y, float z) {
	dst->w = w;
	dst->x = x;
	dst->y = y;
	dst->z = z;
}

void quat_identity(Quaternion *dst) {
	dst->w = 1;
	dst->x = 0;
	dst->y = 0;
	dst->z = 0;
}
void quat_copy(Quaternion *dst, const Quaternion *src) {
	dst->w = src->w;
	dst->x = src->x;
	dst->y = src->y;
	dst->z = src->z;
}

void quat_multiply(Quaternion *dst, const Quaternion *a, const Quaternion *b) {
	dst->w = a->w*b->w - a->x*b->x - a->y*b->y - a->z*b->z;
	dst->x = a->w*b->x + a->x*b->w + a->y*b->z - a->z*b->y;
	dst->y = a->w*b->y - a->x*b->z + a->y*b->w + a->z*b->x;
	dst->z = a->w*b->z + a->x*b->y - a->y*b->x + a->z*b->w;
}

float quat_dot(const Quaternion *a, const Quaternion *b) {
	return a->w*b->w + a->x*b->x + a->y*b->y + a->z*b->z;
}

void quat_axis(Quaternion *dst, float angle, float x, float y, float z) {
	float	t, s;

	t = 0.5*angle;
	s = (float)sinf(t);

	dst->w = (float)cosf(t);
	dst->x = s*x;
	dst->y = s*y;
	dst->z = s*z;
}
void quat_axis_fast(Quaternion *dst, float angle, float x, float y, float z) {
	float	t, s;

	t = 0.5*angle;
	s = sin_fast(t);

	dst->w = cos_fast(t);
	dst->x = s*x;
	dst->y = s*y;
	dst->z = s*z;
}

void quat_rotation(Quaternion *dst, const Matrix *src) {
#if 0
	float	d, s, t, r;
	int		m;

	t = src->rm._11 + src->rm._22 + src->rm._33 + 1;

	if (t>1) {
		r = sqrtf(t);
		s = 2*r;

		dst->w = 0.5*r;
		dst->x = (src->rm._23-src->rm._32)/s;
		dst->y = (src->rm._31-src->rm._13)/s;
		dst->z = (src->rm._12-src->rm._21)/s;

		return;
	}

	d = src->rm._11;
	m = 0;

	if (src->rm._22>d) {
		m = 1;
		d = src->rm._22;
	}

	if (src->rm._33>d) {
		m = 2;
		d = src->rm._33;
	}

	switch(m) {
		case 0:
			s = 2*sqrtf(1+src->rm._11-src->rm._22-src->rm._33);

			dst->w = (src->rm._23-src->rm._21)/s;
			dst->x = 0.25*s;
			dst->y = (src->rm._12+src->rm._21)/s;
			dst->z = (src->rm._13+src->rm._31)/s;

			return;

		case 1:
			s = 2*sqrtf(1+src->rm._22-src->rm._11-src->rm._33);

			dst->w = (src->rm._31-src->rm._13)/s;
			dst->x = (src->rm._12+src->rm._21)/s;
			dst->y = 0.25*s;
			dst->z = (src->rm._23+src->rm._32)/s;

			return;

		default: /* 2 */
			s = 2*sqrtf(1+src->rm._33-src->rm._11-src->rm._22);

			dst->w = (src->rm._12-src->rm._21)/s;
			dst->x = (src->rm._13+src->rm._31)/s;
			dst->y = (src->rm._23+src->rm._32)/s;
			dst->z = 0.25*s;

			return;
	}
#else
	dst->w = 0.5f*sqrtf(__max__(0, 1+src->rm._11+src->rm._22+src->rm._33));
	dst->x = 0.5f*sqrtf(__max__(0, 1+src->rm._11-src->rm._22-src->rm._33));
	dst->y = 0.5f*sqrtf(__max__(0, 1-src->rm._11+src->rm._22-src->rm._33));
	dst->z = 0.5f*sqrtf(__max__(0, 1-src->rm._11-src->rm._22+src->rm._33));
	dst->x = __copysign__(dst->x, src->rm._32-src->rm._23);
	dst->y = __copysign__(dst->y, src->rm._13-src->rm._31);
	dst->z = __copysign__(dst->z, src->rm._21-src->rm._12);
#endif
}
void quat_rotation_fast(Quaternion *dst, const Matrix *src) {
#if 0
	float	d, s, t, r;
	int		m;

	t = src->rm._11 + src->rm._22 + src->rm._33 + 1;

	if (t>1) {
		r = sqrt_fast(t);
		s = 2*r;

		dst->w = 0.5*r;
		dst->x = (src->rm._23-src->rm._32)/s;
		dst->y = (src->rm._31-src->rm._13)/s;
		dst->z = (src->rm._12-src->rm._21)/s;

		return;
	}

	d = src->rm._11;
	m = 0;

	if (src->rm._22>d) {
		m = 1;
		d = src->rm._22;
	}

	if (src->rm._33>d) {
		m = 2;
		d = src->rm._33;
	}

	switch(m) {
		case 0:
			s = 2*sqrt_fast(1+src->rm._11-src->rm._22-src->rm._33);

			dst->w = (src->rm._23-src->rm._21)/s;
			dst->x = 0.25*s;
			dst->y = (src->rm._12+src->rm._21)/s;
			dst->z = (src->rm._13+src->rm._31)/s;

			return;

		case 1:
			s = 2*sqrt_fast(1+src->rm._22-src->rm._11-src->rm._33);

			dst->w = (src->rm._31-src->rm._13)/s;
			dst->x = (src->rm._12+src->rm._21)/s;
			dst->y = 0.25*s;
			dst->z = (src->rm._23+src->rm._32)/s;

			return;

		default: /* 2 */
			s = 2*sqrt_fast(1+src->rm._33-src->rm._11-src->rm._22);

			dst->w = (src->rm._12-src->rm._21)/s;
			dst->x = (src->rm._13+src->rm._31)/s;
			dst->y = (src->rm._23+src->rm._32)/s;
			dst->z = 0.25*s;

			return;
	}
#else
	dst->w = 0.5f*sqrt_fast(__max__(0, 1+src->rm._11+src->rm._22+src->rm._33));
	dst->x = 0.5f*sqrt_fast(__max__(0, 1+src->rm._11-src->rm._22-src->rm._33));
	dst->y = 0.5f*sqrt_fast(__max__(0, 1-src->rm._11+src->rm._22-src->rm._33));
	dst->z = 0.5f*sqrt_fast(__max__(0, 1-src->rm._11-src->rm._22+src->rm._33));
	dst->x = __copysign__(dst->x, src->rm._32-src->rm._23);
	dst->y = __copysign__(dst->y, src->rm._13-src->rm._31);
	dst->z = __copysign__(dst->z, src->rm._21-src->rm._12);
#endif
}

void quat_slerp(Quaternion *dst, const Quaternion *a, const Quaternion *b, float w) {
	float d, e, ew;

	w = 1-w;

	d = quat_dot(a, b);
	e = (d<0) ? -1 : 1;
	ew = e*w;

	dst->w = w*a->w + ew*b->w;
	dst->x = w*a->x + ew*b->x;
	dst->y = w*a->y + ew*b->y;
	dst->z = w*a->z + ew*b->z;
}

void quat_rotate_ex(Quaternion *dst, const Vec3 *src) {
	Quaternion qx, qy, qz, tmp;

	quat_axis(&qx, src->x, 1, 0, 0);
	quat_axis(&qy, src->y, 0, 1, 0);
	quat_axis(&qz, src->z, 0, 0, 1);

	quat_multiply(&tmp, &qx, &qy);
	quat_multiply(dst, &tmp, &qz);
}
void quat_rotate(Quaternion *dst, float x, float y, float z) {
	Vec3 v;

	v.x = x; v.y = y; v.z = z;
	quat_rotate_ex(dst, &v);
}
void quat_rotate_fast_ex(Quaternion *dst, const Vec3 *src) {
	Quaternion qx, qy, qz, tmp;

	quat_axis_fast(&qx, src->x, 1, 0, 0);
	quat_axis_fast(&qy, src->y, 0, 1, 0);
	quat_axis_fast(&qz, src->z, 0, 0, 1);

	quat_multiply(&tmp, &qx, &qy);
	quat_multiply(dst, &tmp, &qz);
}
void quat_rotate_fast(Quaternion *dst, float x, float y, float z) {
	Vec3 v;

	v.x = x; v.y = y; v.z = z;
	quat_rotate_fast_ex(dst, &v);
}

void quat_conjugate(Quaternion *dst, const Quaternion *src) {
	dst->w =  src->w;
	dst->x = -src->x;
	dst->y = -src->y;
	dst->z = -src->z;
}

float quat_length_sqr(const Quaternion *src) {
	return src->w*src->w + src->x*src->x + src->y*src->y + src->z*src->z;
}
float quat_length(const Quaternion *src) {
	return sqrtf(quat_length_sqr(src));
}
float quat_length_fast(const Quaternion *src) {
	return sqrt_fast(quat_length_sqr(src));
}

void quat_normalize(Quaternion *dst, const Quaternion *src) {
	float r;

	r = 1/quat_length(src);

	dst->w = src->w*r;
	dst->x = src->x*r;
	dst->y = src->y*r;
	dst->z = src->z*r;
}
void quat_normalize_fast(Quaternion *dst, const Quaternion *src) {
	float r;

	r = invsqrt_fast(quat_length_sqr(src));

	dst->w = src->w*r;
	dst->x = src->x*r;
	dst->y = src->y*r;
	dst->z = src->z*r;
}

void quat_inverse(Quaternion *dst, const Quaternion *src) {
	quat_conjugate(dst, src);
	quat_normalize(dst, dst);
}
void quat_inverse_fast(Quaternion *dst, const Quaternion *src) {
	quat_conjugate(dst, src);
	quat_normalize_fast(dst, dst);
}


void mat_identity(Matrix *dst) {
	dst->rm._11 = 1; dst->rm._12 = 0; dst->rm._13 = 0; dst->rm._14 = 0;
	dst->rm._21 = 0; dst->rm._22 = 1; dst->rm._23 = 0; dst->rm._24 = 0;
	dst->rm._31 = 0; dst->rm._32 = 0; dst->rm._33 = 1; dst->rm._34 = 0;
	dst->rm._41 = 0; dst->rm._42 = 0; dst->rm._43 = 0; dst->rm._44 = 1;
}

void mat_copy(Matrix *dst, const Matrix *src) {
	dst->rm._11 = src->rm._11;
	dst->rm._12 = src->rm._12;
	dst->rm._13 = src->rm._13;
	dst->rm._14 = src->rm._14;
	dst->rm._21 = src->rm._21;
	dst->rm._22 = src->rm._22;
	dst->rm._23 = src->rm._23;
	dst->rm._24 = src->rm._24;
	dst->rm._31 = src->rm._31;
	dst->rm._32 = src->rm._32;
	dst->rm._33 = src->rm._33;
	dst->rm._34 = src->rm._34;
	dst->rm._41 = src->rm._41;
	dst->rm._42 = src->rm._42;
	dst->rm._43 = src->rm._43;
	dst->rm._44 = src->rm._44;
}
void mat_copy3(Matrix *dst, const Matrix *src) {
	dst->rm._11 = src->rm._11;
	dst->rm._12 = src->rm._12;
	dst->rm._13 = src->rm._13;
	dst->rm._21 = src->rm._21;
	dst->rm._22 = src->rm._22;
	dst->rm._23 = src->rm._23;
	dst->rm._31 = src->rm._31;
	dst->rm._32 = src->rm._32;
	dst->rm._33 = src->rm._33;
}

void mat_multiply3(Matrix *dst, const Matrix *a, const Matrix *b) {
	dst->rm._11 = a->rm._11*b->rm._11 + a->rm._12*b->rm._21 + a->rm._13*b->rm._31;
	dst->rm._12 = a->rm._11*b->rm._12 + a->rm._12*b->rm._22 + a->rm._13*b->rm._32;
	dst->rm._13 = a->rm._11*b->rm._13 + a->rm._12*b->rm._23 + a->rm._13*b->rm._33;

	dst->rm._21 = a->rm._21*b->rm._11 + a->rm._22*b->rm._21 + a->rm._23*b->rm._31;
	dst->rm._22 = a->rm._21*b->rm._12 + a->rm._22*b->rm._22 + a->rm._23*b->rm._32;
	dst->rm._23 = a->rm._21*b->rm._13 + a->rm._22*b->rm._23 + a->rm._23*b->rm._33;

	dst->rm._31 = a->rm._31*b->rm._11 + a->rm._32*b->rm._21 + a->rm._33*b->rm._31;
	dst->rm._32 = a->rm._31*b->rm._12 + a->rm._32*b->rm._22 + a->rm._33*b->rm._32;
	dst->rm._33 = a->rm._31*b->rm._13 + a->rm._32*b->rm._23 + a->rm._33*b->rm._33;
}

void mat_multiply34(Matrix *dst, const Matrix *a, const Matrix *b) {
	dst->rm._11 = a->rm._11*b->rm._11 + a->rm._12*b->rm._21 + a->rm._13*b->rm._31;
	dst->rm._12 = a->rm._11*b->rm._12 + a->rm._12*b->rm._22 + a->rm._13*b->rm._32;
	dst->rm._13 = a->rm._11*b->rm._13 + a->rm._12*b->rm._23 + a->rm._13*b->rm._33;

	dst->rm._21 = a->rm._21*b->rm._11 + a->rm._22*b->rm._21 + a->rm._23*b->rm._31;
	dst->rm._22 = a->rm._21*b->rm._12 + a->rm._22*b->rm._22 + a->rm._23*b->rm._32;
	dst->rm._23 = a->rm._21*b->rm._13 + a->rm._22*b->rm._23 + a->rm._23*b->rm._33;

	dst->rm._31 = a->rm._31*b->rm._11 + a->rm._32*b->rm._21 + a->rm._33*b->rm._31;
	dst->rm._32 = a->rm._31*b->rm._12 + a->rm._32*b->rm._22 + a->rm._33*b->rm._32;
	dst->rm._33 = a->rm._31*b->rm._13 + a->rm._32*b->rm._23 + a->rm._33*b->rm._33;

	dst->rm._41 = a->rm._41*b->rm._11 + a->rm._42*b->rm._21 + a->rm._43*b->rm._31;
	dst->rm._42 = a->rm._41*b->rm._12 + a->rm._42*b->rm._22 + a->rm._43*b->rm._32;
	dst->rm._43 = a->rm._41*b->rm._13 + a->rm._42*b->rm._23 + a->rm._43*b->rm._33;
}

void mat_multiply(Matrix *dst, const Matrix *a, const Matrix *b) {
	dst->rm._11 = a->rm._11*b->rm._11 + a->rm._12*b->rm._21 + a->rm._13*b->rm._31 + a->rm._14*b->rm._41;
	dst->rm._12 = a->rm._11*b->rm._12 + a->rm._12*b->rm._22 + a->rm._13*b->rm._32 + a->rm._14*b->rm._42;
	dst->rm._13 = a->rm._11*b->rm._13 + a->rm._12*b->rm._23 + a->rm._13*b->rm._33 + a->rm._14*b->rm._43;
	dst->rm._14 = a->rm._11*b->rm._14 + a->rm._12*b->rm._24 + a->rm._13*b->rm._34 + a->rm._14*b->rm._44;

	dst->rm._21 = a->rm._21*b->rm._11 + a->rm._22*b->rm._21 + a->rm._23*b->rm._31 + a->rm._24*b->rm._41;
	dst->rm._22 = a->rm._21*b->rm._12 + a->rm._22*b->rm._22 + a->rm._23*b->rm._32 + a->rm._24*b->rm._42;
	dst->rm._23 = a->rm._21*b->rm._13 + a->rm._22*b->rm._23 + a->rm._23*b->rm._33 + a->rm._24*b->rm._43;
	dst->rm._24 = a->rm._21*b->rm._14 + a->rm._22*b->rm._24 + a->rm._23*b->rm._34 + a->rm._24*b->rm._44;

	dst->rm._31 = a->rm._31*b->rm._11 + a->rm._32*b->rm._21 + a->rm._33*b->rm._31 + a->rm._34*b->rm._41;
	dst->rm._32 = a->rm._31*b->rm._12 + a->rm._32*b->rm._22 + a->rm._33*b->rm._32 + a->rm._34*b->rm._42;
	dst->rm._33 = a->rm._31*b->rm._13 + a->rm._32*b->rm._23 + a->rm._33*b->rm._33 + a->rm._34*b->rm._43;
	dst->rm._34 = a->rm._31*b->rm._14 + a->rm._32*b->rm._24 + a->rm._33*b->rm._34 + a->rm._34*b->rm._44;

	dst->rm._41 = a->rm._41*b->rm._11 + a->rm._42*b->rm._21 + a->rm._43*b->rm._31 + a->rm._44*b->rm._41;
	dst->rm._42 = a->rm._41*b->rm._12 + a->rm._42*b->rm._22 + a->rm._43*b->rm._32 + a->rm._44*b->rm._42;
	dst->rm._43 = a->rm._41*b->rm._13 + a->rm._42*b->rm._23 + a->rm._43*b->rm._33 + a->rm._44*b->rm._43;
	dst->rm._44 = a->rm._41*b->rm._14 + a->rm._42*b->rm._24 + a->rm._43*b->rm._34 + a->rm._44*b->rm._44;
}

void mat_transpose3(Matrix *dst, const Matrix *src) {
	if (dst!=src) {
		dst->rm._11 = src->cm._11;
		dst->rm._12 = src->cm._12;
		dst->rm._13 = src->cm._13;
		dst->rm._21 = src->cm._21;
		dst->rm._22 = src->cm._22;
		dst->rm._23 = src->cm._23;
		dst->rm._31 = src->cm._31;
		dst->rm._32 = src->cm._32;
		dst->rm._33 = src->cm._33;
	} else {
		Matrix tmp;

		mat_transpose3(&tmp, src);
		mat_copy(dst, &tmp);
	}
}
void mat_transpose(Matrix *dst, const Matrix *src) {
	if (dst!=src) {
		dst->rm._11 = src->cm._11; dst->rm._12 = src->cm._12;
		dst->rm._13 = src->cm._13; dst->rm._14 = src->cm._14;
		dst->rm._21 = src->cm._21; dst->rm._22 = src->cm._22;
		dst->rm._23 = src->cm._23; dst->rm._24 = src->cm._24;
		dst->rm._31 = src->cm._31; dst->rm._32 = src->cm._32;
		dst->rm._33 = src->cm._33; dst->rm._34 = src->cm._34;
	} else {
		Matrix tmp;

		mat_transpose(&tmp, src);
		mat_copy(dst, &tmp);
	}
}

void mat_quaternion(Matrix *dst, const Quaternion *src) {
	float qw, qx, qy, qz;
	float xx, yy, zz, xy, xz, yz, wx, wy, wz;

	qw = src->w;
	qx = src->x;
	qy = src->y;
	qz = src->z;

	xx = qx*qx; yy = qy*qy; zz = qz*qz;
	xy = qx*qy; xz = qx*qz; yz = qy*qz;
	wx = qw*qx; wy = qw*qy; wz = qw*qz;

	dst->rm._11 = 1-2*(yy+zz);
	dst->rm._12 =   2*(xy-wz);
	dst->rm._13 =   2*(xz+wy);
	dst->rm._14 =   0        ;

	dst->rm._21 =   2*(xy+wz);
	dst->rm._22 = 1-2*(xx+zz);
	dst->rm._23 =   2*(yz-wx);
	dst->rm._24 =   0        ;

	dst->rm._31 =   2*(xz-wy);
	dst->rm._32 =   2*(yz+wx);
	dst->rm._33 = 1-2*(xx+yy);
	dst->rm._34 =   0        ;

	dst->rm._41 =   0        ;
	dst->rm._42 =   0        ;
	dst->rm._43 =   0        ;
	dst->rm._44 =   1        ;
}
void mat_quaternion_fast(Matrix *dst, const Quaternion *src) {
	/*float	r;*/
	float	qw, qx, qy, qz;
	float	xx, yy, zz, xy, xz, yz, wx, wy, wz;

	qw = src->w;
	qx = src->x;
	qy = src->y;
	qz = src->z;

	/*
	r = invsqrt_fast(qw*qw + qx*qx + qy*qy + qz*qz);
	qw *= r;
	qx *= r;
	qy *= r;
	qz *= r;
	*/

	xx = qx*qx; yy = qy*qy; zz = qz*qz;
	xy = qx*qy; xz = qx*qz; yz = qy*qz;
	wx = qw*qx; wy = qw*qy; wz = qw*qz;

	dst->rm._11 = 1-2*(yy+zz);
	dst->rm._12 =   2*(xy+wz);
	dst->rm._13 =   2*(xz-wy);
	dst->rm._14 =   0        ;

	dst->rm._21 =   2*(xy-wz);
	dst->rm._22 = 1-2*(xx+zz);
	dst->rm._23 =   2*(yz+wx);
	dst->rm._24 =   0        ;

	dst->rm._31 =   2*(xz+wy);
	dst->rm._32 =   2*(yz-wx);
	dst->rm._33 = 1-2*(xx+yy);
	dst->rm._34 =   0        ;

	dst->rm._41 =   0        ;
	dst->rm._42 =   0        ;
	dst->rm._43 =   0        ;
	dst->rm._44 =   1        ;
}
void mat_quaternion3(Matrix *dst, const Quaternion *src) {
	float qw, qx, qy, qz;
	float xx, yy, zz, xy, xz, yz, wx, wy, wz;

	qw = src->w;
	qx = src->x;
	qy = src->y;
	qz = src->z;

	xx = qx*qx; yy = qy*qy; zz = qz*qz;
	xy = qx*qy; xz = qx*qz; yz = qy*qz;
	wx = qw*qx; wy = qw*qy; wz = qw*qz;

	dst->rm._11 = 1-2*(yy+zz);
	dst->rm._12 =   2*(xy-wz);
	dst->rm._13 =   2*(xz+wy);

	dst->rm._21 =   2*(xy+wz);
	dst->rm._22 = 1-2*(xx+zz);
	dst->rm._23 =   2*(yz-wx);

	dst->rm._31 =   2*(xz-wy);
	dst->rm._32 =   2*(yz+wx);
	dst->rm._33 = 1-2*(xx+yy);
}
void mat_quaternion3_fast(Matrix *dst, const Quaternion *src) {
	float qw, qx, qy, qz;
	float xx, yy, zz, xy, xz, yz, wx, wy, wz;

	qw = src->w;
	qx = src->x;
	qy = src->y;
	qz = src->z;

	xx = qx*qx; yy = qy*qy; zz = qz*qz;
	xy = qx*qy; xz = qx*qz; yz = qy*qz;
	wx = qw*qx; wy = qw*qy; wz = qw*qz;

	dst->rm._11 = 1-2*(yy+zz);
	dst->rm._12 =   2*(xy-wz);
	dst->rm._13 =   2*(xz+wy);

	dst->rm._21 =   2*(xy+wz);
	dst->rm._22 = 1-2*(xx+zz);
	dst->rm._23 =   2*(yz-wx);

	dst->rm._31 =   2*(xz-wy);
	dst->rm._32 =   2*(yz+wx);
	dst->rm._33 = 1-2*(xx+yy);
}

void mat_decompose3(const Matrix *src, Quaternion *rot, Vec3 *scale) {
	Matrix	normal;
	float	sx, sy, sz;

	scale->x = vec3_length((const Vec3 *)&src->rm._11);
	scale->y = vec3_length((const Vec3 *)&src->rm._21);
	scale->z = vec3_length((const Vec3 *)&src->rm._31);

	sx = 1.0f/scale->x;
	sy = 1.0f/scale->y;
	sz = 1.0f/scale->z;

	vec3_scale((Vec3 *)&normal.rm._11, (const Vec3 *)&src->rm._11, sx);
	vec3_scale((Vec3 *)&normal.rm._21, (const Vec3 *)&src->rm._21, sy);
	vec3_scale((Vec3 *)&normal.rm._31, (const Vec3 *)&src->rm._31, sz);

	quat_rotation(rot, &normal);
}
void mat_decompose_quaternion(const Matrix *src, Quaternion *rot) {
	Matrix	normal;
	float	sx, sy, sz;

	sx = vec3_invlength((const Vec3 *)&src->rm._11);
	sy = vec3_invlength((const Vec3 *)&src->rm._21);
	sz = vec3_invlength((const Vec3 *)&src->rm._31);

	vec3_scale((Vec3 *)&normal.rm._11, (const Vec3 *)&src->rm._11, sx);
	vec3_scale((Vec3 *)&normal.rm._21, (const Vec3 *)&src->rm._21, sy);
	vec3_scale((Vec3 *)&normal.rm._31, (const Vec3 *)&src->rm._31, sz);

	quat_rotation(rot, &normal);
}
void mat_decompose_quaternion_fast(const Matrix *src, Quaternion *rot) {
	Matrix	normal;
	float	sx, sy, sz;

	sx = vec3_invlength_fast((const Vec3 *)&src->rm._11);
	sy = vec3_invlength_fast((const Vec3 *)&src->rm._21);
	sz = vec3_invlength_fast((const Vec3 *)&src->rm._31);

	vec3_scale((Vec3 *)&normal.rm._11, (const Vec3 *)&src->rm._11, sx);
	vec3_scale((Vec3 *)&normal.rm._21, (const Vec3 *)&src->rm._21, sy);
	vec3_scale((Vec3 *)&normal.rm._31, (const Vec3 *)&src->rm._31, sz);

	quat_rotation_fast(rot, &normal);
}
void mat_decompose(const Matrix *src, Vec3 *trans, Quaternion *rot, Vec3 *scale) {
	Matrix	normal;
	float	sx, sy, sz;

	vec3_copy(trans, (const Vec3 *)&src->rm._41);

	scale->x = vec3_length((const Vec3 *)&src->rm._11);
	scale->y = vec3_length((const Vec3 *)&src->rm._21);
	scale->z = vec3_length((const Vec3 *)&src->rm._31);

	sx = 1.0f/scale->x;
	sy = 1.0f/scale->y;
	sz = 1.0f/scale->z;

	vec3_scale((Vec3 *)&normal.rm._11, (const Vec3 *)&src->rm._11, sx);
	vec3_scale((Vec3 *)&normal.rm._21, (const Vec3 *)&src->rm._21, sy);
	vec3_scale((Vec3 *)&normal.rm._31, (const Vec3 *)&src->rm._31, sz);

	quat_rotation(rot, &normal);
}

float mat_determinant3(const Matrix *src) {
	float	aei, bfg, cdh, afh, bdi, ceg;

	aei = src->rm._11*src->rm._22*src->rm._33;
	bfg = src->rm._12*src->rm._23*src->rm._31;
	cdh = src->rm._13*src->rm._21*src->rm._32;
	afh = src->rm._11*src->rm._23*src->rm._32;
	bdi = src->rm._12*src->rm._21*src->rm._33;
	ceg = src->rm._13*src->rm._22*src->rm._31;

	return aei+bfg+cdh-afh-bdi-ceg;
}

void mat_inverse_world(Matrix *dst, const Matrix *src) {
	mat_transpose3(dst, src);
	dst->rm._14 = 0;
	dst->rm._24 = 0;
	dst->rm._34 = 0;
	dst->rm._41 = -(dst->rm._11*src->rm._41 + dst->rm._21*src->rm._42 + dst->rm._31*src->rm._43);
	dst->rm._42 = -(dst->rm._12*src->rm._41 + dst->rm._22*src->rm._42 + dst->rm._32*src->rm._43);
	dst->rm._43 = -(dst->rm._13*src->rm._41 + dst->rm._23*src->rm._42 + dst->rm._33*src->rm._43);
	dst->rm._44 = 1;
}
float mat_inverse3(Matrix *dst, const Matrix *src) {
	Matrix tmp;
	float r, d;
	Vec3 vr, vi[3];
	int i, j, k;

	if (dst==src) {
		r = mat_inverse3(&tmp, src);
		mat_copy(dst, &tmp);
		return r;
	}

	if (!(d = mat_determinant3(src)))
		return 0.0f;

	for(i=0; i<3; i++) {
		for(j=0; j<3; j++) {
			k = j;
			if (k>i)
				k -= 1;

			vi[k].x = src->f[j*4+0];
			vi[k].y = src->f[j*4+1];
			vi[k].z = src->f[j*4+2];
		}

		vec3_cross(&vr, &vi[0], &vi[1]);
		dst->f[0*4+i] = powf(-1, i)*vr.x / d;
		dst->f[1*4+i] = powf(-1, i)*vr.y / d;
		dst->f[2*4+i] = powf(-1, i)*vr.z / d;
	}

	return d;
}

void mat_translation_ex(Matrix *dst, const Vec3 *trans) {
	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
	dst->rm._34 = 0;

	dst->rm._41 = trans->x;
	dst->rm._42 = trans->y;
	dst->rm._43 = trans->z;
	dst->rm._44 = 1;
}
void mat_translation(Matrix *dst, float x, float y, float z) {
	Vec3 trans;

	trans.x = x; trans.y = y; trans.z = z;
	mat_translation_ex(dst, &trans);
}

void mat_scaling_ex(Matrix *dst, const Vec3 *scale) {
	dst->rm._11 = scale->x;
	dst->rm._12 = 0;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = scale->y;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = scale->z;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
void mat_scaling(Matrix *dst, float x, float y, float z) {
	Vec3 scale;

	scale.x = x; scale.y = y; scale.z = z;
	mat_scaling_ex(dst, &scale);
}

void mat_rotation_x(Matrix *dst, float x) {
	float	c, s;

	c = (float)cosf(x);
	s = (float)sinf(x);

	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = c;
	dst->rm._23 = -s;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = s;
	dst->rm._33 = c;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
void mat_rotation_x_fast(Matrix *dst, float x) {
	float	c, s;

	c = cos_fast(x);
	s = sin_fast(x);

	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = c;
	dst->rm._23 = -s;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = s;
	dst->rm._33 = c;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}

void mat_rotation_y(Matrix *dst, float y) {
	float	c, s;

	c = (float)cosf(y);
	s = (float)sinf(y);

	dst->rm._11 = c;
	dst->rm._12 = 0;
	dst->rm._13 = -s;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = s;
	dst->rm._32 = 0;
	dst->rm._33 = c;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
void mat_rotation_y_fast(Matrix *dst, float y) {
	float	c, s;

	c = cos_fast(y);
	s = sin_fast(y);

	dst->rm._11 = c;
	dst->rm._12 = 0;
	dst->rm._13 = -s;
	dst->rm._14 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = s;
	dst->rm._32 = 0;
	dst->rm._33 = c;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}

void mat_rotation_z(Matrix *dst, float z) {
	float	c, s;

	c = (float)cosf(z);
	s = (float)sinf(z);

	dst->rm._11 = c;
	dst->rm._12 = -s;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = s;
	dst->rm._22 = c;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}
void mat_rotation_z_fast(Matrix *dst, float z) {
	float	c, s;

	c = cos_fast(z);
	s = sin_fast(z);

	dst->rm._11 = c;
	dst->rm._12 = -s;
	dst->rm._13 = 0;
	dst->rm._14 = 0;

	dst->rm._21 = s;
	dst->rm._22 = c;
	dst->rm._23 = 0;
	dst->rm._24 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
	dst->rm._34 = 0;

	dst->rm._41 = 0;
	dst->rm._42 = 0;
	dst->rm._43 = 0;
	dst->rm._44 = 1;
}

void mat_rotation3_x(Matrix *dst, float x) {
	float	c, s;

	c = (float)cosf(x);
	s = (float)sinf(x);

	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = c;
	dst->rm._23 = -s;

	dst->rm._31 = 0;
	dst->rm._32 = s;
	dst->rm._33 = c;
}
void mat_rotation3_x_fast(Matrix *dst, float x) {
	float	c, s;

	c = cos_fast(x);
	s = sin_fast(x);

	dst->rm._11 = 1;
	dst->rm._12 = 0;
	dst->rm._13 = 0;

	dst->rm._21 = 0;
	dst->rm._22 = c;
	dst->rm._23 = -s;

	dst->rm._31 = 0;
	dst->rm._32 = s;
	dst->rm._33 = c;
}

void mat_rotation3_y(Matrix *dst, float y) {
	float	c, s;

	c = (float)cosf(y);
	s = (float)sinf(y);

	dst->rm._11 = c;
	dst->rm._12 = 0;
	dst->rm._13 = -s;

	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;

	dst->rm._31 = s;
	dst->rm._32 = 0;
	dst->rm._33 = c;
}
void mat_rotation3_y_fast(Matrix *dst, float y) {
	float	c, s;

	c = cos_fast(y);
	s = sin_fast(y);

	dst->rm._11 = c;
	dst->rm._12 = 0;
	dst->rm._13 = -s;

	dst->rm._21 = 0;
	dst->rm._22 = 1;
	dst->rm._23 = 0;

	dst->rm._31 = s;
	dst->rm._32 = 0;
	dst->rm._33 = c;
}

void mat_rotation3_z(Matrix *dst, float z) {
	float	c, s;

	c = (float)cosf(z);
	s = (float)sinf(z);

	dst->rm._11 = c;
	dst->rm._12 = -s;
	dst->rm._13 = 0;

	dst->rm._21 = s;
	dst->rm._22 = c;
	dst->rm._23 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
}
void mat_rotation3_z_fast(Matrix *dst, float z) {
	float	c, s;

	c = cos_fast(z);
	s = sin_fast(z);

	dst->rm._11 = c;
	dst->rm._12 = -s;
	dst->rm._13 = 0;

	dst->rm._21 = s;
	dst->rm._22 = c;
	dst->rm._23 = 0;

	dst->rm._31 = 0;
	dst->rm._32 = 0;
	dst->rm._33 = 1;
}

void mat_translate_ex(Matrix *dst, const Vec3 *trans) {
	dst->rm._41 = trans->x;
	dst->rm._42 = trans->y;
	dst->rm._43 = trans->z;
}
void mat_translate(Matrix *dst, float x, float y, float z) {
	dst->rm._41 = x;
	dst->rm._42 = y;
	dst->rm._43 = z;
}


float fabs_fast(float x) {
	union { float f; unsigned int i; } v;

	v.f = x;
	v.i &= 0x7FFFFFFF;

	return v.f;
}

/* source: Software Optimization Cookbook, 2006, Intel Press -- p. 187 */
int ftoi_fast(float x) {
	union { float f; int i; } v;

	v.f   = x + (float)(3<<21);
	v.i  -= 0x4AC00000;
	v.i >>= 1;

	return v.i;
}
float sqrt_fast(float x) {
	union { float f; int i; } v;

	v.f   = x;
	v.i  -= 0x3F800000;
	v.i >>= 1;
	v.i  += 0x3F800000;

	return v.f;
}
float invsqrt_fast(float x) {
	union { float f; int i; } v;
	int i;

	/* -Woverflow workaround */
	i   = 0x3F800000;
	i <<= 1;
	i  += 0x3F800000;

	v.f = x;
	v.i = (i-v.i)>>1;
	/*v.i = ((0x3F800000<<1) + 0x3F800000 - v.i) >> 1;*/

	return v.f * (1.47f - 0.47f*x*v.f*v.f);
}
/*float invsqrt_fast(float x) {
	union { float f; int i; } v;
	float h;

	h = 0.5f*x;

	v.f = x;
	v.i = 0x5F3759DF - (v.i>>1);

	x = v.f;
	x = x*(1.5f - h*x*x);

	return x;
}*/

/* source: http://www.devmaster.net/forums/showthread.php?t=5784 */
float sin_fast(float x) {
	const float B = 4/M_PI;
	const float C = -4/(M_PI*M_PI);

	x = B*x + C*x*fabs_fast(x);
	x = 0.225f*(x*fabs_fast(x)-x)+x;

	return x;
}

/* source: http://stackoverflow.com/questions/3380628/fast-arc-cos-algorithm */
float cos_fast(float x) {
	return sin_fast(x + M_PI*0.5);
}
float acos_fast(float x) {
	return (-0.69813170079773212*x*x - 0.87266462599716477)*x + 1.5707963267948966;
}

const Vec3 *vec3_forward(void) { static Vec3 v={0,0,1}; return &v; }
const Vec3 *vec3_up(void) { static Vec3 v={0,1,0}; return &v; }
const Vec3 *vec3_right(void) { static Vec3 v={1,0,0}; return &v; }

float vec3_x(const Vec3 *src) { return src->x; }
float vec3_y(const Vec3 *src) { return src->y; }
float vec3_z(const Vec3 *src) { return src->z; }

void vec3_zero(Vec3 *dst) { dst->x=dst->y=dst->z = 0; }
void vec3_identity(Vec3 *dst) { dst->x=dst->y=dst->z = 1; }

void vec3_set(Vec3 *dst, float x, float y, float z) {
	dst->x = x;
	dst->y = y;
	dst->z = z;
}

void vec3_copy(Vec3 *dst, const Vec3 *src) {
	dst->x = src->x;
	dst->y = src->y;
	dst->z = src->z;
}

void vec3_add(Vec3 *dst, const Vec3 *a, const Vec3 *b) {
	dst->x = a->x+b->x;
	dst->y = a->y+b->y;
	dst->z = a->z+b->z;
}
void vec3_subtract(Vec3 *dst, const Vec3 *a, const Vec3 *b) {
	dst->x = a->x-b->x;
	dst->y = a->y-b->y;
	dst->z = a->z-b->z;
}
void vec3_multiply(Vec3 *dst, const Vec3 *a, const Vec3 *b) {
	dst->x = a->x*b->x;
	dst->y = a->y*b->y;
	dst->z = a->z*b->z;
}
void vec3_divide(Vec3 *dst, const Vec3 *a, const Vec3 *b) {
	dst->x = a->x/b->x;
	dst->y = a->y/b->y;
	dst->z = a->z/b->z;
}
void vec3_scale(Vec3 *dst, const Vec3 *a, float b) {
	dst->x = a->x*b;
	dst->y = a->y*b;
	dst->z = a->z*b;
}

float vec3_dot(const Vec3 *a, const Vec3 *b) {
	return a->x*b->x + a->y*b->y + a->z*b->z;
}
void vec3_cross(Vec3 *dst, const Vec3 *a, const Vec3 *b) {
	dst->x = a->y*b->z - a->z*b->y;
	dst->y = a->z*b->x - a->x*b->z;
	dst->z = a->x*b->y - a->y*b->x;
}

void vec3_lerp_ex(Vec3 *dst, const Vec3 *a, const Vec3 *b, const Vec3 *w) {
	dst->x = a->x + w->x*(b->x-a->x);
	dst->y = a->y + w->y*(b->y-a->y);
	dst->z = a->z + w->z*(b->z-a->z);
}
void vec3_lerp(Vec3 *dst, const Vec3 *a, const Vec3 *b, float w) {
	dst->x = a->x + w*(b->x-a->x);
	dst->y = a->y + w*(b->y-a->y);
	dst->z = a->z + w*(b->z-a->z);
}

float vec3_length(const Vec3 *a) {
	return sqrtf(a->x*a->x + a->y*a->y + a->z*a->z);
}
float vec3_length_fast(const Vec3 *a) {
	return sqrt_fast(a->x*a->x + a->y*a->y + a->z*a->z);
}
float vec3_invlength(const Vec3 *a) {
	return 1.0f/sqrtf(a->x*a->x + a->y*a->y + a->z*a->z);
}
float vec3_invlength_fast(const Vec3 *a) {
	return invsqrt_fast(a->x*a->x + a->y*a->y + a->z*a->z);
}

int vec3_normalize(Vec3 *a) {
	float r;

	r = sqrtf(a->x*a->x + a->y*a->y + a->z*a->z);
	if (!r)
		return 0;

	r = 1.0f/r;

	a->x *= r;
	a->y *= r;
	a->z *= r;

	return 1;
}
int vec3_normalize_fast(Vec3 *a) {
	float r;

	r = invsqrt_fast(a->x*a->x + a->y*a->y + a->z*a->z);

	a->x *= r;
	a->y *= r;
	a->z *= r;

	return 1;
}

float vec3_distance(const Vec3 *a, const Vec3 *b) {
	float x, y, z;

	x = a->x-b->x;
	y = a->y-b->y;
	z = a->z-b->z;

	return sqrtf(x*x + y*y + z*z);
}
float vec3_distance_fast(const Vec3 *a, const Vec3 *b) {
	float x, y, z;

	x = a->x-b->x;
	y = a->y-b->y;
	z = a->z-b->z;

	return sqrt_fast(x*x + y*y + z*z);
}

void vec3_transform3_ex(Vec3 *dst, const Vec3 *a, const Matrix *b) {
	dst->x = a->x*b->rm._11 + a->y*b->rm._21 + a->z*b->rm._31;
	dst->y = a->x*b->rm._12 + a->y*b->rm._22 + a->z*b->rm._32;
	dst->z = a->x*b->rm._13 + a->y*b->rm._23 + a->z*b->rm._33;
}
void vec3_transform_ex(Vec3 *dst, const Vec3 *a, const Matrix *b) {
	dst->x = a->x*b->rm._11 + a->y*b->rm._21 + a->z*b->rm._31 + b->rm._41;
	dst->y = a->x*b->rm._12 + a->y*b->rm._22 + a->z*b->rm._32 + b->rm._42;
	dst->z = a->x*b->rm._13 + a->y*b->rm._23 + a->z*b->rm._33 + b->rm._43;
}
void vec3_transform3(Vec3 *dst, float x, float y, float z, const Matrix *b) {
	Vec3 a;

	a.x = x; a.y = y; a.z = z;
	vec3_transform3_ex(dst, &a, b);
}
void vec3_transform(Vec3 *dst, float x, float y, float z, const Matrix *b) {
	Vec3 a;

	a.x = x; a.y = y; a.z = z;
	vec3_transform_ex(dst, &a, b);
}

void vec3_translate(Vec3 *dst, const Matrix *src) {
	dst->x = src->rm._41;
	dst->y = src->rm._42;
	dst->z = src->rm._43;
}

void vec3_point(Vec3 *dst, const Vec3 *a, const Vec3 *b) {
	Matrix	mat;
	Vec3	diff, vec, frontVec={0,0,1};
	float	x, y;

	dst->z = 0;

	vec3_subtract(&diff, b, a);

	vec.x = diff.x; vec.y = 0; vec.z = diff.z;

	if (vec3_normalize(&vec)) {
		y = vec3_dot(&vec, &frontVec);
		if (y> 1)		y =  1;
		if (y<-1)		y = -1;
						y = acosf(y);
		if (vec.x<0)	y = -y;
	} else
		y = 0;

	mat_rotation_y(&mat, -y);
	vec3_transform3_ex(&vec, &diff, &mat);

	if (vec3_normalize(&vec)) {
		x = vec3_dot(&vec, &frontVec);
		if (x> 1)		x =  1;
		if (x<-1)		x = -1;
						x = acosf(x);
		if (vec.y>0)	x = -x;
	} else
		x = 0;

	dst->x = x;
	dst->y = y;
}
void vec3_point_fast(Vec3 *dst, const Vec3 *a, const Vec3 *b) {
	Matrix	mat;
	Vec3	diff, vec, frontVec={0,0,1};
	float	x, y;

	dst->z = 0;

	vec3_subtract(&diff, b, a);

	vec.x = diff.x; vec.y = 0; vec.z = diff.z;

	vec3_normalize_fast(&vec);
	y = vec3_dot(&vec, &frontVec);
	if (y> 1)		y =  1;
	if (y<-1)		y = -1;
					y =  acos_fast(y);
	if (vec.x<0)	y = -y;

	mat_rotation_y(&mat, -y);
	vec3_transform3_ex(&vec, &diff, &mat);

	vec3_normalize_fast(&vec);
	x = vec3_dot(&vec, &frontVec);
	if (x> 1)		x =  1;
	if (x<-1)		x = -1;
					x = acos_fast(x);
	if (vec.y>0)	x = -x;

	dst->x = x;
	dst->y = y;
}

void vec3_matrix_euler(Vec3 *dst, const Matrix *src) {
	float	v;

	v = -src->rm._32;

	if (v > 0.998) {
		dst->y = atan2f(src->rm._13, src->rm._11);
		dst->x = 0.5*M_PI;
		dst->z = 0;

		return;
	}

	if (v < -0.998) {
		dst->y = atan2f(src->rm._13, src->rm._11);
		dst->x = -0.5*M_PI;
		dst->z = 0;

		return;
	}

	dst->x = asinf(v);
	dst->y = atan2f(src->rm._31, src->rm._33);
	dst->z = atan2f(src->rm._12, src->rm._22);
}
void vec3_quaternion_euler(Vec3 *dst, const Quaternion *src) {
	Matrix mat;

	mat_quaternion(&mat, src);
	vec3_matrix_euler(dst, &mat);
}
/* TODO: Handle it the proper way -- see below (unknown source) */
/*
    const double w2 = q.w*q.w;
    const double x2 = q.x*q.x;
    const double y2 = q.y*q.y;
    const double z2 = q.z*q.z;
    const double unitLength = w2 + x2 + y2 + z2;    // Normalised == 1, otherwise correction divisor.
    const double abcd = q.w*q.x + q.y*q.z;
    const double eps = 1e-7;    // TODO: pick from your math lib instead of hardcoding.
    const double pi = 3.14159265358979323846;   // TODO: pick from your math lib instead of hardcoding.
    if (abcd > (0.5-eps)*unitLength)
    {
        yaw = 2 * atan2(q.y, q.w);
        pitch = pi;
        roll = 0;
    }
    else if (abcd < (-0.5+eps)*unitLength)
    {
        yaw = -2 * ::atan2(q.y, q.w);
        pitch = -pi;
        roll = 0;
    }
    else
    {
        const double adbc = q.w*q.z - q.x*q.y;
        const double acbd = q.w*q.y - q.x*q.z;
        yaw = ::atan2(2*adbc, 1 - 2*(z2+x2));
        pitch = ::asin(2*abcd/unitLength);
        roll = ::atan2(2*acbd, 1 - 2*(y2+x2));
    }
*/

void vec3_rotate_quaternion(Vec3 *dst, const Vec3 *src, const Quaternion *rot) {
	Quaternion p, i, t;

	quat_set(&p, 0, src->x, src->y, src->z);

	quat_inverse(&i, rot);
	quat_multiply(&t, &i, &p);
	quat_multiply(&p, rot, &t);

	dst->x = p.x;
	dst->y = p.y;
	dst->z = p.z;
}

/******************************************************************************
 * -------------------------------------------------------------------------- *
 * T E S T I N G  C O D E  - T E S T I N G  C O D E  - T E S T I N G  C O D E *
 * -------------------------------------------------------------------------- *
 ******************************************************************************/

#include <stdio.h>

void print_matrix(const char *name, const Matrix *m) {
	printf
	(
		"%s {\n\t%.3f,%.3f,%.3f,%.3f,\n"
		"\t%.3f,%.3f,%.3f,%.3f,\n"
		"\t%.3f,%.3f,%.3f,%.3f,\n"
		"\t%.3f,%.3f,%.3f,%.3f\n}\n", name,
		m->rm._11, m->rm._12, m->rm._13, m->rm._14,
		m->rm._21, m->rm._22, m->rm._23, m->rm._24,
		m->rm._31, m->rm._32, m->rm._33, m->rm._34,
		m->rm._41, m->rm._42, m->rm._43, m->rm._44
	);
}

void matrix_test(void) {
	Matrix mident, mtrans, minv;

	printf("MATRIX TEST\n");

	mat_translation(&mtrans, 5, 25, 30);
	mat_inverse_world(&minv, &mtrans);
	mat_multiply(&mident, &minv, &mtrans); /* m*inv(m) = I */
	print_matrix("mtrans", &mtrans);
	print_matrix("minv", &minv);
	print_matrix("mident", &mident);

	printf("\n");
}

void trig_test(void) {
	float x;

	printf("TRIGONOMETRY TEST\n");

	for(x=0.0f; x<(M_PI*2); x+=0.25f) {
		printf("sin/cos slow(%.2f): %.6f,%.6f\n", x, sinf(x), cosf(x));
		printf("sin/cos fast(%.2f): %.6f,%.6f\n", x, sin_fast(x), cos_fast(x));
	}

	printf("\n");
}

int main(int argc, char **argv) {

	if (argc||argv) {/*unused*/}

	matrix_test();
	trig_test();

	return 0;

}
