#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if _WIN32
# include <windows.h>
# if _MSC_VER
typedef unsigned __int64 u64_t;
# else
#  include <stdint.h>
typedef uint64_t u64_t;
# endif
#else
# include <sys/time.h>
# include <stdint.h>
typedef uint64_t u64_t;
#endif
#include <xmmintrin.h>
#include <math.h>

#if __GNUC__ || __clang__
# define __forceinline __inline __attribute__((always_inline))
#elif !_MSC_VER && !__INTEL_COMPILER
# define __forceinline
#endif

typedef union { void *p; size_t n; } ptradr_t;

__forceinline double timer() {
#if _WIN32
	u64_t t, f;

	QueryPerformanceCounter((LARGE_INTEGER *)&t);
	QueryPerformanceFrequency((LARGE_INTEGER *)&f);

	return (double)t/(double)f;
#else
	struct timeval tv;

	gettimeofday(&tv, (void *)0);

	return (double)tv.tv_sec + ((double)tv.tv_usec)/1000000.0;
#endif
}

void *Malloc16(size_t n) {
	ptradr_t ptr;
	void *p;

	if (!(ptr.p = p = malloc(n + 16 + sizeof(void *))))
		return (void *)0;

	ptr.n -= ptr.n%16;
	ptr.n += 16 - sizeof(void *);

	*(void **)ptr.p = p;

	ptr.n += sizeof(void *);

	return ptr.p;
}
void *Realloc16(void *p, size_t n) {
	ptradr_t ptr;

	ptr.p = p;

	ptr.n -= sizeof(void *);

	if (!(ptr.p = p = realloc(*(void **)ptr.p, n + 16 + sizeof(void *))))
		return (void *)0;

	ptr.n -= ptr.n%16;
	ptr.n += 16 - sizeof(void *);

	*(void **)ptr.p = p;

	ptr.n += sizeof(void *);

	return ptr.p;
}
void Free16(void *p) {
	ptradr_t ptr;

	ptr.p = p;

	ptr.n -= sizeof(void *);

	free(*(void **)ptr.p);
}

__forceinline static void MemCopy(void *dst, const void *src, size_t n) {
	size_t i;

	i = 0;

	while(i + 0x80 <= n) {
		_mm_store_ps((float *)(((char *)dst) + i + 0x00),
		             _mm_load_ps((const float *)(((char *)src) + i + 0x00)));
		_mm_store_ps((float *)(((char *)dst) + i + 0x10),
		             _mm_load_ps((const float *)(((char *)src) + i + 0x10)));
		_mm_store_ps((float *)(((char *)dst) + i + 0x20),
		             _mm_load_ps((const float *)(((char *)src) + i + 0x20)));
		_mm_store_ps((float *)(((char *)dst) + i + 0x30),
		             _mm_load_ps((const float *)(((char *)src) + i + 0x30)));
		_mm_store_ps((float *)(((char *)dst) + i + 0x40),
		             _mm_load_ps((const float *)(((char *)src) + i + 0x40)));
		_mm_store_ps((float *)(((char *)dst) + i + 0x50),
		             _mm_load_ps((const float *)(((char *)src) + i + 0x50)));
		_mm_store_ps((float *)(((char *)dst) + i + 0x60),
		             _mm_load_ps((const float *)(((char *)src) + i + 0x60)));
		_mm_store_ps((float *)(((char *)dst) + i + 0x70),
		             _mm_load_ps((const float *)(((char *)src) + i + 0x70)));
		i += 0x80;
	}

	while(i + 0x10 <= n) {
		_mm_store_ps((float *)(((char *)dst) + i),
		             _mm_load_ps((const float *)(((char *)src) + i)));
		i += 0x10;
	}

	while(i + 0x04 <= n) {
		*((int *)(((char *)dst) + i)) = *((int *)(((char *)src) + i));
		i += 0x04;
	}

	while(i < n) {
		*(((char *)dst) + i) = *(((char *)src) + i);
		i++;
	}
}
__forceinline static void ByteCopy(void *dst, const void *src, size_t n) {
	size_t i;

#if 1
	i = 0;
	while(i+4 < n) {
		*(int *)(((char *)dst) + i) = *(int *)(((char *)src) + i);
		i += 4;
	}
	while(i < n) {
		*(((char *)dst) + i) = *(((char *)src) + i);
		i++;
	}
#else
	for(i=0; i<n; i++)
		*(((char *)dst) + i) = *(((char *)src) + i);
#endif
}

__forceinline int Rand(int l, int h) {
	return l + rand()%(h - l);
}
__forceinline void FillRand(void *dst, size_t n) {
	size_t i;

	for(i=0; i<n; i++) {
		*(((char *)dst) + i) = Rand(0, 127);
	}
}

int main() {
#define T 0x1000
#define N (0x1000*0x40)
	double s, e, a[2];
	size_t i;
	void *p, *q;
	void *P[2];
	int j;

	p = Malloc16(N);
	q = Malloc16(N);

	P[0] = p;
	P[1] = q;

	a[0] = 0;
	a[1] = 0;

	printf("Preparing to run tests...\n");
	fflush(stdout);
	FillRand(p, N);
	FillRand(q, N);

	printf("Running tests...\n");
	fflush(stdout);

	for(i=0; i<T; i++) {
		printf("Running test #%i\n", (int)i);
		fflush(stdout);

		j = Rand(0, 1000) >= 500 ? 1 : 0;

		s = timer();
		MemCopy(P[j], P[j^1], N);
		e = timer();
		a[0] += e - s;

		s = timer();
		ByteCopy(P[j^1], P[j], N);
		/*memcpy(P[j^1], P[j], N);*/
		e = timer();
		a[1] += e - s;
	}

	printf("MemCopy() total     : %f second(s)\n", a[0]);
	printf("memcpy() total      : %f second(s)\n", a[1]);
	printf("\n");

	printf("MemCopy() average   : %f second(s)\n", a[0]/T);
	printf("memcpy() average    : %f second(s)\n", a[1]/T);

	Free16(p);
	Free16(p);

	return 0;
}
