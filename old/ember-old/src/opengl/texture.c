#include <ember/texture.h>

static int __tex_alloc( struct stTexture *self )
{
	self->name            = ( char * )0;
	self->tex             = 0;
	self->format          = GL_RGBA;
	self->internal_format = GL_RGBA;
	self->width           = 0;
	self->height          = 0;
	self->depth           = 0;

	self->next = ( texture_t )0;
	if( ( self->prev = g_tex_tail ) != ( texture_t )0 )
		g_tex_tail->next = self;
	else
		g_tex_head = self;
	g_tex_tail     = self;

	return 1;
}
static void __tex_dealloc( struct stTexture *self )
{
	if( self->name )
		free( ( void * )self->name );

	if( self->prev )
		self->prev->next = self->next;
	if( self->next )
		self->next->prev = self->prev;

	if( g_tex_head == self )
		g_tex_head = self->next;
	if( g_tex_tail == self )
		g_tex_tail = self->prev;
}

int tex_init()
{
	g_tex_pool =
	    gc_register_pool( sizeof( struct stTexture ), ( gc_init_t )__tex_alloc,
	        ( gc_deinit_t )__tex_dealloc );
	if( !g_tex_pool )
		return 0;
	return 1;
}
void tex_deinit()
{
	gc_unregister_pool( g_tex_pool );
}

texture_t tex_alloc( const char *name )
{
	texture_t tex;

	if( !( tex = ( texture_t )gc_alloc( g_tex_pool ) ) )
		return ( texture_t )0;

	tex->name = name ? strdup( name ) : ( char * )0;
	return tex;
}
void tex_dealloc( texture_t tex )
{
	gc_release( ( void * )tex );
}

void tex_set_name( texture_t tex, const char *name )
{
	if( tex->name )
		free( ( void * )tex->name );

	tex->name = name ? strdup( name ) : ( char * )0;
}
void tex_set_format( texture_t tex, GLenum format )
{
	tex->format = format;
}
void tex_set_internal_format( texture_t tex, GLenum internal_format )
{
	tex->internal_format = internal_format;
}
void tex_set_size( texture_t tex, unsigned int w, unsigned int h,
    unsigned int d )
{
	tex->width  = w;
	tex->height = h;
	tex->depth  = d;
}

const char *tex_name( texture_t tex )
{
	return tex->name;
}
GLuint tex_gl_id( texture_t tex )
{
	return tex->tex;
}
GLenum tex_format( texture_t tex )
{
	return tex->format;
}
GLenum tex_internal_format( texture_t tex )
{
	return tex->internal_format;
}
unsigned int tex_width( texture_t tex )
{
	return tex->width;
}
unsigned int tex_height( texture_t tex )
{
	return tex->height;
}
unsigned int tex_depth( texture_t tex )
{
	return tex->depth;
}
texture_t tex_before( texture_t tex )
{
	return tex->prev;
}
texture_t tex_after( texture_t tex )
{
	return tex->next;
}
texture_t tex_first()
{
	return g_tex_head;
}
texture_t tex_last()
{
	return g_tex_tail;
}
