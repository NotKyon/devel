#include <meshio/meshio.h>

void *meshio_memory( void *p, size_t n )
{
	if( p && !n ) {
		free( p );
		return ( void * )0;
	}

	if( p && n )
		return realloc( p, n );

	return malloc( n );
}

size_t meshio_data_type_size( EMeshIODataType kind )
{
	switch( kind ) {
	case EMeshIO_Byte:
		return 1;
	case EMeshIO_Short:
		return 2;
	case EMeshIO_Int:
	case EMeshIO_Float:
		return 4;
	case EMeshIO_Int64:
	case EMeshIO_Double:
		return 8;
	default:
		return 0;
	}
}
