#if _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef uint16_t u16_t;
typedef uint32_t u32_t;

#define ASSERT( expr )\
	if( !( expr ) ) \
		doAssert( __FILE__, __LINE__, __func__, #expr, "Assert failed" )

void doAssert( const char *file, int line, const char *func, const char *expr,
const char *msg ) {
	fflush( stdout );

	fprintf( stderr, "\n[%s(%i) %s] %s: %s\n", file, line, func, msg, expr );
	fflush( stderr );

	volatile int doExit = 1;

#if _WIN32
	if( IsDebuggerPresent() ) {
		DebugBreak();
	}
#elif __GNUC__ || __clang__
	__builtin_trap();
#endif

	if( doExit==0 ) {
		return;
	}

	exit( EXIT_FAILURE );
}

#if _DEBUG
# define DEBUG_LOG( ... )\
	doDebugLog( __FILE__, __LINE__, __func__, __VA_ARGS__ )
#else
# define DEBUG_LOG( ... )\
	( void )0
#endif
void doDebugLog( const char *file, int line, const char *func,
const char *format, ... ) {
	va_list args;
	char buf[ 1024 ];

	va_start( args, format );
#if __STDC_WANT_SECURE_LIB__
	vsprintf_s( buf, sizeof( buf ), format, args );
#else
	vsnprintf( buf, sizeof( buf ), format, args );
	buf[ sizeof( buf ) - 1 ] = '\0';
#endif
	va_end( args );

	fprintf( stderr, "[%s(%i) %s] DEBUG: %s\n", file, line, func, buf );
	fflush( stderr );
}

struct pixelVector2_t {
	u16_t x;
	u16_t y;
};
struct pixelRectangle_t {
	pixelVector2_t off;
	pixelVector2_t res;
};

// this #if 0'd code is unnecessary but possibly will be useful in the future
// keep it
#if 0

// determine whether a point is within a rectangle
inline bool isPixelPointInRect( const pixelRectangle_t &enclosingRect,
const pixelVector2_t &testPoint ) {
	// test each axis to see if a point is outside the enclosing rectangle
	// if so, early exit (we know it's not in the rectangle)

	if( testPoint.x - enclosingRect.off.x >= enclosingRect.res.x ) {
		return false;
	}
	if( testPoint.y - enclosingRect.off.y >= enclosingRect.res.y ) {
		return false;
	}

	// it's within the rectangle because neither test failed
	return true;
}

// determine whether a rectangle intersects another rectangle
inline bool isPixelRectIntersectingRect( const pixelRectangle_t &rectA,
const pixelRectangle_t &rectB ) {
	if( isPixelPointInRect( rectA, rectB.off ) ) {
		return true;
	}

	pixelVector2_t endPoint = {
		rectB.off.x + rectB.res.x,
		rectB.off.y + rectB.res.y
	};

	if( isPixelPointInRect( rectA, endPoint ) ) {
		return true;
	}

	return false;
}
#endif //#if 0

class RectangleAllocator {
public:
	RectangleAllocator();
	~RectangleAllocator();

	bool init( const pixelVector2_t &res );
	void fini();

	void *allocateId( u16_t textureId, const pixelVector2_t &res );
	void freeId( void *p );

private:
	struct node_t {
		u16_t textureId; //0=free space, 1+=texture ID
		pixelRectangle_t rect; //the space this rectangle occupies
		node_t *prev, *next; //links
	};
	node_t *head, *tail;
	pixelVector2_t resolution;

	// this will allocate a blank node by default
	node_t *allocateNode( u16_t offX, u16_t offY, u16_t resX, u16_t resY );
	void freeNode( node_t *node );

	node_t *findBestFitNode( u16_t resX, u16_t resY ) const;
	bool splitNode( node_t *node, u16_t textureId, u16_t resX, u16_t resY );
};

// constructor
RectangleAllocator::RectangleAllocator(): head( NULL ), tail( NULL ) {
	resolution.x = 0;
	resolution.y = 0;
}
// destructor
RectangleAllocator::~RectangleAllocator() {
	fini();
}

// initialize
bool RectangleAllocator::init( const pixelVector2_t &res ) {
	// already initialized?
	if( head!=NULL ) {
		return true;
	}
	
	// register the total resolution (must be done before allocating free space
	// node; otherwise the asserts will fail)
	resolution.x = res.x;
	resolution.y = res.y;

	// create the initial free space node
	node_t *p = allocateNode( 0, 0, res.x, res.y );
	if( !p ) {
		return false;
	}

	// the system is in the valid initial state
	return true;
}
// finish
void RectangleAllocator::fini() {
	// don't waste time doing proper unlinks
	while( head!=NULL ) {
		node_t *next = head->next;

		free( ( void * )head );
		head = next;
	}

	// nullify this setting so it's not accidentally used
	tail = NULL;

	// there's no resolution anymore
	resolution.x = 0;
	resolution.y = 0;
}

// allocate an identifier, returning a token
void *RectangleAllocator::allocateId( u16_t textureId,
const pixelVector2_t &res ) {
	DEBUG_LOG( "textureId=%i, res={%i,%i}", ( int )textureId, ( int )res.x,
		( int )res.y );

	ASSERT( textureId>0 );
	ASSERT( res.x>0 );
	ASSERT( res.y>0 );

	node_t *best = findBestFitNode( res.x, res.y );
	if( !best ) {
		return NULL;
	}

	if( !splitNode( best, textureId, res.x, res.y ) ) {
		return NULL;
	}

	return ( void * )best;
}
// deallocate a previously allocated identifier
void RectangleAllocator::freeId( void *p ) {
	if( !p ) {
		return;
	}

#if _DEBUG
	// ensure the passed node was allocated in here
	node_t *test;
	for( test=head; test!=NULL; test=test->next ) {
		if( test==( node_t * )p ) {
			break;
		}
	}

	// if this is NULL it means we exhausted the whole list without finding the
	// node; therefore the pointer passed is invalid either because it was
	// already removed from the list -or- it was never in this list to begin
	// with
	ASSERT( test!=NULL );
#endif

	node_t *node = ( node_t * )p;

	node->textureId = 0;
}

// allocate a free-space node
RectangleAllocator::node_t *RectangleAllocator::allocateNode( u16_t offX,
u16_t offY, u16_t resX, u16_t resY ) {
	node_t *node;

	DEBUG_LOG( "off={%i,%i}, res={%i,%i}", ( int )offX, ( int )offY,
		( int )resX, ( int )resY );

	// verify parameters
	ASSERT( resX>0 );
	ASSERT( resY>0 );
	ASSERT( ( offX + resX )<=resolution.x );
	ASSERT( ( offY + resY )<=resolution.y );

	// NOTE: we don't check for intersections because when a split occurs it
	//       has to operate without altering the existing node

	// allocate
	node = ( node_t * )malloc( sizeof( *node ) );
	if( !node ) {
		return NULL;
	}

	// default settings (free space)
	node->textureId = 0;
	node->rect.off.x = offX;
	node->rect.off.y = offY;
	node->rect.res.x = resX;
	node->rect.res.y = resY;

	// link
	node->next = NULL;
	node->prev = tail;
	if( node->prev!=NULL ) {
		node->prev->next = node;
	} else {
		head = node;
	}
	tail = node;

	// node is ready now
	return node;
}
// deallocate a node (it's okay to pass NULL in)
void RectangleAllocator::freeNode( node_t *node ) {
	// accept NULL input; we need to manually unlink so we can't just pass to
	// free
	if( !node ) {
		return;
	}

	// unlink
	if( node->prev!=NULL ) {
		node->prev->next = node->next;
	} else {
		head = node->next;
	}
	if( node->next!=NULL ) {
		node->next->prev = node->prev;
	} else {
		tail = node->prev;
	}

	// now deallocate the memory
	free( ( void * )node );
}

// find the node which has the best fit for this
RectangleAllocator::node_t *RectangleAllocator::findBestFitNode( u16_t resX,
u16_t resY ) const {
	// ensure parameters match
	ASSERT( resX>0 );
	ASSERT( resY>0 );
	ASSERT( resX<=resolution.x );
	ASSERT( resY<=resolution.y );

	// scoring system
	struct scoredNode_t {
		node_t *node;
		u32_t score;
	};

	scoredNode_t best = { NULL, 0 };

	// find the node with the least wasted space
	for( node_t *node=head; node!=NULL; node=node->next ) {
		// skip if minimum requirements aren't met
		if( node->textureId!=0 ) {
			continue;
		}
		if( node->rect.res.x<resX || node->rect.res.y<resY ) {
			continue;
		}

		// construct the test object
		scoredNode_t test = {
			node,
			( ( u32_t )node->rect.res.x )*( ( u32_t )node->rect.res.y )
		};

		// test against the current high score and keep the better of the two
		if( test.score>best.score ) {
			best = test;
		}
	}

	// return the currently selected node (NULL if none met minimum reqs)
	return best.node;
}
// split a node into one-to-three nodes, marking one as used
bool RectangleAllocator::splitNode( node_t *node, u16_t textureId, u16_t resX,
u16_t resY ) {
	// ensure all the parameters match
	ASSERT( node!=NULL );
	if( node->textureId!=0 ) {
		DEBUG_LOG( "BAD: node->textureId=%i", ( int )node->textureId );
	}
	ASSERT( node->textureId==0 ); //must be a free node
	ASSERT( textureId>0 );
	ASSERT( resX>0 );
	ASSERT( resY>0 );
	ASSERT( resX<=node->rect.res.x );
	ASSERT( resY<=node->rect.res.y );

	// figure out the remaining space
	u16_t splitRes[ 2 ] = {
		( u16_t )( node->rect.res.x - resX ),
		( u16_t )( node->rect.res.y - resY )
	};
	u32_t bestSplit = splitRes[ 0 ]>splitRes[ 1 ] ? 0 : 1;

	// select the offsets of each remaining point
	pixelVector2_t splitPoints[ 2 ] = {
		( u16_t )( node->rect.off.x + resX ), node->rect.off.y,
		node->rect.off.x, ( u16_t )( node->rect.off.y + resY )
	};
	pixelVector2_t splitExtents[ 2 ] = {
		splitRes[ 0 ], bestSplit==0 ? node->rect.res.y : resY,
		bestSplit==1 ? node->rect.res.x : resX, splitRes[ 1 ]
	};

	DEBUG_LOG( "split1: %i, %i; %i, %i", ( int )splitPoints[ 0 ].x,
		( int )splitPoints[ 0 ].y, ( int )splitExtents[ 0 ].x,
		( int )splitExtents[ 0 ].y );
	DEBUG_LOG( "split2: %i, %i; %i, %i", ( int )splitPoints[ 1 ].x,
		( int )splitPoints[ 1 ].y, ( int )splitExtents[ 1 ].x,
		( int )splitExtents[ 1 ].y );

	// splits
	node_t *splits[ 2 ] = { NULL, NULL };

	// make each split
	for( u32_t i=0; i<2; ++i ) {
		// don't make a split here if there's no space
		if( splitRes[ i ]==0 ) {
			continue;
		}

		// there's space for the split; allocate
		splits[ i ] = allocateNode( splitPoints[ i ].x, splitPoints[ i ].y,
			splitExtents[ i ].x, splitExtents[ i ].y );
		if( splits[ i ]==NULL ) {
			// ... clean up (don't leave false free nodes!)
			if( i==1 ) {
				freeNode( splits[ 0 ] );
			}

			// we could not allocate
			return false;
		}
	}

	// list this node as an allocated texture (with the specified resolution)
	node->textureId = textureId;
	node->rect.res.x = resX;
	node->rect.res.y = resY;

	// the node now marked and the partitions are in a valid state
	return true;
}

//------------------------------------------------------------------------------

#if __GNUC__ || __clang__
# define __noreturn __attribute__( ( noreturn ) )
#elif _MSC_VER || __INTEL_COMPILER
# define __noreturn __declspec( noreturn )
#else
# define __noreturn
#endif

__noreturn void fatal( const char *message ) {
	fprintf( stderr, "ERROR: %s\n", message );
	exit( EXIT_FAILURE );
}

int main() {
	RectangleAllocator rectAlloc;

	pixelVector2_t res = { 256, 256 };
	if( !rectAlloc.init( res ) ) {
		fatal( "failed to initialize the RectangleAllocator" );
	}

	void *a, *b, *c, *d;

	pixelVector2_t allocRes = { 128, 128 };
	a = rectAlloc.allocateId( 1, allocRes );
	ASSERT( a!=NULL );
	b = rectAlloc.allocateId( 2, allocRes );
	ASSERT( b!=NULL );
	c = rectAlloc.allocateId( 3, allocRes );
	ASSERT( c!=NULL );
	d = rectAlloc.allocateId( 4, allocRes );
	ASSERT( d!=NULL );

	void *e;
	
	e = rectAlloc.allocateId( 5, allocRes );
	ASSERT( e==NULL ); // the above is expected to return NULL (no slots left)

	rectAlloc.freeId( c ); //this should become free space
	c = NULL;

	e = rectAlloc.allocateId( 5, allocRes );
	ASSERT( e!=NULL ); //the above should now work

	return EXIT_SUCCESS;
}
