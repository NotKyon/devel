#define GB_INIT 1
#include <gamebase.h>

//
//	The following demonstrates how to use some of the timing functions.
//

void GBMain() {
	//
	//	NOTE: gbMillisecs() returns the number of milliseconds that have elapsed
	//	      since the system started.
	//

	gbPrint(gbF("Milliseconds: %u", gbMillisecs()));
	gbDelay(500);
	gbPrint(gbF("Milliseconds: %u", gbMillisecs()));
}
