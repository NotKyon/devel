/*
// Original

// from: iodoom3: neo/renderer/Material.h(92)

// note: keep opNames[] in sync with changes
typedef enum {
	OP_TYPE_ADD,
	OP_TYPE_SUBTRACT,
	OP_TYPE_MULTIPLY,
	OP_TYPE_DIVIDE,
	OP_TYPE_MOD,
	OP_TYPE_TABLE,
	OP_TYPE_GT,
	OP_TYPE_GE,
	OP_TYPE_LT,
	OP_TYPE_LE,
	OP_TYPE_EQ,
	OP_TYPE_NE,
	OP_TYPE_AND,
	OP_TYPE_OR,
	OP_TYPE_SOUND
} expOpType_t;

// from: iodoom3: neo/renderer/Material.cpp(2328)

char *opNames[] = {
	"OP_TYPE_ADD",
	"OP_TYPE_SUBTRACT",
	"OP_TYPE_MULTIPLY",
	"OP_TYPE_DIVIDE",
	"OP_TYPE_MOD",
	"OP_TYPE_TABLE",
	"OP_TYPE_GT",
	"OP_TYPE_GE",
	"OP_TYPE_LT",
	"OP_TYPE_LE",
	"OP_TYPE_EQ",
	"OP_TYPE_NE",
	"OP_TYPE_AND",
	"OP_TYPE_OR"
};

// note the missing "OP_TYPE_SOUND" from opNames[] above
*/

/*
 * Macro Method
 */

#define LIST()\
	ITEM(ADD),\
	ITEM(SUBTRACT),\
	ITEM(MULTIPLY),\
	ITEM(DIVIDE),\
	ITEM(MOD),\
	ITEM(TABLE),\
	ITEM(GT),\
	ITEM(GE),\
	ITEM(LT),\
	ITEM(LE),\
	ITEM(EQ),\
	ITEM(NE),\
	ITEM(AND),\
	ITEM(OR),\
	ITEM(SOUND)

#define ITEM(x) OP_TYPE_##x
typedef enum {
	LIST()
} expOpType_t;

#undef ITEM

#define ITEM(x) "OP_TYPE_" #x
char *opNames[] = {
	LIST()
};
#undef ITEM

