#include <stdio.h>
#include <unistd.h>
#include <string.h> /*implicit declaration of memset*/

#include <Ember/Scheduler.h>
#include <Ember/Memory.h>
#include <Ember/Logging.h>

#define EE_USE_LOG 0 /* set to 1 to use logging; 0 to not */
#include "Debug.h"

#include "Threading.h"
#include "Array.h"

/* -------------------------------------------------------------------------- */

struct Scheduler_s {
	struct Scheduler_s *prev, *next;
	Mutex_t mutex;

	/*
	 * EXCLUSIVE JOB QUEUES
	 * The scheduler has exclusive job queues with jobs that only it can
	 * complete. (For example, an OpenGL server can only complete its own jobs.)
	 * Once the scheduler has run through all of its exclusive jobs, it will
	 * execute jobs from the global job queues.
	 *
	 * If this scheduler is a background scheduler, then the scheduler will only
	 * execute jobs from the exclusive list.
	 */
	struct {
		struct JobQueue_s *head, *tail;
		struct JobQueue_s *cur;
		Mutex_t mutex;
	} exclusive;

	struct {
		int cpu; /*which cpu is this on? (-1 for "background scheduler")*/
		Thread_t thread;
		bool terminate; /*time to end?*/
	} task;
};

struct JobQueue_s {
	/* current / (potentially) exclusive scheduler */
	struct Scheduler_s *sched;

	Mutex_t mutex;

	/*
	 * JOB QUEUE LINKS
	 * A job queue's linked list (owner) can change at any time and is usually
	 * never constant. Several factors contribute to which list the job queue is
	 * actually in.
	 *
	 * First, if 'flags.isActive' is cleared then the job queue is in the
	 * "dead list." Queues in the dead list are recycled on the next frame, so
	 * that malloc() calls don't have to occur frequently.
	 *
	 * Next, if 'sched' is set then the job queue is in the scheduler's
	 * "exclusive" list. (That is, jobs that only that scheduler can complete.)
	 *
	 * Otherwise, 'flags.isActive' is set and 'sched' is NULL, the job queue is
	 * in the "global" list. Schedulers will grab from the "global" list if
	 * there aren't any _jobs_ available in its exclusive lists.
	 */
	struct JobQueue_s *prev, *next;

	/*
	 * JOBS
	 * A job is an individual unit of work for the CPU or GPGPU to execute and
	 * complete. This unit of work is generally a small task that is worth the
	 * overhead of being scheduled. (For example, making a job that schedules
	 * other jobs doesn't make very much sense for this system.)
	 */
	size_t numJobs; /*number of items in the jobs array*/
	size_t curJob; /*current working item (used when running through queues)*/
	size_t refJobs; /*number of jobs completed so far*/
	struct Job_s *jobs; /*jobs array*/

	/*
	 * PHASES
	 * Jobs run out-of-order. For example, if in the queue you have the order
	 * "1, 2, 3, 4" (assume that each number represents an individual job), that
	 * might get run as "2, 4, 3, 1" one time, then "1, 3, 4, 2" another time,
	 * or any other order. Sometimes it's useful to ensure that certain jobs
	 * complete before others. That is, it's sometimes useful for jobs to be
	 * executed in phases.
	 *
	 * Once the 'completed' field is equal to 'phases[curPhase]', 'curPhase' is
	 * incremented and the next set of jobs can be run.
	 */
	size_t numPhases; /*number of items in the 'phases' array*/
	size_t curPhase; /*current point in the 'phases' array*/
	size_t *phases; /*'synchronization points' array*/

	/* flags */
	struct {
		bool exclusive:1; /*only usable by 'sched'?*/
		bool isActive:1; /*is this job queue currently active?*/
		bool deactivate:1; /*deactivate on completion?*/
	} flags;
};
#define JOBQUEUE_PHASE_ARRAY_ALIGNMENT 16
#define JOBQUEUE_JOB_ARRAY_ALIGNMENT 64

struct Job_s {
	ee_JobFunc_t fn;
	void *p;
};

/* -------------------------------------------------------------------------- */

static bool g_sched_init = false;
static bool g_sched_force = false;

static unsigned int g_sched_numRealCPUs = 0;
static unsigned int g_sched_numFakeCPUs = 0;
static unsigned int g_sched_numUsedCPUs = 0;

/* queue lists */
static struct JobQueue_s *g_sched_queueLiveHead = (struct JobQueue_s *)0;
static struct JobQueue_s *g_sched_queueLiveTail = (struct JobQueue_s *)0;
static struct JobQueue_s *g_sched_queueLiveCur = (struct JobQueue_s *)0;
static struct JobQueue_s *g_sched_queueDeadHead = (struct JobQueue_s *)0;
static struct JobQueue_s *g_sched_queueDeadTail = (struct JobQueue_s *)0;
static Mutex_t g_sched_queueLiveMutex;
static Mutex_t g_sched_queueDeadMutex;

/* schedulers assigned to CPUs */
static struct Scheduler_s *g_sched_cpu[64];

/* list of schedulers */
static struct Scheduler_s *g_sched_head = (struct Scheduler_s *)0;
static struct Scheduler_s *g_sched_tail = (struct Scheduler_s *)0;

/* dispatcher task */
static Thread_t g_sched_dispatcherThread;

/* scheduler globals mutex */
static Mutex_t g_sched_mutex;

/* -------------------------------------------------------------------------- */

static unsigned int CountCPUs() {
	unsigned int cpuCount;
#if MK_MSWIN
	SYSTEM_INFO si;
#endif

	EE_LOG_FN_ENTER();

#if MK_MSWIN
	memset(&si, 0, sizeof(si));
	GetSystemInfo(&si);

	cpuCount = (unsigned int)si.dwNumberOfProcessors;
#elif MK_LINUX
	if ((cpuCount = sysconf(_SC_NPROCESSORS_ONLN)) < 1)
		cpuCount = 1;
#else
	EE_TODO("Need to port CountCPUs()");
	cpuCount = 1;
#endif

	EE_LOG_FN_LEAVE();

	return cpuCount < 64 ? cpuCount : 64;
}

/* -------------------------------------------------------------------------- */

/* Unlink a job queue from whatever list it's in */
static void UnlinkJobQueue(struct JobQueue_s *p) {
	struct JobQueue_s **head, **tail;
	Mutex_t *M;

	if (!p->flags.isActive) {
		EE_LOG("   UnlinkJobQueue: dead list");
		M = &g_sched_queueDeadMutex;
		head = &g_sched_queueDeadHead;
		tail = &g_sched_queueDeadTail;
	} else if(p->sched) {
		EE_LOG("    UnlinkJobQueue: scheduler exclusive list");
		M = &p->sched->exclusive.mutex;
		head = &p->sched->exclusive.head;
		tail = &p->sched->exclusive.tail;
	} else {
		EE_LOG("    UnlinkJobQueue: live list");
		M = &g_sched_queueLiveMutex;
		head = &g_sched_queueLiveHead;
		tail = &g_sched_queueLiveTail;
	}

	Task_LockMutex(*M);

	if (p->prev)
		p->prev->next = p->next;
	if (p->next)
		p->next->prev = p->prev;

	if (*head==p)
		*head = p->next;
	if (*tail==p)
		*tail = p->prev;

	p->prev = (struct JobQueue_s *)0;
	p->next = (struct JobQueue_s *)0;

	Task_UnlockMutex(*M);
}

/* -------------------------------------------------------------------------- */

#if __clang__
# define SELFROMZERO__(a) (-(((a) ^ -(a)) >> (sizeof(a)*8 - 1)))
# define SELFROMZERO(a,x,y) (((y)&SELFROMZERO__(a))|((x)&~SELFROMZERO__(a)))
#else
# define SELFROMZERO(a,x,y) (!(a) ? (x) : (y))
#endif

/* Scheduler's main thread (runs whatever the current job is) */
static ThreadResult_t THREADPROCAPI SchedulerTask(void *self) {
	struct JobQueue_s *q;
	ee_Scheduler_t sched;
	struct Job_s job;

	sched = (ee_Scheduler_t)self;

	while(!(sched->task.terminate | g_sched_force)) {
		if (!Task_TryLockMutex(sched->exclusive.mutex)) {
			Task_Relinquish();
			continue;
		}

		sched->exclusive.cur = SELFROMZERO(sched->exclusive.cur,
		                                   sched->exclusive.head,
		                                   sched->exclusive.cur);

		Task_UnlockMutex(sched->exclusive.mutex);

		if (!sched->exclusive.cur) {
			Task_Relinquish();
			continue;
		}

		/*EE_TODO("Run one of the jobs from the job queue!");*/
		q = sched->exclusive.cur;
		EE_LOG(va("Current job: %u", (unsigned int)sched->exclusive.cur));
		job = q->jobs[q->curJob++];

		/*
		 * If sched->exclusive.cur->flags.exclusive==1 then a bit-mask will be
		 * generated of 0xFFFFFFFF (32-bit) or 0xFFFFFFFFFFFFFFFF (64-bit),
		 * which means sched->exclusive.cur->sched will stay. Otherwise, it will
		 * be set to zero.
		 *
		 * No branching! (This does rely on a two's-complement system, but
		 * that's all targetted hardware.)
		 */
		/*
		sched->exclusive.cur->sched = sched->exclusive.cur->sched &
			-(size_t)sched->exclusive.cur->flags.exclusive;
		sched->exclusive.cur = (struct JobQueue_s *)0;
		*/
		if (sched->exclusive.cur->flags.exclusive) {
			if (sched->exclusive.cur->curJob==sched->exclusive.cur->numJobs)
				sched->exclusive.cur = sched->exclusive.cur->next;
		} else {
			sched->exclusive.cur->sched = (struct Scheduler_s *)0;
			sched->exclusive.cur = (struct JobQueue_s *)0;
		}

		job.fn(job.p);
		if (++q->refJobs==q->phases[q->curPhase])
			q->curPhase++;

		/*EE_TODO("More fair exclusive scheduling.");*/
	}

	return (ThreadResult_t)0;
}

/*
 * Dispatch a job queue to the schedulers
 *
 * This function is responsible for enumerating all of the queues and
 * dispatching them to schedulers. The idea is to reduce the number of stalls
 * that occur by skipping any scheduler that is currently locked. Additionally,
 * it will skip any queue that is currently locked or in use. This should help
 * to reduce stalls in the pipeline.
 */
static ThreadResult_t THREADPROCAPI DispatcherTask(void *self) {
	struct Scheduler_s *sched = (struct Scheduler_s *)0;
	struct JobQueue_s *queue = (struct JobQueue_s *)0;

	if (self){/*unused*/}

	/* main loop */
	while(!g_sched_force) {
		/* look for schedulers */
		for(sched=g_sched_head; sched && !g_sched_force; sched=sched->next) {
			/* if the scheduler can't be locked, skip it */
			if (!Task_TryLockMutex(sched->exclusive.mutex))
				continue;

			/* does the scheduler have a current queue? */
			if (!sched->exclusive.cur) {
				/* queue = !queue ? g_sched_queueLiveHead : queue; */
				queue = SELFROMZERO(queue, g_sched_queueLiveHead, queue);

				/* try to find a suitable queue for the scheduler */
				while(queue && !g_sched_force) {
					/* is this queue free? */
					if (!queue->sched) {
						/* the queue might be in the process of being created;
						   lock it. */
						if (Task_TryLockMutex(queue->mutex)) {
							/* if jobs are available, assign the queue */
							if (queue->curJob < queue->numJobs)
								sched->exclusive.cur = queue;

							/* unlock the queue so it can be accessed now */
							Task_UnlockMutex(queue->mutex);
						}
					}

					/* go the next queue */
					queue = queue->next;

					/* if the scheduler had a queue set, exit this loop */
					if (sched->exclusive.cur) {
						sched->exclusive.cur->sched = sched;
						break;
					}
				}
			}

			/* now done with this scheduler; unlock it then continue the loop */
			Task_UnlockMutex(sched->exclusive.mutex);
		}

		/* as part of the main loop, we should let other threads run */
		Task_Relinquish();
	}

	return (ThreadResult_t)0;
}

#undef SELFROMZERO__
#undef SELFROMZERO

/* -------------------------------------------------------------------------- */

/* Initialize the scheduling system */
bool eeStartScheduler() {
	unsigned int numCPUs;
	unsigned int i;

	if (g_sched_init)
		return true;

	EE_LOG_FN_ENTER();

	/*Task_NullifyMutex(g_sched_queueArrayMutex);*/
	Task_NullifyMutex(g_sched_queueLiveMutex);
	Task_NullifyMutex(g_sched_queueDeadMutex);
	Task_NullifyMutex(g_sched_mutex);

	/* find the number of CPUs on this system */
	g_sched_numRealCPUs = CountCPUs();
	/*g_sched_numFakeCPUs = 0;*/
	/*g_sched_numUsedCPUs = g_sched_numRealCPUs;*/
	g_sched_numUsedCPUs = g_sched_numFakeCPUs
	                    ? g_sched_numFakeCPUs
	                    : g_sched_numRealCPUs;
#if DEBUG||_DEBUG||__debug__
	EE_LOCK_LOG();
	EE_LOG_HR();
	EE_LOG(va("Real CPU Count: %u", g_sched_numRealCPUs));
	EE_LOG(va("Fake CPU Count: %u", g_sched_numFakeCPUs));
	EE_LOG(va("Used CPU Count: %u", g_sched_numUsedCPUs));
	EE_LOG(""); /*newline*/
	if (g_sched_numUsedCPUs > g_sched_numRealCPUs) {
		EE_LOG("    CAUTION! You're trying to use more CPUs than you have!");
		EE_LOG("    Performance probably won't increase in the common case.");
		EE_LOG(""); /*newline*/
	}
	EE_UNLOCK_LOG();
#endif

	/*
	 *	TODO: In the future, maybe the NULL job queue can be a "central" queue
	 *        for adding random/miscellaneous jobs. Seems reasonable.
	 */

	/* initialize the live/dead queue lists (dead is for recycling) */
	if(!Task_InitMutex(g_sched_queueLiveMutex)) {
		/*Task_DeinitMutex(g_sched_queueArrayMutex);*/
		return false;
	}
	if(!Task_InitMutex(g_sched_queueDeadMutex)) {
		Task_DeinitMutex(g_sched_queueLiveMutex);
		/*Task_DeinitMutex(g_sched_queueArrayMutex);*/
		return false;
	}

	g_sched_queueLiveHead = (struct JobQueue_s *)0;
	g_sched_queueLiveTail = (struct JobQueue_s *)0;
	g_sched_queueLiveCur = (struct JobQueue_s *)0;
	g_sched_queueDeadHead = (struct JobQueue_s *)0;
	g_sched_queueDeadTail = (struct JobQueue_s *)0;

	/* initialize the scheduler data */
	if(!Task_InitMutex(g_sched_mutex)) {
		Task_DeinitMutex(g_sched_queueDeadMutex);
		Task_DeinitMutex(g_sched_queueLiveMutex);
		/*Task_DeinitMutex(g_sched_queueArrayMutex);*/
		return false;
	}

	memset((void *)g_sched_cpu, 0, sizeof(g_sched_cpu));

	g_sched_head = (struct Scheduler_s *)0;
	g_sched_tail = (struct Scheduler_s *)0;

	/* allocate a scheduler for each CPU found */
	numCPUs = eeGetCPUCount();
	for(i=0; i<numCPUs; i++) {
		if (!(g_sched_cpu[i] = eeNewScheduler())) {
			do {
				eeDeleteScheduler(g_sched_cpu[--i]);
			} while(i != 0);
		}

		g_sched_cpu[i]->task.cpu = (int)i;
		Task_SetThreadCPU(g_sched_cpu[i]->task.thread, i);
	}

	/* start the disptcher thread */
	if (!Task_InitThread(g_sched_dispatcherThread, DispatcherTask, (void *)0)) {
		for(i=0; i<numCPUs; i++)
			eeDeleteScheduler(g_sched_cpu[--i]);

		Task_DeinitMutex(g_sched_mutex);
		Task_DeinitMutex(g_sched_queueDeadMutex);
		Task_DeinitMutex(g_sched_queueLiveMutex);

		EE_LOG_FN_LEAVE();
		return false;
	}

	/* scheduler system has been initialized */
	g_sched_force = false;
	g_sched_init = true;

	EE_LOG_FN_LEAVE();
	return true;
}

/* Deinitialize the scheduling system */
void eeStopScheduler() {
	/* if we're not initialized, no need to exit */
	if (!g_sched_init)
		return;

	EE_LOG_FN_ENTER();

	/* force deletion of certain items */
	g_sched_force = true;

	/* delete all schedulers */
	Task_LockMutex(g_sched_mutex);
	while(g_sched_head)
		eeDeleteScheduler(g_sched_head);
	Task_UnlockMutex(g_sched_mutex);

	Task_DeinitMutex(g_sched_mutex);
	Task_NullifyMutex(g_sched_mutex);

	/* turn all living queues into dead queues */
	Task_LockMutex(g_sched_queueLiveMutex);
	EE_LOG("    Turn all living queues into dead queues");
	while(g_sched_queueLiveHead)
		eeDeleteJobQueue(g_sched_queueLiveHead);
	Task_UnlockMutex(g_sched_queueLiveMutex);

	Task_DeinitMutex(g_sched_queueLiveMutex);
	Task_NullifyMutex(g_sched_queueLiveMutex);

	/* now actually delete all the dead queues */
	Task_LockMutex(g_sched_queueDeadMutex);
	EE_LOG("    Now actually delete all the dead queues");
	while(g_sched_queueDeadHead)
		eeDeleteJobQueue(g_sched_queueDeadHead);
	Task_UnlockMutex(g_sched_queueDeadMutex);

	Task_DeinitMutex(g_sched_queueDeadMutex);
	Task_NullifyMutex(g_sched_queueDeadMutex);

	/* reset certain values */
	g_sched_numFakeCPUs = 0;

	/* we're no longer initialized */
	g_sched_init = false;

	EE_LOG_FN_LEAVE();
}

/* -------------------------------------------------------------------------- */

/* Retrieve the real number of logical cores in the system */
unsigned int eeGetRealCPUCount() {
	return g_sched_numRealCPUs;
}

/* Retrieve the number of cores available to the scheduler */
unsigned int eeGetCPUCount() {
	/*return g_sched_numFakeCPUs ? g_sched_numFakeCPUs : g_sched_numRealCPUs;*/
	return g_sched_numUsedCPUs;
}

/* Set the "virtual" CPU count */
void eeSetCPUCount(unsigned int numCPUs) {
	EE_ASSERT(g_sched_init == false /*can't change after initialization*/);

	g_sched_numFakeCPUs = numCPUs > 64 ? 64 : numCPUs;
/*	g_sched_numUsedCPUs = g_sched_numFakeCPUs
	                    ? g_sched_numFakeCPUs
	                    : g_sched_numRealCPUs; */
}

/* -------------------------------------------------------------------------- */

/* Allocate a scheduler */
ee_Scheduler_t eeNewScheduler() {
	ee_Scheduler_t sched;

	sched = (ee_Scheduler_t)eeMemory((void *)0, sizeof(*sched));

	if (!Task_InitMutex(sched->mutex))
		return (ee_Scheduler_t)eeMemory((void *)sched, 0);

	sched->exclusive.head = (struct JobQueue_s *)0;
	sched->exclusive.tail = (struct JobQueue_s *)0;
	sched->exclusive.cur = (struct JobQueue_s *)0;

	if (!Task_InitMutex(sched->exclusive.mutex)) {
		Task_DeinitMutex(sched->mutex);
		return (ee_Scheduler_t)eeMemory((void *)sched, 0);
	}

	sched->task.cpu = -1;
	if (!Task_InitThread(sched->task.thread, SchedulerTask, (void *)sched)) {
		Task_DeinitMutex(sched->exclusive.mutex);
		Task_DeinitMutex(sched->mutex);
		return (ee_Scheduler_t)eeMemory((void *)sched, 0);
	}

	sched->task.terminate = false;

	Task_LockMutex(g_sched_mutex);
	sched->next = (struct Scheduler_s *)0;
	if ((sched->prev = g_sched_tail) != (struct Scheduler_s *)0)
		g_sched_tail->next = sched;
	else
		g_sched_head = sched;
	g_sched_tail = sched;
	Task_UnlockMutex(g_sched_mutex);

	return sched;
}

/* Delete an allocated scheduler */
ee_Scheduler_t eeDeleteScheduler(ee_Scheduler_t sched) {
	ee_Scheduler_t prev, next;

	if (!sched)
		return (ee_Scheduler_t)0;

	sched->task.terminate = true;

	Task_LockMutex(sched->exclusive.mutex);
	while(sched->exclusive.head)
		UnlinkJobQueue(sched->exclusive.head);
	sched->exclusive.cur = (struct JobQueue_s *)0;
	Task_UnlockMutex(sched->exclusive.mutex);

	Task_LockMutex(sched->mutex);
	if ((prev = sched->prev) != (struct Scheduler_s *)0)
		prev->next = sched->next;
	if ((next = sched->next) != (struct Scheduler_s *)0)
		next->prev = sched->prev;

	sched->prev = (struct Scheduler_s *)0;
	sched->next = (struct Scheduler_s *)0;
	Task_UnlockMutex(sched->mutex);

	Task_LockMutex(g_sched_mutex);
	if (g_sched_head==sched)
		g_sched_head = next;
	if (g_sched_tail==sched)
		g_sched_tail = prev;

	if (sched->task.cpu >= 0) {
		EE_ASSERT(g_sched_cpu[sched->task.cpu] == sched);
		g_sched_cpu[sched->task.cpu] = (ee_Scheduler_t)0;
	}
	Task_UnlockMutex(g_sched_mutex);

	Task_WaitThread(sched->task.thread, 0);
	Task_DeinitThread(sched->task.thread);
	Task_NullifyThread(sched->task.thread);

	Task_DeinitMutex(sched->exclusive.mutex);
	Task_NullifyMutex(sched->exclusive.mutex);

	Task_DeinitMutex(sched->mutex);
	Task_NullifyMutex(sched->mutex);

	eeMemory((void *)sched, 0);

	return (ee_Scheduler_t)0;
}

/* Grab a scheduler for a specific CPU */
ee_Scheduler_t eeGetCPUScheduler(unsigned int cpu) {
	EE_ASSERT(cpu < eeGetCPUCount());
	EE_ASSERT(g_sched_cpu[cpu] != (ee_Scheduler_t)0);
	EE_ASSERT(g_sched_cpu[cpu]->task.cpu == (int)cpu);

	return g_sched_cpu[cpu];
}

/* -------------------------------------------------------------------------- */

/* Create a job queue */
ee_JobQueue_t eeNewJobQueue() {
	struct JobQueue_s *p;

	EE_LOG_FN_ENTER();

	/* try grabbing from the dead queue first */
	Task_LockMutex(g_sched_queueDeadMutex);
	if ((p = g_sched_queueDeadHead) != (struct JobQueue_s *)0) {
		EE_LOG("    Unlinking the dead queue item");
		/*if (p->prev)
			p->prev->next = p->next;
		if (p->next)
			p->next->prev = p->prev;

		if (g_sched_queueDeadHead==p)
			g_sched_queueDeadHead = p->next;
		if (g_sched_queueDeadTail==p)
			g_sched_queueDeadTail = p->prev;

		p->prev = (struct JobQueue_s *)0;
		p->next = (struct JobQueue_s *)0;*/
		UnlinkJobQueue(p);
	}
	Task_UnlockMutex(g_sched_queueDeadMutex);

	/* if nothing was found in the dead queue, try allocating a new slot */
	if (!p) {
		p = (struct JobQueue_s *)eeMemory((void *)0, sizeof(*p));

		/* the job queue's mutexes have to be initialized here */
		if (!Task_InitMutex(p->mutex)) {
			eeMemory((void *)p, 0);
			eeErrorMessage("Failed to initialize a job queue's mutex");
			return (ee_JobQueue_t)0;
		}

		/* allocate the initial job array */
		p->jobs = (struct Job_s *)NewArray(sizeof(struct Job_s));
		SetArrayAlignment((void *)p->jobs, JOBQUEUE_JOB_ARRAY_ALIGNMENT);

		/* allocate the initial phases array */
		p->phases = (size_t *)NewArray(sizeof(size_t));
		SetArrayAlignment((void *)p->phases, JOBQUEUE_PHASE_ARRAY_ALIGNMENT);
	}

	/* we now have access to a job queue handle, let's initialize it */
	EE_ASSERT(p != (struct JobQueue_s *)0);

	p->sched = (struct Scheduler_s *)0;
	/*setting p->self is unnecessary, see above*/

	p->numJobs = 0;
	p->curJob = 0;
	p->refJobs = 0;

	EE_ASSERT(p->jobs != (struct Job_s *)0);
	p->jobs = (struct Job_s *)ResizeArray((void *)p->jobs, 0);
	/* NOTE: the pointer shouldn't actually move or deallocate */

	p->numPhases = 1;
	p->curPhase = 0;

	EE_ASSERT(p->phases != (size_t *)0);
	p->phases = (size_t *)ResizeArray((void *)p->phases, 1); /*see above*/
	p->phases[0] = 0;

	p->flags.exclusive = false;
	p->flags.isActive = true;
	p->flags.deactivate = false;

	/* add the queue to the global list */
	Task_LockMutex(g_sched_queueLiveMutex);
	p->next = (struct JobQueue_s *)0;
	if ((p->prev = g_sched_queueLiveTail) != (struct JobQueue_s *)0)
		g_sched_queueLiveTail->next = p;
	else
		g_sched_queueLiveHead = p;
	g_sched_queueLiveTail = p;
	Task_UnlockMutex(g_sched_queueLiveMutex);

	EE_LOG_FN_LEAVE();
	return p;
}
/* Destroy a job queue (does not wait for any jobs to complete) */
ee_JobQueue_t eeDeleteJobQueue(ee_JobQueue_t p) {
	if (!p)
		return (ee_JobQueue_t)0;

	EE_LOG_FN_ENTER();

	UnlinkJobQueue(p);

	Task_LockMutex(p->mutex);
	p->numJobs = p->curJob
	           = p->refJobs
	           = 0;
	p->numPhases = p->curPhase
	             = 0;
	Task_UnlockMutex(p->mutex);

	if (p->flags.isActive) {
		p->flags.isActive = false;
		p->flags.exclusive = false;
		p->flags.deactivate = false;

		EE_LOG("    Putting in dead queue");

		Task_LockMutex(g_sched_queueDeadMutex);
		p->next = (struct JobQueue_s *)0;
		if ((p->prev = g_sched_queueDeadTail) != (struct JobQueue_s *)0)
			g_sched_queueDeadTail->next = p;
		else
			g_sched_queueDeadHead = p;
		g_sched_queueDeadTail = p;
		Task_UnlockMutex(g_sched_queueDeadMutex);
	} else {
		EE_ASSERT(g_sched_force == true /*attempting to delete dead queue*/);

		EE_LOG("    Forcing deletion...");
		Task_DeinitMutex(p->mutex);
		Task_NullifyMutex(p->mutex);

		DeleteArray((void *)p->jobs);
		DeleteArray((void *)p->phases);

		eeMemory((void *)p, 0);
	}

	return (ee_JobQueue_t)0;
}
/* Set a job queue to a specific scheduler */
void eeSetJobQueueScheduler(ee_JobQueue_t p, ee_Scheduler_t sched) {
	struct JobQueue_s **head, **tail;
	Mutex_t *M;

	EE_LOG_FN_ENTER();

	EE_ASSERT(p != (ee_JobQueue_t)0);

	UnlinkJobQueue(p);

	p->sched = sched;
	p->flags.exclusive = sched ? true : false;

	p->next = (struct JobQueue_s *)0;

	if (sched) {
		M = &sched->exclusive.mutex;
		head = &sched->exclusive.head;
		tail = &sched->exclusive.tail;
	} else {
		M = &g_sched_queueLiveMutex;
		head = &g_sched_queueLiveHead;
		tail = &g_sched_queueLiveTail;
	}

	Task_LockMutex(*M);
	if ((p->prev = *tail) != (struct JobQueue_s *)0)
		(*tail)->next = p;
	else
		*head = p;
	*tail = p;
	Task_UnlockMutex(*M);

	EE_LOG_FN_LEAVE();
}
/* Retrieve the scheduler of a job queue */
ee_Scheduler_t eeGetJobQueueScheduler(ee_JobQueue_t p) {
	EE_ASSERT(p != (ee_JobQueue_t)0);

	return p->flags.exclusive ? p->sched : (ee_Scheduler_t)0;
}

/* -------------------------------------------------------------------------- */

/* Add a job to a queue (may be scheduled immediately) */
void eeEnqueueJob(ee_JobQueue_t p, ee_JobFunc_t fn, void *arg) {
	EE_ASSERT(p != (ee_JobQueue_t)0);
	EE_ASSERT(fn != (ee_JobFunc_t)0);

	EE_ASSERT(p->jobs != (struct Job_s *)0);

	EE_ASSERT(p->phases != (size_t *)0);
	EE_ASSERT(p->numPhases > 0);

	Task_LockMutex(p->mutex);
	p->jobs = (struct Job_s *)ResizeArray((void *)p->jobs, p->numJobs+1);
	p->jobs[p->numJobs].fn = fn;
	p->jobs[p->numJobs].p = arg;
	p->phases[p->numPhases - 1] = p->numJobs++;
	Task_UnlockMutex(p->mutex);
}

/* Add a new phase to a queue */
ee_UInt_t eeEnqueuePhase(ee_JobQueue_t p) {
	ee_UInt_t phase;

	EE_ASSERT(p != (ee_JobQueue_t)0);

	EE_ASSERT(p->phases != (size_t *)0);
	EE_ASSERT(p->numPhases > 0);

	Task_LockMutex(p->mutex);
	phase = p->numPhases;

	p->phases = (size_t *)ResizeArray((void *)p->phases, p->numPhases+1);
	p->phases[p->numPhases++] = p->numJobs;
	Task_UnlockMutex(p->mutex);

	return phase;
}

/* -------------------------------------------------------------------------- */

/* Wait for a phase to be reached */
void eeWaitForPhase(ee_JobQueue_t p, ee_UInt_t phase) {
	EE_ASSERT(p != (ee_JobQueue_t)0);

	while(p->curPhase<=phase && p->curPhase<p->numPhases)
		Task_Relinquish();
}

/* Wait for an entire queue */
void eeWaitForQueue(ee_JobQueue_t p) {
	EE_ASSERT(p != (ee_JobQueue_t)0);

	eeWaitForPhase(p, 0xFFFFFFFF);
}

/* Wait for all job queues to complete */
void eeWaitForAllQueues() {
	/*
	 * TODO: Need to actually wait for _all_ queues to complete. Need a way to
	 *       efficiently handle this, as well as handling the next frame.
	 */
	EE_LOG("eeWaitForAllQueues(): Not yet implemented.");
}
