#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class CTest
{
public:
	CTest()
	: m_Item( 0 )
	{
		printf( "+CTest\n" );
	}
	~CTest()
	{
		printf( "-CTest\n" );
	}

	void Set( int x )
	{
		m_Item = x;
	}
	int Get() const
	{
		return m_Item;
	}

private:
	int								m_Item;
};

typedef void *( *FnAllocObject_t )( void );
typedef void( *FnDeallocObject_t )( void * );

#define FREE_OBJECT_PTR				((void*)0)
#define RESERVED_OBJECT_PTR			((void*)1)

typedef unsigned int				RTIndex_t;
#define MAX_INDEX					((RTIndex_t)0x003FFFFF)

typedef struct RTObjectPool_s
{
	FnAllocObject_t					pfnAlloc;
	FnDeallocObject_t				pfnDealloc;

	void **							ppObjects;
	RTIndex_t						cCapacity;
} RTObjectPool_t;

void RT_InitPool( RTObjectPool_t *pPool, FnAllocObject_t pfnAlloc, FnDeallocObject_t pfnDealloc )
{
	pPool->pfnAlloc		= pfnAlloc;
	pPool->pfnDealloc	= pfnDealloc;
	pPool->ppObjects	= ( void ** )0;
	pPool->cCapacity	= 0;
}
void RT_FiniPool( RTObjectPool_t *pPool )
{
	pPool->pfnAlloc = NULL;

	while( pPool->cCapacity > 0 ) {
		void **ppObj;

		ppObj = &pPool->ppObjects[ --pPool->cCapacity ];
		if( !*ppObj ) {
			continue;
		}

		pPool->pfnDealloc( *ppObj );
		*ppObj = NULL;
	}

	free( ( void * )pPool->ppObjects );
	pPool->ppObjects = ( void ** )0;
}

void RT_AllocateIndex( RTObjectPool_t *pPool, RTIndex_t uIndex )
{
	static const size_t kGrain = 4096/sizeof( void * );
	size_t cCapacity;
	size_t n;
	void *p;

	if( uIndex > MAX_INDEX ) {
		fprintf( stderr, "ERROR: uIndex (0x%.8X) > MAX_INDEX (0x%.8X)\n",
			uIndex, MAX_INDEX );
		exit( EXIT_FAILURE );
	}

	if( uIndex < pPool->cCapacity ) {
		return;
	}

	cCapacity = uIndex - uIndex%kGrain + kGrain;
	p = realloc( ( void * )pPool->ppObjects, sizeof( void * )*cCapacity );
	if( !p ) {
		fprintf( stderr, "ERROR: Out of memory\n" );
		exit( EXIT_FAILURE );
	}

	n = cCapacity - pPool->cCapacity;
	memset( (void *)( ((void**)p) + pPool->cCapacity ), 0, n*sizeof(void*) );

	pPool->ppObjects = ( void ** )p;
	pPool->cCapacity = n;
}

void RT_ReserveIndexes( RTObjectPool_t *pPool, RTIndex_t uBeginIndex, RTIndex_t uEndIndex )
{
	RTIndex_t i;

	RT_AllocateIndex( pPool, uEndIndex );
	for( i = uBeginIndex; i < uEndIndex; ++i ) {
		if( pPool->ppObjects[ i ] != FREE_OBJECT_PTR ) {
			continue;
		}
		
		pPool->ppObjects[ i ] = RESERVED_OBJECT_PTR;
	}
}

RTIndex_t RT_FindIndex( RTObjectPool_t *pPool )
{
	size_t i, j;

	for( j = pPool->cCapacity; j > 0; --j ) {
		i = j - 1;

		if( pPool->ppObjects[ i ] == FREE_OBJECT_PTR ) {
			return i;
		}
	}

	i = pPool->cCapacity;
	RT_AllocateIndex( pPool, i );
	return i;
}

int RT_Exists( const RTObjectPool_t *pPool, RTIndex_t uIndex )
{
	if( uIndex >= pPool->cCapacity ) {
		return 0;
	}

	if( pPool->ppObjects[ uIndex ] == FREE_OBJECT_PTR ) {
		return 0;
	}

	if( pPool->ppObjects[ uIndex ] == RESERVED_OBJECT_PTR ) {
		return 0;
	}

	return 1;
}

void *RT_Alloc( RTObjectPool_t *pPool, RTIndex_t uIndex )
{
	if( RT_Exists( pPool, uIndex ) ) {
		fprintf( stderr, "ERROR: Index (%u) already in use!\n", uIndex );
		exit( EXIT_FAILURE );
	}

	RT_AllocateIndex( pPool, uIndex );

	pPool->ppObjects[ uIndex ] = pPool->pfnAlloc();
	if( !pPool->ppObjects[ uIndex ] ) {
		fprintf( stderr, "ERROR: Out of memory\n" );
		exit( EXIT_FAILURE );
	}

	return pPool->ppObjects[ uIndex ];
}
void RT_Free( RTObjectPool_t *pPool, RTIndex_t uIndex )
{
	if( !RT_Exists( pPool, uIndex ) ) {
		fprintf( stderr, "ERROR: Object does not exist (%u)\n", uIndex );
		exit( EXIT_FAILURE );
	}

	pPool->pfnDealloc( pPool->ppObjects[ uIndex ] );
	pPool->ppObjects[ uIndex ] = FREE_OBJECT_PTR;
}

void *RT_Get( RTObjectPool_t *pPool, RTIndex_t uIndex )
{
	if( !RT_Exists( pPool, uIndex ) ) {
		fprintf( stderr, "ERROR: Object does not exist (%u)\n", uIndex );
		exit( EXIT_FAILURE );
	}

	return pPool->ppObjects[ uIndex ];
}

template< typename tObject >
class THandler
{
public:
	THandler()
	{
		RT_InitPool( &m_Pool, &Alloc_f, &Dealloc_f );
	}
	~THandler()
	{
		RT_FiniPool( &m_Pool );
	}

	bool Exists( RTIndex_t uIndex ) const
	{
		return !!RT_Exists( &m_Pool, uIndex );
	}

	RTIndex_t Make()
	{
		RTIndex_t uIndex = RT_FindIndex( &m_Pool );
		RT_Alloc( &m_Pool, uIndex );
		return uIndex;
	}
	RTIndex_t Make( RTIndex_t uIndex )
	{
		RT_Alloc( &m_Pool, uIndex );
		return uIndex;
	}

	void Free( RTIndex_t uIndex )
	{
		RT_Free( &m_Pool, uIndex );
	}

	tObject &Get( RTIndex_t uIndex )
	{
		return *( tObject * )RT_Get( &m_Pool, uIndex );
	}

private:
	RTObjectPool_t					m_Pool;

	static void *Alloc_f()
	{
		return ( void * )( new tObject() );
	}
	static void Dealloc_f( void *p )
	{
		delete ( tObject * )p;
	}
};

static THandler<CTest> Tests;

int MakeTest( void )
{
	return Tests.Make();
}
void MakeTest( int iID )
{
	if( iID <= 0 ) { return; }

	Tests.Make( iID );
}
void DeleteTest( int iID )
{
	Tests.Free( iID );
}
bool TestExists( int iID )
{
	return Tests.Exists( iID );
}

void SetTestValue( int iID, int iValue )
{
	Tests.Get( iID ).Set( iValue );
}
int GetTestValue( int iID )
{
	return Tests.Get( iID ).Get();
}

int main()
{
	MakeTest( 1 );
	SetTestValue( 1, 100 );
	printf( "Test value: %i\n", GetTestValue( 1 ) );
	printf( "Test 1 exists? %s\n", TestExists( 1 ) ? "true" : "false" );
	DeleteTest( 1 );
	printf( "Test 1 exists? %s\n", TestExists( 1 ) ? "true" : "false" );
	int iTestID = MakeTest();
	printf( "Made test: %i\n", iTestID );
	SetTestValue( iTestID, 200 );
	printf( "Test %i value: %i\n", iTestID, GetTestValue( iTestID ) );
	printf( "Test %i exists? %s\n", iTestID, TestExists( iTestID ) ? "true" : "false" );
	DeleteTest( iTestID );
	printf( "Test %i exists? %s\n", iTestID, TestExists( iTestID ) ? "true" : "false" );
	iTestID = 0;
	MakeTest();
	MakeTest();
	MakeTest();

	return EXIT_SUCCESS;
}
