#include <ember/math.h>
#include <math.h>

float vec4_x( const Vec4 *src )
{
	return src->x;
}
float vec4_y( const Vec4 *src )
{
	return src->y;
}
float vec4_z( const Vec4 *src )
{
	return src->z;
}
float vec4_w( const Vec4 *src )
{
	return src->w;
}

void vec4_zero( Vec4 *dst )
{
	dst->x = dst->y = dst->z = dst->w = 0;
}
void vec4_identity( Vec4 *dst )
{
	dst->x = dst->y = dst->z = dst->w = 1;
}

void vec4_set( Vec4 *dst, float x, float y, float z, float w )
{
	dst->x = x;
	dst->y = y;
	dst->z = z;
	dst->w = w;
}

void vec4_copy( Vec4 *dst, const Vec4 *src )
{
	dst->x = src->x;
	dst->y = src->y;
	dst->z = src->z;
	dst->w = src->w;
}

void vec4_add( Vec4 *dst, const Vec4 *a, const Vec4 *b )
{
	dst->x = a->x + b->x;
	dst->y = a->y + b->y;
	dst->z = a->z + b->z;
	dst->w = a->w + b->w;
}
void vec4_subtract( Vec4 *dst, const Vec4 *a, const Vec4 *b )
{
	dst->x = a->x - b->x;
	dst->y = a->y - b->y;
	dst->z = a->z - b->z;
	dst->w = a->w - b->w;
}
void vec4_multiply( Vec4 *dst, const Vec4 *a, const Vec4 *b )
{
	dst->x = a->x * b->x;
	dst->y = a->y * b->y;
	dst->z = a->z * b->z;
	dst->w = a->w * b->w;
}
void vec4_divide( Vec4 *dst, const Vec4 *a, const Vec4 *b )
{
	dst->x = a->x / b->x;
	dst->y = a->y / b->y;
	dst->z = a->z / b->z;
	dst->w = a->w / b->w;
}
void vec4_scale( Vec4 *dst, const Vec4 *a, float b )
{
	dst->x = a->x * b;
	dst->y = a->y * b;
	dst->z = a->z * b;
	dst->w = a->w * b;
}

float vec4_dot( const Vec4 *a, const Vec4 *b )
{
	return a->x * b->x + a->y * b->y + a->z * b->z + a->w * b->w;
}
void vec4_cross( Vec4 *dst, const Vec4 *a, const Vec4 *b, const Vec4 *c )
{
	float x1, x2, x3, y1, y2, y3, z1, z2, z3, w1, w2, w3;

	/* convenience */
	x1 = a->x;
	x2 = b->x;
	x3 = c->x;
	y1 = a->y;
	y2 = b->y;
	y3 = c->y;
	z1 = a->z;
	z2 = b->z;
	z3 = c->z;
	w1 = a->w;
	w2 = b->w;
	w3 = c->w;

	dst->x = y1 * ( z2 * w3 - z3 * w2 ) - z1 * ( y2 * w3 - y3 * w2 ) +
	         w1 * ( y2 * z3 - z2 * y3 );
	dst->y = -( x1 * ( z2 * w3 - z3 * w2 ) - z1 * ( x2 * w3 - x3 * w2 ) +
	            w1 * ( x2 * z3 - x3 * z2 ) );
	dst->z = x1 * ( y2 * w3 - y3 * w2 ) - y1 * ( x2 * w3 - x3 * w2 ) +
	         w1 * ( x2 * y3 - x3 * y2 );
	dst->w = -( x1 * ( y2 * z3 - y3 * z2 ) - y1 * ( x2 * z3 - x3 * z2 ) +
	            z1 * ( x2 * y3 - x3 * y2 ) );
}

float vec4_length( const Vec4 *a )
{
	return sqrtf( a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w );
}
float vec4_length_fast( const Vec4 *a )
{
	return sqrt_fast( a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w );
}

void vec4_normalize( Vec4 *a )
{
	float r;

	r = 1 / sqrtf( a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w );

	a->x *= r;
	a->y *= r;
	a->z *= r;
	a->w *= r;
}
void vec4_normalize_fast( Vec4 *a )
{
	float r;

	r = invsqrt_fast( a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w );

	a->x *= r;
	a->y *= r;
	a->z *= r;
	a->w *= r;
}

float vec4_distance( const Vec4 *a, const Vec4 *b )
{
	float x, y, z, w;

	x = a->x - b->x;
	y = a->y - b->y;
	z = a->z - b->z;
	w = a->w - b->w;

	return sqrtf( x * x + y * y + z * z + w * w );
}
float vec4_distance_fast( const Vec4 *a, const Vec4 *b )
{
	float x, y, z, w;

	x = a->x - b->x;
	y = a->y - b->y;
	z = a->z - b->z;
	w = a->w - b->w;

	return sqrt_fast( x * x + y * y + z * z + w * w );
}

void vec4_transform_ex( Vec4 *dst, const Vec4 *a, const Matrix *b )
{
	dst->x =
	    a->x * b->rm._11 + a->y * b->rm._21 + a->z * b->rm._31 + a->w * b->rm._41;
	dst->y =
	    a->x * b->rm._12 + a->y * b->rm._22 + a->z * b->rm._32 + a->w * b->rm._42;
	dst->z =
	    a->x * b->rm._13 + a->y * b->rm._23 + a->z * b->rm._33 + a->w * b->rm._43;
	dst->w =
	    a->x * b->rm._14 + a->y * b->rm._24 + a->z * b->rm._34 + a->w * b->rm._44;
}
void vec4_transform( Vec4 *dst, float x, float y, float z, float w,
    const Matrix *b )
{
	Vec4 v;

	v.x = x;
	v.y = y;
	v.z = z;
	v.w = w;
	vec4_transform_ex( dst, &v, b );
}
