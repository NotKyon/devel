/*

	BUSTERS!
	========

	Battle system from Visual Arts / Key's Little Busters!



	HOW IT WORKS
	------------
	
	- Players are given titles and ranks

	- Aside from the initial title assignment, players are given titles under
	  two conditions:

		(1) A player loses in a battle. The victor will bestow a title of his
		 -  or her choosing to the loser.

		(2) A player reaches the top rank. Their title will then be of their own
		 -  choosing.

	- Players can only challenge players of a higher rank.

		- The challenger's rank must not be more than n-ranks lower than the
		  player being challenged. (n is SystemRules::myMinimumRankDistance.)

		- Any player can trade with any other player regardless of rank.

	- During a battle, players are given a weapon randomly.
	
		- The player's can only fight with the weapons they are given.

		- The effectiveness of the weapon in a player's hands increases the more
		  the player uses that weapon. This persists across battles.

	- The battle ends either when one player gives up, their HP reaches zero, or
	  the "time limit" runs out.

		- If a player gives up or their own HP reaches zero then the lose the
		  match. Likewise, the other player wins.

		- If the time limit reaches zero, the match is a draw. (Regardless of
		  where either player's HP is.)

	- Accessories will be applied at the beginning of a battle if they are
	  equipped (and applicable).

		- Some accessories will only be applied under certain conditions. (For
		  example, if the player's health is too low.)

		- Some accessories are "consumable," meaning they are limited to one use
		  before being disposed of. If these accessories are "unconditional"
		  then they will without exception be "consumed" at the beginning of a
		  battle.
	

*/

#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef int8_t						int8;
typedef int16_t						int16;
typedef int32_t						int32;
typedef int64_t						int64;
typedef uint8_t						uint8;
typedef uint16_t					uint16;
typedef uint32_t					uint32;
typedef uint64_t					uint64;
typedef ptrdiff_t					intptr;
typedef size_t						uintptr;

typedef float						float32;
typedef double						float64;

/* Battle statistics */
typedef struct BattleStats
{
	/* How powerful attacks are against your opponent */
	int32							myArmStrength;
	/* How powerful attacks are against you */
	int32							myBodyStrength;
	/* How capable attacks are against your opponent */
	int32							myAgility;
	/* How capable attacks are against you */
	int32							myReflex;
	/* How useful weapons are in your hands */
	int32							myConcentration;
	/* How useful weapons are in your opponent's hands */
	int32							myJudgement;
	/* Your level of luck (for dealing a critical hit or missing a consecutive hit, etc) */
	int32							myLuck;
} BattleStats;

/* Life capacity */
typedef struct LifeForce
{
	uint32							myHealth;
} LifeForce;

/* Accessory is consumed upon usage */
#define ACCESSORYF_CONSUME			0x01
/* Accessory is a key item and cannot be disposed */
#define ACCESSORYF_KEYITEM			0x02
/* Accessory is an upgrade which permanently increases the player's stats each time it's used */
#define ACCESSORYF_UPGRADE			0x04
/* Accessory is only usable *against* the final boss */
#define ACCESSORYF_FINALBOSS		0x08

/* Accessory equipped to player */
typedef struct Accessory
{
	/* Name of the accessory */
	char							myName				[ 32 ];
	/* Behavior flags (ACCESSORYF_) */
	uint32							myFlags;
	/* Modifier to the player's stats */
	BattleStats						myStatModifier;
	/* Modifier to the player's life */
	LifeForce						myLifeModifier;
	/* HP level below which the item will kick in; if 0 the item kicks in at battle start */
	uint32							myHPThreshold;
} Accessory;

/* Land trap weapon: Weapon is left on ground for opposing player to slip on */
#define WEAPONF_LAND_TRAP			0x01
/* Clip trap weapon: Weapon is clipped to opposing player causing damage on each turn */
#define WEAPONF_CLIP_TRAP			0x02

/* Weapon for players to use in battles */
typedef struct Weapon
{
	/* Name of the weapon */
	char							myName				[ 32 ];
	/* Behavior flags (WEAPONF_) */
	uint32							myFlags;
	/* Number of consecutive strikes the weapon can make (by default) */
	uint32							myConsecutiveStrikes;
	/* Number of consecutive strikes the weapon can make at most */
	uint32							myMaxConsecutiveStrikes;
	/* Modifier to the player's stats */
	BattleStats						myStatModifier;
	/* Minimum luck needed to immobilize the opponent; if 0 then immobilization is disabled */
	uint32							myImmobilizationLuck;
	/* Minimum luck needed to use weapon properly */
	uint32							myRegularLuck;
	/* Minimum luck needed to get a critical hit */
	uint32							myCriticalLuck;
} Weapon;

/* General player information */
typedef struct Player
{
	/* Name of the player */
	char							myName				[ 32 ];
	/* Title of the player */
	char							myTitle				[ 64 ];
	/* Player's stats */
	BattleStats						myStats;
	/* Player's life force */
	LifeForce						myLife;

	/* Accessories the player has */
	Accessory *						myAccessories		[ 3 ];
} Player;

/* BattlePlayer is immobile and cannot move! */
#define BPLAYERF_IMMOBILE			0x01
/* BattlePlayer is frightened and cannot make consecutive attacks! */
#define BPLAYERF_SCARED				0x02
/* BattlePlayer's weapon is weakened! */
#define BPLAYERF_BADWEAPON			0x04
/* BattlePlayer is the final boss! */
#define BPLAYERF_FINALBOSS			0x08

/* Player during a battle */
typedef struct BattlePlayer
{
	/* Player this battler is based on */
	Player *						myPlayer;
	/* Weapon the player got */
	Weapon *						myWeapon;
	/* Current flags for the player (BPLAYERF_) */
	uint32							myFlags;
	/* Current stats */
	BattleStats						myStats;
	/* Current life force */
	LifeForce						myLife;
} BattlePlayer;

/* Types of events that can occur in a battle */
typedef enum EBattleEvent
{
	/* General message */
	kBattleEvent_Message,

	/* [Attacker,defender] Attacker wins. Defender loses. */
	kBattleEvent_Fell,
	/* [Attacker,defender] Attacker wins. Defender gave up. */
	kBattleEvent_GaveUp,
	/* It was a draw! */
	kBattleEvent_Draw,
	/* [Attacker,defender] Attacker attacked Defender. */
	kBattleEvent_Attacked,
	/* [Attacker,defender] Attacker landed a critical hit on Defender. */
	kBattleEvent_CriticalHit,
	/* [Attacker,defender] Attacker's attack was parried by Defender. */
	kBattleEvent_Parried,
	/* [Attacker] Attacker slipped on a Ground Trap. (Missing their turn to attack.) */
	kBattleEvent_Slipped,
	/* [Attacker,defender] Attacker scared Defender. Defender is now stricken with fear. */
	kBattleEvent_StrickenWithFear,
	/* [Attacker,defender] Attacker immobilized Defender. Defender loses a turn. */
	kBattleEvent_Immobilized,
	/* [Attacker,defender] Attacker's weapon broke while attacking Defender. */
	kBattleEvent_BrokeWeapon,
	/* [Attacker] Attacker uses item. */
	kBattleEvent_UseItem,

	kNumBattleEvents
} EBattleEvent;

/* Some event within a battle */
typedef struct BattleAction
{
	/* Formatted description of the action */
	char							myMessage			[ 256 ];
	/* The event */
	EBattleEvent					myEvent;
	/* The player that instigated the action */
	const BattlePlayer *			myAttacker;
	/* The player that reacted to the action (can be NULL) */
	const BattlePlayer *			myDefender;
	/* Delta to apply to the attacker's stats */
	BattleStats						myAttackerStatDelta;
	/* Delta to apply to the defender's stats */
	BattleStats						myDefenderStatDelta;
	/* Delta to apply to the attacker's life */
	LifeForce						myAttackerLifeDelta;
	/* Delta to apply to the defender's life */
	LifeForce						myDefenderLifeDelta;
} BattleAction;

#define MAX_BATTLE_ACTIONS			512

/* On-going battle */
typedef struct Battle
{
	/* Battle players */
	BattlePlayer					myPlayer			[ 2 ];
	/* Number of cycles remaining (one cycle = one turn from each player--two turns) */
	uint32							myRemainingCycles;
	/* Current number of actions */
	uint32							myActionCount;
	/* Battle actions */
	BattleAction					myActions			[ MAX_BATTLE_ACTIONS ];
} Battle;

/* Trading with other players are disabled */
#define RULEF_NOTRADES				0x01
/* Any rank can battle any other rank */
#define RULEF_NORANKRESTRICTION		0x02
/* Final boss is disabled and will not appear */
#define RULEF_NOFINALBOSS			0x04

/* Rules applied for all battles */
typedef struct SystemRules
{
	/* General flags (RULEF_) */
	uint32							myFlags;
	/* Minimum rank distance for fights to be allowed */
	uint32							myMinimumRankDistance;
	/* Default number of cycles for a given battle */
	uint32							myDefaultBattleCycles;
	/* Default minimum luck needed to land a critical hit (for weapon-less battles) */
	uint32							myMinimumCriticalLuck;
} SystemRules;
extern SystemRules					theRules;




/*
===============================================================================

	COMMON FUNCTIONS

===============================================================================
*/

char *StrCpy( char *pDst, uintptr cDstMax, const char *pSrc )
{
	uintptr i;

	for( i = 0; pSrc[ i ] != '\0'; ++i ) {
		if( i + 1 >= cDstMax ) {
			break;
		}

		pDst[ i ] = pSrc[ i ];
	}

	pDst[ i ] = '\0';
	return pDst;
}

uint32 Rand( uint32 uValue )
{
	return rand() % uValue;
}




/*
===============================================================================

	FINAL BOSS

===============================================================================
*/

/* Create the accessories only available when the final boss appears */
Accessory *FinalBoss_CreateSitarAccessory( Accessory *pDst )
{
	StrCpy( pDst->myName, sizeof( pDst->myName ), "Ancient Sitar" );
	pDst->myFlags = 0;
	pDst->myStatModifier.myArmStrength = 20;
	pDst->myStatModifier.myBodyStrength = 20;
	pDst->myStatModifier.myAgility = 20;
	pDst->myStatModifier.myReflex = 20;
	pDst->myStatModifier.myConcentration = 20;
	pDst->myStatModifier.myJudgement = 20;
	pDst->myStatModifier.myLuck = 20;
	pDst->myLifeModifier.myHealth = 0;
	pDst->myHPThreshold = 0;
	
	return pDst;
}
Accessory *FinalBoss_CreateAncientMaskAccessory( Accessory *pDst )
{
	StrCpy( pDst->myName, sizeof( pDst->myName ), "Ancient Mask" );
	pDst->myFlags = ACCESSORYF_KEYITEM;
	pDst->myStatModifier.myArmStrength = 50;
	pDst->myStatModifier.myBodyStrength = 50;
	pDst->myStatModifier.myAgility = 50;
	pDst->myStatModifier.myReflex = 50;
	pDst->myStatModifier.myConcentration = 50;
	pDst->myStatModifier.myJudgement = 50;
	pDst->myStatModifier.myLuck = 50;
	pDst->myLifeModifier.myHealth = 0;
	pDst->myHPThreshold = 0;
	
	return pDst;
}
Accessory *FinalBoss_CreateAncientRelicAccessory( Accessory *pDst )
{
	StrCpy( pDst->myName, sizeof( pDst->myName ), "Ancient Relic" );
	pDst->myFlags = ACCESSORYF_FINALBOSS;
	pDst->myStatModifier.myArmStrength = -50;
	pDst->myStatModifier.myBodyStrength = -50;
	pDst->myStatModifier.myAgility = -50;
	pDst->myStatModifier.myReflex = -50;
	pDst->myStatModifier.myConcentration = -50;
	pDst->myStatModifier.myJudgement = -50;
	pDst->myStatModifier.myLuck = -50;
	pDst->myLifeModifier.myHealth = 0;
	pDst->myHPThreshold = 0;
	
	return pDst;
}

Player *FinalBoss_CreatePlayer( Player *pDst )
{
	StrCpy( pDst->myName, sizeof( pDst->myName ), "Ancient FEAR" );
	StrCpy( pDst->myTitle, sizeof( pDst->myTitle ), "Mysterious Wandering Soul" );
	pDst->myStats.myArmStrength = 280;
	pDst->myStats.myBodyStrength = 240;
	pDst->myStats.myAgility = 260;
	pDst->myStats.myReflex = 220;
	pDst->myStats.myConcentration = 235;
	pDst->myStats.myJudgement = 250;
	pDst->myStats.myLuck = 130;
	pDst->myStats.myLife.myHealth = 220;

	return pDst;
}




/*
===============================================================================

	BATTLE SYSTEM

===============================================================================
*/

/* Prepare a battle player */
BattlePlayer *Battle_PreparePlayer( BattlePlayer *pDst, Player *pSrc )
{
	pDst->myPlayer = pSrc;
	pDst->myWeapon = NULL;
	pDst->myFlags = 0;
	pDst->myStats = pSrc->myStats;
	pDst->myLife = pSrc->myLife;

	return pDst;
}

/* Prepare a battle */
Battle *Battle_Prepare( Battle *pDst, Player *pLPlayer, Player *pRPlayer )
{
	Battle_PreparePlayer( &pDst->myPlayer[ 0 ], pLPlayer );
	Battle_PreparePlayer( &pDst->myPlayer[ 1 ], pRPlayer );

	pDst->myRemainingCycles = theRules.myDefaultBattleCycles;

	return pDst;
}
