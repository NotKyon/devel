// g++ -std=gnu++11 -Wno-invalid-offsetof -o intrusivelist intrusivelist.cpp

// const_this
namespace detail
{

	template< typename T >
	inline const T *get_const( T *x )
	{
		return x;
	}

}

#define const_this detail::get_const( this )

// safe_delete
template< typename T >
void safe_delete( T *&x )
{
	delete x;
	x = nullptr;
}

//
//	This is an example implementation of EASTL's intrusive_list.
//	See: http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2271.html
//

#include <stddef.h>
#include <stdint.h>

struct bidirectional_iterator_tag {};

struct intrusive_list_node
{
	intrusive_list_node *prev;
	intrusive_list_node *next;

	inline intrusive_list_node(): prev( this ), next( this )
	{
	}
};

template< typename T, typename Pointer, typename Reference >
class intrusive_list_iterator
{
public:
	typedef intrusive_list_iterator< T, Pointer, Reference > this_type;
	typedef intrusive_list_iterator< T, T *, T & > iterator;
	typedef intrusive_list_iterator< T, const T *, const T & > const_iterator;
	typedef T value_type;
	typedef T node_type;
	typedef ptrdiff_t difference_type;
	typedef Pointer pointer;
	typedef Reference reference;
	typedef bidirectional_iterator_tag iterator_category;

public:
	intrusive_list_iterator(): mNode( nullptr )
	{
	}
	explicit intrusive_list_iterator( pointer x ): mNode( x )
	{
	}
	intrusive_list_iterator( const iterator &x ): mNode( x.mNode )
	{
	}

	pointer get_pointer() const
	{
		return mNode;
	}

	reference operator*() const
	{
		return *mNode;
	}
	pointer operator->() const
	{
		return mNode;
	}

	intrusive_list_iterator &operator++()
	{
		mNode = mNode->next;
		return *this;
	}
	intrusive_list_iterator &operator--()
	{
		mNode = mNode->prev;
		return *this;
	}
	intrusive_list_iterator operator++( int ) const
	{
		return intrusive_list_iterator( mNode->next );
	}
	intrusive_list_iterator operator--( int ) const
	{
		return intrusive_list_iterator( mNode->prev );
	}

	bool operator==( const intrusive_list_iterator &x )
	{
		return mNode == x.mNode;
	}
	bool operator!=( const intrusive_list_iterator &x )
	{
		return mNode != x.mNode;
	}

private:
	pointer mNode;
};

template< typename T = intrusive_list_node >
class intrusive_list
{
public:
	typedef intrusive_list< T > this_type;
	typedef T node_type;
	typedef T value_type;
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	typedef T &reference;
	typedef const T &const_reference;
	typedef T *pointer;
	typedef const T *const_pointer;
	typedef intrusive_list_iterator< T, T *, T & > iterator;
	typedef intrusive_list_iterator< T, const T *, const T & > const_iterator;
	//typedef sdf::reverse_iterator< iterator > reverse_iterator;
	//typedef sdf::reverse_iterator< const_iterator > const_reverse_iterator;

public:
	intrusive_list(): mBase()
	{
	}
	intrusive_list( const this_type &x ): mBase()
	{
		*this = x;
	}

	this_type &operator=( const this_type &x )
	{
		clear();
		for( auto &item : x )
		{
			push_back( item );
		}

		return *this;
	}

	iterator begin()
	{
		return iterator( mBase.next );
	}
	const_iterator begin() const
	{
		return const_iterator( mBase.next );
	}
	iterator end()
	{
		return iterator( &mBase );
	}
	const_iterator end() const
	{
		return const_iterator( &mBase );
	}

	/*
	reverse_iterator rbegin()
	{
	}
	const_reverse_iterator rbegin() const
	{
	}
	reverse_iterator rend()
	{
	}
	const_reverse_iterator rend() const
	{
	}
	*/

	reference front()
	{
		return *mBase.next;
	}
	const_reference front() const
	{
		return *mBase.next;
	}
	reference back()
	{
		return *mBase.prev;
	}
	const_reference back() const
	{
		return *mBase.prev;
	}

	bool empty() const
	{
		return mBase.next != &mBase;
	}
	size_type size() const
	{
		size_type count = 0;

		for( auto *p = mBase.next; p != &mBase; p = p->next )
		{
			++count;
		}

		return count;
	}
	void clear()
	{
		while( mBase.next != &mBase )
		{
			remove( *mBase.next );
		}
	}
	void reverse()
	{
		// TODO!
	}

	void push_front( T &x )
	{
		insert( begin(), x );
	}
	void pop_front()
	{
		remove( mBase.next );
	}
	void push_back( T &x )
	{
		insert( end(), x );
	}
	void pop_back()
	{
		remove( mBase.prev );
	}

	iterator locate( T &x )
	{
		return const_cast< iterator >( const_this->locate( x ) );
	}
	const_iterator locate( const T &x ) const
	{
		for( const auto *p = mBase.next; p != &mBase; p = p->next )
		{
			if( *p == x )
			{
				return const_iterator( p );
			}
		}

		return end();
	}

	iterator insert( iterator pos, T &x )
	{
		x.prev = pos->prev;
		x.next = pos.get_pointer();

		pos->prev->next = &x;
		pos->prev = &x;

		return iterator( &x );
	}
	iterator erase( iterator pos )
	{
		// TODO
		return end();
	}
	iterator erase( iterator pos, iterator last )
	{
		// TODO
		return end();
	}
	void swap( intrusive_list &ls )
	{
		// TODO
	}

	// remove from list without having the list
	static void remove( T &x )
	{
		x.prev->next = x.next;
		x.next->prev = x.prev;

		x.prev = &x;
		x.next = &x;
	}

	void splice( iterator pos, T &x )
	{
		// TODO!
	}
	void splice( iterator pos, intrusive_list &x )
	{
		// TODO!
	}
	void splice( iterator pos, intrusive_list &x, iterator i )
	{
		// TODO!
	}
	void splice( iterator pos, intrusive_list &x, iterator first, iterator last )
	{
		// TODO!
	}

	void merge( this_type &x )
	{
		// TODO!
	}
	template< typename Compare >
	void merge( this_type &x, Compare cmp )
	{
		// TODO!
	}

	void unique()
	{
		// TODO!
	}
	template< typename BinaryPredicate >
	void unique( BinaryPredicate bp )
	{
		// TODO!
	}

	void sort()
	{
		// TODO!
	}
	template< typename Compare >
	void sort( Compare cmp )
	{
		// TODO!
	}

	bool validate() const
	{
		// TODO!
		return true;
	}
	int validate_iterator( const_iterator i ) const
	{
		// TODO!
		return 0;
	}

protected:
	node_type mBase;
};

#define intrusive_list_value_from_node( ValueType, NodePtr, FieldName )\
	( ( ValueType * )( ( size_t )( NodePtr ) -\
		offsetof( ValueType, FieldName ) ) )

//----------------------------------------------------------------------------//

#include <stdio.h>
#include <stdlib.h>

struct Entity
{
	int mValue;
	intrusive_list<>::node_type mNode;

	static intrusive_list<> gList;

	Entity( int x = 0 ): mValue( x ), mNode()
	{
		gList.push_back( mNode );
	}
	~Entity()
	{
		intrusive_list<>::remove( mNode );
	}

	void SetValue( int x )
	{
		mValue = x;
	}
	int GetValue() const
	{
		return mValue;
	}
};
intrusive_list<> Entity::gList;

#define EntityFromNode( nodePtr )\
	intrusive_list_value_from_node( Entity, nodePtr, mNode )

void PrintEnts()
{
	printf( "Entities {" );
	for( auto &entNode : Entity::gList )
	{
		Entity *ent = EntityFromNode( &entNode );
		printf( "\n  entNode = %p\n  ent     = %p\n  value   = %i\n",
			( void * )&entNode, ( void * )ent, ent->GetValue() );
	}
	printf( "}\n" );
}

int main()
{
	// Create some test entities
	Entity *a = new Entity( 7 );
	Entity *b = new Entity( 8 );
	Entity *c = new Entity( 9 );

	PrintEnts();

	// 7 ate 9
	a->SetValue( a->GetValue() + c->GetValue() );

	safe_delete( c );

	PrintEnts();

	// Okay, done with all of 'em
	safe_delete( a );
	safe_delete( b );

	PrintEnts();

	return EXIT_SUCCESS;
}
