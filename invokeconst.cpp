#include <stdio.h>
#include <stdlib.h>

template< typename T >
inline const T *get_const( T *x )
{
	return x;
}

class a
{
public:
	a(): x( 0 )
	{
	}

	int *b()
	{
		return const_cast< int * >( get_const( this )->b() );
	}
	const int *b() const
	{
		return &x;
	}

protected:
	int x;
};

int main()
{
	a x;

	*x.b() = 7;
	printf( "%i\n", *get_const( &x )->b() );

	return EXIT_SUCCESS;
}
