CONFIG ?= DEBUG

DEBUG_FLAGS := -g -DDEBUG -D_DEBUG -D__debug__
RELEASE_FLAGS := -O3 -fno-strict-aliasing -march=core2 -mtune=core2 -DNDEBUG

COMMON_WARNINGS := -W -Wall -pedantic -Wfatal-errors
CXX_STANDARD := -std=gnu++11 -msse2
CXXFLAGS := $(COMMON_WARNINGS) $(CXX_STANDARD) $($(CONFIG)_FLAGS) -static

SET_CXX := g++
SET_CXXFLAGS := $(CXXFLAGS)
SET_TESTS := VP VI IL SL CV SOA

.PHONY: clean STLEntityTest

clean:
	-@rm -f $(wildcard STLEntityTest.log $(foreach A,$(foreach B,$(SET_TESTS),STLEntityTest$(B)$(CONFIG)),$(A) $(A).exe)) 2>/dev/null

STLEntityTest.log: STLEntityTest
	@echo STL Entity Test -- $(CONFIG) >$@
	@echo. >>$@
	@echo VECTOR POINTER >>$@
	@./STLEntityTestVP$(CONFIG) >>$@
	@echo. >>$@
	@echo VECTOR ITERATOR >>$@
	@./STLEntityTestVI$(CONFIG) >>$@
	@echo. >>$@
	@echo INTRUSIVE LIST >>$@
	@./STLEntityTestIL$(CONFIG) >>$@
	@echo. >>$@
	@echo STANDARD LIST >>$@
	@./STLEntityTestSL$(CONFIG) >>$@
	@echo. >>$@
	@echo CUSTOM VECTOR >>$@
	@./STLEntityTestCV$(CONFIG) >>$@
	@echo. >>$@
	@echo SOA >>$@
	@./STLEntityTestSOA$(CONFIG) >>$@
	@echo. >>$@

STLEntityTest: $(foreach X,$(SET_TESTS),STLEntityTest$(X)$(CONFIG))

STLEntityTestVP$(CONFIG): STLEntityTest.cpp
	@echo STLEntityTestVP...
	@$(SET_CXX) $(SET_CXXFLAGS) -DENTITY_STRUCTURE=ENTSTRUC_VECTOR_POINTER -o $@ $<

STLEntityTestVI$(CONFIG): STLEntityTest.cpp
	@echo STLEntityTestVI...
	@$(SET_CXX) $(SET_CXXFLAGS) -DENTITY_STRUCTURE=ENTSTRUC_VECTOR_ITERATOR -o $@ $<

STLEntityTestIL$(CONFIG): STLEntityTest.cpp
	@echo STLEntityTestIL...
	@$(SET_CXX) $(SET_CXXFLAGS) -DENTITY_STRUCTURE=ENTSTRUC_INTRUSIVE_LIST -o $@ $<

STLEntityTestSL$(CONFIG): STLEntityTest.cpp
	@echo STLEntityTestSL...
	@$(SET_CXX) $(SET_CXXFLAGS) -DENTITY_STRUCTURE=ENTSTRUC_STANDARD_LIST -o $@ $<

STLEntityTestCV$(CONFIG): STLEntityTest.cpp
	@echo STLEntityTestCV...
	@$(SET_CXX) $(SET_CXXFLAGS) -DENTITY_STRUCTURE=ENTSTRUC_CUSTOM_VECTOR -o $@ $<

STLEntityTestSOA$(CONFIG): STLEntityTest.cpp
	@echo STLEntityTestSOA...
	@$(SET_CXX) $(SET_CXXFLAGS) -DENTITY_STRUCTURE=ENTSTRUC_SOA -o $@ $<
