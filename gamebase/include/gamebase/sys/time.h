#ifndef GB_SYS_TIME_H
#define GB_SYS_TIME_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Timing */
void gbYield();
void gbDelay(gb_uint_t time);
gb_uint_t gbMillisecs();
gb_u64_t gbMicrosecs();
void gbQueryPerfTimer(gb_u64_t *seconds, gb_u64_t *nanoseconds);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
