#ifndef GB_BASE_CONSOLE_H
#define GB_BASE_CONSOLE_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Console Functions */
typedef enum {
	kGBConCol_Black,
	kGBConCol_Blue,
	kGBConCol_Green,
	kGBConCol_Cyan,
	kGBConCol_Red,
	kGBConCol_Purple,
	kGBConCol_Yellow,
	kGBConCol_White,

	kGBConCol_Grey,
	kGBConCol_BrightBlue,
	kGBConCol_BrightGreen,
	kGBConCol_BrightCyan,
	kGBConCol_BrightRed,
	kGBConCol_BrightPurple,
	kGBConCol_BrightYellow,
	kGBConCol_BrightWhite
} gb_consoleColor_t;

void gbSetConsoleTitle(const char *title);
const char *gbGetConsoleTitle();

void gbSetConsoleTextColor(gb_consoleColor_t conCol);
void gbSetConsoleBackColor(gb_consoleColor_t conCol);
gb_consoleColor_t gbGetConsoleTextColor();
gb_consoleColor_t gbGetConsoleBackColor();

const char *gbReadStdin();
void gbWriteStdout(const char *message);
void gbWriteStderr(const char *message);

void gbPrint(const char *message);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
