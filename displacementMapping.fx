
/*
===============================================================================

	LAYOUT STRUCTURES

	Notation:
	- C = clip space
	- M = model space (AKA local space, object space)
	- T = tangent space (AKA texture space)
	- V = view space
	- W = world space

===============================================================================
*/

// ==== ** Vertex shader input ** ====
struct vertex_t {

	float3 positionM : POSITION;
	float3 normalM : NORMAL;
	float3 tangentM : TANGENT;
	float3 bitangentM : BINORMAL;
	float2 texcoord0 : TEXCOORD0;

};
// ==== ** Tessellation control shader input / vertex shader output ** ====
struct control_t {

	float3 positionW : POSITIONWS;
	float3 normalW : NORMALWS;
	float3 texcoord0 : TEXCOORD0; //z contains distance factor

	float3 lightT : LIGHTVECTORTS;
	float3 viewT : VIEWVECTORTS;

};
// ==== ** Tessellator input / tessellation control shader output ** ====
struct tessellator_t {

	float edges[ 3 ] : SV_TessFactor;
	float inside : SV_InsideTessFactor;

};
// ==== ** Tessellation evaluation shader input ** ====
struct evaluation_t {

	float3 positionW : POSITIONWS;
	float3 normalW : NORMALWS;
	float3 texcoord0 : TEXCOORD0; //z contains depthV

	float3 lightT : LIGHTVECTORTS;
	float3 viewT : VIEWVECTORTS;

};
// ==== ** Fragment shader input / vertex shader output ** ====
struct fragment_t {

	float4 positionC : SV_Position;
	float3 texcoord0 : TEXCOORD0; //z contains depthV

	float3 lightT : LIGHTVECTORTS;
	float3 viewT : VIEWVECTORTS;

};
// ==== ** Rasterizer input / fragment shader output ** ====
struct rasterizer_t {

	float4 color : SV_Target0;

};


/*
===============================================================================

	RESOURCES

===============================================================================
*/

// ==== ** Diffuse ** ====
texture diffuseTexture : register( t0 ) <
	string ResourceName = "rocks.png";
	string ResourceType = "2D";
>;
sampler2D diffuseSampler : register( s0 ) = sampler_state {
	Texture = <diffuseTexture>;
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

// ==== ** Normals/Height ** ====
// Normals in RGB, height in A
texture normalTexture : register( t1 ) <
	string ResourceName = "rocks_NM_height.png";
	string ResourceType = "2D";
>;
sampler2D normalSampler : register( s1 ) = sampler_state {
	Texture = <normalTexture>;
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};


/*
===============================================================================

	UNIFORM INPUTS

===============================================================================
*/

// ==== ** Transformation matrices ** ====
cbuffer transforms : register( b0 ) {

	float4x4 modelToWorld : World;
	//float4x4 modelToView : WorldView;
	//float4x4 worldToView : View;
	//float4x4 modelToClip : WorldViewProjection;
	//float4x4 worldToModelTranspose : WorldInverseTranspose;
	//float4x4 worldToModel : WorldInverse;
	float4x4 viewToWorld : ViewInverse; //needed for position of camera because NF doesn't have a "EYEPOS" semantic
	float4x4 worldToClip : ViewProjection;

};

// ==== ** Material ** ====
cbuffer material : register( b1 )
{

	// ==== ** Lighting ** ====
	float3 lightDirW = { 0.2, 0.9, -0.4 };
	float3 lightColor = { 1, 0.625, 0.25 };

	float4 materialSpecular = { 1, 1, 1, 1 }; //XYZ=RGB, W=power

	// ==== ** Tessellation ** ====
	float2 distanceRange = { 3.0, 30.0 }; //used for adaptive tessellation
	float tessellationFactor = 3.0;

	// ==== ** Displacement ** ====
	float displacementScale = 1.0;

}


/*
===============================================================================

	VERTEX PROGRAM

===============================================================================
*/

// ==== ** Primary program ** ====
control_t displacementMapping_vp( in vertex_t vertex ) {

	control_t result;

	// compute the world space position of the vertex
	result.positionW = mul( float4( vertex.positionM, 0 ), modelToWorld ).xyz;

	// compute the distance factor (distance between the vertex and camera)
	const float dist = distance( result.positionW, viewToWorld[ 3 ].xyz );
	const float mind = distanceRange.x;
	const float maxd = distanceRange.y;
	const float range = distanceRange.y - distanceRange.x;

	// pass through the unaltered texture coordinate; distance factor in z
	result.texcoord0 =
		float3(
			vertex.texcoord0,
			1.0 - saturate( ( dist - mind )/range )
		);

	// move the normals into world space from model space
	result.normalW = normalize( mul( vertex.normalM, ( float3x3 )modelToWorld ) );
	const float3 tangentW = normalize( mul( vertex.tangentM, ( float3x3 )modelToWorld ) );
	const float3 bitangentW = normalize( mul( vertex.bitangentM, ( float3x3 )modelToWorld ) );

	// store the lighting information in tangent space
	const float3x3 worldToTangent = float3x3( tangentW, bitangentW, result.normalW );

	const float3 lightW = normalize( lightDirW )*float3( 1, 1, -1 );
	result.lightT = mul( worldToTangent, lightW );

	const float3 viewW = viewToWorld[ 3 ].xyz - modelToWorld[ 3 ].xyz;
	result.viewT = mul( worldToTangent, viewW );

	// vertex is done
	return result;

}


/*
===============================================================================

	TESSELLATION CONTROL PROGRAM

===============================================================================
*/

// ==== ** Constant program ** ====
tessellator_t displacementMapping_tcpConst( in InputPatch< control_t, 3 > inputPatch, in uint patchIndedx : SV_PrimitiveID ) {

	tessellator_t result;

	result.edges[ 0 ] = ( inputPatch[ 1 ].texcoord0.z + inputPatch[ 2 ].texcoord0.z )/2.0*tessellationFactor;
	result.edges[ 1 ] = ( inputPatch[ 2 ].texcoord0.z + inputPatch[ 0 ].texcoord0.z )/2.0*tessellationFactor;
	result.edges[ 2 ] = ( inputPatch[ 0 ].texcoord0.z + inputPatch[ 1 ].texcoord0.z )/2.0*tessellationFactor;
	result.inside = result.edges[ 0 ];

	return result;

}

// ==== ** Primary program ** ====
[ domain( "tri" ) ]
[ partitioning( "fractional_odd" ) ]
[ outputtopology( "triangle_cw" ) ]
[ outputcontrolpoints( 3 ) ]
[ patchconstantfunc( "displacementMapping_tcpConst" ) ]
[ maxtessfactor( 15.0 ) ]
control_t displacementMapping_tcp( in InputPatch< control_t, 3 > inputPatch, in uint patchIndex : SV_OutputControlPointID ) {

	// Pass through
	return inputPatch[ patchIndex ];

}


/*
===============================================================================

	TESSELLATION EVALUATION PROGRAM

===============================================================================
*/

// ==== ** Primary program ** ====
[ domain( "tri" ) ]
fragment_t displacementMapping_tep( in tessellator_t input, float3 barycentricCoordinates : SV_Domainlocation, const OutputPatch< control_t, 3 > trianglePatch ) {

	fragment_t result;

	// compute the relative world space position and normals
	const float3 positionW =
		barycentricCoordinates.x*trianglePatch[ 0 ].positionW +
		barycentricCoordinates.y*trianglePatch[ 1 ].positionW +
		barycentricCoordinates.z*trianglePatch[ 2 ].positionW;

	const float3 normalW = normalize(
		barycentricCoordinates.x*trianglePatch[ 0 ].normalW +
		barycentricCoordinates.y*trianglePatch[ 1 ].normalW +
		barycentricCoordinates.z*trianglePatch[ 2 ].normalW );

	// compute the relative values of the other attributes
	result.texcoord0 =
		barycentricCoordinates.x*trianglePatch[ 0 ].texcoord0 +
		barycentricCoordinates.y*trianglePatch[ 1 ].texcoord0 +
		barycentricCoordinates.z*trianglePatch[ 2 ].texcoord0;
	result.lightT =
		barycentricCoordinates.x*trianglePatch[ 0 ].lightT +
		barycentricCoordinates.y*trianglePatch[ 1 ].lightT +
		barycentricCoordinates.z*trianglePatch[ 2 ].lightT;
	result.viewT =
		barycentricCoordinates.x*trianglePatch[ 0 ].viewT +
		barycentricCoordinates.y*trianglePatch[ 1 ].viewT +
		barycentricCoordinates.z*trianglePatch[ 2 ].viewT;

	// perform the displacement mapping
	const float mipLevel = clamp( ( distance( positionW, viewToWorld[ 3 ].xyz ) - 100.0 )/100.0, 0.0, 3.0 );
	const float height = tex2Dlod( normalSampler, float4( result.texcoord0.xy, 0, mipLevel ) ).a - 1.0;

	result.positionC = mul( float4( positionW + normalW*( displacementScale*height ), 1.0 ), worldToClip );

	return result;

}


/*
===============================================================================

	FRAGMENT PROGRAM

===============================================================================
*/

rasterizer_t displacementMapping_fp( in fragment_t fragment ) {

	rasterizer_t result;

	const float3 normalT = normalize( tex2D( normalSampler, fragment.texcoord0.xy ).xyz*2.0 - 1.0 );
	const float3 lightT = normalize( fragment.lightT );
	const float3 viewT = normalize( fragment.viewT );

	const float4 base = tex2D( diffuseSampler, fragment.texcoord0.xy );
	const float3 diffuse = saturate( dot( normalT, lightT ) )*base.xyz*lightColor;

	const float3 reflectionT = normalize( 2.0*dot( viewT, normalT )*normalT - viewT );
	const float3 specular = pow( saturate( dot( reflectionT, lightT ) ), materialSpecular.w )*materialSpecular.xyz;

	result.color = float4( diffuse + specular, 1.0 /*base.a*/ );

	return result;

}


/*
===============================================================================

	RENDER PROGRAM

===============================================================================
*/

// ==== ** State ** ====
DepthStencilState DepthEnabled { DepthEnable = TRUE; };

// ==== ** Primary program ** ====
technique11 displacementMapping_rp {

	pass main_pass {

		SetVertexShader( CompileShader( vs_5_0, displacementMapping_vp() ) );
		SetHullShader( CompileShader( hs_5_0, displacementMapping_tcp() ) );
		SetDomainShader( CompileShader( ds_5_0, displacementMapping_tep() ) );
		SetGeometryShader( NULL );
		SetPixelShader( CompileShader( ps_5_0, displacementMapping_fp() ) );

		SetDepthStencilState( DepthEnabled, 0 );

	}

}


