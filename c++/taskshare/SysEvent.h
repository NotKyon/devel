#ifndef TASKSHARE_SYSEVENT_H
#define TASKSHARE_SYSEVENT_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

//------------------------------------------------------------------------------
// SYSTEM EVENTS

__TS_NS_INIT__

typedef struct sysEvent_s {
#if _WIN32
	HANDLE ev;
#else
	pthread_mutex_t mutex;
	pthread_cond_t cond;
#endif
} sysEvent_t;

//----

inline void InitSysEvent(sysEvent_t &sev);
inline void FiniSysEvent(sysEvent_t &sev);

inline void RaiseSysEvent(sysEvent_t &sev);
inline void ResetSysEvent(sysEvent_t &sev);

inline void SyncSysEvent(sysEvent_t &sev);
inline bool TrySyncSysEvent(sysEvent_t &sev);

//----

inline void InitSysEvent(sysEvent_t &sev) {
#if _WIN32
	sev.ev = CreateEvent(0, TRUE, FALSE, 0);
#else
	pthread_mutex_init(&sev.mutex, 0);
	pthread_cond_init(&sev.cond, 0);
#endif
}
inline void FiniSysEvent(sysEvent_t &sev) {
#if _WIN32
	if (sev.ev) {
		CloseHandle(sev.ev);
		sev.ev = (HANDLE)NULL;
	}
#else
	pthread_cond_destroy(&sev.cond);
	pthread_mutex_destroy(&sev.mutex);
#endif
}

inline void RaiseSysEvent(sysEvent_t &sev) {
#if _WIN32
	SetEvent(sev.ev);
#else
	pthread_cond_signal(&sev.cond);
#endif
}
inline void ResetSysEvent(sysEvent_t &sev) {
#if _WIN32
	ResetEvent(sev.ev);
#else
	/* TODO */
#endif
}

inline void SyncSysEvent(sysEvent_t &sev) {
#if _WIN32
	WaitForSingleObject(sev.ev, INFINITE);
#else
	//pthread_mutex_lock(&sev.mutex);
	pthread_cond_wait(&sev.cond, &sev.mutex);
	//pthread_mutex_unlock(&sev.mutex);
#endif
}
inline bool WaitSysEvent(sysEvent_t &sev, unsigned int milliseconds) {
#if _WIN32
	if (WaitForSingleObject(sev.ev, milliseconds) != WAIT_OBJECT_0)
		return false;

	return true;
#else
	static const struct timespec ts = {
		milliseconds/1000,
		(milliseconds%1000)*1000000
	};
	bool r = false;

	//pthread_mutex_lock(&sev.mutex);
	if (pthread_cond_timedwait(&sev.cond, &sev.mutex, &ts) == 0)
		r = true;
	//pthread_mutex_unlock(&sev.mutex);

	return r;
#endif
}
inline bool TrySyncSysEvent(sysEvent_t &sev) {
	return WaitSysEvent(sev, 0);
}

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
