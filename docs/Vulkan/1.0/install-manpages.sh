#!/usr/bin/env bash

# Get the current directory, resolving symlinks
# Thanks: http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

SRCFILES="$( find "$DIR/man/man3" -type f -name "*.3" )"
TARGETDIR="/usr/local/share/man/man3"
mkdir -p "$TARGETDIR" && cp $SRCFILES "$TARGETDIR/" && mandb
