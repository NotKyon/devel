#ifndef QUEUE_H
#define QUEUE_H

#include <cstddef>

template<typename T>
class Queue {
public:
	Queue();
	~Queue();

	size_t Capacity() const;
	size_t Count() const;

	size_t GetGranularity() const;
	void SetGranularity(size_t granularity);

	size_t Expand(size_t amnt);

	void Clear();

	void DestructPointers();
	void DestructObjects();
	void DeleteAll();

	bool IsEmpty() const;

	void Push(const T &x);
	T &Pull();
	const T &Peek() const;

	T &Get(size_t i);
	const T &Get(size_t i) const;

	inline T &operator[](size_t i) { return Get(i); }
	inline const T &operator[](size_t i) const { return Get(i); }

protected:
	size_t mCap, mCnt, mCur;
	size_t mGrn;
	T *mArr;

	virtual void OnAllocationError();
};

template<typename T> inline Queue<T>::Queue() {
	mCap = 0;
	mCnt = 0;
	mCur = 0;
	mGrn = 64;
	mArr = (T *)0;
}
template<typename T> inline Queue<T>::~Queue() {
	Clear();
}

template<typename T> inline size_t Queue<T>::Capacity() const {
	return mCap;
}
template<typename T> inline size_t Queue<T>::Count() const {
	return mCur;
}

template<typename T> inline size_t Queue<T>::GetGranularity() const {
	return mGrn;
}
template<typename T> inline void Queue<T>::SetGranularity(size_t granularity) {
	mGrn = granularity;
}

template<typename T> inline size_t Queue<T>::Expand(size_t amnt) {
	size_t n, i;
	T *p;
	
	if (!amnt)
		return mCur;

	n  = mCnt;
	n += amnt;
	n += mGrn;
	n -= n%mGrn;

	if (n > mCap) {
		mCap = n;

		p = new T[mCap];
		if (!p) {
			OnAllocationError();
			return (size_t)-1;
		}

		for(i=mCur; i<mCnt; i++)
			p[i - mCur] = mArr[i];

		mCur = 0;

		delete mArr;
		mArr = p;
	}

	i = mCur + mCnt;
	mCnt += amnt;

	return i;
}

template<typename T> inline void Queue<T>::Clear() {
	delete mArr;
	mArr = (T *)0;

	mCap = 0;
	mCnt = 0;
	mCur = 0;
}

template<typename T> inline bool Queue<T>::IsEmpty() const {
	return mCnt==0 ? true : false;
}

template<typename T> inline void Queue<T>::Push(const T &x) {
	size_t i;

	i = Expand(1);
	if (i==(size_t)-1)
		return;

	mArr[i] = x;
}
template<typename T> inline T &Queue<T>::Pull() {
	size_t i;

	i = mCur;

	mCur++;
	mCnt--;

	return mArr[i];
}
template<typename T> inline const T &Queue<T>::Peek() const {
	return mArr[mCur];
}

template<typename T> inline T &Queue<T>::Get(size_t i) {
	return mArr[mCur + i];
}
template<typename T> inline const T &Queue<T>::Get(size_t i) const {
	return mArr[mCur + i];
}

#include <cstdio>
#include <cstdlib>
template<typename T> inline void Queue<T>::OnAllocationError() {
	fprintf(stderr, "Error: Failed to allocate memory.\n");
	exit(EXIT_FAILURE);
}

#endif
