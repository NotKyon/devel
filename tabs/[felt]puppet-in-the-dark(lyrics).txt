﻿There is no time to spare, and
The stage has been set.
I cannot hold back the need to know.

Curiosity got to me.
The question's been asked.
I can't take it back again.

The falling stars align,
With darkness close behind.
Locked up in my mind,
With solace, I'm confined.

Can destiny define
The silence in the night?
And so, I've become
A puppet in the dark.

I know I've made up my mind
For something to do,
To clean out all of their stupid rules.

Been avoided for so long,
I'm tired of lies
And moving on for the truth.

Simple, like a drop of sun,
And the world spun on, but upside-down.
Is this what they'd call a miracle?

Falling, each and every one.
Like a house of cards, it's been undone.
Taking a leap without sound...

I'll shout just a bit louder, fight a bit harder.
I will go on. I'm undefeated.
Can't feel the hunger. I won't go under.
I'm my own person. I feel it.

Little bit stronger, going on farther.
No longer do I fear the darkness.
I can fly beyond the distant, new horizon forever.

All this time, reality's been fake.
Now I'm breaking through the tempered glass.
Set aside my doubts, won't hesitate.
I've been waiting through all of my life.

From today, I've buried it away.
In the end, I just might have to face
Something new with every passing day.

Now there's nowhere to run to.
I'm feeling alive,
Revived with a second chance to live.

Brand new days come and go.
So, if we let time
Slip by for eternity...

Small regrets will always add
Up to something too big if given time.
What do we do in adversity?

Even if I take a fall,
Tumbling down without time for a pause,
I know that I will become...

Someone's who's learned to be wiser, a bit of a fighter.
I can make right what had been wrong.
Though I can't be perfect, it'll be worth it.
I'll keep on trying and trying.

Without resistance, I'll make a difference.
Even mistakes will never stop me.
Only then, this world can find a new beginning in time...

Someone's who's learned to be wiser, a bit of a fighter.
I can make right what had been wrong.
Though I can't be perfect, it'll be worth it.
I'll keep on trying and trying.

I'll shout just a bit louder, fight a bit harder.
I will go on. I'm undefeated.
Can't feel the hunger. I won't go under.
I'm my own person. I feel it.

Little bit stronger, going on farther.
No longer do I fear the darkness.
I can fly beyond the distant, new horizon forever.

All this time, reality's been fake.
Now I'm breaking through the tempered glass.
Set aside my doubts, won't hesitate.
I've been waiting through all of my life.

From today, I've buried it away.
In the end, I just might have to face
Something new with every passing day.

The falling stars align,
With darkness close behind.
Locked up in my mind,
With solace, I'm confined.

Can destiny define
The silence in the night?
And so, I've become
A puppet in the dark.
