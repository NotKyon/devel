#include <cstdio>
#include <cstdlib>

#if __GNUC__ || __clang__ || __INTEL_COMPILER
# define likely(x) (__builtin_expect(!!(x), 1))
# define unlikely(x) (__builtin_expect(!!(x), 0))
#else
# define likely(x) (x)
# define unlikely(x) (x)
#endif

namespace lib {

template<typename T, typename KT=unsigned int>
class id_map {
protected:
	struct entry {
		union {
			entry *e;
			T *p;
		} tb[256];
	};

	mutable entry m_table;

	enum error_t {
		kErr_None,

		kErr_NotFound,
		kErr_NoMemory
	};
	mutable error_t m_error;

	bool m_delete;

	inline void set_error(error_t err) const {
		if (m_error)
			return;

		m_error = err;
	}

	T **access(KT key, bool create) const {
		unsigned char *k, j;
		size_t i;
		entry *p, *q;

		k = (unsigned char *)&key;
		p = &m_table;

		for(i=0; i<sizeof(key) - 1; i++) {
			j = k[i];

			if (!(q = p->tb[j].e)) {
				if unlikely (!create) {
					set_error(kErr_NotFound);
					return (T **)NULL;
				}

				if unlikely (!(p->tb[j].e = new entry())) {
					set_error(kErr_NoMemory);
					return (T **)NULL;
				}

				q = p->tb[j].e;
			}

			p = q;
		}

		return &p->tb[k[i]].p;
	}
	void remove_branch(entry *branch, size_t n) {
		size_t i;

		for(i=0; i<256; i++) {
			if (n && branch->tb[i].e)
				remove_branch(branch->tb[i].e, n - 1);
			else if(m_delete)
				delete branch->tb[i].p;
		}

		delete branch;
	}

public:
	id_map() {
		size_t i;

		for(i=0; i<256; i++)
			m_table.tb[i].e = (entry *)NULL;

		m_delete = true;
	}
	~id_map() {
		remove_all();
	}

	void remove_all() {
		size_t i;

		for(i=0; i<256; i++) {
			if (m_table.tb[i].e) {
				remove_branch(m_table.tb[i].e, sizeof(KT) - 1);
				m_table.tb[i].e = (entry *)NULL;
			}
		}
	}

	void set_delete_on_remove(bool del) {
		m_delete = del;
	}
	bool get_delete_on_remove() const {
		return m_delete;
	}

	int get_error() const {
		return (int)m_error;
	}
	void clear_error() {
		m_error = kErr_None;
	}

	T *get(KT key) const {
		T **place;

		if (!(place = access(key, false)))
			return (T *)NULL;

		return *place;
	}

	bool set(KT key, T *p, bool if_exists=false) {
		T **place;

		if unlikely (!(place = access(key, !if_exists)))
			return false;

		*place = p;
		return true;
	}
};

}

class Entity {
protected:
	unsigned int m_id;
	static lib::id_map<Entity> g_map;

	float m_pos[3];

public:
	Entity() {
	}
	~Entity() {
		Fini();
	}

	static bool Exists(unsigned int id) {
		return !!g_map.get(id);
	}
	static Entity *GetPtr(unsigned int id) {
		return g_map.get(id);
	}

	bool Init(unsigned int id) {
		if unlikely (Exists(id))
			return false;

		m_id = id;
		g_map.set(id, this);

		m_pos[0] = 0.0f;
		m_pos[1] = 0.0f;
		m_pos[2] = 0.0f;

		return true;
	}
	void Fini() {
		g_map.set(m_id, (Entity *)NULL);
	}

	void Position(float x, float y, float z) {
		m_pos[0] = x;
		m_pos[1] = y;
		m_pos[2] = z;
	}
	float PositionX() const {
		return m_pos[0];
	}
	float PositionY() const {
		return m_pos[1];
	}
	float PositionZ() const {
		return m_pos[2];
	}
};
lib::id_map<Entity> Entity::g_map;

void RuntimeError(const char *message) {
	fprintf(stderr, "Error: %s\n", message);
	exit(EXIT_FAILURE);
}

Entity *GetEntity(unsigned int id) {
	Entity *ent;

	if unlikely (!(ent = Entity::GetPtr(id)))
		RuntimeError("Entity does not exist!");

	return ent;
}

bool EntityExists(unsigned int id) {
	return Entity::Exists(id);
}
void MakeEntity(unsigned int id) {
	Entity *ent;

	if unlikely (EntityExists(id))
		RuntimeError("Entity already exists!");

	if unlikely (!(ent = new Entity()))
		RuntimeError("Failed to allocate entity.");

	if unlikely (!(ent->Init(id)))
		RuntimeError("Failed to initialize entity!");
}
void DeleteEntity(unsigned int id) {
	delete GetEntity(id);
}

void PositionEntity(unsigned int id, float x, float y, float z) {
	GetEntity(id)->Position(x, y, z);
}
float EntityPositionX(unsigned int id) {
	return GetEntity(id)->PositionX();
}
float EntityPositionY(unsigned int id) {
	return GetEntity(id)->PositionY();
}
float EntityPositionZ(unsigned int id) {
	return GetEntity(id)->PositionZ();
}

void PrintEntity(unsigned int id) {
	printf("%u -> (%f,%f,%f)\n",
		id, EntityPositionX(id),EntityPositionY(id),EntityPositionZ(id));
}

int main() {
	MakeEntity(1);
	MakeEntity(7);
	MakeEntity(94);
	MakeEntity(128);
	MakeEntity(23);
	MakeEntity(26);
	MakeEntity(1000);
	MakeEntity(10000);
	MakeEntity(200000);

	PositionEntity( 1, 1,0,0);
	PositionEntity( 7, 0,1,0);
	PositionEntity(94, 0,0,1);

	PrintEntity(1);
	PrintEntity(7);
	PrintEntity(94);

	PositionEntity(1000, 1,2,3);
	PositionEntity(200000, 4,5,6);

	PrintEntity(1000);
	PrintEntity(200000);

	DeleteEntity(1);
	DeleteEntity(7);
	DeleteEntity(94);
	DeleteEntity(128);
	DeleteEntity(23);
	DeleteEntity(26);
	DeleteEntity(1000);
	DeleteEntity(10000);
	DeleteEntity(200000);

	return EXIT_SUCCESS;
}
