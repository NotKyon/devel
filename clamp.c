#include <stdio.h>

float clamp(float x) {
#if 0
  return x<-1 ? -1 : x>1 ? 1 : x;
#else
  asm
  (
    "andl $0x7FFFFFFF, %[a]\n\t"   /* do one test (|a| <= |1|) instead of two (a>=-1 && a<=1) */
    "sub  %[a], %[c]\n\t"          /* '1.0 - a' will set the sign bit if 'a' is > 1.0 */
    "shrl $31, %[c]\n\t"           /* move that sign bit down */
    "neg  %[c]\n\t"                /* negate (-1=0xFFFFFFFF, -0=0x00000000) */
    "and  %[c], %[d]\n\t"          /* if outside of range, clamp to -1 or 1 */
    "mov  %[d], %[A]\n\t"          /* set to the output value */
    "xorl $0xFFFFFFFF, %[c]\n\t"   /* flip the bits (if -1 then 0; if 0 then -1) */
    "and  %[b], %[c]\n\t"          /* set to the original value if inside the range */
    "or   %[c], %[A]\n\t"          /* add to the output */
    : [A] "=r" (x)
    : [a] "r" (x),
      [b] "r" (x),
      [c] "r" (0x3F800000 /*1.0f*/),
      [d] "r" ((0x80000000&*(int *)&x)|0x3F800000)
  );

  return x;
#endif
}

void test(float x) {
  float f;

  f = clamp(x);
  printf("clamp(%f) = %f (0x%.8X)\n", x, f, *(int *)&f);
}

int main() {
  test(-3.0f);
  test(-1.5f);
  test(-0.25f);
  test(0.334f);
  test(0.9f);
  test(1.74f);
  test(10000.0f);
  test(-10000.0f);
  test(0.0f);

  return 0;
}
