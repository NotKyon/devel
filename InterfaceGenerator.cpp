﻿/*
===============================================================================

	INTERFACE GENERATOR

	Generates interfaces from source files.


	EXAMPLE INPUT

		#include "IInterface.hpp"

		class CImplementation: public virtual IInterface
		{
		public:
			CImplementation(): IInterface()
			{
			}
			virtual ~CImplementation()
			{
			}

			virtual void DoSomething()
			{
				AwesomeSauce( 0 );
				AwesomeSauce( 1 );
			}

			void AwesomeSauce( int x )
			{
				printf( "%i\n", x );
			}
		};

	EXAMPLE OUTPUT

		#ifndef IINTERFACE_HPP
		#define IINTERFACE_HPP

		class IInterface
		{
		public:
			IInterface()
			{
			}
			virtual ~IInterface()
			{
			}

			virtual void DoSomething() = 0;
		};
		
		#endif //IINTERFACE_HPP


	MULTIPLE IMPLEMENTATIONS

		INPUT

			#include "IInterface.hpp"

			class CImplementationA: public virtual IInterface
			{
			public:
				CImplementationA(): IInterface() {}
				virtual ~CImplementationA() { Fini(); }

				virtual bool Init()
				{
					return true;
				}
				virtual void Fini()
				{
				}
				virtual bool IsInitialized() const
				{
					return true;
				}

				virtual void OnAction()
				{
					printf( "Action!\n" );
				}
			};
			class CImplementationB: public virtual IInterface
			{
			public:
				CImplementationB(): IInterface() {}
				virtual ~CImplementationB() { Fini(); }

				virtual bool Init()
				{
					return true;
				}
				virtual void Fini()
				{
				}
				virtual bool IsInitialized() const
				{
					return true;
				}

				virtual void AwesomeSauce()
				{
					printf( "Awesome sauce!\n" );
				}
			};

		OUTPUT

			#ifndef IINTERFACE_HPP
			#define IINTERFACE_HPP
			
			class IInterface
			{
			public:
				IInterface()
				{
				}
				virtual ~IInterface()
				{
				}
				
				virtual bool Init() = 0;
				virtual void Fini() = 0;
				virtual bool IsInitialized() const = 0;
			};
			
			#endif //IINTERFACE_HPP


	DIRECTIVES

	It's possible to tell the program to do something from the source file by
	including special "directive comments" in the code. These comments are of
	the form:

		//::directive-name <optional-parameters>:://

	Where "<optional-parameters>" are arguments for the directive which may
	affect its behavior or results.


	INCLUDE HEADERS DIRECTIVE

		INPUT
		
			#include "IInterface.hpp"

			//::include "EMyEnum.hpp":://
			
			class CImplementation: public virtual IInterface
			{
			public:
				CImplementation(): IInterface()
				{
				}
				virtual ~CImplementation()
				{
				}

				virtual void DoSomething( EMyEnum action )
				{
					switch( action )
					{
						case EMyEnum::Greet:
							printf( "Hello, world!\n" );
							break;
						case EMyEnum::Leave:
							printf( "Okay, thanks. Bye!\n" );
							break;
					}
				}
			};

		OUTPUT
		
			#ifndef IINTERFACE_HPP
			#define IINTERFACE_HPP
			
			#include "EMyEnum.hpp"
			
			class IInterface
			{
			public:
				IInterface()
				{
				}
				virtual ~IInterface()
				{
				}
				
				virtual void DoSomething( EMyEnum action ) = 0;
			};
			
			#endif


	OTHER DIRECTIVES

		MARK FUNCTION AS UNUSED IN INTERFACE
		Useful when the generator picks up a member-function that should not be
		in the interface.

			//::unused-method class-name method-name:://


		SPECIFY INTERFACE HEADER NAME
		Useful if you don't like the automatically-determined interface header
		name.

			//::interface-header interface-name header-name:://

		If multiple interfaces are assigned to the same header file then they
		will both be generated to there, but this behavior is only present
		within one source file. It does not persist across source boundaries.

===============================================================================
*/

#if defined( _DEBUG )
# undef _DEBUG
# define _DEBUG 1
#elif defined( DEBUG ) || defined( __DEBUG__ ) || defined( __debug__ )
# define _DEBUG 1
#else
# undef NDEBUG
# define NDEBUG 1
# define _DEBUG 0
#endif

#if defined( _WIN32 )
# undef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN 1
# include <Windows.h>
# undef min
# undef max
#endif

#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <list>
#include <string>
#include <vector>

namespace Utility
{

	/*
	===========================================================================
	
		UTILITY :: REPORTER

		Handle logging and reports
	
	===========================================================================
	*/

	// Specifies the severity of any given report
	enum class ReportSeverity
	{
		// No severity, just a "normal" report (status, help, etc)
		Normal,
		// Debug text for development purposes
		Debug,
		// A potentially unwanted or non-optimal situation was detected
		Warning,
		// A definitely unwanted or unworkable situation has occurred
		Error
	};

	// Base interface class for reporters
	class Reporter
	{
	public:
		// UNDOC: Constructor
		Reporter()
		{
		}
		// UNDOC: Destructor
		virtual ~Reporter()
		{
		}

		// Handle a report
		//
		// sev: Severity of the report (see ReportSeverity above)
		// file: File the report concerns
		// line: Line the report concerns
		// message: Description of the report
		virtual void Report( ReportSeverity sev, const char *file, int line,
			const char *message ) = 0;
	};
	
	// Internal stuff for reporting
	namespace Implementation
	{

		// Get the reporters list
		std::list< Reporter * > &GetReporters()
		{
			static std::list< Reporter * > instance;
			return instance;
		}

	}

	// Submit a report to all listening reporters
	void Report( ReportSeverity sev, const char *file, int line,
	const char *message )
	{
		auto &reporters = Implementation::GetReporters();

		bool found = false;
		for( Reporter *&r : reporters )
		{
			if( !r )
			{
				continue;
			}
			
			found = true;
			r->Report( sev, file, line, message );
		}
		
		if( !found )
		{
			const char *const sevMsg =
				sev == ReportSeverity::Error ? "Error" :
				sev == ReportSeverity::Warning ? "Warning" :
				sev == ReportSeverity::Debug ? "Debug" :
				sev == ReportSeverity::Normal ? "Normal" :
				"(unknown-severity)";
			fprintf( stderr, "%s file=%s line=%i :: %s\n", sevMsg,
				file != nullptr ? file : "(nullptr)", line, message );
		}
	}
	// Same as above but uses std::string instead of const char pointer
	void Report( ReportSeverity sev, const char *file, int line,
	const std::string &message )
	{
		Report( sev, file, line, message.c_str() );
	}
	// Add a reporter interface for handling reports
	void AddReporter( Reporter *r )
	{
		if( !r )
		{
			return;
		}

		auto &reporters = Implementation::GetReporters();

		for( Reporter *&x : reporters )
		{
			if( x == r )
			{
				return;
			}
		}
		
		reporters.push_back( r );
	}
	// Remove an added reporter interface
	void RemoveReporter( Reporter *r )
	{
		if( !r )
		{
			return;
		}
		
		auto &reporters = Implementation::GetReporters();

		auto end = reporters.end();
		auto start = reporters.begin();
		for( auto iter = start; iter != end; ++iter )
		{
			if( *iter == r )
			{
				reporters.erase( iter );
				return;
			}
		}
	}

	// TRACE(msg) submits a debug report concerning this code IF in debug mode
#if _DEBUG
# define TRACE( msg )\
	Utility::Report( Utility::ReportSeverity::Debug, __FILE__, __LINE__, msg )
#else
# define TRACE( msg )\
	( void )0
#endif

	/*
	===========================================================================
	
		UTILITY :: DEBUG LOG REPORTER (REPORTER IMPLEMENTATION)

		Writes reports to "debug.log"
	
	===========================================================================
	*/
	class DebugLogReporter: public virtual Reporter
	{
	public:
		// Register this reporter with the internal reporting system
		static void Install()
		{
			AddReporter( &GetInstance() );
		}
		// Remove this reporter from the internal reporting system
		static void Uninstall()
		{
			RemoveReporter( &GetInstance() );
		}

		// UNDOC: Handle a report
		virtual void Report( ReportSeverity sev, const char *file, int line,
		const char *message )
		{
			FILE *fp = fopen( "debug.log", "a+" );
			if( !fp )
			{
				return;
			}
			
			const char *sevMsg = "";
			switch( sev )
			{
				case ReportSeverity::Normal:
					break;
					
				case ReportSeverity::Debug:
					sevMsg = "***DEBUG*** ";
					break;
				case ReportSeverity::Warning:
					sevMsg = "Warning ";
					break;
				case ReportSeverity::Error:
					sevMsg = "Error ";
					break;
			}
			
			if( file != nullptr )
			{
				if( line > 0 )
				{
					fprintf( fp, "[%s(%i)]\n\t%s%s\n\n",
						file, line, sevMsg, message );
				}
				else
				{
					fprintf( fp, "[%s]\n\t%s%s\n\n",
						file, sevMsg, message );
				}
			}
			else
			{
				fprintf( fp, "%s%s\n", sevMsg, message );
			}
			
			fclose( fp );
		}

	private:
		// UNDOC: Singleton
		static DebugLogReporter &GetInstance()
		{
			static DebugLogReporter instance;
			return instance;
		}

		// UNDOC: Constructor
		DebugLogReporter(): Reporter()
		{
		}
		// UNDOC: Destructor
		virtual ~DebugLogReporter()
		{
			RemoveReporter( this );
		}
	};

	/*
	===========================================================================
	
		UTILITY :: CONSOLE REPORTER (REPORTER IMPLEMENTATION)

		Implements colored console output for reports
	
	===========================================================================
	*/
	class ConsoleReporter: public virtual Reporter
	{
	public:
		// Register this reporter with the internal reporting system
		static void Install()
		{
			AddReporter( &GetInstance() );
		}
		// Remove this reporter from the internal reporting system
		static void Uninstall()
		{
			RemoveReporter( &GetInstance() );
		}

		// UNDOC: Handle a report
		virtual void Report( ReportSeverity sev, const char *file, int line,
		const char *message )
		{
			const unsigned char curCol = GetCurrentColors( mStdErr );
			const unsigned char bgCol = curCol & 0xF0;

			Color sevCol = kColor_White;
			const char *sevMsg = nullptr;
			
			switch( sev )
			{
				case ReportSeverity::Normal:
					break;
					
				case ReportSeverity::Debug:
					sevCol = kColor_LightMagenta;
					sevMsg = "***DEBUG***";
					break;
				case ReportSeverity::Warning:
					sevCol = kColor_LightBrown;
					sevMsg = "WARNING";
					break;
				case ReportSeverity::Error:
					sevCol = kColor_LightRed;
					sevMsg = "ERROR";
					break;
			}
			
			if( sevMsg != nullptr )
			{
				SetCurrentColors( mStdErr, sevCol | bgCol );
				WriteString( mStdErr, sevMsg );

				SetCurrentColors( mStdErr, curCol );
				WriteString( mStdErr, ": " );
			}

			if( file != nullptr )
			{
				SetCurrentColors( mStdErr, kColor_Cyan | bgCol );
				WriteString( mStdErr, "[" );

				SetCurrentColors( mStdErr, kColor_White ); //black bg
				WriteString( mStdErr, file );
				
				if( line > 0 )
				{
					SetCurrentColors( mStdErr, kColor_LightCyan | bgCol );
					WriteString( mStdErr, "(" );

					char buf[ 32 ];
					char *p = &buf[ 31 ];
					*p = '\0';

					int n = line;
					while( n > 0 )
					{
						*--p = ( char )( '0' + n%10 );
						n /= 10;
					}
					
					SetCurrentColors( mStdErr, kColor_Brown ); //black bg
					WriteString( mStdErr, p ); //line number

					SetCurrentColors( mStdErr, kColor_LightCyan | bgCol );
					WriteString( mStdErr, ")" );
				}

				SetCurrentColors( mStdErr, kColor_Cyan | bgCol );
				WriteString( mStdErr, "] " );
			}

			SetCurrentColors( mStdErr, curCol );
			WriteString( mStdErr, message );
			WriteString( mStdErr, "\n" );
		}

	private:
		// Color codes used internally
		//
		// NOTE: If used as-is then these represent foreground (text) colors
		//       with a black background color.
		enum Color : unsigned char
		{
			kColor_Black		= 0x0,
			kColor_Blue			= 0x1,
			kColor_Green		= 0x2,
			kColor_Cyan			= 0x3,
			kColor_Red			= 0x4,
			kColor_Magenta		= 0x5,
			kColor_Brown		= 0x6,
			kColor_LightGrey	= 0x7,
			kColor_DarkGrey		= 0x8,
			kColor_LightBlue	= 0x9,
			kColor_LightGreen	= 0xA,
			kColor_LightCyan	= 0xB,
			kColor_LightRed		= 0xC,
			kColor_LightMagenta	= 0xD,
			kColor_LightBrown	= 0xE,
			kColor_White		= 0xF,
			
			kColor_Purple		= kColor_Magenta,
			kColor_Yellow		= kColor_LightBrown,
			kColor_Violet		= kColor_LightMagenta,

			kColor_LightGray	= kColor_LightGrey,
			kColor_DarkGray		= kColor_DarkGrey
		};

#if defined( _WIN32 )
		typedef HANDLE FileHandle;
#else
		typedef FILE *FileHandle;
#endif
		FileHandle mStdOut;
		FileHandle mStdErr;

		// UNDOC: Singleton
		static ConsoleReporter &GetInstance()
		{
			static ConsoleReporter instance;
			return instance;
		}

		// UNDOC: Constructor
		ConsoleReporter(): Reporter(), mStdOut( NULL ), mStdErr( NULL )
		{
#if defined( _WIN32 )
			mStdOut = GetStdHandle( STD_OUTPUT_HANDLE );
			if( mStdOut == INVALID_HANDLE_VALUE )
			{
				mStdOut = ( FileHandle )0;
			}

			mStdErr = GetStdHandle( STD_ERROR_HANDLE );
			if( mStdErr == INVALID_HANDLE_VALUE )
			{
				mStdErr = ( FileHandle )0;
			}
#else
			mStdOut = stdout;
			mStdErr = stderr;
#endif

			AddReporter( this );
		}
		// UNDOC: Destructor
		virtual ~ConsoleReporter()
		{
			RemoveReporter( this );
		}
		
		// No copy constructor
		ConsoleReporter( const ConsoleReporter & ) = delete;
		// No assignment operator
		ConsoleReporter &operator=( const ConsoleReporter & ) = delete;

		// Retrieve the current colors of the given console handle
		static unsigned char GetCurrentColors( FileHandle f )
		{
			if( !f )
			{
				return 0x07;
			}
			
#if defined( _WIN32 )
			CONSOLE_SCREEN_BUFFER_INFO csbi;
			
			if( GetConsoleScreenBufferInfo( f, &csbi ) )
			{
				return csbi.wAttributes & 0xFF;
			}
#else
			//
			//	TODO: Support this on GNU/Linux...
			//
#endif

			return 0x07;
		}
		// Set the current colors for the given console handle
		static void SetCurrentColors( FileHandle f, unsigned char colors )
		{
			if( !f )
			{
				return;
			}

#if defined( _WIN32 )
			SetConsoleTextAttribute( f, ( WORD )colors );
#else
			//
			//	MAP ORDER (0-7):
			//	Black, Blue, Green, Cyan, Red, Magenta, Yellow, Grey
			//
			//	TERMINAL COLOR ORDER (0-7):
			//	Black, Red, Green, Yellow, Blue, Magenta, Cyan, Grey
			//
			static const char *const mapF[16] = {
				"\x1b[30;22m", "\x1b[34;22m", "\x1b[32;22m", "\x1b[36;22m",
				"\x1b[31;22m", "\x1b[35;22m", "\x1b[33;22m", "\x1b[37;22m",
				"\x1b[30;1m", "\x1b[34;1m", "\x1b[32;1m", "\x1b[36;1m",
				"\x1b[31;1m", "\x1b[35;1m", "\x1b[33;1m", "\x1b[37;1m"
			};
			
			const char *const code = mapF[ colors & 0x0F ];
			fwrite( ( const void * )code, strlen( code ), 1, f );
#endif
		}
		// Write a string of text to the given console handle
		static void WriteString( FileHandle f, const char *s )
		{
#if defined( _WIN32 )
			//
			//	FIXME: WriteFile() causes crash in some cases
			//
			
			FILE *fp = stdout;
			if( f == GetStdHandle( STD_ERROR_HANDLE ) )
			{
				fp = stderr;
			}
			
			fprintf( fp, "%s", s );
#else
			fprintf( f, "%s", s );
#endif
		}
	};
	
	/*
	===========================================================================
	
		INTRUSIVE LINKED LIST SYSTEM

		List items should inherit from ListElement while list bases should
		inherit from ListBase. (This is not strictly necessary, but probably
		easier to deal with.)

		NOTE: Look at Node for a usage example. (Search for "class Node"; it's
		-     in the TypeInfo namespace.)
	
	===========================================================================
	*/

	// TDestroyPolicy: ListElement<>'s Remove() should only unlink
	struct RemoveOnDestruct { static const bool destroy = false; };
	// TDestroyPolicy: ListElement<>'s Remove() should *DELETE* after unlinking
	struct DestroyOnDestruct { static const bool destroy = true; };

	// Forward declaration
	template< typename T, typename TDestroyPolicy = RemoveOnDestruct >
	class ListElement;

	// UNDOC: ListBase
	template< typename T >
	class ListBase
	{
	friend class ListElement< T, RemoveOnDestruct >;
	friend class ListElement< T, DestroyOnDestruct >;
	public:
		// UNDOC: Constructor
		ListBase()
		: mHead( nullptr )
		, mTail( nullptr )
		{
		}
		// UNDOC: Destructor
		~ListBase()
		{
			printf( " *** ~ListBase\n" );
			Clear();
		}

		ListBase( const ListBase & ) = delete;
		ListBase &operator=( const ListBase & ) = delete;
		
		void Clear()
		{
			while( mHead != nullptr )
			{
				mHead->Remove();
			}
		}
		
		void AddHead( T *x )
		{
			x->Unlink();

			x->mList = this;
			x->mPrev = nullptr;
			x->mNext = mHead;
			if( mHead != nullptr )
			{
				mHead->mPrev = x;
			}
			else
			{
				mTail = x;
			}
			mHead = x;
		}
		void AddTail( T *x )
		{
			printf( "%p->AddTail( %p )\n", ( void * )this, ( void * )x );
			x->Unlink();

			x->mList = this;
			x->mNext = nullptr;
			x->mPrev = mTail;
			if( mTail != nullptr )
			{
				mTail->mNext = x;
			}
			else
			{
				mHead = x;
			}
			mTail = x;
		}
		
		T *Head()
		{
			return mHead;
		}
		const T *Head() const
		{
			return mHead;
		}
		
		T *Tail()
		{
			return mTail;
		}
		const T *Tail() const
		{
			return mTail;
		}

	private:
		T *mHead, *mTail;
	};
	
	// UNDOC: ListElement
	template< typename T, typename TDestroyPolicy >
	class ListElement
	{
	friend class ListBase< T >;
	friend T;
	public:
		void Remove()
		{
			Unlink();

			if( TDestroyPolicy::destroy )
			{
				delete this;
			}
		}

		void Unlink()
		{
			if( !mList )
			{
				return;
			}

			if( mPrev != nullptr )
			{
				mPrev->mNext = mNext;
			}
			else
			{
				mList->mHead = mNext;
			}
			
			if( mNext != nullptr )
			{
				mNext->mPrev = mPrev;
			}
			else
			{
				mList->mTail = mPrev;
			}
			
			mPrev = nullptr;
			mNext = nullptr;
			mList = nullptr;
		}
		
		ListBase< T > *List() { return mList; }
		const ListBase< T > *List() const { return mList; }

		T *Prev() { return mPrev; }
		T *Next() { return mNext; }

		const T *Prev() const { return mPrev; }
		const T *Next() const { return mNext; }
		
	protected:
		ListBase< T > *mList;
		T *mPrev;
		T *mNext;
		
		// UNDOC: Constructor
		ListElement()
		: mList( nullptr )
		, mPrev( nullptr )
		, mNext( nullptr )
		{
		}
		// UNDOC: Destructor
		~ListElement()
		{
			printf( " *** %p->~ListElement\n", ( void * )this );
			Unlink();
		}
		
		// No copy constructor
		ListElement( const ListElement & ) = delete;
		// No assignment operator
		ListElement &operator=( const ListElement & ) = delete;
	};

}

/*
===============================================================================

	TYPE INFO

	Holds various bits of information about C++ source files.

===============================================================================
*/
namespace TypeInfo
{

	// A type attribute / qualifier
	enum class Attribute
	{
		None,
		Signed,
		Unsigned,
		Long,
		Pointer,
		Reference,
		Const,
		Volatile,
		Restrict,
		Override,
		Final,
		Deprecated
	};
	// Represents a built-in type or a reference to another type
	enum class BuiltinType
	{
		Void,
		Char,
		Short,
		Int,
		Float,
		Double,
		WideChar,
		Typename
	};
	// The keyword used for declaring a type (union, struct, or class)
	enum class TypeKeyword
	{
		Namespace, //not really a type but has a use
		Union,
		Struct,
		Class
	};

	/*
	===========================================================================
	
		NODE INTERFACE

		Provides an abstract node which represents an element (or collection of
		elements) within a source file.

		Nodes implement a basic tree hierarchy.
		e.g.,

			A   -   -   - The root node "A" owns B, E, and I
			+-B .   .   . Subnode of A, "B", owns C and D
			| +-C   -   - Leaf-node "C" by definition has no subnodes
			| +-D   .   . "D" also owns no nodes, and so is a leaf-node
			+-E -   -   - Subnode of A, "E" owns one node: F
			| +-F   .   . Subnode of E, "F" owns two nodes: G and H
			|   +-G -   - G is a leaf-node
			|   +-H .   . And so is H
			+-I -   -   - I is owned by A and has two subnodes: J and L
			  +-J   .   . J is owned by I and owns one leaf-node: K
			  | +-K -   - K is owned by J and owns no nodes; it is a leaf-node
			  +-L   .   . L is owned by I and owns no nodes; also a leaf-node

		Each node stores a line number (to help track where it originated from
		a source file). This can be used for reporting errors concerning that
		node.

		Each custom node implementation must implement this function:

			virtual NodeType Type() const = 0;

		It only returns the type that the node is. It is recommended you add a
		type to the NodeType enum for your types rather than re-use another if
		the functionality isn't the same. (And if it is the same then why are
		you reimplementing it instead of just reusing it?)
	
	===========================================================================
	*/

	// Implementation type of a node interface
	enum class NodeType
	{
		// Root node containing all the data in the file
		Source,
		// struct, class, union
		Type,
		// Virtual node for base classes
		TypeBases,
		// Function, member-function (method)
		Function,
		// Global variable, member field, parameter, etc
		Variable,
		// Arbitrary passage of code (not parsed)
		Code,
		// Command for utility, *not* preprocessor directive
		MetaDirective,
		// Doc-block
		Documentation
	};

	// UNDOC: The node interface
	class Node
	: public Utility::ListElement< Node, Utility::DestroyOnDestruct >
	{
	friend class Utility::ListBase< Node >;
	public:
		// UNDOC: Constructor
		Node()
		: ListElement()
		, mNodes()
		, mParent( nullptr )
		, mLine( 0 )
		{
		}
		// UNDOC: Destructor
		virtual ~Node()
		{
			Clear();
		}
		
		// Remove all child nodes
		void Clear()
		{
			mNodes.Clear();
		}
		// Unlink this node
		void Unlink()
		{
			ListElement::Unlink();
			mParent = nullptr;
		}
		// Remove this node
		void Remove()
		{
			Unlink();
			delete this;
		}
		
		// Set the line within the file this node originates from
		void SetLine( int line )
		{
			mLine = line;
		}
		// Retrieve the line of the file this node originates from
		int GetLine() const
		{
			return mLine;
		}
		
		// Adds a child node to this one at the end of the list
		void AddChild( Node *x )
		{
			printf( "%p->AddChild( %p )\n", ( void * )this, ( void * )x );
			mNodes.AddTail( x );
			x->mParent = this;
		}

		// Retrieve this node's parent node
		Node *Parent()
		{
			return mParent;
		}
		// Constant version of the above
		const Node *Parent() const
		{
			return mParent;
		}

		Node *Prev() { return ListElement::Prev(); }
		Node *Next() { return ListElement::Next(); }
		Node *Head() { return mNodes.Head(); }
		Node *Tail() { return mNodes.Tail(); }
		const Node *Prev() const { return ListElement::Prev(); }
		const Node *Next() const { return ListElement::Next(); }
		const Node *Head() const { return mNodes.Head(); }
		const Node *Tail() const { return mNodes.Tail(); }
		
		// Retrieve the implementation type for this node
		virtual NodeType Type() const = 0;

	private:
		Utility::ListBase< Node > mNodes;
		Node *mParent;
		int mLine;
	};
	
	/*
	===========================================================================
	
		SOURCE NODE (IMPLEMENTS NODE INTERFACE)

		Represents a C++ translation unit (source file).
	
	===========================================================================
	*/
	class SourceNode: public virtual Node
	{
	public:
		// UNDOC: Constructor
		SourceNode()
		: Node()
		, mFilename()
		{
		}
		// UNDOC: Destructor
		virtual ~SourceNode()
		{
		}

		// Retrieve the implementation type for this node
		virtual NodeType Type() const
		{
			return NodeType::Source;
		}

		// Set the name of this file
		void SetFilename( const char *p )
		{
			mFilename.assign( p );
		}
		// Retrieve the name of this file
		const char *GetFilename() const
		{
			printf( "SourceNode( %p )::mFilename.length()=%i\n",
				( void * )this, ( int )mFilename.length() );
			return mFilename.c_str();
		}
		
		// Find the source node that owns the given node; returns nullptr if no
		// source node is found
		static SourceNode *Find( Node *x )
		{
			printf( "SourceNode::Find( %p )\n", ( void * )x );
			while( x != nullptr && x->Type() != NodeType::Source )
			{
				printf( "    %p->type=%i\n", ( void * )x, ( int )x->Type() );
				x = x->Parent();
			}
			
			printf( "    return %p\n", ( void * )x );
			return reinterpret_cast< SourceNode * >( x );
		}
		// Retrieve the filename of the source node that owns the given node;
		// returns nullptr if no source node is found or a filename has not
		// been set
		static const char *FindFilename( Node *x )
		{
			printf( "SourceNode::FindFilename( %p )\n", ( void * )x );
			SourceNode *const p = Find( x );
			printf( "%p = SourceNode::Find()\n", ( void * )p );
			if( !p )
			{
				return nullptr;
			}
			
			printf( "Filename = %s\n", p->GetFilename() );
			return p->GetFilename();
		}

	private:
		std::string mFilename;
	};
	
	using Utility::ReportSeverity;
	// Submit a report (Utility::Report()) using a node for file/line info
	void Report( ReportSeverity sev, Node *node, const char *message )
	{
		const char *const file = SourceNode::FindFilename( node );
		const int line = node != nullptr ? node->GetLine() : 0;
		
		printf( "::%s,%i\n", file, line );

		Utility::Report( sev, file, line, message );
	}
	// Same as the Report() above but takes a std::string
	void Report( ReportSeverity sev, Node *node, const std::string &message )
	{
		Report( sev, node, message.c_str() );
	}

	/*
	===========================================================================
	
		TYPE NODE (IMPLEMENTS NODE INTERFACE)

		Represents a type, such as a class or structure. The subnodes represent
		the items owned by the type. (Fields, member functions, sub-types,
		etc.)
	
	===========================================================================
	*/
	class TypeNode: public virtual Node
	{
	public:
		// UNDOC: Constructor
		TypeNode()
		: Node()
		, mTypeKeyword( TypeKeyword::Struct )
		, mName()
		, mBases( nullptr )
		{
		}
		// UNDOC: Destructor
		virtual ~TypeNode()
		{
		}
		
		TypeNode( const TypeNode & ) = delete;
		TypeNode &operator=( const TypeNode & ) = delete;

		// Retrieve the implementation type for this node
		virtual NodeType Type() const
		{
			return NodeType::Type;
		}

		// Set the type keyword for this type
		//
		// The "type keyword" represents the C++ keyword used to declare this
		// type, which has some effect on how it behaves
		void SetTypeKeyword( TypeKeyword tk )
		{
			mTypeKeyword = tk;
		}
		// Retrieve the type keyword for this type
		//
		// The "type keyword" represents the C++ keyword used to declare this
		// type, which has some effect on how it behaves
		TypeKeyword GetTypeKeyword() const
		{
			return mTypeKeyword;
		}
		
		// Set the name of this type
		void SetName( const char *name )
		{
			mName.assign( name );
		}
		// Retrieve the name of this type
		const char *GetName() const
		{
			return mName.c_str();
		}

		// Add a base class for this type
		//
		// This is used for representing types which this type is derived from.
		// Only applies to structs and classes.
		void AddBaseClass( TypeNode *node );
		
		// Retrieve the list of base types
		class TypeBasesNode *Bases()
		{
			return mBases;
		}
		// Retrieve a constant pointer to the list of base types
		const class TypeBasesNode *Bases() const
		{
			return mBases;
		}

	private:
		TypeKeyword mTypeKeyword;
		std::string mName;
		class TypeBasesNode *mBases;
	};
	// Stores the base types for a type node
	class TypeBasesNode: public virtual Node
	{
	public:
		// UNDOC: Constructor
		TypeBasesNode( TypeNode &type ): mType( type )
		{
		}
		// UNDOC: Destructor
		virtual ~TypeBasesNode()
		{
		}

		// Retrieve the implementation type for this node
		virtual NodeType Type() const
		{
			return NodeType::TypeBases;
		}

		// Retrieve the type node that owns this
		TypeNode *GetTypeNode()
		{
			return &mType;
		}
		// Retrieve a constant pointer to the type node that owns this
		const TypeNode *GetTypeNode() const
		{
			return &mType;
		}

	private:
		TypeNode &mType;
	};
	
	void TypeNode::AddBaseClass( TypeNode *node )
	{
		if( !node )
		{
			return;
		}
		
		if( !mBases )
		{
			mBases = new TypeBasesNode( *this );
			if( !mBases )
			{
				Report( ReportSeverity::Error, node, "Unable to add base" );
				return;
			}
		}
	}
	
	// ... Something? Wtf?
	struct Name
	{
		std::list< Attribute > qualifiers;
		std::string identifier;

		inline Name(): qualifiers(), identifier()
		{
		}
		inline ~Name()
		{
		}
	};

}

/*
===============================================================================

	APPLICATION CODE

	Stuff relevant to the front-end and direction of the app.

===============================================================================
*/
namespace App
{

	// Initialize the app from a set of command-line arguments
	bool Init( std::vector< std::string > &args )
	{
		// Should the Utility::ConsoleReporter be installed?
		bool installConsoleReporter = true;
		// Should the Utility::DebugLogReporter be installed?
		bool installDebugLogReporter = false;
		
		// Store a reference to an unknown flag if one comes up
		const std::string *unknownFlag = nullptr;

		// Enumerate each argument
		for( const auto &s : args )
		{
			// Ignore empty arguments
			if( s.empty() )
			{
				continue;
			}

			// If starting with '-' then this is an option (probably)
			if( s[ 0 ] == '-' )
			{
				// Two starting dashes is okay; skip a second if present
				std::string opt = s.substr( 1 );
				if( opt[ 0 ] == '-' )
				{
					opt = opt.substr( 1 );
				}

				// Check if disabling functionality (--no-<something>)
				bool flag = true;
				if( opt.substr( 0, 3 ) == "no-" )
				{
					flag = false;
					opt = opt.substr( 3 );
				}

				// Utility::ConsoleReporter installation permission
				if( opt == "colored" || opt == "coloured" )
				{
					installConsoleReporter = flag;
				}
				// Utility::DebugLogReporter installation permission
				else if( opt == "debug-log" )
				{
					installDebugLogReporter = flag;
				}
				// Unknown argument encountered
				else
				{
					unknownFlag = &s;
				}
			}
		}
		
		// Install the reporters *BEFORE* presenting any errors so the
		// appropriate error reporting mediums can be used
		if( installConsoleReporter )
		{
			Utility::ConsoleReporter::Install();
		}
		if( installDebugLogReporter )
		{
			Utility::DebugLogReporter::Install();
		}

		// Handle an unknown flag error
		if( unknownFlag )
		{
			// Notify of an unknown argument but don't bother anymore with it
			Utility::Report( Utility::ReportSeverity::Warning, nullptr, 0,
				std::string( "Unknown argument: " ) + *unknownFlag );
		}

		// Done
		return true;
	}

	// Implementation of the main program's behavior
	int Main( std::vector< std::string > &args )
	{
		// Initialize the main app
		Init( args );

		// *** TESTING CODE *** //
		
		Utility::Report( Utility::ReportSeverity::Warning, __FILE__, __LINE__,
			"Testing code..." );
		
		TRACE( "TypeInfo::SourceNode test" );
		auto *test = new TypeInfo::SourceNode();
		TRACE( "test.SetFilename" );
		test->SetFilename( "test.cpp" );
		TRACE( "test.SetLine" );
		test->SetLine( 0 );
		
		TRACE( "TypeInfo::TypeNode impl" );
		auto *impl = new TypeInfo::TypeNode();
		TRACE( "impl.SetLine" );
		impl->SetLine( 1 );
		TRACE( "impl.SetTypeKeyword" );
		impl->SetTypeKeyword( TypeInfo::TypeKeyword::Class );
		TRACE( "impl.SetName" );
		impl->SetName( "CImplementation" );

		TRACE( "impl.AddChild" );
		test->AddChild( impl );

		TRACE( "TypeInfo::TypeNode iface" );
		auto *iface = new TypeInfo::TypeNode();
		TRACE( "iface.SetTypeKeyword" );
		iface->SetTypeKeyword( impl->GetTypeKeyword() );
		TRACE( "iface.SetName" );
		iface->SetName( "IInterface" );

		TRACE( "impl.AddBaseClass" );
		impl->AddBaseClass( iface );
		
		TRACE( "Done testing!" );
		
		printf( "::test=%p\n", ( void * )test );
		printf( "::test->head=%p\n", ( void * )test->Head() );
		printf( "::test->tail=%p\n", ( void * )test->Tail() );
		printf( "::impl->type=%i\n", ( int )impl->Type() );
		printf( "::impl->prnt=%p\n", ( void * )impl->Parent() );
		if( impl->Parent() != nullptr )
		{
			printf( "::impl->prnt->type=%i\n", ( int )impl->Parent()->Type() );
		}
		TypeInfo::Report( TypeInfo::ReportSeverity::Normal, impl, "Test" );
		
		delete test;
		
		// Done
		return EXIT_SUCCESS;
	}

}

// Entry point. Invokes App::Main() with a list of arguments
int main( int argc, char **argv )
{
	// Construct the command-line arguments
	std::vector< std::string > args;

	args.reserve( argc );
	for( int i = 1; i < argc; ++i )
	{
		args.push_back( std::string( argv[ i ] ) );
	}
	args.push_back( std::string() );

	// Push the arguments to the app and go
	return App::Main( args );
}
