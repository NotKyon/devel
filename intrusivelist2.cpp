// g++ -std=gnu++11 -o intrusivelist2 intrusivelist2.cpp

// const_this
namespace detail
{

	template< typename T >
	inline const T *get_const( T *x )
	{
		return x;
	}

}

#define const_this detail::get_const( this )

// safe_delete
template< typename T >
void safe_delete( T *&x )
{
	delete x;
	x = nullptr;
}

#include <stddef.h>
#include <stdint.h>

struct bidirectional_iterator_tag
{
};

template< typename T >
class intrusive_list;

struct intrusive_list_node_base
{
	intrusive_list_node_base *prev;
	intrusive_list_node_base *next;
};

template< typename T >
class intrusive_list_node
{
friend class intrusive_list< T >;
public:
	inline intrusive_list_node( T *value = nullptr ): mPrev( nullptr ),
	mNext( nullptr ), mValue( value )
	{
	}

	inline void set_value( T *value )
	{
		mValue = value;
	}
	inline T *get_value()
	{
		return mValue;
	}
	inline const T *get_value() const
	{
		return mValue;
	}

	inline intrusive_list_node *get_prev()
	{
		return mPrev;
	}
	inline const intrusive_list_node *get_prev() const
	{
		return mPrev;
	}
	inline intrusive_list_node *get_next()
	{
		return mNext;
	}
	inline const intrusive_list_node *get_next() const
	{
		return mNextext;
	}

protected:
	intrusive_list_node *mPrev;
	intrusive_list_node *mNext;
	T *mValue;
};

template< typename T, typename Pointer, typename Reference >
class intrusive_list_iterator
{
public:
	typedef intrusive_list_iterator< T, Pointer, Reference > this_type;
	typedef intrusive_list_iterator< T, T *, T & > iterator;
	typedef intrusive_list_iterator< T, const T *, const T & > const_iterator;
	typedef T value_type;
	typedef intrusive_list_node< T > node_type;
	typedef ptrdiff_t difference_type;
	typedef Pointer pointer;
	typedef Reference reference;
	typedef bidirectional_iterator_tag iterator_category;

public:
	inline intrusive_list_iterator( node_type *x = nullptr ): mNode( x )
	{
	}
	inline intrusive_list_iterator( const iterator &x ): mNode( x.mNode )
	{
	}

	inline intrusive_list_iterator &operator=( const this_type &x )
	{
		mNode = x.mNode;
		return *this;
	}

	inline reference operator*() const
	{
		return *mNode->get_value();
	}
	inline pointer operator->() const
	{
		return mNode->get_value();
	}

	inline intrusive_list_iterator &operator++()
	{
		mNode = mNode->get_next();
		return *this;
	}
	inline intrusive_list_iterator operator++( int ) const
	{
		return intrusive_list_iterator( const_cast< node_type * >(
			mNode->get_next() ) );
	}

	inline intrusive_list_iterator &operator--()
	{
		mNode = mNode->get_prev();
		return *this;
	}
	inline intrusive_list_iterator operator--( int ) const
	{
		return intrusive_list_iterator( const_cast< node_type * >(
			mNode->get_prev() ) );
	}

	inline bool operator==( const intrusive_list_iterator &x )
	{
		return mNode == x.mNode;
	}
	inline bool operator!=( const intrusive_list_iterator &x )
	{
		return mNode != x.mNode;
	}

private:
	node_type *mNode;
};

template< typename T >
class intrusive_list
{
public:
	typedef intrusive_list< T > this_type;
	typedef intrusive_list_node< T > node_type;
	typedef T value_type;
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	typedef T &reference;
	typedef const T &const_reference;
	typedef T *pointer;
	typedef const T *const_pointer;
	typedef intrusive_list_iterator< T, T *, T & > iterator;
	typedef intrusive_list_iterator< T, const T *, const T & > const_iterator;
	//typedef sdf::reverse_iterator< iterator > reverse_iterator;
	//typedef sdf::reverse_iterator< const_iterator > const_reverse_iterator;

public:
	intrusive_list(): mBase()
	{
	}
	intrusive_list( const this_type &x ): mBase()
	{
		*this = x;
	}

	this_type &operator=( const this_type &x )
	{
		clear();
		for( auto &item : x )
		{
			push_back( item );
		}

		return *this;
	}

	iterator begin()
	{
		return iterator( mHead );
	}
	const_iterator begin() const
	{
		return const_iterator( mHead );
	}
	iterator end()
	{
		return iterator( &mBase );
	}
	const_iterator end() const
	{
		return const_iterator( &mBase );
	}

	reference front()
	{
		return *mBase.get_next();
	}
	const_reference front() const
	{
		return *mBase.get_next();
	}
	reference back()
	{
		return *mBase.get_prev();
	}
	const_reference back() const
	{
		return *mBase.get_prev();
	}

	bool empty() const
	{
		return mBase.get_next() != &mBase;
	}
	size_type size() const
	{
		size_type count = 0;

		for( auto *p = mBase.get_next(); p != &mBase; p = p->get_next() )
		{
			++count;
		}

		return count;
	}
	void clear()
	{
		while( mBase.get_next() != &mBase )
		{
			remove( *mBase.get_next() );
		}
	}
	void reverse()
	{
		// TODO!
	}

	void push_front( node_type &x )
	{
		insert( begin(), x );
	}
	void pop_front()
	{
		remove( mBase.get_next() );
	}
	void push_back( node_type &x )
	{
		insert( end(), x );
	}
	void pop_back()
	{
		remove( mBase.get_prev() );
	}

	iterator locate( const node_type &x )
	{
		return iterator( &x );
	}
	const_iterator locate( const node_type &x ) const
	{
		return const_iterator( &x );
	}

	iterator insert( iterator pos, node_type &x )
	{
	}

private:
	node_type mBase;
};
