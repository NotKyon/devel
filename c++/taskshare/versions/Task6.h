//
//	FIXME: There are obvious issues here.
//

#ifndef TASK_H
#define TASK_H

#define TASK_CONF_NO_DEFAULT_TASK 1
#include "../TaskShare.h"

//------------------------------------------------------------------------------
// JOB QUEUE

#define TASK_JOBQUEUE_VERSION 6

#undef TASK_CONF_LOCKFREE
#define TASK_CONF_LOCKFREE 0

__TS_NS_INIT__

//----

typedef struct token_s {
	union { jobFunc_t fn; void *fnp; };
	void *p;
} token_t;
#undef TASK_CONF_BUFFER_SIZE
typedef struct queue_s {
	struct entry {
		token_t t;
		entry *next;
	};
	bool doterm;
	lock_t lock;
	signal_t term, done;
	semaphore_t sem;
	volatile entry *head, *tail;
} queue_t;

//----

inline void InitQueue(queue_t &q);
inline void FiniQueue(queue_t &q);

inline void EnqueueToken(queue_t &q, const token_t &t);
inline void Schedule(queue_t &q, jobFunc_t fn, void *p);
inline void ScheduleTermination(queue_t &q);

inline bool Stream(queue_t &q, token_t &t);
inline void Streamer(queue_t &q);

inline void SyncQueue(queue_t &q);
inline void SyncQueueActive(queue_t &q);

//----

inline void InitQueue(queue_t &q) {
	q.doterm = false;
	InitLock(q.lock);
	InitSignal(q.term, 0);
	InitSignal(q.done, 0);
	InitSemaphore(q.sem, 0);
	q.head = (queue_t::entry *)NULL;
	q.tail = (queue_t::entry *)NULL;
}
inline void FiniQueue(queue_t &q) {
	ScheduleTermination(q);
	FiniSemaphore(q.sem);
	FiniSignal(q.done);
	FiniSignal(q.term);
	FiniLock(q.lock);
}

inline void EnqueueToken(queue_t &q, const token_t &t) {
	queue_t::entry *p;

#ifdef unlikely
# define __TASK_unlikely_(x) (unlikely(x))
#else
# define __TASK_unlikely_(x) (x)
#endif
	if __TASK_unlikely_(!(p = new queue_t::entry()))
		return;
#undef __TASK_unlikely

	p->t = t;
	p->next = (queue_t::entry *)NULL;

	Lock(q.lock);
	if (q.tail)
		q.tail->next = p;
	else
		q.head = p;
	q.tail = p;
	Unlock(q.lock);

	AllocSignalSlot(q.done);
	SignalSemaphore(q.sem);
}
inline void Schedule(queue_t &q, jobFunc_t fn, void *p) {
	token_t t;

	t.fn = fn;
	t.p  = p ;

	EnqueueToken(q, t);
}
inline void ScheduleTermination(queue_t &q) {
	size_t i;

	q.doterm = true;
	for(i=0; i<64; i++)
		SignalSemaphore(q.sem);
	SyncSignal(q.term);
}

inline bool Stream(queue_t &q, token_t &t) {
	volatile queue_t::entry *p;

	WaitSemaphore(q.sem);
	if (q.doterm)
		return false;

	Lock(q.lock);
	p = q.head;
	if (!(q.head = p->next)) //'p' has to be valid here
		q.tail = (volatile queue_t::entry *)NULL;
	Unlock(q.lock);

	t.fn = p->t.fn;
	t.p  = p->t.p ;
	delete p;

	return true;
}
inline void Streamer(queue_t &q) {
	token_t t;

	AllocSignalSlot(q.term);

	while(Stream(q, t)) {
		t.fn(t.p);
		RaiseSignal(q.done);
	}

	RaiseSignal(q.term);
}

inline void SyncQueue(queue_t &q) {
	SyncSignal(q.done);
	q.done.expCnt = 0;
	ResetSignal(q.done);
}
inline void SyncQueueActive(queue_t &q) {
	SyncQueue(q);
}

__TS_NS_FINI__

#endif
