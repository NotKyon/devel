/*
===============================================================================

	REAL SHADING
	Based on [Karis13].

	References:
	[Karis13]: Real Shading in Unreal Engine4

===============================================================================
*/

#ifndef DIRECT3D_VERSION
# define DIRECT3D_VERSION 0x0B00
#endif
#if DIRECT3D_VERSION >= 0x0B00
# define MODERN_SYNTAX 1
#else
# define MODERN_SYNTAX 0
#endif

#ifndef PER_PIXEL_LIGHTING_ENABLED
# define PER_PIXEL_LIGHTING_ENABLED 1
#endif

#ifndef OPTIMIZATION_ENABLED
# define OPTIMIZATION_ENABLED 0
#endif

#ifndef PASS_TBN
# define PASS_TBN 1
#endif

// ==== ** OPTIMIZATION TYPEDEFS ** ==== //

#ifndef OPTIMIZATION_ENABLED
# define float_low half
# define vec2_low half2
# define vec3_low half3
# define vec4_low half4
# define mat2_low half2x2
# define mat3_low half3x3
# define mat4_low half4x4
#else
# define float_low float
# define vec2_low float2
# define vec3_low float3
# define vec4_low float4
# define mat2_low float2x2
# define mat3_low float3x3
# define mat4_low float4x4
#endif

// ==== ** PRIMARY STRUCTURES ** ==== //

struct vertex_t {
	vec3_low position : POSITION;
	vec3_low normal : NORMAL;
	vec3_low tangent : TANGENT;
	vec3_low bitangent : BINORMAL;
	vec2_low texcoord0 : TEXCOORD0;
	vec4_low color : COLOR;
};
struct fragment_t {
	vec4_low position : POSITION;
	vec4_low color : COLOR;
	vec2_low texcoord0 : TEXCOORD0;
#if PER_PIXEL_LIGHTING_ENABLED
	vec3_low viewPos : TEXCOORD1;
	vec3_low lightPos : TEXCOORD2;
# if PASS_TBN
	vec3_low tangent : TEXCOORD3;
	vec3_low bitangent : TEXCOORD4;
	vec3_low normal : TEXCOORD5;
# endif
#endif
};
struct rasterizer_t {
	vec4_low color : COLOR0;
};

#if PER_PIXEL_LIGHTING_ENABLED
texture albedo_texture <
	string ResourceName = "gearTower_albedo.dds";
	string ResourceType = "2D";
>;
sampler2D albedo_sampler = sampler_state {
	Texture = <albedo_texture>;
#if MODERN_SYNTAX
	Filter = MIN_MAG_MIP_LINEAR;
#else
	MinFilter = Linear;
	MipFilter = Linear;
	MagFilter = Linear;
#endif
	AddressU = Wrap;
	AddressV = Wrap;
};

texture local_texture <
	string ResourceName = "gearTower_local.dds";
	string ResourceType = "2D";
>;
sampler2D local_sampler = sampler_state {
	Texture = <local_texture>;
#if MODERN_SYNTAX
	Filter = MIN_MAG_MIP_LINEAR;
#else
	MinFilter = Linear;
	MipFilter = Linear;
	MagFilter = Linear;
#endif
	AddressU = Wrap;
	AddressV = Wrap;
};

texture roughness_texture <
	string ResourceName = "gearTower_roughness.dds";
	string ResourceType = "2D";
>;
sampler2D roughness_sampler = sampler_state {
	Texture = <roughness_texture>;
#if MODERN_SYNTAX
	Filter = MIN_MAG_MIP_LINEAR;
#else
	MinFilter = Linear;
	MipFilter = Linear;
	MagFilter = Linear;
#endif
	AddressU = Wrap;
	AddressV = Wrap;
};
#endif

// ==== ** UNTWEAKABLES ** ==== //

float time : Time;

mat4_low modelToWorld : World;
mat4_low modelToView : WorldView;
mat4_low modelToClip : WorldViewProjection;
mat4_low inverseTransposeModelToView : WorldViewInverseTranspose;
#if PER_PIXEL_LIGHTING_ENABLED
mat4_low worldToModelTranspose : WorldInverseTranspose;
mat4_low worldToModel : WorldInverse;
mat4_low viewToWorld : ViewInverse;
#endif

// ==== ** LIGHTING DATA ** ==== //

#define MAX_LIGHTS 1

vec4_low lightPosWS[ MAX_LIGHTS ] = { { 1, 1, 1, 0 } };
vec4_low lightColor[ MAX_LIGHTS ] = { { 1, 1, 1, 1 } };


// ==== ** DIFFUSE ALGORITHMS ** ==== //

vec3_low DiffuseBRDF( vec3_low DiffuseAlbedo ) {

	return DiffuseAlbedo/3.141592653589;

}

// ==== ** SPECULAR ALGORITHMS ** ==== //

float_low SpecularD( in vec3_low N, in vec3_low H, in float_low Roughness ) {

	const float_low a = Roughness*Roughness;
	const float_low a2 = a*a;
	const float_low NdotH = dot( N, H );
	const float_low NdotH2 = NdotH*NdotH;

	const float_low temp = NdotH2*( a2 - 1 ) + 1;

	return a2/( 3.141592653589*temp*temp );

}
float_low SpecularG1( in vec3_low N, in vec3_low V, in float_low k ) {

	const float_low NdotV = dot( N, V );

	return NdotV/( NdotV*( 1 - k ) + k );

}
float_low SpecularG( in vec3_low N, in vec3_low L, in vec3_low V, in vec3_low H, in float_low Roughness ) {

	const float_low k = ( ( Roughness + 1 )*( Roughness + 1 ) )/8;

	return SpecularG1( N, L, k )*SpecularG1( N, V, k );

}

// F0 is the specular reflectance at normal incidence.
float_low SpecularF( in vec3_low V, in vec3_low H, in float_low F0 ) {

	const float_low VdotH = dot( V, H );

	return F0 + ( 1 - F0 )*pow( 2, ( -5.55473*VdotH - 6.98316 )*VdotH );

}

float_low SpecularBRDF( in vec3_low L, in vec3_low V, in vec3_low N, in vec3_low H, in float_low Roughness ) {

	// TODO: Calculate specular reflectance at normal incidence
	//	- TODO: Is this solution correct?
	const float_low F0 = max( dot( N, H ), 0 );//0.0f; // <- here
	//const float_low F0 = 0.1; //this appears to be the default

	const float_low D = SpecularD( N, H, Roughness );
	const float_low F = SpecularF( V, H, F0 );
	const float_low G = SpecularG( N, L, V, H, Roughness );

	return ( D*F*G )/( 4*dot( N, L )*dot( N, V ) );

}


// ==== ** VERTEX PROGRAM ** ==== //

fragment_t realShading_vp( in vertex_t vertex ) {

	fragment_t result;

#if PER_PIXEL_LIGHTING_ENABLED
	result.position = mul( vec4_low( vertex.position.xyz, 1 ), modelToClip );
	result.texcoord0 = vertex.texcoord0;
	result.color = vertex.color;

	const vec3_low P = mul( vec4_low( vertex.position.xyz, 1 ), modelToView ).xyz;

# if PASS_TBN
	result.viewPos = viewToWorld[ 3 ].xyz - P;
	result.lightPos = lightPosWS[ 0 ].xyz - P;

	result.tangent = mul( vertex.tangent, ( mat3_low )worldToModelTranspose ).xyz;
	result.bitangent = mul( vertex.bitangent, ( mat3_low )worldToModelTranspose ).xyz;
	result.normal = mul( vertex.normal, ( mat3_low )worldToModelTranspose ).xyz;
# else
	const mat3_low modelToTangent = transpose( mat3_low( vertex.tangent, vertex.bitangent, vertex.normal ) );

	const vec3_low eyePosModel = mul( vec4_low( viewToWorld[ 0 ].xyz, 1 ), worldToModel );
	result.viewPos = mul( eyePosModel - P, modelToTangent );

	const vec3_low lightPosModel = mul( vec4_low( lightPosWS[ 0 ].xyz, 0 ), worldToModel );
	result.lightPos = mul( lightPosModel, modelToTangent );
# endif
#else
	result.position = mul( vec4_low( vertex.position.xyz, 1 ), modelToClip );
	result.texcoord0 = vertex.texcoord0;

	const vec3_low P = mul( vec4_low( vertex.position.xyz, 1 ), modelToView ).xyz;
	const vec3_low L = normalize( lightPosWS[ 0 ].xyz - P );
	const vec3_low V = -normalize( P );
	const vec3_low N = mul( vertex.normal, inverseTransposeModelToView );
	const vec3_low H = normalize( L + V );
	const float Roughness = 0.3;
	
	const vec3_low diffuse = DiffuseBRDF( vertex.color );
	const float_low specular = SpecularBRDF( L, V, N, H, Roughness );
	result.color = vec4_low( diffuse + specular.xxx, 1 );
#endif
	
	return result;
	
}


// ==== ** FRAGMENT PROGRAM ** ==== //

rasterizer_t realShading_fp( in fragment_t fragment ) {

	rasterizer_t result;

#if PER_PIXEL_LIGHTING_ENABLED
	const vec3_low localBump = tex2D( local_sampler, fragment.texcoord0 ).xyz*2.0 - 1.0;
	const vec3_low P = normalize( fragment.viewPos );
	const vec3_low L = normalize( fragment.lightPos );
	const vec3_low V = -normalize( P );
# if PASS_TBN
	const vec3_low N = normalize( fragment.normal*localBump.z + fragment.tangent*localBump.x - fragment.bitangent*localBump.y );
# else
	const vec3_low N = normalize( localBump );
# endif
	const vec3_low H = normalize( L + V );
	const float Roughness = tex2D( roughness_sampler, fragment.texcoord0 ).r;
	
	const vec3_low diffuse = DiffuseBRDF( tex2D( albedo_sampler, fragment.texcoord0 ).xyz );
	const float_low specular = SpecularBRDF( L, V, N, H, Roughness );
	result.color = vec4_low( diffuse + specular.xxx, 1 );
#else
	result.color = fragment.color;
#endif

	return result;

}


// ==== ** RENDER PROGRAM ** ==== //

#if MODERN_SYNTAX
DepthStencilState DepthEnabled { DepthEnable = TRUE; };

technique11 realShading11 {

	pass realShading {

		SetVertexShader( CompileShader( vs_5_0, realShading_vp() ) );
		SetHullShader( NULL );
		SetDomainShader( NULL );
		SetGeometryShader( NULL );
		SetPixelShader( CompileShader( ps_5_0, realShading_fp() ) );

		SetDepthStencilState( DepthEnabled, 0 );

	}

}
#endif

technique realShading {

	pass realShading {
	
		VertexShader = compile vs_3_0 realShading_vp();
		PixelShader = compile ps_3_0 realShading_fp();
	
	}

}
