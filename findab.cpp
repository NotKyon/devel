#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

enum OPCODE
{
	U_NEGATE,
	B_ADD,
	B_SUB,
	B_MUL,
	B_DIV,
	B_MOD,
	F_FLOOR,
	F_CEIL,
	F_STORE_A,
	F_STORE_B,
	F_PUSH_A,
	F_PUSH_B,
	F_PUSH_SUM,
	F_PUSH_PRODUCT,

	CONSTANT
};
static const double constants[] = {
	-8.0, -7.0, -6.0, -5.0, -4.0, -3.5, -3.14159265358979, -2.5, -2.0, -1.5, -1.0, -0.5,
	0.0,
	0.5, 1.0, 1.5, 2.0, 2.5, 3.14159265358979, 3.5, 4.0, 5.0, 6.0, 7.0, 8.0,
	
	sqrt( 2.0 ), sqrt( 3.0 ), sqrt( 3.14159265358979 )
};
static size_t numconstants = sizeof( constants )/sizeof( constants[ 0 ] );
static unsigned int highop = CONSTANT + ( unsigned int )numconstants;

template< size_t N >
bool Pop( double( &stack )[ N ], size_t &stackp, double &value )
{
	if( !stackp ) {
		return false;
	}

	value = stack[ --stackp ];
	return true;
}
template< size_t N >
bool Push( double( &stack )[ N ], size_t &stackp, double value )
{
	if( stackp == N - 1 ) {
		return false;
	}

	stack[ stackp++ ] = value;
	return true;
}
bool ApxEq( double x, double y )
{
	return fabs( x - y ) < 0.0001;
}
bool RunProgram( const unsigned char *program, size_t programsize, double product, double sum, double known_a, double known_b )
{
	double stack[ 512 ];
	size_t stackp = 0;
	
	double a = 0.0;
	double b = 0.0;
	
	double x = 0.0, y = 0.0;
	
	for( size_t i = 0; i < programsize; ++i ) {
		OPCODE opcode = ( OPCODE )program[ i ];
		switch( opcode )
		{
		case U_NEGATE:
			if( !Pop( stack, stackp, y ) ) { return false; }
			if( !Push( stack, stackp, -y ) ) { return false; }
			break;
		case B_ADD:
			if( !Pop( stack, stackp, y ) ) { return false; }
			if( !Pop( stack, stackp, x ) ) { return false; }
			if( !Push( stack, stackp, x + y ) ) { return false; }
			break;
		case B_SUB:
			if( !Pop( stack, stackp, y ) ) { return false; }
			if( !Pop( stack, stackp, x ) ) { return false; }
			if( !Push( stack, stackp, x - y ) ) { return false; }
			break;
		case B_MUL:
			if( !Pop( stack, stackp, y ) ) { return false; }
			if( !Pop( stack, stackp, x ) ) { return false; }
			if( !Push( stack, stackp, x*y ) ) { return false; }
			break;
		case B_DIV:
			if( !Pop( stack, stackp, y ) ) { return false; }
			if( !Pop( stack, stackp, x ) ) { return false; }
			if( !Push( stack, stackp, x/y ) ) { return false; }
			break;
		case B_MOD:
			if( !Pop( stack, stackp, y ) ) { return false; }
			if( !Pop( stack, stackp, x ) ) { return false; }
			if( !Push( stack, stackp, fmod( x, y ) ) ) { return false; }
			break;
		case F_FLOOR:
			if( !Pop( stack, stackp, y ) ) { return false; }
			if( !Push( stack, stackp, floor( y ) ) ) { return false; }
			break;
		case F_CEIL:
			if( !Pop( stack, stackp, y ) ) { return false; }
			if( !Push( stack, stackp, ceil( y ) ) ) { return false; }
			break;
		case F_STORE_A:
			if( !Pop( stack, stackp, a ) ) { return false; }
			break;
		case F_STORE_B:
			if( !Pop( stack, stackp, b ) ) { return false; }
			break;
		case F_PUSH_A:
			if( !Push( stack, stackp, a ) ) { return false; }
			break;
		case F_PUSH_B:
			if( !Push( stack, stackp, b ) ) { return false; }
			break;
		case F_PUSH_SUM:
			if( !Push( stack, stackp, sum ) ) { return false; }
			break;
		case F_PUSH_PRODUCT:
			if( !Push( stack, stackp, product ) ) { return false; }
			break;
		default:
			{
				const size_t n = ( size_t )( opcode - CONSTANT );
				if( n >= numconstants ) {
					return false;
				}
				
				if( !Push( stack, stackp, constants[ n ] ) ) {
					return false;
				}
			}
			break;
		}
	}
	
	return ( ApxEq( a, known_a ) && ApxEq( b, known_b ) ) || ( ApxEq( a, known_b ) && ApxEq( b, known_a ) );
}

template< size_t N >
size_t GenerateProgram( unsigned char( &program )[ N ], uint64_t seed )
{
	uint64_t i = 0;
	
	bool storesA = false, storesB = false;

	uint64_t n = seed;
	while( n > 0 && i < N ) {
		program[ i ] = ( uint8_t )( n%highop );
		storesA |= program[ i ] == F_STORE_A;
		storesB |= program[ i ] == F_STORE_B;
		n /= highop;
		++i;
	}
	
	if( !storesA && i < N ) {
		program[ i++ ] = F_STORE_A;
	}
	if( !storesB && i < N ) {
		program[ i++ ] = F_STORE_B;
	}
	
	return i;
}
void PrintProgram( const unsigned char *program, size_t programlen )
{
	for( size_t i = 0; i < programlen; ++i ) {
		OPCODE opcode = ( OPCODE )program[ i ];
		switch( opcode )
		{
		case U_NEGATE:
			printf( "U_NEGATE\n" );
			break;
		case B_ADD:
			printf( "B_ADD\n" );
			break;
		case B_SUB:
			printf( "B_SUB\n" );
			break;
		case B_MUL:
			printf( "B_MUL\n" );
			break;
		case B_DIV:
			printf( "B_DIV\n" );
			break;
		case B_MOD:
			printf( "B_MOD\n" );
			break;
		case F_FLOOR:
			printf( "F_FLOOR\n" );
			break;
		case F_CEIL:
			printf( "F_CEIL\n" );
			break;
		case F_STORE_A:
			printf( "F_STORE_A\n" );
			break;
		case F_STORE_B:
			printf( "F_STORE_B\n" );
			break;
		case F_PUSH_A:
			printf( "F_PUSH_A\n" );
			break;
		case F_PUSH_B:
			printf( "F_PUSH_B\n" );
			break;
		case F_PUSH_SUM:
			printf( "F_PUSH_SUM\n" );
			break;
		case F_PUSH_PRODUCT:
			printf( "F_PUSH_PRODUCT\n" );
			break;
		default:
			{
				const size_t n = ( size_t )( opcode - CONSTANT );
				if( n >= numconstants ) {
					printf( "(INVALID)\n" );
					break;
				}
				
				printf( "CONSTANT %g\n", constants[ n ] );
			}
			break;
		}
	}
}

struct TEST_CASE
{
	double a;
	double b;
};
static const TEST_CASE testcases[] = {
	{ 0.25, 16.0 },
	{ 0.4705, 8.5 },
	{ 0.352654, 11.3426 },
	{ 0.458832, 8.7178 }
};
static size_t numtestcases = sizeof( testcases )/sizeof( testcases[ 0 ] );

bool Test( uint64_t seed )
{
	unsigned char program[ 48 ];
	size_t programlen = GenerateProgram( program, seed );
	if( !programlen ) {
		return false;
	}

	size_t numsuccesses = 0;
	bool success = true;
	bool printprog = false;
	for( size_t i = 0; i < numtestcases; ++i ) {
		const double a = testcases[ i ].a;
		const double b = testcases[ i ].b;
		const double product = a*b;
		const double sum = a + b;

		if( !RunProgram( program, programlen, product, sum, a, b ) ) {
			success = false;
			continue;
		}

		// For debugging purposes, show anything that gets the first few right
		printf( "Program (len=%u) solved a=%g, b=%g, product=%g, sum=%g\n",
			( unsigned int )programlen, a, b, product, sum );
		printprog = true;
		++numsuccesses;
	}

	if( success ) {
		printf( "Program solved all test cases\n" );
	}

	if( printprog && numsuccesses > 1 ) {
		PrintProgram( program, programlen );
	}

	return success;
}

int main( int argc, char **argv )
{
	uint64_t base = 5000000000;
	uint64_t count = 8000000000;

	( void )argc;
	( void )argv;

	for( uint64_t i = base; i < count; ++i ) {
		if( Test( i ) ) {
			return EXIT_SUCCESS;
		}
	}

	return EXIT_FAILURE;
}
