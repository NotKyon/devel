#ifndef XS_STRING_H
#define XS_STRING_H

#include "basetypes.h"

/*
 * Make a copy of the string specified.
 *
 * >source (const char *): Handle to the existing string whose contents will be
 *                         used for the new string. If NULL then NULL is
 *                         returned.
 * <returns (char *): Handle to the new string (allocated with XsMemory()).
 */
char *XsDuplicate(const char *source);

/*
 * Make a copy of the string specified, reusing a specific memory location.
 *
 * >p (char *): Handle to the string whose memory will be reused. If NULL, this
 *              function will invoke XsDuplicate().
 * >source (const char *): Handle to the existing string whose contents will be
 *                         used for the new string. If NULL then the existing
 *                         string will be deallocated.
 * <returns (char *): Handle to the new string (reallocated with XsMemory()).
 */
char *XsReduplicate(char *p, const char *source);

/*
 * Append the contents of one string to another, dynamically.
 *
 * >p (char *): Handle to the string to be appended to and reallocated.
 * >source (const char *): Handle to the existing string whose contents will be
 *                         appended to the new string. If NULL then 'p' is
 *                         returned untouched.
 * <returns (char *): Handle to the new string (reallocated with XsMemory()).
 */
char *XsAppend(char *p, const char *source);

/*
 * Securely copy one string into another.
 *
 * >dst (char *): Destination string buffer.
 * >dstn (xs_size_t): Size of the destination string buffer, in bytes. This
 *                    includes enough space for a NULL terminator.
 * >src (const char *): NULL-terminated source string.
 * <returns (char *): Destination string if the copy was successful, NULL
 *                    otherwise. A warning will be logged if the string was
 *                    clipped.
 */
char *XsStrCpy(char *dst, xs_size_t dstn, const char *src);

/*
 * Securely copy one portion of a string into another.
 *
 * >dst (char *): Destination string buffer.
 * >dstn (xs_size_t): Size of the destination string buffer, in bytes. This
 *                    includes enough space for a NULL terminator.
 * >src (const char *): Source string.
 * >srcn (xs_size_t): Number of characters from the source string to copy. If
 *                    0, this function will function like XsStrCpy. If a NULL
 *                    terminator is encountered, no further copying will occur.
 * <return (char *): Destination string if the copy was successful, NULL
 *                   otherwise. A warning will be logged if the string was
 *                   clipped.
 */
char *XsStrCpyN(char *dst, xs_size_t dstn, const char *src, xs_size_t srcn);

/*
 * TODO: Document these
 */
char *XsStrCat(char *dst, xs_size_t dstn, const char *src);
char *XsStrCatN(char *dst, xs_size_t dstn, const char *src, xs_size_t srcn);

#endif
