float Script : StandardsGlobal <
	string ScriptClass = "scene";
	string ScriptOrder = "postprocess";
	string ScriptOutput = "color";
	//string Script = "Technique=SSAO?SSAO:SSAO;";
> = 0.8;

struct PREPVERTEX {
	float3 PosL    : POSITION ;
	float3 NrmL    : NORMAL   ;
};
struct PREPFRAGMENT {
	float4 PosH    : POSITION ;
	float4 CpPosH  : TEXCOORD0;
	float3 NrmL    : TEXCOORD1;
};

struct VERTEX {
	float3 PosL    : POSITION ;
	float2 TexT    : TEXCOORD0;
};
struct FRAGMENT {
	float4 PosH    : POSITION ;
	float2 TexT    : TEXCOORD0;
	float3 ViewDir : TEXCOORD1;
};

float4x4 gProj : Projection;
float4x4 gWVP  : WorldViewProjection;
float4x4 gWV   : WorldView;

float4 gClearColor = {0.0f,0.0f,0.0f,1.0f};
float  gClearDepth = 1.0f;

float gSampleRadius   = 8.0f;
float gDistanceScale  = 0.09f;
float gIntensity      = 0.5f;

float2 gResolution : ViewportPixelSize;

float3 gCornerFrustum;

texture gRndNrmTex : RandomNoise <
	string ResourceName = "noise.png";
	string ResourceType = "2D";
>;
sampler2D gRndNrmSmp:register(s0) = sampler_state {
	Texture = <gRndNrmTex>;
	AddressU = Wrap;
	AddressV = Wrap;
	MagFilter = Linear;
	MinFilter = Linear;
};

texture gDepthTex : RenderColorTarget <
	float2 ViewPortRatio = {1.0f,1.0f};
	int MipLevels = 1;
	string Format = "A16B16G16R16";
>;
sampler2D gDepthSmp:register(s1) = sampler_state {
	Texture = <gDepthTex>;
	AddressU = Clamp;
	AddressV = Clamp;
	MagFilter = Linear;
	MinFilter = Linear;
	MipFilter = Point;
};

PREPFRAGMENT VSPrep(PREPVERTEX I) {
	PREPFRAGMENT O;

	O.PosH   = mul(float4(I.PosL.xyz, 1.0f), gWVP);
	O.CpPosH = O.PosH;
	O.NrmL   = mul(float4(I.NrmL.xyz, 0.0f), gWV );

	return O;
}
float4 PSPrep(PREPFRAGMENT I): COLOR0 {
	I.NrmL = (normalize(I.NrmL) + 1.0f)/2.0f;

	return float4(I.NrmL, I.CpPosH.z);
}

FRAGMENT VSMain(VERTEX I) {
	FRAGMENT O;
	float2   S;

	O.PosH = float4(I.PosL, 1.0f);
	O.TexT = I.TexT;

	S = sign(I.PosL.xy);

	O.ViewDir = float3(-gCornerFrustum.x*S.x,
						gCornerFrustum.y*S.y,
						gCornerFrustum.z);

	return O;
}
float4 PSMain(FRAGMENT I): COLOR0 {
	static const float4 samples[16] = {
		float4( 0.355512 , -0.709318 , -0.102371 , 0.0 ),
		float4( 0.534186 ,  0.71511  , -0.115167 , 0.0 ),
		float4(-0.87866  ,  0.157139 , -0.115167 , 0.0 ),
		float4( 0.140679 , -0.475516 , -0.0639818, 0.0 ),
		float4(-0.0796121,  0.158842 , -0.677075 , 0.0 ),
		float4(-0.0759516, -0.101676 , -0.483625 , 0.0 ),
		float4( 0.12493  , -0.0223423, -0.483625 , 0.0 ),
		float4(-0.0720074,  0.243395 , -0.967251 , 0.0 ),
		float4(-0.207641 ,  0.414286 ,  0.187755 , 0.0 ),
		float4(-0.277332 , -0.371262 ,  0.187755 , 0.0 ),
		float4( 0.63864  , -0.114214 ,  0.262857 , 0.0 ),
		float4(-0.184051 ,  0.622119 ,  0.262857 , 0.0 ),
		float4( 0.110007 , -0.219486 ,  0.435574 , 0.0 ),
		float4( 0.235085 ,  0.314707 ,  0.696918 , 0.0 ),
		float4(-0.290012 ,  0.0518654,  0.522688 , 0.0 ),
		float4( 0.0975089, -0.329594 ,  0.609803 , 0.0 )
	};
	float4 depthNormal;
	float3 se, randNormal;
	float  finalColor;
	int    i;

	I.TexT.xy += 1.0f/gResolution.xy;

	I.ViewDir = normalize(I.ViewDir);

	depthNormal = tex2D(gDepthSmp, I.TexT); //rgb=normal, a=depth
	depthNormal.xyz = normalize(2.0f*depthNormal.xyz - 1.0f);
	se = depthNormal.a*I.ViewDir;

	randNormal = normalize(2.0f*tex2D(gRndNrmSmp, I.TexT*200.0f).xyz - 1.0f);

	finalColor = 0.0f;
	for(i=0; i<16; i++) {
		float4 sample, ss;
		float3 ray;
		float2 sampleTexCoord;
		float  sampleDepth, occlusion;

		ray = reflect(samples[i].xyz, randNormal)*gSampleRadius;

		//if (dot(ray, depthNormal.xyz) < 0)
		//	ray += depthNormal.xyz*gSampleRadius;
		//
		// --or-- (branchless alternative)
		// ray += saturate(sign(-dot(ray, depthNormal.xyz)))*depthNormal.xyz*
		//        gSampleRadius;

		sample = float4(se + ray, 1.0f);
		ss = mul(sample, gProj);

		sampleTexCoord =	0.5f*ss.xy/ss.w + float2(0.5f, 0.5f) +
							1.0f/gResolution;

		sampleDepth = tex2D(gDepthSmp, sampleTexCoord).a;

		if (sampleDepth == 1.0f)
			finalColor++;
		else {
			occlusion = gDistanceScale*saturate(sampleDepth - depthNormal.a);
			finalColor += 1.0f/(1.0f + occlusion*occlusion*0.1f);
		}
	}

	//uncomment this to see the normals
	//return float4(tex2D(gDepthSmp, I.TexT).rgb, 1.0f);
	
	//uncomment this to see the depth
	//return float4(tex2D(gDepthSmp, I.TexT).aaa, 1.0f);

	//uncomment to see the random noise texture
	//return tex2D(gRndNrmSmp, I.TexT);

	finalColor = 1 - (finalColor/16.0f)*gIntensity;

	return float4(finalColor, finalColor, finalColor, 1.0f);
}

technique SSAO <
	string Script =
		"Pass=Prep;"
		"Pass=Main;"
	;
> {
	pass Prep <
		string Script =
			"RenderColorTarget0=gDepthTex;"
			"RenderDepthStencilTarget=;"
			"ClearSetColor=gClearColor;"
			"ClearSetDepth=gClearDepth;"
			"Clear=Color0;"
			"Clear=Depth;"
			"Draw=Geometry;"
		;
	> {
		VertexShader = compile vs_3_0 VSPrep();
		ZEnable = true;
		ZWriteEnable = true;
		ZFunc = LessEqual;
		AlphaBlendEnable = false;
		CullMode = None;
		PixelShader  = compile ps_3_0 PSPrep();
	}

	pass Main <
		string Script =
			"RenderColorTarget0=;"
			"RenderDepthStencilTarget=;"
			"ClearSetColor=gClearColor;"
			"ClearSetDepth=gClearDepth;"
			"Clear=Color;"
			"Clear=Depth;"
			"Draw=Buffer;"
		;
	> {
		VertexShader = compile vs_3_0 VSMain();
		ZEnable = false;
		ZWriteEnable = false;
		ZFunc = LessEqual;
		AlphaBlendEnable = false;
		CullMode = None;
		PixelShader  = compile ps_3_0 PSMain();
	}
}
