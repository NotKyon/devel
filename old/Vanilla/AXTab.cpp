// includes
#include <Axia/Vanilla/Vanilla.h>
#include <Axia/Vanilla/Internal.h>

// AXTab: Constructor
AXTab::AXTab()
: AXGadget(AXTabGadgetType), m_tabIndex(0)
{
}

// AXTab: Destructor
AXTab::~AXTab()
{
}

// AXTab: ShowContents: show the contents of this tab
void AXTab::Show(void)
{
	// declarations
	AXGadget	*pGadget;
	
	// validity checks
	if (!m_pSuper)
		return;
	if (m_pSuper->Type()!=AXTabberGadgetType)
		return;
	
	// enumerate each sub-gadget
	for(pGadget=m_subList.First(); pGadget; pGadget=pGadget->m_subLink.After())
		// show the sub-gadget
		pGadget->Show();
}

// AXTab: HideContents: hide the contents of this tab
void AXTab::Hide(void)
{
	// declarations
	AXGadget	*pGadget;

	// validity checks
	if (!m_pSuper)
		return;
	if (m_pSuper->Type()!=AXTabberGadgetType)
		return;
	
	// enumerate each sub-gadget
	for(pGadget=m_subList.First(); pGadget; pGadget=pGadget->m_subLink.After())
		// hide the sub-gadget
		pGadget->Hide();
}

// AXTab: SetTitle: set the title of the tab
void AXTab::SetTitle(const char *pszTitle)
{
	// declarations
	TCITEMA	tci;
	HWND	hwndRef;
	
	// validity checks
	if (!m_pSuper)
		return;
	if (m_pSuper->Type()!=AXTabberGadgetType)
		return;
	
	// store the handle of the tabber
	hwndRef = ((AXWidget *)m_pSuper)->WindowRef();
	
	// change the title of this tab
	tci.mask = TCIF_TEXT;
	tci.pszText = (char *)pszTitle;
	TabCtrl_SetItem(hwndRef, m_tabIndex, &tci);
}

// AXTab: SetTip: set the tip of the tab
void AXTab::SetTip(const char *pszTip)
{
	// validity checks
	if (!m_pSuper)
		return;
	if (m_pSuper->Type()!=AXTabberGadgetType)
		return;
	
	// TODO: TabCtrl_SetToolTips takes a window handle parameter
	// --need to analyze
}

// AXTab: QueryClientArea: retrieve client area information
void AXTab::QueryClientArea(int *pLeft, int *pTop, int *pRight, int *pBottom)
{
	// validity checks
	if (!m_pSuper)
		return;
	if (m_pSuper->Type()!=AXTabberGadgetType)
		return;
	
	// get the client area of the tabber (tabs don't take space)
	((AXTabber *)m_pSuper)->QueryClientArea(pLeft, pTop, pRight, pBottom);
}
