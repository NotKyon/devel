#ifndef XS_ARRAY_H
#define XS_ARRAY_H

#include "basetypes.h"

typedef struct xs_array_s *xs_array_t;

/*
 * TODO: Document these functions
 */

xs_array_t XsNewArray();
xs_size_t XsArrayCapacity(xs_array_t arr);
xs_size_t XsArraySize(xs_array_t arr);
char **XsArrayData(xs_array_t arr);
char *XsArrayElement(xs_array_t arr, xs_size_t i);
void XsSetArrayElement(xs_array_t arr, xs_size_t i, const char *cstr);
void XsClearArray(xs_array_t arr);
void XsDeleteArray(xs_array_t arr);
void XsDeleteAllArrays();
void XsResizeArray(xs_array_t arr, xs_size_t n);
xs_size_t XsArrayPush(xs_array_t arr, const char *cstr);
void XsRemoveArrayDups(xs_array_t arr);
void XsExplode(xs_array_t arr, const char *str);
char *XsImplode(xs_array_t arr);

#endif
