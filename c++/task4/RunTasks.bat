@echo off

cls

echo.
echo ==================================================
echo   About to run tests... This may take some time.
echo.
echo    PLEASE SHUT DOWN ALL NON-ESSENTIAL APPS NOW.
echo ==================================================
echo.
pause

echo.
echo No seriously, please shut down all apps that aren't
echo important if you haven't done so already. Thank you.
echo.
pause

echo NUMBER_OF_PROCESSORS=%NUMBER_OF_PROCESSORS% >>info.txt
echo PROCESSOR_ARCHITECTURE=%PROCESSOR_ARCHITECTURE% >>info.txt
echo PROCESSOR_IDENTIFIER=%PROCESSOR_IDENTIFIER% >>info.txt
echo PROCESSOR_LEVEL=%PROCESSOR_LEVEL% >>info.txt
echo PROCESSOR_REVISION=%PROCESSOR_REVISION% >>info.txt

echo.
echo -------
echo Phase 1
echo -------
echo Running Task1 tests...
echo P1 Task1 Results (-O3 -fomit-frame-pointer -fno-strict-aliasing -march=pentium)>>info.txt
Task1.exe --info --dont-run>>info.txt
Task1.exe>>info.txt && Task1.exe>>info.txt && Task1.exe>>info.txt && Task1.exe>>info.txt && Task1.exe>>info.txt && Task1.exe>>info.txt
echo.>>info.txt

echo Running Task2 tests...
echo P1 Task2 Results (-O3 -fomit-frame-pointer -fno-strict-aliasing -msse3 -march=core2)>>info.txt
Task2.exe --info --dont-run>>info.txt
Task2.exe>>info.txt && Task2.exe>>info.txt && Task2.exe>>info.txt && Task2.exe>>info.txt && Task2.exe>>info.txt && Task2.exe>>info.txt
echo.>>info.txt

echo Running Task3 tests...
echo P1 Task3 Results (-march=pentium) >>info.txt
Task3.exe --info --dont-run>>info.txt
Task3.exe>>info.txt && Task3.exe>>info.txt && Task3.exe>>info.txt && Task3.exe>>info.txt && Task3.exe>>info.txt && Task3.exe>>info.txt
echo.>>info.txt

echo.
echo -------
echo Phase 2
echo -------
echo Running Task1 tests...
echo P2 Task1 Results (-O3 -fomit-frame-pointer -fno-strict-aliasing -march=pentium)>>info.txt
Task1.exe --just-cores --info --dont-run>>info.txt
Task1.exe --just-cores>>info.txt && Task1.exe --just-cores>>info.txt && Task1.exe --just-cores>>info.txt && Task1.exe --just-cores>>info.txt && Task1.exe --just-cores>>info.txt && Task1.exe --just-cores>>info.txt
echo.>>info.txt

echo Running Task2 tests...
echo P2 Task2 Results (-O3 -fomit-frame-pointer -fno-strict-aliasing -msse3 -march=core2)>>info.txt
Task2.exe --just-cores --info --dont-run>>info.txt
Task2.exe --just-cores>>info.txt && Task2.exe --just-cores>>info.txt && Task2.exe --just-cores>>info.txt && Task2.exe --just-cores>>info.txt && Task2.exe --just-cores>>info.txt && Task2.exe --just-cores>>info.txt
echo.>>info.txt

echo Running Task3 tests...
echo P2 Task3 Results (-march=pentium) >>info.txt
Task3.exe --just-cores --info --dont-run>>info.txt
Task3.exe --just-cores>>info.txt && Task3.exe --just-cores>>info.txt && Task3.exe --just-cores>>info.txt && Task3.exe --just-cores>>info.txt && Task3.exe --just-cores>>info.txt && Task3.exe --just-cores>>info.txt
echo.>>info.txt

echo.
echo.

echo Done running tests!
pause
