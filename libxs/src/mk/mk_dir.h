#ifndef MK_DIR_H
#define MK_DIR_H

#include "mk_types.h"

#include "mk_project.h"

const char *SystemSpecialDir(int sys);
const char *ArchitectureSpecialDir(int arch);
const char *ProjectTypeSpecialDir(int type);

int IsSpecialDir(project_t proj, const char *name);

void EnumSpecialLibDirs(project_t proj);
void EnumSources(project_t proj, const char *srcdir);

int CalcProjectName(project_t proj, const char *path, const char *file);
project_t AddProject(project_t prnt, const char *path, const char *file,
	int type);

void FindProjects(project_t prnt, const char *srcdir);
void FindRootDirs(xs_array_t srcdirs, xs_array_t incdirs, xs_array_t libdirs);

int MakeObjDirs(project_t proj);

#endif
