﻿#include <stdio.h>
#include <stdlib.h>
#include <math.h>

namespace Mathf
{

	static const float Pi      = 3.141592653589793238462643383279502884197169f;
	static const float Rad2Deg = 1.0f/Pi*180.0f;
	static const float Deg2Rad = 1.0f/180.0f*Pi;

	inline float Cos( float x ) { return cos( x*Deg2Rad ); }
	inline float Sin( float x ) { return sin( x*Deg2Rad ); }
	inline float Tan( float x ) { return tan( x*Deg2Rad ); }
	inline float Acos( float x ) { return acos( x )*Rad2Deg; }
	inline float Asin( float x ) { return asin( x )*Rad2Deg; }
	inline float Atan( float x ) { return atan( x )*Rad2Deg; }
	inline float Atan2( float y, float x ) { return atan2( y, x )*Rad2Deg; }

}

void TestAtan2( float x, float y )
{
	printf( "(%.4f, %.4f) -> %.2f\n", x, y, Mathf::Atan2( y, x ) );
}

int main()
{
	TestAtan2(  0,  1 );
	TestAtan2(  1,  1 );
	TestAtan2(  1,  0 );
	TestAtan2(  1, -1 );
	TestAtan2(  0, -1 );
	TestAtan2( -1, -1 );
	TestAtan2( -1,  0 );
	TestAtan2( -1,  1 );

	return EXIT_SUCCESS;
}
