#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

class API {
public:
					API() {}
	virtual			~API() {}

	virtual int		GetSum(int a, int b) = 0;
};

class API_local: public virtual API {
public:
					API_local() {}
	virtual			~API_local() {}

	virtual int		GetSum(int a, int b) { return a + b; }
};

API *				api = NULL;

int main() {
	API_local apiLocal;

	api = &apiLocal;

	printf( "%i\n", api->GetSum( 2, 2 ) );
	return EXIT_SUCCESS;
}
