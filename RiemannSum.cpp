#include <stdio.h>
#include <stdlib.h>
#include <math.h>

template< class Functor >
double RiemannSum( double a, double b, const Functor &f, unsigned int res )
{
	const double delta = ( b - a )/double( res );

	double sum = 0.0;
	for( unsigned int i = 0; i < res; ++i )
	{
		const double funcResult = f( a + double( i )*delta );
		sum += funcResult*delta;
	}

	return sum;
}

class BasicSqrt
{
public:
	inline BasicSqrt()
	{
	}
	inline double operator()( double x ) const
	{
		return sqrt( x );
	}
};

int main()
{
	// the correct value is 2/3 (0.666666...)
	printf( "Integral(0, 1) of squareRoot(x) using 512 samples = %g\n",
		RiemannSum( 0.0, 1.0, BasicSqrt(), 512 ) );
	return EXIT_SUCCESS;
}
