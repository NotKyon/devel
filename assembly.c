#include <stdio.h>

extern int get_sum(int x, int y);
extern void *x86_call(void(*fn)(), size_t n, void **p);

int average(int a, int b, int c) {
  printf("average(%i, %i, %i)\n", a, b, c);fflush(stdout);
  return (a + b + c)/3;
}

int main() {
	union { void *p; int i; } r1, r2, a[3];
	int x, y;

	x = 2;
	y = 3;

	printf("%i + %i = %i\n", x, y, get_sum(x, y));
	fflush(stdout);

	r1.p = (void *)0;
	r2.p = (void *)0;

	a[0].p = (void *)0;
	a[1].p = (void *)0;
	a[2].p = (void *)0;

	a[0].i = 3;
	a[1].i = 4;
	a[2].i = 5;

	printf("First call...\n");fflush(stdout);
	r1.p = x86_call((void(*)())get_sum, 2, &a[0].p);
	printf("Result: %i\n", r1.i);fflush(stdout);

	printf("Second call...\n");fflush(stdout);
	r2.p = x86_call((void(*)())average, 3, &a[0].p);
	printf("Result: %i\n", r2.i);fflush(stdout);

	return 0;
}
