struct VertexInput {
	float3 vPos : POSITION;
};
struct FragmentInput {
	float4 vPos : POSITION;
	float4 vColor : TEXCOORD0;
};

float4x4 gWvp : WORLDVIEWPROJECTION;

FragmentInput VS( VertexInput I ) {
	FragmentInput O;
	
	O.vPos = mul( float4( I.vPos.xyz, 1 ), gWvp );
	O.vColor = float4( I.vPos.xyz, 1 );
	
	return O;
}
float4 PS( in FragmentInput I, uniform float4 color ): COLOR {
	return I.vColor + float4( 0.25, 0, 0.25, 1 )*color;
}

technique X {
	pass Y {
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS( float4( 1, 1, 1, 1 ) );
	}
	pass Z {
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS( float4( 0, 0, 0, 0 ) );
	}
}

