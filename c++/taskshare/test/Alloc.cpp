//
//	NOTE: This file should be included into the main source. NOT compiled
//	      separately.
//

//------------------------------------------------------------------------------
// TASK IMPLEMENTATIONS

void *task::Alloc(size_t n) {
	void *p;

	if (!n)
		return (void *)0;

	p = malloc(n);
	if (!p) {
		perror("Failed to allocate memory");
		exit(EXIT_FAILURE);
	}

	return p;
}
void *task::Realloc(void *p, size_t n) {
	void *q;

	if (!p)
		return Alloc(n);

	if (!n)
		return Free(p);

	q = realloc(p, n);
	if (!q) {
		perror("Failed to reallocate memory");
		exit(EXIT_FAILURE);
	}

	return q;
}
void *task::Free(void *p) {
	if (!p)
		return (void *)0;

	free(p);
	return (void *)0;
}
