#include <stdio.h>
#include <stdlib.h>
#include "DLLTest_SharedData.h"

extern __declspec(dllimport) int LoliLoliHunter( void );

static SharedData g_Shared;
__declspec(dllexport) SharedData *const g_pShared = &g_Shared;

int main()
{
	g_Shared.LoliLoliHunter = 1;
	printf( "Loli-Loli Hunter? %i\n", LoliLoliHunter() );
	return EXIT_SUCCESS;
}
