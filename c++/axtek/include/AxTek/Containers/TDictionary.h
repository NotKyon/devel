#ifndef AXTEK_CONTAINERS_TDICTIONARY_H
#define AXTEK_CONTAINERS_TDICTIONARY_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

__AX_LIB_NS_INIT__

template<typename T> class TDictionary {
public:
	struct SEntry {
	friend class TDictionary<T>;
	public:
		T *p;
		SEntry *entries;

		inline SEntry(): p((T *)0), entries((SEntry *)0) {}
		inline ~SEntry() {
			delete [] entries; entries = (SEntry *)0;
			//delete p; p = (T *)0;
		}
	};

	TDictionary(const char *allowed);
	~TDictionary();

	SEntry *Find(const char *key);
	SEntry *Lookup(const char *key);

protected:
	u8 m_convmap[256];

	u8 m_numEntries;
	SEntry *m_entries;

	u8 GenerateConvmap(const char *allowed);
	SEntry *FindFromEntry(SEntry *&entries, const char *str, bool create);
};

template<typename T>
u8 TDictionary<T>::GenerateConvmap(const char *allowed) {
	u8 i, j, k;

	for(i=0; i<255; i++)
		m_convmap[i] = 0xFF;

	for(i=0; allowed[i] && i<255; i++) {
		k = ((u8 *)allowed)[i];
		if (k >= 255)
			return 0;

		m_convmap[k] = i;
	}

	for(j=0; j<i; j++) {
		for(k=j + 1; k<i; k++) {
			if (allowed[j]==allowed[k])
				return 0; /*duplicate entries*/
		}
	}

	return i;
}

template<typename T>
typename TDictionary<T>::SEntry *TDictionary<T>::FindFromEntry
(SEntry *&entries, const char *str, bool create) {
	u8 i;

	AX_ASSERT(m_entries != (SEntry *)0);
	AX_ASSERT(entries != (SEntry *)0);
	AX_ASSERT(str != (const char *)0);

	i = m_convmap[*(u8 *)str++];
	if (i==0xFF)
		return (SEntry *)0;

	if (*str) {
		if (!entries[i].entries) {
			if (create)
				entries[i].entries = new SEntry[m_numEntries]();

			if (!entries[i].entries)
				return (SEntry *)0;
		}

		return FindFromEntry(entries[i].entries, str, create);
	}

	return &entries[i];
}

template<typename T>
inline TDictionary<T>::TDictionary(const char *allowed) {
	AX_ASSERT(allowed != (const char *)0);

	m_numEntries = GenerateConvmap(allowed);
	AX_ASSERT(m_numEntries != 0);

	m_entries = new SEntry[m_numEntries]();
}
template<typename T>
inline TDictionary<T>::~TDictionary() {
	delete [] m_entries; m_entries = (SEntry *)0;
	m_numEntries = 0;
}

template<typename T>
inline typename TDictionary<T>::SEntry *TDictionary<T>::Find(const char *key) {
	return FindFromEntry(m_entries, key, false);
}
template<typename T>
inline typename TDictionary<T>::SEntry *TDictionary<T>::Lookup
(const char *key) {
	return FindFromEntry(m_entries, key, true);
}

__AX_LIB_NS_FINI__

#endif
