﻿//
//	C++ Name Mangling Test
//	Look at the names produced by this file for information.
//

struct STRUCTNAME
{
	int FIELD_ONE;
	float FIELD_TWO;
};

class CLASSNAME
{
public:
	CLASSNAME();
	~CLASSNAME();

	int GETSUM( int, int );
	int GETSUM( int, int ) const;
};

CLASSNAME::CLASSNAME()
{
}
CLASSNAME::~CLASSNAME()
{
}

int CLASSNAME::GETSUM( int a, int b )
{
	return a + b;
}
int CLASSNAME::GETSUM( int a, int b ) const
{
	return a + b;
}

int GETSUM( int a, int b )
{
	return a + b;
}
float GETSUM( float a, float b )
{
	return a + b;
}
unsigned int GETSUM( unsigned int a, unsigned int b )
{
	return a + b;
}
double GETSUM( double a, double b )
{
	return a + b;
}

void DONOTHINGWITHSTRUCT( STRUCTNAME * )
{
}
STRUCTNAME *DONTRETURNSTRUCT( const STRUCTNAME * )
{
	return ( STRUCTNAME * )0;
}

int GETNUMBER()
{
	return 42;
}
float GETFLOATNUMBER()
{
	return 2.3f;
}
const char *GETSTRINGPOINTER()
{
	return "Hello, world!";
}
void TAKESTRINGPOINTER( const char * )
{
}
void TAKEMODIFIABLESTRING( char * )
{
}
void TAKESTRINGPOINTERPOINTER( char ** )
{
}
void TAKESTRINGPOINTERPOINTER( const char *const * )
{
}

float FloatFunction( float )
{
	return 0.0f;
}
int __fastcall FastIntFunction( int, int, int )
{
	return 0;
}
int __stdcall StandardIntFunction( int, int, int )
{
	return 0;
}

/*
__ZN9CLASSNAMEC2Ev
__ZN9CLASSNAMEC1Ev
__ZN9CLASSNAMED2Ev
__ZN9CLASSNAMED1Ev
__ZN9CLASSNAME6GETSUMEii
__ZNK9CLASSNAME6GETSUMEii
__Z6GETSUMii
__Z6GETSUMff
__Z6GETSUMjj
__Z6GETSUMdd
__Z19DONOTHINGWITHSTRUCTP10STRUCTNAME
__Z16DONTRETURNSTRUCTPK10STRUCTNAME
__Z9GETNUMBERv
__Z14GETFLOATNUMBERv
__Z16GETSTRINGPOINTERv
__Z17TAKESTRINGPOINTERPKc
__Z20TAKEMODIFIABLESTRINGPc
__Z24TAKESTRINGPOINTERPOINTERPPc
__Z24TAKESTRINGPOINTERPOINTERPKPKc
__Z13FloatFunctionf
@_Z15FastIntFunctioniii@12
__Z19StandardIntFunctioniii@12

??0CLASSNAME@@QEAA@XZ
??1CLASSNAME@@QEAA@XZ
?GETSUM@CLASSNAME@@QEAAHHH@Z
?GETSUM@CLASSNAME@@QEBAHHH@Z
?GETSUM@@YAHHH@Z
?GETSUM@@YAMMM@Z
?GETSUM@@YAIII@Z
?GETSUM@@YANNN@Z
?DONOTHINGWITHSTRUCT@@YAXPEAUSTRUCTNAME@@@Z
?DONTRETURNSTRUCT@@YAPEAUSTRUCTNAME@@PEBU1@@Z
?GETNUMBER@@YAHXZ
?GETFLOATNUMBER@@YAMXZ
?GETSTRINGPOINTER@@YAPEBDXZ
?TAKESTRINGPOINTER@@YAXPEBD@Z
?TAKEMODIFIABLESTRING@@YAXPEAD@Z
?TAKESTRINGPOINTERPOINTER@@YAXPEAPEAD@Z
?TAKESTRINGPOINTERPOINTER@@YAXPEBQEBD@Z
?FloatFunction@@YAMM@Z
?FastIntFunction@@YAHHHH@Z
?StandardIntFunction@@YAHHHH@Z
*/
