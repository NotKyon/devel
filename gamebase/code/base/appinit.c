#if _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif

#include <gamebase.h>
#include <errno.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>

#include "local.h"
#include "../sys/local.h"

/* -------------------------------------------------------------------------- */

/* Mac OS X */
#if defined(__APPLE__)
# include <mach-o/dyld.h>
static char *GetExecName(char *buf, size_t n) {
	uint32_t size;

	size = n;

	if (_NSGetExecutablePath(buf, &size)==0) {
		errno = ERANGE;
		return (char *)NULL;
	}

	return buf;
}
/* Linux */
#elif defined(linux)||defined(__linux)
# include <unistd.h>
# include <sys/types.h>
static char *GetExecName(char *buf, size_t n) {
	pid_t pid;
	char name[256];
	int r;

	pid = getpid();

	snprintf(name, sizeof(name), "/proc/%i/exe", (int)pid);

	if ((r = readlink(name, buf, n))==-1)
		return (char *)NULL;
	else if(r >= n) {
		errno = ERANGE;
		return (char *)NULL;
	}

	buf[r] = 0;
	return buf;
}
/* Solaris */
#elif (defined(sun)||defined(__sun))&&(defined(__SVR4)||defined(__svr4__))
# include <stdlib.h>
static char *GetExecName(char *buf, size_t n) {
	strncpy(buf, getexecname(), n-1);
	buf[n-1] = 0;

	return buf;
}
/* FreeBSD */
#elif defined(__FreeBSD__)
# include <sys/sysctl.h>
static char *GetExecName(char *buf, size_t n) {
	size_t size;
	int name[4];

	name[0] = CTL_KERN;
	name[1] = KERN_PROC;
	name[2] = KERN_PROC_PATHNAME;
	name[3] = -1;

	if (sysctl(name, sizeof(name)/sizeof(name[0]), buf, &size, 0, 0)==-1)
		return (char *)NULL;

	return buf;
}
/* UNIX */
#elif defined(unix)||defined(__unix__)
# include <stdio.h>
# include <stdlib.h>
static char *GetExecName(char *buf, size_t n) {
	char *s;

	if ((s=getenv("_"))!=(char *)0) {
		snprintf(buf, s, n-1);
		buf[n-1] = 0;

		return buf;
	}

	/* out of luck buddy, write a port */
	gbRuntimeError("GetExecName: Your UNIX distribution isn't supported.");

	return (char *)NULL;
}
/* Windows */
#elif defined(_WIN32)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
static char *GetExecName(char *buf, size_t n) {
	if (!GetModuleFileNameA(NULL, buf, n))
		return (char *)NULL;

	return buf;
}
/* wtf? */
#else
# error "GetExecName() needs a port"
#endif

/* -------------------------------------------------------------------------- */

static gb_bool_t DeliverFault(EXCEPTION_RECORD *record, const char *msg) {
	gb_exception_t except;

	except.kind = kGBExcept_Fault;
	except.message = msg;

	except.address = record->ExceptionAddress;
	except.data.p = (void *)NULL;

	return gbRaiseException(&except);
}

/* The thread tried to read from or write to a virtual address for which it does
   not have the appropriate access. */
static gb_bool_t OnAccessViolation(EXCEPTION_RECORD *record) {
	gb_accessViolationData_t accessViolationData;
	gb_exception_t except;

	accessViolationData.writeAccess = record->ExceptionInformation[0] & GB_TRUE;
	accessViolationData.address = (void *)record->ExceptionInformation[1];

	except.kind = kGBExcept_MemoryViolation;
	except.message = "There was an error accessing memory.";

	except.address = record->ExceptionAddress;
	except.data.accessViolation = &accessViolationData;

	return gbRaiseException(&except);
}

/* The thread tried to access an array element that is out of bounds and the
   underlying hardware supports bounds checking. */
static gb_bool_t OnArrayBoundsExceeded(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Array bounds exceeded.");
}

/* A breakpoint was encountered. */
static gb_bool_t OnBreakpoint(EXCEPTION_RECORD *record) {
	gb_exception_t except;

	except.kind = kGBExcept_Breakpoint;
	except.message = "Breakpoint tripped!";

	except.address = record->ExceptionAddress;
	except.data.p = (void *)NULL;

	return gbRaiseException(&except);
}

/* The thread tried to read or write data that is misaligned on hardware that
   does not provide alignment. For example, 16-bit values must be aligned on
   2-byte boundaries; 32-bit values on 4-byte boundaries, and so on. */
static gb_bool_t OnDatatypeMisalignment(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Data-type misalignment.");
}

/* One of the operands in a floating-point operation is denormal. A denormal
   value is one that is too small to represent as a standard floating-point
   value. */
static gb_bool_t OnFloatDenormalOperand(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "A floating-point operand is denormal.");
}

/* The thread tried to divide a floating-point value by a floating-point divisor
   of zero. */
static gb_bool_t OnFloatDivideByZero(EXCEPTION_RECORD *record) {
	gb_exception_t except;

	except.kind = kGBExcept_DivideByZero;
	except.message = "Floating-point division by zero.";

	except.address = record->ExceptionAddress;
	except.data.p = (void *)NULL;

	return gbRaiseException(&except);
}

/* The result of a floating-point operation cannot be represented exactly as a
   decimal fraction. */
static gb_bool_t OnFloatInexactResult(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Floating-point result is inexact.");
}

/* This exception represents any floating-point exception not included in this
   list. */
static gb_bool_t OnFloatInvalidOperation(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Invalid floating-point operation.");
}

/* The exponent of a floating-point operation is greater than the magnitude
   allowed by the corresponding type. */
static gb_bool_t OnFloatOverflow(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Floating-point exponent too high.");
}

/* The stack overflowed or underflowed as the result of a floating-point
   operation. */
static gb_bool_t OnFloatStackCheck(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Floating-point stack error.");
}

/* The exponent of a floating-point operation is less than the magnitude allowed
   by the corresponding type. */
static gb_bool_t OnFloatUnderflow(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Floating-point exponent too low.");
}

/* The thread tried to execute an invalid instruction. */
static gb_bool_t OnIllegalInstruction(EXCEPTION_RECORD *record) {
	gb_exception_t except;

	except.kind = kGBExcept_UndefinedInstruction;
	except.message = "Undefined or illegal instruction sequence.";

	except.address = record->ExceptionAddress;
	except.data.p = (void *)NULL;

	return gbRaiseException(&except);
}

/* The thread tried to access a page that was not present, and the system was
   unable to load the page. For example, this exception might occur if a network
   connection is lost while running a program over the network. */
static gb_bool_t OnInPageError(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Failed to load memory page.");
}

/* The thread tried to divide an integer value by an integer divisor of zero. */
static gb_bool_t OnIntDivideByZero(EXCEPTION_RECORD *record) {
	gb_exception_t except;

	except.kind = kGBExcept_DivideByZero;
	except.message = "Integer division by zero.";

	except.address = record->ExceptionAddress;
	except.data.p = (void *)NULL;

	return gbRaiseException(&except);
}

/* The result of an integer operation caused a carry out of the most significant
   bit of the result. */
static gb_bool_t OnIntOverflow(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Integer overflow.");
}

/* An exception handler returned an invalid disposition to the exception
   dispatcher. Programmers using a high-level language such as C should never
   encounter this exception. */
static gb_bool_t OnInvalidDisposition(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Bad mojo.");
}

/* The thread tried to continue execution after a noncontinuable exception
   occurred. */
static gb_bool_t OnNoncontinuableException(EXCEPTION_RECORD *record) {
	return DeliverFault(record, "Fatal exception!");
}

/* The thread tried to execute an instruction whose operation is not allowed in
   the current machine mode. */
static gb_bool_t OnPrivInstruction(EXCEPTION_RECORD *record) {
	gb_exception_t except;

	except.kind = kGBExcept_PrivilegedInstruction;
	except.message = "Permission to execute instruction: DENIED.";

	except.address = record->ExceptionAddress;
	except.data.p = (void *)NULL;

	return gbRaiseException(&except);
}

/* A trace trap or other single-instruction mechanism signaled that one
   instruction has been executed. */
static gb_bool_t OnSingleStep(EXCEPTION_RECORD *record) {
	gb_exception_t except;

	except.kind = kGBExcept_Breakpoint;
	except.message = "Single step.";

	except.address = record->ExceptionAddress;
	except.data.p = (void *)NULL;

	return gbRaiseException(&except);
}

/* The thread used up its stack. */
static gb_bool_t OnStackOverflow(EXCEPTION_RECORD *record) {
	gb_exception_t except;

	except.kind = kGBExcept_StackOverflow;
	except.message = "Thread overflowed its stack.";

	except.address = record->ExceptionAddress;
	except.data.p = (void *)NULL;

	return gbRaiseException(&except);
}

/* Unknown exception! (Just call it a memory violation) */
static gb_bool_t OnUnknown(EXCEPTION_RECORD *record) {
	gb_exception_t except;

	except.kind = kGBExcept_Fault;
	except.message = "Unknown error";

	except.address = record->ExceptionAddress;
	except.data.p = (void *)NULL;

	return gbRaiseException(&except);
}

/* -------------------------------------------------------------------------- */

#if _WIN32
static LPTOP_LEVEL_EXCEPTION_FILTER g_oldExceptionFilter =\
 (LPTOP_LEVEL_EXCEPTION_FILTER)NULL;

static LONG CALLBACK ExceptionHandler_f(EXCEPTION_POINTERS *info) {
# define CHOOSE(x)\
((x) ? EXCEPTION_CONTINUE_EXECUTION : EXCEPTION_CONTINUE_SEARCH)
	EXCEPTION_RECORD *record;
	gb_bool_t r;

	record = info->ExceptionRecord;

	switch(record->ExceptionCode) {
# define HANDLE(x,y) case EXCEPTION_##x: return CHOOSE(On##y(record))
	HANDLE(ACCESS_VIOLATION			, AccessViolation			);
	HANDLE(ARRAY_BOUNDS_EXCEEDED	, ArrayBoundsExceeded		);
	HANDLE(BREAKPOINT				, Breakpoint				);
	HANDLE(DATATYPE_MISALIGNMENT	, DatatypeMisalignment		);
	HANDLE(FLT_DENORMAL_OPERAND		, FloatDenormalOperand		);
	HANDLE(FLT_DIVIDE_BY_ZERO		, FloatDivideByZero			);
	HANDLE(FLT_INEXACT_RESULT		, FloatInexactResult		);
	HANDLE(FLT_INVALID_OPERATION	, FloatInvalidOperation		);
	HANDLE(FLT_OVERFLOW				, FloatOverflow				);
	HANDLE(FLT_STACK_CHECK			, FloatStackCheck			);
	HANDLE(FLT_UNDERFLOW			, FloatUnderflow			);
	HANDLE(ILLEGAL_INSTRUCTION		, IllegalInstruction		);
	HANDLE(IN_PAGE_ERROR			, InPageError				);
	HANDLE(INT_DIVIDE_BY_ZERO		, IntDivideByZero			);
	HANDLE(INT_OVERFLOW				, IntOverflow				);
	HANDLE(INVALID_DISPOSITION		, InvalidDisposition		);
	HANDLE(NONCONTINUABLE_EXCEPTION	, NoncontinuableException	);
	HANDLE(PRIV_INSTRUCTION			, PrivInstruction			);
	HANDLE(SINGLE_STEP				, SingleStep				);
	HANDLE(STACK_OVERFLOW			, StackOverflow				);
# undef HANDLE
	}

	r = OnUnknown(record);
	if (r==GB_FALSE) {
		if (g_oldExceptionFilter)
			return g_oldExceptionFilter(info);
		else
			return UnhandledExceptionFilter(info);
	}

	return CHOOSE(r);
# undef CHOOSE
}
#endif

/* -------------------------------------------------------------------------- */

static size_t g_fileMax = 0, g_fileNum = 0;
static char **g_files = (char **)NULL;

static gb_opt_t *g_optP = (gb_opt_t *)NULL;
static size_t g_optN = 0;
static const char *g_alias[256];

void gbSetOptTable(gb_opt_t *p, size_t n) {
	g_optP = p;
	g_optN = n;
}
void gbSetOptAlias(const char *alias[256]) {
	memcpy((void *)g_alias, (const void *)alias, sizeof(g_alias));
}

void gbAppInit(int argc, char **argv) {
#if _WIN32
	char path[261];
#else
	char path[4096];
#endif
	char *p;

	gbOnEnd(gbAppFini);
	gbSetExceptionHandler(gbDefaultException_f);

#if _WIN32
	g_oldExceptionFilter = SetUnhandledExceptionFilter(ExceptionHandler_f);
#else
# error "Need exception handling for this platform..."
#endif

	gblocalInitHandleSystem();
	gblocalInitThreading();
	gblocalInitEventQueue();

	path[0] = '\0';
	GetExecName(path, sizeof(path));

#if _WIN32
	p = &path[0];
	while((p = strchr(p, '\\')) != (char *)NULL)
		*p++ = '/';
#endif

	gbSetAppFile(path);

	p = strrchr(path, '/');
	if (p)
		*(p + 1) = '\0';

	gbSetAppDir(path);

	p = getcwd(path, sizeof(path) - 2);
	if (p) {
#if _WIN32
		while((p = strchr(p, '\\')) != (char *)NULL)
			*p++ = '/';
#endif

		p = strrchr(path, '/');
		if (!p || *(p + 1)!='\0') {
			p = strchr(path, '\0');

			*(p + 0) = '/';
			*(p + 1) = '\0';
		}

		gbSetLaunchDir(path);
	} else
		gbSetLaunchDir("/");

	if (argc > 1)
		gbAddAppArgs(argc - 1, &argv[1]);
}
void gbAppFini() {
	size_t i;

	gblocalFiniThreading();
	gblocalFiniHandleSystem();

	for(i=0; i<g_fileNum; i++)
		g_files[i] = (char *)gbMemFree((void *)g_files[i]);

	g_fileNum = 0;
	g_fileMax = 0;
	g_files = (char **)gbMemFree((void *)g_files);
}

void gbPushArgFile(const char *f) {
	gbAssert(f != (const char *)NULL);

	if (g_fileNum + 1 > g_fileMax) {
		g_fileMax += 16;

		g_files = (char **)gbMemExtend((void *)g_files,
			g_fileMax*sizeof(char *));
	}

	g_files[g_fileNum++] = gbDuplicate(f);
}
size_t gbCountArgFiles() {
	return g_fileNum;
}
const char *gbGetArgFile(size_t i) {
	gbAssert(i < g_fileNum);

	return g_files[i];
}

/* -------------------------------------------------------------------------- */

static int FindOpt(const char *opt, size_t *j) {
	size_t i;

	for(i=0; i<g_optN; i++) {
		if (!strcmp(opt, g_optP[i].name)) {
			*j = i;
			return 1;
		}
	}

	return 0;
}

void gbProcessArgs() {
	const char *alias, *opts[2];
	gb_opt_t *opt;
	size_t i, j, k, n;
	const char *arg, *p, *parm;
	char buf[256];
	int filemode, invert;

	filemode = 0;

	n = gbCountAppArgs();
	for(i=0; i<n; i++) {
		arg = gbAppArg(i);

		/* handle files passed to the command-line */
		if (*arg != '-' || filemode) {
			gbPushArgFile(arg);
			continue;
		}

		/* neither requirement above is true, therefore: *arg == '-' */
		arg++;

		/* did we encounter a single "-" meaning every command-line argument is
		   now actually just a file reference? */
		if (*arg=='\0') {
			filemode = 1;
			continue;
		}

		/* check to see if the argument is actually an alias */
		alias = g_alias[(int)(unsigned char)*arg];

		/* if the argument begins with "--" it's, by design, not an alias */
		invert = 0;
		if (*arg=='-') {
			arg++; /* NOTE: 'arg' is only incremented upon 'if true' */
			alias = (const char *)NULL;

			/* check if we're toggling with "--no-" */
			if (!strncmp(arg, "no-", 3)) {
				invert = 1;
				arg += 3;
			}
		}

		/* search for either a parameter, or the end of the argument string */
		parm = (const char *)NULL;
		p = alias ? arg : strchr(arg, '=');
		if (p)
			parm = *(p + 1) ? p + 1 : (const char *)NULL;
		else
			p = strchr(arg, '\0');
		/* if there is no more to this argument string, it's an error */
		if (arg==p && !alias) {
			gbWarnMessage("Ignoring invalid argument: %s", gbAppArg(i));
			continue;
		}

		/* copy the string into the buffer */
		gbStrCpyN(buf, sizeof(buf), arg, p - arg);

		/* prepare to find first */
		if (alias) {
			opts[0] = alias;
			opts[1] = buf;
			k = 2;
		} else {
			opts[0] = buf;
			k = 1;
		}

		/* find first */
		while(k > 0) {
			if (FindOpt(opts[k - 1], &j))
				break;

			k--;
		}

		/* k will be nonzero due to the break above if it succeeded, but the
		   check occurs for k - 1; this is valid

		   NOTE: 'k' is post-decremented! */
		if (k--==0) {
			gbWarnMessage("Unknown argument: %s", gbAppArg(i));
			continue;
		}

		/*
		 * TODO? In the event that something isn't right with the alias, check
		 *       the other option for validity. If that option is valid then use
		 *       it. (The 'k--' above is in preparation for this.)
		 */

		gbAssert(j < g_optN);
		opt = &g_optP[j];

		/* was 'no-' used on a non-switch option? */
		if (invert && (~opt->flags & kGBOptF_Switch_Bit))
			gbWarnMessage("Ignoring 'no-' on non-switch argument: %s",
				gbAppArg(i));

		/* was a parameter passed to an option that takes none? */
		if (parm && (~opt->flags & kGBOptF_Param_Bit))
			gbWarnMessage("Ignoring parameter on argument: %s", gbAppArg(i));

		/* was an option that is disabled used? */
		if (opt->flags & kGBOptF_Disabled_Bit) {
			if (opt->flags & kGBOptF_RunOnce_Bit)
				gbWarnMessage("Ignoring duplicate argument: %s", gbAppArg(i));
			else
				gbWarnMessage("Ignoring disabled argument: %s", gbAppArg(i));

			if (((opt->flags & kGBOptF_Param_Bit) &
			(~opt->flags & kGBOptF_Optional_Bit)) && !parm)
				i++; /* skip the potential argument here */

			continue;
		}

		/* handle switches */
		if (opt->flags & kGBOptF_Switch_Bit) {
			gbAssert(opt->bitfield != (int *)NULL);

			if (invert)
				*opt->bitfield &= ~opt->bitmask;
			else
				*opt->bitfield |= opt->bitmask;
		}

		/* handle parameters */
		if (opt->flags & kGBOptF_Param_Bit) {
			/* ensure there's a parameter */
			if (!parm) {
				if (i + 1 >= n)
					goto __stop_it_;

				if ((opt->flags & kGBOptF_Optional_Bit)
				&& *gbAppArg(i + 1)=='-')
					parm = (char *)NULL;
				else
					parm = gbAppArg(++i);
			} __stop_it_:
			if (!parm) {
				if (~opt->flags & kGBOptF_Optional_Bit) {
					gbWarnMessage("Parameter not given: %s", gbAppArg(i));
					break;
				}
			}

			gbAssert(opt->fn != (gb_optfn_t)NULL);
			opt->fn(parm);
		}

		/* run once? */
		if (opt->flags & kGBOptF_RunOnce_Bit) {
			opt->flags |= kGBOptF_Disabled_Bit;
			continue;
		}
	}
}
