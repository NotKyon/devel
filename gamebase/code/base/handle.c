#include <gamebase/base/memory.h>
#include <gamebase/base/report.h>
#include <gamebase/base/handle.h>

#include "../sys/lock.h"

#ifndef ZX
# if _WIN32
#  define ZX "0x"
# else
#  define ZX
# endif
#endif

#ifndef SIZE_T_MAX
# define SIZE_T_MAX ((size_t)(0xFFFFFFFF)|\
	(((size_t)0xFFFFFFFF)<<((sizeof(size_t)/4 - 1)*32)))
#endif

#ifndef ALIGNMENT
# define ALIGNMENT 16
#endif

struct objh_s;
struct pool_s;

enum {
	kObjF_Dead_Bit = (1<<0)
};
typedef struct objh_s {
	void *p;
#if GB_DEBUG
	char *dbg_name;
#endif
	size_t refCnt;
	size_t flags;
	struct pool_s *pool;
	struct objh_s *prev, *next;
} objh_t;

typedef struct pool_s {
	struct pool_s *base;
	size_t refCnt;

	char *name;

	gb_fnCtor_t fnCtor;
	gb_fnDtor_t fnDtor;

	size_t size_l, size_g; /* local size; total size */
	size_t offset;

	size_t avail_max;

	size_t avail_size, alloc_size;
	lock_t avail_lock, alloc_lock;
	struct objh_s *avail_head, *avail_tail;
	struct objh_s *alloc_head, *alloc_tail;

	struct pool_s *prev, *next;
} pool_t;

static size_t g_availMax = SIZE_T_MAX;

static pool_t *g_pool_head = (pool_t *)NULL;
static pool_t *g_pool_tail = (pool_t *)NULL;
static lock_t g_pool_lock;
static gb_bool_t g_didInit = GB_FALSE;

GB_FORCEINLINE void *AddPtr(void *p, size_t n) {
	union { void *p; size_t n; } x;

	x.p  = p;
	x.n += n;

	return x.p;
}
GB_FORCEINLINE void *SubPtr(void *p, size_t n) {
	union { void *p; size_t n; } x;

	x.p  = p;
	x.n -= n;

	return x.p;
}

static size_t AlignSize(size_t n) {
	return n + (n + 16)%16;
}
static size_t CalculateObjectSize(pool_t *pool) {
	return pool ? pool->size_l + CalculateObjectSize(pool->base) : 0;
}

void gblocalInitHandleSystem() {
	if (GB_LIKELY(g_didInit))
		return;

	g_pool_head = (struct pool_s *)NULL;
	g_pool_tail = (struct pool_s *)NULL;

	InitLock(&g_pool_lock);

	g_didInit = GB_TRUE;
}
void gblocalFiniHandleSystem() {
	if (!g_didInit)
		return;

	g_didInit = GB_FALSE;
}

void gblocalLockHandleSystem() {
	Lock(&g_pool_lock);
}
void gblocalUnlockHandleSystem() {
	Unlock(&g_pool_lock);
}

void gblocalLockPoolAvailList(struct pool_s *pool) {
	Lock(&pool->avail_lock);
}
void gblocalUnlockPoolAvailList(struct pool_s *pool) {
	Unlock(&pool->avail_lock);
}
void gblocalLockPoolAllocList(struct pool_s *pool) {
	Lock(&pool->alloc_lock);
}
void gblocalUnlockPoolAllocList(struct pool_s *pool) {
	Unlock(&pool->alloc_lock);
}

static gb_bool_t DoConstructor(void *base, struct pool_s *pool) {
	gbAssert(gbIsHandleTypeValid((gb_handleType_t)pool));

	if (pool->base && !DoConstructor(base, pool->base))
		return GB_FALSE;

	if (pool->fnCtor)
		return pool->fnCtor(AddPtr(base, pool->offset));

	return GB_TRUE;
}
static void DoDestructor(void *base, struct pool_s *pool) {
	gbAssert(gbIsHandleTypeValid((gb_handleType_t)pool));

	if (pool->fnDtor)
		pool->fnDtor(AddPtr(base, pool->offset));

	if (pool->base)
		DoDestructor(base, pool->base);
}
gb_bool_t gblocalObjectConstructorChain(struct objh_s *objh) {
	return DoConstructor(objh->p, objh->pool);
}
void gblocalObjectDestructorChain(struct objh_s *objh) {
	DoDestructor(objh->p, objh->pool);
}

gb_handleType_t gblocalPurgeHandleType(gb_handleType_t type) {
	pool_t *pool;

	if ((pool = (pool_t *)type) == (pool_t *)NULL)
		return (gb_handleType_t)NULL;

	if (pool->base)
		gbDropHandleType((gb_handleType_t)pool->base);

	gblocalLockPoolAllocList(pool);
	pool->avail_max = 0;
	pool->refCnt = 1; /*avoid reference count errors*/
	while(pool->alloc_head)
		gbPurgeHandle((gb_handle_t)pool->alloc_head);
	gblocalLockPoolAvailList(pool);
	while(pool->avail_head)
		gbPurgeHandle((gb_handle_t)pool->avail_head);
	gblocalUnlockPoolAvailList(pool);
	pool->size_l = 0;
	pool->size_g = 0;
	pool->offset = 0;
	pool->refCnt = 0;
	gblocalUnlockPoolAllocList(pool);

	gblocalLockHandleSystem();
	if (GB_LIKELY(pool->prev != (pool_t *)NULL))
		pool->prev->next = pool->next;
	else
		g_pool_head = pool->next;
	if (GB_LIKELY(pool->next != (pool_t *)NULL))
		pool->next->prev = pool->prev;
	else
		g_pool_tail = pool->prev;
	gblocalUnlockHandleSystem();

	FiniLock(&pool->alloc_lock);
	FiniLock(&pool->avail_lock);

	pool->name = (char *)gbMemFree((void *)pool->name);

	return (gb_handleType_t)gbMemFree((void *)pool);
}

gb_bool_t gbIsHandleTypeValid(gb_handleType_t type) {
	pool_t *pool;

	if ((pool = (pool_t *)type) == (pool_t *)NULL) {
		gbDebugLog("Handle type is NULL");
		return GB_FALSE;
	}

	if (!pool->name) {
		gbDebugLog(gbF("Handle type "ZX"%p does not have a name", type));
		return GB_FALSE;
	}

#if GB_DEBUG
	if (pool->base && !gbIsHandleTypeValid(pool->base)) {
		gbDebugLog(gbF("Handle type %s:"ZX"%p does not have a valid base",
			pool->name, type));
		return GB_FALSE;
	}
#endif

	if (!pool->size_g) {
		gbDebugLog(gbF("Handle type %s:"ZX"%p has null global alloc. size",
			pool->name, type));
		return GB_FALSE;
	}
	if (!pool->size_l) {
		gbDebugLog(gbF("Handle type %s:"ZX"%p has null local alloc. size",
			pool->name, type));
		return GB_FALSE;
	}

	if (!pool->refCnt) {
		gbDebugLog(gbF("Handle type %s:"ZX"%p has no reference count",
			pool->refCnt));
		return GB_FALSE;
	}

	return GB_TRUE;
}
gb_bool_t gbIsHandleValid(gb_handle_t handle) {
#if GB_DEBUG
# define DBGNMA "%s%s%s"
# define DBGNMB (objh->dbg_name ? " [" : ""),\
				(objh->dbg_name ? objh->dbg_name : ""),\
				(objh->dbg_name ? "]" : ""),
#else
# define DBGNMA
# define DBGNMB
#endif
	pool_t *pool;
	objh_t *objh;

	if ((objh = (objh_t *)handle) == (objh_t *)NULL) {
		gbDebugLog("Handle is null.");
		return GB_FALSE;
	}

	if ((pool = objh->pool) == (pool_t *)NULL) {
		gbDebugLog(gbF("Handle "ZX"%p"DBGNMA" has no type", DBGNMB handle));
		return GB_FALSE;
	}

	if (!gbIsHandleTypeValid((gb_handleType_t)pool)) {
		gbDebugLog(gbF("Handle "ZX"%p"DBGNMA"'s type is invalid",
			DBGNMB handle));
		return GB_FALSE;
	}

	if (!objh->refCnt) {
		gbDebugLog(gbF(
			"Handle "ZX"%p"DBGNMA" of type %s:"ZX"%p has no reference count",
			DBGNMB handle, pool->name, (void *)pool));
		return GB_FALSE;
	}

	if (objh->flags & kObjF_Dead_Bit) {
		gbDebugLog(gbF(
			"Handle "ZX"%p"DBGNMA" of type %s:"ZX"%p has its 'dead' bit set",
			DBGNMB handle, pool->name, (void *)pool));
		return GB_FALSE;
	}

	return GB_TRUE;
#undef DBGNMB
#undef DBGNMA
}

gb_handleType_t gbExtendHandleType(const char *name, size_t n, gb_fnCtor_t ctor,
gb_fnDtor_t dtor, gb_handleType_t base) {
	pool_t *pool, *prnt;

	gbAssert(name != (const char *)NULL);
	gbAssert(n > 0);

	prnt = (pool_t *)base;

	pool = (pool_t *)gbMemAlloc(sizeof(*pool));

	pool->base = prnt;
	if (base)
		gbGrabHandleType(base);
	pool->refCnt = 1;

	pool->name = gbDuplicate(name);

	pool->fnCtor = ctor;
	pool->fnDtor = dtor;

	pool->size_l = n;
	pool->size_g = CalculateObjectSize(pool) + AlignSize(sizeof(objh_t));
	pool->offset = CalculateObjectSize(prnt);

	pool->avail_max = SIZE_T_MAX;

	pool->avail_size = 0;
	pool->alloc_size = 0;

	InitLock(&pool->avail_lock);
	InitLock(&pool->alloc_lock);

	pool->avail_head = (objh_t *)NULL;
	pool->avail_tail = (objh_t *)NULL;
	pool->alloc_head = (objh_t *)NULL;
	pool->alloc_tail = (objh_t *)NULL;

	pool->next = (pool_t *)NULL;
	gblocalLockHandleSystem();
	if (GB_LIKELY((pool->prev = g_pool_tail) != (pool_t *)NULL))
		g_pool_tail->next = pool;
	else
		g_pool_head = pool;
	g_pool_tail = pool;
	gblocalUnlockHandleSystem();

	return (gb_handleType_t)pool;
}
gb_handleType_t gbAddHandleType(const char *name, size_t n, gb_fnCtor_t ctor,
gb_fnDtor_t dtor) {
	return gbExtendHandleType(name, n, ctor, dtor, (gb_handleType_t)NULL);
}
void gbGrabHandleType(gb_handleType_t type) {
	gbAssert(gbIsHandleTypeValid(type));

	((pool_t *)type)->refCnt++;
}
gb_handleType_t gbDropHandleType(gb_handleType_t type) {
	gbAssert(gbIsHandleTypeValid(type));

	if (--((pool_t *)type)->refCnt == 0)
		return gblocalPurgeHandleType(type);

	return (gb_handleType_t)NULL;
}

size_t gbGetDeadObjectLimit(gb_handleType_t type) {
	size_t m;

	gbAssert(gbIsHandleTypeValid(type));

	m = ((pool_t *)type)->avail_max;
	return m < g_availMax ? m : g_availMax;
}
gb_bool_t gbDeadObjectVacancy(gb_handleType_t type) {
	gb_bool_t r;
	size_t m;

	gbAssert(gbIsHandleTypeValid(type));

	gblocalLockPoolAvailList((pool_t *)type);

	m = ((pool_t *)type)->avail_max;
	m = m < g_availMax ? m : g_availMax;

	r = m > ((pool_t *)type)->avail_size ? GB_TRUE : GB_FALSE;

	gblocalUnlockPoolAvailList((pool_t *)type);

	return r;
}

void gbSetGlobalDeadObjectLimit(size_t limit) {
	g_availMax = limit;
}
size_t gbGetGlobalDeadObjectLimit() {
	return g_availMax;
}

void gbSetLocalDeadObjectLimit(gb_handleType_t type, size_t limit) {
	gbAssert(gbIsHandleTypeValid(type));

	((pool_t *)type)->avail_max = limit;
}
size_t gbGetLocalDeadObjectLimit(gb_handleType_t type) {
	gbAssert(gbIsHandleTypeValid(type));

	return ((pool_t *)type)->avail_max;
}

size_t gbCountLiveHandles(gb_handleType_t type) {
	union { void *p; size_t n; } x;

	gbAssert(gbIsHandleTypeValid(type));

	x.p = gbAtomicReadPtr((void **)&((pool_t *)type)->alloc_size);
	return x.n;
}
size_t gbCountDeadHandles(gb_handleType_t type) {
	union { void *p; size_t n; } x;

	gbAssert(gbIsHandleTypeValid(type));

	x.p = gbAtomicReadPtr((void **)&((pool_t *)type)->avail_size);
	return x.n;
}

gb_handle_t gbNewHandle(gb_handleType_t type) {
	pool_t *pool;
	objh_t *objh;

	gbAssert(gbIsHandleTypeValid(type));
	pool = (pool_t *)type;

	objh = (objh_t *)NULL;

	gblocalLockPoolAvailList(pool);
	if (pool->avail_head) {
		pool->avail_size--;
		objh = pool->avail_head;

		if (objh->next)
			objh->next->prev = (objh_t *)NULL;
		else
			pool->avail_tail = (objh_t *)NULL;
		pool->avail_head = objh->next;
	}
	gblocalUnlockPoolAvailList(pool);

	if (!objh)
		objh = (objh_t *)gbMemAlloc(pool->size_g);

	objh->p = AddPtr((void *)objh, AlignSize(sizeof(*objh)));
#if GB_DEBUG
	objh->dbg_name = (char *)NULL;
#endif
	objh->refCnt = 1;
	objh->flags = 0;

	objh->pool = pool;

	objh->next = (objh_t *)NULL;
	objh->prev = (objh_t *)NULL;

	if (!gblocalObjectConstructorChain(objh)) {
		gblocalLockPoolAvailList(pool);
		if (gbDeadObjectVacancy((gb_handleType_t)pool)) {
			pool->avail_size++;
			objh->flags |= kObjF_Dead_Bit;
			if ((objh->prev = pool->avail_tail) != (objh_t *)NULL)
				pool->avail_tail->next = objh;
			else
				pool->avail_head = objh;
			pool->avail_tail = objh;
		}
		gblocalUnlockPoolAvailList(pool);

		return (gb_handle_t)NULL;
	}

	gblocalLockPoolAllocList(pool);
	pool->alloc_size++;
	if ((objh->prev = pool->alloc_tail) != (objh_t *)NULL)
		pool->alloc_tail->next = objh;
	else
		pool->alloc_head = objh;
	pool->alloc_tail = objh;
	gblocalUnlockPoolAllocList(pool);

	return (gb_handle_t)objh;
}
gb_handle_t gbPurgeHandle(gb_handle_t handle) {
	gb_bool_t doFree, isDead;
	objh_t **p_head, **p_tail;
	objh_t *objh;
	lock_t *p_lock;
	size_t *p_size;
	pool_t *pool;

	if ((objh = (objh_t *)handle) == (objh_t *)NULL)
		return (gb_handle_t)NULL;

	doFree = GB_TRUE;

	pool = objh->pool;
	gbAssert(gbIsHandleTypeValid((gb_handleType_t)pool));

#if GB_DEBUG
	if (objh->refCnt > 0) {
		const char *dbg_name;

		dbg_name = objh->dbg_name;

		gbDebugLog(gbF("Deleting '%s':"ZX"%p%s%s%s with reference count of %u",
			pool->name, (void *)objh, dbg_name ? " (" : "",
			dbg_name ? dbg_name : "", dbg_name ? ")" : "",
			(gb_uint_t)objh->refCnt));
	}
#endif

	isDead = objh->flags & kObjF_Dead_Bit;

	p_head = isDead ? &pool->avail_head : &pool->alloc_head;
	p_tail = isDead ? &pool->avail_tail : &pool->alloc_tail;
	p_lock = isDead ? &pool->avail_lock : &pool->alloc_lock;
	p_size = isDead ? &pool->avail_size : &pool->alloc_size;

	Lock(p_lock);
	(*p_size)--;
	if (objh->prev) /*deleting from the front may be common*/
		objh->prev->next = objh->next;
	else
		*p_head = objh->next;
	if (GB_LIKELY(objh->next != (objh_t *)NULL))
		objh->next->prev = objh->prev;
	else
		*p_tail = objh->prev;
	Unlock(p_lock);

	if (!isDead) {
		gblocalObjectDestructorChain(objh);

		Lock(&pool->avail_lock);
		if (gbDeadObjectVacancy((gb_handleType_t)pool)) {
			doFree = GB_FALSE;

			pool->avail_size++;
#if GB_DEBUG
			objh->dbg_name = (char *)gbMemFree((void *)objh->dbg_name);
#endif
			objh->flags |= kObjF_Dead_Bit;

			objh->next = (objh_t *)NULL;
			if ((objh->prev = pool->avail_tail) != (objh_t *)NULL)
				pool->avail_tail->next = objh;
			else
				pool->avail_head = objh;
			pool->avail_tail = objh;
		}
		Unlock(&pool->avail_lock);
	} else if(objh->refCnt) {
		objh->refCnt = 0;
		gblocalObjectDestructorChain(objh);
	}

	if (doFree) {
#if GB_DEBUG
		objh->dbg_name = (char *)gbMemFree((void *)objh->dbg_name);
#endif
		gbMemFree((void *)objh);
	}

	return (gb_handle_t)NULL;
}
gb_handleType_t gbHandleType(gb_handle_t handle) {
	gbAssert(gbIsHandleValid(handle));

	return (gb_handleType_t)(((objh_t *)handle)->pool);
}
gb_handleType_t gbHandleSuperType(gb_handle_t handle) {
	gbAssert(gbIsHandleValid(handle));

	return (gb_handleType_t)(((objh_t *)handle)->pool->base);
}
gb_handleType_t gbHandleBaseType(gb_handle_t handle) {
	pool_t *pool;

	gbAssert(gbIsHandleValid(handle));

	pool = ((objh_t *)handle)->pool;
	while(pool->base)
		pool = pool->base;

	return (gb_handleType_t)pool;
}
gb_bool_t gbIsHandleDerived(gb_handle_t handle, gb_handleType_t from) {
	pool_t *pool;

	gbAssert(gbIsHandleValid(handle));
	gbAssert(from != (gb_handleType_t)NULL);

	pool = ((objh_t *)handle)->pool;
	while(pool) {
		if (pool==(pool_t *)from)
			return GB_TRUE;

		pool = pool->base;
	}

	return GB_FALSE;
}

gb_handle_t gbHandleEx(void *p, gb_handleType_t type) {
	gb_handle_t h;
	size_t offset;

	gbAssert(p != (void *)NULL);

	offset = 0;
	if (type) {
		gbAssert(gbIsHandleTypeValid(type));

		offset = ((pool_t *)type)->offset;
	}

	h = (gb_handle_t)SubPtr(p, offset + AlignSize(sizeof(objh_t)));
	gbAssert(gbIsHandleValid(h));
	gbAssert(gbIsHandleDerived(h, type));

	return h;
}
void *gbObjectEx(gb_handle_t handle, gb_handleType_t type) {
	size_t offset;
	void *p;

	gbAssert(gbIsHandleValid(handle));

	offset = 0;
	if (type) {
		gbAssert(gbIsHandleTypeValid(type));
		gbAssert(gbIsHandleDerived(handle, type));

		offset = ((pool_t *)type)->offset;
	}

	p = AddPtr(((objh_t *)handle)->p, offset);

	return p;
}

gb_handle_t gbHandle(void *p) {
	return gbHandleEx(p, (gb_handleType_t)NULL);
}
void *gbObject(gb_handle_t handle) {
	return gbObjectEx(handle, (gb_handleType_t)NULL);
}

void gbGrabHandle(gb_handle_t handle) {
	gbAssert(gbIsHandleValid(handle));

	((objh_t *)handle)->refCnt++;
}
gb_handle_t gbDropHandle(gb_handle_t handle) {
	gbAssert(gbIsHandleValid(handle));

	if (--((objh_t *)handle)->refCnt == 0)
		return gbPurgeHandle(handle);

	return (gb_handle_t)NULL;
}

void gbSetHandleDebugName(gb_handle_t handle, const char *name) {
#if GB_DEBUG
	objh_t *objh;

	gbAssert(gbIsHandleValid(handle));
	objh = (objh_t *)handle;

	objh->dbg_name = gbCopy(objh->dbg_name, name);
#else
	if (handle||name) {} //unused
#endif
}
const char *gbGetHandleDebugName(gb_handle_t handle) {
#if GB_DEBUG
	gbAssert(gbIsHandleValid(handle));

	return ((objh_t *)handle)->dbg_name;
#else
	if (handle) {} //unused

	return (const char *)NULL;
#endif
}
