#include <AxTek/Containers/TList.h>
#include <stdio.h>

struct Item {
	int v;
	ax::lib::TListLink<Item> link;
	static ax::lib::TListBase<Item> g_list;

	inline Item(int x): v(x), link(this, &g_list) {
	}
	inline ~Item() {
	}

	inline void Set(int x) { v = x; }
	inline int Get() const { return v; }

	static inline void PrintList(const char *heading) {
		Item *p;

		printf("[%s]\n", heading);
		for(p=g_list.First(); p; p=p->link.After())
			printf("%p - %i\n", (void *)p, p->Get());
		printf("\n");
	}
};
ax::lib::TListBase<Item> Item::g_list;

int main() {
	Item *a, *b, *c;

	Item::g_list.SetDeleteObjectOnRemove(true);

	a = new Item(1);
	b = new Item(2);
	c = new Item(3);

	Item::PrintList("A");

	b->link.InsertBefore(&a->link);
	c->link.InsertAfter(&a->link);

	Item::PrintList("B");

	b->link.Remove();

	Item::PrintList("C");

	Item::g_list.RemoveAll();

	Item::PrintList("D");

	return 0;
}
