#include <ember/config.h>
#include <ember/gc.h>
#include <ember/math.h>
#include <ember/entity.h>
#include <ember/scene.h>

/*
 * -----------------------------------------------------------------------------
 * ENTITY SYNCING
 * -----------------------------------------------------------------------------
 */

/*
 * TODO: Entity transform syncing operations here.
 */

/*
 * -----------------------------------------------------------------------------
 * ENTITY API
 * -----------------------------------------------------------------------------
 */

static int __ent_alloc( entity_t ent )
{
	ent->data[ 0 ] = ( void * )0;
	ent->data[ 1 ] = ( void * )0;

	ent->prnt = ( struct stEntity * )0;
	ent->head = ( struct stEntity * )0;
	ent->tail = ( struct stEntity * )0;
	ent->prev = ( struct stEntity * )0;
	ent->next = ( struct stEntity * )0;

	ent->scene = ( struct stScene * )0;

	tform_init( &ent->tform, ent );

	return 1;
}
static void __ent_dealloc( struct stEntity *ent )
{
	struct stEntity *tmp, *next;

	if( ent->head ) {
		for( tmp = ent->head; tmp; tmp = next ) {
			next = tmp->next;
			ent_set_parent( tmp, ent->prnt );
		}
	}

	if( ent->prev )
		ent->prev->next = ent->next;
	if( ent->next )
		ent->next->prev = ent->prev;

	if( ent->prnt ) {
		if( ent->prnt->head == ent )
			ent->prnt->head = ent->next;
		if( ent->prnt->tail == ent )
			ent->prnt->tail = ent->prev;
	} else {
		if( ent->scene->head == ent )
			ent->scene->head = ent->next;
		if( ent->scene->tail == ent )
			ent->scene->tail = ent->prev;
	}

	gc_release( ( void * )ent->scene );
}

int ent_init( void )
{
	g_ent_pool = gc_register_pool( sizeof( struct stEntity ), ( gc_init_t )__ent_alloc,
	    ( gc_deinit_t )__ent_dealloc );
	if( !g_ent_pool )
		return 0;
	return 1;
}
void ent_deinit( void )
{
	gc_unregister_pool( g_ent_pool );
}

entity_t ent_alloc( entity_t prnt, scene_t scene )
{
	entity_t ent;

	if( !( ent = ( entity_t )gc_alloc( g_ent_pool ) ) )
		return ( entity_t )0;

	ent->prnt = prnt;
	if( prnt ) {
		ent->prev = prnt->tail;
		ent->next = 0;
		if( prnt->tail )
			prnt->tail->next = ent;
		prnt->tail = ent;
		if( !prnt->head )
			prnt->head = ent;
		ent->scene     = scene ? scene : prnt->scene;
	} else
		ent->scene = scene ? scene : sce_first();

	/* NOTE: there should always be a scene */
	gc_retain( ( void * )ent->scene );

	return ent;
}

entity_t ent_dealloc( entity_t ent )
{
	gc_release( ( void * )ent );
	return ( entity_t )0;
}

int ent_is_descendant( entity_t ent, entity_t prnt )
{
	if( !prnt )
		return 1;

	while( ent ) {
		if( ent->prnt == prnt )
			return 1;

		ent = ent->prnt;
	}

	return 0;
}
void ent_set_parent( entity_t ent, entity_t prnt )
{
	if( ent->prnt == prnt )
		return;

	/* avoid recursion issue */
	if( ent_is_descendant( prnt, ent ) )
		ent_set_parent( prnt, ent->prnt );

	if( ent->prev )
		ent->prev->next = ent->next;
	if( ent->next )
		ent->next->prev = ent->prev;

	if( ent->prnt ) {
		if( ent->prnt->head == ent )
			ent->prnt->head = ent->next;
		if( ent->prnt->tail == ent )
			ent->prnt->tail = ent->prev;
	} else {
		if( ent->scene->head == ent )
			ent->scene->head = ent->next;
		if( ent->scene->tail == ent )
			ent->scene->tail = ent->prev;
	}

	ent->prnt = prnt;
	ent->next = ( entity_t )0;
	if( ent->prnt ) {
		ent->prev = ent->prnt->tail;
		if( ent->prnt->tail )
			ent->prnt->tail->next = ent;
		ent->prnt->tail = ent;
		if( !ent->prnt->head )
			ent->prnt->head = ent;
	} else {
		ent->prev = ent->scene->tail;
		if( ent->scene->tail )
			ent->scene->tail->next = ent;
		ent->scene->tail = ent;
		if( !ent->scene->head )
			ent->scene->head = ent;
	}
}

void ent_set_scene( entity_t ent, scene_t scene )
{
	scene = scene ? scene : sce_first();
	gc_retain( scene );
	gc_release( ent->scene );
	ent->scene = scene;
}

entity_t ent_parent( entity_t ent )
{
	return ent->prnt;
}
entity_t ent_first( entity_t ent )
{
	return ent->head;
}
entity_t ent_last( entity_t ent )
{
	return ent->tail;
}
entity_t ent_before( entity_t ent )
{
	return ent->prev;
}
entity_t ent_after( entity_t ent )
{
	return ent->next;
}
scene_t ent_scene( entity_t ent )
{
	return ent->scene;
}

void ent_set_data( entity_t ent, void *data )
{
	ent->data[ 0 ] = data;
}
void *ent_data( entity_t ent )
{
	return ent->data[ 0 ];
}

void ent_set_sys_data( entity_t ent, void *data )
{
	ent->data[ 1 ] = data;
}
void *ent_sys_data( entity_t ent )
{
	return ent->data[ 1 ];
}

void ent_translate_ex( entity_t ent, const Vec3 *v )
{
	tform_translate_ex( &ent->tform, v );
}
void ent_rotate_ex( entity_t ent, const Vec3 *v )
{
	tform_rotate_ex( &ent->tform, v );
}
void ent_scale_ex( entity_t ent, const Vec3 *v )
{
	tform_scale_ex( &ent->tform, v );
}
void ent_move_ex( entity_t ent, const Vec3 *v )
{
	tform_move_ex( &ent->tform, v );
}
void ent_turn_ex( entity_t ent, const Vec3 *v )
{
	tform_turn_ex( &ent->tform, v );
}

void ent_translate( entity_t ent, float x, float y, float z )
{
	tform_translate( &ent->tform, x, y, z );
}
void ent_rotate( entity_t ent, float x, float y, float z )
{
	tform_rotate( &ent->tform, x, y, z );
}
void ent_scale( entity_t ent, float x, float y, float z )
{
	tform_scale( &ent->tform, x, y, z );
}
void ent_move( entity_t ent, float x, float y, float z )
{
	tform_move( &ent->tform, x, y, z );
}
void ent_turn( entity_t ent, float x, float y, float z )
{
	tform_turn( &ent->tform, x, y, z );
}

void ent_move_x( entity_t ent, float x )
{
	tform_move_x( &ent->tform, x );
}
void ent_move_y( entity_t ent, float y )
{
	tform_move_y( &ent->tform, y );
}
void ent_move_z( entity_t ent, float z )
{
	tform_move_z( &ent->tform, z );
}

void ent_turn_x( entity_t ent, float x )
{
	tform_turn_x( &ent->tform, x );
}
void ent_turn_y( entity_t ent, float y )
{
	tform_turn_y( &ent->tform, y );
}
void ent_turn_z( entity_t ent, float z )
{
	tform_turn_z( &ent->tform, z );
}

void ent_get_translation_ex( entity_t ent, Vec3 *v )
{
	tform_get_translation_ex( &ent->tform, v );
}
void ent_get_rotation_ex( entity_t ent, Vec3 *v )
{
	tform_get_rotation_ex( &ent->tform, v );
}
void ent_get_scale_ex( entity_t ent, Vec3 *v )
{
	tform_get_scale_ex( &ent->tform, v );
}

void ent_get_translation( entity_t ent, float *x, float *y, float *z )
{
	tform_get_translation( &ent->tform, x, y, z );
}
void ent_get_rotation( entity_t ent, float *x, float *y, float *z )
{
	tform_get_rotation( &ent->tform, x, y, z );
}
void ent_get_scale( entity_t ent, float *x, float *y, float *z )
{
	tform_get_scale( &ent->tform, x, y, z );
}

float ent_translation_x( entity_t ent )
{
	return tform_translation_x( &ent->tform );
}
float ent_translation_y( entity_t ent )
{
	return tform_translation_y( &ent->tform );
}
float ent_translation_z( entity_t ent )
{
	return tform_translation_z( &ent->tform );
}

float ent_rotation_x( entity_t ent )
{
	return tform_rotation_x( &ent->tform );
}
float ent_rotation_y( entity_t ent )
{
	return tform_rotation_y( &ent->tform );
}
float ent_rotation_z( entity_t ent )
{
	return tform_rotation_z( &ent->tform );
}

float ent_scale_x( entity_t ent )
{
	return tform_scale_x( &ent->tform );
}
float ent_scale_y( entity_t ent )
{
	return tform_scale_y( &ent->tform );
}
float ent_scale_z( entity_t ent )
{
	return tform_scale_z( &ent->tform );
}

Matrix *ent_local_matrix( entity_t ent )
{
	return tform_local_matrix( &ent->tform );
}
Matrix *ent_global_matrix( entity_t ent )
{
	return tform_global_matrix( &ent->tform );
}

void ent_set_local_matrix( entity_t ent, const Matrix *m )
{
	tform_set_local_matrix( &ent->tform, m );
}
void ent_set_global_matrix( entity_t ent, const Matrix *m )
{
	tform_set_global_matrix( &ent->tform, m );
}
