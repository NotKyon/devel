#include <stdio.h>
#include <stdlib.h>

template< typename T >
class A
{
public:
	T x, y;

	A( T x = 0, T y = 0 ): x( x ), y( y ) {}

	A< bool > operator<( const A &v ) const { return A< bool >( x < v.x, y < v.y ); }
};
template<>
class A< bool >
{
public:
	bool x, y;

	A( bool x = false, bool y = false ): x( x ), y( y ) {}

	inline bool all() const { return x && y; }
	inline bool any() const { return x || y; }
};

int main()
{
	
	return EXIT_SUCCESS;
}
