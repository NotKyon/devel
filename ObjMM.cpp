#include <stddef.h>
#include <stdint.h>

#ifdef _WIN32
# include <Windows.h>
#endif

#if defined( _MSC_VER )
# include <intrin.h>


# define OBJMM_ATOMIC_EXCHANGE_FULL32( Dst, Src )\
	( OBJMM::uint32( _InterlockedExchange( ( volatile long * )( Dst ), ( long )( Src ) ) ) )
# define OBJMM_ATOMIC_COMPARE_EXCHANGE_FULL32( Dst, Src, Cmp )\
	( OBJMM::uint32( _InterlockedCompareExchange( ( volatile long * )( Dst ), ( long )( Src ), ( long )( Cmp ) ) ) )

# define OBJMM_ATOMIC_FETCH_ADD_FULL32( Dst, Src )\
	( OBJMM::uint32( _InterlockedExchangeAdd( ( volatile long * )( Dst ), ( long )( Src ) ) ) )
# define OBJMM_ATOMIC_FETCH_SUB_FULL32( Dst, Src )\
	( OBJMM::uint32( _InterlockedExchangeAdd( ( volatile long * )( Dst ), -( long )( Src ) ) ) )
# define OBJMM_ATOMIC_FETCH_AND_FULL32( Dst, Src )\
	( OBJMM::uint32( _InterlockedAnd( ( volatile long * )( Dst ), ( long )( Src ) ) ) )
# define OBJMM_ATOMIC_FETCH_OR_FULL32( Dst, Src )\
	( OBJMM::uint32( _InterlockedOr( ( volatile long * )( Dst ), ( long )( Src ) ) ) )
# define OBJMM_ATOMIC_FETCH_XOR_FULL32( Dst, Src )\
	( OBJMM::uint32( _InterlockedXor( ( volatile long * )( Dst ), ( long )( Src ) ) ) )

# define OBJMM_ATOMIC_EXCHANGE_FULLPTR( Dst, Src )\
	( ( void * )( _InterlockedExchangePointer( ( void *volatile * )( Dst ), ( void * )( Src ) ) ) )
# define OBJMM_ATOMIC_COMPARE_EXCHANGE_FULLPTR( Dst, Src, Cmp )\
	( ( void * )( _InterlockedCompareExchangePointer( ( void *volatile * )( Dst ), ( void * )( Src ), ( void * )( Cmp ) ) ) )


#elif defined( __GNUC__ ) || defined( __clang__ )


# define OBJMM_ATOMIC_EXCHANGE_FULL32( Dst, Src )\
	( OBJMM::uint32( __sync_lock_test_and_set( ( volatile OBJMM::uint32 * )( Dst ), ( OBJMM::uint32 )( Src ) ) ) )
# define OBJMM_ATOMIC_COMPARE_EXCHANGE_FULL32( Dst, Src, Cmp )\
	( OBJMM::uint32( __sync_val_compare_and_swap( ( volatile OBJMM::uint32 * )( Dst ), ( OBJMM::uint32 )( Src ), ( OBJMM::uint32 )( Cmp ) ) ) )

# define OBJMM_ATOMIC_FETCH_ADD_FULL32( Dst, Src )\
	( OBJMM::uint32( __sync_fetch_and_add( ( volatile OBJMM::uint32 * )( Dst ), ( OBJMM::uint32 )( Src ) ) ) )
# define OBJMM_ATOMIC_FETCH_SUB_FULL32( Dst, Src )\
	( OBJMM::uint32( __sync_fetch_and_sub( ( volatile OBJMM::uint32 * )( Dst ), ( OBJMM::uint32 )( Src ) ) ) )
# define OBJMM_ATOMIC_FETCH_AND_FULL32( Dst, Src )\
	( OBJMM::uint32( __sync_fetch_and_and( ( volatile OBJMM::uint32 * )( Dst ), ( OBJMM::uint32 )( Src ) ) ) )
# define OBJMM_ATOMIC_FETCH_OR_FULL32( Dst, Src )\
	( OBJMM::uint32( __sync_fetch_and_or( ( volatile OBJMM::uint32 * )( Dst ), ( OBJMM::uint32 )( Src ) ) ) )
# define OBJMM_ATOMIC_FETCH_XOR_FULL32( Dst, Src )\
	( OBJMM::uint32( __sync_fetch_and_xor( ( volatile OBJMM::uint32 * )( Dst ), ( OBJMM::uint32 )( Src ) ) ) )

# define OBJMM_ATOMIC_EXCHANGE_FULLPTR( Dst, Src )\
	( ( void * )( __sync_lock_test_and_set( ( void *volatile * )( Dst ), ( void * )( Src ) ) ) )
# define OBJMM_ATOMIC_COMPARE_EXCHANGE_FULLPTR( Dst, Src, Cmp )\
	( ( void * )( __sync_val_compare_and_swap( ( void *volatile * )( Dst ), ( void * )( Src ), ( void * )( Cmp ) ) ) )


#endif

namespace OBJMM
{

#ifdef _WIN32
	typedef CRITICAL_SECTION		MutexType;

	inline void InitMutex( MutexType &Mutex )
	{
		InitializeCriticalSection( &Mutex );
	}
	inline void FiniMutex( MutexType & )
	{
	}

	inline void AcquireMutex( MutexType &Mutex )
	{
		EnterCriticalSection( &Mutex );
	}
	inline bool TryAcquireMutex( MutexType &Mutex )
	{
		return TryEnterCriticalSection( &Mutex ) != FALSE;
	}
	inline void ReleaseMutex( MutexType &Mutex )
	{
		LeaveCriticalSection( &Mutex );
	}
#endif

	typedef int8_t					int8;
	typedef int16_t					int16;
	typedef int32_t					int32;
	typedef int64_t					int64;
	typedef uint8_t					uint8;
	typedef uint16_t				uint16;
	typedef uint32_t				uint32;
	typedef uint64_t				uint64;
	typedef ptrdiff_t				intptr;
	typedef size_t					uintptr;

	struct SMemoryChunkMeta;
	struct SMemoryChunk;
	struct SObjectType;
	struct SObjectPoolEntry;
	struct SObjectPoolPage;
	struct SObjectPool;
	struct SRegistry;
	
	namespace EPageProtection
	{
		enum Type
		{
			// No access is allowed for the page
			None,
			// Read-only access
			Read,
			// Write-only access (NOTE: Some platforms don't support "write-only," so this should silently map to ReadWrite)
			Write,
			// Read and write access
			ReadWrite
		};
	}

	typedef void *( *FnAllocPage )( EPageProtection::Type Protection );
	typedef void( *FnDeallocPage )( void *pPageBase );
	typedef void( *FnProtectPage )( void *pPageBase, EPageProtection::Type Protection );

	typedef void *( *FnAlloc )( uintptr cBytes );
	typedef void( *FnDealloc )( void *pMemory );
	typedef void( *FnInit )( void *pObject );
	typedef void( *FnFini )( void *pObject );
	
	// Metadata for a given memory chunk
	//
	// This contains information used by allocation systems
	struct SMemoryChunkMeta
	{
		// Each bit represents one cache-line of the memory chunk
		uint64						uAllocatedCachelines;
	};
	// A chunk of memory (equal to a 4K page)
	struct SMemoryChunk
	{
		union
		{
			// Memory chunk meta data
			SMemoryChunkMeta		Meta;
			// Actual bytes of data
			uint8					uBytes[ 4096 ];
		};
	};
	
	enum EObjectTypeFlag
	{
		// Objects will be "double buffered" with one read pointer and one write
		// pointer each. In this case the current "tick" for the type represents
		// which buffer is being used for each operation.
		kTypeF_DoubleBuffered		= 1<<0
	};

	// Represents information about the type of an object.
	//
	// Types can inherit from other types which allows a derived type to be
	// passed to systems expecting an instance of a type that the given type
	// instance has derived from.
	//
	// For example:
	//
	//		Given the following hierarchy...
	//
	//			- Vehicle
	//			--- Car (inherits from Vehicle)
	//
	//		Passing an object of type "Car" to a function taking a "Vehicle"
	//		will work just fine.
	//
	// Types specify the number of bytes they need and their alignment
	// requirements. The largest alignment of the class hierarchy is used. So,
	// even if "Car" requires an alignment of 16, if "Vehicle" requires an
	// alignment of 64 then 64 is used. Likewise if Vehicle only required 16 and
	// Car required 64 then 64 would still be used since it is the larger of the
	// two.
	//
	// Types specify an initialization and finishing callback pair. These
	// callbacks act as the equivalent of a constructor/destructor. In the
	// absence of an initialization callback the memory of the part of the
	// object representing the type without the initialization callback is
	// initialized to zero.
	struct SObjectType
	{
		SObjectType *				pSuper;
		uintptr						cBytes;
		uintptr						uAlign;
		uintptr						uOffset;
		FnInit						pfnInit;
		FnFini						pfnFini;
	};
	
	// Individual entry in the pool for an object (only contains
	// meta-information about an object's validity)
	#pragma pack(push,1)
	struct SObjectPoolEntry
	{
		// Number of active references to an object
		//
		// Each time this reaches 0 the uGeneration member is incremented.
		uint16						cReferences;
		// Current generation for the object. This must match the handle's value
		// for the generation for the object to be valid for that handle. There
		// is a resolution of kMaxGenerations (64) generations.
		uint8						uGeneration;
		// Reserved field, for future settings. Set to 0.
		uint8						uFlags;
	};
	#pragma pack(pop)

	enum
	{
		kNumPageEntryBytes			= 2048,
		kNumTotalEntries			= 0x00100000,										// 1048576
		kNumEntriesPerPage			= kNumPageEntryBytes/sizeof( SObjectPoolEntry ),	//  512 (32-bit),  256 (64-bit)
		kNumPagesPerPool			= kNumTotalEntries/kNumEntriesPerPage,				// 2048 (32-bit), 4096 (64-bit)
		kNumFreeMasksPerPage		= kNumPageEntryBytes/( sizeof( uint32 )*8 ),		//   64
		kGenerationBits				= 3,
		kMaxGenerations				= 1<<kGenerationBits,								// 8 (generations)
		kTypeBits					= 12 - kGenerationBits,								// 9 (type bits)
		kMaxTypes					= 1<<kTypeBits,										// 512 (types)
		kChunkSize					= 4096,
		kCachelineSize				= 64,
		kCachelinesPerChunk			= kChunkSize/kCachelineSize							// 64
	};

	// Individual section of objects in a pool
	struct SObjectPoolPage
	{
		// Base memory (double buffered - read page and write page are swapped each "tick")
		SMemoryChunk *				pMemory				[ 2 ];

		// Collection of individual object entries for this range
		SObjectPoolEntry			Entries				[ kNumEntriesPerPage ];
		// The entries that are "free" in the range
		uint32						FreeEntries			[ kNumFreeMasksPerPage ];
	};
	// Collection of all objects for this pool, separated into individual pages
	struct SObjectPool
	{
		// Individual page pools for the object
		SObjectPoolPage *			pPages				[ kNumPagesPerPool ];

		// Current number of unallocated pages
		uintptr						cFreePages;
		// Unallocated pages (for allocating a new page)
		uint16						uFreePages			[ kNumPagesPerPool ];
	};

	// The OBJMM registry, used for all systems
	struct SRegistry
	{
		// Function used to allocate a new page (4096 bytes)
		FnAllocPage					pfnAllocPage;
		// Function used to deallocate a page
		FnDeallocPage				pfnDeallocPage;
		// Function used to change the protection settings of a page
		FnProtectPage				pfnProtectPage;
		// Function used to allocate arbitrary memory
		FnAlloc						pfnAlloc;
		// Function used to deallocate arbitrary memory
		FnDealloc					pfnDealloc;
		// Current tick
		uint32						uTick;
		// Number of types in the system
		uint32						cTypes;
		// Individual type pointers
		SObjectType *				pTypes				[ kMaxTypes ];
		// Individual pool pointers (corresponding to the types)
		SObjectPool *				pPools				[ kMaxTypes ];
	};
	
	// Allocate a type -- individual types cannot be deallocated (except at exit)
	SObjectType *AllocType( SRegistry &InRegistry );

}

//----------------------------------------------------------------------------//

namespace OBJMM
{

	struct PlcmntNw {};

}

inline void *operator new( size_t, void *p, const OBJMM::PlcmntNw & )
{
	return p;
}

namespace OBJMM
{

	namespace Generic
	{
	
		void *AllocPage( EPageProtection::Type )
		{
			return malloc( 4096 );
		}
		void DeallocPage( void *pPageBase )
		{
			free( pPageBase );
		}
		void ProtectPage( void *, EPageProtection::Type )
		{
		}
		
		void *Alloc( uintptr cBytes )
		{
			return malloc( cBytes );
		}
		void Dealloc( void *pMemory )
		{
			free( pMemory );
		}
	
	}

#ifdef _WIN32
	namespace Windows
	{
	
		namespace Detail
		{
		
			uintptr GetPageSize()
			{
				struct SSysInfo
				{
					SYSTEM_INFO				Info;
					
					SSysInfo()
					{
						GetSystemInfo( &Info );
					}
				};
				static SSysInfo Instance;
				
				return uintptr( Instance.dwPageSize );
			}
			DWORD GetProtection( EPageProtection::Type Protection )
			{
				switch( Protection )
				{
					case EPageProtection::None:
						return PAGE_NOACCESS;

					case EPageProtection::Read:
						return PAGE_READONLY;

					case EPageProtection::Write:
						// Is there any way (or OS version) to set write-only?
						return PAGE_READWRITE;

					case EPageProtection::ReadWrite:
						return PAGE_READWRITE;
				}

				return 0;
			}
		
		}
	
		void *AllocPage( EPageProtection::Type Protection )
		{
			if( Detail::GetPageSize() != 4096 ) {
				return malloc( 4096 );
			}
			
			return VirtualAlloc( NULL, 4096, MEM_COMMIT | MEM_RESERVE, Detail::GetProtection( Protection ) );
		}
		void DeallocPage( void *pPageBase )
		{
			if( !pPageBase ) {
				return;
			}

			if( Detail::GetPageSize() != 4096 ) {
				free( pPageBase );
				return;
			}

			VirtualFree( pPageBase, 4096, MEM_RELEASE );
		}
		void ProtectPage( void *pPageBase, EPageProtection::Type Protection )
		{
			if( Detail::GetPageSize() != 4096 ) {
				return;
			}

			DWORD dwDontCareAboutPriorProtection;
			VirtualProtect( pPageBase, 4096, Detail::GetProtection( Protection ), &dwDontCareAboutPriorProtection );
			( void )dwDontCareAboutPriorProtection;
		}
	
	}
#endif

	template< typename tObject >
	static tObject *Alloc( SRegistry &InRegistry )
	{
		tObject *const p = ( tObject * )InRegistry.pfnAlloc( sizeof( *p ) );
		if( !p ) {
			return NULL;
		}

		new( p, PlcmntNw() ) tObject();
		return p;
	}
	template< typename tObject >
	static tObject *Dealloc( SRegistry &InRegistry, tObject *pObject )
	{
		if( !pObject ) {
			return NULL;
		}

		pObject->~tObject();
		InRegistry.pfnDealloc( ( void * )pObject );
		return NULL;
	}

	static void CPUSpin()
	{
		for( volatile uint32 i = 0; i < 1024; ++i ) {
		}
	}

	SObjectType *AllocType( SRegistry &InRegistry )
	{
		SObjectType *pType = NULL;

		for(;;) {
			const uint32 uTypeIndex = *( volatile uint32 * )&InRegistry.cTypes;
			
			if( uTypeIndex >= kMaxTypes ) {
				return Dealloc( InRegistry, pType );
			}

			if( !pType ) {
				pType = Alloc< SObjectType >();
				if( !pType ) {
					return NULL;
				}
			}

			SObjectType *const pCmpxchgResult =
				( SObjectType * )
				OBJMM_ATOMIC_COMPARE_EXCHANGE_FULLPTR
				(
					&InRegistry.pTypes[ uTypeIndex ], 
					pType,
					NULL
				);

			if( pCmpxchgResult != pType ) {
				CPUSpin();
				continue;
			}

			break;
		}

		return pType;
	}
	void DeallocType( SRegistry &InRegistry, SObjectType *pObjectType )
	{
		if( !pObjectType ) {
			return;
		}

		
	}

}
