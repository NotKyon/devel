#ifndef MESHIO_GEOMETRY_H
#define MESHIO_GEOMETRY_H

typedef enum {
	EMeshIO_Points,

	EMeshIO_Lines,
	EMeshIO_LineStrip,
	EMeshIO_LineLoop,

	EMeshIO_Triangles,
	EMeshIO_TriangleStrip,
	EMeshIO_TriangleFan,

	EMeshIO_Quads,

	EMeshIO_Patch /* tessellation */
} EMeshIOTopology;

typedef enum {
	EMeshIO_PositionStream,
	EMeshIO_NormalStream,
	EMeshIO_BoneStream,
	EMeshIO_WeightStream,
	EMeshIO_ColorStream,
	EMeshIO_TexCoordStream,
	EMeshIO_BinormalStream,
	EMeshIO_TangentStream
} EMeshIOStreamType;

#ifndef meshio_geostream_t_defined
#define meshio_geostream_t_defined
typedef struct MeshIO_GeoStream *meshio_geostream_t;
#endif

#ifndef meshio_geometry_t_defined
#define meshio_geometry_t_defined
typedef struct MeshIO_Geometry *meshio_geometry_t;
#endif

typedef struct MESHIO_STREAM_DESC {
	EMeshIOStreamType kind;
	uint8_t index;
	EMeshIODataType data_kind;
	uint8_t data_size;
	uint32_t offset;
	uint32_t stride;
} MESHIO_STREAM_DESC;
struct MeshIO_GeoStream {
	MESHIO_STREAM_DESC desc;

	union {
		void *ptr;
		unsigned char *byte_ptr;
		unsigned short *short_ptr;
		unsigned int *int_ptr;
		float *float_ptr;
		double *double_ptr;
	} data;

	struct MeshIO_Geometry *prnt;
	struct MeshIO_GeoStream *prev,
	    *next;
};
struct MeshIO_Geometry {
	unsigned int refcnt;

	struct {
		EMeshIOTopology topology;
		EMeshIODataType kind;
		uint8_t patch_count;
		uint32_t count;
		union {
			void *ptr;
			unsigned char *byte_ptr;
			unsigned short *short_ptr;
			unsigned int *int_ptr;
		} data;
	} primitive;

	struct {
		size_t count;
		size_t vertex_count;
		struct MeshIO_GeoStream *head,
		    *tail;
	} stream;

	struct MeshIO_Material *mtrl;

	struct MeshIO_Geometry *prev,
	    *next;
};

extern meshio_geometry_t meshio_alloc_geometry();
extern meshio_geometry_t meshio_dealloc_geometry( meshio_geometry_t geo );
extern void meshio_retain_geometry( meshio_geometry_t geo );
extern meshio_geometry_t meshio_release_geometry( meshio_geometry_t geo );
extern size_t meshio_geometry_count();
extern meshio_geometry_t meshio_first_geometry();
extern meshio_geometry_t meshio_last_geometry();
extern meshio_geometry_t meshio_geometry_before( meshio_geometry_t geo );
extern meshio_geometry_t meshio_geometry_after( meshio_geometry_t geo );
extern EMeshIOTopology meshio_geometry_primitive_topology( meshio_geometry_t geo );
extern EMeshIODataType meshio_geometry_primitive_data_type( meshio_geometry_t geo );
extern uint8_t meshio_geometry_primitive_patch_count( meshio_geometry_t geo );
extern uint32_t meshio_geometry_primitive_count( meshio_geometry_t geo );
extern void *meshio_geometry_primitive_data( meshio_geometry_t geo );
extern uint8_t *meshio_geometry_primitive_bytes( meshio_geometry_t geo );
extern uint16_t *meshio_geometry_primitive_shorts( meshio_geometry_t geo );
extern uint32_t *meshio_geometry_primitive_ints( meshio_geometry_t geo );
extern size_t meshio_geometry_stream_count( meshio_geometry_t geo );
extern size_t meshio_geometry_vertex_count( meshio_geometry_t geo );
extern meshio_geostream_t meshio_first_geometry_stream( meshio_geometry_t geo );
extern meshio_geostream_t meshio_last_geometry_stream( meshio_geometry_t geo );
extern meshio_geostream_t meshio_geometry_stream_before( meshio_geostream_t stream );
extern meshio_geostream_t meshio_geometry_stream_after( meshio_geostream_t stream );
extern meshio_material_t meshio_geometry_material( meshio_geometry_t geo );
extern void meshio_set_geometry_primitive_topology( meshio_geometry_t geo, EMeshIOTopology topology, ... );
extern int meshio_set_geometry_primitive_data_type( meshio_geometry_t geo, EMeshIODataType kind );
extern void *meshio_set_geometry_primitive_count( meshio_geometry_t geo, uint32_t count );
extern size_t meshio_calculate_geometry_vertex_size( meshio_geometry_t geo, size_t vertex_count );
extern size_t meshio_geometry_vertex_size( meshio_geometry_t geo );
extern meshio_geostream_t meshio_alloc_geostream( meshio_geometry_t geo, const MESHIO_STREAM_DESC *desc );
extern meshio_geostream_t meshio_dealloc_geostream( meshio_geostream_t gs );

#endif
