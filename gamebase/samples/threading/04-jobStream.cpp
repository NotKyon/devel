//
//	The built-in job streaming routines provide a mechanism for exploiting
//	scalable cross-platform parallelism/concurrency in your application. The
//	other built-in systems utilize the job streamer internally for maximum
//	performance.
//
//	Unlike other systems where a fixed number of threads are used, this system
//	creates as many threads as there are hardware threads. (In some cases more,
//	if the system determines that it would be beneficial for your system.) The
//	internal code is then broken up into a bunch of smaller jobs which can be
//	executed efficiently on the job streamer.
//
//	Here's an example of why this is better. Assume there were one thread for
//	each subsystem. e.g., one for A.I., one for rendering, one for physics, one
//	for sound processing, one for networking, and one for miscellaneous updates.
//	That's six threads in total. On a dual-core machine, six threads is too
//	high. Only two can run at a time, meaning there will be a couple of
//	unnecessary context switches, as well as potential cache invalidation as
//	threads move between cores. (The latter isn't an issue if an affinity is
//	set, but that may cause inefficient resource allocation.)
//
//	Now let's take that example on an eight-, sixteen-, or even thirtytwo-core
//	system. Because the threads are fixed in size, they can't take advantage of
//	this type of system.
//
//	The job streamer solves both of these cases by allocating however many
//	threads it determines are necessary (usually the number of hardware threads
//	available in your system). Then, with each job broken up to be as small as
//	possible, the tasks can be divided efficiently among the available cores.
//
//	This example demonstrates how you can use this system (it's easy!) to
//	determine how to find which numbers are prime within a subset of numbers.
//	There are more uses to this system, however.
//
#define GB_INIT 1
#include <gamebase.h>

//
//	We need to choose how many tests we're going to perform; 1024 is default.
//
#define NUM_TESTS 1024

//
//	Here are the numbers we'll test against.
//
int g_numbers[NUM_TESTS];

//
//	Simple primality test, good for our function
//
gb_bool_t IsPrime(gb_uint_t number) {
	gb_uint_t i;

	// check a small sample set of known non-primes
	if (number <= 2)
		return GB_FALSE;

	// even numbers aren't primes
	if (~number & 1)
		return GB_FALSE;

	// enumerate the numbers and check for even divisibility
	// NOTE: doing this the long way for demonstration purposes
	for(i=2; i<number; i++) {
		if (number%i == 0)
			return GB_FALSE;
	}

	// appears to be a prime
	return GB_TRUE;
}

//
//	Job stream function
//
void IsPrime_f(void *p) {
	*(int *)p = IsPrime(*(gb_uint_t *)p);
}

//
//	Perform the test
//
void GBMain() {
	struct { gb_u64_t sec, nsec; } s, e; //timer query
	gb_uint_t i;

	// tell the system that we want to use the default streamer
	gbRequireDefStream();

	// take the starting time
	gbQueryPerfTimer(&s.sec, &s.nsec);

	// start streaming
	for(i=0; i<NUM_TESTS; i++) {
		g_numbers[i] = 100000000 + i;
		gbSchedule(IsPrime_f, &g_numbers[i]);
	}

	// wait for streaming to complete
	gbSyncDefStream();

	// take the ending time
	gbQueryPerfTimer(&e.sec, &e.nsec);

	// note how long it took
	gbPrint(gbF("%u.%u",
		(gb_uint_t)(e.sec - s.sec),
		(gb_uint_t)(e.nsec - s.nsec)));
}
