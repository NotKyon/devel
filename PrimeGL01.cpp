﻿

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
void EnableOpenGL(HWND hwnd, HDC*, HGLRC*);
void DisableOpenGL(HWND, HDC, HGLRC);

// キョン修正：始まる
#include <math.h>
namespace KYON
{

	//=======================================================================//
	//
	//	MATH
	//
	//=======================================================================//

	float Degrees(float x) { return x/3.1415926535f*180.0f; }
	float Radians(float x) { return x/180.0f*3.1415926535f; }

	float Cos(float x) { return cosf(Radians(x)); }
	float Sin(float x) { return sinf(Radians(x)); }
	float Tan(float x) { return tanf(Radians(x)); }

	float ACos(float x) { return Degrees(acosf(x)); }
	float ASin(float x) { return Degrees(asinf(x)); }
	float ATan(float x) { return Degrees(atanf(x)); }
	float ATan2(float y, float x) { return Degrees(atan2f(y, x)); }

	float Sqrt(float x) { return sqrtf(x); }
	float InvSqrt(float x) { return 1.0f/sqrtf(x); }

	//=======================================================================//
	//
	//	MATRIX
	//
	//=======================================================================//

	//
	//	PROJECTION
	//

	// Load a perspective matrix
	void LoadPerspective(float( &M )[ 16 ], float fov, float aspect, float zn, float zf) {
		float a, b, c, d;

		b = 1.0f/Tan(0.5f*fov);
		a = b/aspect;

		c =     zf/(zf - zn);
		d = -zn*zf/(zf - zn);

		M[ 0]=a; M[ 4]=0; M[ 8]=0; M[12]=0;
		M[ 1]=0; M[ 5]=b; M[ 9]=0; M[13]=0;
		M[ 2]=0; M[ 6]=0; M[10]=c; M[14]=d;
		M[ 3]=0; M[ 7]=0; M[11]=1; M[15]=0;
	}
	// Load an orthographic matrix
	void LoadOrtho(float( &M )[ 16 ], float l, float r, float b, float t, float zn, float zf) {
		float A, B, C, D, E, F;

		A =    2.0f/(r  -  l);
		B = (l + r)/(l  -  r);
		C =    2.0f/(t  -  b);
		D = (t + b)/(b  -  t);
		E =    1.0f/(zf - zn);
		F =      zn/(zn - zf);

		M[ 0]=A; M[ 4]=0; M[ 8]=0; M[12]=B;
		M[ 1]=0; M[ 5]=C; M[ 9]=0; M[13]=D;
		M[ 2]=0; M[ 6]=0; M[10]=E; M[14]=F;
		M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
	}

	//
	//	GENERAL
	//

	void LoadIdentity(float( &M )[ 16 ]) {
		M[ 0]=1; M[ 4]=0; M[ 8]=0; M[12]=0;
		M[ 1]=0; M[ 5]=1; M[ 9]=0; M[13]=0;
		M[ 2]=0; M[ 6]=0; M[10]=1; M[14]=0;
		M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
	}

	void LoadTranslation(float( &M )[ 16 ], float x, float y, float z) {
		M[ 0]=1; M[ 4]=0; M[ 8]=0; M[12]=x;
		M[ 1]=0; M[ 5]=1; M[ 9]=0; M[13]=y;
		M[ 2]=0; M[ 6]=0; M[10]=1; M[14]=z;
		M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
	}
	void LoadRotation(float( &M )[ 16 ], float x, float y, float z) {
		float sx,sy,sz, cx,cy,cz;
		float cz_nsx;

		sx=Sin(x); sy=Sin(y); sz=Sin(z);
		cx=Cos(x); cy=Cos(y); cz=Cos(z);

		cz_nsx = cz*-sx;

		M[ 0]= cz*cy;              M[ 4]=-sz*cx;  M[ 8]= cz*-sy + (sz*sx)*cy;
		M[ 1]= sz*cy + cz_nsx*sy;  M[ 5]= cz*cx;  M[ 9]= sz*-sy + cz_nsx*cy;
		M[ 2]= cx*sy;              M[ 6]= sx;     M[10]= cx*cy;

								   M[12]=0;
								   M[13]=0;
								   M[14]=0;
		M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
	}
	void LoadScaling(float( &M )[ 16 ], float x, float y, float z) {
		M[ 0]=x; M[ 4]=0; M[ 8]=0; M[12]=0;
		M[ 1]=0; M[ 5]=y; M[ 9]=0; M[13]=0;
		M[ 2]=0; M[ 6]=0; M[10]=z; M[14]=0;
		M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
	}

	void ApplyTranslation(float( &M )[ 16 ], float x, float y, float z) {
		M[12] += M[ 0]*x + M[ 4]*y + M[ 8]*z;
		M[13] += M[ 1]*x + M[ 5]*y + M[ 9]*z;
		M[14] += M[ 2]*x + M[ 6]*y + M[10]*z;
	}
	void ApplyXRotation(float( &M )[ 16 ], float x) {
		float c, s, t;

		c = Cos(x);
		s = Sin(x);

		t = M[ 4];
		M[ 4] = t*c  + M[ 8]*s;
		M[ 8] = t*-s + M[ 8]*c;

		t = M[ 5];
		M[ 5] = t*c  + M[ 9]*s;
		M[ 9] = t*-s + M[ 9]*c;

		t = M[ 6];
		M[ 6] = t*c  + M[10]*s;
		M[10] = t*-s + M[10]*c;
	}
	void ApplyYRotation(float( &M )[ 16 ], float y) {
		float c, s, t;

		c = Cos(y);
		s = Sin(y);

		t = M[ 0];
		M[ 0] = t*c  + M[ 8]*s;
		M[ 8] = t*-s + M[ 8]*c;

		t = M[ 1];
		M[ 1] = t*c  + M[ 9]*s;
		M[ 9] = t*-s + M[ 9]*c;

		t = M[ 2];
		M[ 2] = t*c  + M[10]*s;
		M[10] = t*-s + M[10]*c;
	}
	void ApplyZRotation(float( &M )[ 16 ], float z) {
		float c, s, t;

		c = Cos(z);
		s = Sin(z);

		t = M[ 0];
		M[ 0] = t*c  + M[ 4]*s;
		M[ 4] = t*-s + M[ 4]*c;

		t = M[ 1];
		M[ 1] = t*c  + M[ 5]*s;
		M[ 5] = t*-s + M[ 5]*c;

		t = M[ 2];
		M[ 2] = t*c  + M[ 6]*s;
		M[ 6] = t*-s + M[ 6]*c;
	}
	void ApplyRotation(float( &M )[ 16 ], float x, float y, float z) {
		ApplyZRotation(M, z);
		ApplyXRotation(M, x);
		ApplyYRotation(M, y);
	}

	void LoadAffineInverse(float( &M )[ 16 ], const float *P) {
		float x, y, z;

		x = -(P[12]*P[ 0] + P[13]*P[ 1] + P[14]*P[ 2]);
		y = -(P[12]*P[ 4] + P[13]*P[ 5] + P[14]*P[ 6]);
		z = -(P[12]*P[ 8] + P[13]*P[ 9] + P[14]*P[10]);

		M[ 0]=P[ 0]; M[ 4]=P[ 1]; M[ 8]=P[ 2]; M[12]=x;
		M[ 1]=P[ 4]; M[ 5]=P[ 5]; M[ 9]=P[ 6]; M[13]=y;
		M[ 2]=P[ 8]; M[ 6]=P[ 9]; M[10]=P[10]; M[14]=z;
		M[ 3]=   0 ; M[ 7]=   0 ; M[11]=   0 ; M[15]=1;
	}
	void AffineMultiply(float( &M )[ 16 ], const float *P, const float *Q) {
		M[ 0]=P[ 0]*Q[ 0] + P[ 4]*Q[ 1] + P[ 8]*Q[ 2];
		M[ 4]=P[ 0]*Q[ 4] + P[ 4]*Q[ 5] + P[ 8]*Q[ 6];
		M[ 8]=P[ 0]*Q[ 8] + P[ 4]*Q[ 9] + P[ 8]*Q[10];
		M[12]=P[ 0]*Q[12] + P[ 4]*Q[13] + P[ 8]*Q[14] + P[12];

		M[ 1]=P[ 1]*Q[ 0] + P[ 5]*Q[ 1] + P[ 9]*Q[ 2];
		M[ 5]=P[ 1]*Q[ 4] + P[ 5]*Q[ 5] + P[ 9]*Q[ 6];
		M[ 9]=P[ 1]*Q[ 8] + P[ 5]*Q[ 9] + P[ 9]*Q[10];
		M[13]=P[ 1]*Q[12] + P[ 5]*Q[13] + P[ 9]*Q[14] + P[13];

		M[ 2]=P[ 2]*Q[ 0] + P[ 6]*Q[ 1] + P[10]*Q[ 2];
		M[ 6]=P[ 2]*Q[ 4] + P[ 6]*Q[ 5] + P[10]*Q[ 6];
		M[10]=P[ 2]*Q[ 8] + P[ 6]*Q[ 9] + P[10]*Q[10];
		M[14]=P[ 2]*Q[12] + P[ 6]*Q[13] + P[10]*Q[14] + P[14];

		M[ 3]=0;
		M[ 7]=0;
		M[11]=0;
		M[15]=1;
	}
	void Multiply3(float( &M )[ 16 ], const float *P, const float *Q) {
		M[ 0]=P[ 0]*Q[ 0] + P[ 4]*Q[ 1] + P[ 8]*Q[ 2];
		M[ 4]=P[ 0]*Q[ 4] + P[ 4]*Q[ 5] + P[ 8]*Q[ 6];
		M[ 8]=P[ 0]*Q[ 8] + P[ 4]*Q[ 9] + P[ 8]*Q[10];
		M[12]=0;

		M[ 1]=P[ 1]*Q[ 0] + P[ 5]*Q[ 1] + P[ 9]*Q[ 2];
		M[ 5]=P[ 1]*Q[ 4] + P[ 5]*Q[ 5] + P[ 9]*Q[ 6];
		M[ 9]=P[ 1]*Q[ 8] + P[ 5]*Q[ 9] + P[ 9]*Q[10];
		M[13]=0;

		M[ 2]=P[ 2]*Q[ 0] + P[ 6]*Q[ 1] + P[10]*Q[ 2];
		M[ 6]=P[ 2]*Q[ 4] + P[ 6]*Q[ 5] + P[10]*Q[ 6];
		M[10]=P[ 2]*Q[ 8] + P[ 6]*Q[ 9] + P[10]*Q[10];
		M[14]=0;

		M[ 3]=0;
		M[ 7]=0;
		M[11]=0;
		M[15]=1;
	}
	void Multiply4(float( &M )[ 16 ], const float *P, const float *Q) {
		M[ 0]=P[ 0]*Q[ 0] + P[ 4]*Q[ 1] + P[ 8]*Q[ 2] + P[12]*Q[ 3];
		M[ 4]=P[ 0]*Q[ 4] + P[ 4]*Q[ 5] + P[ 8]*Q[ 6] + P[12]*Q[ 7];
		M[ 8]=P[ 0]*Q[ 8] + P[ 4]*Q[ 9] + P[ 8]*Q[10] + P[12]*Q[11];
		M[12]=P[ 0]*Q[12] + P[ 4]*Q[13] + P[ 8]*Q[14] + P[12]*Q[15];

		M[ 1]=P[ 1]*Q[ 0] + P[ 5]*Q[ 1] + P[ 9]*Q[ 2] + P[13]*Q[ 3];
		M[ 5]=P[ 1]*Q[ 4] + P[ 5]*Q[ 5] + P[ 9]*Q[ 6] + P[13]*Q[ 7];
		M[ 9]=P[ 1]*Q[ 8] + P[ 5]*Q[ 9] + P[ 9]*Q[10] + P[13]*Q[11];
		M[13]=P[ 1]*Q[12] + P[ 5]*Q[13] + P[ 9]*Q[14] + P[13]*Q[15];

		M[ 2]=P[ 2]*Q[ 0] + P[ 6]*Q[ 1] + P[10]*Q[ 2] + P[14]*Q[ 3];
		M[ 6]=P[ 2]*Q[ 4] + P[ 6]*Q[ 5] + P[10]*Q[ 6] + P[14]*Q[ 7];
		M[10]=P[ 2]*Q[ 8] + P[ 6]*Q[ 9] + P[10]*Q[10] + P[14]*Q[11];
		M[14]=P[ 2]*Q[12] + P[ 6]*Q[13] + P[10]*Q[14] + P[14]*Q[15];

		M[ 3]=P[ 3]*Q[ 0] + P[ 7]*Q[ 1] + P[11]*Q[ 2] + P[15]*Q[ 3];
		M[ 7]=P[ 3]*Q[ 4] + P[ 7]*Q[ 5] + P[11]*Q[ 6] + P[15]*Q[ 7];
		M[11]=P[ 3]*Q[ 8] + P[ 7]*Q[ 9] + P[11]*Q[10] + P[15]*Q[11];
		M[15]=P[ 3]*Q[12] + P[ 7]*Q[13] + P[11]*Q[14] + P[15]*Q[15];
	}

}
// キョン修正：終わり


int WINAPI WinMain(HINSTANCE hInstance,
										HINSTANCE hPrevInstance,
										LPSTR lpCmdLine,
										int nCmdShow)
{
		WNDCLASSEX wcex;
		HWND hwnd;
		HDC hDC;
		HGLRC hRC;
		MSG msg;
		BOOL bQuit = FALSE;
		float theta = 0.0f;

		/* register window class */
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_OWNDC;
		wcex.lpfnWndProc = WindowProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wcex.lpszMenuName = NULL;
		wcex.lpszClassName = "GLSample";
		wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);;


		if (!RegisterClassEx(&wcex))
				return 0;

		/* create main window */
		hwnd = CreateWindowEx(0,
													"GLSample",
													"OpenGL Sample",
													WS_OVERLAPPEDWINDOW,
													CW_USEDEFAULT,
													CW_USEDEFAULT,
													256,
													256,
													NULL,
													NULL,
													hInstance,
													NULL);

		ShowWindow(hwnd, nCmdShow);

		/* enable OpenGL for the window */
		EnableOpenGL(hwnd, &hDC, &hRC);

		/* program main loop */
		while (!bQuit)
		{
				/* check for messages */
				if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
				{
						/* handle or dispatch messages */
						if (msg.message == WM_QUIT)
						{
								bQuit = TRUE;
						}
						else
						{
								TranslateMessage(&msg);
								DispatchMessage(&msg);
						}
				}
				else
				{
						/* OpenGL animation code goes here */

						// Enable depth testing/
						glEnable(GL_DEPTH_TEST);

						glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
						//glClear(GL_COLOR_BUFFER_BIT); // must clear depth buffer too .
						glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); // 	| GL_STENCIL_BUFFER_BIT

						// キョン修正：始まる
						RECT rc = { 0, 0, 0, 0 };
						GetClientRect( hwnd, &rc );
						glViewport( 0, 0, rc.right, rc.bottom );

						float projMatrix[ 16 ];
						const float fov = 60.0f;
						const float w = ( float )rc.right;  // get window width
						const float h = ( float )rc.bottom; // get window height
						const float aspect = w/h;
						const float zn = 1.0f;
						const float zf = 4096.0f;
						KYON::LoadPerspective( projMatrix, fov, aspect, zn, zf );

						glMatrixMode( GL_PROJECTION );
						glLoadMatrixf( ( const GLfloat * )projMatrix );
						// キョン修正：終わり


						glMatrixMode( GL_MODELVIEW );

						glPushMatrix();
						glLoadIdentity();
						glTranslatef(0.0f, 0.0f, 5.0f); //キョン修正：need to place in front of "camera"
						glRotatef(theta, 0.0f, 0.0f, 1.0f);

						glBegin(GL_TRIANGLES); // GL_TRIANGLES / GL_POLYGON

								glColor3f(1.0f, 0.0f, 0.0f); 	glVertex3f(0.0f, 	1.0f, 0.0f ); 		// glVertex2f
								glColor3f(0.0f, 1.0f, 0.0f); 	glVertex3f(0.87f, 	-0.5f, (theta -180.0f)/18.0f );
								glColor3f(0.0f, 0.0f, 1.0f); 	glVertex3f(-0.87f, -0.5f, 0.0f );

						glEnd();

						glPopMatrix();

						//glFlush();

						SwapBuffers(hDC);

						theta += 1.0f;
						if ( theta > 360.0f ) theta = 0.0f ;
						Sleep (23);

						/* OpenGL animation code ends here */
				}
		}

		/* shutdown OpenGL */
		DisableOpenGL(hwnd, hDC, hRC);

		/* destroy the window explicitly */
		DestroyWindow(hwnd);

		return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
		switch (uMsg)
		{
				case WM_CLOSE:
						PostQuitMessage(0);
				break;

				case WM_DESTROY:
						return 0;

				case WM_KEYDOWN:
				{
						switch (wParam)
						{
								case VK_ESCAPE:
										PostQuitMessage(0);
								break;
						}
				}
				break;

				default:
						return DefWindowProc(hwnd, uMsg, wParam, lParam);
		}

		return 0;
}

void EnableOpenGL(HWND hwnd, HDC* hDC, HGLRC* hRC)
{
		PIXELFORMATDESCRIPTOR pfd;

		int iFormat;

		/* get the device context (DC) */
		*hDC = GetDC(hwnd);

		/* set the pixel format for the DC */
		ZeroMemory(&pfd, sizeof(pfd));

		pfd.nSize = sizeof(pfd);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW |
									PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 24;
		pfd.cDepthBits = 32;
		pfd.iLayerType = PFD_MAIN_PLANE;

		iFormat = ChoosePixelFormat(*hDC, &pfd);

		SetPixelFormat(*hDC, iFormat, &pfd);

		/* create and enable the render context (RC) */
		*hRC = wglCreateContext(*hDC);

		wglMakeCurrent(*hDC, *hRC);
}

void DisableOpenGL (HWND hwnd, HDC hDC, HGLRC hRC)
{
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(hRC);
		ReleaseDC(hwnd, hDC);
}

