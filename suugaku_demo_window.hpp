﻿#pragma once

bool InitWindow();
void FiniWindow();

unsigned GetWindowResX();
unsigned GetWindowResY();

bool ProcessWindowEvents();

#ifdef _WIN32
# undef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN
# include <Windows.h>
# undef min
# undef max
HWND GetWindowHandle_Win32();
#endif

void SwapBuffers();

// Keyboard key scancode
enum EKey
{
	EKey_Escape             = 1,                //!< escape key (US KBD)
	EKey_One                = 2,                //!< '1'/'!' key (US KBD)
	EKey_Two                = 3,                //!< '2'/'@' key (US KBD)
	EKey_Three              = 4,                //!< '3'/'#' key (US KBD)
	EKey_Four               = 5,                //!< '4'/'$' key (US KBD)
	EKey_Five               = 6,                //!< '5'/'%' key (US KBD)
	EKey_Six                = 7,                //!< '6'/'^' key (US KBD)
	EKey_Seven              = 8,                //!< '7'/'&' key (US KBD)
	EKey_Eight              = 9,                //!< '8'/'*' key (US KBD)
	EKey_Nine               = 10,               //!< '9'/'(' key (US KBD)
	EKey_Zero               = 11,               //!< '0'/')' key (US KBD)
	EKey_Minus              = 12,               //!< '-'/'_' key (US KBD)
	EKey_Equals             = 13,               //!< '='/'+' key (US KBD)
	EKey_Back               = 14,               //!< backspace key (US KBD)
	EKey_Tab                = 15,               //!< tab key (US KBD)
	EKey_Q                  = 16,               //!< 'q'/'Q' key (US KBD)
	EKey_W                  = 17,               //!< 'w'/'W' key (US KBD)
	EKey_E                  = 18,               //!< 'e'/'E' key (US KBD)
	EKey_R                  = 19,               //!< 'r'/'R' key (US KBD)
	EKey_T                  = 20,               //!< 't'/'T' key (US KBD)
	EKey_Y                  = 21,               //!< 'y'/'Y' key (US KBD)
	EKey_U                  = 22,               //!< 'u'/'U' key (US KBD)
	EKey_I                  = 23,               //!< 'i'/'I' key (US KBD)
	EKey_O                  = 24,               //!< 'o'/'O' key (US KBD)
	EKey_P                  = 25,               //!< 'p'/'P' key (US KBD)
	EKey_LBracket           = 26,               //!< '['/'{' key (US KBD)
	EKey_RBracket           = 27,               //!< ']'/'}' key (US KBD)
	EKey_Return             = 28,               //!< enter/return key (US KBD)
	EKey_LControl           = 29,               //!< left control key (US KBD)
	EKey_A                  = 30,               //!< 'a'/'A' key (US KBD)
	EKey_S                  = 31,               //!< 's'/'S' key (US KBD)
	EKey_D                  = 32,               //!< 'd'/'D' key (US KBD)
	EKey_F                  = 33,               //!< 'f'/'F' key (US KBD)
	EKey_G                  = 34,               //!< 'g'/'G' key (US KBD)
	EKey_H                  = 35,               //!< 'h'/'H' key (US KBD)
	EKey_J                  = 36,               //!< 'j'/'J' key (US KBD)
	EKey_K                  = 37,               //!< 'k'/'K' key (US KBD)
	EKey_L                  = 38,               //!< 'l'/'L' key (US KBD)
	EKey_Semicolon          = 39,               //!< ';'/':' key (US KBD)
	EKey_Apostrophe         = 40,               //!< '''/'"' key (US KBD)
	EKey_Grave              = 41,               //!<
	EKey_LShift             = 42,               //!< left shift key (US KBD)
	EKey_Backslash          = 43,               //!< '\'/'|' key (US KBD)
	EKey_Z                  = 44,               //!< 'z'/'Z' key (US KBD)
	EKey_X                  = 45,               //!< 'x'/'X' key (US KBD)
	EKey_C                  = 46,               //!< 'c'/'C' key (US KBD)
	EKey_V                  = 47,               //!< 'v'/'V' key (US KBD)
	EKey_B                  = 48,               //!< 'b'/'B' key (US KBD)
	EKey_N                  = 49,               //!< 'n'/'N' key (US KBD)
	EKey_M                  = 50,               //!< 'm'/'M' key (US KBD)
	EKey_Comma              = 51,               //!< ','/'<' key (US KBD)
	EKey_Period             = 52,               //!< '.'/'>' key (US KBD)
	EKey_Slash              = 53,               //!< '/'/'?' key (US KBD)
	EKey_RShift             = 54,               //!< right shift key (US KBD)
	EKey_Multiply           = 55,               //!< '*' (numpad) key (US KBD)
	EKey_LMenu              = 56,               //!< left alt key (US KBD)
	EKey_Space              = 57,               //!< space bar key (US KBD)
	EKey_Capital            = 58,               //!< caps-lock key (US KBD)
	EKey_F1                 = 59,               //!< F1 key (US KBD)
	EKey_F2                 = 60,               //!< F2 key (US KBD)
	EKey_F3                 = 61,               //!< F3 key (US KBD)
	EKey_F4                 = 62,               //!< F4 key (US KBD)
	EKey_F5                 = 63,               //!< F5 key (US KBD)
	EKey_F6                 = 64,               //!< F6 key (US KBD)
	EKey_F7                 = 65,               //!< F7 key (US KBD)
	EKey_F8                 = 66,               //!< F8 key (US KBD)
	EKey_F9                 = 67,               //!< F9 key (US KBD)
	EKey_F10                = 68,               //!< F10 key (US KBD)
	EKey_NumLock            = 69,               //!< num-lock key (US KBD)
	EKey_Scroll             = 70,               //!< scroll-lock key (US KBD)
	EKey_NumPad7            = 71,               //!< '7' (numpad) key (US KBD)
	EKey_NumPad8            = 72,               //!< '8' (numpad) key (US KBD)
	EKey_NumPad9            = 73,               //!< '9' (numpad) key (US KBD)
	EKey_Subtract           = 74,               //!< '-' (numpad) key (US KBD)
	EKey_NumPad4            = 75,               //!< '4' (numpad) key (US KBD)
	EKey_NumPad5            = 76,               //!< '5' (numpad) key (US KBD)
	EKey_NumPad6            = 77,               //!< '6' (numpad) key (US KBD)
	EKey_Add                = 78,               //!< '+' (numpad) key (US KBD)
	EKey_NumPad1            = 79,               //!< '1' (numpad) key (US KBD)
	EKey_NumPad2            = 80,               //!< '2' (numpad) key (US KBD)
	EKey_NumPad3            = 81,               //!< '3' (numpad) key (US KBD)
	EKey_NumPad0            = 82,               //!< '0' (numpad) key (US KBD)
	EKey_Decimal            = 83,               //!< '.' (numpad) key (US KBD)
	EKey_Oem102             = 86,               //!<
	EKey_F11                = 87,               //!< F11 key (US KBD)
	EKey_F12                = 88,               //!< F12 key (US KBD)
	EKey_F13                = 100,              //!< F13 key (US KBD)
	EKey_F14                = 101,              //!< F14 key (US KBD)
	EKey_F15                = 102,              //!< F15 key (US KBD)
	EKey_Kana               = 112,              //!<
	EKey_AbntC1             = 115,              //!<
	EKey_Convert            = 121,              //!<
	EKey_NoConvert          = 123,              //!<
	EKey_Yen                = 125,              //!<
	EKey_AbntC2             = 126,              //!<
	EKey_NumPadEquals       = 141,              //!< '=' (numpad) key (US KBD)
	EKey_PrevTrack          = 144,              //!<
	EKey_At                 = 145,              //!< '@' (alternate) key (US KBD)
	EKey_Colon              = 146,              //!< ':' (alternate) key (US KBD)
	EKey_Underline          = 147,              //!< '_' (alternate) key (US KBD)
	EKey_Kanji              = 148,              //!<
	EKey_Stop               = 149,              //!<
	EKey_Ax                 = 150,              //!<
	EKey_Unlabeled          = 151,              //!<
	EKey_NextTrack          = 153,              //!<
	EKey_NumPadEnter        = 156,              //!< enter (numpad) key (US KBD)
	EKey_RControl           = 157,              //!< right control key (US KBD)
	EKey_Mute               = 160,              //!<
	EKey_Calculator         = 161,              //!<
	EKey_PlayPause          = 162,              //!<
	EKey_MediaStop          = 164,              //!<
	EKey_VolumeDown         = 174,              //!<
	EKey_VolumeUp           = 176,              //!<
	EKey_WebHome            = 178,              //!<
	EKey_NumPadComma        = 179,              //!< ',' (numpad) key (US KBD)
	EKey_Divide             = 181,              //!< '/' (numpad) key (US KBD)
	EKey_SysRq              = 183,              //!<
	EKey_RMenu              = 184,              //!< right alt key (US KBD)
	EKey_Pause              = 197,              //!< pause key (US KBD)
	EKey_Home               = 199,              //!< home key (US KBD)
	EKey_Up                 = 200,              //!< up arrow key (US KBD)
	EKey_Prior              = 201,              //!< page up key (US KBD)
	EKey_Left               = 203,              //!< left arrow key (US KBD)
	EKey_Right              = 205,              //!< right arrow key (US KBD)
	EKey_End                = 207,              //!< end key (US KBD)
	EKey_Down               = 208,              //!< down key (US KBD)
	EKey_Next               = 209,              //!< page down key (US KBD)
	EKey_Insert             = 210,              //!< insert key (US KBD)
	EKey_Delete             = 211,              //!< delete key (US KBD)
	EKey_LWin               = 219,              //!< left Windows key (US KBD)
	EKey_RWin               = 220,              //!< right Windows key (US KBD)
	EKey_Apps               = 221,              //!<
	EKey_Power              = 222,              //!<
	EKey_Sleep              = 223,              //!<
	EKey_Wake               = 227,              //!<
	EKey_WebSearch          = 229,              //!<
	EKey_WebFavorites       = 230,              //!<
	EKey_WebRefresh         = 231,              //!<
	EKey_WebStop            = 232,              //!<
	EKey_WebForward         = 233,              //!<
	EKey_WebBack            = 234,              //!<
	EKey_MyComputer         = 235,              //!<
	EKey_Mail               = 236,              //!<
	EKey_MediaSelect        = 237,              //!<

	EKey_Esc                = EKey_Escape,
	EKey_Backspace          = EKey_Back,
	EKey_NumPadStar         = EKey_Multiply,
	EKey_LAlt               = EKey_LMenu,
	EKey_CapsLock           = EKey_Capital,
	EKey_NumPadMinus        = EKey_Subtract,
	EKey_NumPadPlus         = EKey_Add,
	EKey_NumPadPeriod       = EKey_Decimal,
	EKey_NumPadSlash        = EKey_Divide,
	EKey_RAlt               = EKey_RMenu,
	EKey_UpArrow            = EKey_Up,
	EKey_PgUp               = EKey_Prior,
	EKey_LeftArrow          = EKey_Left,
	EKey_RightArrow         = EKey_Right,
	EKey_DownArrow          = EKey_Down,
	EKey_PgDn               = EKey_Next,
	EKey_Circumflex         = EKey_PrevTrack,
	EKey_LCtrl              = EKey_LControl,
	EKey_RCtrl              = EKey_RControl,
	EKey_LSuper             = EKey_LWin,
	EKey_RSuper             = EKey_RWin,
	EKey_LCommand           = EKey_LWin,
	EKey_RCommand           = EKey_RWin,
	EKey_LCmd               = EKey_LWin,
	EKey_RCmd               = EKey_RWin,
	EKey_LApple             = EKey_LWin,
	EKey_RApple             = EKey_RWin,
	EKey_Enter              = EKey_Return,

	EKey_UArrow             = EKey_Up,
	EKey_LArrow             = EKey_Left,
	EKey_RArrow             = EKey_Right,
	EKey_DArrow             = EKey_Down
};

bool KeyDown( EKey Key );
