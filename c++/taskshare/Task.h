#ifndef TASKSHARE_TASK_LOCAL_H
#define TASKSHARE_TASK_LOCAL_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Signal.h"
#include "Atomics.h"
#include "SysEvent.h"
#include "Semaphore.h"
#include "Threading.h"

#if !TASK_CONF_NO_DEFAULT_TASK
//# include "versions/Task5.h"
# include "versions/Task6.h"
#endif

#endif
