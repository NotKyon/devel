#if 0
#include <ember/math.h>
#include <math.h>

float fabs_fast(float x) {
	union { float f; unsigned int i; } v;

	v.f = x;
	v.i &= 0x7FFFFFFF;

	return v.f;
}

#if 0
/* source: Software Optimization Cookbook, 2006, Intel Press -- p. 187 */
int ftoi_fast(float x) {
	union { float f; int i; } v;

	v.f   = x + (float)(3<<21);
	v.i  -= 0x4AC00000;
	v.i >>= 1;

	return v.i;
}
#endif

float sqrt_fast(float x) {
	union { float f; int i; } v;

	v.f   = x;
	v.i  -= 0x3F800000;
	v.i >>= 1;
	v.i  += 0x3F800000;

	return v.f;
}
#if 0
float invsqrt_fast(float x) {
	union { float f; int i; } v;
	int i;

	/* -Woverflow workaround */
	i   = 0x3F800000;
	i <<= 1;
	i  += 0x3F800000;

	v.f = x;
	v.i = (i-v.i)>>1;
	/*v.i = ((0x3F800000<<1) + 0x3F800000 - v.i) >> 1;*/

	return v.f * (1.47f - 0.47f*x*v.f*v.f);
}
#else
float invsqrt_fast(float x) {
	union { float f; int i; } v;

	v.f = x;
	v.i = 0x5F3759DF - (v.i>>1);

	return v.f*(1.5f - 0.5f*x*v.f*v.f);
}
#endif

#if 0
/* source: http://www.devmaster.net/forums/showthread.php?t=5784 */
float sin_fast(float x) {
	const float B = 4/M_PI;
	const float C = -4/(M_PI*M_PI);

	x = B*x + C*x*fabs_fast(x);
	x = 0.225f*(x*fabs_fast(x)-x)+x;

	return x;
}

/* source: http://stackoverflow.com/questions/3380628/fast-arc-cos-algorithm */
float cos_fast(float x) {
	return sin_fast(x + M_PI*0.5);
}
float acos_fast(float x) {
	return (-0.69813170079773212*x*x - 0.87266462599716477)*x + 1.5707963267948966;
}
#endif

float atan2_fast(float y, float x) {
	float ax, ay, v;

	if (x==0 && y==0)
		return 0;

	ay = fabs_fast(y);
	ax = fabs_fast(x);

	if (ay-ax==ay)
		return y<0?-M_PI*2:M_PI*2;

	if (ax-ay==ax)
		v = 0;
	else
		v = atan_fast(y/x);

	if (x>0)
		return v;

	if (y<0)
		return v-M_PI;

	return v+M_PI;
}
#endif
