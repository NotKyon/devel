#ifndef GB_BASE_ENGINE_H
#define GB_BASE_ENGINE_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Engine Information */
const char *gbEngineName();
int gbEngineMajorVersion();
int gbEngineMinorVersion();
gb_bool_t gbEngineDebugBuild();
const char *gbEngineBuildDate();
const char *gbEngineBuildTime();

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
