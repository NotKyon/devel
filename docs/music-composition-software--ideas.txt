﻿Music Composition Software Ideas
================================

General Philosophy
------------------
- Visually assist the user in composing their music.
- Complete control over how everything sounds and fits together.
- One-click note placement, and note painting.
- Fiddling with the composition should be easy.
- User should not be restricted to one way of doing things.
- NOT a tool for doing live concerts (techno or otherwise)
- NOT meant for just reading or writing guitar music


Software Implementation Notes
-----------------------------
- Support multiple monitors or die.
- Not written in Delphi, .NET, Java, BASIC, Rust, Go, Lua, HTML/CSS/JS/PHP, or
  any variation of any of those languages, unless the variation is under our
  direct control.
- Neither C nor C++ should be the "final grounds" for where the software is
  written. Preferably, the software should be primarily written in Lain.
- Run on Windows 7+, Linux (X11/Wayland), and Mac OS X (10.6+).
--- If supporting an older OS version is detrimental to development then don't
`   bother.


2014-12-06(23:34) Aaron
-----------------------

「Snap-to-scale」:: Forces all placeable notes to only be on the currently set
scale (with something like Alt+LMB to ignore this setting).

「Note Suggestions」:: "Analyze" the current and previous bars to suggest the
next note. (e.g., another set on the current time to complete a chord, or the
next note to continue the bar.)

「Note rules/margins」:: Allow custom rules to be specified for displaying
alignment-lines/margins. (e.g., an octave's difference, or whole-tones only, or
based on a template or notes within the scale.)

「One-click note placement」:: Q-Midi-styled "painting" of notes. Changing
timing (e.g., 1/8th → 1/4th) should be a hot-key (e.g., w, h, q, e, s, t, x) or
mouse gesture (e.g., mouse wheel or middle-click + drag).

「"Patch" & "Diff"」:: Ability to mark a set of notes as being based on (or
relative-to) another set of notes, with a set of customizable rules for
determining how to adjust. (e.g., "relative to scale" rather than "relative to
notes.")

「Chord Browser」:: Chord browser should be available with filter settings for,
e.g., current scale; major/minor; root note; only made of the notes in the
current bar (or some variation thereof).

「"Sound Shaders"」:: Enter code in directly for how any instrument should sound
(or effect, or mixer, etc).

「Generate」:: System that can generate a random melody (to help the composer).
Various rules can be used here.

「Specialized Drum Interface」:: Laying the track for drums (when using a
drum-kit) should use standard drum scoring notation (with support for custom
notations, e.g., for non-standard sound effects).

「Custom "Sound Renderers" By Parameter」:: An instrument can have custom sound
renderers (e.g., audio file/synth) assigned to note ranges, velocities, and
other attributes (e.g., "harmonic," "muted," "plucked," "slapped," etc).

「Specialized String Instrument/Guitar Interface」:: A tablature editor (for
stringed instruments by default, but not restricted to them) should be
supported.

「Node-based Mixer」:: Mixer can be set-up as a flow-editor, directing which
tracks/channels output to which mixers, and where those mixers send their output
(e.g., to a reverb effect; to an equalizer; to the generic output).




2014-12-08(21:02) Aaron
-----------------------

「Timing Templates」:: A mode that automatically places "rest" notes with the
timing (e.g., ESSEQQ) of the last bar, or some sequence of bars, when a new bar
is made would be useful for quickly laying out or continuing a melody.

「Note Coloring」:: Having the ability to give specific notes a color based on
their distance from the previous note (or how "sharp" or "soft" the note will
sound based on the distance, which can be modelled algorithmically, or by user
configuration) would be useful. For example, a note could appear "green" when
it's two whole-tones away. If it were only one semi-tone away it could appear
red (due to being very dissonant). The user could configure this or not use the
feature at all. (Opt-in, not opt-out. i.e., off by default.)

「Chord Coloring」:: When constructing a chord, coloring the individual notes of
the chord according to some (user-specified) rule would be useful. For example,
some chords follow the pattern of 0-8-12 (0 being a root note, with 12 being an
octave up) and other similar rules. Showing how close a note is to a given
pattern by coloring it (closer to, e.g., "green" for "correct" and closer to,
e.g., "red" for "off").

「Chord Prefabs」:: User could set-up certain bars in the composition to have
chords follow a certain pattern. Being able to "paint" chords onto the
composition easily would be useful. (This would be like placing a prefab.)




2014-12-30(18:31) Aaron
-----------------------

「Save For Later」:: System accessible from right-clicking a selection that
allows you to save musical ideas / snippets for other compositions.

「Version Control」:: Multiple versions of a work can be stored inside the file,
or separated out into additional files, enabling VCS-like revert/diff/etc
support for a piece.

「Wave Graph For Synthesizers」:: For the "sound shaders" (direct code) and
general synthesizers (knobs controlling oscillators and such) there should be a
waveform graph that will visually show the sound for a few phases.

## How difficult would it be to allow code to be suggested from the user
`  attempting to manipulate the shown waveform?

「FFT Display」:: Show the individual bands (as done in Foobar2000) for various
sounds. (Especially individual sound shaders / synthesizers.)

「General Notes and Diagrams」:: Enable drawing text and diagrams (e.g., lines,
curves, etc) directly into the editor, as a visual aid for the composer. These
are manual drawings, as opposed to the automated ones the software would
optionally perform.

「Custom Pitch Notes」:: Enter a specific frequency for any note. (e.g., 430Hz.)
Setting the frequency can be done on individual instances of a given note, or
with defaults applied to a track or a whole project, etc.

