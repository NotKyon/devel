#ifndef STRING_H
#define STRING_H

#include "local.h"

typedef struct string_s {
	size_t capacity, length;
	char *p;
} string_t;

void InitString(string_t *str);
void FiniString(string_t *str);

void ResizeString(string_t *str, size_t len);

void PushChar(string_t *str, char ch);
void PushStringN(string_t *str, const char *s, size_t n);
void PushString(string_t *str, const char *s);

size_t StringCapacity(const string_t *str);
size_t StringLength(const string_t *str);
const char *StringBuffer(const string_t *str);
char *StringData(string_t *str);

void SetStringN(string_t *str, const char *s, size_t n);
void SetString(string_t *str, const char *s);

#endif
