MEMORY ASSOCIATIONS
===================

Recalling Words
---------------
Key types: meaning, emotion.

Finding synonyms is relatively easy: link each one or have a single "virtual"
entry that lists all of the words, with each word linking to that entry. (Much
less links; more compact.)

Linking an "intention" to a word is more difficult. Some thoughts, ideas, and
theories:

 - Intentions require (or at least benefit from) context
 --- Is there a concrete definition of "context" though? How can it be defined
     in an "algorithmic manner?"
 - Emotions can hint at intentions, which may serve as attributes for the words
   or items being recalled. For each emotion a list of related words could be
   stored.


Words
-----
Key types: spelling, sounds.
Links to: meanings (see Word Meaning below).

Collections of similes per-word may be useful.

Linking the reactions objects had to words, to those words would be useful in
determining whether certain words could be used when responding to queries from
those objects.


Word Meaning
------------
Key types: ???

[Note: Each word could have multiple meanings]

	Webster's Dictionary (ISBN:1582793921)
	--------------------------------------
	meaning:
		adj. revealing; having purpose
		n. significance or purpose; that which is understood
		<< adj. meaningful
		<< adv. meaningfully, meaningly
		<< n. meaningfulness
	meaningless:
		adj. without purpose; insignificant
		<< adv. meaninglessly
		<< n. meaninglessness
	purpose:
		n. an aim or goal; an intention
	intention:
		n. aim; purpose
	intentional:
		adj. deliberate; planned
		<< adv. intentionally
	intent:
		adj. firmly fixed in the mind
		n. something intended
		<< adv. intently
	intend:
		vt. to have in mind, as for a particular use or plan of action
	intended:
		adj. intentional
	aim:
		vi. to have a purpose; aim to improve; to point, as a firearm
		vt. to point at or act against a person, object, etc.;
			"aim your remarks to the audience"
		n. the act of aiming an intention

Meaning may require a use-case (how the meaning is supposed to be used). In the
above dictionary excerpts we see meanings based on how the word is used (noun,
adjective, adverb, etc).

Meanings are probably best associated with abstract nodes -- not hard-wired
nodes. For example, one meaning could be a particular "emotion." (Which itself
_could_, but not necessarily would be, considered "positive" or "negative.")
Other meanings would be patterns or chains of meanings, which might draw upon
past experience (whether real or fictional). For example, "loquacious" could be
linked to a person (object) talking (action) too much (degree), which could
itself also be linked to an emotional reaction to that particular experience.

How would a word like "dangerous" be prescribed a meaning?
- This word is negative, so a negative emotion (unwanted) would be used.
- That alone isn't enough to fully understand the word though. Something is
  dangerous because it could cause harm to the affected object's well-being.
  How would that be encoded?
--- LifeAI's system of stats would make sense here. To reference a virtual stat
    such as "well-being" (which would be composed of a number of other stats
    such as "physical health," "mental health," "social health," "net wealth,"
    etc) and how it might be affected could be used.
--- An "experiences" system (lists of situations tagged with "dangerous" as a
    description, showing how various systems were affected) would be useful in
    this case.

How would a word like "is" or "the" be prescribed a meaning?
- These words are used only for grammar to indicate what something is. Their use
  comes from the "rules of the language," and so would link back to whatever the
  language is and the rules for that language.
--- The "rules" still have to be encoded somehow, even if the meaning just leads
    to another node, the node has to be deciphered in the context of the
    meaning.
--- A general description of it is needed as well, but this can probably be
    accomplished by a procedural construction based on where each node is
    linked.
------ Trivia (e.g., Bill Clinton's "What the definition of 'is' is") should be
       linked to words, meanings, etc., as well.
------ If a query is presented to the program too often or repeated too quickly,
       if the desire is to appear as human as possible, then the response should
       be varied and the act of repeatedly taking away the system's time should
       be "annoying" to it. (As when Bill Gates' mother asked what he was doing
       and he responded with "Thinking, mother. You should try it some time.")

How should erratic use of meanings be dealt with?
- Context of the situation is needed.
- Prior experience in similar situations could be drawn upon for prediction and
  error correction.
- "Similarities" between words could be drawn upon. (Phonetic links between
  words? Words would have their pronunciations encoded.)

** MEANING LINKS **
Each meaning would have to link back to each word that references it. This would
enable translation between different languages.
