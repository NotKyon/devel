//
//	The built-in job streaming routines provide a mechanism for exploiting
//	scalable cross-platform parallelism/concurrency in your application. The
//	other built-in systems utilize the job streamer internally for maximum
//	performance.
//
//	Unlike other systems where a fixed number of threads are used, this system
//	creates as many threads as there are hardware threads. (In some cases more,
//	if the system determines that it would be beneficial for your system.) The
//	internal code is then broken up into a bunch of smaller jobs which can be
//	executed efficiently on the job streamer.
//
//	Here's an example of why this is better. Assume there were one thread for
//	each subsystem. e.g., one for A.I., one for rendering, one for physics, one
//	for sound processing, one for networking, and one for miscellaneous updates.
//	That's six threads in total. On a dual-core machine, six threads is too
//	high. Only two can run at a time, meaning there will be a couple of
//	unnecessary context switches, as well as potential cache invalidation as
//	threads move between cores. (The latter isn't an issue if an affinity is
//	set, but that may cause inefficient resource allocation.)
//
//	Now let's take that example on an eight-, sixteen-, or even thirtytwo-core
//	system. Because the threads are fixed in size, they can't take advantage of
//	this type of system.
//
//	The job streamer solves both of these cases by allocating however many
//	threads it determines are necessary (usually the number of hardware threads
//	available in your system). Then, with each job broken up to be as small as
//	possible, the tasks can be divided efficiently among the available cores.
//
//	This example demonstrates how you can use this system (it's easy!) to
//	determine how to find which numbers are prime within a subset of numbers.
//	There are more uses to this system, however.
//
#include <AxTek/Threading/CCommandBuffer.h>
#include <AxTek/Threading/CThread.h>
#include <AxTek/Threading/CPU.h>

#include "IsPrime.h"

#include <stdio.h>
#include <stdlib.h>

//
//	We need to choose how many tests we're going to perform; 1024 is default.
//
#define NUM_TESTS 1024
//#define NUM_TESTS 16

//
//	Here are the numbers we'll test against.
//
int g_numbers[NUM_TESTS];

//
//	Job stream function
//
ax::task::result_t THREADPROCAPI is_prime_f(void *p) {
	*(int *)p = is_prime(*(ax::uint_t *)p);
	return (ax::task::result_t)0;
}

//
//	Perform the test
//
int main() {
	//struct { ax::u64 sec, nsec; } s, e; //timer query
	ax::task::CCommandBuffer cmdbuf;
	ax::task::CThread threads[32];
	ax::uint_t i, n;

	// determine how many threads to use, then initialize them
	n = ax::task::get_cpu_count();
	for(i=0; i<n; i++)
		threads[i].Init(ax::task::CCommandBuffer::streamer_f, &cmdbuf);

	// take the starting time
	//gbQueryPerfTimer(&s.sec, &s.nsec);

	// start streaming
	for(i=0; i<NUM_TESTS; i++) {
		g_numbers[i] = 100000000 + i;
		cmdbuf.Dispatch(is_prime_f, &g_numbers[i]);
	}

	// wait for streaming to complete
	cmdbuf.Sync();

	// take the ending time
	//gbQueryPerfTimer(&e.sec, &e.nsec);

	// note how long it took
	/*gbPrint(gbF("%u.%u",
		(gb_uint_t)(e.sec - s.sec),
		(gb_uint_t)(e.nsec - s.nsec)));*/

	// when done with the command buffer, you have to sync the streams (other-
	// wise the threads might try to join first)
	cmdbuf.SyncStreams();

	// done
	return EXIT_SUCCESS;
}
