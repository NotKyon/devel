#ifndef LOCAL_ISPRIME_H
#define LOCAL_ISPRIME_H

#if _MSC_VER > 1000
# pragma once
#endif

//
//	Simple primality test, good for our function
//
__forceinline bool is_prime(ax::uint_t number) {
	ax::uint_t i;

	// check a small sample set of known non-primes
	if (number <= 2)
		return false;

	// even numbers aren't primes
	if (~number & 1)
		return false;

	// enumerate the numbers and check for even divisibility
	// NOTE: doing this the long way for demonstration purposes
	for(i=2; i<number; i++) {
		if (number%i == 0)
			return false;
	}

	// appears to be a prime
	return true;
}

#endif
