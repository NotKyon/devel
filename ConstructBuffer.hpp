﻿#include <stddef.h>

namespace Ax { namespace Detail { struct SPlcmntNw {}; } }

inline void *operator new( size_t, void *p, Ax::Detail::SPlcmntNw )
{
	return p;
}
inline void operator delete( void *, void *, Ax::Detail::SPlcmntNw ) throw()
{
}

namespace Ax
{

	template< typename tClass >
	class TConstructBuffer
	{
	public:
		static const size_t kSizeInBytes = sizeof( tClass );

		inline tClass *Ptr()
		{
			return ( tClass * )&m_Data[ 0 ];
		}
		inline const tClass *Ptr() const
		{
			return ( const tClass * )&m_Data[ 0 ];
		}

		inline void Construct()
		{
			new( ( void * )Ptr(), Ax::Detail::SPlcmntNw() ) tClass();
		}
		inline void Destruct()
		{
			Ptr()->~tClass();
		}

		inline tClass *operator->()
		{
			return ( tClass * )&m_Data[ 0 ];
		}
		inline const tClass *operator->() const
		{
			return ( const tClass * )&m_Data[ 0 ];
		}

		inline tClass &operator*()
		{
			return *( tClass * )&m_Data[ 0 ];
		}
		inline const tClass &operator*() const
		{
			return *( const tClass * )&m_Data[ 0 ];
		}

	private:
		unsigned char m_Data[ kSizeInBytes ];
	};

}
