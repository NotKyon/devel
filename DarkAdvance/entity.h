#ifndef ENTITY_H
#define ENTITY_H

#include "export.h"
#include "matrix.h"
#include <stddef.h>

#ifndef __bitfield_t_defined__
# define __bitfield_t_defined__ 1
typedef size_t bitfield_t;
#endif

typedef struct entity_s *entity_t;

enum {
	kEntity_RecalcGlobalTransform_Bit = 0x01,
	kEntity_RecalcPostTransform_Bit = 0x02,
	kEntity_RecalcLocalRot_Bit = 0x04,
	kEntity_RecalcGlobalRot_Bit = 0x08,
	kEntity_RecalcPostRot_Bit = 0x10,
	kEntity_RecalcOffsetRot_Bit = 0x20
};

struct entity_s {
	//hierarchy
	entity_t	prnt;
	entity_t	head, tail;
	entity_t	prev, next;
	entity_t	*p_head, *p_tail;

	//transformation matrices
	matrix_t	matLocal;
	matrix_t	matOffset;
	matrix_t	matGlobal;
	matrix_t	matPost;

	//transformation flags
	bitfield_t	tformFlags;

	//transformation queries
	vec3_t		vecLocalRot;
	vec3_t		vecOffsetRot;
	vec3_t		vecGlobalRot;
	vec3_t		vecPostRot;

	//information
	char		*name;
	char		*text;
};
extern entity_t gEntityHead;
extern entity_t gEntityTail;

// Entity Management
EXPORT entity_t NewEntity(entity_t prnt);
EXPORT entity_t DeleteEntity(entity_t ent);
EXPORT void DeleteAllEntities();
EXPORT entity_t FirstEntity();
EXPORT entity_t LastEntity();
EXPORT entity_t EntityBefore(entity_t ent);
EXPORT entity_t EntityAfter(entity_t ent);
EXPORT entity_t FirstChildEntity(entity_t prnt);
EXPORT entity_t LastChildEntity(entity_t prnt);
EXPORT entity_t EntityParent(entity_t ent);
EXPORT int IsEntityDescendantOf(entity_t ent, entity_t prnt);
EXPORT void SetEntityParent(entity_t ent, entity_t prnt);

// Entity Matrix Management
EXPORT void SetEntityMatrix(entity_t ent, const matrix_t *src);
EXPORT void SetEntityOffsetMatrix(entity_t ent, const matrix_t *src);
EXPORT void SetEntityGlobalMatrix(entity_t ent, const matrix_t *src);
EXPORT matrix_t *GetEntityMatrix(entity_t ent);
EXPORT matrix_t *GetEntityOffsetMatrix(entity_t ent);
EXPORT matrix_t *GetEntityGlobalMatrix(entity_t ent);
EXPORT matrix_t *GetEntityPostMatrix(entity_t ent);
EXPORT void CalculateEntityGlobalInverseMatrix(matrix_t *dst, entity_t ent);
EXPORT void EntityLocalToGlobal(entity_t ent);

// Entity Rotation Vector Management
EXPORT void SetEntityRotation(entity_t ent, const vec3_t *v);
EXPORT void SetEntityOffsetRotation(entity_t ent, const vec3_t *v);
EXPORT void SetEntityGlobalRotation(entity_t ent, const vec3_t *v);
EXPORT vec3_t *GetEntityRotation(entity_t ent);
EXPORT vec3_t *GetEntityOffsetRotation(entity_t ent);
EXPORT vec3_t *GetEntityGlobalRotation(entity_t ent);
EXPORT vec3_t *GetEntityPostRotation(entity_t ent);

// Entity Translation Management
EXPORT void SetEntityPosition(entity_t ent, const vec3_t *v);
EXPORT void SetEntityOffsetPosition(entity_t ent, const vec3_t *v);
EXPORT void SetEntityGlobalPosition(entity_t ent, const vec3_t *v);
EXPORT vec3_t *GetEntityPosition(entity_t ent);
EXPORT vec3_t *GetEntityOffsetPosition(entity_t ent);
EXPORT vec3_t *GetEntityGlobalPosition(entity_t ent);
EXPORT vec3_t *GetEntityPostPosition(entity_t ent);

// Entity Scale Management
EXPORT void SetEntityScale(entity_t ent, const vec3_t *v);
EXPORT void SetEntityOffsetScale(entity_t ent, const vec3_t *v);
EXPORT void CalculateEntityScale(vec3_t *dst, entity_t ent);
EXPORT void CalculateEntityOffsetScale(vec3_t *dst, entity_t ent);
EXPORT void CalculateEntityGlobalScale(vec3_t *dst, entity_t ent);
EXPORT void CalculateEntityPostScale(vec3_t *dst, entity_t ent);

// Entity Local Transformations
EXPORT void MoveEntity(entity_t ent, const vec3_t *v);
EXPORT void MoveEntityX(entity_t ent, float x);
EXPORT void MoveEntityY(entity_t ent, float y);
EXPORT void MoveEntityZ(entity_t ent, float z);
EXPORT void TurnEntity(entity_t ent, const vec3_t *v);
EXPORT void TurnEntityX(entity_t ent, float x);
EXPORT void TurnEntityY(entity_t ent, float y);
EXPORT void TurnEntityZ(entity_t ent, float z);
EXPORT void ScaleEntity(entity_t ent, const vec3_t *v);
EXPORT void ScaleEntityX(entity_t ent, float x);
EXPORT void ScaleEntityY(entity_t ent, float y);
EXPORT void ScaleEntityZ(entity_t ent, float z);

// Entity Offset Transformations
EXPORT void MoveEntityOffset(entity_t ent, const vec3_t *v);
EXPORT void MoveEntityOffsetX(entity_t ent, float x);
EXPORT void MoveEntityOffsetY(entity_t ent, float y);
EXPORT void MoveEntityOffsetZ(entity_t ent, float z);
EXPORT void TurnEntityOffset(entity_t ent, const vec3_t *v);
EXPORT void TurnEntityOffsetX(entity_t ent, float x);
EXPORT void TurnEntityOffsetY(entity_t ent, float y);
EXPORT void TurnEntityOffsetZ(entity_t ent, float z);
EXPORT void ScaleEntityOffset(entity_t ent, const vec3_t *v);
EXPORT void ScaleEntityOffsetX(entity_t ent, float x);
EXPORT void ScaleEntityOffsetY(entity_t ent, float y);
EXPORT void ScaleEntityOffsetZ(entity_t ent, float z);

// Entity Naming
EXPORT void SetEntityName(entity_t ent, const char *name);
EXPORT void SetEntityText(entity_t ent, const char *text);
EXPORT const char *GetEntityName(entity_t ent);
EXPORT const char *GetEntityText(entity_t ent);

#endif
