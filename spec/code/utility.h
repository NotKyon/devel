#ifndef UTILITY_H
#define UTILITY_H

#include "local.h"

const char *va(const char *format, ...);

int Shell(int show, const char *format, ...);

int GetIntegralDate();

bool_t Wildcard(const char *source, const char *pattern);

#endif
