﻿#define AXFIBER_IMPLEMENTATION
#include "ax_fiber.h"
#include <stddef.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#undef AX_KILOBYTES
#define AX_KILOBYTES(N_)			( (N_)*1024 )

#ifndef AX_FIBER_SMALL_STACK_SIZE
# define AX_FIBER_SMALL_STACK_SIZE	AX_KILOBYTES(64)
#endif
#ifndef AX_FIBER_SMALL_POOL_COUNT
# define AX_FIBER_SMALL_POOL_COUNT	128
#endif

#ifndef AX_FIBER_LARGE_STACK_SIZE
# define AX_FIBER_LARGE_STACK_SIZE	AX_KILOBYTES(512)
#endif
#ifndef AX_FIBER_LARGE_POOL_COUNT
# define AX_FIBER_LARGE_POOL_COUNT	32
#endif

namespace Ax {

	typedef axfi_fn_fiber_t FiberFn_t;

	template< typename tElement, size_t tNum >
	inline void Zero( tElement( &Arr )[ tNum ] )
	{
		memset( &Arr[0], 0, sizeof( tElement )*tNum );
	}

	namespace Detail {

		struct Fiber
		{
			axfiber_t RealFiber;

			volatile FiberFn_t pfnRoutine;
			void *volatile pRoutineData;

			bool bIsLarge;

			bool Init( unsigned uStackSize );
			void Fini();

			void Call( FiberFn_t pfnFunc, void *pData );

			static void InitThread();
			static inline void FiniThread()
			{
				axfi_fiber_to_thread();
			}
		};

		template< unsigned tPoolSize >
		inline Fiber *FindFiber( Fiber *( &Fibers )[ tPoolSize ], unsigned &cFreeFibers )
		{
			if( !cFreeFibers ) {
				return ( Fiber * )0;
			}

			return Fibers[ --cFreeFibers ];
		}

		struct FiberPool
		{
			static __thread axfiber_t g_ThreadFiber;

			Fiber SmallFibers[ AX_FIBER_SMALL_POOL_COUNT ];
			Fiber LargeFibers[ AX_FIBER_LARGE_POOL_COUNT ];

			Fiber *pFreeSmallFibers[ AX_FIBER_SMALL_POOL_COUNT ];
			Fiber *pFreeLargeFibers[ AX_FIBER_LARGE_POOL_COUNT ];
			unsigned cFreeSmallFibers;
			unsigned cFreeLargeFibers;

			// Ideally use priority queues, but this is just a test
			Fiber *pWaitingFibers[ AX_FIBER_SMALL_POOL_COUNT + AX_FIBER_LARGE_POOL_COUNT ];
			unsigned cWaitingFibers;

			static FiberPool &GetInstance();

			inline bool Init()
			{
				cFreeSmallFibers = 0;
				cFreeLargeFibers = 0;

				for( unsigned i = 0; i < AX_FIBER_SMALL_POOL_COUNT; ++i ) {
					if( SmallFibers[ i ].Init( AX_FIBER_SMALL_STACK_SIZE ) ) {
						pFreeSmallFibers[ cFreeSmallFibers++ ] = &SmallFibers[ i ];
						continue;
					}

					while( i > 0 ) {
						SmallFibers[ --i ].Fini();
					}

					cFreeSmallFibers = 0;
					return false;
				}

				for( unsigned i = 0; i < AX_FIBER_LARGE_POOL_COUNT; ++i ) {
					if( LargeFibers[ i ].Init( AX_FIBER_LARGE_STACK_SIZE ) ) {
						pFreeLargeFibers[ cFreeLargeFibers++ ] = &LargeFibers[ i ];
						continue;
					}

					while( i > 0 ) {
						LargeFibers[ --i ].Fini();
					}

					cFreeLargeFibers = 0;

					i = AX_FIBER_SMALL_POOL_COUNT;
					while( i > 0 ) {
						SmallFibers[ -- i ].Fini();
					}

					cFreeSmallFibers = 0;
					return false;
				}

				return true;
			}
			inline void Fini()
			{
				unsigned i;

				i = AX_FIBER_LARGE_POOL_COUNT;
				while( i > 0 ) {
					LargeFibers[ --i ].Fini();
				}
				cFreeLargeFibers = 0;

				i = AX_FIBER_SMALL_POOL_COUNT;
				while( i > 0 ) {
					SmallFibers[ --i ].Fini();
				}
				cFreeSmallFibers = 0;
			}

			inline Fiber *FindSmall()
			{
				return FindFiber( pFreeSmallFibers, cFreeSmallFibers );
			}
			inline Fiber *FindLarge()
			{
				return FindFiber( pFreeLargeFibers, cFreeLargeFibers );
			}

			inline void AddToWaitingList( Fiber *pFiber )
			{
				pWaitingFibers[ cWaitingFibers++ ] = pFiber;
			}
			inline Fiber *PullFromWaitingList()
			{
				if( !cWaitingFibers ) {
					return ( Fiber * )0;
				}

				Fiber *const pFiber = pWaitingFibers[ 0 ];
				pWaitingFibers[ 0 ] = pWaitingFibers[ --cWaitingFibers ];

				return pFiber;
			}

		private:
			inline FiberPool()
			{
				Zero( SmallFibers );
				Zero( LargeFibers );

				Zero( pFreeSmallFibers );
				Zero( pFreeLargeFibers );

				cFreeSmallFibers = 0;
				cFreeLargeFibers = 0;

				Zero( pWaitingFibers );
				cWaitingFibers = 0;
			}
			inline ~FiberPool()
			{
			}
		};

		__thread axfiber_t FiberPool::g_ThreadFiber;
		
		inline void Fiber::InitThread()
		{
			if( !axfi_thread_to_fiber( &FiberPool::g_ThreadFiber, ( void * )0 ) ) {
				fprintf( stderr, "ERROR: Ax::Detail::Fiber::InitThread() failed\n" );
				fflush( stderr );
				return;
			}
		}
		
		inline void Fiber::Call( FiberFn_t pfnFunc, void *pData )
		{
			pfnRoutine = pfnFunc;
			pRoutineData = pData;

			Fiber *const pCurrFiber = ( Fiber * )axfi_get_data();
			if( pCurrFiber != ( Fiber * )0 ) {
				FiberPool::GetInstance().AddToWaitingList( pCurrFiber );
			}

			axfi_switch( &RealFiber );
		}

		namespace {
			//
			//	NOTE: The management of these (to the pool) is not safe, at all
			//
			AXFIBER_ENTRY_POINT( Fiber_f, pParm )
			{
				FiberPool &ThePool = FiberPool::GetInstance();

				Fiber *const pFiber = ( Fiber * )pParm;
				if( !pFiber ) {
					return;
				}

				for(;;) {
					if( pFiber->pfnRoutine != ( FiberFn_t )0 ) {
						pFiber->pfnRoutine( pFiber->pRoutineData );

						pFiber->pRoutineData = ( void * )0;
						pFiber->pfnRoutine = ( FiberFn_t )0;

						if( pFiber->bIsLarge ) {
							ThePool.pFreeLargeFibers[ ThePool.cFreeLargeFibers++ ] = pFiber;
						} else {
							ThePool.pFreeSmallFibers[ ThePool.cFreeSmallFibers++ ] = pFiber;
						}
					}

					Fiber *const pNextFiber = ThePool.PullFromWaitingList();
					axfi_switch( !pNextFiber ? &FiberPool::g_ThreadFiber : &pNextFiber->RealFiber );
				}
			}
		} //anonymous namespace

		inline bool Fiber::Init( unsigned uStackSize )
		{
			pfnRoutine = ( FiberFn_t )0;
			pRoutineData = ( void * )0;

			bIsLarge = uStackSize > AX_FIBER_SMALL_STACK_SIZE;

			return axfi_init( &RealFiber, uStackSize, &Fiber_f, ( void * )this ) != ( axfiber_t * )0;
		}
		inline void Fiber::Fini()
		{
			axfi_fini( &RealFiber );
		}

	}

	void InitFibers()
	{
		const bool r = Detail::FiberPool::GetInstance().Init();
		assert( r && "Failed to initialize fibers" );
	}
	void FiniFibers()
	{
		Detail::FiberPool::GetInstance().Fini();
	}

	void InitThreadFibers()
	{
		Detail::Fiber::InitThread();
	}
	void FiniThreadFibers()
	{
		Detail::Fiber::FiniThread();
	}

	void RunSmallFiber( FiberFn_t pfnFunc, void *pParm = ( void * )0 )
	{
		Detail::Fiber *const pFiber = Detail::FiberPool::GetInstance().FindSmall();
		assert( pFiber != ( Detail::Fiber * )0 && "Ran out of small fibers" );

		pFiber->Call( pfnFunc, pParm );
	}
	void RunLargeFiber( FiberFn_t pfnFunc, void *pParm = ( void * )0 )
	{
		Detail::Fiber *const pFiber = Detail::FiberPool::GetInstance().FindSmall();
		assert( pFiber != ( Detail::Fiber * )0 && "Ran out of large fibers" );

		pFiber->Call( pfnFunc, pParm );
	}

}

Ax::Detail::FiberPool &Ax::Detail::FiberPool::GetInstance()
{
	static FiberPool instance;
	return instance;
}

void FiberTestB_f( void *p )
{
	const char *const pszStr = ( const char * )p;

	if( !pszStr ) {
		printf( "(null)\n" );
	} else {
		printf( "\"%s\"\n", pszStr );
	}
	fflush( stdout );
}
void FiberTestA_f( void * )
{
	printf( "+\n" ); fflush( stdout );
	Ax::RunSmallFiber( &FiberTestB_f, ( void * )"Hello, world!" );
	printf( "-\n" ); fflush( stdout );
}

int main()
{
	Ax::InitFibers();
	Ax::InitThreadFibers();

	Ax::RunSmallFiber( &FiberTestA_f );
	Ax::RunSmallFiber( &FiberTestB_f, ( void * )"Testing 1" );
	Ax::RunSmallFiber( &FiberTestB_f, ( void * )"Testing 2" );
	Ax::RunSmallFiber( &FiberTestB_f, ( void * )"Testing 3" );

	Ax::FiniThreadFibers();
	Ax::FiniFibers();
}

