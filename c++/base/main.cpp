#include "CObject.h"
#include <cstdlib>

IObject *NewObject() {
	return new CObject();
}

int main() {
	IObject *obj;

	obj = NewObject();
	obj->DoSomething();
	obj->Drop(); obj = NULL;

	return EXIT_SUCCESS;
}
