#ifndef GB_SYS_THREAD_H
#define GB_SYS_THREAD_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"
#include "../base/handle.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* CPU */
gb_bool_t gbIsHyperthreadingAvailable();
gb_u32_t gbGetCPUCoreCount();
gb_u32_t gbGetCPUThreadCount();

/* Threading */
typedef enum {
	kGBThreadPriority_Idle,
	kGBThreadPriority_VeryLow,
	kGBThreadPriority_Low,
	kGBThreadPriority_BelowNormal,
	kGBThreadPriority_Normal,
	kGBThreadPriority_AboveNormal,
	kGBThreadPriority_High,
	kGBThreadPriority_VeryHigh,
	kGBThreadPriority_Critical,

	kNumGBThreadPriorities
} gb_threadPriority_t;

typedef gb_handle_t gb_thread_t;
typedef int(*gb_fnThread_t)(void *);

gb_thread_t gbNewThread(gb_fnThread_t fnThread, void *p);
gb_thread_t gbDeleteThread(gb_thread_t thread);

gb_bool_t gbIsThreadRunning(gb_thread_t thread);
int gbGetThreadExitValue(gb_thread_t thread);

void gbSetThreadCPU(gb_thread_t thread, gb_u32_t cpu);

void gbJoinThread(gb_thread_t thread);
/*gbYield() in time.h*/

void gbSetThreadPriority(gb_thread_t thread, gb_threadPriority_t priority);

	/*
	 * TODO: Provide ability to determine current thread (EFFICIENTLY)
	 *       Provide a method to retrieve the internal handle of a thread (not
	 *       just the thread data structure)
	 */

/* Locks */
typedef gb_handle_t gb_lock_t;

gb_lock_t gbNewLock();
gb_lock_t gbDeleteLock(gb_lock_t lock);

void gbLock(gb_lock_t lock);
gb_bool_t gbTryLock(gb_lock_t lock);
void gbUnlock(gb_lock_t lock);

/* Signals */
typedef gb_handle_t gb_signal_t;

gb_signal_t gbNewSignal(gb_uint_t expCnt);
gb_signal_t gbDeleteSignal(gb_signal_t sig);

void gbResetSignal(gb_signal_t sig, gb_uint_t expCnt);
void gbAllocSignalSlot(gb_signal_t sig);
void gbRaiseSignal(gb_signal_t sig);

void gbSyncSignal(gb_signal_t sig);
gb_bool_t gbTrySyncSignal(gb_signal_t sig);

void gbSpinSyncSignal(gb_signal_t sig, size_t spinCount);

/* Job Stream */
typedef void(*gb_jobFunc_t)(void *);

void gbRequireDefStream();

gb_uint_t gbCountDefStreamThreads();
gb_uint_t gbGetJobStreamBufferSize();

void gbSchedule(gb_jobFunc_t fn, void *p);

void gbSyncDefStream();
void gbSyncDefStreamActive();

	/*
	 * TODO: Provide ability to manage individual job streams.
	 */

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
