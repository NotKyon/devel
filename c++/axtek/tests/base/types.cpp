#include <AxTek/Base/Types.h>
#include <stdio.h>

int main() {
#define CHECK_SIZE(x,y) if(sizeof(x)!=(y)){\
	fprintf(stderr,"sizeof(%s)=%u != %u\n",#x,(unsigned int)sizeof(x),\
		(unsigned int)(y));\
	errcnt++;\
}
	int errcnt = 0;

	CHECK_SIZE(ax::i8    , 1);
	CHECK_SIZE(ax::i16   , 2);
	CHECK_SIZE(ax::i32   , 4);
	CHECK_SIZE(ax::i64   , 8);
	CHECK_SIZE(ax::u8    , 1);
	CHECK_SIZE(ax::u16   , 2);
	CHECK_SIZE(ax::u32   , 4);
	CHECK_SIZE(ax::u64   , 8);
	CHECK_SIZE(ax::f32   , 4);
	CHECK_SIZE(ax::f64   , 8);
	CHECK_SIZE(ax::sint_t, sizeof(int));
	CHECK_SIZE(ax::uint_t, sizeof(int));
	CHECK_SIZE(ax::sptr_t, sizeof(void *));
	CHECK_SIZE(ax::uptr_t, sizeof(void *));

	return errcnt;
#undef CHECK_SIZE
}
