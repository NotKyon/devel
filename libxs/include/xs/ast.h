#ifndef XS_AST_H
#define XS_AST_H

typedef struct xs_ast_s *xs_ast_t;

/*
 * Create a new AST node.
 *
 * >prnt (xs_ast_t): The parent node. The node you create will be added to this
 *                   node's child list.
 * >lexan (const char *): This is the text of the node. If you're creating a
 *                        root node, this should be the name of the file.
 * >token (int): Token of the node. i.e., the type of the node. This will depend
 *               on how you lexically analyze the file. If you're creating a
 *               root node, this should be 0.
 * <return (xs_ast_t): Handle to the node.
 */
xs_ast_t XsNewAST(xs_ast_t prnt, const char *lexan, int token);

/*
 * Delete an existing AST node.
 *
 * >ast (xs_ast_t): Handle to the node to be deleted. All child nodes will also
 *                  be deleted.
 * <return (xs_ast_t): NULL.
 */
xs_ast_t XsDeleteAST(xs_ast_t ast);

/*
 * Retrieve the root AST node of another node.
 *
 * >ast (xs_ast_t): Handle to the node whose root node is to be retrieved.
 * <return (xs_ast_t): Handle to the root node if 'ast' is not the root node.
 */
xs_ast_t XsASTRoot(xs_ast_t ast);

/*
 * Retrieve the parent AST node of another node.
 *
 * >ast (xs_ast_t): Handle to the node whose parent node is to be retrieved.
 * <return (xs_ast_t): Handle to the parent node.
 */
xs_ast_t XsASTParent(xs_ast_t ast);

/*
 * Retrieve the first child AST node of a node.
 *
 * >ast (xs_ast_t): Handle to the node whose first child node is to be
 *                  retrieved.
 * <return (xs_ast_t): Handle to the first child node.
 */
xs_ast_t XsFirstAST(xs_ast_t ast);

/*
 * Retrieve the last child AST node of a node.
 *
 * >ast (xs_ast_t): Handle to the node whose last child node is to be retrieved.
 * <return (xs_ast_t): Handle to the last child node.
 */
xs_ast_t XsLastAST(xs_ast_t ast);

/*
 * Retrieve the sibling AST node before another node.
 *
 * >ast (xs_ast_t): Handle to the node whose sibling is to be retrieved.
 * <return (xs_ast_t): Handle to the sibling before 'ast'.
 */
xs_ast_t XsASTBefore(xs_ast_t ast);

/*
 * Retrieve the sibling AST node after another node.
 *
 * >ast (xs_ast_t): Handle to the node whose sibling is to be retrieved.
 * <return (xs_ast_t): Handle to the sibling after 'ast'.
 */
xs_ast_t XsASTAfter(xs_ast_t ast);

/*
 * Retrieve the lexan of an AST node.
 *
 * >ast (xs_ast_t): Handle to the node whose lexan is to be retrieved.
 * <return (const char *): Lexan of the node.
 */
const char *XsASTLexan(xs_ast_t ast);

/*
 * Retrieve the token of an AST node.
 *
 * >ast (xs_ast_t): Handle to the node whose token is to be retrieved.
 * <return (int): Token of the node.
 */
int XsASTToken(xs_ast_t ast);

/*
 * Retrieve the file an AST node is in. i.e., retrieve the lexan of the root
 * node of an AST.
 *
 * >ast (xs_ast_t): Handle to the node whose file is to be queried.
 * <return (const char *): Lexan of the root node of 'ast', or lexan of 'ast' if
 *                         there is no root node.
 */
const char *XsASTFile(xs_ast_t ast);

#endif
