#ifndef MEMORY_H
#define MEMORY_H

#include <stddef.h>

void *mem_Alloc(size_t n);
void *mem_AllocZero(size_t n);
void *mem_Realloc(void *p, size_t n);
void *mem_Free(void *p);

#endif
