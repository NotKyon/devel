// includes
#include <Axia/Vanilla/Vanilla.h>
#include <Axia/Vanilla/Internal.h>

// AXTabber: Constructor
AXTabber::AXTabber(HWND hwndRef, AXGadget *pSuper)
: AXWidget(AXTabberGadgetType, hwndRef, pSuper), m_curSel(0)
{
}

// AXTabber: QueryClientArea: retrieve client area information
void AXTabber::QueryClientArea(int *pLeft, int *pTop, int *pRight, int *pBottom)
{
	// declarations
	RECT	rcArea	= { 0, 0, 0, 0 };

	// convert the rectangle of the control to the area for sub-controls
	::GetClientRect(m_hwndRef, &rcArea);
	TabCtrl_AdjustRect(m_hwndRef, FALSE, &rcArea);

	// store the area
	if (pLeft)		*pLeft		= rcArea.left;
	if (pTop)		*pTop		= rcArea.top;
	if (pRight)		*pRight		= rcArea.right;
	if (pBottom)	*pBottom	= rcArea.bottom;
}

// AXTabber: FindTab: find a tab handle by its numeric index
AXTab *AXTabber::FindTab(int index)
{
	// declarations
	AXGadget	*pGadget;
	AXTab		*pTab;

	// early exit on invalid index
	if (index < 0)
		return (AXTab *)0;

	// enumerate each sub-gadget
	for(pGadget=m_subList.First(); pGadget; pGadget=pGadget->m_subLink.After())
	{
		// looking for tabs specifically; ignore other gadget types
		if (pGadget->Type() != AXTabGadgetType)
			continue;

		// convenience: store the tab handle
		pTab = reinterpret_cast<AXTab *>(pGadget);

		// check if this tab is the one we're looking for
		if (pTab->TabIndex()==index)
			// found: return the tab
			return pTab;
	}

	// not found: return null
	return (AXTab *)0;
}

// AXTabber: ChangeSelection: change the selection
void AXTabber::ChangeSelection(AXTab *pTab)
{
	// declarations
	AXTab	*pOldTab;

	// find the index of the old tab (so its contents can be hidden)
	pOldTab = FindTab(m_curSel);

	// check for redundancy; if the new tab IS the old tab, just return
	if (pOldTab==pTab)
		return;

	// if an old tab was found, hide its contents
	if (pOldTab)
		pOldTab->Hide();

	// early exit for no new tab
	if (!pTab)
	{
		// must save the current selection: -1 means "none"
		m_curSel = -1;
		return;
	}

	// save the index of the new tab and show its contents
	m_curSel = pTab->TabIndex();
	pTab->Show();
}
