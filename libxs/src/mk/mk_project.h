#ifndef MK_PROJECT_H
#define MK_PROJECT_H

#include "mk_types.h"

enum {
	kProjType_Library,
	kProjType_DynamicLibrary,
	kProjType_Executable
};

enum {
	kProjSys_MSWin,
	kProjSys_Linux,
	kProjSys_MacOS,

	kNumProjSys
};

enum {
	kProjArch_X86,
	kProjArch_X86_64,
	kProjArch_ARM,
	kProjArch_PowerPC,
	kProjArch_MIPS,

	kNumProjArch
};

enum {
	kProjCfg_UsesCxx_Bit = BIT(0),
	kProjCfg_NeedRelink_Bit = BIT(1),
	kProjCfg_Linking_Bit = BIT(2)
};

typedef struct project_s {
	char *name;
	char *path;
	char *outpath;

	int type;
	int sys;
	int arch;

	xs_array_t sources;
	xs_array_t specialdirs;
	xs_array_t libs;
	xs_array_t libdirs;

	char *linkerflags;

	bitfield_t config;

	struct project_s *prnt, **p_head, **p_tail;
	struct project_s *head, *tail;
	struct project_s *prev, *next;
} *project_t;

extern const int g_host_sys;
extern const int g_host_arch;

extern project_t g_proj_head;
extern project_t g_proj_tail;

project_t NewProject(project_t prnt);
void DeleteProject(project_t proj);
void DeleteAllProjects();

project_t FirstRootProject();
project_t LastRootProject();

project_t ProjectParent(project_t proj);
project_t ProjectBefore(project_t proj);
project_t ProjectAfter(project_t proj);
project_t FirstProject(project_t prnt);
project_t LastProject(project_t prnt);

void SetProjectName(project_t proj, const char *name);
void SetProjectPath(project_t proj, const char *path);
void SetProjectType(project_t proj, int type);

const char *ProjectName(project_t proj);
const char *ProjectPath(project_t proj);
const char *ProjectOutPath(project_t proj);

int ProjectType(project_t proj);

void AddProjectLib(project_t proj, const char *libname);
size_t ProjectLibCount(project_t proj);
const char *ProjectLib(project_t proj, size_t i);

void AppendProjectLinkerFlag(project_t proj, const char *flags);
const char *ProjectLinkerFlags(project_t proj);

void AddProjectSource(project_t proj, const char *src);
size_t ProjectSourceCount(project_t proj);
const char *ProjectSource(project_t proj, size_t i);

void AddProjectSpecialDir(project_t proj, const char *dir);
size_t ProjectSpecialDirCount(project_t proj);
const char *ProjectSpecialDir(project_t proj, size_t i);

int IsProjectTargetted(project_t proj);

void PrintProjects(project_t proj, const char *margin);
void CalcProjectLibFlags(project_t proj);

#endif
