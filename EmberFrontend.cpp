#include <exception>
#include <stdexcept>

#ifndef EE_DEBUG_ENABLED
# if DEBUG||_DEBUG||__debug__
#  define EE_DEBUG_ENABLED 1
# else
#  define EE_DEBUG_ENABLED 0
# endif
#endif

#ifndef EE_TEST_ENABLED
# if TEST||_TEST||__test__
#  define EE_TEST_ENABLED 1
# else
#  define EE_TEST_ENABLED 0
# endif
#endif

#ifndef EE_ASSERT_ENABLED
# if EE_DEBUG_ENABLED || EE_TEST_ENABLED
#  define EE_ASSERT_ENABLED 1
# else
#  define EE_ASSERT_ENABLED 0
# endif
#endif

#ifndef EE_ASSERT_MSG
# if EE_ASSERT_ENABLED
#  define EE_ASSERT_MSG(x,m) if(!(x))throw ::ee::assert_error(__FILE__,__LINE__,EE_CURRENT_FUNCTION,#x,(m))
# else
#  define EE_ASSERT_MSG(x,m) (void)0
# endif
#endif

#ifndef EE_ASSERT
# if EE_ASSERT_ENABLED
#  define EE_ASSERT(x) EE_ASSERT_MSG((x),"Assert failure")
# else
#  define EE_ASSERT(x) (void)0
# endif
#endif

namespace ee {

typedef unsigned char						u8_t;
typedef unsigned short						u16_t;
typedef unsigned int						u32_t;
typedef unsigned long long int				u64_t;
typedef   signed char						s8_t;
typedef   signed short						s16_t;
typedef   signed int						s32_t;
typedef   signed long long int				s64_t;

template<int _Size> struct __ptr_size {};
template<> struct __ptr_size<2> { typedef u16_t uint_t; typedef s16_t sint_t; };
template<> struct __ptr_size<4> { typedef u32_t uint_t; typedef s32_t sint_t; };
template<> struct __ptr_size<8> { typedef u64_t uint_t; typedef s64_t sint_t; };

typedef __ptr_size<sizeof(void *)>::uint_t	uint_t;
typedef __ptr_size<sizeof(void *)>::sint_t	sint_t;

#define __EE_IMPORT(x) using x = std::x
__EE_IMPORT(exception);
__EE_IMPORT(logic_error);
__EE_IMPORT(runtime_error);
#undef __EE_IMPORT

class assert_error: public logic_error {
public:
	inline									assert_error(const char *file, unsigned int line, const char *func,
											const char *expr, const char *msg): logic_error(msg), m_file(file),
											m_line(line), m_func(func), m_expr(expr) {}
	inline virtual							~assert_error();

	inline char const *						file() const { return m_file; }
	inline unsigned int						line() const { return m_line; }
	inline char const *						function() const { return m_func; }
	inline char const *						expression() const { return m_expr; }

protected:
	char const *							m_file;
	unsigned int							m_line;
	char const *							m_func;
	char const *							m_expr;
};

class reference_counted {
public:
	inline									reference_counted(): m_refCnt(1) {}
	inline virtual							~reference_counted() {}

	inline virtual void						grab() { m_refCnt++; }
	inline virtual void						drop() { if (--m_refCnt==0) delete this; }

private:
	uint_t									m_refCnt;
};

class game: public virtual reference_counted {
public:
	class action_group;

	class action;
	class push_action;
	class axis_action;

	class view;

	virtual									~game();

	virtual action_group *					register_action_group(const char *name) = 0;

	virtual uint_t							get_action_group_count() const = 0;
	virtual action_group *					get_action_group(uint_t index) = 0;
	virtual action_group const *			get_action_group(uint_t index) const = 0;

	virtual uint_t							get_view_count() const = 0;
	virtual view *							get_view(uint_t index=0) = 0;
	virtual view const *					get_view(uint_t index=0) const = 0;
};

class game::action_group: public virtual reference_counted {
public:
	virtual									~action_group();

	virtual push_action *					register_push_action(const char *name) = 0;
	virtual axis_action *					register_axis_action(const char *name) = 0;

	virtual uint_t							get_action_count() const = 0;
	virtual action *						get_action(uint_t index) = 0;
	virtual action const *					get_action(uint_t index) const = 0;
};
class game::action: public virtual reference_counted {
public:
	virtual									~action();

	virtual action_group *					get_group() const = 0;
	virtual const char *					get_name() const = 0;

	virtual push_action *					as_push_action() = 0;
	virtual axis_action *					as_axis_action() = 0;

	virtual push_action const *				as_push_action() const = 0;
	virtual axis_action const *				as_axis_action() const = 0;
};

class game::push_action: public virtual game::action {
public:
	typedef void(*callback_t)(bool currState, bool prevState);

	virtual									~push_action();

	virtual void							set_callback(callback_t callback) = 0;
	virtual callback_t						get_callback() const = 0;
};
class game::axis_action: public virtual game::action {
public:
	typedef void(*callback_t)(s16_t position, s16_t delta);

	virtual									~axis_action();

	virtual void							set_callback(callback_t callback) = 0;
	virtual callback_t						get_callback() const = 0;
};

class game::view: public virtual reference_counted {
public:
	struct information_t {
		struct {
			u16_t							x;
			u16_t							y;
		}									res;
	};

	virtual									~view();

	virtual information_t const *			get_information() const = 0;
};

}

