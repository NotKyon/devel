#include "mk_compmgr.h"

#include "mk_dep.h"
#include "mk_autolink.h"

int FindSourceLibs(xs_array_t dst, int sys, const char *obj, const char *dep) {
	autolink_t al;
	size_t i, n;
	dep_t d;

	if ((d = FindDependency(obj)) != (dep_t)0)
		DeleteDependency(d);

	if (!ReadDeps(dep)) {
		ErrorMessage(va("Failed to read dependency \"%s\"", dep));
		return 0;
	}

	if (!(d = FindDependency(obj))) {
		ErrorMessage(va("FindDependency(\"%s\") failed", obj));
		return 0;
	}

	n = DependencyCount(d);
	for(i=0; i<n; i++) {
		if (!(al = FindAutolink(sys, GetDependency(d, i))))
			continue;

		XsArrayPush(dst, AutolinkLib(al));
	}

	return 1;
}

int ShouldCompile(const char *obj) {
	struct stat s, obj_s;
	xs_path_t dep;
	size_t i, n;
	dep_t d;

	assert(obj != (const char *)0);

	if (g_flags & kFlag_Rebuild_Bit)
		return 1;
	if (g_flags & kFlag_NoCompile_Bit)
		return 0;

	if (stat(obj, &obj_s)==-1)
		return 1;

	XsExtSubst(dep, sizeof(dep), obj, ".d");

	if (stat(dep, &s)==-1)
		return 1;

	d = FindDependency(obj);
	if (!d)
		if (!ReadDeps(dep))
			return 1;

	d = FindDependency(obj);
	if (!d)
		return 1;

	n = DependencyCount(d);
	for(i=0; i<n; i++) {
		if (stat(GetDependency(d, i), &s)==-1)
			return 1; /* need recompile for new dependency list; this file is
						 (potentially) missing */

		if (obj_s.st_mtime <= s.st_mtime)
			return 1;
	}

	return 0; /*no reason to rebuild*/
}
int ShouldLink(const char *bin, int numbuilds) {
	struct stat bin_s;

	assert(bin != (const char *)0);

	if (g_flags & kFlag_Rebuild_Bit)
		return 1;
	if (g_flags & kFlag_NoLink_Bit)
		return 0;

	if (stat(bin, &bin_s)==-1)
		return 1;

	if (numbuilds != 0)
		return 1;

	return 0;
}

const char *GetCompilerFlags(project_t proj, const char *obj, const char *src) {
#define ADDF(s) XsStrCat(flags, sizeof(flags), s)
	/*
	 * TODO: Visual C++ and dependencies. How?
	 */
	static char flags[16384];
	static char *p, *q;
	size_t i, n;

	assert(proj != (project_t)0);
	assert(obj != (const char *)0);
	assert(src != (const char *)0);

	flags[0] = 0;

	g  = "";
	if (g_flags & kFlag_Profile_Bit)
		g = "-pg ";
	else if(g_flags & kFlag_Release_Bit)
		g = "-g ";

	/*
	 * TODO: Allow the front-end to override the warning level
	 */

	/* cl: /Wall */
	ADDF("-W -Wall ");
	if (g_flags & kFlag_Pedantic_Bit)
		ADDF("-pedantic ");

	if (isextcpp(getext(src)))
		ADDF("-Weffc++ ");

	/* architecture specific flags */
	switch(proj->arch) {
	case kProjArch_X86:
		/* cl: /DMK_ARCH_X86 /DMK_BITS=32 /arch:SSE */
		ADDF("-DMK_ARCH_X86 -DMK_BITS=32 -msse ");
		break;
	case kProjArch_X86_64:
		/* cl: /DMK_ARCH_X86_64 /DMK_BITS=64 /arch:SSE */
		ADDF("-DMK_ARCH_X86_64 -DMK_BITS=64 -msse ");
		break;
	case kProjArch_ARM:
		ADDF("-DMK_ARCH_ARM -DMK_BITS=32 ");
		break;
	case kProjArch_PowerPC:
		/* TODO: PowerPC bits = 64? */
		ADDF("-DMK_ARCH_POWERPC -DMK_BITS=32 ");
		break;
	case kProjArch_MIPS:
		ADDF("-DMK_ARCH_MIPS -DMK_BITS=32 ");
		break;
	default:
		break;
	}

	/* optimization/debugging */
	ADDF(g);
	if (g_flags & kFlag_Release_Bit) {
		/* cl: /DNDEBUG /Og /Ox /Oy /GL /QIfist; /Qpar? (parallel code gen.) */
		ADDF("-DNDEBUG -s -O3 ");
		switch(proj->arch) {
		case kProjArch_X86:
			ADDF("-fomit-frame-pointer -m32 ");
			break;
		case kProjArch_X86_64:
			ADDF("-fomit-frame-pointer -m64 ");
			break;
		default:
			/*
			 * NOTE: If you have architecture specific optimizations to apply
			 *       to releasecode, add them here.
			 */
			break;
		}
	} else
		/* cl: /Zi /D_DEBUG /DDEBUG /D__debug__ */
		ADDF("-D_DEBUG -DDEBUG -D__debug__ ");

	ADDF(va("-DMK_INTEGRALDATE=%i ", getintegraldate()));

	/* add a macro for the target system (some systems don't provide their own,
	   or aren't consistent/useful */
	switch(proj->sys) {
	case kProjSys_MSWin:
		/* cl: /DMK_MSWIN */
		ADDF("-DMK_MSWIN ");
		break;
	case kProjSys_Linux:
		ADDF("-DMK_LINUX ");
		break;
	case kProjSys_MacOS:
		ADDF("-DMK_MACOS ");
		break;
	default:
		/*
		 * PLAN: Add Android/iOS support here too.
		 */
		assertmsg(0, "Project system not handled");
		break;
	}

	/* add a macro for the target build type */
	switch(proj->type) {
	case kProjType_Executable:
		/* cl: /DEXECUTABLE */
		ADDF("-DEXECUTABLE ");
		break;
	case kProjType_Library:
		ADDF("-DLIBRARY ");
		break;
	case kProjType_DynamicLibrary:
		ADDF("-DDYNAMICLIBRARY ");
		/* " -fPIC" -- doesn't appear to be necessary and causes a warning */
		break;
	default:
		assertmsg(0, "Project type not handled");
		break;
	}

	/* add the include search paths */
	n = XsArraySize(g_incdirs);
	for(i=0; i<n; i++)
		/* cl: "/I \"%s\" " */
		ADDF(va("-I \"%s\" ", XsArrayElement(g_incdirs, i)));

	/* add the remaining flags (e.g., dependencies, compile-only, etc) */
	ADDF("-MD -MP -c ");

	/* add the appropriate compilation flags */
	ADDF(flags, sizeof(flags), va("-o \"%s\" \"%s\"", obj, src));

	/* done */
	return flags;
#undef ADDF
}

void GetProjectLibs(project_t proj, char *dst, size_t n) {
	const char *name, *suffix;
	project_t node;
	xs_path_t bin;
	int type;

	assert(proj != (project_t)0);
	assert(dst != (char *)0);
	assert(n > 1);

	type = ProjectType(proj);
	name = ProjectName(proj);

	suffix = (g_flags & kFlag_Release_Bit) ? "" : "d";

	if (type==kProjType_Library) {
		/*
		 * TODO: Propogate each library's linking flags
		 */
		for(node=FirstProject(proj); node; node=ProjectAfter(node))
			GetProjectLibs(node, dst, n);

		if (~proj->config & kProjCfg_Linking_Bit)
			XsStrCat(dst, n, va("-l%s%s ", name, suffix));
	} else if(type==kProjType_DynamicLibrary) {
		if (~proj->config & kProjCfg_Linking_Bit) {
			GetBinName(proj, bin, sizeof(bin));
			XsStrCat(dst, n, va("\"%s\" ", bin));
		}
	}
}

const char *GetLinkFlags(project_t proj, const char *bin, xs_array_t objs) {
#define ADDF(x) XsStrCat(flags, sizeof(flags), x)
	static char flags[16384];
	static char libs[16384];
	project_t p;
	size_t i, n;

	assert(proj != (project_t)0);
	assert(bin != (const char *)0);
	assert(objs != (xs_array_t)0);

	flags[0] = 0;
	libs[0] = 0;

	if (ProjectType(proj)!=kProjType_Library) {
		if (g_flags & kFlag_Profile_Bit)
			ADDF("-pg ");
	}

	switch(ProjectType(proj)) {
	case kProjType_Executable:
		if (g_flags & kFlag_Release_Bit)
			ADDF("-s ");

		ADDF(va("-o \"%s\" ", bin));
		break;
	case kProjType_Library:
		ADDF(va("cr \"%s\" ", bin));
		break;
	case kProjType_DynamicLibrary:
		ADDF(va("-shared -o \"%s\" ", bin));
		break;
	default:
		assertmsg(0, "Unhandled project type");
		break;
	}

	if (ProjectType(proj)!=kProjType_Library) {
		n = XsArraySize(proj->libdirs);
		for(i=0; i<n; i++)
			ADDF(va("-L \"%s\" ", XsArrayElement(proj->libdirs, i)));
	}

	n = XsArraySize(objs);
	for(i=0; i<n; i++)
		ADDF(va("\"%s\" ", XsArrayElement(objs, i)));

	if (ProjectType(proj)!=kProjType_Library) {
		proj->config |= kProjCfg_Linking_Bit;

		p = ProjectParent(proj) ? FirstProject(ProjectParent(proj))
			: FirstProject();

		while(p) {
			GetProjectLibs(p, libs, sizeof(libs));
			p = ProjectAfter(p);
		}

		proj->config &= ~kProjCfg_Linking_Bit;
		ADDF(libs);

		switch(proj->sys) {
		case kProjSys_MSWin:
#if 0
			if (projecttype(proj)==kProjType_DynamicLibrary)
				i_strcat(flags, sizeof(flags),
					/* NOTE: we don't use the import library; it's pointless */
					va(/*"\"-Wl,--out-implib=%s%s.a\" "*/
					   "-Wl,--export-all-symbols "
					   "-Wl,--enable-auto-import "/*, bin,
					   ~g_flags & kFlag_Release_Bit ? "d" : ""*/));

			/*i_strcat(flags, sizeof(flags), "-lkernel32 -luser32 -lgdi32 "
				"-lshell32 -lole32 -lopengl32 -lmsimg32");*/
#endif
			break;
		case kProjSys_Linux:
			break;
		case kProjSys_MacOS:
			/*ADDF("-lobjc -framework Cocoa -framework OpenGL");*/
			break;
		default:
			/*
			 * NOTE: Add OS specific link flags here.
			 */
			break;
		}

		ADDF(ProjectLinkerFlags(proj));
		ADDF(" -lm"); /*aaron-20120401: wtf? why is this still here?*/
	}

	return flags;
}

void GetObjName(project_t proj, char *obj, size_t n, const char *src) {
	const char *p;

	assert(proj != (project_t)0);
	assert(obj != (char *)0);
	assert(n > 1);
	assert(src != (const char *)0);

	p = ""; /* 'p' may be used uninitialized */

	switch(ProjectType(proj)) {
	case kProjType_Executable:
		p = "exec";
		break;
	case kProjType_Library:
		p = "lib";
		break;
	case kprojType_DynamicLibrary:
		p = "dylib";
		break;
	default:
		assertmsg(0, "Project type is invalid");
		break;
	}

	XsExtSubst(obj, n, va("obj/%s/%s/%s",
		g_flags & kFlag_Release_Bit ? "rel" : "dbg", p, src), ".o");
}

void GetBinName(project_t proj, char *bin, size_t n) {
	const char *dir, *prefix, *name, *symbol, *suffix;
	int type, sys;

	assert(proj != (project_t)0);
	assert(bin != (char *)0);
	assert(n > 1);

	type = ProjectType(proj);
	sys = proj->sys;

	/*dir = type==kProjType_Library ? "lib/" : "bin/";*/
	dir = ProjectOutPath(proj);
	assert(dir != (const char *)0);

	prefix = "";
	suffix = "";

	name = ProjectName(proj);
	symbol = (g_flags & kFlag_Release_Bit) ? "" : "d";

	switch(type) {
	case kProjType_Executable:
		if (sys==kProjSys_MSWin)
			suffix = ".exe";
		break;
	case kProjType_Library:
		prefix = "lib";
		suffix = ".a";
		break;
	case kProjType_DynamicLibrary:
		if (sys!=kProjSys_MSWin)
			prefix = "lib";

		if (sys==kProjSys_MSWin)
			suffix = ".dll";
		else if(sys==kProjSys_Linux)
			suffix = ".so";
		else if(sys==kProjSys_MacOS)
			suffix = ".dylib";

		break;
	default:
		assertmsg(0, "Unhandled project type");
		break;
	}

	XsStrCpy(bin, n, va("%s%s%s%s%s", dir, prefix, name, symbol, suffix));
}

void SortProjects(project_t proj) {
	project_t *head, *tail;
	project_t p, next;
	int numsorts;

	head = proj ? &proj->head : &g_proj_head;
	tail = proj ? &proj->tail : &g_proj_tail;

	do {
		numsorts = 0;

		for(p=&head; p; p=next) {
			if (!(next = p->next))
				break;

			if (p->type > next->type) {
				numsorts++;

				if (p==*head)
					*head = next;
				if (next==*tail)
					*tail = p;

				if (p->prev)
					p->prev->next = next;
				if (next->next)
					next->next->prev = p;

				next->prev = p->prev;
				p->prev = next;
				p->next = next->next;
				next->next = p;

				next = p;
			}
		}
	} while(numsorts > 0);
}

void RelinkDependents(project_t proj) {
	project_t prnt, next;

	assert(proj != (project_t)0);

	for(prnt=proj->prnt; prnt; prnt=prnt->prnt)
		prnt->config |= kProjCfg_NeedRelink_Bit;

	if (proj->type==kProjType_Executable)
		return;

	for(next=proj->next; next; next=next->next) {
		if (proj->type > next->type)
			break;

		next->config |= kProjCfg_NeedRelink_Bit;
		RelinkDependents(next);
	}
}

int BuildProject(project_t proj) {
	const char *src, *lnk, *tool;
	xs_array_t objs;
	xs_path_t cwd, obj, bin;
	project_t chld;
	size_t cwd_l;
	size_t i, n;
	int numbuilds;

	/* build the child projects */
	SortProjects(proj);
	for(chld=FirstProject(proj); chld; chld=ProjectAfter(chld)) {
		if (!BuildProject(chld))
			return 0;
	}

	/* retrieve the current working directory */
	if (getcwd(cwd, sizeof(cwd))==(char *)0)
		ErrorExit("getcwd() failed");

	cwd_l = strlen(cwd);

	if(!ProjectSourceCount(proj)
	&&  ProjectType(proj)!=kProjType_DynamicLibrary) {
		ErrorMessage(va("Project '%s' has no source files!",
			ProjectName(proj)));
		return 1;
	}

	tool = proj->condig & kProjCfg_UsesCxx_Bit ? g_cxx : g_cc;

	/* make the object directories */
	MakeObjDirs(proj);

	/* store each object file */
	objs = NewArray();

	/* run through each source file */
	numbuilds = 0;
	n = ProjectSourceCount(proj);
	for(i=0; i<n; i++) {
		src = ProjectSource(proj, i);

		GetObjName(proj, obj, sizeof(obj), &src[cwd_l + 1]);
		XsArrayPush(objs, obj);

		if (ShouldCompile(obj)) {
			if (shell("%s %s", tool, GetCompileFlags(proj, obj,
			&src[cwd_l + 1]))) {
				XsDeleteArray(objs);
				return 0;
			}

			numbuilds++;
		}

		if (~g_flags & kFlag_NoLink_Bit) {
			XsExtSubst(bin, sizeof(bin), obj, ".d");
			if (!FindSourceLibs(proj->libs, proj->sys, obj, bin)) {
				ErrorMessage("Call to FindSourceLibs() failed");
				return 0;
			}
		}
	}

	/* link the project's object files together */
	GetBinName(proj, bin, sizeof(bin));
	XsMkDirs(ProjectOutPath(proj));

	if (ShouldLink(bin, numbuilds) || (proj->config&kProjCfg_NeedRelink_Bit)) {
		XsRemoveArrayDups(proj->libs);

		CalcProjectLibFlags(proj);
		EnumSpecialLibDirs(proj);

		/* find the libraries this project needs */
		lnk = ProjectType(proj)==kProjType ? "ar" : tool;
		if (shell("%s %s", lnk, GetLinkFlags(proj, bin, objs))) {
			XsDeleteArray(objs);
			return 0;
		}

		/* dependent projects need to be rebuilt */
		RelinkDependents(proj);
	}

	/* clean */
	if (g_flags & kFlag_Clean_Bit) {
		n = XsArraySize(objs);
		for(i=0; i<n; i++) {
			XsExtSubst(obj, sizeof(obj), XsArrayElement(objs, i), ".d");
			remove(XsArrayElement(objs, i)); /*object (.o)*/
			remove(obj); /*dependency (.d)*/
		}

		if (g_flags & kFlag_NoLink_Bit)
			remove(bin);
	}

	XsDeleteArray(objs);

	return 1;
}
int BuildProjects() {
	project_t proj;

	SortProjets((project_t)0);

	for(proj=FirstRootProject(); proj; proj=ProjectAfter(proj)) {
		if (!BuildProject(proj))
			return 0;
	}

	return 1;
}
