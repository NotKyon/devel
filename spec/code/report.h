#ifndef REPORT_H
#define REPORT_H

#include <stdarg.h>

#define TYPE_WRN "WARNING"
#define TYPE_ERR "ERROR"

void ReportV(const char *type, const char *file, int line, const char *func,
const char *message, va_list args);
void Report(const char *type, const char *file, int line, const char *func,
const char *message, ...);

void Warn(const char *file, int line, const char *func, const char *message,
...);
void Error(const char *file, int line, const char *func, const char *message,
...);

void WarnMessage(const char *message, ...);
void ErrorMessage(const char *message, ...);

void WarnFile(const char *file, int line, const char *message, ...);
void ErrorFile(const char *file, int line, const char *message, ...);

void ErrorFileExit(const char *file, int line, const char *message, ...);
void ErrorExit(const char *message, ...);

#endif
