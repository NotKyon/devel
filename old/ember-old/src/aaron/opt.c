#include <string.h>
#include <aaron/opt.h>

static struct stOpt *g_opt_head         = ( struct stOpt * )0,
                    *g_opt_tail         = ( struct stOpt * )0;
static struct stOptInput *g_opt_in_head = ( struct stOptInput * )0,
                         *g_opt_in_tail = ( struct stOptInput * )0;

static int __opt_add_input( struct stOptInput ***ptr, const char *name )
{
	struct stOptInput *in;

	if( !( in = ( struct stOptInput * )malloc( sizeof( *in ) ) ) )
		return -1;

	in->name = name;

	in->next = ( struct stOptInput * )0;
	if( *ptr[ 1 ] )
		( *ptr[ 1 ] )->next = in;
	else
		*ptr[ 0 ] = in;
	*ptr[ 1 ]     = in;

	return 0;
}
static void __opt_remove_inputs( struct stOpt *opt )
{
	struct stOptInput *in, *next;

	for( in = opt->in_head; in; in = next ) {
		next = in->next;
		free( ( void * )in );
	}

	opt->in_head = ( struct stOptInput * )0;
	opt->in_tail = ( struct stOptInput * )0;
}
static void __opt_remove_base_inputs()
{
	struct stOptInput *in, *next;

	for( in = g_opt_in_head; in; in = next ) {
		next = in->next;
		free( ( void * )in );
	}

	g_opt_in_head = ( struct stOptInput * )0;
	g_opt_in_tail = ( struct stOptInput * )0;
}

int opt_add( char sn, const char *ln, bitfield_t f, opt_callback_t cb )
{
	struct stOpt *opt;

	if( !( opt = ( struct stOpt * )malloc( sizeof( *opt ) ) ) )
		return -1;

	opt->sname    = sn;
	opt->lname    = ln ? strdup( ln ) : ( char * )0;
	opt->flags    = f;
	opt->callback = cb;

	opt->in_head = ( struct stOptInput * )0;
	opt->in_tail = ( struct stOptInput * )0;

	opt->next = ( struct stOpt * )0;
	if( g_opt_tail )
		g_opt_tail->next = opt;
	else
		g_opt_head = opt;
	g_opt_tail     = opt;

	return 0;
}

void opt_deinit()
{
	struct stOpt *opt, *next;

	for( opt = g_opt_head; opt; opt = next ) {
		next = opt->next;

		if( opt->lname )
			free( ( void * )opt->lname );

		free( ( void * )opt );
	}

	g_opt_head = g_opt_tail = ( struct stOpt * )0;
}

int opt_parse( int c, const char *const *v )
{
	struct stOpt *opt;
	const char **t[ 2 ];
	const char *p, *s;
	int only_files = 0;
	int i, j;

	for( i = 1; i < c; i++ ) {
		p = v[ i ];
		if( only_files ) {
			t[ 0 ] = &g_opt_in_head;
			t[ 1 ] = &g_opt_in_tail;
			__opt_add_input( t, p );
		} else if( *p == '-' ) {
			if( p[ 1 ] == '-' ) {
				if( !p[ 2 ] ) {
					only_files = 1;
					continue;
				} else {
					s = &p[ 2 ];
					while( *s != '=' && *s )
						s++;
					if( *s )
						s++;
					for( opt = g_opt_head; opt; opt = opt->next ) {
						if( !strcmp( opt->lname, &p[ 2 ] ) ) {
							if( s ) {
								t[ 0 ] = &opt->head;
								t[ 1 ] = &opt->tail;
								__opt_add_input( t, s );
							}
							if( !opt->callback( opt->in_head ) ) {
								__opt_remove_inputs( opt );
								return 0;
							}
							__opt_remove_inputs( opt );
							break;
						}
					}
				}
			} else if( !p[ 1 ] ) {
				t[ 0 ] = &g_opt_in_head;
				t[ 1 ] = &g_opt_in_tail;
				__opt_add_input( t, "-" );
			} else {
				for( j = 1; p[ j ]; j++ ) {
					for( opt = g_opt_head; opt; opt = opt->next ) {
						if( opt->sname == p[ j ] ) {
							if( !opt->callback( opt->in_head ) )
								return 0;
							break;
						}
					}
				}
			}
		} else {
			t[ 0 ] = &g_opt_in_head;
			t[ 1 ] = &g_opt_in_tail;
			__opt_add_input( t, p );
		}
	}

	if( !incallback( g_opt_in_head ) ) {
		__opt_remove_base_inputs( opt );
		return 0;
	}

	__opt_remove_base_inputs( opt );
	return 1;
}
