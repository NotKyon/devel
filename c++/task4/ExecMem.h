#ifndef EXECMEM_H
#define EXECMEM_H

#if _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#else
# include <sys/mman.h>
#endif

#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <cstring>

typedef struct execMem_s {
	union { void *p; int(*fn)(int x); } ptr;
	size_t n;

	struct execMem_s *prev, *next;
} execMem_t;
#if EXECMEM_INIT
execMem_t *g_execMem_head = (execMem_t *)0;
execMem_t *g_execMem_tail = (execMem_t *)0;
#else
extern execMem_t *g_execMem_head;
extern execMem_t *g_execMem_tail;
#endif

inline execMem_t *NewExecMem(size_t n) {
	execMem_t *execMem;

	execMem = (execMem_t *)malloc(sizeof(*execMem));
	if (!execMem)
		exit(EXIT_FAILURE);

#if _WIN32
	execMem->ptr.p = VirtualAlloc(0, n, MEM_COMMIT|MEM_RESERVE,
		PAGE_EXECUTE_READWRITE);
#else
	execMem->ptr.p = mmap(0, n, PROT_READ|PROT_WRITE|PROT_EXEC,
		MAP_ANON|MAP_PRIVATE, 0, 0);
#endif
	if (!execMem->ptr.p) {
		free((void *)execMem);
		exit(EXIT_FAILURE);
	}

	execMem->n = n;

	execMem->next = (execMem_t *)0;
	if ((execMem->prev = g_execMem_tail) != (execMem_t *)0)
		g_execMem_tail->next = execMem;
	else
		g_execMem_head = execMem;
	g_execMem_tail = execMem;

	return execMem;
}
inline execMem_t *DeleteExecMem(execMem_t *execMem) {
	if (!execMem)
		return (execMem_t *)0;

#if _WIN32
	VirtualFree(execMem->ptr.p, 0, MEM_RELEASE);
#else
	munmap(execMem->ptr.p, execMem->n);
#endif

	if (execMem->prev)
		execMem->prev->next = execMem->next;
	if (execMem->next)
		execMem->next->prev = execMem->prev;

	if (g_execMem_head==execMem)
		g_execMem_head = execMem->next;
	if (g_execMem_tail==execMem)
		g_execMem_tail = execMem->prev;

	free((void *)execMem);
	return (execMem_t *)0;
}
inline void *ExecMemPtr(execMem_t *execMem) {
	return execMem->ptr.p;
}

#endif
