#include "../taskshare/versions/Task3.h"

#include <ctime>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>

//------------------------------------------------------------------------------
// TASK IMPLEMENTATIONS

#include "../taskshare/test/Alloc.cpp"

//------------------------------------------------------------------------------
// USER TEST

#if 1
# define NUM_TESTS 1024
#else
# define NUM_TESTS 4
#endif

#include "../taskshare/test/IsPrime.cpp"

task::queue_t gMainQueue;
int gNumbers[NUM_TESTS];

task::threadResult_t THREADPROCAPI WorkerThread(void *) {
	task::Streamer(gMainQueue);
	return (task::threadResult_t)0;
}

int main() {
	task::perfQuery_t pq;
	task::thread_t threads[32];
	size_t i, n;

	n = task::GetCPUThreadCount();
	//n = task::GetCPUCoreCount();
	if (n > sizeof(threads)/sizeof(threads[0]))
		n = sizeof(threads)/sizeof(threads[0]);
	printf("%u cores available\n", (unsigned int)n);
	fflush(stdout);

	task::InitQueue(gMainQueue);

	// NOTE: threads before tasks if queue is not large enough
	for(i=0; i<NUM_TESTS; i++) {
		gNumbers[i] = 100000000 + i;
		task::Schedule(gMainQueue, IsPrime_f, &gNumbers[i]);
	}

	for(i=0; i<n; i++)
		threads[i] = task::NewThread(WorkerThread, (void *)0);

	task::InitPerfQuery(pq);
	task::SyncQueue(gMainQueue);
	task::FiniPerfQuery(pq);

	task::FiniQueue(gMainQueue);

	for(i=0; i<n; i++) {
		task::JoinThread(threads[i]);
		task::KillThread(threads[i]);
	}

	return 0;
}
