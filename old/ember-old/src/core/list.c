#include <ember/list.h>

void list_init( ListBase *base )
{
	base->head = ( ListLink * )0;
	base->tail = ( ListLink * )0;
}

void list_remove_all( ListBase *base )
{
	while( base->head )
		list_remove( base->head );
}

ListLink *list_first_link( ListBase *base )
{
	return base->head;
}
ListLink *list_last_link( ListBase *base )
{
	return base->tail;
}

void *list_first( ListBase *base )
{
	return ( base->head ) ? base->head->obj : ( void * )0;
}
void *list_last( ListBase *base )
{
	return ( base->tail ) ? base->tail->obj : ( void * )0;
}

void list_link( ListLink *link, void *obj )
{
	link->obj  = obj;
	link->prev = ( ListLink * )0;
	link->next = ( ListLink * )0;
	link->base = ( ListBase * )0;
}
void list_remove( ListLink *link )
{
	if( !link->base )
		return;

	if( link->prev )
		link->prev->next = link->next;
	if( link->next )
		link->next->prev = link->prev;

	if( link->base->head == link )
		link->base->head = link->next;
	if( link->base->tail == link )
		link->base->tail = link->prev;

	link->base = ( ListBase * )0;
}

void list_add_first( ListBase *base, ListLink *link )
{
	link->base = base;

	link->prev = ( ListLink * )0;
	link->next = base->head;
	if( base->head )
		base->head->prev = link;
	base->head = link;
	if( !base->tail )
		base->tail = link;
}
void list_add( ListBase *base, ListLink *link )
{
	link->base = base;

	link->next = ( ListLink * )0;
	if( ( link->prev = base->tail ) != ( ListLink * )0 )
		base->tail->next = link;
	else
		base->head = link;
	base->tail     = link;
}

ListLink *list_prev_link( ListLink *link )
{
	return link->prev;
}
ListLink *list_next_link( ListLink *link )
{
	return link->next;
}

void *list_prev( ListLink *link )
{
	return ( link->prev ) ? link->prev->obj : ( void * )0;
}
void *list_next( ListLink *link )
{
	return ( link->next ) ? link->next->obj : ( void * )0;
}

void list_set( ListLink *link, void *obj )
{
	link->obj = obj;
}
void *list_get( ListLink *link )
{
	return link->obj;
}

int list_for_each( ListBase *base, int ( *callback )( ListLink *, void * ),
    void *data )
{
	ListLink *link, *next;
	int r;

	if( !callback )
		return 0;

	for( link = base->head; link; link = next ) {
		next = link->next;
		if( ( r = callback( link, data ) ) != 0 )
			return r;
	}

	return 0;
}

void list_insert_before( ListLink *link, ListLink *before )
{
	if( link->base ) {
		if( link->prev )
			link->prev->next = link->next;
		if( link->next )
			link->next->prev = link->prev;

		if( link->base->head == link )
			link->base->head = link->next;
		if( link->base->tail == link )
			link->base->tail = link->prev;
	}

	link->base = before->base;

	link->next = before;
	if( ( link->prev = before->prev ) != ( ListLink * )0 )
		link->prev->next = link;
	else
		link->base->head = link;
	before->prev         = link;
}

void list_insert_after( ListLink *link, ListLink *after )
{
	if( link->base ) {
		if( link->prev )
			link->prev->next = link->next;
		if( link->next )
			link->next->prev = link->prev;

		if( link->base->head == link )
			link->base->head = link->next;
		if( link->base->tail == link )
			link->base->tail = link->prev;
	}

	link->base = after->base;

	link->prev = after;
	if( ( link->next = after->next ) != ( ListLink * )0 )
		link->next->prev = link;
	else
		link->base->tail = link;
	after->next          = link;
}
