#ifndef GB_SYS_EVENT_H
#define GB_SYS_EVENT_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Events */
enum {
	kGBEvent_None,

	kGBEvent_AppSuspend,
	kGBEvent_AppResume,
	kGBEvent_AppTerminate,

	kGBEvent_KeyDown,
	kGBEvent_KeyUp,
	kGBEvent_KeyChar,

	kGBEvent_MouseDown,
	kGBEvent_MouseUp,
	kGBEvent_MouseWheel,
	kGBEvent_MouseMove,
	kGBEvent_MouseEnter,
	kGBEvent_MouseLeave,

	kGBEvent_TimerTick,

	kGBEvent_HotKeyHit,
	kGBEvent_MenuAction,

	kGBEvent_WindowMove,
	kGBEvent_WindowSize,
	kGBEvent_WindowClose,
	kGBEvent_WindowActivate,
	kGBEvent_WindowAccept,

	kGBEvent_GadgetAction,
	kGBEvent_GadgetPaint,
	kGBEvent_GadgetSelect,
	kGBEvent_GadgetMenu,
	kGBEvent_GadgetOpen,
	kGBEvent_GadgetClose
};
enum {
	kGBMod_Ctrl_Bit = (1<<0),
	kGBMod_Shift_Bit = (1<<1),
	kGBMod_Alt_Bit = (1<<2),
	kGBMod_Super_Bit = (1<<3)
};
enum {
	kGBKey_A='A',
	kGBKey_B='B',
	kGBKey_C='C',
	kGBKey_D='D',
	kGBKey_E='E',
	kGBKey_F='F',
	kGBKey_G='G',
	kGBKey_H='H',
	kGBKey_I='I',
	kGBKey_J='J',
	kGBKey_K='K',
	kGBKey_L='L',
	kGBKey_M='M',
	kGBKey_N='N',
	kGBKey_O='O',
	kGBKey_P='P',
	kGBKey_Q='Q',
	kGBKey_R='R',
	kGBKey_S='S',
	kGBKey_T='T',
	kGBKey_U='U',
	kGBKey_V='V',
	kGBKey_W='W',
	kGBKey_X='X',
	kGBKey_Y='Y',
	kGBKey_Z='Z',

	kGBKey_Tab='\t',
	kGBKey_Enter='\n',
	kGBKey_Space=' ',
	kGBKey_Backspace='\b',

	kGBKey_Tilde='~',
	kGBKey_1='1',
	kGBKey_2='2',
	kGBKey_3='3',
	kGBKey_4='4',
	kGBKey_5='5',
	kGBKey_6='6',
	kGBKey_7='7',
	kGBKey_8='8',
	kGBKey_9='9',
	kGBKey_0='0',
	kGBKey_Minus='-',
	kGBKey_Equal='=',
	kGBKey_LBracket='[',
	kGBKey_RBracket=']',
	kGBKey_Backslash='\\',
	kGBKey_Semicolon=';',
	kGBKey_Apostrophe='\'',
	kGBKey_Comma=',',
	kGBKey_Period='.',
	kGBKey_Slash='/',

	kGBKey_Exclamation=kGBKey_1,
	kGBKey_At=kGBKey_2,
	kGBKey_Hash=kGBKey_3,
	kGBKey_Dollar=kGBKey_4,
	kGBKey_Percent=kGBKey_5,
	kGBKey_Caret=kGBKey_6,
	kGBKey_Ampersand=kGBKey_7,
	kGBKey_Asterisk=kGBKey_8,
	kGBKey_LParen=kGBKey_9,
	kGBKey_RParen=kGBKey_0,
	kGBKey_LBrace=kGBKey_LBracket,
	kGBKey_RBrace=kGBKey_RBracket,
	kGBKey_Pipe=kGBKey_Backslash,
	kGBKey_Colon=kGBKey_Semicolon,
	kGBKey_Quote=kGBKey_Apostrophe,
	kGBKey_Less=kGBKey_Comma,
	kGBKey_Greater=kGBKey_Period,
	kGBKey_Question=kGBKey_Slash,

	__kGBKey_Special__=256,

	kGBKey_Esc,
	kGBKey_Escape = kGBKey_Esc,

	kGBKey_F1,
	kGBKey_F2,
	kGBKey_F3,
	kGBKey_F4,
	kGBKey_F5,
	kGBKey_F6,
	kGBKey_F7,
	kGBKey_F8,
	kGBKey_F9,
	kGBKey_F10,
	kGBKey_F11,
	kGBKey_F12,
	kGBKey_F13,
	kGBKey_F14,
	kGBKey_F15,
	kGBKey_F16,
	kGBKey_F17,
	kGBKey_F18,
	kGBKey_F19,
	kGBKey_F20,
	kGBKey_F21,
	kGBKey_F22,
	kGBKey_F23,
	kGBKey_F24,
	kGBKey_F25,

	kGBKey_LShift,
	kGBKey_RShift,
	kGBKey_LCtrl,
	kGBKey_RCtrl,
	kGBKey_LAlt,
	kGBKey_RAlt,
	kGBKey_LSuper,
	kGBKey_RSuper,

	kGBKey_Menu,
	kGBKey_Pause,

	kGBKey_Ins,
	kGBKey_Del,
	kGBKey_Home,
	kGBKey_End,
	kGBKey_PgUp,
	kGBKey_PgDn,

	kGBKey_Up,
	kGBKey_Left,
	kGBKey_Down,
	kGBKey_Right,

	kGBKey_CapsLock,
	kGBKey_ScrollLock,

	kGBKey_KP_NumLock,
	kGBKey_KP_1,
	kGBKey_KP_2,
	kGBKey_KP_3,
	kGBKey_KP_4,
	kGBKey_KP_5,
	kGBKey_KP_6,
	kGBKey_KP_7,
	kGBKey_KP_8,
	kGBKey_KP_9,
	kGBKey_KP_0,
	kGBKey_KP_Div,
	kGBKey_KP_Mul,
	kGBKey_KP_Sub,
	kGBKey_KP_Add,
	kGBKey_KP_Decimal,
	kGBKey_KP_Equal,
	kGBKey_KP_Enter
};
typedef int gb_eventID_t;

typedef struct gb_event_s {
	int id;
	void *source;
	int data;
	gb_uint_t mods;
	short int x, y;
	void *extra;
} gb_event_t;

gb_eventID_t gbPeekEvent(gb_event_t *ev);
gb_eventID_t gbPollEvent();
gb_eventID_t gbWaitEvent();

gb_eventID_t gbCurrentEvent(gb_event_t *ev);

void gbEmitEventEx(gb_event_t *ev);
void gbEmitEvent(int id, void *source, int data, gb_uint_t mods, int x, int y,
	void *extra);

int gbEventID();
void *gbEventSource();
int gbEventData();
gb_uint_t gbEventMods();
int gbEventX();
int gbEventY();
void *gbEventExtra();
const char *gbEventExtraAsText();

gb_eventID_t gbAllocUserEventID();

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
