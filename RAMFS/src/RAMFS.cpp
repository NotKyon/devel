#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#define NF_ENGINE_PLUGIN 1
#define NF_RUN_MODE NF_ERROR_EXCEPTION
#include <nf.h>

#include <stdio.h>
#include <malloc.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <map>

//------------------------------------------------------------------------------
#define FUNC extern "C" __declspec(dllexport) __stdcall
struct RAMFile;

FUNC void PluginMain() {
}

FUNC int RAMFS_Init();
FUNC void RAMFS_Fini();

FUNC RAMFile *RAMFS_NewFile(const char *name, void *ptr, size_t nptr);
FUNC RAMFile *RAMFS_DeleteFile(RAMFile *rf);

FUNC int RAMFS_LockFile(RAMFile *rf);
FUNC int RAMFS_TryLockFile(RAMFile *rf);
FUNC int RAMFS_UnlockFile(RAMFile *rf);

FUNC int RAMFS_IsEnabled();
FUNC void RAMFS_Enable();
FUNC void RAMFS_Disable();

FUNC RAMFile *RAMFS_MakeMemoryFile(const char *name, size_t nptr);
FUNC int RAMFS_ReadFile(RAMFile *rf, void *dst, size_t n);
FUNC int RAMFS_WriteFile(RAMFile *rf, const void *src, size_t n);
FUNC size_t RAMFS_FileSize(RAMFile *rf);
FUNC size_t RAMFS_GetFilePos(RAMFile *rf);
FUNC int RAMFS_SetFilePos(RAMFile *rf, size_t n);
FUNC int RAMFS_IsEOF(RAMFile *rf);

FUNC void *RAMFS_GetDirectPtr(RAMFile *rf);
#undef FUNC
//------------------------------------------------------------------------------

#define RAMFS_ALIGNMENT 64

typedef const char *func_str;

#define C __cdecl
#define S __stdcall

#define FUNC_LIST\
	FUNC_CALL(fopen,OPEN,C,FILE *,(const char *filename, const char *mode))\
			FUNC_DELM\
	FUNC_CALL(fread,READ,C,size_t,(void *buf, size_t s, size_t c, FILE *f))\
			FUNC_DELM\
	FUNC_CALL(fseek,SEEK,C,int,(FILE *f, long offset, int origin))FUNC_DELM\
	FUNC_CALL(ftell,TELL,C,long,(FILE *f))FUNC_DELM\
	FUNC_CALL(fflush,FLUSH,C,int,(FILE *f))FUNC_DELM\
	FUNC_CALL(feof,EOF,C,int,(FILE *f))FUNC_DELM\
	FUNC_CALL(ferror,ERROR,C,int,(FILE *f))FUNC_DELM\
	FUNC_CALL(clearerr,CLEARERR,C,void,(FILE *f))FUNC_DELM\
	FUNC_CALL(remove,REMOVE,C,int,(const char *path))FUNC_DELM\
	\
	FUNC_CALL(CloneFile,CLONEFILE,S,void,(func_str srcfile, func_str dstfile))\
			FUNC_DELM\
	FUNC_CALL(RelocateFile,RELOCATEFILE,S,bool,(func_str srcfile,\
			func_str dstfile))FUNC_DELM\
	FUNC_CALL(FileExists,FILEEXISTS,S,WORD,(func_str filename))FUNC_DELM\
	FUNC_CALL(FileSize,FILESIZE,S,DWORD,(func_str filename))FUNC_DELM\
	FUNC_CALL(MakeDirectory,MAKEDIR,S,bool,(func_str foldername))FUNC_DELM\
	FUNC_CALL(DeleteDirectory,DELETEDIR,S,bool,(func_str foldername))FUNC_DELM\
	\
	FUNC_CALL(FindFirstFile,FINDFIRST,S,HANDLE,(const char *,\
			LPWIN32_FIND_DATA))FUNC_DELM\
	FUNC_CALL(FindNextFile,FINDNEXT,S,BOOL,(HANDLE, LPWIN32_FIND_DATA))\
			FUNC_DELM\
	FUNC_CALL(SetCurrentDirectory,SETCURRENTDIR,S,BOOL,(const char *))FUNC_DELM\
	FUNC_CALL(GetCurrentDirectory,GETCURRENTDIR,S,DWORD,(DWORD, char *))\
			FUNC_DELM\
	\
	//end of list

#define FUNC_CALL(n,d,c,r,p) typedef r(c *n##_t)p
#define FUNC_DELM ;
FUNC_LIST
#undef FUNC_DELM
#undef FUNC_CALL

struct RAMFile {
	string filename;

	void *ptr;
	size_t nptr;

	size_t npos;

	int ownsptr;

	RAMFile *prev, *next;
	CRITICAL_SECTION lock;

	inline RAMFile() {
	}
	inline ~RAMFile() {
	}
};
static RAMFile *g_head = (RAMFile *)NULL;
static RAMFile *g_tail = (RAMFile *)NULL;
static CRITICAL_SECTION g_lock;
static bool g_didInit = false;
static int g_isEnabled = 0;

static std::map<FILE *, RAMFile *> g_fileMap;
static std::map<HANDLE, RAMFile *> g_handleMap;

#define FUNC_CALL(n,d,c,r,p) static n##_t g_##n = (n##_t)NULL
#define FUNC_DELM ;
FUNC_LIST
#undef FUNC_DELM
#undef FUNC_CALL

#if __GNUC__ || __clang__ || __INTEL_COMPILER
# define likely(x) __builtin_expect((x), 1)
# define unlikely(x) __builtin_expect((x), 0)
# if !__INTEL_COMPILER
#  define __forceinline static __inline __attribute__((always_inline))
# endif
#else
# define likely(x) (x)
# define unlikely(x) (x)
#endif

__forceinline void LockList() {
	EnterCriticalSection(&g_lock);
}
__forceinline void UnlockList() {
	LeaveCriticalSection(&g_lock);
}

__forceinline void LockFile(RAMFile *file) {
	EnterCriticalSection(&file->lock);
}
__forceinline void UnlockFile(RAMFile *file) {
	LeaveCriticalSection(&file->lock);
}

__forceinline int CheckRAMFile(RAMFile *f) {
	if (likely(f != (RAMFile *)NULL))
		return 1;

	MessageBoxA(GetActiveWindow(), "RAMFile not valid", "RAMFS",
		MB_ICONERROR|MB_OK);

	return 0;
}

__forceinline void SetRAMFileFromFile(RAMFile *rf, FILE *f) {
	g_fileMap[f] = rf;
}
__forceinline void SetRAMFileFromHandle(RAMFile *rf, HANDLE h) {
	g_handleMap[h] = rf;
}
__forceinline RAMFile *RAMFileFromFile(FILE *f) {
	return g_fileMap[f];
}
__forceinline HANDLE RAMFileFromHandle(HANDLE h) {
	return g_handleMap[h];
}

#if _WIN32
# define strcasecmp _stricmp
# define strncasecmp _strnicmp
#endif
static RAMFile *FindRAMFile(const char *path) {
#define PROTOCOL "ram://"
	RAMFile *f;

	//if (strncasecmp(path, PROTOCOL, sizeof(PROTOCOL) - 1) != 0)

	MessageBoxA(GetActiveWindow(), path, "Part 1", 64);

	if (strncmp(path, PROTOCOL, sizeof(PROTOCOL) - 1) != 0)
		return (RAMFile *)NULL;

	path += sizeof(PROTOCOL) - 1;

	MessageBoxA(GetActiveWindow(), path, "Part 2", 64);

	LockList();
	for(f=g_head; f; f=f->next) {
		if (!strcasecmp(f->filename.c_str(), path))
			break;
	}
	UnlockList();

	return f;
}

static FILE *C ramfs_fopen(const char *filename, const char *mode) {
	RAMFile *rf;

	rf = FindRAMFile(filename);
	if (!rf)
		return fopen(filename, mode);

	SetRAMFileFromFile(rf, (FILE *)rf);
	return (FILE *)rf;
}
static size_t C ramfs_fread(void *buf, size_t s, size_t c, FILE *f) {
	RAMFile *rf;
	size_t n;

	rf = RAMFileFromFile(f);
	if (!rf)
		return fread(buf, s, c, f);

	n = s*c;
	while(n && rf->npos + n >= rf->nptr)
		n = s*(--c);

	if (!n)
		return 0;

	memcpy(buf, &((unsigned char *)rf->ptr)[rf->npos], n);
	rf->npos += n;

	return c;
}
static int C ramfs_fseek(FILE *f, long offset, int origin) {
	RAMFile *rf;
	size_t n;

	rf = RAMFileFromFile(f);
	if (!rf)
		return fseek(f, offset, origin);

	n = (size_t)-1;

	switch(origin) {
	case SEEK_SET:
		n = (size_t)offset;
		break;
	case SEEK_CUR:
		n = rf->npos + (size_t)offset;
		break;
	case SEEK_END:
		n = rf->nptr + (size_t)offset;
		break;
	}

	if (n > rf->nptr)
		return 0;

	return 1;
}
static long C ramfs_ftell(FILE *f) {
	RAMFile *rf;

	rf = RAMFileFromFile(f);
	if (!rf)
		return ftell(f);

	return (long)rf->npos;
}
static int C ramfs_fflush(FILE *f) {
	RAMFile *rf;

	rf = RAMFileFromFile(f);
	if (!rf)
		return fflush(f);

	return 0;
}
static int C ramfs_feof(FILE *f) {
	RAMFile *rf;

	rf = RAMFileFromFile(f);
	if (!rf)
		return feof(f);

	return rf->npos>=rf->nptr ? 1 : 0;
}
static int C ramfs_ferror(FILE *f) {
	RAMFile *rf;

	rf = RAMFileFromFile(f);
	if (!rf)
		return ferror(f);

	return 0;
}
static void C ramfs_clearerr(FILE *f) {
	if (!RAMFileFromFile(f))
		clearerr(f);
}
static int C ramfs_remove(const char *path) {
	if (g_remove)
		return remove(path);

	return -1;
}

static void S ramfs_CloneFile(func_str srcfile, func_str dstfile) {
	CopyFile(srcfile, dstfile, FALSE);
	//if (g_CloneFile)
	//	g_CloneFile(srcfile, dstfile);
}
static bool S ramfs_RelocateFile(func_str srcfile, func_str dstfile) {
	return (bool)MoveFile(srcfile, dstfile);

	//if (g_RelocateFile)
	//	return g_RelocateFile(srcfile, dstfile);

	//return false;
}
static WORD S ramfs_FileExists(func_str filename) {
	if (FindRAMFile(filename))
		return 1;

	//if (g_FileExists)
	//	return g_FileExists(filename);
	{
		FILE *f;

		f = fopen(filename, "rb");
		if (f) {
			fclose(f);
			return 1;
		}
	}

	return 0;
}
static DWORD S ramfs_FileSize(func_str filename) {
	RAMFile *rf;

	rf = FindRAMFile(filename);
	if (rf)
		return (DWORD)rf->nptr;

	{
		FILE *f;

		f = fopen(filename, "rb");
		if (f) {
			long x;

			fseek(f, 0, SEEK_END);
			x = ftell(f);
			fclose(f);

			return x;
		}

		return 0;
	}
	//return g_FileSize ? g_FileSize(filename) : 0;
}
static bool S ramfs_MakeDirectory(func_str foldername) {
	return (bool)CreateDirectory(foldername, NULL);
	//return g_MakeDirectory ? g_MakeDirectory(foldername) : false;
}
static bool S ramfs_DeleteDirectory(func_str foldername) {
	return (bool)RemoveDirectory(foldername);
	//return g_DeleteDirectory ? g_DeleteDirectory(foldername) : false;
}

static HANDLE S ramfs_FindFirstFile(const char *path, LPWIN32_FIND_DATA fd) {
	return FindFirstFile(path, fd);
	//return g_FindFirstFile ? g_FindFirstFile(path, fd) : (HANDLE)0;
}
static BOOL S ramfs_FindNextFile(HANDLE h, LPWIN32_FIND_DATA fd) {
	return FindNextFile(h, fd);
	//return g_FindNextFile ? g_FindNextFile(h, fd) : FALSE;
}
static BOOL S ramfs_SetCurrentDirectory(const char *dir) {
	return SetCurrentDirectory(dir);
	//return g_SetCurrentDirectory ? g_SetCurrentDirectory(dir) : FALSE;
}
static DWORD S ramfs_GetCurrentDirectory(DWORD buflen, char *buf) {
	return GetCurrentDirectory(buflen, buf);
	//return g_GetCurrentDirectory ? g_GetCurrentDirectory(buflen, buf) : 0;
}



int RAMFS_Init() {
	if (likely(g_didInit==true))
		return 1;

	InitializeCriticalSection(&g_lock);

	g_didInit = true;
	return 1;
}
void RAMFS_Fini() {
	if (unlikely(g_didInit==false))
		return;

	LockList();
	while(g_head)
		RAMFS_DeleteFile(g_head);
	UnlockList();

	g_didInit = false;
}

RAMFile *RAMFS_NewFile(const char *name, void *ptr, size_t nptr) {
	RAMFile *rf;

	rf = FindRAMFile(name);
	if (unlikely(rf != (RAMFile *)NULL))
		return (RAMFile *)NULL;

	rf = new RAMFile();
	if (unlikely(!rf))
		return (RAMFile *)NULL;

	rf->filename.assign(name);

	rf->ptr = ptr;
	rf->nptr = nptr;

	rf->npos = 0;

	rf->ownsptr = 0;

	InitializeCriticalSection(&rf->lock);

	rf->next = (RAMFile *)NULL;
	LockList();
	if (likely((rf->prev = g_tail) != (RAMFile *)NULL))
		g_tail->next = rf;
	else
		g_head = rf;
	g_tail = rf;
	UnlockList();

	return rf;
}
RAMFile *RAMFS_DeleteFile(RAMFile *rf) {
	if (!rf)
		return (RAMFile *)NULL;

	LockFile(rf);
	rf->filename.clear();

	if (rf->ownsptr) {
		if (likely(rf->ptr != (void *)NULL))
			HeapFree(GetProcessHeap(), 0, rf->ptr);
	}
	rf->ptr = (void *)NULL;
	rf->nptr = 0;

	LockList();
	if (likely(rf->prev != (RAMFile *)NULL))
		rf->prev->next = rf->next;
	else
		g_head = rf->next;
	if (likely(rf->next != (RAMFile *)NULL))
		rf->next->prev = rf->prev;
	else
		g_tail = rf->prev;
	UnlockList();
	UnlockFile(rf);

	delete rf;
	return (RAMFile *)NULL;
}
int RAMFS_LockFile(RAMFile *rf) {
	CheckRAMFile(rf);

	LockFile(rf);
	return 1;
}
int RAMFS_TryLockFile(RAMFile *rf) {
	CheckRAMFile(rf);

	if (!TryEnterCriticalSection(&rf->lock))
		return 0;

	return 1;
}
int RAMFS_UnlockFile(RAMFile *rf) {
	CheckRAMFile(rf);

	UnlockFile(rf);
	return 1;
}

int RAMFS_IsEnabled() {
	return g_isEnabled;
}

void RAMFS_Enable() {
	if (g_isEnabled)
		return;
#define FUNC_DELM ;
#define FUNC_CALL(n,d,c,r,p) g_##n = (n##_t)GetDeveloperData(NDEV_FILE_##d,NULL)
	FUNC_LIST
#undef FUNC_CALL
#define FUNC_CALL(n,d,c,r,p) SetDeveloperData(NDEV_FILE_##d,(void *)ramfs_##n,\
		NULL)
	FUNC_LIST
#undef FUNC_CALL
#undef FUNC_DELM
	g_isEnabled = 1;
}
void RAMFS_Disable() {
	if (!g_isEnabled)
		return;
#define FUNC_CALL(n,d,c,r,p) SetDeveloperData(NDEV_FILE_##d,(void *)g_##n,NULL)
#define FUNC_DELM ;
	FUNC_LIST
#undef FUNC_DELM
#undef FUNC_CALL
	g_isEnabled = 0;
}

RAMFile *RAMFS_MakeMemoryFile(const char *name, size_t nptr) {
	RAMFile *rf;
	void *ptr;

	ptr = HeapAlloc(GetProcessHeap(), 0, nptr);
	if (unlikely(!ptr))
		return (RAMFile *)NULL;

	rf = RAMFS_NewFile(name, ptr, nptr);
	if (unlikely(!rf)) {
		HeapFree(GetProcessHeap(), 0, ptr);
		return (RAMFile *)NULL;
	}

	rf->ownsptr = 1;

	return rf;
}
int RAMFS_ReadFile(RAMFile *rf, void *dst, size_t n) {
	CheckRAMFile(rf);

	if (unlikely(rf->npos + n >= rf->nptr))
		return 0;

	memcpy(dst, &((unsigned char *)rf->ptr)[rf->npos], n);
	rf->npos += n;

	return 1;
}
int RAMFS_WriteFile(RAMFile *rf, const void *src, size_t n) {
	CheckRAMFile(rf);

	if (unlikely(rf->npos + n >= rf->nptr))
		return 0;

	memcpy(&((unsigned char *)rf->ptr)[rf->npos], src, n);
	rf->npos += n;

	return 1;
}
size_t RAMFS_FileSize(RAMFile *rf) {
	CheckRAMFile(rf);

	return rf->nptr;
}
size_t RAMFS_GetFilePos(RAMFile *rf) {
	CheckRAMFile(rf);

	return rf->npos;
}
int RAMFS_SetFilePos(RAMFile *rf, size_t n) {
	CheckRAMFile(rf);

	if (n > rf->nptr)
		return 0;

	rf->npos = n;
	return 1;
}
int RAMFS_IsEOF(RAMFile *rf) {
	CheckRAMFile(rf);

	if (rf->npos >= rf->nptr)
		return 1;

	return 0;
}

void *RAMFS_GetDirectPtr(RAMFile *rf) {
	CheckRAMFile(rf);

	return rf->ptr;
}
