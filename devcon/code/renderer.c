#include <GL/glfw.h>
#include <math.h>

#include "renderer.h"
#include "devcon.h"

#include "system.h"

color_t g_bgColor = { 0.4f, 0.7f, 0.9f, 1.0f };

int g_vp[4] = { 0, 0, 0, 0 };

/* Perspective matrix */
static void Perspective(float fov, float aspect, float zn, float zf) {
	GLfloat p[16];
	float x, y, a, b;

	y = 1 / tanf(fov/2.0f/180.0f*3.1415926535f); /*that's a weird constant*/
	x = y / aspect;

	a =     zf/(zf - zn);
	b = -zn*zf/(zf - zn);

#if 0
	p[ 0]=x; p[ 1]=0; p[ 2]=0; p[ 3]=0;
	p[ 4]=0; p[ 5]=y; p[ 6]=0; p[ 7]=0;
	p[ 8]=0; p[ 9]=0; p[10]=a; p[11]=b;
	p[12]=0; p[13]=0; p[14]=1; p[15]=0;
#else
	p[ 0]=x; p[ 4]=0; p[ 8]=0; p[12]=0;
	p[ 1]=0; p[ 5]=y; p[ 9]=0; p[13]=0;
	p[ 2]=0; p[ 6]=0; p[10]=a; p[14]=b;
	p[ 3]=0; p[ 7]=0; p[11]=1; p[15]=0;
#endif

	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(p);

	glMatrixMode(GL_MODELVIEW);
}

/*
 * =============================================================================
 *
 *	RENDERER
 *
 * =============================================================================
 */

void r_Frame(double time) {
	static float angle = 0.0f;

	if (time) {/*unused*/}

	glViewport(g_vp[0], g_vp[1], g_vp[2], g_vp[3]);

	r_Clear();
	{
		/* prepare the perspective projection */
		Perspective(60.0f, (float)v_Width()/(float)v_Height(), 1.0f, 1000.0f);

		/* render a triangle */
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, 1.0f);
		glRotatef(angle, 0.0f, 1.0f, 0.0f);

		angle = angle + 180.0f*time; /*180 degrees each second*/

		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_LIGHTING);
		glDisable(GL_ALPHA_TEST);
		glDisable(GL_BLEND);

		glBegin(GL_TRIANGLES);
			glColor3f(1.0f, 0.0f, 0.0f);  glVertex3f(-0.5f, -0.5f, 0.0f);
			glColor3f(0.0f, 1.0f, 0.0f);  glVertex3f( 0.0f,  0.5f, 0.0f);
			glColor3f(0.0f, 0.0f, 1.0f);  glVertex3f( 0.5f, -0.5f, 0.0f);
		glEnd();

		/* render the developer console */
		r_Console();
	}
	r_Flip();
}

void r_Console() {
	if (!con_IsAlive())
		return;

	if (!con_IsVisible())
		return;

	con_Render();
}

void r_Clear() {
	glClearColor(g_bgColor[0], g_bgColor[1], g_bgColor[2], g_bgColor[3]);
	glClear(GL_COLOR_BUFFER_BIT);
}

void r_Flip() {
	glfwSwapBuffers();
}

void r_DrawRect(int rct[4], vec4_t color) {
	vec2_t size;
	vec2_t tl, br;

	v2_Set(size, v_Width(), v_Height());

	v2_ScreenToProj(tl, Vec2(rct[0], rct[1]), size);
	v2_ScreenToProj(br, Vec2(rct[2], rct[3]), size);

	/*
	 * TODO: Fix this; immediate mode is fugly.
	 */
	glBegin(GL_TRIANGLE_STRIP);
	{
		glColor4f(color[0], color[1], color[2], color[3]);

		glVertex2f(tl[0], tl[1]);
		glVertex2f(tl[0], br[1]);
		glVertex2f(br[0], tl[1]);
		glVertex2f(br[0], br[1]);
	}
	glEnd();
}

/*
 * =============================================================================
 *
 *	VIEWPORT
 *
 * =============================================================================
 */

void v_Offset(int x, int y) {
	g_vp[0] = x;
	g_vp[1] = y;
}
void v_Resize(int w, int h) {
	sys_Log("Viewport resize: %i,%i\n", w, h);

	g_vp[2] = w;
	g_vp[3] = h;
}
int v_X() {
	return g_vp[0];
}
int v_Y() {
	return g_vp[1];
}
int v_Width() {
	return g_vp[2];
}
int v_Height() {
	return g_vp[3];
}
