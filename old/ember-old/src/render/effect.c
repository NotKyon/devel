#include <ember/effect.h>
#include <string.h>
#include <malloc.h>
#include <stdio.h>

static int __fx_init( struct stEffect *self )
{
	self->name = ( char * )0;

	self->pr_head = ( struct stProperty * )0;
	self->pr_tail = ( struct stProperty * )0;

	self->gr_head = ( struct stGroup * )0;
	self->gr_tail = ( struct stGroup * )0;

	self->tc_head = ( struct stTechnique * )0;
	self->tc_tail = ( struct stTechnique * )0;

	self->fx_next = ( struct stEffect * )0;
	if( ( self->fx_prev = g_fx_tail ) != ( struct stEffect * )0 )
		g_fx_tail->fx_next = self;
	else
		g_fx_head = self;
	g_fx_tail     = self;

	return 1;
}
static void __fx_deinit( struct stEffect *self )
{
	while( self->tc_head )
		fx_dealloc_technique( self->tc_head );

	while( self->pr_head )
		fx_dealloc_property( self->pr_head );

	while( self->gr_head )
		fx_dealloc_group( self->gr_head );

	if( self->name )
		free( ( void * )self->name );

	if( self->fx_prev )
		self->fx_prev->fx_next = self->fx_next;
	if( self->fx_next )
		self->fx_next->fx_prev = self->fx_prev;

	if( g_fx_head == self )
		g_fx_head = self->fx_next;
	if( g_fx_tail == self )
		g_fx_tail = self->fx_prev;
}

static int __property_init( struct stProperty *self )
{
	self->name = ( char * )0;
	self->desc = ( char * )0;

	self->kind = 0;

	self->effect  = ( struct stEffect * )0;
	self->pr_prev = ( struct stProperty * )0;
	self->pr_next = ( struct stProperty * )0;

	return 1;
}
static void __property_deinit( struct stProperty *self )
{
	if( self->name )
		free( ( void * )self->name );
	if( self->desc )
		free( ( void * )self->desc );

	if( self->kind == EPropertyKind_Texture2D ||
	    self->kind == EPropertyKind_TextureRect ||
	    self->kind == EPropertyKind_TextureCube )
		if( self->value.name )
			free( ( void * )self->value.name );

	if( !self->effect )
		return;

	if( self->pr_prev )
		self->pr_prev->pr_next = self->pr_next;
	if( self->pr_next )
		self->pr_next->pr_prev = self->pr_prev;

	if( self->effect->pr_head == self )
		self->effect->pr_head = self->pr_next;
	if( self->effect->pr_tail == self )
		self->effect->pr_tail = self->pr_prev;
}

static int __group_init( struct stGroup *self )
{
	self->effect = ( struct stEffect * )0;

	self->gr_prev = ( struct stGroup * )0;
	self->gr_next = ( struct stGroup * )0;

	return 1;
}
static void __group_deinit( struct stGroup *self )
{
	if( self->gr_prev )
		self->gr_prev->gr_next = self->gr_next;
	if( self->gr_next )
		self->gr_next->gr_prev = self->gr_prev;

	if( self->effect->gr_head == self )
		self->effect->gr_head = self->gr_next;
	if( self->effect->gr_tail == self )
		self->effect->gr_tail = self->gr_prev;
}

static int __technique_init( struct stTechnique *self )
{
	self->tags.tg_head = ( struct stTag * )0;
	self->tags.tg_tail = ( struct stTag * )0;

	self->ps_head = ( struct stPass * )0;
	self->ps_tail = ( struct stPass * )0;

	self->tc_prev = ( struct stTechnique * )0;
	self->tc_next = ( struct stTechnique * )0;

	return 1;
}
static void __technique_deinit( struct stTechnique *self )
{
	while( self->tags.tg_head )
		fx_dealloc_tag( self->tags.tg_head );

	while( self->ps_head )
		fx_dealloc_pass( self->ps_head );

	if( self->tc_prev )
		self->tc_prev->tc_next = self->tc_next;
	if( self->tc_next )
		self->tc_next->tc_prev = self->tc_prev;

	if( self->effect->tc_head == self )
		self->effect->tc_head = self->tc_next;
	if( self->effect->tc_tail == self )
		self->effect->tc_tail = self->tc_prev;
}

static int __pass_init( struct stPass *self )
{
	self->name = ( char * )0;

	self->owner = ( struct stTechnique * )0;
	self->group = ( struct stGroup * )0;

	self->tags.tg_head = ( struct stTag * )0;
	self->tags.tg_tail = ( struct stTag * )0;

	self->ps_prev = ( struct stPass * )0;
	self->ps_next = ( struct stPass * )0;

	return 1;
}
static void __pass_deinit( struct stPass *self )
{
	while( self->tags.tg_head )
		fx_dealloc_tag( self->tags.tg_head );

	if( self->name )
		free( ( void * )self->name );

	if( self->ps_prev )
		self->ps_prev->ps_next = self->ps_next;
	if( self->ps_next )
		self->ps_next->ps_prev = self->ps_prev;

	if( self->owner->ps_head == self )
		self->owner->ps_head = self->ps_next;
	if( self->owner->ps_tail == self )
		self->owner->ps_tail = self->ps_prev;
}

static int __tag_init( struct stTag *self )
{
	self->name  = ( char * )0;
	self->value = ( char * )0;

	self->owner   = ( struct stTagOwner * )0;
	self->tg_prev = ( struct stTag * )0;
	self->tg_next = ( struct stTag * )0;

	return 1;
}
static void __tag_deinit( struct stTag *self )
{
	if( self->name )
		free( ( void * )self->name );
	if( self->value )
		free( ( void * )self->value );

	if( !self->owner )
		return;

	if( self->tg_prev )
		self->tg_prev->tg_next = self->tg_next;
	if( self->tg_next )
		self->tg_next->tg_prev = self->tg_prev;

	if( self->owner->tg_head == self )
		self->owner->tg_head = self->tg_next;
	if( self->owner->tg_tail == self )
		self->owner->tg_tail = self->tg_prev;
}

int fx_init()
{
	if( !( g_fx_pool =
	            gc_register_pool( sizeof( struct stEffect ), ( gc_init_t )__fx_init,
	                ( gc_deinit_t )__fx_deinit ) ) )
		return 0;
	if( !( g_fx_tech_pool = gc_register_pool( sizeof( struct stTechnique ),
	           ( gc_init_t )__technique_init,
	           ( gc_deinit_t )__technique_deinit ) ) ) {
		gc_unregister_pool( g_fx_pool );
		return 0;
	}
	if( !( g_fx_group_pool =
	            gc_register_pool( sizeof( struct stGroup ), ( gc_init_t )__group_init,
	                ( gc_deinit_t )__group_deinit ) ) ) {
		gc_unregister_pool( g_fx_tech_pool );
		gc_unregister_pool( g_fx_pool );
		return 0;
	}
	if( !( g_fx_pass_pool =
	            gc_register_pool( sizeof( struct stPass ), ( gc_init_t )__pass_init,
	                ( gc_deinit_t )__pass_deinit ) ) ) {
		gc_unregister_pool( g_fx_group_pool );
		gc_unregister_pool( g_fx_tech_pool );
		gc_unregister_pool( g_fx_pool );
		return 0;
	}

	if( !( g_fx_tag_pool =
	            gc_register_pool( sizeof( struct stTag ), ( gc_init_t )__tag_init,
	                ( gc_deinit_t )__tag_deinit ) ) ) {
		gc_unregister_pool( g_fx_pass_pool );
		gc_unregister_pool( g_fx_group_pool );
		gc_unregister_pool( g_fx_tech_pool );
		gc_unregister_pool( g_fx_pool );
		return 0;
	}
	if( !( g_fx_pr_pool = gc_register_pool( sizeof( struct stProperty ),
	           ( gc_init_t )__property_init,
	           ( gc_deinit_t )__property_deinit ) ) ) {
		gc_unregister_pool( g_fx_tag_pool );
		gc_unregister_pool( g_fx_pass_pool );
		gc_unregister_pool( g_fx_group_pool );
		gc_unregister_pool( g_fx_tech_pool );
		gc_unregister_pool( g_fx_pool );
		return 0;
	}

	return 1;
}
void fx_deinit()
{
	gc_unregister_pool( g_fx_pr_pool );
	gc_unregister_pool( g_fx_group_pool );
	gc_unregister_pool( g_fx_pass_pool );
	gc_unregister_pool( g_fx_tech_pool );
	gc_unregister_pool( g_fx_tag_pool );
	gc_unregister_pool( g_fx_pool );
}

effect_t fx_alloc( const char *name )
{
	effect_t effect;

	if( !( effect = gc_alloc( g_fx_pool ) ) )
		return ( effect_t )0;

	effect->name = name ? strdup( name ) : ( char * )0;

	return effect;
}
void fx_dealloc( effect_t effect )
{
	gc_release( ( void * )effect );
}

property_t fx_alloc_property( effect_t effect, const char *name )
{
	property_t pr;

	if( !( pr = ( property_t )gc_alloc( g_fx_pr_pool ) ) )
		return ( property_t )0;

	pr->name = name ? strdup( name ) : ( char * )0;

	pr->effect  = effect;
	pr->pr_next = ( property_t )0;
	if( ( pr->pr_prev = effect->pr_tail ) != ( property_t )0 )
		effect->pr_tail->pr_next = pr;
	else
		effect->pr_head = pr;
	effect->pr_tail     = pr;

	return pr;
}
void fx_dealloc_property( property_t property )
{
	gc_release( ( void * )property );
}

group_t fx_alloc_group( effect_t effect )
{
	group_t g;

	if( !( g = gc_alloc( g_fx_group_pool ) ) )
		return ( group_t )0;

	g->effect = effect;

	g->gr_next = ( struct stGroup * )0;
	if( ( g->gr_prev = effect->gr_tail ) != ( struct stGroup * )0 )
		effect->gr_tail->gr_next = g;
	else
		effect->gr_head = g;
	effect->gr_tail     = g;

	return g;
}
void fx_dealloc_group( group_t group )
{
	gc_release( ( void * )group );
}

technique_t fx_alloc_technique( effect_t effect )
{
	technique_t tech;

	if( !( tech = ( technique_t )gc_alloc( g_fx_tech_pool ) ) )
		return ( technique_t )0;

	tech->effect = effect;

	tech->tc_next = ( technique_t )0;
	if( ( tech->tc_prev = effect->tc_tail ) != ( technique_t )0 )
		effect->tc_tail->tc_next = tech;
	else
		effect->tc_head = tech;
	effect->tc_tail     = tech;

	return tech;
}
void fx_dealloc_technique( technique_t tech )
{
	gc_release( ( technique_t )tech );
}

pass_t fx_alloc_pass( technique_t tech, const char *name )
{
	pass_t pass;

	if( !( pass = ( pass_t )gc_alloc( g_fx_pass_pool ) ) )
		return ( pass_t )0;

	pass->name = name ? strdup( name ) : ( char * )0;

	pass->owner   = tech;
	pass->ps_next = ( pass_t )0;
	if( ( pass->ps_prev = tech->ps_tail ) != ( pass_t )0 )
		tech->ps_tail->ps_next = pass;
	else
		tech->ps_head = pass;
	tech->ps_tail     = pass;

	return pass;
}
void fx_dealloc_pass( pass_t pass )
{
	gc_release( ( void * )pass );
}

tagowner_t fx_technique_tagowner( technique_t tech )
{
	return &tech->tags;
}
tagowner_t fx_pass_tagowner( pass_t pass )
{
	return &pass->tags;
}
tag_t fx_alloc_tag( tagowner_t owner, const char *name )
{
	tag_t tg;

	if( !( tg = ( tag_t )gc_alloc( g_fx_tag_pool ) ) )
		return ( tag_t )0;

	tg->name = name ? strdup( name ) : ( char * )0;

	tg->owner   = owner;
	tg->tg_next = ( tag_t )0;
	if( ( tg->tg_prev = owner->tg_tail ) != ( tag_t )0 )
		owner->tg_tail->tg_next = tg;
	else
		owner->tg_head = tg;
	owner->tg_tail     = tg;

	return tg;
}
void fx_dealloc_tag( tag_t tg )
{
	gc_release( ( void * )tg );
}

void fx_set_name( effect_t effect, const char *name )
{
	if( effect->name )
		free( ( void * )effect->name );

	effect->name = name ? strdup( name ) : ( char * )0;
}

void fx_set_pass_name( pass_t pass, const char *name )
{
	if( pass->name )
		free( ( void * )pass->name );

	pass->name = name ? strdup( name ) : ( char * )0;
}

void fx_set_tag_name( tag_t tg, const char *name )
{
	if( tg->name )
		free( ( void * )tg->name );

	tg->name = name ? strdup( name ) : ( char * )0;
}
void fx_set_tag_value( tag_t tg, const char *value )
{
	if( tg->value )
		free( ( void * )tg->value );

	tg->value = value ? strdup( value ) : ( char * )0;
}

const char *fx_tag_name( tag_t tg )
{
	return tg->name;
}
const char *fx_tag_value( tag_t tg )
{
	return tg->value;
}

void fx_set_property_name( property_t pr, const char *name )
{
	if( pr->name )
		free( ( void * )pr->name );

	pr->name = name ? strdup( name ) : ( char * )0;
}
void fx_set_property_desc( property_t pr, const char *desc )
{
	if( pr->desc )
		free( ( void * )pr->desc );

	pr->desc = desc ? strdup( desc ) : ( char * )0;
}

void fx_set_property_kind( property_t pr, EPropertyKind kind )
{
	if( pr->kind == EPropertyKind_Texture2D ||
	    pr->kind == EPropertyKind_TextureRect ||
	    pr->kind == EPropertyKind_TextureCube )
		if( pr->value.name )
			free( ( void * )pr->value.name );

	pr->kind = kind;
	if( kind == EPropertyKind_Texture2D || kind == EPropertyKind_TextureRect ||
	    kind == EPropertyKind_TextureCube ) {
		pr->options.flags = 0;
		pr->value.name = ( char * )0;
	} else if( kind == EPropertyKind_Range || kind == EPropertyKind_Float ) {
		pr->options.range[ 0 ] = 0;
		pr->options.range[ 1 ] = 1;
		pr->value.scalar = 0;
	} else if( kind == EPropertyKind_Color || kind == EPropertyKind_Vector ) {
		pr->value.vector[ 0 ] = 0;
		pr->value.vector[ 1 ] = 0;
		pr->value.vector[ 2 ] = 0;
		pr->value.vector[ 3 ] = 0;
	}
}

void fx_set_property_range( property_t pr, float l, float h )
{
	pr->options.range[ 0 ] = l;
	pr->options.range[ 1 ] = h;
}
void fx_set_property_flags( property_t pr, bitfield_t flags )
{
	pr->options.flags = flags;
}
void fx_set_property_texgen( property_t pr, EPropertyTexGen texgen )
{
	pr->options.texgen = texgen;
}
void fx_set_property_lightmap_mode( property_t pr, int on )
{
	if( on )
		pr->options.flags |= 1;
	else
		pr->options.flags &= ~1;
}

void fx_set_property_scalar( property_t pr, float v )
{
	pr->value.scalar = v;
}
void fx_set_property_vector( property_t pr, float x, float y, float z, float w )
{
	pr->value.vector[ 0 ] = x;
	pr->value.vector[ 1 ] = y;
	pr->value.vector[ 2 ] = z;
	pr->value.vector[ 3 ] = w;
}
void fx_set_property_string( property_t pr, const char *s )
{
	if( pr->kind != EPropertyKind_Texture2D &&
	    pr->kind != EPropertyKind_TextureRect &&
	    pr->kind != EPropertyKind_TextureCube )
		return;

	if( pr->value.name )
		free( ( void * )pr->value.name );

	pr->value.name = s ? strdup( s ) : ( char * )0;
}

const char *fx_name( effect_t effect )
{
	return effect->name;
}
const char *fx_property_name( property_t pr )
{
	return pr->name;
}
const char *fx_property_desc( property_t pr )
{
	return pr->desc;
}
const char *fx_pass_name( pass_t pass )
{
	return pass->name;
}

EPropertyKind fx_property_kind( property_t pr )
{
	return pr->kind;
}
float fx_property_range( property_t pr, int i )
{
	return pr->options.range[ i ];
}
bitfield_t fx_property_flags( property_t pr )
{
	return pr->options.flags;
}
EPropertyTexGen fx_property_texgen( property_t pr )
{
	return pr->options.texgen;
}
int fx_property_lightmap_mode( property_t pr )
{
	return pr->options.flags & 1 ? 1 : 0;
}

float fx_property_range_low( property_t pr )
{
	return pr->options.range[ 0 ];
}
float fx_property_range_high( property_t pr )
{
	return pr->options.range[ 1 ];
}

float fx_property_scalar( property_t pr )
{
	return pr->value.scalar;
}

float fx_property_vec_x( property_t pr )
{
	return pr->value.vector[ 0 ];
}
float fx_property_vec_y( property_t pr )
{
	return pr->value.vector[ 1 ];
}
float fx_property_vec_z( property_t pr )
{
	return pr->value.vector[ 2 ];
}
float fx_property_vec_w( property_t pr )
{
	return pr->value.vector[ 3 ];
}

const char *fx_property_string( property_t pr )
{
	if( pr->kind == EPropertyKind_Texture2D ||
	    pr->kind == EPropertyKind_TextureRect ||
	    pr->kind == EPropertyKind_TextureCube )
		return pr->value.name ? pr->value.name : "";

	return "";
}

effect_t fx_first()
{
	return g_fx_head;
}
effect_t fx_last()
{
	return g_fx_tail;
}
effect_t fx_before( effect_t effect )
{
	return effect->fx_prev;
}
effect_t fx_after( effect_t effect )
{
	return effect->fx_next;
}

property_t fx_property_first( effect_t effect )
{
	return effect->pr_head;
}
property_t fx_property_last( effect_t effect )
{
	return effect->pr_tail;
}
property_t fx_property_before( property_t pr )
{
	return pr->pr_prev;
}
property_t fx_property_after( property_t pr )
{
	return pr->pr_next;
}

tag_t fx_tag_first( tagowner_t owner )
{
	return owner->tg_head;
}
tag_t fx_tag_last( tagowner_t owner )
{
	return owner->tg_tail;
}
tag_t fx_tag_before( tag_t tg )
{
	return tg->tg_prev;
}
tag_t fx_tag_after( tag_t tg )
{
	return tg->tg_next;
}

group_t fx_group_first( effect_t effect )
{
	return effect->gr_head;
}
group_t fx_group_last( effect_t effect )
{
	return effect->gr_tail;
}
group_t fx_group_before( group_t g )
{
	return g->gr_prev;
}
group_t fx_group_after( group_t g )
{
	return g->gr_next;
}

technique_t fx_technique_first( effect_t effect )
{
	return effect->tc_head;
}
technique_t fx_technique_last( effect_t effect )
{
	return effect->tc_tail;
}
technique_t fx_technique_before( technique_t tech )
{
	return tech->tc_prev;
}
technique_t fx_technique_after( technique_t tech )
{
	return tech->tc_next;
}

pass_t fx_pass_first( technique_t tech )
{
	return tech->ps_head;
}
pass_t fx_pass_tail( technique_t tech )
{
	return tech->ps_tail;
}
pass_t fx_pass_before( pass_t pass )
{
	return pass->ps_prev;
}
pass_t fx_pass_after( pass_t pass )
{
	return pass->ps_next;
}

effect_t fx_find( const char *name )
{
	effect_t effect;

	for( effect = fx_first(); effect; effect = fx_after( effect ) ) {
		if( !effect->name )
			continue;

		if( !strcmp( effect->name, name ) )
			return effect;
	}

	return ( effect_t )0;
}

pass_t fx_find_pass( technique_t tech, const char *name )
{
	pass_t pass;

	for( pass = fx_pass_first( tech ); pass; pass = fx_pass_after( pass ) ) {
		if( !pass->name )
			continue;

		if( !strcmp( pass->name, name ) )
			return pass;
	}

	return ( pass_t )0;
}
