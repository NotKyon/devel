#include <AxTek/Threading/CPU.h>

#include <stdio.h>
#include <stdlib.h>

int main() {
	printf("cpu count: %u\n", ax::task::get_cpu_count());

#if AXTEK_ARCH_X86
	ax::u32 cores, threads;

	ax::task::x86::get_cpu_count(cores, threads);
	printf("x86 cpu core count  : %u\n", cores);
	printf("x86 cpu thread count: %u\n", threads);

	if (threads != ax::task::get_cpu_count())
		return EXIT_FAILURE;
#endif

	return EXIT_SUCCESS;
}
