#ifndef MK_MAIN_H
#define MK_MAIN_H

#include "mk_types.h"

enum {
	kFlag_ShowHelp_Bit = BIT(0),
	kFlag_ShowVersion_Bit = BIT(1),
	kFlag_Verbose_Bit = BIT(2),
	kFlag_Release_Bit = BIT(3),
	kFlag_Rebuild_Bit = BIT(4),
	kFlag_NoCompile_Bit = BIT(5),
	kFlag_NoLink_Bit = BIT(6),
	kFlag_Clean_Bit = BIT(7),
	kFlag_PrintHierarchy_Bit = BIT(8),
	kFlag_Pedantic_Bit = BIT(9),
	kFlag_Profile_Bit = BIT(10)
};
extern bitfield_t g_flags;

#endif
