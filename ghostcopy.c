﻿/*

	Copy a directory tree to a new directory, creating blank files in place of
	the files that exist in the source directory.

*/

#ifdef _WIN32
# ifdef _MSC_VER
#  undef _CRT_SECURE_NO_WARNINGS
#  define _CRT_SECURE_NO_WARNINGS 1
# endif
# undef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
# undef min
# undef max
#endif
#include <time.h>
#include <errno.h>
#include <stdio.h>
#ifdef _MSC_VER
# include "dirent.h"
#else
# include <dirent.h>
#endif
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
# include <io.h>
# include <direct.h>
#else
# include <unistd.h>
#endif
#include <sys/stat.h>
#include <sys/types.h>
#ifndef _WIN32
# include <execinfo.h>
#endif

#ifdef _DEBUG
# undef _DEBUG
# define _DEBUG 1
#else
# if defined( DEBUG )||defined( __debug__ )||defined( __DEBUG__ )
#  define _DEBUG 1
# endif
#endif

#ifndef S_IFREG
# define S_IFREG 0100000
#endif
#ifndef S_IFDIR
# define S_IFDIR 0040000
#endif

#ifndef PATH_MAX
# ifdef MAX_PATH
#  define PATH_MAX MAX_PATH
# else
#  define PATH_MAX 512
# endif
#endif

#ifndef bitfield_t_defined
# define bitfield_t_defined 1
typedef size_t bitfield_t;
#endif

#ifndef NORETURN
# if _MSC_VER
#  define NORETURN _declspec( noreturn )
# else
#  define NORETURN __attribute__( ( noreturn ) )
# endif
#endif

/* macros for ASSERT errors */
#if _MSC_VER
# define __func__ __FUNCTION__
#endif
#if _DEBUG
# undef ASSERT
# undef ASSERT_MSG
# define ASSERT_MSG( x, m )\
	if( !( x ) )\
		Log_ErrorAssert( __FILE__, __LINE__, __func__, m )
# define ASSERT( x )\
	ASSERT_MSG( ( x ), "ASSERT( " #x " ) failed!" )
#else
# undef ASSERT
# undef ASSERT_MSG
# define ASSERT( x ) /*nothing*/
# define ASSERT_MSG( x, m ) /*nothing*/
#endif

/* memory logging */
#ifndef LOG_MEMORY_ALLOCS
# define LOG_MEMORY_ALLOCS 0
#endif

/* -------------------------------------------------------------------------- */
typedef enum
{
	SIO_OUT,
	SIO_ERR,

	__SIO_NUM__
} sio_t;

#define S_COLOR_BLACK			"^0"
#define S_COLOR_BLUE			"^1"
#define S_COLOR_GREEN			"^2"
#define S_COLOR_CYAN			"^3"
#define S_COLOR_RED				"^4"
#define S_COLOR_MAGENTA			"^5"
#define S_COLOR_BROWN			"^6"
#define S_COLOR_LIGHT_GREY		"^7"
#define S_COLOR_DARK_GREY		"^8"
#define S_COLOR_LIGHT_BLUE		"^9"
#define S_COLOR_LIGHT_GREEN		"^A"
#define S_COLOR_LIGHT_CYAN		"^B"
#define S_COLOR_LIGHT_RED		"^C"
#define S_COLOR_LIGHT_MAGENTA	"^D"
#define S_COLOR_LIGHT_BROWN		"^E"
#define S_COLOR_WHITE			"^F"

#define S_COLOR_PURPLE			"^5"
#define S_COLOR_YELLOW			"^E"
#define S_COLOR_VIOLET			"^D"

#define S_COLOR_RESTORE			"^&"

#define COLOR_BLACK				0x0
#define COLOR_BLUE				0x1
#define COLOR_GREEN				0x2
#define COLOR_CYAN				0x3
#define COLOR_RED				0x4
#define COLOR_MAGENTA			0x5
#define COLOR_BROWN				0x6
#define COLOR_LIGHT_GREY		0x7
#define COLOR_DARK_GREY			0x8
#define COLOR_LIGHT_BLUE		0x9
#define COLOR_LIGHT_GREEN		0xA
#define COLOR_LIGHT_CYAN		0xB
#define COLOR_LIGHT_RED			0xC
#define COLOR_LIGHT_MAGENTA		0xD
#define COLOR_LIGHT_BROWN		0xE
#define COLOR_WHITE				0xF

#define COLOR_PURPLE			0x5
#define COLOR_YELLOW			0xE
#define COLOR_VIOLET			0xD

void Sys_InitColoredOutput();
unsigned char Sys_GetCurrColor( sio_t sio );
void Sys_SetCurrColor( sio_t sio, unsigned char color );
void Sys_UncoloredPuts( sio_t sio, const char *text, size_t len );
int Sys_CharToColorCode( char c );
void Sys_Puts( sio_t sio, const char *text );
void Sys_Printf( sio_t sio, const char *format, ... );
void Sys_PrintStr( sio_t sio, unsigned char color, const char *str );
void Sys_PrintUint( sio_t sio, unsigned char color, unsigned int val );
void Sys_PrintInt( sio_t sio, unsigned char color, int val );

/* -------------------------------------------------------------------------- */
void Log_ErrorMsg( const char *message );
NORETURN void Log_FatalError( const char *message );
void Log_Error( const char *file, unsigned int line, const char *func,
	const char *message );
NORETURN void Log_ErrorAssert( const char *file, unsigned int line,
	const char *func, const char *message );

/* -------------------------------------------------------------------------- */
void *Com_Memory( void *p, size_t n );
const char *Com_Va( const char *format, ... );
size_t Com_Strlen( const char *src );
size_t Com_Strcat( char *buf, size_t bufn, const char *src );
size_t Com_Strncat( char *buf, size_t bufn, const char *src, size_t srcn );
void Com_Strcpy( char *dst, size_t dstn, const char *src );
void Com_Strncpy( char *buf, size_t bufn, const char *src, size_t srcn );
char *Com_Strdup( const char *cstr );

char *Com_Dup( char *dst, const char *src );
char *Com_DupN( char *dst, const char *src, size_t numchars );
char *Com_Append( char *dst, const char *src );
const char *Com_ExtractDir( char *buf, size_t n, const char *filename );
void Com_SubstExt( char *dst, size_t dstn, const char *src, const char *ext );
int Com_Shell( const char *format, ... );
int Com_MatchPath( const char *rpath, const char *apath );
int Com_GetIntDate();
const char *Com_FindArgEnd( const char *arg );
int Com_MatchArg( const char *a, const char *b );
const char *Com_SkipArg( const char *arg );
void Com_StripArgs( char *dst, size_t n, const char *src );
int Com_MatchPathChar( char a, char b );
int Com_RelPath( char *dst, size_t dstn, const char *curpath,
	const char *abspath );
int Com_RelPathCWD( char *dst, size_t dstn, const char *abspath );

/* -------------------------------------------------------------------------- */
typedef struct strList_s *strList_t;

strList_t SL_New();
size_t SL_Capacity( strList_t arr );
size_t SL_Size( strList_t arr );
char **SL_Data( strList_t arr );
char *SL_Get( strList_t arr, size_t i );
void SL_Set( strList_t arr, size_t i, const char *cstr );
void SL_Clear( strList_t arr );
void SL_Delete( strList_t arr );
void SL_DeleteAll();
void SL_Resize( strList_t arr, size_t n );
void SL_PushBack( strList_t arr, const char *cstr );
void SL_Print( strList_t arr );
void SL_DbgPrint( strList_t arr );
static int _SL_Cmp( const void *a, const void *b );
void SL_Sort( strList_t arr );
void SL_OrderedSort( strList_t arr, size_t *const buffer, size_t maxBuffer );
void SL_IndexedSort( strList_t arr, const size_t *const buffer,
	size_t bufferLen );
void SL_PrintOrderedBuffer( const size_t *const buffer, size_t bufferLen );

void SL_Unique( strList_t arr );

/* -------------------------------------------------------------------------- */
void FS_Init();
void FS_Fini();
char *FS_GetCWD( char *cwd, size_t n );
int FS_Enter( const char *path );
void FS_Leave();
int FS_IsFile( const char *path );
int FS_IsDir( const char *path );
void FS_MkDirs( const char *dirs );
char *FS_RealPath( const char *filename, char *resolvedname, size_t maxn );
DIR *FS_OpenDir( const char *path );
struct dirent *FS_ReadDir( DIR *d );


/*
 *	========================================================================
 *	DEBUGGING CODE
 *	========================================================================
 *	Debug helpers.
 */

#if _DEBUG
FILE *g_debugLog = ( FILE * )0;
#endif

void _Dbg_CloseLog() {
#if _DEBUG
	if( !g_debugLog )
	{
		return;
	}

	fprintf( g_debugLog, "\n\n[ [ ==  DEBUG LOG CLOSED  == ] ]\n\n" );

	fclose( g_debugLog );
	g_debugLog = ( FILE * )0;
#endif
}

/* write to the debug log, no formatting */
void Dbg_Out( const char *str ) {
#if _DEBUG
	if( !g_debugLog )
	{
		g_debugLog = fopen( "mk-debug.log", "a+" );
		if( !g_debugLog )
		{
			return;
		}

		fprintf( g_debugLog, "\n\n[[ == DEBUG LOG OPENED == ]]\n\n" );
		atexit( _Dbg_CloseLog );
	}

	fwrite( str, strlen( str ), 1, g_debugLog );
	fflush( g_debugLog );
#else
	( void )str;
#endif
}

/* write to the debug log ( va_args ) */
void Dbg_OutfV( const char *format, va_list args ) {
#if _DEBUG
	static char buf[ 65536 ];

# if __STDC_WANT_SECURE_LIB__
	vsprintf_s( buf, sizeof( buf ), format, args );
# else
	vsnprintf( buf, sizeof( buf ), format, args );
	buf[ sizeof( buf ) - 1 ] = '\0';
# endif

	Dbg_Out( buf );
#else
	( void )format;
	( void )args;
#endif
}
/* write to the debug log */
void Dbg_Outf( const char *format, ... )
{
	va_list args;

	va_start( args, format );
	Dbg_OutfV( format, args );
	va_end( args );
}

/* backtrace support... */
void Dbg_BackTrace() {
#if _WIN32
#else
	int i, n;
	void *buffer[ PATH_MAX ];
	char **strings;

	n = backtrace( buffer, sizeof( buffer )/sizeof( buffer[ 0 ] ) );
	if( n < 1 )
	{
		return;
	}

	strings = backtrace_symbols( buffer, n );
	if( !strings )
	{
		return;
	}

	Dbg_Outf( "BACKTRACE ( %i )\n", n );
	for( i=0; i < n; i++ )
	{
		Dbg_Outf( "  %s\n", strings[ i ] );
	}
	Dbg_Out( "\n" );

	free( ( void * )strings );
#endif
}

/*
 *	========================================================================
 *	COLORED PRINTING CODE
 *	========================================================================
 *	Display colored output.
 */

#if _WIN32
HANDLE g_sioh[ __SIO_NUM__ ];
#endif
FILE *g_siof[ __SIO_NUM__ ];

int Sys_IsColoredOutputEnabled()
{
	return 1;
}

void Sys_InitColoredOutput() {
#if _WIN32
	g_sioh[ SIO_OUT ] = GetStdHandle( STD_OUTPUT_HANDLE );
	g_sioh[ SIO_ERR ] = GetStdHandle( STD_ERROR_HANDLE );
#endif

	/*
	 * TODO: Make sure that colored output is possible on this terminal
	 */

	g_siof[ SIO_OUT ] = stdout;
	g_siof[ SIO_ERR ] = stderr;
}

unsigned char Sys_GetCurrColor( sio_t sio ) {
#if _WIN32
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	if( g_sioh[ sio ] != INVALID_HANDLE_VALUE )
	{
		if( g_sioh[ sio ] && GetConsoleScreenBufferInfo( g_sioh[ sio ],
		&csbi ) )
		{
			return csbi.wAttributes & 0xFF;
		}
	}
#else
	/*
	 * TODO: Support this on GNU/Linux...
	 */
	( void )sio;
#endif

	return 0x07;
}
void Sys_SetCurrColor( sio_t sio, unsigned char color )
{
	if( !Sys_IsColoredOutputEnabled() )
	{
		return;
	}

#if _WIN32
	if( g_sioh[ sio ] != INVALID_HANDLE_VALUE )
	{
		SetConsoleTextAttribute( g_sioh[ sio ], ( WORD )color );
	}
#else
	/*
	 * MAP ORDER:
	 * Black, Blue, Green, Cyan, Red, Magenta, Yellow, Grey
	 *
	 * TERMINAL COLOR ORDER:
	 * Black=0, Red=1, Green=2, Yellow=3, Blue=4, Magenta=5, Cyan=6, Grey=7
	 */
	static const char *const mapF[ 16 ] =
	{
		"\x1b[ 30;22m", "\x1b[ 34;22m", "\x1b[ 32;22m", "\x1b[ 36;22m",
		"\x1b[ 31;22m", "\x1b[ 35;22m", "\x1b[ 33;22m", "\x1b[ 37;22m",
		"\x1b[ 30;1m", "\x1b[ 34;1m", "\x1b[ 32;1m", "\x1b[ 36;1m",
		"\x1b[ 31;1m", "\x1b[ 35;1m", "\x1b[ 33;1m", "\x1b[ 37;1m"
	};
#if 0
	static const char *const mapB[ 16 ] =
	{
		"\x1b[ 40m", "\x1b[ 41m", "\x1b[ 42m", "\x1b[ 43m",
		"\x1b[ 44m", "\x1b[ 45m", "\x1b[ 46m", "\x1b[ 47m",
		"\x1b[ 40;1m", "\x1b[ 41;1m", "\x1b[ 42;1m", "\x1b[ 43;1m",
		"\x1b[ 44;1m", "\x1b[ 45;1m", "\x1b[ 46;1m", "\x1b[ 47;1m"
	};
#endif

	fwrite( mapF[ color & 0x0F ], strlen( mapF[ color & 0x0F ] ), 1,
		g_siof[ sio ] );
#endif
}
void Sys_UncoloredPuts( sio_t sio, const char *text, size_t len )
{
	if( !len )
	{
		len = strlen( text );
	}

	Dbg_Outf( "%.*s", len, text );

#if _WIN32 && 0
	if( g_sioh[ sio ] != INVALID_HANDLE_VALUE )
	{
		WriteFile( g_sioh[ sio ], text, len, NULL, NULL );
		return;
	}
#endif

	fwrite( text, len, 1, g_siof[ sio ] );
}
int Sys_CharToColorCode( char c )
{
	if( c >= '0' && c <= '9' )
	{
		return  0 + ( int )( c - '0' );
	}

	if( c >= 'A' && c <= 'F' )
	{
		return 10 + ( int )( c - 'A' );
	}

	if( c >= 'a' && c <= 'f' )
	{
		return 10 + ( int )( c - 'a' );
	}

	return -1;
}
void Sys_Puts( sio_t sio, const char *text )
{
	unsigned char prevColor;
	const char *s, *e;
	int color;

	/* set due to potential bad input pulling garbage color */
	prevColor = Sys_GetCurrColor( sio );

	s = text;
	while( 1 )
	{
		/* write normal characters in one chunk */
		if( *s != '^' )
		{
			e = strchr( s, '^' );
			if( !e )
			{
				e = strchr( s, '\0' );
			}

			Sys_UncoloredPuts( sio, s, e - s );
			if( *e == '\0' )
			{
				break;
			}

			s = e;
			continue;
		}

		/* must be a special character, treat it as such */
		color = Sys_CharToColorCode( *++s );
		if( color != -1 )
		{
			prevColor = Sys_GetCurrColor( sio );
			Sys_SetCurrColor( sio, ( unsigned char )color );
		}
		else if( *s == '&' )
		{
			color = ( int )prevColor;
			prevColor = Sys_GetCurrColor( sio );
			Sys_SetCurrColor( sio, ( unsigned char )color );
		}
		else if( *s == '^' )
		{
			Sys_UncoloredPuts( sio, "^", 1 );
		}

		s++;
	}
}
void Sys_Printf( sio_t sio, const char *format, ... )
{
	static char buf[ 32768 ];
	va_list args;

	va_start( args, format );
#if __STDC_WANT_SECURE_LIB__
	vsprintf_s( buf, sizeof( buf ), format, args );
#else
	vsnprintf( buf, sizeof( buf ), format, args );
	buf[ sizeof( buf ) - 1 ] = '\0';
#endif
	va_end( args );

	Sys_Puts( sio, buf );
}

void Sys_PrintStr( sio_t sio, unsigned char color, const char *str )
{
	unsigned char curColor;

	ASSERT( str != ( const char * )0 );

	curColor = Sys_GetCurrColor( sio );
	Sys_SetCurrColor( sio, color );
	Sys_UncoloredPuts( sio, str, 0 );
	Sys_SetCurrColor( sio, curColor );
}
void Sys_PrintUint( sio_t sio, unsigned char color, unsigned int val )
{
	char buf[ 64 ];

#if __STDC_WANT_SECURE_LIB__
	sprintf_s( buf, sizeof( buf ), "%u", val );
#else
	snprintf( buf, sizeof( buf ), "%u", val );
	buf[ sizeof( buf ) - 1 ] = '\0';
#endif

	Sys_PrintStr( sio, color, buf );
}
void Sys_PrintInt( sio_t sio, unsigned char color, int val )
{
	char buf[ 64 ];

#if __STDC_WANT_SECURE_LIB__
	sprintf_s( buf, sizeof( buf ), "%i", val );
#else
	snprintf( buf, sizeof( buf ), "%i", val );
	buf[ sizeof( buf ) - 1 ] = '\0';
#endif

	Sys_PrintStr( sio, color, buf );
}

/*
 *	========================================================================
 *	ERROR HANDLING CODE
 *	========================================================================
 *	Display errors in an appropriate manner.
 */

/* display a simple Log_Error message */
void Log_ErrorMsg( const char *message )
{
	unsigned char curColor;

	curColor = Sys_GetCurrColor( SIO_ERR );
	Sys_Printf( SIO_ERR, S_COLOR_RED "ERROR" S_COLOR_RESTORE ": %s", message );
	Sys_SetCurrColor( SIO_ERR, curColor );
	if( errno )
	{
		Sys_Printf( SIO_ERR, ": %s [ " S_COLOR_MAGENTA "%d"
			S_COLOR_RESTORE " ]", strerror( errno ), errno );
	}
	Sys_UncoloredPuts( SIO_ERR, "\n", 1 );

	Dbg_BackTrace();
}

/* exit with an Log_Error message; if errno is set, display its Log_Error */
NORETURN void Log_FatalError( const char *message )
{
	Log_ErrorMsg( message );
	exit( EXIT_FAILURE );
}

/* provide a general purpose Log_Error, without terminating */
void Log_Error( const char *file, unsigned int line, const char *func,
const char *message )
{
	if( file )
	{
		Sys_PrintStr( SIO_ERR, COLOR_WHITE, file );
		Sys_UncoloredPuts( SIO_ERR, ":", 1 );
		if( line )
		{
			Sys_PrintUint( SIO_ERR, COLOR_BROWN, line );
			Sys_UncoloredPuts( SIO_ERR, ":", 1 );
		}
		Sys_UncoloredPuts( SIO_ERR, " ", 1 );
	}

	Sys_Puts( SIO_ERR, S_COLOR_RED "ERROR" S_COLOR_RESTORE ": " );

	if( func )
	{
		unsigned char curColor;

		curColor = Sys_GetCurrColor( SIO_ERR );
		Sys_Puts( SIO_ERR, "in " S_COLOR_GREEN );
		Sys_UncoloredPuts( SIO_ERR, func, 0 );
		Sys_SetCurrColor( SIO_ERR, curColor );
		Sys_UncoloredPuts( SIO_ERR, ": ", 2 );
	}

	if( message )
	{
		Sys_UncoloredPuts( SIO_ERR, message, 0 );
	}

	if( ( errno&&message ) || !message )
	{
		if( message )
		{
			Sys_UncoloredPuts( SIO_ERR, ": ", 2 );
		}
		Sys_UncoloredPuts( SIO_ERR, strerror( errno ), 0 );
		Sys_Printf( SIO_ERR, "[ " S_COLOR_MAGENTA "%d" S_COLOR_RESTORE " ]",
			errno );
	}

	Sys_UncoloredPuts( SIO_ERR, "\n", 1 );
	Dbg_BackTrace();
}

/* provide an Log_Error for a failed ASSERT */
NORETURN void Log_ErrorAssert( const char *file, unsigned int line,
const char *func, const char *message )
{
	Log_Error( file, line, func, message );
	exit( EXIT_FAILURE );
}

/*
 *	========================================================================
 *	UTILITY CODE
 *	========================================================================
 *	A set of utility functions for making the rest of this easier.
 */

/*
 == == == == == == ==  == 
Com_Memory

manage memory; never worry about errors
 == == == == == == ==  == 
*/
#if _DEBUG
# define Com_Memory( p,n ) Com_MemoryDbg( __FILE__,__LINE__,__func__,p, n )
#else
# define Com_Memory( p,n ) Com_MemoryRel( p, n )
#endif
void *Com_MemoryDbg( const char *file, int line, const char *func, void *p,
size_t n )
{
	errno = 0;

#if LOG_MEMORY_ALLOCS
	Dbg_Outf( "MEMORY: %s( %i ) in %s: %p, %u;",
		file, line, func, p, ( unsigned int )n );
#else
	( void )file;
	( void )line;
	( void )func;
#endif

	if( !p&&n )
	{
		if( !( p = malloc( n ) ) )
		{
			Log_FatalError( "failed to allocate Com_Memory" );
		}
	}
	else if( p&&n )
	{
		void *q;

		if( !( q = realloc( p, n ) ) )
		{
			free( p );
			Log_FatalError( "failed to reallocate Com_Memory" );
		}

		p = q;
	}
	else
	{
		if( p&&!n )
		{
			free( p );
		}

		p = ( void * )0;
	}

#if LOG_MEMORY_ALLOCS
	Dbg_Outf( " //RET %p\n", p );
#endif
	return p;
}
void *Com_MemoryRel( void *p, size_t n )
{
	errno = 0;

	if( !p&&n ) {
#if LOG_MEMORY_ALLOCS
		printf( "allocating %u bytes... ", ( unsigned int )n );
		fflush( stdout );
#endif
		if( !( p = malloc( n ) ) )
		{
			Log_FatalError( "failed to allocate Com_Memory" );
		}
	}
	else if( p&&n )
	{
		void *q;

#if LOG_MEMORY_ALLOCS
		printf( "reallocating %p to %u bytes... ", p, ( unsigned int )n );
		fflush( stdout );
#endif
		if( !( q = realloc( p, n ) ) )
		{
			free( p );
			Log_FatalError( "failed to reallocate Com_Memory" );
		}

		p = q;
	}
	else {
#if LOG_MEMORY_ALLOCS
		printf( "deallocating %p... ", p );
		fflush( stdout );
#endif
		if( p&&!n )
		{
			free( p );
		}

		p = ( void * )0;
	}

#if LOG_MEMORY_ALLOCS
	printf( "done:%p\n", p );
	fflush( stdout );
#endif
	return p;
}

/* provide printf-style formating on the fly */
const char *Com_Va( const char *format, ... ) {
#define MINIMUM_SIZE 8192
	static char buf[ 65536 ];
	static size_t index = 0;
	va_list args;
	size_t n;
	char *p;

	p = &buf[ index ];

	va_start( args, format );
#if _STDC_WANT_SECURE_LIB_
	n = vsprintf_s( &buf[ index ], sizeof( buf ) - index, format, args );
#else
	n = vsnprintf( &buf[ index ], sizeof( buf ) - index, format, args );
	buf[ sizeof( buf ) - 1 ] = '\0';
#endif
	va_end( args );

	index += n;
	if( index + MINIMUM_SIZE > sizeof( buf ) )
	{
		index = 0;
	}

	return p;
#undef MINIMUM_SIZE
}

/* secure strlen */
size_t Com_Strlen( const char *src )
{
	ASSERT( src != ( const char * )0 );
	return strlen( src );
}

/* secure strcat */
size_t Com_Strcat( char *buf, size_t bufn, const char *src )
{
	size_t index, len;

	index = Com_Strlen( buf );
	len = Com_Strlen( src );

	if( index+len >= bufn )
	{
		Log_FatalError( "detected overflow" );
	}

	memcpy( ( void * )&buf[ index ], ( const void * )src, len+1 );
	return index+len;
}

/* secure strncat */
size_t Com_Strncat( char *buf, size_t bufn, const char *src, size_t srcn )
{
	size_t index, len;

	index = Com_Strlen( buf );
	len = srcn ? srcn : Com_Strlen( src );

	if( index+len >= bufn )
	{
		Log_FatalError( "detected overflow" );
	}

	memcpy( ( void * )&buf[ index ], ( const void * )src, len );
	buf[ index + len ] = '\0';

	return index+len;
}

/* secure strcpy */
void Com_Strcpy( char *dst, size_t dstn, const char *src )
{
	size_t i;

	for( i=0; src[ i ]; i++ )
	{
		dst[ i ] = src[ i ];
		if( i == dstn-1 )
		{
			Log_FatalError( "detected overflow" );
		}
	}

	dst[ i ] = 0;
}

/* secure strncpy */
void Com_Strncpy( char *buf, size_t bufn, const char *src, size_t srcn )
{
	if( srcn >= bufn )
	{
		Log_FatalError( "detected overflow" );
	}

	strncpy( buf, src, srcn );
	buf[ srcn ] = 0;
}

/* secure strdup; uses Com_Memory() */
char *Com_Strdup( const char *cstr )
{
	size_t n;
	char *p;

	ASSERT( cstr != ( const char * )0 );

	n = Com_Strlen( cstr ) + 1;

	p = ( char * )Com_Memory( ( void * )0, n );
	memcpy( ( void * )p, ( const void * )cstr, n );

	return p;
}

/* strdup() in-place alternative */
char *Com_Dup( char *dst, const char *src )
{
	size_t n;

	if( src )
	{
		n = Com_Strlen( src ) + 1;

		dst = ( char * )Com_Memory( ( void * )dst, n );
		memcpy( ( void * )dst, ( const void * )src, n );
	}
	else
	{
		dst = ( char * )Com_Memory( ( void * )dst, 0 );
	}

	return dst;
}
char *Com_DupN( char *dst, const char *src, size_t numchars )
{
	if( !numchars )
	{
		numchars = Com_Strlen( src );
	}

	if( src )
	{
		dst = ( char * )Com_Memory( ( void * )dst, numchars + 1 );
		memcpy( ( void * )dst, ( const void * )src, numchars );
		dst[ numchars ] = '\0';
	}
	else
	{
		dst = ( char * )Com_Memory( ( void * )dst, 0 );
	}

	return dst;
}

/* dynamic version of strcat() */
char *Com_Append( char *dst, const char *src )
{
	size_t l, n;

	if( !src )
	{
		return dst;
	}

	l = dst ? Com_Strlen( dst ) : 0;
	n = Com_Strlen( src ) + 1;

	dst = ( char * )Com_Memory( ( void * )dst, l + n );
	memcpy( ( void * )&dst[ l ], ( const void * )src, n );

	return dst;
}

/* extract the directory part of a filename */
const char *Com_ExtractDir( char *buf, size_t n, const char *filename )
{
	const char *p;

	if( ( p = strrchr( filename, '/' ) ) != ( const char * )0 )
	{
		Com_Strncpy( buf, n, filename, ( p-filename ) + 1 );
		return &p[ 1 ];
	}

	*buf = 0;
	return filename;
}

/* copy the source string into the destination string, overwriting the
   extension ( if present, otherwise appending ) with 'ext' */
void Com_SubstExt( char *dst, size_t dstn, const char *src, const char *ext )
{
	const char *p;

	ASSERT( dst != ( char * )0 );
	ASSERT( dstn > 1 );
	ASSERT( src != ( const char * )0 );

	p = strrchr( src, '.' );
	if( !p )
	{
		p = strchr( src, 0 );
	}

	ASSERT( p != ( const char * )0 );

	Com_Strncpy( dst, dstn, src, p - src );
	Com_Strcpy( &dst[ p - src ], dstn - ( size_t )( p - src ), ext );
}

/* check if we're running in verbose mode */
int Com_IsVerbose()
{
#if _DEBUG
	return 1;
#else
	return 0;
#endif
}

/* run a command in the Com_Shell */
int Com_Shell( const char *format, ... )
{
	static char cmd[ 16384 ];
	va_list args;

	va_start( args, format );
#if __STDC_WANT_SECURE_LIB__
	vsprintf_s( cmd, sizeof( cmd ), format, args );
#else
	vsnprintf( cmd, sizeof( cmd ), format, args );
	cmd[ sizeof( cmd )-1 ] = 0;
#endif
	va_end( args );

#ifdef _WIN32
	{
		char *p, *e;

		e = strchr( cmd, ' ' );
		if( !e )
		{
			e = strchr( cmd, '\0' );
		}

		for( p=&cmd[ 0 ]; p && p < e; p = strchr( p, '/' ) )
		{
			if( *p == '/' )
			{
				*p = '\\';
			}
		}
	}
#endif

	if( Com_IsVerbose() )
	{
		Sys_PrintStr( SIO_ERR, COLOR_LIGHT_CYAN, "> " );
		Sys_PrintStr( SIO_ERR, COLOR_CYAN, cmd );
		Sys_UncoloredPuts( SIO_ERR, "\n", 1 );
	}

	fflush( g_siof[ SIO_ERR ] );

	return system( cmd );
}

/* determine whether a given relative path is part of an absolute path */
int Com_MatchPath( const char *rpath, const char *apath )
{
	size_t rl, al;

	ASSERT( rpath != ( const char * )0 );
	ASSERT( apath != ( const char * )0 );

	rl = Com_Strlen( rpath );
	al = Com_Strlen( apath );

	if( rl > al )
	{
		return 0;
	}

#if _WIN32
	if( _stricmp( &apath[ al - rl ], rpath ) != 0 )
	{
		return 0;
	}
#else
	if( strcmp( &apath[ al - rl ], rpath ) != 0 )
	{
		return 0;
	}
#endif

	if( al - rl > 0 )
	{
		if( apath[ al -rl - 1 ] != '/' )
		{
			return 0;
		}
	}

	return 1;
}

/* retrieve the current date as an integral date -- YYYYMMDD, e.g., 20120223 */
int Com_GetIntDate()
{
	struct tm *p;
	time_t t;

	t = time( 0 );
	p = localtime( &t );

	return ( p->tm_year + 1900 )*10000 + ( p->tm_mon + 1 )*100 + p->tm_mday;
}

/* find the end of an argument within a string */
const char *Com_FindArgEnd( const char *arg )
{
	const char *p;

	if( *arg != '\"' )
	{
		p = strchr( arg, ' ' );
		if( !p )
		{
			p = strchr( arg, '\0' );
		}

		return p;
	}

	for( p = arg; *p != '\0'; ++p )
	{
		if( *p == '\\' )
		{
			p++;
			continue;
		}

		if( *p == '\"' )
		{
			return p + 1;
		}
	}

	return p;
}

/* determine whether one argument in a string is equal to another */
int Com_MatchArg( const char *a, const char *b )
{
	const char *x, *y;

	x = Com_FindArgEnd( a );
	y = Com_FindArgEnd( b );

	if( x - a != y - b )
	{
		return 0;
	}

	/*
	 * TODO: compare content of each argument, not the raw input
	 */

	return ( int )( strncmp( a, b, x - a ) == 0 );
}

/* skip to next argument within a string */
const char *Com_SkipArg( const char *arg )
{
	const char *p;

	p = Com_FindArgEnd( arg );
	while( *p <= ' ' && *p != '\0' )
	{
		++p;
	}

	if( *p == '\0' )
	{
		return NULL;
	}

	return p;
}

/* puts the lotion on its skin; else it gets the hose again */
void Com_StripArgs( char *dst, size_t n, const char *src )
{
	const char *p, *q, *next;

	Com_Strcpy( dst, n, "" );

	for( p = src; p != NULL; p = next )
	{
		next = Com_SkipArg( p );

		for( q = src; q < p; q = Com_SkipArg( q ) )
		{
			if( Com_MatchArg( q, p ) )
			{
				break;
			}
		}

		if( q < p )
		{
			continue;
		}

		q = next != NULL ? next : strchr( p, '\0' );
		Com_Strncat( dst, n, p, q - p );
	}
}

/* determine whether two characters of a path match */
int Com_MatchPathChar( char a, char b ) {
#if _WIN32
	if( a >= 'A' && a <= 'Z' )
	{
		a = a - 'A' + 'a';
	}
	if( b >= 'A' && b <= 'Z' )
	{
		b = b - 'A' + 'a';
	}
	if( a == '\\' )
	{
		a = '/';
	}
	if( b == '\\' )
	{
		b = '/';
	}
#endif

	return a == b ? 1 : 0;
}

/* retrieve a relative path */
int Com_RelPath( char *dst, size_t dstn, const char *curpath,
const char *abspath )
{
	size_t i;

	ASSERT( dst != ( char * )0 );
	ASSERT( dstn > 1 );
	ASSERT( curpath != ( const char * )0 );
	ASSERT( abspath != ( const char * )0 );

	i = 0;
	while( Com_MatchPathChar( curpath[ i ], abspath[ i ] ) &&
	curpath[ i ] != '\0' )
	{
		i++;
	}

	if( curpath[ i ] == '\0' && abspath[ i ] == '\0' )
	{
		Com_Strcpy( dst, dstn, "" );
	}
	else
	{
		Com_Strcpy( dst, dstn, &abspath[ i ] );
	}

	return 1;
}

/* retrieve a relative path based on the current working directory */
int Com_RelPathCWD( char *dst, size_t dstn, const char *abspath )
{
	char cwd[ PATH_MAX ];

	if( !FS_GetCWD( cwd, sizeof( cwd ) ) )
	{
		return 0;
	}

	return Com_RelPath( dst, dstn, cwd, abspath );
}

/*
 *	========================================================================
 *	DYNAMIC STRING ARRAY SYSTEM
 *	========================================================================
 *	Manages a dynamic array of strings. Functions similarly to the C++ STL's
 *	std::vector < std::string> class.
 */

struct strList_s
{
	size_t capacity;
	size_t size;
	char **data;

	struct strList_s *prev, *next;
};
struct strList_s *g_arr_head = ( struct strList_s * )0;
struct strList_s *g_arr_tail = ( struct strList_s * )0;

/* create a new ( empty ) array */
strList_t SL_New()
{
	strList_t arr;

	arr = ( strList_t )Com_Memory( ( void * )0, sizeof( *arr ) );

	arr->capacity = 0;
	arr->size = 0;
	arr->data = ( char ** )0;

	arr->next = ( struct strList_s * )0;
	if( ( arr->prev = g_arr_tail ) != ( struct strList_s * )0 )
	{
		g_arr_tail->next = arr;
	}
	else
	{
		g_arr_head = arr;
	}
	g_arr_tail = arr;

	return arr;
}

/* retrieve the current amount of Com_Memory allocated for the array */
size_t SL_Capacity( strList_t arr )
{
	ASSERT( arr != ( strList_t )0 );

	return arr->capacity;
}

/* retrieve how many elements are in the array */
size_t SL_Size( strList_t arr )
{
	if( !arr )
	{
		return 0;
	}

	return arr->size;
}

/* retrieve the data of the array */
char **SL_Data( strList_t arr )
{
	ASSERT( arr != ( strList_t )0 );

	return arr->data;
}

/* retrieve a single element of the array */
char *SL_Get( strList_t arr, size_t i )
{
	ASSERT( arr != ( strList_t )0 );
	ASSERT( i < arr->size );

	return arr->data[ i ];
}

/* set a single element of the array */
void SL_Set( strList_t arr, size_t i, const char *cstr )
{
	ASSERT( arr != ( strList_t )0 );
	ASSERT( i < arr->size );

	if( !cstr )
	{
		arr->data[ i ] = ( char * )Com_Memory( ( void * )arr->data[ i ], 0 );
	}
	else
	{
		arr->data[ i ] = Com_Strdup( cstr );
	}
}

/* deallocate the internal Com_Memory used by the array */
void SL_Clear( strList_t arr )
{
	size_t i;

	ASSERT( arr != ( strList_t )0 );

	for( i=0; i < arr->size; i++ )
	{
		arr->data[ i ] = ( char * )Com_Memory( ( void * )arr->data[ i ], 0 );
	}

	arr->data = ( char ** )Com_Memory( ( void * )arr->data, 0 );
	arr->capacity = 0;
	arr->size = 0;
}

/* delete an array */
void SL_Delete( strList_t arr )
{
	if( !arr )
	{
		return;
	}

	SL_Clear( arr );

	if( arr->prev )
	{
		arr->prev->next = arr->next;
	}
	if( arr->next )
	{
		arr->next->prev = arr->prev;
	}

	if( g_arr_head == arr )
	{
		g_arr_head = arr->next;
	}
	if( g_arr_tail == arr )
	{
		g_arr_tail = arr->prev;
	}

	Com_Memory( ( void * )arr, 0 );
}

/* delete all arrays */
void SL_DeleteAll()
{
	while( g_arr_head )
	{
		SL_Delete( g_arr_head );
	}
}

/* set the new size of an array */
void SL_Resize( strList_t arr, size_t n )
{
	size_t i;

	ASSERT( arr != ( strList_t )0 );

	if( n > arr->capacity )
	{
		i = arr->capacity;
		arr->capacity += 4096/sizeof( void * );
		/*arr->capacity += 32;*/

		arr->data = ( char ** )Com_Memory( ( void * )arr->data,
			arr->capacity*sizeof( char * ) );

		memset( ( void * )&arr->data[ i ], 0,
			( arr->capacity - i )*sizeof( char * ) );
	}

	arr->size = n;
}

/* add an element to the array, resizing if necessary */
void SL_PushBack( strList_t arr, const char *cstr )
{
	size_t i;

	ASSERT( arr != ( strList_t )0 );

	i = SL_Size( arr );
	SL_Resize( arr, i+1 );
	SL_Set( arr, i, cstr );
}

/* display the contents of an array */
void SL_Print( strList_t arr )
{
	size_t i, n;

	ASSERT( arr != ( strList_t )0 );

	n = SL_Size( arr );
	for( i=0; i < n; i++ ) {
#if _WIN32
		printf( "%2u. \"%s\"\n", ( unsigned int )i, SL_Get( arr, i ) );
#else
		/* ISO C90 does not support the 'z' modifier; ignore... */
		printf( "%2zu. \"%s\"\n", i, SL_Get( arr, i ) );
#endif
	}

	printf( "\n" );
}
void SL_DbgPrint( strList_t arr )
{
	size_t i, n;

	ASSERT( arr != ( strList_t )0 );

	n = SL_Size( arr );
	for( i=0; i < n; i++ )
	{
#if _WIN32
		Dbg_Outf( "%2u. \"%s\"\n", ( unsigned int )i, SL_Get( arr, i ) );
#else
		/* ISO C90 does not support the 'z' modifier; ignore... */
		Dbg_Outf( "%2zu. \"%s\"\n", i, SL_Get( arr, i ) );
#endif
	}

	Dbg_Outf( "\n" );
}

/* alphabetically sort an array */
static int _SL_Cmp( const void *a, const void *b )
{
	return strcmp( *( char *const * )a, *( char *const * )b );
}

void SL_Sort( strList_t arr )
{
	if( arr->size < 2 )
	{
		return;
	}

	qsort( ( void * )arr->data, arr->size, sizeof( char * ), _SL_Cmp );
}

void SL_OrderedSort( strList_t arr, size_t *const buffer, size_t maxBuffer )
{
	size_t i, j;
	size_t n;

	ASSERT( buffer != NULL );
	ASSERT( maxBuffer >= arr->size );

	n = arr->size < maxBuffer ? arr->size : maxBuffer;

	for( i=0; i < n; i++ )
	{
		buffer[ i ] = ( size_t )arr->data[ i ];
	}

	SL_Sort( arr );

	for( i=0; i < n; i++ )
	{
		for( j=0; j < n; j++ )
		{
			if( buffer[ i ] == ( size_t )arr->data[ j ] )
			{
				buffer[ i ] = j;
				break;
			}
		}

		ASSERT( j < n ); /* did exit from loop? */
	}
}
void SL_IndexedSort( strList_t arr, const size_t *const buffer,
size_t bufferLen ) {
#if 1
	strList_t backup;
	size_t i, n;

	/*
	 *	XXX: This is ugly.
	 *	FIXME: Ugly.
	 *	XXX: You can't fix ugly.
	 *	TODO: Buy a makeup kit for the beast.
	 *
	 *	In all seriousness, an in-place implementation should be written. This
	 *	works for now, though.
	 */

	ASSERT( buffer != NULL );
	ASSERT( bufferLen > 0 );

	backup = SL_New();
	n = bufferLen < arr->size ? bufferLen : arr->size;
	for( i=0; i < n; i++ )
	{
		SL_PushBack( backup, SL_Get( arr, i ) );
	}

	for( i=0; i < n; i++ )
	{
		SL_Set( arr, buffer[ i ], SL_Get( backup, i ) );
	}

	SL_Delete( backup );
#else
	char *tmp;
	size_t i;

	/*
	 *	FIXME: This partially works, but produces bad results.
	 */

	for( i=0; i < bufferLen; i++ )
	{
		if( i > buffer[ i ] )
			continue;

		tmp = arr->data[ buffer[ i ] ];
		arr->data[ buffer[ i ] ] = arr->data[ i ];
		arr->data[ i ] = tmp;
	}
#endif
}
void SL_PrintOrderedBuffer( const size_t *const buffer, size_t bufferLen )
{
	size_t i;

	printf( "array buffer %p ( %u element%s ):\n", ( void * )buffer,
		( unsigned int )bufferLen, bufferLen == 1?"":"s" );
	for( i=0; i < bufferLen; i++ )
	{
		printf( "  %u\n", ( unsigned int )buffer[ i ] );
	}
	printf( "\n" );
}

/* remove Com_Dup entries from an array */
void SL_Unique( strList_t arr )
{
	const char *a, *b;
	size_t i, j, n;

	n = SL_Size( arr );
	for( i=0; i < n; i++ )
	{
		if( !( a = SL_Get( arr, i ) ) )
			continue;

		for( j = i+1; j < n; j++ )
		{
			if( !( b = SL_Get( arr, j ) ) )
				continue;

			if( !strcmp( a, b ) )
				SL_Set( arr, j, ( const char * )0 );
		}
	}

	arr->size = 0;
	for( i=0; i < n; i++ )
	{
		if( arr->data[ i ] != ( char * )0 )
		{
			arr->size++;
			continue;
		}

		for( j = i; j+1 < n; j++ )
		{
			arr->data[ j ] = arr->data[ j+1 ];
		}
	}
}

/*
 *	========================================================================
 *	FILE SYSTEM MANAGEMENT CODE
 *	========================================================================
 *	This code deals with various file system related subjects. This includes
 *	making directories and finding where the executable is, etc.
 */

strList_t g_fs_dirstack = ( strList_t )0;

/* initialize file system */
void FS_Init()
{
	atexit( FS_Fini );
}

/* deinitialize file system */
void FS_Fini()
{
	SL_Delete( g_fs_dirstack );
	g_fs_dirstack = ( strList_t )0;
}

/* retrieve the current directory */
char *FS_GetCWD( char *cwd, size_t n )
{
	char *p;

	ASSERT( cwd != ( char * )0 );
	ASSERT( n > 1 );

#if _MSC_VER
	if( !( p = _getcwd( cwd, n ) ) )
	{
		Log_FatalError( "getcwd() failed" );
	}
#else
	if( !( p = getcwd( cwd, n ) ) )
	{
		Log_FatalError( "getcwd() failed" );
	}
#endif

#if _WIN32
	while( ( p = strchr( p,'\\' ) ) != ( char * )0 )
	{
		*p = '/';
	}
#endif

	p = strrchr( cwd, '/' );
	if( ( p && *( p + 1 ) != '\0' ) || !p )
	{
		Com_Strcat( cwd, n, "/" );
	}

	return cwd;
}

/* enter a directory ( uses directory stack ) */
int FS_Enter( const char *path )
{
	char cwd[ PATH_MAX ];

	FS_GetCWD( cwd, sizeof( cwd ) );

	if( !g_fs_dirstack )
	{
		g_fs_dirstack = SL_New();
	}

	SL_PushBack( g_fs_dirstack, cwd );

	if( chdir( path ) == -1 )
	{
		Log_ErrorMsg( Com_Va( "chdir( ^F\"%s\"^& ) failed", path ) );
		if( chdir( cwd ) == -1 )
		{
			Log_FatalError( "failed to restore current directory" );
		}

		return 0;
	}

	return 1;
}

/* exit the current directory ( uses directory stack ) */
void FS_Leave()
{
	size_t i;

	ASSERT( g_fs_dirstack != ( strList_t )0 );
	ASSERT( SL_Size( g_fs_dirstack ) > 0 );

	i = SL_Size( g_fs_dirstack )-1;

	if( chdir( SL_Get( g_fs_dirstack, i ) ) == -1 )
	{
		Log_FatalError( Com_Va( "chdir( \"%s\" ) failed",
			SL_Get( g_fs_dirstack, i ) ) );
	}

	SL_Set( g_fs_dirstack, i, ( const char * )0 );
	SL_Resize( g_fs_dirstack, i );
}

/* determine whether the path specified is a file. */
int FS_IsFile( const char *path )
{
	struct stat s;

	if( stat( path, &s ) == -1 )
	{
		return 0;
	}

	if( ~s.st_mode & S_IFREG )
	{
		errno = s.st_mode & S_IFDIR ? EISDIR : EBADF;
		return 0;
	}

	return 1;
}

/* determine whether the path specified is a directory. */
int FS_IsDir( const char *path )
{
	static char temp[ PATH_MAX ];
	struct stat s;
	const char *p;

	p = strrchr( path, '/' );
	if( p && *( p + 1 ) == '\0' )
	{
		Com_Strncpy( temp, sizeof( temp ), path, p - path );
		temp[ p - path ] = '\0';
		p = &temp[ 0 ];
	}
	else
	{
		p = path;
	}

	if( stat( p, &s ) == -1 )
	{
		return 0;
	}

	if( ~s.st_mode & S_IFDIR )
	{
		errno = ENOTDIR;
		return 0;
	}

	return 1;
}

/* create a series of directories ( e.g., a/b/c/d/... ) */
void FS_MkDirs( const char *dirs )
{
	/*
	 *	! This is old code !
	 *	Just ignore bad practices, mmkay?
	 */
	const char *p;
	char buf[ PATH_MAX ], *path;

	/* ignore the root directory */
	if( dirs[ 0 ] == '/' )
	{
		buf[ 0 ] = dirs[ 0 ];
		path = &buf[ 1 ];
		p = &dirs[ 1 ];
	}
	else if( dirs[ 1 ] == ':' && dirs[ 2 ] == '/' )
	{
		buf[ 0 ] = dirs[ 0 ];
		buf[ 1 ] = dirs[ 1 ];
		buf[ 2 ] = dirs[ 2 ];
		path = &buf[ 3 ];
		p = &dirs[ 3 ];
	}
	else
	{
		path = &buf[ 0 ];
		p = &dirs[ 0 ];
	}

	/* make each directory, one by one */
	while( 1 )
	{
		if( *p == '/' || *p == 0 )
		{
			*path = 0;

			errno = 0;
#ifdef _WIN32
			mkdir( buf );
#else
			mkdir( buf, 0740 );
#endif
			if( errno && errno != EEXIST )
			{
				Log_FatalError( Com_Va( "couldn't create directory \"%s\"",
					buf ) );
			}

			if( !( *path++ = *p++ ) )
			{
				return;
			}
			else if( *p == '\0' )
			{ /* handle a '/' ending */
				return;
			}
		}
		else
		{
			*path++ = *p++;
		}

		if( path == &buf[ sizeof( buf )-1 ] )
			Log_FatalError( "path is too long" );
	}
}

/* find the real path to a file */
#ifdef _WIN32
char *FS_RealPath( const char *filename, char *resolvedname, size_t maxn )
{
	static char buf[ PATH_MAX ];
	size_t i;
	DWORD r;

	if( !( r = GetFullPathNameA( filename, sizeof( buf ), buf,
	( char ** )0 ) ) )
	{
		errno = ENOSYS;
		return ( char * )0;
	}
	else if( r >= ( DWORD )maxn )
	{
		errno = ERANGE;
		return ( char * )0;
	}

	for( i=0; i < sizeof( buf ); i++ )
	{
		if( !buf[ i ] )
		{
			break;
		}

		if( buf[ i ] == '\\' )
		{
			buf[ i ] = '/';
		}
	}

	if( buf[ 1 ] == ':' )
	{
		if( buf[ 0 ] >= 'A' && buf[ 0 ] <= 'Z' )
		{
			buf[ 0 ] = buf[ 0 ] - 'A' + 'a';
		}
	}

	strncpy( resolvedname, buf, maxn-1 );
	resolvedname[ maxn-1 ] = 0;

	return resolvedname;
}
#else /*#elif __linux__||__linux||linux*/
char *FS_RealPath( const char *filename, char *resolvedname, size_t maxn )
{
	static char buf[ PATH_MAX + 1 ];

	if( maxn > PATH_MAX )
	{
		if( !realpath( filename, resolvedname ) )
		{
			return ( char * )0;
		}

		resolvedname[ PATH_MAX ] = 0;
		return resolvedname;
	}

	if( !realpath( filename, buf ) )
	{
		return ( char * )0;
	}

	buf[ PATH_MAX ] = 0;
	strncpy( resolvedname, buf, maxn );

	if( FS_IsDir( resolvedname ) )
	{
		Com_Strcat( resolvedname, maxn, "/" );
	}

	resolvedname[ maxn-1 ] = 0;
	return resolvedname;
}
#endif

DIR *FS_OpenDir( const char *path )
{
	static char buf[ PATH_MAX ];
	const char *p, *usepath;

	Dbg_Outf( "FS_OpenDir( \"%s\" );\n", path );

	p = strrchr( path, '/' );
	if( p && *( p + 1 ) == '\0' )
	{
		Com_Strncpy( buf, sizeof( buf ), path, ( size_t )( p - path ) );
		usepath = buf;
	}
	else
	{
		usepath = path;
	}

	return opendir( usepath );
}
struct dirent *FS_ReadDir( DIR *d )
{
	struct dirent *dp;

	/* Okay... how is errno getting set? From what? */
	if( errno )
	{
		Log_Error( NULL, 0, "FS_ReadDir", NULL );
	}

	/* TEMP-FIX: buggy version of readdir() sets errno? */
	dp = readdir( d );
	if( dp )
	{
		errno = 0;
	}
	else if( errno == ENOTDIR )
	{
		errno = 0;
	}

	return dp;
}

/*
 *	========================================================================
 *	PROGRAM CODE
 *	========================================================================
 *	Main program
 */

/* ghost copy */
void App_GhostCopy( const char *srcdir, const char *dstdir )
{
	DIR *d;
	struct dirent *de;
	struct stat s;
	char targetdir[ PATH_MAX ];
	char *p, *q, *x;

	char *targetdirp;
	size_t targetdirn;

	/* target path */
	if( !FS_RealPath( dstdir, targetdir, sizeof( targetdir ) ) )
	{
		Log_Error( dstdir, 0, "FS_RealPath", "Could not resolve real path" );
		return;
	}

	p = strrchr( targetdir, '\\' );
	q = strrchr( targetdir, '/' );
	x = p > q ? p : q;
	if( x == NULL || *( x + 1 ) != '\0' )
	{
		Com_Strcat( targetdir, sizeof( targetdir ), "/" );
	}

	targetdirp = strchr( targetdir, '\0' );
	targetdirn = sizeof( targetdir ) - ( targetdirp - &targetdir[ 0 ] );

	if( !FS_Enter( srcdir ) )
	{
		Log_Error( srcdir, 0, "FS_Enter", NULL );
		return;
	}

	d = FS_OpenDir( "." );
	if( !d )
	{
		Log_Error( srcdir, 0, "FS_OpenDir", NULL );
		FS_Leave();
		return;
	}

	while( 1 )
	{
		de = FS_ReadDir( d );
		if( !de )
		{
			break;
		}

		if( strcmp( de->d_name, "." ) == 0 || strcmp( de->d_name, ".." ) == 0 )
		{
			continue;
		}

		if( stat( de->d_name, &s ) == -1 )
		{
			Log_Error( de->d_name, 0, "stat", NULL );
			continue;
			//break;
		}

		Com_Strcpy( targetdirp, targetdirn, de->d_name );

		if( s.st_mode & S_IFREG )
		{
			FILE *fp;

			fp = fopen( targetdir, "wb" );
			if( !fp )
			{
				Log_Error( targetdir, 0, NULL, "Failed to open for writing" );
			}
			else
			{
				fclose( fp );
			}
		}
		else
		{
			FS_MkDirs( targetdir );
			App_GhostCopy( de->d_name, targetdir );
		}

		*targetdirp = '\0';
	}

	closedir( d );

	FS_Leave();
}

/* entry point */
int main( int argc, char **argv )
{
	/* core initialization */
	Sys_InitColoredOutput();
	FS_Init();
	errno = 0;
	
	/* expect source/destination */
	if( argc != 3 )
	{
		printf( "ghostcopy [SourceDir] [TargetDir]\n\n" );
		printf( "Copies the directory structure of SourceDir to TargetDir\n" );
		printf( "Creates blank files in TargetDir with the same names as "
			"the files in SourceDir\n" );
		return EXIT_FAILURE;
	}
	
	/* run the app */
	App_GhostCopy( argv[ 1 ], argv[ 2 ] );

	/* done */
	return EXIT_SUCCESS;
}
