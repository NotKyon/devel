#ifndef GB_BASE_EXCEPT_H
#define GB_BASE_EXCEPT_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Exception Handling */
typedef enum {
	kGBExcept_None,

	kGBExcept_RuntimeError,
	kGBExcept_AssertError,

	kGBExcept_Fault,
	kGBExcept_StackOverflow,

	kGBExcept_MemoryViolation,
	kGBExcept_DivideByZero,

	kGBExcept_UndefinedInstruction,
	kGBExcept_PrivilegedInstruction,
	kGBExcept_Breakpoint
} gb_exceptKind_t;
typedef enum {
	kGBExceptRes_Terminate,
	kGBExceptRes_RunNext,
	kGBExceptRes_Break
} gb_exceptResult_t;
typedef struct gb_accessViolationData_s {
	gb_bool_t writeAccess; /*GB_FALSE=read, GB_TRUE=write*/
	void *address;
} gb_accessViolationData_t;
typedef struct gb_exception_s {
	gb_exceptKind_t kind;

	const char *message;

	void *address;
	union {
		void *p;
		gb_accessViolationData_t *accessViolation;
	} data;
} gb_exception_t;

typedef gb_exceptResult_t(*gb_fnExcept_t)(const gb_exception_t *except);

const char *gbExceptionName(gb_exceptKind_t kind);
gb_exceptResult_t gbDefaultException_f(const gb_exception_t *except);

void gbPushExceptionHandler();
void gbPopExceptionHandler();

void gbSetExceptionHandler(gb_fnExcept_t fnExcept);
gb_fnExcept_t gbGetExceptionHandler();

gb_bool_t gbRaiseException(const gb_exception_t *except);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
