#ifndef GB_FRAMEWORK_DICTIONARY_H
#define GB_FRAMEWORK_DICTIONARY_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Dictionary */
#define GB_DICT_LOWER "abcdefghijklmnopqrstuvwxyz"
#define GB_DICT_UPPER "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define GB_DICT_DIGIT "0123456789"
#define GB_DICT_UNDER "_"
#define GB_DICT_IDENT GB_DICT_LOWER GB_DICT_UPPER GB_DICT_DIGIT GB_DICT_UNDER

typedef struct gb_entry_s {
	struct gb_entry_s *entries;
	void *p;
} gb_entry_t;
typedef struct gb_dictionary_s {
	int convmap[256];

	int numEntries;
	struct gb_entry_s *entries;
} gb_dictionary_t;

int gbInitDict(gb_dictionary_t *dict, const char *allowed);
void gbFiniDict(gb_dictionary_t *dict);

gb_entry_t *gbFindEntry(gb_dictionary_t *dict, const char *str);
gb_entry_t *gbLookupEntry(gb_dictionary_t *dict, const char *str);

gb_entry_t *gbNewEntry(gb_dictionary_t *dict, const char *str, void *p);
gb_entry_t *gbSetEntry(gb_dictionary_t *dict, const char *str, void *p);
void *gbGetEntry(gb_dictionary_t *dict, const char *str);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
