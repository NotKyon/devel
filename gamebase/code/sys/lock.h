#ifndef LOCK_H
#define LOCK_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "threadconf.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/*
 *	TODO: Implement these with atomic operations instead of OS calls.
 */

/* Mutual Exclusion */
GB_INLINE void InitLock(lock_t *M) {
#if __USE_WIN32_THREADS__
	InitializeCriticalSectionAndSpinCount(M, 4000);
#else
	static pthread_mutexattr_t defAttr;
	static gb_bool_t didInitAttr = GB_FALSE;

	if (GB_UNLIKELY(!didInitAttr)) {
		pthread_mutexattr_init(&defAttr);
		pthread_mutexattr_settype(&defAttr, PTHREAD_MUTEX_RECURSIVE);

		didInitAttr = GB_TRUE;
	}

	if (pthread_mutex_init(M, &defAttr) != 0) {
		gbError(__FILE__,__LINE__,__func__, "pthread_mutex_init() fail");
		exit(EXIT_FAILURE);
	}
#endif
}
GB_INLINE void FiniLock(lock_t *M) {
#if __USE_WIN32_THREADS__
	DeleteCriticalSection(M);
#else
	pthread_mutex_destroy(M);
#endif
	memset((void *)M, 0, sizeof(M));
}

GB_INLINE void Lock(lock_t *M) {
#if __USE_WIN32_THREADS__
	EnterCriticalSection(M);
	WaitForSingleObject(M, INFINITE);
#else
	pthread_mutex_lock(M);
#endif
}
GB_INLINE gb_bool_t TryLock(lock_t *M) {
#if __USE_WIN32_THREADS__
	return (gb_bool_t)(GB_TRUE & TryEnterCriticalSection(M));
#else
	if (!pthread_mutex_trylock(M))
		return GB_TRUE;

	return GB_FALSE;
#endif
}
GB_INLINE void Unlock(lock_t *M) {
#if __USE_WIN32_THREADS__
	LeaveCriticalSection(M);
#else
	pthread_mutex_unlock(M);
#endif
}

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
