#ifndef MK_COMPMGR_H
#define MK_COMPMGR_H

#include "mk_types.h"

int FindSourceLibs(xs_array_t dst, int sys, const char *obj, const char *dep);

int ShouldCompile(const char *obj);
int ShouldLink(const char *bin, int numbuilds);

const char *GetCompilerFlags(project_t proj, const char *obj, const char *src);

void GetProjectLibs(project_t proj, char *dst, size_t n);
const char *GetLinkFlags(project_t proj, const char *bin, xs_array_t objs);

void GetObjName(project_t proj, char *obj, size_t n, const char *src);
void GetBinName(project_t proj, char *bin, size_t n);

void SortProjects(project_t proj);
void RelinkDependents(project_t proj);

int BuildProject(project_t proj);
int BuildProjects();

#endif
