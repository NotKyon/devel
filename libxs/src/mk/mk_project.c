#include "mk_project.h"

#if _WIN32 && !__CYGWIN__
# define HOST_SYS kProjSys_MSWin
#elif __linux__||__linux||linux || __CYGWIN__
# define HOST_SYS kProjSys_Linux
#elif __APPLE__
# define HOST_SYS kProjSys_MacOS
#elif __unix__||__unix
# define HOST_SYS kProjSys_Linux /*add a kProjSys_Unix?*/
#else
# error "OS not recognized."
#endif

#if __amd64__||__amd64||__x86_64__||x86_64||_M_X64
# define HOST_ARCH kProjArch_X86_64
#elif __i386__||__i386||_M_IX86
# define HOST_ARCH kProjArch_X86
#elif __arm__||_ARM
# define HOST_ARCH kProjArch_ARM
#elif __powerpc__||__powerpc||__POWERPC__||__ppc__||_M_PPC||_ARCH_PPC
# define HOST_ARCH kProjArch_PowerPC
#elif __mips__||__mips||mips||__MIPS__
# define HOST_ARCH kProjArch_MIPS
#else
# error "Architecture not recognized."
#endif

const int g_host_sys = HOST_SYS;
const int g_host_arch = HOST_ARCH;

project_t g_proj_head = (project_t)0;
project_t g_proj_tail = (project_t)0;

project_t NewProject(project_t prnt) {
	project_t proj;
	size_t i, n;

	proj = (project_t)XsMemory((void *)0, sizeof(*proj));

	proj->name = (char *)0;
	proj->path = (char *)0;
	proj->outpath = (char *)0;

	proj->type = kProjType_Executable;
	proj->sys = g_host_sys;
	proj->arch = g_host_arch;

	proj->sources = XsNewArray();
	proj->specialdirs = XsNewArray();
	proj->libs = XsNewArray();
	proj->libdirs = XsNewArray();

	n = XsArraySize(g_libdirs);
	for(i=0; i<n; i++)
		XsArrayPush(proj->libdirs, XsArrayElement(g_libdirs, i));

	proj->linkerflags = (char *)0;

	proj->config = 0;

	proj->prnt = prnt;
	proj->p_head = prnt ? &prnt->head : &g_proj_head;
	proj->p_tail = prnt ? &prnt->tail : &g_proj_tail;
	proj->head = (project_t)0;
	proj->tail = (project_t)0;
	proj->next = (project_t)0;
	if ((proj->prev = *proj->p_tail) != (project_t)0)
		(*proj->p_tail)->next = proj;
	else
		*proj->p_head = proj;
	*proj->p_tail = proj;

	return proj;
}
void DeleteProject(project_t proj) {
	if (!proj)
		return;

	while(proj->head)
		DeleteProject(proj->head);

	proj->name = (char *)XsMemory((void *)proj->name, 0);
	proj->path = (char *)XsMemory((void *)proj->path, 0);
	proj->outpath = (char *)XsMemory((void *)proj->outpath, 0);

	XsDeleteArray(proj->sources);
	XsDeleteArray(proj->specialdirs);
	XsDeleteArray(proj->libs);

	proj->linkerflags = (char *)XsMemory((void *)proj->linkerflags, 0);

	if (proj->prev)
		proj->prev->next = proj->next;
	if (proj->next)
		proj->next->prev = proj->prev;

	assert(proj->p_head != (project_t *)0);
	assert(proj->p_tail != (project_t *)0);

	if (*proj->p_head==proj)
		*proj->p_head = proj->next;
	if (*proj->p_tail==proj)
		*proj->p_tail = proj->prev;

	XsMemory((void *)proj, 0);
}
void DeleteAllProjects() {
	while(g_proj_head)
		DeleteProject(g_proj_head);
}

project_t FirstRootProject() {
	return g_proj_head;
}
project_t LastRootProject() {
	return g_proj_tail;
}

project_t ProjectParent(project_t proj) {
	assert(proj != (project_t)0);

	return proj->prnt;
}
project_t ProjectBefore(project_t proj) {
	assert(proj != (project_t)0);

	return proj->prev;
}
project_t ProjectAfter(project_t proj) {
	assert(proj != (project_t)0);

	return proj->next;
}
project_t FirstProject(project_t prnt) {
	assert(prnt != (project_t)0);

	return proj->head;
}
project_t LastProject(project_t prnt) {
	assert(prnt != (project_t)0);

	return proj->tail;
}

void SetProjectName(project_t proj, const char *name) {
	const char *p;
	char tmp[1024];

	assert(proj != (project_t)0);

	tmp[0] = 0;
	p = (const char *)0;

	if (name) {
		if (!(p = strrchr(name, '/')))
			p = strrchr(name, '\\');

		if (p) {
			/* can't end in "/" */
			if (*(p + 1)==0)
				ErrorExit(va("Invalid project name \"%s\"", name));

			XsStrCpyN(tmp, sizeof(tmp), name, (p - name) + 1);
			p++;
		} else
			p = name;
	}

	assert(p != (const char *)0);

	proj->outpath = XsReduplicate(proj->outpath, tmp[0] ? tmp : 0);
	proj->name = XsReduplicate(proj->name, p);
}
void SetProjectPath(project_t proj, const char *path) {
	assert(proj != (project_t)0);

	proj->path = XsReduplicate(proj->path, path);
}
void SetProjectType(project_t proj, int type) {
	assert(proj != (project_t)0);

	if (!proj->outpath)
		proj->outpath = XsReduplicate(proj->outpath,
			type==kProjType_Library ? "lib/" : "bin/");

	proj->type = type;
}

const char *ProjectName(project_t proj) {
	assert(proj != (project_t)0);

	return proj->name;
}
const char *ProjectPath(project_t proj) {
	assert(proj != (project_t)0);

	return proj->path;
}
const char *ProjectOutPath(project_t proj) {
	assert(proj != (project_t)0);

	return proj->outpath;
}

int ProjectType(project_t proj) {
	assert(proj != (project_t)0);

	return proj->type;
}

void AddProjectLib(project_t proj, const char *libname) {
	assert(proj != (project_t)0);
	assert(libname != (const char *)0);

	XsArrayPush(proj->libs, libname);
}
size_t ProjectLibCount(project_t proj) {
	assert(proj != (project_t)0);

	return XsArraySize(proj->libs);
}
const char *ProjectLib(project_t proj, size_t i) {
	assert(proj != (project_t)0);
	assert(i < ProjectLibCount(proj));

	return XsArrayElement(proj->libs, i);
}

void AppendProjectLinkerFlag(project_t proj, const char *flags) {
	size_t l;

	assert(proj != (project_t)0);
	assert(flags != (const char *)0);

	l = proj->linkerflags ? proj->linkerflags[0]!=0 : 0;

	if (l)
		proj->linkerflags = XsAppend(proj->linkerflags, " ");

	proj->linkerflags = XsAppend(proj->linkerflags, flags);
}
const char *ProjectLinkerFlags(project_t proj) {
	assert(proj != (project_t)0);

	return proj->linkerflags ? proj->linkerflags : "";
}

void AddProjectSource(project_t proj, const char *src) {
	const char *p;

	assert(proj != (project_t)0);
	assert(src != (const char *)0);

	assert(proj->sources != (xs_array_t)0);

	if (isextcpp(getext(src)))
		proj->config |= kProjCfg_UsesCxx_Bit;

	XsArrayPush(proj->sources, src);
}
size_t ProjectSourceCount(project_t proj) {
	assert(proj != (project_t)0);
	assert(proj->sources != (xs_array_t)0);

	return XsArraySize(proj->sources);
}
const char *ProjectSource(project_t proj, size_t i) {
	assert(proj != (project_t)0);
	assert(proj->sources != (xs_array_t)0);

	return XsArrayElement(proj->sources, i);
}

void AddProjectSpecialDir(project_t proj, const char *dir) {
	assert(proj != (project_t)0);
	assert(dir != (const char *)0);
	assert(proj->specialdirs != (xs_array_t)0);

	XsArrayPush(proj->specialdirs, dir);
}
size_t ProjectSpecialDirCount(project_t proj) {
	assert(proj != (project_t)0);
	assert(proj->specialdirs != (xs_array_t)0);

	return XsArraySize(proj->specialdirs);
}
const char *ProjectSpecialDir(project_t proj, size_t i) {
	assert(proj != (project_t)0);
	assert(proj->specialdirs != (xs_array_t)0);
	assert(i < ProjectSpecialDirCount(proj));

	return XsArrayElement(proj->specialdirs, i);
}
int IsProjectTargetted(project_t proj) {
	size_t i, n;

	n = XsArraySize(g_targets);
	if (!n)
		return 1;

	for(i=0; i<n; i++) {
		if (!strcmp(projectname(proj), XsArrayElement(g_targets, i)))
			return 1;
	}

	return 0;
}

void PrintProjects(project_t proj, const char *margin) {
	size_t i, n;
	char marginbuf[256];
	char bin[256];

	assert(margin != (const char *)0);

	if (!proj) {
		for(proj=FirstRootProject(); proj; proj=ProjectAfter(proj))
			PrintProjects(proj, margin);

		return;
	}

	if (!IsProjectTargetted(proj)) {
		GetBinName(proj, bin, sizeof(bin));

		printf("%s%s; \"%s\"\n", margin, ProjectName(proj), bin);

		n = ProjectSourceCount(proj);
		for(i=0; i<n; i++)
			printf("%s * %s\n", margin, ProjectSource(proj, i));
	}

	XsStrCpy(marginbuf, sizeof(marginbuf), margin);
	XsStrCat(marginbuf, sizeof(marginbuf), "  ");
	for(proj=FirstProject(proj); proj; proj=ProjectAfter(proj))
		PrintProjects(proj, marginbuf);
}

void CalcProjectLibFlags(project_t proj) {
	const char *libname;
	size_t i, n;
	lib_t lib;

	n = ProjectLibCount(proj);
	for(i=0; i<n; i++) {
		libname = ProjectLib(proj, i);
		if (!libname)
			continue;

		lib = FindLib(libname);
		if (!lib) {
			ErrorMessage(va("Couldn't find library \"%s\"", libname));
			continue;
		}

		AppendProjectLinkerFlag(proj, LibFlags(lib, proj->sys));
	}
}
