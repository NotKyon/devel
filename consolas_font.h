#ifndef CONSOLAS_FONT_H
#define CONSOLAS_FONT_H

#include "math.h"

int InitConsoleFont();
void DeinitConsoleFont();
void BindConsoleFont();
void DrawConsoleText(const char *str, int x, int y, int numPalette,
	color_t *palette, vec2_t size);

#endif
