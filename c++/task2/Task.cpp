#include "../taskshare/versions/Task2.h"

#include <ctime>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>

//------------------------------------------------------------------------------
// TASK IMPLEMENTATIONS

#include "../taskshare/test/Alloc.cpp"

//------------------------------------------------------------------------------
// USER TEST

#if 1
# define NUM_TESTS 1024
#else
# define NUM_TESTS 4
#endif

#include "../taskshare/test/IsPrime.cpp"

task::queue_t gMainQueue;
int gNumbers[NUM_TESTS];

task::threadResult_t THREADPROCAPI WorkerThread(void *) {
	task::Streamer(gMainQueue);
	return (task::threadResult_t)0;
}

int main() {
	task::thread_t threads[32];
	size_t i, n;
#if _WIN32
	DWORD s, e;
#else
	clock_t s, e;
#endif

	n = task::GetCPUThreadCount();
	if (n > sizeof(threads)/sizeof(threads[0]))
		n = sizeof(threads)/sizeof(threads[0]);
	printf("%u cores available\n", (unsigned int)n);

	task::InitQueue(gMainQueue);

	// NOTE: threads before tasks if queue is not large enough
	for(i=0; i<NUM_TESTS; i++) {
		gNumbers[i] = 100000000 + i;
		task::Schedule(gMainQueue, IsPrime_f, &gNumbers[i]);
	}

	for(i=0; i<n; i++)
		threads[i] = task::NewThread(WorkerThread, (void *)0);

#if _WIN32
	s = GetTickCount();
#else
	s = clock();
#endif
	task::SyncQueue(gMainQueue);
#if _WIN32
	e = GetTickCount();
#else
	e = clock();
#endif

	printf("%.3f second(s)\n", double(e - s)/1000.0);

	task::FiniQueue(gMainQueue);

	for(i=0; i<n; i++) {
		task::JoinThread(threads[i]);
		task::KillThread(threads[i]);
	}

	return 0;
}
