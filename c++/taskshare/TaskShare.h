#ifndef TASKSHARE_TASKSHARE_H
#define TASKSHARE_TASKSHARE_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

#include "CPU.h"
#include "Lock.h"
#include "Task.h"
#include "Signal.h"
#include "Timing.h"
#include "Atomics.h"
#include "SysEvent.h"
#include "Semaphore.h"
#include "Threading.h"

#endif
