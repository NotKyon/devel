#ifndef XS_XS_H
#define XS_XS_H

/* not restricted to a library */
#include "basetypes.h"

/* xs-core */
#include "array.h"
#include "assert.h"
#include "memory.h"
#include "string.h"
#include "logging.h"

/* xs-buffer */
#include "buffer.h"
#include "ast.h"

/* xs-conf */
#include "config.h"

#endif
