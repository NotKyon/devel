#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

static const struct Tuple {
    double x, y;
} numbers[] = {
#if 0
    { 0.5000,  10.0 },
    { 0.6920,   8.0 },
    { 0.8000,   6.0 },
    { 0.8750,   4.0 },
    { 0.9375,   2.0 }
#else
    { 0.5000,  10.0 },
    { 0.5000,   8.0 }
#endif
};

#define ARRAY_SIZE( arr ) ( sizeof( arr )/sizeof( arr[0] ) )

double f() {
    size_t i;
    double sum, pcn;

    sum = 0.0;
    pcn = 1.0;
    for( i=0; i<ARRAY_SIZE( numbers ); ++i ) {
        pcn *= numbers[ i ].x;
        sum += numbers[ i ].y*pcn;
    }

    return sum/( 1.0 - pcn );
}

int main() {
    printf( "%g\n", f() );
    return EXIT_SUCCESS;
}

/*
(4:17:17 PM) nuclearglory@gmail.com: What's the formula for knowing $10 gain at 75% is better than $20 loss at 25%? My brain is off...
(4:17:32 PM) Aaron Miller: Thinking...
(4:17:41 PM) nuclearglory@gmail.com: Is that 10*.75 > -20*.25 = good?
(4:17:54 PM) Aaron Miller: $10*.75 - $20*.25 is the first thing that comes to mind.
(4:17:56 PM) nuclearglory@gmail.com: Taking te minus sign out I guess.
(4:18:14 PM) nuclearglory@gmail.com: I like how that was compiler optimized. xP
(4:18:27 PM) Aaron Miller: Compiler optimized?
(4:18:34 PM) Aaron Miller: Also, that evaluates to 2.5.
(4:18:49 PM) nuclearglory@gmail.com: > using internal subtraction and you went straight to a subtract xP
(4:18:51 PM) Aaron Miller: We need more numbers we can say "Yes, this works." or "No, this is bad," and test that against that.
(4:19:25 PM) Aaron Miller: Well, tbf, mine is technically better in systems where branching is bad.
(4:19:34 PM) nuclearglory@gmail.com: Hahaha
(4:19:57 PM) nuclearglory@gmail.com: Okay, next bit because my brain hurts.
(4:20:05 PM) nuclearglory@gmail.com: We had this:
(4:20:09 PM) Aaron Miller: "What's 5/2?" xD
(4:20:51 PM) nuclearglory@gmail.com: 0.5 $10
0.5 $10

That's $10 at 75%

So given...

0.5 $10
0.5 $8

This is [what?] at 75%?
(4:21:14 PM) nuclearglory@gmail.com: We know it's 75% but the magic number part of my mind isn't coming on.
(4:21:24 PM) nuclearglory@gmail.com: Somewhere between 8 and 10!
(4:21:27 PM) nuclearglory@gmail.com: But where? LOL!!!
(4:22:00 PM) nuclearglory@gmail.com: It's not 9 because 10 has a higher weight.
(4:22:12 PM) Aaron Miller: I'm going to make a guess and say $8.75.
(4:22:23 PM) nuclearglory@gmail.com: I need the formula.
(4:22:25 PM) nuclearglory@gmail.com: lol!
(4:22:30 PM) Aaron Miller: Does that sound right?
(4:22:43 PM) Aaron Miller: If it does: (10 - 8)*.5*.5
(4:23:12 PM) Aaron Miller: If not... what number are we looking for?
(4:23:21 PM) Aaron Miller: You said not 9 so...
(4:23:42 PM) Aaron Miller: Sorry, I did *.75*.5
(4:24:01 PM) Aaron Miller: If at *.5*.5 it's $8.50
(4:24:24 PM) nuclearglory@gmail.com: I THINK the number is... (10*.5+8*.25)/.75
(4:24:47 PM) Aaron Miller: .25 from *.5*.5, and .75 from 1 - .25 ?
(4:25:13 PM) nuclearglory@gmail.com: .75 from 1.0-(.5+.25)

(5:08:54 PM) nuclearglory@gmail.com: The gain on this one:
gain = (0.5*10+0.5*0.5*10)/.75
(5:09:17 PM) nuclearglory@gmail.com: That gives you the plain english of "10 gain 75% of the time"
(5:10:17 PM) nuclearglory@gmail.com: The loss is a running total of the dollar amounts, that much is pretty easy.
(5:10:27 PM) nuclearglory@gmail.com: Then just pump it through the... comparison step.
(5:10:44 PM) nuclearglory@gmail.com: I came out with 9.77>6.81
*/
