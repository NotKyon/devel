﻿#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum report_e {
	kReport_InternalError,
	kReport_Error,
	kReport_Warning,
	kReport_Hint,
	kReport_Note,
	kReport_Debug
} report_t;

typedef struct lineInfo_s {
	const char *start;
	unsigned int line;
	unsigned int column;
} lineInfo_t;

typedef enum token_e {
	kToken_Invalid = -1,
	kToken_EndOfFile,
	kToken_Literal,
	kToken_Operator,
	kToken_Identifier
} token_t;

typedef enum subtoken_e {
	kSubtoken_Unused = 0,

	kSubtoken_Lit_Number = 0,
	kSubtoken_Lit_String,
	
	kSubtoken_Op_Assign = 0,			// '='
	kSubtoken_Op_AssignAdd,				// '+='
	kSubtoken_Op_AssignSub,				// '-='
	kSubtoken_Op_AssignMul,				// '*='
	kSubtoken_Op_AssignDiv,				// '/='
	kSubtoken_Op_AssignMod,				// '%='
	kSubtoken_Op_AssignAnd,				// '&='
	kSubtoken_Op_AssignOr,				// '|='
	kSubtoken_Op_AssignXor,				// '^='
	kSubtoken_Op_AssignShl,				// '<<='
	kSubtoken_Op_AssignShr,				// '>>='
	kSubtoken_Op_Add,					// '+'
	kSubtoken_Op_Sub,					// '-'
	kSubtoken_Op_Mul,					// '*'
	kSubtoken_Op_Div,					// '/'
	kSubtoken_Op_Mod,					// '%'
	kSubtoken_Op_And,					// '&'
	kSubtoken_Op_Or,					// '|'
	kSubtoken_Op_Xor,					// '^'
	kSubtoken_Op_Shl,					// '<<'
	kSubtoken_Op_Shr,					// '>>'
	kSubtoken_Op_Inc,					// '++'
	kSubtoken_Op_Dec,					// '--'
	kSubtoken_Op_CmpNot,				// '!'
	kSubtoken_Op_CmpAnd,				// '&&'
	kSubtoken_Op_CmpOr,					// '||'
	kSubtoken_Op_CmpEq,					// '=='
	kSubtoken_Op_CmpNEq,				// '!='
	kSubtoken_Op_CmpLt,					// '<'
	kSubtoken_Op_CmpGt,					// '>'
	kSubtoken_Op_CmpLe,					// '<='
	kSubtoken_Op_CmpGe,					// '>='
	kSubtoken_Op_CmpApxEq,				// '~='
	kSubtoken_Op_If,					// '?'
	kSubtoken_Op_Dot,					// '.'
	kSubtoken_Op_Colon,					// ':'
	kSubtoken_Op_Semicolon,				// ';'
	kSubtoken_Op_LParen,				// '('
	kSubtoken_Op_RParen,				// ')'
	kSubtoken_Op_LBracket,				// '['
	kSubtoken_Op_RBracket,				// ']'
	kSubtoken_Op_LBrace,				// '{'
	kSubtoken_Op_RBrace,				// '}'
	kSubtoken_Op_Attribute,				// '@'

	kSubtoken_Id_User = 0,
	kSubtoken_Id_TypeU8,				// u8
	kSubtoken_Id_TypeU16,				// u16
	kSubtoken_Id_TypeU32,				// u32
	kSubtoken_Id_TypeU64,				// u64
	kSubtoken_Id_TypeS8,				// s8
	kSubtoken_Id_TypeS16,				// s16
	kSubtoken_Id_TypeS32,				// s32
	kSubtoken_Id_TypeS64,				// s64
	kSubtoken_Id_TypeUInt,				// uint
	kSubtoken_Id_TypeSInt,				// sint
	kSubtoken_Id_TypeUPtr,				// uptr
	kSubtoken_Id_TypeSPtr,				// sptr
	kSubtoken_Id_TypeF32,				// f32
	kSubtoken_Id_TypeF64,				// f64
	kSubtoken_Id_TypeVec2,				// vec2
	kSubtoken_Id_TypeVec3,				// vec3
	kSubtoken_Id_TypeVec4,				// vec4
	kSubtoken_Id_TypeQuat,				// quat
	kSubtoken_Id_TypeMat2,				// mat2
	kSubtoken_Id_TypeMat3,				// mat3
	kSubtoken_Id_TypeMat4,				// mat4
	kSubtoken_Id_TypeVoid,				// void
	kSubtoken_Id_TypeString,			// string
	kSubtoken_Id_LowPrec,				// lowp
	kSubtoken_Id_MediumPrec,			// mediump
	kSubtoken_Id_HighPrec,				// highp
	kSubtoken_Id_In,					// in
	kSubtoken_Id_Out,					// out
	kSubtoken_Id_InOut,					// inout
	kSubtoken_Id_Ref,					// ref
	kSubtoken_Id_Auto,					// auto
	kSubtoken_Id_Static,				// static
	kSubtoken_Id_Extern,				// extern
	kSubtoken_Id_Volatile,				// volatile
	kSubtoken_Id_Const,					// const
	kSubtoken_Id_PureStrong,			// pure_strong
	kSubtoken_Id_PureWeak,				// pure_weak
	kSubtoken_Id_Struct,				// struct
	kSubtoken_Id_Class,					// class
	kSubtoken_Id_Interface,				// interface
	kSubtoken_Id_Enum,					// enum
	kSubtoken_Id_If,					// if
	kSubtoken_Id_Else,					// else
	kSubtoken_Id_While,					// while
	kSubtoken_Id_For,					// for
	kSubtoken_Id_Break,					// break
	kSubtoken_Id_Continue,				// continue
	kSubtoken_Id_Switch,				// switch
	kSubtoken_Id_Case,					// case
	kSubtoken_Id_Default,				// default
	kSubtoken_Id_Goto,					// goto
	kSubtoken_Id_Return,				// return
	kSubtoken_Id_Assert,				// assert
	kSubtoken_Id_Expect,				// expect
	kSubtoken_Id_Theory,				// theory
	kSubtoken_Id_Fact,					// fact
	kSubtoken_Id_Kernel,				// kernel
	kSubtoken_Id_Constructor,			// __constructor__
	kSubtoken_Id_Destructor,			// __destructor__
	kSubtoken_Id_New,					// new
	kSubtoken_Id_Delete,				// delete
	kSubtoken_Id_Null,					// null
	kSubtoken_Id_Template,				// template
	kSubtoken_Id_Typename,				// typename
	kSubtoken_Id_Align,					// align
	kSubtoken_Id_AlignOf,				// alignof
	kSubtoken_Id_SizeOf,				// sizeof
	kSubtoken_Id_OffsetOf,				// offsetof
	kSubtoken_Id_TypeOf,				// typeof
	kSubtoken_Id_File,					// __file__
	kSubtoken_Id_Line,					// __line__
	kSubtoken_Id_Function,				// __func__
	kSubtoken_Id_Super,					// __super__
	kSubtoken_Id_Base					// __base__
} subtoken_t;

typedef struct lexan_s {
	const char *base;
	const char *start;
	size_t length;
	token_t token;
	subtoken_t subtoken;
} lexan_t;

typedef struct syntaxNode_s {
	lexan_t lexan;

	struct syntaxNode_s *prnt;
	struct syntaxNode_s *head;
	struct syntaxNode_s *tail;
	struct syntaxNode_s *prev;
	struct syntaxNode_s *next;
} syntaxNode_t;

int gIdentifierBaseTable[ 256 ];
int gIdentifierTable[ 256 ];
int gNumeralTable[ 256 ];

const char *Com_GetReportName( report_t report ) {
	switch( report ) {
	case kReport_InternalError:
		return "Internal-Error";

	case kReport_Error:
		return "Error";

	case kReport_Warning:
		return "Warning";

	case kReport_Hint:
		return "Hint";

	case kReport_Note:
		return "Note";

	case kReport_Debug:
		return "Debug";
	}

	return "(unknown)";
}
void Com_ReportV( report_t report, const char *filename,
const lineInfo_t *lineInfo, const char *function, const char *format,
va_list args ) {
	const char *reportName;
	char buf[ 4096 ];
	int e;

	assert( format != NULL );

	e = errno;
	reportName = Com_GetReportName( report );

	if( filename != NULL ) {
		fprintf( stderr, "[%s", filename );

		if( lineInfo != NULL && lineInfo->line > 0 ) {
			if( lineInfo->column > 0 ) {
				fprintf( stderr, "(%i:%i)", lineInfo->line, lineInfo->column );
			} else {
				fprintf( stderr, "(%i)", lineInfo->line );
			}
		}

		if( function != NULL ) {
			fprintf( stderr, " %s] ", function );
		} else {
			fprintf( stderr, "] " );
		}
	} else if( function != NULL ) {
		fprintf( stderr, "<%s> ", function );
	}

	fprintf( stderr, "%s: ", format );

#if defined( _MSC_VER ) && defined( __STDC_WANT_SECURE_LIB__ )
	vsprintf_s( buf, sizeof( buf ), format, args );
#else
	vsnprintf( buf, sizeof( buf ), format, args );
	buf[ sizeof( buf ) - 1 ] = '\0';
#endif

	fprintf( stderr, buf );

	if( e != 0 ) {
#if defined( _MSC_VER ) && defined( __STDC_WANT_SECURE_LIB__ )
		char errbuf[ 256 ];

		errbuf[ 0 ] = '\0';
		( void )strerror_s( errbuf, sizeof( errbuf ), e );

		fprintf( stderr, ": %s [%i]\n", errbuf, e );
#else
		fprintf( stderr, ": %s [%i]\n", strerror( e ), e );
#endif
	} else {
		fprintf( stderr, "\n" );
	}
}
void Com_InternalError( const char *filename, const lineInfo_t *lineInfo,
const char *function, const char *format, ... ) {
	va_list args;

	va_start( args, format );
	Com_ReportV( kReport_InternalError, filename, lineInfo, function, format,
		args );
	va_end( args );
}
void Com_Error( const char *filename, const lineInfo_t *lineInfo,
const char *function, const char *format, ... ) {
	va_list args;

	va_start( args, format );
	Com_ReportV( kReport_Error, filename, lineInfo, function, format, args );
	va_end( args );
}
void Com_Warning( const char *filename, const lineInfo_t *lineInfo,
const char *function, const char *format, ... ) {
	va_list args;

	va_start( args, format );
	Com_ReportV( kReport_Warning, filename, lineInfo, function, format, args );
	va_end( args );
}
void Com_Hint( const char *filename, const lineInfo_t *lineInfo,
const char *function, const char *format, ... ) {
	va_list args;

	va_start( args, format );
	Com_ReportV( kReport_Hint, filename, lineInfo, function, format, args );
	va_end( args );
}
void Com_Note( const char *filename, const lineInfo_t *lineInfo,
const char *function, const char *format, ... ) {
	va_list args;

	va_start( args, format );
	Com_ReportV( kReport_Note, filename, lineInfo, function, format, args );
	va_end( args );
}
void Com_Debug( const char *filename, const lineInfo_t *lineInfo,
const char *function, const char *format, ... ) {
	va_list args;

	va_start( args, format );
	Com_ReportV( kReport_Debug, filename, lineInfo, function, format, args );
	va_end( args );
}

void Table_Clear( int table[ 256 ] ) {
	memset( &table[ 0 ], 0, sizeof( table ) );
}
void Table_SetRange( int table[ 256 ], char start, char end ) {
	for( char ch = start; ch <= end; ++ch ) {
		table[ ( unsigned char )ch ] = 1;
	}
}

void Lex_InitIdentifierTables( void ) {
	Table_Clear( gIdentifierBaseTable );
	Table_Clear( gIdentifierTable );

	Table_SetRange( gIdentifierBaseTable, 'a', 'z' );
	Table_SetRange( gIdentifierBaseTable, 'A', 'Z' );
	gIdentifierBaseTable[ '_' ] = 1;
	gIdentifierBaseTable[ '$' ] = 1;

	Table_SetRange( gIdentifierTable, 'a', 'z' );
	Table_SetRange( gIdentifierTable, 'A', 'Z' );
	Table_SetRange( gIdentifierTable, '0', '9' );
	gIdentifierTable[ '_' ] = 1;
	gIdentifierTable[ '$' ] = 1;
}
void Lex_InitNumeralTable( void ) {
	int i;

	for( i = 0; i < 256; ++ i ) {
		gNumeralTable[ i ] = 9999;
	}

	for( i = 0; i < 10; ++i ) {
		gNumeralTable[ '0' + i ] = i;
	}

	for( i = 0; i < 26; ++i ) {
		gNumeralTable[ 'a' + i ] = 10 + i;
		gNumeralTable[ 'A' + i ] = 10 + i;
	}
}

int Lex_GetLineInfo( lineInfo_t *lineInfo, const char *base, const char *ptr ) {
	const char *curPtr;
	const char *nlPtr;
	int curLine;

	assert( lineInfo != NULL );
	assert( base != NULL );
	assert( ptr != NULL );

	curLine = 1;
	curPtr = base;

	while( *curPtr != '\0' && curPtr < ptr ) {
		nlPtr = strchr( curPtr, '\n' );
		if( !nlPtr ) {
			nlPtr = strchr( curPtr, '\0' );
		}

		if( nlPtr < ptr ) {
			if( *nlPtr == '\0' ) {
				break;
			}

			curPtr = nlPtr + 1;
			continue;
		}

		lineInfo->start = curPtr;
		lineInfo->line = curLine;
		lineInfo->column = ptr - curPtr;
		return 1;
	}

	lineInfo->start = NULL;
	lineInfo->line = 0;
	lineInfo->column = 0;
	return 0;
}

int Lex_IsWhitespace( char ch ) {
	return !!( ch <= ' ' );
}
int Lex_IsIdentifier( char ch ) {
	return gIdentifierTable[ ( unsigned char )ch ];
}
int Lex_IsBaseIdentifier( char ch ) {
	return gIdentifierBaseTable[ ( unsigned char )ch ];
}
int Lex_IsNumber( char ch, int radix ) {
	return !!( gNumeralTable[ ( unsigned char )ch ] < radix );
}
int Lex_CheckRadix( char ch, int *radix ) {
	assert( radix != NULL );

	if( ch == 'x' || ch == 'X' ) {
		*radix = 16;
		return 1;
	}

	if( ch == 'b' || ch == 'B' ) {
		*radix = 2;
		return 1;
	}

	if( ch == 'o' || ch == 'O' ) {
		*radix = 8;
		return 1;
	}

	*radix = 10;
	return 0;
}

const char *Lex_SkipWhitespace( const char *p ) {
	assert( p != NULL );
	
	while( Lex_IsWhitespace( *p ) ) {
		++p;
	}

	return p;
}
const char *Lex_SkipIdentifier( const char *p ) {
	assert( p != NULL );

	while( Lex_IsIdentifier( *p ) ) {
		++p;
	}

	return p;
}
const char *Lex_SkipNumber( const char *p ) {
	int radix;

	assert( p != NULL );

	if( *p == '-' && Lex_IsNumber( *( p + 1 ), 10 ) ) {
		++p;
	}

	if( *p == '0' && Lex_CheckRadix( *( p + 1 ), &radix ) ) {
		p += 2;
	} else {
		radix = 10;
	}

	while( Lex_IsNumber( *p, radix ) ) {
		++p;
	}

	if( *p == '.' && radix == 10 && Lex_IsNumber( *( p + 1 ), radix ) ) {
		p += 2;

		while( Lex_IsNumber( *p, radix ) ) {
			++p;
		}
	}

	if( radix == 10 ) {
		if( *p == 'e' || *p == 'E' ) {
			++p;
			
			if( *p == '-' || *p == '+' ) {
				++p;
			}
			
			while( Lex_IsNumber( *p, radix ) ) {
				++p;
			}
		}
	}

	return p;
}

const char *Lex_SkipString( const char *p ) {
	assert( p != NULL );

	if( *p != '\"' ) {
		return p;
	}

	++p;
	while( *p != '\"' && *p != '\0' ) {
		if( *p == '\\' && *( p + 1 ) != '\0' ) {
			++p;
		}

		++p;
	}

	return p;
}

int main( int argc, char **argv ) {
	return EXIT_SUCCESS;
}
