#include "threadconf.h"

gb_bool_t gbIsHyperthreadingAvailable() {
	//
	//	TODO: Fix this.
	//
#if __i386__||__x86_64__ || _M_IX86 || _M_IX64
	static gb_bool_t didInit = GB_FALSE;
	static gb_bool_t htAvail;

	if (!didInit) {
		gb_u32_t d;

# if __GNUC__ || __clang__
		__asm__ __volatile__("cpuid" : "=d" (d) : "a" (1)); //get feature set
# else
		__asm {
			xor edx, edx
			mov eax, 1
			cpuid
			mov [d], edx
		};
# endif

		htAvail = ((d>>28) & 1);
		didInit = GB_TRUE;
	}

	return htAvail;
#else
	return GB_FALSE;
#endif
}
gb_u32_t gbGetCPUCoreCount() {
#if _WIN32
	SYSTEM_INFO si;

	memset(&si, 0, sizeof(si));
	GetSystemInfo(&si);

	return (gb_u32_t)si.dwNumberOfProcessors;
#elif __linux__||__linux||linux
	int count;

	if ((count = sysconf(_SC_NPROCESSORS_ONLN)) < 1) {
		fprintf(stderr, "Warning: sysconf(_SC_NPROCESSORS_ONLN) failed!\n");
		fflush(stderr);

		count = 1;
	}

	return count;
#else
	return 1; //TODO
#endif
}
gb_u32_t gbGetCPUThreadCount() {
	return gbGetCPUCoreCount()*(1 + (gb_u32_t)gbIsHyperthreadingAvailable());
}
