#include "simplify.h"

entity_t NewEntity__NoParent() {
	return NewEntity((entity_t)0);
}

float EntityPositionX(entity_t ent) {
	return GetEntityPosition(ent)->x;
}
float EntityPositionY(entity_t ent) {
	return GetEntityPosition(ent)->y;
}
float EntityPositionZ(entity_t ent) {
	return GetEntityPosition(ent)->z;
}

float EntityOffsetPositionX(entity_t ent) {
	return GetEntityOffsetPosition(ent)->x;
}
float EntityOffsetPositionY(entity_t ent) {
	return GetEntityOffsetPosition(ent)->y;
}
float EntityOffsetPositionZ(entity_t ent) {
	return GetEntityOffsetPosition(ent)->z;
}

float EntityGlobalPositionX(entity_t ent) {
	return GetEntityGlobalPosition(ent)->x;
}
float EntityGlobalPositionY(entity_t ent) {
	return GetEntityGlobalPosition(ent)->y;
}
float EntityGlobalPositionZ(entity_t ent) {
	return GetEntityGlobalPosition(ent)->z;
}

float EntityPostPositionX(entity_t ent) {
	return GetEntityPostPosition(ent)->x;
}
float EntityPostPositionY(entity_t ent) {
	return GetEntityPostPosition(ent)->y;
}
float EntityPostPositionZ(entity_t ent) {
	return GetEntityPostPosition(ent)->z;
}

float CalculateEntityScaleX(entity_t ent) {
	vec3_t v;

	CalculateEntityScale(&v, ent);
	return v.x;
}
float CalculateEntityScaleY(entity_t ent) {
	vec3_t v;

	CalculateEntityScale(&v, ent);
	return v.y;
}
float CalculateEntityScaleZ(entity_t ent) {
	vec3_t v;

	CalculateEntityScale(&v, ent);
	return v.z;
}

float CalculateEntityOffsetScaleX(entity_t ent) {
	vec3_t v;

	CalculateEntityOffsetScale(&v, ent);
	return v.x;
}
float CalculateEntityOffsetScaleY(entity_t ent) {
	vec3_t v;

	CalculateEntityOffsetScale(&v, ent);
	return v.y;
}
float CalculateEntityOffsetScaleZ(entity_t ent) {
	vec3_t v;

	CalculateEntityOffsetScale(&v, ent);
	return v.z;
}

float CalculateEntityGlobalScaleX(entity_t ent) {
	vec3_t v;

	CalculateEntityGlobalScale(&v, ent);
	return v.x;
}
float CalculateEntityGlobalScaleY(entity_t ent) {
	vec3_t v;

	CalculateEntityGlobalScale(&v, ent);
	return v.y;
}
float CalculateEntityGlobalScaleZ(entity_t ent) {
	vec3_t v;

	CalculateEntityGlobalScale(&v, ent);
	return v.z;
}

float CalculateEntityPostScaleX(entity_t ent) {
	vec3_t v;

	CalculateEntityPostScale(&v, ent);
	return v.x;
}
float CalculateEntityPostScaleY(entity_t ent) {
	vec3_t v;

	CalculateEntityPostScale(&v, ent);
	return v.y;
}
float CalculateEntityPostScaleZ(entity_t ent) {
	vec3_t v;

	CalculateEntityPostScale(&v, ent);
	return v.z;
}

float EntityRotationX(entity_t ent) {
	return GetEntityRotation(ent)->x;
}
float EntityRotationY(entity_t ent) {
	return GetEntityRotation(ent)->y;
}
float EntityRotationZ(entity_t ent) {
	return GetEntityRotation(ent)->z;
}

float EntityOffsetRotationX(entity_t ent) {
	return GetEntityOffsetRotation(ent)->x;
}
float EntityOffsetRotationY(entity_t ent) {
	return GetEntityOffsetRotation(ent)->y;
}
float EntityOffsetRotationZ(entity_t ent) {
	return GetEntityOffsetRotation(ent)->z;
}

float EntityGlobalRotationX(entity_t ent) {
	return GetEntityGlobalRotation(ent)->x;
}
float EntityGlobalRotationY(entity_t ent) {
	return GetEntityGlobalRotation(ent)->y;
}
float EntityGlobalRotationZ(entity_t ent) {
	return GetEntityGlobalRotation(ent)->z;
}

float EntityPostRotationX(entity_t ent) {
	return GetEntityPostRotation(ent)->x;
}
float EntityPostRotationY(entity_t ent) {
	return GetEntityPostRotation(ent)->y;
}
float EntityPostRotationZ(entity_t ent) {
	return GetEntityPostRotation(ent)->z;
}
