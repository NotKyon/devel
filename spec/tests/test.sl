//
//	This is a specification library.
//	It provides global configuration for the library.
//

//==============================================================================
// Each library can begin with a global namespace, but it is optional.

// Namespaces can have abbreviations
namespace MyLib "ML";

//==============================================================================
// Now we can configure various options for the library...

Config {

	// The following represents the default transformations

	FunctionFormat = "$(name)"
	NamespaceFunctionFormat = "$(lower $(namespace))_";

		//these are subject to NamespaceFunctionFormat transformation
		MethodFormat = "$(name)"
		CMethodFormat = "$(object)_$(name)"

	TypedefFormat = "$(name)_t"
	NamespaceTypedefFormat = "$(upper $(namespace_abrv))$(name)"

	License = None;
		/* Valid options:
		>> None
		>> GPL
		>> GPL2
		>> GPL3
		>> BSD
		>> NewBSD
		>> Apache2
		>> MIT
		>> PublicDomain
		** You can also reference your own file:
		>> <path/to/my/license.txt>
		** Or just place the text directly:
		>> "Totally bland license."
		** Note: This applies to all config fields, not just the license field.
		*/

	Src = { C, C++ }; // Also supported: ObjC, ObjC++, Java
	Doc = { HTML, LaTeX }; // Also supported: BBCode

}
