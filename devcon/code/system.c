#include "system.h"
#include "devcon.h"

#include <stdio.h>

void sys_LogV(const char *format, va_list args) {
	if (con_IsAlive()) {
		con_PrintV(format, args);
		return;
	}

	vfprintf(stdout, format, args);
	fflush(stdout);
}
void sys_Log(const char *format, ...) {
	va_list args;

	va_start(args, format);
	sys_LogV(format, args);
	va_end(args);
}

void sys_Error(const char *format, ...) {
	static const char *prompt = "ERROR: ";
	va_list args;
	char buf[4096];

	va_start(args, format);
	vsnprintf(buf, sizeof(buf)-1, format, args);
	buf[sizeof(buf)-1] = 0;
	va_end(args);

	if (con_IsAlive())
		sys_Log("\xF1%s\xF0%s\n", prompt, buf);
	else
		sys_Log("%s%s\n", prompt, buf);
}
void sys_Warning(const char *format, ...) {
	static const char *prompt = "WARNING: ";
	va_list args;
	char buf[4096];

	va_start(args, format);
	vsnprintf(buf, sizeof(buf)-1, format, args);
	buf[sizeof(buf)-1] = 0;
	va_end(args);

	if (con_IsAlive())
		sys_Log("\xF2%s\xF0%s\n", prompt, buf);
	else
		sys_Log("%s%s\n", prompt, buf);
}
