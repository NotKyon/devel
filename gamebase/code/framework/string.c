#include <gamebase.h>

#include <stdlib.h>
#include <string.h>

#define STR_CHUNK 64

void gbInitString(gb_string_t *str) {
	gbAssert(str != (gb_string_t *)NULL);

	str->cap = 0;
	str->len = 0;
	str->p = (char *)NULL;
}
void gbFiniString(gb_string_t *str) {
	gbAssert(str != (gb_string_t *)NULL);

	str->cap = 0;
	str->len = 0;
	str->p = (char *)gbMemFree((void *)str->p);
}

void gbResizeString(gb_string_t *str, size_t len) {
	gbAssert(str != (gb_string_t *)NULL);

	if (len + 1 > str->cap) {
		str->cap  = len + 1;
		str->cap -= str->cap%STR_CHUNK;
		str->cap += STR_CHUNK;

		str->p = (char *)gbMemExtend((void *)str->p, str->cap);
	}

	str->len = len;
}

void gbPushChar(gb_string_t *str, char ch) {
	size_t len;

	gbAssert(str != (gb_string_t *)NULL);

	len = str->len;
	gbResizeString(str, len + 1);

	str->p[len + 0] = ch;
	str->p[len + 1] = '\0';
}
void gbPushStringN(gb_string_t *str, const char *s, size_t n) {
	size_t l1, l2;

	gbAssert(str != (gb_string_t *)NULL);
	gbAssert(s != (const char *)NULL);

	l1 = str->len;
	l2 = n ? n : strlen(s);

	gbResizeString(str, l1 + l2);

	memcpy(&str->p[l1], (const void *)s, l2);
	str->p[l1 + l2] = '\0';
}
void gbPushString(gb_string_t *str, const char *s) {
	gbAssert(str != (gb_string_t *)NULL);
	gbAssert(s != (const char *)NULL);

	gbPushStringN(str, s, 0);
}

size_t gbStringCapacity(const gb_string_t *str) {
	gbAssert(str != (const gb_string_t *)NULL);

	return str->cap;
}
size_t gbStringLength(const gb_string_t *str) {
	gbAssert(str != (const gb_string_t *)NULL);

	return str->len;
}
const char *gbStringBuffer(const gb_string_t *str) {
	gbAssert(str != (const gb_string_t *)NULL);

	return str->p;
}
char *gbStringData(gb_string_t *str) {
	gbAssert(str != (gb_string_t *)NULL);

	return str->p;
}

void gbSetStringN(gb_string_t *str, const char *s, size_t n) {
	size_t len;

	gbAssert(str != (gb_string_t *)NULL);
	gbAssert(s != (const char *)0);

	len = n ? n : strlen(s);
	gbResizeString(str, len);

	memcpy(&str->p[0], (const void *)s, len);
	str->p[len] = 0;
}
void gbSetString(gb_string_t *str, const char *s) {
	gbAssert(str != (gb_string_t *)NULL);
	gbAssert(s != (const char *)0);

	gbSetStringN(str, s, 0);
}
