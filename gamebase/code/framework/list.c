#include <gamebase.h>

void gbInitList(gb_list_t *list) {
	gbAssert(list != (gb_list_t *)NULL);

	list->head = (gb_node_t *)NULL;
	list->tail = (gb_node_t *)NULL;
	list->rmcb = (gb_fnRemoveNode_t)NULL;
}
void gbFiniList(gb_list_t *list) {
	gbAssert(list != (gb_list_t *)NULL);

	while(list->head)
		gbRemoveNode(list->head);

	list->rmcb = (gb_fnRemoveNode_t)NULL;
}

void gbSetListRemoveCallback(gb_list_t *list, gb_fnRemoveNode_t rmcb) {
	gbAssert(list != (gb_list_t *)NULL);

	list->rmcb = rmcb;
}
gb_fnRemoveNode_t gbGetListRemoveCallback(gb_list_t *list) {
	gbAssert(list != (gb_list_t *)NULL);

	return list->rmcb;
}

void gbInitNode(gb_node_t *node, void *obj) {
	gbAssert(node != (gb_node_t *)NULL);

	node->obj = obj;
	node->prev = (gb_node_t *)NULL;
	node->next = (gb_node_t *)NULL;
	node->list = (gb_list_t *)NULL;
}
void gbFiniNode(gb_node_t *node) {
	gbAssert(node != (gb_node_t *)NULL);

	gbRemoveNode(node);
}

void gbSetNodeObject(gb_node_t *node, void *obj) {
	gbAssert(node != (gb_node_t *)NULL);

	node->obj = obj;
}
void *gbNodeObject(gb_node_t *node) {
	gbAssert(node != (gb_node_t *)NULL);

	return node->obj;
}

void gbAddFirst(gb_list_t *list, gb_node_t *node) {
	gbAssert(list != (gb_list_t *)NULL);
	gbAssert(node != (gb_node_t *)NULL);

	gbUnlinkNode(node);

	node->list = list;

	if ((node->next = list->head) != (gb_node_t *)NULL)
		list->head->prev = node;
	else
		list->tail = node;
	list->head = node;
}
void gbAddLast(gb_list_t *list, gb_node_t *node) {
	gbAssert(list != (gb_list_t *)NULL);
	gbAssert(node != (gb_node_t *)NULL);

	gbUnlinkNode(node);

	node->list = list;

	if ((node->prev = list->tail) != (gb_node_t *)NULL)
		list->tail->next = node;
	else
		list->head = node;
	list->tail = node;
}
void gbInsertBefore(gb_node_t *node, gb_node_t *before) {
	gbAssert(node != (gb_node_t *)NULL);
	gbAssert(before != (gb_node_t *)NULL);

	if (node->next==before)
		return;

	gbUnlinkNode(node);

	node->list = before->list;
	gbAssert(node->list != (gb_list_t *)NULL);

	node->prev = before->prev;
	node->next = before;

	if (node->prev)
		node->prev->next = node;
	else
		node->list->head = node;

	before->prev = node;
}
void gbInsertAfter(gb_node_t *node, gb_node_t *after) {
	gbAssert(node != (gb_node_t *)NULL);
	gbAssert(after != (gb_node_t *)NULL);

	if (node->prev==after)
		return;

	gbUnlinkNode(node);

	node->list = after->list;
	gbAssert(node->list != (gb_list_t *)NULL);

	node->prev = after;
	node->next = after->next;

	if (node->next)
		node->next->prev = node;
	else
		node->list->tail = node;

	after->next = node;
}

gb_list_t *gbNodeList(gb_node_t *node) {
	gbAssert(node != (gb_node_t *)NULL);

	return node->list;
}
void gbUnlinkNode(gb_node_t *node) {
	gbAssert(node != (gb_node_t *)NULL);

	if (!node->list)
		return;

	if (node->prev)
		node->prev->next = node->next;
	else
		node->list->head = node->next;

	if (node->next)
		node->next->prev = node->prev;
	else
		node->list->tail = node->prev;

	node->prev = (gb_node_t *)NULL;
	node->next = (gb_node_t *)NULL;
	node->list = (gb_list_t *)NULL;
}
void gbRemoveNode(gb_node_t *node) {
	gb_list_t *list;

	gbAssert(node != (gb_node_t *)NULL);

	list = node->list;
	if (!list)
		return;

	gbUnlinkNode(node);

	if (list->rmcb && node->obj)
		list->rmcb(node->obj);
}

gb_node_t *gbFirstNode(gb_list_t *list) {
	gbAssert(list != (gb_list_t *)NULL);

	return list->head;
}
gb_node_t *gbLastNode(gb_list_t *list) {
	gbAssert(list != (gb_list_t *)NULL);

	return list->tail;
}
gb_node_t *gbNodeBefore(gb_node_t *node) {
	gbAssert(node != (gb_node_t *)NULL);

	return node->prev;
}
gb_node_t *gbNodeAfter(gb_node_t *node) {
	gbAssert(node != (gb_node_t *)NULL);

	return node->next;
}

void *gbFirst(gb_list_t *list) {
	gbAssert(list != (gb_list_t *)NULL);

	return list->head ? list->head->obj : (void *)NULL;
}
void *gbLast(gb_list_t *list) {
	gbAssert(list != (gb_list_t *)NULL);

	return list->tail ? list->tail->obj : (void *)NULL;
}
void *gbBefore(gb_node_t *node) {
	gbAssert(node != (gb_node_t *)NULL);

	return node->prev ? node->prev->obj : (void *)NULL;
}
void *gbAfter(gb_node_t *node) {
	gbAssert(node != (gb_node_t *)NULL);

	return node->next ? node->next->obj : (void *)NULL;
}
