// includes
#include <Axia/Vanilla/Vanilla.h>
#include <Axia/Vanilla/Internal.h>
#include <stdio.h>

// global: running
bool										g_running = false;

// global: lists
AXEvent										*g_pEvent = (AXEvent *)0;
AXListBase<AXEvent>							AXEvent::g_eventQueue;
AXListBase<AXHook>							g_hookList;

// get the hwnd of a super-gadget for a control
inline HWND GetSuperHWND(AXGadget *pGadget)
{
	if (!pGadget)
		return (HWND)0;

	if (pGadget->Type()==AXTabGadgetType)
	{
		if (!pGadget->Parent())
			return (HWND)0;

		return ((AXWidget *)pGadget->Parent())->WindowRef();
	}

	if (!pGadget->IsWidget())
		return (HWND)0;

	return ((AXWidget *)pGadget)->WindowRef();
}

// process an event
static void ProcessMessage(MSG *pMsg)
{
	switch(pMsg->message)
	{
		case WM_QUIT:
			g_running = false;
			new AXEvent(AXAppTerminateEvent, (void *)0, 0, 0, 0, (void *)(int)pMsg->wParam);
			break;

		case WM_TIMER:
			if (pMsg->wParam==1)
				break;
			new AXEvent(AXTimerTickEvent, (void *)(int)pMsg->wParam);
			break;

		default:
			break;
	}

	TranslateMessage(pMsg);
	DispatchMessageA(pMsg);
}

// grab an event from the queue
static AXEvent *NextEvent(void)
{
	if (g_pEvent)
		delete g_pEvent;

	if ((g_pEvent = AXEvent::g_eventQueue.First()) != (AXEvent *)0)
		AXRunHooks(g_pEvent->id, g_pEvent, g_pEvent->pData);

	return g_pEvent;
}

// default window
static LRESULT CALLBACK DefaultFrameProc(HWND hwndRef, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	/*
	switch(uiMsg)
	{
		case WM_ERASEBKGND:
			return 0;
		case WM_PAINT:
		{
			PAINTSTRUCT	ps;
			HDC			hdcRef;

			hdcRef = BeginPaint(hwndRef, &ps);
			FillRect(hdcRef, &ps.rcPaint, GetSysColorBrush(COLOR_BTNFACE));
			EndPaint(hwndRef, &ps);

			return 1;
		}
	}
	*/

	return DefWindowProcA(hwndRef, uiMsg, wParam, lParam);
}

// initialize
AX_FUNC void AX_API AXInitVanilla(void)
{
	// declarations
	WNDCLASSEXA			wc;

	// fill the descriptor
	wc.cbSize			= sizeof(wc);
	wc.style			= CS_OWNDC|CS_HREDRAW|CS_VREDRAW;
	wc.lpfnWndProc		= DefaultFrameProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= GetModuleHandleA(0);
	wc.hIcon			= LoadIconA(wc.hInstance, (const char *)1);
	wc.hCursor			= LoadCursorA(0, IDC_ARROW);
	wc.hbrBackground	= GetSysColorBrush(COLOR_BTNFACE);
	wc.lpszMenuName		= 0;
	wc.lpszClassName	= "AXDefaultFrame";
	wc.hIconSm			= LoadIconA(wc.hInstance, (const char *)1);

	// register
	RegisterClassExA(&wc);

	// initialize the common controls
	//InitCommonControls();

	// running
	g_running = true;
}

// deinitialize
AX_FUNC void AX_API AXDeinitVanilla(void)
{
	// unregister
	UnregisterClassA("AXDefaultFrame", GetModuleHandleA(0));

	// destroy the lists
	AXEvent::g_eventQueue.DeleteAll();
	g_hookList.DeleteAll();
}

// display a hexadecimal value
AX_FUNC const char *AX_API AXHex(unsigned int value)
{
	static char	szBuff[10];

#if _MSC_VER>=1300
	sprintf_s(szBuff, sizeof(szBuff), "%.8X", value);
#elif defined(_MSC_VER)
	sprintf(szBuff, "%.8X", value);
#else
	snprintf(szBuff, sizeof(szBuff), "%.8X", value);
	szBuff[9] = 0;
#endif
	return &szBuff[0];
}

// determine whether the app is currently running
AX_FUNC int AX_API AXIsRunning(void)
{
	return (g_running)?1:0;
}

// perform a fast synchronize
AX_FUNC void AX_API AXFastSync(void)
{
	MSG	message;

	while(PeekMessageA(&message, 0, 0, 0, PM_REMOVE))
		ProcessMessage(&message);

	while(NextEvent())
	{
	}
}

// perform a waiting synchronize
AX_FUNC void AX_API AXSync(void)
{
	MSG	message;

	if (GetMessageA(&message, 0, 0, 0) <= 0)
	{
		g_running = false;
		return;
	}

	ProcessMessage(&message);

	while(NextEvent())
	{
	}
}

// quit
AX_FUNC void AX_API AXQuit(void)
{
	g_running = false;
	PostQuitMessage(0);
}

// poll an event
AX_FUNC int AX_API AXPollEvent(void)
{
	AXEvent	*pEvent;
	MSG		msg;

	while(PeekMessageA(&msg, 0, 0, 0, PM_REMOVE))
		ProcessMessage(&msg);

	if ((pEvent=NextEvent()) != (AXEvent *)0)
		return pEvent->id;

	return 0;
}

// wait for an event
AX_FUNC int AX_API AXWaitEvent(void)
{
	AXEvent	*pEvent;
	MSG		msg;

	while(!(pEvent=NextEvent()))
	{
		if (GetMessageA(&msg, 0, 0, 0) <= 0)
			return 0;

		ProcessMessage(&msg);
	}

	return pEvent->id;
}

// post an event
AX_FUNC void AX_API AXPostEvent(int id, void *pData, int mods, int x, int y, void *pExtra)
{
	new AXEvent(id, pData, mods, x, y, pExtra);
}

// retrieve the ID of the current event
AX_FUNC int AX_API AXEventID(void)
{
	return (g_pEvent)?g_pEvent->id:0;
}

// retrieve the x value of the current event
AX_FUNC int AX_API AXEventX(void)
{
	return (g_pEvent)?g_pEvent->x:0;
}

// retrieve the y value of the current event
AX_FUNC int AX_API AXEventY(void)
{
	return (g_pEvent)?g_pEvent->y:0;
}

// retrieve the data of the current event
AX_FUNC void *AX_API AXEventData(void)
{
	return (g_pEvent)?g_pEvent->pData:(void *)0;
}

// retrieve the modifiers of the current event
AX_FUNC int AX_API AXEventMods(void)
{
	return (g_pEvent)?g_pEvent->mods:0;
}

// retrieve the extra member of the current event
AX_FUNC void *AX_API AXEventExtra(void)
{
	return (g_pEvent)?g_pEvent->pExtra:(void *)0;
}

// retrieve the text member of the current event
AX_FUNC const char *AX_API AXEventText(void)
{
	return (g_pEvent)?(const char *)g_pEvent->pExtra:(const char *)0;
}

// add a hook
AX_FUNC void AX_API AXAddHook(int id, AXHookCallback_t pfnFunc, void *pContext)
{
	AXHook	*pHook;

	if (!pfnFunc)
		return;

	if (!(pHook = new AXHook()))
		return;

	pHook->id = id;
	pHook->pfnFunc = pfnFunc;
	pHook->pContext = pContext;

	pHook->hookLink.SetObject(pHook);
	g_hookList.Add(&pHook->hookLink);
}

// run all hooks for a given object
AX_FUNC int AX_API AXRunHooks(int id, void *pData, void *pContext)
{
	AXHook	*pHook;
	int		lastData = 0;

	for(pHook=g_hookList.First(); pHook; pHook=pHook->hookLink.After())
	{
		if (pHook->id != id)
			continue;

		if (pHook->pContext!=pContext && pHook->pContext!=(void *)0)
			continue;

		lastData = pHook->pfnFunc(id, pData, pContext);
	}

	return lastData;
}

// replace a given hook
AX_FUNC void AX_API AXReplaceHook(int id, AXHookCallback_t pfnOldFunc, AXHookCallback_t pfnNewFunc, void *pContext)
{
	AXHook	*pHook;

	for(pHook=g_hookList.First(); pHook; pHook=pHook->hookLink.After())
	{
		if (pHook->id != id)
			continue;

		if (pHook->pContext != pContext)
			continue;

		if (pHook->pfnFunc != pfnOldFunc)
			continue;

		pHook->pfnFunc = pfnNewFunc;
		break;
	}
}

// remove a hook
AX_FUNC void AX_API AXRemoveHook(int id, AXHookCallback_t pfnFunc, void *pContext)
{
	AXHook	*pHook;

	for(pHook=g_hookList.First(); pHook; pHook=pHook->hookLink.After())
	{
		if (pHook->id != id)
			continue;

		if (pHook->pContext != pContext)
			continue;

		if (pHook->pfnFunc != pfnFunc)
			continue;

		g_hookList.Remove(&pHook->hookLink);
		delete pHook;

		break;
	}
}

// remove all hooks for a given id and context
AX_FUNC void AX_API AXRemoveAllHooks(int id, void *pContext)
{
	AXHook	*pHook;

	for(pHook=g_hookList.First(); pHook; pHook=pHook->hookLink.After())
	{
		if (pHook->id != id)
			continue;

		if (pHook->pContext != pContext)
			continue;

		g_hookList.Remove(&pHook->hookLink);
		delete pHook;

		break;
	}
}

// get the number of hooks for a given context
AX_FUNC int AX_API AXGetHookCount(int id, void *pContext)
{
	// TODO
	MessageBoxA(0, "AXGetHookCount: Feature not implemented.", "Not Implemented", MB_ICONWARNING|MB_OK);
	return 0;
}

// get a hook function for a given hook index
AX_FUNC AXHookCallback_t AX_API AXGetHook(int id, int index, void *pContext)
{
	// TODO
	MessageBoxA(0, "AXGetHook: Feature not implemented.", "Not Implemented", MB_ICONWARNING|MB_OK);
	return (AXHookCallback_t)0;
}

// create a widget from a window handle
AX_FUNC AXWidget *AX_API AXMakeWidget(void *hwndRef)
{
	return new AXWidget(AXWidgetGadgetType, *(HWND *)&hwndRef);
}

// destroy a widget
AX_FUNC void AX_API AXKillWidget(AXGadget *pWidget)
{
	if (!pWidget)
		return;

	delete pWidget;
}

// get the type of a gadget
AX_FUNC int AX_API AXGadgetType(AXGadget *pGadget)
{
	if (!pGadget)
		return AXUnknownGadgetType;

	return pGadget->Type();
}

// create a window
AX_FUNC AXWidget *AX_API AXMakeWindow(const char *pszTitle, int w, int h)
{
	DWORD	dwExStyle, dwStyle;
	HWND	hwndRef;
	RECT	rcArea;

	dwExStyle		= WS_EX_APPWINDOW;
	dwStyle			= WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX|WS_MAXIMIZEBOX|WS_SIZEBOX;
	dwStyle		   |= WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	rcArea.left		= GetSystemMetrics(SM_CXSCREEN)/2 - w/2;
	rcArea.top		= GetSystemMetrics(SM_CYSCREEN)/2 - h/2;
	rcArea.right	= rcArea.left+w;
	rcArea.bottom	= rcArea.top+h;

	AdjustWindowRectEx(&rcArea, dwStyle, FALSE, dwExStyle);

	rcArea.right   -= rcArea.left;
	rcArea.bottom  -= rcArea.top;

	hwndRef = CreateWindowExA
	(
		dwExStyle, "AXDefaultFrame", pszTitle, dwStyle,
		rcArea.left, rcArea.top, rcArea.right, rcArea.bottom,
		(HWND)0, (HMENU)0, GetModuleHandleA(0), 0
	);
	if (!hwndRef)
		return (AXWidget *)0;

	return new AXWidget(AXWindowGadgetType, hwndRef);
}

// create an mdi window
AX_FUNC AXWidget *AX_API AXMakeMDIWindow(const char *pszTitle, int w, int h)
{
	return (AXWidget *)0;
}

// create an mdi child window
AX_FUNC AXWidget *AX_API AXMakeChildWindow(AXWidget *pSuper, const char *pszTitle, int w, int h)
{
	return (AXWidget *)0;
}

// create a dialog window
AX_FUNC AXWidget *AX_API AXMakeDialogWindow(AXWidget *pOwner, const char *pszTitle, int w, int h)
{
	return (AXWidget *)0;
}

// calculate the width needed for a particular control
AX_FUNC int AX_API AXCalculateControlWidth(const char *pszText, int flags)
{
	// constants
	static const bool	c_tempDC = true;

	// declarations
	HGDIOBJ				hOld;
	DWORD				dwStyle;
	RECT				rcArea;
	HDC					hdcTemp;
	int					width;

	// text drawing style
	dwStyle			=	DT_CALCRECT|DT_NOCLIP|DT_SINGLELINE;
	if ((flags&1)==0)
		dwStyle		=	dwStyle|DT_NOPREFIX;

	// nullify the test rectangle
	rcArea.left		=	0;
	rcArea.top		=	0;
	rcArea.right	=	0;
	rcArea.bottom	=	0;

	// determine the device context to draw into
	if (c_tempDC)
		hdcTemp		=	CreateCompatibleDC(0);
	else
		hdcTemp		=	0;

	// select the default GUI font as the current
	hOld			=	SelectObject
					(
						hdcTemp, GetStockObject(DEFAULT_GUI_FONT)
					);

	// calculate the dimensions of the text and restore the previous font
	DrawTextA		(	hdcTemp, pszText, -1, &rcArea, dwStyle	);
	SelectObject	(	hdcTemp, hOld							);

	// destroy the temporary device context (if it was created)
	if (c_tempDC)
		DeleteDC	(	hdcTemp									);

	// calculate the width (align to 8 pixels)
	width			=	rcArea.right-rcArea.left+10;
	width			=	width + (width%8);

	// done
	return				width;
}

// calculate the layout for a control
static void CalculateLayout(AXGadget *pSuper, const char *pszTitle, int *pX, int *pY, int *pWidth, int *pHeight, int add_width=0)
{
	// declarations
	int	left, top, right, bottom;

	// tab gadgets are treated differently: check for a tab
	if (pSuper->Type()==AXTabGadgetType)
		// convert the super to the tab's parent (the tabber)
		pSuper = pSuper->Parent();

	// query the super-gadget for its client area
	pSuper->QueryClientArea(&left, &top, &right, &bottom);

	// adjust the position
	*pX += left;
	*pY += top;

	// calculate the width needed for the control
	if (!(*pWidth))
		*pWidth = (pszTitle) ? (AXCalculateControlWidth(pszTitle, 1)+add_width) : 180;

	// calculate the height needed for the control
	if (!(*pHeight))
		*pHeight = (pszTitle) ? 24 : 100;
}

// create a button
AX_FUNC AXWidget *AX_API AXMakeButton(AXWidget *pSuper, const char *pszTitle, int x, int y, int w, int h, int flags)
{
	// declarations
	DWORD	dwExStyle, dwStyle;
	HWND	hwndRef;

	// select the super-window
	if (!(hwndRef=GetSuperHWND(pSuper)))
		return (AXWidget *)0;

	// calculate the style for this control
	dwExStyle = 0;
	dwStyle   = WS_CHILD|WS_VISIBLE|((flags&1)?BS_DEFPUSHBUTTON:BS_PUSHBUTTON);
	dwStyle  |= WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	// calculate the layout needed for this control
	CalculateLayout(pSuper, pszTitle, &x, &y, &w, &h);

	// create the native window handle
	hwndRef = CreateWindowExA(dwExStyle, "Button", pszTitle, dwStyle, x, y, w, h, hwndRef, (HMENU)0, GetModuleHandleA(0), 0);
	if (!hwndRef)
		return (AXWidget *)0;

	// change the font from "system" to the default GUI font
	SendMessageA(hwndRef, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);

	// create and return the widget
	return new AXWidget(AXButtonGadgetType, hwndRef, pSuper);
}

// create a check box
AX_FUNC AXWidget *AX_API AXMakeCheckBox(AXWidget *pSuper, const char *pszTitle, int x, int y, int w, int h, int flags)
{
	// declarations
	DWORD	dwExStyle, dwStyle;
	HWND	hwndRef;

	// select the super-window
	if (!(hwndRef=GetSuperHWND(pSuper)))
		return (AXWidget *)0;

	// calculate the style for this control
	dwExStyle = 0;
	dwStyle   = WS_CHILD|WS_VISIBLE|((flags&1)?BS_AUTOCHECKBOX:BS_CHECKBOX);
	dwStyle  |= WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	// calculate the layout needed for this control
	CalculateLayout(pSuper, pszTitle, &x, &y, &w, &h, GetSystemMetrics(SM_CXSMICON));

	// create the native window handle
	hwndRef = CreateWindowExA(dwExStyle, "Button", pszTitle, dwStyle, x, y, w, h, hwndRef, (HMENU)0, GetModuleHandleA(0), 0);
	if (!hwndRef)
		return (AXWidget *)0;

	// change the font from "system" to the default GUI font
	SendMessageA(hwndRef, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);

	// create and return the widget
	return new AXWidget(AXButtonGadgetType, hwndRef, pSuper);
}

// create a radio box
AX_FUNC AXWidget *AX_API AXMakeRadioBox(AXWidget *pSuper, const char *pszTitle, int x, int y, int w, int h, int flags)
{
	// declarations
	DWORD	dwExStyle, dwStyle;
	HWND	hwndRef;

	// select the super-window
	if (!(hwndRef=GetSuperHWND(pSuper)))
		return (AXWidget *)0;

	// calculate the style for this control
	dwExStyle = 0;
	dwStyle   = WS_CHILD|WS_VISIBLE|((flags&1)?BS_AUTORADIOBUTTON:BS_RADIOBUTTON);
	dwStyle  |= WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	// calculate the layout needed for this control
	CalculateLayout(pSuper, pszTitle, &x, &y, &w, &h, GetSystemMetrics(SM_CXSMICON));

	// create the native window handle
	hwndRef = CreateWindowExA(dwExStyle, "Button", pszTitle, dwStyle, x, y, w, h, hwndRef, (HMENU)0, GetModuleHandleA(0), 0);
	if (!hwndRef)
		return (AXWidget *)0;

	// change the font from "system" to the default GUI font
	SendMessageA(hwndRef, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);

	// create and return the widget
	return new AXWidget(AXButtonGadgetType, hwndRef, pSuper);
}

// create a text field
AX_FUNC AXWidget *AX_API AXMakeTextField(AXWidget *pSuper, const char *pszTitle, int x, int y, int w, int h)
{
	// declarations
	DWORD	dwExStyle, dwStyle;
	HWND	hwndRef;

	// select the super-window
	if (!(hwndRef=GetSuperHWND(pSuper)))
		return (AXWidget *)0;

	// calculate the style for this control
	dwExStyle = WS_EX_CLIENTEDGE;
	dwStyle   = WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL;
	dwStyle  |= WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	// calculate the layout needed for this control
	CalculateLayout(pSuper, pszTitle, &x, &y, &w, &h);

	// create the native window handle
	hwndRef = CreateWindowExA(dwExStyle, "Edit", pszTitle, dwStyle, x, y, w, h, hwndRef, (HMENU)0, GetModuleHandleA(0), 0);
	if (!hwndRef)
		return (AXWidget *)0;

	// change the font from "system" to the default GUI font
	SendMessageA(hwndRef, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);

	// create and return the widget
	return new AXWidget(AXTextFieldGadgetType, hwndRef, pSuper);
}

// create a text area
AX_FUNC AXWidget *AX_API AXMakeTextArea(AXWidget *pSuper, const char *pszTitle, int x, int y, int w, int h)
{
	// declarations
	DWORD	dwExStyle, dwStyle;
	HWND	hwndRef;

	// select the super-window
	if (!(hwndRef=GetSuperHWND(pSuper)))
		return (AXWidget *)0;

	// calculate the style for this control
	dwExStyle = WS_EX_CLIENTEDGE;
	dwStyle   = WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|ES_AUTOVSCROLL|ES_MULTILINE;
	dwStyle  |= WS_CLIPSIBLINGS|WS_CLIPCHILDREN|WS_HSCROLL|WS_VSCROLL;

	// calculate the layout needed for this control
	CalculateLayout(pSuper, pszTitle, &x, &y, &w, &h);

	// create the native window handle
	hwndRef = CreateWindowExA(dwExStyle, "Edit", pszTitle, dwStyle, x, y, w, h, hwndRef, (HMENU)0, GetModuleHandleA(0), 0);
	if (!hwndRef)
		return (AXWidget *)0;

	// change the font from "system" to the default GUI font
	SendMessageA(hwndRef, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);

	// create and return the widget
	return new AXWidget(AXTextAreaGadgetType, hwndRef, pSuper);
}

// create a tab pane
AX_FUNC AXWidget *AX_API AXMakeTabber(AXWidget *pSuper, int x, int y, int w, int h)
{
	// declarations
	DWORD	dwExStyle, dwStyle;
	HWND	hwndRef;

	// select the super-window
	if (!(hwndRef=GetSuperHWND(pSuper)))
		return (AXWidget *)0;

	// calculate the style for this control
	dwExStyle = 0;
	dwStyle   = WS_CHILD|WS_VISIBLE|TCS_FOCUSNEVER;
	dwStyle  |= WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	// calculate the layout needed for this control
	CalculateLayout(pSuper, (const char *)0, &x, &y, &w, &h);

	// create the native window handle
	hwndRef = CreateWindowExA(dwExStyle, "SysTabControl32", "", dwStyle, x, y, w, h, hwndRef, (HMENU)0, GetModuleHandleA(0), 0);
	if (!hwndRef)
		return (AXWidget *)0;

	// change the font from "system" to the default GUI font
	SendMessageA(hwndRef, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);

	// create and return the widget
	return new AXTabber(hwndRef, pSuper);
}

// create a tab
AX_FUNC AXGadget *AX_API AXMakeTab(AXWidget *pTabPane, const char *pszTitle)
{
	// declarations
	TCITEMA	tci;
	AXTab	*pTab;

	// validate the super-gadget (should be a tabber)
	if (!GadgetIsType(pTabPane, AXTabberGadgetType))
		return (AXGadget *)0;

	// create the tab gadget handle
	if (!(pTab = new AXTab()))
		return (AXGadget *)0;

	// specify the parent for the tab gadget (select the tabber passed in)
	pTab->SetParent(pTabPane);

	// create a new tab for the tabber and store its index in the tab gadget
	tci.mask = TCIF_TEXT;
	tci.pszText = (char *)pszTitle;
	pTab->SetTabIndex(TabCtrl_InsertItem(pTabPane->WindowRef(), 32000, &tci));

	// return the gadget
	return pTab;
}

// create a tree view
AX_FUNC AXWidget *AX_API AXMakeTreeView(AXWidget *pSuper, int x, int y, int w, int h)
{
	// declarations
	DWORD	dwExStyle, dwStyle;
	HWND	hwndRef;

	// select the super-window
	if (!(hwndRef=GetSuperHWND(pSuper)))
		return (AXWidget *)0;

	// calculate the style for this control
	dwExStyle = WS_EX_CLIENTEDGE;
	dwStyle   = WS_CHILD|WS_VISIBLE|TVS_HASLINES|TVS_EDITLABELS;
	dwStyle  |= WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	// calculate the layout needed for this control
	CalculateLayout(pSuper, (const char *)0, &x, &y, &w, &h);

	// create the native window handle
	hwndRef = CreateWindowExA(dwExStyle, "SysTreeView32", "", dwStyle, x, y, w, h, hwndRef, (HMENU)0, GetModuleHandleA(0), 0);
	if (!hwndRef)
		return (AXWidget *)0;

	// change the font from "system" to the default GUI font
	SendMessageA(hwndRef, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);

	// create and return the widget
	return new AXWidget(AXTreeViewGadgetType, hwndRef, pSuper);
}

// create a list box
AX_FUNC AXWidget *AX_API AXMakeListBox(AXWidget *pSuper, int x, int y, int w, int h)
{
	// declarations
	DWORD	dwExStyle, dwStyle;
	HWND	hwndRef;

	// select the super-window
	if (!(hwndRef=GetSuperHWND(pSuper)))
		return (AXWidget *)0;

	// calculate the style for this control
	dwExStyle = WS_EX_CLIENTEDGE;
	dwStyle   = WS_CHILD|WS_VISIBLE|LBS_HASSTRINGS;
	dwStyle  |= WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	// calculate the layout needed for this control
	CalculateLayout(pSuper, (const char *)0, &x, &y, &w, &h);

	// create the native window handle
	hwndRef = CreateWindowExA(dwExStyle, "ListBox", "", dwStyle, x, y, w, h, hwndRef, (HMENU)0, GetModuleHandleA(0), 0);
	if (!hwndRef)
		return (AXWidget *)0;

	// change the font from "system" to the default GUI font
	SendMessageA(hwndRef, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);

	// create and return the widget
	return new AXWidget(AXListBoxGadgetType, hwndRef, pSuper);
}

// retrieve the handle (HWND) of a window
AX_FUNC void *AX_API AXWindowRef(AXWidget *pWidget)
{
	// validate the widget
	if (!pWidget)
		return (void *)0;
	if (!pWidget->IsWidget())
		return (void *)0;

	// return the window reference
	return (void *)pWidget->WindowRef();
}

// retrieve the context (HDC) of a window
AX_FUNC void *AX_API AXContextRef(AXCanvas *pWidget)
{
	// validate the widget
	if (!pWidget)
		return (void *)0;
	if (pWidget->Type()!=AXCanvasGadgetType)
		return (void *)0;

	// return the context reference
	return (void *)pWidget->ContextRef();
}

// retrieve the message procedure (WNDPROC) of a window
AX_FUNC void *AX_API AXMessageProc(AXWidget *pWidget)
{
	// validate the widget
	if (!pWidget)
		return (void *)0;
	if (pWidget->IsWidget())
		return (void *)0;

	// retrieve the window procedure
	return (void *)pWidget->MessageProc();
}

// find a window by reference; if the window doesn't exist then create it
AX_FUNC AXWidget *AX_API AXFindWidget(void *hwndRef)
{
	// find a widget by HWND; create a handle if one doesn't exist
	return AXWidget::Find(*(HWND *)&hwndRef);
}

// set the title of a widget
AX_FUNC void AX_API AXSetTitle(AXGadget *pWidget, const char *pszTitle)
{
	pWidget->SetTitle(pszTitle);
}

// get the title of a widget
AX_FUNC const char *AX_API AXTitle(AXGadget *pWidget)
{
	return pWidget->Title();
}

// set the position of a widget
AX_FUNC void AX_API AXSetPosition(AXWidget *pWidget, int x, int y, int global)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// change the position
	pWidget->SetPosition(x, y, (global)?true:false);
}

// set the size of a widget
AX_FUNC void AX_API AXSetSize(AXWidget *pWidget, int w, int h, int global)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// change the size
	pWidget->SetSize(w, h, (global)?true:false);
}

// set the shape of a widget
AX_FUNC void AX_API AXSetShape(AXWidget *pWidget, int left, int top, int right, int bottom, int global)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// change the position and size by rectangle
	pWidget->SetShape(left, top, right, bottom, (global)?true:false);
}

// set the minimum size of a widget
AX_FUNC void AX_API AXSetMinimumSize(AXWidget *pWidget, int w, int h)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// specify the minimum size of the widget
	pWidget->SetMinimumSize(w, h);
}

// set the maximum size of a widget
AX_FUNC void AX_API AXSetMaximumSize(AXWidget *pWidget, int w, int h)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// specify the maximum size of the widget
	pWidget->SetMaximumSize(w, h);
}

// retrieve the x position of a widget
AX_FUNC int AX_API AXPositionX(AXWidget *pWidget, int global)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the translation of the widget on the X axis in either global or local space
	return pWidget->PositionX((global)?true:false);
}

// retrieve the y position of a widget
AX_FUNC int AX_API AXPositionY(AXWidget *pWidget, int global)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the translation of the widget on the Y axis in either global or local space
	return pWidget->PositionY((global)?true:false);
}

// retrieve the width of a widget
AX_FUNC int AX_API AXWidth(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the horizontal extent of the widget
	return pWidget->Width(true);
}

// retrieve the height of a widget
AX_FUNC int AX_API AXHeight(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the vertical extent of the widget
	return pWidget->Height(true);
}

// retrieve the width of a widget's client area
AX_FUNC int AX_API AXClientWidth(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the horizontal extent of the widget's internal view
	return pWidget->Width(false);
}

// retrieve the height of a widget's client area
AX_FUNC int AX_API AXClientHeight(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the vertical extent of the widget's internal view
	return pWidget->Height(false);
}

// retrieve the minimum width of a widget
AX_FUNC int AX_API AXMinimumWidth(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the minimum-allowed horizontal extent of the widget's internal view
	return pWidget->MinimumWidth();
}

// retrieve the minimum height of a widget
AX_FUNC int AX_API AXMinimumHeight(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the minimum-allowed vertical extent of the widget's internal view
	return pWidget->MinimumHeight();
}

// retrieve the maximum width of a widget
AX_FUNC int AX_API AXMaximumWidth(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the maximum-allowed horizontal extent of the widget's internal view
	return pWidget->MaximumWidth();
}

// retrieve the maximum height of a widget
AX_FUNC int AX_API AXMaximumHeight(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the maximum-allowed vertical extent of the widget's internal view
	return pWidget->MaximumHeight();
}

// center a widget on the screen or within another widget
AX_FUNC void AX_API AXCenter(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// center the widget
	pWidget->Center();
}

// set whether a widget is released on close
AX_FUNC void AX_API AXSetReleaseOnClose(AXGadget *pWidget, int releaseOnClose)
{
	pWidget->SetReleaseOnClose((releaseOnClose)?true:false);
}

// determine whether a widget is released on close
AX_FUNC int AX_API AXReleaseOnClose(AXGadget *pWidget)
{
	return (pWidget->ReleaseOnClose())?1:0;
}

// retrieve the parent of a widget
AX_FUNC AXGadget *AX_API AXParent(AXGadget *pSub)
{
	return pSub->Parent();
}

// determine whether a particular widget is a descendant of the specified widget
AX_FUNC int AX_API AXIsDescendantOf(AXGadget *pSub, AXGadget *pSuper)
{
	return pSub->IsDescendantOf(pSuper);
}

// set the parent of a widget
AX_FUNC void AX_API AXSetParent(AXGadget *pSub, AXGadget *pSuper)
{
	pSub->SetParent(pSuper);
}

// add a child to a widget
AX_FUNC void AX_API AXAddChild(AXGadget *pSuper, AXGadget *pSub)
{
	pSuper->AddChild(pSub);
}

// retrieve the first child of a widget
AX_FUNC AXGadget *AX_API AXFirstChild(AXGadget *pSuper)
{
	return pSuper->FirstChild();
}

// retrieve the last child of a widget
AX_FUNC AXGadget *AX_API AXLastChild(AXGadget *pSuper)
{
	return pSuper->LastChild();
}

// retrieve the sibling widget before this one
AX_FUNC AXGadget *AX_API AXSiblingBefore(AXGadget *pWidget)
{
	return pWidget->SiblingBefore();
}

// retrieve the sibling widget after this one
AX_FUNC AXGadget *AX_API AXSiblingAfter(AXGadget *pWidget)
{
	return pWidget->SiblingAfter();
}

// retrieve the core widget handle before this one
AX_FUNC AXGadget *AX_API AXHandleBefore(AXGadget *pWidget)
{
	return pWidget->HandleBefore();
}

// retrieve the core widget handle after this one
AX_FUNC AXGadget *AX_API AXHandleAfter(AXGadget *pWidget)
{
	return pWidget->HandleAfter();
}

// retrieve the first root widget
AX_FUNC AXGadget *AX_API AXFirstRoot(void)
{
	return AXGadget::FirstRoot();
}

// retrieve the last root widget
AX_FUNC AXGadget *AX_API AXLastRoot(void)
{
	return AXGadget::LastRoot();
}

// retrieve the first core widget handle
AX_FUNC AXGadget *AX_API AXFirstHandle(void)
{
	return AXGadget::FirstHandle();
}

// retrieve the last core widget handle
AX_FUNC AXGadget *AX_API AXLastHandle(void)
{
	return AXGadget::LastHandle();
}

// make a widget the key widget
AX_FUNC void AX_API AXMakeKey(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// assign the widget key status
	pWidget->MakeKey();
}

// make a widget the key widget and order it to the front (show it)
AX_FUNC void AX_API AXShow(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// show it
	pWidget->Show();
}
// hide a widget
AX_FUNC void AX_API AXHide(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// hide it
	pWidget->Hide();
}

// make a widget the main widget
AX_FUNC void AX_API AXMakeMain(AXWindow *pWidget)
{
	// validate the window
	if (!GadgetIsWindow(pWidget))
		return;

	// make it the main window
	pWidget->MakeMain();
}

// mark a widget for display
AX_FUNC void AX_API AXSetNeedsDisplay(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// mark the widget for needing display
	pWidget->SetNeedsDisplay();
}

// mark a sub-rectangle of a widget for display
AX_FUNC void AX_API AXSetNeedsDisplayInRect(AXWidget *pWidget, int left, int top, int right, int bottom)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// mark a portion of the widget for needing display
	pWidget->SetNeedsDisplay(left, top, right, bottom);
}

// determine whether a widget is the key widget
AX_FUNC int AX_API AXIsKey(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// determine whether the widget was assigned key status
	return (pWidget->IsKey())?1:0;
}

// determine whether a widget is the main widget
AX_FUNC int AX_API AXIsMain(AXWindow *pWidget)
{
	// validate the window
	if (!GadgetIsWindow(pWidget))
		return 0;

	// determine whether the given window is the main window
	return (pWidget->IsMain())?1:0;
}

// simulate a close-button press
AX_FUNC void AX_API AXPerformClose(AXWindow *pWidget)
{
	// validate the window
	if (!GadgetIsWindow(pWidget))
		return;

	// simulate a close-box press
	pWidget->PerformClose();
}

// simulate a zoom-button press
AX_FUNC void AX_API AXPerformZoom(AXWindow *pWidget)
{
	// validate the window
	if (!GadgetIsWindow(pWidget))
		return;

	// simulate a maximize-box press
	pWidget->PerformZoom();
}

// simulate a minify-button press
AX_FUNC void AX_API AXPerformMinify(AXWindow *pWidget)
{
	// validate the window
	if (!GadgetIsWindow(pWidget))
		return;

	// simulate a minimize-box press
	pWidget->PerformMinify();
}

// close a widget
AX_FUNC void AX_API AXClose(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// close it
	pWidget->Close();
}

// maximize/restore a widget
AX_FUNC void AX_API AXZoom(AXWindow *pWidget)
{
	// validate the window
	if (!GadgetIsWindow(pWidget))
		return;

	// maximize it
	pWidget->Zoom();
}

// minimize/restore a widget
AX_FUNC void AX_API AXMinify(AXWindow *pWidget)
{
	// validate the window
	if (!GadgetIsWindow(pWidget))
		return;

	// minimize it
	pWidget->Minify();
}

// determine whether a particular widget is closed
AX_FUNC int AX_API AXIsClosed(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// determine whether it's closed
	return (pWidget->IsClosed())?1:0;
}

// determine whether a particular widget is zoomed
AX_FUNC int AX_API AXIsZoomed(AXWindow *pWidget)
{
	// validate the window
	if (!GadgetIsWindow(pWidget))
		return 0;

	// determine whether it's maximized
	return (pWidget->IsZoomed())?1:0;
}

// determine whether a particular widget is minified
AX_FUNC int AX_API AXIsMinified(AXWindow *pWidget)
{
	// validate the window
	if (!GadgetIsWindow(pWidget))
		return 0;

	// determine whether it's minimized
	return (pWidget->IsMinified())?1:0;
}

// set the edge layout of a gadget
AX_FUNC void AX_API AXSetLayout(AXWidget *pWidget, int layout)
{
	// declarations
	AXGadget	*pSuper;
	int			left, top, right, bottom;

	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return;

	// ensure the widget is a sub-widget (control)
	if (!(pSuper=pWidget->Parent()))
		return;

	// retrieve the client area of the super-widget
	pSuper->QueryClientArea(&left, &top, &right, &bottom);

	// calculate the distances of the widget's edges to the super's corresponding edges
	left = pWidget->PositionX()-left;
	top = pWidget->PositionY()-top;
	right = right-(left+pWidget->Width());
	bottom = bottom-(top+pWidget->Height());

	// save the layout setting and distance settings
	pWidget->SetLayout(layout);
	pWidget->SetLayoutDistance(left, top, right, bottom);

	// request the super-widget to reflow its children based on the new settings
	pSuper->Reflow();
}

// retrieve the edge layout of a gadget
AX_FUNC int AX_API AXLayout(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// retrieve the layout
	return pWidget->GetLayout();
}

// retrieve the left edge layout of a gadget
AX_FUNC int AX_API AXIsLeftEdgeAligned(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// determine whether the widget is left-edge aligned
	return (pWidget->GetLayout()&AXAlignLeft)?1:0;
}

// retrieve the top edge layout of a gadget
AX_FUNC int AX_API AXIsTopEdgeAligned(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// determine whether the widget is top-edge aligned
	return (pWidget->GetLayout()&AXAlignTop)?1:0;
}

// retrieve the right edge layout of a gadget
AX_FUNC int AX_API AXIsRightEdgeAligned(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// determine whether the widget is right-edge aligned
	return (pWidget->GetLayout()&AXAlignRight)?1:0;
}

// retrieve the bottom edge layout of a gadget
AX_FUNC int AX_API AXIsBottomEdgeAligned(AXWidget *pWidget)
{
	// validate the widget
	if (!GadgetIsWidget(pWidget))
		return 0;

	// determine whether the widget is bottom-edge aligned
	return (pWidget->GetLayout()&AXAlignBottom)?1:0;
}

// set a checkbox/radiobox as checked
AX_FUNC void AX_API AXSetChecked(AXWidget *pWidget)
{
	// validate the button
	if (!GadgetIsType(pWidget, AXButtonGadgetType))
		return;

	// change the status to "check"
	SendMessageA(pWidget->WindowRef(), BM_SETCHECK, BST_CHECKED, 0);
}

// set a checkbox/radiobox as unchecked
AX_FUNC void AX_API AXSetUnchecked(AXWidget *pWidget)
{
	// validate the button
	if (!GadgetIsType(pWidget, AXButtonGadgetType))
		return;

	// change the status to "unchecked"
	SendMessageA(pWidget->WindowRef(), BM_SETCHECK, BST_UNCHECKED, 0);
}

// determine whether a checkbox/radiobox is checked
AX_FUNC int AX_API AXIsChecked(AXWidget *pWidget)
{
	// validate the button
	if (!GadgetIsType(pWidget, AXButtonGadgetType))
		return 0;

	// determine whether the button is "checked" (true) or "unchecked" (false)
	return (SendMessageA(pWidget->WindowRef(), BM_GETCHECK, 0, 0)==BST_CHECKED)?1:0;
}
