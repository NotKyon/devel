#ifndef EMBER_ENTITY_H
#define EMBER_ENTITY_H

#include "config.h"
#include "math.h"
#include "transform.h"
#include "scene.h"

#ifndef __scene_t_defined__
#define __scene_t_defined__ 1
typedef struct stScene *scene_t;
#endif

#ifndef __entity_t_defined__
#define __entity_t_defined__ 1
typedef struct stEntity *entity_t;
#endif

struct stEntity {
	void *data[ 2 ];

	struct stEntity *prnt, *head, *tail, *prev, *next;

	struct stScene *scene;
	struct stTransform tform;
};
__em_extrn gc_pool_t g_ent_pool __em_nil();

extern int ent_init( void );
extern void ent_deinit( void );
extern entity_t ent_alloc( entity_t prnt, scene_t scene );
extern entity_t ent_dealloc( entity_t ent );
extern int ent_is_descendant( entity_t ent, entity_t prnt );
extern void ent_set_parent( entity_t ent, entity_t prnt );
extern void ent_set_scene( entity_t ent, scene_t scene );
extern entity_t ent_parent( entity_t ent );
extern entity_t ent_first( entity_t ent );
extern entity_t ent_last( entity_t ent );
extern entity_t ent_before( entity_t ent );
extern entity_t ent_after( entity_t ent );
extern scene_t ent_scene( entity_t ent );
extern void ent_set_data( entity_t ent, void *data );
extern void *ent_data( entity_t ent );
extern void ent_set_sys_data( entity_t ent, void *data );
extern void *ent_sys_data( entity_t ent );

extern tform_t ent_transform( entity_t ent );

extern void ent_translate_ex( entity_t ent, const Vec3 *v );
extern void ent_rotate_ex( entity_t ent, const Vec3 *v );
extern void ent_scale_ex( entity_t ent, const Vec3 *v );
extern void ent_move_ex( entity_t ent, const Vec3 *v );
extern void ent_turn_ex( entity_t ent, const Vec3 *v );

extern void ent_translate( entity_t ent, float x, float y, float z );
extern void ent_rotate( entity_t ent, float x, float y, float z );
extern void ent_scale( entity_t ent, float x, float y, float z );
extern void ent_move( entity_t ent, float x, float y, float z );
extern void ent_turn( entity_t ent, float x, float y, float z );

extern void ent_move_x( entity_t ent, float x );
extern void ent_move_y( entity_t ent, float y );
extern void ent_move_z( entity_t ent, float z );

extern void ent_turn_x( entity_t ent, float x );
extern void ent_turn_y( entity_t ent, float y );
extern void ent_turn_z( entity_t ent, float z );

extern void ent_get_translation_ex( entity_t ent, Vec3 *v );
extern void ent_get_rotation_ex( entity_t ent, Vec3 *v );
extern void ent_get_scale_ex( entity_t ent, Vec3 *v );

extern void ent_get_translation( entity_t ent, float *x, float *y, float *z );
extern void ent_get_rotation( entity_t ent, float *x, float *y, float *z );
extern void ent_get_scale( entity_t ent, float *x, float *y, float *z );

extern float ent_translation_x( entity_t ent );
extern float ent_translation_y( entity_t ent );
extern float ent_translation_z( entity_t ent );

extern float ent_rotation_x( entity_t ent );
extern float ent_rotation_y( entity_t ent );
extern float ent_rotation_z( entity_t ent );

extern float ent_scale_x( entity_t ent );
extern float ent_scale_y( entity_t ent );
extern float ent_scale_z( entity_t ent );

extern Matrix *ent_local_matrix( entity_t ent );
extern Matrix *ent_global_matrix( entity_t ent );

extern void ent_set_local_matrix( entity_t ent, const Matrix *m );
extern void ent_set_global_matrix( entity_t ent, const Matrix *m );

#endif
