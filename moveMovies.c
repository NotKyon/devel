﻿#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

__attribute__( ( noreturn ) ) void fatalError( const char *fmt, ... ) {
	static char buf[ 65536 ];
	va_list args;
	int e;

	e = errno;
	
	va_start( args, fmt );
	snprintf( buf, sizeof( buf ), fmt, args );
	buf[ sizeof( buf ) - 1 ] = '\0';
	va_end( args );
	
	fprintf( stderr, "ERROR: %s: %s [%i]\n", buf, strerror( e ), e );
	exit( EXIT_FAILURE );
}

int main() {
	struct dirent *de;
	char dirname[ 256 ];
	char newpath[ 256 ];
	DIR *dp;
	
	dp = opendir( "." );
	if( !dp ) {
		fatalError( "Failed to open directory" );
	}
	
	while( 1 ) {
		const char *bas;
		const char *ext;

		de = readdir( dp );
		if( !de ) {
			break;
		}
		
		bas = &de->d_name[ 0 ];
		
		printf( "... %s\n", bas );
		
		ext = strrchr( bas, '.' );
		if( !ext ) {
			continue;
		}
		
		if( strcmp( ext, ".mp4" ) != 0 && strcmp( bas, ".avi" ) != 0 &&
		strcmp( bas, ".mkv" ) != 0 ) {
			continue;
		}
		
		snprintf( dirname, sizeof( dirname ), "%.*s", ext - bas, bas );
		dirname[ sizeof( dirname ) - 1 ] = '\0';
		
		snprintf( newpath, sizeof( newpath ), "%s/%s", dirname, bas );
		newpath[ sizeof( newpath ) - 1 ] = '\0';
		
		printf( "Moving \"%s\" -> \"%s\"\n", bas, newpath );
	}

	closedir( dp );
	dp = NULL;

	return EXIT_SUCCESS;
}
