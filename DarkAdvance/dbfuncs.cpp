#include "dbfuncs.h"

GlobStruct *gGlob = (GlobStruct *)0;

//==============================================================================
// CORE
// Initialization routine
void Constructor() {
}
// Deinitialization routine
void Destructor() {
}
/*
// Deinitialization routine (runs first)
void PreDestructor() {
}
*/
// Receive the GlobStruct
void ReceiveCoreDataPtr(void *p) {
	gGlob = (GlobStruct *)p;
}
/*
// Return the DLLs that are required by this plug-in
int GetAssociatedDLLs() {
	// 1 = Basic3D and similar
	return 0;
}
// Retrieve the number of dependencies this DLL has
int GetNumDependencies() {
	return 0;
}
// Retrieve the dependency 'i' this DLL requires
const char *GetDependencyID(int i) {
	return "";
}
// Retrieve the HINSTANCE of a given dependency
void ReceiveDependenciesHinstance(char *dllname, HINSTANCE hinst) {
	if (dllname||hinst) {}
}
*/
/*
void OptionalSecurityCode ( int iInitCode )
{
	// Load Core DLL (get code from core)
	typedef int ( *DLL_CoreCall ) ( void );
	HINSTANCE hDLL = LoadLibrary("DBProCore.dll");
	if ( hDLL )
	{
		DLL_CoreCall g_DLL_GetSecurityCode;
		g_DLL_GetSecurityCode = ( DLL_CoreCall ) GetProcAddress ( hDLL, "?GetSecurityCode@@YAHXZ" );
		if ( g_DLL_GetSecurityCode )
		{
			int iSecurityCode = g_DLL_GetSecurityCode();
			if ( iSecurityCode!=-1 )
			{
				if ( iInitCode==iSecurityCode )
				{
					// if they match, can activate this PLUGIN
					MessageBox ( NULL, "ACTIVATED", "ME", MB_OK );
				}
			}
		}
		FreeLibrary(hDLL);
	}
}
*/
/*
// D3D device was lost
void D3DDeviceLost() {
}
// D3D device was not reset
void D3DDeviceNotReset() {
}
*/

//==============================================================================
// FASTMATH.CPP
int dbFloatSign(float a) {
	return dbFloat(FloatSign(a));
}
int dbFloatAbs(float x) {
	return dbFloat(FloatAbs(x));
}
int dbSqrtFast(float x) {
	return dbFloat(SqrtFast(x));
}
int dbInvSqrtFast(float x) {
	return dbFloat(InvSqrtFast(x));
}
int dbCopySign(float a, float b) {
	return dbFloat(CopySign(a, b));
}

//==============================================================================
// VECTOR.CPP
int dbVec3_Dot(const vec3_t *a, const vec3_t *b) {
	return dbFloat(Vec3_Dot(a, b));
}
int dbVec3_LengthSq(const vec3_t *src) {
	return dbFloat(Vec3_LengthSq(src));
}
int dbVec3_Length(const vec3_t *src) {
	return dbFloat(Vec3_Length(src));
}
int dbVec3_LengthFast(const vec3_t *src) {
	return dbFloat(Vec3_LengthFast(src));
}
int dbVec3_InvLengthFast(const vec3_t *src) {
	return dbFloat(Vec3_InvLengthFast(src));
}
int dbVec3_Distance(const vec3_t *a, const vec3_t *b) {
	return dbFloat(Vec3_Distance(a, b));
}
int dbVec3_DistanceFast(const vec3_t *a, const vec3_t *b) {
	return dbFloat(Vec3_DistanceFast(a, b));
}
int dbVec4_Dot(const vec4_t *a, const vec4_t *b) {
	return dbFloat(Vec4_Dot(a, b));
}
int dbVec4_LengthSq(const vec4_t *src) {
	return dbFloat(Vec4_LengthSq(src));
}
int dbVec4_Length(const vec4_t *src) {
	return dbFloat(Vec4_Length(src));
}
int dbVec4_LengthFast(const vec4_t *src) {
	return dbFloat(Vec4_LengthFast(src));
}
int dbVec4_InvLengthFast(const vec4_t *src) {
	return dbFloat(Vec4_InvLengthFast(src));
}
int dbVec4_Distance(const vec4_t *a, const vec4_t *b) {
	return dbFloat(Vec4_Distance(a, b));
}
int dbVec4_DistanceFast(const vec4_t *a, const vec4_t *b) {
	return dbFloat(Vec4_DistanceFast(a, b));
}

//==============================================================================
// QUATERNION.CPP
int dbQuat_Dot(const quat_t *a, const quat_t *b) {
	return dbFloat(Quat_Dot(a, b));
}
int dbQuat_LengthSq(const quat_t *src) {
	return dbFloat(Quat_LengthSq(src));
}
int dbQuat_Length(const quat_t *src) {
	return dbFloat(Quat_Length(src));
}
int dbQuat_LengthFast(const quat_t *src) {
	return dbFloat(Quat_LengthFast(src));
}
int dbQuat_InvLengthFast(const quat_t *src) {
	return dbFloat(Quat_InvLengthFast(src));
}

//==============================================================================
// VECTORSIMPLIFY.CPP
int dbVec_X(const vec3_t *src) {
	return dbFloat(Vec_X(src));
}
int dbVec_Y(const vec3_t *src) {
	return dbFloat(Vec_Y(src));
}
int dbVec_Z(const vec3_t *src) {
	return dbFloat(Vec_Z(src));
}
int dbVec_W(const vec4_t *src) {
	return dbFloat(Vec_W(src));
}

//==============================================================================
// MATRIXSIMPLIFY.CPP
int dbMatrix_GetElement(const matrix_t *src, unsigned int r, unsigned int c) {
	return dbFloat(Matrix_GetElement(src, r, c));
}
int dbMatrix_11(const matrix_t *src) {
	return dbFloat(Matrix_11(src));
}
int dbMatrix_12(const matrix_t *src) {
	return dbFloat(Matrix_12(src));
}
int dbMatrix_13(const matrix_t *src) {
	return dbFloat(Matrix_13(src));
}
int dbMatrix_14(const matrix_t *src) {
	return dbFloat(Matrix_14(src));
}
int dbMatrix_21(const matrix_t *src) {
	return dbFloat(Matrix_21(src));
}
int dbMatrix_22(const matrix_t *src) {
	return dbFloat(Matrix_22(src));
}
int dbMatrix_23(const matrix_t *src) {
	return dbFloat(Matrix_23(src));
}
int dbMatrix_24(const matrix_t *src) {
	return dbFloat(Matrix_24(src));
}
int dbMatrix_31(const matrix_t *src) {
	return dbFloat(Matrix_31(src));
}
int dbMatrix_32(const matrix_t *src) {
	return dbFloat(Matrix_32(src));
}
int dbMatrix_33(const matrix_t *src) {
	return dbFloat(Matrix_33(src));
}
int dbMatrix_34(const matrix_t *src) {
	return dbFloat(Matrix_34(src));
}
int dbMatrix_41(const matrix_t *src) {
	return dbFloat(Matrix_41(src));
}
int dbMatrix_42(const matrix_t *src) {
	return dbFloat(Matrix_42(src));
}
int dbMatrix_43(const matrix_t *src) {
	return dbFloat(Matrix_43(src));
}
int dbMatrix_44(const matrix_t *src) {
	return dbFloat(Matrix_44(src));
}

//==============================================================================
// QUATERNIONSIMPLIFY.CPP
int dbQuat_W(const quat_t *src) {
	return dbFloat(Quat_W(src));
}
int dbQuat_X(const quat_t *src) {
	return dbFloat(Quat_X(src));
}
int dbQuat_Y(const quat_t *src) {
	return dbFloat(Quat_Y(src));
}
int dbQuat_Z(const quat_t *src) {
	return dbFloat(Quat_Z(src));
}

//==============================================================================
// ENTITY.CPP
char *dbGetEntityName(char *oldstr, entity_t ent) {
	return dbString(oldstr, GetEntityName(ent));
}
char *dbGetEntityText(char *oldstr, entity_t ent) {
	return dbString(oldstr, GetEntityText(ent));
}

//==============================================================================
// ENTITYSIMPLIFY.CPP
int dbEntityPositionX(entity_t ent) {
	return dbFloat(EntityPositionX(ent));
}
int dbEntityPositionY(entity_t ent) {
	return dbFloat(EntityPositionY(ent));
}
int dbEntityPositionZ(entity_t ent) {
	return dbFloat(EntityPositionZ(ent));
}
int dbEntityOffsetPositionX(entity_t ent) {
	return dbFloat(EntityOffsetPositionX(ent));
}
int dbEntityOffsetPositionY(entity_t ent) {
	return dbFloat(EntityOffsetPositionY(ent));
}
int dbEntityOffsetPositionZ(entity_t ent) {
	return dbFloat(EntityOffsetPositionZ(ent));
}
int dbEntityGlobalPositionX(entity_t ent) {
	return dbFloat(EntityGlobalPositionX(ent));
}
int dbEntityGlobalPositionY(entity_t ent) {
	return dbFloat(EntityGlobalPositionY(ent));
}
int dbEntityGlobalPositionZ(entity_t ent) {
	return dbFloat(EntityGlobalPositionZ(ent));
}
int dbEntityPostPositionX(entity_t ent) {
	return dbFloat(EntityPostPositionX(ent));
}
int dbEntityPostPositionY(entity_t ent) {
	return dbFloat(EntityPostPositionY(ent));
}
int dbEntityPostPositionZ(entity_t ent) {
	return dbFloat(EntityPostPositionZ(ent));
}
int dbCalculateEntityScaleX(entity_t ent) {
	return dbFloat(CalculateEntityScaleX(ent));
}
int dbCalculateEntityScaleY(entity_t ent) {
	return dbFloat(CalculateEntityScaleY(ent));
}
int dbCalculateEntityScaleZ(entity_t ent) {
	return dbFloat(CalculateEntityScaleZ(ent));
}
int dbCalculateEntityOffsetScaleX(entity_t ent) {
	return dbFloat(CalculateEntityOffsetScaleX(ent));
}
int dbCalculateEntityOffsetScaleY(entity_t ent) {
	return dbFloat(CalculateEntityOffsetScaleY(ent));
}
int dbCalculateEntityOffsetScaleZ(entity_t ent) {
	return dbFloat(CalculateEntityOffsetScaleZ(ent));
}
int dbCalculateEntityGlobalScaleX(entity_t ent) {
	return dbFloat(CalculateEntityGlobalScaleX(ent));
}
int dbCalculateEntityGlobalScaleY(entity_t ent) {
	return dbFloat(CalculateEntityGlobalScaleY(ent));
}
int dbCalculateEntityGlobalScaleZ(entity_t ent) {
	return dbFloat(CalculateEntityGlobalScaleZ(ent));
}
int dbCalculateEntityPostScaleX(entity_t ent) {
	return dbFloat(CalculateEntityPostScaleX(ent));
}
int dbCalculateEntityPostScaleY(entity_t ent) {
	return dbFloat(CalculateEntityPostScaleY(ent));
}
int dbCalculateEntityPostScaleZ(entity_t ent) {
	return dbFloat(CalculateEntityPostScaleZ(ent));
}
int dbEntityRotationX(entity_t ent) {
	return dbFloat(EntityRotationX(ent));
}
int dbEntityRotationY(entity_t ent) {
	return dbFloat(EntityRotationY(ent));
}
int dbEntityRotationZ(entity_t ent) {
	return dbFloat(EntityRotationZ(ent));
}
int dbEntityOffsetRotationX(entity_t ent) {
	return dbFloat(EntityOffsetRotationX(ent));
}
int dbEntityOffsetRotationY(entity_t ent) {
	return dbFloat(EntityOffsetRotationY(ent));
}
int dbEntityOffsetRotationZ(entity_t ent) {
	return dbFloat(EntityOffsetRotationZ(ent));
}
int dbEntityGlobalRotationX(entity_t ent) {
	return dbFloat(EntityGlobalRotationX(ent));
}
int dbEntityGlobalRotationY(entity_t ent) {
	return dbFloat(EntityGlobalRotationY(ent));
}
int dbEntityGlobalRotationZ(entity_t ent) {
	return dbFloat(EntityGlobalRotationZ(ent));
}
int dbEntityPostRotationX(entity_t ent) {
	return dbFloat(EntityPostRotationX(ent));
}
int dbEntityPostRotationY(entity_t ent) {
	return dbFloat(EntityPostRotationY(ent));
}
int dbEntityPostRotationZ(entity_t ent) {
	return dbFloat(EntityPostRotationZ(ent));
}
