#pragma once

bool InitWindow();
void FiniWindow();

unsigned GetWindowResX();
unsigned GetWindowResY();

bool ProcessWindowEvents();

#ifdef _WIN32
# undef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN
# include <Windows.h>
# undef min
# undef max
HWND GetWindowHandle_Win32();
#endif
