#ifndef MEMORY_H
#define MEMORY_H

#include "export.h"
#include <stddef.h>

inline void *AddPtr(void *p, size_t i) {
	union { size_t i; void *p; } v;

	v.p  = p;
	v.i += i;

	return v.p;
}

EXPORT void *NewMemory(size_t n);
EXPORT void *DeleteMemory(void *p);
EXPORT void *RenewMemory(void *p, size_t size);

EXPORT void PokeInt8(void *p, size_t offset, unsigned char v);
EXPORT void PokeInt16(void *p, size_t offset, unsigned short v);
EXPORT void PokeInt32(void *p, size_t offset, unsigned int v);

EXPORT int PeekInt8(void *p, size_t offset);
EXPORT int PeekInt16(void *p, size_t offset);
EXPORT int PeekInt32(void *p, size_t offset);

#endif
