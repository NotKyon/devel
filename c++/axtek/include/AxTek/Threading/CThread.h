#ifndef AXTEK_THREADING_CTHREAD_H
#define AXTEK_THREADING_CTHREAD_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

__AX_TASK_NS_INIT__

//
//	TODO: Improve this interface. The improvement will cause code to break.
//

class CThread {
protected:
#if _WIN32
	HANDLE m_handle;
	DWORD m_affinity;
#else
	pthread_t m_handle;
#endif
	threadFunc_t m_func;
	void *m_parm;

	bool m_joinOnKill;

	inline void _Nullify() {
		m_func = (threadFunc_t)NULL;
		m_parm = (void *)NULL;
		m_joinOnKill = true;

#if _WIN32
		m_handle = (HANDLE)NULL;
		m_affinity = 0xFFFFFFFF;
#else
		m_handle = (pthread_t)NULL;
#endif
	}

public:
	inline CThread() {
		_Nullify();
	}
	inline CThread(threadFunc_t fn, void *p=(void *)NULL) {
		_Nullify();
		Init(fn, p);
	}
	inline ~CThread() {
		Kill();
	}

	inline bool Init(threadFunc_t func, void *parm=(void *)NULL) {
		if (m_func)
			return false;

#if _WIN32
		m_handle = CreateThread(0, 0, func, parm, 0, 0);
		if __unlikely (!m_handle)
			return false;
#else
		memset(&m_handle, 0, sizeof(m_handle));
		if (pthread_create(&m_handle, (const pthread_attr_t *)NULL, func,
		parm)!=0)
			return false;
#endif

		m_func = func;
		m_parm = parm;

		return true;
	}

	inline result_t Join() {
		result_t r;

#if _WIN32
		WaitForSingleObject(m_handle, INFINITE);
		if (!GetExitCodeThread(m_handle, &r))
			r = (DWORD)-1;
#else
		if (pthread_join(m_handle, &r) != 0)
			r = (void *)-1;
#endif

		return r;
	}
	inline void Kill() {
		if (m_joinOnKill)
			Join();

#if _WIN32
		CloseHandle(m_handle);
		m_handle = (HANDLE)NULL;
#else
		memset((void *)&m_handle, 0, sizeof(m_handle));
#endif
	}

	inline void SetAffinity(u32 mask) {
#if _WIN32
		SetThreadAffinityMask(m_handle, mask);
		m_affinity = mask;
#else
		/* TODO */
#endif
	}
	inline u32 GetAffinity() {
#if _WIN32
		return m_affinity;
#else
		/* TODO */
		return 0xFFFFFFFF;
#endif
	}
};

static inline void yield() {
#if _WIN32
	Sleep(0);
#else
	sched_yield(); //use nanosleep() instead?
#endif
}

__AX_TASK_NS_FINI__

#endif
