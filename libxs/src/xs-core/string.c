#include <xs/string.h>
#include <xs/memory.h>
#include <xs/logging.h>
#include <string.h>

char *XsDuplicate(const char *source) {
	size_t n;
	char *p;

	if (!source)
		return (char *)0;

	n = strlen(source) + 1;
	p = (char *)XsMemory((void *)0, n);

	memcpy((void *)p, (const void *)source, n);
	return p;
}

char *XsReduplicate(char *p, const char *source) {
	size_t n;

	if (!p)
		return XsDuplicate(source);

	if (!source)
		return (char *)XsMemory((void *)p, 0);

	n = strlen(source) + 1;

	p = (char *)XsMemory((void *)p, n);

	memcpy((void *)p, (const void *)source, n);
	return p;
}

char *XsAppend(char *p, const char *source) {
	size_t l, n;

	if (!p)
		return XsDuplicate(source);

	l = strlen(p);
	n = strlen(source) + 1;

	p = (char *)XsMemory((void *)p, l + n);

	memcpy(&p[l], (const void *)source, n);
	return p;
}

char *XsStrCpy(char *dst, xs_size_t dstn, const char *src) {
	return XsStrCpyN(dst, dstn, src, 0);
}

char *XsStrCpyN(char *dst, xs_size_t dstn, const char *src, xs_size_t srcn) {
	xs_size_t l;
	char *r;

	if (!dst) {
		XsLogMessage(0, 0, "XsStrCpy", kXsLog_Error,
			"NULL destination string passed.");
		return (char *)0;
	}

	if (!src) {
		XsLogMessage(0, 0, "XsStrCpy", kXsLog_Error,
			"NULL source string passed.");
		return (char *)0;
	}

	if (!dstn) {
		XsLogMessage(0, 0, "XsStrCpy", kXsLog_Error,
			"No room in destination string.");
		return (char *)0;
	}

	l = srcn ? srcn : strlen(src);

	if (l + 1 > dstn) {
		l = dstn - 1;
		r = (char *)0;

		XsLogMessage(0, 0, __func__, kXsLog_Warning,
			"Overflow prevented.");
	} else
		r = dst;

	if (l)
		memcpy((void *)dst, (const void *)src, l);
	dst[l] = 0;

	return r;
}

char *XsStrCat(char *dst, xs_size_t dstn, const char *src) {
	return XsStrCatN(dst, dstn, src, 0);
}
char *XsStrCatN(char *dst, xs_size_t dstn, const char *src, xs_size_t srcn) {
	char *p;

	p = strchr(dst, '\0');

	p = XsStrCpyN(p, dstn - (p - dst), src, srcn);
	if (p != (char *)0)
		p = dst;

	return p;
}
