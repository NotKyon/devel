#include <math.h>
#include <stdio.h>

void test(int i, int j) {
	const char *s;
	double x;
	int k;

	k = i + j;

	x = pow(-1.0, (double)k);

	s = x > 0 ? "+" : "";

	printf("pow(-1, (%i + %i)) = pow(-1, %.2i) = %s%.0f\n", i, j, k, s, x);
}

int main() {
	int i, j;

	for(i=1; i<=6; i++)
		for(j=1; j<=6; j++)
			test(i, j);

	return 0;
}
