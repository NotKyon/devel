
/*
 * These bits (used in Entity.tform_flags) define what NEEDS to be updated.
 */
#define ENT_TFORM_LWORLD_BIT	0x01
/*#define ENT_TFORM_LIWRLD_BIT	0x02*/
#define ENT_TFORM_GWORLD_BIT	0x04
#define ENT_TFORM_GIWRLD_BIT	0x08
#define ENT_TFORM_LOCAL_MASK	(ENT_TFORM_LWORLD_BIT/*|ENT_TFORM_LIWRLD_BIT*/)
#define ENT_TFORM_GLOBL_MASK	(ENT_TFORM_GWORLD_BIT|ENT_TFORM_GIWRLD_BIT)
#define ENT_TFORM_DIRTY_MASK	(ENT_TFORM_LOCAL_MASK|ENT_TFORM_GLOBL_MASK)

typedef struct Entity {

	struct Entity	*prnt,
					*prev, *next,
					*head, *tail;

	Matrix			l_world, /*l_worldinv,*/ g_world, g_worldinv;
	bitfield_t		tform_flags;

} *entity_t;

static void __ent_invalidate_g(entity_t ent) {
	entity_t node;

	for(node=ent->head; node; node=node->next) {
		/*
		 * No global bit can be set if any local bits are also set. The local
		 * changes must take effect before trying to mark the entity for global
		 * changes.
		 */
		if (node->tform_flags & ENT_TFORM_LOCAL_MASK)
			__ent_update(ent);
	}
}
static void __ent_update(entity_t ent) {

	if (ent->tform_flags&ENT_TFORM_UPDATE_MASK==0)
		return;

	if (ent->prnt)
		__ent_update(ent->prnt);

	if (ent->tform_flags&ENT_TFORM_GWORLD_BIT) {
		if (ent->prnt)
			mat_multiply34(&ent->g_world, &ent->prnt->g_world, &ent->l_world);
		else
			memcpy(&ent->g_world, &ent->l_world, sizeof(Matrix));

		ent->tform_flags &= ~ENT_TFORM_GWORLD_BIT;
		ent->tform_flags |=  ENT_TFORM_GIWRLD_BIT;

		__ent_invalidate_g(ent);
	}

	if (ent->tform_flags&ENT_TFORM_GIWRLD_BIT) {
		mat_inverse_world(&ent->g_worldinv, &ent->g_world);

		ent->tform_flags &= ~ENT_TFORM_GIWRLD_BIT;
	}

	if (ent->tform_flags&ENT_TFORM_LWORLD_BIT) {
		if (ent->prnt)
			mat_multiply34(&ent->l_world, &ent->prnt->g_worldinv, &ent->g_world);
		else
			memcpy(&ent->l_world, &ent->g_world);

		ent->tform_flags &= ~ENT_TFORM_LWORLD_BIT;
	}

}

void ent_position_ex(entity_t ent, const Vec3 *v) {

	__ent_update(ent);

	mat_translate_ex(&ent->l_world, v);

	ent->tform_flags |= ENT_TFORM_GLOBL_MASK;

}

void ent_rotate_ex(entity_t ent, const Vec3 *v) {
	Matrix tmp, rotx, roty, rotz;

	__ent_update(ent);

	mat_rotation3_x_fast(&rotx, v->x);
	mat_rotation3_y_fast(&roty, v->y);
	mat_rotation3_z_fast(&rotz, v->z);
	mat_multiply3(&tmp, &rotz, &rotx);
	mat_multiply3(&ent->l_world, &tmp, &roty);

	ent->tform_flags |= ENT_TFORM_GLOBL_MASK;

}

void ent_turn_ex(entity_t ent, const Vec3 *v) {
	Matrix wrld, rot, tmp, rotx, roty, rotz;

	__ent_update(ent);

	mat_rotation3_x_fast(&rotx, v->x);
	mat_rotation3_y_fast(&roty, v->y);
	mat_rotation3_z_fast(&rotz, v->z);
	mat_multiply3(&tmp, &rotz, &rotx);
	mat_multiply3(&rot, &tmp, &roty);

	mat_multiply3(&wrld, &ent->l_world, &rot);
	mat_copy3(&ent->l_world, &wrld);

}

void ent_scale_ex(entity_t ent, const Vec3 *v) {
	Matrix scale;

	s

}

void ent_turn_x(entity_t ent, float f) {
	Matrix tmp, rotx;

	__ent_update(ent);

	mat_rotation3_x_fast(&rotx, f);
	mat_multiply3(&tmp, &ent->l_world, &rotx);
	mat_copy3(&ent->l_world, &tmp);

	ent->tform_flags |= ENT_TFORM_GLOBL_MASK;
}
