#include <math.h>
#include <stdio.h>
#include <stdlib.h>

unsigned int floatToUintBits( float f )
{
	union
	{
		float f;
		unsigned int u;
	} x;

	x.f = f;
	return x.u;
}
float uintBitsToFloat( unsigned int u )
{
	union
	{
		float f;
		unsigned int u;
	} x;

	x.u = u;
	return x.f;
}
const char *getBinary( unsigned int x )
{
	static char buffer[ 8*( sizeof( x )*8 + 1) ];
	static unsigned int buffer_i = 0;

	char *buffer_p = &buffer[ buffer_i + sizeof( x )*8 ];
	buffer_i = ( buffer_i + sizeof( x )*8 + 1 )%sizeof( buffer );

	*buffer_p = '\0';

	do
	{
		*--buffer_p = '0' + ( char )( x%2 );
		x /= 2;
	} while( x > 0 );

	return buffer_p;
}
float myFloor( float x )
{
	if( ( floatToUintBits( x ) & 0x801FFFFF ) > 0x80000000 )
	{
		return ( float )( ( int )x - 1 );
	}

	return ( float )( int )x;
}
void test( float x )
{
	const float a = floorf( x );
	const float b = myFloor( x );

	const unsigned int xi = floatToUintBits( x );
	const unsigned int ai = floatToUintBits( a );
	const unsigned int bi = floatToUintBits( b );

	printf( "floor(%g) -> %g, %g\n", x, a, b );
	if( ai == bi )
	{
		return;
	}

	printf( "  0x%.8X -> 0x%.8X, 0x%.8X\n", xi, ai, bi );
	printf( "  x:%s\n  a:%s\n  b:%s\n",
		getBinary( xi ), getBinary( ai ), getBinary( bi ) );
}

int main()
{
	for( float f = -10.0f; f <= 10.1f; f += 0.1f )
	{
		test( f );
	}

	return EXIT_SUCCESS;
}
