//
//	2D Platformer Engine
//	Written by Aaron J. Miller, 2012
//

#include <GL/glfw.h>
#include <GL/gl.h>
#include <GL/glext.h>

#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#define VECTOR_SIZE 16
#define CACHELINE_SIZE 64

#if 1
# if _MSC_VER || __INTEL_COMPILER
#  define VECTOR_ALIGNED __declspec(align(VECTOR_SIZE))
#  define CACHELINE_ALIGNED __declspec(align(CACHELINE_SIZE))
# else
#  define VECTOR_ALIGNED __attribute__((aligned(VECTOR_SIZE)))
#  define CACHELINE_ALIGNED __attribute__((aligned(CACHELINE_SIZE)))
# endif
#else
# define VECTOR_ALIGNED
# define CACHELINE_ALIGNED
#endif

#if _MSC_VER || __INTEL_COMPILER
typedef   signed __int64 sint64;
typedef unsigned __int64 uint64;
#else
# include <stdint.h>
typedef  int64_t sint64;
typedef uint64_t uint64;
#endif

//------------------------------------------------------------------------------

class CEngine {
public:
	CEngine();
	~CEngine();

	void Init();
	void Fini();

	bool IsRunning() const;

protected:
	bool mDidInit;
	bool mIsRunning;
} Engine;

CEngine::CEngine() {
	mDidInit = false;
	mIsRunning = false;
}
CEngine::~CEngine() {
	Fini();
}

void CEngine::Init() {
	if (mDidInit)
		return;

	glfwInit();

	if (!glfwOpenWindow(640, 480, 8, 8, 8, 8, 0, 0, GLFW_WINDOW))
		exit(EXIT_FAILURE);

	mDidInit = true;
	mIsRunning = true;
}
void CEngine::Fini() {
}

bool CEngine::IsRunning() const {
	return mIsRunning;
}
