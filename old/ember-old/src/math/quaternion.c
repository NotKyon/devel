#include <ember/math.h>
#include <math.h>

#define __max__( a, b ) ( ( a ) > ( b ) ? ( a ) : ( b ) )

#ifdef _MSC_VER
#define __copysign__ _copysign
#else
#ifdef __GNUC__
#define __copysign_api static __inline __attribute__( ( always_inline ) )
#else
#define __copysign_api static
#endif
__copysign_api float __copysign__( float a, float b )
{
	union {
		unsigned int u;
		float f;
	} _a, _b;
	_a.f = a;
	_b.f = b;
	_a.u = ( _a.u & 0x7FFFFFFF ) | ( _b.u & 0x80000000 );
	return _a.f;
}
#endif

float quat_w( const Quaternion *src )
{
	return src->w;
}
float quat_x( const Quaternion *src )
{
	return src->x;
}
float quat_y( const Quaternion *src )
{
	return src->y;
}
float quat_z( const Quaternion *src )
{
	return src->z;
}

void quat_set( Quaternion *dst, float w, float x, float y, float z )
{
	dst->w = w;
	dst->x = x;
	dst->y = y;
	dst->z = z;
}

void quat_identity( Quaternion *dst )
{
	dst->w = 1;
	dst->x = 0;
	dst->y = 0;
	dst->z = 0;
}
void quat_copy( Quaternion *dst, const Quaternion *src )
{
	dst->w = src->w;
	dst->x = src->x;
	dst->y = src->y;
	dst->z = src->z;
}

void quat_multiply( Quaternion *dst, const Quaternion *a, const Quaternion *b )
{
	dst->w = a->w * b->w - a->x * b->x - a->y * b->y - a->z * b->z;
	dst->x = a->w * b->x + a->x * b->w + a->y * b->z - a->z * b->y;
	dst->y = a->w * b->y - a->x * b->z + a->y * b->w + a->z * b->x;
	dst->z = a->w * b->z + a->x * b->y - a->y * b->x + a->z * b->w;
}

float quat_dot( const Quaternion *a, const Quaternion *b )
{
	return a->w * b->w + a->x * b->x + a->y * b->y + a->z * b->z;
}

void quat_axis( Quaternion *dst, float angle, float x, float y, float z )
{
	float t, s;

	t = 0.5 * angle;
	s = sinf( t );

	dst->w = cosf( t );
	dst->x = s * x;
	dst->y = s * y;
	dst->z = s * z;
}
void quat_axis_fast( Quaternion *dst, float angle, float x, float y, float z )
{
	float t, s;

	t = 0.5 * angle;
	s = sin_fast( t );

	dst->w = cos_fast( t );
	dst->x = s * x;
	dst->y = s * y;
	dst->z = s * z;
}

void quat_rotation( Quaternion *dst, const Matrix *src )
{
#if 0
	float d, s, t, r;
	int m;

	t = src->rm._11 + src->rm._22 + src->rm._33 + 1;

	if (t > 1) {
		r = sqrtf(t);
		s = 2 * r;

		dst->w = 0.5 * r;
		dst->x = (src->rm._23 - src->rm._32) / s;
		dst->y = (src->rm._31 - src->rm._13) / s;
		dst->z = (src->rm._12 - src->rm._21) / s;

		return;
	}

	d = src->rm._11;
	m = 0;

	if (src->rm._22 > d) {
		m = 1;
		d = src->rm._22;
	}

	if (src->rm._33 > d) {
		m = 2;
		d = src->rm._33;
	}

	switch (m) {
	case 0:
		s = 2 * sqrtf(1 + src->rm._11 - src->rm._22 - src->rm._33);

		dst->w = (src->rm._23 - src->rm._21) / s;
		dst->x = 0.25 * s;
		dst->y = (src->rm._12 + src->rm._21) / s;
		dst->z = (src->rm._13 + src->rm._31) / s;

		return;

	case 1:
		s = 2 * sqrtf(1 + src->rm._22 - src->rm._11 - src->rm._33);

		dst->w = (src->rm._31 - src->rm._13) / s;
		dst->x = (src->rm._12 + src->rm._21) / s;
		dst->y = 0.25 * s;
		dst->z = (src->rm._23 + src->rm._32) / s;

		return;

	default:     /* 2 */
		s = 2 * sqrtf(1 + src->rm._33 - src->rm._11 - src->rm._22);

		dst->w = (src->rm._12 - src->rm._21) / s;
		dst->x = (src->rm._13 + src->rm._31) / s;
		dst->y = (src->rm._23 + src->rm._32) / s;
		dst->z = 0.25 * s;

		return;
	}
#else
	dst->w =
	    0.5f * sqrtf( __max__( 0, 1 + src->rm._11 + src->rm._22 + src->rm._33 ) );
	dst->x =
	    0.5f * sqrtf( __max__( 0, 1 + src->rm._11 - src->rm._22 - src->rm._33 ) );
	dst->y =
	    0.5f * sqrtf( __max__( 0, 1 - src->rm._11 + src->rm._22 - src->rm._33 ) );
	dst->z =
	    0.5f * sqrtf( __max__( 0, 1 - src->rm._11 - src->rm._22 + src->rm._33 ) );
	dst->x = __copysign__( dst->x, src->rm._32 - src->rm._23 );
	dst->y = __copysign__( dst->y, src->rm._13 - src->rm._31 );
	dst->z = __copysign__( dst->z, src->rm._21 - src->rm._12 );
#endif
}
void quat_rotation_fast( Quaternion *dst, const Matrix *src )
{
#if 0
	float d, s, t, r;
	int m;

	t = src->rm._11 + src->rm._22 + src->rm._33 + 1;

	if (t > 1) {
		r = sqrt_fast(t);
		s = 2 * r;

		dst->w = 0.5 * r;
		dst->x = (src->rm._23 - src->rm._32) / s;
		dst->y = (src->rm._31 - src->rm._13) / s;
		dst->z = (src->rm._12 - src->rm._21) / s;

		return;
	}

	d = src->rm._11;
	m = 0;

	if (src->rm._22 > d) {
		m = 1;
		d = src->rm._22;
	}

	if (src->rm._33 > d) {
		m = 2;
		d = src->rm._33;
	}

	switch (m) {
	case 0:
		s = 2 * sqrt_fast(1 + src->rm._11 - src->rm._22 - src->rm._33);

		dst->w = (src->rm._23 - src->rm._21) / s;
		dst->x = 0.25 * s;
		dst->y = (src->rm._12 + src->rm._21) / s;
		dst->z = (src->rm._13 + src->rm._31) / s;

		return;

	case 1:
		s = 2 * sqrt_fast(1 + src->rm._22 - src->rm._11 - src->rm._33);

		dst->w = (src->rm._31 - src->rm._13) / s;
		dst->x = (src->rm._12 + src->rm._21) / s;
		dst->y = 0.25 * s;
		dst->z = (src->rm._23 + src->rm._32) / s;

		return;

	default:     /* 2 */
		s = 2 * sqrt_fast(1 + src->rm._33 - src->rm._11 - src->rm._22);

		dst->w = (src->rm._12 - src->rm._21) / s;
		dst->x = (src->rm._13 + src->rm._31) / s;
		dst->y = (src->rm._23 + src->rm._32) / s;
		dst->z = 0.25 * s;

		return;
	}
#else
	dst->w =
	    0.5f * sqrt_fast( __max__( 0, 1 + src->rm._11 + src->rm._22 + src->rm._33 ) );
	dst->x =
	    0.5f * sqrt_fast( __max__( 0, 1 + src->rm._11 - src->rm._22 - src->rm._33 ) );
	dst->y =
	    0.5f * sqrt_fast( __max__( 0, 1 - src->rm._11 + src->rm._22 - src->rm._33 ) );
	dst->z =
	    0.5f * sqrt_fast( __max__( 0, 1 - src->rm._11 - src->rm._22 + src->rm._33 ) );
	dst->x = __copysign__( dst->x, src->rm._32 - src->rm._23 );
	dst->y = __copysign__( dst->y, src->rm._13 - src->rm._31 );
	dst->z = __copysign__( dst->z, src->rm._21 - src->rm._12 );
#endif
}

void quat_slerp( Quaternion *dst, const Quaternion *a, const Quaternion *b,
    float w )
{
	float d, e, ew;

	w = 1 - w;

	d  = quat_dot( a, b );
	e  = ( d < 0 ) ? -1 : 1;
	ew = e * w;

	dst->w = w * a->w + ew * b->w;
	dst->x = w * a->x + ew * b->x;
	dst->y = w * a->y + ew * b->y;
	dst->z = w * a->z + ew * b->z;
}

void quat_rotate_ex( Quaternion *dst, const Vec3 *src )
{
	Quaternion qx, qy, qz, tmp;

	quat_axis( &qx, src->x, 1, 0, 0 );
	quat_axis( &qy, src->y, 0, 1, 0 );
	quat_axis( &qz, src->z, 0, 0, 1 );

	quat_multiply( &tmp, &qx, &qy );
	quat_multiply( dst, &tmp, &qz );
}
void quat_rotate( Quaternion *dst, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	quat_rotate_ex( dst, &v );
}
void quat_rotate_fast_ex( Quaternion *dst, const Vec3 *src )
{
	Quaternion qx, qy, qz, tmp;

	quat_axis_fast( &qx, src->x, 1, 0, 0 );
	quat_axis_fast( &qy, src->y, 0, 1, 0 );
	quat_axis_fast( &qz, src->z, 0, 0, 1 );

	quat_multiply( &tmp, &qx, &qy );
	quat_multiply( dst, &tmp, &qz );
}
void quat_rotate_fast( Quaternion *dst, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	quat_rotate_fast_ex( dst, &v );
}

void quat_conjugate( Quaternion *dst, const Quaternion *src )
{
	dst->w = src->w;
	dst->x = -src->x;
	dst->y = -src->y;
	dst->z = -src->z;
}

float quat_length_sqr( const Quaternion *src )
{
	return src->w * src->w + src->x * src->x + src->y * src->y + src->z * src->z;
}
float quat_length( const Quaternion *src )
{
	return sqrtf( quat_length_sqr( src ) );
}
float quat_length_fast( const Quaternion *src )
{
	return sqrt_fast( quat_length_sqr( src ) );
}

void quat_normalize( Quaternion *dst, const Quaternion *src )
{
	float r;

	r = 1 / quat_length( src );

	dst->w = src->w * r;
	dst->x = src->x * r;
	dst->y = src->y * r;
	dst->z = src->z * r;
}
void quat_normalize_fast( Quaternion *dst, const Quaternion *src )
{
	float r;

	r = invsqrt_fast( quat_length_sqr( src ) );

	dst->w = src->w * r;
	dst->x = src->x * r;
	dst->y = src->y * r;
	dst->z = src->z * r;
}

void quat_inverse( Quaternion *dst, const Quaternion *src )
{
	quat_conjugate( dst, src );
	quat_normalize( dst, dst );
}
void quat_inverse_fast( Quaternion *dst, const Quaternion *src )
{
	quat_conjugate( dst, src );
	quat_normalize_fast( dst, dst );
}
