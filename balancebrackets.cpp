﻿/*

	Implements parenthesis/curly-brace/square-bracket balance checking.

	Has an arbitrary value stack that makes efficient use of integer space when
	the value range covers only a few bits. (e.g., only values of 0..2 are
	used, as is the case here.)

	Has a TCountDivs<> template (should be mostly pre-C++11 compatible) that
	calculates the number of divisions it would take with a given denominator
	and an initial given numerator to reach 0. (e.g., `TCountDivs<9,3>::count`
	should be 3. Because @i=0:9/3=3; @i=1:3/3=1; @i=2:1/3=0.)

	Compile:
	g++ -W -Wall -pedantic -std=gnu++14 -o balancebrackets balancebrackets.cpp
	
	Run:
	./balancebrackets

	Stress test:
	./balancebrackets 
	ACBCCCCCCCACCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCACBCACBCAC
	cacbcacbcacccccccccccccccccccccccccccccccccccccccccccccccccacccccccbca

		^ the above should be all one line, without any spacing between one line
		` and the next (except for the initial "./balancebrackets ")

	NOTE: A=( a=)
	`     B=[ b=]
	`     C={ c=}

*/

#include <stdio.h>
#include <stdlib.h>

#include <vector>

typedef void Void;
typedef bool Bool;
typedef unsigned long long int U64;

namespace Detail
{

	typedef unsigned long long int ValueStoreType;
	
	template< typename TArrType >
	struct TValueStackMutArrAdapter
	{
		static inline Bool isEmpty( const TArrType &x )
		{
			return x.isEmpty();
		}
		static inline Bool append( TArrType &x, ValueStoreType v )
		{
			return x.append( v );
		}
		static inline Void removeLast( TArrType &x )
		{
			return x.removeLast();
		}
		static inline ValueStoreType &last( TArrType &x )
		{
			return x.last();
		}
		static inline const ValueStoreType &last( const TArrType &x )
		{
			return x.last();
		}
	};

	template<>
	struct TValueStackMutArrAdapter< std::vector< ValueStoreType > >
	{
		typedef std::vector< ValueStoreType > TArrType;

		static inline Bool isEmpty( const TArrType &x )
		{
			return x.empty();
		}
		static inline Bool append( TArrType &x, ValueStoreType v )
		{
			x.push_back( v );
			return true;
		}
		static inline Void removeLast( TArrType &x )
		{
			x.erase( x.end() - 1 );
		}
		static inline ValueStoreType &last( TArrType &x )
		{
			return x[ x.size() - 1 ];
		}
		static inline const ValueStoreType &last( const TArrType &x )
		{
			return x[ x.size() - 1 ];
		}
	};

	template< unsigned tDenom, U64 tMaxValue = ~U64(0) - ( tDenom - 1 ), unsigned tIter = 0 >
	struct TCountDivs
	{
		static_assert( tDenom > 0, "Division by zero" );

		static const unsigned count =
			( tMaxValue >= tDenom )
			? TCountDivs< tDenom, tMaxValue/tDenom, tIter + 1 >::count
			: tIter;
	};
	template< unsigned tDenom, unsigned tIter >
	struct TCountDivs< tDenom, 0, tIter >
	{
		static const unsigned count = tIter;
	};
	
}

template< unsigned tDenom, U64 tMaxValue = ~U64(0) >
struct TCountDivs: public Detail::TCountDivs< tDenom, tMaxValue, 0 >
{
};

template< unsigned tHighestValue, class TMutArr = std::vector< Detail::ValueStoreType > >
class TValueStack
{
public:
	typedef Detail::ValueStoreType ItemType;
	typedef unsigned               SizeType;
	typedef unsigned               ValueType;
	typedef TMutArr                MutArrType;

	static const unsigned kValueRange    = tHighestValue;
	static const unsigned kValuesPerItem = TCountDivs< kValueRange, ~ItemType(0) >::count;

	inline TValueStack()
	: m_cValues( 0 )
	, m_mainValue( 0 )
	, m_extraValues()
	{
	}
	inline ~TValueStack()
	{
	}

	inline Bool push( ValueType value )
	{
		if( value >= kValueRange || m_cValues == ~SizeType( 0 ) ) {
			return false;
		}

		if( ( m_cValues + 1 )/kValuesPerItem > m_cValues/kValuesPerItem ) {
			if( !appendExtra( 0 ) ) {
				return false;
			}
		}

		ItemType &n = last();
		n *= kValueRange;
		n += value;

		++m_cValues;

		return true;
	}
	inline Bool pop( ValueType &dstValue )
	{
		if( !m_cValues ) {
			return false;
		}

		Detail::ValueStoreType &n = last();

		dstValue = n%kValueRange;
		n /= kValueRange;

		if( ( m_cValues - 1 )/kValuesPerItem < m_cValues/kValuesPerItem ) {
			removeLastExtra();
		}

		--m_cValues;
		return true;
	}

	inline SizeType num() const
	{
		return m_cValues;
	}
	inline SizeType len() const
	{
		return m_cValues;
	}
	inline Bool isEmpty() const
	{
		return m_cValues == 0;
	}
	inline Bool isUsed() const
	{
		return m_cValues != 0;
	}

private:
	typedef Detail::TValueStackMutArrAdapter< TMutArr > MutArrAdapter;
	
	SizeType              m_cValues;
	ItemType              m_mainValue;
	MutArrType            m_extraValues;

	inline Bool isExtraEmpty() const
	{
		return MutArrAdapter::isEmpty( m_extraValues );
	}
	inline Bool appendExtra( Detail::ValueStoreType v )
	{
		printf( "**appending extra**\n" );
		return MutArrAdapter::append( m_extraValues, v );
	}
	inline Void removeLastExtra()
	{
		printf( "**removing extra**\n" );
		MutArrAdapter::removeLast( m_extraValues );
	}
	inline Detail::ValueStoreType &lastExtra()
	{
		return MutArrAdapter::last( m_extraValues );
	}
	inline Detail::ValueStoreType lastExtra() const
	{
		return MutArrAdapter::last( m_extraValues );
	}
	
	inline Detail::ValueStoreType &last()
	{
		return isExtraEmpty() ? m_mainValue : lastExtra();
	}
	inline Detail::ValueStoreType last() const
	{
		return isExtraEmpty() ? m_mainValue : lastExtra();
	}
};

#ifndef USE_VALUESTACK
# define USE_VALUESTACK 1
#endif

bool balance( const char *psz )
{
	struct myscope
	{
		// on function enter
		myscope( const char *s )
		{
			printf( "balancing \"%s\"...\n", s );
		}
		// on function leave
		~myscope()
		{
			fflush( stdout );
			fflush( stderr );
			printf( "(finished)\n\n" );
		}
	} x__( psz );
	
	static const char *const pszL[ 3 ] = {
		"paren_l(A)", "brack_l(B)", "brace_l(C)"
	};
	static const char *const pszR[ 3 ] = {
		"paren_r(a)", "brack_r(b)", "brace_r(c)"
	};

#ifndef USE_VALUESTACK
# error "USE_VALUESTACK must be defined to either 1 or 0"
#endif

#if USE_VALUESTACK
	TValueStack< 3 > vs;
#else
	unsigned long long int n = 0;
	unsigned c = 0;
#endif

	for(;;) {
#if USE_VALUESTACK
		const unsigned c = vs.num();
#endif
		enum { none, open, close, term } mode = none;
		unsigned index = ~0U;
		switch( *psz++ ) {
		case '\0':
			mode = term;
			break;
			
		case '(':
		case 'A':
			mode = open;
			index = 0;
			break;
		case '[':
		case 'B':
			mode = open;
			index = 1;
			break;
		case '{':
		case 'C':
			mode = open;
			index = 2;
			break;

		case '}':
		case 'c':
			mode = close;
			index = 2;
			break;
		case ']':
		case 'b':
			mode = close;
			index = 1;
			break;
		case ')':
		case 'a':
			mode = close;
			index = 0;
			break;

		default:
			break;
		}
		
		switch( mode ) {
		case none:
			break;

		case term:
			if( c != 0 ) {
#if USE_VALUESTACK
				unsigned n = 0;
				vs.pop( n );
#endif
				if( c == 1 ) {
					fprintf( stderr, "terminated with unmatched %s\n", pszL[ n%3 ] );
				} else {
					fprintf( stderr, "terminated with unmatched %s (and %u more)\n", pszL[ n%3 ], c - 1 );
				}
				return false;
			}

			return true;

		case open:
#if !USE_VALUESTACK
			if( n*3 + index%3 < n ) {
				fprintf( stderr, "too many levels\n" );
				return false;
			}

			++c;
			n *= 3;
			n += index%3;
			printf( "%s: %I64u (%u) -- 0x%.16I64X\n", pszL[ index%3 ], n, c, n );
#else
			vs.push( index%3 );
			printf( "%s: %u\n", pszL[ index%3 ], vs.num() );
#endif
			break;

		case close:
			if( !c ) {
				fprintf( stderr, "no matching %s for %s\n", pszL[ index%3 ], pszR[ index%3 ] );
				return false;
			}
#if USE_VALUESTACK
			{
				unsigned int n = 0;
				vs.pop( n );
				if( index != n ) {
					fprintf( stderr, "%s mismatched with %s\n", pszL[ n%3 ], pszR[ index%3 ] );
					return false;
				}
			}
			printf( "%s: %u\n", pszR[ index%3 ], vs.num() );
#else
			--c;
			if( n%3 != index%3 ) {
				fprintf( stderr, "%s mismatched with %s\n", pszL[ n%3 ], pszR[ index%3 ] );
				return false;
			}
			n /= 3;
			printf( "%s: %I64u (%u) -- 0x%.16I64X\n", pszR[ index%3 ], n, c, n );
#endif
			break;
		}
	}

	fprintf( stderr, "internal error: unreachable\n" );
	return false;
}

int main( int argc, char **argv )
{
	bool r = true;

	for( int i = 1; i < argc; ++i ) {
		r &= balance( argv[ i ] );
	}

	if( argc <= 1 ) {
		r &= balance( "{([()[({[]})]])}()" );
	}
}
