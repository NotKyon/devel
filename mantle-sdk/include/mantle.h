﻿#ifndef MANTLE_H
#define MANTLE_H

/*

	[MSVC++ USERS]
	Use /DELAYLOAD:mantle64.dll then check if mantle64.dll exists with a call to
	LoadLibrary(). If the library exists, call FreeLibrary() to reduce the
	reference count. Mantle functions can be used as normal.

	[MINGW USERS]
	There's no equivalent of MSVC's /DELAYLOAD switch. Either require Mantle or
	put your Mantle related code in a separate library and only load that
	library if you verify Mantle exists (as with the LoadLibrary/FreeLibrary
	method above).




	You can find the Mantle Programming Guide here:

		http://www.amd.com/Documents/Mantle-Programming-Guide-and-API-Reference.pdf

*/

/* From "Rendering Battlefield 4 with Mantle" by Johan Anderson:

	DESCRIPTOR SETS (10/53)

		// map & reset our single descriptor set for the frame
		grBeginDescriptorSetUpdate( descriptorSet );

		for( DrawCall &call : drawCalls )
		{
			// set the resources used by the draw call. this is just an example
			grAttachMemoryViewDescriptors( descriptorSet, vbSlotStart, vertexBufferCount, vbMemoryViews );
			grAttachSamplerDescriptors( descriptorSet, psSamplerSlotStart, psSamplerCount, psSamplers );
			grAttachImageViewDescriptors( descriptorSet, psImageSlotStart, psImageCount, psImages );
		}

		// unmap the descriptor in the end of the frame
		grEndDescriptorSetUpdate( descriptorSet );

		FUTURE OPTIMIZATIONS

			- Use static descriptor sets when possible
			- Reduce resource duplication by reusing & sharing more across shader stages
			- Nested descriptor sets

	GRAPHICS PIPELINES (13/53)

		- All graphics shader stages combined to a single pipeline object together with important graphics state
		- ~10,000 graphics pipelines in BF4 on a single level, ~25 MB of video memory
		- Could use smaller working pool of active state objects to keep reasonable amount in memory
			- Have not been required for us

		PRE-BUILDING PIPELINES (14/53)

			- Graphics pipeline creation is expensive operation, do at load time instead of runtime!
				- Creating one of our graphics pipelines take ~10-60 ms each
				- Pre-build using N parallel low-priority jobs
				- Avoid 99.9% of runtime stalls caused by pipeline creation!

			- Requires knowing the graphics pipeline state that will be used with the shaders
				- Primitive type
				- Render target formats
				- Render target write masks
				- Blend modes

			- Not fully trivial to know all state, may require engine changes / pre-defining use cases
				- Important to design for!

		PIPELINE CACHE (15/53)

			GR_RESULT GR_STDCALL grStorePipeline(
				GR_PIPELINE pipeline,
				GR_SIZE*    pDataSize,
				GR_VOID*    pData);

			GR_RESULT GR_STDCALL grLoadPipeline(
				GR_DEVICE      device,
				GR_SIZE        dataSize,
				const GR_VOID* pData,
				GR_PIPELINE*   pPipeline);

			- Cache built pipelines both in memory cache and disk cache
				- Improve loading times
				- Max 300 MB
				- Simple LRU policy
				- LZ4 compressed (free)

			- Database signature:
				- Driver version
				- Vendor ID
				- Device ID


	MEMORY (16/53)

		MEMORY MANAGEMENT (17/53)

			- Mantle devices exposes multiple memory heaps with characteristics
				- Can be different between devices, drivers and OS:es

			┌───┬────────────────────────────────────┬────┬─────┬────┬─────┐
			│Type  │Size    │Page  │CPU access                                            │GPU Read│GPU Write │CPU Read│CPU Write │
			│════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════│
			│Local │256 MB  │65535 │CpuVisible+CpuGpuCoherent+CpuUncached+CpuWriteCombined│130     │170       │0.0058  │2.8       │
			├───┼────┼───┼───────────────────────────┼────┼─────┼────┼─────┤
			│Local │4096 MB │65535 │                                                      │130     │180       │0       │0         │
			├───┼────┼───┼───────────────────────────┼────┼─────┼────┼─────┤
			│Remote│16106 MB│65535 │CpuVisible+CpuGpuCoherent+CpuUncached+CpuWriteCombined│2.6     │2.6       │0.1     │3.3       │
			├───┼────┼───┼───────────────────────────┼────┼─────┼────┼─────┤
			│Remote│16106 MB│65535 │CpuVisible+CpuGpuCoherent                             │2.6     │2.6       │3.2     │2.9       │
			└───┴────┴───┴───────────────────────────┴────┴─────┴────┴─────┘

			- User explicitly places resources in wanted heaps
				- Driver suggests preferred heaps when creating objects, not a requirement

		FROSTBITE MEMORY HEAPS (18/53)

			- System Shared Mapped
				- CPU memory that is GPU visible
				- Write combined & persistently mapped = easy & fast to write to in parallel at any time

			- System Shared Pinned
				- CPU cached for readback
				- Not used much

			- Video Shared
				- GPU memory accessible by CPU. Used for descriptor sets and dynamic buffers
				- Max 256 MB (legacy constraint)
				- Avoid keeping persistently mapped as WDMM doesn't like this and can decide to move it back to CPU memory :(

			- Video Private
				- GPU private memory
				- Used for render targets, textures and other resources CPU does not need to access

		MEMORY PREFERENCES (19/53)

			- WDDM needs to know which memory allocations are referenced for each command buffer
				- In order to make sure they are resident and not paged out
				- Max ~1,700 memory references are supported
				- Overhead with having lots of references

			- Engine needs to keep track of what memory is referenced while building the command buffers
				- Easy & fast to do
				- Each reference is either read-only or read/write
				- We use a simple global list of references shared for all command buffers

		MEMORY POOLING (20/53)

			- Pooling memory allocations were required for us
				- Sub allocate within larger 1 - 32 MB chunks
				- All resources stored memory handle + offset
				- Not as elegant as just void* on consoles
				- Fragmentation can be a concern, not too much issues for us in practice

			- GPU virtual memory mapping is fully supported, can simplify & optimize management

		OVERCOMMITTING VIDEO MEMORY (21/53)

			- Avoid overcommitting video memory!
				- Will lead to svere stalls as VidMM moves blocks and moves memory back and forth
				- VidMM is a black box :(
				- One of the biggest issues we ran into during development

			- Recommendations
				- Balance memory pools
				- Make sure to use read-only memory references
				- Use memory priorities

		MEMORY PRIORITIES (22/53)

			- Setting priorities on the memory allocations help VidMM choose what to page out when it has to

			- 5 priority levels
				- Very high = Render targets with MSAA
				- High = Render targets and UAAVs
				- Normal = Textures
				- Low = Shader & constant buffers
				- Very low = Vertex & index buffers

		MEMORY RESIDENCY FUTURE (23/53)

			- For best results manage which resources are in video memory yourself & keep only ~80% used
				- Avoid all stalls
				- Can async DMA in and out

			- We are thinking of redesigning to fully avoid possibility of overcommitting

			- Hoping WDDM's memory residency management can be simplified & improved in the future


	RESOURCE MANAGEMENT (24/53)

		RESOURCE LIFETIMES (25/53)

			- App manages lifetime of all resources
				- Have to make sure GPU is not using an object or memory while we are freeing it on the CPU
				- How we've always worked with GPUs on the consoles
				- Multi-GPU adds some additional complexity that consoles do not have

			- We keep track of lifetimes on a per frame granularity
				- Queues for object destruction & free memory operations
				- Add to queue at any time on the CPU
				- Process queues when GPU command buffers for the frame are done executing
				- Tracked with command buffer fences

		LINEAR FRAME ALLOCATOR (26/53)

			- We use multiple linear allocators with Mantle for both transient buffers & images
				- Used for huge amount of small constant data and other GPU frame data that CPU writes
				- Easy to use and very low overhead
				- Don't have to care about lifetimes or state

			- Fixed memory buffers for each frame
				- Super cheap sub-allocation from any thread
				- If full, use heap allocation (also fast due to pooling)

			- Alternative: Ring buffers
				- Requires being able to stall & drain pipeline at any allocation if full, additional complexity for us

		TILING (27/53)

			- Textures should be tiled for performance
				- Explicitly handled in Mantle, user selects linear or tiled
				- Some formats (BC) can't be accessed as linear by the GPU

			- On consoles we handle tiling offline as part of our data processing pipeline
				- We know the exact tiling formats and have separate resources per platform

			- For Mantle
				- Tiling formats are opaque, can be different between GPU architectures and image types
				- Tile textures with DMA image upload from SystemShared to VideoPrivate
					- Linear source, tiled destination
					- Free


	COMMAND BUFFERS (28/53)

		COMMAND BUFFERS (29/53)

			- Command buffers are the atomic unit of work dispatched to the GPU
				- Separate creation from execution
				- No "immediate context" a la DX11 that can execute work at any call
				- Makes resource synchronization and setup significantly easier & faster

			- Typical BF4 scenes have around ~50 command buffers per frame
				- Reasonable tradeoff for us with submission overhead vs CPU load-balancing

		COMMAND BUFFER SOURCES (30/53)

			- Frostbite has 2 separate sources of command buffers

				- World rendering
					- Rendering the world with tons of objects, lots of draw calls. Have all frame data up front
					- All resources except for render targets are read-only
					- Generated in parallel up front each frame

				- Immediate rendering ("the rest")
					- Setting up rendering and doing lighting, post-fx, virtual texturing, compute, etc
					- Managing resource state, memory and running on different queues (graphics, compute, DMA)
					- Sequentially generated in a single job, simulate an immediate context by splitting the command buffer

			- Both are very important and have different requirements

		RESOURCE TRANSITIONS (31/53)

			- Key design in Mantle to significantly lower driver overhead & complexity
				- Explicit hazard tracking by the app/engine
				- Drives architecture-specific caches & compression
				- AMD: FMASK, CMASK, HTILE
				- Enables explicit memory management

			- Examples:
				- Optimal render target writes → Graphics shader read-only
				- Compute shader write-only → DrawIndirect arguments

			- Mantle has a strong validation layer that tracks transitions which is a major help

		MANAGING RESOURCE TRANSITIONS (32/53)

			- Engines need a clear design on how to handle state transitions

			- Multiple approaches possible:

				- Sequential in-order command buffers
					- Generate one command buffer at the time in order
					- Transition resources on-demand when doing operation on them, very simple
					- Recommendation: start with this

				- Out-of-order multiple command buffers
					- Track state per command buffer, fix up transitions when order of command buffers is known

				- Hybrid approaches & more

		MANAGING RESOURCE TRANSITIONS IN FROSTBITE (33/53)

			- Current approach in Frostbite is quite basic:
				- We keep track of a single state for each resource (not subresource)
				- The "immediate rendering" transition resources as needed depending on operation
				- The out of order "world rendering" command buffers don't need to transition states
					- Already have write access to MRTs and read-access to all resources setup outside them
					- Avoids the problem of them not knowing the state during generation

			- Works now but as we do more general parallel rendering it will have to change
				- Track resource state for each command buffer & fixup between command buffers

		DYNAMIC STATE OBJECTS

			- Graphics state is only set with the pipeline object and 5 dynamic state objects
				- State objects: color blend, raster, viewport, depth-stencil, MSAA
				- No other parameters such as in DX11 with stencil ref or SetViewport functions

			typedef struct _GR_DEPTH_STENCIL_STATE_CREATE_INFO
			{
				GR_BOOL             depthEnable;
				GR_BOOL             depthWriteEnable;
				GR_ENUM             depthFunc;          // GR_COMPARE_FUNC
				GR_BOOL             depthBoundsEnable;
				GR_FLOAT            minDepth;
				GR_FLOAT            maxDepth;
				GR_BOOL             stencilEnable;
				GR_UINT8            stencilReadMask;
				GR_UINT8            stencilWriteMask;
				GR_DEPTH_STENCIL_OP front;
				GR_DEPTH_STENCIL_OP back;
			} GR_DEPTH_STENCIL_STATE_CREATE_INFO;

			// NOTE: minDepth and maxDepth are highlighted in the above

			- Frostbite use case:
				- Pre-create when possible
				- Otherwise on-demand creation (hash map)
				- Only ~100 state objects!

			- Still possible to end up with lots of state objects
				- Esp. with state object float & integer values (depth bounds, depth bias, viewport)
				- But no need to store all permutations in memory, objects are fast to create & app manages lifetimes

	QUEUES (35/53)

		QUEUES (36/53)

			- Universal queue can do both graphics, compute and presents

			- We also use additional queues to parallelize GPU operations:
				- DMA queue - Improve perf with faster transfers & avoiding idling graphics while transfering
				- Compute queue - Improve perf by utilizing idle ALU and update resources simultaneously with gfx

			- More GPUs = more queues!

		QUEUES SYNCHRONIZATION (37/53)

			- Order of execution within a queue is sequential
			- Synchronize multiple queues with GPU semaphores (signal & wait)
			- Also works across multiple GPUs

				`          |    |    |    |    |    |    |
				Compute    |[   Wait | ][ |][S]|    |    |
				`          |    |    |↑  |   #→#  |    |
				Graphics   |[  ]|[  ][S][ |][  ][W][| ][ |  ]

			(cont, 38/53)

			- Started out with explicit semaphores
				- Error prone to handle when having lots of different semaphores & queues
				- Difficult to visualize & debug

			- Switched to a representation more similar to a job graph

			- Just a model on top of the semaphores

		GPU JOB GRAPH (39/53)

			- Each GPU job has list of dependencies (other command buffers)

			- Dependencies has to finish first before job can run on its queue

			- The dependencies can be from any queue

				`        [ DMA ]┐
				`               │
				`               ├→[ COMPUTE ]
				`               │
				` [ GRAPHICS 1 ]┴→[ GRAPHICS 2 ]→[ GRAPHICS 3 ]

			- Was easier to work with, debug and visualize

			- Really extendable going forward

		ASYNC DMA (40/53)

			- AMD GPUs have dedicated hardware DMA engines, let's use them!
				- Uploading through DMA is faster than on universal queue, even if blocking
				- DMA have alignment restrictions, have to support falling back to copies on universal queue

			- Use case: Frame buffer & texture uploads
				- Used by resource initial data uploads and our UpdateSubresource
				- Guaranteed to be finished before the GPU universal queue starts rendering the frame

			- Use case: Multi-GPU frame buffer copy
				- Peer-to-peer copy of the frame buffer to the GPU that will present it

		ASYNC COMPUTE (41/53)

			- Frostbite has lots of compute shader passes that could run in parallel with graphics work
				- HBAO, blurring, classification, tile-based lighting, etc

			- Running as async compute can improve GPU performance by utilizing "free" ALU
				- For example while doing shadowmap rendering (ROP bound)

		ASYNC COMPUTE - TILE-BASED LIGHTING (42/53)

			`              |       |       |       |       |       |       |       |       |       |
			` [ Compute  ] | [   Wait   ][ TileZ ][| Cull Lights  ][Lighting][S]   |       |       |
			` [ Graphics ] | [Gbuffer][S][ Shadowmaps ][ Reflection|][ Distort ][W][ Transp|]      |

			- 3 sequential compute shaders
				- Input: zbuffer & gbuffer
				- Output: HDR texture/UAV

			- Runs in parallel with graphics pipeline that renders to other targets

			(cont 43/53)

			- We manually prepare the resources for the async compute
				- Important to not access the resources on other queues at the same time (unless read-only state)
				- Have to transition resources on the queue that last used it

			- Up to 80% faster in our initial tests, but not fully reliable
				- But is a pretty small part of the frame time
				- Not in BF4 yet


	CONCLUSION (50/53)

		MANTLE DEV RECOMMENDATIONS (51/53)

			- The validation layer is a critical friend!

			- You'll end up with a lot of object & memory management code, try to share with the console code

			- Make sure you have control over memory usage and can avoid overcommitting video memory

			- Build a robust solution for resource state management early

			- Figure out how to pre-create your graphics pipelines, can require engine design changes

			- Build for multi-GPU support from the start, easier than to retrofit

		FUTURE (52/53)

			- Second wave of Frostbite Mantle titles

			- Adapt Frostbite core rendering layer based on learnings from Mantle
				- Refine binding & buffer updates to further reduce overhead
				- Virtual memory management
				- More async compute & async DMAs
				- Multi-GPU job graph R&D

			- Linux
				- Would like to see how our Mantle renderer behaves with different memory management & driver model

*/

/* From "Mantle and Nitrous: Combining efficient engine design with a modern API" by Dan Baker:

	Nitrous Command Buffers are translated into Mantle Command Buffers at one section
	Gets more optimal use out of instruction cache and data cache


	Starting Our Frame

		g_fTotalFrameTime = System::Time::TimeAsSeconds( System::Time::PeekTime() ) - g_StartTotalFrameTime;
		g_uFrameBuffer = ( g_uFrame % TransferMemory::BUFFERS );

		if( g_bProtectMemory ) {
			ProtectMemory( g_CmdTransferMemory.PerFrameMemory[ g_uFrameBuffer ].pData, g_CmdTransferMemory.Capacity, PO_READWRITE );
		}

		gr = grWaitForFences( g_Device, 1, &g_FrameFences[ g_uFrameBuffer ], true, MAX_FRAME_WAIT_TIME_SECONDS );
		if( gr == GR_TIMEOUT ) { // Throws the frame out
			return false;
		}

		g_StartTotalFrameTime = System::Time::TimeAsSeconds( System::Time::PeekTime() );

		// Reset our allocation and map the current GPU memory
		HeapAndChunk HeapChunk = g_GPUTransferMemory.PerFrameMemory[ g_uFrameBuffer ].Memory;
		MarkMemoryChunk( HeapChunk );
		GR_GPU_MEMORY DynamicMemory = g_MemoryHeap.MemoryChunk[ HeapChunk.Heap ][ HeapChunk.SubChunk ].Memory;

		gr = grMapMemory( DynamicMemory, 0, ( GR_VOID ** )&g_GPUTransferMemory.PerFrameMemory[ g_uFrameBuffer ].pData );

		g_CmdTransferMemory.PerFrameMemory[ g_uFrameBuffer ].Offset = 0;
		g_GPUTransferMemory.PerFrameMemory[ g_uFrameBuffer ].Offset = 0;

		g_GPUTransferMemory.pCurrentMantleMemoryBase = g_GPUTransferMemory.PerFrameMemory[ g_uFrameBuffer ].pData;
		g_GPUTransferMemory.pCurrentMantleMemoryEnd = g_GPUTransferMemory.PerFrameMemory[ g_uFrameBuffer ].pData + g_GPUTransferMemory.Capacity;
		g_GPUTransferMemory.CurrentMantleMemory = DynamicMemory;
		g_GPUTransferMemory.TempHeapMemory = 0;

		return true;

	// Use memory heap that has highest cpuWritePerfRating
	// In Debug, rather than copying directly to GPU memory, allocate CPU memory
	// - Or use pinned Mantle memory
	// Then, use OS call Virtual Protect with PAGE_NOACCESS for any data that affects the frame, while the frame is being accessed by GPU, or could be being translated by the CPU
	// If any part of system inadvertently writes to the memory, will throw exception


	CREATING A RESOURCE

		// Setup our resource descriptions
		ResourceDesc RedLUTDesc, BlueLUTDesc, GreenLUTDesc;
		RedLUTDesc.Init(OX_FORMAT_R32_FLOAT, 1, 1, RT_TEXTURE_1D, v3ui(1024,1,1), pfRedData, 0);
		BlueLUTDesc.Init(OX_FORMAT_R32_FLOAT, 1, 1, RT_TEXTURE_1D, v3ui(1024,1,1), pfBlueData, 0);
		GreenLUTDesc.Init(OX_FORMAT_R32_FLOAT, 1, 1, RT_TEXTURE_1D, v3ui(1024,1,1), pfGreenData, 0);

		// Calculate size
		uint64 uSize = 0;
		uSize += GetResourceMemorySize( RedLUTDesc, Graphics::RF_SHADERRESOURCE, Graphics::HT_GPU );
		uSize += GetResourceMemorySize( BlueLUTDesc, Graphics::RF_SHADERRESOURCE, Graphics::HT_GPU );
		uSize += GetResourceMemorySize( GreenLUTDesc, Graphics::RF_SHADERRESOURCE, Graphics::HT_GPU );

		// Create a GPU heap, sized to what we want
		g_GPUMemory = AllocateHeap( uSize, Graphics::HT_GPU );

		// Create the resources
		ColorLuts.RedLUT.Resource = CreateResource( RedLUTDesc, RF_SHADERRESOURCE, RSTATE_DEFAULT, g_GPUMemory );
		ColorLuts.BlueLUT.Resource = CreateResource( BlueLUTDesc, RF_SHADERRESOURCE, RSTATE_DEFAULT, g_GPUMemory );
		ColorLuts.GreenLUT.Resource = CreateResource( GreenLUTDesc, RF_SHADERRESOURCE, RSTATE_DEFAULT, g_GPUMemory );


	CONSTANT BUFFERS
	- Nitrous does not have concept of constant buffers
	- Instead, all constant data is thrown out every frame
	--- When we render an object, CPU will generate the constants needed for that frame
	--- Grab a piece of the Frame Memory and write to it
	- Constant bindings are just references into our frame memory
	- But... be careful! CPU is writing straight to GPU memory. Do NOT read it back!
	- Evidence suggests no performance advantage of persisting constants across frames, regenerating every frame is ample fast. 100k+ batches not a problem


	What about that Present()?
	- Unlike other APIs, we do not need, or should, block on the present on the main thread
	- Instead we spawn a job, which we block against on the next present

		void PresentJob()
		{
			// ...

			result = grQueueSubmit( g_UniversalQueue, g_cCommandBuffers, g_CommandBuffers, cMemRefs, MemRef, g_FrameFences[ g_uSubmittingFrameBuffer ] );

			uint32 PresentFlags = 0;

			if( g_bVSync ) {
				PresentFlags = GR_WSI_WIN_PRESENT_FLIP_DONOTWAIT;
			}

			// instruct the GPU to present the backbuffer in the application window
			GR_WSI_WIN_PRESENT_INFO presentInfo =
			{
				g_hWnd, g_MasterResourceList.Images[ DR_BACKBUFFER ],
				GR_WSI_WIN_PRESENT_MODE_BLT, 0, PresentFlags
			};

			result = grWsiWinQueuePresent( g_UniversalQueue, &presentInfo );
			SignalProcessAndPresentDone( pInfo );
		}

	// What our frame submission looks like:
	// 1. Block on last frames present's job (e.g., NOT the fence, the actual job we spawned)
	// 2. Process any pending resource transitions from newly created resources
	// 3. Generate all pending unordered commands, by generating into 1 or more cmd buffers
	// 4. Send signals to the issuers of unordered commands, to notify them the commands are submitted
	// 5. Begin translation of Nitrous cmds into Mantle cmds -- usually 100-500 jobs across all cores
	// 6. Flush the deletion queues for this frame (likely a few frames old at this point)
	// 7. Any item in our master deletion queue, add to the now empty deletion queue for this frame
	// 8. Handle memory readbacks
	// 9. Spawn Present() job

*/

#ifdef __cplusplus
# define GR__ENTER_C        extern "C" {
# define GR__LEAVE_C        }
#else
# define GR__ENTER_C
# define GR__LEAVE_C
#endif

#define GR_DLLIMPORT        extern __declspec(dllimport)
#define GR_STDCALL          __stdcall

/*
===============================================================================

	BASIC TYPES

===============================================================================
*/

#ifdef _MSC_VER
typedef signed __int8       GR_INT8;
typedef signed __int16      GR_INT16;
typedef signed __int32      GR_INT32;
typedef signed __int64      GR_INT64;
typedef unsigned __int8     GR_UINT8;
typedef unsigned __int16    GR_UINT16;
typedef unsigned __int32    GR_UINT32;
typedef unsigned __int64    GR_UINT64;
#else
# include <stdint.h>
typedef int8_t              GR_INT8;
typedef int16_t             GR_INT16;
typedef int32_t             GR_INT32;
typedef int64_t             GR_INT64;
typedef uint8_t             GR_UINT8;
typedef uint16_t            GR_UINT16;
typedef uint32_t            GR_UINT32;
typedef uint64_t            GR_UINT64;
#endif

typedef void                GR_VOID;
typedef char                GR_CHAR;
typedef float               GR_FLOAT;

typedef GR_INT32            GR_INT;
typedef GR_UINT32           GR_UINT;
typedef GR_UINT32           GR_BOOL;

#include <stddef.h>
typedef size_t              GR_SIZE;
typedef GR_UINT64           GR_GPU_SIZE;

typedef GR_INT32            GR_ENUM;
typedef GR_INT32            GR_FLAGS;

typedef GR_UINT32           GR_SAMPLE_MASK; /*guess*/

typedef GR_UINT64           GR_PHYSICAL_GPU;
typedef GR_UINT64           GR_DEVICE;
typedef GR_UINT64           GR_QUEUE;

typedef GR_UINT64           GR_BASE_OBJECT;
typedef GR_UINT64           GR_OBJECT;

typedef GR_UINT64           GR_CMD_BUFFER;
typedef GR_UINT64           GR_DESCRIPTOR_SET;
typedef GR_UINT64           GR_FENCE;
typedef GR_UINT64           GR_GPU_MEMORY;
typedef GR_UINT64           GR_IMAGE;
typedef GR_UINT64           GR_PIPELINE;
typedef GR_UINT64           GR_SAMPLER;
typedef GR_UINT64           GR_SHADER;
typedef GR_UINT64           GR_QUEUE_SEMAPHORE;
typedef GR_UINT64           GR_QUERY_POOL;
typedef GR_UINT64           GR_EVENT;

typedef GR_UINT64           GR_IMAGE_VIEW;
typedef GR_UINT64           GR_COLOR_TARGET_VIEW;
typedef GR_UINT64           GR_DEPTH_STENCIL_VIEW;

typedef GR_UINT64           GR_STATE_OBJECT;
typedef GR_UINT64           GR_COLOR_BLEND_STATE_OBJECT;
typedef GR_UINT64           GR_DEPTH_STENCIL_STATE_OBJECT;
typedef GR_UINT64           GR_MSAA_STATE_OBJECT;
typedef GR_UINT64           GR_RASTER_STATE_OBJECT;
typedef GR_UINT64           GR_VIEWPORT_STATE_OBJECT;

typedef GR_UINT64           GR_WSI_WIN_DISPLAY;




/*
===============================================================================

	BASIC CONSTANTS

===============================================================================
*/

const GR_UINT32 GR_API_VERSION                   = 1;

const GR_UINT64 GR_NULL_HANDLE                   = 0;

const GR_BOOL GR_FALSE                           = 0;
const GR_BOOL GR_TRUE                            = 1;

const GR_UINT32 GR_MAX_PHYSICAL_GPUS             = 4;
const GR_UINT32 GR_MAX_PHYSICAL_GPU_NAME         = 255; /*guess*/
const GR_UINT32 GR_MAX_VIEWPORTS                 = 16; /*guess*/
const GR_UINT32 GR_MAX_MEMORY_HEAPS              = 8;
const GR_UINT32 GR_MAX_COLOR_TARGETS             = 16; /*guess*/
const GR_UINT32 GR_MAX_DESCRIPTOR_SETS           = 2;

const GR_UINT32 GR_MAX_DEVICE_NAME_LEN           = 128; /*guess*/
const GR_UINT32 GR_MAX_GAMMA_RAMP_CONTROL_POINTS = 4; /*guess*/




GR__ENTER_C

/*
===============================================================================
###############################################################################

	CORE ENUMERATIONS

###############################################################################
===============================================================================
*/

/*
	Defines an image and memory view channel format.
*/
typedef enum _GR_CHANNEL_FORMAT
{
	/* An undefined channel format. */
	GR_CH_FMT_UNDEFINED     = 0,
	/* A channel format of R4G4. */
	GR_CH_FMT_R4G4          = 1,
	/* A channel format of R4G4B4A4. */
	GR_CH_FMT_R4G4B4A4      = 2,
	/* A channel format of R5G6B5. */
	GR_CH_FMT_R5G6B5        = 3,
	/* A channel format of B5G6R5 */
	GR_CH_FMT_B5G6R5        = 4,
	/* A channel format of R5G5B5A1 */
	GR_CH_FMT_R5G5B5A1      = 5,
	/* A channel format of R8 */
	GR_CH_FMT_R8            = 6,
	/* A channel format of R8G8 */
	GR_CH_FMT_R8G8          = 7,
	/* A channel format of R8G8B8A8 */
	GR_CH_FMT_R8G8B8A8      = 8,
	/* A channel format of B8G8R8A8 */
	GR_CH_FMT_B8G8R8A8      = 9,
	/* A channel format of R10G11B11 */
	GR_CH_FMT_R10G11B11     = 10,
	/* A channel format of R11G11B10 */
	GR_CH_FMT_R11G11B10     = 11,
	/* A channel format of R10G10B10A2 */
	GR_CH_FMT_R10G10B10A2   = 12,
	/* A channel format of R16 */
	GR_CH_FMT_R16           = 13,
	/* A channel format of R16G16 */
	GR_CH_FMT_R16G16        = 14,
	/* A channel format of R16G16B16A16 */
	GR_CH_FMT_R16G16B16A16  = 15,
	/* A channel format of R32 */
	GR_CH_FMT_R32           = 16,
	/* A channel format of R32G32 */
	GR_CH_FMT_R32G32        = 17,
	/* A channel format of R32G32B32 */
	GR_CH_FMT_R32G32B32     = 18,
	/* A channel format of R32G32B32A32 */
	GR_CH_FMT_R32G32B32A32  = 19,
	/* A channel format of R16G8 */
	GR_CH_FMT_R16G8         = 20,
	/* A channel format of R32G8 */
	GR_CH_FMT_R32G8         = 21,
	/* A channel format of R9G9B9E5 */
	GR_CH_FMT_R9G9B9E5      = 22,
	/* A channel format of BC1 */
	GR_CH_FMT_BC1           = 23,
	/* A channel format of BC2 */
	GR_CH_FMT_BC2           = 24,
	/* A channel format of BC3 */
	GR_CH_FMT_BC3           = 25,
	/* A channel format of BC4 */
	GR_CH_FMT_BC4           = 26,
	/* A channel format of BC5 */
	GR_CH_FMT_BC5           = 27,
	/* A channel format of BC6U */
	GR_CH_FMT_BC6U          = 28,
	/* A channel format of BC6S */
	GR_CH_FMT_BC6S          = 29,
	/* A channel format of BC7 */
	GR_CH_FMT_BC7           = 30
} GR_CHANNEL_FORMAT;

/*
	Defines an image and memory view number format.
*/
typedef enum _GR_NUM_FORMAT
{
	/* An undefined number format. */
	GR_NUM_FMT_UNDEFINED    = 0,
	/* An unsigned normalized integer format. */
	GR_NUM_FMT_UNORM        = 1,
	/* A signed normalized integer format. */
	GR_NUM_FMT_SNORM        = 2,
	/* An unsigned integer format. */
	GR_NUM_FMT_UINT         = 3,
	/* A signed integer format. */
	GR_NUM_FMT_SINT         = 4,
	/* A floating-point format. */
	GR_NUM_FMT_FLOAT        = 5,
	/* An unsigned normalized sRGB integer format. */
	GR_NUM_FMT_SRGB         = 6,
	/* A depth-stencil format. */
	GR_NUM_FMT_DS           = 7
} GR_NUM_FORMAT;

/*
	The GPU queue type.
*/
typedef enum _GR_QUEUE_TYPE
{
	/* A universal pipeline queue capable of executing graphics and compute workloads. */
	GR_QUEUE_UNIVERSAL = 0x1000,
	/* A compute only pipeline queue. */
	GR_QUEUE_COMPUTE   = 0x1001
} GR_QUEUE_TYPE;

/*
	GPU memory object priority that provides a hint to the GPU memory manager regarding how hard it should try to keep allocation in a preferred heap.
*/
typedef enum _GR_MEMORY_PRIORITY
{
	/* Normal GPU memory object priority. */
	GR_MEMORY_PRIORITY_NORMAL    = 0x1100,
	/* High GPU memory object priority. Should be used for storing performance critical resources, such as render targets, depth buffers, and write accessible images. */
	GR_MEMORY_PRIORITY_HIGH      = 0x1101,
	/* Low GPU memory object priority. Should be used for infrequently accessed resources that generally do not require a lot of memory bandwidth. */
	GR_MEMORY_PRIORITY_LOW       = 0x1102,
	/* GPU memory priority for marking memory objects that are not a part of the working set. Should only be set for memory allocations that do not contain any used resources. */
	GR_MEMORY_PRIORITY_UNUSED    = 0x1103,
	/* Highest GPU memory object priority. Should be used for storing performance critical resources, such as high-priority render targets, depth buffers, and write accessible images. */
	GR_MEMORY_PRIORITY_VERY_HIGH = 0x1104,
	/* Lowest GPU memory object priority. Should be used for lowest priority infrequently accessed resources that generally do not require a lot of memory bandwidth. */
	GR_MEMORY_PRIORITY_VERY_LOW  = 0x1105
} GR_MEMORY_PRIORITY;

/*
	The memory state defines how the GPU expects to use a range of memory.
*/
typedef enum _GR_MEMORY_STATE
{
	/* Memory range is accessible by the CPU for data transfer, or it can be copied to and from by the GPU. */
	GR_MEMORY_STATE_DATA_TRANSFER               = 0x1200,
	/* Memory range can be used as a read-only memory view in the graphics pipeline. */
	GR_MEMORY_STATE_GRAPHICS_SHADER_READ_ONLY   = 0x1201,
	/* Memory range can be used as a write-only memory view in the graphics pipeline. */
	GR_MEMORY_STATE_GRAPHICS_SHADER_WRITE_ONLY  = 0x1202,
	/* Memory range can be used as a read or write memory view in the graphics pipeline. */
	GR_MEMORY_STATE_GRAPHICS_SHADER_READ_WRITE  = 0x1203,
	/* Memory range can be used as a read-only memory view in the compute pipeline. */
	GR_MEMORY_STATE_COMPUTE_SHADER_READ_ONLY    = 0x1204,
	/* Memory range can be used as a write-only memory view in the compute pipeline. */
	GR_MEMORY_STATE_COMPUTE_SHADER_WRITE_ONLY   = 0x1205,
	/* Memory range can be used as a read or write memory view in the compute pipeline. */
	GR_MEMORY_STATE_COMPUTE_SHADER_READ_WRITE   = 0x1206,
	/* Memory range can be simultaneously used as a read-only memory view in the graphics or compute pipelines, or as index data, or as indirect arguments for draws and dispatches. */
	GR_MEMORY_STATE_MULTI_USE_READ_ONLY         = 0x1207,
	/* Memory range can be used by the graphics pipeline as index data. */
	GR_MEMORY_STATE_INDEX_DATA                  = 0x1208,
	/* Memory range can be used as arguments for indirect draw or dispatch operations. */
	GR_MEMORY_STATE_INDIRECT_ARG                = 0x1209,
	/* Memory range can be used as destination for writing GPU timestamps. */
	GR_MEMORY_STATE_WRITE_TIMESTAMP             = 0x120a,
	/* Memory range can be used for performing queue atomic operations. */
	GR_MEMORY_STATE_QUEUE_ATOMIC                = 0x120b,
	/* Memory range state should not be tracked and is invalid until it is transitioned to a valid state. */
	GR_MEMORY_STATE_DISCARD                     = 0x120c,
	/* Memory range can be used as a source for GPU copies. */
	GR_MEMORY_STATE_DATA_TRANSFER_SOURCE        = 0x120d,
	/* Memory range can be used as a destination for GPU copies. */
	GR_MEMORY_STATE_DATA_TRANSFER_DESTINATION   = 0x120e
} GR_MEMORY_STATE;

/*
	The image state defines how the GPU expects to use a range of image subresources.
*/
typedef enum _GR_IMAGE_STATE
{
	/* Range of image subresources is accessible by the CPU for data transfer or can be copied by the GPU. */
	GR_IMAGE_STATE_DATA_TRANSFER                = 0x1300,
	/* Range of image subresources can be used as a read-only image view in the graphics pipeline. */
	GR_IMAGE_STATE_GRAPHICS_SHADER_READ_ONLY    = 0x1301,
	/* Range of image subresources can be used as a write-only image view in the graphics pipeline. */
	GR_IMAGE_STATE_GRAPHICS_SHADER_WRITE_ONLY   = 0x1302,
	/* Range of image subresources can be used as a read or write image view in the graphics pipeline. */
	GR_IMAGE_STATE_GRAPHICS_SHADER_READ_WRITE   = 0x1303,
	/* Range of image subresources can be used as a read-only image view in the compute pipeline. */
	GR_IMAGE_STATE_COMPUTE_SHADER_READ_ONLY     = 0x1304,
	/* Range of image subresources can be used as a write-only image view in the compute pipeline. */
	GR_IMAGE_STATE_COMPUTE_SHADER_WRITE_ONLY    = 0x1305,
	/* Range of image subresources can be used as a read or write image view in the compute pipeline. */
	GR_IMAGE_STATE_COMPUTE_SHADER_READ_WRITE    = 0x1306,
	/* Range of image subresources can be simultaneously used as a read-only image view in both the graphics and compute pipelines. */
	GR_IMAGE_STATE_MULTI_SHADER_READ_ONLY       = 0x1307,
	/* Range of image subresources can be simultaneously used as read-only depth or stencil target and as a read-only image view in the graphics pipeline. */
	GR_IMAGE_STATE_TARGET_AND_SHADER_READ_ONLY  = 0x1308,
	/* Range of image subresources in depth-stencil or color target images is assumed to be in an undefined state. GR_IMAGE_STATE_UNINITIALIZED is the default state for all target image subresources after binding the target image to a new memory location. The state cannot be used for any operation. */
	GR_IMAGE_STATE_UNINITIALIZED                = 0x1309,
	/* Range of image subresources is intended to be used as a color or depth-stencil target. The image is optimized for rendering. */
	GR_IMAGE_STATE_TARGET_RENDER_ACCESS_OPTIMAL = 0x130a,
	/* Range of image subresources is intended to be used as a color or depth-stencil target. The image is optimized for shader access. */
	GR_IMAGE_STATE_TARGET_SHADER_ACCESS_OPTIMAL = 0x130b,
	/* Range of image subresources can be used for image clears. */
	GR_IMAGE_STATE_CLEAR                        = 0x130c,
	/* Range of image subresources can be used as a source for resolve operation. */
	GR_IMAGE_STATE_RESOLVE_SOURCE               = 0x130d,
	/* Range of image subresources can be used as a destination for resolve operation. */
	GR_IMAGE_STATE_RESOLVE_DESTINATION          = 0x130e,
	/* Range of image subresources is in invalid state until they are transitioned to a valid state. */
	GR_IMAGE_STATE_DISCARD                      = 0x131f,
	/* Range of image subresources can be used as a source for the GPU copies. */
	GR_IMAGE_STATE_DATA_TRANSFER_SOURCE         = 0x1310,
	/* Range of image subresources can be used as a destination for the GPU copies. */
	GR_IMAGE_STATE_DATA_TRANSFER_DESTINATION    = 0x1311
} GR_IMAGE_STATE;

/*
	Image type defines image dimensionality and organization of subresources.
*/
typedef enum _GR_IMAGE_TYPE
{
	/* The image is a 1D texture or 1D texture array. */
	GR_IMAGE_1D         = 0x1400,
	/* The image is a 2D texture or 2D texture array. */
	GR_IMAGE_2D         = 0x1401,
	/* The image is a 3D texture. */
	GR_IMAGE_3D         = 0x1402
} GR_IMAGE_TYPE;

/*
	Image tiling defines internal texel layout in memory.
*/
typedef enum _GR_IMAGE_TILING
{
	/* Images with linear tiling are stored linearly in memory with device specific pitch. */
	GR_LINEAR_TILING    = 0x1500,
	/* Images with optimal tiling have device-optimal texel layout in memory. */
	GR_OPTIMAL_TILING   = 0x1501
} GR_IMAGE_TILING;

/*
	Defines image view type for shader image access.
*/
typedef enum _GR_IMAGE_VIEW_TYPE
{
	/* The image view is a 1D texture or 1D texture array. */
	GR_IMAGE_VIEW_1D       = 0x1600,
	/* The image view is a 2D texture or 2D texture array. */
	GR_IMAGE_VIEW_2D       = 0x1601,
	/* The image view is a 3D texture. */
	GR_IMAGE_VIEW_3D       = 0x1602,
	/* The image view is a cubemap texture or a cubemap texture array. */
	GR_IMAGE_VIEW_CUBE     = 0x1603
} GR_IMAGE_VIEW_TYPE;

/*
	Image aspect defines what components of the image object are referenced: color, depth, or stencil.
*/
typedef enum _GR_IMAGE_ASPECT
{
	/* Color components of the image. */
	GR_IMAGE_ASPECT_COLOR   = 0x1700,
	/* Depth component of the image. */
	GR_IMAGE_ASPECT_DEPTH   = 0x1701,
	/* Stencil component of the image. */
	GR_IMAGE_ASPECT_STENCIL = 0x1702
} GR_IMAGE_ASPECT;

/*
	Channel swizzle defines remapping of texture channels in image views.
*/
typedef enum _GR_CHANNEL_SWIZZLE
{
	/* Image fetch returns zero value. */
	GR_CHANNEL_SWIZZLE_ZERO = 0x1800,
	/* Image fetch returns a value of one. */
	GR_CHANNEL_SWIZZLE_ONE  = 0x1801,
	/* Maps image data to red channel. */
	GR_CHANNEL_SWIZZLE_R    = 0x1802,
	/* Maps image data to green channel. */
	GR_CHANNEL_SWIZZLE_G    = 0x1803,
	/* Maps image data to blue channel. */
	GR_CHANNEL_SWIZZLE_B    = 0x1804,
	/* Maps image data to alpha channel. */
	GR_CHANNEL_SWIZZLE_A    = 0x1805
} GR_CHANNEL_SWIZZLE;

/*
	Defines a type of object expected by a shader in a descriptor slot.
*/
typedef enum _GR_DESCRIPTOR_SET_SLOT_TYPE
{
	/* The descriptor set slot is not used by the shader. */
	GR_SLOT_UNUSED              = 0x1900,
	/* The descriptor set slot maps to a “t#” shader resource. */
	GR_SLOT_SHADER_RESOURCE     = 0x1901,
	/* The descriptor set slot maps to a “u#” shader UAV resource. */
	GR_SLOT_SHADER_UAV          = 0x1902,
	/* The descriptor set slot maps to a sampler. */
	GR_SLOT_SHADER_SAMPLER      = 0x1903,
	/* The descriptor set stores a pointer to the next level of a nested descriptor set. */
	GR_SLOT_NEXT_DESCRIPTOR_SET = 0x1904
} GR_DESCRIPTOR_SET_SLOT_TYPE;

/*
	The types of GPU queries.
*/
typedef enum _GR_QUERY_TYPE
{
	/* An occlusion query counts a number of samples that pass depth and stencil tests. */
	GR_QUERY_OCCLUSION              = 0x1a00,
	/* A pipeline statistics query counts a number of processed elements at different stages in a pipeline. */
	GR_QUERY_PIPELINE_STATISTICS    = 0x1a01
} GR_QUERY_TYPE;

/*
	The GPU timestamp type determines where in a pipeline timestamps are generated.
*/
typedef enum _GR_TIMESTAMP_TYPE
{
	/* Top-of-pipe timestamp is generated when draw or dispatch become active. */
	GR_TIMESTAMP_TOP        = 0x1b00,
	/* Bottom-of-pipe timestamp is generated when draw or dispatch have finished execution. */
	GR_TIMESTAMP_BOTTOM     = 0x1b01
} GR_TIMESTAMP_TYPE;

/*
	The border color type specifies what color is fetched in the GR_TEX_ADDRESS_CLAMP_BORDER texture addressing mode for coordinates outside of the range [0..1].
*/
typedef enum _GR_BORDER_COLOR_TYPE
{
	/* White (1.0, 1.0, 1.0, 1.0) */
	GR_BORDER_COLOR_WHITE             = 0x1c00,
	/* Transparent black (0.0, 0.0, 0.0, 0.0) */
	GR_BORDER_COLOR_TRANSPARENT_BLACK = 0x1c01,
	/* Opaque black (0.0, 0.0, 0.0, 1.0) */
	GR_BORDER_COLOR_OPAQUE_BLACK      = 0x1c02
} GR_BORDER_COLOR_TYPE;

/*
	The pipeline bind point.
*/
typedef enum _GR_PIPELINE_BIND_POINT
{
	/* The bind point for compute pipelines. */
	GR_PIPELINE_BIND_POINT_COMPUTE  = 0x1e00,
	/* The bind point for graphics pipelines. */
	GR_PIPELINE_BIND_POINT_GRAPHICS = 0x1e01
} GR_PIPELINE_BIND_POINT;

/*
	The bind points for the dynamic fixed-function state.
*/
typedef enum _GR_STATE_BIND_POINT
{
	/* Bind point for a viewport and scissor dynamic state. */
	GR_STATE_BIND_VIEWPORT      = 0x1f00,
	/* Bind point for a rasterizer dynamic state. */
	GR_STATE_BIND_RASTER        = 0x1f01,
	/* Bind point for a depth-stencil dynamic state. */
	GR_STATE_BIND_DEPTH_STENCIL = 0x1f02,
	/* Bind point for a color blender dynamic state. */
	GR_STATE_BIND_COLOR_BLEND   = 0x1f03,
	/* Bind point for a multisampling dynamic state. */
	GR_STATE_BIND_MSAA          = 0x1f04
} GR_STATE_BIND_POINT;

/*
	Primitive topology determines the type of the graphic primitives and vertex ordering for rendered geometry.
*/
typedef enum _GR_PRIMITIVE_TOPOLOGY
{
	/* Input geometry is a list of points. */
	GR_TOPOLOGY_POINT_LIST          = 0x2000,
	/* Input geometry is a list of lines. */
	GR_TOPOLOGY_LINE_LIST           = 0x2001,
	/* Input geometry is a line strip. */
	GR_TOPOLOGY_LINE_STRIP          = 0x2002,
	/* Input geometry is a list of triangles. */
	GR_TOPOLOGY_TRIANGLE_LIST       = 0x2003,
	/* Input geometry is a triangle strip. */
	GR_TOPOLOGY_TRIANGLE_STRIP      = 0x2004,
	/* Input geometry is a list of screen-aligned, non-clipped rectangles defined by three vertices. */
	GR_TOPOLOGY_RECT_LIST           = 0x2005,
	/* Input geometry is a list of quads. */
	GR_TOPOLOGY_QUAD_LIST           = 0x2006,
	/* Input geometry is a quad strip. */
	GR_TOPOLOGY_QUAD_STRIP          = 0x2007,
	/* Input geometry is a list of lines with adjacency information. */
	GR_TOPOLOGY_LINE_LIST_ADJ       = 0x2008,
	/* Input geometry is a line strip with adjacency information. */
	GR_TOPOLOGY_LINE_STRIP_ADJ      = 0x2009,
	/* Input geometry is a list of triangles with adjacency information. */
	GR_TOPOLOGY_TRIANGLE_LIST_ADJ   = 0x200a,
	/* Input geometry is a triangle strip with adjacency information. */
	GR_TOPOLOGY_TRIANGLE_STRIP_ADJ  = 0x200b,
	/* Input geometry is a list of tessellated patches. */
	GR_TOPOLOGY_PATCH               = 0x200c
} GR_PRIMITIVE_TOPOLOGY;

/*
	Index type defines size of the index elements.
*/
typedef enum _GR_INDEX_TYPE
{
	/* The index data are 16-bits per index. */
	GR_INDEX_16         = 0x2100,
	/* The index data are 32-bits per index. */
	GR_INDEX_32         = 0x2101
} GR_INDEX_TYPE;

/*
	The texture filter determines how sampled texture color is derived from neighboring texels.
*/
typedef enum _GR_TEX_FILTER
{
	/* Point sample for magnification, point sample for minification, and point sample for mipmap level filtering */
	GR_TEX_FILTER_MAG_POINT_MIN_POINT_MIP_POINT     = 0x2340,
	/* Linear interpolation for magnification, point sample for minification, and point sample for mipmap level filtering */
	GR_TEX_FILTER_MAG_LINEAR_MIN_POINT_MIP_POINT    = 0x2341,
	/* Point sample for magnification, linear interpolation for minification, and point sample for mipmap level filtering */
	GR_TEX_FILTER_MAG_POINT_MIN_LINEAR_MIP_POINT    = 0x2344,
	/* Linear interpolation for magnification, linear interpolation for minification, and point sample for mipmap level filtering */
	GR_TEX_FILTER_MAG_LINEAR_MIN_LINEAR_MIP_POINT   = 0x2345,
	/* Point sample for magnification, point sample for minification, and linear interpolation for mipmap level filtering */
	GR_TEX_FILTER_MAG_POINT_MIN_POINT_MIP_LINEAR    = 0x2380,
	/* Linear interpolation for magnification, point sample for minification, and linear interpolation for mipmap level filtering */
	GR_TEX_FILTER_MAG_LINEAR_MIN_POINT_MIP_LINEAR   = 0x2381,
	/* Point sample for magnification, linear interpolation for minification, and linear interpolation for mipmap level filtering */
	GR_TEX_FILTER_MAG_POINT_MIN_LINEAR_MIP_LINEAR   = 0x2384,
	/* Linear interpolation for magnification, linear interpolation for minification, linear interpolation for and mipmap level filtering */
	GR_TEX_FILTER_MAG_LINEAR_MIN_LINEAR_MIP_LINEAR  = 0x2385,
	/* Anisotropic interpolation */
	GR_TEX_FILTER_ANISOTROPIC                       = 0x238f
} GR_TEX_FILTER;

/*
	Texture address mode determines how texture coordinates outside of texture boundaries are interpreted.
*/
typedef enum _GR_TEX_ADDRESS
{
	/* Repeats the texture in a given direction. */
	GR_TEX_ADDRESS_WRAP         = 0x2400,
	/* Mirrors the texture in a given direction by flipping the texture at every other coordinate interval. */
	GR_TEX_ADDRESS_MIRROR       = 0x2401,
	/* Clamps the texture to the last edge pixel. */
	GR_TEX_ADDRESS_CLAMP        = 0x2402,
	/* Mirrors the texture just once, then clamps it. */
	GR_TEX_ADDRESS_MIRROR_ONCE  = 0x2403,
	/* Clamps the texture to the border color specified in the sampler. */
	GR_TEX_ADDRESS_CLAMP_BORDER = 0x2404
} GR_TEX_ADDRESS;

/*
	A comparison function determines how a condition that compares two values is evaluated. For depth and stencil comparison, the first value comes from source data and the second value comes from destination data.
*/
typedef enum _GR_COMPARE_FUNC
{
	/* Function never passes the comparison. */
	GR_COMPARE_NEVER            = 0x2500,
	/* The comparison passes if the first value is less than the second value. */
	GR_COMPARE_LESS             = 0x2501,
	/* The comparison passes if the first value is equal to the second value. */
	GR_COMPARE_EQUAL            = 0x2502,
	/* The comparison passes if the first value is less than or equal the second value. */
	GR_COMPARE_LESS_EQUAL       = 0x2503,
	/* The comparison passes if the first value is greater than the second value. */
	GR_COMPARE_GREATER          = 0x2504,
	/* The comparison passes if the first value is not equal to the second value. */
	GR_COMPARE_NOT_EQUAL        = 0x2505,
	/* The comparison passes if the first value is greater than or equal to the second value. */
	GR_COMPARE_GREATER_EQUAL    = 0x2506,
	/* Function always passes the comparison. */
	GR_COMPARE_ALWAYS           = 0x2507
} GR_COMPARE_FUNC;

/*
	Defines triangle rendering mode.
*/
typedef enum _GR_FILL_MODE
{
	/* Draws filled triangles. */
	GR_FILL_SOLID       = 0x2600,
	/* Draws triangles as wire-frame. */
	GR_FILL_WIREFRAME   = 0x2601
} GR_FILL_MODE;

/*
	Defines triangle facing direction used for primitive culling.
*/
typedef enum _GR_CULL_MODE
{
	/* Always draw geometry. */
	GR_CULL_NONE        = 0x2700,
	/* Cull front-facing triangles. */
	GR_CULL_FRONT       = 0x2701,
	/* Cull back-facing triangles. */
	GR_CULL_BACK        = 0x2702
} GR_CULL_MODE;

/*
	Defines front-facing triangle orientation to be used for culling.
*/
typedef enum _GR_FACE_ORIENTATION
{
	/* A triangle is front-facing if vertices are oriented counter-clockwise. */
	GR_FRONT_FACE_CCW   = 0x2800,
	/* A triangle is front-facing if vertices are oriented clockwise. */
	GR_FRONT_FACE_CW    = 0x2801
} GR_FACE_ORIENTATION;

/*
	Blend factors define how source and destination parts of the blend equation are computed.
*/
typedef enum _GR_BLEND
{
	/* Blend factor is set to black color (0,0,0,0). */
	GR_BLEND_ZERO                       = 0x2900,
	/* Blend factor is set to white color (1,1,1,1). */
	GR_BLEND_ONE                        = 0x2901,
	/* Blend factor is set to the source color coming from a pixel shader (RGB). */
	GR_BLEND_SRC_COLOR                  = 0x2902,
	/* Blend factor is set to the inverted source color coming from a pixel shader (1-RGB). */
	GR_BLEND_ONE_MINUS_SRC_COLOR        = 0x2903,
	/* Blend factor is set to the destination color coming from a target image (RGB). */
	GR_BLEND_DEST_COLOR                 = 0x2904,
	/* Blend factor is set to the inverted destination color coming from a target image (RGB). */
	GR_BLEND_ONE_MINUS_DEST_COLOR       = 0x2905,
	/* Blend factor is set to the source alpha coming from a pixel shader (A). */
	GR_BLEND_SRC_ALPHA                  = 0x2906,
	/* Blend factor is set to the inverted source alpha coming from a pixel shader (1-A). */
	GR_BLEND_ONE_MINUS_SRC_ALPHA        = 0x2907,
	/* Blend factor is set to the destination alpha coming from a target image (A). */
	GR_BLEND_DEST_ALPHA                 = 0x2908,
	/* Blend factor is set to the inverted destination alpha coming from a target image (1-A). */
	GR_BLEND_ONE_MINUS_DEST_ALPHA       = 0x2909,
	/* Blend factor is set to the constant color specified in blend state (blendConstRGB). */
	GR_BLEND_CONSTANT_COLOR             = 0x290a,
	/* Blend factor is set to the inverted constant color specified in blend state (1-blendConstRGB). */
	GR_BLEND_ONE_MINUS_CONSTANT_COLOR   = 0x290b,
	/* Blend factor is set to the constant alpha specified in blend state (blendConstA). */
	GR_BLEND_CONSTANT_ALPHA             = 0x290c,
	/* Blend factor is set to the inverted constant alpha specified in blend state (1-blendConstA). */
	GR_BLEND_ONE_MINUS_CONSTANT_ALPHA   = 0x290d,
	/* Blend factor is set to the source alpha coming from a pixel shader (A) clamped to an inverted destination alpha coming from a target image. */
	GR_BLEND_SRC_ALPHA_SATURATE         = 0x290e,
	/* Blend factor is set to the second source color coming from a pixel shader (RGB1). Used for dual source blend mode. */
	GR_BLEND_SRC1_COLOR                 = 0x290f,
	/* Blend factor is set to the inverted second source color coming from a pixel shader (1-RGB1). Used for dual source blend mode. */
	GR_BLEND_ONE_MINUS_SRC1_COLOR       = 0x2910,
	/* Blend factor is set to the second source alpha coming from a pixel shader (A1). Used for dual source blend mode. */
	GR_BLEND_SRC1_ALPHA                 = 0x2911,
	/* Blend factor is set to the inverted second source alpha coming from a pixel shader (1-A1). Used for dual source blend mode. */
	GR_BLEND_ONE_MINUS_SRC1_ALPHA       = 0x2912
} GR_BLEND;

/*
	Defines blend function in a blend equation.
*/
typedef enum _GR_BLEND_FUNC
{
	/* Add source and destination parts of a blend equation. */
	GR_BLEND_FUNC_ADD               = 0x2a00,
	/* Subtract destination part of a blend equation from source. */
	GR_BLEND_FUNC_SUBTRACT          = 0x2a01,
	/* Subtract source part of a blend equation from destination. */
	GR_BLEND_FUNC_REVERSE_SUBTRACT  = 0x2a02,
	/* Compute minimum of source and destination parts of a blend equation. */
	GR_BLEND_FUNC_MIN               = 0x2a03,
	/* Compute maximum of source and destination parts of a blend equation. */
	GR_BLEND_FUNC_MAX               = 0x2a04
} GR_BLEND_FUNC;

/*
	Defines a stencil operation performed during a stencil test.
*/
typedef enum _GR_STENCIL_OP
{
	/* Keeps the stencil unchanged. */
	GR_STENCIL_OP_KEEP      = 0x2b00,
	/* Sets the stencil data to zero. */
	GR_STENCIL_OP_ZERO      = 0x2b01,
	/* Sets the stencil data to a reference value. */
	GR_STENCIL_OP_REPLACE   = 0x2b02,
	/* Increments the stencil data and clamps the result. */
	GR_STENCIL_OP_INC_CLAMP = 0x2b03,
	/* Decrements the stencil data and clamps the result. */
	GR_STENCIL_OP_DEC_CLAMP = 0x2b04,
	/* Inverts the stencil data. */
	GR_STENCIL_OP_INVERT    = 0x2b05,
	/* Increments the stencil data and wraps the result. */
	GR_STENCIL_OP_INC_WRAP  = 0x2b06,
	/* Decrements the stencil data and wraps the result. */
	GR_STENCIL_OP_DEC_WRAP  = 0x2b07
} GR_STENCIL_OP;

/*
	Defines a logical operation applied between the color coming from pixel shader and the value in the target image.
*/
typedef enum _GR_LOGIC_OP
{
	/* Writes the value coming from pixel shader. GR_LOGIC_OP_COPY effectively disables logic operations. */
	GR_LOGIC_OP_COPY            = 0x2c00,
	/* Writes the zero value. */
	GR_LOGIC_OP_CLEAR           = 0x2c01,
	/* Performs a logical AND between the value coming from pixel shader and the destination value. */
	GR_LOGIC_OP_AND             = 0x2c02,
	/* Performs a logical AND between the value coming from pixel shader and the inverse of the destination value. */
	GR_LOGIC_OP_AND_REVERSE     = 0x2c03,
	/* Performs a logical AND between the inverse of value coming from pixel shader and the destination value. */
	GR_LOGIC_OP_AND_INVERTED    = 0x2c04,
	/* Preserves the original target value. */
	GR_LOGIC_OP_NOOP            = 0x2c05,
	/* Performs a logical XOR between the value coming from pixel shader and the destination value. */
	GR_LOGIC_OP_XOR             = 0x2c06,
	/* Performs a logical OR between the value coming from pixel shader and the destination value. */
	GR_LOGIC_OP_OR              = 0x2c07,
	/* Performs a logical NOR between the value coming from pixel shader and the destination value. */
	GR_LOGIC_OP_NOR             = 0x2c08,
	/* Performs an equivalency test between the value coming from pixel shader and the destination value. */
	GR_LOGIC_OP_EQUIV           = 0x2c09,
	/* Writes the inverted destination value. */
	GR_LOGIC_OP_INVERT          = 0x2c0a,
	/* Performs a logical OR between the value coming from pixel shader and the inverse of the destination value. */
	GR_LOGIC_OP_OR_REVERSE      = 0x2c0b,
	/* Writes the inverted value coming from pixel shader. */
	GR_LOGIC_OP_COPY_INVERTED   = 0x2c0c,
	/* Performs a logical OR between the inverse of value coming from pixel shader and the destination value. */
	GR_LOGIC_OP_OR_INVERTED     = 0x2c0d,
	/* Performs a logical AND between the value coming from pixel shader and the destination value. */
	GR_LOGIC_OP_NAND            = 0x2c0e,
	/* Writes a value with all bits set to 1. */
	GR_LOGIC_OP_SET             = 0x2c0f
} GR_LOGIC_OP;

/*
	Defines a memory atomic operation that can be performed from command buffers.
*/
typedef enum _GR_ATOMIC_OP
{
	/* destData = destData + srcData */
	GR_ATOMIC_ADD_INT32     = 0x2d00,
	/* destData = destData - srcData */
	GR_ATOMIC_SUB_INT32     = 0x2d01,
	/* destData = (srcData < destData) ? srcData : destData, unsigned */
	GR_ATOMIC_MIN_UINT32    = 0x2d02,
	/* destData = (srcData > destData) ? srcData : destData, unsigned */
	GR_ATOMIC_MAX_UINT32    = 0x2d03,
	/* destData = (srcData < destData) ? srcData : destData, signed */
	GR_ATOMIC_MIN_SINT32    = 0x2d04,
	/* destData = (srcData > destData) ? srcData : destData, signed */
	GR_ATOMIC_MAX_SINT32    = 0x2d05,
	/* destData = srcData & destData */
	GR_ATOMIC_AND_INT32     = 0x2d06,
	/* destData = srcData | destData */
	GR_ATOMIC_OR_INT32      = 0x2d07,
	/* destData = srcData ^ destData */
	GR_ATOMIC_XOR_INT32     = 0x2d08,
	/* destData = (destData >= srcData) ? 0 : (destData + 1), unsigned */
	GR_ATOMIC_INC_UINT32    = 0x2d09,
	/* destData = ((destData == 0) || (destData > srcData)) ? srcData : (destData - 1), unsigned */
	GR_ATOMIC_DEC_UINT32    = 0x2d0a,
	/* destData = destData + srcData */
	GR_ATOMIC_ADD_INT64     = 0x2d0b,
	/* destData = destData - srcData */
	GR_ATOMIC_SUB_INT64     = 0x2d0c,
	/* destData = (srcData < destData) ? srcData : destData, unsigned */
	GR_ATOMIC_MIN_UINT64    = 0x2d0d,
	/* destData = (srcData > destData) ? srcData : destData, unsigned */
	GR_ATOMIC_MAX_UINT64    = 0x2d0e,
	/* destData = (srcData < destData) ? srcData : destData, signed */
	GR_ATOMIC_MIN_SINT64    = 0x2d0f,
	/* destData = (srcData > destData) ? srcData : destData, signed */
	GR_ATOMIC_MAX_SINT64    = 0x2d10,
	/* destData = srcData & destData */
	GR_ATOMIC_AND_INT64     = 0x2d11,
	/* destData = srcData | destData */
	GR_ATOMIC_OR_INT64      = 0x2d12,
	/* destData = srcData ^ destData */
	GR_ATOMIC_XOR_INT64     = 0x2d13,
	/* destData = (destData >= srcData) ? 0 : (destData + 1) , unsigned */
	GR_ATOMIC_INC_UINT64    = 0x2d14,
	/* destData = ((destData == 0) || (destData > srcData)) ? srcData : (destData - 1) , unsigned */
	GR_ATOMIC_DEC_UINT64    = 0x2d15
} GR_ATOMIC_OP;

/*
	Defines the system memory allocation type reported in allocator callback.
*/
typedef enum _GR_SYSTEM_ALLOC_TYPE
{
	/* The allocation is used for an API object or for other data that share the lifetime of an API object. */
	GR_SYSTEM_ALLOC_API_OBJECT      = 0x2e00,
	/* The allocation is used for an internal structure that driver expects to be relatively long-lived. */
	GR_SYSTEM_ALLOC_INTERNAL        = 0x2e01,
	/* The allocation is used for an internal structure that driver expects to be short-lived. A general lifetime expectancy for this allocation type is the duration of an API call. */
	GR_SYSTEM_ALLOC_INTERNAL_TEMP   = 0x2e02,
	/* The allocation is used for an internal structure used for shader compilation that driver expects to be short-lived. A general lifetime expectancy for this allocation type is the duration of pipeline creation call. */
	GR_SYSTEM_ALLOC_INTERNAL_SHADER = 0x2e03,
	/* The allocation is used for validation layer internal data other than API objects. */
	GR_SYSTEM_ALLOC_DEBUG           = 0x2e04
} GR_SYSTEM_ALLOC_TYPE;

/*
	Defines the type of memory heap.
*/
typedef enum _GR_HEAP_MEMORY_TYPE
{
	/* Heap memory type that does not belong to any other category. */
	GR_HEAP_MEMORY_OTHER    = 0x2f00,
	/* Heap represents local video memory. */
	GR_HEAP_MEMORY_LOCAL    = 0x2f01,
	/* Heap represents remote (non-local) video memory. */
	GR_HEAP_MEMORY_REMOTE   = 0x2f02,
	/* Heap represents memory physically connected to the GPU (e.g., on-chip memory). */
	GR_HEAP_MEMORY_EMBEDDED = 0x2f03
} GR_HEAP_MEMORY_TYPE;

/*
	Defines the physical GPU type.
*/
typedef enum _GR_PHYSICAL_GPU_TYPE
{
	/* The GPU type that does not belong to any other category. */
	GR_GPU_TYPE_OTHER                = 0x3000,
	/* An integrated GPU, which is part of the APU. */
	GR_GPU_TYPE_INTEGRATED           = 0x3001,
	/* A discrete GPU. */
	GR_GPU_TYPE_DISCRETE             = 0x3002,
	/* A virtual GPU. */
	GR_GPU_TYPE_VIRTUAL              = 0x3003
} GR_PHYSICAL_GPU_TYPE;

/*
	Defines types of information that can be retrieved from different objects.
*/
typedef enum _GR_INFO_TYPE
{
	/* Retrieves physical GPU information with grGetGpuInfo(). */
	GR_INFO_TYPE_PHYSICAL_GPU_PROPERTIES            = 0x6100,
	/* Retrieves physical GPU performance information with grGetGpuInfo(). */
	GR_INFO_TYPE_PHYSICAL_GPU_PERFORMANCE           = 0x6101,
	/* Retrieves information about all queues available in a physical GPU with grGetGpuInfo(). */
	GR_INFO_TYPE_PHYSICAL_GPU_QUEUE_PROPERTIES      = 0x6102,
	/* Retrieves information about memory management capabilities for a physical GPU with grGetGpuInfo(). */
	GR_INFO_TYPE_PHYSICAL_GPU_MEMORY_PROPERTIES     = 0x6103,
	/* Retrieves information about image capabilities for a physical GPU with grGetGpuInfo(). */
	GR_INFO_TYPE_PHYSICAL_GPU_IMAGE_PROPERTIES      = 0x6104,
	/* Retrieves GPU memory heap information with grGetMemoryHeapInfo(). */
	GR_INFO_TYPE_MEMORY_HEAP_PROPERTIES             = 0x6200,
	/* Retrieves information on format properties with grGetFormatInfo(). */
	GR_INFO_TYPE_FORMAT_PROPERTIES                  = 0x6300,
	/* Retrieves information about image subresource layout with grGetImageSubresourceInfo(). */
	GR_INFO_TYPE_SUBRESOURCE_LAYOUT                 = 0x6400,
	/* Retrieves information about object GPU memory requirements with grGetObjectInfo(). Valid for all object types that can have memory requirements. */
	GR_INFO_TYPE_MEMORY_REQUIREMENTS                = 0x6800,
	/* Retrieves parent device handle for API objects with grGetObjectInfo(). */
	GR_INFO_TYPE_PARENT_DEVICE                      = 0x6801,
	/* Retrieves a parent physical GPU handle for the Mantle device with grGetObjectInfo(). */
	GR_INFO_TYPE_PARENT_PHYSICAL_GPU                = 0x6802
} GR_INFO_TYPE;

/*
	Defines a level of validation.
*/
typedef enum _GR_VALIDATION_LEVEL
{
	/* At this validation level, trivial API checks are performed (e.g., checking function parameters). This is the default level of checks without the validation level. At this level command buffer, building is not validated. */
	GR_VALIDATION_LEVEL_0   = 0x8000,
	/* Level 1 validation adds checks that do not require command buffer analysis or knowledge of the execution-time memory layout. At this level, command buffer building is partially validated. */
	GR_VALIDATION_LEVEL_1   = 0x8001,
	/* Level 2 validation adds command buffer checks that depend on submission-time analysis of command buffer contents, but have no knowledge of the execution-time memory layout. */
	GR_VALIDATION_LEVEL_2   = 0x8002,
	/* Level 3 validation adds checks that require relatively lightweight analysis of execution-time memory layout. */
	GR_VALIDATION_LEVEL_3   = 0x8003,
	/* Level 4 validation adds checks that require full analysis of execution-time memory layout. */
	GR_VALIDATION_LEVEL_4   = 0x8004
} GR_VALIDATION_LEVEL;

/*
	Return values
*/
typedef enum _GR_RESULT {
	/* The API call successfully completed. */
	GR_SUCCESS                              = 0x10000,
	/* The API call successfully completed, but the requested feature is not available. */
	GR_UNSUPPORTED                          = 0x10001,
	/* The API call successfully completed, but the result of the operation is not ready. */
	GR_NOT_READY                            = 0x10002,
	/* The wait operation completed due to a timeout condition. */
	GR_TIMEOUT                              = 0x10003,
	/* The event is in the "set" state. */
	GR_EVENT_SET                            = 0x10004,
	/* The event is in the "reset" state. */
	GR_EVENT_RESET                          = 0x10005,

	/* An unknown error was encountered during the operation. */
	GR_ERROR_UNKNOWN                        = 0x11000,
	/* The requested operation is unavailable at this time. */
	GR_ERROR_UNAVAILABLE                    = 0x11001,
	/* Cannot initialize the Mantle device or driver. */
	GR_ERROR_INITIALIZATION_FAILED          = 0x11002,
	/* Cannot complete the operation due to insufficient system memory. */
	GR_ERROR_OUT_OF_MEMORY                  = 0x11003,
	/* Cannot complete the operation due to insufficient video memory. */
	GR_ERROR_OUT_OF_GPU_MEMORY              = 0x11004,
	/* Cannot create a device since there is already an active device for the same physical GPU. */
	GR_ERROR_DEVICE_ALREADY_CREATED         = 0x11005,
	/* The device was lost due to its removal or possible hang and recovery condition. */
	GR_ERROR_DEVICE_LOST                    = 0x11006,
	/* An invalid pointer was passed to the call. */
	GR_ERROR_INVALID_POINTER                = 0x11007,
	/* An invalid value was passed to the call. */
	GR_ERROR_INVALID_VALUE                  = 0x11008,
	/* An invalid API object handle was passed to the call. */
	GR_ERROR_INVALID_HANDLE                 = 0x11009,
	/* An invalid ordinal value was passed to the call. */
	GR_ERROR_INVALID_ORDINAL                = 0x1100a,
	/* An invalid memory size was specified as an input parameter for the operation. */
	GR_ERROR_INVALID_MEMORY_SIZE            = 0x1100b,
	/* An invalid extension was requested during device creation. */
	GR_ERROR_INVALID_EXTENSION              = 0x1100c,
	/* Invalid flags were passed to the call. */
	GR_ERROR_INVALID_FLAGS                  = 0x1100d,
	/* An invalid alignment was specified for the requested operation. */
	GR_ERROR_INVALID_ALIGNMENT              = 0x1100e,
	/* An invalid resource format was specified. */
	GR_ERROR_INVALID_FORMAT                 = 0x1100f,
	/* The requested operation cannot be performed on the provided image object. */
	GR_ERROR_INVALID_IMAGE                  = 0x11010,
	/* The descriptor set data are invalid or does not match pipeline expectations. */
	GR_ERROR_INVALID_DESCRIPTOR_SET_DATA    = 0x11011,
	/* An invalid queue type was specified for the requested operation. */
	GR_ERROR_INVALID_QUEUE_TYPE             = 0x11012,
	/* An invalid object type was specified for the requested operation. */
	GR_ERROR_INVALID_OBJECT_TYPE            = 0x11013,
	/* Unsupported shader IL version. */
	GR_ERROR_UNSUPPORTED_SHADER_IL_VERSION  = 0x11014,
	/* Corrupt or invalid shader code detected. */
	GR_ERROR_BAD_SHADER_CODE                = 0x11015,
	/* Invalid pipeline data are detected. */
	GR_ERROR_BAD_PIPELINE_DATA              = 0x11016,
	/* Too many memory references are used for this queue operation. */
	GR_ERROR_TOO_MANY_MEMORY_REFERENCES     = 0x11017,
	/* The memory object cannot be mapped as it does not reside in a CPU visible heap. */
	GR_ERROR_NOT_MAPPABLE                   = 0x11018,
	/* The map operation failed due to an unknown or system reason. */
	GR_ERROR_MEMORY_MAP_FAILED              = 0x11019,
	/* The unmap operation failed due to an unknown or system reason. */
	GR_ERROR_MEMORY_UNMAP_FAILED            = 0x1101a,
	/* The pipeline load operation failed due to an incompatible device. */
	GR_ERROR_INCOMPATIBLE_DEVICE            = 0x1101b,
	/* The pipeline load operation failed due to an incompatible driver version. */
	GR_ERROR_INCOMPATIBLE_DRIVER            = 0x1101c,
	/* The requested operation cannot be completed due to an incomplete command buffer construction. */
	GR_ERROR_INCOMPLETE_COMMAND_BUFFER      = 0x1101d,
	/* The requested operation cannot be completed due to a failed command buffer construction. */
	GR_ERROR_BUILDING_COMMAND_BUFFER        = 0x1101e,
	/* The operation cannot complete since not all objects have valid memory bound to them. */
	GR_ERROR_MEMORY_NOT_BOUND               = 0x1101f,
	/* The requested operation failed due to incompatible queue type. */
	GR_ERROR_INCOMPATIBLE_QUEUE             = 0x11020,
	/* The object cannot be created or opened for sharing between multiple GPU devices. */
	GR_ERROR_NOT_SHAREABLE                  = 0x11021
} GR_RESULT;




/*
===============================================================================
###############################################################################

	CORE FLAGS

###############################################################################
===============================================================================
*/

/*
	Optional hints to specify command buffer building optimizations.
*/
typedef enum _GR_CMD_BUFFER_BUILD_FLAGS
{
	/* Optimize command buffer building for a large number of draw or dispatch operations that are GPU front-end limited. Optimization might increase CPU overhead during command buffer building. */
	GR_CMD_BUFFER_OPTIMIZE_GPU_SMALL_BATCH       = 0x00000001,
	/* Optimize command buffer building for the case of frequent pipeline switching. Optimization might increase CPU overhead during command buffer building. */
	GR_CMD_BUFFER_OPTIMIZE_PIPELINE_SWITCH       = 0x00000002,
	/* Optimizes command buffer building for single command buffer submission. Command buffers built with this flag cannot be submitted more than once. */
	GR_CMD_BUFFER_OPTIMIZE_ONE_TIME_SUBMIT       = 0x00000004,
	/* Optimizes command buffer building for the case of frequent descriptor set switching. Optimization might increase CPU overhead during command buffer building. */
	GR_CMD_BUFFER_OPTIMIZE_DESCRIPTOR_SET_SWITCH = 0x00000008
} GR_CMD_BUFFER_BUILD_FLAGS;

/*
	Depth-stencil view creation flags.
*/
typedef enum _GR_DEPTH_STENCIL_VIEW_CREATE_FLAGS
{
	/* Depth-stencil view has depth that is available for read-only access. */
	GR_DEPTH_STENCIL_VIEW_CREATE_READ_ONLY_DEPTH   = 0x00000001,
	/* Depth-stencil view has stencil that is available for read-only access. */
	GR_DEPTH_STENCIL_VIEW_CREATE_READ_ONLY_STENCIL = 0x00000002
} GR_DEPTH_STENCIL_VIEW_CREATE_FLAGS;

/*
	Device creation flags.
*/
typedef enum _GR_DEVICE_CREATE_FLAGS
{
	/* Enables validation layer for the device. */
	GR_DEVICE_CREATE_VALIDATION = 0x00000001
} GR_DEVICE_CREATE_FLAGS;

/*
	Format capability flags for images and memory views.
*/
typedef enum _GR_FORMAT_FEATURE_FLAGS
{
	/* Images of this format can be accessed in shaders for read operations. */
	GR_FORMAT_IMAGE_SHADER_READ    = 0x00000001,
	/* Images of this format can be accessed in shaders for write operations. */
	GR_FORMAT_IMAGE_SHADER_WRITE   = 0x00000002,
	/* Images of this format could be used as source or destination for image copy operations. */
	GR_FORMAT_IMAGE_COPY           = 0x00000004,
	/* Memory views of this format can be accessed in shaders for read or write operations. */
	GR_FORMAT_MEMORY_SHADER_ACCESS = 0x00000008,
	/* Images of this format can be used as color targets. */
	GR_FORMAT_COLOR_TARGET_WRITE   = 0x00000010,
	/* Images of this format can be used as blendable color targets. */
	GR_FORMAT_COLOR_TARGET_BLEND   = 0x00000020,
	/* Images of this format can be used as depth targets. */
	GR_FORMAT_DEPTH_TARGET         = 0x00000040,
	/* Images of this format can be used as stencil targets. */
	GR_FORMAT_STENCIL_TARGET       = 0x00000080,
	/* Images of this format support multisampling. */
	GR_FORMAT_MSAA_TARGET          = 0x00000100,
	/* Images of this format support format conversion on image copy operations. */
	GR_FORMAT_CONVERSION           = 0x00000200
} GR_FORMAT_FEATURE_FLAGS;

/*
	GPU compatibility flags for multi-device configurations.
*/
typedef enum _GR_GPU_COMPATIBILITY_FLAGS
{
	/* GPUs have compatible ASIC features (exactly the same internal tiling, the same pipeline binary data, etc.). */
	GR_GPU_COMPAT_ASIC_FEATURES       = 0x00000001,
	/* GPUs can generate images with similar image quality. */
	GR_GPU_COMPAT_IQ_MATCH            = 0x00000002,
	/* GPUs support peer-to-peer transfers over the PCIe. */
	GR_GPU_COMPAT_PEER_WRITE_TRANSFER = 0x00000004,
	/* GPUs can share some memory objects. */
	GR_GPU_COMPAT_SHARED_MEMORY       = 0x00000008,
	/* GPUs can share queue semaphores. */
	GR_GPU_COMPAT_SHARED_SYNC         = 0x00000010,
	/* GPU1 can create a presentable image on a display connected to GPU0. */
	GR_GPU_COMPAT_SHARED_GPU0_DISPLAY = 0x00000020,
	/* GPU0 can create a presentable image on a display connected to GPU1. */
	GR_GPU_COMPAT_SHARED_GPU1_DISPLAY = 0x00000040
} GR_GPU_COMPATIBILITY_FLAGS;

/*
	Image creation flags.
*/
typedef enum _GR_IMAGE_CREATE_FLAGS
{
	/* Images of exactly the same creation parameters are guaranteed to have consistent data layout. */
	GR_IMAGE_CREATE_INVARIANT_DATA     = 0x00000001,
	/* Image can be used as a source or destination for cloning operation. */
	GR_IMAGE_CREATE_CLONEABLE          = 0x00000002,
	/* Image can be shared between compatible devices. */
	GR_IMAGE_CREATE_SHAREABLE          = 0x00000004,
	/* Image can have its format changed in image or color target views. */
	GR_IMAGE_CREATE_VIEW_FORMAT_CHANGE = 0x00000008
} GR_IMAGE_CREATE_FLAGS;

/*
	Image usage flags.
*/
typedef enum _GR_IMAGE_USAGE_FLAGS
{
	/* Image will be bound to shaders for read access. */
	GR_IMAGE_USAGE_SHADER_ACCESS_READ  = 0x00000001,
	/* Image will be bound to shaders for write access. Only applies to direct image writes from shaders; it is not required for targets. */
	GR_IMAGE_USAGE_SHADER_ACCESS_WRITE = 0x00000002,
	/* Image will be used as a color target. Used for color target output and blending. */
	GR_IMAGE_USAGE_COLOR_TARGET        = 0x00000004,
	/* Image will be used as a depth-stencil target. */
	GR_IMAGE_USAGE_DEPTH_STENCIL       = 0x00000008
} GR_IMAGE_USAGE_FLAGS;

/*
	Memory allocation flags.
*/
typedef enum _GR_MEMORY_ALLOC_FLAGS
{
	/* Memory object represents a virtual allocation. */
	GR_MEMORY_ALLOC_VIRTUAL   = 0x00000001,
	/* Memory object can be shared between compatible devices. */
	GR_MEMORY_ALLOC_SHAREABLE = 0x00000002
} GR_MEMORY_ALLOC_FLAGS;

/*
	GPU memory heap property flags.
*/
typedef enum _GR_MEMORY_HEAP_FLAGS
{
	/* Memory heap is in a CPU address space and is CPU accessible through map mechanism. */
	GR_MEMORY_HEAP_CPU_VISIBLE        = 0x00000001,
	/* Memory heap is cache coherent between the CPU and GPU. */
	GR_MEMORY_HEAP_CPU_GPU_COHERENT   = 0x00000002,
	/* Memory heap is not cached by the CPU, but it could still be cached by the GPU. */
	GR_MEMORY_HEAP_CPU_UNCACHED       = 0x00000004,
	/* Memory heap is write-combined by the CPU. */
	GR_MEMORY_HEAP_CPU_WRITE_COMBINED = 0x00000008,
	/* All pinned memory objects behave as if they were created in a heap marked with this flag. Only one heap has this flag set. */
	GR_MEMORY_HEAP_HOLDS_PINNED       = 0x00000010,
	/* Memory heap can be used for memory objects that can be shared between multiple GPUs. */
	GR_MEMORY_HEAP_SHAREABLE          = 0x00000020
} GR_MEMORY_HEAP_FLAGS;

/*
	Flags for GPU memory system properties for the physical GPU.
*/
typedef enum _GR_MEMORY_PROPERTY_FLAGS
{
	/* The GPU memory manager supports dynamic memory object migration. */
	GR_MEMORY_MIGRATION_SUPPORT         = 0x00000001,
	/* The GPU memory manager supports virtual memory remapping. */
	GR_MEMORY_VIRTUAL_REMAPPING_SUPPORT = 0x00000002,
	/* The GPU memory manager supports pinning of system memory. */
	GR_MEMORY_PINNING_SUPPORT           = 0x00000004,
	/* When set, the application should prefer using global memory references instead of per command buffer memory references for CPU performance reasons. */
	GR_MEMORY_PREFER_GLOBAL_REFS        = 0x00000008
} GR_MEMORY_PROPERTY_FLAGS;

/*
	Flags for GPU memory object references used for command buffer submission.
*/
typedef enum _GR_MEMORY_REF_FLAGS
{
	/* GPU memory object is only used for read-only access in the submitted command buffers. */
	GR_MEMORY_REF_READ_ONLY = 0x00000001
} GR_MEMORY_REF_FLAGS;

/*
	Pipeline creation flags.
*/
typedef enum _GR_PIPELINE_CREATE_FLAGS
{
	/* Disables pipeline link-time optimizations. Should only be used for debugging. */
	GR_PIPELINE_CREATE_DISABLE_OPTIMIZATION = 0x00000001
} GR_PIPELINE_CREATE_FLAGS;

/*
	Flags for controlling GPU query behavior.
*/
typedef enum _GR_QUERY_CONTROL_FLAGS
{
	/* Controls accuracy of query data collection. Available only for occlusion queries. If set, the occlusion query is guaranteed to return an imprecise non-zero value in case any of the samples pass a depth and stencil test. Using imprecise occlusion query results could improve rendering performance while an occlusion query is active. */
	GR_QUERY_IMPRECISE_DATA = 0x00000001
} GR_QUERY_CONTROL_FLAGS;

/*
	Queue semaphore creation flags.
*/
typedef enum _GR_SEMAPHORE_CREATE_FLAGS
{
	/* Queue semaphore can be shared between compatible devices. */
	GR_SEMAPHORE_CREATE_SHAREABLE = 0x00000001
} GR_SEMAPHORE_CREATE_FLAGS;

/*
	Shader creation flags.
*/
typedef enum _GR_SHADER_CREATE_FLAGS
{
	/* Pixel shader can have Re-Z enabled (applicable to pixel shaders only). */
	GR_SHADER_CREATE_ALLOW_RE_Z = 0x00000001
} GR_SHADER_CREATE_FLAGS;




/*
===============================================================================
###############################################################################

	CORE CALLBACKS

###############################################################################
===============================================================================
*/

/*
	Application callback to allocate a block of system memory.
*/
typedef GR_VOID* (GR_STDCALL *GR_ALLOC_FUNCTION)(
	/* System memory allocation size in bytes. */
	GR_SIZE size,
	/* Allocation requirements in bytes. */
	GR_SIZE alignment,
	/* System memory allocation type. See GR_SYSTEM_ALLOC_TYPE. */
	GR_ENUM allocType);

/*
	Application callback to free a block of system memory.
*/
typedef GR_VOID (GR_STDCALL *GR_FREE_FUNCTION)(
	/* System memory allocation to free. The allocation was previously created through the GR_ALLOC_FUNCTION callback. */
	GR_VOID* pMem);




/*
===============================================================================
###############################################################################

	CORE DATA STRUCTURES

###############################################################################
===============================================================================
*/

/*
	Application provided callbacks for system memory allocations inside of the Mantle driver.
*/
typedef struct _GR_ALLOC_CALLBACKS
{
	/* [in] An allocation provided callback to allocate system memory inside the Mantle driver. See GR_ALLOC_FUNCTION. */
	GR_ALLOC_FUNCTION pfnAlloc;
	/* [in] An application provided callback to free system memory inside the Mantle driver. See GR_FREE_FUNCTION. */
	GR_FREE_FUNCTION  pfnFree;
} GR_ALLOC_CALLBACKS;

/*
	Application identification information that can be communicated by the application to the driver.
*/
typedef struct _GR_APPLICATION_INFO
{
	/* [in] A string with the name of the applications. */
	const GR_CHAR* pAppName;
	/* The version of the application encoded using GR_MAKE_VERSION macro. */
	GR_UINT32      appVersion;
	/* [in] A string with the engine name. */
	const GR_CHAR* pEngineName;
	/* The engine version encoded using the GR_MAKE_VERSION macro. */
	GR_UINT32      engineVersion;
	/* The API version to which the application is compiled; encoded using the GR_MAKE_VERSION macro. */
	GR_UINT32      apiVersion;
} GR_APPLICATION_INFO;

/*
	Channel mapping for image views.
*/
typedef struct _GR_CHANNEL_MAPPING
{
	/* Swizzle for red channel. See GR_CHANNEL_SWIZZLE. */
	GR_ENUM r;
	/* Swizzle for green channel. See GR_CHANNEL_SWIZZLE. */
	GR_ENUM g;
	/* Swizzle for blue channel. See GR_CHANNEL_SWIZZLE. */
	GR_ENUM b;
	/* Swizzle for alpha channel. See GR_CHANNEL_SWIZZLE. */
	GR_ENUM a;
} GR_CHANNEL_MAPPING;

/*
	Command buffer creation information.
*/
typedef struct _GR_CMD_BUFFER_CREATE_INFO
{
	/* Queue type the command buffer is prepared for. See GR_QUEUE_TYPE. */
	GR_ENUM  queueType;
	/* Reserved, must be zero. */
	GR_FLAGS flags;
} GR_CMD_BUFFER_CREATE_INFO;

/*
	Per target dynamic color blender state object creation information.
*/
typedef struct _GR_COLOR_TARGET_BLEND_STATE
{
	/* Per color target blending operation enable. */
	GR_BOOL blendEnable;
	/* Source part of the blend equation for color. See GR_BLEND. */
	GR_ENUM srcBlendColor;
	/* Destination part of the blend equation for color. See GR_BLEND. */
	GR_ENUM destBlendColor;
	/* Blend function for color. See GR_BLEND_FUNC. */
	GR_ENUM blendFuncColor;
	/* Source part of the blend equation for alpha. See GR_BLEND. */
	GR_ENUM srcBlendAlpha;
	/* Destination part of the blend equation for alpha. See GR_BLEND. */
	GR_ENUM destBlendAlpha;
	/* Blend function for alpha. See GR_BLEND_FUNC. */
	GR_ENUM blendFuncAlpha;
} GR_COLOR_TARGET_BLEND_STATE;

/*
	Dynamic color blender state object creation information.
*/
typedef struct _GR_COLOR_BLEND_STATE_CREATE_INFO
{
	/* Array of blender state per color target. See GR_COLOR_TARGET_BLEND_STATE. */
	GR_COLOR_TARGET_BLEND_STATE target[GR_MAX_COLOR_TARGETS];
	/* Constant color value to use for blending. */
	GR_FLOAT                    blendConst[4];
} GR_COLOR_BLEND_STATE_CREATE_INFO;

/*
	Per color target information for binding it to command buffer state.
*/
typedef struct _GR_COLOR_TARGET_BIND_INFO
{
	/* Color target view to bind. */
	GR_COLOR_TARGET_VIEW view;
	/* Color target view image state at the draw time. See GR_IMAGE_STATE. */
	GR_ENUM              colorTargetState;
} GR_COLOR_TARGET_BIND_INFO;

/*
	Image or memory view format.
*/
typedef struct _GR_FORMAT
{
	/* The channel format. See GR_CHANNEL_FORMAT. */
	GR_UINT32 channelFormat : 16;
	/* The numeric format. See GR_NUM_FORMAT. */
	GR_UINT32 numericFormat : 16;
} GR_FORMAT;

/*
	Color target view creation information.
*/
typedef struct _GR_COLOR_TARGET_VIEW_CREATE_INFO
{
	/* Image for the view. */
	GR_IMAGE  image;
	/* Format for the view. Has to be compatible with the image format. See GR_FORMAT. */
	GR_FORMAT format;
	/* Mipmap level to render. */
	GR_UINT   mipLevel;
	/* First array slice for 2D array resources, or first depth slice for 3D image resources. */
	GR_UINT   baseArraySlice;
	/* Number of array slice for 2D array resources, or number of depth slices for 3D image resources. */
	GR_UINT   arraySize;
} GR_COLOR_TARGET_VIEW_CREATE_INFO;

/*
	Mapping of descriptor slot to the shader IL entities.
*/
typedef struct _GR_DESCRIPTOR_SLOT_INFO
{
	/* The object type a pipeline expects to see in the descriptor set at the draw time. See GR_DESCRIPTOR_SET_SLOT_TYPE. */
	GR_ENUM slotObjectType;
	union
	{
		/* The shader entity index, if the slot object type references one of the shader entities. */
		GR_UINT                                  shaderEntityIndex;
		/* The pointer to the next level of descriptor set mapping information, if the slot object type references a nested descriptor set (for hierarchical descriptor sets). See GR_DESCRIPTOR_SET_MAPPING. */
		const struct _GR_DESCRIPTOR_SET_MAPPING* pNextLevelSet;
	};
} GR_DESCRIPTOR_SLOT_INFO;

/*
	Descriptor set mapping for pipeline shaders. Provides association of descriptor sets to the shader resources. The structure represents the descriptor set layout that is used at the draw time. A separate mapping is provided for each shader in the pipeline.
*/
typedef struct _GR_DESCRIPTOR_SET_MAPPING
{
	/* Number of slots in a descriptor set that are available to the shader. */
	GR_UINT                        descriptorCount;
	/* Array of descriptor slot mappings. See GR_DESCRIPTOR_SLOT_INFO. */
	const GR_DESCRIPTOR_SLOT_INFO* pDescriptorInfo;
} GR_DESCRIPTOR_SET_MAPPING;

/*
	Constant data for link-time pipeline optimizations.
*/
typedef struct _GR_LINK_CONST_BUFFER
{
	/* Constant buffer ID to match references in IL shader. */
	GR_UINT        bufferId;
	/* Constant buffer size in bytes (has to be a multiple of 16-bytes). */
	GR_SIZE        bufferSize;
	/* Pointer to application provided link time constant buffer data. */
	const GR_VOID* pBufferData;
} GR_LINK_CONST_BUFFER;

/*
Per shader mapping of dynamic memory view to shader entity.
*/
typedef struct _GR_DYNAMIC_MEMORY_VIEW_SLOT_INFO
{
	/* The object type a pipeline expects to see in the descriptor set at the draw time. See GR_DESCRIPTOR_SET_SLOT_TYPE. Only GR_SLOT_SHADER_RESOURCE and GR_SLOT_SHADER_UAV values are valid for dynamic memory view. */
	GR_ENUM slotObjectType;
	/* The shader entity index. */
	GR_UINT shaderEntityIndex;
} GR_DYNAMIC_MEMORY_VIEW_SLOT_INFO;

/*
	Definition of the shader and its resource mappings to descriptor sets and dynamic memory view for programmable pipeline stages.
*/
typedef struct _GR_PIPELINE_SHADER
{
	/* Shader object to be used for the pipeline stage. */
	GR_SHADER                   shader;
	/* Array of descriptor set mapping information. One entry per descriptor set bind point. See GR_DESCRIPTOR_SET_MAPPING. */
	GR_DESCRIPTOR_SET_MAPPING   descriptorSetMapping[GR_MAX_DESCRIPTOR_SETS];
	/* Number of link-time constant buffers. */
	GR_UINT                     linkConstBufferCount;
	/* Array of constant data structures. One constant data structure per link-time constant buffer. See GR_LINK_CONST_BUFFER. */
	const GR_LINK_CONST_BUFFER* pLinkConstBufferInfo;
	/* Mapping of dynamic memory view to shader entity. See GR_DYNAMIC_MEMORY_VIEW_SLOT_INFO. */
	GR_DYNAMIC_MEMORY_VIEW_SLOT_INFO dynamicMemoryViewMapping;
} GR_PIPELINE_SHADER;

/*
	Compute pipeline creation information.
*/
typedef struct _GR_COMPUTE_PIPELINE_CREATE_INFO
{
	/* Compute shader information. See GR_PIPELINE_SHADER. */
	GR_PIPELINE_SHADER cs;
	/* Flags for pipeline creation. See GR_PIPELINE_CREATE_FLAGS. */
	GR_FLAGS           flags;
} GR_COMPUTE_PIPELINE_CREATE_INFO;

/*
	Depth-stencil target information for binding it to command buffer state.
*/
typedef struct _GR_DEPTH_STENCIL_BIND_INFO
{
	/* Depth-stencil view to bind. */
	GR_DEPTH_STENCIL_VIEW view;
	/* Depth aspect target view image state at the draw time. See GR_IMAGE_STATE. */
	GR_ENUM               depthState;
	/* Stencil aspect target view image state at the draw time. See GR_IMAGE_STATE. */
	GR_ENUM               stencilState;
} GR_DEPTH_STENCIL_BIND_INFO;

/*
	Per face (front or back) stencil state for the dynamic depth-stencil state.
*/
typedef struct _GR_DEPTH_STENCIL_OP
{
	/* Stencil operation to apply when stencil test fails. See GR_STENCIL_OP. */
	GR_ENUM  stencilFailOp;
	/* Stencil operation to apply when stencil and depth tests pass. See GR_STENCIL_OP. */
	GR_ENUM  stencilPassOp;
	/* Stencil operation to apply when stencil test passes and depth test fails. See GR_STENCIL_OP. */
	GR_ENUM  stencilDepthFailOp;
	/* Stencil comparison function. See GR_COMPARE_FUNC. */
	GR_ENUM  stencilFunc;
	/* Stencil reference value. */
	GR_UINT8 stencilRef;
} GR_DEPTH_STENCIL_OP;

/*
	Dynamic depth-stencil state creation information.
*/
typedef struct _GR_DEPTH_STENCIL_STATE_CREATE_INFO
{
	/* Enable depth testing. */
	GR_BOOL             depthEnable;
	/* Enable depth writing. */
	GR_BOOL             depthWriteEnable;
	/* Depth comparison function. See GR_COMPARE_FUNC. */
	GR_ENUM             depthFunc;
	/* Enable depth bounds. */
	GR_BOOL             depthBoundsEnable;
	/* Minimal depth bounds value. */
	GR_FLOAT            minDepth;
	/* Maximum depth bounds value. */
	GR_FLOAT            maxDepth;
	/* Enable stencil testing. */
	GR_BOOL             stencilEnable;
	/* Bitmask to apply to stencil reads. */
	GR_UINT8            stencilReadMask;
	/* Bitmask to apply to stencil writes. */
	GR_UINT8            stencilWriteMask;
	/* Stencil operations for front-facing geometry. See GR_DEPTH_STENCIL_OP. */
	GR_DEPTH_STENCIL_OP front;
	/* Stencil operations for back-facing geometry. See GR_DEPTH_STENCIL_OP. */
	GR_DEPTH_STENCIL_OP back;
} GR_DEPTH_STENCIL_STATE_CREATE_INFO;

/*
	Depth-stencil target view creation information.
*/
typedef struct _GR_DEPTH_STENCIL_VIEW_CREATE_INFO
{
	/* Image for the view. */
	GR_IMAGE image;
	/* Mipmap level to render. */
	GR_UINT  mipLevel;
	/* First array slice for 2D array resources, or first depth slice for 3D image resources. */
	GR_UINT  baseArraySlice;
	/* Number of array slice for 2D array resources, or number of depth slices for 3D image resources. */
	GR_UINT  arraySize;
	/* Depth-stencil view flags. See GR_DEPTH_STENCIL_VIEW_CREATE_FLAGS. */
	GR_FLAGS flags;
} GR_DEPTH_STENCIL_VIEW_CREATE_INFO;

/*
	Descriptor set range attachment info for building hierarchical descriptor sets.
*/
typedef struct _GR_DESCRIPTOR_SET_ATTACH_INFO
{
	/* Descriptor set handle to use for binding. */
	GR_DESCRIPTOR_SET descriptorSet;
	/* The first slot in the descriptor set to be used for binding. */
	GR_UINT           slotOffset;
} GR_DESCRIPTOR_SET_ATTACH_INFO;

/*
	Descriptor set creation information.
*/
typedef struct _GR_DESCRIPTOR_SET_CREATE_INFO
{
	/* Total number of resource slots in the descriptor set. */
	GR_UINT slots;
} GR_DESCRIPTOR_SET_CREATE_INFO;

/*
	Per-queue type initialization information specified on device creation.
*/
typedef struct _GR_DEVICE_QUEUE_CREATE_INFO
{
	/* The type of queue to initialize on device creation. See GR_QUEUE_TYPE. */
	GR_ENUM queueType;
	/* The number of queues of a given type to initialize on device creation. */
	GR_UINT queueCount;
} GR_DEVICE_QUEUE_CREATE_INFO;

/*
	Device creation information.
*/
typedef struct _GR_DEVICE_CREATE_INFO
{
	/* The number of queue initialization records. */
	GR_UINT                            queueRecordCount;
	/* [in] An array of queue initialization records. See GR_DEVICE_QUEUE_CREATE_INFO. There could only be one record per queue type. */
	const GR_DEVICE_QUEUE_CREATE_INFO* pRequestedQueues;
	/* The number of extensions requested on device creation. */
	GR_UINT                            extensionCount;
	/* [in] The array of strings with extension names the application would like to enable on the device. */
	const GR_CHAR*const*               ppEnabledExtensionNames;
	/* The maximum validation level that could be enabled on a device during application execution. See GR_VALIDATION_LEVEL. If validation is disabled, the only valid value is GR_VALIDATION_LEVEL_0. */
	GR_ENUM                            maxValidationLevel;
	/* Device creation flags. See GR_DEVICE_CREATE_FLAGS. */
	GR_FLAGS                           flags;
} GR_DEVICE_CREATE_INFO;

/*
	Structure describing work dimensions for indirect dispatch.
*/
typedef struct _GR_DISPATCH_INDIRECT_ARG
{
	/* Number of thread groups in X direction. */
	GR_UINT32 x;
	/* Number of thread groups in Y direction. */
	GR_UINT32 y;
	/* Number of thread groups in Z direction. */
	GR_UINT32 z;
} GR_DISPATCH_INDIRECT_ARG;

/*
	Structure describing work parameters for indirect indexed draw.
*/
typedef struct _GR_DRAW_INDEXED_INDIRECT_ARG
{
	/* Number of indices per instance. */
	GR_UINT32 indexCount;
	/* Number of instances. */
	GR_UINT32 instanceCount;
	/* Index offset. */
	GR_UINT32 firstIndex;
	/* Vertex offset. */
	GR_INT32  vertexOffset;
	/* Instance offset. */
	GR_UINT32 firstInstance;
} GR_DRAW_INDEXED_INDIRECT_ARG;

/*
	Structure describing work parameters for indirect draw.
*/
typedef struct _GR_DRAW_INDIRECT_ARG
{
	/* Number of vertices per instance. */
	GR_UINT32 vertexCount;
	/* Number of instances. */
	GR_UINT32 instanceCount;
	/* First vertex offset. */
	GR_UINT32 firstVertex;
	/* First instance offset. */
	GR_UINT32 firstInstance;
} GR_DRAW_INDIRECT_ARG;

/*
	Event object creation information.
*/
typedef struct _GR_EVENT_CREATE_INFO
{
	/* Reserved, must be zero. */
	GR_FLAGS flags;
} GR_EVENT_CREATE_INFO;

/*
	The width and height for a 2D image region.
*/
typedef struct _GR_EXTENT2D
{
	/* The width for a 2D image. */
	GR_INT width;
	/* The height for a 2D image. */
	GR_INT height;
} GR_EXTENT2D;

/*
	The width, height, and depth for a 3D image region.
*/
typedef struct _GR_EXTENT3D
{
	/* The width for a 3D image region. */
	GR_INT width;
	/* The height for a 3D image region. */
	GR_INT height;
	/* The depth for a 3D image region. */
	GR_INT depth;
} GR_EXTENT3D;

/*
	Fence object creation information.
*/
typedef struct _GR_FENCE_CREATE_INFO
{
	/* Reserved, must be zero. */
	GR_FLAGS flags;
} GR_FENCE_CREATE_INFO;

/*
	Reported format properties for different tiling modes.
*/
typedef struct _GR_FORMAT_PROPERTIES
{
	/* Format properties for images of linear tiling and memory views. See GR_FORMAT_FEATURE_FLAGS. */
	GR_FLAGS linearTilingFeatures;
	/* Format properties for images of optimal tiling. See GR_FORMAT_FEATURE_FLAGS. */
	GR_FLAGS optimalTilingFeatures;
} GR_FORMAT_PROPERTIES;

/*
	Cross-GPU compatibility information.
*/
typedef struct _GR_GPU_COMPATIBILITY_INFO
{
	/* Cross-GPU compatibility flags. See GR_GPU_COMPATIBILITY_FLAGS. */
	GR_FLAGS compatibilityFlags;
} GR_GPU_COMPATIBILITY_INFO;

/*
	Per color target description of the color blender and output state for pipeline.
*/
typedef struct _GR_PIPELINE_CB_TARGET_STATE
{
	/* Blend enable for color target. */
	GR_BOOL   blendEnable;
	/* Color target format at the draw time. Should match the actual target format used for rendering. See GR_FORMAT. */
	GR_FORMAT format;
	/* Color target write mask. Each bit controls a color channel in R, G, B, A order, with bit 0 controlling the red channel and so on. */
	GR_UINT8  channelWriteMask;
} GR_PIPELINE_CB_TARGET_STATE;

/*
	Static color blender and output state for pipeline.
*/
typedef struct _GR_PIPELINE_CB_STATE
{
	/* Alpha to coverage enable. */
	GR_BOOL                     alphaToCoverageEnable;
	/* The blend state used at the draw time specifies the dual source blend mode. */
	GR_BOOL                     dualSourceBlendEnable;
	/* Logic operation to perform. See GR_LOGIC_OP. */
	GR_ENUM                     logicOp;
	/* Per color target description of the state. See GR_PIPELINE_CB_TARGET_STATE. */
	GR_PIPELINE_CB_TARGET_STATE target[GR_MAX_COLOR_TARGETS];
} GR_PIPELINE_CB_STATE;

/*
	Static depth-stencil state for pipeline.
*/
typedef struct _GR_PIPELINE_DB_STATE
{
	/* Depth-stencil target format at the draw time. Should match the actual depth-stencil format used for rendering. See GR_FORMAT. */
	GR_FORMAT format;
} GR_PIPELINE_DB_STATE;

/*
	Static input assembler state for pipeline.
*/
typedef struct _GR_PIPELINE_IA_STATE
{
	/* Primitive topology. See GR_PRIMITIVE_TOPOLOGY. */
	GR_ENUM topology;
	/* Provides ability to disable vertex reuse in indexed draws when set to GR_TRUE (disables post-transform cache). */
	GR_BOOL disableVertexReuse;
} GR_PIPELINE_IA_STATE;

/*
	Static rasterizer state for pipeline.
*/
typedef struct _GR_PIPELINE_RS_STATE
{
	/* Depth clip functionality enable. */
	GR_BOOL depthClipEnable;
} GR_PIPELINE_RS_STATE;

/*
	Result of pipeline statistics query.
*/
typedef struct _GR_PIPELINE_STATISTICS_DATA
{
	/* Pixel shader invocations. */
	GR_UINT64 psInvocations;
	/* Clipper primitives. */
	GR_UINT64 cPrimitives;
	/* Clipper invocations. */
	GR_UINT64 cInvocations;
	/* Vertex shader invocations. */
	GR_UINT64 vsInvocations;
	/* Geometry shader invocations. */
	GR_UINT64 gsInvocations;
	/* Geometry shader primitives. */
	GR_UINT64 gsPrimitives;
	/* Input primitives. */
	GR_UINT64 iaPrimitives;
	/* Input vertices. */
	GR_UINT64 iaVertices;
	/* Hull shader invocations. */
	GR_UINT64 hsInvocations;
	/* Domain shader invocations. */
	GR_UINT64 dsInvocations;
	/* Compute shader invocations. */
	GR_UINT64 csInvocations;
} GR_PIPELINE_STATISTICS_DATA;

/*
	Static tessellator state for pipeline.
*/
typedef struct _GR_PIPELINE_TESS_STATE
{
	/* Number of control points per patch. */
	GR_UINT  patchControlPoints;
	/* Tessellation factor to optimize pipeline operation for. */
	GR_FLOAT optimalTessFactor;
} GR_PIPELINE_TESS_STATE;

/*
	Graphics pipeline creation information.
*/
typedef struct _GR_GRAPHICS_PIPELINE_CREATE_INFO
{
	/* Vertex shader information. See GR_PIPELINE_SHADER. */
	GR_PIPELINE_SHADER     vs;
	/* Hull shader information. See GR_PIPELINE_SHADER. */
	GR_PIPELINE_SHADER     hs;
	/* Domain shader information. See GR_PIPELINE_SHADER. */
	GR_PIPELINE_SHADER     ds;
	/* Geometry shader information. See GR_PIPELINE_SHADER. */
	GR_PIPELINE_SHADER     gs;
	/* Pixel shader information. See GR_PIPELINE_SHADER. */
	GR_PIPELINE_SHADER     ps;
	/* Input assembler static pipeline state. See GR_PIPELINE_IA_STATE. */
	GR_PIPELINE_IA_STATE   iaState;
	/* Tessellator static pipeline state. See GR_PIPELINE_TESS_STATE. */
	GR_PIPELINE_TESS_STATE tessState;
	/* Rasterizer static pipeline state. See GR_PIPELINE_RS_STATE. */
	GR_PIPELINE_RS_STATE   rsState;
	/* Color blender and output static pipeline state. See GR_PIPELINE_CB_STATE. */
	GR_PIPELINE_CB_STATE   cbState;
	/* Depth-stencil static pipeline state. See GR_PIPELINE_DB_STATE. */
	GR_PIPELINE_DB_STATE   dbState;
	/* Pipeline creation flags. See GR_PIPELINE_CREATE_FLAGS. */
	GR_FLAGS               flags;
} GR_GRAPHICS_PIPELINE_CREATE_INFO;

/*
	Image subresource identifier.
*/
typedef struct _GR_IMAGE_SUBRESOURCE
{
	/* Image aspect the subresource belongs to. See GR_IMAGE_ASPECT. */
	GR_ENUM aspect;
	/* Image mipmap level for the subresource. */
	GR_UINT mipLevel;
	/* Image array slice for the subresource. */
	GR_UINT arraySlice;
} GR_IMAGE_SUBRESOURCE;

/*
	The 2D image coordinate offset for image manipulation.
*/
typedef struct _GR_OFFSET2D
{
	/* The x coordinate for the offset. */
	GR_INT x;
	/* The y coordinate for the offset. */
	GR_INT y;
} GR_OFFSET2D;

/*
	The 3D image coordinate offset for image manipulation.
*/
typedef struct _GR_OFFSET3D
{
	/* The x coordinate for the offset. */
	GR_INT x;
	/* The y coordinate for the offset. */
	GR_INT y;
	/* The z coordinate for the offset. */
	GR_INT z;
} GR_OFFSET3D;

/*
	Image to image region copy description.
*/
typedef struct _GR_IMAGE_COPY
{
	/* Source image subresource. See GR_IMAGE_SUBRESOURCE. */
	GR_IMAGE_SUBRESOURCE srcSubresource;
	/* Texel offset in the source subresource. For compressed images use compression blocks instead of texels. See GR_OFFSET3D. */
	GR_OFFSET3D          srcOffset;
	/* Destination image subresource. See GR_IMAGE_SUBRESOURCE. */
	GR_IMAGE_SUBRESOURCE destSubresource;
	/* Texel offset in the destination subresource. For compressed images, use compression blocks instead of texels. See GR_OFFSET3D. */
	GR_OFFSET3D          destOffset;
	/* Texel dimensions of the image region to copy. For compressed images, use compression blocks instead of texels. See GR_EXTENT3D. */
	GR_EXTENT3D          extent;
} GR_IMAGE_COPY;

/*
	Image creation information.
*/
typedef struct _GR_IMAGE_CREATE_INFO
{
	/* Image type (1D, 2D or 3D). See GR_INDEX_TYPE. */
	GR_ENUM     imageType;
	/* Image format. See GR_FORMAT. */
	GR_FORMAT   format;
	/* Image dimensions in texels. See GR_EXTENT3D. */
	GR_EXTENT3D extent;
	/* Number of mipmap levels. Cannot be zero. */
	GR_UINT     mipLevels;
	/* Array size. Use value of one for non-array images. Cannot be zero. */
	GR_UINT     arraySize;
	/* Number of coverage samples. Use value of one for non-multisampled images. */
	GR_UINT     samples;
	/* Image tiling. See GR_IMAGE_TILING. */
	GR_ENUM     tiling;
	/* Image usage flags. See GR_IMAGE_USAGE_FLAGS. */
	GR_FLAGS    usage;
	/* Image creation flags. See GR_IMAGE_CREATE_FLAGS. */
	GR_FLAGS    flags;
} GR_IMAGE_CREATE_INFO;

/*
	Image resolve region description.
*/
typedef struct _GR_IMAGE_RESOLVE
{
	/* Subresource in multisampled source image. See GR_IMAGE_SUBRESOURCE. */
	GR_IMAGE_SUBRESOURCE srcSubresource;
	/* Texel offset in the source subresource. See GR_OFFSET2D. */
	GR_OFFSET2D          srcOffset;
	/* Subresource in non-multisampled destination image. See GR_IMAGE_SUBRESOURCE. */
	GR_IMAGE_SUBRESOURCE destSubresource;
	/* Texel offset in the destination subresource. See GR_OFFSET2D. */
	GR_OFFSET2D          destOffset;
	/* Texel dimensions of the image region to resolve. See GR_EXTENT2D. */
	GR_EXTENT2D          extent;
} GR_IMAGE_RESOLVE;

/*
	Defines a range of subresources within an image aspect.
*/
typedef struct _GR_IMAGE_SUBRESOURCE_RANGE
{
	/* Image aspect the subresource range belongs to. See GR_IMAGE_ASPECT. */
	GR_ENUM aspect;
	/* Base image mipmap level for the subresource range. */
	GR_UINT baseMipLevel;
	/* Number of image mipmap levels in the subresource range. Use GR_LAST_MIP_OR_SLICE to specify the range of mipmap levels from baseMipLevel to the last one available in in the image. */
	GR_UINT mipLevels;
	/* Base image array slice for the subresource range. */
	GR_UINT baseArraySlice;
	/* Number of image array slices in the subresource range. Use GR_LAST_MIP_OR_SLICE to specify the range of array slices from baseArraySlice to the last one available in in the image. */
	GR_UINT arraySize;
} GR_IMAGE_SUBRESOURCE_RANGE;

/*
	Description of image state transition for a range of subresources.
*/
typedef struct _GR_IMAGE_STATE_TRANSITION
{
	/* Image object to use for state transition. */
	GR_IMAGE                   image;
	/* Previous image state. See GR_IMAGE_STATE. */
	GR_ENUM                    oldState;
	/* New image state. See GR_IMAGE_STATE. */
	GR_ENUM                    newState;
	/* Images subresource range. See GR_IMAGE_SUBRESOURCE_RANGE. */
	GR_IMAGE_SUBRESOURCE_RANGE subresourceRange;
} GR_IMAGE_STATE_TRANSITION;

/*
	Image view description for attachment to descriptor set slots.
*/
typedef struct _GR_IMAGE_VIEW_ATTACH_INFO
{
	/* Image view object. */
	GR_IMAGE_VIEW view;
	/* Image state for the view subresources at the draw time. See GR_IMAGE_STATE. */
	GR_ENUM       state;
} GR_IMAGE_VIEW_ATTACH_INFO;

/*
	Image view creation information.
*/
typedef struct _GR_IMAGE_VIEW_CREATE_INFO
{
	/* Image for the view. */
	GR_IMAGE                   image;
	/* View type matching the image topology. See GR_IMAGE_VIEW_TYPE. */
	GR_ENUM                    viewType;
	/* Image format for the view; has to be compatible with the format of the image. See GR_FORMAT. */
	GR_FORMAT                  format;
	/* Channel swizzle. See GR_CHANNEL_MAPPING. */
	GR_CHANNEL_MAPPING         channels;
	/* Contiguous range of subresources to use for the image view. See GR_IMAGE_SUBRESOURCE_RANGE. */
	GR_IMAGE_SUBRESOURCE_RANGE subresourceRange;
	/* Highest-resolution mipmap level available for access through the view. */
	GR_FLOAT                   minLod;
} GR_IMAGE_VIEW_CREATE_INFO;

/*
	GPU memory allocation information.
*/
typedef struct _GR_MEMORY_ALLOC_INFO
{
	/* The size of the GPU memory allocation in bytes. */
	GR_GPU_SIZE size;
	/* Optional GPU memory alignment in bytes. Must be multiple of the biggest page size. */
	GR_GPU_SIZE alignment;
	/* The flags for the memory allocation. See GR_MEMORY_ALLOC_FLAGS. */
	GR_FLAGS    flags;
	/* The number of GPU memory heaps allowed for allocation placement. */
	GR_UINT     heapCount;
	/* An array of memory heap IDs allowed for allocation placement. The order of heap IDs defines preferred placement priority for the GPU memory heap selection. */
	GR_UINT     heaps[GR_MAX_MEMORY_HEAPS];
	/* The memory priorities for the allocation at creation time. See GR_MEMORY_PRIORITY. */
	GR_ENUM     memPriority;
} GR_MEMORY_ALLOC_INFO;

/*
	Memory to memory copy region information.
*/
typedef struct _GR_MEMORY_COPY
{
	/* Byte offset in the source memory object. */
	GR_GPU_SIZE srcOffset;
	/* Byte offset in the destination memory object. */
	GR_GPU_SIZE destOffset;
	/* Copy region in bytes. */
	GR_GPU_SIZE copySize;
} GR_MEMORY_COPY;

/*
	Memory heap properties.
*/
typedef struct _GR_MEMORY_HEAP_PROPERTIES
{
	/* The GPU memory heap type. See GR_HEAP_MEMORY_TYPE. */
	GR_ENUM     heapMemoryType;
	/* The size of the GPU memory heap in bytes. */
	GR_GPU_SIZE heapSize;
	/* The page size the GPU memory heap in bytes. */
	GR_GPU_SIZE pageSize;
	/* GPU memory heap property flags. See GR_MEMORY_HEAP_FLAGS. */
	GR_FLAGS    flags;
	/* Relative heap performance rating for GPU reads. */
	GR_FLOAT    gpuReadPerfRating;
	/* Relative heap performance rating for GPU writes. */
	GR_FLOAT    gpuWritePerfRating;
	/* Relative heap performance rating for CPU reads. */
	GR_FLOAT    cpuReadPerfRating;
	/* Relative heap performance rating for CPU writes. */
	GR_FLOAT    cpuWritePerfRating;
} GR_MEMORY_HEAP_PROPERTIES;

/*
	Memory to image and image to memory copy region description.
*/
typedef struct _GR_MEMORY_IMAGE_COPY
{
	/* Byte offset in the memory object. */
	GR_GPU_SIZE          memOffset;
	/* Image subresource to use for copy. See GR_IMAGE_SUBRESOURCE. */
	GR_IMAGE_SUBRESOURCE imageSubresource;
	/* Texel offset in the image subresource. For compressed images, use compression blocks instead of texels. See GR_OFFSET3D. */
	GR_OFFSET3D          imageOffset;
	/* Texel dimensions of the image region to copy. For compressed images, use compression blocks instead of texels. See GR_EXTENT3D. */
	GR_EXTENT3D          imageExtent;
} GR_MEMORY_IMAGE_COPY;

/*
	Parameters for opening shared GPU memory object on another device.
*/
typedef struct _GR_MEMORY_OPEN_INFO
{
	/* The handle of a shared GPU memory object from another device to open. */
	GR_GPU_MEMORY sharedMem;
} GR_MEMORY_OPEN_INFO;

/*
	Information about memory object reference in command buffer for submission.
*/
typedef struct _GR_MEMORY_REF
{
	/* Memory object for the reference. */
	GR_GPU_MEMORY mem;
	/* Memory reference flags. See GR_MEMORY_REF_FLAGS. */
	GR_FLAGS      flags;
} GR_MEMORY_REF;

/*
	Memory binding requirements for an object.
*/
typedef struct _GR_MEMORY_REQUIREMENTS
{
	/* GPU memory size in bytes required for object storage. */
	GR_GPU_SIZE size;
	/* Memory alignment in bytes. */
	GR_GPU_SIZE alignment;
	/* Number of valid entries returned in heaps array. */
	GR_UINT     heapCount;
	/* Array of returned heap IDs for all heaps that can be used for the object placement. */
	GR_UINT     heaps[GR_MAX_MEMORY_HEAPS];
} GR_MEMORY_REQUIREMENTS;

/*
	Defines memory state transition for a range of memory.
*/
typedef struct _GR_MEMORY_STATE_TRANSITION
{
	/* GPU memory object to use for state transition. */
	GR_GPU_MEMORY mem;
	/* Previous memory state for the range. See GR_MEMORY_STATE. */
	GR_ENUM       oldState;
	/* New memory state for the range. See GR_MEMORY_STATE. */
	GR_ENUM       newState;
	/* Byte offset within the GPU memory object that defines the beginning of the memory range for state transition. */
	GR_GPU_SIZE   offset;
	/* GPU memory region size in bytes to use for state transition. */
	GR_GPU_SIZE   regionSize;
} GR_MEMORY_STATE_TRANSITION;

/*
	Memory view description for attachment to descriptor set slots.
*/
typedef struct _GR_MEMORY_VIEW_ATTACH_INFO
{
	/* GPU memory object to use for memory view. */
	GR_GPU_MEMORY mem;
	/* Byte offset within the GPU memory object to the beginning of memory view. */
	GR_GPU_SIZE   offset;
	/* Memory range in bytes for the memory view. */
	GR_GPU_SIZE   range;
	/* Element stride for the memory view. */
	GR_GPU_SIZE   stride;
	/* Optional format for typed memory views. See GR_FORMAT. */
	GR_FORMAT     format;
	/* Current memory state for the memory view range. See GR_MEMORY_STATE. */
	GR_ENUM       state;
} GR_MEMORY_VIEW_ATTACH_INFO;

/*
	Dynamic multisampling state creation information.
*/
typedef struct _GR_MSAA_STATE_CREATE_INFO
{
	/* Number of samples. */
	GR_UINT        samples;
	/* Sample bit-mask. Determines which samples in color targets are updated. Lower bit represents sample zero. */
	GR_SAMPLE_MASK sampleMask;
} GR_MSAA_STATE_CREATE_INFO;

/*
	Information about the parent device for an API object.
*/
typedef struct _GR_PARENT_DEVICE
{
	/* The handle of a parent device. */
	GR_DEVICE device;
} GR_PARENT_DEVICE;

/*
	Information about parent physical GPU for a device object.
*/
typedef struct _GR_PARENT_PHYSICAL_GPU
{
	/* The handle of a parent physical GPU object. */
	GR_PHYSICAL_GPU gpu;
} GR_PARENT_PHYSICAL_GPU;

/*
	Parameters for opening image object on another device for peer-to-peer image transfers.
*/
typedef struct _GR_PEER_IMAGE_OPEN_INFO
{
	/* The handle of an image object from another device to open for peer-to-peer image transfers. */
	GR_IMAGE originalImage;
} GR_PEER_IMAGE_OPEN_INFO;

/*
	Parameters for opening memory object on another device for peer-to-peer memory transfers.
*/
typedef struct _GR_PEER_MEMORY_OPEN_INFO
{
	/* The handle of an memory object from another device to open for peer-to-peer memory transfers. */
	GR_GPU_MEMORY originalMem;
} GR_PEER_MEMORY_OPEN_INFO;

/*
	Image support capabilities of a physical GPU object.
*/
typedef struct _GR_PHYSICAL_GPU_IMAGE_PROPERTIES
{
	/* Maximum image slice width in texels. */
	GR_UINT     maxSliceWidth;
	/* Maximum image slice height in texels. */
	GR_UINT     maxSliceHeight;
	/* Maximum 3D image depth. */
	GR_UINT     maxDepth;
	/* Maximum number of slices in an image array. */
	GR_UINT     maxArraySlices;
	/* Reserved. */
	GR_UINT     reserved1;
	/* Reserved. */
	GR_UINT     reserved2;
	/* Maximum memory alignment requirements any image can have in bytes. */
	GR_GPU_SIZE maxMemoryAlignment;
	/* Sparse image support level. */
	GR_UINT32   sparseImageSupportLevel;
	/* Reserved. */
	GR_FLAGS    flags;
} GR_PHYSICAL_GPU_IMAGE_PROPERTIES;

/*
	Memory management capabilities of a physical GPU object.
*/
typedef struct _GR_PHYSICAL_GPU_MEMORY_PROPERTIES
{
	/* The GPU memory manager capability flags. See GR_MEMORY_PROPERTY_FLAGS. */
	GR_FLAGS    flags;
	/* The virtual memory page size for the GPU. Zero if virtual memory remapping is not supported. */
	GR_GPU_SIZE virtualMemPageSize;
	/* The upper bound of the address range available for creation of virtual memory objects. Zero if virtual memory remapping is not supported or if unknown. */
	GR_GPU_SIZE maxVirtualMemSize;
	/* The upper bound of all GPU accessible memory in the system. Zero if unknown. */
	GR_GPU_SIZE maxPhysicalMemSize;
} GR_PHYSICAL_GPU_MEMORY_PROPERTIES;

/*
	Performance properties of a physical GPU object. Provides rough performance estimates for the GPU performance.
*/
typedef struct _GR_PHYSICAL_GPU_PERFORMANCE
{
	/* The maximum GPU engine clock in MHz. */
	GR_FLOAT maxGpuClock;
	/* The maximum number of shader ALU operations per clock. */
	GR_FLOAT aluPerClock;
	/* The maximum number of texture fetches per clock. */
	GR_FLOAT texPerClock;
	/* The maximum number of processed geometry primitives per clock. */
	GR_FLOAT primsPerClock;
	/* The maximum number of processed pixels per clock. */
	GR_FLOAT pixelsPerClock;
} GR_PHYSICAL_GPU_PERFORMANCE;

/*
	General properties of a physical GPU object.
*/
typedef struct _GR_PHYSICAL_GPU_PROPERTIES
{
	/* The Mantle API version supported by the GPU. */
	GR_UINT32   apiVersion;
	/* The driver version. */
	GR_UINT32   driverVersion;
	/* The vendor ID of the GPU. */
	GR_UINT32   vendorId;
	/* The device ID of the GPU. */
	GR_UINT32   deviceId;
	/* The GPU type. See GR_PHYSICAL_GPU_TYPE. */
	GR_ENUM     gpuType;
	/* A string with the GPU description. */
	GR_CHAR     gpuName[GR_MAX_PHYSICAL_GPU_NAME];
	/* The maximum number of memory references per submission for the GPU. */
	GR_UINT     maxMemRefsPerSubmission;
	/* Reserved. */
	GR_GPU_SIZE reserved;
	/* The maximum inline memory update size for the GPU. */
	GR_GPU_SIZE maxInlineMemoryUpdateSize;
	/* The maximum number of bound descriptor sets for the GPU. */
	GR_UINT     maxBoundDescriptorSets;
	/* The maximum compute thread group size for the GPU. */
	GR_UINT     maxThreadGroupSize;
	/* The timestamp frequency for the GPU in Hz. */
	GR_UINT64   timestampFrequency;
	/* A flag indicating support of multiple color target clears for the GPU. */
	GR_BOOL     multiColorTargetClears;
} GR_PHYSICAL_GPU_PROPERTIES;

/*
	Queue type properties for a physical GPU.
*/
typedef struct _GR_PHYSICAL_GPU_QUEUE_PROPERTIES
{
	/* The type of queue. See GR_QUEUE_TYPE. */
	GR_ENUM queueType;
	/* The maximum available queue count. */
	GR_UINT queueCount;
	/* The maximum number of atomic counters available for the queues of the given type. */
	GR_UINT maxAtomicCounters;
	/* The timestamps support flag for the queues of the given type. */
	GR_BOOL supportsTimestamps;
} GR_PHYSICAL_GPU_QUEUE_PROPERTIES;

/*
	Query pool creation information.
*/
typedef struct _GR_QUERY_POOL_CREATE_INFO
{
	/* Type of the queries that are used with this query pool. Queries of only one type can be present in the query pool. See GR_QUERY_TYPE. */
	GR_ENUM queryType;
	/* Number of query slots in the pool. */
	GR_UINT slots;
} GR_QUERY_POOL_CREATE_INFO;

/*
	Queue semaphore creation information.
*/
typedef struct _GR_QUEUE_SEMAPHORE_CREATE_INFO
{
	/* Initial queue semaphore count. Value must be in [0..31] range. */
	GR_UINT  initialCount;
	/* Semaphore creation flags. See GR_SEMAPHORE_CREATE_FLAGS. */
	GR_FLAGS flags;
} GR_QUEUE_SEMAPHORE_CREATE_INFO;

/*
	Parameters for opening a shared queue semaphore on another device.
*/
typedef struct _GR_QUEUE_SEMAPHORE_OPEN_INFO
{
	/* The handle of a shared queue semaphore from another device to open. */
	GR_QUEUE_SEMAPHORE sharedSemaphore;
} GR_QUEUE_SEMAPHORE_OPEN_INFO;

/*
	Dynamic rasterizer state creation information.
*/
typedef struct _GR_RASTER_STATE_CREATE_INFO
{
	/* Fill mode. See GR_FILL_MODE. */
	GR_ENUM  fillMode;
	/* Cull mode. See GR_CULL_MODE. */
	GR_ENUM  cullMode;
	/* Front face orientation. See GR_FACE_ORIENTATION. */
	GR_ENUM  frontFace;
	/* Value added to pixel depth. */
	GR_INT   depthBias;
	/* Maximum depth bias value. */
	GR_FLOAT depthBiasClamp;
	/* Scale of the slope-based value added to pixel depth. */
	GR_FLOAT slopeScaledDepthBias;
} GR_RASTER_STATE_CREATE_INFO;

/*
	A rectangle region for 2D image.
*/
typedef struct _GR_RECT
{
	/* The rectangle region offset. See GR_OFFSET2D. */
	GR_OFFSET2D offset;
	/* The extent of the rectangle region. See GR_EXTENT2D. */
	GR_EXTENT2D extent;
} GR_RECT;

/*
	Sampler creation information.
*/
typedef struct _GR_SAMPLER_CREATE_INFO
{
	/* Filtering to apply to texture fetches. See GR_TEX_FILTER. */
	GR_ENUM  filter;
	/* Texture addressing mode for outside of the [0..1] range for U texture coordinate. See GR_TEX_ADDRESS. */
	GR_ENUM  addressU;
	/* Texture addressing mode for outside of the [0..1] range for V texture coordinate. See GR_TEX_ADDRESS. */
	GR_ENUM  addressV;
	/* Texture addressing mode for outside of the [0..1] range for W texture coordinate. See GR_TEX_ADDRESS. */
	GR_ENUM  addressW;
	/* LOD bias. */
	GR_FLOAT mipLodBias;
	/* Anisotropy value clamp when filter mode is GR_TEX_FILTER_ANISOTROPIC. */
	GR_UINT  maxAnisotropy;
	/* Comparison function to apply to fetched data. See GR_COMPARE_FUNC. */
	GR_ENUM  compareFunc;
	/* Highest-resolution mipmap level available for access. */
	GR_FLOAT minLod;
	/* Lowest-resolution mipmap level available for access; has to be greater or equal to minLod. */
	GR_FLOAT maxLod;
	/* One of predefined border color values (white, transparent black or opaque black). See GR_BORDER_COLOR_TYPE. */
	GR_ENUM  borderColor;
} GR_SAMPLER_CREATE_INFO;

/*
	Shader creation information.
*/
typedef struct _GR_SHADER_CREATE_INFO
{
	/* Input shader code size in bytes. */
	GR_SIZE        codeSize;
	/* Pointer to the input shader binary code. */
	const GR_VOID* pCode;
	/* Shader creation flags. See GR_SHADER_CREATE_FLAGS. */
	GR_FLAGS       flags;
} GR_SHADER_CREATE_INFO;

/*
	Subresource layout returned for a subresource.
*/
typedef struct _GR_SUBRESOURCE_LAYOUT
{
	/* Byte offset of the subresource data relative to the beginning of memory associated with an image object. */
	GR_GPU_SIZE offset;
	/* Subresource size in bytes. */
	GR_GPU_SIZE size;
	/* Row pitch in bytes. For opaque resources reported pitch is zero. */
	GR_GPU_SIZE rowPitch;
	/* Depth pitch for image arrays and 3D images in bytes. For opaque resources, reported pitch is zero. */
	GR_GPU_SIZE depthPitch;
} GR_SUBRESOURCE_LAYOUT;

/*
	Defines dimensions of a viewport.
*/
typedef struct _GR_VIEWPORT
{
	/* The x coordinate for the origin of the viewport. */
	GR_FLOAT originX;
	/* The y coordinate for the origin of the viewport. */
	GR_FLOAT originY;
	/* The width of the viewport. */
	GR_FLOAT width;
	/* The height of the viewport. */
	GR_FLOAT height;
	/* The minimum depth value of the viewport. The valid range is [0..1]. */
	GR_FLOAT minDepth;
	/* The maximum depth value of the viewport. The valid range is [0..1]. The maximum viewport depth value has to be greater than minimum depth value. */
	GR_FLOAT maxDepth;
} GR_VIEWPORT;

/*
	Dynamic viewport and scissor state creation information.
*/
typedef struct _GR_VIEWPORT_STATE_CREATE_INFO
{
	/* Number of viewports. */
	GR_UINT     viewportCount;
	/* Scissor enable flag. */
	GR_BOOL     scissorEnable;
	/* Array of viewports. See GR_VIEWPORT. */
	GR_VIEWPORT viewports[GR_MAX_VIEWPORTS];
	/* Array of scissors. See GR_RECT. */
	GR_RECT     scissors[GR_MAX_VIEWPORTS];
} GR_VIEWPORT_STATE_CREATE_INFO;

/*
	Specified a range of pages in a virtual memory object for remapping to pages of real memory object.
*/
typedef struct _GR_VIRTUAL_MEMORY_REMAP_RANGE
{
	/* A virtual memory object handle for page remapping. */
	GR_GPU_MEMORY virtualMem;
	/* First page of a virtual memory object in a remapped range. */
	GR_GPU_SIZE   virtualStartPage;
	/* Handle of a real memory object to which virtual memory object pages are remapped. */
	GR_GPU_MEMORY realMem;
	/* First page of a real memory object to which virtual memory pages are remapped. */
	GR_GPU_SIZE   realStartPage;
	/* Number of pages in a range to remap. */
	GR_GPU_SIZE   pageCount;
} GR_VIRTUAL_MEMORY_REMAP_RANGE;




/*
===============================================================================
###############################################################################

	CORE - INITIALIZATION AND DEVICE FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Initializes the Mantle runtime and enumerates the handles of all Mantle-capable physical GPUs present in the system. Each GPU is reported separately for multi-GPU boards. This function is also used to re-enumerate GPUs after receiving a GR_ERROR_DEVICE_LOST error code.


	RETURNS
	If successful, grInitAndEnumerateGpus() returns GR_SUCCESS, the number of available Mantle GPUs is written to the location specified by pGpuCount, and the list of GPU handles is written to gpus. The number of reported GPUs can be zero.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INITIALIZATION_FAILED if the loader cannot load any Mantle ICDs
	▼ GR_ERROR_INCOMPATIBLE_DRIVER if the loader loaded a Mantle ICD, but it is incompatible with the Mantle version supported by the loader
	▼ GR_ERROR_INVALID_POINTER if pGpuCount is NULL
	▼ GR_ERROR_INVALID_POINTER if application information is specified, but pAppName is NULL
	▼ GR_ERROR_INVALID_POINTER if pAllocCb is not NULL, but one or more of the callback function pointers are NULL
	▼ GR_ERROR_INVALID_POINTER if this function is called more than once and the function callback pointers are different from previous function invocations


	NOTES
	An application can call this function multiple times if necessary. The first grInitAndEnumerateGpus() call loads and initializes the drivers; subsequent calls force the driver re-initialization. Before calling this function a second time, all devices and other Mantle objects must be destroyed by the application.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grInitAndEnumerateGpus(
	/* [in] Application information provided to the Mantle drivers. See GR_APPLICATION_INFO. */
	const GR_APPLICATION_INFO* pAppInfo,
	/* [in] Optional system memory alloc/free function callbacks. Can be NULL. See GR_ALLOC_CALLBACKS. */
	const GR_ALLOC_CALLBACKS* pAllocCb,
	/* [out] Count of available Mantle GPUs. */
	GR_UINT* pGpuCount,
	/* [out] Handles of all available Mantle GPUs. */
	GR_PHYSICAL_GPU gpus[GR_MAX_PHYSICAL_GPUS]);

/*
	Retrieves specific information about a Mantle GPU. This function is called before device creation in order to select a suitable GPU.


	RETURNS
	If successful, grGetGpuInfo() returns GR_SUCCESS and the queried info is written to the location specified by pData.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the gpu handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the gpu handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if infoType is not one of the supported values
	▼ GR_ERROR_INVALID_POINTER if pDataSize is NULL
	▼ GR_ERROR_INVALID_MEMORY_SIZE if pData is not NULL and the pDataSize input value is smaller than the size of the appropriate return data structure


	NOTES
	If pData is NULL, the input pDataSize value does not matter and the function returns the expected data structure size in pDataSize.

	GPU Properties
	The GPU properties are retrieved with the GR_INFO_TYPE_PHYSICAL_GPU_PROPERTIES information type. Returned is the GR_PHYSICAL_GPU_PROPERTIES structure.

	GPU Performance
	The GPU performance properties are retrieved with the GR_INFO_TYPE_PHYSICAL_GPU_PERFORMANCE information type. Returned is the GR_PHYSICAL_GPU_PERFORMANCE structure.

	Queue Properties
	Queue properties are retrieved on physical GPUs with the GR_INFO_TYPE_PHYSICAL_GPU_QUEUE_PROPERTIES information type. Returned is a list of GR_PHYSICAL_GPU_QUEUE_PROPERTIES structures, one per queue type.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetGpuInfo(
	/* Physical GPU device handle. */
	GR_PHYSICAL_GPU gpu,
	/* Type of information to retrieve. See GR_INFO_TYPE. */
	GR_ENUM infoType,
	/* [in/out] Input value specifies the size in bytes of the pData output buffer; output value reports the number of bytes written to pData. */
	GR_SIZE* pDataSize,
	/* [out] Device information structure. Can be NULL. */
	GR_VOID* pData);

/*
	Creates a Mantle device object.


	RETURNS
	If successful, grCreateDevice() returns GR_SUCCESS and the created Mantle device handle is written to the location specified by pDevice.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the gpu handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the gpu handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pDevice are NULL
	▼ GR_ERROR_INVALID_POINTER if a non-zero number of extensions is specified and pCreateInfo.ppEnabledExtensionNames is NULL, or any extension name pointer is NULL
	▼ GR_ERROR_INVALID_VALUE if the requested number of queues for each type is invalid
	▼ GR_ERROR_INVALID_VALUE if the validation level is invalid; if no validation level is enabled, only GR_VALIDATION_LEVEL_0 can be specified
	▼ GR_ERROR_INVALID_EXTENSION if a requested extension is not supported
	▼ GR_ERROR_INVALID_FLAGS if the creation flags are invalid
	▼ GR_ERROR_INITIALIZATION_FAILED if the driver could not initialize the device object for internal reasons
	▼ GR_ERROR_DEVICE_ALREADY_CREATED if a device instance is already active for the given physical GPU


	NOTES
	pCreateInfo.ppEnabledExtensionNames pointer can be NULL if pCreateInfo.extensionCount is zero.

	See GR_VALIDATION_LEVEL for a list of available validation levels.

	At least one universal queue must be specified.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateDevice(
	/* Physical GPU device handle. */
	GR_PHYSICAL_GPU gpu,
	/* [in] Device creation parameters. See GR_DEVICE_CREATE_INFO. */
	const GR_DEVICE_CREATE_INFO* pCreateInfo,
	/* [out] Device handle. */
	GR_DEVICE* pDevice);

/*
	Destroys a valid Mantle device.


	RETURNS
	grDestroyDevice() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grDestroyDevice(
	/* Device handle. */
	GR_DEVICE device);




/*
===============================================================================
###############################################################################

	CORE - EXTENSION DISCOVERY FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Checks extension support by name.


	RETURNS
	grGetExtensionSupport() returns GR_SUCCESS if the function executed successfully and the specified extension is supported. If the function executed successfully, but the specified extension is not available, GR_UNSUPPORTED is returned.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the gpu handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the gpu handle references invalid object type
	▼ GR_ERROR_INVALID_POINTER if pExtName is NULL


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetExtensionSupport(
	/* Physical GPU device handle. */
	GR_PHYSICAL_GPU gpu,
	/* [in] Extension name for which to check support. */
	const GR_CHAR* pExtName);




/*
===============================================================================
###############################################################################

	CORE - QUEUE FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Returns a queue handle for the specified queue type and ordinal.


	RETURNS
	If successful, grGetDeviceQueue() returns GR_SUCCESS and the queried queue handle is written to the location specified by pQueue. Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_ORDINAL if the queue ordinal exceeds the number of queues requested at device creation
	▼ GR_ERROR_INVALID_POINTER if pQueue is NULL
	▼ GR_ERROR_INVALID_QUEUE_TYPE if queueType is invalid


	NOT THREAD SAFE for calls referencing the same device object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetDeviceQueue(
	/* Device handle. */
	GR_DEVICE device,
	/* Queue type. See GR_QUEUE_TYPE. */
	GR_ENUM queueType,
	/* Queue ordinal for the given queue type. */
	GR_UINT queueId,
	/* [out] Queue handle. */
	GR_QUEUE* pQueue);

/*
	Waits for a specific queue to complete execution of all submitted command buffers before returning to the application.


	RETURNS
	grQueueWaitIdle() returns GR_SUCCESS if the function executed successfully.
	
	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the queue handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the queue handle references an invalid object type


	NOT THREAD SAFE for calls referencing the same device object associated with the queue or any other objects associated with that device.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grQueueWaitIdle(
	/* Queue handle. */
	GR_QUEUE queue);

/*
	Waits for all queues associated with a device to complete execution of all submitted command buffers before returning to the application.


	RETURNS
	grDeviceWaitIdle() returns GR_SUCCESS if the function executed successfully.
	
	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type


	NOT THREAD SAFE for calls referencing the same device object associated with the queue or any other objects associated with that device.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grDeviceWaitIdle(
	/* Device handle. */
	GR_DEVICE device);

/*
	Submits a command buffer to a queue for execution.


	RETURNS
	grQueueSubmit() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the queue handle is invalid
	▼ GR_ERROR_INVALID_HANDLE if any memory or command buffer object handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the queue handle references an invalid object type
	▼ GR_ERROR_TOO_MANY_MEMORY_REFERENCES if the total number of memory references (queue global and per-command buffer) is too large
	▼ GR_ERROR_INVALID_POINTER if pMemRefs is NULL when the memory reference count is greater than zero
	▼ GR_ERROR_INVALID_POINTER if pCmdBuffers is NULL
	▼ GR_ERROR_INVALID_VALUE if cmdBufferCount is zero
	▼ GR_ERROR_INVALID_FLAGS if flags in pMemRefs[].flags are invalid
	▼ GR_ERROR_INCOMPLETE_COMMAND_BUFFER if any of the submitted command buffers is not properly constructed
	▼ GR_ERROR_INCOMPATIBLE_QUEUE if a command buffer is submitted with the wrong queue type


	NOTES
	When a valid fence object is provided, the driver submits the fence after the last command buffer from the list executes.


	NOT THREAD SAFE for calls referencing the same queue object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grQueueSubmit(
	/* Queue handle. */
	GR_QUEUE queue,
	/* Number of command buffers to be submitted. */
	GR_UINT cmdBufferCount,
	/* [in] List of command buffer handles. */
	const GR_CMD_BUFFER* pCmdBuffers,
	/* Number of memory object references for this command buffer (i.e., size of the pMemRefs array). Can be zero. */
	GR_UINT memRefCount,
	/* [in] Array of memory reference descriptors. Can be NULL if memRefCount is zero. See GR_MEMORY_REF. */
	const GR_MEMORY_REF* pMemRefs,
	/* Handle of the fence object to be associated with this submission (optional, can be GR_NULL_HANDLE). */
	GR_FENCE fence);

/*
	Sets a list of per-queue memory object references that persists across command buffer submissions. A snapshot of the current global queue memory reference list is taken at command buffer submission time. After submission, the global queue memory reference list can be changed for subsequent submissions without affecting previously queued submissions.


	RETURNS
	grQueueSetGlobalMemReferences() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the queue handle is invalid or if any memory object handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the queue handle references an invalid object type
	▼ GR_ERROR_TOO_MANY_MEMORY_REFERENCES if the number of global memory references exceeds the limit supported by the device
	▼ GR_ERROR_INVALID_POINTER if pMemRefs is NULL
	▼ GR_ERROR_INVALID_FLAGS if flags in pMemRefs[].flags are invalid.


	NOT THREAD SAFE for calls referencing the same queue object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grQueueSetGlobalMemReferences(
	/* Queue handle. */
	GR_QUEUE queue,
	/* Number of global memory object references for this queue (i.e., size of the pMemRefs array). Can be zero. */
	GR_UINT memRefCount,
	/* [in] Array of memory reference descriptors. See grQueueSubmit(). Can be NULL if memRefCount is zero. See GR_MEMORY_REF. */
	const GR_MEMORY_REF* pMemRefs);




/*
===============================================================================
###############################################################################

	CORE - MEMORY MANAGEMENT FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Returns the number of GPU memory heaps for a Mantle device.


	RETURNS
	If successful, grGetMemoryHeapCount() returns GR_SUCCESS and the number of GPU memory heaps is written to the location specified by pCount.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pCount is NULL


	NOTES
	The number of heaps returned is guaranteed to be at least one.


	NOT THREAD SAFE for calls referencing the same device object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetMemoryHeapCount(
	/* Device handle. */
	GR_DEVICE device,
	/* [out] Number of GPU memory heaps. */
	GR_UINT* pCount);

/*
	Retrieves specific information about a GPU memory heap.


	RETURNS
	If successful, grGetMemoryHeapInfo() returns GR_SUCCESS and the queried info is written to the location specified by pData.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if infoType is invalid
	▼ GR_ERROR_INVALID_ORDINAL if the GPU memory heap ordinal is invalid
	▼ GR_ERROR_INVALID_POINTER if pDataSize is NULL
	▼ GR_ERROR_INVALID_MEMORY_SIZE if pData is not NULL and pDataSize input value is smaller than the size of the appropriate return data structure


	NOTES
	If pData is NULL, the input pDataSize value does not matter and the function returns the expected data structure size in pDataSize.

	For heaps not visible by the CPU (the GR_MEMORY_HEAP_FLAG_CPU_VISIBLE flag is not set), the CPU read and write performance ratings are zero.


	NOT THREAD SAFE for calls referencing the same device object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetMemoryHeapInfo(
	/* Device handle. */
	GR_DEVICE device,
	/* GPU memory heap ordinal up to the number of heaps reported by grGetMemoryHeapCount(). */
	GR_UINT heapId,
	/* Type of information to retrieve. See GR_INFO_TYPE. */
	GR_ENUM infoType,
	/* [in/out] Input value specifies the size in bytes of the pData output buffer; output value reports the number of bytes written to pData. */
	GR_SIZE* pDataSize,
	/* [out] Memory heap information structure. */
	GR_VOID* pData);

/*
	Allocates GPU memory by creating a memory object.


	RETURNS
	If successful, grAllocMemory() returns GR_SUCCESS and the handle of the created GPU
	memory object is written to the location specified by pMem.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pAllocInfo or pMem are NULL
	▼ GR_ERROR_INVALID_MEMORY_SIZE if the allocation size is invalid
	▼ GR_ERROR_INVALID_ALIGNMENT if the allocation alignment is invalid
	▼ GR_ERROR_INVALID_VALUE if the priority value is invalid, or if pAllocInfo.heapCount is zero for real allocations or greater than zero for virtual allocations
	▼ GR_ERROR_INVALID_ORDINAL if an invalid valid heap ordinal is specified or if a heap ordinal is used more than once
	▼ GR_ERROR_INVALID_FLAGS if the flags are invalid or incompatible
	▼ GR_ERROR_OUT_OF_GPU_MEMORY if memory object creation failed due to a lack of video memory
	▼ GR_ERROR_UNAVAILABLE if attempting to create virtual allocation and memory virtual remapping functionality is unavailable


	NOTES
	A particular heap ID may not appear in the heap list more than once. Real allocations must have at least one heap specified.

	Virtual allocations must have zero heaps specified. Priority has no effect for virtual allocations.

	Memory size is specified in bytes and must be a multiple of the heap's page size. If an allocation can be placed in multiple memory heaps, the largest page size should be used.

	The optional memory allocation alignment is specified in bytes. When zero, the alignment is equal to the specified page size, otherwise it must be a multiple of the page size.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grAllocMemory(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Creation data for the memory object. See GR_MEMORY_ALLOC_INFO. */
	const GR_MEMORY_ALLOC_INFO* pAllocInfo,
	/* [out] Memory object handle. */
	GR_GPU_MEMORY* pMem);

/*
	Frees GPU memory and destroys the memory object. For pinned memory objects, the underlying system memory is unpinned.


	RETURNS
	grFreeMemory() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the mem handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the mem handle references an invalid object type
	▼ GR_ERROR_UNAVAILABLE if the mem handle references an internal memory object that cannot be freed


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grFreeMemory(
	/* Memory object handle. */
	GR_GPU_MEMORY mem);

/*
	Sets a new priority for the specified memory object.


	RETURNS
	grSetMemoryPriority() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the mem handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the mem handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if priority is invalid
	▼ GR_ERROR_UNAVAILABLE if the memory object is not a real allocation


	NOTES
	Not available for pinned and virtual memory objects.


	NOT THREAD SAFE for calls referencing the same memory object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grSetMemoryPriority(
	/* Memory object handle. */
	GR_GPU_MEMORY mem,
	/* New priority for the memory object. See GR_MEMORY_PRIORITY. */
	GR_ENUM priority);

/*
	Get a CPU pointer to the data contained in a memory object.


	RETURNS
	If successful, grMapMemory() returns GR_SUCCESS and a pointer to the CPU-accessible memory object data is written to the location specified by ppData.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the mem handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the mem handle references an invalid object type
	▼ GR_ERROR_INVALID_FLAGS if the flags are invalid
	▼ GR_ERROR_INVALID_POINTER if ppData is NULL
	▼ GR_ERROR_MEMORY_MAP_FAILED if the memory object is busy and cannot be mapped by the OS
	▼ GR_ERROR_NOT_MAPPABLE if the memory object cannot be mapped due to some of its heaps not having the CPU visible flag set
	▼ GR_ERROR_UNAVAILABLE if the memory object is not a real allocation


	NOTES
	Memory objects cannot be mapped multiple times concurrently. Mapping is not available for pinned and virtual memory objects.


	NOT THREAD SAFE for calls referencing the same memory object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grMapMemory(
	/* Memory object handle. */
	GR_GPU_MEMORY mem,
	/* Map flags, reserved. */
	GR_FLAGS flags,
	/* [out] CPU pointer to the memory object data. */
	GR_VOID** ppData);

/*
	Remove CPU access from a previously mapped memory object.


	RETURNS
	grUnmapMemory() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the mem handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the mem handle references an invalid object type
	▼ GR_ERROR_UNAVAILABLE if the memory object is not a real allocation
	▼ GR_ERROR_MEMORY_UNMAP_FAILED if the memory object cannot be unlocked by the OS


	NOTES
	Not available for pinned and virtual memory objects.


	NOT THREAD SAFE for calls referencing the same memory object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grUnmapMemory(
	/* Memory object handle. */
	GR_GPU_MEMORY mem);

/*
	Update memory mappings for a virtual allocation. The remapping is performed on a page boundary.


	RETURNS
	grRemapVirtualMemoryPages() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_HANDLE if the pRanges.virtualMem handles are invalid or reference non-virtual allocations
	▼ GR_ERROR_INVALID_HANDLE if the pRanges.realMem handles are invalid or reference non-real allocations
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the page range for any of the memory objects is invalid
	▼ GR_ERROR_INVALID_POINTER if pRanges is a NULL pointer
	▼ GR_ERROR_INVALID_POINTER if pPreWaitSemaphores is a NULL pointer, but preWaitSemaphoreCount is greater than zero
	▼ GR_ERROR_INVALID_POINTER if pPostSignalSemaphores is a NULL pointer, but postSignalSemaphoreCount is greater than zero
	▼ GR_ERROR_UNAVAILABLE if memory virtual remapping functionality is unavailable


	NOTES
	It is valid to specify a GR_NULL_HANDLE object handle in pRanges.realMem – it unmaps pages in the specified range. Remapping is not available for pinned and real memory objects.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grRemapVirtualMemoryPages(
	/* Device handle. */
	GR_DEVICE device,
	/* Number of ranges to remap. */
	GR_UINT rangeCount,
	/* [in] Array of memory range descriptors. See GR_VIRTUAL_MEMORY_REMAP_RANGE. */
	const GR_VIRTUAL_MEMORY_REMAP_RANGE* pRanges,
	/* Number of semaphores in pPreWaitSemaphores. */
	GR_UINT preWaitSemaphoreCount,
	/* [in] Array of queue semaphores to wait on before performing memory remapping. Can be NULL if preWaitSemaphoreCount is zero. */
	const GR_QUEUE_SEMAPHORE* pPreWaitSemaphores,
	/* Number of semaphores in pPostSignalSemaphores. */
	GR_UINT postSignalSemaphoreCount,
	/* [in] Array of queue semaphores to signal after performing memory remapping. Can be NULL if postSignalSemaphoreCount is zero. */
	const GR_QUEUE_SEMAPHORE* pPostSignalSemaphores);

/*
	Pins a system memory region and creates a Mantle memory object representing it. Pinned memory objects are freed via grFreeMemory() like regular memory objects.


	RETURNS
	If successful, grPinSystemMemory() returns GR_SUCCESS and a GPU memory object handle representing the pinned memory is written to the location specified by pMem.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_OUT_OF_MEMORY if memory object creation failed due to an inability to pin memory
	▼ GR_ERROR_INVALID_MEMORY_SIZE if memSize is not page size aligned
	▼ GR_ERROR_INVALID_POINTER if pMem is NULL, or pSysMem is NULL, or pSysMem is not page size aligned
	▼ GR_ERROR_UNAVAILABLE if memory pinning functionality is unavailable


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grPinSystemMemory(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Pointer to the system memory region to pin. Must be aligned to a page boundary of the heap marked with the GR_MEMORY_HEAP_FLAG_HOLDS_PINNED flag. */
	const GR_VOID* pSysMem,
	/* Size of the system memory region to pin. Must be aligned to a page boundary of the heap marked with the GR_MEMORY_HEAP_FLAG_HOLDS_PINNED flag. */
	GR_SIZE memSize,
	/* [out] Memory object handle. */
	GR_GPU_MEMORY* pMem);




/*
===============================================================================
###############################################################################

	CORE - GENERIC API OBJECT MANAGEMENT FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Destroys an API object. Not applicable to devices, queues, or memory objects.


	RETURNS
	grDestroyObject() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the object handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the object handle references an invalid object type


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grDestroyObject(
	/* API object handle. */
	GR_OBJECT object);

/*
	Retrieves info such as memory requirements for a given API object. Not applicable to physical GPU objects.


	RETURNS
	If successful, grGetObjectInfo() returns GR_SUCCESS and the queried API object information is written to the location specified by pData.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the object handle is invalid
	▼ GR_ERROR_INVALID_VALUE if infoType is invalid for the given object type or debug information is requested without enabling the validation layer
	▼ GR_ERROR_INVALID_MEMORY_SIZE if pData is not NULL and pDataSize input value is smaller than the size of the appropriate return data structure
	▼ GR_ERROR_INVALID_POINTER if pDataSize is NULL.
	▼ GR_ERROR_UNAVAILABLE if running with the validation layer enabled, attempting to retrieve an object tag and the tag information is not attached to an object


	NOTES
	If pData is NULL, the input pDataSize value does not matter and the function returns the expected data structure size in pDataSize.

	Memory requirements

	Memory requirements are retrieved with the GR_INFO_TYPE_MEMORY_REQUIREMENTS information type. The returned data are in the GR_MEMORY_REQUIREMENTS structure. Available for all objects types except physical GPU, device, queue, shader, and memory, which do not support memory binding.

	heaps[] in GR_MEMORY_REQUIREMENTS stores the heap ordinals (same as those used with grGetMemoryHeapInfo()).

	Not all objects have memory requirements, in which case it is valid for the requirements structure to return zero size and alignment, and no heaps. For objects with valid memoryrequirements, at least one valid heap is returned.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetObjectInfo(
	/* API object handle. */
	GR_BASE_OBJECT object,
	/* Type of object information to retrieve; valid values vary by object type. See GR_INFO_TYPE. */
	GR_ENUM infoType,
	/* [in/out] Input value specifies the size in bytes of the pData output buffer; output value reports the number of bytes written to pData. */
	GR_SIZE* pDataSize,
	/* [out] Object info structure. Can be NULL. */
	GR_VOID* pData);

/*
	Binds memory to an API object according to previously queried memory requirements. Not applicable to devices, queues, shaders, or memory objects. Specifying a GR_NULL_HANDLE memory object unbinds the currently bound memory from an object.


	RETURNS
	grBindObjectMemory() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the object handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the referenced object does not supports memory binding – a device, queue, shader or memory object
	▼ GR_ERROR_UNAVAILABLE if binding non-image type object to virtual allocation
	▼ GR_ERROR_INVALID_ALIGNMENT if the offset does not match the alignment requirements for the object
	▼ GR_ERROR_INVALID_MEMORY_SIZE if the API object memory size at the given offset is not completely within the memory object range
	▼ GR_ERROR_UNAVAILABLE if the object does not have any memory requirements


	NOTES
	Binding memory to objects other than images automatically initializes the object memory as necessary. Image objects used as color or depth-stencil targets have to be explicitly initialized in command buffers using a grCmdPrepareImages() command to transition them from GR_IMAGE_STATE_UNINITIALIZED to an appropriate image state.

	Device, queue, shader, and memory objects do not support memory binding.

	Binding memory to an object automatically unbinds any previously bound memory. There is no need to bind GR_NULL_HANDLE memory to an object to explicitly unbind previously bound memory before binding a new memory.

	This call is invalid on objects that have no memory requirements, even if binding GR_NULL_HANDLE memory.

	Virtual memory objects can only be used for binding image objects.


	NOT THREAD SAFE for calls referencing the same API object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grBindObjectMemory(
	/* API object handle. */
	GR_OBJECT object,
	/* Memory object handle to use for memory binding. Can be GR_NULL_HANDLE. */
	GR_GPU_MEMORY mem,
	/* Byte offset into the memory object. */
	GR_GPU_SIZE offset);




/*
===============================================================================
###############################################################################

	CORE - IMAGE AND SAMPLER FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Retrieves resource format information and capabilities.


	RETURNS
	If successful, grGetFormatInfo() returns GR_SUCCESS and the queried format info is written to the location specified by pData.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if infoType is invalid
	▼ GR_ERROR_INVALID_FORMAT if format is invalid
	▼ GR_ERROR_INVALID_POINTER if pDataSize is NULL
	▼ GR_ERROR_INVALID_MEMORY_SIZE if pData is not NULL and pDataSize input value is smaller than the size of the appropriate return data structure


	NOTES
	If pData is NULL, the input pDataSize value does not matter and the function returns the expected data structure size in pDataSize.

	Currently only GR_INFO_TYPE_FORMAT_PROPERTIES information type is valid. The returned information is in GR_FORMAT_PROPERTIES.

	It is allowed for some channel and numeric format combinations to expose no capabilities. The format is not illegal from the API perspective, but it cannot really be used for anything.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetFormatInfo(
	/* Device handle. */
	GR_DEVICE device,
	/* Resource format. See GR_FORMAT. */
	GR_FORMAT format,
	/* Type of format information to retrieve. See GR_INFO_TYPE. */
	GR_ENUM infoType,
	/* [in/out] Input value specifies the size in bytes of the pData output buffer; output value reports the number of bytes written to pData. */
	GR_SIZE* pDataSize,
	/* [out] Format information structure. Can be NULL. */
	GR_VOID* pData);

/*
	Creates a 1D, 2D or 3D image object.


	RETURNS
	If successful, grCreateImage() returns GR_SUCCESS and the created image object handle is written to the location specified by pImage.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the image type or tiling type is invalid
	▼ GR_ERROR_INVALID_VALUE if the image dimensions are invalid for a given image type
	▼ GR_ERROR_INVALID_VALUE if for compressed formats, the image dimensions aren't multiples of the compression block size
	▼ GR_ERROR_INVALID_VALUE if the number of samples is invalid for the given image type and format
	▼ GR_ERROR_INVALID_VALUE if MSAA is enabled (has samples > 1) for images that don't have a color target or depth-stencil usage flag set
	▼ GR_ERROR_INVALID_VALUE if MSAA images have more than 1 mipmap level
	▼ GR_ERROR_INVALID_VALUE if the array size is zero or greater than supported for 1D or 2D images, or the arrays size isn't equal to 1 for 3D images
	▼ GR_ERROR_INVALID_VALUE if the size of the mipmap chain is invalid for the given image type and dimensions
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pImage is NULL
	▼ GR_ERROR_INVALID_FORMAT if the format doesn't match usage flags
	▼ GR_ERROR_INVALID_FORMAT if a compressed format is used with a 1D image type
	▼ GR_ERROR_INVALID_FLAGS if invalid image creation flags or image usage flags are specified
	▼ GR_ERROR_INVALID_FLAGS if color target and depth-stencil flags are set together
	▼ GR_ERROR_INVALID_FLAGS if the color target flag is set for 1D images
	▼ GR_ERROR_INVALID_FLAGS if the depth-stencil flag is set for non-2D images


	NOTES
	The GR_IMAGE_USAGE_COLOR_TARGET and GR_IMAGE_USAGE_DEPTH_STENCIL flags are mutually exclusive. Depth-stencil images must be 2D, color target images must be 2D or 3D.

	The number of mipmap level is specified explicitly and should always be greater than or equal to 1.

	The number of samples greater than 1 is only available for 2D images and only for formats that support multisampling.

	The array size must be 1 for 3D images.

	Images with more than 1 sample (MSAA images) must have only 1 mipmap level.

	1D images ignore height and depth parameters, 2D images ignore depth parameter.

	For compressed images, the dimensions are specified in texels and the top-most mipmap level dimensions must be a multiple of the compression block size.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateImage(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Image creation info. See GR_IMAGE_CREATE_INFO. */
	const GR_IMAGE_CREATE_INFO* pCreateInfo,
	/* [out] Image object handle. */
	GR_IMAGE* pImage);

/*
	Retrieves information about an image subresource.


	RETURNS
	If successful, grGetImageSubresourceInfo() returns GR_SUCCESS and the queried
	subresource information is written to the location specified by pData.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the image handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the image handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if infoType is invalid
	▼ GR_ERROR_INVALID_ORDINAL if the subresource ID ordinal is invalid
	▼ GR_ERROR_INVALID_POINTER if pDataSize is NULL
	▼ GR_ERROR_INVALID_MEMORY_SIZE if pData isn't NULL and pDataSize input value is smaller than the size of the appropriate return data structure


	NOTES
	If pData is NULL, the input pDataSize value does not matter and the function returns the expected data structure size in pDataSize.

	The internal subresource memory layout is returned by querying subresource properties with the GR_INFO_TYPE_SUBRESOURCE_LAYOUT information type. The returned data are in the GR_SUBRESOURCE_LAYOUT structure. The offset returned in the layout structure is relative to the beginning of the memory range associated with the image object.

	Depending on the internal memory organization of the image, some image subresources may alias to the same offset.

	For opaque images, the returned pitch values are zero.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetImageSubresourceInfo(
	/* Image handle. */
	GR_IMAGE image,
	/* Pointer to subresource ID to retrieve information about. See GR_IMAGE_SUBRESOURCE. */
	const GR_IMAGE_SUBRESOURCE* pSubresource,
	/* Type of information to retrieve. See GR_INFO_TYPE. */
	GR_ENUM infoType,
	/* [in/out] Input value specifies the size in bytes of the pData output buffer; output value reports the number of bytes written to pData. */
	GR_SIZE* pDataSize,
	/* [out] Subresource information structure. Can be NULL. */
	GR_VOID* pData);

/*
	Creates a sampler object.


	RETURNS
	If successful, grCreateSampler() returns GR_SUCCESS and the handle of the created sampler object is written to the location specified by pSampler.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if any filtering, addressing modes or comparison function are invalid
	▼ GR_ERROR_INVALID_VALUE if the border color value is invalid
	▼ GR_ERROR_INVALID_VALUE if the anisotropy or level-of-detail (LOD) bias value is outside of the allowed range
	▼ GR_ERROR_INVALID_VALUE if the min/max LOD values are outside of the allowed range or the max LOD is smaller than min LOD
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pSampler is NULL


	NOTES
	Min/max LOD and mipmap LOD bias values are specified in floating point with the value of 0.0 corresponding to the largest mipmap level, 1.0 corresponding to the next mipmap level, and so on. The valid range for min/max LOD is [0..16] and for mipmap LOD bias the valid range is [16..16].

	The valid range for max anisotropy values is [1..16].

	More texture filter modes can be added to reflect hardware capabilities.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateSampler(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Sampler creation info. See GR_SAMPLER_CREATE_INFO. */
	const GR_SAMPLER_CREATE_INFO* pCreateInfo,
	/* [out] Sampler object handle. */
	GR_SAMPLER* pSampler);




/*
===============================================================================
###############################################################################

	CORE - IMAGE VIEW FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Creates an image representation that can be bound to the graphics or compute pipeline for shader read or write access.


	RETURNS
	If successful, grCreateImageView() returns GR_SUCCESS and the handle of the created image view object is written to the location specified by pView.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid or the image handle in pCreateInfo is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the view type or image aspect is invalid
	▼ GR_ERROR_INVALID_VALUE if the channel swizzle value is invalid or the swizzle refers to a channel not present in the given format
	▼ GR_ERROR_INVALID_VALUE if the color image aspect is specified for a depth/stencil image
	▼ GR_ERROR_INVALID_VALUE if the depth image aspect is specified for an image that doesn't have depth
	▼ GR_ERROR_INVALID_VALUE if the stencil image aspect is specified for an image that doesn't have stencil
	▼ GR_ERROR_INVALID_VALUE if the number of array slices is zero or a range of slices starting from the base is greater than what is available in the image object
	▼ GR_ERROR_INVALID_VALUE if the base mipmap level is invalid for the given image object
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pView is NULL
	▼ GR_ERROR_INVALID_FORMAT if the view format is incompatible with the image format (has a different bit-depth)
	▼ GR_ERROR_INVALID_FORMAT if the view format is incompatible with the image usage flags or image aspect
	▼ GR_ERROR_INVALID_IMAGE if the image object doesn't have the shader read or write flags set
	▼ GR_ERROR_INVALID_IMAGE if the view type is incompatible with the given image object type
	▼ GR_ERROR_INVALID_IMAGE if a cubemap view is created for an image object that doesn't have square 2D slices
	▼ GR_ERROR_INVALID_IMAGE if a cubemap view is created for an MSAA image


	NOTES
	The view references a subset of mipmap levels and image array slices or the whole image.

	The range of mipmap levels is truncated to the available dimensions of the image object.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateImageView(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] View creation info. See GR_IMAGE_VIEW_CREATE_INFO. */
	const GR_IMAGE_VIEW_CREATE_INFO* pCreateInfo,
	/* [out] Image view handle. */
	GR_IMAGE_VIEW* pView);

/*
	Creates an image representation that can be bound to the graphics pipeline state for color render target writes.


	RETURNS
	If successful, grCreateColorTargetView() returns GR_SUCCESS and the handle of the created color target view object is written to the location specified by pView.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid or the image handle in pCreateInfo is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the base slice is invalid for the given image object and view type
	▼ GR_ERROR_INVALID_VALUE if the number of array slices is zero or the range of slices is greater than what is available in the image object
	▼ GR_ERROR_INVALID_VALUE if the mipmap level is invalid for the given image object
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pView is NULL
	▼ GR_ERROR_INVALID_IMAGE if the image object doesn't have the color target access flag set


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateColorTargetView(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Color target view creation info. See GR_COLOR_TARGET_VIEW_CREATE_INFO. */
	const GR_COLOR_TARGET_VIEW_CREATE_INFO* pCreateInfo,
	/* [out] Color render target view handle. */
	GR_COLOR_TARGET_VIEW* pView);

/*
	Creates an image representation that can be bound to the graphics pipeline state as a depthstencil target.


	RETURNS
	If successful, grCreateDepthStencilView() returns GR_SUCCESS and the handle of the created depth-stencil target view object is written to the location specified by pView.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid or the image handle in pCreateInfo is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the base slice is invalid for the given image object and view type
	▼ GR_ERROR_INVALID_VALUE if the number of array slices is zero or the range of slices is greater than what is available in the image object
	▼ GR_ERROR_INVALID_VALUE if the mipmap level is invalid for the given image object
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pView is NULL
	▼ GR_ERROR_INVALID_IMAGE if the image object doesn't have the depth-stencil target access flag set
	▼ GR_ERROR_INVALID_IMAGE if the image object doesn't have the appropriate depth or stencil aspect for read-only depth or stencil flag
	▼ GR_ERROR_INVALID_FLAGS if the view creation flags are invalid


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateDepthStencilView(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Depth-stencil target view creation info. See GR_DEPTH_STENCIL_VIEW_CREATE_INFO. */
	const GR_DEPTH_STENCIL_VIEW_CREATE_INFO* pCreateInfo,
	/* [out] Depth-stencil target view handle. */
	GR_DEPTH_STENCIL_VIEW* pView);




/*
===============================================================================
###############################################################################

	CORE - SHADER AND PIPELINE FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Creates a shader object for the specified source IL.


	RETURNS
	If successful, grCreateShader() returns GR_SUCCESS and the handle of the created shader object is written to the location specified by pShader.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if code size is zero
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo, or pShader, or code pointer is NULL
	▼ GR_ERROR_UNSUPPORTED_SHADER_IL_VERSION if shader IL version is not supported
	▼ GR_ERROR_BAD_SHADER_CODE if an unknown shader type or inconsistent shader code is detected
	▼ GR_ERROR_INVALID_FLAGS if flags are invalid


	NOTES
	Pre-processes shader IL and performs rudimentary validation of the correctness of shader IL code.


	THREAD SAFE.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateShader(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Shader creation info. See GR_SHADER_CREATE_INFO. */
	const GR_SHADER_CREATE_INFO* pCreateInfo,
	/* [out] Shader handle. */
	GR_SHADER* pShader);

/*
	Creates a graphics pipeline object.


	RETURNS
	If successful, grCreateGraphicsPipeline() returns GR_SUCCESS and the handle of the created graphics pipeline object is written to the location specified by pPipeline.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_HANDLE if the vertex shader handle is invalid
	▼ GR_ERROR_INVALID_HANDLE if either hull or domain shader handle is GR_NULL_HANDLE (both have to be either valid or GR_NULL_HANDLE occurs)
	▼ GR_ERROR_INVALID_VALUE if the topology value is invalid
	▼ GR_ERROR_INVALID_VALUE if the logic operation in the color output and blender state is valid
	▼ GR_ERROR_INVALID_VALUE if the primitive type is invalid for the given pipeline configuration
	▼ GR_ERROR_INVALID_VALUE if the number of control points is invalid for the tessellation pipeline
	▼ GR_ERROR_INVALID_VALUE if the logic operation is enabled while some of the color targets enable blending
	▼ GR_ERROR_INVALID_VALUE if the dual source blend enable doesn't match expectations for the color target and blend enable setup
	▼ GR_ERROR_INVALID_VALUE if for any of the active shader stages the pLinkConstBufferInfo pointer isn't consistent with the linkConstBufferCount value (the pLinkConstBufferInfo pointer should be valid only if linkConstBufferCount is greater than zero)
	▼ GR_ERROR_INVALID_VALUE if for any of the active shader stages the dynamic data mapping slot object type is invalid (should be either unused, or resource, or UAV)
	▼ GR_ERROR_INVALID_VALUE if the link time constant buffer size or ID is invalid
	▼ GR_ERROR_INVALID_FORMAT if the specified depth-stencil format is invalid
	▼ GR_ERROR_INVALID_FORMAT if the specified color target format is invalid
	▼ GR_ERROR_INVALID_FORMAT if blending is enabled, but the color target format is not compatible with blending
	▼ GR_ERROR_INVALID_FORMAT if a logic op is enabled, but an incompatible format is used
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pPipeline is NULL
	▼ GR_ERROR_INVALID_POINTER if the link time constant data pointer is NULL
	▼ GR_ERROR_UNSUPPORTED_SHADER_IL_VERSION if an incorrect shader type is used in any of the shader stages
	▼ GR_ERROR_INVALID_FLAGS if the flags are invalid
	▼ GR_ERROR_INVALID_VALUE if the descriptor set slot type is invalid
	▼ GR_ERROR_INVALID_DESCRIPTOR_SET_DATA if the descriptor set data are invalid or inconsistent


	NOTES
	If the pipeline does not use a color target, the target's format must be undefined and color write mask must be zero.

	If the pipeline doesn't use depth-stencil, the depth-stencil's format must be undefined.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateGraphicsPipeline(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Pipeline creation info. See GR_GRAPHICS_PIPELINE_CREATE_INFO. */
	const GR_GRAPHICS_PIPELINE_CREATE_INFO* pCreateInfo,
	/* [out] Pipeline handle. */
	GR_PIPELINE* pPipeline);

/*
	Creates a compute pipeline object.


	RETURNS
	If successful, grCreateComputePipeline() returns GR_SUCCESS and the handle of the created compute pipeline object is written to the location specified by pPipeline.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_HANDLE if the compute shader handle is invalid
	▼ GR_ERROR_INVALID_VALUE if the pLinkConstBufferInfo pointer isn't consistent with the linkConstBufferCount value (the pLinkConstBufferInfo pointer should be valid only if the linkConstBufferCount is greater than zero)
	▼ GR_ERROR_INVALID_VALUE if the dynamic data mapping slot object type is invalid (should be either unused, resource, or UAV)
	▼ GR_ERROR_INVALID_VALUE if the link time constant buffer size or ID is invalid
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pPipeline is NULL
	▼ GR_ERROR_INVALID_POINTER if the link time constant data pointer is NULL
	▼ GR_ERROR_UNSUPPORTED_SHADER_IL_VERSION if an incorrect shader type is used in any of the shader stages
	▼ GR_ERROR_INVALID_FLAGS if the flags are invalid
	▼ GR_ERROR_INVALID_DESCRIPTOR_SET_DATA if descriptor set data are invalid or inconsistent.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateComputePipeline(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Pipeline creation info. See GR_COMPUTE_PIPELINE_CREATE_INFO. */
	const GR_COMPUTE_PIPELINE_CREATE_INFO* pCreateInfo,
	/* [out] Compute pipeline handle. */
	GR_PIPELINE* pPipeline);

/*
	Stores an internal binary pipeline representation to a region of CPU memory.


	RETURNS
	If successful, grStorePipeline() returns GR_SUCCESS and the internal binary pipeline representation is stored to the location specified by pData.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the pipeline handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the pipeline handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pDataSize is NULL
	▼ GR_ERROR_INVALID_MEMORY_SIZE if pData isn't NULL and pDataSize input value is smaller than the size of the appropriate return data structure


	NOTES
	If pData is NULL, the input pDataSize value does not matter and the function returns the expected pipeline data size in pDataSize.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grStorePipeline(
	/* Pipeline handle. */
	GR_PIPELINE pipeline,
	/* [in/out] Input value specifies the size in bytes of the pData output buffer; output value reports the number of bytes written to pData. */
	GR_SIZE* pDataSize,
	/* [out] Internal binary pipeline representation. Can be NULL. */
	GR_VOID* pData);

/*
	Creates a pipeline object from an internal binary representation. Only works when the GPU and driver version match those used when grStorePipeline() generated the binary representation.


	RETURNS
	If successful, grLoadPipeline() returns GR_SUCCESS and the handle of the created pipeline object is written to the location specified by pPipeline.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_MEMORY_SIZE if dataSize value does not match the expected pipeline data size
	▼ GR_ERROR_INVALID_POINTER if pData is NULL
	▼ GR_ERROR_INCOMPATIBLE_DEVICE if the device is incompatible with the GPU device where the binary pipeline was saved
	▼ GR_ERROR_INCOMPATIBLE_DRIVER if the driver version is incompatible with the one used for saving the binary pipeline
	▼ GR_ERROR_BAD_PIPELINE_DATA if invalid pipeline code is detected


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grLoadPipeline(
	/* Device handle. */
	GR_DEVICE device,
	/* Data size for internal binary pipeline representation. */
	GR_SIZE dataSize,
	/* [in] Internal binary pipeline representation as generated by grStorePipeline(). */
	const GR_VOID* pData,
	/* [out] Pipeline handle. */
	GR_PIPELINE* pPipeline);




/*
===============================================================================
###############################################################################

	CORE - DESCRIPTOR SET FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Creates a descriptor set object.


	RETURNS
	If successful, grCreateDescriptorSet() returns GR_SUCCESS and the handle of the created
	descriptor set object is written to the location specified by pDescriptorSet.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the number of slots is greater than the max allowed descriptor set size
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pDescriptorSet is NULL


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateDescriptorSet(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Descriptor set creation info. See GR_DESCRIPTOR_SET_CREATE_INFO. */
	const GR_DESCRIPTOR_SET_CREATE_INFO* pCreateInfo,
	/* [out] Descriptor set object handle. */
	GR_DESCRIPTOR_SET* pDescriptorSet);

/*
	Initiates a descriptor set update. Should be called before updating descriptor set slots using grAttachSamplerDescriptors(), grAttachImageViewDescriptors(), grAttachMemoryViewDescriptors(), grAttachNestedDescriptors(), or grClearDescriptorSetSlots().


	NOTES
	For performance reasons, this function does not perform any sanity checking.


	NOT THREAD SAFE for calls referencing the same descriptor set object.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grBeginDescriptorSetUpdate(
	/* Descriptor set handle. */
	GR_DESCRIPTOR_SET descriptorSet);

/*
	Ends a descriptor set update. Should be called after updating descriptor set slots.


	NOTES
	For performance reasons, this function does not perform any sanity checking.


	NOT THREAD SAFE for calls referencing the same descriptor set object.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grEndDescriptorSetUpdate(
	/* Descriptor set handle. */
	GR_DESCRIPTOR_SET descriptorSet);

/*
	Updates a range of descriptor set slots with sampler objects.


	NOTES
	For performance reasons, this function does not perform any sanity checking.


	NOT THREAD SAFE for calls referencing the same descriptor set object.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grAttachSamplerDescriptors(
	/* Descriptor set handle. */
	GR_DESCRIPTOR_SET descriptorSet,
	/* First descriptor set slot in a range to update. */
	GR_UINT startSlot,
	/* Number of descriptor set slots to update. */
	GR_UINT slotCount,
	/* [in] Array of sampler object handles. */
	const GR_SAMPLER* pSamplers);

/*
	Updates a range of descriptor set slots with image view objects.


	NOTES
	For performance reasons, this function does not perform any sanity checking.


	NOT THREAD SAFE for calls referencing the same descriptor set object.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grAttachImageViewDescriptors(
	/* Descriptor set handle. */
	GR_DESCRIPTOR_SET descriptorSet,
	/* First descriptor set slot in a range to update. */
	GR_UINT startSlot,
	/* Number of descriptor set slots to update. */
	GR_UINT slotCount,
	/* [in] Array of image view object handles and attachment properties. See GR_IMAGE_VIEW_ATTACH_INFO. */
	const GR_IMAGE_VIEW_ATTACH_INFO* pImageViews);

/*
	Updates a range of descriptor set slots with memory views.


	NOTES
	For performance reasons, this function does not perform any sanity checking.


	NOT THREAD SAFE for calls referencing the same descriptor set object.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grAttachMemoryViewDescriptors(
	/* Descriptor set handle. */
	GR_DESCRIPTOR_SET descriptorSet,
	/* First descriptor set slot in a range to update. */
	GR_UINT startSlot,
	/* Number of descriptor set slots to update. */
	GR_UINT slotCount,
	/* [in] Array of memory views. See GR_MEMORY_VIEW_ATTACH_INFO. */
	const GR_MEMORY_VIEW_ATTACH_INFO* pMemViews);

/*
	Updates a range of descriptor set slots with nested references to other descriptor sets.


	NOTES
	For performance reasons, this function does not perform any sanity checking.


	NOT THREAD SAFE for calls referencing the same descriptor set object.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grAttachNestedDescriptors(
	/* Descriptor set handle. */
	GR_DESCRIPTOR_SET descriptorSet,
	/* First descriptor set slot in a range to update. */
	GR_UINT startSlot,
	/* Number of descriptor set slots to update. */
	GR_UINT slotCount,
	/* [in] Array of nested descriptor sets and attachment point offsets. See GR_DESCRIPTOR_SET_ATTACH_INFO. */
	const GR_DESCRIPTOR_SET_ATTACH_INFO* pNestedDescriptorSets);

/*
	Resets a range of descriptor set slots to a cleared state.


	NOTES
	For performance reasons, this function does not perform any sanity checking.


	NOT THREAD SAFE for calls referencing the same descriptor set object.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grClearDescriptorSetSlots(
	/* Descriptor set handle. */
	GR_DESCRIPTOR_SET descriptorSet,
	/* First descriptor set slot in a range to update. */
	GR_UINT startSlot,
	/* Number of descriptor set slots to update. */
	GR_UINT slotCount);




/*
===============================================================================
###############################################################################

	CORE - STATE OBJECT FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Creates a viewport state object.


	RETURNS
	If successful, grCreateViewportState() returns GR_SUCCESS and the handle of the created viewport state object is written to the location specified by pState.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if an invalid number of viewports is specified
	▼ GR_ERROR_INVALID_VALUE if any viewport parameters are outside of the valid range
	▼ GR_ERROR_INVALID_VALUE if scissors are enabled and any scissor parameters are outside of the valid range
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pState is NULL


	NOTES
	Viewport offset is valid in the [-32768..32768] range and scissor offset is valid in the [0..32768] range. Both the viewport and scissor sizes cannot exceed 32768 pixels.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateViewportState(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Viewport state object creation info. See GR_VIEWPORT_STATE_CREATE_INFO. */
	const GR_VIEWPORT_STATE_CREATE_INFO* pCreateInfo,
	/* [out] Viewport state object handle. */
	GR_VIEWPORT_STATE_OBJECT* pState);

/*
	Creates a rasterizer state object.


	RETURNS
	If successful, grCreateRasterState() returns GR_SUCCESS and the handle of the created rasterizer state object is written to the location specified by pState.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the fill mode or cull mode is invalid
	▼ GR_ERROR_INVALID_VALUE if the face orientation is invalid
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pState is NULL


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateRasterState(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Rasterizer state object creation info. See GR_RASTER_STATE_CREATE_INFO. */
	const GR_RASTER_STATE_CREATE_INFO* pCreateInfo,
	/* [out] Rasterizer state object handle. */
	GR_RASTER_STATE_OBJECT* pState);

/*
	Creates a color blender state object.


	RETURNS
	If successful, grCreateColorBlendState() returns GR_SUCCESS and the handle of the created color blend state object is written to the location specified by pState.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the source or destination color/alpha blend operation is invalid for color targets that have blending enabled
	▼ GR_ERROR_INVALID_VALUE if the color/alpha blend function is invalid for color targets that have blending enabled
	▼ GR_ERROR_INVALID_VALUE if an unsupported blend function is used with a dual source blend
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pState is NULL


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateColorBlendState(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Blender state object creation info. See GR_COLOR_BLEND_STATE_CREATE_INFO. */
	const GR_COLOR_BLEND_STATE_CREATE_INFO* pCreateInfo,
	/* [out] Blender state object handle. */
	GR_COLOR_BLEND_STATE_OBJECT* pState);

/*
	Creates a depth-stencil state object.


	RETURNS
	If successful, grCreateDepthStencilState() returns GR_SUCCESS and the handle of the created depth-stencil state object is written to the location specified by pState.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the depth function is invalid
	▼ GR_ERROR_INVALID_VALUE if the depth bounds feature is enabled and the depth range is invalid
	▼ GR_ERROR_INVALID_VALUE if any stencil operation is invalid
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pState is NULL


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateDepthStencilState(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Depth-stencil state object creation info. See GR_DEPTH_STENCIL_STATE_CREATE_INFO. */
	const GR_DEPTH_STENCIL_STATE_CREATE_INFO* pCreateInfo,
	/* [out] Depth-stencil state object handle. */
	GR_DEPTH_STENCIL_STATE_OBJECT* pState);

/*
	Creates multisampling state object.


	RETURNS
	If successful, grCreateMsaaState() returns GR_SUCCESS and the handle of the created MSAA state object is written to the location specified by pState.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the number of samples is unsupported
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pState is NULL


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateMsaaState(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Multisampling state object creation info. See GR_MSAA_STATE_CREATE_INFO. */
	const GR_MSAA_STATE_CREATE_INFO* pCreateInfo,
	/* [out] Multisampling state object handle. */
	GR_MSAA_STATE_OBJECT* pState);




/*
===============================================================================
###############################################################################

	CORE - QUERY AND SYNCHRONIZATION FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Creates a query object pool of the specified size.


	RETURNS
	If successful, grCreateQueryPool() returns GR_SUCCESS and the handle of the created query pool object is written to the location specified by pQueryPool.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the query type is invalid
	▼ GR_ERROR_INVALID_VALUE if the number of slots is zero
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pQueryPool is NULL


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateQueryPool(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Query pool object creation info. See GR_QUERY_POOL_CREATE_INFO. */
	const GR_QUERY_POOL_CREATE_INFO* pCreateInfo,
	/* [out] Query pool handle. */
	GR_QUERY_POOL* pQueryPool);

/*
	Retrieves query results from a query pool. Multiple consecutive query results can be retrieved with one function call.


	RETURNS
	If the function is successful and all query slot information is available, grGetQueryPoolResults() returns GR_SUCCESS and the query results are written to the location specified by pData. If the function executed successfully and any of the requested query slots do not have results available, the function returns GR_NOT_READY.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the queryPool handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the queryPool handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the range of queries defined by startQuery and queryCount is invalid for the given query pool
	▼ GR_ERROR_INVALID_POINTER if pDataSize is NULL
	▼ GR_ERROR_INVALID_MEMORY_SIZE if pData isn't NULL and pDataSize input value is smaller than the size of the appropriate return data
	▼ GR_ERROR_MEMORY_NOT_BOUND if query pool requires GPU memory, but it wasn't bound


	NOTES
	If pData is NULL, the input pDataSize value does not matter and the function returns the expected data structure size in pDataSize.

	Occlusion query results
	For occlusion queries, the results are returned as an array of 64-bit unsigned integers, one per query pool slot.

	Pipeline statistics results
	For pipeline statistics queries, the results are returned as an array of the GR_PIPELINE_STATISTICS_DATA structures, one per query pool slot.


	NOT THREAD SAFE for calls referencing the same query pool object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetQueryPoolResults(
	/* Query pool handle. */
	GR_QUERY_POOL queryPool,
	/* Start of query pool slot range for which data are retrieved. */
	GR_UINT startQuery,
	/* Consecutive number of query slots in a range for which data are retrieved. */
	GR_UINT queryCount,
	/* [in/out] Input value specifies the size in bytes of the pData output buffer; output value reports the number of bytes written to pData. */
	GR_SIZE* pDataSize,
	/* [out] Query results. Can be NULL. */
	GR_VOID* pData);

/*
	Creates a GPU execution fence object.


	RETURNS
	If successful, grCreateFence() returns GR_SUCCESS and the handle of the created fence object is written to the location specified by pFence.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_FLAGS if the flags are invalid
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pFence is NULL


	NOTES
	The fence creation flags are currently reserved.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateFence(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Fence object creation info. See GR_FENCE_CREATE_INFO. */
	const GR_FENCE_CREATE_INFO* pCreateInfo,
	/* [out] Fence object handle. */
	GR_FENCE* pFence);

/*
	Retrieves the status of a fence object.


	RETURNS
	If the function call is successful and the fence has been reached, grGetFenceStatus() returns GR_SUCCESS. If the function call is successful, but the fence hasn't been reached, the function returns GR_NOT_READY.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the fence handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the fence handle references an invalid object type
	▼ GR_ERROR_UNAVAILABLE if the fence hasn't been submitted by the application
	▼ GR_ERROR_MEMORY_NOT_BOUND if fence requires GPU memory, but it wasn't bound


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetFenceStatus(
	/* Fence object handle. */
	GR_FENCE fence);

/*
	Stalls the current thread until any or all of the fences have been reached by GPU.


	RETURNS
	If the function executed successfully and the fences have been reached, grWaitForFences() returns GR_SUCCESS. If the function executed successfully, but the fences haven't been reached before the timeout, the function returns GR_TIMEOUT.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_HANDLE if any of the fence handles are invalid
	▼ GR_ERROR_INVALID_VALUE if the fence count is zero
	▼ GR_ERROR_INVALID_POINTER if pFences is NULL
	▼ GR_ERROR_UNAVAILABLE if any of the fences haven't been submitted by the application


	NOTES
	All fences have to be submitted at least once to return a non-error result. Returns GR_TIMEOUT if the required fences have not completed after timeout seconds have passed. Using zero timeout value returns immediately and can be used to determine if all required fences have been completed.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWaitForFences(
	/* Device handle. */
	GR_DEVICE device,
	/* Number of fences to wait for. */
	GR_UINT fenceCount,
	/* [in] Array of fence object handles. */
	const GR_FENCE* pFences,
	/* Wait behavior: if GR_TRUE, wait for completion of all fences in the provided list; if GR_FALSE, wait for completion of any of the provided fences. */
	GR_BOOL waitAll,
	/* Wait timeout in seconds. */
	GR_FLOAT timeout);

/*
	Creates a counting semaphore object to be used for GPU queue synchronization.


	RETURNS
	If successful, grCreateQueueSemaphore() returns GR_SUCCESS and the handle of the created queue semaphore object is written to the location specified by pSemaphore.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the initial semaphore count is invalid
	▼ GR_ERROR_INVALID_FLAGS if the flags are invalid
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pSemaphore is NULL


	NOTES
	The semaphore creation flags are currently reserved.

	The initial semaphore count must be in the range [0..31].


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateQueueSemaphore(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Queue semaphore object creation info. See GR_QUEUE_SEMAPHORE_CREATE_INFO. */
	const GR_QUEUE_SEMAPHORE_CREATE_INFO* pCreateInfo,
	/* [out] Queue semaphore object handle. */
	GR_QUEUE_SEMAPHORE* pSemaphore);

/*
	Inserts a semaphore signal into a GPU queue.


	RETURNS
	grSignalQueueSemaphore() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the queue or semaphore handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the queue handle references an invalid object type


	NOT THREAD SAFE for calls referencing the same queue or semaphore object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grSignalQueueSemaphore(
	/* Queue handle. */
	GR_QUEUE queue,
	/* Queue semaphore to signal. */
	GR_QUEUE_SEMAPHORE semaphore);

/*
	Inserts a semaphore wait into a GPU queue.


	RETURNS
	grWaitQueueSemaphore() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the queue or semaphore handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the queue handle references an invalid object type


	NOT THREAD SAFE for calls referencing the same queue or semaphore object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWaitQueueSemaphore(
	/* Qeue handle. */
	GR_QUEUE queue,
	/* Queue semaphore to wait on. */
	GR_QUEUE_SEMAPHORE semaphore);

/*
	Creates a GPU event object that can be set and reset by the CPU directly or by the GPU via command buffers.


	RETURNS
	If successful, grCreateEvent() returns GR_SUCCESS and the handle of the create event object is written to the location specified by pEvent.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_FLAGS if the flags are invalid
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pEvent is NULL


	NOTES
	The event creation flags are currently reserved.

	The event is in the reset state at creation.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateEvent(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Event object creation info. See GR_EVENT_CREATE_INFO. */
	const GR_EVENT_CREATE_INFO* pCreateInfo,
	/* [out] Event object handle. */
	GR_EVENT* pEvent);

/*
	Retrieves the status of an event object.


	RETURNS
	If the function executed successfully and the event is set, grGetEventStatus() returns GR_EVENT_SET. If the function executed successfully and the event is reset, the function returns GR_EVENT_RESET.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the event handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the event handle references an invalid object type
	▼ GR_ERROR_MEMORY_NOT_BOUND if event requires GPU memory, but it wasn't bound


	NOT THREAD SAFE for calls accessing the same event object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetEventStatus(
	/* Event object handle. */
	GR_EVENT event);

/*
	Sets an event object's status from the CPU.


	RETURNS
	grSetEvent() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the event handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the event handle references an invalid object type
	▼ GR_ERROR_MEMORY_NOT_BOUND if event requires GPU memory, but it wasn't bound


	NOT THREAD SAFE for calls accessing the same event object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grSetEvent(
	/* Event object handle. */
	GR_EVENT event);

/*
	Resets an event object's status from the CPU.


	RETURNS
	grResetEvent() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the event handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the event handle references an invalid object type
	▼ GR_ERROR_MEMORY_NOT_BOUND if event requires GPU memory, but it wasn't bound


	NOT THREAD SAFE for calls accessing the same event object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grResetEvent(
	/* Event object handle. */
	GR_EVENT event);




/*
===============================================================================
###############################################################################

	CORE - MULTI-DEVICE MANAGEMENT FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Retrieves information about Mantle GPU compatibility features.


	RETURNS
	If successful, grGetMultiGpuCompatibility() returns GR_SUCCESS and multi-GPU compatibility information.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the gpu0 or gpu1 handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the gpu0 or gpu1 handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pInfo is NULL


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grGetMultiGpuCompatibility(
	/* First physical GPU handle. */
	GR_PHYSICAL_GPU gpu0,
	/* Second physical GPU handle. */
	GR_PHYSICAL_GPU gpu1,
	/* [out] Multi-GPU compatibility info. See GR_GPU_COMPATIBILITY_INFO. */
	GR_GPU_COMPATIBILITY_INFO* pInfo);

/*
	Opens a previously created GPU memory object for sharing on another device.


	RETURNS
	If successful, grOpenSharedMemory() returns GR_SUCCESS and the handle of the shared GPU memory object is written to the location specified by pMem.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pOpenInfo or pMem are NULL
	▼ GR_ERROR_INVALID_HANDLE if pOpenInfo->sharedMem handle is invalid
	▼ GR_ERROR_NOT_SHAREABLE if pOpenInfo->sharedMem was not marked as shareable at creation


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grOpenSharedMemory(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Data for opening a shared memory. See GR_MEMORY_OPEN_INFO. */
	const GR_MEMORY_OPEN_INFO* pOpenInfo,
	/* [out] Shared memory object handle. */
	GR_GPU_MEMORY* pMem);

/*
	Opens a previously created queue semaphore object for sharing on another device.


	RETURNS
	If successful, grOpenSharedQueueSemaphore() returns GR_SUCCESS and the handle of the shared queue semaphore object is written to the location specified by pSemaphore.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pOpenInfo or pSemaphore are NULL
	▼ GR_ERROR_INVALID_HANDLE if pOpenInfo->sharedSemaphore handle is invalid
	▼ GR_ERROR_NOT_SHAREABLE if pOpenInfo->sharedSemaphore was not marked as shareable at creation


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grOpenSharedQueueSemaphore(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Data for opening a shared queue semaphore. See GR_QUEUE_SEMAPHORE_OPEN_INFO. */
	const GR_QUEUE_SEMAPHORE_OPEN_INFO* pOpenInfo,
	/* [out] Shared queue semaphore handle. */
	GR_QUEUE_SEMAPHORE* pSemaphore);

/*
	Opens a previously created GPU memory object for peer access on another device.


	RETURNS
	If successful, grOpenPeerMemory() returns GR_SUCCESS and the handle of the peer access memory object is written to the location specified by pMem.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pOpenInfo or pMem are NULL
	▼ GR_ERROR_INVALID_HANDLE if pOpenInfo->originalMem handle is invalid


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grOpenPeerMemory(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Data for opening a peer memory. See GR_PEER_MEMORY_OPEN_INFO. */
	const GR_PEER_MEMORY_OPEN_INFO* pOpenInfo,
	/* [out] Peer access memory object handle. */
	GR_GPU_MEMORY* pMem);

/*
	Opens a previously created image object for peer access on another device.


	RETURNS
	If successful, grOpenPeerImage() returns GR_SUCCESS, the handle of the peer access image object is written to the location specified by pImage, and the memory object handle for the peer access image object is written to the location specified by pMem.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pOpenInfo, or pImage. or pMem are NULL
	▼ GR_ERROR_INVALID_HANDLE if pOpenInfo->originalImage handle is invalid


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grOpenPeerImage(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Data for opening a peer image. See GR_PEER_IMAGE_OPEN_INFO. */
	const GR_PEER_IMAGE_OPEN_INFO* pOpenInfo,
	/* [out] Peer access image object handle. */
	GR_IMAGE* pImage,
	/* [out] Memory object handle for peer access image. */
	GR_GPU_MEMORY* pMem);




/*
===============================================================================
###############################################################################

	CORE - COMMAND BUFFER MANAGEMENT FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Creates a command buffer object that supports submission to a specific queue type.


	RETURNS
	If successful, grCreateCommandBuffer() returns GR_SUCCESS and the handle of the created command buffer object is written to the location specified by pCmdBuffer.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pCmdBuffer is NULL
	▼ GR_ERROR_INVALID_FLAGS if the flags are invalid
	▼ GR_ERROR_INVALID_QUEUE_TYPE if the queue is invalid or not supported by the device


	NOTES
	The command buffer creation flags are currently reserved.


	THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grCreateCommandBuffer(
	/* Device handle. */
	GR_DEVICE device,
	/* [in] Command buffer creation info. See GR_CMD_BUFFER_CREATE_INFO. */
	const GR_CMD_BUFFER_CREATE_INFO* pCreateInfo,
	/* [out] Command buffer object handle. */
	GR_CMD_BUFFER* pCmdBuffer);

/*
	Resets the command buffer's previous contents and state, then puts it in a building state allowing new command buffer data to be recorded.


	RETURNS
	grBeginCommandBuffer() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the cmdBuffer handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the cmdBuffer handle references an invalid object type
	▼ GR_ERROR_INVALID_FLAGS if the flags are invalid
	▼ GR_ERROR_INCOMPLETE_COMMAND_BUFFER if the command buffer is already in the building state


	NOTES
	The command buffer begin flags are currently reserved.

	Trying to start command buffer recording on a buffer that was not properly terminated returns GR_ERROR_INCOMPLETE_COMMAND_BUFFER. In that case, the application should explicitly reset the command buffer by calling grResetCommandBuffer().


	NOT THREAD SAFE for calls referencing the same command buffer object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grBeginCommandBuffer(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Command buffer recording flags. See GR_CMD_BUFFER_BUILD_FLAGS. */
	GR_FLAGS flags);

/*
	Completes recording of a command buffer in the building state.


	RETURNS
	grEndCommandBuffer() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the cmdBuffer handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the cmdBuffer handle references an invalid object type
	▼ GR_ERROR_INCOMPLETE_COMMAND_BUFFER if the command buffer is not in the building state
	▼ GR_ERROR_BUILDING_COMMAND_BUFFER if some error has occurred during the command buffer building and the command buffer cannot be used for submission


	NOTES
	Trying to end a command buffer that isn't in a building state returns GR_ERROR_INCOMPLETE_COMMAND_BUFFER. In that case, the application should explicitly reset the command buffer by calling grResetCommandBuffer().


	NOT THREAD SAFE for calls referencing the same command buffer object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grEndCommandBuffer(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer);

/*
	Explicitly resets a command buffer and releases any internal resources associated with it. Must be used to reset command buffers that have previously reported a GR_ERROR_INCOMPLETE_COMMAND_BUFFER error.


	RETURNS
	grResetCommandBuffer() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the cmdBuffer handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the cmdBuffer handle references an invalid object type


	NOT THREAD SAFE for calls referencing the same command buffer object.
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grResetCommandBuffer(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer);




/*
===============================================================================
###############################################################################

	CORE - COMMAND BUFFER BUILDING FUNCTIONS

	General notes:
	For performance reasons, all command building function do not perform a lot of sanity checking. If something goes wrong, the call gets dropped or produces undefined results.

	General thread safety:
	Not thread safe for calls referencing the same command buffer object.

###############################################################################
===============================================================================
*/

/*
	Binds a graphics or compute pipeline to the current command buffer state.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdBindPipeline(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Pipeline binding point (graphics or compute). See GR_PIPELINE_BIND_POINT. */
	GR_ENUM pipelineBindPoint,
	/* Pipeline object handle. */
	GR_PIPELINE pipeline);

/*
	Binds a state object to the current command buffer state.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdBindStateObject(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* State bind point. See GR_STATE_BIND_POINT. */
	GR_ENUM stateBindPoint,
	/* State object handle. */
	GR_STATE_OBJECT state);

/*
	Binds a descriptor set to the current command buffer state making it accessible from either the graphics or compute pipeline.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdBindDescriptorSet(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Pipeline type to make the descriptor set available to. See GR_PIPELINE_BIND_POINT. */
	GR_ENUM pipelineBindPoint,
	/* Descriptor set bind point index. */
	GR_UINT index,
	/* Descriptor set handle. */
	GR_DESCRIPTOR_SET descriptorSet,
	/* Descriptor set slot offset to use for binding. */
	GR_UINT slotOffset);

/*
	Binds a dynamic memory view to the current command buffer state making it accessible from either the graphics or compute pipeline.


	NOTES
	The dynamic memory view behaves similarly to a memory view bound through the descriptor set hierarchy.

	Specifying NULL in pMemView unbinds currently bound dynamic memory view.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdBindDynamicMemoryView(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Pipeline type to make the dynamic memory view available to. See GR_PIPELINE_BIND_POINT. */
	GR_ENUM pipelineBindPoint,
	/* [in] Memory view description. See GR_MEMORY_VIEW_ATTACH_INFO. Can be NULL. */
	const GR_MEMORY_VIEW_ATTACH_INFO* pMemView);

/*
	Binds draw index data to the current command buffer state.


	NOTES
	The memory offset must be index element size aligned – 2-byte aligned for 16-bit indices or 4-byte aligned for 32-bit indices.

	Memory regions containing indices needs to be in the GR_MEMORY_STATE_INDEX_DATA state before being bound as index data.

	Specifying GR_NULL_HANDLE for memory object unbinds currently bound index data.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdBindIndexData(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Memory object containing index data. Can be GR_NULL_HANDLE. */
	GR_GPU_MEMORY mem,
	/* Byte offset within a memory object to the first index. */
	GR_GPU_SIZE offset,
	/* Data type of the indices. See GR_INDEX_TYPE. */
	GR_ENUM indexType);

/*
	Binds color and depth-stencil targets to the current command buffer state. The current image state for targets is also specified at bind time.


	NOTES
	The valid states for binding color targets and depth-stencil target are GR_IMAGE_STATE_TARGET_RENDER_ACCESS_OPTIMAL and GR_IMAGE_STATE_TARGET_SHADER_ACCESS_OPTIMAL. Additionally depth-stencil target can be in GR_IMAGE_STATE_TARGET_AND_SHADER_READ_ONLY for either depth or stencil aspects.

	Specifying NULL pDepthTarget unbinds previously bound depth-stencil target.

	Specifying NULL pColorTargets and zero colorTargetCount unbinds all previously bound color targets. Specifying GR_NULL_HANDLE for any of the color target view objects also unbinds previously bound color target.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdBindTargets(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Number of color render targets to be bound. */
	GR_UINT colorTargetCount,
	/* [in] Array of color render target view handles. See GR_COLOR_TARGET_BIND_INFO. Can be NULL if colorTargetCount is zero. */
	const GR_COLOR_TARGET_BIND_INFO* pColorTargets,
	/* [in] Depth-stencil view handle. See GR_DEPTH_STENCIL_BIND_INFO. Can be NULL. */
	const GR_DEPTH_STENCIL_BIND_INFO* pDepthTarget);

/*
	Specifies memory region state transition for a given list of memory objects.


	NOTES
	Specifying a memory range (or some portion of a range) multiple times produces undefined results.

	Memory range that has not been used before is assumed to be in the GR_MEMORY_STATE_DATA_TRANSFER state.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdPrepareMemoryRegions(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Number of memory regions that need to perform a state transition. */
	GR_UINT transitionCount,
	/* [in] Array of structures describing each memory region state transition. See GR_MEMORY_STATE_TRANSITION. */
	const GR_MEMORY_STATE_TRANSITION* pStateTransitions);

/*
	Specifies image state transitions for a given list of image resources.


	NOTES
	Specifying a subresource multiple times in one image preparation call produces undefined results.

	When memory is bound to an image object used as a render target or a depth-stencil, the resource state is implicitly set to GR_IMAGE_STATE_UNINITIALIZED, and it needs to be transitioned to a proper state before its first use. All other image objects implicitly receive the GR_IMAGE_STATE_DATA_TRANSFER state on memory bind.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdPrepareImages(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Number of image resources that need to perform a state transition. */
	GR_UINT transitionCount,
	/* [in] Array of structures describing each image state transition. See GR_IMAGE_STATE_TRANSITION. */
	const GR_IMAGE_STATE_TRANSITION* pStateTransitions);

/*
	Draws instanced, non-indexed geometry using the current graphics state.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdDraw(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Offset to the first vertex. */
	GR_UINT firstVertex,
	/* Number of vertices per instance to draw. */
	GR_UINT vertexCount,
	/* Offset to the first instance. */
	GR_UINT firstInstance,
	/* Number of instances to draw. */
	GR_UINT instanceCount);

/*
	Draws instanced, indexed geometry using the current graphics state.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdDrawIndexed(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Offset to the first index. */
	GR_UINT firstIndex,
	/* Number of indices per instance to draw. */
	GR_UINT indexCount,
	/* Vertex offset to be added to each vertex index. */
	GR_INT vertexOffset,
	/* Offset to the first instance. */
	GR_UINT firstInstance,
	/* Number of instances to draw. */
	GR_UINT instanceCount);

/*
	Draws instanced, non-indexed geometry using the current graphics state. The draw arguments come from data stored in GPU memory.


	NOTES
	Draw argument data offset in memory must be 4-byte aligned. The layout of the argument data is defined in GR_DRAW_INDIRECT_ARG.

	The memory range used for draw arguments needs to be in the GR_MEMORY_STATE_INDIRECT_ARG state before using it as argument data.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdDrawIndirect(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Memory object containing the draw argument data. */
	GR_GPU_MEMORY mem,
	/* Byte offset from the beginning of the memory object to the draw argument data. */
	GR_GPU_SIZE offset);

/*
	Draws instanced, indexed geometry using the current graphics state. The draw arguments come from data stored in GPU memory.


	NOTES
	Draw argument data offset in the memory must be 4-byte aligned. The layout of the argument data is defined in GR_DRAW_INDEXED_INDIRECT_ARG.

	The memory range used for draw arguments needs to be in the GR_MEMORY_STATE_INDIRECT_ARG state before using it as argument data.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdDrawIndexedIndirect(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Memory object containing the draw argument data. */
	GR_GPU_MEMORY mem,
	/* Byte offset from the beginning of the memory object to the draw argument data. */
	GR_GPU_SIZE offset);

/*
	Dispatches a compute workload of the given dimensions using the current compute state.


	NOTES
	The thread group size is defined in the compute shader.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdDispatch(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Thread groups to dispatch in the X dimension. */
	GR_UINT x,
	/* Thread groups to dispatch in the Y dimension. */
	GR_UINT y,
	/* Thread groups to dispatch in the Z dimension. */
	GR_UINT z);

/*
	Dispatches a compute workload using the current compute state. The dimensions of the workload come from data stored in GPU memory.


	NOTES
	The thread group size is defined in the compute shader.

	The dispatch argument data offset in the memory object must be 4-byte aligned. The layout of the argument data is defined in GR_DISPATCH_INDIRECT_ARG.

	The memory range used for dispatch arguments needs to be in the GR_MEMORY_STATE_INDIRECT_ARG state before using it as argument data.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdDispatchIndirect(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Memory object containing the dispatch arguments. */
	GR_GPU_MEMORY mem,
	/* Byte offset from the beginning of the memory object to the dispatch argument data. */
	GR_GPU_SIZE offset);

/*
	Copies multiple regions from one GPU memory object to another.


	NOTES
	None of the destination regions are allowed to overlap with each other or with source regions. Overlapping any of them produces undefined results.

	For performance reasons, it is preferred to align offsets and copy sizes to 4-byte boundaries.

	Both the source and destination memory regions must be in the GR_MEMORY_STATE_DATA_TRANSFER or an appropriate specialized data transfer state before performing a copy operation.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdCopyMemory(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Source memory object. */
	GR_GPU_MEMORY srcMem,
	/* Destination memory object. */
	GR_GPU_MEMORY destMem,
	/* Number of regions for the copy operation. */
	GR_UINT regionCount,
	/* [in] Array of copy region descriptors. See GR_MEMORY_COPY. */
	const GR_MEMORY_COPY* pRegions);

/*
	Copies multiple regions from one image to another.


	NOTES
	The source and destination subresources are not allowed to be the same. Overlapping any of the source and destination subresources produces undefined copy results. Additionally, destination subresources cannot be present more than once per grCmdCopyImage() function call.

	The source and destination formats do not have to match; appropriate format conversion is performed automatically if image and destination formats support conversion, which is indicated by the GR_FORMAT_CONVERSION format capability flag. Format conversions cannot be performed for compressed image formats. For resources with multiple aspects, each aspect format is determined according to the subresource. When either the source or destination image format does not have a GR_FORMAT_CONVERSION flag, the pixel size must match, and a raw image data copy is performed. For compressed images, the compression block size is used as a pixel size.

	For compressed images, the image extents are specified in compression blocks.

	The source and destination images must to be of the same type (1D, 2D, or 3D).

	The MSAA source and destination images must have the same number of samples.

	Both the source and destination images must be in the GR_IMAGE_STATE_DATA_TRANSFER or an appropriate specialized data transfer state before performing a copy operation.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdCopyImage(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Source image handle. */
	GR_IMAGE srcImage,
	/* Destination image handle. */
	GR_IMAGE destImage,
	/* Number of regions for the copy operation. */
	GR_UINT regionCount,
	/* [in] Array of copy region descriptors. See GR_IMAGE_COPY. */
	const GR_IMAGE_COPY* pRegions);

/*
	Copies data directly from a GPU memory object to an image.


	NOTES
	For compressed images, the image extents are specified in compression blocks.

	The size of the data copied from memory is implicitly derived from the extents.

	The destination memory offset has to be aligned to the smaller of the copied texel size or the 4-byte boundary. The destination subresources cannot be present more than once per grCmdCopyMemoryToImage function call.

	The source memory regions need to be in the GR_MEMORY_STATE_DATA_TRANSFER or GR_MEMORY_STATE_DATA_TRANSFER_SOURCE state, and the destination image needs to be in the GR_IMAGE_STATE_DATA_TRANSFER or GR_IMAGE_STATE_DATA_TRANSFER_DESTINATION state before performing a copy operation.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdCopyMemoryToImage(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Source memory object. */
	GR_GPU_MEMORY srcMem,
	/* Destination image handle. */
	GR_IMAGE destImage,
	/* Number of regions for the copy operation. */
	GR_UINT regionCount,
	/* [in] Array of copy region descriptors. See GR_MEMORY_IMAGE_COPY. */
	const GR_MEMORY_IMAGE_COPY* pRegions);

/*
	Copies data from an image directly to a GPU memory object.


	NOTES
	For compressed images, the image extents are specified in compression blocks.

	The size of the data copied to memory is implicitly derived from the extents.

	The destination memory offset has to be aligned to the smaller of the copied texel size or the 4-byte boundary.

	The destination memory regions need to be in the GR_MEMORY_STATE_DATA_TRANSFER or GR_MEMORY_STATE_DATA_TRANSFER_DESTINATION state, and the source image needs to be in the GR_IMAGE_STATE_DATA_TRANSFER or GR_IMAGE_STATE_DATA_TRANSFER_SOURCE state before performing a copy operation.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdCopyImageToMemory(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Source image handle. */
	GR_IMAGE srcImage,
	/* Destination memory object. */
	GR_GPU_MEMORY destMem,
	/* Number of regions for the copy operation. */
	GR_UINT regionCount,
	/* [in] Array of copy region descriptors. See GR_MEMORY_IMAGE_COPY. */
	const GR_MEMORY_IMAGE_COPY* pRegions);

/*
	Resolves multiple rectangles from a multisampled resource to a single	sampled-resource.


	NOTES
	The source image has to be a 2D multisampled image and the destination must be a single sample image. The formats of the source and destination images should match.

	For depth-stencil images the resolve is performed by copying the first sample from the target image to the destination image.

	The destination subresources cannot be present more than once in an array of regions.

	The source image must be in the GR_IMAGE_STATE_RESOLVE_SOURCE state and the destination image must be in the GR_IMAGE_STATE_RESOLVE_DESTINATION state before performing a resolve operation.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdResolveImage(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Source image handle. */
	GR_IMAGE srcImage,
	/* Destination image handle. */
	GR_IMAGE destImage,
	/* Number of regions for the resolve operation. */
	GR_UINT regionCount,
	/* [in] Array of resolve region descriptors. See GR_IMAGE_RESOLVE. */
	const GR_IMAGE_RESOLVE* pRegions);

/*
	Clones data of one image object to another while preserving the image state. The source and destination images must be created with identical creation parameters and have memory bound.


	NOTES
	Both the source and destination image have to be created with GR_IMAGE_CREATE_CLONEABLE flag.

	Both resources can be in any state before the cloning operation. After the cloning operation, the source image state is left intact and the destination image state becomes the same as the source.

	The clone operation clones all subresources. All subresources of the source image have to be in the same state. All subresources of the destination image have to be in the same state. A mismatch of subresource state produces undefined results.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdCloneImageData(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Source image handle. */
	GR_IMAGE srcImage,
	/* Source image state before cloning. See GR_IMAGE_STATE. */
	GR_ENUM srcImageState,
	/* Destination image handle. */
	GR_IMAGE destImage,
	/* Destination image state before cloning. See GR_IMAGE_STATE. */
	GR_ENUM destImageState);

/*
	Directly updates a GPU memory object with a small amount of host data.


	NOTES
	The memory region needs to be in the GR_MEMORY_STATE_DATA_TRANSFER or GR_MEMORY_STATE_DATA_TRANSFER_DESTINATION state before updating its data.

	The GPU memory offset and data size must be 4-byte aligned.

	The amount of data must be less than or equal to what is reported in the physical GPU properties – see GPU Identification and Initialization.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdUpdateMemory(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Destination memory object handle. */
	GR_GPU_MEMORY destMem,
	/* Offset into the destination memory object. */
	GR_GPU_SIZE destOffset,
	/* Data size in bytes. */
	GR_GPU_SIZE dataSize,
	/* [in] Data to write into the memory object. */
	const GR_UINT32* pData);

/*
	Fills a range of GPU memory object with provided 32-bit data.


	NOTES
	The memory region needs to be in the GR_MEMORY_STATE_DATA_TRANSFER or GR_MEMORY_STATE_DATA_TRANSFER_DESTINATION state before updating its data.

	The GPU memory offset and data size must be 4-byte aligned.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdFillMemory(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Destination memory object handle. */
	GR_GPU_MEMORY destMem,
	/* Offset into the destination memory object. */
	GR_GPU_SIZE destOffset,
	/* Fill memory range in bytes. */
	GR_GPU_SIZE fillSize,
	/* Value to fill the memory object with. */
	GR_UINT32 data);

/*
	Clears a color image to a color specified in floating point format.


	NOTES
	For images of GR_NUM_FMT_UNORM type, the color values must be in the [0..1] range. For images of GR_NUM_FMT_SNORM type, the color values must be in the [-1..1] range.

	For images of GR_NUM_FMT_UINT type, the floating point color is rounded down to an integer value.

	Specifying a clear value outside of the range representable by an image format produces undefined results.

	All image subresources have to be in the GR_IMAGE_STATE_CLEAR state before performing a clear operation.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdClearColorImage(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Image handle. */
	GR_IMAGE image,
	/* Clear color in floating point format. */
	const GR_FLOAT color[4],
	/* Number of subresource ranges to clear. */
	GR_UINT rangeCount,
	/* [in] Array of subresource ranges. See GR_IMAGE_SUBRESOURCE_RANGE. */
	const GR_IMAGE_SUBRESOURCE_RANGE* pRanges);

/*
	Clears a color image to a color specified with raw data bits.
	

	NOTES
	The lowest bits of the clear color (number of bits depending on format) are stored in the cleared image per channel.

	All image subresources have to be in the GR_IMAGE_STATE_CLEAR state before performing a clear operation.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdClearColorImageRaw(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Image handle. */
	GR_IMAGE image,
	/* Raw clear color value in integer format. */
	const GR_UINT32 color[4],
	/* Number of subresource ranges to clear. */
	GR_UINT rangeCount,
	/* [in] Array of subresource ranges. See GR_IMAGE_SUBRESOURCE_RANGE. */
	const GR_IMAGE_SUBRESOURCE_RANGE* pRanges);

/*
	Clears a depth-stencil image to the specified clear values.


	NOTES
	All image subresources have to be in the GR_IMAGE_STATE_CLEAR state before performing a clear operation.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdClearDepthStencil(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Image handle. */
	GR_IMAGE image,
	/* Depth clear value. */
	GR_FLOAT depth,
	/* Stencil clear values. */
	GR_UINT8 stencil,
	/* Number of subresource ranges to clear. */
	GR_UINT rangeCount,
	/* [in] Array of subresource ranges. See GR_IMAGE_SUBRESOURCE_RANGE. */
	const GR_IMAGE_SUBRESOURCE_RANGE* pRanges);

/*
	Sets an event object from a command buffer when all previous work completes.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdSetEvent(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Event handle. */
	GR_EVENT event);

/*
	Resets an event object from a command buffer when all previous work completes.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdResetEvent(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Event handle. */
	GR_EVENT event);

/*
	Performs a 32-bit or 64-bit memory atomic operation consistently with atomics in the shaders.


	NOTES
	The data size (32-bits or 64-bits) is determined by the operation type. For 32-bit atomics only, the lower 32-bits of srcData is used.

	The destination GPU memory offset must be 4-byte aligned for 32-bit atomics, and 8-byte aligned for 64-bit atomics.

	The memory range must be in the GR_MEMORY_STATE_QUEUE_ATOMIC state before performing an atomic operation.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdMemoryAtomic(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Memory object. */
	GR_GPU_MEMORY destMem,
	/* Byte offset into the destination memory object. */
	GR_GPU_SIZE destOffset,
	/* Source data to use for atomic operation. */
	GR_UINT64 srcData,
	/* Atomic operation type. See GR_ATOMIC_OP. */
	GR_ENUM atomicOp);

/*
	Starts query operation for the given slot of a query pool.


	NOTES
	The query slot must have been previously cleared with grCmdResetQueryPool() before starting the query operation.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdBeginQuery(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Query pool handle. */
	GR_QUERY_POOL queryPool,
	/* Query pool slot to start query. */
	GR_UINT slot,
	/* Flags controlling query execution. See GR_QUERY_CONTROL_FLAGS. */
	GR_FLAGS flags);

/*
	Stops query operation for the given slot of a query pool.


	NOTES
	Should only be called after grCmdBeginQuery() was issued on the query slot.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdEndQuery(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Query pool handle. */
	GR_QUERY_POOL queryPool,
	/* Query pool slot to stop query. */
	GR_UINT slot);

/*
	Resets a range of query slots in a query pool. A query slot must be reset each time before the query can be started to generate meaningful results.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdResetQueryPool(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Query pool handle. */
	GR_QUERY_POOL queryPool,
	/* Fist query pool slot to reset. */
	GR_UINT startQuery,
	/* Number of query slots to reset. */
	GR_UINT queryCount);

/*
	Writes a top or bottom of pipe 64-bit timestamp to a memory location.


	NOTES
	The memory needs to be in the GR_MEMORY_STATE_WRITE_TIMESTAMP state before writing the timestamp.

	The destination memory address must be 8-byte aligned.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdWriteTimestamp(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Timestamp type. See GR_TIMESTAMP_TYPE. */
	GR_ENUM timestampType,
	/* Destination memory object. */
	GR_GPU_MEMORY destMem,
	/* Byte offset in the memory object to the timestamp data. */
	GR_GPU_SIZE destOffset);

/*
	Loads atomic counter with provided values.


	NOTES
	Each counter has a 32-bit value, each of which is consecutively loaded from provided system memory.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdInitAtomicCounters(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Pipeline type to load atomic counters for. See GR_PIPELINE_BIND_POINT. */
	GR_ENUM pipelineBindPoint,
	/* First atomic counter slot to load. */
	GR_UINT startCounter,
	/* Number of atomic counter slots to load. */
	GR_UINT counterCount,
	/* [in] The counter data. */
	const GR_UINT32* pData);

/*
	Loads atomic counter values from a memory location.


	NOTES
	The memory must be in the GR_MEMORY_STATE_DATA_TRANSFER state before loading atomic counter data.

	Each counter has a 32-bit value, each of which is consecutively loaded from memory.

	The source memory offset must be 4-byte aligned.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdLoadAtomicCounters(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Pipeline type to load atomic counters for. See GR_PIPELINE_BIND_POINT. */
	GR_ENUM pipelineBindPoint,
	/* First atomic counter slot to load. */
	GR_UINT startCounter,
	/* Number of atomic counter slots to load. */
	GR_UINT counterCount,
	/* Source memory object. */
	GR_GPU_MEMORY srcMem,
	/* Byte offset in the memory object to the beginning of the counter data. */
	GR_GPU_SIZE srcOffset);

/*
	Saves current atomic counter values to a memory location.


	NOTES
	The memory must be in the GR_MEMORY_STATE_DATA_TRANSFER state before saving atomic counter data.

	Each counter has a 32-bit value, each of which is consecutively stored to GPU memory.

	The destination memory offset must be 4-byte aligned.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdSaveAtomicCounters(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* Pipeline type to save atomic counters for. See GR_PIPELINE_BIND_POINT. */
	GR_ENUM pipelineBindPoint,
	/* First atomic counter slot to save. */
	GR_UINT startCounter,
	/* Number of atomic counter slots to save. */
	GR_UINT counterCount,
	/* Destination memory object. */
	GR_GPU_MEMORY destMem,
	/* Byte offset in the memory object to the beginning of the counter data. */
	GR_GPU_SIZE destOffset);




/*
===============================================================================
###############################################################################
###############################################################################
###############################################################################

	-- DEBUG EXTENSION --

###############################################################################
###############################################################################
###############################################################################
===============================================================================
*/




/*
===============================================================================
###############################################################################

	DEBUG - ENUMERATIONS

###############################################################################
===============================================================================
*/

/*
	Defines debug message type.
*/
typedef enum _GR_DBG_MSG_TYPE
{
	/* Not a recognized message type. */
	GR_DBG_MSG_UNKNOWN      = 0x00020000,
	/* Error message. */
	GR_DBG_MSG_ERROR        = 0x00020001,
	/* Warning message. */
	GR_DBG_MSG_WARNING      = 0x00020002,
	/* Performance warning message. */
	GR_DBG_MSG_PERF_WARNING = 0x00020003
} GR_DBG_MSG_TYPE;

/*
	Defines global debug options that apply to all Mantle devices.
*/
typedef enum _GR_DBG_GLOBAL_OPTION
{
	/* Enables/disables echoing debug message output. When application registers its message callback function, it might want to disable debug output to reduce the CPU overhead. By default the debug messages are logged to a debug output. */
	GR_DBG_OPTION_DEBUG_ECHO_ENABLE = 0x00020100,
	/* Enables breaking into debugger on generation of an error message. */
	GR_DBG_OPTION_BREAK_ON_ERROR    = 0x00020101,
	/* Enables breaking into debugger on generation of a warning message. */
	GR_DBG_OPTION_BREAK_ON_WARNING  = 0x00020102
} GR_DBG_GLOBAL_OPTION;

/*
	Defines per-device debug options available with validation layer.
*/
typedef enum _GR_DBG_DEVICE_OPTION
{
	/* Disables pipeline loads by making any call to grLoadPipeline() fail with an error message if the value for this option is set to GR_TRUE. */
	GR_DBG_OPTION_DISABLE_PIPELINE_LOADS         = 0x00020400,
	/* Forces all applicable API objects to have GPU memory requirements if the value for this option is set to GR_TRUE. */
	GR_DBG_OPTION_FORCE_OBJECT_MEMORY_REQS       = 0x00020401,
	/* Forces all images that are larger that a GPU memory page size to have memory requirements a multiple of the page size if the value for this option is set to GR_TRUE. */
	GR_DBG_OPTION_FORCE_LARGE_IMAGE_ALIGNMENT    = 0x00020402,
	/* Controls validation layer behavior in case of an error. The core execution can be skipped if the value for this option is set to GR_TRUE. */
	GR_DBG_OPTION_SKIP_EXECUTION_ON_ERROR        = 0x00020403
} GR_DBG_DEVICE_OPTION;

/*
	Defines debug message filtering options.
*/
typedef enum _GR_DBG_MSG_FILTER
{
	/* The message is not filtered. */
	GR_DBG_MSG_FILTER_NONE     = 0x00020800,
	/* The repeated message is filtered, any message is reported only once until filtering is reset. */
	GR_DBG_MSG_FILTER_REPEATED = 0x00020801,
	/* All instances of the message are filtered. */
	GR_DBG_MSG_FILTER_ALL      = 0x00020802
} GR_DBG_MSG_FILTER;

/*
	Object type returned by the validation layer for API objects.
*/
typedef enum _GR_DBG_OBJECT_TYPE
{
	/* Object type is unknown. */
	GR_DBG_OBJECT_UNKNOWN                = 0x00020900,
	/* Object is a device. */
	GR_DBG_OBJECT_DEVICE                 = 0x00020901,
	/* Object is a queue. */
	GR_DBG_OBJECT_QUEUE                  = 0x00020902,
	/* Object is a regular GPU memory. */
	GR_DBG_OBJECT_GPU_MEMORY             = 0x00020903,
	/* Object is a regular image. */
	GR_DBG_OBJECT_IMAGE                  = 0x00020904,
	/* Object is an image view. */
	GR_DBG_OBJECT_IMAGE_VIEW             = 0x00020905,
	/* Object is a color target view. */
	GR_DBG_OBJECT_COLOR_TARGET_VIEW      = 0x00020906,
	/* Object is a depth-stencil view. */
	GR_DBG_OBJECT_DEPTH_STENCIL_VIEW     = 0x00020907,
	/* Object is a shader. */
	GR_DBG_OBJECT_SHADER                 = 0x00020908,
	/* Object is a graphics pipeline. */
	GR_DBG_OBJECT_GRAPHICS_PIPELINE      = 0x00020909,
	/* Object is a compute pipeline. */
	GR_DBG_OBJECT_COMPUTE_PIPELINE       = 0x0002090a,
	/* Object is a sampler. */
	GR_DBG_OBJECT_SAMPLER                = 0x0002090b,
	/* Object is a descriptor set. */
	GR_DBG_OBJECT_DESCRIPTOR_SET         = 0x0002090c,
	/* Object is a viewport state. */
	GR_DBG_OBJECT_VIEWPORT_STATE         = 0x0002090d,
	/* Object is a rasterizer state. */
	GR_DBG_OBJECT_RASTER_STATE           = 0x0002090e,
	/* Object is a multisampling state. */
	GR_DBG_OBJECT_MSAA_STATE             = 0x0002090f,
	/* Object is a color blending state. */
	GR_DBG_OBJECT_COLOR_BLEND_STATE      = 0x00020910,
	/* Object is a depth-stencil state. */
	GR_DBG_OBJECT_DEPTH_STENCIL_STATE    = 0x00020911,
	/* Object is a command buffer. */
	GR_DBG_OBJECT_CMD_BUFFER             = 0x00020912,
	/* Object is a fence. */
	GR_DBG_OBJECT_FENCE                  = 0x00020913,
	/* Object is a regular queue semaphore. */
	GR_DBG_OBJECT_QUEUE_SEMAPHORE        = 0x00020914,
	/* Object is an event. */
	GR_DBG_OBJECT_EVENT                  = 0x00020915,
	/* Object is a query pool. */
	GR_DBG_OBJECT_QUERY_POOL             = 0x00020916,
	/* Object is a shared GPU memory. */
	GR_DBG_OBJECT_SHARED_GPU_MEMORY      = 0x00020917,
	/* Object is an opened queue semaphore. */
	GR_DBG_OBJECT_SHARED_QUEUE_SEMAPHORE = 0x00020918,
	/* Object is an opened peer GPU memory. */
	GR_DBG_OBJECT_PEER_GPU_MEMORY        = 0x00020919,
	/* Object is an opened peer image. */
	GR_DBG_OBJECT_PEER_IMAGE             = 0x0002091a,
	/* Object is pinned memory. */
	GR_DBG_OBJECT_PINNED_GPU_MEMORY      = 0x0002091b,
	/* Object is an internal GPU memory. */
	GR_DBG_OBJECT_INTERNAL_GPU_MEMORY    = 0x0002091c
} GR_DBG_OBJECT_TYPE;

/*
	Defines type of debug related data returned by validation layer.
*/
typedef enum _GR_DBG_DATA_TYPE
{
	/* Retrieves object type with grGetObjectInfo(). */
	GR_DBG_DATA_OBJECT_TYPE          = 0x00020a00,
	/* Retrieves object creation information with grGetObjectInfo(). */
	GR_DBG_DATA_OBJECT_CREATE_INFO   = 0x00020a01,
	/* Retrieves object debug tag with grGetObjectInfo(). */
	GR_DBG_DATA_OBJECT_TAG           = 0x00020a02,
	/* Retrieves recorded command buffer API trace with grGetObjectInfo(). Valid only for command buffer objects. */
	GR_DBG_DATA_CMD_BUFFER_API_TRACE = 0x00020b00,
	/* Retrieves ranges of memory object bindings with grGetObjectInfo(). Valid only for memory objects. */
	GR_DBG_DATA_MEMORY_OBJECT_LAYOUT = 0x00020c00,
	/* Retrieves ranges of memory object state with grGetObjectInfo(). Valid only for memory objects. */
	GR_DBG_DATA_MEMORY_OBJECT_STATE  = 0x00020c01,
	/* Retrieves internal status of a semaphore with grGetObjectInfo(). Valid only for semaphore objects. */
	GR_DBG_DATA_SEMAPHORE_IS_BLOCKED = 0x00020d00
} GR_DBG_DATA_TYPE;




/*
===============================================================================
###############################################################################

	DEBUG - CALLBACKS

###############################################################################
===============================================================================
*/

/*
	Application callback to allocate a block of system memory.
*/
typedef GR_VOID (GR_STDCALL *GR_DBG_MSG_CALLBACK_FUNCTION)(
	/* Debug message type. See GR_DBG_MSG_TYPE. */
	GR_ENUM msgType,
	/* Validation level at which the debug message was generated. See GR_VALIDATION_LEVEL. */
	GR_ENUM validationLevel,
	/* API handle for the object that generated the debug message. */
	GR_BASE_OBJECT srcObject,
	/* Optional location or array element that is responsible for the debug message. */
	GR_SIZE location,
	/* Debug message code. See GR_DBG_MSG_CODE. */
	GR_ENUM msgCode,
	/* Debug message text. */
	const GR_CHAR* pMsg,
	/* User data passed to the driver when registering the debug message callback. */
	GR_VOID* pUserData);




/*
===============================================================================
###############################################################################

	DEBUG - FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Sets the current validation level for the given device. The level cannot exceed the maximum validation level requested at device creation.


	RETURNS
	grDbgSetValidationLevel() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the validation level exceeds the maximum level requested at device creation
	▼ GR_ERROR_INCOMPLETE_COMMAND_BUFFER if the application is in the middle of command buffer creation during this call
	▼ GR_ERROR_UNAVAILABLE if the debug layer has not been enabled at device creation


	NOTES
	Cannot be called while any command buffers are in the building state.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grDbgSetValidationLevel(
	/* Device handle. */
	GR_DEVICE device,
	/* Requested validation level. See GR_VALIDATION_LEVEL. */
	GR_ENUM   validationLevel);

/*
	Registers an error message callback function. Multiple callbacks can be registered simultaneously; however, the order of callback invocation is not guaranteed.


	RETURNS
	grDbgRegisterMsgCallback() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns the following error:
	▼ GR_ERROR_INVALID_POINTER if the callback function pointer is invalid


	NOTES
	It is allowed to register the same function multiple times without unregistering it first. This just replaces the old user data with a new one, keeping only one instance of the callback function registered.

	This function does not generate debug message callbacks.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grDbgRegisterMsgCallback(
	/* [in] User message callback function pointer. See GR_DBG_MSG_CALLBACK_FUNCTION. */
	GR_DBG_MSG_CALLBACK_FUNCTION pfnMsgCallback,
	/* [in] Pointer to user data that needs to be passed to the callback. Can be NULL. */
	GR_VOID* pUserData);

/*
	Unregisters a previously registered error message callback function.


	RETURNS
	grDbgUnregisterMsgCallback() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns the following error:
	▼ GR_ERROR_INVALID_POINTER if the callback function pointer is invalid or hasn't been previously registered


	NOTES
	This function does not generate debug message callbacks.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grDbgUnregisterMsgCallback(
	/* [in] User message callback function pointer. */
	GR_DBG_MSG_CALLBACK_FUNCTION pfnMsgCallback);

/*
	Enables filtering of a registered error message callback function for a specific message type. Multiple message types can be simultaneously filtered by calling this function multiple times. Debug message filtering does not affect returned error codes for any API functions.


	RETURNS
	grDbgSetMessageFilter() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid.
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type.
	▼ GR_ERROR_INVALID_VALUE if the message code or filter type is invalid.


	NOTES
	Errors generated by the ICD loader cannot be filtered. The messages repetition status is kept globally per device. If multiple objects generate messages of the same type and the filter is set to GR_DBG_MSG_FILTER_REPEATED, then only the first message across these objects results in an application message callback.

	Calling grDbgSetMessageFilter() with any filter type resets the message repetition state for the given message type.

	This function does not generate debug message callbacks.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grDbgSetMessageFilter(
	/* Device handle. */
	GR_DEVICE device,
	/* Message code to filter. */
	GR_ENUM msgCode,
	/* Filter to apply to a particular message type. See GR_DBG_MSG_FILTER. */
	GR_ENUM filter);

/*
	Attaches an application specific binary data object (tag) to any Mantle object, including devices, queues, and memory objects. Tags cannot be attached to physical GPU objects.


	RETURNS
	grDbgSetObjectTag() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the object handle is invalid
	▼ GR_ERROR_INVALID_MEMORY_SIZE if tag size is zero and tag pointer is not NULL


	NOTES
	Object tagging is only available when the validation layer is enabled at any validation level. If the validation layer is disabled, the operation has no effect.

	The driver makes an internal copy of the tag data when storing it with an object.

	Specifying a NULL tag pointer removes the previously set tag data for the given object.

	This function does not generate debug message callbacks.


	NOT THREAD SAFE for the tagged object
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grDbgSetObjectTag(
	/* Any Mantle object handle other than a physical GPU. */
	GR_BASE_OBJECT object,
	/* Size of the binary tag to store with the object. */
	GR_SIZE tagSize,
	/* [in] Binary tag to attach to the object. Can be NULL. */
	const GR_VOID* pTag);

/*
	Sets global debug and validation options.


	RETURNS
	grDbgSetGlobalOption() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_VALUE if dbgOption is invalid
	▼ GR_ERROR_INVALID_VALUE if the data are invalid for the given debug option
	▼ GR_ERROR_INVALID_MEMORY_SIZE if the data size is invalid


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grDbgSetGlobalOption(
	/* Debug option being set. See GR_DBG_GLOBAL_OPTION. */
	GR_DBG_GLOBAL_OPTION dbgOption,
	/* Data size being set for the debug option. */
	GR_SIZE dataSize,
	/* [in] Data to be set for the debug option. */
	const GR_VOID* pData);

/*
	Sets device-specific miscellaneous debug and validation options.


	RETURNS
	grDbgSetDeviceOption() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if dbgOption is invalid
	▼ GR_ERROR_INVALID_VALUE if the data are invalid for the given debug option
	▼ GR_ERROR_INVALID_MEMORY_SIZE if the data size is invalid


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grDbgSetDeviceOption(
	/* Device handle. */
	GR_DEVICE device,
	/* Debug option being set. See GR_DBG_DEVICE_OPTION. */
	GR_DBG_DEVICE_OPTION dbgOption,
	/* Data size being set for the debug option. */
	GR_SIZE dataSize,
	/* [in] Data to be set for the debug option. */
	const GR_VOID* pData);

/*
	Inserts a debug “begin” marker for command buffer debugger inspection.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdDbgMarkerBegin(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer,
	/* [in] Debug marker string. */
	const GR_CHAR* pMarker);

/*
	Inserts a debug “end” marker for command buffer debugger inspection.
*/
GR_DLLIMPORT
GR_VOID GR_STDCALL grCmdDbgMarkerEnd(
	/* Command buffer handle. */
	GR_CMD_BUFFER cmdBuffer);




/*
===============================================================================
###############################################################################
###############################################################################
###############################################################################

	-- WINDOW SYSTEM INTERFACE (WSI) FOR WINDOWS EXTENSION --

###############################################################################
###############################################################################
###############################################################################
===============================================================================
*/




/*
===============================================================================
###############################################################################

	WSIWIN - ENUMERATIONS

###############################################################################
===============================================================================
*/

/*
	Image states used for presenting images.
*/
typedef enum _GR_WSI_WIN_IMAGE_STATE
{
	/* Image is used as a source for windowed presentation operations. */
    GR_WSI_WIN_IMAGE_STATE_PRESENT_WINDOWED   = 0x00200000,
	/* Image is used as a source for fullscreen presentation operations. */
    GR_WSI_WIN_IMAGE_STATE_PRESENT_FULLSCREEN = 0x00200001
} GR_WSI_WIN_IMAGE_STATE;

/*
	Display rotation angle.
*/
typedef enum _GR_WSI_WIN_ROTATION_ANGLE
{
	/* Display is not rotated. */
    GR_WSI_WIN_ROTATION_ANGLE_0   = 0x00200100,
	/* Display is rotated 90 degrees clockwise. */
    GR_WSI_WIN_ROTATION_ANGLE_90  = 0x00200101,
	/* Display is rotated 180 degrees clockwise. */
    GR_WSI_WIN_ROTATION_ANGLE_180 = 0x00200102,
	/* Display is rotated 270 degrees clockwise. */
    GR_WSI_WIN_ROTATION_ANGLE_270 = 0x00200103
} GR_WSI_WIN_ROTATION_ANGLE;

/*
	Presentation mode.
*/
typedef enum _GR_WSI_WIN_PRESENT_MODE
{
	/* Windowed mode presentation. */
    GR_WSI_WIN_PRESENT_MODE_WINDOWED   = 0x00200200,
	/* Fullscreen mode presentation. */
    GR_WSI_WIN_PRESENT_MODE_FULLSCREEN = 0x00200201
} GR_WSI_WIN_PRESENT_MODE;

/*
	Defines types of information related to WSI functionality that can be retrieved from different objects.
*/
typedef enum _GR_WSI_WIN_INFO_TYPE
{
	/* Retrieve WSI related queue properties with grGetObjectInfo(). Valid for GR_QUEUE objects. */
    GR_WSI_WIN_INFO_TYPE_QUEUE_PROPERTIES             = 0x00206800,
	/* Retrieve display properties with grGetObjectInfo(). Valid for GR_WSI_WIN_DISPLAY objects. */
    GR_WSI_WIN_INFO_TYPE_DISPLAY_PROPERTIES           = 0x00206801,
	/* Retrieve display gamma ramp capabilities with grGetObjectInfo(). Valid for GR_WSI_WIN_DISPLAY objects. */
    GR_WSI_WIN_INFO_TYPE_GAMMA_RAMP_CAPABILITIES      = 0x00206802,
	/* Retrieve FreeSync display capabilities. Reserved. */
    GR_WSI_WIN_INFO_TYPE_DISPLAY_FREESYNC_SUPPORT     = 0x00206803,
	/* Retrieve presentable image properties with grGetObjectInfo(). Valid for presentable images only. */
    GR_WSI_WIN_INFO_TYPE_PRESENTABLE_IMAGE_PROPERTIES = 0x00206804,
	/* Retrieve extended display properties with grGetObjectInfo(). Valid for GR_WSI_WIN_DISPLAY objects. */
    GR_WSI_WIN_INFO_TYPE_EXTENDED_DISPLAY_PROPERTIES  = 0x00206805
} GR_WSI_WIN_INFO_TYPE;




/*
===============================================================================
###############################################################################

	WSIWIN - FLAGS

###############################################################################
===============================================================================
*/

/*
	Extended display property flags.
*/
typedef enum _GR_WSI_WIN_EXTENDED_DISPLAY_FLAGS
{
	/* Wait on V-blank period with the grWsiWinWaitForVerticalBlank() function is supported in windowed mode. */
    GR_WSI_WIN_WINDOWED_VBLANK_WAIT  = 0x00000001,
	/* Current display scanline can be retrieved with the grWsiWinGetScanLine() function in windowed mode. */
    GR_WSI_WIN_WINDOWED_GET_SCANLINE = 0x00000002
} GR_WSI_WIN_EXTENDED_DISPLAY_FLAGS;

/*
	WSI creation flags for presentable image.
*/
typedef enum _GR_WSI_WIN_IMAGE_CREATE_FLAGS
{
	/* Create presentable image for fullscreen presentation. */
    GR_WSI_WIN_IMAGE_CREATE_FULLSCREEN_PRESENT = 0x00000001,
	/* Create image for stereoscopic rendering and display. */
    GR_WSI_WIN_IMAGE_CREATE_STEREO             = 0x00000002
} GR_WSI_WIN_IMAGE_CREATE_FLAGS;

/*
	Presentation flags.
*/
typedef enum _GR_WSI_WIN_PRESENT_FLAGS
{
	/* Fail present call if present queue is full. Application could use this mode in conjunction with command buffer control features to reduce frame latency. Only valid if presentMode is GR_WSI_WIN_PRESENT_MODE_FULLSCREEN. */
    GR_WSI_WIN_PRESENT_FULLSCREEN_DONOTWAIT = 0x00000001,
	/* Present should present both right and left images of a stereo allocation. Only valid if presentMode is GR_WSI_WIN_PRESENT_MODE_FULLSCREEN. */
    GR_WSI_WIN_PRESENT_FULLSCREEN_STEREO    = 0x00000002
} GR_WSI_WIN_PRESENT_FLAGS;

/*
	Flags describing types of present operation supported by the queue.
*/
typedef enum _GR_WSI_WIN_PRESENT_SUPPORT_FLAGS
{
	/* Queue supports fullscreen mode presentation. */
    GR_WSI_WIN_FULLSCREEN_PRESENT_SUPPORTED = 0x00000001,
	/* Queue supports windowed mode presentation. */
    GR_WSI_WIN_WINDOWED_PRESENT_SUPPORTED   = 0x00000002
} GR_WSI_WIN_PRESENT_SUPPORT_FLAGS;




/*
===============================================================================
###############################################################################

	WSIWIN - DATA STRUCTURES

###############################################################################
===============================================================================
*/

/*
	Color in RGB format.
*/
typedef struct _GR_RGB_FLOAT
{
	/* Red channel value. */
    GR_FLOAT red;
	/* Green channel value. */
    GR_FLOAT green;
	/* Blue channel value. */
    GR_FLOAT blue;
} GR_RGB_FLOAT;

/*
	Display mode description.
*/
typedef struct _GR_WSI_WIN_DISPLAY_MODE
{
	/* Display mode dimensions. See GR_EXTENT2D. */
    GR_EXTENT2D extent;
	/* The pixel format of the display mode. See GR_FORMAT. */
    GR_FORMAT   format;
	/* Refresh rate in Hz. */
    GR_UINT     refreshRate;
	/* The display mode supports stereoscopic rendering and display, if GR_TRUE. */
    GR_BOOL     stereo;
	/* The display mode supports cross-display presentation to the display (present through hardware compositor in multi-device configurations), if GR_TRUE. */
    GR_BOOL     crossDisplayPresent;
} GR_WSI_WIN_DISPLAY_MODE;

/*
	Display properties.
*/
typedef struct _GR_WSI_WIN_DISPLAY_PROPERTIES
{
	/* Monitor handle for physical display in Windows®. */
    HMONITOR hMonitor;
	/* String specifying the device name of the display. */
    GR_CHAR  displayName[GR_MAX_DEVICE_NAME_LEN];
	/* Specifies the display rectangle, expressed in virtual screen coordinates. Note that if the display is not the desktop’s primary display, some of the rectangle’s coordinates may be negative values. See GR_RECT. */
    GR_RECT  desktopCoordinates;
	/* Display rotation angle. See GR_WSI_WIN_ROTATION_ANGLE. */
    GR_ENUM  rotation;
} GR_WSI_WIN_DISPLAY_PROPERTIES;

/*
	Extended display properties.
*/
typedef struct _GR_WSI_WIN_EXTENDED_DISPLAY_PROPERTIES
{
	/* Extended display property flags. See GR_WSI_WIN_EXTENDED_DISPLAY_FLAGS. */
    GR_FLAGS extendedProperties;
} GR_WSI_WIN_EXTENDED_DISPLAY_PROPERTIES;

/*
	Definition of custom gamma ramp.
*/
typedef struct _GR_WSI_WIN_GAMMA_RAMP
{
	/* RGB float scale value. Scaling is performed after gamma curve conversion, but before the offset is added. */
    GR_RGB_FLOAT scale;
	/* RGB float offset value. Offset is added after scaling. */
    GR_RGB_FLOAT offset;
	/* RGB float values corresponding to output value per control point. Gamma curve conversion is performed before any scale or offset are applied. Gamma curve defined by approximation across control points, including the end points. The actual number of curve control point used is retrieved in gamma ramp capabilities. See GR_WSI_WIN_GAMMA_RAMP_CAPABILITIES. */
    GR_RGB_FLOAT gammaCurve[GR_MAX_GAMMA_RAMP_CONTROL_POINTS];
} GR_WSI_WIN_GAMMA_RAMP;

/*
	Custom gamma ramp capabilities.
*/
typedef struct _GR_WSI_WIN_GAMMA_RAMP_CAPABILITIES
{
	/* The display supports post-conversion scale and offset support, if GR_TRUE. */
    GR_BOOL  supportsScaleAndOffset;
	/* Minimum supported output value. */
    GR_FLOAT minConvertedValue;
	/* Maximum supported output value. */
    GR_FLOAT maxConvertedValue;
	/* Number of valid entries in the controlPointPositions array. */
    GR_UINT  controlPointCount;
	/* Array of floating point values describing the position of each control point. */
    GR_FLOAT controlPointPositions[GR_MAX_GAMMA_RAMP_CONTROL_POINTS];
} GR_WSI_WIN_GAMMA_RAMP_CAPABILITIES;

/*
	Presentation information.
*/
typedef struct _GR_WSI_WIN_PRESENT_INFO
{
	/* Windows® handle of the destination window. Must be NULL if presentMode is GR_WSI_WIN_PRESENT_MODE_FULLSCREEN. */
    HWND     hWndDest;
	/* Source image for the present. */
    GR_IMAGE srcImage;
	/* Type of present. See GR_WSI_WIN_PRESENT_MODE. */
    GR_ENUM  presentMode;
	/* Integer from 0 to 4. Indicates if the fullscreen mode presentation should occur immediately (0) or after 1-4 vertical syncs. For windowed mode only, immediate presentation is valid. */
    GR_UINT  presentInterval;
	/* Presentation flags. See GR_WSI_WIN_PRESENT_FLAGS. */
    GR_FLAGS flags;
} GR_WSI_WIN_PRESENT_INFO;

/*
	Presentable image creation information.
*/
typedef struct _GR_WSI_WIN_PRESENTABLE_IMAGE_CREATE_INFO
{
	/* Presentable image pixel format. See GR_FORMAT. */
    GR_FORMAT          format;
	/* Image usage flags. See GR_IMAGE_USAGE_FLAGS. */
    GR_FLAGS           usage;
	/* Width and height of the image in pixels. See GR_EXTENT2D. */
    GR_EXTENT2D        extent;
	/* Mantle display object corresponding to this image. Only valid for fullscreen presentable images. */
    GR_WSI_WIN_DISPLAY display;
	/* WSI specific presentable image flags. See GR_WSI_WIN_IMAGE_CREATE_FLAGS. */
    GR_FLAGS           flags;
} GR_WSI_WIN_PRESENTABLE_IMAGE_CREATE_INFO;

/*
	Information about presentable image object.
*/
typedef struct _GR_WSI_WIN_PRESENTABLE_IMAGE_PROPERTIES
{
	/* Presentable image creation information. See GR_WSI_WIN_PRESENTABLE_IMAGE_CREATE_INFO. */
    GR_WSI_WIN_PRESENTABLE_IMAGE_CREATE_INFO createInfo;
	/* Handle of GPU memory object that is bound to presentable image. */
    GR_GPU_MEMORY                            mem;
} GR_WSI_WIN_PRESENTABLE_IMAGE_PROPERTIES;

/*
	WSI related queue properties.
*/
typedef struct _GR_WSI_WIN_QUEUE_PROPERTIES
{
	/* Flags indicating type of presentation mode (windowed or fullscreen) supported by the queue. See GR_WSI_WIN_PRESENT_SUPPORT_FLAGS. */
    GR_FLAGS presentSupport;
} GR_WSI_WIN_QUEUE_PROPERTIES;




/*
===============================================================================
###############################################################################

	WSIWIN - FUNCTIONS

###############################################################################
===============================================================================
*/

/*
	Retrieves a list of displays attached to the device.


	RETURNS
	If successful, grWsiWinGetDisplays() returns GR_SUCCESS and the handles of attached displays are written to pDisplayList.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pDisplayCount is NULL
	▼ GR_ERROR_INVALID_MEMORY_SIZE if pDisplayList is not NULL and the pDisplayCount input value is smaller than the number of attached displays


	NOTES
	If pDisplayList is NULL, the input pDisplayCount value does not matter and the function returns the number of displays in pDisplaysCount.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWsiWinGetDisplays(
	/* Device handle. */
    GR_DEVICE device,
	/* [in/out] The maximum number of displays to enumerate, and the output value specifies the total number of displays that were enumerated in pDisplayList. */
    GR_UINT* pDisplayCount,
	/* [out] Array of returned display handles. Can be NULL. */
    GR_WSI_WIN_DISPLAY* pDisplayList);

/*
	Retrieves a list of supported display modes for the display object.


	RETURNS
	If successful, grWsiWinGetDisplayModeList() returns GR_SUCCESS and the display mode information written to pDisplayModeList.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pDisplayModeCount is NULL
	▼ GR_ERROR_INVALID_MEMORY_SIZE if pDisplayModeList is not NULL and the pDisplayModeCount input value is smaller than the number of attached displays


	NOTES
	If pDisplayModeList is NULL, the input pDisplayModeCount value does not matter and the function returns the number of displays in pDisplayModeCount.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWsiWinGetDisplayModeList(
	/* Display object handle. */
    GR_WSI_WIN_DISPLAY display,
	/* [in/out] The maximum number of display modes to enumerate, and the output value specifies the total number of display modes that were enumerated in pDisplayModeList. */
    GR_UINT* pDisplayModeCount,
	/* [out] Array of returned display modes. See GR_WSI_WIN_DISPLAY_MODE. Can be NULL. */
    GR_WSI_WIN_DISPLAY_MODE* pDisplayModeList);

/*
	Application enters fullscreen mode.


	RETURNS
	grWsiWinTakeFullscreenOwnership() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the display or image handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the display handle references an invalid object type
	▼ GR_ERROR_UNAVAILABLE if display is in fullscreen mode
	▼ GR_WSI_WIN_ERROR_INVALID_RESOLUTION if presentable image resolution does not match the fullscreen mode


	NOTES
	The presentable image should specify GR_WSI_WIN_IMAGE_CREATE_FULLSCREEN_PRESENT flag on creation and must be associated with this display.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWsiWinTakeFullscreenOwnership(
	/* Display object handle. */
    GR_WSI_WIN_DISPLAY display,
	/* Presentable image object handle. */
    GR_IMAGE image);

/*
	Application exits fullscreen mode after it was entered with grWsiWinTakeFullscreenOwnership().


	RETURNS
	grWsiWinReleaseFullscreenOwnership() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the display handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the display handle references an invalid object type
	▼ GR_ERROR_UNAVAILABLE if display is not in fullscreen mode


	NOTES
	Applications must release fullscreen ownership before destroying an associated device. Furthermore, the application must respond to losing focus (i.e., WM_KILLFOCUS events) by releasing fullscreen ownership and retaking fullscreen ownership when appropriate (i.e., a subsequent WM_SETFOCUS event).


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWsiWinReleaseFullscreenOwnership(
	/* Display object handle. */
    GR_WSI_WIN_DISPLAY display);

/*
	Sets custom gamma ramp in fullscreen mode.


	RETURNS
	grWsiWinSetGammaRamp() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the display handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the display handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pGammaRamp is NULL
	▼ GR_ERROR_INVALID_VALUE if any of the gamma ramp parameters are not in a valid range
	▼ GR_ERROR_UNAVAILABLE if display is not in fullscreen mode


	NOTES
	The gamma ramp is reset when exiting fullscreen exclusive mode. The application should restore custom gamma ramp when returning to fullscreen exclusive mode.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWsiWinSetGammaRamp(
	/* Display object handle. */
    GR_WSI_WIN_DISPLAY display,
	/* [in] Gamma ramp parameters. See GR_WSI_WIN_GAMMA_RAMP. */
    const GR_WSI_WIN_GAMMA_RAMP* pGammaRamp);

/*
	Waits for vertical blanking interval on display.


	RETURNS
	grWsiWinWaitForVerticalBlank() returns GR_SUCCESS if the function successfully waited for vertical blanking interval.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the display handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the display handle references an invalid object type
	▼ GR_ERROR_UNAVAILABLE if display is not in fullscreen mode and functionality is unavailable in windowed mode


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWsiWinWaitForVerticalBlank(
	/* Display object handle. */
    GR_WSI_WIN_DISPLAY display);

/*
	Returns current scan line for the display.


	RETURNS
	If successful, grWsiWinGetScanLine() returns GR_SUCCESS and the current scan line is written to pScanLine.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the display handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the display handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pScanLine is NULL
	▼ GR_ERROR_UNAVAILABLE if display is not in fullscreen mode and functionality is unavailable in windowed mode


	NOTES
	A value of -1 indicates the display is currently in its vertical blanking period.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWsiWinGetScanLine(
	/* Display object handle. */
    GR_WSI_WIN_DISPLAY display,
	/* [out] Current scan line. */
    GR_INT* pScanLine);

/*
	Creates an image that can be used as a source for presentation.


	RETURNS
	If successful, grWsiWinCreatePresentableImage() returns GR_SUCCESS and the created image object and its internal memory object is written to the location specified by pImage and pMem.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if the image dimensions are invalid
	▼ GR_ERROR_INVALID_VALUE if the refresh rate is invalid
	▼ GR_ERROR_INVALID_POINTER if pCreateInfo or pImage or pMem is NULL
	▼ GR_ERROR_INVALID_FORMAT if the format cannot be used for presentable image
	▼ GR_ERROR_INVALID_FLAGS if invalid presentable image creation flags or image usage flags are specified


	NOTES
	By definition, presentable images have a 2D image type, optimal tiling, a depth of 1, 1 mipmap level, and are single sampled. Fullscreen stereo images have an implicit array size of 2; all other presentable images have an implicit array size of 1.

	The internal memory object for presentable image returned in pMem cannot be freed, mapped or used for object binding.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWsiWinCreatePresentableImage(
	/* Device handle. */
    GR_DEVICE device,
	/* [in] Presentable image creation info. See GR_WSI_WIN_PRESENTABLE_IMAGE_CREATE_INFO. */
    const GR_WSI_WIN_PRESENTABLE_IMAGE_CREATE_INFO* pCreateInfo,
	/* [out] Presentable image object handle. */
    GR_IMAGE* pImage,
	/* [out] Memory handle for presentable image. */
    GR_GPU_MEMORY* pMem);

/*
	Displays a presentable image.


	RETURNS
	grWsiWinQueuePresent() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the queue handle or presentable image handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the queue handle references an invalid object type
	▼ GR_ERROR_INVALID_POINTER if pPresentInfo is NULL
	▼ GR_ERROR_INVALID_VALUE if the presentation mode or presentation interval is invalid
	▼ GR_ERROR_INVALID_FLAGS if presentation flags are invalid or do not match the present mode
	▼ GR_ERROR_INVALID_IMAGE if image is not presentable or if it does not match the present mode


	NOTES
	The presentable image has to be in the appropriate state for the used presentation method. Image has to be in the GR_WSI_WIN_IMAGE_STATE_PRESENT_WINDOWED state for windowed presentation and the GR_WSI_WIN_IMAGE_STATE_PRESENT_FULLSCREEN state for fullscreen presentation.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWsiWinQueuePresent(
	/* Device handle. */
    GR_QUEUE queue,
	/* [in] Presentation parameters. See GR_WSI_WIN_PRESENT_INFO. */
    const GR_WSI_WIN_PRESENT_INFO* pPresentInfo);

/*
	Specifies how many frames can be placed in the presentation queue.


	RETURNS
	grWsiWinSetMaxQueuedFrames() returns GR_SUCCESS if the function executed successfully.

	Otherwise, it returns one of the following errors:
	▼ GR_ERROR_INVALID_HANDLE if the device handle is invalid
	▼ GR_ERROR_INVALID_OBJECT_TYPE if the device handle references an invalid object type
	▼ GR_ERROR_INVALID_VALUE if specified queue limit is invalid


	NOTES
	When specifying the presentation queue limit for multiple GPUs used in multi-device configurations (e.g., for alternate frame rendering), the same value has to be set on all GPUs.


	NOT THREAD SAFE
*/
GR_DLLIMPORT
GR_RESULT GR_STDCALL grWsiWinSetMaxQueuedFrames(
	/* Device handle. */
    GR_DEVICE device,
	/* Maximum number of frames that can be batched. Specifying a value of zero resets the queue limit to a default system value (3 frames). */
    GR_UINT maxFrames);




GR__LEAVE_C

#endif
