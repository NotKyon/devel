﻿#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

template< typename T, size_t S >
inline size_t array_size( const T ( & )[ S ] )
{
	return S;
}

int main()
{
	const wchar_t *const src = L"abc";
	const wchar_t *p = src;
	while( *p != L'\0' )
	{
		++p;
	}
	printf( "%i\n", ( int )( p - src ) );
	printf( "%i\n", ( ( int )( p - src ) )/sizeof( wchar_t ) );
	char x[ 5 ];
	printf( "%i\n", ( int )array_size( x ) );
	return EXIT_SUCCESS;
}
