﻿/*
===============================================================================

	VARINT TEST

	This tests the concept of returning a temporary object that can be modified
	to propagate changes back to a containing object. (Proxies.)

	Overall the concept seems to work. If the proxy object is not explicitly
	coded to be _just_ a proxy then it would be a good idea to implement the
	concept of ownership into the object.

	The concept is most useful for systems where a value that is returned must
	be both readable and writable but cannot be represented as a direct pointer
	into an object. For example, a "utf8_string" class (where characters can be
	of varying bit-widths) would benefit from having a utf32_char_proxy class
	where the constructor converts an utf8 input string into a single utf32
	character (which is much easier to manage) while storing the index to the
	character in question and a reference to the string class. Then on the proxy
	class's destruction, if any changes were made they would be propagated to
	the containing class. (The exact semantics may change in practice; this is
	only an example afterall.)

===============================================================================
*/

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

/*
template< class T >
inline T &&forward( T &x )
{
	return static_cast< T && >( x );
}
template< class T >
inline T &&forward( T &&x )
{
	return static_cast< T && >( x );
}
template< class T >
inline T &&move( T &&x )
{
	return static_cast< T && >( x );
}
*/

unsigned char gVarintBuf[ 4096 ];
size_t gVarintBufPtr = 0;

inline unsigned char *Alloc( size_t n )
{
	const size_t i = gVarintBufPtr;
	gVarintBufPtr += n;

	return &gVarintBuf[ i ];
}
inline void Dealloc( unsigned char *p )
{
	if( !p ) { return; }

	gVarintBufPtr = ( size_t )( p - &gVarintBuf[ 0 ] );
}

class Varint
{
public:
	Varint(): mLength( 0 ), mBuffer( nullptr ), mOwns( true )
	{
	}
	/*
	Varint( Varint &&x ): mLength( x.mLength ), mBuffer( x.mBuffer ),
	mOwns( x.mOwns )
	{
		x.mOwns = false;
	}
	*/
	Varint( size_t len, unsigned char *bytes ): mLength( len ), mBuffer( bytes ),
	mOwns( false )
	{
	}
	Varint( int x ): mLength( 4 ), mBuffer( Alloc( 4 ) ), mOwns( true )
	{
		*( int * )mBuffer = x;
	}
	~Varint()
	{
		if( mOwns )
		{
			Dealloc( mBuffer );
		}
	}

	Varint &operator=( const Varint &x )
	{
		if( mLength != x.mLength )
		{
			if( mOwns )
			{
				Dealloc( mBuffer );
			}

			mLength = x.mLength;
			mBuffer = Alloc( mLength );

			mOwns = true;
		}
		
		for( size_t i = 0; i < mLength; ++i )
		{
			mBuffer[ i ] = x.mBuffer[ i ];
		}
		
		return *this;
	}
	operator int() const
	{
		int x = 0;

		const size_t i = mLength > 4 ? mLength - 4 : 0;
		size_t j = 0;
		while( i + j < mLength )
		{
			*( ( ( unsigned char * )&x ) + j ) = mBuffer[ j ];
			++j;
		}
		
		return x;
	}

private:
	size_t mLength;
	unsigned char *mBuffer;
	bool mOwns;
};

class VarintContainer
{
public:
	VarintContainer(): mSize( 0 )
	{
	}
	~VarintContainer()
	{
	}

	void AppendInt()
	{
		size_t lastPos = 0;
		if( mSize > 0 )
		{
			lastPos = mIndex[ mSize - 1 ] + mLength[ mSize - 1 ];
		}

		mIndex[ mSize ] = lastPos;
		mLength[ mSize ] = 4;

		mBuffer[ lastPos + 0 ] = 0;
		mBuffer[ lastPos + 1 ] = 0;
		mBuffer[ lastPos + 2 ] = 0;
		mBuffer[ lastPos + 3 ] = 0;

		++mSize;
	}
	size_t GetSize() const
	{
		return mSize;
	}

	Varint operator[]( size_t i )
	{
		if( i >= mSize ) { return Varint( 0 ); }
		return Varint( mLength[ i ], &mBuffer[ mIndex[ i ] ] );
	}

private:
	unsigned char mBuffer[ 4096 ];
	size_t mIndex[ 4096 ];
	size_t mLength[ 4096 ];
	size_t mSize;
};

int main()
{
	VarintContainer test;

	for( int count = 0; count < 16; ++count )
	{
		test.AppendInt();
		test[ count ] = rand() % 2048;
	}

	for( size_t i = 0; i < test.GetSize(); ++i )
	{
		const int x = ( int )test[ i ];
		const int r = rand() % 2048;
		test[ i ] = r;
		const int y = ( int )test[ i ];
		printf( "%i -> %i ? %s (%i)\n", x, y, y == r ? "PASS" : "FAIL", r );
	}

	return EXIT_SUCCESS;
}
