#ifndef MESHIO_CORE_H
#define MESHIO_CORE_H

typedef enum {
	EMeshIO_Byte,
	EMeshIO_Short,
	EMeshIO_Int,
	EMeshIO_Int64,
	EMeshIO_Float,
	EMeshIO_Double
} EMeshIODataType;

/*
 * Allocate, reallocate, or free memory.
 *
 * If `p' is non-zero and `n' is zero, the memory pointed to by `p' is freed. If
 * `p' is non-zero and `n' is non-zero, the memory pointed to by `p' is resized
 * to `n' bytes. Otherwise, `n' bytes are allocated and returned.
 */
extern void *meshio_memory( void *p, size_t n );

/*
 * Determine the size in bytes of a specific data type.
 *
 * If `kind' is not a member of EMeshIODataType, zero is returned.
 */
extern size_t meshio_data_type_size( EMeshIODataType kind );

#endif
