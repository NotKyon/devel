#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

const char *g_text =
"function f%s(x as integer, y as float, z$)\n\
	a as integer\n\
	b as integer\n\
	c as float\n\
	d as float\n\
	e as string\n\
	s as string\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	a = x + 5\n\
	a = x + a\n\
	a = x - 3\n\
	a = x*5 + x*6 - x*7 + x*8\n\
	b = a*x\n\
	b = b - a\n\
	b = b - x\n\
	b = b + a*(3 - x)/x + x*(4 - a)/a\n\
\n\
	x = 7 + a + b - x*x*x\n\
\n\
	c = y*1.2 + 3.4 - y*5.6 + y*7.8 - 9.10\n\
	d = c*3.1415926535897932384626433832795028841971693993751058209\n\
	d = d - c/1.23456789\n\
	c = c - d/9.87654321\n\
	c = c + y/c/d\n\
	d = d + y/d/c\n\
\n\
	y = 3.5 + c + d - y*y*y\n\
\n\
	c = y*1.2 + 3.4 - y*5.6 + y*7.8 - 9.10\n\
	d = c*3.1415926535897932384626433832795028841971693993751058209\n\
	d = d - c/1.23456789\n\
	c = c - d/9.87654321\n\
	c = c + y/c/d\n\
	d = d + y/d/c\n\
\n\
	y = 3.5 + c + d - y*y*y\n\
\n\
	c = y*1.2 + 3.4 - y*5.6 + y*7.8 - 9.10\n\
	d = c*3.1415926535897932384626433832795028841971693993751058209\n\
	d = d - c/1.23456789\n\
	c = c - d/9.87654321\n\
	c = c + y/c/d\n\
	d = d + y/d/c\n\
\n\
	y = 3.5 + c + d - y*y*y\n\
\n\
	c = y*1.2 + 3.4 - y*5.6 + y*7.8 - 9.10\n\
	d = c*3.1415926535897932384626433832795028841971693993751058209\n\
	d = d - c/1.23456789\n\
	c = c - d/9.87654321\n\
	c = c + y/c/d\n\
	d = d + y/d/c\n\
\n\
	y = 3.5 + c + d - y*y*y\n\
\n\
	c = y*1.2 + 3.4 - y*5.6 + y*7.8 - 9.10\n\
	d = c*3.1415926535897932384626433832795028841971693993751058209\n\
	d = d - c/1.23456789\n\
	c = c - d/9.87654321\n\
	c = c + y/c/d\n\
	d = d + y/d/c\n\
\n\
	y = 3.5 + c + d - y*y*y\n\
\n\
	c = y*1.2 + 3.4 - y*5.6 + y*7.8 - 9.10\n\
	d = c*3.1415926535897932384626433832795028841971693993751058209\n\
	d = d - c/1.23456789\n\
	c = c - d/9.87654321\n\
	c = c + y/c/d\n\
	d = d + y/d/c\n\
\n\
	y = 3.5 + c + d - y*y*y\n\
\n\
	c = y*1.2 + 3.4 - y*5.6 + y*7.8 - 9.10\n\
	d = c*3.1415926535897932384626433832795028841971693993751058209\n\
	d = d - c/1.23456789\n\
	c = c - d/9.87654321\n\
	c = c + y/c/d\n\
	d = d + y/d/c\n\
\n\
	y = 3.5 + c + d - y*y*y\n\
\n\
	c = y*1.2 + 3.4 - y*5.6 + y*7.8 - 9.10\n\
	d = c*3.1415926535897932384626433832795028841971693993751058209\n\
	d = d - c/1.23456789\n\
	c = c - d/9.87654321\n\
	c = c + y/c/d\n\
	d = d + y/d/c\n\
\n\
	y = 3.5 + c + d - y*y*y\n\
\n\
	c = y*1.2 + 3.4 - y*5.6 + y*7.8 - 9.10\n\
	d = c*3.1415926535897932384626433832795028841971693993751058209\n\
	d = d - c/1.23456789\n\
	c = c - d/9.87654321\n\
	c = c + y/c/d\n\
	d = d + y/d/c\n\
\n\
	y = 3.5 + c + d - y*y*y\n\
\n\
	c = y*1.2 + 3.4 - y*5.6 + y*7.8 - 9.10\n\
	d = c*3.1415926535897932384626433832795028841971693993751058209\n\
	d = d - c/1.23456789\n\
	c = c - d/9.87654321\n\
	c = c + y/c/d\n\
	d = d + y/d/c\n\
\n\
	y = 3.5 + c + d - y*y*y\n\
\n\
	e = z$ + \"A\"\n\
	s = e + \"BC\"\n\
\n\
	z$ = e + s + z$\n\
\n\
	e = z$ + \"A\"\n\
	s = e + \"BC\"\n\
\n\
	z$ = e + s + z$\n\
\n\
	e = z$ + \"A\"\n\
	s = e + \"BC\"\n\
\n\
	z$ = e + s + z$\n\
\n\
	e = z$ + \"A\"\n\
	s = e + \"BC\"\n\
\n\
	z$ = e + s + z$\n\
\n\
	e = z$ + \"A\"\n\
	s = e + \"BC\"\n\
\n\
	z$ = e + s + z$\n\
\n\
	return val(z$)*x*y*a*b*c*d + val(e)\n\
endfunction\n";

void strcatf(char *buf, size_t bufn, const char *format, ...) {
	char tmp[256];
	va_list args;

	va_start(args, format);
#if __STDC_WANT_SECURE_LIB__
	vsprintf_s(tmp, sizeof(tmp), format, args);
#else
	vsnprintf(tmp, sizeof(tmp), format, args);
	tmp[sizeof(tmp) - 1] = '\0';
#endif
	va_end(args);

#if __STDC_WANT_SECURE_LIB__
	strcat_s(buf, bufn, tmp);
#else
	{
		char *p;

		p = strchr(buf, '\0');
		strncpy(p, tmp, bufn - (p - buf));
		p[bufn - (p - buf)] = '\0';
	}
#endif
}
void generate(FILE *out, char a, char b, char c, int i) {
	char buf[256];

	buf[0] = '\0';
	if (a)
		strcatf(buf, sizeof(buf), "%c", a);
	if (b)
		strcatf(buf, sizeof(buf), "%c", b);
	if (c)
		strcatf(buf, sizeof(buf), "%c", c);
	if (i)
		strcatf(buf, sizeof(buf), "%i", i);

	fprintf(out, g_text, buf);
}

void dogen(FILE *out) {
	static const char A[] = { '\0','A','B','C','R','S','T' };
	static const char B[] = { '\0','g','h','j','k','l','v' };
	static const char C[] = { '\0','X','Y','Z','W' };
	int i, j, k, l;

	for(i=0; i<sizeof(A); i++) {
		for(j=0; j<sizeof(B); j++) {
			for(k=0; k<sizeof(C); k++) {
				for(l=1; l<=99; l++) {
					generate(out, A[i],B[j],C[k], l);
				}
			}
		}
	}
}

int main() {
	dogen(stdout);
	return EXIT_SUCCESS;
}
