//
//	Signals are a useful mechanism for specifying that something is ready for
//	synchronization with some other object. This example demonstrates one way
//	signals can be used.
//
#define GB_INIT 1
#include <gamebase.h>

//
//	We'll define some data to work on, as well as the synchronization primitives
//	used (signals).
//
gb_signal_t g_initSig;	//signal: begin working on the data
gb_signal_t g_finiSig;	//signal: finished working on the data

int g_data;				//the data to be worked on

//
//	This thread will perform work on the data
//
int Thread_f(void *p) {
	gb_uint_t i;
	gb_uint_t seed;

	// set the seed value
	seed = *(int *)&p;

	// main loop
	while(1) {
		// wait until we can begin work on the data
		gbSyncSignal(g_initSig);

		// we'll reset this signal now so it can be raised after we finish
		gbResetSignal(g_initSig, 1);

		// if the data we now have is '0' it means we must exit
		if (g_data == 0)
			break;

		// let's work on this!
		for(i=2; i<40000000 - seed; i++)
			g_data += (g_data*i - g_data*i*(seed + 1))/(i*(i - 1)) + i*seed;

		// done!
		gbRaiseSignal(g_finiSig);
	}

	// done!
	return 0;
}

//
//	The primary thread initializes the signals, and starts the secondary thread
//
void GBMain() {
	gb_thread_t thread;
	gb_uint_t i;
	int v[] = { 42, 23, -14 };

	// create the signals ('1' is the number of times a signal has to be raised
	//                     before it is considered "signalled")
	g_initSig = gbNewSignal(1);
	g_finiSig = gbNewSignal(1);

	// start the auxillary thread (with a seed value as the parameter)
	thread = gbNewThread(Thread_f, (void *)274);

	// we'll run this loop three times
	for(i=0; i<sizeof(v)/sizeof(v[0]); i++) {
		// tell the thread to do processing
		g_data = v[i];
		gbRaiseSignal(g_initSig);

		// now we'll wait for the data to finish
		gbSyncSignal(g_finiSig);
		gbResetSignal(g_finiSig, 1);

		// save and print the result
		gbPrint(gbF("%i", g_data));
	}

	// now that we're done with this thread, we'll *safely* terminate it
	g_data = 0;
	gbRaiseSignal(g_initSig); //tell the thread to finish (g_data=0)
	thread = gbDeleteThread(thread); //otherwise this would never complete

	// clean-up
	gbDeleteSignal(g_finiSig);
	gbDeleteSignal(g_initSig);
}
