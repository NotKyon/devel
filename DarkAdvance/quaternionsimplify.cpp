#include "simplify.h"

quat_t *NewQuat() {
	quat_t *p;

	p = (quat_t *)NewMemory(sizeof(*p));
	if (!p)
		return (quat_t *)0;

	p->w = 0;
	p->x = 0;
	p->y = 0;
	p->z = 0;

	return p;
}

void CopyQuat(quat_t *dst, const quat_t *src) {
	dst->w = src->w;
	dst->x = src->x;
	dst->y = src->y;
	dst->z = src->z;
}
quat_t *DuplicateQuat(const quat_t *src) {
	quat_t *p;

	p = (quat_t *)NewMemory(sizeof(*p));
	if (!p)
		return (quat_t *)0;

	CopyQuat(p, src);
	return p;
}

void Quat_Set(quat_t *dst, float w, float x, float y, float z) {
	dst->w = w;
	dst->x = x;
	dst->y = y;
	dst->z = z;
}
void Quat_SetW(quat_t *dst, float w) {
	dst->w = w;
}
void Quat_SetX(quat_t *dst, float x) {
	dst->x = x;
}
void Quat_SetY(quat_t *dst, float y) {
	dst->y = y;
}
void Quat_SetZ(quat_t *dst, float z) {
	dst->z = z;
}
float Quat_W(const quat_t *src) {
	return src->w;
}
float Quat_X(const quat_t *src) {
	return src->x;
}
float Quat_Y(const quat_t *src) {
	return src->y;
}
float Quat_Z(const quat_t *src) {
	return src->z;
}
