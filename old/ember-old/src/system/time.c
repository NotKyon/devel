#include <ember/config.h>
#include <ember/time.h>
#include <stdlib.h>
#include <string.h>
#if defined( _WIN32 ) && !defined( __CYGWIN__ )
#include <windows.h>
#else
#include <sys/time.h>
#include <unistd.h>
#endif

#define MAX_QUEUE 4

static struct {
	timestamp_t queue[ MAX_QUEUE ];
	timestamp_t sort[ MAX_QUEUE ];
	double average;
	double tween;
	size_t pos;
} g_time;

static int time__compare( const void *a, const void *b )
{
	return ( int )( ( *( const timestamp_t * )a ) - ( *( const timestamp_t * )b ) );
}

static void time__queue( timestamp_t t )
{
	int i, pos;

	pos = ( int )( g_time.pos++ % MAX_QUEUE );

	g_time.queue[ pos ] = t;

	memcpy( g_time.sort, g_time.queue, sizeof( timestamp_t ) * MAX_QUEUE );
	qsort( g_time.sort, MAX_QUEUE, sizeof( timestamp_t ), time__compare );

	g_time.average = 0;

	if( MAX_QUEUE > 2 ) {
		for( i = 1; i < MAX_QUEUE - 1; i++ )
			g_time.average += ( double )g_time.sort[ i ];
		g_time.average /= ( double )( MAX_QUEUE - 2 );
	} else {
		for( i = 0; i < MAX_QUEUE; i++ )
			g_time.average += ( double )g_time.sort[ i ];
		g_time.average /= ( double )MAX_QUEUE;
	}

	g_time.tween = g_time.average / 1000000.0;
}

timestamp_t time_get()
{
#ifdef _WIN32
	LARGE_INTEGER f, c;
	double fd, cd;

	QueryPerformanceFrequency( &f );
	QueryPerformanceCounter( &c );

	fd = ( double )*( timestamp_t * )&f;
	cd = ( double )*( timestamp_t * )&c;

	cd = cd / fd;

	return ( timestamp_t )( cd * 1000000 );
#else
	struct timeval tv;
	timestamp_t t;

	gettimeofday( &tv, 0 );

	t = ( ( timestamp_t )tv.tv_sec ) * 1000000;
	t += ( ( timestamp_t )tv.tv_usec );

	return t;
#endif
}

void time_init()
{
	int i;

	for( i = 0; i < MAX_QUEUE; i++ )
		g_time.queue[ i ] = 1000000 / 60;

	g_time.pos = 0;
}

void time_update()
{
	static timestamp_t t1 = 0;
	timestamp_t t2;

	t2 = time_get();
	time__queue( t2 - t1 );

	t1 = t2;
}

float time_tween()
{
	return g_time.tween;
}

void time_yield()
{
#ifdef _WIN32
	Sleep( 0 );
#else
	struct timespec t;

	t.tv_sec  = 0;
	t.tv_nsec = 1.0 / 1200.0 * 1000000000.0;

	nanosleep( &t, 0 );
#endif
}
