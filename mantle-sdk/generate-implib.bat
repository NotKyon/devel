@echo off
dumpbin /exports %~dp0%1.dll > %~dp0%1.exports.txt
if errorlevel 1 goto fail
echo LIBRARY %1 > %~dp0%1.def
echo EXPORTS >> %~dp0%1.def
for /f "skip=19 tokens=4" %%A in ( %~dp0%1.exports.txt ) do (
	echo %%A >> %~dp0%1.def
)
lib /def:%~dp0%1.def /out:%~dp0%1.lib /machine:amd64
goto e

:fail
echo Could not run dumpbin
del /Q %~dp0%1.def
del /Q %~dp0%1.exports.txt

:e


