#include "lookup.h"

#include "memory.h"
#include "report.h"

static int GenerateConvmap(int dst[256], const char *allowed) {
	int i, j, k;

	for(i=0; i<256; i++)
		dst[i] = -1;

	for(i=0; allowed[i]; i++)
		dst[(unsigned char)allowed[i]] = i;

	for(j=0; j<i; j++) {
		for(k=j+1; k<i; k++) {
			if (allowed[j]==allowed[k])
				return -1; /*duplicate entries*/
		}
	}

	return i;
}

static entry_t *FindFromEntry(lookup_t *table, entry_t *entries,
const char *str, int create) {
	size_t n;
	int i;

	ASSERT(table != (lookup_t *)0);
	ASSERT(entries != (entry_t *)0);
	ASSERT(str != (const char *)0);

	i = table->convmap[*(unsigned char *)str++];
	if (i == -1)
		return (entry_t *)0; /*invalid character*/

	/* if there's more to the string... */
	if (*str) {
		/* generate the entries if they're not there */
		if (!entries[i].entries) {
			if (create) {
				n = sizeof(entry_t)*table->numEntries;

				entries[i].entries = (entry_t *)AllocZero(n);
			}

			if (!entries[i].entries)
				return (entry_t *)0; /*malloc failure/avoidance*/
		}

		/* continue the search (recursive!) */
		return FindFromEntry(table, entries[i].entries, str, create);
	}

	/* otherwise this is the last point */
	return &entries[i];
}

int InitLookup(lookup_t *table, const char *allowed) {
	size_t n;

	ASSERT(table != (lookup_t *)0);
	ASSERT(allowed != (const char *)0);

	table->numEntries = GenerateConvmap(table->convmap, allowed);
	if (table->numEntries < 1)
		return 0;

	n = sizeof(entry_t)*table->numEntries;

	table->entries = (entry_t *)AllocZero(n);
	if (!table->entries)
		return 0;

	return 1;
}
void DeleteLookupEntries(lookup_t *table, entry_t *entries) {
	int i;

	ASSERT(table != (lookup_t *)0);

	if (!entries)
		return;

	for(i=0; i<table->numEntries; i++) {
		if (entries[i].entries)
			DeleteLookupEntries(table, entries[i].entries);
	}

	Free((void *)entries);
}
void DeinitLookup(lookup_t *table) {
	ASSERT(table != (lookup_t *)0);

	DeleteLookupEntries(table, table->entries);
	table->entries = (entry_t *)0;

	table->numEntries = 0;
}

entry_t *FindEntry(lookup_t *table, const char *str) {
	ASSERT(table != (lookup_t *)0);
	ASSERT(str != (const char *)0);

	return FindFromEntry(table, table->entries, str, 0);
}
entry_t *LookupEntry(lookup_t *table, const char *str) {
	ASSERT(table != (lookup_t *)0);
	ASSERT(str != (const char *)0);

	return FindFromEntry(table, table->entries, str, 1);
}

entry_t *NewEntry(lookup_t *table, const char *str, void *p) {
	entry_t *entry;

	ASSERT(table != (lookup_t *)0);
	ASSERT(str != (const char *)0);
	ASSERT(p != (void *)0);

	entry = LookupEntry(table, str);
	if (!entry)
		return (entry_t *)0;

	if (entry->p)
		return (entry_t *)0;

	entry->p = p;
	return entry;
}
entry_t *SetEntry(lookup_t *table, const char *str, void *p) {
	entry_t *entry;

	ASSERT(table != (lookup_t *)0);
	ASSERT(str != (const char *)0);

	entry = FindEntry(table, str);
	if (!entry)
		return (entry_t *)0;

	entry->p = p;
	return entry;
}
void *GetEntry(lookup_t *table, const char *str) {
	entry_t *entry;

	ASSERT(table != (lookup_t *)0);
	ASSERT(str != (const char *)0);

	entry = FindEntry(table, str);
	if (!entry)
		return (void *)0;

	return entry->p;
}
