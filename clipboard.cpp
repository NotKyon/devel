#include <Windows.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>

bool CopyTextToClipboard( const std::string &str ) {
    if( !str.length() ) {
        return false;
    }

    const size_t len = str.length();

    if( !OpenClipboard( NULL ) ) {
        return false;
    }

    EmptyClipboard();

    const HGLOBAL clipMem = GlobalAlloc( GMEM_DDESHARE, len + 1 );
    if( !clipMem ) {
        CloseClipboard();
        return false;
    }

    char *const clipBuf = ( char * )GlobalLock( clipMem );
    if( !clipBuf ) {
        ( void )GlobalFree( clipMem );
        CloseClipboard();
        return false;
    }

    memcpy( clipBuf, ( const void * )str.c_str(), len );
    clipBuf[ len ] = '\0';
    GlobalUnlock( clipMem );

    SetClipboardData( CF_TEXT, clipMem );

    return true;
}
std::string PasteTextFromClipboard() {
    if( !OpenClipboard( NULL ) ) {
        return "";
    }

    std::string r( ( const char * )GetClipboardData( CF_TEXT ) );

    CloseClipboard();
    return r;
}

int main() {
	printf( "%s\n", PasteTextFromClipboard().c_str() );
	CopyTextToClipboard( "Hello, world!" );
	return EXIT_SUCCESS;
}
