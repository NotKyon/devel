#ifndef TASK_H
#define TASK_H

#if defined(EE_CONF_USE_PTHREAD) && !defined(TASK_CONF_USE_PTHREAD)
# define TASK_CONF_USE_PTHREAD EE_CONF_USE_PTHREAD
#endif

#undef __USE_WIN32_THREADS__
#if _WIN32 && !TASK_CONF_USE_PTHREAD
# define __USE_WIN32_THREADS__ 1

# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#else
# define __USE_WIN32_THREADS__ 0

# ifndef TASK_CONF_USE_PTHREAD
#  define TASK_CONF_USE_PTHREAD 1
# elif !TASK_CONF_USE_PTHREAD
#  error "Configured to not use pthread but only pthread available."
# endif

# ifndef _GNU_SOURCE
#  define _GNU_SOURCE 1
# endif
# include <pthread.h>
#endif

namespace task {

#if __USE_WIN32_THREADS__
# define THREADPROCAPI WINAPI
typedef HANDLE thread_t;
typedef CRITICAL_SECTION lock_t;
typedef DWORD threadResult_t;
#else
# define THREADPROCAPI
typedef pthread_t thread_t;
typedef pthread_mutex_t lock_t;
typedef void *threadResult_t;
#endif

typedef threadResult_t(THREADPROCAPI *threadFunc_t)(void *);
typedef void(*jobFunc_t)(void *);

typedef struct signal_s {
	unsigned int expCnt, curCnt;
#if _WIN32
	HANDLE ev;
#else
# error "TODO: Add event equivalent"
#endif
} signal_t;

typedef struct token_s {
	typedef enum {
		kDispatch,
		kSignal,
		kSync
	} type_t;

	type_t type;

	union {
		struct {
			jobFunc_t fn;
			void *p;
		};
		signal_t *sig;
		signal_t *sync;
	};
} token_t;
typedef struct queue_s {
	size_t cnt, cap, cur;
	token_t *tokens;
	lock_t lock;			//used to access all of the above fields

	signal_t readySig;		//signalled when there is work in the queue
	signal_t doneSig;		//signalled when there is no work in the queue

	bool stop;				//set to true when the queue should stop working
	signal_t stoppedSig;	//signalled when the stop completes
} queue_t;

//------------------------------------------------------------------------------

// user functions
extern void *Alloc(size_t n);
extern void *Realloc(void *p, size_t n);
extern void *Free(void *p);

// task functions
inline void InitLock(lock_t &M);
inline void FiniLock(lock_t &M);

inline void Lock(lock_t &M);
inline bool TryLock(lock_t &M);
inline void Unlock(lock_t &M);

inline bool IsHyperthreadingAvailable();
inline unsigned int GetCPUCoreCount();
inline unsigned int GetCPUThreadCount();

inline thread_t NewThread(threadFunc_t func, void *p);
inline void SetThreadCPU(thread_t &thread, unsigned int cpu);
inline void JoinThread(thread_t &thread);
inline void KillThread(thread_t &thread);
inline void Relinquish();

inline unsigned int AtomicIncrement(volatile unsigned int *x);
inline unsigned int AtomicDecrement(volatile unsigned int *x);

inline void InitSignal(signal_t &sig, unsigned int expCnt);
inline void FiniSignal(signal_t &sig);

inline void ResetSignal(signal_t &sig, unsigned int expCnt=0);
inline void AllocSignalSlot(signal_t &sig);
inline void RaiseSignal(signal_t &sig);

inline void SyncSignal(signal_t &sig);
inline bool TrySyncSignal(signal_t &sig);

inline void SpinSyncSignal(signal_t &sig, size_t spinCount=4000);
inline bool SyncSignals(size_t *index, size_t n, signal_t *sigs,
	bool all=false);

inline void InitQueue(queue_t &queue);
inline void FiniQueue(queue_t &queue);

inline token_t *AllocateQueueToken(queue_t &queue);

inline void EnqueueJob(queue_t &queue, jobFunc_t fn, void *p);
inline void EnqueueSignal(queue_t &queue, signal_t *sig);
inline void EnqueueSync(queue_t &queue, signal_t *sig);

inline void FlushQueue(queue_t &queue);
inline void FinishQueue(queue_t &queue);

inline void QueueWorkerTask(queue_t *queue);

//------------------------------------------------------------------------------
// MUTUAL EXCLUSION

inline void InitLock(lock_t &M) {
#if __USE_WIN32_THREADS__
	InitializeCriticalSection(&M);
#else
	static pthread_mutexattr_t defAttr;
	static bool didInitAttr = false;

	if (!didInitAttr) {
		pthread_mutexattr_init(&defAttr);
		pthread_mutexattr_settype(&defAttr, PTHREAD_MUTEX_RECURSIVE);

		didInitAttr = true;
	}

	if (!pthread_mutex_init(&M, &defAttr))
		ee::Sys->ErrorExit("Failed to initialize mutex.");
#endif
}
inline void FiniLock(lock_t &M) {
#if __USE_WIN32_THREADS__
	DeleteCriticalSection(&M);
#else
	pthread_mutex_destroy(&M);
#endif
	memset((void *)&M, 0, sizeof(M));
}

inline void Lock(lock_t &M) {
#if __USE_WIN32_THREADS__
	WaitForSingleObject(&M, INFINITE);
#else
	pthread_mutex_lock(&M);
#endif
}
inline bool TryLock(lock_t &M) {
#if __USE_WIN32_THREADS__
	return (bool)(true & TryEnterCriticalSection(&M));
#else
	if (!pthread_mutex_trylock(&M))
		return true;

	return false;
#endif
}
inline void Unlock(lock_t &M) {
#if __USE_WIN32_THREADS__
	LeaveCriticalSection(&M);
#else
	pthread_mutex_unlock(&M);
#endif
}

//------------------------------------------------------------------------------
// CPU INFORMATION

inline bool IsHyperthreadingAvailable() {
#if __i386__||__x86_64__ || _M_IX86 || _M_IX64
	static bool didInit = false;
	static bool htAvail;

	if (!didInit) {
		unsigned int d;

# if __GNUC__ || __clang__
		__asm__("cpuid" : "=d" (d) : "a" (1)); //get feature set
# else
		__asm {
			mov eax, 1
			cpuid
			mov [d], edx
		};
# endif

		htAvail = (d>>28) & true;
		didInit = true;
	}

	return htAvail;
#else
	return false;
#endif
}
inline unsigned int GetCPUCoreCount() {
#if _WIN32
	SYSTEM_INFO si;

	memset(&si, 0, sizeof(si));
	GetSystemInfo(&si);

	return (unsigned int)si.dwNumberOfProcessors;
#elif __linux__||__linux||linux
	int count;

	if ((count = sysconf(_SC_NPROCESSORS_ONLN)) < 1)
		count = 1;

	return count;
#else
	return 1; //TODO
#endif
}
inline unsigned int GetCPUThreadCount() {
	return GetCPUCoreCount()*(1 + (unsigned int)IsHyperthreadingAvailable());
}

//------------------------------------------------------------------------------
// THREADS

inline thread_t NewThread(threadFunc_t func, void *p) {
	thread_t h;

#if __USE_WIN32_THREADS__
	h = CreateThread(0, 0, func, p, 0, 0);
#else
	memset(&h, 0, sizeof(h));
	pthread_create(&h, (const pthread_attr_t *)0, func, p);
#endif

	return h;
}

inline void SetThreadCPU(thread_t &thread, unsigned int cpu) {
#if __USE_WIN32_THREADS__
	DWORD_PTR mask, cpuMask, stride;

	stride = GetCPUThreadCount()/GetCPUCoreCount();

	cpuMask  = 1;
	cpuMask |= stride&(1<<1);
	cpuMask |= stride&(1<<2);	//maybe there will be more threads one day
	cpuMask |= stride&(1<<3);

	mask = cpuMask<<(cpu*stride);

	SetThreadAffinityMask(thread, mask);
#else
	/*
	 * See: pthread_setaffinity_np: http://bit.ly/yw7R3W
	 *
	 * int pthread_attr_setaffinity_np(pthread_attr_t *attr, size_t cpusetsize,
	 * const cpu_set_t *cpuset);
	 */
#endif
}

inline void JoinThread(thread_t &thread) {
#if __USE_WIN32_THREADS__
	WaitForSingleObject(thread, INFINITE);
#else
	pthread_join(&thread, (void **)0);
#endif
}

inline void KillThread(thread_t &thread) {
#if __USE_WIN32_THREADS__
	//TerminateThread(thread, 0);
	CloseHandle(thread);

	thread = (thread_t)0;
#else
	// no pthread_destroy()? there's a pthread_kill()
	memset((void *)&thread, 0, sizeof(thread));
#endif
}

inline void Relinquish() {
#if __USE_WIN32_THREADS__
	Sleep(0);
#else
	sched_yield(); //nanosleep()?
#endif
}

//------------------------------------------------------------------------------
// ATOMICS

inline unsigned int AtomicIncrement(volatile unsigned int *x) {
#if _WIN32
	return InterlockedIncrement((volatile LONG *)x);
#else
	return __sync_fetch_and_add(x, 1);
#endif
}
inline unsigned int AtomicDecrement(volatile unsigned int *x) {
#if _WIN32
	return InterlockedDecrement((volatile LONG *)x);
#else
	return __sync_fetch_and_sub(x, 1);
#endif
}

//------------------------------------------------------------------------------
// SIGNALS

inline void InitSignal(signal_t &sig, unsigned int expCnt) {
	sig.expCnt = expCnt;
	sig.curCnt = 0;

#if _WIN32
	sig.ev = CreateEvent(0, TRUE, FALSE, 0);
#else
# error "TODO: Add event equivalent"
#endif
}
inline void FiniSignal(signal_t &sig) {
#if _WIN32
	if (sig.ev) {
		CloseHandle(sig.ev);
		sig.ev = 0;
	}
#else
# error "TODO: Add event equivalent"
#endif
}

inline void ResetSignal(signal_t &sig, unsigned int expCnt) {
	if (expCnt)
		sig.expCnt = expCnt;
	sig.curCnt = 0;

	ResetEvent(sig.ev);
}
inline void AllocSignalSlot(signal_t &sig) {
	sig.expCnt++;

	if (sig.curCnt>=(sig.expCnt - 1))
		ResetEvent(sig.ev);
}
inline void RaiseSignal(signal_t &sig) {
	AtomicIncrement(&sig.curCnt);

	if (sig.curCnt==sig.expCnt) {
#if _WIN32
		SetEvent(sig.ev);
#else
# error "TODO: Add event equivalent"
#endif
	}
}

inline void SyncSignal(signal_t &sig) {
#if _WIN32
	WaitForSingleObject(sig.ev, INFINITE);
#else
# error "TODO: Add event equivalent"
#endif
}
inline bool TrySyncSignal(signal_t &sig) {
#if _WIN32
	if (WaitForSingleObject(sig.ev, 0) != WAIT_OBJECT_0)
		return false;

	return true;
#else
# error "TODO: Add event equivalent"
#endif
}

inline void SpinSyncSignal(signal_t &sig, size_t spinCount) {
	size_t i;

	for(i=0; i<spinCount; i++) {
		if (TrySyncSignal(sig))
			return;
	}

	SyncSignal(sig);
}
inline bool SyncSignals(size_t *index, size_t n, signal_t **sigs, bool all) {
#if _WIN32
	HANDLE h[128];
	size_t i;
	DWORD r;

	if (n > sizeof(h)/sizeof(h[0]))
		return false;

	if (!sigs)
		return false;

	for(i=0; i<n; i++) {
		if (!sigs[i])
			return false;
		h[i] = sigs[i]->ev;
	}

	r = WaitForMultipleObjects(n, h, (BOOL)all, INFINITE);
	if (index) {
		if (r - WAIT_OBJECT_0 < n)
			*index = r - WAIT_OBJECT_0;
		else if(r - WAIT_ABANDONED_0 < n)
			*index = r - WAIT_ABANDONED_0;
	}

	return true;
#else
# error "TODO: Add event equivalent"
#endif
}

//------------------------------------------------------------------------------
// JOB QUEUE MANAGEMENT

inline void InitQueue(queue_t &queue) {
	queue.cnt = 0;
	queue.cap = 0;
	queue.cur = 0;
	queue.tokens = (token_t *)0;

	InitLock(queue.lock);

	InitSignal(queue.readySig, 1);
	InitSignal(queue.doneSig, 0);

	queue.stop = false;
	InitSignal(queue.stoppedSig, 1);
}
inline void FiniQueue(queue_t &queue) {
	queue.stop = true;
	RaiseSignal(queue.readySig);
	SyncSignal(queue.stoppedSig);

	Lock(queue.lock);
		queue.cnt = 0;
		queue.cap = 0;
		queue.cur = 0;
		queue.tokens = (token_t *)Free((void *)queue.tokens);

		FiniSignal(queue.readySig);
		FiniSignal(queue.doneSig);

		FiniSignal(queue.stoppedSig);
	Unlock(queue.lock);

	FiniLock(queue.lock);
}

inline token_t *AllocateQueueToken(queue_t &queue) {
	static const size_t granularity = 4096/sizeof(token_t);
	token_t *p;

	if (queue.cnt + 1 >= queue.cap) {
		size_t n;

		queue.cap  = queue.cnt + 1;
		queue.cap += granularity;
		queue.cap -= queue.cap%granularity;

		n = queue.cap*sizeof(token_t);

		queue.tokens = (token_t *)Realloc((void *)queue.tokens, n);
	}

	p = &queue.tokens[queue.cnt++];
	AllocSignalSlot(queue.doneSig);

	return p;
}

inline void EnqueueJob(queue_t &queue, jobFunc_t fn, void *p) {
	token_t *tok;

	Lock(queue.lock);
		tok = AllocateQueueToken(queue);

		tok->type = token_t::kDispatch;

		tok->fn   = fn;
		tok->p    = p;
	Unlock(queue.lock);
}
inline void EnqueueSignal(queue_t &queue, signal_t *sig) {
	token_t *tok;

	Lock(queue.lock);
		tok = AllocateQueueToken(queue);

		tok->type = token_t::kSignal;

		tok->sig  = sig;
	Unlock(queue.lock);
}
inline void EnqueueSync(queue_t &queue, signal_t *sig) {
	token_t *tok;

	Lock(queue.lock);
		tok = AllocateQueueToken(queue);

		tok->type = token_t::kSync;

		tok->sync = sig;
	Unlock(queue.lock);
}

inline void FlushQueue(queue_t &queue) {
	Lock(queue.lock);
		if (!queue.cnt) {
			Unlock(queue.lock);
			return;
		}

		ResetSignal(queue.doneSig);
	Unlock(queue.lock);

	RaiseSignal(queue.readySig);
}
inline void FinishQueue(queue_t &queue) {
	FlushQueue(queue);

	SyncSignal(queue.doneSig);
}

inline void QueueWorkerTask(queue_t *queue) {
	token_t *p;

	while(!queue->stop) {
		SyncSignal(queue->readySig);

		Lock(queue->lock);
			p = (queue->cur < queue->cnt) ? &queue->tokens[queue->cur++] : 0;
		Unlock(queue->lock);

		if (!p) {
			Relinquish();
			continue;
		}

		switch(p->type) {
		case token_t::kDispatch:
			p->fn(p->p);
			RaiseSignal(queue->doneSig);
			break;
		case token_t::kSignal:
			RaiseSignal(*p->sig);
			RaiseSignal(queue->doneSig);
			break;
		case token_t::kSync:
			while(!TrySyncSignal(*p->sync) && !queue->stop)
				Relinquish();
			RaiseSignal(queue->doneSig);
			break;
		}
	}

	ResetSignal(queue->doneSig, 1);
	RaiseSignal(queue->stoppedSig);
	RaiseSignal(queue->doneSig);
}

} //task namespace

#endif

//==============================================================================

//#include "task.h"
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>

//------------------------------------------------------------------------------
// TASK IMPLEMENTATIONS

void *task::Alloc(size_t n) {
	void *p;

	if (!n)
		return (void *)0;

	p = malloc(n);
	if (!p) {
		perror("Failed to allocate memory");
		exit(EXIT_FAILURE);
	}

	return p;
}
void *task::Realloc(void *p, size_t n) {
	void *q;

	if (!p)
		return Alloc(n);

	if (!n)
		return Free(p);

	q = realloc(p, n);
	if (!q) {
		perror("Failed to reallocate memory");
		exit(EXIT_FAILURE);
	}

	return q;
}
void *task::Free(void *p) {
	if (!p)
		return (void *)0;

	free(p);
	return (void *)0;
}

//------------------------------------------------------------------------------
// USER TEST

#define NUM_TESTS 1024

task::queue_t gMainQueue;
//task::signal_t gStoppedSig;
int gNumbers[NUM_TESTS];

void IsPrime_f(void *p) {
	char buf[32];
	int j;
	int i;

	i = *(int *)p;

	if (i<=1)
		snprintf(buf, sizeof(buf) - 1, "%i isn't prime", i);
	else if(i==2)
		snprintf(buf, sizeof(buf) - 1, "%i is prime", i);
	else {
		for(j=3; j<i; j+=2) {
			if (i%j == 0) {
				snprintf(buf, sizeof(buf) - 1, "%i isn't prime", i);
				j = 0;
				break;
			}
		}

		if (j != 0)
			snprintf(buf, sizeof(buf) - 1, "%i is prime", i);
	}

	buf[sizeof(buf) - 1] = '\0';
	printf("%s\n", buf);
	//ThreadPrintf("%s", buf);
}

task::threadResult_t THREADPROCAPI WorkerThread(void *) {
	task::QueueWorkerTask(&gMainQueue);
	//task::RaiseSignal(gStoppedSig);
	return (task::threadResult_t)0;
}

int main() {
	task::thread_t threads[32];
	size_t i, n;
	DWORD s, e;

	n = task::GetCPUThreadCount();
	if (n > sizeof(threads)/sizeof(threads[0]))
		n = sizeof(threads)/sizeof(threads[0]);
	printf("%u cores available\n", (unsigned int)n);
	fflush(stdout);

	//task::InitSignal(gStoppedSig, n);
	task::InitQueue(gMainQueue);

	for(i=0; i<NUM_TESTS; i++) {
		gNumbers[i] = 100000000 + i;
		task::EnqueueJob(gMainQueue, IsPrime_f, &gNumbers[i]);
	}

	for(i=0; i<n; i++)
		threads[i] = task::NewThread(WorkerThread, (void *)0);

	s = GetTickCount();
	task::FinishQueue(gMainQueue);
	e = GetTickCount();

	printf("%.3f second(s)\n", double(e - s)/1000.0);

	task::FiniQueue(gMainQueue);		//must call FiniQueue() before stopping!
	//task::SyncSignal(gStoppedSig);

	for(i=0; i<n; i++) {
		task::JoinThread(threads[i]);
		task::KillThread(threads[i]);
	}
		//task::KillThread(threads[i]);

	return 0;
}
