#include <stdio.h>

unsigned int nlp2(unsigned int x) {
	x |= (x >> 1);
	x |= (x >> 2);
	x |= (x >> 4);
	x |= (x >> 8);
	x |= (x >> 16);

	return x + 1;
}

void test(unsigned int x) {
	printf("nlp2(%u) = %u\n", x, nlp2(x));
}

int main() {
	test(0);
	test(1);
	test(2);
	test(3);
	test(4);
	test(5);
	test(6);
	test(7);
	test(8);
	test(9);
	test(10);
	test(11);
	test(12);

	return 0;
}
