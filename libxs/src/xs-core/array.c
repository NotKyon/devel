#include <xs/array.h>
#include <xs/assert.h>
#include <xs/memory.h>
#include <xs/string.h>

#include <string.h>

struct xs_array_s {
	xs_size_t capacity, size;
	char **data;

	struct xs_array_s *prev, *next;
};
static struct xs_array_s *g_arr_head = (struct xs_array_s *)0;
static struct xs_array_s *g_arr_tail = (struct xs_array_s *)0;

xs_array_t XsNewArray() {
	xs_array_t arr;

	arr = (xs_array_t)XsMemory((void *)0, sizeof(*arr));

	arr->capacity = 0;
	arr->size = 0;
	arr->data = (char **)0;

	arr->next = (xs_array_t)0;
	if ((arr->prev = g_arr_tail) != (xs_array_t)0)
		g_arr_tail->next = arr;
	else
		g_arr_head = arr;
	g_arr_tail = arr;

	return arr;
}

xs_size_t XsArrayCapacity(xs_array_t arr) {
	XS_ASSERT(arr != (xs_array_t)0);

	return arr->capacity;
}
xs_size_t XsArraySize(xs_array_t arr) {
	XS_ASSERT(arr != (xs_array_t)0);

	return arr->size;
}

char **XsArrayData(xs_array_t arr) {
	XS_ASSERT(arr != (xs_array_t)0);

	return arr->data;
}

char *XsArrayElement(xs_array_t arr, xs_size_t i) {
	XS_ASSERT(arr != (xs_array_t)0);
	XS_ASSERT(i < arr->size);

	return arr->data[i];
}

void XsSetArrayElement(xs_array_t arr, xs_size_t i, const char *cstr) {
	XS_ASSERT(arr != (xs_array_t)0);
	XS_ASSERT(i < arr->size);

	arr->data[i] = XsReduplicate(arr->data[i], cstr);
}

void XsClearArray(xs_array_t arr) {
	xs_size_t i;

	XS_ASSERT(arr != (xs_array_t)0);

	for(i=0; i<arr->size; i++)
		arr->data[i] = (char *)XsMemory((void *)arr->data[i], 0);

	arr->data = (char **)XsMemory((void *)arr->data, 0);

	arr->capacity = 0;
	arr->size = 0;
}

void XsDeleteArray(xs_array_t arr) {
	if (!arr)
		return;

	XsClearArray(arr);

	if (arr->prev)
		arr->prev->next = arr->next;
	if (arr->next)
		arr->next->prev = arr->prev;

	if (g_arr_head==arr)
		g_arr_head = arr->next;
	if (g_arr_tail==arr)
		g_arr_tail = arr->prev;

	XsMemory((void *)arr, 0);
}

void XsDeleteAllArrays() {
	while(g_arr_head)
		XsDeleteArray(g_arr_head);
}

void XsResizeArray(xs_array_t arr, xs_size_t n) {
	xs_size_t i;

	XS_ASSERT(arr != (xs_array_t)0);

	if (n > arr->capacity) {
		i = arr->capacity;
		arr->capacity += 4096/sizeof(void *);

		arr->data = (char **)XsMemory((void *)arr->data,
			arr->capacity*sizeof(char *));

		memset((void *)&arr->data[i], 0, (arr->capacity - i)*sizeof(char *));
	}

	arr->size = n;
}

xs_size_t XsArrayPush(xs_array_t arr, const char *cstr) {
	xs_size_t i;

	XS_ASSERT(arr != (xs_array_t)0);

	i = XsArraySize(arr);

	XsResizeArray(arr, i + 1);
	XsSetArrayElement(arr, i, cstr);

	return i;
}

void XsRemoveArrayDups(xs_array_t arr) {
	const char *a, *b;
	xs_size_t i, j, n;

	n = XsArraySize(arr);
	for(i=0; i<n; i++) {
		if (!(a = XsArrayElement(arr, i)))
			continue;

		for(j=i + 1; j<n; j++) {
			if (!(b = XsArrayElement(arr, j)))
				continue;

			if (!strcmp(a, b))
				XsSetArrayElement(arr, j, (const char *)0);
		}
	}
}
void XsExplode(xs_array_t arr, const char *str) {
	const char *p;
	char tmp[1024];

	p = str;
	while((p=strchr(p, ';')) != (const char *)0) {
		XsStrCpyN(tmp, sizeof(tmp), str, p - str);
		XsArrayPush(arr, tmp);
		p++;
	}

	p = strchr(p, '\0');
	if (p - str > 0) {
		XsStrCpyN(tmp, sizeof(tmp), str, p - str);
		XsArrayPush(arr, tmp);
	}
}
char *XsImplode(xs_array_t arr) {
	xs_size_t i, n;
	char *p;

	XS_ASSERT(arr != (xs_array_t)0);

	p = XsDuplicate("");

	n = XsArraySize(arr);
	for(i=0; i<n; i++) {
		if (i > 0)
			XsAppend(p, ";");

		/*
		 * FIXME: escape ';' in the source string
		 */
		p = XsAppend(p, XsArrayElement(arr, i));
	}

	return p;
}
