#include "mk_lib.h"

lib_t g_lib_head = (lib_t)0;
lib_t g_lib_tail = (lib_t)0;

lib_t NewLib() {
	size_t i;
	lib_t lib;

	lib = (lib_t)XsMemory((void *)0, sizeof(*lib));

	lib->name = (char *)0;
	for(i=0; i<kNumProjSys; i++)
		lib->flags[i] = (char *)0;

	lib->config = 0;

	lib->next = (lib_t)0;
	if ((lib->prev = g_lib_tail) != (lib_t)0)
		g_lib_tail->next = lib;
	else
		g_lib_head = lib;
	g_lib_tail = lib;

	return lib;
}
void DeleteLib(lib_t lib) {
	size_t i;

	if (!lib)
		return;

	lib->name = (char *)XsMemory((void *)lib->name, 0);
	for(i=0; i<kNumProjSys; i++)
		lib->flags[i] = (char *)XsMemory((void *)lib->flags[i], 0);

	if (lib->prev)
		lib->prev->next = lib->next;
	if (lib->next)
		lib->next->prev = lib->prev;

	if (g_lib_head==lib)
		g_lib_head = lib->next;
	if (g_lib_tail==lib)
		g_lib_tail = lib->prev;

	XsMemory((void *)lib, 0);
}
void DeleteAllLibs() {
	while(g_lib_head)
		DeleteLib(g_lib_head);
}

void SetLibName(lib_t lib, const char *name) {
	assert(lib != (lib_t)0);

	lib->name = XsReduplicate(lib->name, name);
}
void SetLibFlags(lib_t lib, int sys, const char *flags) {
	assert(lib != (lib_t)0);
	assert((unsigned int)sys < kNumProjSys);

	lib->flags[sys] = XsReduplicate(lib->flags[sys], flags);
}

const char *LibName(lib_t lib) {
	assert(lib != (lib_t)0);

	return lib->name;
}
const char *LibFlags(lib_t lib, int sys) {
	assert(lib != (lib_t)0);
	assert((unsigned int)sys < kNumProjSys);

	return lib->flags[sys];
}

lib_t LibBefore(lib_t lib) {
	assert(lib != (lib_t)0);

	return lib->prev;
}
lib_t LibAfter(lib_t lib) {
	assert(lib != (lib_t)0);

	return lib->next;
}
lib_t FirstLib() {
	return g_lib_head;
}
lib_t LastLib() {
	return g_lib_tail;
}
lib_t FindLib(const char *name) {
	lib_t lib;

	assert(name != (const char *)0);

	for(lib=g_lib_head; lib; lib=lib->next) {
		if (!lib->name)
			continue;

		if (!strcmp(lib->name, name))
			return lib;
	}

	return (lib_t)0;
}

void ClearLibProcessed(lib_t lib) {
	assert(lib != (lib_t)0);

	lib->config &= ~kLib_Processed_Bit;
}
void SetLibProcessed(lib_t lib) {
	assert(lib != (lib_t)0);

	lib->config |= kLib_Processed_Bit;
}
int TestLibProcessed(lib_t lib) {
	assert(lib != (lib_t)0);

	return (lib->config & kLib_Processed_Bit) ? 1 : 0;
}

void ClearAllLibsProcessed() {
	lib_t lib;

	for(lib=FirstLib(); lib; lib=LibAfter(lib))
		ClearLibProcessed(lib);
}
void SetAllLibsProcessed() {
	lib_t lib;

	for(lib=FirstLib(); lib; lib=LibAfter(lib))
		SetLibProcessed(lib);
}
int TestAnyLibsProcessed() {
	lib_t lib;

	for(lib=FirstLib(); lib; lib=LibAfter(lib))
		if (TestLibProcessed(lib))
			return 1;

	return 0;
}
