#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void pause()
{
	char x;

	fprintf(stderr, "Press ENTER to continue... ");
	fflush(stderr);

	fread(&x, 1, 1, stdin);
}

int main()
{
	pause();
	return EXIT_SUCCESS;
}
