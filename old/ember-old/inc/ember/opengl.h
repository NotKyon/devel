#ifndef EMBER_OPENGL_H
#define EMBER_OPENGL_H

#if defined( _WIN32 )
#include <windows.h>
#include <GL/glew.h>
#include <GL/wglew.h>
#elif defined( __APPLE__ )
/*
 * TODO
 */
#elif defined( __linux__ )
/*
 * TODO
 */
#endif

typedef enum {
	EGLParam_MajorVersion,
	EGLParam_MinorVersion,
	EGLParam_Profile,
	EGLParam_DisplayWidth,
	EGLParam_DisplayHeight,
	EGLParam_Fullscreen,
	EGLParam_WindowVisible
} EGLParam;

typedef enum {
	EGLProfile_Core,
	EGLProfile_Compatibility
} EGLProfile;

#if defined( _WIN32 )
extern HWND egl_get_hwnd__();
extern HDC egl_get_hdc__();
extern HGLRC egl_get_hglrc__();
#endif

extern int egl_set_param( EGLParam h, int v );
extern int egl_get_param( EGLParam h );
extern int egl_init();
extern void egl_deinit();
extern void egl_update();
extern int egl_running();
extern void egl_set_running( int r );
extern void egl_flip();
extern void egl_set_title( const char *title );
extern const char *egl_title();

#endif
