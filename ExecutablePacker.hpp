﻿#ifndef AX_EXECUTABLE_PACKER_HPP
#define AX_EXECUTABLE_PACKER_HPP

#include <vector>
#include <string>

#ifndef _WIN32
# error This is not supported on a non-Windows platform
#endif
#include <windows.h>

/*
===============================================================================

	TYPES
	Because MSVC doesn't understand what a standard is.

===============================================================================
*/
#ifndef STDINT_ENABLED
# if defined( _MSC_VER )
#  if _MSC_VER>=1600
#   define STDINT_ENABLED 1
#  else
#   define STDINT_ENABLED 0
#  endif
# else
#  define STDINT_ENABLED 1
# endif
#endif

#if STDINT_ENABLED
# include <stdint.h>
namespace Ax {

typedef int8_t s8_t;
typedef int16_t s16_t;
typedef int32_t s32_t;
typedef int64_t s64_t;
typedef uint8_t u8_t;
typedef uint16_t u16_t;
typedef uint32_t u32_t;
typedef uint64_t u64_t;

}
#elif defined( _MSC_VER ) || defined( __INTEL_COMPILER )
namespace Ax {

typedef signed __int8 s8_t;
typedef signed __int16 s16_t;
typedef signed __int32 s32_t;
typedef signed __int64 s64_t;
typedef unsigned __int8 u8_t;
typedef unsigned __int16 u16_t;
typedef unsigned __int32 u32_t;
typedef unsigned __int64 u64_t;

}
#else
# error Unknown method for type sizes
#endif

namespace Ax {

namespace _Detail {

template< int _size_ >
struct PointerSizedType {
};

template<>
struct PointerSizedType< 4 > {
	typedef s32_t sptr_t;
	typedef u32_t uptr_t;
};

template<>
struct PointerSizedType< 8 > {
	typedef s64_t sptr_t;
	typedef u64_t uptr_t;
};

}

typedef _Detail::PointerSizedType< sizeof( void * ) >::sptr_t sptr_t;
typedef _Detail::PointerSizedType< sizeof( void * ) >::uptr_t uptr_t;

// thrown in case of emergency
class Exception {
public:
	inline Exception( const std::string &message ): message( message ) {
	}
	inline Exception( const Exception &from ): message( from.message ) {
	}
	inline ~Exception() {
	}
	
	inline const char *getMessage() const {
		return message.c_str();
	}

private:
	const std::string message;
};

}

/*
===============================================================================

	EXECUTABLE PACKER
	This is for Windows executables. This will not work for GNU/Linux, UNIX,
	BeOS/Haiku, or any other OS.
	
	Instantiate an ExecutablePacker class and provide it with the files you
	want to pack, then tell it what stub executable to use, and what the final
	executable should be called.
	
	This can operate in a number of different modes. Select the mode you want
	it to operate in upon calling the 'pack' method.

	The stub would invoke the appropriate 'unpack' method which would specify
	the directory to put the files in, and where the pack file is. The pack
	file should be an absolute path to the pack file. If the pack file is the
	current executable, you can get the full path to it through
	GetModuleFileNameA( NULL, nameBuff, sizeof( nameBuff ) ).

===============================================================================
*/
namespace Ax {

enum packingMethod_t {
	// copy files to 'packName' directory on pack; leave alone for "unpack"
	kExecPack_CopyFiles,
	// copy files into 'packName' file; unpack from 'packName' to destination
	kExecPack_SeparateArchive,
	// append each file to 'stubName' file, as 'packName'; unpack from stub
	kExecPack_AppendExecutable
};

namespace _Detail {

struct InsertFile {
	std::string realName;
	std::string virtName;
	
	inline InsertFile(): realName(), virtName() {
	}
	inline InsertFile( const char *realName, const char *virtName ):
	realName( realName ), virtName( virtName ) {
	}
	inline InsertFile( const InsertFile &from ): realName( from.realName ),
	virtName( from.virtName ) {
	}
	inline ~InsertFile() {
	}
	
	inline const char *getRealName() const {
		return realName.c_str();
	}
	inline const char *getVirtualName() const {
		return virtName.c_str();
	}
	
	inline InsertFile &operator=( const InsertFile &from ) {
		realName = from.realName;
		virtName = from.virtName;
		
		return *this;
	}
};

template< packingMethod_t _method_ >
struct PackingPolicy {
};
// === Copy Files ===
template<>
struct PackingPolicy< kExecPack_CopyFiles > {
	// Pack
	static inline void pack( const char *stubName, const char *packName,
	const std::vector< InsertFile > &insertFiles ) {
		( void )stubName;
		
		char destName[ 260 ];
		for( size_t i = 0; i < insertFiles.size(); ++i ) {
#if defined( __STDC_WANT_SECURE_LIB__ )
# define snprintf sprintf_s
#endif
			snprintf( destName, sizeof( destName ), "%s\\%s", packName,
				insertFiles[ i ].getVirtualName() );
			destName[ sizeof( destName ) - 1 ] = '\0';

			// if there's no need to copy then don't copy
			if( !isFileNewer( insertFiles[ i ].getRealName(), destName ) ) {
				continue;
			}
			
			// copy the file over
			copyFile( insertFiles[ i ].getRealName(), destName );
		}
		
		const char *stubBase = strrchr( stubName, '\\' );
		if( !stubBase ) {
			stubBase = strrchr( stubName, '/' );
			if( !stubBase ) {
				stubBase = stubName;
			}
		}

		snprintf( destName, sizeof( destName ), "%s\\%s", packName, stubBase );
		destName[ sizeof( destName ) - 1 ] = '\0';
		
		copyFile( stubName, destName );
#if defined( __STDC_WANT_SECURE_LIB__ )
# undef snprintf
#endif
	}
	// Unpack
	static inline void unpack( const char *destName,
	const char *packName ) {
		( void )destName;
		( void )packName;
		
		// Nothing to do; files are already there
	}

private:
	// Determine whether a given file is newer than another file
	static inline BOOL isFileNewer( const char *isNewer,
	const char *isOlder ) {
		WIN32_FILE_ATTRIBUTE_DATA isNewerAttribs;
		WIN32_FILE_ATTRIBUTE_DATA isOlderAttribs;
		BOOL result;

		result = GetFileAttributesExA( isNewer, GetFileExInfoStandard,
			&isNewerAttribs );
		if( !result ) {
			throw Exception( std::string( "File doesn't exist: " ) +
				isNewer );
		}
		
		result = GetFileAttributesExA( isOlder, GetFileExInfoStandard,
			&isOlderAttribs );
		if( !result ) {
			return TRUE;
		}

		const u64_t newerTime = makeTimestampFromFiletime(
			isNewerAttribs.ftLastWriteTime );
		const u64_t olderTime = makeTimestampFromFiletime(
			isOlderAttribs.ftLastWriteTime );

		if( newerTime <= olderTime ) {
			return FALSE;
		}
		
		return TRUE;
	}
	// Construct a timestamp
	static inline u64_t makeTimestampFromFiletime( const FILETIME &time ) {
		u64_t timestamp;

		timestamp = ( u64_t )time.dwHighDateTime;
		timestamp <<= 32;
		timestamp |= ( u64_t )time.dwLowDateTime;
		
		return timestamp;
	}
	
	// Copy the file over
	static inline void copyFile( const char *source, const char *target ) {
		if( !CopyFileA( source, target, FALSE ) ) {
			std::string failure = "Failed to copy file: \"";
			failure += source;
			failure += "\" -> \"";
			failure += target;
			failure += "\"";

			throw Exception( failure );
		}
	}
};
// === Separate Archive ===
template<>
struct PackingPolicy< kExecPack_SeparateArchive > {
	// Pack
	static inline void pack( const char *stubName, const char *packName,
	const std::vector< InsertFile > &insertFiles ) {
		throw Exception( "Separate Archives are not implemented" );
	}
	// Unpack
	static inline void unpack( const char *destName,
	const char *packName ) {
		throw Exception( "Separate Archives are not implemented" );
	}
};
// === Append Executable ===
template<>
struct PackingPolicy< kExecPack_AppendExecutable > {
	// Pack
	static inline void pack( const char *stubName, const char *packName,
	const std::vector< InsertFile > &insertFiles ) {
		FILE *execFile;
		
		execFile = fopen( stubName, "rb" );
		if( !execFile ) {
			throw Exception( std::string( "Could not open: " ) + stubName );
		}
		
		fseek( execFile, 0, SEEK_END );
		const size_t fileEnd = ( size_t )ftell( execFile );
		fseek( execFile, 0, SEEK_SET );
		
		FILE *packFile;
		packFile = fopen( packName, "wb" );
		if( !packFile ) {
			fclose( execFile );
			execFile = NULL;

			throw Exception( std::string( "Could not write: " ) +
				packName );
		}

		// First write out the stub
		if( !copyToFile( execFile, packFile, fileEnd ) ) {
			fclose( packFile );
			packFile = NULL;
			
			fclose( execFile );
			execFile = NULL;

			throw Exception( std::string( "Could not copy: " ) +
				stubName );
		}

		// Done with the executable file
		fclose( execFile );
		execFile = NULL;
		
		// Write out how many files there are
		if( !writeFileData( ( u32_t )insertFiles.size(), packFile ) ) {
			fclose( packFile );
			packFile = NULL;

			throw Exception( std::string( "Could not write: " ) +
				packName );
		}
		
		// Write each file
		for( u32_t i = 0; i < ( u32_t )insertFiles.size(); ++i ) {
			const InsertFile &file = insertFiles[ i ];
			
			// Begin each file with the name (length + data)
			if( !writeString( file.getVirtualName(), packFile ) ) {
				fclose( packFile );
				packFile = NULL;

				throw Exception( std::string( "Could not write name: " ) +
					packName );
			}
			
			// Write the file now
			result_t result = writeFile( file.getRealName(), packFile );
			if( result != kResult_Success ) {
				fclose( packFile );
				packFile = NULL;

				std::string baseError = "Could not copy: ";
				
				if( result == kResult_Error_TooBig ) {
					baseError = "File is too big: ";
				}
				
				throw Exception( baseError + packName );
			}
		}
		
		// We need to know how to get back now
		writeFileData( ( u32_t )fileEnd, packFile );
		
		// File is over... Done.
		fclose( packFile );
		packFile = NULL;
	}
	// Unpack
	static inline void unpack( const char *destName,
	const char *packName ) {
		FILE *packFile;
		
		packFile = fopen( packName, "rb" );
		if( !packFile ) {
			throw Exception( std::string( "Failed to read: " ) + packName );
		}
		
		fseek( packFile, -4, SEEK_END );
		u32_t beginPack = 0;
		if( !readFileData( &beginPack, packFile ) ) {
			fclose( packFile );
			packFile = NULL;

			throw Exception( std::string( "Corrupt file: " ) + packName );
		}
		
		fseek( packFile, beginPack, SEEK_SET );
		
		u32_t numberOfFiles;
		if( !readFileData( &numberOfFiles, packFile ) ) {
			fclose( packFile );
			packFile = NULL;
			
			throw Exception( std::string( "Invalid read (count): " ) +
				packName );
		}

		std::string virtName;
		std::string realName;
		for( u32_t foundFiles = 0; foundFiles < numberOfFiles; ++foundFiles ) {
			virtName.clear();
			if( !readString( virtName, packFile ) ) {
				fclose( packFile );
				packFile = NULL;
				
				throw Exception( std::string( "Invalid read (filename): " )
					+ packName );
			}
			
			u32_t fileSize;
			if( !readFileData( &fileSize, packFile ) ) {
				fclose( packFile );
				packFile = NULL;

				throw Exception( std::string( "Invalid read (file size): " )
					+ packName );
			}
			
			realName = destName;
			realName += "\\";
			realName += virtName;
			
			FILE *dstFile;
			
			dstFile = fopen( realName.c_str(), "wb" );
			if( !dstFile ) {
				fclose( packFile );
				packFile = NULL;
				
				throw Exception( std::string( "Failed to write: " ) +
					realName );
			}
			
			if( !copyToFile( packFile, dstFile, ( size_t )fileSize ) ) {
				fclose( dstFile );
				dstFile = NULL;
				
				fclose( packFile );
				packFile = NULL;
				
				throw Exception( std::string( "Failed to write (data): " ) +
					realName );
			}
			
			fclose( dstFile );
			dstFile = NULL;
		}
		
		fclose( packFile );
		packFile = NULL;
	}

private:
	enum result_t {
		kResult_Success = 1,
		kResult_Error = 0,

		kResult_Error_TooBig = -1
	};
	
	// Copy a portion of one file to another
	static inline bool copyToFile( FILE *fromFile, FILE *toFile,
	size_t numBytes ) {
		char buf[ 8192 ];
		const size_t bufSize = sizeof( buf );

		// copy to the file in chunks
		size_t amountRead = 0;
		while( numBytes > 0 ) {
			size_t readBytes = numBytes > bufSize ? bufSize : numBytes;
			amountRead = ( size_t )fread( buf, 1, readBytes, fromFile );

			// if it's negative or zero, subtracting by one will make it
			// have a value much larger than the buffer's size (unsigned)
			if( ( amountRead - 1 ) > sizeof( buf ) ) {
				// something's wrong; we can't copy
				return false;
			}
			
			if( !fwrite( buf, amountRead, 1, toFile ) ) {
				return false;
			}

			numBytes -= amountRead;
		}
		
		return true;
	}
	
	// Read a single element from a file
	template< typename _type_ >
	static inline bool readFileData( _type_ *dst, FILE *file ) {
		if( !fread( ( void * )dst, sizeof( *dst ), 1, file ) ) {
			return false;
		}
		
		return true;
	}
	// Write a single element to a file
	template< typename _type_ >
	static inline bool writeFileData( const _type_ &src, FILE *file ) {
		if( !fwrite( ( const void * )&src, sizeof( src ), 1, file ) ) {
			return false;
		}
		
		return true;
	}
	
	// Write a string out
	static inline bool writeString( const char *src, FILE *file ) {
		u32_t length;

		length = ( u32_t )strlen( src );
		if( !fwrite( ( const void * )&length, sizeof( length ), 1,
		file ) ) {
			return false;
		}
		if( !fwrite( ( const void * )src, length, 1, file ) ) {
			return false;
		}
		
		return true;
	}
	// Read a string from a file
	static inline bool readString( std::string &dst, FILE *file ) {
		u32_t length;
		
		if( !readFileData( &length, file ) ) {
			return false;
		}
		
		char buf[ 512 ];
		const size_t bufSize = sizeof( buf );
		while( length > 0 ) {
			size_t readSize = length > bufSize - 1 ? bufSize - 1 : length;
			if( !fread( buf, readSize, 1, file ) ) {
				return false;
			}
			
			buf[ readSize ] = '\0';
			dst += buf;
			
			length -= readSize;
		}
		
		return true;
	}
	
	// Write a file out
	static inline result_t writeFile( const char *name, FILE *toFile ) {
		WIN32_FILE_ATTRIBUTE_DATA fileAttribs;
		
		BOOL result = GetFileAttributesExA( name, GetFileExInfoStandard,
			&fileAttribs );
		if( !result ) {
			return kResult_Error;
		}
		
		if( fileAttribs.nFileSizeHigh > 0 ) {
			return kResult_Error_TooBig;
		}
		
		const size_t fileSize = ( size_t )fileAttribs.nFileSizeLow;

		FILE *fromFile;
		
		fromFile = fopen( name, "rb" );
		if( !fromFile ) {
			return kResult_Error;
		}
		
		// Each file begins with the size of the file's data
		if( !writeFileData( ( u32_t )fileSize, toFile ) ) {
			fclose( fromFile );
			fromFile = NULL;
			
			return kResult_Error;
		}
		
		// Followed by the actual data of the file
		if( !copyToFile( fromFile, toFile, ( size_t )fileSize ) ) {
			fclose( fromFile );
			fromFile = NULL;
			
			return kResult_Error;
		}
		
		fclose( fromFile );
		fromFile = NULL;
		
		// Done
		return kResult_Success;
	}
};

}

class ExecutablePacker {
public:
	// constructor
	inline ExecutablePacker(): items() {
	}
	// destructor
	inline ~ExecutablePacker() {
	}

	// pack all of the files using the specified method
	template< packingMethod_t _method_ >
	inline void pack( const char *stubName, const char *packName ) {
		_Detail::PackingPolicy< _method_ >::pack( stubName, packName, items );
	}
	// unpack the files using the specified method from the given pack
	template< packingMethod_t _method_ >
	inline void unpack( const char *destName, const char *packName ) {
		_Detail::PackingPolicy< _method_ >::unpack( destName, packName );
	}
	
	// add 'realName' file to the list of files to be packed using 'virtName'
	// as the name it will be stored as internally. (This matches the name it
	// will be extracted as.)
	inline void addFile( const char *realName, const char *virtName ) {
		items.push_back( _Detail::InsertFile( realName, virtName ) );
	}

protected:
	std::vector< _Detail::InsertFile > items;
};

}

#endif
