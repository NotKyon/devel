#ifndef TASK_H
#define TASK_H

#include "../TaskShare.h"

//------------------------------------------------------------------------------
// JOB QUEUE

__TS_NS_INIT__

typedef struct token_s {
	typedef enum {
		kDispatch,
		kSignal,
		kSync
	} type_t;

	type_t type;

	union {
		struct {
			jobFunc_t fn;
			void *p;
		};
		signal_t *sig;
		signal_t *sync;
	};
} token_t;
typedef struct queue_s {
	size_t cnt, cap, cur;
	token_t *tokens;
	lock_t lock;			//used to access all of the above fields

	signal_t readySig;		//signalled when there is work in the queue
	signal_t doneSig;		//signalled when there is no work in the queue

	bool stop;				//set to true when the queue should stop working
	signal_t stoppedSig;	//signalled when the stop completes
} queue_t;

inline void InitQueue(queue_t &queue);
inline void FiniQueue(queue_t &queue);

inline token_t *AllocateQueueToken(queue_t &queue);

inline void EnqueueJob(queue_t &queue, jobFunc_t fn, void *p);
inline void EnqueueSignal(queue_t &queue, signal_t *sig);
inline void EnqueueSync(queue_t &queue, signal_t *sig);

inline void FlushQueue(queue_t &queue);
inline void FinishQueue(queue_t &queue);

inline void QueueWorkerTask(queue_t *queue);

//------------------------------------------------------------------------------
// JOB QUEUE MANAGEMENT

inline void InitQueue(queue_t &queue) {
	queue.cnt = 0;
	queue.cap = 0;
	queue.cur = 0;
	queue.tokens = (token_t *)0;

	InitLock(queue.lock);

	InitSignal(queue.readySig, 1);
	InitSignal(queue.doneSig, 0);

	queue.stop = false;
	InitSignal(queue.stoppedSig, 1);
}
inline void FiniQueue(queue_t &queue) {
	queue.stop = true;
	RaiseSignal(queue.readySig);
	SyncSignal(queue.stoppedSig);

	Lock(queue.lock);
		queue.cnt = 0;
		queue.cap = 0;
		queue.cur = 0;
		queue.tokens = (token_t *)Free((void *)queue.tokens);

		FiniSignal(queue.readySig);
		FiniSignal(queue.doneSig);

		FiniSignal(queue.stoppedSig);
	Unlock(queue.lock);

	FiniLock(queue.lock);
}

inline token_t *AllocateQueueToken(queue_t &queue) {
	static const size_t granularity = 4096/sizeof(token_t);
	token_t *p;

	if (queue.cnt + 1 >= queue.cap) {
		size_t n;

		queue.cap  = queue.cnt + 1;
		queue.cap += granularity;
		queue.cap -= queue.cap%granularity;

		n = queue.cap*sizeof(token_t);

		queue.tokens = (token_t *)Realloc((void *)queue.tokens, n);
	}

	p = &queue.tokens[queue.cnt++];
	AllocSignalSlot(queue.doneSig);

	return p;
}

inline void EnqueueJob(queue_t &queue, jobFunc_t fn, void *p) {
	token_t *tok;

	Lock(queue.lock);
		tok = AllocateQueueToken(queue);

		tok->type = token_t::kDispatch;

		tok->fn   = fn;
		tok->p    = p;
	Unlock(queue.lock);
}
inline void EnqueueSignal(queue_t &queue, signal_t *sig) {
	token_t *tok;

	Lock(queue.lock);
		tok = AllocateQueueToken(queue);

		tok->type = token_t::kSignal;

		tok->sig  = sig;
	Unlock(queue.lock);
}
inline void EnqueueSync(queue_t &queue, signal_t *sig) {
	token_t *tok;

	Lock(queue.lock);
		tok = AllocateQueueToken(queue);

		tok->type = token_t::kSync;

		tok->sync = sig;
	Unlock(queue.lock);
}

inline void FlushQueue(queue_t &queue) {
	Lock(queue.lock);
		if (!queue.cnt) {
			Unlock(queue.lock);
			return;
		}

		ResetSignal(queue.doneSig);
	Unlock(queue.lock);

	RaiseSignal(queue.readySig);
}
inline void FinishQueue(queue_t &queue) {
	FlushQueue(queue);

	SyncSignal(queue.doneSig);
}

inline void QueueWorkerTask(queue_t *queue) {
	token_t *p;

	while(!queue->stop) {
		SyncSignal(queue->readySig);

		Lock(queue->lock);
			p = (queue->cur < queue->cnt) ? &queue->tokens[queue->cur++] : 0;
		Unlock(queue->lock);

		if (!p) {
			Relinquish();
			continue;
		}

		switch(p->type) {
		case token_t::kDispatch:
			p->fn(p->p);
			RaiseSignal(queue->doneSig);
			break;
		case token_t::kSignal:
			RaiseSignal(*p->sig);
			RaiseSignal(queue->doneSig);
			break;
		case token_t::kSync:
			while(!TrySyncSignal(*p->sync) && !queue->stop)
				Relinquish();
			RaiseSignal(queue->doneSig);
			break;
		}
	}

	ResetSignal(queue->doneSig, 1);
	RaiseSignal(queue->stoppedSig);
	RaiseSignal(queue->doneSig);
}

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
