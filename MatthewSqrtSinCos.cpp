#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#undef PI
#define PI 3.1415926535897932384626433832795028841971693993751058209

double f1( double input )
{
	double N = input;
	double b = N;
	double a = 1;
	double P = 2*( a + b );
	double L = ( a + b )/2;
	double base = ( b - 1 )/2;
	double cosB = base/L;
	double sinA = base/L;
	double cosA = cos( asin( sinA ) );
	double rd = L*cosA;

	( void )P;
	( void )cosB;
	return rd;
}
double f2( double input )
{
	// Matthew's Notes
	double N = input;
	double b = N;
	double a = 1;
	double P = 2*( a + b );
	double L = ( a + b )/2;
	double base = ( b - 1 )/2;
	//double cosB = base/L;
	//double sinA = base/L;
	//double cosA = cos( asin( sinA ) );
	//double rd = L*cosA;

	double cosB = base/L;
	double sinA = base/L;
	//double cosA = ( b - 1 + PI/2 )/( a + b ); // ← VERY CLOSE (between -0.05 and 0.22~ off)
	//double cosA = ( b - PI/4 + PI/2 )/( a + b ); // ← VERY CLOSE!!! (between -0.19~ and 0.10~ off)
	double cosA = ( cosB*cosB )/( L + PI/2 );
	double rd = L*cosA;

	( void )P;
	( void )cosB;
	( void )sinA;
	( void )rd;
	//return rd;
	return ( a*b )/P;
}

void Test( double input )
{
	const double gresult = f1( input );
	const double cresult = f2( input );
	const double difference = gresult - cresult;
	const bool correct = fabs( gresult - cresult ) < 0.0001;

	printf( "%s f(%.1f) = %.6f (%.6f; %.6f)\n", correct ? "OK" : "KO", input, cresult, gresult, difference );
}

int main()
{
	Test( 0.0 );
	Test( 0.5 );
	Test( 1.0 );
	Test( 1.5 );
	Test( 2.0 );
	Test( 2.5 );
	Test( 3.0 );
	Test( PI );
	Test( 4.0 );

	return EXIT_SUCCESS;
}
