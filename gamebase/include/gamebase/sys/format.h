#ifndef GB_SYS_FORMAT_H
#define GB_SYS_FORMAT_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Formats (More later) */
typedef enum {
	kGBFmt_Unknown,

	kGBFmt_RGBA32,
	kGBFmt_RGBA32F,
	kGBFmt_RGBA32UI,
	kGBFmt_RGBA32SI,
	kGBFmt_RGB32,
	kGBFmt_RGB32F,
	kGBFmt_RGB32UI,
	kGBFmt_RGB32SI,
	kGBFmt_RGBA16,
	kGBFmt_RGBA16F,
	kGBFmt_RGBA16UN,
	kGBFmt_RGBA16UI,
	kGBFmt_RGBA16SN,
	kGBFmt_RGBA16SI,
	kGBFmt_RG32,
	kGBFmt_RG32F,
	kGBFmt_RG32UI,
	kGBFmt_RG32SI,
	kGBFmt_R32G8X24,
	kGBFmt_D32F_S8X24UI,
	kGBFmt_R32F_X8X24,
	kGBFmt_X32_G8X24UI,
	kGBFmt_RGB10A2,
	kGBFmt_RGB10A2UN,
	kGBFmt_RGB10A2UI,
	kGBFmt_RG11B10F,
	kGBFmt_RGBA8,
	kGBFmt_RGBA8UN,
	kGBFmt_RGBA8UN_sRGB,
	kGBFmt_RGBA8UI,
	kGBFmt_RGBA8SN,
	kGBFmt_RGBA8SI,
	kGBFmt_RG16,
	kGBFmt_RG16F,
	kGBFmt_RG16UN,
	kGBFmt_RG16UI,
	kGBFmt_RG16SN,
	kGBFmt_RG16SI,
	kGBFmt_R32,
	kGBFmt_D32F,
	kGBFmt_R32F,
	kGBFmt_R32UI,
	kGBFmt_R32SI,
	kGBFmt_R24G8,
	kGBFmt_D24UN_S8UI,
	kGBFmt_R24UN_X8,
	kGBFmt_X24_G8UI,
	kGBFmt_RG8,
	kGBFmt_RG8UN,
	kGBFmt_RG8UI,
	kGBFmt_RG8SN,
	kGBFmt_RG8SI,
	kGBFmt_R16,
	kGBFmt_R16F,
	kGBFmt_D16UN,
	kGBFmt_R16UN,
	kGBFmt_R16UI,
	kGBFmt_R16SN,
	kGBFmt_R16SI,
	kGBFmt_R8,
	kGBFmt_R8UN,
	kGBFmt_R8UI,
	kGBFmt_R8SN,
	kGBFmt_R8SI,
	kGBFmt_A8UN,
	kGBFmt_R1UN,
	kGBFmt_RGB9E5_SharedExp,
	kGBFmt_RG8_B8G8UN,
	kGBFmt_GR8_G8B8UN,
	kGBFmt_BC1,
	kGBFmt_BC1UN,
	kGBFmt_BC1UN_sRGB,
	kGBFmt_BC2,
	kGBFmt_BC2UN,
	kGBFmt_BC2UN_sRGB,
	kGBFmt_BC3,
	kGBFmt_BC3UN,
	kGBFmt_BC3UN_sRGB,
	kGBFmt_BC4,
	kGBFmt_BC4UN,
	kGBFmt_BC4SN,
	kGBFmt_BC5,
	kGBFmt_BC5UN,
	kGBFmt_BC5SN,
	kGBFmt_B5G6R5UN,
	kGBFmt_BGR5A1UN,
	kGBFmt_BGRA8UN,
	kGBFmt_BGRX8UN,
	kGBFmt_RGB10_XRBias_A2UN,
	kGBFmt_BGR8A8,
	kGBFmt_BGR8A8UN_sRGB,
	kGBFmt_BGR8X8,
	kGBFmt_BGR8X8UN_sRGB,
	kGBFmt_BC6H,
	kGBFmt_BC6H_UF16,
	kGBFmt_BC6H_SF16,
	kGBFmt_BC7,
	kGBFmt_BC7UN,
	kGBFmt_BC7UN_sRGB,
	kGBFmt_AYUV,
	kGBFmt_Y410,
	kGBFmt_Y416,
	kGBFmt_NV12,
	kGBFmt_P010,
	kGBFmt_P016,
	kGBFmt_420_Opaque,
	kGBFmt_YUY2,
	kGBFmt_Y210,
	kGBFmt_Y216,
	kGBFmt_NV11,
	kGBFmt_AI44,
	kGBFmt_IA44,
	kGBFmt_P8,
	kGBFmt_A8P8,
	kGBFmt_BGR4A4UN
} gb_format_t;

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
