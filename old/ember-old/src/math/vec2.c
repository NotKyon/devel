#include <ember/math.h>
#include <math.h>

float vec2_x( const Vec2 *src )
{
	return src->x;
}
float vec2_y( const Vec2 *src )
{
	return src->y;
}

void vec2_zero( Vec2 *dst )
{
	dst->x = dst->y = 0;
}
void vec2_identity( Vec2 *dst )
{
	dst->x = dst->y = 1;
}

void vec2_set( Vec2 *dst, float x, float y )
{
	dst->x = x;
	dst->y = y;
}

void vec2_copy( Vec2 *dst, const Vec2 *src )
{
	dst->x = src->x;
	dst->y = src->y;
}

void vec2_add( Vec2 *dst, const Vec2 *a, const Vec2 *b )
{
	dst->x = a->x + b->x;
	dst->y = a->y + b->y;
}
void vec2_subtract( Vec2 *dst, const Vec2 *a, const Vec2 *b )
{
	dst->x = a->x - b->x;
	dst->y = a->y - b->y;
}
void vec2_multiply( Vec2 *dst, const Vec2 *a, const Vec2 *b )
{
	dst->x = a->x * b->x;
	dst->y = a->y * b->y;
}
void vec2_divide( Vec2 *dst, const Vec2 *a, const Vec2 *b )
{
	dst->x = a->x / b->x;
	dst->y = a->y / b->y;
}
void vec2_scale( Vec2 *dst, const Vec2 *a, float b )
{
	dst->x = a->x * b;
	dst->y = a->y * b;
}

float vec2_dot( const Vec2 *a, const Vec2 *b )
{
	return a->x * b->x + a->y * b->y;
}

void vec2_lerp_ex( Vec2 *dst, const Vec2 *a, const Vec2 *b, const Vec2 *w )
{
	dst->x = a->x + w->x * ( b->x - a->x );
	dst->y = a->y + w->y * ( b->y - a->y );
}
void vec2_lerp( Vec2 *dst, const Vec2 *a, const Vec2 *b, float w )
{
	dst->x = a->x + w * ( b->x - a->x );
	dst->y = a->y + w * ( b->y - a->y );
}

float vec2_length( const Vec2 *a )
{
	return sqrtf( a->x * a->x + a->y * a->y );
}
float vec2_length_fast( const Vec2 *a )
{
	return sqrt_fast( a->x * a->x + a->y * a->y );
}

void vec2_normalize( Vec2 *a )
{
	float r;

	r = 1 / sqrtf( a->x * a->x + a->y * a->y );
	a->x *= r;
	a->y *= r;
}
void vec2_normalize_fast( Vec2 *a )
{
	float r;

	r = 1 / sqrt_fast( a->x * a->x + a->y * a->y );
	a->x *= r;
	a->y *= r;
}

float vec2_distance( const Vec2 *a, const Vec2 *b )
{
	float x, y;

	x = a->x - b->x;
	y = a->y - b->y;
	return sqrtf( x * x + y * y );
}
float vec2_distance_fast( const Vec2 *a, const Vec2 *b )
{
	float x, y;

	x = a->x - b->x;
	y = a->y - b->y;
	return sqrt_fast( x * x + y * y );
}

float vec2_point( const Vec3 *a, const Vec3 *b )
{
	return -atan2( a->x - b->x, a->y - b->y );
}

void vec2_transform_ex( Vec2 *dst, const Vec2 *a, const Matrix *b )
{
	dst->x = a->x * b->rm._11 + a->y * b->rm._21 + b->rm._31 + b->rm._41;
	dst->y = a->x * b->rm._12 + a->y * b->rm._22 + b->rm._32 + b->rm._42;
}
void vec2_transform( Vec2 *dst, float x, float y, const Matrix *b )
{
	dst->x = x * b->rm._11 + y * b->rm._21 + b->rm._31 + b->rm._41;
	dst->y = x * b->rm._12 + y * b->rm._22 + b->rm._32 + b->rm._42;
}
