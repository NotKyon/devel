#define GB_INIT 1
#include <gamebase.h>

//
//	gb_string_t is mostly useful in C where std::string and similar constructs
//	are not available. This example shows how you would typically use one.
//

void GBMain() {
	gb_string_t str;

	// initialize the string
	gbInitString(&str);

	// set the data of the string
	gbSetString(&str, "This string is");

	// append more data to the string
	gbPushChar(&str, ' ');
	gbPushString(&str, "a test");
	gbPushChar(&str, '!');

	// get information about the string
	gbPrint(gbF("string buffer: %s", gbStringBuffer(&str)));
	gbPrint(gbF("string length: %u", (unsigned int)gbStringLength(&str)));
	gbPrint(gbF("string capacity: %u", (unsigned int)gbStringCapacity(&str)));

	// deinitialize the string
	gbFiniString(&str);
}
