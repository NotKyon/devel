#ifndef MATH_H
#define MATH_H

#ifdef VEC_DOUBLE
typedef double vec_t;
#else
typedef float vec_t;
#endif

typedef vec_t vec2_t[2], vec3_t[3], vec4_t[4];
typedef struct mat3_s {
	vec_t xx, yx, zx,
	      xy, yy, zy,
	      xz, yz, zz;
} mat3_t;
typedef struct mat4_s {
	vec_t xx, yx, zx, wx,
	      xy, yy, zy, wy,
	      xz, yz, zz, wz,
	      xw, yw, zw, ww;
} mat4_t;

typedef vec4_t color_t;


vec_t *Vec2(vec_t x, vec_t y);
vec_t *Vec3(vec_t x, vec_t y, vec_t z);
vec_t *Vec4(vec_t x, vec_t y, vec_t z, vec_t w);


#define v2_Dot(a,b) (a[0]*b[0] + a[1]*b[1])
#define v2_LengthSq(a) v2_Dot((a),(a))
#define v2_Length(a) sqrtf(v2_LengthSq(a))

#define v3_Dot(a,b) (a[0]*b[0] + a[1]*b[1] + a[2]*b[2])
#define v3_LengthSq(a) v3_Dot((a),(a))
#define v3_Length(a) sqrtf(v3_LengthSq(a))

#define v4_Dot(a,b) (a[0]*b[0] + a[1]*b[1] + a[2]*b[2] + a[3]*b[3])
#define v4_LengthSq(a) v4_Dot((a),(a))
#define v4_Length(a) sqrtf(v4_LengthSq(a))

#ifndef INLINE
# define INLINE static __inline
#endif

INLINE vec_t *v2_Set(vec2_t dst, vec_t x, vec_t y) {
	dst[0] = x;
	dst[1] = y;

	return dst;
}
INLINE vec_t *v3_Set(vec3_t dst, vec_t x, vec_t y, vec_t z) {
	dst[0] = x;
	dst[1] = y;
	dst[2] = z;

	return dst;
}
INLINE vec_t *v4_Set(vec4_t dst, vec_t x, vec_t y, vec_t z, vec_t w) {
	dst[0] = x;
	dst[1] = y;
	dst[2] = z;
	dst[3] = w;

	return dst;
}

INLINE vec_t *v2_ScreenToProj(vec2_t dst, const vec2_t src, const vec2_t size) {
	dst[0] = -1.0f + ((vec_t)(src[0]*2))/size[0];
	dst[1] =  1.0f - ((vec_t)(src[1]*2))/size[1];

	return dst;
}


INLINE vec_t *v2_Add(vec2_t dst, const vec2_t a, const vec2_t b) {
	dst[0] = a[0] + b[0];
	dst[1] = a[1] + b[1];

	return dst;
}
INLINE vec_t *v2_Sub(vec2_t dst, const vec2_t a, const vec2_t b) {
	dst[0] = a[0] - b[0];
	dst[1] = a[1] - b[1];

	return dst;
}
INLINE vec_t *v2_Mul(vec2_t dst, const vec2_t a, const vec2_t b) {
	dst[0] = a[0] * b[0];
	dst[1] = a[1] * b[1];

	return dst;
}
INLINE vec_t *v2_Muls(vec2_t dst, const vec2_t a, vec_t b) {
	dst[0] = a[0] * b;
	dst[1] = a[1] * b;

	return dst;
}
INLINE vec_t *v2_Div(vec2_t dst, const vec2_t a, const vec2_t b) {
	dst[0] = a[0] / b[0];
	dst[1] = a[1] / b[1];

	return dst;
}

INLINE vec_t *v3_Add(vec3_t dst, const vec3_t a, const vec3_t b) {
	dst[0] = a[0] + b[0];
	dst[1] = a[1] + b[1];
	dst[2] = a[2] + b[2];

	return dst;
}
INLINE vec_t *v3_Sub(vec3_t dst, const vec3_t a, const vec3_t b) {
	dst[0] = a[0] - b[0];
	dst[1] = a[1] - b[1];
	dst[2] = a[2] - b[2];

	return dst;
}
INLINE vec_t *v3_Mul(vec3_t dst, const vec3_t a, const vec3_t b) {
	dst[0] = a[0] * b[0];
	dst[1] = a[1] * b[1];
	dst[2] = a[2] * b[2];

	return dst;
}
INLINE vec_t *v3_Muls(vec3_t dst, const vec3_t a, vec_t b) {
	dst[0] = a[0] * b;
	dst[1] = a[1] * b;
	dst[2] = a[2] * b;

	return dst;
}
INLINE vec_t *v3_Div(vec3_t dst, const vec3_t a, const vec3_t b) {
	dst[0] = a[0] / b[0];
	dst[1] = a[1] / b[1];
	dst[2] = a[2] / b[2];

	return dst;
}

/*
INLINE vec_t *v3_Cross(vec3_t dst, const vec3_t a, const vec3_t b) {
	dst[0] = a[1]*b[2] - a[2]*b[1];
	dst[1] = a[2]*b[0] - a[0]*b[2];
	dst[2] = a[0]*b[1] - a[1]*b[0];

	return dst;
}
*/
#define v3_Cross_(c,a,b) (c[0]=a[1]*b[2]-a[2]*b[1],\
                          c[1]=a[2]*b[0]-a[0]*b[2],\
                          c[2]=a[0]*b[1]-a[1]*b[0],c)
#define v3_Cross(c,a,b) v3_Cross_((c),(a),(b))


INLINE vec_t *v4_Add(vec4_t dst, const vec4_t a, const vec4_t b) {
	dst[0] = a[0] + b[0];
	dst[1] = a[1] + b[1];
	dst[2] = a[2] + b[2];
	dst[3] = a[3] + b[3];

	return dst;
}
INLINE vec_t *v4_Sub(vec4_t dst, const vec4_t a, const vec4_t b) {
	dst[0] = a[0] - b[0];
	dst[1] = a[1] - b[1];
	dst[2] = a[2] - b[2];
	dst[3] = a[3] - b[3];

	return dst;
}
INLINE vec_t *v4_Mul(vec4_t dst, const vec4_t a, const vec4_t b) {
	dst[0] = a[0] * b[0];
	dst[1] = a[1] * b[1];
	dst[2] = a[2] * b[2];
	dst[3] = a[3] * b[3];

	return dst;
}
INLINE vec_t *v4_Muls(vec4_t dst, const vec4_t a, vec_t b) {
	dst[0] = a[0] * b;
	dst[1] = a[1] * b;
	dst[2] = a[2] * b;
	dst[3] = a[3] * b;

	return dst;
}
INLINE vec_t *v4_Div(vec4_t dst, const vec4_t a, const vec4_t b) {
	dst[0] = a[0] / b[0];
	dst[1] = a[1] / b[1];
	dst[2] = a[2] / b[2];
	dst[3] = a[3] / b[3];

	return dst;
}

#endif
