#include <xs/memory.h>
#include <xs/assert.h>
#include <xs/logging.h>
#include <string.h>
#include <stdlib.h>

typedef union { void *p; size_t n; } ptradr_t;

void *XsAlloc(xs_size_t n, xs_size_t alignment) {
	ptradr_t ptr;
	void *p;

	if (!n)
		return (void *)0;

	if (!alignment)
		alignment = XS_DEFAULT_ALIGNMENT;

	n += n%alignment;

	if (!(ptr.p = p = malloc(n + 16 + sizeof(void *))))
		return (void *)0;

	ptr.n -= ptr.n%alignment;
	ptr.n += alignment - sizeof(void *);

	*(void **)ptr.p = p;

	ptr.n += sizeof(void *);
	return ptr.p;
}
void *XsRealloc(void *p, xs_size_t n, xs_size_t alignment) {
	ptradr_t ptr;

	if (!p)
		return XsAlloc(n, alignment);

	if (!alignment)
		alignment = XS_DEFAULT_ALIGNMENT;

	ptr.p = p;

	XS_ASSERT(ptr.n > alignment + sizeof(void *));
	XS_ASSERT(ptr.n%alignment == 0);

	ptr.n -= sizeof(void *);

	XS_ASSERT(*(void **)ptr.p != (void *)0);

	n += n%alignment;

	if (!(ptr.p = p = realloc(*(void **)ptr.p, n + alignment + sizeof(void *))))
		return (void *)0;

	ptr.n -= ptr.n%alignment;
	ptr.n += alignment - sizeof(void *);

	*(void **)ptr.p = p;

	ptr.n += sizeof(void *);
	return ptr.p;
}
void XsFree(void *p, xs_size_t alignment) {
	ptradr_t ptr;

	if (!p)
		return;

	if (!alignment)
		alignment = XS_DEFAULT_ALIGNMENT;

	ptr.p = p;

	XS_ASSERT(ptr.n > alignment + sizeof(void *));
	XS_ASSERT(ptr.n%alignment == 0);

	ptr.n -= sizeof(void *);

	XS_ASSERT(*(void **)ptr.p != (void *)0);

	free(*(void **)ptr.p);
}

void *XsMemory(void *p, xs_size_t n) {
#if 0
	if (!p && n>0) {
		if (!(p = XsAlloc(n, XS_DEFAULT_ALIGNMENT))) {
			XsLogMessage(0, 0, 0, kXsLog_Error, "Failed to allocate memory.");
			exit(EXIT_FAILURE);
		}
	} else if(p && n>0) {
		if (!(p = XsRealloc(p, n, XS_DEFAULT_ALIGNMENT))) {
			XsLogMessage(0, 0, 0, kXsLog_Error, "Failed to reallocate memory.");
			exit(EXIT_FAILURE);
		}
	} else if(p) /*n==0*/ {
		XsFree(p, XS_DEFAULT_ALIGNMENT);
		p = (void *)0;
	}

	return p;
#else
	if (!p && n>0) {
		if (!(p = malloc(n))) {
			XsLogMessage(0, 0, 0, kXsLog_Error, "Failed to allocate memory.");
			exit(EXIT_FAILURE);
		}
	} else if(p && n>0) {
		if (!(p = realloc(p, n))) {
			XsLogMessage(0, 0, 0, kXsLog_Error, "Failed to reallocate memory.");
			exit(EXIT_FAILURE);
		}
	} else if(p) {
		free(p);
		p = (void *)0;
	}

	return p;
#endif
}
