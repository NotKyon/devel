#include "memory.h"
#include "system.h"
#include <stdlib.h>
#include <string.h>

void *mem_Alloc(size_t n) {
	void *p;

	ASSERT(n != 0);

	p = malloc(n);
	if (!p) {
		sys_Error("Failed to allocate memory");
		exit(EXIT_FAILURE);
	}

	return p;
}
void *mem_AllocZero(size_t n) {
	void *p;

	p = mem_Alloc(n);
	memset(p, 0, n);

	return p;
}
void *mem_Realloc(void *p, size_t n) {
	if (!p)
		return mem_Alloc(n);

	p = realloc(p, n);
	if (!p) {
		sys_Error("Failed to reallocate memory");
		exit(EXIT_FAILURE);
	}

	return p;
}
void *mem_Free(void *p) {
	if (p != (void *)0)
		free(p);

	return (void *)0;
}
