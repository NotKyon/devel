#ifndef EMBER_RENDERQUEUE_H
#define EMBER_RENDERQUEUE_H

#include "config.h"
#include "math.h"
#include "opengl.h"
#include "transform.h"
#include "material.h"

/*
 * Specifies the rendering mode. Apply to render queues.
 */
typedef enum {
	/*
   * Sort twice, first by distance (front to back), then by state. On the first
   * pass, objects are rendered depth-only. On the second pass, they're rendered
   * color-only.
   */
	ERenderQueueMode_FillPerformance,
	/*
   * Sort once, by state.
   */
	ERenderQueueMode_StatePerformance,
	/*
   * Sort by distance, back to front. This is useful for transparent objects.
   */
	ERenderQueueMode_BackToFront,
	/*
   * Sort by distance, front to back.
   */
	ERenderQueueMode_FrontToBack
} ERenderQueueMode;

/*
 * RENDER QUEUE
 * Stores a list of render items to be sorted based on specific sorting
 * techniques specified by the rendering mode. There is a count of items within
 * the queue, and a queue capacity. The queue capacity will be resized as
 * necessary.
 */
struct stRenderQueue {
	/* sorting/rendering mode */
	ERenderQueueMode mode;

	/* number of items in the buffer, number of items reserved, items */
	size_t nitems, capacity;
	struct stRenderItem **item;

	/* list */
	struct stRenderQueue *prev, *next;
};

/*
 * RENDER ITEM
 * Specifies an item to be drawn.
 */
struct stRenderItem {
	/* transformation */
	const struct stTransform *transform;

	/* render info */
	const struct stMaterial *material;

	/* draw data */
	const struct stDrawBuffer *buffer;
	const struct stDrawInfo *draw;
};

/*
 * DRAW INFO
 * Information about how to perform the draw call.
 */
struct stDrawInfo {
	struct stDrawBuffer *vbuffer, *ibuffer;

	GLenum mode;

	GLsizei count;
	GLsizei index_offset;
	GLsizei base_vertex;

	struct stDrawInfo *i_prev, *i_next;
};

/*
 * DRAW BUFFER
 * Graphics resource for drawing. Multiple items are combined into a buffer when
 * vertex shaders are available and the base vertex extension is also available.
 * This allows for animation of various geometries on a single buffer, thus
 * improving speed.
 */
struct stDrawBuffer {
	/* capacity of buffer, amount used (in bytes) */
	GLsizei capacity, length;

	/* used chunks */
	struct stDrawChunk *c_head, *c_tail;
};

/*
 * DRAW CHUNK
 * Represents an unused (read: available) portion of a draw buffer.
 */
struct stDrawChunk {
	/* base element, number of elements following */
	GLsizei base, count;

	/* list */
	struct stDrawChunk *c_prev, *c_next;
};

#endif
