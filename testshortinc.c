#include <stdio.h>
#include <stdlib.h>

int main() {
    unsigned char buf[] = { 'A', 'B', 'c', 'd', 'E', 'F', 'g', 'h', '\0' };
    unsigned char *p;

    p = &buf[ 0 ];
    printf("%c\n", (char)*p++);
    printf("%c\n", (char)*p++);
    printf("%i\n", *((short *)p)++);
    printf("%c\n", (char)*p++);

    return EXIT_SUCCESS;
}
