
x86call.o:     file format elf32-i386


Disassembly of section .text:

00000000 <get_sum>:
#include <stdio.h>

int get_sum(int x, int y) {
   0:	55                   	push   ebp
   1:	89 e5                	mov    ebp,esp
   3:	83 ec 18             	sub    esp,0x18
  printf("get_sum(%i, %i)\n", x, y);fflush(stdout);
   6:	b8 00 00 00 00       	mov    eax,0x0
   b:	8b 55 0c             	mov    edx,DWORD PTR [ebp+0xc]
   e:	89 54 24 08          	mov    DWORD PTR [esp+0x8],edx
  12:	8b 55 08             	mov    edx,DWORD PTR [ebp+0x8]
  15:	89 54 24 04          	mov    DWORD PTR [esp+0x4],edx
  19:	89 04 24             	mov    DWORD PTR [esp],eax
  1c:	e8 fc ff ff ff       	call   1d <get_sum+0x1d>
  21:	a1 00 00 00 00       	mov    eax,ds:0x0
  26:	89 04 24             	mov    DWORD PTR [esp],eax
  29:	e8 fc ff ff ff       	call   2a <get_sum+0x2a>
  return x + y;
  2e:	8b 45 0c             	mov    eax,DWORD PTR [ebp+0xc]
  31:	8b 55 08             	mov    edx,DWORD PTR [ebp+0x8]
  34:	01 d0                	add    eax,edx
}
  36:	c9                   	leave  
  37:	c3                   	ret    

00000038 <average>:
int average(int a, int b, int c) {
  38:	55                   	push   ebp
  39:	89 e5                	mov    ebp,esp
  3b:	83 ec 18             	sub    esp,0x18
  printf("average(%i, %i, %i)\n", a, b, c);fflush(stdout);
  3e:	b8 11 00 00 00       	mov    eax,0x11
  43:	8b 55 10             	mov    edx,DWORD PTR [ebp+0x10]
  46:	89 54 24 0c          	mov    DWORD PTR [esp+0xc],edx
  4a:	8b 55 0c             	mov    edx,DWORD PTR [ebp+0xc]
  4d:	89 54 24 08          	mov    DWORD PTR [esp+0x8],edx
  51:	8b 55 08             	mov    edx,DWORD PTR [ebp+0x8]
  54:	89 54 24 04          	mov    DWORD PTR [esp+0x4],edx
  58:	89 04 24             	mov    DWORD PTR [esp],eax
  5b:	e8 fc ff ff ff       	call   5c <average+0x24>
  60:	a1 00 00 00 00       	mov    eax,ds:0x0
  65:	89 04 24             	mov    DWORD PTR [esp],eax
  68:	e8 fc ff ff ff       	call   69 <average+0x31>
  return (a + b + c)/3;
  6d:	8b 45 0c             	mov    eax,DWORD PTR [ebp+0xc]
  70:	8b 55 08             	mov    edx,DWORD PTR [ebp+0x8]
  73:	01 d0                	add    eax,edx
  75:	89 c1                	mov    ecx,eax
  77:	03 4d 10             	add    ecx,DWORD PTR [ebp+0x10]
  7a:	ba 56 55 55 55       	mov    edx,0x55555556
  7f:	89 c8                	mov    eax,ecx
  81:	f7 ea                	imul   edx
  83:	89 c8                	mov    eax,ecx
  85:	c1 f8 1f             	sar    eax,0x1f
  88:	89 d1                	mov    ecx,edx
  8a:	29 c1                	sub    ecx,eax
  8c:	89 c8                	mov    eax,ecx
}
  8e:	c9                   	leave  
  8f:	c3                   	ret    

00000090 <x86_call>:

void *x86_call(void(*fn)(), size_t n, void **p) {
  90:	55                   	push   ebp
  91:	89 e5                	mov    ebp,esp
  93:	53                   	push   ebx
  94:	83 ec 18             	sub    esp,0x18
#if 1
  size_t esp, i;
  void *ret;

  asm("movl %%esp, %0" : "=r" (esp));
  97:	89 e3                	mov    ebx,esp
  99:	89 5d f4             	mov    DWORD PTR [ebp-0xc],ebx

  for(i=1; i<=n; i++)
  9c:	c7 45 f0 01 00 00 00 	mov    DWORD PTR [ebp-0x10],0x1
  a3:	eb 19                	jmp    be <x86_call+0x2e>
    asm("pushl %0\n" :: "r" (p[n - i]));
  a5:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10]
  a8:	8b 55 0c             	mov    edx,DWORD PTR [ebp+0xc]
  ab:	89 d1                	mov    ecx,edx
  ad:	29 c1                	sub    ecx,eax
  af:	89 c8                	mov    eax,ecx
  b1:	c1 e0 02             	shl    eax,0x2
  b4:	03 45 10             	add    eax,DWORD PTR [ebp+0x10]
  b7:	8b 00                	mov    eax,DWORD PTR [eax]
  b9:	50                   	push   eax
  size_t esp, i;
  void *ret;

  asm("movl %%esp, %0" : "=r" (esp));

  for(i=1; i<=n; i++)
  ba:	83 45 f0 01          	add    DWORD PTR [ebp-0x10],0x1
  be:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10]
  c1:	3b 45 0c             	cmp    eax,DWORD PTR [ebp+0xc]
  c4:	76 df                	jbe    a5 <x86_call+0x15>
    asm("pushl %0\n" :: "r" (p[n - i]));

  asm("call *%1\n\tmovl %2, %%esp" : "=a" (ret) : "r" (fn), "b" (esp));
  c6:	8b 45 08             	mov    eax,DWORD PTR [ebp+0x8]
  c9:	89 45 e4             	mov    DWORD PTR [ebp-0x1c],eax
  cc:	8b 55 f4             	mov    edx,DWORD PTR [ebp-0xc]
  cf:	8b 45 e4             	mov    eax,DWORD PTR [ebp-0x1c]
  d2:	89 d3                	mov    ebx,edx
  d4:	ff d0                	call   eax
  d6:	89 dc                	mov    esp,ebx
  d8:	89 45 e8             	mov    DWORD PTR [ebp-0x18],eax
  db:	8b 45 e8             	mov    eax,DWORD PTR [ebp-0x18]
  de:	89 45 f8             	mov    DWORD PTR [ebp-0x8],eax
      [p] "r" (p),
      [f] "r" (fn)
  );
#endif

  return ret;
  e1:	8b 45 f8             	mov    eax,DWORD PTR [ebp-0x8]
}
  e4:	83 c4 18             	add    esp,0x18
  e7:	5b                   	pop    ebx
  e8:	5d                   	pop    ebp
  e9:	c3                   	ret    

000000ea <main>:

int main() {
  ea:	55                   	push   ebp
  eb:	89 e5                	mov    ebp,esp
  ed:	83 e4 f0             	and    esp,0xfffffff0
  f0:	83 ec 30             	sub    esp,0x30
  union { void *p; int i; } r1, r2, a[3];

  r1.p = (void *)0;
  f3:	c7 44 24 28 00 00 00 	mov    DWORD PTR [esp+0x28],0x0
  fa:	00 
  r2.p = (void *)0;
  fb:	c7 44 24 2c 00 00 00 	mov    DWORD PTR [esp+0x2c],0x0
 102:	00 

  a[0].p = (void *)0;
 103:	c7 44 24 1c 00 00 00 	mov    DWORD PTR [esp+0x1c],0x0
 10a:	00 
  a[1].p = (void *)0;
 10b:	c7 44 24 20 00 00 00 	mov    DWORD PTR [esp+0x20],0x0
 112:	00 
  a[2].p = (void *)0;
 113:	c7 44 24 24 00 00 00 	mov    DWORD PTR [esp+0x24],0x0
 11a:	00 

  a[0].i = 3;
 11b:	c7 44 24 1c 03 00 00 	mov    DWORD PTR [esp+0x1c],0x3
 122:	00 
  a[1].i = 4;
 123:	c7 44 24 20 04 00 00 	mov    DWORD PTR [esp+0x20],0x4
 12a:	00 
  a[2].i = 5;
 12b:	c7 44 24 24 05 00 00 	mov    DWORD PTR [esp+0x24],0x5
 132:	00 

  printf("First call...\n");fflush(stdout);
 133:	c7 04 24 26 00 00 00 	mov    DWORD PTR [esp],0x26
 13a:	e8 fc ff ff ff       	call   13b <main+0x51>
 13f:	a1 00 00 00 00       	mov    eax,ds:0x0
 144:	89 04 24             	mov    DWORD PTR [esp],eax
 147:	e8 fc ff ff ff       	call   148 <main+0x5e>
  r1.p = x86_call((void(*)())get_sum, 2, &a[0].p);
 14c:	b8 00 00 00 00       	mov    eax,0x0
 151:	8d 54 24 1c          	lea    edx,[esp+0x1c]
 155:	89 54 24 08          	mov    DWORD PTR [esp+0x8],edx
 159:	c7 44 24 04 02 00 00 	mov    DWORD PTR [esp+0x4],0x2
 160:	00 
 161:	89 04 24             	mov    DWORD PTR [esp],eax
 164:	e8 fc ff ff ff       	call   165 <main+0x7b>
 169:	89 44 24 28          	mov    DWORD PTR [esp+0x28],eax
  printf("Result: %i\n", r1.i);fflush(stdout);
 16d:	8b 54 24 28          	mov    edx,DWORD PTR [esp+0x28]
 171:	b8 34 00 00 00       	mov    eax,0x34
 176:	89 54 24 04          	mov    DWORD PTR [esp+0x4],edx
 17a:	89 04 24             	mov    DWORD PTR [esp],eax
 17d:	e8 fc ff ff ff       	call   17e <main+0x94>
 182:	a1 00 00 00 00       	mov    eax,ds:0x0
 187:	89 04 24             	mov    DWORD PTR [esp],eax
 18a:	e8 fc ff ff ff       	call   18b <main+0xa1>

  printf("Second call...\n");fflush(stdout);
 18f:	c7 04 24 40 00 00 00 	mov    DWORD PTR [esp],0x40
 196:	e8 fc ff ff ff       	call   197 <main+0xad>
 19b:	a1 00 00 00 00       	mov    eax,ds:0x0
 1a0:	89 04 24             	mov    DWORD PTR [esp],eax
 1a3:	e8 fc ff ff ff       	call   1a4 <main+0xba>
  r2.p = x86_call((void(*)())average, 3, &a[0].p);
 1a8:	b8 00 00 00 00       	mov    eax,0x0
 1ad:	8d 54 24 1c          	lea    edx,[esp+0x1c]
 1b1:	89 54 24 08          	mov    DWORD PTR [esp+0x8],edx
 1b5:	c7 44 24 04 03 00 00 	mov    DWORD PTR [esp+0x4],0x3
 1bc:	00 
 1bd:	89 04 24             	mov    DWORD PTR [esp],eax
 1c0:	e8 fc ff ff ff       	call   1c1 <main+0xd7>
 1c5:	89 44 24 2c          	mov    DWORD PTR [esp+0x2c],eax
  printf("Result: %i\n", r2.i);fflush(stdout);
 1c9:	8b 54 24 2c          	mov    edx,DWORD PTR [esp+0x2c]
 1cd:	b8 34 00 00 00       	mov    eax,0x34
 1d2:	89 54 24 04          	mov    DWORD PTR [esp+0x4],edx
 1d6:	89 04 24             	mov    DWORD PTR [esp],eax
 1d9:	e8 fc ff ff ff       	call   1da <main+0xf0>
 1de:	a1 00 00 00 00       	mov    eax,ds:0x0
 1e3:	89 04 24             	mov    DWORD PTR [esp],eax
 1e6:	e8 fc ff ff ff       	call   1e7 <main+0xfd>

  return 0;
 1eb:	b8 00 00 00 00       	mov    eax,0x0
}
 1f0:	c9                   	leave  
 1f1:	c3                   	ret    
