#if 1

#include <windows.h>

#include <stdio.h>
#include <stdlib.h>

#define AX_ENTER_NS() namespace ax {
#define AX_LEAVE_NS() }

#define AX_ENTER_MODULE(nm) AX_ENTER_NS() namespace nm {
#define AX_LEAVE_MODULE(nm) } AX_LEAVE_NS()

#define AX_ENTER_SCOPED_ENUM(nm) namespace nm { enum Enum {
#define AX_LEAVE_SCOPED_ENUM(nm) }; }

#define AX_UNCOPYABLE(C)\
	private:\
		C(const C &) = delete;\
		C &operator=(const C &) = delete

#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#if !_MSC_VER || _MSC_VER >= 1600 || __INTEL_COMPILER
# define AX_STDINT_INCLUDED 1
# include <stdint.h>
#endif

AX_ENTER_NS()

#if _MSC_VER || __INTEL_COMPILER
typedef   signed __int8         i8 ;
typedef   signed __int16        i16;
typedef   signed __int32        i32;
typedef   signed __int64        i64;
typedef unsigned __int8         u8 ;
typedef unsigned __int16        u16;
typedef unsigned __int32        u32;
typedef unsigned __int64        u64;
#elif AX_STDINT_INCLUDED
typedef            int8_t       i8 ;
typedef            int16_t      i16;
typedef            int32_t      i32;
typedef            int64_t      i64;
typedef           uint8_t       u8 ;
typedef           uint16_t      u16;
typedef           uint32_t      u32;
typedef           uint64_t      u64;
#else
typedef   signed char           i8 ;
typedef   signed short          i16;
typedef   signed int            i32;
typedef   signed long long int  i64;
typedef unsigned char           u8 ;
typedef unsigned short          u16;
typedef unsigned int            u32;
typedef unsigned long long int  u64;
#endif

#if AX_ARCH_BITS==64
typedef          i64            sint;
typedef          u64            uint;
#else
typedef          i32            sint;
typedef          u32            uint;
#endif
typedef          sint           iint;

typedef          i8             s8 ;
typedef          i16            s16;
typedef          i32            s32;
typedef          i64            s64;

typedef          float          f32;
typedef          double         f64;

static_assert(sizeof(i8 )==1, "i8 is not 8-bits");
static_assert(sizeof(i16)==2, "i16 is not 16-bits");
static_assert(sizeof(i32)==4, "i32 is not 32-bits");
static_assert(sizeof(i64)==8, "i64 is not 64-bits");
static_assert(sizeof(u8 )==1, "u8 is not 8-bits");
static_assert(sizeof(u16)==2, "u16 is not 16-bits");
static_assert(sizeof(u32)==4, "u32 is not 32-bits");
static_assert(sizeof(u64)==8, "u64 is not 64-bits");

static_assert(sizeof(f32)==4, "f32 is not 32-bits");
static_assert(sizeof(f64)==8, "f64 is not 64-bits");

static_assert(sizeof(sint)==sizeof(void *), "sint does not match the size of a pointer");
static_assert(sizeof(uint)==sizeof(void *), "uint does not match the size of a pointer");

//C++11 issues? replace with `typedef void *nullptr_t;' and `#define nullptr nullptr_t(0)'
typedef decltype(nullptr)       nullptr_t;

template<typename T> class TRmRef       { public: typedef T type; };
template<typename T> class TRmRef<T  &> { public: typedef T type; };
template<typename T> class TRmRef<T &&> { public: typedef T type; };

template<typename T, T v> class TIntConst {
public:
	static constexpr T Value = v;
	typedef T value_type;
	typedef TIntConst type;
	inline constexpr operator T() const { return Value; }
};
typedef TIntConst<bool, false> CFalse;
typedef TIntConst<bool, true > CTrue ;

template<typename T> class TIsLValRef     : public CFalse {};
template<typename T> class TIsLValRef<T &>: public CTrue  {};

template<typename T> class TIsRValRef      : public CFalse {};
template<typename T> class TIsRValRef<T &&>: public CTrue  {};

template<typename T> class TIsRef      : public CFalse {};
template<typename T> class TIsRef<T  &>: public CTrue  {};
template<typename T> class TIsRef<T &&>: public CTrue  {};

template<typename T> constexpr T &&forward(typename TRmRef<T>::type  &p) noexcept { return static_cast<T &&>(p); }
template<typename T> constexpr T &&forward(typename TRmRef<T>::type &&p) noexcept {
	static_assert(!TIsLValRef<T>::Value, "lvalue references are not allowed");
	return static_cast<T &&>(p);
}

AX_LEAVE_NS()

AX_ENTER_MODULE(Com)

inline void FatalErrorfEx(const char *f, uint ln, const char *fn, const char *fmt, ...) {
	va_list args;

	fprintf(stderr, "[%s(%u) %s] FATAL-ERROR: ", f, ln, fn);
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fprintf(stderr, "\n");
	fflush(stderr);
}

AX_LEAVE_MODULE(Com)

AX_ENTER_NS()

inline void handle_assert(const char *f, uint ln, const char *fn, const char *expr, const char *msg) {
	Com::FatalErrorfEx(f, ln, fn, "%s\nexpression: %s", msg, expr);
	__builtin_trap();
}

AX_LEAVE_NS()

#define assert_msg(x,y) if (!(x))::ax::handle_assert(__FILE__,__LINE__,__func__,#x,y)
#define assert(x) assert_msg(x, "assertion failed!")

AX_ENTER_NS()

enum class WaitStatus {
	Success,
	TimeOut
};

class CThread {
public:
	enum class Status {
		Uninitialized,
		Running,
		Finished,
		Aborted
	};
	enum class Error {
		None,
		SystemError,
		AlreadyInitialized,
		AlreadyFinished,
		NotRunning,
		StillRunning
	};
	enum class Priority {
		Idle,
		Low,
		Normal,
		High,
		RealTime
	};

	typedef int(*function_type)(void *);

protected:
	Status m_status;
	Priority m_priority;
	mutable Error m_error;
	int m_exitValue;
	function_type m_func;
	void *m_parm;

#if _WIN32
	HANDLE m_thread;
#else
	pthread_t m_thread;
#endif

	inline void SetError(Error errc) const {
		if (m_error != Error::None)
			return;

		m_error = errc;
	}

	inline bool NotRunning() {
		if (m_thread==nullptr)
			return true;

		if (m_status!=Status::Running) {
			Join();
			return true;
		}

		return false;
	}

#if _WIN32
# define AX_THREAD_CALL __stdcall
	typedef DWORD result_type;
#else
# define AX_THREAD_CALL
	typedef void *result_type;
#endif

	static inline result_type AX_THREAD_CALL Thread_f(void *p) {
		CThread *thread;

		thread = reinterpret_cast<CThread *>(p);
		assert(thread != nullptr);

		thread->m_exitValue = thread->m_func(thread->m_parm);
		thread->m_status = Status::Finished;

		return static_cast<result_type>(thread->m_exitValue);
	}

public:
	inline CThread(function_type func=nullptr, void *parm=nullptr, bool run=true): m_status(Status::Uninitialized),
	m_priority(Priority::Normal), m_error(Error::None), m_func(func), m_parm(parm), m_exitValue(0), m_thread(nullptr) {
		if (!m_func)
			return;

		if (run)
			Run();
	}
	inline ~CThread() {
		Abort();
	}

	inline void SetRoutine(function_type func, void *parm=nullptr) {
		if (m_func) {
			SetError(Error::AlreadyInitialized);
			return;
		}

		m_func = func;
		m_parm = parm;
	}

	inline void Run() {
		if (!NotRunning()) {
			SetError(Error::StillRunning);
			return;
		}

		m_status = Status::Running;
		m_exitValue = -1;

#if _WIN32
		m_thread = CreateThread(nullptr, 0, &Thread_f, reinterpret_cast<void *>(this), 0, nullptr);
		if (!m_thread) {
			SetError(Error::SystemError);
			m_status = Status::Aborted;
			return;
		}
#else
		if (pthread_create(&m_thread, nullptr, &Thread_f, reinterpret_cast<void *>(this))!=0) {
			SetError(Error::SystemError);
			m_status = Status::Aborted;
			return;
		}

		pthread_detach(m_thread);
#endif
	}

	inline Status GetStatus() const { return m_status; }
	inline Priority GetPriority() const { return m_priority; }
	inline Error GetError() const { return m_error; }
	inline void ClearError() { m_error = Error::None; }
	inline int GetExitValue() const {
		if (m_status==Status::Running) {
			SetError(Error::StillRunning);
			return -1;
		}

		return m_exitValue;
	}

	inline void Abort() {
		if (m_status != Status::Running) {
			SetError(Error::NotRunning);
			return;
		}

#if _WIN32
		TerminateThread(m_thread, static_cast<DWORD>(-1UL));
		CloseHandle(m_thread);
#else
		pthread_cancel(m_thread);
#endif
		m_thread = nullptr;

		m_status = Status::Aborted;
	}

	inline int Join() {
		if (!m_thread) {
			SetError(Error::AlreadyFinished);
			return -1;
		}

#if _WIN32
		if (WaitForSingleObject(m_thread, INFINITE) != WAIT_OBJECT_0) {
			SetError(Error::SystemError);
			return -1;
		}

		CloseHandle(m_thread);
#else
		pthread_join(m_thread);
#endif

		m_thread = nullptr;
		return m_exitValue;
	}
};

AX_LEAVE_NS()

int main() {
	ax::CThread task1([](void *){printf("A\n");return 0;}, nullptr);
	ax::CThread task2([](void *){printf("B\n");return 0;}, nullptr);
	ax::CThread task3([](void *){printf("C\n");return 0;}, nullptr);
	ax::CThread task4([](void *){printf("D\n");return 0;}, nullptr);

	task1.Join();
	task2.Join();
	task3.Join();
	task4.Join();

	//TPackagedTask<int, int, int> task([](int x, int y){return x+y;}, 2, 2);
	//printf("%i\n", task.Call());

	return EXIT_SUCCESS;
}

#else

#include <iostream>
#include <future>
#include <thread>
 
int main()
{
	// future from a packaged_task
	std::packaged_task<int()> task([](){ return 7; }); // wrap the function
	std::future<int> f1 = task.get_future();  // get a future
	std::thread(std::move(task)).detach(); // launch on a thread
 
	// future from an async()
	std::future<int> f2 = std::async(std::launch::async, [](){ return 8; });
 
	// future from a promise
	std::promise<int> p;
	std::future<int> f3 = p.get_future();
	std::thread( [](std::promise<int>& p){ p.set_value(9); }, 
				 std::ref(p) ).detach();
 
	std::cout << "Waiting...";
	f1.wait();
	f2.wait();
	f3.wait();
	std::cout << "Done!\nResults are: "
			  << f1.get() << ' ' << f2.get() << ' ' << f3.get() << '\n';
}

#endif
