#include <GL/glfw.h>
#include <GL/gl.h>

/*
 * MATH STRUCTURES AND GLOBALS
 */
typedef struct Vec3_s {
	GLfloat x, y, z;
} __attribute__((packed)) Vec3_t;

typedef struct Mat3_s {
	GLfloat xx, yx, zx,
	        xy, yy, zy,
	        xz, yz, zz;
} __attribute__((packed)) Mat3_t;

typedef struct Mat4_s {
	GLfloat xx, yx, zx, wx,
	        xy, yy, zy, wy,
	        xz, yz, zz, wz,
	        xw, yw, zw, ww;
} __attribute__((packed)) Mat4_t;

#define MAX_SINCOS 32768
double g_sinCosTable[MAX_SINCOS][2];

#define PI (3.141592653589793238462643383279502884197169)
#define TWOPI (2.0*PI)
#define HALFPI (0.5*PI)

#define GRAVITY (9.8)

/*
 * GEOMETRY STRUCTURES AND GLOBALS
 */
typedef struct Vertex_s {
	Vec3_t pos;
	Vec3_t norm;
} __attribute__((packed)) Vertex_t;

typedef struct Surface_s {
	GLushort numVertices;
	GLuint numIndices;

	Vertex_t *vertices; /*in memory vertices*/
	GLushort *indices; /*in memory indices*/
	GLenum primtype; /*primitive type*/
	GLuint numPrimitives;
	GLuint baseVertex; /*where to start at*/

	struct {
		unsigned collision:1;	/*can collide?*/
		unsigned physics:1;		/*gravity affects this?*/
		unsigned visible:1;		/*visible?*/
		unsigned editor:1;		/*editor object? (only visible when editing)*/
	} flags;
} *Surface_t;

/*
 * SCENE STRUCTURES AND GLOBALS
 */
typedef struct Entity_s {
	char *name;
	char *text;

    struct Entity_s *prnt, **p_head,**p_tail;
	struct Entity_s *head, *tail;
	struct Entity_s *prev, *next;

	Mat3_t lRot, gRot;

	GLuint numSurfaces;
	struct Surface_s *surfaces;
} *Entity_t;

/*
 * MATH FUNCTIONS
 */
void InitSinCosTable() {
	GLuint i;

	for(i=0; i<MAX_SINCOS; i++) {
		double x;

		x = (((double)i)/MAX_SINCOS)*TWOPI;
		g_sinCosTable[i][0] = sin(x);
		g_sinCosTable[i][1] = cos(x);
	}
}

float Sin(float x) {
	return (float)g_sinCosTable[(GLuint)(x/360.0f*MAX_SINCOS) % MAX_SINCOS][0];
}
float Cos(float x) {
	return (float)g_sinCosTable[(GLuint)(x/360.0f*MAX_SINCOS) % MAX_SINCOS][1];
}
void SinCos(float *sc, float x) {
	GLuint i;

	i = (GLuint)(x/360.0f*MAX_SINCOS) % MAX_SINCOS;

	sc[0] = (float)g_sinCosTable[i][0];
	sc[1] = (float)g_sinCosTable[i][1];
}

Vec3_t *Vec3(GLfloat x, GLfloat y, GLfloat z) {
	static Vec3_t vecs[32];
	static GLuint i = 0;
	Vec3_t *v;

	v = &vecs[i];
	i = (i + 1)%(sizeof(vecs)/sizeof(vecs[0]));

	v->x = x;
	v->y = y;
	v->z = z;

	return v;
}
float Dot(const Vec3_t *a, const Vec3_t *b) {
	return a->x*b->x + a->y*b->y + a->z*b->z;
}
