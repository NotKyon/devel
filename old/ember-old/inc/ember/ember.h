#ifndef EMBER_EMBER_H
#define EMBER_EMBER_H

#include "config.h"
#include "gc.h"
#include "hook.h"
#include "list.h"
#include "btree.h"
#include "buffer.h"
#include "time.h"
#include "math.h"
#include "opengl.h"
#include "texture.h"
#include "effect.h"
#include "transform.h"
#include "scene.h"
#include "entity.h"

#endif
