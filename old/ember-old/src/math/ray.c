#include <ember/math.h>
#include <math.h>

float ray_org_x( const Ray *src )
{
	return src->org.x;
}
float ray_org_y( const Ray *src )
{
	return src->org.y;
}
float ray_org_z( const Ray *src )
{
	return src->org.z;
}

float ray_dir_x( const Ray *src )
{
	return src->dir.x;
}
float ray_dir_y( const Ray *src )
{
	return src->dir.y;
}
float ray_dir_z( const Ray *src )
{
	return src->dir.z;
}

void ray_set_ex( Ray *dst, const Vec3 *org, const Vec3 *dir )
{
	vec3_copy( &dst->org, org );
	vec3_copy( &dst->dir, dir );
}
void ray_set( Ray *dst, float px, float py, float pz, float dx, float dy,
    float dz )
{
	dst->org.x = px;
	dst->org.y = py;
	dst->org.z = pz;

	dst->dir.x = dx;
	dst->dir.y = dy;
	dst->dir.z = dz;
}

void ray_copy( Ray *dst, const Ray *src )
{
	vec3_copy( &dst->org, &src->org );
	vec3_copy( &dst->dir, &src->dir );
}

int ray_intersects_tri_ex( const Ray *ray, const Vec3 *verts, Vec2 *uv,
    float *dist )
{
	Vec3 diff, na, nb, vt, norm, pt;
	float delta, distn;
	float d00, d01, d02, d11, d12, r;
	float u, v;

	vec3_subtract( &na, &verts[ 1 ], &verts[ 0 ] );
	vec3_subtract( &nb, &verts[ 2 ], &verts[ 0 ] );

	vec3_cross( &norm, &na, &nb );
	vec3_normalize( &norm );

	vec3_subtract( &diff, &ray->org, &verts[ 0 ] );
	if( ( delta = vec3_dot( &diff, &norm ) ) < 0 )
		return 0;

	if( ( distn = -delta / vec3_dot( &ray->dir, &norm ) ) < 0 )
		return 0;

	pt.x = ray->org.x + ray->dir.x * distn;
	pt.y = ray->org.y + ray->dir.y * distn;
	pt.z = ray->org.z + ray->dir.z * distn;

	vec3_subtract( &vt, &pt, &verts[ 0 ] );

	d00 = vec3_dot( &nb, &nb );
	d01 = vec3_dot( &nb, &na );
	d02 = vec3_dot( &nb, &vt );
	d11 = vec3_dot( &na, &na );
	d12 = vec3_dot( &na, &vt );

	r = 1.0f / ( d00 * d01 - d01 * d01 );
	u = ( d11 * d02 - d01 * d12 ) * r;
	v = ( d00 * d12 - d01 * d02 ) * r;

	if( u > 0 && v > 0 && u + v < 1 ) {
		if( uv ) {
			uv->x = u;
			uv->y = v;
		}

		if( dist )
			*dist = distn;

		return 1;
	}

	return 0;
#if 0
	Matrix m, i;
	/*Vec4	t, v;*/
	Vec3 t, v;

	m.rm._11 = tris[1].x - tris[0].x;
	m.rm._12 = tris[2].x - tris[0].x;
	m.rm._13 = -ray->dir.x;
	/*i.rm._14 = 0;*/

	m.rm._21 = tris[1].y - tris[0].y;
	m.rm._22 = tris[2].y - tris[0].y;
	m.rm._23 = -ray->dir.y;
	/*i.rm._24 = 0;*/

	m.rm._31 = tris[1].z - tris[0].z;
	m.rm._32 = tris[2].z - tris[0].z;
	m.rm._33 = -ray->dir.z;
	/*i.rm._34 = 0;*/

	/*i.rm._41 = 0;
	 *  i.rm._42 = 0;
	 *  i.rm._43 = 0;
	 *  i.rm._44 = 1;*/

	/*t.x = ray->org.x-tris[0].x;
	 * t.y = ray->org.y-tris[0].y;
	 * t.z = ray->org.z-tris[0].z;
	 * t.w = 0;*/
	vec3_subtract(&t, &ray->org, &tris[0]);

	/*mat_inverse(&i, &m);
	 * vec4_transform_ex(&v, &t, &i);*/
	mat_inverse3(&i, &m);
	vec3_transform3_ex(&v, &t, &i);

	if (v.x >= 0 && v.y >= 0 && v.x + v.y <= 1.0 && v.z >= 0) {
		uv->x = v.x;
		uv->y = v.y;
		*dist = v.z;

		return 1;
	}

	return 0;
#endif
}

/* If this is needed, it can be enabled... Disabled for cleanliness. */
/*
 * int ray_intersects_tri
 * (	float px, float py, float pz,
 * float dx, float dy, float dz,
 * float v0x, float v0y, float v0z,
 * float v1x, float v1y, float v1z,
 * float v2x, float v2y, float v2z,
 * float *u, float *v, float *dist)
 * {
 * Ray ray;
 * Vec3 tri[3];
 * Vec2 uv;
 * int r;
 *
 * ray.org.x = px; ray.org.y = py; ray.org.z = pz;
 * ray.dir.x = dx; ray.dir.y = dy; ray.dir.z = dz;
 *
 * tri[0].x = v0x; tri[0].y = v0y; tri[0].z = v0z;
 * tri[1].x = v1x; tri[1].y = v1y; tri[1].z = v1z;
 * tri[2].x = v2x; tri[2].y = v2y; tri[2].z = v2z;
 *
 * r = ray_intersects_tri_ex(&ray, &tri[0], &uv, dist)
 * if (u)	*u = uv.x;
 * if (v)	*v = uv.y;
 *
 * return r;
 * }
 */
int ray_intersects_sphere_ex( const Ray *ray, const Vec3 *center, float radius,
    Vec3 *hit )
{
	/* TODO: This can be better written using an algorithm similar to the
       * above, but with delta distance checking against the radius of the
   * sphere,
       * or squared distance (may be faster). */

	Vec3 a, b, dir, tmp;
	float d, dist, p, r;

	vec3_subtract( &a, center, &ray->org );

	vec3_copy( &dir, &ray->dir );
	vec3_normalize( &dir );
	d = vec3_dot( &a, &dir );
	vec3_scale( &tmp, &dir, d );
	vec3_add( &b, &ray->org, &tmp );

	vec3_subtract( &tmp, center, &b );
	if( ( dist = vec3_length( &tmp ) ) > radius )
		return 0;

	if( !hit ) /* TODO: Patch this */
		return 1;

	p = sqrtf( radius * radius - dist * dist );
	r = vec3_length( &a );

	if( vec3_dot( &a, &dir ) > 0 ) {
		if( r > radius ) {
			vec3_scale( &tmp, &dir, d - p );
			vec3_add( hit, &ray->org, &tmp );
		} else {
			vec3_scale( &tmp, &dir, p );
			vec3_add( hit, &b, &tmp );
		}
	} else {
		if( r > radius ) {
			/* TODO: Patch this */
			return 0;
		} else {
			vec3_scale( &tmp, &dir, p );
			vec3_add( hit, &b, &tmp );
		}
	}

	return 1;
}

void ray_local( Ray *dst, const Ray *src, const Matrix *space )
{
	Matrix inv;

	mat_inverse( &inv, space );

	vec3_transform_ex( &dst->org, &src->org, &inv );
	vec3_transform3_ex( &dst->dir, &src->dir, &inv );
	vec3_normalize( &dst->dir );
}

void ray_pick( Ray *dst, float x, float y, float width, float height,
    const Matrix *invwrldviewproj )
{
	Vec3 p;

	p.x = x;
	p.y = y;

	p.z = 0;
	vec3_unproject( &dst->org, &p, width, height, invwrldviewproj );

	p.z = 1;
	vec3_unproject( &dst->dir, &p, width, height, invwrldviewproj );
	vec3_subtract( &dst->dir, &dst->dir, &dst->org );
}
