USERNAME=Administrator

NUMBER_OF_PROCESSORS=4 
PROCESSOR_ARCHITECTURE=x86
PROCESSOR_IDENTIFIER=x86 Family 18 Model 1 Stepping 0, AuthenticAMD
PROCESSOR_LEVEL=18
PROCESSOR_REVISION=0100
OS=Windows_NT

NO GPU DEBUG INFO GENERATED

Task1 Results (-O3 -fomit-frame-pointer -fno-strict-aliasing -march=pentium)
4.029
4.082
3.994
4.054
4.112
3.915

Task2 Results (-O3 -fomit-frame-pointer -fno-strict-aliasing -msse3 -march=core2)
4.045
4.063
4.052
4.166
3.926
4.149

Task3 Results (-march=pentium) 
3.982
3.902
3.982
3.988
3.987
3.953

NUMBER_OF_PROCESSORS=4 
PROCESSOR_ARCHITECTURE=x86 
PROCESSOR_IDENTIFIER=x86 Family 18 Model 1 Stepping 0, AuthenticAMD 
PROCESSOR_LEVEL=18 
PROCESSOR_REVISION=0100 
P1 Task1 Results (-O3 -fomit-frame-pointer -fno-strict-aliasing -march=pentium)
implementation: lockfree (4)
compiled with: gcc (4)
built: Jun 16 2012 15:55:12

cpu-core-count  : 4
cpu-thread-count: 8
usr-thread-count: 32
4.030
4.041
3.944
4.133
4.195
4.206

P1 Task2 Results (-O3 -fomit-frame-pointer -fno-strict-aliasing -msse3 -march=core2)
implementation: lockfree (4)
compiled with: gcc (4)
built: Jun 16 2012 15:55:12

cpu-core-count  : 4
cpu-thread-count: 8
usr-thread-count: 32
3.878
3.984
4.086
4.082
4.141
4.227

P1 Task3 Results (-march=pentium) 
implementation: lockfree (4)
compiled with: gcc (4)
built: Jun 16 2012 15:55:13

cpu-core-count  : 4
cpu-thread-count: 8
usr-thread-count: 8
4.006
3.955
3.898
3.936
3.927
3.903

P2 Task1 Results (-O3 -fomit-frame-pointer -fno-strict-aliasing -march=pentium)
implementation: lockfree (4)
compiled with: gcc (4)
built: Jun 16 2012 15:55:12

cpu-core-count  : 4
cpu-thread-count: 16793618
usr-thread-count: 4
5.274
5.310
5.187
3.959
5.287
5.219

P2 Task2 Results (-O3 -fomit-frame-pointer -fno-strict-aliasing -msse3 -march=core2)
implementation: lockfree (4)
compiled with: gcc (4)
built: Jun 16 2012 15:55:12

cpu-core-count  : 4
cpu-thread-count: 16793618
usr-thread-count: 4
5.291
4.002
3.950
5.228
5.257
5.295

P2 Task3 Results (-march=pentium) 
implementation: lockfree (4)
compiled with: gcc (4)
built: Jun 16 2012 15:55:13

cpu-core-count  : 4
cpu-thread-count: 8
usr-thread-count: 4
5.249
5.300
5.406
4.100
5.482
5.557

