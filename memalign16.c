/* 16-byte alignment memory management routines */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define EE_ASSERT(x) if(!(x)){fflush(stdout);fprintf(stderr,"%s(%i): error in %s: EE_ASSERT(%s) failed!\n", __FILE__,__LINE__,__func__,#x);exit(EXIT_FAILURE);}

typedef union { void *p; size_t n; } ptradr_t;
static void *Malloc16(size_t n) {
	ptradr_t ptr;
	void *p;

	EE_ASSERT(n > 0);

	if (!(ptr.p = p = malloc(n + 16 + sizeof(void *))))
		return (void *)0;

	ptr.n -= ptr.n%16;
	ptr.n += 16 - sizeof(void *);

	*(void **)ptr.p = p;

	ptr.n += sizeof(void *);

	EE_ASSERT(ptr.n%16==0 /*should be aligned*/);

	return ptr.p;
}
static void *Realloc16(void *p, size_t n) {
	ptradr_t ptr;

	ptr.p = p;

	EE_ASSERT(p != (void *)0);
	EE_ASSERT(ptr.n > 16 + sizeof(void *));
	EE_ASSERT(ptr.n%16==0 /*if not aligned,invalid memory*/);
	EE_ASSERT(n > 0);

	ptr.n -= sizeof(void *);

	EE_ASSERT(*(void **)ptr.p != (void *)0 /*never a null pointer*/);

	if (!(ptr.p = p = realloc(*(void **)ptr.p, n + 16 + sizeof(void *))))
		return (void *)0;

	ptr.n -= ptr.n%16;
	ptr.n += 16 - sizeof(void *);

	*(void **)ptr.p = p;

	ptr.n += sizeof(void *);

	EE_ASSERT(ptr.n%16==0 /*should be aligned*/);

	return ptr.p;
}
static void Free16(void *p) {
	ptradr_t ptr;

	ptr.p = p;

	EE_ASSERT(p != (void *)0);
	EE_ASSERT(ptr.n > 16 + sizeof(void *));
	EE_ASSERT(ptr.n%16==0 /*if not aligned,invalid memory*/);

	ptr.n -= sizeof(void *);

	EE_ASSERT(*(void **)ptr.p != (void *)0 /*never a null pointer*/);

	free(*(void **)ptr.p);
}

int Rand(int l, int h) {
	return l + rand()%(h - l);
}

void *TestMM() {
	int n;
	void *p;

	n = Rand(512, 1024);
	p = Malloc16(n);
	printf("Malloc16(%i) = %p\n", n, p);

	n = Rand(2, 2048);
	p = Realloc16(p, n);
	printf("Realloc16(%i) = %p\n", n, p);

	return p;
}

int main() {
#define N 32
	void *p[N];
	int i;

	for(i=0; i<N; i++)
		p[i] = TestMM();

	for(i=0; i<N; i++)
		Free16(p[i]);

	return 0;
#undef N
}
