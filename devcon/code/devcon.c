/*
 * NOTE: This system is totally NOT efficient. At all. It's not clever. It's not
 * something that you'll look at and think "Ah! That's awesome!" or any
 * variation thereof. It's meant to work, and it's mostly a hack-job. Most of
 * this code should be cleaned up. Some of the mess is due to some independence
 * of a specific rendering engine. It mostly relies on OpenGL and some extra
 * functions.
 */
#include "devcon.h"

#include "cvar.h"
#include "cmd.h"

#include "event.h"
#include "memory.h"
#include "system.h"
#include "string.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glfw.h>

/* devcon */
int g_devconAlive = 0;
devcon_t g_devcon;

/*g_devconFont[] is in font_data.c*/

#define FONT_WIDTH 8
#define FONT_HEIGHT 16

static int NextRow(int index) {
	index -= index%DEVCON_COLUMNS;
	index += DEVCON_COLUMNS;

	return index;
}
static int GetRow(int index) {
	return (index - (index%DEVCON_COLUMNS))/DEVCON_COLUMNS;
}

static void Clear() {
	g_devcon.curPalette = kConColor_Foreground;

	g_devcon.curRow = 0;
	g_devcon.numRows = 0;

	g_devcon.curText = 0;

	memset(g_devcon.text, 0, sizeof(g_devcon.text));
}

static size_t Format(unsigned short *dst, size_t n, const char *src, ...) {
	static char buf[0x1000];
	va_list args;
	size_t i;
	char c;
	int color;

	va_start(args, src);
	vsnprintf(buf, sizeof(buf) - 1, src, args);
	buf[sizeof(buf) - 1] = 0;
	va_end(args);

	src = buf;

	color = 0; /*assume default color at first*/
	i = 0;

	if (!n)
		return 0;

	while((c = *src++) != '\0') {
		if (i == n)
			break;

		if (c=='^') {
			if ((c = *src++) == '\0')
				break;

			if (c>='0' && c<='9') {
				color = (int)(c - '0');
				continue;
			}
		}

		dst[i++] = (unsigned short)((color<<8)|((int)(unsigned char)c));
	}

	dst[i] = 0;
	return i;
}

static void Print(const char *text) {
	char c;
	int color;
	int textRow, numRows;
	int i, j, doScroll;

	/* grab information */
	color = g_devcon.curPalette - 1;
	i = g_devcon.curText;

	/*textRow = (i - (i % DEVCON_COLUMNS))/DEVCON_COLUMNS;*/
	textRow = GetRow(i);
	doScroll = g_devcon.curRow==textRow ? 1 : 0;

	/* enumerate the string */
	while((c = *text++) != '\0') {
		/* color */
		if (c=='^') {
			if ((c = *text++) == '\0')
				break;

			if (c>='0' && c<='9') {
				color = (int)(c - '0');
				continue;
			}
		}

		/* tabs */
		if (c=='\t') {
			do {
				g_devcon.text[i++ % DEVCON_TEXT] = (color<<8) | 0x20;
			} while(i % 4 != 0);

			continue;
		}

		/* line feed */
		if (c=='\n') {
			j = NextRow(i);
			while(i < j)
				g_devcon.text[i++ % DEVCON_TEXT] = (color<<8) | 0x20;

			continue;
		}

		/* carriage return */
		if (c=='\r') {
			i = i - (i % DEVCON_COLUMNS);
			continue;
		}

		/* normal text character */
		g_devcon.text[i++ % DEVCON_TEXT] = (color<<8) | (int)c;
	}

	/* save information to the console */
	numRows = (i - (i % DEVCON_COLUMNS))/DEVCON_COLUMNS -
		(g_devcon.curText - (g_devcon.curText % DEVCON_COLUMNS))/DEVCON_COLUMNS;

	g_devcon.curText = i;
	g_devcon.curPalette = color + 1;

	g_devcon.numRows += numRows;
	if (g_devcon.numRows > DEVCON_ROWS)
		g_devcon.numRows = DEVCON_ROWS;

	if (doScroll)
		g_devcon.curRow += numRows;
}

static void GetCharPos(vec2_t tl, vec2_t br, int index) {
	vec2_t size;
	int rct[4];
	int col, row;

	v2_Set(size, FONT_WIDTH*DEVCON_COLUMNS, v_Height());

	row = index/DEVCON_COLUMNS;
	col = index%DEVCON_COLUMNS;

	rct[0] = col*FONT_WIDTH;
	rct[1] = row*FONT_HEIGHT;
	rct[2] = rct[0] + FONT_WIDTH;
	rct[3] = rct[1] + FONT_HEIGHT;

	v2_ScreenToProj(tl, Vec2(rct[0], rct[1]), size);
	v2_ScreenToProj(br, Vec2(rct[2], rct[3]), size);
}

static void GetCharTexMap(vec2_t tc[2], int c) {
	c -= 0x20;

	if (*(unsigned int *)&c >= 0x7F)
		c = 0; /*use blank space for non-printable characters*/

	tc[0][0] = ((float)((c%16)))*((float)FONT_WIDTH)/128.0f;
	tc[0][1] = ((float)((c/16)))*((float)FONT_HEIGHT)/128.0f;
	tc[1][0] = tc[0][0] + ((float)FONT_WIDTH)/128.0f;
	tc[1][1] = tc[0][1] + ((float)FONT_HEIGHT)/128.0f;
}

static void BindConsoleFont() {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, g_devcon.fontImage);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.1f);
}

static void ApplyColor(int color) {
	vec_t *p;

	p = g_devcon.palette[color + 1];
	glColor4f(p[0], p[1], p[2], p[3]);
}

static int g_color;

static void DrawChar(int outIndex, int charIndex, int color) {
	vec2_t tl, br, tc[2];

	GetCharPos(tl, br, outIndex);
	GetCharTexMap(tc, charIndex);

	if (g_color != color) {
		g_color = color;
		ApplyColor(g_color);
	}

	glTexCoord2f(tc[0][0], tc[0][1]); glVertex2f(tl[0], tl[1]);
	glTexCoord2f(tc[1][0], tc[0][1]); glVertex2f(br[0], tl[1]);
	glTexCoord2f(tc[1][0], tc[1][1]); glVertex2f(br[0], br[1]);
	glTexCoord2f(tc[0][0], tc[1][1]); glVertex2f(tl[0], br[1]);
}

static void DrawInsideText() {
	unsigned short txt;
	int i, j;

	if ((i = (g_devcon.curRow - g_devcon.visRows)*DEVCON_COLUMNS) < 0)
		i = 0;

	j = 0;

	while(j < g_devcon.visRows*DEVCON_COLUMNS) {
		txt = g_devcon.text[i++ % DEVCON_TEXT];

		DrawChar(j++, txt&0xFF, ((txt&0xFF00)>>8) % (kNumConColors - 1));
	}
}

static size_t GetUsedRows(size_t len) {
	len = len/DEVCON_COLUMNS + (len%DEVCON_COLUMNS > 0 ? 1 : 0);
	return len ? len : 1;
}
static void DrawCommandPrompt() {
	static const double toggletime = 0.5;
	unsigned short buf[0x200];
	size_t i, bufl;
	int j;

	const char *cursor;

	if( fmod( glfwGetTime(), toggletime*2 ) < toggletime ) {
		cursor = "_";
	} else {
		cursor = "";
	}

	bufl = Format(buf, sizeof(buf), "^3%s^4%s^5%s^1%s",
		g_devcon.prompt, g_devcon.command, g_devcon.autocomplete, cursor);
	g_devcon.visRows = g_devcon.height/FONT_HEIGHT - GetUsedRows(bufl);

	j = g_devcon.visRows*DEVCON_COLUMNS;

	for(i=0; i<bufl; i++)
		DrawChar(j++, buf[i]&0xFF, (buf[i]&0xFF00)>>8);
}

static void DrawConsoleText() {
	g_color = -1;

	glBegin(GL_QUADS);
		DrawInsideText();
		DrawCommandPrompt();
	glEnd();
}

/*
 * =============================================================================
 *
 *	DEVELOPER CONSOLE FUNCTIONS
 *
 * =============================================================================
 */

void con_Init() {
#define SET_PALETTE(x,r,g,b,a)\
	g_devcon.palette[kConColor_##x][0] = r;\
	g_devcon.palette[kConColor_##x][1] = g;\
	g_devcon.palette[kConColor_##x][2] = b;\
	g_devcon.palette[kConColor_##x][3] = a

	if (g_devconAlive) {
		sys_Warning("Attempted to initialize already initialized console.");
		return;
	}

	/* prepare the palette */
	SET_PALETTE( Background   , 0.1f,0.1f,0.1f,0.8f );

	SET_PALETTE( Foreground   , 0.8f,0.8f,0.8f,1.0f );
	SET_PALETTE( Error        , 1.0f,0.0f,0.0f,1.0f );
	SET_PALETTE( Warning      , 1.0f,1.0f,0.7f,1.0f );

	SET_PALETTE( Prompt       , 0.0f,1.0f,0.0f,1.0f );
	SET_PALETTE( EditField    , 1.0f,1.0f,1.0f,1.0f );
	SET_PALETTE( Autocomplete , 0.3f,0.7f,1.0f,1.0f );

	g_devcon.visible = 0; /*not visible by default*/

	/*
	 * NOTE: GLint vp[4]; glGetIntergv(GL_VIEWPORT, vp); will get the viewport's
	 *       dimensions.
	 */
	g_devcon.height = 15*FONT_HEIGHT;

	/* clear the console (initializes various fields) */
	Clear();

	/* prepare the command prompt */
	snprintf(g_devcon.prompt, sizeof(g_devcon.prompt) - 1, "]");
	g_devcon.prompt[sizeof(g_devcon.prompt) - 1] = 0;

	g_devcon.command[0] = 0;
	g_devcon.autocomplete[0] = 0;

	/* build the font's texture */
	glGenTextures(1,  &g_devcon.fontImage);

	glBindTexture(GL_TEXTURE_2D, g_devcon.fontImage);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 128, 128, 0, GL_LUMINANCE,
		GL_UNSIGNED_BYTE, g_devconFont);

	g_devconAlive = 1;
#undef SET_PALETTE
}
void con_Deinit() {
	if (!g_devconAlive)
		return;

	g_devconAlive = 0;
}
int con_IsAlive() {
	return g_devconAlive;
}

void con_Show() {
	ASSERT(con_IsAlive());

	g_devcon.visible = 1;

	g_devcon.visRows = g_devcon.height/FONT_HEIGHT - 1;
}
void con_Hide() {
	ASSERT(con_IsAlive());

	g_devcon.visible = 0;
}

int con_IsVisible() {
	ASSERT(con_IsAlive());

	return g_devcon.visible;
}

void con_Render() {
	/*struct devconLine_s *line;*/
	vec2_t tl, br, size;
	int conRct[4];

	ASSERT(con_IsAlive());

	/* overwrite the matrix to avoid weird effects */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/* convert from screen space to projection space */
	size[0] = (vec_t)v_Width();
	size[1] = (vec_t)v_Height();

	conRct[0] = 0;
	conRct[1] = 0;
	conRct[2] = v_Width();
	conRct[3] = g_devcon.height;

	v2_ScreenToProj(tl, Vec2(conRct[0], conRct[1]), size);
	v2_ScreenToProj(br, Vec2(conRct[2], conRct[3]), size);

	/* disable stuff that shouldn't affect the console */
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	/* enable blending for translucency */
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/* improve performance by only doing a render of this area */
	glEnable(GL_SCISSOR_TEST);
	glScissor(v_X(), v_Y(), v_Width(), v_Height());

	/* background */
	r_DrawRect(conRct, g_devcon.palette[kConColor_Background]);

	/* render the console */
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	BindConsoleFont();
	DrawConsoleText();

	/* disable the scissor test now */
	glDisable(GL_SCISSOR_TEST);
}

void con_PrintV(const char *format, va_list args) {
	char buf[4096];

	ASSERT(con_IsAlive());

	printf("con_PrintV...");
	fflush(stdout);

	vsnprintf(buf, sizeof(buf)-1, format, args);
	buf[sizeof(buf)-1] = 0;

#if 0
	AddLinesToConsole(buf);
#else
	Print(buf);
#endif

	printf(" [done]\n");
	fflush(stdout);
}
void con_Print(const char *format, ...) {
	va_list args;

	ASSERT(con_IsAlive());

	va_start(args, format);
	con_PrintV(format, args);
	va_end(args);
}

void con_KeyEvent() {
	static int keymap[256], shiftmap[256];
	static int didinit = 0;
	int key, state, shift;

	key = ev_X();
	state = ev_Code()==kEvent_KeyUp ? 0 : 1;

	shift = glfwGetKey(287) | glfwGetKey(288);/*287/288 = left/right shift*/

	if (!didinit) {
		int i;

		for(i=0; i<256; i++) {
			keymap[i] = i;
			shiftmap[i] = i;
		}

		for(i='A'; i<='Z'; i++)
			keymap[i] = i - 'A' + 'a';

		for(i='a'; i<='z'; i++)
			shiftmap[i] = i - 'a' + 'A';

		shiftmap['1'] = '!';
		shiftmap['2'] = '@';
		shiftmap['3'] = '#';
		shiftmap['4'] = '$';
		shiftmap['5'] = '%';
		shiftmap['6'] = '^';
		shiftmap['7'] = '&';
		shiftmap['8'] = '*';
		shiftmap['9'] = '(';
		shiftmap['0'] = ')';

		shiftmap['`'] = '~';
		shiftmap['-'] = '_';
		shiftmap['='] = '+';
		shiftmap['['] = '{';
		shiftmap[']'] = '}';
		shiftmap['\\'] = '|';
		shiftmap[';'] = ':';
		shiftmap['\''] = '\"';
		shiftmap[','] = '<';
		shiftmap['.'] = '>';
		shiftmap['/'] = '?';

		didinit = 1;
	}

	if (key==DEVCON_KEY) {
		if (state)
			return;

		con_Hide();
		return;
	}
	if (key==257/*esc*/) {
		if (state)
			return;

		con_Hide();
		return;
	}

	if (key==294/*enter*/) {
		if (state)
			return;

		sys_Log("You typed: ^4%s^0\n", g_devcon.command);

		cmd_Add(g_devcon.command);
		if (!cmd_Execute())
			sys_Error("Failed to execute command: %s", g_devcon.command);

		g_devcon.command[0] = 0;
		g_devcon.curCommand = 0;

		return;
	}

	if (key==298/*pgup*/ || key==299/*pgdn*/) {
		if (!state) {
			/*
			 * TODO: Stop auto-repeat
			 */
		}

		/*
		 * TODO: Scrolling
		 */

		return;
	}

	if (key==287/*left shift*/ || key==288/*right shift*/)
		return;

	/*
	 * TODO: Key repeat
	 */

	if (key==295/*backspace*/) {
		if (!state) {
			/*
			 * TODO: Stop auto-repeat
			 */

			return;
		}

		if (!g_devcon.curCommand)
			return;

		g_devcon.command[--g_devcon.curCommand] = 0;
		return;
	}

	/* is printable? */
	if (key>=' ' && key<0x7F) {
		if (!state) {
			/*
			 * TODO: Stop auto-repeat
			 */

			return;
		}

		if (g_devcon.curCommand==DEVCON_COMMAND - 1) {
			sys_Warning("Command buffer is full.");
			return;
		}

		key = keymap[key];
		if (shift)
			key = shiftmap[key];

		g_devcon.command[g_devcon.curCommand++] = key;
		g_devcon.command[g_devcon.curCommand] = 0;

		return;
	}

	sys_Warning("Key %s: %i", state ? "pressed" : "released", key);
}

void con_MouseEvent() {
}
