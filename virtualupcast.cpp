#include <stdio.h>
#include <stdlib.h>

// Cast from a virtual base class to a derived class
template< typename TDst, typename TSrc >
inline TDst *virtual_upcast( TSrc *x )
{
	// We are making an important assumption here...
	static_assert( __is_base_of( TSrc, TDst ), "TSrc must be the base type of TDst" );

	// Need to know how big TDst is on its own (if it didn't inherit from TSrc)
	static const size_t diffsize = sizeof( TDst ) - sizeof( TSrc );

	// The dst address is the src address minus the dst's relative size
	const size_t srcaddr = size_t( x );
	const size_t dstaddr = srcaddr - diffsize;

	// We now have the casted address
	return reinterpret_cast< TDst * >( dstaddr );
}

class VirtualBase
{
public:
	VirtualBase()
	: x( mX )
	, y( mY )
	, mX( 1 )
	, mY( 2 )
	{
	}
	virtual ~VirtualBase()
	{
	}
	
	const int &x;
	const int &y;

private:
	int mX;
	int mY;
};
class Derived: public virtual VirtualBase
{
public:
	Derived()
	: VirtualBase()
	, extra( mExtra )
	, mExtra( 3 )
	{
	}
	virtual ~Derived()
	{
	}
	
	const int &extra;

private:
	int mExtra;
};

int main()
{
	Derived d;
	
	printf( "%p: %i, %i, %i\n", ( void * )&d, d.x, d.y, d.extra );
	fflush( stdout );
	
	VirtualBase *const base = &d;
	printf( "%p: %i, %i\n", ( void * )base, base->x, base->y );
	fflush( stdout );
	
	Derived *const derived = virtual_upcast< Derived >( base );
	printf( "%p: %i, %i, %i\n", ( void * )derived, derived->x, derived->y, derived->extra );
	fflush( stdout );
	
	return EXIT_SUCCESS;
}
