/*

	HANDLES

	Handles are used to reduce memory consumption, improve reliability, aid
	debugging, and allow types containing them to be trivially copyable (vs.
	using pointers).

	Memory: Handles take up less space than pointers, especially for 64-bit
	platforms.

	Reliability: Pointers can be delete'd and free()'d then reused in subsequent
	allocations. If an object that contains a pointer, that has since been
	deallocated and recycled elsewhere, attempts to access that pointer, it will
	be working with invalid state. This implementation of handles alleviates
	this by providing an extra (optional) "generation" field to be encoded in
	the handle. More on this below.

	Debugging: The optional generation field encoded in the handle can be used
	to determine the relative age of the handle in "create/destroy" cycles. The
	handle's base component is a linear index into an array elsewhere. This
	index might be re-used. An extra "generation" field can be used to determine
	whether an old index is being used by comparing with a "current generation"
	stored along with the pointer in the main handle pool. This makes it easier
	to detect bugs earlier.

	Trivially Copyable: Because the index is relative to the data it references,
	there's no need to adjust the index unless the data becomes discontinuous.

*/

// The handle is specified as a template struct to support the optional
// generation field, as well as allowing the exact number of bits used for the
// index/generation to be "fitted." (e.g., 12-bits for 4096 unique indices.)
// NOTE: This should stay within 16-bits.
template< int _indexBits_, int _generationBits_ >
struct Handle {
	unsigned index:_indexBits_;
	unsigned generation:_generationBits_;
};
template< int _indexBits_ >
struct Handle< _indexBits_, 0 > {
	unsigned index:_indexBits_;
};

// Internal namespace for the following structure...
namespace _Detail {

// To make it easier to set the generation field of a handle, template
// specialization is used. This system mimics policies.
template< int _indexBits_, int _generationBits_ >
struct HandleCond {
	typedef Handle< _indexBits_, _generationBits_ > HandleType;

	static void setGeneration( HandleType &h, int generation ) {
		h.generation = generation;
	}
	static int getGeneration( const HandleType &h ) {
		return h.generation;
	}
};
template< int _indexBits_ >
struct HandleCond< _indexBits_, 0 > {
	typedef Handle< _indexBits_, 0 > HandleType;

	static void setGeneration( HandleType &h, int generation ) {
		(void)h;
		(void)generation;
	}
	static int getGeneration( const HandleType &h ) {
		(void)h;
		return 0;
	}
};

} //end of namespace _Detail

// Helper function to set the index and generation of a handle. The generation
// is not accessed at all if the handle has no field for it.
// (See _Detail::HandleSetCond<_indexBits_,_generationBits_>::setGeneration())
template< int _indexBits_, int _generationBits_ >
inline void setHandle( Handle< _indexBits_, _generationBits_ > &h, int index,
int generation ) {
	h.index = index;
	_Detail::HandleCond< _indexBits_, _generationBits_ >::setGeneration( h,
		generation );
}
// Helper function to retrieve the generation of the handle. If the handle has
// no generations support then the field is ignored.
template< int _indexBits_, int _generationBits_ >
inline int getHandleGeneration(
const Handle< _indexBits_, _generationBits_ > &h ) {
	return _Detail::HandleCond< _indexBits_, _generationBits_ >::getGeneration(
		h );
}

// To hide some of the syntax ugliness a macro is defined. It just asks how many
// bits are needed for the index and specifies the generation bits from that.
// NOTE: This will fail if more than 16-bits are requested.
#if _DEBUG
# define BASE_HANDLE( indexBits )\
	Handle< indexBits, 16 - indexBits >
#else
# define BASE_HANDLE( indexBits )\
	Handle< indexBits + (16 - indexBits), 0 >
#endif

// A test handle is defined here
typedef BASE_HANDLE( 12 ) TestHandle;

// Used for printf
#include <cstdio>
// Used for EXIT_SUCCESS
#include <cstdlib>

template<int _num_>
struct TestStruc {
	static const int NUM_ELEMENTS = _num_;

	int arr[ _num_ ];

	inline int getNumber() const {
		return NUM_ELEMENTS;
	}
};
template<>
struct TestStruc<0> {
	static const int NUM_ELEMENTS = 0;

	inline int getNumber() const {
		return NUM_ELEMENTS;
	}
};

struct ExtraStruc {
	TestStruc<0> x;
};

// Small example to show the code compiles just fine
int main() {
	TestHandle h;

	setHandle( h, 5, 0 );
	setHandle( h, 6, 1 );

	ExtraStruc es;
	printf( "%i\n", es.x.getNumber() );

	return EXIT_SUCCESS;
}
