#include <stdio.h>
#include <stdlib.h>
#include <utility>

class A
{
public:
	inline A(): x( ++n ) { printf( "+A(%i):Default\n", x ); }
	/*
	inline A( A &&a ): x( ++n ) { printf( "+A(%i):Move(%i)\n", x, a.x ); }
	*/
	inline A( A &&a ): x( ++n ) { *this = std::move( a ); }
	inline ~A() { printf( "-A(%i)\n", x ); }

	inline A &operator=( const A &a )
	{
		printf( "=A(%i):From(%i)\n", x, a.x );
		return *this;
	}
	inline A &operator=( A &&a )
	{
		printf( "=A(%i):Move(%i)\n", x, a.x );
		return *this;
	}

private:
	int x;
	static int n;
};
int A::n = 0;

int main()
{
	A a;
	A b( std::move( a ) );
	A c( std::move( A() ) );
	A d = std::move( a );
	b = std::move( c );

	printf( "...\n" );

	return EXIT_SUCCESS;
}
