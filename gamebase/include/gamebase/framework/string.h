#ifndef GB_FRAMEWORK_STRING_H
#define GB_FRAMEWORK_STRING_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Strings */
typedef struct gb_string_s {
	size_t cap, len;
	char *p;
} gb_string_t;

void gbInitString(gb_string_t *str);
void gbFiniString(gb_string_t *str);

void gbResizeString(gb_string_t *str, size_t len);

void gbPushChar(gb_string_t *str, char ch);
void gbPushStringN(gb_string_t *str, const char *s, size_t n);
void gbPushString(gb_string_t *str, const char *s);

size_t gbStringCapacity(const gb_string_t *str);
size_t gbStringLength(const gb_string_t *str);
const char *gbStringBuffer(const gb_string_t *str);
char *gbStringData(gb_string_t *str);

void gbSetStringN(gb_string_t *str, const char *s, size_t n);
void gbSetString(gb_string_t *str, const char *s);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
