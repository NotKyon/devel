#include <ember/btree.h>

void btree_init( BTreeBase *base )
{
	base->root = ( BTreeNode * )0;
	base->head = ( BTreeNode * )0;
	base->tail = ( BTreeNode * )0;
}

void btree_remove_all( BTreeBase *base )
{
	BTreeNode *node, *next;

	for( node = base->head; node; node = next ) {
		next       = node->next;
		node->prev = node->next = ( BTreeNode * )0;
		node->prnt = ( BTreeNode * )0;
		node->left = node->rght = ( BTreeNode * )0;
		node->base = ( BTreeBase * )0;
	}

	base->root = ( BTreeNode * )0;
	base->head = ( BTreeNode * )0;
	base->tail = ( BTreeNode * )0;
}

static void btree__list_node( BTreeBase *base, BTreeNode *node, int key )
{
	node->base = base;

	node->key = key;
	node->obj = ( void * )0;

	node->prnt = ( BTreeNode * )0;
	node->left = ( BTreeNode * )0;
	node->rght = ( BTreeNode * )0;

	node->next = ( BTreeNode * )0;
	if( ( node->prev = base->tail ) != ( BTreeNode * )0 )
		base->tail->next = node;
	else
		base->head = node;
	base->tail     = node;

	/*  node->prev = base->tail;
node->next = (BTreeNode *)0;
if (base->tail)
  base->tail->next = node;
base->tail = node;
if (!base->head)
base->head = node; */
}

BTreeNode *btree_find( BTreeBase *base, int key, BTreeNode *create )
{
	BTreeNode *node, *next, *top, **spot;

	if( !base->root && create ) {
		btree__list_node( base, create, key );

		base->root = create;
		return create;
	}

	top = ( BTreeNode * )0;
	for( node = base->root; node; node = next ) {
		if( node->key == key )
			return node;

		spot = ( key < node->key ) ? &node->left : &node->rght;
		top  = node;

		if( !( next = *spot ) ) {
			if( create ) {
				btree__list_node( base, create, key );

				create->prnt = top;
				return *spot = create;
			}

			return ( BTreeNode * )0;
		}
	}

	return ( BTreeNode * )0;
}

void btree_remove( BTreeNode *node )
{
	BTreeBase *base;
	BTreeNode *r, *s;

	if( !( base = node->base ) )
		return;

	if( !( r = node->rght ) ) {
		if( node->prnt ) {
			node->prnt->left = node->left;
			node->prnt->rght = ( BTreeNode * )0; /* hack */
		} else
			base->root = node->left;

		if( node->left )
			node->left->prnt = node->prnt;
	} else if( !r->left ) {
		r->prnt = node->prnt;

		if( ( r->left = node->left ) != ( BTreeNode * )0 )
			node->left->prnt = r;

		if( node->prnt )
			node->prnt->left = r;
		else
			base->root = r;
	} else {
		s = r->left;
		while( s->left )
			s = s->left;

		s->left = node->left;

		if( ( s->prnt->left = s->rght ) != ( BTreeNode * )0 )
			s->rght->prnt = s->prnt;
		s->rght           = r;

		if( ( s->prnt = node->prnt ) != ( BTreeNode * )0 ) {
			if( node->prnt->left == node )
				node->prnt->left = s;
			else
				node->prnt->rght = s;
		} else
			base->root = s;
	}

	if( node->prev )
		node->prev->next = node->next;
	if( node->next )
		node->next->prev = node->prev;

	if( base->head == node )
		base->head = node->next;
	if( base->tail == node )
		base->tail = node->prev;
}

void btree_set( BTreeNode *node, void *obj )
{
	node->obj = obj;
}
void *btree_get( BTreeNode *node )
{
	return node->obj;
}
