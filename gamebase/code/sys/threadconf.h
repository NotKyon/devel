#ifndef THREADCONF_H
#define THREADCONF_H

#if _MSC_VER > 1000
# pragma once
#endif

#if defined(EE_CONF_USE_PTHREAD) && !defined(TASK_CONF_USE_PTHREAD)
# define TASK_CONF_USE_PTHREAD EE_CONF_USE_PTHREAD
#endif

#undef __USE_WIN32_THREADS__
#if _WIN32 && !TASK_CONF_USE_PTHREAD
# define __USE_WIN32_THREADS__ 1

# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#else
# define __USE_WIN32_THREADS__ 0

# ifndef TASK_CONF_USE_PTHREAD
#  define TASK_CONF_USE_PTHREAD 1
# elif !TASK_CONF_USE_PTHREAD
#  error "Configured to not use pthread but only pthread available."
# endif

# ifndef _GNU_SOURCE
#  define _GNU_SOURCE 1
# endif
# include <sched.h>
# include <pthread.h>

# include <cstdio>
# include <cstdlib>
# include <cstring>
#endif

#include "local.h"

#if __cplusplus
extern "C" {
#endif

#if __USE_WIN32_THREADS__
# define THREADPROCAPI WINAPI
typedef HANDLE thread_t;
typedef CRITICAL_SECTION lock_t;
typedef DWORD threadResult_t;
#else
# define THREADPROCAPI
typedef pthread_t thread_t;
typedef pthread_mutex_t lock_t;
typedef void *threadResult_t;
#endif

typedef threadResult_t(THREADPROCAPI *threadFunc_t)(void *);

thread_t gblocalThreadSelf();
thread_t gblocalNewThread(threadFunc_t func, void *p);
void gblocalSetThreadCPU(thread_t thread, gb_u32_t cpu);

void gblocalJoinThread(thread_t thread);
void gblocalKillThread(thread_t thread);

void gblocalSetThreadPriority(thread_t thread, gb_threadPriority_t priority);

#if __cplusplus
}
#endif

#endif
