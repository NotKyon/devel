#ifndef MK_DEPREADER_H
#define MK_DEPREADER_H

#include "mk_types.h"

int ReadDeps(const char *filename);

#endif
