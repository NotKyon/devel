#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

typedef uint8_t M_BYTE;
typedef uint16_t M_WORD;
typedef uint32_t M_DWORD;
typedef int16_t M_WORD_S;
typedef int32_t M_DWORD_S;

#define LO_NIBBLE(x) ( ( ( ( M_BYTE )( x ) ) & 0x0F ) >> 0 )
#define HI_NIBBLE(x) ( ( ( ( M_BYTE )( x ) ) & 0xF0 ) >> 4 )

#define LO_BYTE(x) ( ( M_BYTE )( ( ( M_WORD )( ( x ) & 0x00FF ) ) >> 0 ) )
#define HI_BYTE(x) ( ( M_BYTE )( ( ( M_WORD )( ( x ) & 0xFF00 ) ) >> 8 ) )

#define LO_WORD(x) ( ( M_WORD )( ( ( M_DWORD )( ( x ) & 0x0000FFFF ) ) >> 0 ) )
#define HI_WORD(x) ( ( M_WORD )( ( ( M_DWORD )( ( x ) & 0xFFFF0000 ) ) >> 16 ) )

#define MK_BYTE(lo,hi) ( ( M_BYTE )( ( lo ) & 0x0F ) | ( ( M_BYTE )( ( hi ) & 0x0F ) << 4 ) )
#define MK_WORD(lo,hi) ( ( M_WORD )( ( ( lo ) & 0x00FF ) << 0 ) | ( M_WORD )( ( ( hi ) & 0x00FF ) << 8 ) )
#define MK_DWORD(lo,hi) ( ( M_DWORD )( ( ( lo ) & 0xFFFF ) << 0 ) | ( M_DWORD )( ( ( hi ) & 0xFFFF ) << 16 ) )

typedef enum memoryFlag_e
{
	kMemF_ReadOnly = 1<<0
} memoryFlag_t;

struct MEMORY
{
	M_DWORD size; // in bytes
	M_BYTE flags;
	M_BYTE *data;
};

struct REGISTER_BANK
{
	M_DWORD regs[ 16 ];
};
struct PROCESSOR
{
	REGISTER_BANK banks[ 16 ];
	M_BYTE currentBanks;

	M_DWORD addressRegister;
	M_DWORD extraRegister;

	M_DWORD conditionRegister[ 2 ];
	M_DWORD stackPointer;
	M_DWORD framePointer;

	MEMORY *randomMemory;

	MEMORY *programMemory;
	M_DWORD programCounter;

	M_WORD faultCode;
};

#define FAULT_PROGRAM_MEMORY_READ_ERROR					1
#define FAULT_RANDOM_MEMORY_READ_ERROR					2
#define FAULT_ILLEGAL_INSTRUCTION						3
#define FAULT_RANDOM_MEMORY_WRITE_ERROR					4
#define FAULT_DIVIDE_BY_ZERO							5

#define OP_SWITCH_REGISTER_BANK							0x00
#define OP_LOAD											0x01
#define OP_ADD											0x02
#define OP_SUB											0x03
#define OP_AND											0x04
#define OP_OR											0x05
#define OP_XOR											0x06
#define OP_NOT											0x07
#define OP_MUL											0x08
#define OP_DIV											0x09
#define OP_IMUL											0x0A
#define OP_IDIV											0x0B
#define OP_JUMP											0x0C
#define OP_CALL											0x0D
#define OP_RETURN										0x0E
#define OP_SHL											0x0F
#define OP_SHR											0x10

#define COND_ALWAYS										0x0
#define COND_NEVER										0x1
#define COND_EQUAL										0x2
#define COND_NOT_EQUAL									0x3
#define COND_LESS										0x4
#define COND_GREATER									0x5
#define COND_LESS_EQUAL									0x6
#define COND_GREATER_EQUAL								0x7
#define COND_SIGNED_LESS								0xC
#define COND_SIGNED_GREATER								0xD
#define COND_SIGNED_LESS_EQUAL							0xE
#define COND_SIGNED_GREATER_EQUAL						0xF

int ReadMemoryWord( const MEMORY *memory, M_DWORD address, M_WORD *dst )
{
	assert( memory != NULL );

	if( address >= 0xFFFFFFFE || address + 2 > memory->size ) {
		return 0;
	}

	assert( memory->data != NULL );
	if( dst != NULL ) {
		*dst = *( M_WORD * )&memory->data[ address ];
	}

	return 1;
}
int WriteMemoryDword( MEMORY *memory, M_DWORD address, M_DWORD src )
{
	assert( memory != NULL );

	if( address >= 0xFFFFFFFC || address + 4 > memory->size ) {
		return 0;
	}

	assert( memory->data != NULL );
	*( M_DWORD * )&memory->data[ address ] = src;

	return 1;
}
int WriteMemoryWord( MEMORY *memory, M_DWORD address, M_WORD src )
{
	assert( memory != NULL );

	if( address >= 0xFFFFFFFE || address + 2 > memory->size ) {
		return 0;
	}

	assert( memory->data != NULL );
	*( M_WORD * )&memory->data[ address ] = src;

	return 1;
}
int WriteMemoryByte( MEMORY *memory, M_DWORD address, M_BYTE src )
{
	assert( memory != NULL );

	if( address >= memory->size ) {
		return 0;
	}

	assert( memory->data != NULL );
	memory->data[ address ] = src;

	return 1;
}
int StepProcessor( PROCESSOR *processor )
{
	M_DWORD pc;
	M_WORD instruction;
	M_WORD immediate;
	M_BYTE bnk1, bnk2;
	M_BYTE reg1, reg2;

	pc = processor->programCounter;

	if( !ReadMemoryWord( processor->programMemory, pc, &instruction ) ) {
		processor->faultCode = FAULT_PROGRAM_MEMORY_READ_ERROR;
		return 0;
	}

	pc += 2;

	immediate = 0;
	if( ( instruction & 0x8000 ) != 0 || HI_BYTE( instruction ) == OP_LOAD ) {
		if( !ReadMemoryWord( processor->programMemory, pc, &immediate ) ) {
			processor->faultCode = FAULT_PROGRAM_MEMORY_READ_ERROR;
			return 0;
		}

		pc += 2;
	}

	bnk1 = HI_NIBBLE( processor->currentBanks );
	bnk2 = LO_NIBBLE( processor->currentBanks );

	reg1 = HI_NIBBLE( LO_BYTE( instruction ) );
	reg2 = LO_NIBBLE( LO_BYTE( instruction ) );

	switch( HI_BYTE( instruction ) & 0x7F ) {
	case OP_SWITCH_REGISTER_BANK:
		processor->currentBanks = LO_BYTE( instruction );
		break;
	case OP_LOAD:
		/* set the address register */
		if( LO_BYTE( instruction ) == 0xFF ) {
			if( HI_BYTE( instruction ) & 0x80 ) {
				processor->addressRegister |= ( ( M_DWORD )immediate ) << 16;
			} else {
				processor->addressRegister = ( M_DWORD )immediate;
			}
		/* store the value pointed to by the address register into a general register */
		} else if( LO_BYTE( instruction ) == 0xFE ) {
			M_WORD templo, temphi;
			M_BYTE bnkIdx;
			M_BYTE regIdx;

			temp1 = 0;
			temp2 = 0;

			if( !ReadMemoryWord( processor->randomMemory, processor->addressRegister + 2, &templo ) ) {
				processor->faultCode = FAULT_RANDOM_MEMORY_READ_ERROR;
				return 0;
			}

			if( HI_BYTE( instruction ) & 0x80 ) {
				if( !ReadMemoryWord( processor->randomMemory, processor->addressRegister, &temphi ) ) {
					processor->faultCode = FAULT_RANDOM_MEMORY_READ_ERROR;
					return 0;
				}
			}

			bnkIdx = LO_NIBBLE( LO_BYTE( immediate ) );
			regIdx = HI_NIBBLE( LO_BYTE( immediate ) );
			processor->banks[ bnkIdx ].regs[ regIdx ] = MK_DWORD( templo, temphi );
		/* store a 16-bit value in memory */
		} else if( LO_BYTE( instruction ) == 0xFD ) {
			M_DWORD bias;

			bias = ( HI_BYTE( instruction ) & 0x80 ) ? 0 : 2;

			if( !WriteMemoryWord( processor->randomMemory, processor->addressRegister + bias, immediate ) ) {
				processor->faultCode = FAULT_RANDOM_MEMORY_WRITE_ERROR;
				return 0;
			}
		/* store an 8-bit value in memory */
		} else if( LO_BYTE( instruction ) == 0xFC ) {
			M_DWORD bias;

			bias = ( HI_BYTE( instruction ) & 0x80 ) ? 0 : 1;

			if( !WriteMemoryByte( processor->randomMemory, processor->addressRegister + bias, LO_BYTE( immediate ) ) ) {
				processor->faultCode = FAULT_RANDOM_MEMORY_WRITE_ERROR;
				return 0;
			}
		/* store the value of a register into memory (32-bit) */
		} else if( LO_BYTE( instruction ) == 0xFB ) {
			M_BYTE bnkIdx;
			M_BYTE regIdx;
			M_DWORD value;
			
			bnkIdx = LO_NIBBLE( LO_BYTE( immediate ) );
			regIdx = HI_NIBBLE( LO_BYTE( immediate ) );

			value = processor->banks[ bnkIdx ].regs[ regIdx ];

			if( !WriteMemoryDword( processor->randomMemory, processor->addressRegister, value ) ) {
				processor->faultCode = FAULT_RANDOM_MEMORY_WRITE_ERROR;
				return 0;
			}
		/* store the value of a register into memory (16-bit) */
		} else if( LO_BYTE( instruction ) == 0xFA ) {
			M_BYTE bnkIdx;
			M_BYTE regIdx;
			M_DWORD temp;
			M_WORD value;
			
			bnkIdx = LO_NIBBLE( LO_BYTE( immediate ) );
			regIdx = HI_NIBBLE( LO_BYTE( immediate ) );

			temp = processor->banks[ bnkIdx ].regs[ regIdx ];
			if( HI_BYTE( instruction ) & 0x80 ) {
				value = HI_WORD( temp );
			} else {
				value = LO_WORD( temp );
			}

			if( !WriteMemoryWord( processor->randomMemory, processor->addressRegister, value ) ) {
				processor->faultCode = FAULT_RANDOM_MEMORY_WRITE_ERROR;
				return 0;
			}
		/* store the value of a register into memory (8-bit) */
		} else if( LO_BYTE( instruction ) == 0xF9 ) {
			M_BYTE bnkIdx;
			M_BYTE regIdx;
			M_WORD temp;
			M_BYTE value;
			
			bnkIdx = LO_NIBBLE( LO_BYTE( immediate ) );
			regIdx = HI_NIBBLE( LO_BYTE( immediate ) );

			temp = LO_WORD( processor->banks[ bnkIdx ].regs[ regIdx ] );
			if( HI_BYTE( instruction ) & 0x80 ) {
				value = HI_BYTE( temp );
			} else {
				value = LO_BYTE( temp );
			}

			if( !WriteMemoryByte( processor->randomMemory, processor->addressRegister, value ) ) {
				processor->faultCode = FAULT_RANDOM_MEMORY_WRITE_ERROR;
				return 0;
			}
		/* move the local register to another register */
		} else if( LO_BYTE( instruction ) == 0xF8 ) {
			M_BYTE bnkIdx;
			M_BYTE regIdx;
			
			bnkIdx = LO_NIBBLE( LO_BYTE( immediate ) );
			regIdx = HI_NIBBLE( LO_BYTE( immediate ) );

			processor->banks[ bnkIdx ].regs[ regIdx ] = processor->extraRegister;
		/* load the condition registers with other registers */
		} else if( LO_BYTE( instruction ) == 0xF7 ) {
			M_BYTE bnk1, bnk2;
			M_BYTE reg1, reg2;

			bnk1 = LO_NIBBLE( LO_BYTE( immediate ) );
			reg1 = HI_NIBBLE( LO_BYTE( immediate ) );

			bnk2 = LO_NIBBLE( HI_BYTE( immediate ) );
			reg2 = HI_NIBBLE( HI_BYTE( immediate ) );

			processor->conditionRegister[ 0 ] = processor->banks[ bnk1 ].regs[ reg1 ];
			processor->conditionRegister[ 1 ] = processor->banks[ bnk2 ].regs[ reg2 ];
		/* load the stack and frame register into other registers */
		} else if( LO_BYTE( instruction ) == 0xF6 ) {
			M_BYTE bnk1, bnk2;
			M_BYTE reg1, reg2;

			bnk1 = LO_NIBBLE( LO_BYTE( immediate ) );
			reg1 = HI_NIBBLE( LO_BYTE( immediate ) );

			bnk2 = LO_NIBBLE( HI_BYTE( immediate ) );
			reg2 = HI_NIBBLE( HI_BYTE( immediate ) );

			processor->banks[ bnk1 ].regs[ reg1 ] = processor->stackPointer;
			processor->banks[ bnk2 ].regs[ reg2 ] = processor->framePointer;
		/* load the stack and frame register from other registers */
		} else if( LO_BYTE( instruction ) == 0xF5 ) {
			M_BYTE bnk1, bnk2;
			M_BYTE reg1, reg2;

			bnk1 = LO_NIBBLE( LO_BYTE( immediate ) );
			reg1 = HI_NIBBLE( LO_BYTE( immediate ) );

			bnk2 = LO_NIBBLE( HI_BYTE( immediate ) );
			reg2 = HI_NIBBLE( HI_BYTE( immediate ) );

			processor->stackPointer = processor->banks[ bnk1 ].regs[ reg1 ];
			processor->framePointer = processor->banks[ bnk2 ].regs[ reg2 ];
		/* load a register */
		} else {
			M_BYTE bnkIdx;
			M_BYTE regIdx;

			bnkIdx = HI_NIBBLE( processor->currentBanks );
			regIdx = HI_NIBBLE( LO_BYTE( instruction ) );

			if( HI_BYTE( instruction ) & 0x80 ) {
				processor->banks[ bnkIdx ].regs[ regIdx ] |= ( ( M_DWORD )immediate ) << 16;
			} else {
				processor->banks[ bnkIdx ].regs[ regIdx ] = ( M_DWORD )immediate;
			}
		}
		break;
	case OP_ADD:
		if( HI_BYTE( instruction ) & 0x80 ) {
			processor->banks[ bnk1 ].regs[ reg1 ] += ( M_DWORD )immediate;
		} else {
			processor->banks[ bnk1 ].regs[ reg1 ] += processor->banks[ bnk2 ].regs[ reg2 ];
		}
		break;
	case OP_SUB:
		if( HI_BYTE( instruction ) & 0x80 ) {
			processor->banks[ bnk1 ].regs[ reg1 ] -= ( M_DWORD )immediate;
		} else {
			processor->banks[ bnk1 ].regs[ reg1 ] -= processor->banks[ bnk2 ].regs[ reg2 ];
		}
		break;
	case OP_XOR:
		if( HI_BYTE( instruction ) & 0x80 ) {
			processor->banks[ bnk1 ].regs[ reg1 ] ^= ( M_DWORD )immediate;
		} else {
			processor->banks[ bnk1 ].regs[ reg1 ] ^= processor->banks[ bnk2 ].regs[ reg2 ];
		}
		break;
	case OP_NOT:
		if( HI_BYTE( instruction ) & 0x80 ) {
			processor->banks[ bnk1 ].regs[ reg1 ] = ( M_DWORD )~immediate;
		} else {
			processor->banks[ bnk1 ].regs[ reg1 ] = ~processor->banks[ bnk1 ].regs[ reg1 ];
		}
		break;
	case OP_MUL:
		if( HI_BYTE( instruction ) & 0x80 ) {
			processor->banks[ bnk1 ].regs[ reg1 ] *= ( M_DWORD )immediate;
		} else {
			processor->banks[ bnk1 ].regs[ reg1 ] *= processor->banks[ bnk2 ].regs[ reg2 ];
		}
		break;
	case OP_DIV:
		{
			M_DWORD x, y;

			x = processor->banks[ bnk1 ].regs[ reg1 ];
			if( HI_BYTE( instruction ) & 0x80 ) {
				y = ( M_DWORD )immediate;
			} else {
				y = processor->banks[ bnk2 ].regs[ reg2 ];
			}

			if( !y ) {
				processor->faultCode = FAULT_DIVIDE_BY_ZERO;
				return 0;
			}

			processor->banks[ bnk1 ].regs[ reg1 ] = x/y;
			processor->extraRegister = x%y;
		}
		break;
	case OP_IMUL:
		{
			M_DWORD_S x, y;

			x = ( M_DWORD_S )processor->banks[ bnk1 ].regs[ reg1 ];
			if( HI_BYTE( instruction ) & 0x80 ) {
				y = ( M_DWORD_S )( M_WORD_S )immediate;
			} else {
				y = ( M_DWORD_S )processor->banks[ bnk2 ].regs[ reg2 ];
			}

			processor->banks[ bnk1 ].regs[ reg1 ] = ( M_DWORD )( x*y );
		}
		break;
	case OP_IDIV:
		{
			M_DWORD_S x, y;

			x = ( M_DWORD_S )processor->banks[ bnk1 ].regs[ reg1 ];
			if( HI_BYTE( instruction ) & 0x80 ) {
				y = ( M_DWORD_S )( M_WORD_S )immediate;
			} else {
				y = ( M_DWORD_S )processor->banks[ bnk2 ].regs[ reg2 ];
			}

			if( !y ) {
				processor->faultCode = FAULT_DIVIDE_BY_ZERO;
				return 0;
			}

			processor->banks[ bnk1 ].regs[ reg1 ] = ( M_DWORD )( x/y );
			processor->extraRegister = ( M_DWORD )( x%y );
		}
		break;
	case OP_JUMP:
		{
			int doJump;
			M_DWORD x, y;
			M_DWORD_S a, b;

			x = processor->conditionRegister[ 0 ];
			y = processor->conditionRegister[ 1 ];

			a = ( M_DWORD_S )x;
			b = ( M_DWORD_S )y;

			doJump = 0;
			switch( LO_NIBBLE( LO_BYTE( instruction ) ) ) {
			case COND_ALWAYS:				doJump = 1;			break;
			case COND_NEVER:				doJump = 0;			break;
			case COND_EQUAL:				doJump = x == y;	break;
			case COND_NOT_EQUAL:			doJump = x != y;	break;
			case COND_LESS:					doJump = x <  y;	break;
			case COND_GREATER:				doJump = x >  y;	break;
			case COND_LESS_EQUAL:			doJump = x <= y;	break;
			case COND_GREATER_EQUAL:		doJump = x >= y;	break;
			case COND_SIGNED_LESS:			doJump = a <  b;	break;
			case COND_SIGNED_GREATER:		doJump = a >  b;	break;
			case COND_SIGNED_LESS_EQUAL:	doJump = a <= b;	break;
			case COND_SIGNED_GREATER_EQUAL:	doJump = a >= b;	break;
			}

			if( doJump ) {
				if( HI_BYTE( instruction ) & 0x80 ) {
					pc -= 4; /*go to start of this instruction*/
					pc += ( ( M_DWORD_S )( M_WORD_S )immediate )*2;
				} else {
					pc = processor->addressRegister;
				}
			}
		}
		break;
	case OP_CALL:
		{
			int doJump;
			M_DWORD x, y;
			M_DWORD_S a, b;

			x = processor->conditionRegister[ 0 ];
			y = processor->conditionRegister[ 1 ];

			a = ( M_DWORD_S )x;
			b = ( M_DWORD_S )y;

			doJump = 0;
			switch( LO_NIBBLE( LO_BYTE( instruction ) ) ) {
			case COND_ALWAYS:				doJump = 1;			break;
			case COND_NEVER:				doJump = 0;			break;
			case COND_EQUAL:				doJump = x == y;	break;
			case COND_NOT_EQUAL:			doJump = x != y;	break;
			case COND_LESS:					doJump = x <  y;	break;
			case COND_GREATER:				doJump = x >  y;	break;
			case COND_LESS_EQUAL:			doJump = x <= y;	break;
			case COND_GREATER_EQUAL:		doJump = x >= y;	break;
			case COND_SIGNED_LESS:			doJump = a <  b;	break;
			case COND_SIGNED_GREATER:		doJump = a >  b;	break;
			case COND_SIGNED_LESS_EQUAL:	doJump = a <= b;	break;
			case COND_SIGNED_GREATER_EQUAL:	doJump = a >= b;	break;
			}

			if( doJump ) {
				if( !WriteMemoryDword( processor->randomMemory, processor->stackPointer, processor->programCounter ) ) {
					processor->faultCode = FAULT_RANDOM_MEMORY_WRITE_ERROR;
					return 0;
				}

				processor->stackPointer -= 4;

				if( HI_BYTE( instruction ) & 0x80 ) {
					pc -= 4; /*go to start of this instruction*/
					pc += ( ( M_DWORD_S )( M_WORD_S )immediate )*2;
				} else {
					pc = processor->addressRegister;
				}
			}
		}
		break;
	case OP_RETURN:
		{
			int doJump;
			M_DWORD x, y;
			M_DWORD_S a, b;

			x = processor->conditionRegister[ 0 ];
			y = processor->conditionRegister[ 1 ];

			a = ( M_DWORD_S )x;
			b = ( M_DWORD_S )y;

			doJump = 0;
			switch( LO_NIBBLE( LO_BYTE( instruction ) ) ) {
			case COND_ALWAYS:				doJump = 1;			break;
			case COND_NEVER:				doJump = 0;			break;
			case COND_EQUAL:				doJump = x == y;	break;
			case COND_NOT_EQUAL:			doJump = x != y;	break;
			case COND_LESS:					doJump = x <  y;	break;
			case COND_GREATER:				doJump = x >  y;	break;
			case COND_LESS_EQUAL:			doJump = x <= y;	break;
			case COND_GREATER_EQUAL:		doJump = x >= y;	break;
			case COND_SIGNED_LESS:			doJump = a <  b;	break;
			case COND_SIGNED_GREATER:		doJump = a >  b;	break;
			case COND_SIGNED_LESS_EQUAL:	doJump = a <= b;	break;
			case COND_SIGNED_GREATER_EQUAL:	doJump = a >= b;	break;
			}

			if( doJump ) {
				M_DWORD sp;

				sp = processor->stackPointer + 4;
				if( HI_BYTE( instruction ) & 0x80 ) {
					sp += ( ( M_DWORD )immediate )*4;
				}

				if( !ReadMemoryDword( processor->randomMemory, sp, &pc ) ) {
					processor->faultCode = FAULT_RANDOM_MEMORY_READ_ERROR;
					return 0;
				}

				processor->stackPointer = sp;
			}
		}
		break;
	case OP_SHL:
		if( HI_BYTE( instruction ) & 0x80 ) {
			processor->banks[ bnk1 ].regs[ reg1 ] <<= ( M_DWORD )immediate;
		} else {
			processor->banks[ bnk1 ].regs[ reg1 ] <<= processor->banks[ bnk2 ].regs[ reg2 ];
		}
		break;
	case OP_SHR:
		if( HI_BYTE( instruction ) & 0x80 ) {
			processor->banks[ bnk1 ].regs[ reg1 ] >>= ( M_DWORD )immediate;
		} else {
			processor->banks[ bnk1 ].regs[ reg1 ] >>= processor->banks[ bnk2 ].regs[ reg2 ];
		}
		break;
	default:
		processor->faultCode = FAULT_ILLEGAL_INSTRUCTION;
		return 0;
	}

	processor->programCounter = pc;

	return 1;
}
