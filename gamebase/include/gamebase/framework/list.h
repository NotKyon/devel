#ifndef GB_FRAMEWORK_LIST_H
#define GB_FRAMEWORK_LIST_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Linked Lists */
struct gb_node_s;
struct gb_list_s;
typedef void(*gb_fnRemoveNode_t)(void *obj);
typedef struct gb_node_s {
	void *obj;
	struct gb_node_s *prev, *next;
	struct gb_list_s *list;
} gb_node_t;
typedef struct gb_list_s {
	struct gb_node_s *head, *tail;
	gb_fnRemoveNode_t rmcb;
} gb_list_t;

void gbInitList(gb_list_t *list);
void gbFiniList(gb_list_t *list);

void gbSetListRemoveCallback(gb_list_t *list, gb_fnRemoveNode_t rmcb);
gb_fnRemoveNode_t gbGetListRemoveCallback(gb_list_t *list);

void gbInitNode(gb_node_t *node, void *obj);
void gbFiniNode(gb_node_t *node);

void gbSetNodeObject(gb_node_t *node, void *obj);
void *gbNodeObject(gb_node_t *node);

void gbAddFirst(gb_list_t *list, gb_node_t *node);
void gbAddLast(gb_list_t *list, gb_node_t *node);
void gbInsertBefore(gb_node_t *node, gb_node_t *before);
void gbInsertAfter(gb_node_t *node, gb_node_t *after);

gb_list_t *gbNodeList(gb_node_t *node);
void gbUnlinkNode(gb_node_t *node);
void gbRemoveNode(gb_node_t *node);

gb_node_t *gbFirstNode(gb_list_t *list);
gb_node_t *gbLastNode(gb_list_t *list);
gb_node_t *gbNodeBefore(gb_node_t *node);
gb_node_t *gbNodeAfter(gb_node_t *node);

void *gbFirst(gb_list_t *list);
void *gbLast(gb_list_t *list);
void *gbBefore(gb_node_t *node);
void *gbAfter(gb_node_t *node);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
