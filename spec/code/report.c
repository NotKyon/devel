#include "report.h"

#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

void ReportV(const char *type, const char *file, int line, const char *func,
const char *message, va_list args) {
	int e;

	e = errno;

	fflush(stdout);

	if (file) {
		fprintf(stderr, "[%s", file);
		if (line)
			fprintf(stderr, "(%i)", line);

		if (func)
			fprintf(stderr, " %s", func);

		fprintf(stderr, "] ");
	} else if(func)
		fprintf(stderr, "%s: ", func);

	fprintf(stderr, "%s: ", type);

	vfprintf(stderr, message, args);

	if (e)
		fprintf(stderr, ": %s (%i)", strerror(e), e);

	fprintf(stderr, "\n");
	fflush(stderr);
}
void Report(const char *type, const char *file, int line, const char *func,
const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(type, file, line, func, message, args);
	va_end(args);
}

void Warn(const char *file, int line, const char *func, const char *message,
...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_WRN, file, line, func, message, args);
	va_end(args);
}
void Error(const char *file, int line, const char *func, const char *message,
...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_ERR, file, line, func, message, args);
	va_end(args);
}

void WarnMessage(const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_WRN, (const char *)0, 0, (const char *)0, message, args);
	va_end(args);
}
void ErrorMessage(const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_ERR, (const char *)0, 0, (const char *)0, message, args);
	va_end(args);
}

void WarnFile(const char *file, int line, const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_WRN, file, line, (const char *)0, message, args);
	va_end(args);
}
void ErrorFile(const char *file, int line, const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_ERR, file, line, (const char *)0, message, args);
	va_end(args);
}

void ErrorFileExit(const char *file, int line, const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_ERR, file, line, (const char *)0, message, args);
	va_end(args);

	exit(EXIT_FAILURE);
}
void ErrorExit(const char *message, ...) {
	va_list args;

	va_start(args, message);
	ReportV(TYPE_ERR, (const char *)0, 0, (const char *)0, message, args);
	va_end(args);

	exit(EXIT_FAILURE);
}
