#ifndef GB_BASE_MEMORY_H
#define GB_BASE_MEMORY_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Memory Functions */
void *gbMemAlloc(size_t n);
void *gbMemFree(void *p);
void *gbMemExtend(void *p, size_t n);

void gbMemClear(void *p, size_t n);
void gbMemFill(void *p, char s, size_t n);
void gbMemCopy(void *dst, const void *src, size_t n);
void gbMemMove(void *dst, const void *src, size_t n);

void *gbMemAllocZero(size_t n);

char *gbDuplicateN(const char *src, size_t srcn);
char *gbDuplicate(const char *src);
char *gbCopyN(char *dst, const char *src, size_t srcn);
char *gbCopy(char *dst, const char *src);
char *gbAppendNChar(char *dst, const char *src, size_t srcn, char ch);
char *gbAppendN(char *dst, const char *src, size_t srcn);
char *gbAppend(char *dst, const char *src);

char *gbTrimAppendChar(char *dst, const char *src, char ch);
char *gbTrimAppend(char *dst, const char *src);

char *gbStrCpy(char *dst, size_t dstn, const char *src);
char *gbStrCpyN(char *dst, size_t dstn, const char *src, size_t srcn);

char *gbStrCat(char *dst, size_t dstn, const char *src);
char *gbStrCatN(char *dst, size_t dstn, const char *src, size_t srcn);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
