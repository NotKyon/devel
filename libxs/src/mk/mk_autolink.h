#ifndef MK_AUTOLINK_H
#define MK_AUTOLINK_H

#include "mk_types.h"
#include "mk_lib.h"

typedef struct autolink_s {
	char *header[kNumProjSys];
	char *lib;

	struct autolink_s *prev, *next;
} *autolink_t;
extern autolink_t g_al_head;
extern autolink_t g_al_tail;

autolink_t NewAutolink();
void DeleteAutolink(autolink_t al);
void DeleteAllAutolinks();

void SetAutolinkHeader(autolink_t al, int sys, const char *header);
void SetAutolinkLib(autolink_t al, const char *libname);

const char *AutolinkHeader(autolink_t al, int sys);
const char *AutolinkLib(autolink_t al);

autolink_t AutolinkBefore(autolink_t al);
autolink_t AutolinkAfter(autolink_t al);
autolink_t FirstAutolink();
autolink_t LastAutolink();

autolink_t FindAutolink(int sys, const char *header);

#endif
