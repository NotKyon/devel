#include <stdio.h>

int main(int argc, char **argv) {
	int i;

	if (argc < 2)
		return 1;

	printf("Hello, %s!\n", argv[1]);
	return 0;
}
