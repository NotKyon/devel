#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <Windows.h>

HBITMAP load_bmp( const char *pszFilename )
{
	HANDLE h;
	char szFullPath[ 512 ];

	if( !GetFullPathNameA( pszFilename, sizeof( szFullPath ), szFullPath, NULL ) ) {
		fprintf( stderr, "ERROR: Could not get full path of bitmap file \"%s\"\n", pszFilename );
		exit( EXIT_FAILURE );
	}

	h = LoadImageA( NULL, szFullPath, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );

	if( !h ) {
		fprintf( stderr, "ERROR: Failed to load bitmap \"%s\" (winerr=0x%.8X)\n", szFullPath, ( unsigned int )GetLastError() );
		exit( EXIT_FAILURE );
	}

	return ( HBITMAP )h;
}
HBITMAP unload_bmp( HBITMAP hBmp )
{
	DeleteObject( ( HGDIOBJ )hBmp );
	return ( HBITMAP )NULL;
}

void save_bmp_as_pbm( HBITMAP hBmp, const char *pszDstFilename )
{
	FILE *fp;
	BITMAP bmp;
	SIZE res;
	int x, y;
	HDC disp;

	disp = CreateCompatibleDC( NULL );
	if( !disp ) {
		fprintf( stderr, "ERROR: Failed to create display-compatible device context\n" );
		exit( EXIT_FAILURE );
	}
	
	SelectObject( disp, ( HGDIOBJ )hBmp );

	if( !GetObject( hBmp, sizeof( BITMAP ), &bmp ) ) {
		fprintf( stderr, "ERROR: Failed to get bitmap information\n" );
		exit( EXIT_FAILURE );
	}

	res.cx = bmp.bmWidth;
	res.cy = bmp.bmHeight;

	if( !res.cx || !res.cy ) {
		fprintf( stderr, "ERROR: Invalid bitmap dimensions\n" );
		exit( EXIT_FAILURE );
	}

	printf( "Bitmap res: %i, %i\n", ( int )res.cx, ( int )res.cy );

	fp = fopen( pszDstFilename, "wb" );
	if( !fp ) {
		fprintf( stderr, "ERROR: Failed to open pbm for writing \"%s\"\n", pszDstFilename );
		exit( EXIT_FAILURE );
	}

	fprintf( fp, "P6 %u %u 255\n", ( unsigned int )res.cx, ( unsigned int )res.cy );
	for( y = 0; y < res.cy; ++y ) {
		for( x = 0; x < res.cx; ++x ) {
			COLORREF color;
			unsigned char rgb[ 3 ];

			color = GetPixel( disp, x, y );
			rgb[0] = ( unsigned char )( ( color & 0x000000FF ) >>  0 );
			rgb[1] = ( unsigned char )( ( color & 0x0000FF00 ) >>  8 );
			rgb[2] = ( unsigned char )( ( color & 0x00FF0000 ) >> 16 );

			if( fwrite( &rgb[ 0 ], 3, 1, fp ) != 1 ) {
				fprintf( stderr, "ERROR: Failed to write pixel 0x%.6X at (%i,%i)\n", ( unsigned int )color, x, y );
				exit( EXIT_FAILURE );
			}
		}
	}

	fclose( fp );
	fp = NULL;

	DeleteDC( disp );
	disp = NULL;
}

void do_bmp_to_pbm_conv( const char *pszInputBMPFilename, const char *pszOutputPBMFilename )
{
	HBITMAP hBmp;

	hBmp = load_bmp( pszInputBMPFilename );
	save_bmp_as_pbm( hBmp, pszOutputPBMFilename );
	hBmp = unload_bmp( hBmp );
}

int main( int argc, char **argv )
{
	if( argc < 3 ) {
		printf( "Usage:\n  bmp2pbm input.bmp output.pbm\n\n" );
		return EXIT_FAILURE;
	}
	if( argc > 3 ) {
		fprintf( stderr, "WARNING: Too many arguments; ignoring \"%s\" and after\n", argv[ 3 ] );
	}

	do_bmp_to_pbm_conv( argv[ 1 ], argv[ 2 ] );
	return EXIT_SUCCESS;
}
