﻿#ifndef EMBER_EMG_H
#define EMBER_EMG_H

/*
 * Ember Geometry (.emg) Specification - 1.00
 * Copyright (C) 2011 Axia Entertainment, Aaron J. Miller
 *
 * All values encoded in little endian.
 *
 * char = signed 8-bit text character
 * short = signed 16-bit integer
 * int = signed 32-bit integer
 */

/* polynomial to use whenever a checksum is requested */
#define EMG_POLYNOMIAL 0xCAFEBABE

/* primitive topologies */
#define EMG_TRIANGLE_LIST_TOPOLOGY 1
#define EMG_TRIANGLE_STRIP_TOPOLOGY 2

/* geometry chunk flags */
#define EMG_VTX_POSITION_BIT 0x01
#define EMG_VTX_NORMAL_BIT 0x02
#define EMG_VTX_DIFFUSE_BIT 0x04
#define EMG_VTX_SPECULAR_BIT 0x08
#define EMG_VTX_BINORMAL_BIT 0x10
#define EMG_VTX_TANGENT_BIT 0x20
#define EMG_VTX_TEX3D_BIT 0x40 /* uvw, not uv */

/* universally unique identifier: not part of the format, just a data type */
#ifndef __stUUID_defined__
#define __stUUID_defined__ 1
struct stUUID {
	unsigned int A;
	unsigned short B;
	unsigned short C;
	unsigned char D[ 8 ];
};
#endif

/* two-dimensional vector */
#ifndef __stVec2_defined__
#define __stVec2_defined__ 1
struct stVec2 {
	float x, y;
};
#endif

/* three-dimensional vector */
#ifndef __stVec3_defined__
#define __stVec3_defined__ 1
struct stVec3 {
	float x, y, z;
};
#endif

/* quaternion */
#ifndef __stQuat_defined__
#define __stQuat_defined__ 1
struct stQuat {
	float w, x, y, z;
};
#endif

/* header: always occurs first in the format */
struct stHeaderEmg {
	char tag[ 13 ]; /* 'EmberGeometry' */
	short ver;      /* 100 */

	unsigned int size;    /* size, in bytes, of the file (including header) */
	unsigned int nchunks; /* number of chunks in the file */
};

/* chunk header: each chunk occurs directly after the header */
struct stChunkEmg {
	struct stUUID kind; /* UUID of the chunk kind */
	unsigned int size;  /* size, in bytes, of the chunk data */

	unsigned int checksum; /* CRC32 checksum of the data following */
};

/* {E2FF8BD5-ED84-4823-BE09-19D30029B944}: geometry chunk */
struct stGeometryChunkEmg {
	unsigned int nvtx; /* number of vertices */
	unsigned int nidx; /* number of indices */

	unsigned int nbones; /* number of bones */
	unsigned int nkeys;  /* number of key frames */

	unsigned char topology;   /* primitive topology */
	unsigned char flags;      /* combination of EMG_VTX bits */
	unsigned char ntexcoords; /* number of texture coordinates */
};
/* vertices directly following the geometry chunk, indices directly follow */
struct stVertexEmg {
	float x, y, z;         /* EMG_VTX_POSITION_BIT */
	float nx, ny, nz;      /* EMG_VTX_NORMAL_BIT */
	unsigned int diffuse;  /* EMG_VTX_DIFFUSE_BIT */
	unsigned int specular; /* EMG_VTX_SPECULAR_BIT */
	stVec3 texcoords[ 8 ]; /* [ntexcoords] */
	float bx, by, bz;      /* EMG_VTX_BINORMAL_BIT */
	float tx, ty, tz;      /* EMG_VTX_TANGENT_BIT */
};

/* {32B276BE-6791-44C9-A8ED-A4D49B4555D6}: transform */
struct stTransformEmg {
	struct stVec3 translation;
	struct stQuat rotation;
	struct stVec3 scale;
};

#endif
