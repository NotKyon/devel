#ifndef AXTEK_THREADING_CPU_H
#define AXTEK_THREADING_CPU_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

__AX_TASK_NS_INIT__

#if AXTEK_ARCH_X86
namespace x86 {

inline void cpuid(u32 f, u32 regs[4]) {
#if __GNUC__ || __clang__
	__asm__ __volatile__
	(
		"cpuid"
	  :	"=a" (regs[0]),
		"=b" (regs[1]),
		"=c" (regs[2]),
		"=d" (regs[3])
	  :	"a" (f),
	    "c" (0) //set ECX to zero for function 4
	);
#else
# if _MSC_VER || __INTEL_COMPILER
	__cpuid((int *)regs, (int)f);
# else
	__asm {
		mov eax, [f]
		xor ecx, ecx
		cpuid
		mov [regs +  0], eax
		mov [regs +  4], ebx
		mov [regs +  8], ecx
		mov [regs + 12], edx
	}
# endif
#endif
}

inline void get_cpu_count(u32 &cores, u32 &threads) {
	u32 regs[4], features;
	u32 vendor[4];
	bool hyperthreading;

	// vendor
	cpuid(0, vendor);
	vendor[3] = 0;

	// features
	cpuid(1, regs);
	features = regs[3];
	threads = (regs[1] >> 16) & 0xFF;
	cores = threads;

	// GenuineIntel
	if(vendor[0]==0x756E6547
	&& vendor[1]==0x49656E69
	&& vendor[2]==0x6C65746E) {
		cpuid(4, regs);
		cores = ((regs[0] >> 26) & 0x3F) + 1;
	} else
	// AuthenticAMD
	if(vendor[0]==0x68747541
	&& vendor[1]==0x69746E65
	&& vendor[2]==0x444D4163) {
		x86::cpuid(0x80000008, regs);
		cores = (regs[2] & 0xFF) + 1;
	}

	// check hyperthreading
	if (!(hyperthreading = (features & (1 << 28)) && (cores < threads)))
		threads = cores; //HT must be enabled too
}

}
#endif

inline u32 get_cpu_count() {
#if _WIN32
	SYSTEM_INFO si;

	GetSystemInfo(&si);

	return si.dwNumberOfProcessors;
#elif __linux__
	int count;

	if unlikely ((count = sysconf(_SC_NPROCESSORS_ONLN)) < 1)
		count = 1;

	return (u32)count;
#else
# if MK_ARCH_X86 || MK_ARCH_AMD64
	u32 cores, threads;

	x86::get_cpu_count(cores, threads);

	return threads;
# else
	/* TODO: Find the count */
	return 1;
# endif
#endif
}

__AX_TASK_NS_FINI__

#endif
