#include <ember/config.h>
#include <ember/opengl.h>
#include <string.h>
#include <stdio.h>

static int g_gl_class_init = 0;
static HWND g_gl_wnd = (HWND)0;
static HDC g_gl_dc = (HDC)0;
static HGLRC g_gl_rc = (HGLRC)0;

static int g_gl_running = 1;

static int g_gl_major = 4, g_gl_minor = 2, g_gl_compat = 0;

static int g_gl_wnd_width = 800, g_gl_wnd_height = 600;
static int g_gl_fullscreen = 0, g_gl_visible = 1;

HWND egl_get_hwnd__() { return g_gl_wnd; }
HDC egl_get_hdc__() { return g_gl_dc; }
HGLRC egl_get_hglrc__() { return g_gl_rc; }

static int __egl_is_valid_version(int major, int minor) {
  switch(major) {
  case 1:
    return (minor>=1&&minor<=5)?1:0;
  case 2:
    return (minor>=0&&minor<=1)?1:0;
  case 3:
    return (minor>=0&&minor<=3)?1:0;
  case 4:
    return (minor>=0&&minor<=2)?1:0;
  default:
    return 0;
  }
}
static int __egl_highest_minor(int major) {
  switch(major) {
  case 1:
    return 5;
  case 2:
    return 1;
  case 3:
    return 3;
  case 4:
    return 2;
  default:
    return -1;
  }
}

int egl_set_param(EGLParam h, int v) {
  int t;
  switch(h) {
  case EGLParam_MajorVersion:
    if ((t=__egl_highest_minor(v))>=0) {
      g_gl_major = v;
      g_gl_minor = t;
      return 1;
    }
    return 0;
  case EGLParam_MinorVersion:
    if (__egl_is_valid_version(g_gl_major, v)) {
      g_gl_minor = v;
      return 1;
    }
    return 0;
  case EGLParam_Profile:
    if (v==EGLProfile_Core
	|| v==EGLProfile_Compatibility) {
      g_gl_compat = v==EGLProfile_Compatibility?1:0;
      return 1;
    }
    return 0;
  case EGLParam_DisplayWidth:
    g_gl_wnd_width = v;
    return 1;
  case EGLParam_DisplayHeight:
    g_gl_wnd_height = v;
    return 1;
  case EGLParam_Fullscreen:
    g_gl_fullscreen = v?1:0;
    return 1;
  case EGLParam_WindowVisible:
    g_gl_visible = v?1:0;
    return 1;
  default:
    return 0;
  }
}
int egl_get_param(EGLParam h) {
  switch(h) {
  case EGLParam_MajorVersion:
    return g_gl_major;
  case EGLParam_MinorVersion:
    return g_gl_minor;
  case EGLParam_Profile:
    return g_gl_compat?EGLProfile_Compatibility:EGLProfile_Core;
  case EGLParam_DisplayWidth:
    return g_gl_wnd_width;
  case EGLParam_DisplayHeight:
    return g_gl_wnd_height;
  case EGLParam_Fullscreen:
    return g_gl_fullscreen;
  case EGLParam_WindowVisible:
    return g_gl_visible;
  default:
    return 0;
  }
}

static void __egl_size(int w, int h) {
  g_gl_wnd_width = w;
  g_gl_wnd_height = h<1?1:h;
}
static void __egl_paint() {
  glClearColor(0.33f, 0.41f, 0.47f, 1.0f); /* cadet */
  glClear(GL_COLOR_BUFFER_BIT);

  SwapBuffers(g_gl_dc);
  ValidateRect(g_gl_wnd, 0);
}
static LRESULT CALLBACK __egl_msg_proc(HWND h, UINT m, WPARAM w, LPARAM l) {
  switch(m) {
  case WM_DESTROY:
    PostQuitMessage(0);
    g_gl_running = 0;
    break;
    
  case WM_SIZE:
    __egl_size((int)LOWORD(l), (int)HIWORD(l));
    break;
    
  case WM_ERASEBKGND:
    return 1;
  case WM_PAINT:
    __egl_paint();
    return 0;
  }

  return DefWindowProcA(h, m, w, l);
}
static int __egl_init_window_class() {
  WNDCLASSEXA wc;

  if (g_gl_class_init)
    return 1;

  wc.cbSize = sizeof(wc);
  wc.style = CS_OWNDC|CS_HREDRAW|CS_VREDRAW;
  wc.lpfnWndProc = __egl_msg_proc;
  wc.cbClsExtra = 0;
  wc.cbWndExtra = 0;
  wc.hInstance = GetModuleHandleA(0);
  wc.hIcon = 0; /* TODO */
  wc.hCursor = LoadCursorA(0, IDC_ARROW);
  wc.hbrBackground = (HBRUSH)0;
  wc.lpszMenuName = (const char *)0;
  wc.lpszClassName = "__egl_wnd__";
  wc.hIconSm = 0; /* TODO */

  if (!RegisterClassExA(&wc))
    return 0;

  g_gl_class_init = 1;
  return 1;
}
/*static int __egl_check_version(int major, int minor) {
	char buff[256];

	snprintf(buff, sizeof(buff), "GL_VERSION_%d_%d", major, minor);
	return wglewIsSupported(buff);
}*/
static int __egl_init_window() {
  PIXELFORMATDESCRIPTOR pfd;
  HGLRC trc;
  DWORD exstyle, style;
  RECT rc;
  int attribs[10];
  int pf;
  int x, y, w, h;

  if (!__egl_init_window_class())
    return 0;

  w = g_gl_wnd_width;
  h = g_gl_wnd_height;

  if (!w)
    w = GetSystemMetrics(SM_CXSCREEN);
  if (!h)
    h = GetSystemMetrics(SM_CYSCREEN);

  if (g_gl_fullscreen) {
    exstyle = WS_EX_TOPMOST;
    style = WS_POPUP;
    x = 0;
    y = 0;
  } else {
    exstyle = 0;
    style = WS_OVERLAPPED|WS_SYSMENU|WS_MINIMIZEBOX|WS_MAXIMIZEBOX|WS_SIZEBOX;
    x = GetSystemMetrics(SM_CXSCREEN)/2-w/2;
    y = GetSystemMetrics(SM_CYSCREEN)/2-h/2;
  }

  rc.left = x; rc.top = y;
  rc.right = x+w; rc.bottom = y+h;
  AdjustWindowRectEx(&rc, style, 0, exstyle);

  if (!(g_gl_wnd = CreateWindowExA
	(
	 exstyle,
	 "__egl_wnd__",
	 "",
	 style,
	 rc.left,
	 rc.top,
	 rc.right-rc.left,
	 rc.bottom-rc.top,
	 (HWND)0,
	 (HMENU)0,
	 GetModuleHandleA(0),
	 (void *)0
	 )))
    return 0;

  g_gl_dc = GetDC(g_gl_wnd);

  pfd.nSize = sizeof(pfd);
  pfd.nVersion = 1;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
  pfd.iPixelType = PFD_TYPE_RGBA;
  pfd.cColorBits = 24;
  pfd.cRedBits = 0;
  pfd.cRedShift = 0;
  pfd.cGreenBits = 0;
  pfd.cGreenShift = 0;
  pfd.cBlueBits = 0;
  pfd.cBlueShift = 0;
  pfd.cAlphaBits = 0;
  pfd.cAlphaShift = 0;
  pfd.cAccumBits = 0;
  pfd.cAccumRedBits = 0;
  pfd.cAccumGreenBits = 0;
  pfd.cAccumBlueBits = 0;
  pfd.cAccumAlphaBits = 0;
  pfd.cDepthBits = 24;
  pfd.cStencilBits = 8;
  pfd.cAuxBuffers = 0;
  pfd.iLayerType = PFD_MAIN_PLANE;
  pfd.bReserved = 0;
  pfd.dwLayerMask = 0;
  pfd.dwVisibleMask = 0;
  pfd.dwDamageMask = 0;

  pf = ChoosePixelFormat(g_gl_dc, &pfd);
  SetPixelFormat(g_gl_dc, pf, &pfd);

  g_gl_rc = wglCreateContext(g_gl_dc);
  wglMakeCurrent(g_gl_dc, g_gl_rc);

  glewInit();

  fprintf(stdout, "g_gl_major: %d\n", g_gl_major);
  fprintf(stdout, "g_gl_minor: %d\n", g_gl_minor);
  fprintf(stdout, "g_gl_comp.: %d\n", g_gl_compat);
  fprintf(stdout, "\n");
  fflush(stdout);

  if (g_gl_major>=3) {
    if (wglewIsSupported("WGL_ARB_create_context")) {
      attribs[0] = WGL_CONTEXT_MAJOR_VERSION_ARB;
      attribs[1] = WGL_CONTEXT_MINOR_VERSION_ARB;
      attribs[2] = g_gl_major;
      attribs[3] = g_gl_minor;
      attribs[4] = WGL_CONTEXT_FLAGS_ARB;
      attribs[5] = g_gl_compat!=0?0:WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB;
      attribs[6] = WGL_CONTEXT_PROFILE_MASK_ARB;
      attribs[7] = g_gl_compat!=0?WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB:
	WGL_CONTEXT_CORE_PROFILE_BIT_ARB;
      attribs[8] = 0;
      attribs[9] = 0;

      if ((trc = wglCreateContextAttribsARB(g_gl_dc, 0, attribs))!=0) {
	wglMakeCurrent(0, 0);
	wglDeleteContext(g_gl_rc);

	g_gl_rc = trc;
	wglMakeCurrent(g_gl_dc, g_gl_rc);
      }
    }
  }

  if (g_gl_visible) {
    ShowWindow(g_gl_wnd, SW_SHOW);
    UpdateWindow(g_gl_wnd);
  }

  sscanf
    (
     (const char *)glGetString(GL_VERSION), 
     "%d.%d",
     &g_gl_major, &g_gl_minor
     );

  return 1;
}
static int __egl_reinit_window() {
  DWORD exstyle, style;
  RECT rc;
  int x, y, w, h;

  w = g_gl_wnd_width;
  h = g_gl_wnd_height;

  if (!w)
    w = GetSystemMetrics(SM_CXSCREEN);
  if (!h)
    h = GetSystemMetrics(SM_CYSCREEN);

  if (g_gl_fullscreen) {
    exstyle = WS_EX_TOPMOST;
    style = WS_POPUP;
    x = 0;
    y = 0;
  } else {
    exstyle = 0;
    style = WS_OVERLAPPED|WS_SYSMENU|WS_MINIMIZEBOX|WS_MAXIMIZEBOX|WS_SIZEBOX;
    x = GetSystemMetrics(SM_CXSCREEN)/2-w/2;
    y = GetSystemMetrics(SM_CYSCREEN)/2-h/2;
  }

  rc.left = x; rc.top = y;
  rc.right = x+w; rc.bottom = y+h;
  AdjustWindowRectEx(&rc, style, 0, exstyle);

  SetWindowLongPtrA(g_gl_wnd, GWL_EXSTYLE, (LONG_PTR)exstyle);
  SetWindowLongPtrA(g_gl_wnd, GWL_STYLE, (LONG_PTR)style);

  if (!SetWindowPos
      (
       g_gl_wnd,
       HWND_TOP,
       rc.left,
       rc.top,
       rc.right-rc.left, rc.bottom-rc.top,
       SWP_FRAMECHANGED
       ))
    return 0;

  if (g_gl_visible) {
    ShowWindow(g_gl_wnd, SW_SHOW);
    UpdateWindow(g_gl_wnd);
  } else
    ShowWindow(g_gl_wnd, SW_HIDE);

  return 1;
}
static void __egl_deinit_window() {
  wglMakeCurrent(0, 0);
  wglDeleteContext(g_gl_rc);
  ReleaseDC(g_gl_wnd, g_gl_dc);
  DestroyWindow(g_gl_wnd);
  UnregisterClassA("__egl_wnd__", GetModuleHandleA(0));
  g_gl_class_init = 0;
}

int egl_init() {
  if (g_gl_wnd)
    return __egl_reinit_window();

  return __egl_init_window();
}
void egl_deinit() {
  if (!g_gl_wnd)
    return;

  __egl_deinit_window();
}
void egl_update() {
  MSG m;

  while(PeekMessageA(&m, 0, 0, 0, PM_REMOVE)) {
    if (m.message==WM_QUIT)
      g_gl_running = 0;

    TranslateMessage(&m);
    DispatchMessageA(&m);
  }
}
int egl_running() {
  return g_gl_running;
}
void egl_set_running(int r) {
  g_gl_running = r;
}

void egl_flip() {
  InvalidateRect(g_gl_wnd, 0, 0);
}

void egl_setTitle(const char *title) {
  SetWindowTextA(g_gl_wnd, title);
}
const char *egl_title() {
  static char buff[256];

  GetWindowTextA(g_gl_wnd, buff, sizeof(buff));
  return (const char *)buff;
}
