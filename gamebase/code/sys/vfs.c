#include <gamebase/sys/fs.h>

#include <gamebase/base/report.h>
#include <gamebase/base/runtime.h>

#include <gamebase/framework/list.h>
#include <gamebase/framework/byteorder.h>
#include <gamebase/framework/dictionary.h>

#include <string.h>

#define MAX_PROTO_LIST 512
#define MAX_PROTO 32

/* -------------------------------------------------------------------------- */

struct VFS {
	gb_vfsDriverDesc_t desc;
	gb_node_t prioLink;
	gb_node_t link;
};

struct File {
	struct VFS *vfs;
	gb_uint_t openMode;
	gb_node_t link;
};
struct Dir {
	struct VFS *vfs;
	gb_node_t link;
};

/* -------------------------------------------------------------------------- */

static gb_handleType_t g_vfsBaseHandleType = (gb_handleType_t)NULL;
static gb_handleType_t g_filBaseHandleType = (gb_handleType_t)NULL;
static gb_handleType_t g_dirBaseHandleType = (gb_handleType_t)NULL;

static gb_dictionary_t g_vfsDc;
static gb_list_t g_vfsLs;
static gb_list_t g_vfsPrioLs;
static gb_list_t *g_curVfsLs;

typedef struct VFS *(*fnNextFromProto_t)(struct VFS *vfs);

static gb_list_t g_filLs;
static gb_list_t g_dirLs;

void gblocalInitVFS() {
	if (g_vfsBaseHandleType)
		return;

	gbInitDict(&g_vfsDc, GB_DICT_LOWER "-" GB_DICT_DIGIT);
	gbInitList(&g_vfsLs);
	gbInitList(&g_vfsPrioLs);

	gbInitList(&g_filLs);
	gbInitList(&g_dirLs);
}
void gblocalFiniVFS() {
	if (!g_vfsBaseHandleType)
		return;
}

static struct VFS *GetVFSPtr(gb_vfs_t vfs) {
	gbAssert(gbIsHandleDerived(vfs, g_vfsBaseHandleType));

	return (struct VFS *)gbObjectEx(vfs, g_vfsBaseHandleType);
}
static struct File *GetFilePtr(gb_file_t fil) {
	gbAssert(gbIsHandleDerived(fil, g_filBaseHandleType));

	return (struct File *)gbObjectEx(fil, g_filBaseHandleType);
}
static struct Dir *GetDirPtr(gb_dir_t dir) {
	gbAssert(gbIsHandleDerived(dir, g_dirBaseHandleType));

	return (struct Dir *)gbObjectEx(dir, g_dirBaseHandleType);
}

/* -------------------------------------------------------------------------- */

static struct VFS *FirstFromProtoNorm_f() {
	gb_vfs_t h;
	
	h = (gb_vfs_t)gbFirst(&g_vfsLs);
	
	return h ? GetVFSPtr(h) : (struct VFS *)NULL;
}
static struct VFS *NextFromProtoNorm_f(struct VFS *vfs) {
	gb_vfs_t h;

	h = (gb_vfs_t)gbAfter(&vfs->link);

	return h ? GetVFSPtr(h) : (struct VFS *)NULL;
}
static struct VFS *FirstFromProtoPrio_f() {
	return (struct VFS *)gbFirst(&g_vfsPrioLs);
}
static struct VFS *NextFromProtoPrio_f(struct VFS *vfs) {
	return (struct VFS *)gbAfter(&vfs->prioLink);
}

static struct VFS *FirstFromProto(const char **path,
fnNextFromProto_t *fnNextFromProto) {
	struct VFS *vfs;
	const char *p;
	char buf[MAX_PROTO_LIST], proto[MAX_PROTO];
	char *s, *e;

	p = strstr(*path, "://");
	if (!p || ((size_t)(p - *path)) >= sizeof(buf)) {
		if (GB_UNLIKELY(p && ((size_t)(p - *path)) >= sizeof(buf))) {
			gbDebugLog(gbF("\"%s\": protocol field is too long", *path));
		}

		*fnNextFromProto = NextFromProtoNorm_f;
		return FirstFromProtoNorm_f();
	}

	strncpy(buf, *path, p - *path);
	buf[p - *path] = '\0';

	*path = p + (sizeof("://") - 1);

	s = &buf[0];
	while(1) {
		e = strchr(s, ',');
		if (!e)
			break;

		if (GB_UNLIKELY(((size_t)(e - s)) > sizeof(proto))) {
			gbDebugLog(gbF("\"%s\": protocol (in field) is too long", s));
			break;
		}

		strncpy(proto, s, e - s);
		proto[e - s] = '\0';

		s++;

		vfs = (struct VFS *)gbGetEntry(&g_vfsDc, proto);
		if (GB_UNLIKELY(!vfs)) {
			gbErrorMessage(gbF("\"%s\": protocol not found", proto));
			continue;
		}

		gbInitNode(&vfs->prioLink, (void *)vfs);
		gbAddLast(&g_vfsPrioLs, &vfs->prioLink);
	}

	*fnNextFromProto = NextFromProtoPrio_f;
	return FirstFromProtoPrio_f();
}

static struct VFS *VFSFromProtocol(const char **path) {
	struct VFS *vfs;
	const char *p;
	char buf[256];

	p = strstr(*path, "://");
	if (!p || ((size_t)(p - *path)) >= sizeof(buf))
		return (struct VFS *)NULL;

	strncpy(buf, *path, p - *path);
	buf[p - *path] = '\0';

	gbAssert(strchr(buf, ',')==(char *)NULL); /*multiples not allowed*/
	
	vfs = (struct VFS *)gbGetEntry(&g_vfsDc, buf);
	if (GB_LIKELY(vfs != (struct VFS *)NULL))
		*path = p + (sizeof("://") - 1);

	return vfs;
}
static struct VFS *VFSFirst() {
	gb_vfs_t h;
	
	h = (gb_vfs_t)gbFirst(&g_vfsLs);
	
	return h ? GetVFSPtr(h) : (struct VFS *)NULL;
}
static struct VFS *VFSAfter(struct VFS *vfs) {
	gb_vfs_t h;

	h = (gb_vfs_t)gbAfter(&vfs->link);

	return h ? GetVFSPtr(h) : (struct VFS *)NULL;
}
static struct VFS *VFSProtoFirst(const char **path) {
	struct VFS *vfs;

	if ((vfs = VFSFromProtocol(path)) != (struct VFS *)NULL)
		return vfs;

	return VFSFirst();
}

/* -------------------------------------------------------------------------- */

gb_handleType_t gbGetBaseFileHandleType() {
	return g_filBaseHandleType;
}
gb_handleType_t gbGetBaseDirHandleType() {
	return g_dirBaseHandleType;
}

gb_vfs_t gbRegisterVFS(gb_vfsDriverDesc_t *drvDsc) {
	if (drvDsc) {} /*unused*/

	return (gb_vfs_t)NULL;
}
gb_vfs_t gbUnregisterVFS(gb_vfs_t vfs) {
	return gbDropHandle(vfs);
}

gb_vfs_t gbGetVFS(const char *proto) {
	return (gb_vfs_t)gbGetEntry(&g_vfsDc, proto);
}
gb_vfs_t gbFirstVFS() {
	return (gb_vfs_t)gbFirst(&g_vfsLs);
}
gb_vfs_t gbLastVFS() {
	return (gb_vfs_t)gbLast(&g_vfsLs);
}
gb_vfs_t gbVFSBefore(gb_vfs_t vfs) {
	return vfs ? (gb_vfs_t)gbBefore(&GetVFSPtr(vfs)->link) : (gb_vfs_t)NULL;
}
gb_vfs_t gbVFSAfter(gb_vfs_t vfs) {
	return vfs ? (gb_vfs_t)gbAfter(&GetVFSPtr(vfs)->link) : (gb_vfs_t)NULL;
}

#define VFS_REQUIRE(vfs,x)\
	gbAssert(GetVFSPtr(vfs)->desc.fn##x != (gb_fnVFS##x##_t)NULL)
size_t gbGetVFSExtensionCount(gb_vfs_t vfs) {
	VFS_REQUIRE(vfs,GetExtensionCount);
	return GetVFSPtr(vfs)->desc.fnGetExtensionCount();
}
const char *gbGetVFSExtensionName(gb_vfs_t vfs, size_t i) {
	VFS_REQUIRE(vfs,GetExtensionName);
	return GetVFSPtr(vfs)->desc.fnGetExtensionName(i);
}
gb_fnPtr_t gbGetVFSExtensionProc(gb_vfs_t vfs, const char *sym) {
	VFS_REQUIRE(vfs,GetExtensionProc);
	return GetVFSPtr(vfs)->desc.fnGetExtensionProc(sym);
}
gb_bool_t gbIsVFSExtensionSupported(gb_vfs_t vfs, const char *name) {
	size_t i, n;

	n = gbGetVFSExtensionCount(vfs);
	for(i=0; i<n; i++) {
		if (!strcmp(gbGetVFSExtensionName(vfs, i), name))
			return GB_TRUE;
	}

	return GB_FALSE;
}

size_t gbVFSQueryString(gb_vfs_t vfs, const char *qry, void *p, size_t n) {
	VFS_REQUIRE(vfs,QueryString);
	return GetVFSPtr(vfs)->desc.fnQueryString(qry, p, n);
}
size_t gbVFSQueryPName(gb_vfs_t vfs, gb_uint_t pname, void *p, size_t n) {
	VFS_REQUIRE(vfs,QueryPName);
	return GetVFSPtr(vfs)->desc.fnQueryPName(pname, p, n);
}

/* -------------------------------------------------------------------------- */

#define IMPL_VFS1(path,fn,req,retType,parms)\
	fnNextFromProto_t NextFromProto;\
	struct VFS *vfs;\
	retType r;\
	\
	vfs = FirstFromProto(&path, &NextFromProto);\
	for(; vfs; vfs=NextFromProto(vfs)) {\
		if (req) { gbAssert(vfs->desc.fn != NULL); }\
		else if(!vfs->desc.fn) continue;\
		\
		if (!(r = vfs->desc.fn parms))\
			continue;\
		\
		return r;\
	}\
	\
	return (retType)0

size_t gbPathPhysicalToVirtual(char *dst, size_t dstn, const char *path) {
	IMPL_VFS1(path, fnPathPhysicalToVirtual, 1, size_t, (dst, dstn, path));
}
size_t gbPathVirtualToPhysical(char *dst, size_t dstn, const char *path) {
	IMPL_VFS1(path, fnPathVirtualToPhysical, 1, size_t, (dst, dstn, path));
}

gb_bool_t gbCopyFile(const char *src, const char *dst) {
	struct VFS *vfs1, *vfs2;
	const char *orgs, *orgd;
	gb_file_t s, d;
	gb_bool_t eof;
	char buf[4096];

	orgs = src;
	orgd = dst;
	
	vfs1 = VFSFromProtocol(&src);
	vfs2 = VFSFromProtocol(&dst);
	
	/* make explicitly specified protocols match where one was forgotten */
	if (!vfs2)
		vfs2 = vfs1;

	if (!vfs1)
		vfs1 = vfs2;

	/* check to see whether the protocols are the same */
	if (vfs1==vfs2) {
		/* simple version! */
		if (!vfs1)
			vfs1 = VFSFirst();

		for(; vfs1; vfs1=VFSAfter(vfs1)) {
			vfs2 = vfs1;

			if (!vfs1->desc.fnCopyFile) {
				if (orgs!=src || orgd!=dst)
					break;
				else
					continue;
			}

			if (!vfs1->desc.fnCopyFile(src, dst)) {
				if (orgs!=src || orgd!=dst)
					break;
				else
					continue;
			}
			
			return GB_TRUE;
		}
		
		/* if this fails, purposely fallback to the built-in file I/O */
	}

	/* have to actually perform the copy operation */
	s = gbOpenFile(orgs, kGBFileAcc_Read_Bit);
	d = gbOpenFile(orgd, kGBFileAcc_Write_Bit);

	if (GB_UNLIKELY(!s)) {
		d = gbCloseFile(d);
		return GB_FALSE;
	}
	if (GB_UNLIKELY(!d)) {
		s = gbCloseFile(s);
		return GB_FALSE;
	}
	
	while(gbReadFile(s, buf, sizeof(buf), 1)) {
		if (!gbWriteFile(d, buf, sizeof(buf), 1))
			break;
	}

	eof = gbIsEOF(s);

	d = gbCloseFile(d);
	s = gbCloseFile(s);

	return eof;
}
gb_bool_t gbRenameFile(const char *src, const char *dst) {
	struct VFS *vfs1, *vfs2;
	const char *orgs, *orgd;

	orgs = src;
	orgd = dst;
	
	vfs1 = VFSFromProtocol(&src);
	vfs2 = VFSFromProtocol(&dst);
	
	/* make explicitly specified protocols match where one was forgotten */
	if (!vfs2)
		vfs2 = vfs1;

	if (!vfs1)
		vfs1 = vfs2;

	/* ensure there's no inconsistency in protocols (can't rename across
	   protocols */
	if (vfs1 != vfs2)
		return GB_FALSE;

	/* simple version! */
	if (!vfs1)
		vfs1 = VFSFirst();

	for(; vfs1; vfs1=VFSAfter(vfs1)) {
		vfs2 = vfs1;
		
		if (!vfs1->desc.fnRenameFile) {
			if (orgs!=src || orgd!=dst)
				break;
			else
				continue;
		}
		
		if (!vfs1->desc.fnRenameFile(src, dst)) {
			if (orgs!=src || orgd!=dst)
				break;
			else
				continue;
		}
		
		return GB_TRUE;
	}

	return GB_FALSE;
}
gb_bool_t gbDeleteFile(const char *path) {
	IMPL_VFS1(path, fnDeleteFile, 0, gb_bool_t, (path));
}
gb_bool_t gbStatFile(const char *path, gb_fileStat_t *fileInfo) {
	IMPL_VFS1(path, fnStatFile, 1, gb_bool_t, (path, fileInfo));
}

gb_dir_t gbOpenDir(const char *path) {
	IMPL_VFS1(path, fnOpenDir, 1, gb_dir_t, (path));
}
gb_dir_t gbCloseDir(gb_dir_t dir) {
	return (gb_dir_t)gbDropHandle((gb_handle_t)dir);
}
gb_bool_t gbReadDir(gb_dir_t dir, gb_fileStat_t *fileInfo) {
	struct Dir *p;

	p = GetDirPtr(dir);
	gbAssert(p->vfs != (struct VFS *)NULL);
	gbAssert(p->vfs->desc.fnReadDir != NULL);

	return p->vfs->desc.fnReadDir(dir, fileInfo);
}

gb_file_t gbOpenFile(const char *path, gb_uint_t flags) {
	IMPL_VFS1(path, fnOpenFile, 1, gb_file_t, (path, flags));
}
gb_file_t gbCloseFile(gb_file_t file) {
	return (gb_file_t)gbDropHandle((gb_handle_t)file);
}
size_t gbReadFile(gb_file_t file, void *dst, size_t size, size_t num) {
	struct File *p;

	p = GetFilePtr(file);
	gbAssert(p->vfs != (struct VFS *)NULL);
	gbAssert((p->openMode & kGBFileAcc_Read_Bit) != 0);
	gbAssert(p->vfs->desc.fnReadFile != NULL);

	return p->vfs->desc.fnReadFile(file, dst, size, num);
}
size_t gbWriteFile(gb_file_t file, const void *src, size_t size, size_t num) {
	struct File *p;

	p = GetFilePtr(file);
	gbAssert(p->vfs != (struct VFS *)NULL);
	gbAssert((p->openMode & kGBFileAcc_Write_Bit) != 0);
	gbAssert(p->vfs->desc.fnWriteFile != NULL);

	return p->vfs->desc.fnWriteFile(file, src, size, num);
}
gb_bool_t gbSetFilePos(gb_file_t file, gb_u64_t pos) {
	struct File *p;

	p = GetFilePtr(file);
	gbAssert(p->vfs != (struct VFS *)NULL);
	if (GB_UNLIKELY(!p->vfs->desc.fnSetFilePos))
		return GB_FALSE;

	return p->vfs->desc.fnSetFilePos(file, pos);
}
gb_u64_t gbGetFilePos(gb_file_t file) {
	struct File *p;

	p = GetFilePtr(file);
	gbAssert(p->vfs != (struct VFS *)NULL);
	if (GB_UNLIKELY(!p->vfs->desc.fnGetFilePos))
		return (gb_u64_t)0;

	return p->vfs->desc.fnGetFilePos(file);
}

void *gbGetNativeDirHandle(gb_dir_t dir) {
	struct Dir *p;

	p = GetDirPtr(dir);
	gbAssert(p->vfs != (struct VFS *)NULL);
	gbAssert(p->vfs->desc.fnGetNativeDirHandle != NULL);

	return p->vfs->desc.fnGetNativeDirHandle(dir);
}
void *gbGetNativeFileHandle(gb_file_t file) {
	struct File *p;

	p = GetFilePtr(file);
	gbAssert(p->vfs != (struct VFS *)NULL);
	gbAssert(p->vfs->desc.fnGetNativeFileHandle != NULL);

	return p->vfs->desc.fnGetNativeFileHandle(file);
}

#define CHECK_VALID_SIZE(size)\
	gbAssert(size==2 || size==4 || size==8)
size_t gbReadFileLE(gb_file_t file, void *dst, size_t size, size_t num) {
	size_t i, n;

	CHECK_VALID_SIZE(size);

	n = gbReadFile(file, dst, size, num);
	switch(size) {
	case 2:
		for(i=0; i<n; i++)
			((gb_u16_t *)dst)[i] = gbLittleEndian16(((gb_u16_t *)dst)[i]);
		break;
	case 4:
		for(i=0; i<n; i++)
			((gb_u32_t *)dst)[i] = gbLittleEndian32(((gb_u32_t *)dst)[i]);
		break;
	case 8:
		for(i=0; i<n; i++)
			((gb_u64_t *)dst)[i] = gbLittleEndian64(((gb_u64_t *)dst)[i]);
		break;
	}

	return n;
}
size_t gbReadFileBE(gb_file_t file, void *dst, size_t size, size_t num) {
	size_t i, n;

	CHECK_VALID_SIZE(size);

	n = gbReadFile(file, dst, size, num);
	switch(size) {
	case 2:
		for(i=0; i<n; i++)
			((gb_u16_t *)dst)[i] = gbBigEndian16(((gb_u16_t *)dst)[i]);
		break;
	case 4:
		for(i=0; i<n; i++)
			((gb_u32_t *)dst)[i] = gbBigEndian32(((gb_u32_t *)dst)[i]);
		break;
	case 8:
		for(i=0; i<n; i++)
			((gb_u64_t *)dst)[i] = gbBigEndian64(((gb_u64_t *)dst)[i]);
		break;
	}

	return n;
}
#define WRITE_BUFFER_SIZE 4096
size_t gbWriteFileLE(gb_file_t file, const void *src, size_t size, size_t num) {
#define CPLE(b,d,s,di,si)\
	((gb_u##b##_t *)d)[di] = gbLittleEndian##b(((const gb_u##b##_t *)s)[si])
	gb_u8_t buf[WRITE_BUFFER_SIZE];
	size_t i, n, r;

	CHECK_VALID_SIZE(size);

	r = 0;
	while(r < num) {
		n = sizeof(buf)/size;
		if (num < n)
			n = num;

		switch(size) {
		case 2:
			for(i=0; i<n; i++)
				CPLE(16, buf, src, i, r + i);
			break;
		case 4:
			for(i=0; i<n; i++)
				CPLE(32, buf, src, i, r + i);
			break;
		case 8:
			for(i=0; i<n; i++)
				CPLE(64, buf, src, i, r + i);
			break;
		}

		i = gbWriteFile(file, &buf[0], size, sizeof(buf));
		r += i;
		num -= r;

		if (i != n)
			break;
	}

	return r;
#undef CPLE
}
size_t gbWriteFileBE(gb_file_t file, const void *src, size_t size, size_t num) {
#define CPBE(b,d,s,di,si)\
	((gb_u##b##_t *)d)[di] = gbBigEndian##b(((const gb_u##b##_t *)s)[si])
	gb_u8_t buf[WRITE_BUFFER_SIZE];
	size_t i, n, r;

	CHECK_VALID_SIZE(size);

	r = 0;
	while(r < num) {
		n = sizeof(buf)/size;
		if (n > num)
			n = num;

		switch(size) {
		case 2:
			for(i=0; i<n; i++)
				CPBE(16, buf, src, i, r + i);
			break;
		case 4:
			for(i=0; i<n; i++)
				CPBE(32, buf, src, i, r + i);
			break;
		case 8:
			for(i=0; i<n; i++)
				CPBE(64, buf, src, i, r + i);
			break;
		}

		i = gbWriteFile(file, &buf[0], size, sizeof(buf));
		r += i;
		num -= r;

		if (i != n)
			break;
	}

	return r;
#undef CPBE
}

gb_bool_t gbWrite8(gb_file_t file, gb_u8_t x) {
	return !!gbWriteFile(file, &x, sizeof(x), 1);
}
gb_bool_t gbRead8(gb_file_t file, gb_u8_t *x) {
	return !!gbReadFile(file, (void *)x, sizeof(x), 1);
}

gb_bool_t gbWrite16(gb_file_t file, gb_u16_t x) {
	return !!gbWriteFile(file, &x, sizeof(x), 1);
}
gb_bool_t gbWrite32(gb_file_t file, gb_u32_t x) {
	return !!gbWriteFile(file, &x, sizeof(x), 1);
}
gb_bool_t gbWrite64(gb_file_t file, gb_u64_t x) {
	return !!gbWriteFile(file, &x, sizeof(x), 1);
}
gb_bool_t gbRead16(gb_file_t file, gb_u16_t *x) {
	return !!gbReadFile(file, (void *)x, sizeof(x), 1);
}
gb_bool_t gbRead32(gb_file_t file, gb_u32_t *x) {
	return !!gbReadFile(file, (void *)x, sizeof(x), 1);
}
gb_bool_t gbRead64(gb_file_t file, gb_u64_t *x) {
	return !!gbReadFile(file, (void *)x, sizeof(x), 1);
}

gb_bool_t gbWriteL16(gb_file_t file, gb_u16_t x) {
	return !!gbWriteFileLE(file, &x, sizeof(x), 1);
}
gb_bool_t gbWriteL32(gb_file_t file, gb_u32_t x) {
	return !!gbWriteFileLE(file, &x, sizeof(x), 1);
}
gb_bool_t gbWriteL64(gb_file_t file, gb_u64_t x) {
	return !!gbWriteFileLE(file, &x, sizeof(x), 1);
}
gb_bool_t gbReadL16(gb_file_t file, gb_u16_t *x) {
	return !!gbReadFileLE(file, (void *)x, sizeof(x), 1);
}
gb_bool_t gbReadL32(gb_file_t file, gb_u32_t *x) {
	return !!gbReadFileLE(file, (void *)x, sizeof(x), 1);
}
gb_bool_t gbReadL64(gb_file_t file, gb_u64_t *x) {
	return !!gbReadFileLE(file, (void *)x, sizeof(x), 1);
}

gb_bool_t gbWriteB16(gb_file_t file, gb_u16_t x) {
	return !!gbWriteFileBE(file, &x, sizeof(x), 1);
}
gb_bool_t gbWriteB32(gb_file_t file, gb_u32_t x) {
	return !!gbWriteFileBE(file, &x, sizeof(x), 1);
}
gb_bool_t gbWriteB64(gb_file_t file, gb_u64_t x) {
	return !!gbWriteFileBE(file, &x, sizeof(x), 1);
}
gb_bool_t gbReadB16(gb_file_t file, gb_u16_t *x) {
	return !!gbReadFileBE(file, (void *)x, sizeof(x), 1);
}
gb_bool_t gbReadB32(gb_file_t file, gb_u32_t *x) {
	return !!gbReadFileBE(file, (void *)x, sizeof(x), 1);
}
gb_bool_t gbReadB64(gb_file_t file, gb_u64_t *x) {
	return !!gbReadFileBE(file, (void *)x, sizeof(x), 1);
}

gb_bool_t gbWriteString(gb_file_t file, const char *src) {
	gb_u32_t len;

	len = strlen(src);

	if (!gbWriteB32(file, len))
		return GB_FALSE;

	return !!gbWriteFile(file, (const void *)src, len, 1);
}
gb_bool_t gbReadString(gb_file_t file, char *dst, size_t dstn) {
	gb_u32_t len;
	size_t amount;

	if (!gbReadB32(file, &len))
		return GB_FALSE;

	amount = dstn - 1;
	if (amount > (size_t)len)
		amount = (size_t)len;

	if (!gbReadFile(file, (void *)dst, amount, 1))
		return GB_FALSE;

	dst[amount] = '\0';
	return GB_TRUE;
}

gb_bool_t gbWriteFloat(gb_file_t file, float x) {
	return !!gbWriteFile(file, &x, sizeof(x), 1);
}
gb_bool_t gbReadFloat(gb_file_t file, float *x) {
	return !!gbReadFile(file, (void *)x, sizeof(x), 1);
}

gb_bool_t gbWriteDouble(gb_file_t file, double x) {
	return !!gbWriteFile(file, &x, sizeof(x), 1);
}
gb_bool_t gbReadDouble(gb_file_t file, double *x) {
	return !!gbReadFile(file, (void *)x, sizeof(x), 1);
}

gb_bool_t gbIsEOF(gb_file_t file) {
	struct File *p;
	gb_fileStat_t info;
	gb_u64_t pos;

	p = GetFilePtr(file);
	gbAssert(p->vfs != (struct VFS *)NULL);
	if (GB_UNLIKELY(!p->vfs->desc.fnGetFilePos))
		return GB_FALSE;

	pos = p->vfs->desc.fnGetFilePos(file);

	gbAssert(p->vfs->desc.fnStatFile != NULL);
	p->vfs->desc.fnStatFile(file, &info);

	if (pos>=info.fileSize)
		return GB_TRUE;

	return GB_FALSE;
}
gb_bool_t gbSkipFile(gb_file_t file, gb_i64_t amnt) {
	return gbSetFilePos(file, gbGetFilePos(file) + amnt);
}

gb_dir_t gbFirstOpenDir() {
	return (gb_dir_t)gbFirst(&g_dirLs);
}
gb_dir_t gbLastOpenDir() {
	return (gb_dir_t)gbLast(&g_dirLs);
}
gb_dir_t gbOpenDirBefore(gb_dir_t dir) {
	return (gb_dir_t)gbBefore(&GetDirPtr(dir)->link);
}
gb_dir_t gbOpenDirAfter(gb_dir_t dir) {
	return (gb_dir_t)gbAfter(&GetDirPtr(dir)->link);
}

gb_file_t gbFirstOpenFile() {
	return (gb_file_t)gbFirst(&g_filLs);
}
gb_file_t gbLastOpenFile() {
	return (gb_file_t)gbLast(&g_filLs);
}
gb_file_t gbOpenFileBefore(gb_file_t file) {
	return (gb_file_t)gbBefore(&GetFilePtr(file)->link);
}
gb_file_t gbOpenFileAfter(gb_file_t file) {
	return (gb_file_t)gbAfter(&GetFilePtr(file)->link);
}
