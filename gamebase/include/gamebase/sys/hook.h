#ifndef GB_SYS_HOOK_H
#define GB_SYS_HOOK_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Hooks */
typedef int gb_hookID_t;
typedef int(*gb_fnHook_t)(gb_hookID_t id, void *data, void *context);

gb_hookID_t gbAllocHookID();

void gbAddHook(gb_hookID_t id, gb_fnHook_t fnHook, void *context);
int gbRunHooks(gb_hookID_t id, void *data, void *context);
void gbReplaceHook(gb_hookID_t id, gb_fnHook_t fnOld, gb_fnHook_t fnNew,
	void *context);
void gbRemoveHook(gb_hookID_t id, gb_fnHook_t fnHook, void *context);
void gbRemoveAllHooks(gb_hookID_t id, void *context);
size_t gbCountHooks(gb_hookID_t id, void *context);
gb_fnHook_t gbGetHook(gb_hookID_t id, size_t index, void *context);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
