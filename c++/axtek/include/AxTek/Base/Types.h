#ifndef AXTEK_BASE_TYPES_H
#define AXTEK_BASE_TYPES_H

#if _MSC_VER > 1000
# pragma once
#endif

#include <stdarg.h>
#include <stddef.h>

#if !_MSC_VER || _MSC_VER > 1600 || __INTEL_COMPILER
# define AXTEK_STDINT_INCLUDED 1
# include <stdint.h>
#endif

#include "Config.h"
#include "Arch.h"

__AX_NS_INIT__

#if _MSC_VER || __INTEL_COMPILER
typedef   signed __int8         i8 ;
typedef   signed __int16        i16;
typedef   signed __int32        i32;
typedef   signed __int64        i64;
typedef unsigned __int8         u8 ;
typedef unsigned __int16        u16;
typedef unsigned __int32        u32;
typedef unsigned __int64        u64;
#elif AXTEK_STDINT_INCLUDED
typedef            int8_t       i8 ;
typedef            int16_t      i16;
typedef            int32_t      i32;
typedef            int64_t      i64;
typedef           uint8_t       u8 ;
typedef           uint16_t      u16;
typedef           uint32_t      u32;
typedef           uint64_t      u64;
#else
typedef   signed char           i8 ;
typedef   signed short          i16;
typedef   signed int            i32;
typedef   signed long long int  i64;
typedef unsigned char           u8 ;
typedef unsigned short          u16;
typedef unsigned int            u32;
typedef unsigned long long int  u64;
#endif

typedef   signed int            sint_t;
typedef unsigned int            uint_t;

#if AXTEK_ARCH_BITS==64
typedef          i64            sptr_t;
typedef          u64            uptr_t;
#else
typedef          i32            sptr_t;
typedef          u32            uptr_t;
#endif

typedef          i8             s8 ;
typedef          i16            s16;
typedef          i32            s32;
typedef          i64            s64;

typedef          float          f32;
typedef          double         f64;

__AX_NS_FINI__

#endif
