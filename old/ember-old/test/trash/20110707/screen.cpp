// Includes
#include <math.h>
#include "Radiation.h"

// Entity screen rectangle
static RECT		g_entScreenRect = { 0, 0, 0, 0 };
static float	g_entScreenDist = 0.0f;

// Picking
static NUKE		g_ray = 0;

// Retrieve an entity's screen rectangle
void CalcEntScreenRect(NUKE ent, NUKE viewport)
{
	// Declarations
	NUKE		pCamera;
	NukeVec3	pnts[8];
	NukeVec3	vlow, vhigh;
	NukeMatrix	mat;
	float		x, y, z, lowZ=0.0f;
	RECT		*pRect = &g_entScreenRect;

	// Reset the rectangle
	pRect->left = 0;
	pRect->top = 0;
	pRect->right = 0;
	pRect->bottom = 0;

	// Check the viewport
	if (!viewport)
		viewport = GetDefaultViewport();

	// Retrieve the camera of the viewport
	pCamera = GetViewportCamera(viewport);

	// Find the entity
	GetEntMatrix(ent,&mat,true);
	GetEntBoundingEx(ent,&vlow,&vhigh,2,true,true);

	// First point
	pnts[0] = vlow;

	// Second point
	pnts[1] = vlow;
	pnts[1].x = vhigh.x;

	// Third point
	pnts[2] = vlow;
	pnts[2].y = vhigh.y;

	// Fourth point
	pnts[3] = vlow;
	pnts[3].z = vhigh.z;

	// Fifth point
	pnts[4] = vhigh;

	// Sixth point
	pnts[5] = vhigh;
	pnts[5].x = vlow.x;

	// Seventh point
	pnts[6] = vhigh;
	pnts[6].y = vlow.y;

	// Eighth point
	pnts[7] = vhigh;
	pnts[7].z = vlow.z;

	// Calculate the rectangle
	for(int p=0;p<8;p++)
	{
		VecTransformCoord3DEx(&pnts[p],&mat);
		ConvertViewportCoordFrom3DEx(viewport,&x,&y,&pnts[p],&z);
		if(p)
		{
			if(x<(float)pRect->left)   { pRect->left = (long)x;  }
			if(x>(float)pRect->right)  { pRect->right = (long)x; }
			if(y<(float)pRect->top)    { pRect->top = (long)y;  }
			if(y>(float)pRect->bottom) { pRect->bottom = (long)y; }
			if(z<lowZ)                 { lowZ = z; }
		}
		else
		{
			pRect->left = pRect->right = (long)x;
			pRect->bottom = pRect->top = (long)y;
			lowZ = z;
		}
	}

	// Save the screen distance
	g_entScreenDist = lowZ;
}
// Retrieve X1 of the calculated entity screen rectangle
int GetScreenRectX1(void)
{
	return g_entScreenRect.left;
}
// Retrieve Y1 of the calculated entity screen rectangle
int GetScreenRectY1(void)
{
	return g_entScreenRect.top;
}
// Retrieve X2 of the calculated entity screen rectangle
int GetScreenRectX2(void)
{
	return g_entScreenRect.right;
}
// Retrieve Y2 of the calculated entity screen rectangle
int GetScreenRectY2(void)
{
	return g_entScreenRect.bottom;
}
// Retrieve the distance to the entity
float GetEntScreenDistance(void)
{
	return g_entScreenDist;
}

/*
 * PICKING FOR NUCLEAR FUSION
 */
#if 0
typedef struct Vec2 {
	float x, y;
} Vec2;
typedef struct Vec3 {
	float x, y, z;
} Vec3;
typedef struct Vec4 {
	float x, y, z, w;
} Vec4;
typedef union Matrix {
	struct {
		float _11, _21, _31, _41;
		float _12, _22, _32, _42;
		float _13, _23, _33, _43;
		float _14, _24, _34, _44;
	} cm;
	struct {
		float _11, _12, _13, _14;
		float _21, _22, _23, _24;
		float _31, _32, _33, _34;
		float _41, _42, _43, _44;
	} rm;
	float f[16];
	float m[4][4];
} Matrix;
typedef struct Ray {
	Vec3	org, dir;
} Ray;

static void vec3_subtract(Vec3 *dst, const Vec3 *a, const Vec3 *b) {
	dst->x = a->x-b->x;
	dst->y = a->y-b->y;
	dst->z = a->z-b->z;
}
static void vec3_cross(Vec3 *dst, const Vec3 *a, const Vec3 *b) {
	dst->x = a->y*b->z - a->z*b->y;
	dst->y = a->z*b->x - a->x*b->z;
	dst->z = a->x*b->y - a->y*b->x;
}
void vec4_cross(Vec4 *dst, const Vec4 *a, const Vec4 *b, const Vec4 *c) {
	float	x1, x2, x3, y1, y2, y3, z1, z2, z3, w1, w2, w3;

	x1 = a->x; x2 = b->x; x3 = c->x;
	y1 = a->y; y2 = b->y; y3 = c->y;
	z1 = a->z; z2 = b->z; z3 = c->z;
	w1 = a->w; w2 = b->w; w3 = c->w;

	dst->x =   y1*(z2*w3 - z3*w2) - z1*(y2*w3 - y3*w2) + w1*(y2*z3 - z2*y3) ;
	dst->y = -(x1*(z2*w3 - z3*w2) - z1*(x2*w3 - x3*w2) + w1*(x2*z3 - x3*z2));
	dst->z =   x1*(y2*w3 - y3*w2) - y1*(x2*w3 - x3*w2) + w1*(x2*y3 - x3*y2) ;
	dst->w = -(x1*(y2*z3 - y3*z2) - y1*(x2*z3 - x3*z2) + z1*(x2*y3 - x3*y2));
}
static float mat_determinant3(const Matrix *src) {
	float aei, bfg, cdh, afh, bdi, ceg;

	aei = src->rm._11*src->rm._22*src->rm._33;
	bfg = src->rm._12*src->rm._23*src->rm._31;
	cdh = src->rm._13*src->rm._21*src->rm._32;
	afh = src->rm._11*src->rm._23*src->rm._32;
	bdi = src->rm._12*src->rm._21*src->rm._33;
	ceg = src->rm._13*src->rm._22*src->rm._31;

	return aei+bfg+cdh-afh-bdi-ceg;
}
float mat_determinant(const Matrix *src) {
	Vec4	vm, vx, vy, vz;

	vx.x = src->rm._11;
	vx.y = src->rm._21;
	vx.z = src->rm._31;
	vx.w = src->rm._41;

	vy.x = src->rm._12;
	vy.y = src->rm._22;
	vy.z = src->rm._32;
	vy.w = src->rm._42;

	vz.x = src->rm._13;
	vz.y = src->rm._32;
	vz.z = src->rm._33;
	vz.w = src->rm._43;

	vec4_cross(&vm, &vx, &vy, &vz);

	return -(src->rm._14*vm.x + src->rm._24*vm.y + src->rm._34*vm.z + src->rm._44*vm.w);
}
static float mat_inverse3(Matrix *dst, const Matrix *src) {
	float d;
	Vec3 vr, vi[3];
	int i, j, k;

	if (!(d = mat_determinant3(src)))
		return 0.0f;

	for(i=0; i<3; i++) {
		for(j=0; j<3; j++) {
			k = j;
			if (k>i)
				k -= 1;

			vi[k].x = src->f[j*4+0];
			vi[k].y = src->f[j*4+1];
			vi[k].z = src->f[j*4+2];
		}

		vec3_cross(&vr, &vi[0], &vi[1]);
		dst->f[0*4+i] = powf(-1, (float)i)*vr.x / d;
		dst->f[1*4+i] = powf(-1, (float)i)*vr.y / d;
		dst->f[2*4+i] = powf(-1, (float)i)*vr.z / d;
	}

	return d;
}
float mat_inverse(Matrix *dst, const Matrix *src) {
	float d;
	Vec4 vr, vi[4];
	int i, j, k;

	if (!(d = mat_determinant(src)))
		return 0;

	for(i=0; i<4; i++) {
		for(j=0; j<4; j++) {
			k = j;
			if (k>i)
				k -= 1;

			vi[k].x = src->f[j*4+0];
			vi[k].y = src->f[j*4+1];
			vi[k].z = src->f[j*4+2];
			vi[k].w = src->f[j*4+3];
		}

		vec4_cross(&vr, &vi[0], &vi[1], &vi[2]);
		dst->f[0*4+i] = powf(-1, (float)i)*vr.x / d;
		dst->f[1*4+i] = powf(-1, (float)i)*vr.y / d;
		dst->f[2*4+i] = powf(-1, (float)i)*vr.z / d;
		dst->f[3*4+i] = powf(-1, (float)i)*vr.w / d;
	}

	return d;
}
void vec3_transform3_ex(Vec3 *dst, const Vec3 *a, const Matrix *b) {
	dst->x = a->x*b->rm._11 + a->y*b->rm._21 + a->z*b->rm._31;
	dst->y = a->x*b->rm._12 + a->y*b->rm._22 + a->z*b->rm._32;
	dst->z = a->x*b->rm._13 + a->y*b->rm._23 + a->z*b->rm._33;
}
void vec3_transform_ex(Vec3 *dst, const Vec3 *a, const Matrix *b) {
	float norm;
	norm = b->rm._14*a->x + b->rm._24*a->y + b->rm._34*a->z + b->rm._44;

	if (norm) {
		dst->x = (a->x*b->rm._11 + a->y*b->rm._21 + a->z*b->rm._31 + b->rm._41) / norm;
		dst->y = (a->x*b->rm._12 + a->y*b->rm._22 + a->z*b->rm._32 + b->rm._42) / norm;
		dst->z = (a->x*b->rm._13 + a->y*b->rm._23 + a->z*b->rm._33 + b->rm._43) / norm;
	} else {
		dst->x = 0;
		dst->y = 0;
		dst->z = 0;
	}
}
void vec3_translate(Vec3 *dst, const Matrix *src) {
	dst->x = src->rm._41;
	dst->y = src->rm._42;
	dst->z = src->rm._43;
}
int vec3_normalize(Vec3 *a) {
	float r;

	r = sqrtf(a->x*a->x + a->y*a->y + a->z*a->z);
	if (!r)
		return 0;

	r = 1.0f/r;

	a->x *= r;
	a->y *= r;
	a->z *= r;

	return 1;
}
int vec3_in_triangle(const Vec3 *p, const Vec3 *verts, Vec2 *uv) {
	Vec3 v[3];
	float d00, d01, d02, d11, d12;
	float r;

	vec3_subtract(&v[0], &verts[2], &verts[0]);
	vec3_subtract(&v[1], &verts[1], &verts[0]);
	vec3_subtract(&v[2], p, &verts[0]);

	d00 = vec3_dot(&v[0], &v[0]);
	d01 = vec3_dot(&v[0], &v[1]);
	d02 = vec3_dot(&v[0], &v[2]);
	d11 = vec3_dot(&v[1], &v[1]);
	d12 = vec3_dot(&v[1], &v[2]);

	r = 1.0f / (d00*d11 - d01*d01);
	uv->x = (d11*d02 - d01*d12)*r;
	uv->y = (d00*d12 - d01*d02)*r;

	if (uv->x>0 && uv->y>0 && uv->x+uv->y<1)
		return 1;

	return 0;
}
int ray_intersects_tri_ex(const Ray *ray, const Vec3 *tris, Vec2 *uv, float *dist) {
	Matrix	m, i;
	Vec3	t, v;

	m.rm._11 = tris[1].x-tris[0].x;
	m.rm._12 = tris[2].x-tris[0].x;
	m.rm._13 = -ray->dir.x;
	m.rm._14 = 0;

	m.rm._21 = tris[1].y-tris[0].y;
	m.rm._22 = tris[2].y-tris[0].y;
	m.rm._23 = -ray->dir.y;
	m.rm._24 = 0;

	m.rm._31 = tris[1].z-tris[0].z;
	m.rm._32 = tris[2].z-tris[0].z;
	m.rm._33 = -ray->dir.z;
	m.rm._34 = 0;

	m.rm._41 = 0;
	m.rm._42 = 0;
	m.rm._43 = 0;
	m.rm._44 = 1;

	vec3_subtract(&t, &ray->org, &tris[0]);

	mat_inverse3(&i, &m);
	vec3_transform3_ex(&v, &t, &i);

	if (v.x>=0 && v.y>=0 && v.x+v.y<=1.0 && v.z>=0) {
		uv->x = v.x;
		uv->y = v.y;
		*dist = v.z;

		return 1;
	}

	return 0;
}
void ray_local(Ray *dst, const Ray *src, const Matrix *space) {
	Matrix	inv;

	mat_inverse(&inv, space);

	vec3_transform_ex(&dst->org, &src->org, &inv);
	vec3_transform3_ex(&dst->dir, &src->dir, &inv);
	vec3_normalize(&dst->dir);
}
void ray_project(Ray *dst, const Vec3 *org, float width, float height, const Matrix *viewinv, const Matrix *proj) {
	Vec3 dir;

	dir.x =  (((2.0f*org->x)/width )-1.0f)/proj->rm._11;
	dir.y = -(((2.0f*org->y)/height)-1.0f)/proj->rm._22;
	dir.z =  1.0f;
	/*dir.z =          org->z                            ;*/

	/*vec3_transform3_ex(&dst->dir, &dir, viewinv);
	vec3_translate(&dst->org, viewinv);*/
	dst->dir.x = dir.x*viewinv->rm._11 + dir.y*viewinv->rm._21 + dir.z*viewinv->rm._31;
	dst->dir.y = dir.x*viewinv->rm._12 + dir.y*viewinv->rm._22 + dir.z*viewinv->rm._32;
	dst->dir.z = dir.x*viewinv->rm._13 + dir.y*viewinv->rm._23 + dir.z*viewinv->rm._33;
	dst->org.x = viewinv->rm._41;
	dst->org.y = viewinv->rm._42;
	dst->org.z = viewinv->rm._43;
}
#endif
#if 0

// Calculate the vectors of the ray
//   NOTE: For a 2D pick, the Z axis of `vec` should be 1.0.
void CalculateRayVectors(const NukeVec3 *vec, NUKE viewport, NukeVec3 *rayOrg, NukeVec3 *rayDir)
{
	NukeMatrix	matProj, matView;
	//NukeVec3	v;
	float		width, height;
	NUKE		cam;

	if (!viewport)
		viewport = GetDefaultViewport();
	cam = GetViewportCamera(viewport);

	width = (float)GetViewportWidth(viewport);
	height = (float)GetViewportHeight(viewport);

	// Convert the input into projection space
	GetViewportProjectionMatrix(&matProj, viewport);
	rayDir->x =  (((2.0f*vec->x)/width )-1)/matProj.m0; // matProj[0][0]
	rayDir->y = -(((2.0f*vec->y)/height)-1)/matProj.m5; // matProj[1][1]
	rayDir->z =  vec->z;	// Should be 1.0 for 2D pick

	// Grab the inverse view matrix (NF entities all have the same matrix, the
	// camera's matrix is inverted to become the view matrix. The inverse of a
	// matrix's inverse is the original matrix. No need to convert.)
	//   NOTE: Global matrix needed, not local.
	GetEntMatrix(cam, &matView, true);

	// Create the 3D space array
	//   NOTE: VecTransformCoord3DEx adds (4,1), (4,2), and (4,3) to the vector.
	//         That may be an issue, but has not yet been assessed.
	VecTransformCoord3DEx(rayDir, &matView);

	// Extract the origin
	rayOrg->x = 0; rayOrg->y = 0; rayOrg->z = 0;
	VecTranslateCoord3DEx(rayOrg, &matView);
}

// Convert a ray to local (model) space
void RayToModelSpace(NUKE ent, NukeVec3 *rayOrg, NukeVec3 *rayDir)
{
	NukeMatrix	matInverse;
	NukeMatrix	matWorld;

	// Find the inverse of the entity's world matrix
	GetEntMatrix(ent, &matWorld, true);
	MatrixInverse(&matInverse, &matWorld);

	// Transform the origin and direction into the model space
	VecTransformCoord3DEx(rayOrg, &matInverse);
	VecTransformCoord3DEx(rayDir, &matInverse);	// VecTransformCoord3DEx adds an extra column, may be dangerous
	VecNormalize3DEx(rayDir);
}

// Determine whether a ray intersects a particular triangle
bool RayIntersectsTriangleEx(const NukeVec3 tri[3], const NukeVec3 *rayOrg, const NukeVec3 *rayDir, float *texU, float *texV, float *dist)
{
	NukeMatrix	m, matInverse;
	NukeVec3	v;

	m.m0  = tri[1].x-tri[0].x;
	m.m1  = tri[2].x-tri[0].x;
	m.m2  = -rayDir->x;
	m.m3  = 0;

	m.m4  = tri[1].y-tri[0].y;
	m.m5  = tri[2].y-tri[0].y;
	m.m6  = -rayDir->y;
	m.m7  = 0;

	m.m8  = tri[1].z-tri[0].z;
	m.m9  = tri[2].z-tri[0].z;
	m.m10 = -rayDir->z;
	m.m11 = 0;

	m.m12 = 0;
	m.m13 = 0;
	m.m14 = 0;
	m.m15 = 1;

	v.x = rayOrg->x-tri[0].x;
	v.y = rayOrg->y-tri[0].y;
	v.z = rayOrg->z-tri[0].z;

	MatrixInverse(&matInverse, &m);
	VecTransformCoord3DEx(&v, &matInverse);

	if (v.x>=0 && v.y>=0 && v.x+v.y<=1.0 && v.z>=0)
	{
		*texU = v.x;
		*texV = v.x;
		*dist = v.z;

		return true;
	}

	return false;
}
#endif

// Calculate the perspective of the viewport
void GetViewportProjectionMatrix(NukeMatrix *out, NUKE viewport)
{
	NUKE	cam;
	float	width, height;
	float	fov;
	float	aspect;
	float	zn, zf;

	if (!viewport)
		viewport = GetDefaultViewport();
	width = (float)GetViewportWidth(viewport);
	height = (float)GetViewportHeight(viewport);

	cam = GetViewportCamera(viewport);
	GetCameraRange(cam, &zn, &zf);

	if (GetCameraProj(cam)==1)
		MatrixOrthoEx(out, width*GetCameraAspectRange(cam), height, zn, zf);
	else
	{
		fov = 90.0f / GetCameraZoom(cam);
		aspect = width/height*GetCameraAspectRange(cam);
		MatrixPerspectiveEx(out, fov, aspect, zn, zf);
	}
}

void CalculateRayVectors(const NukeVec3 *vec, NUKE viewport, NukeVec3 *rayOrg, NukeVec3 *rayDir)
{
	/*
	Ray			ray;
	NUKE		cam, begin, finish;
	float		width, height;
	NukeMatrix	matProj, matView;

	cam = GetViewportCamera(viewport);

	width = (float)GetViewportWidth(viewport);
	height = (float)GetViewportHeight(viewport);

	GetViewportProjectionMatrix(&matProj, viewport);
	GetEntMatrix(cam, &matView, true);

	ray_project(&ray, (const Vec3 *)vec, width, height, (const Matrix *)&matView, (const Matrix *)&matProj);

	rayOrg->x = ray.org.x;
	rayOrg->y = ray.org.y;
	rayOrg->z = ray.org.z;

	rayDir->x = ray.dir.x;
	rayDir->y = ray.dir.y;
	rayDir->z = ray.dir.z;
	vec3_normalize((Vec3 *)rayDir);
	*/
	static const float dist = 15.0f;
	float x1, y1, z1, x2, y2, z2;
	/*NUKE begin, finish, line, linesurf, cam;

	cam = GetViewportCamera(viewport);
	ScaleEnt(cam,1,1,1);
	SetCameraProj(cam,0);*/

	ConvertViewportCoordTo3D(viewport, vec->x, vec->y, &rayOrg->x, &rayOrg->y, &rayOrg->z, 0.0001f);
	ConvertViewportCoordTo3D(viewport, vec->x, vec->y, &rayDir->x, &rayDir->y, &rayDir->z, 1.0001f);
	rayDir->x -= rayOrg->x;
	rayDir->y -= rayOrg->y;
	rayDir->z -= rayOrg->z;
	//VecNormalize3DEx(rayDir);

	/*begin = MakeSphere();
	finish = MakeCone();
	ScaleMesh(begin, 0.25, 0.25, 0.25);
	ScaleMesh(finish, 0.25, 0.25, 0.25);
	RotateMesh(finish, 0, 180, 0);

	line = MakeMesh();
	linesurf = MakeSurface(line);

	SetEntColor(begin, MakeColor(255, 0, 0));
	SetEntColor(line, MakeColor(0, 255, 0));
	SetEntColor(finish, MakeColor(0, 0, 255));

	//RotateEnt(finish, GetEntRotX(cam, true), 180-GetEntRotY(cam, true), GetEntRotZ(cam, true), true);
	AlignEntToEnt(finish, cam);
//void AlignEntToEnt( NUKE handle, NUKE ent, DWORD axis=3, float speed=1.0f, bool freeroll=TRUE ) 

	x1 = rayOrg->x;
	y1 = rayOrg->y;
	z1 = rayOrg->z;

	x2 = x1 + rayDir->x*dist;
	y2 = y1 + rayDir->y*dist;
	z2 = z1 + rayDir->z*dist;

	PositionEnt(begin, x1, y1, z1, true);
	PositionEnt(finish, x2, y2, z2, true);

	AddSurfaceVertex(linesurf, x1, y1, z1);
	AddSurfaceVertex(linesurf, x2, y2, z2);
	AddSurfaceTriangle(linesurf, 0, 1, 1);

	SetSurfaceFillMode(linesurf, 1);
	SetSurfaceCulling(linesurf, 2);
	SetSurfaceFullBright(linesurf, true);*/
}
#if 0
// Convert a ray to local (model) space
void RayToModelSpace(NUKE ent, NukeVec3 *rayOrg, NukeVec3 *rayDir)
{
	NukeMatrix	matWorld;
	Ray			dst, ray;

	GetEntMatrix(ent, &matWorld, true);

	ray.org.x = rayOrg->x;
	ray.org.y = rayOrg->y;
	ray.org.z = rayOrg->z;

	ray.dir.x = rayDir->x;
	ray.dir.y = rayDir->y;
	ray.dir.z = rayDir->z;

	ray_local(&dst, &ray, (const Matrix *)&matWorld);

	rayOrg->x = dst.org.x;
	rayOrg->y = dst.org.y;
	rayOrg->z = dst.org.z;

	rayDir->x = dst.dir.x;
	rayDir->y = dst.dir.y;
	rayDir->z = dst.dir.z;
}
#endif
// Subtract two vectors
void VecSubtract3DEx(NukeVec3 *dst, const NukeVec3 *a, const NukeVec3 *b)
{
	dst->x = a->x-b->x;
	dst->y = a->y-b->y;
	dst->z = a->z-b->z;
}
void VecSubtract3D(float *x, float *y, float *z, float ax, float ay, float az, float bx, float by, float bz)
{
	*x = ax-bx;
	*y = ay-by;
	*z = az-bz;
}
// Determine whether a ray intersects a particular triangle
bool RayIntersectsTriangle(NUKE surface, DWORD triangle, const NukeVec3 *rayOrg, const NukeVec3 *rayDir, float *texU, float *texV, float *distance)
{
#if 1
	/*
	NukeVec3	tri[3];
	DWORD		vtx[3];

	vtx[0]		= GetSurfaceTriangleVertex(surface, triangle, 0);
	vtx[1]		= GetSurfaceTriangleVertex(surface, triangle, 1);
	vtx[2]		= GetSurfaceTriangleVertex(surface, triangle, 2);

	GetVertexPosEx(surface, vtx[0], &tri[0]);
	GetVertexPosEx(surface, vtx[1], &tri[1]);
	GetVertexPosEx(surface, vtx[2], &tri[2]);

	return RayIntersectsTriangleEx(tri, rayOrg, rayDir, texU, texV, dist);
	*/
	/*
	Ray			ray;
	Vec3		vtx[3];
	Vec2		uv;
	int			r;

	GetVertexPosEx(surface, GetSurfaceTriangleVertex(surface, triangle, 0), (NukeVec3 *)&vtx[0]);
	GetVertexPosEx(surface, GetSurfaceTriangleVertex(surface, triangle, 1), (NukeVec3 *)&vtx[1]);
	GetVertexPosEx(surface, GetSurfaceTriangleVertex(surface, triangle, 2), (NukeVec3 *)&vtx[2]);

	ray.org.x = rayOrg->x;
	ray.org.y = rayOrg->y;
	ray.org.z = rayOrg->z;
	ray.dir.x = rayDir->x;
	ray.dir.y = rayDir->y;
	ray.dir.z = rayDir->z;
	r = ray_intersects_tri_ex(&ray, &vtx[0], &uv, dist);
	if (texU)		*texU = uv.x;
	if (texV)		*texV = uv.y;

	return (r) ? true : false;
	*/
	NukeVec3	diff, vert[3];
	NukeVec3	na, nb, norm, pt, dir;
	NukeVec3	vt[3];
	float		delta, dot, dist;
	float		d00, d01, d02, d11, d12, r;
	float		u, v;

	GetVertexPosEx(surface, GetSurfaceTriangleVertex(surface, triangle, 0), &vert[0]);
	GetVertexPosEx(surface, GetSurfaceTriangleVertex(surface, triangle, 1), &vert[1]);
	GetVertexPosEx(surface, GetSurfaceTriangleVertex(surface, triangle, 2), &vert[2]);

	/*na.x = vert[1].x - vert[0].x;
	na.y = vert[1].y - vert[0].y;
	na.z = vert[1].z - vert[0].z;

	nb.x = vert[2].x - vert[0].x;
	nb.y = vert[2].y - vert[0].y;
	nb.z = vert[2].z - vert[0].z;*/
	VecSubtract3DEx(&na, &vert[1], &vert[0]);
	VecSubtract3DEx(&nb, &vert[2], &vert[0]);

	VecCross3DEx(&norm, &na, &nb);
	VecNormalize3DEx(&norm);

	/*diff.x = rayOrg->x - vert[0].x;
	diff.y = rayOrg->y - vert[0].y;
	diff.z = rayOrg->z - vert[0].z;*/
	VecSubtract3DEx(&diff, rayOrg, &vert[0]);
	if ((delta = VecDot3DEx(&diff, &norm)) < 0)
		return false;

	dir.x = rayDir->x;
	dir.y = rayDir->y;
	dir.z = rayDir->z;
	/*if ((dot = VecDot3DEx(&dir, &norm)) <= 0)
		return false;*/
	dot = VecDot3DEx(&dir, &norm);

	if ((dist = -delta / dot) < 0)
		return false;

	pt.x = rayOrg->x + rayDir->x*dist;
	pt.y = rayOrg->y + rayDir->y*dist;
	pt.z = rayOrg->z + rayDir->z*dist;

	/*vt[0].x = vert[2].x-vert[0].x;
	vt[0].y = vert[2].y-vert[0].y;
	vt[0].z = vert[2].z-vert[0].z;

	vt[1].x = vert[1].x-vert[0].x;
	vt[1].y = vert[1].y-vert[0].y;
	vt[1].z = vert[1].z-vert[0].z;*/
	vt[0] = nb;
	vt[1] = na;

	vt[2].x = pt.x-vert[0].x;
	vt[2].y = pt.y-vert[0].y;
	vt[2].z = pt.z-vert[0].z;

	d00 = VecDot3DEx(&vt[0], &vt[0]);
	d01 = VecDot3DEx(&vt[0], &vt[1]);
	d02 = VecDot3DEx(&vt[0], &vt[2]);
	d11 = VecDot3DEx(&vt[1], &vt[1]);
	d12 = VecDot3DEx(&vt[1], &vt[2]);

	r = 1.0f / (d00*d11 - d01*d01);
	u = (d11*d02 - d01*d12)*r;
	v = (d00*d12 - d01*d02)*r;

	if (u>0 && v>0 && u+v<1)
	{
		if (texU)		*texU = u;
		if (texV)		*texV = v;
		if (distance)	*distance = dist;

		return true;
	}

	return false;
#else
	NukeVec3	vert[3], dir;
	NukeVec3	a, b, c, f, g;
	float		d, e;
	float		u, v;

	GetVertexPosEx(surface, GetSurfaceTriangleVertex(surface, triangle, 0), &vert[0]);
	GetVertexPosEx(surface, GetSurfaceTriangleVertex(surface, triangle, 1), &vert[1]);
	GetVertexPosEx(surface, GetSurfaceTriangleVertex(surface, triangle, 2), &vert[2]);

	VecSubtract3DEx(&a, &vert[1], &vert[0]);
	VecSubtract3DEx(&b, &vert[2], &vert[0]);

	dir = *rayDir;
	VecCross3DEx(&c, &dir, &b);

	d = VecDot3DEx(&a, &c);
	if (d>-0.00001 && d<0.00001)
		return false;

	e = 1.0 / d;
	
	VecSubtract3DEx(&f, rayDir, &vert[0]);

	u = e * VecDot3DEx(&f, &c);
	if (u<0 || u>1)
		return false;

	VecCross3DEx(&g, &f, &a);

	v = e * VecDot3DEx(&dir, &g);

	if (v<0 || u+v>1)
		return false;

	return true;
#endif
}

// Determine whether a ray intersects any triangle on a surface
bool RayIntersectsSurface(NUKE surface, const NukeVec3 *rayOrg, const NukeVec3 *rayDir, float *texU, float *texV, float *dist, DWORD *triangle)
{
	DWORD			ntris;
	DWORD			tri;
	float			bestdist;
	bool			r;

	bestdist		= 9999999999.99999f;
	r				= false;
	ntris			= GetSurfaceTriangleCount(surface);

	for(tri=0; tri<ntris; tri++)
	{
		if (RayIntersectsTriangle(surface, tri, rayOrg, rayDir, texU, texV, dist))
		{
			r |= true;
			if (*dist < bestdist)
			{
				bestdist = *dist;
				*triangle = tri;
			}
		}
	}

	return r;
}

// Determine whether a ray intersects any triangle of any surface on an entity
bool RayIntersectsEntity(NUKE ent, const NukeVec3 *rayOrg, const NukeVec3 *rayDir, float *texU, float *texV, float *dist, NUKE *surface, DWORD *triangle)
{
	DWORD			nsurfs;
	DWORD			i, tmp, tri;
	NUKE			surf;
	float			bestdist;
	bool			r;

	bestdist		= 9999999999.99999f;
	tri				= 0xFFFFFFFF;
	r				= false;

	if (GetEntKind(ent)!=2)
		return false;

	nsurfs = GetMeshSurfaceCount(ent);
	for(i=0; i<nsurfs; i++)
	{
		surf = GetMeshSurface(ent, i);
		if (RayIntersectsSurface(surf, rayOrg, rayDir, texU, texV, dist, &tmp))
		{
			r |= true;
			if (*dist < bestdist)
			{
				bestdist = *dist;
				tri = tmp;
				*surface = surf;
			}
		}
	}

	*triangle = tri;
	return r;
}

// STRUCTURE: Pick Candidate
struct PickCandidate
{
	NUKE					ent;
	int						x1, y1, x2, y2;
	float					dist;

	struct
	{
		DWORD				tri;
		NUKE				surf;
		float				u, v;
		float				dist;
	}						pick;

	PickCandidate			*prev,
							*next;
};
static PickCandidate		*g_firstCandidate = (PickCandidate *)0,
							*g_lastCandidate = (PickCandidate *)0;

// Add a pick candidate to the list
void AddPickCandidate(NUKE ent, int x1, int y1, int x2, int y2, float dist)
{
	PickCandidate			*pick;

	if (!(pick = (PickCandidate *)malloc(sizeof(PickCandidate))))
		return;

	pick->ent = ent;
	pick->x1 = x1;
	pick->y1 = y1;
	pick->x2 = x2;
	pick->y2 = y2;

	pick->prev = g_lastCandidate;
	pick->next = (PickCandidate *)0;

	if (g_lastCandidate)
		g_lastCandidate->next = pick;
	g_lastCandidate = pick;

	if (!g_firstCandidate)
		g_firstCandidate = pick;
}

// Clear the list of pick candidates
void RemovePickCandidates(void)
{
	PickCandidate		*pick, *next;

	for(pick=g_firstCandidate; pick; pick=next)
	{
		next = pick->next;
		free((void *)pick);
	}

	g_firstCandidate = (PickCandidate *)0;
	g_lastCandidate = (PickCandidate *)0;
}

// Add to the list of entities to search through for picking
DWORD ListPickCandidatesForEnt(NUKE ent, int x, int y, NUKE viewport)
{
	DWORD	n, i;
	DWORD	cnt;
	NUKE	child;
	int		width, height;

	cnt		= 0;

	width	= (int)GetViewportWidth(viewport);
	height	= (int)GetViewportHeight(viewport);

	n = GetEntChildCount(ent);
	for(i=0; i<n; i++)
	{
		child = GetEntChild(ent, i);
		cnt += ListPickCandidatesForEnt(child, x, y, viewport);
	}

	// TODO: Add this code in
	//if (GetEntKind(ent)!=2) //mesh
	//	return cnt;

	CalcEntScreenRect(ent, viewport);

	if (GetScreenRectX2()<=0||GetScreenRectX1()>=width)
		return cnt;
	if (GetScreenRectY2()<=0||GetScreenRectY1()>=height)
		return cnt;

	if (x<GetScreenRectX1()||x>GetScreenRectX2())
		return cnt;
	if (y<GetScreenRectY1()||y>GetScreenRectY2())
		return cnt;

	AddPickCandidate
	(
		ent,

		GetScreenRectX1(),
		GetScreenRectY1(),
		GetScreenRectX2(),
		GetScreenRectY2(),

		GetEntScreenDistance()
	);

	return ++cnt;
}

// Create a list of entities to search through for picking
DWORD ListPickCandidates(int x, int y, NUKE viewport)
{
	DWORD	cnt;
	DWORD	nents, i;
	NUKE	cam, world, ent;
	int		width, height;

	cnt = 0;

	width = (int)GetViewportWidth(viewport);
	height = (int)GetViewportHeight(viewport);

	if (x<0 || y<0)
		return 0;

	if (x>=width || y>=height)
		return 0;

	cam = GetViewportCamera(viewport);
	world = GetEntWorld(cam);

	nents = GetWorldEntCount(world);
	for(i=0; i<nents; i++)
	{
		ent = GetWorldEnt(world, i);
		cnt += ListPickCandidatesForEnt(ent, x, y, viewport);
	}

	return cnt;
}

// Pick an entity on screen
static NUKE g_pickedEnt = 0;
static NUKE g_pickedSurface = 0;
static DWORD g_pickedTriangle = 0;
static float g_pickedUV[2] = { 0, 0 };
static float g_pickedDistance = 0;
void PickEnt(int x, int y, NUKE viewport)
{
	PickCandidate	*pick, *picked;
	NukeVec3		scrPick;
	NukeVec3		rayOrg, rayDir;//, tmpRayOrg, tmpRayDir;
	float			bestdist=9999999999.99999f,
					dist;
	bool			r;

	g_pickedEnt = 0;
	g_pickedSurface = 0;
	g_pickedTriangle = 0;
	g_pickedUV[0] = 0; g_pickedUV[1] = 0;
	g_pickedDistance = 0;

	if (!viewport)
		viewport = GetDefaultViewport();

	if (!ListPickCandidates(x, y, viewport))
		return;

	picked = (PickCandidate *)0;
	r = false;

	scrPick.x = (float)x;
	scrPick.y = (float)y;
	scrPick.z = 1.0f; // 2D
	CalculateRayVectors(&scrPick, viewport, &rayOrg, &rayDir);

	for(pick=g_firstCandidate; pick; pick=pick->next)
	{
		/*tmpRayOrg = rayOrg;
		tmpRayDir = rayDir;
		RayToModelSpace(pick->ent, &tmpRayOrg, &tmpRayDir);*/

		if (RayIntersectsEntity(pick->ent, &rayOrg, &rayDir, &pick->pick.u, &pick->pick.v, &dist, &pick->pick.surf, &pick->pick.tri))
		{
			pick->pick.dist = dist;
			if (dist<bestdist)
			{
				bestdist = dist;
				picked = pick;
			}
		}
	}

	if (picked)
	{
		g_pickedEnt = picked->ent;
		g_pickedSurface = picked->pick.surf;
		g_pickedTriangle = picked->pick.tri;
		g_pickedUV[0] = picked->pick.u;
		g_pickedUV[1] = picked->pick.v;
		g_pickedDistance = picked->pick.dist;
	}

	RemovePickCandidates();
}

NUKE GetPickEnt(void)
{
	return g_pickedEnt;
}
NUKE GetPickSurface(void)
{
	return g_pickedSurface;
}
DWORD GetPickTriangle(void)
{
	return g_pickedTriangle;
}
float GetPickVectorX(void)
{
	// TODO
	return 0.0f;
}
float GetPickVectorY(void)
{
	// TODO
	return 0.0f;
}
float GetPickVectorZ(void)
{
	// TODO
	return 0.0f;
}
float GetPickDirectionX(void)
{
	// TODO
	return 0.0f;
}
float GetPickDirectionY(void)
{
	// TODO
	return 0.0f;
}
float GetPickDirectionZ(void)
{
	// TODO
	return 0.0f;
}
float GetPickU(void)
{
	return g_pickedUV[0];
}
float GetPickV(void)
{
	return g_pickedUV[1];
}
