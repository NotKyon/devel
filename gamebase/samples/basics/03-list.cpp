#define GB_INIT 1
#include <gamebase.h>

//
//	This form of the linked-list construct is, like the string construct, mostly
//	useful for C.
//
//	You have the base of your list represented with a 'gb_list_t' and each item
//	within your list gets a 'gb_node_t' for linking.
//

struct Book {
	gb_string_t text;
	gb_node_t link;		//each node provides a link to the list
};
gb_list_t g_bookList;	//contains the list

// when a list is removed, a callback is invoked (if available) for cleaning up
// the item's data (NOTE: the node is unlinked from the list when the callback
// is invoked)
static void BookRemoval_f(Book *p) {
	gbPrint(gbF("Removing book \"%s\"", gbStringBuffer(&p->text)));
	gbFiniString(&p->text);
}

// initialize the "bookshelf" for storing books
void InitBookShelf() {
	// first initialize the list
	gbInitList(&g_bookList);
	// then set the removal callback for the list (see above)
	gbSetListRemoveCallback(&g_bookList, (gb_fnRemoveNode_t)BookRemoval_f);
}
// deinitialize
void FiniBookShelf() {
	// deinitialize the list (removes all current nodes)
	gbFiniList(&g_bookList);
}

// add a book to the shelf
Book *AddBook(const char *text) {
	Book *p;

	// ... allocate...
	p = (Book *)gbMemAlloc(sizeof(*p));

	gbInitString(&p->text);
	gbSetString(&p->text, text);

	// initialize the link (and tell it what it's linking against)
	gbInitNode(&p->link, (void *)p);

	// add this book to the end of the list
	gbAddLast(&g_bookList, &p->link);

	// return the newly allocated book!
	return p;
}
// remove a book from the shelf
Book *RemoveBook(Book *p) {
	if (!p)
		return (Book *)NULL;

	// remove the link (this deinitializes us thanks to the removal callback)
	gbRemoveNode(&p->link);

	// ... deallocate...
	return (Book *)gbMemFree((void *)p);
}
// print the contents of the book
void PrintBook(Book *p) {
	gbPrint(gbStringBuffer(&p->text));
}
// print all of the books
void PrintAllBooks() {
	Book *p;

	gbPrint("Books:");

	// we start at the first book, then go on
	for(p=(Book *)gbFirst(&g_bookList); p; p=(Book *)gbAfter(&p->link))
		// and we print each book along the way
		PrintBook(p);

	gbPrint("");
}

//------------------------------------------------------------------------------

//
//	This small test application will add a few books, print them, remove a few,
//	print the remaining, etc.
//
void GBMain() {
	Book *p[7];

	// this will initialize the linked list's base
	InitBookShelf();

	// now we'll add a few simple books
	p[0] = AddBook("A Study in Scarlet");
	p[1] = AddBook("The Sign of the Four");
	p[2] = AddBook("The Hound of the Baskervilles");
	p[3] = AddBook("The Valley of Fear");
	p[4] = AddBook("A Scandal in Bohemia");
	p[5] = AddBook("The Adventure of the Red-Headed League");
	p[6] = AddBook("A Case of Identity");

	// display the current state of the list
	PrintAllBooks();

	// let's remove some
	p[5] = RemoveBook(p[5]);
	p[2] = RemoveBook(p[2]);

	// display the current state of the list, again
	PrintAllBooks();

	// let's add a few more
	AddBook("The Final Problem");
	AddBook("The Adventure of the Empty House");

	// display the current state of the list, one last time
	PrintAllBooks();

	// now remove all books in the list and "destroy" the bookshelf
	FiniBookShelf();
}
