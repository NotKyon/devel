#include <ember/config.h>
#include <ember/gc.h>
#include <ember/hook.h>

static int __hook_collection_alloc( struct HookCollection *self )
{
	self->head = ( struct Hook * )0;
	self->tail = ( struct Hook * )0;

	return 1;
}
static void __hook_collection_dealloc( struct HookCollection *self )
{
	while( self->head )
		hook_remove( self, self->head->func, self->head->ctx );
}

static int __hook_alloc( struct Hook *self )
{
	self->func = ( hookcall_t )0;
	self->ctx  = ( void * )0;

	self->hc   = ( struct HookCollection * )0;
	self->prev = ( struct Hook * )0;
	self->next = ( struct Hook * )0;

	return 1;
}
static void __hook_dealloc( struct Hook *self )
{
	if( !self->hc )
		return;

	if( self->prev )
		self->prev->next = self->next;
	if( self->next )
		self->next->prev = self->prev;

	if( self->hc->head == self )
		self->hc->head = self->next;
	if( self->hc->tail == self )
		self->hc->tail = self->prev;
}

static struct Hook *__hook_find( struct HookCollection *hc, hookcall_t f,
    void *c )
{
	struct Hook *h;

	for( h = hc->head; h; h = h->next ) {
		if( h->func == f && h->ctx == c )
			return h;
	}

	return ( struct Hook * )0;
}

int hook_init()
{
	g_hook_collection_pool = gc_register_pool(
	    sizeof( struct HookCollection ), ( gc_init_t )__hook_collection_alloc,
	    ( gc_deinit_t )__hook_collection_dealloc );
	if( !g_hook_collection_pool )
		return 0;

	g_hook_pool = gc_register_pool( sizeof( struct Hook ), ( gc_init_t )__hook_alloc,
	    ( gc_deinit_t )__hook_dealloc );
	if( !g_hook_pool ) {
		gc_unregister_pool( g_hook_collection_pool );
		g_hook_collection_pool = ( gc_pool_t )0;

		return 0;
	}

	return 1;
}

void hook_deinit()
{
	gc_unregister_pool( g_hook_pool );
	gc_unregister_pool( g_hook_collection_pool );

	g_hook_pool            = ( gc_pool_t )0;
	g_hook_collection_pool = ( gc_pool_t )0;
}

hook_collection_t hook_alloc_collection()
{
	hook_collection_t hc;

	if( !( hc = ( hook_collection_t )gc_alloc( g_hook_collection_pool ) ) )
		return ( hook_collection_t )0;

	return hc;
}

void hook_dealloc_collection( hook_collection_t hc )
{
	gc_release( ( void * )hc );
}

void hook_add( hook_collection_t hc, hookcall_t f, void *ctx )
{
	struct Hook *h;

	if( ( h = __hook_find( hc, f, ctx ) ) != ( struct Hook * )0 )
		return;

	h->hc = hc;

	h->next = ( struct Hook * )0;
	if( ( h->prev = hc->tail ) != ( struct Hook * )0 )
		hc->tail->next = h;
	else
		hc->head = h;
	hc->tail     = h;
}

void hook_remove( hook_collection_t hc, hookcall_t f, void *ctx )
{
	struct Hook *h;

	if( ( h = __hook_find( hc, f, ctx ) ) != ( struct Hook * )0 )
		return;

	gc_release( ( void * )h );
}

int hook_run_all( hook_collection_t hc, void *dat )
{
	struct Hook *h, *n;
	int r;

	for( h = hc->head; h; h = n ) {
		n = h->next;
		r = h->func( dat, ( void * )0 );
	}

	return r;
}

int hook_run( hook_collection_t hc, void *dat, void *ctx )
{
	struct Hook *h, *n;
	int r;

	for( h = hc->head; h; h = n ) {
		n = h->next;
		if( h->ctx == ctx )
			r = h->func( dat, ctx );
	}

	return r;
}
