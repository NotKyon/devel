#ifndef ASSERT_H
#define ASSERT_H

#if _MSC_VER||__INTEL_COMPILER
# define __func__ __FUNCTION__
#endif

#ifndef BREAKPOINT
# if MK_ARCH_X86||MK_ARCH_X86_64 || __i386__||__x86_64__ || _M_IX86||_M_IX64
#  if __GNUC__ || __clang__
#   define BREAKPOINT() __asm__("int $3")
#  else
#   define BREAKPOINT() __asm int 3
#  endif
# elif __GNUC__ || __clang__
#  define BREAKPOINT() __builtin_trap()
# else
#  error "Please provide a BREAKPOINT() macro for your platform."
# endif
#endif

#if DEBUG||_DEBUG||__debug__ || MK_DEBUG
# define ASSERT(x) if(!(x)){Error(__FILE__,__LINE__,__func__,\
	"ASSERT(%s) failed!", #x);BREAKPOINT();}
#else
# define ASSERT(x) /*do nothing!*/
#endif

#endif
