	.def	 _Method1;
	.scl	2;
	.type	32;
	.endef
	.text
	.globl	_Method1
	.align	16, 0x90
_Method1:
	xorl	%eax, %eax
	movl	4(%esp), %ecx
	testl	%ecx, %ecx
	je	LBB0_7
	movl	8(%esp), %edx
	testl	%edx, %edx
	je	LBB0_7
	movss	8(%ecx), %xmm1
	movss	(%ecx), %xmm0
	movaps	%xmm0, %xmm3
	subss	%xmm1, %xmm3
	movss	(%edx), %xmm2
	ucomiss	%xmm3, %xmm2
	jb	LBB0_6
	addss	%xmm1, %xmm0
	ucomiss	%xmm2, %xmm0
	jb	LBB0_6
	movss	4(%ecx), %xmm0
	movss	12(%ecx), %xmm1
	movaps	%xmm0, %xmm3
	subss	%xmm1, %xmm3
	movss	4(%edx), %xmm2
	ucomiss	%xmm3, %xmm2
	jb	LBB0_6
	addss	%xmm1, %xmm0
	movl	$1, %eax
	ucomiss	%xmm2, %xmm0
	jae	LBB0_7
LBB0_6:
	xorl	%eax, %eax
LBB0_7:
	ret

	.def	 _Method2;
	.scl	2;
	.type	32;
	.endef
	.globl	_Method2
	.align	16, 0x90
_Method2:
	xorl	%eax, %eax
	movl	4(%esp), %ecx
	testl	%ecx, %ecx
	je	LBB1_7
	movl	8(%esp), %edx
	testl	%edx, %edx
	je	LBB1_7
	movss	(%ecx), %xmm0
	movss	8(%ecx), %xmm2
	movss	(%edx), %xmm1
	movaps	%xmm0, %xmm3
	subss	%xmm2, %xmm3
	ucomiss	%xmm3, %xmm1
	jb	LBB1_6
	addss	%xmm2, %xmm0
	ucomiss	%xmm1, %xmm0
	jb	LBB1_6
	movss	4(%ecx), %xmm0
	movss	12(%ecx), %xmm2
	movss	4(%edx), %xmm1
	movaps	%xmm0, %xmm3
	subss	%xmm2, %xmm3
	ucomiss	%xmm3, %xmm1
	jb	LBB1_6
	addss	%xmm2, %xmm0
	movl	$1, %eax
	ucomiss	%xmm1, %xmm0
	jae	LBB1_7
LBB1_6:
	xorl	%eax, %eax
LBB1_7:
	ret


