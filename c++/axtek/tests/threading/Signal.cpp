//
//	Signals are a useful mechanism for specifying that something is ready for
//	synchronization with some other object. This example demonstrates one way
//	signals can be used.
//
#include <AxTek/Threading/CThread.h>
#include <AxTek/Threading/CSignal.h>

#include <stdio.h>  //printf
#include <stdlib.h> //EXIT_SUCCESS

//
//	We'll define some data to work on, as well as the synchronization primitives
//	used (signals).
//
ax::task::CSignal g_initSig;	//signal: begin working on the data
ax::task::CSignal g_finiSig;	//signal: finished working on the data

int g_data;						//the data to be worked on

//
//	This thread will perform work on the data
//
ax::task::result_t THREADPROCAPI thread_f(void *p) {
	ax::uint_t i;
	ax::uint_t seed;

	// set the seed value
	seed = *(ax::uint_t *)&p;

	// main loop
	while(1) {
		// wait until we can begin work on the data
		g_initSig.Sync();

		// we'll reset this signal now so it can be raised after we finish
		g_initSig.Clear();

		// if the data we now have is '0' it means we must exit
		if (g_data == 0)
			break;

		// let's work on this!
		for(i=2; i<40000000 - seed; i++)
			g_data += (g_data*i - g_data*i*(seed + 1))/(i*(i - 1)) + i*seed;

		// done!
		g_finiSig.Raise();
	}

	// done!
	return (ax::task::result_t)0;
}

//
//	The primary thread initializes the signals, and starts the secondary thread
//
int main() {
	ax::task::CThread thread;
	ax::uint_t i;
	int v[] = { 42, 23, -14 };

	// create the signals ('1' is the number of times a signal has to be raised
	//                     before it triggers)
	g_initSig.Reset(1);
	g_finiSig.Reset(1);

	// start the auxillary thread (with a seed value as the parameter)
	thread.Init(thread_f, (void *)274);

	// we'll run this loop three times
	for(i=0; i<sizeof(v)/sizeof(v[0]); i++) {
		// tell the thread to do processing
		g_data = v[i];
		g_initSig.Raise();

		// now we'll wait for the data to finish
		g_finiSig.Sync();
		g_finiSig.Clear();

		// save and print the result
		printf("%i\n", g_data);
	}

	// now that we're done with this thread, we'll *safely* terminate it
	g_data = 0;
	g_initSig.Raise(); //tell the thread to finish (g_data=0)
	thread.Join();

	// done
	return EXIT_SUCCESS;
}
