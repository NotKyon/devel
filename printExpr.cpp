#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

template< bool UseData >
struct base
{
};

template<>
struct base< true >
{
	int x;
};

struct test
{
	base< false > a;
	base< true > b;
};

int main()
{
#define PRINT_EXPR( x ) printf( "%s = %u\n", #x, ( unsigned int )( x ) )
	PRINT_EXPR( sizeof( base< false > ) );
	PRINT_EXPR( sizeof( base< true > ) );
	PRINT_EXPR( sizeof( test ) );
	PRINT_EXPR( offsetof( test, a ) );
	PRINT_EXPR( offsetof( test, b ) );
#undef PRINT_EXPR

	return EXIT_SUCCESS;
}
