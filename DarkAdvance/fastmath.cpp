#include "matrix.h"

int IntSign(int a) {
	return (1 | (a>>31));
}
float FloatSign(float a) {
	return (float)(1 | (((int)a)>>31));
}
int IntAbs(int x) {
	return (x+(x>>31))^(x>>31);
}
float FloatAbs(float x) {
	union { float f; unsigned int i; } v;

	v.f = x;
	v.i &= 0x7FFFFFFF;

	return v.f;
}
int FloatToIntFast(float x) {
	union { float f; int i; } v;

	v.f   = x + (float)(3 << 21);
	v.i  -= 0x4AC00000;
	v.i >>= 1;

	return v.i;
}
unsigned int FloatToUIntFast(float x) {
	union { float f; int i; } v;

	v.f   = x + (float)(3 << 21);
	v.i  -= 0x4AC00000;
	v.i >>= 1;
	v.i  &= 0x7FFFFFFF;

	return *(unsigned int *)&v.i;
}
float SqrtFast(float x) {
	union { float f; int i; } v;

	v.f   = x;
	v.i  -= 0x3F800000;
	v.i >>= 1;
	v.i  += 0x3F800000;

	return v.f;
}
float InvSqrtFast(float x) {
	union { float f; int i; } v;

	v.f = x;
	v.i = 0x5F3759DF - (v.i >> 1);

	return v.f * (1.5f - 0.5f * x * v.f * v.f);
}
float CopySign(float a, float b) {
	union { unsigned int i; float f; } A, B;

	A.f = a;
	B.f = b;

	A.i = (A.i&0x7FFFFFFF)|(B.i&0x80000000);

	return A.f;
}
