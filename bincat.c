#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#if _MSC_VER || __INTEL_COMPILER
# define NORETURN __declspec(noreturn)
#else
# define NORETURN __attribute__((noreturn))
#endif

#if !__STDC_WANT_SECURE_LIB__
int vsprintf_s(char *dst, size_t dstn, const char *format, va_list args) {
	int x;

	x = vsnprintf(dst, dstn, format, args);
# if _WIN32
	dst[dstn - 1] = '\0';
# endif

	return x;
}
#endif

NORETURN void exiterror(const char *format, ...) {
	static char buf[65536];
	va_list args;
	int err;

	err = errno;

	va_start(args, format);
	vsprintf_s(buf, sizeof(buf), format, args);
	va_end(args);

	fprintf(stderr, "ERROR: %s", buf);
	if (err)
		fprintf(stderr, ": %s[%i]\n", strerror(err), err);
	fprintf(stderr, "\n");
	fflush(stderr);

	exit(EXIT_FAILURE);
}

int concat_fileptr(FILE *outfile, FILE *infile) {
	size_t n;
	char buf[8192];

	while(!feof(infile)) {
		if (ferror(infile))
			return 1;

		n = fread(buf, 1, sizeof(buf), infile);
		if (!n)
			return 2;

		if (!fwrite(buf, n, 1, outfile))
			return 3;
	}

	return 0;
}

void concat_file(FILE *outfile, const char *inpath) {
	FILE *infile;
	int r;

	infile = fopen(inpath, "rb");
	if (!infile)
		exiterror("Failed to open \"%s\" for reading", inpath);

	r = concat_fileptr(outfile, infile);
	fclose(infile);

	switch(r)
	{
	case 0:
		break;
	case 1:
		exiterror("There was an error reading from \"%s\"", inpath);
	case 2:
		exiterror("fread() failed while reading from \"%s\"", inpath);
	case 3:
		exiterror("fwrite() failed during write (read from \"%s\"", inpath);
	default:
		exiterror("unknown error %i while reading from \"%s\"", r, inpath);
	}
}

void concat_stdin(FILE *outfile) {
	unsigned int numlines;
	char buf[8192], *p;

	numlines = 0;

	while(!feof(stdin)) {
		if (!fgets(buf, sizeof(buf), stdin))
			break;

		p = strchr(buf, '\r');
		if (p)
			*p = '\0';
		p = strchr(buf, '\n');
		if (p)
			*p = '\0';

		numlines++;
		concat_file(outfile, buf);
	}
}

FILE *g_outfile = NULL;

void close_outfile() {
	if (!g_outfile)
		return;

	fclose(g_outfile);
	g_outfile = NULL;
}

int main(int argc, char **argv) {
	int i;

	if (argc <= 2) {
		fprintf(stderr, "Usage: bincat DEST SRC1 [SRC2 [... SRCN]]\n");
		return EXIT_SUCCESS;
	}

	g_outfile = fopen(argv[1], "wb");
	if (!g_outfile)
		exiterror("Failed to open \"%s\" for writing", argv[1]);

	atexit(close_outfile);

	for(i=2; i<argc; i++) {
		if (!strcmp(argv[i], "-"))
			concat_stdin(g_outfile);
		else
			concat_file(g_outfile, argv[i]);
	}

	close_outfile();
}
