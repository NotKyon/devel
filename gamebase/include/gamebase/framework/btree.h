#ifndef GB_FRAMEWORK_BTREE_H
#define GB_FRAMEWORK_BTREE_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Binary Tree */
typedef struct gb_btreeNode_s {
	int key;
	void *obj;

	struct gb_btreeNode_s *prnt;
	struct gb_btreeNode_s *left, *rght;

	struct gb_btreeNode_s *prev, *next;

	struct gb_btree_s *base;
} gb_btreeNode_t;
typedef struct gb_btree_s {
	struct gb_btreeNode_s *root;

	struct gb_btreeNode_s *head, *tail;
} gb_btree_t;

void gbBTreeInit(gb_btree_t *base);
void gbBTreeRemoveAll(gb_btree_t *base);
gb_btreeNode_t *gbBTreeFind(gb_btree_t *base, int key, gb_btreeNode_t *create);
void gbBTreeRemove(gb_btreeNode_t *node);
void gbBTreeSet(gb_btreeNode_t *node, void *obj);
void *gbBTreeGet(gb_btreeNode_t *node);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
