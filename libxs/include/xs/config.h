#ifndef XS_CONFIG_H
#define XS_CONFIG_H

#include "basetypes.h"
#include "buffer.h"
#include "memory.h"
#include "logging.h"

typedef struct xs_config_s *xs_config_t;
typedef struct xs_section_s *xs_section_t;
typedef struct xs_key_s *xs_key_t;

/*
 * Create a new (empty) configuration.
 *
 * <return (xs_config_t): Handle to the configuration.
 */
xs_config_t XsNewConfig();

/*
 * Load a configuration from a buffer.
 *
 * >buf (xs_buffer_t): Handle to the buffer to load the configuration from.
 * <return (xs_config_t): Handle to the configuration if successful. NULL
 *                        otherwise.
 */
xs_config_t XsLoadConfigFromBuffer(xs_buffer_t buf);

/*
 * Load a configuration from a file.
 *
 * >filename (const char *): Name of the file on disk.
 * <return (xs_config_t): Handle to the configuration if successful. NULL
 *                        otherwise.
 */
xs_config_t XsLoadConfig(const char *filename);

/*
 * Delete an existing configuration and all of its sections/keys.
 *
 * >conf (xs_config_t): Handle to the configuration to be deleted.
 * <return (xs_config_t): NULL.
 */
xs_config_t XsDeleteConfig(xs_config_t conf);

/*
 * Create a new section within a configuration (possibly a subsection if 'prnt'
 * is set.
 *
 * >conf (xs_config_t): Handle to the configuration to create the section in.
 * >prnt (xs_section_t): Parent section if a subsection is to be created.
 *                       Otherwise, this value should be NULL.
 * >name (const char *): Name of the section.
 * <return (xs_section_t): Handle to the section if successful. NULL otherwise.
 */
xs_section_t XsNewSection(xs_config_t conf, xs_section_t prnt,
const char *name);

/*
 * Delete an existing section, all of its subsections, and all of its keys.
 *
 * >sect (xs_section_t): Handle to the section to be deleted.
 * <return (xs_section_t): NULL.
 */
xs_section_t XsDeleteSection(xs_section_t sect);

/*
 * Retrieve the configuration a section belongs in.
 *
 * >sect (xs_section_t): Handle to the section whose configuration is to be
 *                       retrieved.
 * <return (xs_config_t): Handle to the configuration.
 */
xs_config_t XsSectionConfig(xs_section_t sect);

/*
 * Retrieve the name of a section.
 *
 * >sect (xs_section_t): Handle to the section whose name is to be retrieved.
 * <return (const char *): Name of the configuration. (Do NOT modify.)
 */
const char *XsSectionName(xs_section_t sect);

/*
 * Retrieve the supersection of a given subsection.
 *
 * >sect (xs_section_t): Handle to the section whose parent is to be retrieved.
 * <return (xs_section_t): Handle to the parent section if 'sect' is a
 *                         subsection, or NULL otherwise.
 */
xs_section_t XsSectionParent(xs_section_t sect);

/*
 * Retrieve the first section in a configuration (a root section).
 *
 * >conf (xs_config_t): Handle to the configuration whose first section shall be
 *                      retrieved.
 * <return (xs_section_t): Handle to the first root section.
 */
xs_section_t XsFirstRootSection(xs_config_t conf);

/*
 * Retrieve the last section in a configuration (a root section).
 *
 * >conf (xs_config_t): Handle to the configuration whose last section shall be
 *                      retrieved.
 * <return (xs_section_t): Handle to the last root section.
 */
xs_section_t XsLastRootSection(xs_config_t conf);

/*
 * Retrieve the first subsection of a given section.
 *
 * >sect (xs_section_t): Handle to the section whose first subsection shall be
 *                       retrieved.
 * <return (xs_section_t): Handle to the first subsection of 'sect'.
 */
xs_section_t XsFirstSection(xs_section_t sect);

/*
 * Retrieve the last subsection of a given section.
 *
 * >sect (xs_section_t): Handle to the section whose last subsection shall be
 *                       retrieved.
 * <return (xs_section_t): Handle to the last subsection of 'sect'.
 */
xs_section_t XsLastSection(xs_section_t sect);

/*
 * Retrieve the sibling section before another section.
 *
 * >sect (xs_section_t): Handle to the section whose previous sibling shall be
 *                       retrieved.
 * <return (xs_section_t): Handle to the sibling section before 'sect'.
 */
xs_section_t XsSectionBefore(xs_section_t sect);

/*
 * Retrieve the sibling section after another section.
 *
 * >sect (xs_section_t): Handle to the section whose next sibling shall be
 *                       retrieved.
 * <return (xs_section_t): Handle to the sibling section after 'sect'.
 */
xs_section_t XsSectionAfter(xs_section_t sect);

/*
 * Create a new key within a section with a given name and value.
 *
 * >sect (xs_section_t): Handle to the section to create the key within.
 * >name (const char *): Name of the key.
 * >value (const char *): Value of the key.
 * <return (xs_key_t): Handle to the key.
 */
xs_key_t XsNewKey(xs_section_t sect, const char *name, const char *value);

/*
 * Delete an existing key.
 *
 * >key (xs_key_t): Handle to the key to be deleted.
 * <return (xs_key_t): NULL.
 */
xs_key_t XsDeleteKey(xs_key_t key);

/*
 * Retrieve the section a key resides within.
 *
 * >key (xs_key_t): Handle to the key whose section shall be retrieved.
 * <return (xs_section_t): The section containing the key.
 */
xs_section_t XsKeySection(xs_key_t key);

/*
 * Retrieve the name of a key.
 *
 * >key (xs_key_t): Handle to the key whose name shall be retrieved.
 * <return (const char *): The name of the key.
 */
const char *XsKeyName(xs_key_t key);

/*
 * Retrieve the value of a key.
 *
 * >key (xs_key_t): Handle to the key whose value shall be retrieved.
 * <return (const char *): The value of the key.
 */
const char *XsKeyValue(xs_key_t key);

/*
 * Retrieve the first key of a section.
 *
 * >sect (xs_section_t): Handle to the section whose first key shall be
 *                       retrieved.
 * <return (xs_key_t): Handle to the first key in the section.
 */
xs_key_t XsFirstKey(xs_section_t sect);

/*
 * Retrieve the last key of a section.
 *
 * >sect (xs_section_t): Handle to the section whose last key shall be
 *                       retrieved.
 * <return (xs_key_t): Handle to the last key in the section.
 */
xs_key_t XsLastKey(xs_section_t sect);

/*
 * Retrieve the sibling key before another key.
 *
 * >key (xs_key_t): Handle to the key whose previous sibling is to be retrieved.
 * <return (xs_key_t): The sibling key before 'key'.
 */
xs_key_t XsKeyBefore(xs_key_t key);

/*
 * Retrieve the sibling key after another key.
 *
 * >key (xs_key_t): Handle to the key whose next sibling is to be retrieved.
 * <return (xs_key_t): The sibling key before 'key'.
 */
xs_key_t XsKeyAfter(xs_key_t key);

/*
 * Find a section within a configuration based on the name. Sections are
 * separated from their subsections by a forward slash ('/') character. Section
 * names are not escaped.
 *
 * >conf (xs_config_t): Handle to the configuration containing the sections
 *                      specified.
 * >name (const char *): Path string leading to the section.
 * <return (xs_section_t): Handle to the section if it was found. Otherwise this
 *                         will be NULL. Only the last section in the path
 *                         string will be returned.
 */
xs_section_t XsFindSection(xs_config_t conf, const char *name);

/*
 * Find a key within a section based on the key's name.
 *
 * >sect (xs_section_t): Handle to the section to query for the key.
 * >name (const char *): Name of the key.
 * <return (xs_key_t): Handle to the key if it was found. Otherwise NULL.
 */
xs_key_t XsFindKeyInSection(xs_section_t sect, const char *name);

/*
 * Find a key within a configuration. This is handled similarly to
 * XsFindSection(), except the last part of the path is the name of the key to
 * be found. If no section is given, the first section of the configuration is
 * assumed.
 *
 * >conf (xs_config_t): Handle to the configuration to be queried for the key.
 * >name (const char *): Path to the key.
 * <return (xs_key_t): Handle to the key if it was found. Otherwise NULL.
 */
xs_key_t XsFindKey(xs_config_t conf, const char *name);

#endif
