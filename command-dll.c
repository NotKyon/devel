#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#if __GNUC__ || __clang__ || __INTEL_COMPILER
# if !__INTEL_COMPILER
#  define __forceinline static __inline __attribute__((always_inline))
# endif
# define likely(x) __builtin_expect(!!(x), 1)
# define unlikely(x) __builtin_expect(!!(x), 0)
#else
# define likely(x) (x)
# define unlikely(x) (x)
#endif

#if __cplusplus
# define EXPORT extern "C" __declspec(dllexport) __stdcall
#else
# define EXPORT __declspec(dllexport) __stdcall
#endif

/* -------------------------------------------------------------------------- */

/* Errors */
#if 0
static const char *va(const char *fmt, ...) {
	static char buf[8192];
	va_list args;

	va_start(args, fmt);
	vsnprintf(buf, sizeof(buf), fmt, args);
	buf[sizeof(buf) - 1] = '\0';
	va_end(args);

	return buf;
}
#endif
void Error(const char *message) {
	MessageBoxA(GetActiveWindow(), message, "Error", MB_ICONERROR|MB_OK);
	exit(EXIT_FAILURE);
}

/* -------------------------------------------------------------------------- */

/* Memory */
void *MemAlloc(size_t n) {
	void *p;

	if (unlikely(!n))
		return (void *)NULL;

	p = malloc(n);
	if (unlikely(!p)) {
		Error("Failed to allocate memory.");
		return (void *)NULL;
	}

	return p;
}
void *MemAllocZero(size_t n) {
	void *p;

	p = MemAlloc(n);
	memset(p, 0, n);

	return p;
}
void *MemFree(void *p) {
	if (!p)
		return (void *)NULL;

	free(p);
	return (void *)NULL;
}
void *MemExpand(void *p, size_t n) {
	void *q;

	if (unlikely(!p))
		return MemAlloc(n);

	if (unlikely(!n))
		return MemFree(p);

	q = realloc(p, n);
	if (unlikely(!q)) {
		Error("Failed to reallocate memory.");
		return (void *)NULL;
	}

	return q;
}

/* -------------------------------------------------------------------------- */

/* Dictionary */
#define GB_DICT_LOWER "abcdefghijklmnopqrstuvwxyz"
#define GB_DICT_UPPER "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define GB_DICT_ALPHA GB_DICT_LOWER GB_DICT_UPPER
#define GB_DICT_DIGIT "0123456789"
#define GB_DICT_UNDER "_"
#define GB_DICT_IDENT GB_DICT_ALPHA GB_DICT_DIGIT GB_DICT_UNDER

typedef struct gb_entry_s {
	struct gb_entry_s *entries;
	void *p;
} gb_entry_t;
typedef struct gb_dictionary_s {
	int convmap[256];

	int numEntries;
	struct gb_entry_s *entries;
} gb_dictionary_t;

int gbInitDict(gb_dictionary_t *dict, const char *allowed);
void gbFiniDict(gb_dictionary_t *dict);

gb_entry_t *gbFindEntry(gb_dictionary_t *dict, const char *str);
gb_entry_t *gbLookupEntry(gb_dictionary_t *dict, const char *str);

gb_entry_t *gbNewEntry(gb_dictionary_t *dict, const char *str, void *p);
gb_entry_t *gbSetEntry(gb_dictionary_t *dict, const char *str, void *p);
void *gbGetEntry(gb_dictionary_t *dict, const char *str);

static int GenerateConvmap(int dst[256], const char *allowed) {
	int i, j, k;

	for(i=0; i<256; i++)
		dst[i] = -1;

	for(i=0; allowed[i]; i++)
		dst[(unsigned char)allowed[i]] = i;

	for(j=0; j<i; j++) {
		for(k=j+1; k<i; k++) {
			if (allowed[j]==allowed[k])
				return -1; /*duplicate entries*/
		}
	}

	return i;
}
static void DeleteEntries(gb_dictionary_t *dict, gb_entry_t *entries) {
	int i;

	if (!entries)
		return;

	for(i=0; i<dict->numEntries; i++) {
		if (entries[i].entries)
			DeleteEntries(dict, entries[i].entries);
	}

	MemFree((void *)entries);
}
static gb_entry_t *FindFromEntry(gb_dictionary_t *dict, gb_entry_t *entries,
const char *str, int create) {
	size_t n;
	int i;

	i = dict->convmap[*(unsigned char *)str++];
	if (unlikely(i == -1))
		return (gb_entry_t *)NULL; /*invalid character*/

	/* if there's more to the string... */
	if (*str) {
		/* generate the entries if they're not there */
		if (!entries[i].entries) {
			if (create) {
				n = sizeof(gb_entry_t)*dict->numEntries;

				entries[i].entries = (gb_entry_t *)MemAllocZero(n);
			}

			if (!entries[i].entries)
				return (gb_entry_t *)NULL;
		}

		/* continue the search (recursive!) */
		return FindFromEntry(dict, entries[i].entries, str, create);
	}

	/* otherwise this is the last point */
	return &entries[i];
}

int gbInitDict(gb_dictionary_t *dict, const char *allowed) {
	size_t n;

	dict->numEntries = GenerateConvmap(dict->convmap, allowed);
	if (unlikely(dict->numEntries < 1))
		return 0;

	n = sizeof(gb_entry_t)*dict->numEntries;

	dict->entries = (gb_entry_t *)MemAllocZero(n);
	if (unlikely(!dict->entries))
		return 0;

	return 1;
}
void gbFiniDict(gb_dictionary_t *dict) {
	DeleteEntries(dict, dict->entries);
	dict->entries = (gb_entry_t *)NULL;

	dict->numEntries = 0;
}

gb_entry_t *gbFindEntry(gb_dictionary_t *dict, const char *str) {
	return FindFromEntry(dict, dict->entries, str, 0);
}
gb_entry_t *gbLookupEntry(gb_dictionary_t *dict, const char *str) {
	return FindFromEntry(dict, dict->entries, str, 1);
}

gb_entry_t *gbNewEntry(gb_dictionary_t *dict, const char *str, void *p) {
	gb_entry_t *entry;

	entry = gbLookupEntry(dict, str);
	if (!entry)
		return (gb_entry_t *)NULL;

	if (entry->p)
		return (gb_entry_t *)NULL;

	entry->p = p;
	return entry;
}
gb_entry_t *gbSetEntry(gb_dictionary_t *dict, const char *str, void *p) {
	gb_entry_t *entry;

	entry = gbFindEntry(dict, str);
	if (!entry)
		return (gb_entry_t *)NULL;

	entry->p = p;
	return entry;
}
void *gbGetEntry(gb_dictionary_t *dict, const char *str) {
	gb_entry_t *entry;

	entry = gbFindEntry(dict, str);
	if (!entry)
		return (void *)NULL;

	return entry->p;
}

/* -------------------------------------------------------------------------- */

typedef int(__stdcall *CmdFunc_t)();

typedef struct cmd_s {
	gb_entry_t *entry;
	CmdFunc_t fnCmd;
	size_t maxParms;
	struct cmd_s *prev, *next;
} cmd_t;
static cmd_t *g_cmd_head = (cmd_t *)NULL;
static cmd_t *g_cmd_tail = (cmd_t *)NULL;
gb_dictionary_t g_cmd_dict;

static size_t g_cmd_numParms = 0, g_cmd_maxParms = 0;
static char **g_cmd_parms = (char **)NULL;

static int g_cmd_err = 0;

EXPORT int cmd_Init();
EXPORT int cmd_Fini();

EXPORT cmd_t *cmd_Get(const char *name);
EXPORT cmd_t *cmd_Set(const char *name, CmdFunc_t fnCmd, int maxParms);
EXPORT cmd_t *cmd_Rm(cmd_t *cmd);

EXPORT int cmd_CountParms();
EXPORT const char *cmd_GetParm(int i);

EXPORT int cmd_SetError(int e);
EXPORT int cmd_GetError();
EXPORT const char *cmd_GetErrorName();

EXPORT int cmd_PushParm(const char *parm);
EXPORT int cmd_ClearParms();

EXPORT int cmd_Invoke(const char *string);

typedef struct var_s {
	gb_entry_t *entry;
	char *s;
	struct var_s *prev, *next;
} var_t;
static var_t *g_var_head = (var_t *)NULL;
static var_t *g_var_tail = (var_t *)NULL;
gb_dictionary_t g_var_dict;

EXPORT const char *var_Get(const char *name);
EXPORT var_t *var_Set(const char *name, const char *value);
EXPORT var_t *var_Find(const char *name);
EXPORT int var_SetValue(var_t *var, const char *value);
EXPORT const char *var_GetValue(var_t *var);
EXPORT var_t *var_Rm(var_t *var);

/* -------------------------------------------------------------------------- */

EXPORT int cmd_Init() {
	g_cmd_head = (cmd_t *)NULL;
	g_cmd_tail = (cmd_t *)NULL;

	g_var_head = (var_t *)NULL;
	g_var_tail = (var_t *)NULL;

	gbInitDict(&g_cmd_dict, GB_DICT_IDENT ".-");
	gbInitDict(&g_var_dict, GB_DICT_IDENT ".-");

	return 1;
}
EXPORT int cmd_Fini() {
	while(g_cmd_head)
		cmd_Rm(g_cmd_head);
	while(g_var_head)
		var_Rm(g_var_head);

	return 1;
}

EXPORT cmd_t *cmd_Get(const char *name) {
	gb_entry_t *entry;
	cmd_t *cmd;

	entry = gbLookupEntry(&g_cmd_dict, name);
	if (unlikely(!entry)) {
		Error("Failed to lookup entry.");
		return (cmd_t *)NULL;
	}

	if (entry->p)
		return (cmd_t *)entry->p;

	cmd = (cmd_t *)MemAlloc(sizeof(*cmd));
	cmd->entry = entry;
	cmd->fnCmd = (CmdFunc_t)NULL;
	cmd->maxParms = 0;
	cmd->next = (cmd_t *)NULL;
	if ((cmd->prev = g_cmd_tail) != (cmd_t *)NULL)
		g_cmd_tail->next = cmd;
	else
		g_cmd_head = cmd;
	g_cmd_tail = cmd;

	entry->p = (void *)cmd;
	return cmd;
}
EXPORT cmd_t *cmd_Set(const char *name, CmdFunc_t fnCmd, int maxParms) {
	cmd_t *cmd;

	cmd = cmd_Get(name);

	cmd->fnCmd = fnCmd;
	cmd->maxParms = maxParms;

	return cmd;
}
EXPORT cmd_t *cmd_Rm(cmd_t *cmd) {
	if (!cmd)
		return (cmd_t *)NULL;

	if (cmd->prev)
		cmd->prev->next = cmd->next;
	else
		g_cmd_head = cmd->next;
	if (cmd->next)
		cmd->next->prev = cmd->prev;
	else
		g_cmd_tail = cmd->prev;

	cmd->entry->p = (void *)NULL;

	return (cmd_t *)MemFree((void *)cmd);
}

EXPORT int cmd_CountParms() {
	return g_cmd_numParms;
}
EXPORT const char *cmd_GetParm(int i) {
	if ((size_t)i >= g_cmd_numParms)
		return (const char *)NULL;

	return g_cmd_parms[i];
}

EXPORT const char *var_Get(const char *name) {
	var_t *var;

	var = var_Find(name);
	if (unlikely(!var))
		return (const char *)NULL;

	return var_GetValue(var);
}
EXPORT var_t *var_Set(const char *name, const char *value) {
	size_t n;
	var_t *var;

	var = var_Find(name);
	if (unlikely(!var))
		return (var_t *)NULL;

	n = strlen(value);
	var->s = (char *)MemExpand((void *)var->s, n + 1);
	memcpy((void *)var->s, (const void *)value, n + 1);

	return var;
}
EXPORT var_t *var_Find(const char *name) {
	gb_entry_t *entry;
	var_t *var;

	entry = gbLookupEntry(&g_var_dict, name);
	if (unlikely(!entry)) {
		Error("Failed to lookup entry.");
		return (var_t *)NULL;
	}

	if (entry->p)
		return (var_t *)entry->p;

	var = (var_t *)MemAlloc(sizeof(*var));
	var->entry = entry;
	var->s = (char *)NULL;
	var->next = (var_t *)NULL;
	if ((var->prev = g_var_tail) != (var_t *)NULL)
		g_var_tail->next = var;
	else
		g_var_head = var;
	g_var_tail = var;

	entry->p = (void *)var;
	return var;
}
EXPORT int var_SetValue(var_t *var, const char *value) {
	size_t n;

	if (unlikely(!var))
		return 0;

	n = strlen(value);
	var->s = (char *)MemExpand((void *)var->s, n + 1);
	memcpy((void *)var->s, (const void *)value, n + 1);

	return 1;
}
EXPORT const char *var_GetValue(var_t *var) {
	if (unlikely(!var))
		return "";

	if (unlikely(!var->s))
		return "";

	return var->s;
}
EXPORT var_t *var_Rm(var_t *var) {
	if (var->prev)
		var->prev->next = var->next;
	else
		g_var_head = var->next;
	if (var->next)
		var->next->prev = var->prev;
	else
		g_var_tail = var->prev;

	var->s = (char *)MemFree((void *)var->s);

	if (likely(var->entry))
		var->entry->p = (void *)NULL;

	return (var_t *)MemFree((void *)var);
}

EXPORT int cmd_SetError(int e) {
	int x;

	x = g_cmd_err;
	g_cmd_err = e;

	return x;
}
EXPORT int cmd_GetError() {
	return g_cmd_err;
}
EXPORT const char *cmd_GetErrorName() {
#define CASE(x,y) case x: return y
	switch(g_cmd_err) {
	CASE(0,		"No error.");
	CASE(1,		"Command name is too long.");
	CASE(2,		"Command not found.");
	CASE(3,		"Parameter is too long.");
	CASE(4,		"Parameter could not be pushed.");
	CASE(5,		"Too many parameters passed to command.");
	default:
		break;
	}

	return "(unknown error code)";
}

EXPORT int cmd_PushParm(const char *parm) {
	size_t n;

	n = g_cmd_numParms + 1;

	if (n >= g_cmd_maxParms) {
		n  = g_cmd_maxParms + 8;
		g_cmd_parms = (char **)MemExpand((void *)g_cmd_parms,
			n*sizeof(char *));
	}

	n = strlen(parm);
	g_cmd_parms[g_cmd_numParms] = (char *)MemAlloc(n + 1);
	memcpy((void *)g_cmd_parms[g_cmd_numParms], (const void *)parm, n + 1);

	g_cmd_numParms++;

	return 1;
}
EXPORT int cmd_ClearParms() {
	size_t i;

	for(i=0; i<g_cmd_numParms; i++)
		MemFree((void *)g_cmd_parms[i]);

	g_cmd_numParms = 0;
	return 0;
}

static const char *SkipWhitespace(const char *p) {
	while(*p <= ' ' && *p != '\0')
		p++;

	return p;
}
static const char *FindWhitespace(const char *p) {
	while(*p > ' ')
		p++;

	return p;
}
EXPORT int cmd_Invoke(const char *string) {
	const char *s, *e;
	cmd_t *cmd;
	char buf[512];

	s = SkipWhitespace(string);
	e = FindWhitespace(s);

	if (unlikely(((size_t)(e - s)) >= sizeof(buf) - 1)) {
		cmd_SetError(1);
		return -1;
	}

	memcpy((void *)buf, (const void *)s, e - s);
	buf[e - s] = '\0';

	cmd = cmd_Get(buf);
	if (unlikely(!cmd || !cmd->fnCmd)) {
		cmd_SetError(2);
		return -1;
	}

	cmd_ClearParms();

	while(1) {
		s = SkipWhitespace(e);
		e = FindWhitespace(s);

		if (e==s)
			break;

		if (unlikely(((size_t)(e - s)) >= sizeof(buf) - 1)) {
			cmd_SetError(3);
			return -1;
		}

		memcpy((void *)buf, (const void *)s, e - s);
		buf[e - s] = '\0';

		if (unlikely(!cmd_PushParm(buf))) {
			cmd_SetError(4);
			return -1;
		}
	}

	if (unlikely(((size_t)cmd_CountParms()) > cmd->maxParms)) {
		cmd_SetError(5);
		return -1;
	}

	cmd_SetError(0);
	return cmd->fnCmd();
}
