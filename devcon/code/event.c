#include <time.h>

#include "event.h"
#include "memory.h"

eventSystem_t g_ev;
static int g_didInit = 0;

void ev_Init() {
	if (g_didInit)
		return;

	g_ev.head = (event_t *)0;
	g_ev.tail = (event_t *)0;

	g_ev.cur = (event_t *)0;
}
void ev_Deinit() {
	if (!g_didInit)
		return;

	ev_DeleteAll();

	g_didInit = 0;
}

event_t *ev_New(int code, int x, int y) {
	event_t *ev;

	ev = (event_t *)mem_Alloc(sizeof(*ev));
	if (!ev)
		return (event_t *)0;

	ev->tag = (code >> EVENT_TAG_SHIFT) & 0xFF;
	ev->code = code;

	ev->time = clock();

	ev->x = x;
	ev->y = y;

	ev->next = (event_t *)0;
	if ((ev->prev = g_ev.tail) != (event_t *)0)
		g_ev.tail->next = ev;
	else
		g_ev.head = ev;
	g_ev.tail = ev;

	return ev;
}
void ev_Delete(event_t *ev) {
	if (!ev)
		return;

	if (ev->prev)
		ev->prev->next = ev->next;
	if (ev->next)
		ev->next->prev = ev->prev;

	if (g_ev.head==ev)
		g_ev.head = ev->next;
	if (g_ev.tail==ev)
		g_ev.tail = ev->prev;

	if (g_ev.cur==ev)
		g_ev.cur = (event_t *)0;

	mem_Free((void *)ev);
}
void ev_DeleteAll() {
	while(g_ev.head)
		ev_Delete(g_ev.head);
}

void ev_Next() {
	if (g_ev.cur)
		ev_Delete(g_ev.cur);

	g_ev.cur = g_ev.head;
	if (g_ev.cur) {
		if (g_ev.cur->next)
			g_ev.cur->next->prev = (event_t *)0;

		if (g_ev.tail==g_ev.cur)
			g_ev.tail = (event_t *)0;

		g_ev.head = g_ev.cur->next;
	}
}

int ev_Tag() {
	if (!g_ev.cur)
		return 0;

	return g_ev.cur->tag;
}
int ev_Code() {
	if (!g_ev.cur)
		return 0;

	return g_ev.cur->code;
}
int ev_Time() {
	if (!g_ev.cur)
		return 0;

	return g_ev.cur->time;
}
int ev_X() {
	if (!g_ev.cur)
		return 0;

	return g_ev.cur->x;
}
int ev_Y() {
	if (!g_ev.cur)
		return 0;

	return g_ev.cur->y;
}
