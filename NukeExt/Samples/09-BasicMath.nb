//
//	There are a few areas we programmers struggle to gain that last bit of
//	performance out of, to claw away at that bottleneck and break free for those
//	few extra sweet, precious milliseconds. Sometimes we're willing to sacrifice
//	some accuracy for speed. (Hey, that's a given for pretty much all rendering
//	you do if it's real-time!) That's where the "fast math" commands come in.
//	They're optimized methods for performing certain operations. For example,
//	sine, cosine, and tangent are all operations that are commonly performed in
//	games for various reasons. Here we'll benchmark this plugin's commands and
//	compare them to the native commands.
//
//	This file provides a benchmark so you can test some of the fast commands
//	yourself.
//
[DefineVars=True]
Include "../Include/NukeExt.nb"

LoadNukeExt "../NukeExt.dll"

// We'll declare some values for later
Local p1_s#, p1_e# //phase one
Local p2_s#, p2_e# //phase two
Local t#
Local i, j
Const N = 256000 //number of times to run the test!
//Const R = 128000 //number of runs to make (this one took two hours for me!)
Const R = 32

// Let's do some simple tests to demonstrate accuracy
TestTrig 45.0
TestTrig 365.0
TestTrig -180.0
Print ""

// And same for square roots
TestSqrt 123.456
TestSqrt 654.321
Print ""

// Let the user know we're running some tests and this may take a while
Print "Benchmarking built-in commands..."

// Run Sin(), Cos(), and Tan() 'N' times
p1_s = PerfTimer()
For j=1 To R
	For i=1 To N
		Sin 45.0
		Cos 93.0
		Tan 42.0
		Sqr 123456.789
		t = 1/Sqr(295.73)
	Next
Next
p1_e = PerfTimer()

// Let the user know we've reached the second set
Print "Benchmarking fast commands..."

// Run SinFast(), CosFast(), and TanFast() 'N' times
p2_s = PerfTimer()
For j=1 To R
	For i=1 To N
		SinFast  45.0
		CosFast  93.0
		TanFast  42.0
		SqrtFast 123456.789
		t = InvSqrtFast(295.73)
	Next
Next
p2_e = PerfTimer()

// And the results?
Print "Built-in commands  : " + StrFromFloat(p1_e - p1_s) + " seconds"
Print "Fast commands      : " + StrFromFloat(p2_e - p2_s) + " seconds"
Print ""

// Done!
Print "Press any key to exit..."
SetConsoleWaitForKey
End

//----

// These are just the utility functions (called above)

Function TestTrig(x#)
	Local s$
	
	s = StrFromFloat(x)
	Print "Sin    (" + s + ") = " + StrFromFloat(Sin(x), 4)
	Print "SinFast(" + s + ") = " + StrFromFloat(SinFast(x), 4)
	Print "Cos    (" + s + ") = " + StrFromFloat(Cos(x), 4)
	Print "CosFast(" + s + ") = " + StrFromFloat(CosFast(x), 4)
	Print "Tan    (" + s + ") = " + StrFromFloat(Tan(x), 4)
	Print "TanFast(" + s + ") = " + StrFromFloat(TanFast(x), 4)
	Print ""
EndFunction
Function TestSqrt(x#)
	Local s$
	
	s = StrFromFloat(x)
	Print "    Sqr     (" + s + ") = " + StrFromFloat(Sqr(x), 4)
	Print "    SqrtFast(" + s + ") = " + StrFromFloat(SqrtFast(x), 4)
	Print "1.0/Sqr     (" + s + ") = " + StrFromFloat(1.0/Sqr(x), 4)
	Print " InvSqrtFast(" + s + ") = " + StrFromFloat(InvSqrtFast(x), 4)
EndFunction
