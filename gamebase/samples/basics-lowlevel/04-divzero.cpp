//
//	This test ensures the default exception handler is working by performing a
//	division by zero.
//
#define GB_INIT 1
#include <gamebase.h>

void GBMain() {
	volatile int n=4, d=0;

	n /= d; //division by zero here!
}
