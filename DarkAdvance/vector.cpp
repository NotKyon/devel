#include "matrix.h"

/*
 * Allocate a vector3 or vector4 for use as a parameter.
 * NOTE: These should be used sparingly.
 *
 * CREDIT: Idea came from the Quake source
 */
const vec3_t *Vec3(float x, float y, float z) {
	static vec3_t v[MAX_TEMPORARY_VECTORS];
	static size_t i=0;
	vec3_t *p;

	p = &v[i];
	i = (i+1)%(sizeof(v)/sizeof(v[0]));

	p->x = x;
	p->y = y;
	p->z = z;

	return (const vec3_t *)p;
}
const vec4_t *Vec4(float x, float y, float z, float w) {
	static vec4_t v[MAX_TEMPORARY_VECTORS];
	static size_t i=0;
	vec4_t *p;

	p = &v[i];
	i = (i+1)%(sizeof(v)/sizeof(v[0]));

	p->x = x;
	p->y = y;
	p->z = z;
	p->w = w;

	return (const vec4_t *)p;
}

/*
 * Default Vectors
 */
const vec3_t *Vec3_Right()   { static vec3_t v = { 1, 0, 0 }; return &v; }
const vec3_t *Vec3_Up()      { static vec3_t v = { 0, 1, 0 }; return &v; }
const vec3_t *Vec3_Forward() { static vec3_t v = { 0, 0, 1 }; return &v; }

/*
 * Simple Vector Arithmetic
 */
void Vec3_Add(vec3_t *dst, const vec3_t *a, const vec3_t *b) {
	dst->x = a->x + b->x;
	dst->y = a->y + b->y;
	dst->z = a->z + b->z;
}
void Vec3_Subtract(vec3_t *dst, const vec3_t *a, const vec3_t *b) {
	dst->x = a->x - b->x;
	dst->y = a->y - b->y;
	dst->z = a->z - b->z;
}
void Vec3_Multiply(vec3_t *dst, const vec3_t *a, const vec3_t *b) {
	dst->x = a->x * b->x;
	dst->y = a->y * b->y;
	dst->z = a->z * b->z;
}
void Vec3_Scale(vec3_t *dst, const vec3_t *a, float b) {
	dst->x = a->x * b;
	dst->y = a->y * b;
	dst->z = a->z * b;
}
void Vec3_Divide(vec3_t *dst, const vec3_t *a, const vec3_t *b) {
	dst->x = a->x / b->x;
	dst->y = a->y / b->y;
	dst->z = a->z / b->z;
}
void Vec3_LerpEx(vec3_t *dst, const vec3_t *a, const vec3_t *b, const vec3_t *w) {
	dst->x = a->x + (b->x - a->x)*w->x;
	dst->y = a->y + (b->y - a->y)*w->y;
	dst->z = a->z + (b->z - a->z)*w->z;
}
void Vec3_Lerp(vec3_t *dst, const vec3_t *a, const vec3_t *b, float w) {
	dst->x = a->x + (b->x - a->x)*w;
	dst->y = a->y + (b->y - a->y)*w;
	dst->z = a->z + (b->z - a->z)*w;
}
void Vec4_Add(vec4_t *dst, const vec4_t *a, const vec4_t *b) {
	dst->x = a->x + b->x;
	dst->y = a->y + b->y;
	dst->z = a->z + b->z;
	dst->w = a->w + b->w;
}
void Vec4_Subtract(vec4_t *dst, const vec4_t *a, const vec4_t *b) {
	dst->x = a->x - b->x;
	dst->y = a->y - b->y;
	dst->z = a->z - b->z;
	dst->w = a->w - b->w;
}
void Vec4_Multiply(vec4_t *dst, const vec4_t *a, const vec4_t *b) {
	dst->x = a->x * b->x;
	dst->y = a->y * b->y;
	dst->z = a->z * b->z;
	dst->w = a->w * b->w;
}
void Vec4_Scale(vec4_t *dst, const vec4_t *a, float b) {
	dst->x = a->x * b;
	dst->y = a->y * b;
	dst->z = a->z * b;
	dst->w = a->w * b;
}
void Vec4_Divide(vec4_t *dst, const vec4_t *a, const vec4_t *b) {
	dst->x = a->x / b->x;
	dst->y = a->y / b->y;
	dst->z = a->z / b->z;
	dst->w = a->w / b->w;
}
void Vec4_LerpEx(vec4_t *dst, const vec4_t *a, const vec4_t *b, const vec4_t *w) {
	dst->x = a->x + (b->x - a->x)*w->x;
	dst->y = a->y + (b->y - a->y)*w->y;
	dst->z = a->z + (b->z - a->z)*w->z;
	dst->w = a->w + (b->w - a->w)*w->w;
}
void Vec4_Lerp(vec4_t *dst, const vec4_t *a, const vec4_t *b, float w) {
	dst->x = a->x + (b->x - a->x)*w;
	dst->y = a->y + (b->y - a->y)*w;
	dst->z = a->z + (b->z - a->z)*w;
	dst->w = a->w + (b->w - a->w)*w;
}

/*
 * Vector dot product, cross product, length, normalize, distance
 */
float Vec3_Dot(const vec3_t *a, const vec3_t *b) {
	return a->x*b->x + a->y*b->y + a->z*b->z;
}
void Vec3_Cross(vec3_t *dst, const vec3_t *a, const vec3_t *b) {
	dst->x = a->y*b->z - a->z*b->y;
	dst->y = a->z*b->x - a->x*b->z;
	dst->z = a->x*b->y - a->y*b->x;
}
float Vec3_LengthSq(const vec3_t *src) {
	return Vec3_Dot(src, src);
}
float Vec3_Length(const vec3_t *src) {
	return sqrtf(Vec3_LengthSq(src));
}
float Vec3_LengthFast(const vec3_t *src) {
	return SqrtFast(Vec3_LengthSq(src));
}
float Vec3_InvLengthFast(const vec3_t *src) {
	return InvSqrtFast(Vec3_LengthSq(src));
}
void Vec3_Normalize(vec3_t *dst, const vec3_t *src) {
	float r;

	r = 1.0f / Vec3_Length(src);

	dst->x = src->x*r;
	dst->y = src->y*r;
	dst->z = src->z*r;
}
void Vec3_NormalizeFast(vec3_t *dst, const vec3_t *src) {
	float r;

	r = Vec3_InvLengthFast(src);

	dst->x = src->x*r;
	dst->y = src->y*r;
	dst->z = src->z*r;
}
float Vec3_Distance(const vec3_t *a, const vec3_t *b) {
	vec3_t t;

	Vec3_Subtract(&t, a, b);
	return Vec3_Length(&t);
}
float Vec3_DistanceFast(const vec3_t *a, const vec3_t *b) {
	vec3_t t;

	Vec3_Subtract(&t, a, b);
	return Vec3_LengthFast(&t);
}
float Vec4_Dot(const vec4_t *a, const vec4_t *b) {
	return a->x*b->x + a->y*b->y + a->z*b->z + a->w*b->w;
}
void Vec4_Cross(vec4_t *dst, const vec4_t *a, const vec4_t *b, const vec4_t *c) {
	float x1, x2, x3, y1, y2, y3, z1, z2, z3, w1, w2, w3;

	x1 = a->x; x2 = b->x; x3 = c->x;
	y1 = a->y; y2 = b->y; y3 = c->y;
	z1 = a->z; z2 = b->z; z3 = c->z;
	w1 = a->w; w2 = b->w; w3 = c->w;

	dst->x =   y1*(z2*w3 - z3*w2) - z1*(y2*w3 - y3*w2) + w1*(y2*z3 - z2*y3);
	dst->y = -(x1*(z2*w3 - z3*w2) - z1*(x2*w3 - x3*w2) + w1*(x2*z3 - x3*z2));
	dst->z =   x1*(y2*w3 - y3*w2) - y1*(x2*w3 - x3*w2) + w1*(x2*y3 - x3*y2);
	dst->w = -(x1*(y2*z3 - y3*z2) - y1*(x2*z3 - x3*z2) + z1*(x2*y3 - x3*y2));
}
float Vec4_LengthSq(const vec4_t *src) {
	return Vec4_Dot(src, src);
}
float Vec4_Length(const vec4_t *src) {
	return sqrtf(Vec4_LengthSq(src));
}
float Vec4_LengthFast(const vec4_t *src) {
	return SqrtFast(Vec4_LengthSq(src));
}
float Vec4_InvLengthFast(const vec4_t *src) {
	return InvSqrtFast(Vec4_LengthSq(src));
}
void Vec4_Normalize(vec4_t *dst, const vec4_t *src) {
	float r;

	r = 1.0f / Vec4_Length(src);

	dst->x = src->x*r;
	dst->y = src->y*r;
	dst->z = src->z*r;
	dst->w = src->w*r;
}
void Vec4_NormalizeFast(vec4_t *dst, const vec4_t *src) {
	float r;

	r = Vec4_InvLengthFast(src);

	dst->x = src->x*r;
	dst->y = src->y*r;
	dst->z = src->z*r;
	dst->w = src->w*r;
}
float Vec4_Distance(const vec4_t *a, const vec4_t *b) {
	vec4_t t;

	Vec4_Subtract(&t, a, b);
	return Vec4_Length(&t);
}
float Vec4_DistanceFast(const vec4_t *a, const vec4_t *b) {
	vec4_t t;

	Vec4_Subtract(&t, a, b);
	return Vec4_LengthFast(&t);
}

/*
 * Vector 3D Functions
 */
void Vec3_Transform(vec3_t *dst, const vec3_t *a, const matrix_t *b) {
	dst->x = a->x*b->m11 + a->y*b->m12 + a->z*b->m13 + b->m14;
	dst->y = a->x*b->m21 + a->y*b->m22 + a->z*b->m23 + b->m24;
	dst->z = a->x*b->m31 + a->y*b->m32 + a->z*b->m33 + b->m34;
}
void Vec3_Transform3x3(vec3_t *dst, const vec3_t *a, const matrix_t *b) {
	dst->x = a->x*b->m11 + a->y*b->m12 + a->z*b->m13;
	dst->y = a->x*b->m21 + a->y*b->m22 + a->z*b->m23;
	dst->z = a->x*b->m31 + a->y*b->m32 + a->z*b->m33;
}
void Vec3_Point(vec3_t *dst, const vec3_t *src, const vec3_t *target) {
	matrix_t mat;
	vec3_t diff, vec;
	float x, y;

	dst->z = 0;

	Vec3_Subtract(&diff, target, src);

	vec.x=diff.x; vec.y=0; vec.z=diff.z;

	if (Vec3_LengthSq(&vec)) {
		Vec3_Normalize(&vec, Vec3(vec.x, vec.y, vec.z));

		y = Vec3_Dot(&vec, Vec3_Forward());

		if (y > 1)
			y = 1;
		if (y < -1)
			y = -1;

		y = ACos(y);
		if (vec.x < 0)
			y = -y;
	}
		y = 0;

	Matrix_RotationY(&mat, -y);
	Vec3_Transform3x3(&vec, &diff, &mat);

	if (Vec3_LengthSq(&vec)) {
		Vec3_Normalize(&vec, Vec3(vec.x, vec.y, vec.z));

		x = Vec3_Dot(&vec, Vec3_Forward());

		if (x > 1)
			x = 1;
		if (x < -1)
			x = -1;

		x = ACos(x);
		if (vec.y > 0)
			x = -x;
	} else
		x = 0;

	dst->x = x;
	dst->y = y;
}
void Vec3_TriangleNormal(vec3_t *dst, const vec3_t *verts) {
	vec3_t a, b;

	Vec3_Subtract(&a, &verts[1], &verts[0]);
	Vec3_Subtract(&b, &verts[2], &verts[0]);

	Vec3_Cross(dst, &a, &b);
	Vec3_Normalize(dst, dst);
}
void Vec3_TriangleSlide(vec3_t *dst, const vec3_t *src, const vec3_t *norm, float speed) {
	vec3_t objDir, wallDir;
	float d;

	Vec3_Normalize(&objDir, src);

	d = Vec3_Dot(norm, &objDir);
	Vec3_Cross(&wallDir, norm, Vec3_Up());
	Vec3_Normalize(&wallDir, Vec3(wallDir.x, wallDir.y, wallDir.z));

	Vec3_Scale(dst, &wallDir, speed*d);
}
void Vec3_RotateQuaternion(vec3_t *dst, const vec3_t *src, const quat_t *rot) {
	quat_t p, i, t;

	p.w = 0;
	p.x = src->x;
	p.y = src->y;
	p.z = src->z;

	Quat_Inverse(&i, rot);
	Quat_Multiply(&t, &i, &p);
	Quat_Multiply(&p, rot, &t);

	dst->x = p.x;
	dst->y = p.y;
	dst->z = p.z;
}
