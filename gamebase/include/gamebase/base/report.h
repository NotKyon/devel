#ifndef GB_BASE_REPORT_H
#define GB_BASE_REPORT_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../config.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Reporting Functions */
typedef enum {
	kGBLogType_Debug,
	kGBLogType_FatalError,
	kGBLogType_Error,
	kGBLogType_Warning,
	kGBLogType_Remark,
	kGBLogType_Normal,

	kNumGBLogTypes
} gb_logType_t;

void gbReportV(gb_logType_t type, const char *file, int line, const char *func,
const char *message, va_list args);
void gbReport(gb_logType_t type, const char *file, int line, const char *func,
const char *message, ...);

void gbWarn(const char *file, int line, const char *func, const char *message,
...);
void gbError(const char *file, int line, const char *func, const char *message,
...);

void gbWarnMessage(const char *message, ...);
void gbErrorMessage(const char *message, ...);

void gbWarnFile(const char *file, int line, const char *message, ...);
void gbErrorFile(const char *file, int line, const char *message, ...);

void gbErrorFileExit(const char *file, int line, const char *message, ...);
void gbErrorExit(const char *message, ...);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
