﻿#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

void renameFile( const char *sourceFilename ) {
	const char *p, *baseFilename;
	char title[ 260 ];
	int year = 0;
	
	if( *sourceFilename != '[' ) {
		return;
	}

	for( p = sourceFilename + 1; *p >= '0' && *p <= '9'; ++p ) {
		year *= 10;
		year += +( *p - '0' );
	}
	
	if( *p == ']' ) {
		++p;
	}

	while( *p != '\0' && *p <= ' ' ) {
		++p;
	}
	baseFilename = p;

	p = strrchr( baseFilename, '.' );
	if( !p ) {
		return;
	}

	snprintf( title, sizeof( title ) - 1, "%.*s (%i)%s", p - baseFilename,
		baseFilename, year, p );
	title[ sizeof( title ) - 1 ] = '\0';

	printf( "\"%s\" -> \"%s\"\n", sourceFilename, title );

	int r = rename( sourceFilename, title );
	if( r != 0 ) {
		perror( "rename" );
	}
}

int main() {
	WIN32_FIND_DATAA findData;
	HANDLE findHandle;
	BOOL findResult;

	findHandle = FindFirstFileA( ".\\*", &findData );
	if( findHandle == INVALID_HANDLE_VALUE ) {
		return EXIT_FAILURE;
	}

	findResult = TRUE;
	while( findResult ) {
		if( ~findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) {
			renameFile( findData.cFileName );
		}

		findResult = FindNextFileA( findHandle, &findData );
	}
	
	FindClose( findHandle );
	findHandle = INVALID_HANDLE_VALUE;

	return EXIT_SUCCESS;
}
