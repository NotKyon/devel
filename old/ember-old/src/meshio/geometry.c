#include <stdarg.h>
#include <meshio/config.h>
#include <meshio/core.h>
#include <meshio/texture.h>
#include <meshio/material.h>
#include <meshio/geometry.h>

static size_t g_geo_count           = 0;
static meshio_geometry_t g_geo_head = ( meshio_geometry_t )0,
                         g_geo_tail = ( meshio_geometry_t )0;

meshio_geometry_t meshio_alloc_geometry()
{
	meshio_geometry_t geo;

	if( !( geo = ( meshio_geometry_t )meshio_memory( 0, sizeof( *meshio_geometry_t ) ) ) )
		return ( meshio_geometry_t )0;

	geo->refcnt = 1;

	geo->primitive.topology    = EMeshIO_Triangles;
	geo->primitive.kind        = EMeshIO_Short;
	geo->primitive.patch_count = 3;
	geo->primitive.count       = 0;
	geo->primitive.data.ptr    = ( void * )0;

	geo->stream.count        = 0;
	geo->stream.vertex_count = 0;
	geo->stream.head         = ( meshio_geostream_t )0;
	geo->stream.tail         = ( meshio_geostream_t )0;

	geo->mtrl = ( meshio_material_t )0;

	geo->next = ( meshio_geometry_t )0;
	if( ( geo->prev = g_geo_tail ) != ( meshio_geometry_t )0 )
		g_geo_tail->next = geo;
	else
		g_geo_head = geo;
	g_geo_tail     = geo;

	g_geo_count++;
	return geo;
}
meshio_geometry_t meshio_dealloc_geometry( meshio_geometry_t geo )
{
	if( geo->primitive.data.ptr )
		geo->primitive.data.ptr = meshio_memory( geo->primitive.data.ptr, 0 );

	while( geo->stream.head )
		meshio_dealloc_geostream( geo->stream.head );

	if( geo->mtrl )
		geo->mtrl = meshio_release_material( geo->mtrl );

	if( geo->prev )
		geo->prev->next = geo->next;
	if( geo->next )
		geo->next->prev = geo->prev;

	if( g_geo_head == geo )
		g_geo_head = geo->next;
	if( g_geo_tail == geo )
		g_geo_tail = geo->prev;

	g_geo_count--;
	return ( meshio_geometry_t )meshio_memory( ( void * )geo, 0 );
}
void meshio_retain_geometry( meshio_geometry_t geo )
{
	geo->refcnt++;
}
meshio_geometry_t meshio_release_geometry( meshio_geometry_t geo )
{
	return --geo->refcnt == 0 ? meshio_dealloc_geometry( geo ) : ( meshio_geometry_t )0;
}

size_t meshio_geometry_count()
{
	return g_geo_count;
}
meshio_geometry_t meshio_first_geometry()
{
	return g_geo_head;
}
meshio_geometry_t meshio_last_geometry()
{
	return g_geo_tail;
}
meshio_geometry_t meshio_geometry_before( meshio_geometry_t geo )
{
	return geo->prev;
}
meshio_geometry_t meshio_geometry_after( meshio_geometry_t geo )
{
	return geo->next;
}

EMeshIOTopology meshio_geometry_primitive_topology( meshio_geometry_t geo )
{
	return geo->primitive.topology;
}
EMeshIODataType meshio_geometry_primitive_data_type( meshio_geometry_t geo )
{
	return geo->primitive.kind;
}
uint8_t meshio_geometry_primitive_patch_count( meshio_geometry_t geo )
{
	return geo->primitive.patch_count;
}
uint32_t meshio_geometry_primitive_count( meshio_geometry_t geo )
{
	return geo->primitive.count;
}
void *meshio_geometry_primitive_data( meshio_geometry_t geo )
{
	return geo->primitive.data.ptr;
}
uint8_t *meshio_geometry_primitive_bytes( meshio_geometry_t geo )
{
	return geo->primitive.data.byte_ptr;
}
uint16_t *meshio_geometry_primitive_shorts( meshio_geometry_t geo )
{
	return geo->primitive.data.short_ptr;
}
uint32_t *meshio_geometry_primitive_ints( meshio_geometry_t geo )
{
	return geo->primitive.data.int_ptr;
}

size_t meshio_geometry_stream_count( meshio_geometry_t geo )
{
	return geo->stream.count;
}
size_t meshio_geometry_vertex_count( meshio_geometry_t geo )
{
	return geo->stream.vertex_count;
}
meshio_geostream_t meshio_first_geometry_stream( meshio_geometry_t geo )
{
	return geo->stream.head;
}
meshio_geostream_t meshio_last_geometry_stream( meshio_geometry_t geo )
{
	return geo->stream.tail;
}
meshio_geostream_t meshio_geometry_stream_before( meshio_geostream_t stream )
{
	return stream->prev;
}
meshio_geostream_t meshio_geometry_stream_after( meshio_geostream_t stream )
{
	return stream->next;
}

meshio_material_t meshio_geometry_material( meshio_geometry_t geo )
{
	return stream->mtrl;
}

void meshio_set_geometry_primitive_topology( meshio_geometry_t geo,
    EMeshIOTopology topology, ... )
{
	switch( ( geo->primitive.topology = topology ) ) {
	case EMeshIO_Points:
		geo->primitive.patch_count = 1;
		break;
	case EMeshIO_Lines:
	case EMeshIO_LineStrip:
	case EMeshIO_LineLoop:
		geo->primitive.patch_count = 2;
		break;
	case EMeshIO_Triangles:
	case EMeshIO_TriangleStrip:
	case EMeshIO_TriangleFan:
		geo->primitive.patch_count = 3;
		break;
	case EMeshIO_Quads:
		geo->primitive.patch_count = 4;
		break;
	case EMeshIO_Patch: {
		va_list args;
		va_start( args, topology );
		geo->primitive.patch_count = va_arg( args, uint32_t );
		va_end( args );
		break;
	}
	}
}
int meshio_set_geometry_primitive_data_type( meshio_geometry_t geo,
    EMeshIODataType kind )
{
	switch( kind ) {
	case EMeshIO_Byte:
	case EMeshIO_Short:
	case EMeshIO_Int:
		geo->primitive.kind = kind;
		return 1;
	default:
		return 0;
	}
}
void *meshio_set_geometry_primitive_count( meshio_geometry_t geo,
    uint32_t count )
{
	size_t n;

	n = meshio_data_type_size( geo->primitive.kind ) * count;
	if( !geo->primitive.data.ptr && !n )
		return;

	geo->primitive.count           = count;
	return geo->primitive.data.ptr = meshio_memory( geo->primitive.data.ptr, n );
}

size_t meshio_calculate_geometry_vertex_size( meshio_geometry_t geo,
    size_t vertex_count )
{
	meshio_geostream_t gs;
	size_t n, t;

	n = 0;
	for( gs = geo->stream.head; gs; gs = gs->next ) {
		t = ( ( size_t )gs->desc.offset ) + ( ( size_t )gs->desc.stride ) * vertex_count;
		if( n < t )
			n = t;
	}

	return n;
}
size_t meshio_geometry_vertex_size( meshio_geometry_t geo )
{
	return meshio_calculate_geometry_vertex_size( geo, geo->stream.vertex_count );
}

meshio_geostream_t meshio_alloc_geostream( meshio_geometry_t geo,
    const MESHIO_STREAM_DESC *desc )
{
	meshio_geostream_t gs;

	if( !( gs = ( meshio_geostream_t )meshio_memory( 0, sizeof( *meshio_geostream_t ) ) ) )
		return ( meshio_geostream_t )0;

	gs->desc     = *desc;
	gs->data.ptr = ( void * )0;

	gs->prnt = geo;
	gs->next = ( meshio_geostream_t )0;
	if( ( gs->prev = geo->stream.tail ) != ( meshio_geostream_t )0 )
		geo->stream.tail->next = gs;
	else
		geo->stream.head = gs;
	geo->stream.tail     = gs;

	return gs;
}
meshio_geostream_t meshio_dealloc_geostream( meshio_geostream_t gs )
{
	if( gs->data.ptr )
		gs->data.ptr = meshio_memory( gs->data.ptr, 0 );

	if( gs->prev )
		gs->prev->next = gs->next;
	if( gs->next )
		gs->next->prev = gs->prev;

	if( gs->prnt->stream.head == gs )
		gs->prnt->stream.head = gs->next;
	if( gs->prnt->stream.tail == gs )
		gs->prnt->stream.tail = gs->prev;

	return ( meshio_geostream_t )meshio_memory( ( void * )gs, 0 );
}
