function findTreeviewBranch( thisNode ) {
	var x = thisNode.nextSibling;
	var n = 20;
	
	while( x.className != "treeview_branch" && n > 0 ) {
		x = x.nextSibling;
		if( !x ) {
			return;
		}
		
		n--;
	}

	return x;
}
function toggle( thisNode ) {
	// find the command's content entity
	var content = findTreeviewBranch( thisNode );

	// toggle the visibility of it
	if( thisNode.className == "unfolded" ) {
		thisNode.className = "folded";
		content.style.display = "none";
	} else {
		thisNode.className = "unfolded";
		content.style.display = "block";
	}
}
function see( thisElement ) {
	var commandName = thisElement.getAttribute( "href" ).substr( 1 );
	var command = document.getElementById( commandName );
	var content = findTreeviewBranch( command );
	
	if( command.className == "folded" ) {
		command.className = "unfolded";
		content.style.display = "block";
	}
}
