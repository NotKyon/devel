################################################################################
#
#	AUTO-MAKEFILE
#	Makefile for automating a few settings
#
#	NOTE: This file _should_ be included in your projects (probably modified to
#	      suit your needs) either by another Makefile, or as the primary
#	      Makefile.
#
#	NOT RESPONSIBLE FOR YOUR COMPUTER BLOWING UP. (Or anything related to the
#	usage of this Makefile.)
#
################################################################################

# Select the configuration
CONFIG ?= DEBUG

# Select the source directories
# NOTE: You can have multiple source directories
SRCDIRS ?= $(wildcard src/ source/ code/)
INCDIRS ?= $(wildcard inc/ include/ headers/)
ifeq ($(CONFIG),DEBUG)
 LIBDIRS ?= $(wildcard lib-dbg/ libs/ lib-all/ library/)
else
 LIBDIRS ?= $(wildcard lib/ libs/ lib-all/ library/)
endif

# Some defaults
GCC_CFLAGS := -std=gnu99 $(foreach D,$(MACROS),"-D$(D)") $(ARCHFLAGS)
GCC_CDEBUGFLAGS :=
GCC_CRELEASEFLAGS :=

#GCC_CXXFLAGS := -Weffc++
GCC_CXXFLAGS := $(foreach D,$(MACROS),"-D$(D)") $(ARCHFLAGS)
GCC_CXXDEBUGFLAGS :=
GCC_CXXRELEASEFLAGS :=

GCC_LNKFLAGS := $(foreach D,$(LIBDIRS),"-L$(D)")
GCC_LNKDEBUGFLAGS :=
GCC_LNKRELEASEFLAGS := -s

GCC_LNKLIBS :=
GCC_LNKDEBUGLIBS :=
GCC_LNKRELEASELIBS :=

GCC_ARFLAGS := cr
GCC_ARDEBUGFLAGS :=
GCC_ARRELEASEFLAGS :=

GCC_FLAGS := -W -Wall -pedantic $(foreach D,$(INCDIRS),"-I$(D)")
GCC_DEBUGFLAGS := -g -DDEBUG -D_DEBUG
GCC_RELEASEFLAGS := -O3 -s -fomit-frame-pointer -fno-strict-aliasing -DNDEBUG

# Use defaults if not specified
CFLAGS ?= $(GCC_CFLAGS)
CDEBUGFLAGS ?= $(GCC_CDEBUGFLAGS)
CRELEASEFLAGS ?= $(GCC_CRELEASEFLAGS)

CXXFLAGS ?= $(GCC_CXXFLAGS)
CXXDEBUGFLAGS ?= $(GCC_CXXDEBUGFLAGS)
CXXRELEASEFLAGS ?= $(GCC_CXXRELEASEFLAGS)

LNKFLAGS ?= $(GCC_LNKFLAGS)
LNKDEBUGFLAGS ?= $(GCC_LNKDEBUGFLAGS)
LNKRELEASEFLAGS ?= $(GCC_LNKRELEASEFLAGS)

LNKLIBS ?= $(GCC_LNKLIBS)
LNKDEBUGLIBS ?= $(GCC_LNKDEBUGLIBS)
LNKRELEASELIBS ?= $(GCC_LNKRELEASELIBS)

ARFLAGS ?= $(GCC_ARFLAGS)
ARDEBUGFLAGS ?= $(GCC_ARDEBUGFLAGS)
ARRELEASEFLAGS ?= $(GCC_ARRELEASEFLAGS)

ALLFLAGS ?= $(GCC_FLAGS)
DEBUGFLAGS ?= $(GCC_DEBUGFLAGS)
RELEASEFLAGS ?= $(GCC_RELEASEFLAGS)

# Select which flags to use for C and C++ files under current configuration
CUR_CFLAGS    := $(ALLFLAGS) $($(CONFIG)FLAGS) $(CFLAGS) $(C$(CONFIG)FLAGS)
CUR_CXXFLAGS  := $(ALLFLAGS) $($(CONFIG)FLAGS) $(CXXFLAGS) $(CXX$(CONFIG)FLAGS)
CUR_LNKFLAGS  := $(LNKFLAGS) $(LNK$(CONFIG)FLAGS)
CUR_LNKLIBS   := $(LNKLIBS) $(LNK$(CONFIG)LIBS)
CUR_ARFLAGS   := cr
CUR_POSTFLAGS := -MD -MP -c -o

# Select the compiler
CC ?= gcc
ifeq ($(CC),cc)
 CC := gcc
endif
CXX ?= g++
AR ?= ar

# Source file extensions
SRCEXTS ?= .c .cpp

# Macro that runs foreach(DIR,...,foreach(EXT,...,wildcard DIR*EXT))
foreachdirext = $(foreach D,$(1),$(foreach E,$(2),$(wildcard $(D)*$(E))))

# Default project type [executable, dynamiclibrary, library]
TARGETTYPE ?= executable
ifeq ($(TARGETTYPE),executable)
 TARGETPREFIX :=
 TARGETSUFFIX :=
 TARGETLINKTYPE := LNK
else
 ifeq ($(TARGETTYPE),dynamiclibrary)
  TARGETPREFIX := lib
  TARGETSUFFIX := .so
  TARGETLINKTYPE := LNK
 else
  ifeq ($(TARGETTYPE),library)
   TARGETPREFIX := lib
   TARGETSUFFIX := .a
   TARGETLINKTYPE := AR
  else
   $(error TARGETTYPE needs to be 'executable', 'dynamiclibrary', or 'library')
  endif
 endif
endif

# Select the object and output directories
DEBUGOBJDIR ?= obj/dbg/
RELEASEOBJDIR ?= obj/rel/

ifeq ($(TARGETTYPE),library)
 DEBUGOUTDIR ?= lib-dbg/
 RELEASEOUTDIR ?= lib/
else
 DEBUGOUTDIR ?= bin-dbg/
 RELEASEOUTDIR ?= bin/
endif

OBJDIR ?= $($(CONFIG)OBJDIR)
OUTDIR ?= $($(CONFIG)OUTDIR)

# Find all sources
SOURCES := $(call foreachdirext,$(SRCDIRS),$(SRCEXTS))
OBJECTS := $(foreach SRC,$(SOURCES),$(OBJDIR)$(SRC).o)
DEPENDS := $(OBJECTS:.o=.d)

# Default project name
PROJECTNAME ?= out

# Default target
TARGET ?= $(OUTDIR)$(TARGETPREFIX)$(PROJECTNAME)$(TARGETSUFFIX)

# What gets cleaned up?
CLEANDIRS ?= $(OBJDIR) $(OUTDIR)

CLEANEXTS ?= .o .d .a .out .so .dylib .exe .dll .obj .lib .pdb .res
CLEANFILES := $(call foreachdirext,$(CLEANDIRS),$(CLEANEXTS)) $(TARGET)\
 $(OBJECTS) $(DEPENDS)

# Determine which linker to use
ifeq ($(TARGETTYPE),library)
 LNK := $(AR)
else
 ifeq ($(wildcard $(filter %.cpp,$(SOURCES))),)
  LNK := $(CC)
 else
  LNK := $(CXX)
 endif
endif

# Linking objects
LNKOBJS_WC := $(wildcard $(CUR_LNKLIBS))
LNKOBJS := $(foreach LIB,$(filter-out $(LNKOBJS_WC),$(CUR_LNKLIBS)),-l$(LIB))\
 $(foreach FW,$(LNKFWS),-framework $(FW)) $(LNKOBJS_WC)
AROBJS :=

ALLNAME ?= all
CLEANNAME ?= clean
RUNNAME ?= run

# Fakers
.PHONY: $(ALLNAME) $(CLEANNAME) $(RUNNAME)

# Auto-targets
$(ALLNAME): $(TARGET)

$(CLEANNAME):
	-@rm -f $(wildcard $(CLEANFILES)) 2>/dev/null

$(RUNNAME): $(TARGET)
	$(info [run ] $@)
	$(TARGET) $(ARGS)

# Link
$(TARGET): $(OBJECTS)
	$(info [link] $@)
	@mkdir -p $(dir $@)
	@$(LNK) $(CUR_$(TARGETLINKTYPE)FLAGS) -o "$@" $^ $($(TARGETLINKTYPE)OBJS)

# Compile
$(OBJDIR)%.cpp.o: %.cpp
	$(info [c++ ] $<)
	@mkdir -p $(dir $@)
	@$(CXX) $(strip $(CUR_CXXFLAGS) $(CUR_POSTFLAGS)) "$@" "$<"

$(OBJDIR)%.c.o: %.c
	$(info [cc  ] $<)
	@mkdir -p $(dir $@)
	@$(CC) $(strip $(CUR_CFLAGS) $(CUR_POSTFLAGS)) "$@" "$<"

# Dependencies
ifneq ($(MAKECMDGOALS),clean)
 ifneq ($(DEPENDS),)
  -include $(DEPENDS)
 endif
endif
