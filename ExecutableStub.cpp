﻿#include "ExecutablePacker.hpp"

void printImmediately( const char *message ) {
	fprintf( stdout, "%s\n", message );
	fflush( stdout );
}

int main() {
	char name[ 512 ];
	
	GetModuleFileNameA( NULL, name, sizeof( name ) );
	
	printImmediately( "Beginning unpack..." );
	try {
		Ax::ExecutablePacker packer;
		
		packer.unpack< Ax::kExecPack_AppendExecutable >( ".", name );
		printImmediately( "Completed unpack." );
	} catch( const Ax::Exception &except ) {
		fprintf( stderr, "ERROR: %s\n", except.getMessage() );
		exit( EXIT_FAILURE );
	} catch( ... ) {
		fprintf( stderr, "ERROR: (unknown)\n" );
		exit( EXIT_FAILURE );
	}
	
	return EXIT_SUCCESS;
}
