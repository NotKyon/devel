#include "utility.h"

#include "memory.h"
#include "report.h"

#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

const char *va(const char *format, ...) {
	static char buf[0x10000];
	static size_t i = 0;
	va_list args;
	size_t j;
	int r;

	ASSERT(format != (const char *)0);

	j = i;

	va_start(args, format);
	r = vsnprintf(&buf[j], sizeof(buf)-1-j, format, args);
	if (r==-1) {
		/* overflow prevented? */
		buf[sizeof(buf)-1] = 0;
		i = 0;
	} else {
		/* advance to the next position in the buffer */
		buf[j + r] = 0;
		i += r + 1;
	}
	va_end(args);

	return &buf[j];
}

int Shell(int show, const char *format, ...) {
	static char cmd[16384];
	va_list args;

	ASSERT(format != (const char *)0);

	va_start(args, format);
	vsnprintf(cmd, sizeof(cmd) - 1, format, args);
	cmd[sizeof(cmd) - 1] = 0;
	va_end(args);

	if (show) {
		fprintf(stdout, "%s\n", cmd);
		fflush(stdout);
	}

	return system(cmd);
}

int GetIntegralDate() {
	struct tm *p;
	time_t t;

	t = time(0);
	p = localtime(&t);

	return (p->tm_year + 1900)*10000 + (p->tm_mon + 1)*100 + p->tm_mday;
}

bool_t Wildcard(const char *source, const char *pattern) {
	while(1) {
		if (*pattern=='*') {
			while(*pattern=='*')
				pattern++;

			if (*pattern=='\0')
				return TRUE;

			while(*source != *pattern) {
				if (*source=='\0')
					return FALSE;

				source++;
			}

			continue;
		}

		if (*source != *pattern)
			break;

		if (*source=='\0')
			return TRUE;

		source++;
		pattern++;
	}

	return FALSE;
}
