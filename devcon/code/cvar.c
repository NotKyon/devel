#include "cvar.h"
#include "memory.h"
#include "string.h"
#include "system.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* cvars */
int g_cvarsAlive = 0;
cvarSystem_t g_cvars;

/*
 * =============================================================================
 *
 *	CVAR FUNCTIONS
 *
 * =============================================================================
 */

int cvar_Init() {
	if (g_cvarsAlive)
		return 0;

	g_cvars.head = (cvar_t *)0;
	g_cvars.tail = (cvar_t *)0;

	if (!InitLookup(&g_cvars.table, LOOKUP_IDENT))
		return 0;

	g_cvarsAlive = 1;
	return 1;
}

void cvar_Deinit() {
	if (!g_cvarsAlive)
		return;

	while(g_cvars.head)
		cvar_Delete(g_cvars.head);

	DeinitLookup(&g_cvars.table);

	g_cvarsAlive = 0;
}

int cvar_IsAlive() {
	return g_cvarsAlive;
}

cvar_t *cvar_Find(const char *name) {
	return (cvar_t *)GetEntry(&g_cvars.table, name);
}

cvar_t *cvar_New(const char *name, int flags, const char *value) {
	cvar_t *v;

	v = cvar_Find(name);
	if (v) {
		if (v->flags & kCVar_Const_Bit)
			return v;
	} else {
		v = (cvar_t *)mem_AllocZero(sizeof(*v));
		if (!v)
			return (cvar_t *)0;

		if (!NewEntry(&g_cvars.table, name, (void *)v)) {
			mem_Free((void *)v);
			return (cvar_t *)0;
		}
	}

	v->name = str_Copy(v->name, name);
	if ((v->flags & kCVar_Type_Mask) == kCVar_String)
		mem_Free((void *)v->value.s);

	v->flags = flags;

	v->value.s = str_Duplicate((const char *)value);
	v->value.i = atoi(v->value.s);
	v->value.f = atof(v->value.s);

	if ((v->flags & kCVar_Type_Mask) == kCVar_Boolean)
		v->value.i &= 1;

	v->next = (cvar_t *)0;
	if ((v->prev = g_cvars.tail) != (cvar_t *)0)
		g_cvars.tail->next = v;
	else
		g_cvars.head = v;
	g_cvars.tail = v;

	return v;
}
void cvar_Delete(cvar_t *cvar) {
	if (!cvar)
		return;

	if (cvar->prev)
		cvar->prev->next = cvar->next;
	if (cvar->next)
		cvar->next->prev = cvar->prev;

	if (g_cvars.head==cvar)
		g_cvars.head = cvar->next;
	if (g_cvars.tail==cvar)
		g_cvars.tail = cvar->prev;

	SetEntry(&g_cvars.table, cvar->name, (void *)0);

	mem_Free((void *)cvar->name);
	mem_Free((void *)cvar);
}

const char *cvar_Name(const cvar_t *cvar) {
	return cvar->name;
}

int cvar_Type(const cvar_t *cvar) {
	return cvar->flags & kCVar_Type_Mask;
}

void cvar_ConstOn(cvar_t *cvar) {
	cvar->flags |= kCVar_Const_Bit;
}
void cvar_ConstOff(cvar_t *cvar) {
	cvar->flags &= ~kCVar_Const_Bit;
}
int cvar_IsConst(const cvar_t *cvar) {
	return (cvar->flags & kCVar_Const_Bit) ? 1 : 0;
}

void cvar_NoCheatOn(cvar_t *cvar) {
	cvar->flags |= kCVar_NoCheat_Bit;
}
void cvar_NoCheatOff(cvar_t *cvar) {
	cvar->flags &= ~kCVar_NoCheat_Bit;
}
int cvar_IsNoCheat(const cvar_t *cvar) {
	return (cvar->flags & kCVar_NoCheat_Bit) ? 1 : 0;
}

void cvar_ModifiedOn(cvar_t *cvar) {
	cvar->flags |= kCVar_Modified_Bit;
}
void cvar_ModifiedOff(cvar_t *cvar) {
	cvar->flags &= ~kCVar_Modified_Bit;
}
int cvar_IsModified(const cvar_t *cvar) {
	return (cvar->flags & kCVar_Modified_Bit) ? 1 : 0;
}

void cvar_MinMaxOn(cvar_t *cvar) {
	cvar->flags |= kCVar_MinMax_Bit;
}
void cvar_MinMaxOff(cvar_t *cvar) {
	cvar->flags &= ~kCVar_MinMax_Bit;
}
int cvar_IsMinMax(const cvar_t *cvar) {
	return (cvar->flags & kCVar_MinMax_Bit) ? 1 : 0;
}

void cvar_SetString(cvar_t *cvar, const char *s) {
	ASSERT(cvar_Type(cvar) == kCVar_String);

	cvar->value.s = str_Copy(cvar->value.s, s);
	cvar_ModifiedOn(cvar);
}
void cvar_SetInteger(cvar_t *cvar, int i) {
	char buf[64];

	ASSERT(cvar_Type(cvar)==kCVar_Integer || cvar_Type(cvar)==kCVar_Boolean);

	snprintf(buf, sizeof(buf)-1, "%i", i);
	buf[sizeof(buf) - 1] = 0;

	cvar->value.s = str_Copy(cvar->value.s, buf);

	cvar->value.i = i;
	cvar->value.f = (float)i;
}
void cvar_SetBoolean(cvar_t *cvar, int i) {
	ASSERT(cvar_Type(cvar) == kCVar_Boolean);

	cvar_SetInteger(cvar, i%2);
}
void cvar_SetFloat(cvar_t *cvar, float f) {
	char buf[64];

	ASSERT(cvar_Type(cvar) == kCVar_Float);

	snprintf(buf, sizeof(buf)-1, "%f", f);
	buf[sizeof(buf)-1] = 0;

	cvar->value.s = str_Copy(cvar->value.s, buf);

	cvar->value.i = (int)f;
	cvar->value.f = f;
}

const char *cvar_String(const cvar_t *cvar) {
	ASSERT(cvar_Type(cvar) == kCVar_String);

	return cvar->value.s;
}
int cvar_Integer(cvar_t *cvar) {
	ASSERT(cvar_Type(cvar)==kCVar_Integer || cvar_Type(cvar)==kCVar_Boolean);

	if (cvar_IsModified(cvar)) {
		cvar->value.i = atoi(cvar->value.s);
		cvar->value.f = atof(cvar->value.s);

		cvar_ModifiedOff(cvar);
	}

	return cvar->value.i;
}
int cvar_Boolean(cvar_t *cvar) {
	ASSERT(cvar_Type(cvar) == kCVar_Boolean);

	return cvar_Integer(cvar) % 2;
}
float cvar_Float(cvar_t *cvar) {
	ASSERT(cvar_Type(cvar) == kCVar_Float);

	if (cvar_IsModified(cvar)) {
		cvar->value.i = atoi(cvar->value.s);
		cvar->value.f = atof(cvar->value.s);

		cvar_ModifiedOff(cvar);
	}

	return cvar->value.f;
}

void cvar_SetMinMaxI(cvar_t *cvar, int l, int h) {
	ASSERT(cvar_Type(cvar) == kCVar_Integer);

	cvar->l.i = l;
	cvar->h.i = h;
}
void cvar_SetMinMaxF(cvar_t *cvar, float l, float h) {
	ASSERT(cvar_Type(cvar) == kCVar_Float);

	cvar->l.f = l;
	cvar->h.f = h;
}

int cvar_MinI(const cvar_t *cvar) {
	ASSERT(cvar_Type(cvar) == kCVar_Integer);

	return cvar->l.i;
}
int cvar_MaxI(const cvar_t *cvar) {
	ASSERT(cvar_Type(cvar) == kCVar_Integer);

	return cvar->h.i;
}

float cvar_MinF(const cvar_t *cvar) {
	ASSERT(cvar_Type(cvar) == kCVar_Float);

	return cvar->l.f;
}
float cvar_MaxF(const cvar_t *cvar) {
	ASSERT(cvar_Type(cvar) == kCVar_Float);

	return cvar->h.f;
}

cvar_t *cvar_First() {
	return g_cvars.head;
}
cvar_t *cvar_Last() {
	return g_cvars.tail;
}
cvar_t *cvar_Before(cvar_t *cvar) {
	return cvar->prev;
}
cvar_t *cvar_After(cvar_t *cvar) {
	return cvar->next;
}
