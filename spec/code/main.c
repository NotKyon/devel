#include "main.h"

enum {
	kFlag0_Help_Bit = (1<<0),
	kFlag0_ShowVersion_Bit = (1<<1),
	kFlag0_Verbose_Bit = (1<<2)
};
static int gFlags[1] = { 0 };
static char *gHelpArg = (char *)NULL;

static void ShowHelp(const char *parm);

/* -------------------------------------------------------------------------- */

opt_t gOptTable[] = {

	OPT_SWITCH_PARM_LOOSE("help", ShowHelp, &gFlags[0], kFlag0_Help_Bit),
	OPT_SWITCH("version", &gFlags[0], kFlag0_ShowVersion_Bit),
	OPT_SWITCH("verbose", &gFlags[0], kFlag0_Verbose_Bit)

};
size_t gOptTableSize = sizeof(gOptTable)/sizeof(gOptTable[0]);

/* -------------------------------------------------------------------------- */

static void ShowHelp(const char *parm) {
	gHelpArg = Copy(gHelpArg, parm);
}

/* -------------------------------------------------------------------------- */

void Init(int argc, char **argv) {
	memset((void *)gOptAlias, 0, sizeof(gOptAlias));
	gOptAlias['h'] = "help";
	gOptAlias['v'] = "version";
	gOptAlias['V'] = "verbose";

	ProcessOpts(argc, argv);
}
void Fini() {
	FiniFiles();
}

/* -------------------------------------------------------------------------- */

int main(int argc, char **argv) {
	size_t i, n;
	ir_t *ir;

	Init(argc, argv);
	atexit(Fini);

	if (gFlags[0] & kFlag0_Help_Bit)
		printf("help! %s\n", gHelpArg ? gHelpArg : "(null)");
	if (gFlags[0] & kFlag0_ShowVersion_Bit)
		printf("version!\n");
	if (gFlags[0] & kFlag0_Verbose_Bit)
		printf("verbose!\n");

	ir = NewIR();

	{
		type_t *type;
		ns_t *ns;

		NewLib();

		ns = NewNamespace((ns_t *)NULL);
		SetNamespaceName(ns, "ember");
		SetNamespaceAbrv(ns, "ee");

		type = NewTypedef();
		SetTypedefName(type, "i32");
		SetTypedefFrom(type, "int");

		type = NewTypedef();
		SetTypedefName(type, "i64");
		SetTypedefFrom(type, "__int64");

		AddIRNamespaceToken(ir, ns);
	}

	AddIRTokenWithType(ir, kIR_TokType_Enumerations);
	AddIRTokenWithType(ir, kIR_TokType_Typedefs);
	AddIRTokenWithType(ir, kIR_TokType_Interfaces);
	AddIRTokenWithType(ir, kIR_TokType_Functions);

	n = CountFiles();
	for(i=0; i<n; i++) {
		printf("%2u/%u: %s\n", (unsigned int)(i + 1), (unsigned int)n,
			GetFile(i));

		Gen(ir, GetFile(i));
	}

	return EXIT_SUCCESS;
}
