#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

struct node_s {
	char *name;
	char *link;

	struct node_s *prev, *next;
	struct node_s *head, *tail;
	struct node_s *prnt;
};
typedef struct node_s node_t;

void *x_alloc( int n ) {
	void *p;

	if( n <= 0 ) {
		fprintf( stderr, "ERROR: INVALID MEMORY SIZE '%i'\n", n );
		exit( EXIT_FAILURE );
	}

	p = malloc( ( size_t )n );
	if( !p ) {
		fprintf( stderr, "ERROR: OUT OF MEMORY (ALLOC SIZE '%i') %s\n", n, strerror( errno ) );
		exit( EXIT_FAILURE );
	}

	memset( p, 0, n );
	return p;
}
void *x_dealloc( void *p ) {
	free( p );
	return NULL;
}

char *x_strndup( char **ptr, const char *src, int n ) {
	char *p;

	p = NULL;
	if( src != NULL ) {
		if( n == -1 ) {
			n = ( int )strlen( src );
		}

		p = ( char * )x_alloc( n + 1 );
		memcpy( ( void * )p, ( const void * )src, n );
	}

	if( ptr != NULL ) {
		x_dealloc( *ptr );
		*ptr = p;
	}

	return p;
}
char *x_strdup( char **ptr, const char *src ) {
	return x_strndup( ptr, src, -1 );
}

node_t *new_node( node_t *prnt ) {
	node_t *p;

	p = ( node_t * )x_alloc( sizeof( *p ) );
	p->prnt = prnt;
	if( prnt != NULL ) {
		p->prev = prnt->tail;
		p->next = NULL;
		if( prnt->tail != NULL ) {
			prnt->tail->next = p;
		} else {
			prnt->head = p;
		}
		prnt->tail = p;
	}

	return p;
}
node_t *delete_node( node_t *p ) {
	if( !p ) {
		return NULL;
	}

	if( p->prev != NULL ) {
		p->prev->next = p->next;
	} else if( p->prnt != NULL ) {
		p->prnt->head = p->next;
	}
	p->next = NULL;

	if( p->next != NULL ) {
		p->next->prev = p->prev;
	} else if( p->prnt != NULL ) {
		p->prnt->tail = p->prev;
	}
	p->prev = NULL;

	p->prnt = NULL;

	p->name = ( char * )x_dealloc( ( void * )p->name );
	p->link = ( char * )x_dealloc( ( void * )p->link );

	return ( node_t * )x_dealloc( ( void * )p );
}

node_t *find_tabbed_node( node_t *root, int level ) {
	node_t *p;
	int i;

	if( !root ) {
		fprintf( stderr, "ERROR: INVALID ROOT NODE\n" );
		exit( EXIT_FAILURE );
	}

	p = root;
	for( i = 0; i < level; ++i ) {
		p = p->tail;
		if( !p ) {
			fprintf( stderr, "ERROR: NOT ENOUGH NODES (LEVEL '%i')\n", i );
			exit( EXIT_FAILURE );
		}
	}

	return p;
}

char *read_file( const char *filename ) {
	FILE *f;
	char *p;
	int n;

#if defined( _MSC_VER ) && defined( __STDC_WANT_SECURE_LIB__ )
	f = NULL;
	if( fopen_s( &f, filename, "rb" ) != 0 ) {
		fprintf( stderr, "ERROR: COULD NOT READ FILE (\"%s\") %i\n", filename, ( int )errno );
		exit( EXIT_FAILURE );
	}
#else
	f = fopen( filename, "rb" );
	if( !f ) {
		fprintf( stderr, "ERROR: COULD NOT READ FILE (\"%s\") %i\n", filename, ( int )errno );
		exit( EXIT_FAILURE );
	}
#endif

	fseek( f, 0, SEEK_END );
	n = ( int )ftell( f );

	fseek( f, 0, SEEK_SET );
	p = ( char * )x_alloc( n + 1 );

	fread( ( void * )p, n, 1, f );
	p[ n ] = '\0';

	fclose( f );
	f = NULL;

	return p;
}

void write_html_node( FILE *f, node_t *node, int level ) {
	node_t *p;
	int opened;

	opened = 0;
	if( node->link == NULL || *node->link == '\0' ) {
		opened = 1;
		fprintf
		(
			f,
			"<a class=\"folded\" onclick=\"toggle(this)\">%s</a>"
			"<br />"
			"<div class=\"treeview_branch\">\r\n",
			node->name
		);
	} else {
		fprintf( f, "<a href=\"%s\">%s</a><br />\r\n", node->link, node->name );
	}
	for( p = node->head; p != NULL; p = p->next ) {
		write_html_node( f, p, level + 1 );
	}
	if( opened ) {
		fprintf( f, "</div>\r\n" );
	}
}
void write_html( const char *filename, node_t *root ) {
	FILE *f;
	node_t *p;

	if( strcmp( filename, "-" ) == 0 ) {
		f = stdout;
	} else {
#if defined( _MSC_VER ) && defined( __STDC_WANT_SECURE_LIB__ )
		f = NULL;
		if( fopen_s( &f, filename, "wb" ) != 0 ) {
			fprintf( stderr, "ERROR: COULD NOT WRITE FILE (\"%s\") %i\n", filename, ( int )errno );
			exit( EXIT_FAILURE );
		}
#else
		f = fopen( filename, "wb" );
		if( !f ) {
			fprintf( stderr, "ERROR: COULD NOT WRITE FILE (\"%s\") %i\n", filename, ( int )errno );
			exit( EXIT_FAILURE );
		}
#endif
	}

	fprintf
	(
		f,
		"<html>\r\n"
		"<head>\r\n"
		" <link rel=\"stylesheet\" type=\"text/css\" href=\"ngdocindex.css\" />\r\n"
		" <script src=\"ngdocindex.js\"></script>\r\n"
		"</head>\r\n"
		"<body><!-- キョン was here -->\r\n"
	);
	for( p = root->head; p != NULL; p = p->next ) {
		write_html_node( f, p, 0 );
	}
	fprintf( f, "</body></html>\r\n" );
}

const char *parse_line( node_t *root, const char *src, int line ) {
	const char *e, *p, *q;
	const char *next;
	node_t *prnt;
	node_t *node;
	int level;

	if( !root ) {
		fprintf( stderr, "ERROR: EXPECTED NON-NULL ROOT NODE (parse_line)\n" );
		exit( EXIT_FAILURE );
	}

	if( !src || *src == '\0' ) {
		return src;
	}

	level = 0;
	while( *src == '\t' ) {
		++src;
		++level;
	}

	e = strchr( src, '\n' );
	if( !e ) {
		e = strchr( src, '\0' );
		next = e;
	} else {
		next = e + 1;
		if( e > src && *( e - 1 ) == '\r' ) {
			--e;
		}
	}

	if( src == e ) {
		return next;
	}

	p = strchr( src, '|' );
	if( !p || p > e ) {
		fprintf( stderr, "INVALID LINE %i: %.*s\n", line, e - src, src );
		return next;
	}

	prnt = find_tabbed_node( root, level );
	node = new_node( prnt );

	x_strndup( &node->name, src, p - src );

	++p;

	if( *p == '1' ) {
		return next;
	}

	if( *p != '0' || *( p + 1 ) != '|' ) {
		fprintf( stderr, "INVALID LINE %i (expected '0|'): %.*s\n", line, e - src, src );
		return next;
	}

	p += 2;

	if( strncmp( p, "<a href=\"", 9 ) != 0 ) {
		fprintf( stderr, "INVALID LINE %i (expected href): %.*s\n", line, e - src, src );
		return next;
	}

	p += 9;

	q = strchr( p, '\"' );
	if( !q || q > e ) {
		fprintf( stderr, "INVALID LINE %i (expected '\"'): %.*s\n", line, e - src, src );
		return next;
	}

	x_strndup( &node->link, p, q - p );
	return next;
}

void process( const char *dstfile, const char *srcfile ) {
	char *srctext;
	const char *p;
	node_t *root;
	int line;

	if( !dstfile || !srcfile || *dstfile == '\0' || *srcfile == '\0' ) {
		fprintf( stderr, "ERROR: NEED FILE NAMES (process)\n" );
		exit( EXIT_FAILURE );
	}

	if( strcmp( dstfile, srcfile ) == 0 ) {
		fprintf( stderr, "ERROR: OUTPUT FILE AND INPUT FILE MATCH\n" );
		exit( EXIT_FAILURE );
	}

	srctext = read_file( srcfile );
	root = new_node( NULL );

	p = srctext;
	line = 1;
	while( p != NULL && *p != '\0' ) {
		p = parse_line( root, p, line );
		++line;
	}

	srctext = ( char * )x_dealloc( ( void * )srctext );

	write_html( dstfile, root );
	root = delete_node( root );
}

int main( int argc, char **argv ) {
	int i;

	for( i = 1; i < argc; ++i ) {
		process( "ngdocindex.html", argv[ i ] );
	}

	return EXIT_SUCCESS;
}
