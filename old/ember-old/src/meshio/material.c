#include <meshio/config.h>
#include <meshio/core.h>
#include <meshio/texture.h>
#include <meshio/material.h>

static size_t g_mtrl_count           = 1;
static meshio_material_t g_mtrl_head = ( meshio_material_t )0,
                         g_mtrl_tail = ( meshio_material_t )0;

meshio_material_t meshio_alloc_material()
{
	meshio_material_t mtrl;
	unsigned int i;

	if( !( mtrl = ( meshio_material_t )meshio_memory( 0, sizeof( *meshio_material_t ) ) ) )
		return ( meshio_material_t )0;

	mtrl->refcnt = 1;

	mtrl->name.n = 0;
	mtrl->name.p = ( char * )0;

	mtrl->flags = 0x00000000;

	for( i = 0; i < 4; i++ ) {
		mtrl->ambient[ i ]  = 1.0f;
		mtrl->diffuse[ i ]  = 1.0f;
		mtrl->specular[ i ] = 1.0f;
		mtrl->emissive[ i ] = 1.0f;
		mtrl->shininess     = 1.0f;
	}

	mtrl->texture_enabled = 0x0000;
	for( i = 0; i < 16; i++ ) {
		mtrl->texture_coordinate_index[ i ] = i;
		mtrl->texture[ i ]                  = ( meshio_texture_t )0;
	}

	mtrl->next = ( meshio_material_t )0;
	if( ( mtrl->prev = g_mtrl_tail ) != ( meshio_material_t )0 )
		g_mtrl_tail->next = mtrl;
	else
		g_mtrl_head = mtrl;
	g_mtrl_tail     = mtrl;

	g_mtrl_count++;
	return mtrl;
}
meshio_material_t meshio_dealloc_material( meshio_material_t mtrl )
{
	unsigned int i;

	for( i = 0; i < 16; i++ )
		if( mtrl->texture[ i ] )
			mtrl->texture[ i ] = meshio_release_texture( mtrl->texture[ i ] );

	if( mtrl->prev )
		mtrl->prev->next = mtrl->next;
	if( mtrl->next )
		mtrl->next->prev = mtrl->prev;

	if( g_mtrl_head == mtrl )
		g_mtrl_head = mtrl->next;
	if( g_mtrl_tail == mtrl )
		g_mtrl_tail = mtrl->prev;

	g_mtrl_count--;
	return ( meshio_material_t )meshio_memory( ( void * )mtrl, 0 );
}

void meshio_retain_material( meshio_material_t mtrl )
{
	mtrl->refcnt++;
}
meshio_material_t meshio_release_material( meshio_material_t mtrl )
{
	return --mtrl->refcnt == 0 ? meshio_dealloc_material( mtrl ) : ( meshio_material_t )0;
}

size_t meshio_material_count()
{
	return g_mtrl_count;
}
meshio_material_t meshio_first_material()
{
	return g_mtrl_head;
}
meshio_material_t meshio_last_material()
{
	return g_mtrl_tail;
}
meshio_material_t meshio_material_before( meshio_material_t mtrl )
{
	return mtrl->prev;
}
meshio_material_t meshio_material_after( meshio_material_t mtrl )
{
	return mtrl->next;
}

size_t meshio_material_name_length( meshio_material_t mtrl )
{
	return mtrl->name.n;
}
const char *meshio_material_name( meshio_material_t mtrl )
{
	return mtrl->name.p ? mtrl->name.p : "";
}

void meshio_set_material_name( meshio_material_t mtrl, const char *name )
{
	size_t i, n;

	n            = name ? strlen( name ) : 0;
	mtrl->name.p = ( char * )meshio_memory( ( void * )mtrl->name.p, n + 1 );

	for( i = 0; i < n; i++ )
		mtrl->name.p[ i ] = name[ i ];
	mtrl->name.p[ n ]     = 0;
	mtrl->name.n          = n;
}

bitfield_t meshio_material_flags( meshio_material_t mtrl )
{
	return mtrl->flags;
}
void meshio_set_material_flags( meshio_material_t mtrl, bitfield_t flags )
{
	mtrl->flags = flags;
}

const float *meshio_material_ambient( meshio_material_t mtrl )
{
	return &mtrl->ambient[ 0 ];
}
const float *meshio_material_diffuse( meshio_material_t mtrl )
{
	return &mtrl->diffuse[ 0 ];
}
const float *meshio_material_specular( meshio_material_t mtrl )
{
	return &mtrl->specular[ 0 ];
}
const float *meshio_material_emissive( meshio_material_t mtrl )
{
	return &mtrl->emissive[ 0 ];
}
float meshio_material_shininess( meshio_material_t mtrl )
{
	return mtrl->shininess;
}

void meshio_set_material_ambient( meshio_material_t mtrl, const float *rgba )
{
	unsigned int i;

	for( i = 0; i < 4; i++ )
		mtrl->ambient[ i ] = rgba[ i ];
}
void meshio_set_material_diffuse( meshio_material_t mtrl, const float *rgba )
{
	unsigned int i;

	for( i = 0; i < 4; i++ )
		mtrl->diffuse[ i ] = rgba[ i ];
}
void meshio_set_material_specular( meshio_material_t mtrl, const float *rgba )
{
	unsigned int i;

	for( i = 0; i < 4; i++ )
		mtrl->specular[ i ] = rgba[ i ];
}
void meshio_set_material_emissive( meshio_material_t mtrl, const float *rgba )
{
	unsigned int i;

	for( i = 0; i < 4; i++ )
		mtrl->emissive[ i ] = rgba[ i ];
}
void meshio_set_material_shininess( meshio_material_t mtrl, float shininess )
{
	mtrl->shininess = shininess;
}

int meshio_material_texture_enabled( meshio_material_t mtrl, unsigned int i )
{
	return ( int )( mtrl->texture_enabled & ( 1 << i ) );
}
uint8_t meshio_material_texture_coordinate_index( meshio_material_t mtrl,
    unsigned int i )
{
	return mtrl->texture_coordinate_index[ i ];
}
meshio_texture_t meshio_material_texture( meshio_material_t mtrl,
    unsigned int i )
{
	return mtrl->texture[ i ];
}

void meshio_enable_material_texture( meshio_material_t mtrl, unsigned int i )
{
	mtrl->texture_enabled |= ( 1 << i );
}
void meshio_disable_material_texture( meshio_material_t mtrl, unsigned int i )
{
	mtrl->texture_enabled &= ~( 1 << i );
}
void meshio_set_material_texture_coordinate_index( meshio_material_t mtrl,
    unsigned int i, uint8_t tci )
{
	mtrl->texture_coordinate_index[ i ] = tci;
}
void meshio_set_material_texture( meshio_material_t mtrl, unsigned int i,
    meshio_texture_t tex )
{
	if( tex )
		meshio_retain_texture( tex );
	if( mtrl->texture[ i ] )
		meshio_release_texture( mtrl->texture[ i ] );
	mtrl->texture[ i ] = tex;
}
