define RECIPE
$$(TARGET): $$(PREREQUISITE)
	gcc -o $$@ $$<


endef
define \n


endef
define \t
	
endef
#RECIPE = $(1): $(2)${\n}${\t}$gcc -o $@ $<${\n}${\n}

PROJECTS := abs ascii

.PHONY: all clean

all: $(PROJECTS)

clean:
	-@rm -f $(wildcard $(foreach p,$(PROJECTS),$(P) $(P).exe)) 2>/dev/null

TARGET = abs
PREREQUISITE = abs.c
RECIPE

TARGET = ascii
PREREQUISITE = ascii.c
RECIPE
#$(foreach P,$(PROJECTS),$(call RECIPE,$(P),$(P).c))
#$(foreach P,$(PROJECTS),$(P): $(P).c${\n}${\t}$gcc -o $@ $<${\n}${\n})
