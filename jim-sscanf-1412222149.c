#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	float a;
	char b;

	a = 0.0;
	b = '\?';
	sscanf( "10M", "%f%c", &a, &b );
	
	printf( "%f%c\n", a, b );

	return EXIT_SUCCESS;
}
