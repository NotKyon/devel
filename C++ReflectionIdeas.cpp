/*

	Reflection In C++ (Compile-time type information)

*/

// find the offset of a member within a type
template< class T, auto T::&M >
constexpr size_t member_offset()
{
	return
		static_cast< size_t >(
			&reinterpret_cast< char & >(
				reinterpret_cast< T * >( nullptr )->M 
			)
		);

	// or... __builtin_offsetof( T, M )
}
// Example
struct Item { int x; int y; };
static const size_t kItemYOff1 = member_offset< Item, Item::y >(); //expanded
static const size_t kItemYOff2 = member_offset< Item::y >(); //implied

// Enumerate all members of a type
template< typename Type, auto Type::&Members... >
struct Example1 {};

// Enumerate all public members of a type
template< typename Type, auto Type::&public Members... >
struct Example2 {};

// Enumerate all private members of a type
template< typename Type, auto Type::&private Members... >
struct Example3 {};

// Enumerate all public members beginning with "m_"
template< typename Type, auto Type::&public "m_" Members... >
struct Example4 {};

// Enumerate all private members ending with "__"
template< typename Type, auto Type::&private Members "__"... >
struct Example5 {};


// retrieve a pointer to a base class from a member
template< class T, auto T::&M, typename MemberType = decltype( M ) >
T* base_from_member( MemberType &member )
{
	return
		reinterpret_cast< T * >(
			reinterpret_cast< size_t >( &member ) - member_offset< T, M >()
		);
}

// generate a non-const version from a given const function
//
// the default parameter for 'T' specifies that it should be the same type as
// the class it resides in
//
// the default parameter for 'Function' specifies that the function should be
// the definition that follows
template< class T = this, auto &Function(Args...) = default >
typename generate_nonconst
{
	typename remove_const< T >::type Function( Args... ) = default;
	typename add_const< T >::type Function( Args... ) const = default;
};

// intrusive list node
//
// the "auto T::&M = *this" syntax is to specify that the member in question is
// the instance of the type, by default. Therefore, 'T' will be the type of
// whatever 'intrusive_list_node's instance resides within
template< class T, auto T::&M = *this >
class intrusive_list_node
{
public:
	typedef T value_type;
	typedef decltype( M ) link_type;
	typedef intrusive_list_node< T, M > this_type;
	// NOTE: link_type and this_type will be the exact same type
	typedef value_type *pointer;
	typedef value_type &reference;

	// A single keyword for the constructor would be awesome...
	__construct()
	{
	}
	// Same with the destructor...
	__destruct()
	{
		remove();
	}

	// The use of generate_const here causes two versions of function following
	// it to be generated from the same code

	generate_const<> prev() { return prev; }
	generate_const<> next() { return next; }

	/*
		The above is the same as:

		intrusive_list_node *prev() { return prev; }
		const intrusive_list_node *prev() const { return prev; }

		intrusive_list_node *next() { return next; }
		const intrusive_list_node *next() const { return next; }
	*/

	// If a return type isn't specified, assume void
	remove()
	{
		prev->next = next;
		next->prev = prev;

		prev = this;
		next = this;
	}

	generate_const< pointer > operator->()
	{
		return base_from_member< T, M >( *this );
	}
	generate_const< reference > operator*()
	{
		return *base_from_member< T, M >( *this );
	}

private:
	intrusive_list_node *prev = this;
	intrusive_list_node *next = this;
};

/*

	Generic Programming Ideas

*/

// Pure template class
//
// A pure template class provides an interface that classes which comply with it
// must implement
//
// Any code that is implemented in a pure template class should be considered
// the default implementation, but that would have to be explicitly requested
// through a 'using' statement in the code.

// Putting 'template' after 'class' (or before 'Base') specifies that it is a
// pure template class. In prior versions of C++ this is an invalid operation,
// so it should not cause conflict with existing code-bases. Additionally, this
// approach avoids the introduction of a new keyword into the language, which
// also helps ensure existing code will not have conflict.
class template Base
{
public:
	int PerformOp( int x, int y ) const;
};
// The derivation puts 'template' before 'Base' as a way to clarify between
// normal classes and template classes. The semantics of their application would
// differ in that inheriting from a template class requires that it has no run-
// time layout requirements, and so the compiler is free to do what is optimal.
// (Unlike a pure virtual interface.) Additionally, normal classes can be
// inherited as template classes. That will be covered later.
class DerivedA: public template Base
{
public:
	int PerformOp( int x, int y ) const { return x + y; }
};
// Another derivation
class DerivedB
{
public:
	int PerformOp( int x, int y ) const { return x - y; };
};

// Generic code can then ask for a template class in the parameters, in place of
// a more general 'typename', which would allow for more strictness. (For
// example, standard container classes take an Alloc template parameter which is
// generally expected to have a specific set of functions, typedefs, and
// constants which might not be accounted for. In situations where that is not
// desirable, this provides a practical solution.
template< typename T >
class template Allocator
{
public:
	// An incomplete typedef that needs to be implemented
	//
	// Use of the 'auto' keyword here is to signify an implementation-dependent
	// detail and is only valid for template classes.
	typedef auto value_type;
	typedef auto pointer;
	typedef auto reference;
	typedef auto const_pointer;
	typedef auto const_reference;
	typedef auto size_type;
	typedef auto difference_type;

	// The default parameter specified here will automatically carry over to
	// the inheriting class unless a different default is specified or
	// the parameter's declaration includes the 'explicit' keyword prior to the
	// type declaration. The 'explicit' keyword would indicate that the default
	// parameter is intentionally left out
	pointer allocate( size_type n, Allocator< void >::pointer hint = nullptr );
	void deallocate( pointer p, size_type n );
};
// Here is a specialization of the same allocator interface for void pointers
//
// Another new convention is the use of 'default' for the typename. This is to
// get at the default version of a type which may be specialized.
template<>
class template Allocator< void >: public template Allocator< default >
{
public:
	// 'void' here is meaningless
	// '= delete' is, to borrow prior C++11 syntax, used to remove a
	// typedef from an inherited template class. That is, to specifically state
	// that it is omitted.
	//
	// Note that this is a specialization of a template class. This
	// specialization allows the requirements to differ based on the type. In
	// the case of the allocator it is warranted because 'reference' and
	// 'const_reference' are meant to be omitted in void specialization.
	typedef void reference = delete;
	typedef void const_reference = delete;
};

// For completeness, here is an implementation based on the above
template< typename T >
class BasicAllocator: public template Allocator< T >
{
public:
	typedef T value_type;
	typedef T *pointer;
	typedef T &reference;
	typedef const T *const_pointer;
	typedef const T &const_reference;
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;

	// The use of 'auto' here is to signify that we don't care about this
	// parameter and it should be ignored. Any expression passed to the
	// parameter should be parsed and checked for type safety as usual, but must
	// not be executed at run-time. (The code should be considered dead and
	// removal of it is guaranteed.)
	pointer allocate( size_type n, auto )
	{
		return malloc( n );
	}
	// As stated elsewhere, if a return type isn't provided then 'void' should
	// be assumed.
	deallocate( pointer p, auto )
	{
		free( p );
	}
};

// Here's another example adapted from [thread.mutex.class]
namespace std
{

	class template Mutex
	{
	public:
		constexpr Mutex() noexcept;
		~Mutex();

		Mutex( const Mutex & ) = delete;
		Mutex &operator=( const Mutex & ) = delete;

		void lock();
		bool try_lock();
		void unlock();

		typedef auto native_handle_type;
		native_handle_type native_handle();
	};

}
