#ifndef MESHIO_MATERIAL_H
#define MESHIO_MATERIAL_H

#ifndef meshio_material_t_defined
#define meshio_material_t_defined
typedef struct MeshIO_Material *meshio_material_t;
#endif

#define MESHIO_MTRL_AMBIENT_BIT 0x00000001
#define MESHIO_MTRL_DIFFUSE_BIT 0x00000002
#define MESHIO_MTRL_SPECULAR_BIT 0x00000004
#define MESHIO_MTRL_EMISSIVE_BIT 0x00000008
#define MESHIO_MTRL_SHININESS_BIT 0x00000010

struct MeshIO_Material {
	unsigned int refcnt;

	struct {
		size_t n;
		char *p;
	} name;

	bitfield_t flags;

	float ambient[ 4 ];
	float diffuse[ 4 ];
	float specular[ 4 ];
	float emissive[ 4 ];
	float shininess;

	uint16_t texture_enabled;
	uint8_t texture_coordinate_index[ 16 ];
	struct MeshIO_Texture *texture[ 16 ];

	struct MeshIO_Material *prev,
	    *next;
};

extern meshio_material_t meshio_alloc_material();
extern meshio_material_t meshio_dealloc_material( meshio_material_t mtrl );
extern void meshio_retain_material( meshio_material_t mtrl );
extern meshio_material_t meshio_release_material( meshio_material_t mtrl );
extern size_t meshio_material_count();
extern meshio_material_t meshio_first_material();
extern meshio_material_t meshio_last_material();
extern meshio_material_t meshio_material_before( meshio_material_t mtrl );
extern meshio_material_t meshio_material_after( meshio_material_t mtrl );
extern size_t meshio_material_name_length( meshio_material_t mtrl );
extern extern const char *meshio_material_name( meshio_material_t mtrl );
extern void meshio_set_material_name( meshio_material_t mtrl, const char *name );
extern bitfield_t meshio_material_flags( meshio_material_t mtrl );
extern void meshio_set_material_flags( meshio_material_t mtrl, bitfield_t flags );
extern const float *meshio_material_ambient( meshio_material_t mtrl );
extern const float *meshio_material_diffuse( meshio_material_t mtrl );
extern const float *meshio_material_specular( meshio_material_t mtrl );
extern const float *meshio_material_emissive( meshio_material_t mtrl );
extern float meshio_material_shininess( meshio_material_t mtrl );
extern void meshio_set_material_ambient( meshio_material_t mtrl, const float *rgba );
extern void meshio_set_material_diffuse( meshio_material_t mtrl, const float *rgba );
extern void meshio_set_material_specular( meshio_material_t mtrl, const float *rgba );
extern void meshio_set_material_emissive( meshio_material_t mtrl, const float *rgba );
extern void meshio_set_material_shininess( meshio_material_t mtrl, float shininess );
extern int meshio_material_texture_enabled( meshio_material_t mtrl, unsigned int i );
extern uint8_t meshio_material_texture_coordinate_index( meshio_material_t mtrl, unsigned int i );
extern meshio_texture_t meshio_material_texture( meshio_material_t mtrl, unsigned int i );
extern void meshio_enable_material_texture( meshio_material_t mtrl, unsigned int i );
extern void meshio_disable_material_texture( meshio_material_t mtrl, unsigned int i );
extern void meshio_set_material_texture_coordinate_index( meshio_material_t mtrl, unsigned int i, uint8_t tci );
extern void meshio_set_material_texture( meshio_material_t mtrl, unsigned int i, meshio_texture_t tex );

#endif
