//
//	SHADOW WORLD - TEST
//
//	The idea is to make stencil shadows look trippy by replacing the underlying
//	world with a different world
//

[DefineVars=True]

Global gBaseMediaPath$ = "base"

SetCurrentDir( gBaseMediaPath )

Global gWindow = MakeWindowDX()
If Not gWindow Then DebugError "No window for you"

SetWindowTitle( gWindow, "Shadow World - NB v" + GetEngineVersion() )

Global gViewport = GetDefaultViewport()
Global gCamera = GetDefaultCamera()

Global gWaterRenderprogName$ = "decls/renderprogs/water.fx"
Global gWaterRenderprog = LoadShader( gWaterRenderprogName )
If Not gWaterRenderprog Then DebugError( "No " + gWaterRenderprogName + " for you" )

//SetShaderCurrentTechniqueByName( gWaterRenderprog, "waterSurfaceScroll_shadow" )
//SetShaderCurrentTechnique( gWaterRenderprog, 1 )

Global gWallTex = LoadTexture( "textures/d_industrialcity/concretetunnel1.png" )
ScaleTexture( gWallTex, 1.0/4.0, 1 )

Global gWallNX = MakePlane( 20, 4 )
RotateEnt( gWallNX, 90, 90, 0 )
PositionEnt( gWallNX, -10, 1.5, 0 )

Global gWallPX = MakePlane( 20, 4 )
RotateEnt( gWallPX, 90, -90, 0 )
PositionEnt( gWallPX, 10, 1.5, 0 )

Global gWallNZ = MakePlane( 20, 4 )
RotateEnt( gWallNZ, 90, 0, 0 )
PositionEnt( gWallNZ, 0, 1.5, -10 )

Global gWallPZ = MakePlane( 20, 4 )
RotateEnt( gWallPZ, 90, -180, 0 )
PositionEnt( gWallPZ, 0, 1.5, 10 )

SetEntTexture( gWallNX, gWallTex )
SetEntFullBright( gWallNX, True )

SetEntTexture( gWallPX, gWallTex )
SetEntFullBright( gWallPX, True )

SetEntTexture( gWallNZ, gWallTex )
SetEntFullBright( gWallNZ, True )

SetEntTexture( gWallPZ, gWallTex )
SetEntFullBright( gWallPZ, True )

Global gCeilTex = LoadTexture( "textures/d_industrialcity/ceiling1b_wet.png" )
ScaleTexture( gCeilTex, 1.0/2, 1.0/2 )

Global gCeil = MakePlane( 20, 20 )
RotateEnt( gCeil, 180, 0, 0 )
PositionEnt( gCeil, 0, 3.5, 0 )

SetEntTexture( gCeil, gCeilTex )
SetEntFullBright( gCeil, True )

/*
Global gShadowWorld = MakeWorld()
Global gShadowCam = MakeCamera()
SetEntWorld( gShadowCam, gShadowWorld )
*/

Global gFloorTex = LoadTexture( "textures/d_industrialcity/brokenconcrete1d_d.png" )
ScaleTexture( gFloorTex, 1.0/2, 1.0/2 )

Global gFloor = MakePlane( 20, 20 )

SetEntTexture( gFloor, gFloorTex )
SetEntFullBright( gFloor, True )

Global gShadowFloor = MakePlane( 20, 20 )
//SetEntWorld( gShadowFloor, gShadowWorld )
SetEntShader( gShadowFloor, gWaterRenderprog )
SetEntOrder( gShadowFloor, 1 )
SetEntVisible( gShadowFloor, False )

PositionEnt( gFloor, 0, -0.5, 0 )
PositionEnt( gShadowFloor, 0, -0.5, 0 )

/*
Global gShadowViewport = MakeViewport()
SetViewportWindow( gShadowViewport, gWindow )
SetViewportTransparent( gShadowViewport, True, 3 )
SetViewportAutoResize( gShadowViewport, True )
SetViewportCamera( gShadowViewport, gShadowCam )
*/

Global gCube = MakeCube()
PositionEnt( gCube, 0, 0.5, 0 )
//SetEntCastsShadows( gCube, True )
//SetLightCastsShadows( GetDefaultLight(), True )

PositionEnt( GetDefaultLight(), 2, 2, -2 )

//
//	NOTE: The water ("shadow floor") is supposed to only show up in the area
//	-     that is shadowed
//

//SetEntVisible( gShadowFloor, False )

Global gCameraSpeed# = 0.1 //normal walking speed
Global gCameraSprint# = gCameraSpeed*1.7 //speed when sprinting

Global gCameraAccelTime# = 0.5 //number of seconds to accelerate
Global gCameraDecelTime# = 0.2 //number of seconds to decelerate

Global gCameraEnergy# = 1.0 //amount of energy left for sprinting
Global gCameraEnergyAccelRate# = 0.25
Global gCameraEnergyDecelRate# = 0.35

Global gCameraSprintStartTime //time when sprinting started
Global gCameraSprintStopTime //time when sprinting was last stopped
Global gCameraWasSprinting = False

Global gCameraOnGround = True
Global gCameraJumpVelocity# = 0.0
Global gCameraJumpVecX#
Global gCameraJumpVecZ#
Global gGravity# = 9.8

Global gCamAngX#
Global gCamAngY#

Global gFloorPosY# = GetEntPosY( gFloor )
Global gPlayerHeight# = 1.8

PositionEnt( gCamera, 0, gFloorPosY + gPlayerHeight, -5 )

Sync()

CenterMouse( gWindow )
HidePointer()

Global gBaseTime = Millisecs()
Global gTimer = MakeTimer()

While Not KeyHit( 1, gWindow )
	If Do3DTweening()
		Local curTime = Millisecs() - gBaseTime
		Local deltaTime# = Float( GetTimerMicrosecs( gTimer ) )/1000000.0

		Local cosAngY# = Cos( gCamAngY )
		Local sinAngY# = Sin( gCamAngY )

		Local camPosX# = GetEntPosX( gCamera )
		Local camPosY# = GetEntPosY( gCamera )
		Local camPosZ# = GetEntPosZ( gCamera )

		Local moveVecX# = 0.0
		Local moveVecY# = -gGravity
		Local moveVecZ# = 0.0

		Local speed# = gCameraSpeed

		// W -- forward
		If KeyDown( 17, gWindow )
			moveVecX = moveVecX + sinAngY
			moveVecZ = moveVecZ + cosAngY
		EndIf
		// S -- backward
		If KeyDown( 31, gWindow )
			moveVecX = moveVecX - sinAngY
			moveVecZ = moveVecZ - cosAngY
		EndIf

		// A -- strafe left
		If KeyDown( 30, gWindow )
			moveVecX = moveVecX - cosAngY
			moveVecZ = moveVecZ + sinAngY
		EndIf
		// D -- strafe right
		If KeyDown( 32, gWindow )
			moveVecX = moveVecX + cosAngY
			moveVecZ = moveVecZ - sinAngY
		EndIf

		If gCameraOnGround
			// are we sprinting
			If KeyDown( 42, gWindow ) Or KeyDown( 54, gWindow )
				// start sprinting
				If Not gCameraWasSprinting
					gCameraSprintStartTime = curTime
					gCameraWasSprinting = True
				EndIf

				// remove energy at that rate
				gCameraEnergy = Clamp( gCameraEnergy - gCameraEnergyAccelRate*deltaTime, 0, 1 )
			Else
				// stop sprinting
				If gCameraWasSprinting
					gCameraSprintStopTime = curTime
					gCameraWasSprinting = False
				EndIf

				// add energy at that rate
				gCameraEnergy = Clamp( gCameraEnergy + gCameraEnergyDecelRate*deltaTime, 0, 1 )
			EndIf

			If gCameraWasSprinting And gCameraEnergy > 0.0
				speed = gCameraSprint
			EndIf

			// normalize the movement vector
			Local mag# = moveVecX*moveVecX + moveVecZ*moveVecZ
			Local invMag# = 0
			If mag < -0.00001 Or mag > 0.00001 Then invMag# = 1.0/Sqr( mag )
			moveVecX = invMag*moveVecX
			moveVecZ = invMag*moveVecZ

			moveVecX = speed*moveVecX
			moveVecZ = speed*moveVecZ

			// Space -- jump
			If KeyHit( 57, gWindow ) Or KeyDown( 57, gWindow )
				gCameraOnGround = False
				gCameraJumpVelocity = gGravity + 5.0

				gCameraJumpVecX = moveVecX
				gCameraJumpVecZ = moveVecZ
			EndIf
		Else
			/*
			Local jumpSpeed# = Sqr( gCameraJumpVecX*gCameraJumpVecX + gCameraJumpVecZ*gCameraJumpVecZ )
			Local newVecX# = gCameraJumpVecX + moveVecX
			Local newVecZ# = gCameraJumpVecZ + moveVecZ
			Local newSpeed# = Sqr( newVecX*newVecX + newVecZ*newVecZ )
			Local rcpSpeed# = 1.0/newSpeed
			Local scale# = rcpSpeed*newSpeed

			moveVecX = newVecX*scale
			moveVecZ = newVecZ*scale
			*/
			moveVecX = gCameraJumpVecX
			moveVecZ = gCameraJumpVecZ
			KeyHit( 57, gWindow ) //reset the counter
		EndIf

		moveVecY = moveVecY + gCameraJumpVelocity

		gCameraJumpVelocity = gCameraJumpVelocity - deltaTime*gGravity
		If gCameraJumpVelocity < 0.00001
			gCameraJumpVelocity = 0
		EndIf

		Print "moveVecY = " + moveVecY
		Print "jumpVelY = " + gCameraJumpVelocity
		Print "onGround = " + gCameraOnGround
		Print "gravity = " + gGravity

		// add the movement vector to the camera
		camPosX = camPosX + moveVecX
		camPosY = camPosY + deltaTime*moveVecY
		camPosZ = camPosZ + moveVecZ

		// collision
		Local radius# = 0.6
		Local boundL# = -10.0 + radius*2
		Local boundH# = 10.0 - radius*2
		camPosX = Clamp( camPosX, boundL, boundH )
		camPosZ = Clamp( camPosZ, boundL, boundH )

		If camPosY < gFloorPosY + gPlayerHeight
			camPosY = gFloorPosY + gPlayerHeight
			gCameraOnGround = True
		EndIf

		PositionEnt( gCamera, camPosX, camPosY, camPosZ )

		Local spdX# = MouseSpeedX()
		Local spdY# = MouseSpeedY()
		CenterMouse( gWindow )

		gCamAngX = gCamAngX + spdY*0.8
		gCamAngY = Wrap360( gCamAngY + spdX*0.8 )

		If gCamAngX < -90 Then gCamAngX = -90
		If gCamAngX >  90 Then gCamAngX =  90

		RotateEnt( gCamera, gCamAngX, gCamAngY, 0 )

		If KeyHit( 28, gWindow )
			SetEntVisible( gShadowFloor, 1 - GetEntVisible( gShadowFloor ) )
			SetEntVisible( gFloor, 1 - GetEntVisible( gShadowFloor ) )
		EndIf

		//PositionEnt( gShadowCam, camPosX, camPosY, camPosZ )
		//RotateEnt( gShadowCam, gCamAngX, gCamAngY, 0 )

		ResetTimer( gTimer )
	EndIf

	If Not Sync() Then Break
Wend

Function CenterMouse( wnd )
	SetMousePos GetWindowWidth( wnd, True )/2, GetWindowHeight( wnd, True )/2, wnd
	MouseSpeedX() : MouseSpeedY()
EndFunction
Function Wrap360( x# )
	If x < 0 Or x >= 360.0
		x = x - Floor( x/360 )*360
	EndIf

	Return x
EndFunction
Function Wrap180( x# )
	x = Wrap360( x )

	If x > 180.0 Then Return x - 360.0
	Return x
EndFunction

Function Clamp#( x#, l#, h# )
	If x < l Then Return l
	If x > h Then Return h

	Return x
EndFunction
