bits 32
;jmp $							;EB FE
;nop							;90
;call 0xFFFFFFFF				;E8 FA FF FF FF FF
;call eax						;FF D0
;push dword [0xFFFFFFFF]		;FF 35 FF FF FF FF
;push dword 0x7FFFFFFF			;68 FF FF FF 7F
;mov eax, 0xFFFFFFFF			;B8 FF FF FF FF
;push eax						;50
;add esp, 4						;83 C4 04
;pop eax						;58
;xor eax, eax					;31 C0
;jmp short L0					;EB <XX> -- bytes past end of instr.
	; e.g., if L0 were right after the jmp, XX would be 00.
	;       if L0 were one byte after jmp, XX would be 01.
