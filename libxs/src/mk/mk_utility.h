#ifndef MK_UTILITY_H
#define MK_UTILITY_H

#include "mk_types.h"

const char *va(const char *format, ...);
const char *getdir(char *buf, size_t n, const char *filename);
void extsubst(char *dst, size_t dstn, const char *src, const char *ext);
int shell(const char *format, ...);
int matchpath(const char *rpath, const char *apath);
int getintegraldate();
const char *getext(const char *filename);
int isextcpp(const char *ext);
int isextc(const char *ext);

#endif
