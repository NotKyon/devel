#ifndef BUFFER_H
#define BUFFER_H

#include "local.h"

typedef struct buffer_s {
	char *filename;
	size_t numlines;
	size_t *lines; /* indices corresponding to each line */
	size_t length;
	size_t index;
	char *source;
	char *func;

	struct buffer_s *prev, *next;
} buffer_t;

buffer_t *NewBuffer(const char *filename, const char *source);

buffer_t *LoadBuffer(const char *filename);

buffer_t *DeleteBuffer(buffer_t *buf);
void DeleteAllBuffers();

const char *BufferFilename(buffer_t *buf);
const char *BufferSource(buffer_t *buf);
size_t BufferLine(buffer_t *buf);
size_t BufferIndex(buffer_t *buf);
size_t BufferLength(buffer_t *buf);

void SetBufferFunction(buffer_t *buf, const char *func);
const char *BufferFunction(buffer_t *buf);

char PeekBuffer(buffer_t *buf);
char PullBuffer(buffer_t *buf);
char EatBuffer(buffer_t *buf);
char UnpullBuffer(buffer_t *buf);
size_t SetBufferIndex(buffer_t *buf, size_t index);
bool_t BufferEnd(buffer_t *buf);

void SkipBufferWhitespace(buffer_t *buf);
void SkipBufferLine(buffer_t *buf);
bool_t ReadBufferLine(buffer_t *buf, char *line, size_t n);

buffer_t *FirstBuffer();
buffer_t *LastBuffer();
buffer_t *BufferBefore(buffer_t *buf);
buffer_t *BufferAfter(buffer_t *buf);

void ReportBufferV(buffer_t *buf, const char *type, const char *format,
va_list args);
void ReportBuffer(buffer_t *buf, const char *type, const char *format, ...);

void ErrorBuffer(buffer_t *buf, const char *format, ...);
void WarnBuffer(buffer_t *buf, const char *format, ...);
void NoteBuffer(buffer_t *buf, const char *format, ...);

#endif
