float Script : StandardsGlobal <
	string ScriptClass = "scene";
	string ScriptOrder = "postprocess";
	string ScriptOutput = "color";
> = 0.8;

struct VERTEX {
	float3 PosL		: POSITION;
};
struct FRAGMENT {
	float4 PosH		: POSITION;
};

//float3 gWireColor	: WireframeColor; //maybe one day NF will automate?
float3 gWireColor	: CUSTOM3;
float4x4 gWVP		: WorldViewProjection;

FRAGMENT VS(in VERTEX I) {
	FRAGMENT O;

	O.PosH = mul(float4(I.PosL, 1.0f), gWVP);

	return O;
}
float4 PS(in FRAGMENT I): COLOR {
	return float4(gWireColor, 1.0f);
}

technique Wire {
	pass P0 <
		string Script="Draw=Geometry;";
	> {
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PS();
		FillMode = Wireframe;
	}
}
