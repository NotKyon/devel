#ifndef AXTEK_THREADING_CSEMAPHORE_H
#define AXTEK_THREADING_CSEMAPHORE_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

__AX_TASK_NS_INIT__

class CSemaphore {
protected:
#if _WIN32
	HANDLE m_sem;
#else
	sem_t m_sem;
#endif

public:
	inline CSemaphore(u32_t initCount=0) {
#if _WIN32
		m_sem = CreateSemaphoreA(NULL, initCount, 0x7FFFFFFF, NULL);
#else
		sem_init(&m_sem, 0, initCount);
#endif
	}
	inline ~CSemaphore() {
#if _WIN32
		CloseHandle(m_sem);
#else
		sem_destroy(&m_sem);
#endif
	}

	inline void Sync() {
#if _WIN32
		WaitForSingleObject(m_sem, INFINITE);
#else
		sem_wait(&m_sem);
#endif
	}
	inline bool Wait(u32_t milliseconds) {
#if _WIN32
		if (WaitForSingleObject(m_sem, milliseconds)==WAIT_OBJECT_0)
			return true;

		return false;
#else
		/* TODO */
		return false;
#endif
	}
	inline bool TryWait() {
#if _WIN32
		return Wait(0);
#else
		return !sem_trywait(&m_sem);
#endif
	}
	inline void Signal() {
#if _WIN32
		ReleaseSemaphore(m_sem, 1, NULL);
#else
		sem_post(&m_sem);
#endif
	}
	inline u32_t Read() {
#if _WIN32
		LONG cnt;

		if (!ReleaseSemaphore(m_sem, 0, &cnt))
			cnt = 0;

		return (u32_t)cnt;
#else
		int cnt;

		if (sem_getvalue(&m_sem)==-1)
			cnt = 0;

		return (u32_t)cnt;
#endif
	}
};

__AX_TASK_NS_FINI__

#endif
