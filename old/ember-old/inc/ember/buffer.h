#ifndef EMBER_BUFFER_H
#define EMBER_BUFFER_H

#include "config.h"
#include "gc.h"

/*
 * Buffers are used for storing a block of text for lexical analysis and
 * parsing. This structure stores the name of the file (for use with error
 * reporting), the current line (for use with functions that retrieve characters
 * from the buffer), as well as callback routines (for error reporting).
 */
#ifndef __buffer_t_defined__
#define __buffer_t_defined__ 1
typedef struct stBuffer *buffer_t;
#endif
#define BUF_ERRLVL_ERROR 0
#define BUF_ERRLVL_WARN1 1
#define BUF_ERRLVL_WARN2 2
#define BUF_ERRLVL_WARN3 3
#define BUF_ERRLVL_WARN4 4
typedef void ( *buffer_callback_t )( buffer_t, int /*level*/, const char * /*msg*/ );
struct stBuffer {
	char *name;
	unsigned int line;

	const char *source;
	const char *ptr;

	buffer_callback_t cb;
};
__em_extrn gc_pool_t g_buf_pool __em_nil();

extern int buf_init();
extern void buf_deinit();
extern buffer_t buf_alloc( const char *name, char *source, buffer_callback_t cb );
extern void buf_dealloc( buffer_t buf );
extern const char *buf_name( buffer_t buf );
extern unsigned int buf_line( buffer_t buf );
extern char buf_read( buffer_t buf );
extern char buf_look( buffer_t buf );
extern char buf_peek( buffer_t buf );
extern void buf_unget( buffer_t buf );
extern void buf_reset( buffer_t buf );
extern void buf_report( buffer_t buf, int level, const char *format, ... );
extern buffer_t buf_load( const char *filename, buffer_callback_t cb );

#endif
