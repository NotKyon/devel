#include <stdio.h>
#include <stdlib.h>

#define PRINTV( x ) printf( "%s = %i\n", #x, x )

int main()
{
#define FOUND 0
#if defined( _MSC_VER )
# undef FOUND
# define FOUND 1
	PRINTV( _MSC_VER );
	PRINTV( _MSC_FULL_VER );
#endif
#if defined( __clang__ )
# undef FOUND
# define FOUND 1
	PRINTV( __clang__ );
	PRINTV( __clang_major__ );
	PRINTV( __clang_minor__ );
	PRINTV( __clang_patchlevel__ );
#endif
#if defined( __GNUC__ )
# undef FOUND
# define FOUND 1
	PRINTV( __GNUC__ );
	PRINTV( __GNUC_MINOR__ );
	PRINTV( __GNUC_PATCHLEVEL__ );
#endif
#if defined( __INTEL_COMPILER )
# undef FOUND
# define FOUND 1
	PRINTV( __INTEL_COMPILER );
#endif

#if !FOUND
	printf( "ERROR: Unknown compiler\n" );
#endif

	return EXIT_SUCCESS;
}
