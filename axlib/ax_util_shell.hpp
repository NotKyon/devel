#pragma once

#include "axlib.hpp"
#include "ax_dictionary.hpp"

#include <errno.h>

#ifdef AX_IMPLEMENTATION
# undef AXSHELL_IMPLEMENTATION
# define AXSHELL_IMPLEMENTATION
#endif

#if AXSHELL_IMPLEMENTATION
# define AXSHELL_IMPLEMENT 1
#else
# define AXSHELL_IMPLEMENT 0
#endif

namespace Ax { namespace Util {

	enum EStandardStream
	{
		kStandardOutput,
		kStandardError
	};

	// Built-in command for execution by the shell
	class IShellCommand
	{
	public:
		IShellCommand() {}
		virtual ~IShellCommand() {}

		// run the command -- return EXIT_SUCCESS on success or errno on failure
		virtual int run( const TArr<Str> &args ) = 0;
	};

	// Output filter (for processing text from a shell command)
	class IOutputFilter
	{
	public:
		IOutputFilter() {}
		virtual ~IOutputFilter() {}

		// Ask whether this filter wants to process output from the given command
		//
		// The first element in the args array will be the name of the process
		//
		// Return true if this filter can be used to process output for the
		// given process.
		virtual bool canProcess( const TArr< Str > &args ) const = 0;

		// Invoked before any output from the given stream is processed
		virtual void enter( EStandardStream stream ) {}
		// Invoked after all output from the given stream has been processed
		virtual void leave( EStandardStream stream ) {}
		// Invoked to process output for the given stream
		virtual void write( EStandardStream stream, const Str &text ) = 0;
	};

	class MShell
	{
	public:
		static MShell &get();

		int run( const TArr< Str > &args );

		int run( const Str &command );
		int runfv( const char *pszFormat, va_list args );
		int runf( const char *pszFormat, ... );

		// Convert the given argument array into a version CommandLineToArgvW()
		// will process properly (except in UTF-8 instead of UTF-16).
		//
		// How Windows parses command arguments:
		// https://msdn.microsoft.com/en-us/library/windows/desktop/17w5ykft%28v=vs.85%29.aspx
		//
		// `inoutString` will be appended to, not assigned to. If it already has
		// data in it, that data will not be erased.
		//
		// Returns true upon success.
		bool getWindowsSafeCommandLine( MutStr &inoutString, const TArr< Str > &args ) const;

	private:
		typedef TDictionary<IShellCommand>::SEntry CmdEntryType;

		bool                        m_bLoggingEnabled;
		TMutStr<IOutputFilter *>    m_filters;
		TMutStr<IShellCommand *>    m_commands;
		TDictionary<IShellCommand>  m_commandMap;

		MShell();
		~MShell();

		void logCommand( const TArr< Str > &args, const Str &prefix ) const;
		void logCommand( const Str &commandLine, const Str &prefix ) const;

		AX_DELETE_COPYFUNCS(MShell);
	};

	static Ax::TManager<MShell> g_shell;

#if AXSHELL_IMPLEMENT || 1

	MShell &MShell::GetInstance()
	{
		static MShell instance;
		return instance;
	}

	MShell::MShell()
	: m_bLoggingEnabled( !!AX_DEBUG_ENABLED )
	, m_filters()
	, m_commands()
	, m_commandMap()
	{
		AX_EXPECT_MSG( m_commandMap.Init( TENSHI_SHELLCOMMAND_ALLOWED, Ax::ECase::Insensitive ), "Out of memory" );

		if( GetConsoleOutputCP() != CP_UTF8 ) {
			SetConsoleOutputCP( CP_UTF8 );
		}
	}
	MShell::~MShell()
	{
	}

	bool MShell::getWindowsSafeCommandLine( MutStr &inoutString, const TArr< Str > &args ) const
	{
		// Save some time by requesting most of our needed memory up-front
		//
		// If this fails it's fine as it's only meant as a performance
		// optimization. (Keep us from re-allocating all the time.)
		inoutString.reserve( inoutString.len() + args.num()*128 );

		// Process each argument
		for( UPtr i = 0; i < args.num(); ++i ) {
			// Reference to the current argument in the array
			const Ax::String &arg = args[ i ];

			// If this is not the first argument to the string then add a space
			if( !inoutString.isEmpty() ) {
				if( !inoutString.Append( ' ' ) ) {
					return false;
				}
			}

			// Determine whether surrounding quotes are needed
			const bool bNeedQuotes = ( arg.find( ' ' ) >= 0 || arg.find( '\t' ) >= 0 || arg.isEmpty() );

			// Add an opening quote if quotes are necessary
			if( bNeedQuotes && !inoutString.tryAppend( '\"' ) ) {
				return false;
			}

			// Current parsing pointer for the argument
			const char *p = arg.CString();
			// Base pointer (for adding a series of characters all at once)
			const char *b = p;

			// Parse this argument
			for(;;) {
				// Backslashes might need special handling
				if( *p == '\\' ) {
					// Flush the current range of text
					if( !inoutString.tryAppend( b, p ) ) {
						return false;
					}

					// Number of backslashes
					size_t n = 0;
					// Starting location of the first backslash
					const char *const t = p;

					// Count the backslashes
					while( *p == '\\' ) {
						++n;
						++p;
					}

					// Backslashes followed by a quotation mark is handled specially
					if( *p == '\"' ) {
						// Eat the quote
						++p;

						// Add a pair of backslashes for each of the counted
						// backslashes. (Windows will treat each two as one
						// because they are followed by a double quote.)
						while( n > 0 ) {
							--n;
							if( !inoutString.tryAppend( "\\\\" ) ) {
								return false;
							}
						}

						// Escape the quote
						if( !inoutString.tryAppend( "\\\"" ) ) {
							return false;
						}
					} else {
						// Turns out special handling wasn't necessary, so just
						// copy the backslashes verbatim
						if( !inoutString.tryAppend( t, p ) ) {
							return false;
						}
					}

					// Reset the base pointer and resume the loop
					b = p;
					continue;
				}

				// Quotation marks need to be escaped
				if( *p == '\"' ) {
					// Flush the current range of text
					if( !inoutString.tryAppend( b, p ) ) {
						return false;
					}
					// Escape the quote
					if( !inoutString.tryAppend( "\\\"" ) ) {
						return false;
					}

					// Set the base and pointer to one after the quote
					b = ++p;
					continue;
				}

				// If the end of the string has been reached then exit the loop
				if( *p == '\0' ) {
					// Flush the current range of text before exiting
					if( !inoutString.tryAppend( b, p ) ) {
						return false;
					}

					break;
				}

				// This character was nothing special, so skip it (it will be
				// flushed to commandArgs later)
				++p;
			}

			// Add a closing quote if necessary
			if( bNeedQuotes && !inoutString.tryAppend( '\"' ) ) {
				return false;
			}
		}

		// Done!
		return true;
	}

	void MShell::logCommand( const TArr< Str > &args, const Str &prefix ) const
	{
		if( !m_bLoggingEnabled ) {
			return;
		}

		MutStr commandLine;
		UPtr cReserveBytes = 0;
		for( UPtr i = 0; i < args.num(); ++i ) {
			cReserveBytes += args[ i ].len() + 8;
		}

		AX_EXPECT_MEMORY( commandLine.reserve( cReserveBytes ) );
		for( UPtr i = 0; i < args.num(); ++i ) {
			const Str &arg = args[ i ];

			if( commandLine.isEmpty() ) {
				AX_EXPECT_MEMORY( commandLine.append( ' ' ) );
			}

			if( arg.find( ' ' ) >= 0 || arg.find( '\t' ) >= 0 || arg.find( '\"' ) >= 0 || arg.find( '\\' ) >= 0 ) {
				AX_EXPECT_MEMORY( commandLine.append( arg.escaped().quoted() ) );
			} else {
				AX_EXPECT_MEMORY( commandLine.append( arg ) );
			}
		}

		logCommand( commandLine, prefix );
	}
	void MShell::logCommand( const Str &commandLine, const Str &prefix ) const
	{
		if( !m_bLoggingEnabled ) {
			return;
		}

		static const Str defaultPrefix( ">" );

		const Str &selPrefix = prefix.isEmpty() ? defaultPrefix : prefix;

		const int         n1 = selPrefix.intLen();
		const char *const p1 = selPrefix.get();

		const int         n2 = commandLine.intLen();
		const char *const p2 = commandLine.get();

		basicStatusf( "%.*s%.*s", n1,p1, n2,p2 );
	}

	int MShell::run( const TArr< Str > &args )
	{
		if( args.isEmpty() || args[ 0 ].isEmpty() ) {
			return EINVAL;
		}

		CmdEntryType *const pEntry = m_commandMap.find( args[ 0 ] );
		if( pEntry != NULL && pEntry->pData != NULL ) {
			logCommand( args, "$builtin>" );
			return pEntry->pData->run( args );
		}

#ifdef _WIN32
		MutStr appName;
		MutStr commandArgs;
		TMutArr< U16 > appNameW;
		TMutArr< U16 > commandArgsW;

		if( !getWindowsSafeCommandLine( commandArgs, args ) ) {
			Ax::Errorf( args[ 0 ], "Ran out of memory in MShell::getWindowsSafeCommandLine()" );
			return ENOMEM;
		}

		if( args[ 0 ].caseEndsWith( ".bat" ) || args[ 0 ].caseEndsWith( ".cmd" ) ) {
			if( !appName.Assign( "cmd.exe" ) ) {
				Ax::Errorf( args[ 0 ], "Ran out of memory while selecting command prompt to execute file" );
				return ENOMEM;
			}

			MutStr shuffle;
			TMutArr<MutStr> shuffleArgs;

			bool bWin = true;
			bWin = bWin && shuffleArgs.append();
			bWin = bWin && shuffleArgs.append( "/c" );
			bWin = bWin && shuffleArgs.append( args[ 0 ] );
			bWin = bWin && shuffleArgs.num() == 3 && shuffleArgs[ 1 ].len() == 2;
			bWin = bWin && shuffleArgs.num() == 3 && shuffleArgs[ 2 ].len() == args[ 0 ].len();

			if( !bWin ) {
				Ax::Errorf( args[ 0 ], "Ran out of memory while reconfiguring command-line for Windows (cmd.exe)" );
				return ENOMEM;
			}

			if( !getWindowsSafeCommandLine( shuffle, shuffleArgs ) ) {
				Ax::Errorf( args[ 0 ], "Ran out of memory while preparing safe command-line for Windows (cmd.exe)" );
				return ENOMEM;
			}

			if( !shuffle.tryAppend( ' ' ) ) {
				Ax::Errorf( args[ 0 ], "Ran out of memory while fixing safe command-line for Windows (cmd.exe)" );
				return ENOMEM;
			}

			if( !commandArgs.prepend( shuffle ) ) {
				Ax::Errorf( args[ 0 ], "Ran out of memory while inserting batch filename into command-line for Windows (cmd.exe)" );
				return ENOMEM;
			}
		} else if( !args[ 0 ].caseEndsWith( ".exe" ) && !args[ 0 ].caseEndsWith( ".com" ) ) {
			if( !appName.Assign( args[ 0 ] ) ) {
				Ax::Errorf( args[ 0 ], "Ran out of memory while preparing process name for Windows" );
				return ENOMEM;
			}

			if( !appName.Append( ".exe" ) ) {
				Ax::Errorf( args[ 0 ], "Ran out of memory while appending '.exe' to process name for Windows" );
				return ENOMEM;
			}
		}

		logCommand( commandArgs, ">" );

		bool bAppNameW = false;
		if( appName.isEmpty() ) {
			bAppNameW = args[ 0 ].ToUTF16( appNameW );
		} else {
			bAppNameW = appName.ToUTF16( appNameW );
		}
		if( !bAppNameW ) {
			Ax::Errorf( args[ 0 ], "Ran out of memory while converting UTF-8 process name to UTF-16 for Windows" );
			return ENOMEM;
		}

		if( !commandArgs.ToUTF16( commandArgsW ) ) {
			Ax::Errorf( args[ 0 ], "Ran out of memory while converting UTF-8 command line to UTF-16 for Windows" );
			return ENOMEM;
		}

		static const UPtr kMaxBytes = 4096;

		Ax::String Text;
		if( !Text.Reserve( kMaxBytes ) ) {
			Ax::Errorf( args[ 0 ], "Failed to allocate enough space for reading output" );
			return ENOMEM;
		}

		STARTUPINFOW startInfo;

		startInfo.cb = sizeof( startInfo );
		startInfo.lpReserved = NULL;
		startInfo.lpDesktop = NULL;
		startInfo.lpTitle = NULL;
		startInfo.dwX = 0;
		startInfo.dwY = 0;
		startInfo.dwXSize = 0;
		startInfo.dwYSize = 0;
		startInfo.dwXCountChars = 0;
		startInfo.dwYCountChars = 0;
		startInfo.dwFillAttribute = 0;
		startInfo.dwFlags = 0;
		startInfo.wShowWindow = 0;
		startInfo.cbReserved2 = 0;
		startInfo.lpReserved2 = NULL;
		startInfo.hStdInput = GetStdHandle( STD_INPUT_HANDLE );
		startInfo.hStdOutput = GetStdHandle( STD_OUTPUT_HANDLE );
		startInfo.hStdError = GetStdHandle( STD_ERROR_HANDLE );

		HANDLE hOutRd = NULL, hOutWr = NULL;
		HANDLE hErrRd = NULL, hErrWr = NULL;

		AX_SCOPE_GUARD({CloseHandle(hOutRd);CloseHandle(hOutWr);CloseHandle(hErrRd);CloseHandle(hErrWr);});

		IOutputFilter *pFilter = NULL;
		for( UPtr j = m_filters.num(); j > 0; --j ) {
			const UPtr i = j - 1;
			IOutputFilter *const pCheck = m_filters[ i ];
			if( !pCheck || !pCheck->CanProcess( args ) ) {
				continue;
			}

			pFilter = pCheck;
			break;
		}

		if( pFilter != NULL && !CreatePipe( &hOutRd, &hOutWr, NULL, 0 ) ) {
			pFilter = NULL;
		}

		if( pFilter != NULL && !CreatePipe( &hErrRd, &hErrWr, NULL, 0 ) ) {
			pFilter = NULL;
		}

		if( pFilter != NULL ) {
			startInfo.dwFlags |= STARTF_USESTDHANDLES;

			startInfo.hStdOutput = hOutWr;
			startInfo.hStdError = hErrWr;
		}

		PROCESS_INFORMATION procInfo;
		if( !CreateProcessW
		(
			( const wchar_t * )appNameW.Pointer(),
			( wchar_t * )commandArgsW.Pointer(),
			NULL,   // lpProcessAttributes
			NULL,   // lpThreadAttributes
			FALSE,  // bInheritHandles
			0,      // dwCreationFlags
			NULL,   // lpEnvironment
			NULL,   // lpCurrentDirectory
			&startInfo,
			&procInfo
		) ) {
			Ax::Errorf( args[ 0 ], "Could not find or execute process" );
			return EEXIST;
		}

		if( pFilter != NULL ) {
			DWORD dwResult;
			bool bDidEnterOut = false;
			bool bDidEnterErr = false;

			for(;;) {
				char Data[ kMaxBytes ];
				DWORD cReadBytes = 0;

				if( ReadFile( hOutRd, &Data[ 0 ], sizeof( Data ), &cReadBytes, NULL ) ) {
					AX_EXPECT( cReadBytes <= kMaxBytes );
					Text.Assign( &Data[ 0 ], &Data[ cReadBytes ] );

					if( !bDidEnterOut ) {
						bDidEnterOut = true;
						pFilter->Enter( EStandardStream::Output );
					}

					pFilter->Write( EStandardStream::Output, Text );
				}

				if( ReadFile( hErrRd, &Data[ 0 ], sizeof( Data ), &cReadBytes, NULL ) ) {
					AX_EXPECT( cReadBytes <= kMaxBytes );
					Text.Assign( &Data[ 0 ], &Data[ cReadBytes ] );

					if( !bDidEnterErr ) {
						bDidEnterErr = true;
						pFilter->Enter( EStandardStream::Error );
					}

					pFilter->Write( EStandardStream::Error, Text );
				}

				dwResult = WaitForSingleObject( procInfo.hProcess, 250 );
				if( dwResult != WAIT_TIMEOUT ) {
					break;
				}
			}

			if( bDidEnterOut ) {
				pFilter->Leave( EStandardStream::Output );
			}
			if( bDidEnterErr ) {
				pFilter->Leave( EStandardStream::Error );
			}
		}

		WaitForSingleObject( procInfo.hProcess, INFINITE );

		DWORD dwExitCode = EXIT_FAILURE;
		( void )GetExitCodeProcess( procInfo.hProcess, &dwExitCode );

		CloseHandle( procInfo.hThread );
		CloseHandle( procInfo.hProcess );

		if( m_bLoggingEnabled ) {
			Ax::Statusf( args[ 0 ], "Process exited with code %i (0x%.8X)", ( int )dwExitCode, ( unsigned int )dwExitCode );
		}

		return ( int )dwExitCode;
#else
		logCommand( args, ">" );

# error This has not been implemented for your platform.
#endif
	}

	int MShell::run( const char *pszCommand )
	{
		AX_ASSERT_NOT_NULL( pszCommand );
		if( !pszCommand ) {
			return EINVAL;
		}

		Ax::String command;

		if( !command.Assign( pszCommand ) ) {
			Ax::BasicErrorf( "Ran out of memory while preparing to execute: %s", pszCommand );
			return ENOMEM;
		}

		return run( command );
	}
	int MShell::run( const Ax::String &command )
	{
		Ax::TArray< Ax::String > args;
		args = command.SplitUnquoted( " ", '\\', Ax::EKeepQuotes::No );
		return run( args );
	}
	int MShell::runfv( const char *pszFormat, va_list args )
	{
		Ax::String command;

		AX_ASSERT_NOT_NULL( pszFormat );
		if( !pszFormat ) {
			return EINVAL;
		}

		if( !command.FormatV( pszFormat, args ) ) {
			Ax::BasicErrorf( "Ran out of memory while preparing to execute formatted command: %s", pszFormat );
			return ENOMEM;
		}

		return run( command );
	}
	int MShell::runf( const char *pszFormat, ... )
	{
		Ax::String command;
		va_list args;

		AX_ASSERT_NOT_NULL( pszFormat );
		if( !pszFormat ) {
			return EINVAL;
		}

		va_start( args, pszFormat );
		const int iResult = Runfv( pszFormat, args );
		va_end( args );

		return run( command );
	}
#endif

}}
