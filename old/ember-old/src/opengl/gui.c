#include <ember/gui.h>

/*
 * TODO
 * A lot of these routines need to be fixed up.
 */

static struct stResponderGL g_gui_def_responder;
static int g_gui_workspace_width = 0, g_gui_workspace_height = 0;

static void __on_init( struct stWindowGL *self, void *p )
{
	if( self || p ) {
	}
}

static void __on_deinit( struct stWindowGL *self )
{
	if( self ) {
	}
}

static void __on_close( struct stWindowGL *self )
{
	gui_destroy( self );
}

static void __on_move( struct stWindowGL *self, int x, int y )
{
	if( self || x || y ) {
	}
}
static void __on_size( struct stWindowGL *self, int x, int y )
{
	if( self || x || y ) {
	}
}

static void __pre_draw( struct stWindowGL *self )
{
	if( self ) {
	}
}
static void __on_draw( struct stWindowGL *self )
{
	if( self ) {
	}
}
static void __post_draw( struct stWindowGL *self )
{
	if( self ) {
	}
}

static void __on_mouse_down( struct stWindowGL *self, EGUIMouseButton b, int x,
    int y )
{
	if( self || b || x || y ) {
	}
}
static void __on_mouse_up( struct stWindowGL *self, EGUIMouseButton b, int x,
    int y )
{
	if( self || b || x || y ) {
	}
}
static void __on_mouse_move( struct stWindowGL *self, int delta_x, int delta_y )
{
	if( self || delta_x || delta_y ) {
	}
}
static void __on_mouse_wheel( struct stWindowGL *self, int delta )
{
	if( self || delta ) {
	}
}

static void __on_key_char( struct stWindowGL *self, char c )
{
	if( self || c ) {
	}
}
static void __on_key_down( struct stWindowGL *self, int k )
{
	if( self || k ) {
	}
}
static void __on_key_up( struct stWindowGL *self, int k )
{
	if( self || k ) {
	}
}

static int __gui_w_alloc( struct stWindowGL *self )
{
	self->title       = ( char * )0;
	self->placeholder = ( char * )0;

	self->style = 0;
	self->level = EGUIWindowLevel_Normal;

	self->normal_rect.x  = 0;
	self->normal_rect.y  = 0;
	self->normal_rect.w  = 0;
	self->normal_rect.h  = 0;
	self->restore_rect.x = 0;
	self->restore_rect.y = 0;
	self->restore_rect.w = 0;
	self->restore_rect.h = 0;

	self->min_extent.w = 100;
	self->min_extent.h = 25;
	self->max_extent.w = 0;
	self->max_extent.h = 0;

	self->caption_height = 20;

	self->bg_color.r = 0.0f; /* cool black */
	self->bg_color.g = 0.18f;
	self->bg_color.b = 0.39f;
	self->bg_color.a = 0.75f;

	self->responder = ( struct stResponderGL * )0;

	self->w_prnt = ( struct stWindowGL * )0;
	self->w_head = ( struct stWindowGL * )0;
	self->w_tail = ( struct stWindowGL * )0;
	self->w_prev = ( struct stWindowGL * )0;
	self->w_next = ( struct stWindowGL * )0;

	self->l_prev = ( struct stWindowGL * )0;
	self->l_next = ( struct stWindowGL * )0;

	return 1;
}
static void __gui_w_dealloc( struct stWindowGL *self )
{
	if( self->title )
		free( ( void * )self->title );

	if( self->placeholder )
		free( ( void * )self->placeholder );

	if( self->tip )
		free( ( void * )self->tip );

	if( self->l_prev )
		self->l_prev->l_next = self->l_next;
	if( self->l_next )
		self->l_next->l_prev = self->l_prev;

	if( g_gui_l_head[ self->level ] == self )
		g_gui_l_head[ self->level ] = self->l_next;
	if( g_gui_l_tail[ self->level ] == self )
		g_gui_l_tail[ self->level ] = self->l_prev;

	if( self->w_prnt ) {
		if( self->w_prev )
			self->w_prev->w_next = self->w_next;
		if( self->w_next )
			self->w_next->w_prev = self->w_prev;

		if( self->w_prnt->w_head == self )
			self->w_prnt->w_head = self->w_next;
		if( self->w_prnt->w_tail == self )
			self->w_prnt->w_tail = self->w_prev;
	}
}

static void __gui_on_size( struct stWindowGL *self )
{
	int w, h;

	w = gui_client_width( self );
	h = gui_client_height( self );

	if( self->responder )
		self->responder->on_size( self, w, h );
	else
		g_gui_def_responder.on_size( self, w, h );
}

int gui_init()
{
	unsigned int i;

	g_gui_def_responder.on_init        = __on_init;
	g_gui_def_responder.on_deinit      = __on_deinit;
	g_gui_def_responder.on_close       = __on_close;
	g_gui_def_responder.on_move        = __on_move;
	g_gui_def_responder.on_size        = __on_size;
	g_gui_def_responder.pre_draw       = __pre_draw;
	g_gui_def_responder.on_draw        = __on_draw;
	g_gui_def_responder.post_draw      = __post_draw;
	g_gui_def_responder.on_mouse_down  = __on_mouse_down;
	g_gui_def_responder.on_mouse_up    = __on_mouse_up;
	g_gui_def_responder.on_mouse_move  = __on_mouse_move;
	g_gui_def_responder.on_mouse_wheel = __on_mouse_wheel;
	g_gui_def_responder.on_key_char    = __on_key_char;
	g_gui_def_responder.on_key_down    = __on_key_down;
	g_gui_def_responder.on_key_up      = __on_key_up;

	for( i = 0; i < EGUINumWindowLevels; i++ )
		g_gui_l_head[ i ] = g_gui_l_tail[ i ] = ( struct stWindowGL * )0;

	g_gui_w_pool =
	    gc_register_pool( sizeof( struct stWindowGL ), ( gc_init_t )__gui_w_alloc,
	        ( gc_deinit_t )__gui_w_dealloc );
	if( !g_gui_w_pool )
		return 0;
	return 1;
}
void gui_deinit()
{
	gc_unregister_pool( g_gui_w_pool );
}

void gui_resize_workspace( int w, int h )
{
	unsigned int i;
	window_t p;

	for( i = 0; i < EGUINumWindowLevels; i++ ) {
		for( p = g_gui_l_head[ i ]; p; p = p->l_next ) {
			if( p->style & GUI_ZOOMED_BIT ) {
				if( p->responder )
					p->responder->on_size( p, w, h );
				else
					g_gui_def_responder.on_size( p, w, h );
			}
		}
	}
}
int gui_workspace_width()
{
	return g_gui_workspace_width;
}
int gui_workspace_height()
{
	return g_gui_workspace_height;
}

window_t gui_create( responder_t resp, const char *title, bitfield_t style,
    int x, int y, int w, int h )
{
	window_t self;

	if( !( self = ( window_t )gc_alloc( g_gui_w_pool ) ) )
		return ( window_t )0;

	self->style         = style;
	self->responder     = resp;
	self->title         = title ? strdup( title ) : ( char * )0;
	self->normal_rect.x = x;
	self->normal_rect.y = y;
	self->normal_rect.w = w;
	self->normal_rect.h = h;

	return self;
}
void gui_destroy( window_t self )
{
	gc_release( ( void * )self );
}
void gui_close( window_t self )
{
	if( self->responder )
		self->responder->on_close( self );
	else
		g_gui_def_responder.on_close( self );
}

void gui_set_title( window_t self, const char *title )
{
	if( self->title )
		free( ( void * )self->title );

	self->title = title ? strdup( title ) : ( char * )0;
}
void gui_set_placeholder( window_t self, const char *placeholder )
{
	if( self->placeholder )
		free( ( void * )self->placeholder );

	self->placeholder = placeholder ? strdup( placeholder ) : ( char * )0;
}
void gui_set_tip( window_t self, const char *tip )
{
	if( self->tip )
		free( ( void * )self->tip );

	self->tip = tip ? strdup( tip ) : ( char * )0;
}
void gui_set_style( window_t self, bitfield_t style )
{
	self->style = style;
}
void gui_set_level( window_t self, EGUIWindowLevel level )
{
	if( *( ( unsigned int * )&level ) >= EGUINumWindowLevels )
		return;
	if( self->w_prnt )
		return;

	if( self->l_prev )
		self->l_prev->l_next = self->l_next;
	if( self->l_next )
		self->l_next->l_prev = self->l_prev;

	if( g_gui_l_head[ self->level ] == self )
		g_gui_l_head[ self->level ] = self->l_next;
	if( g_gui_l_tail[ self->level ] == self )
		g_gui_l_tail[ self->level ] = self->l_prev;

	self->level = level;
	if( ( self->l_prev = g_gui_l_tail[ level ] ) != ( window_t )0 )
		g_gui_l_tail[ level ]->l_next = self;
	else
		g_gui_l_head[ level ] = self;
	g_gui_l_tail[ level ]     = self;
}
void gui_move_window( window_t self, int x, int y )
{
	if( self->style & ( GUI_ZOOMED_BIT | GUI_ICONIFIED_BIT ) )
		return;

	self->normal_rect.x = x;
	self->normal_rect.y = y;

	if( self->responder )
		self->responder->on_move( self, x, y );
	else
		g_gui_def_responder.on_move( self, x, y );
}
void gui_resize_window( window_t self, int w, int h )
{
	if( w < 0 || h < 0 )
		return;

	if( self->style & ( GUI_ZOOMED_BIT | GUI_ICONIFIED_BIT ) )
		return;

	if( w < self->min_extent.w )
		w = self->min_extent.w;
	if( h < self->min_extent.h )
		h = self->min_extent.h;

	if( self->max_extent.w && w > self->max_extent.w )
		w = self->max_extent.w;
	if( self->max_extent.h && h > self->max_extent.h )
		h = self->max_extent.h;

	self->normal_rect.w = w;
	self->normal_rect.h = h;
	__gui_on_size( self );
}
static int __gui_save_rect( window_t self )
{
	if( self->style & ( GUI_ICONIFIED_BIT | GUI_ZOOMED_BIT ) )
		return 0;

	self->restore_rect.x = self->normal_rect.x;
	self->restore_rect.y = self->normal_rect.y;
	self->restore_rect.w = self->normal_rect.w;
	self->restore_rect.h = self->normal_rect.h;

	return 1;
}
void gui_iconify( window_t self )
{
	if( !__gui_save_rect( self ) )
		return;

	self->style |= GUI_ICONIFIED_BIT;

	self->normal_rect.x = self->normal_rect.y = -1;
	self->normal_rect.w = self->normal_rect.h = 0;
}
void gui_zoom( window_t self )
{
	if( !__gui_save_rect( self ) )
		return;

	self->normal_rect.x = self->normal_rect.y = 0;
	self->normal_rect.w = gui_workspace_width();
	self->normal_rect.h = gui_workspace_height();

	__gui_on_size( self );
}
void gui_restore( window_t self )
{
	if( !( self->style & ( GUI_ICONIFIED_BIT | GUI_ZOOMED_BIT ) ) )
		return;

	self->normal_rect.x = self->restore_rect.x;
	self->normal_rect.y = self->restore_rect.y;
	self->normal_rect.w = self->restore_rect.w;
	self->normal_rect.h = self->restore_rect.h;

	__gui_on_size( self );
}
void gui_set_min_extent( window_t self, int min_w, int min_h )
{
	self->min_extent.w = min_w;
	self->min_extent.h = min_h;
}
void gui_set_max_extent( window_t self, int max_w, int max_h )
{
	self->max_extent.w = max_w;
	self->max_extent.h = max_h;
}
void gui_set_caption_height( window_t self, int caption_height )
{
	self->caption_height = caption_height;
}
void gui_set_background_color( window_t self, float r, float g, float b )
{
	self->bg_color.r = r;
	self->bg_color.g = g;
	self->bg_color.b = b;
}
void gui_set_transparency( window_t self, float a )
{
	self->bg_color.a = a;
}
responder_t gui_set_responder( window_t self, responder_t responder )
{
	responder_t old;

	old             = self->responder;
	self->responder = responder;

	return old;
}
int gui_is_descendant( window_t self, window_t prnt )
{
	if( !prnt )
		return 0;

	while( self ) {
		if( self->w_prnt == prnt )
			return 1;

		self = self->w_prnt;
	}

	return 0;
}
void gui_set_parent( window_t self, window_t prnt )
{
	if( self->w_prnt == prnt )
		return;

	if( gui_is_descendant( prnt, self ) )
		gui_set_parent( prnt, self->w_prnt );

	if( self->w_prev )
		self->w_prev->w_next = self->w_next;
	if( self->w_next )
		self->w_next->w_prev = self->w_prev;

	if( self->w_prnt ) {
		if( self->w_prnt->w_head == self )
			self->w_prnt->w_head = self->w_next;
		if( self->w_prnt->w_tail == self )
			self->w_prnt->w_tail = self->w_prev;
	}

	if( !prnt ) {
		if( self->w_prnt )
			gui_set_level( self, self->w_prnt->level );

		return;
	}

	if( !self->w_prnt ) {
		if( self->l_prev )
			self->l_prev->l_next = self->l_next;
		if( self->l_next )
			self->l_next->l_prev = self->l_prev;

		if( g_gui_l_head[ self->level ] == self )
			g_gui_l_head[ self->level ] = self->l_next;
		if( g_gui_l_tail[ self->level ] == self )
			g_gui_l_tail[ self->level ] = self->l_prev;
	}

	self->w_prnt = prnt;
	if( ( self->w_prev = prnt->w_tail ) != ( window_t )0 )
		prnt->w_tail->w_next = self;
	else
		prnt->w_head = self;
	prnt->w_tail     = self;
}

const char *gui_title( window_t self )
{
	return self->title;
}
const char *gui_placeholder( window_t self )
{
	return self->placeholder;
}
const char *gui_tip( window_t self )
{
	return self->tip;
}
bitfield_t gui_style( window_t self )
{
	return self->style;
}
EGUIWindowLevel gui_level( window_t self )
{
	return self->level;
}
int gui_position_x( window_t self )
{
	return self->normal_rect.x;
}
int gui_position_y( window_t self )
{
	return self->normal_rect.y;
}
int gui_width( window_t self )
{
	return self->normal_rect.w;
}
int gui_height( window_t self )
{
	return self->normal_rect.h;
}
int gui_restore_position_x( window_t self )
{
	return self->restore_rect.x;
}
int gui_restore_position_y( window_t self )
{
	return self->restore_rect.y;
}
int gui_restore_width( window_t self )
{
	return self->restore_rect.w;
}
int gui_restore_height( window_t self )
{
	return self->restore_rect.h;
}
int gui_min_width( window_t self )
{
	return self->min_extent.w;
}
int gui_min_height( window_t self )
{
	return self->min_extent.h;
}
int gui_max_width( window_t self )
{
	return self->max_extent.w;
}
int gui_max_height( window_t self )
{
	return self->max_extent.h;
}
int gui_caption_height( window_t self )
{
	return self->caption_height;
}
void gui_get_background_color( window_t self, float *out )
{
	out[ 0 ] = self->bg_color.r;
	out[ 1 ] = self->bg_color.g;
	out[ 2 ] = self->bg_color.b;
}
float gui_background_color_red( window_t self )
{
	return self->bg_color.r;
}
float gui_background_color_green( window_t self )
{
	return self->bg_color.g;
}
float gui_background_color_blue( window_t self )
{
	return self->bg_color.b;
}
float gui_transparency( window_t self )
{
	return self->bg_color.a;
}
responder_t gui_responder( window_t self )
{
	return self->responder;
}
window_t gui_super( window_t self )
{
	return self->w_prnt;
}
window_t gui_first_sub( window_t self )
{
	return self->w_head;
}
window_t gui_last_sub( window_t self )
{
	return self->w_tail;
}
window_t gui_sub_before( window_t self )
{
	return self->w_prev;
}
window_t gui_sub_after( window_t self )
{
	return self->w_next;
}
window_t gui_first( EGUIWindowLevel level )
{
	if( *( ( unsigned int * )&level ) >= EGUINumWindowLevels )
		return ( window_t )0;
	return g_gui_l_head[ level ];
}
window_t gui_last( EGUIWindowLevel level )
{
	if( *( ( unsigned int * )&level ) >= EGUINumWindowLevels )
		return ( window_t )0;
	return g_gui_l_tail[ level ];
}

int gui_client_width( window_t self )
{
	return self->style & GUI_ZOOMED_BIT ? gui_workspace_width() : self->normal_rect.w;
}
int gui_client_height( window_t self )
{
	return ( self->style & GUI_ZOOMED_BIT ? gui_workspace_height() : self->normal_rect.h ) -
	       self->caption_height;
}
