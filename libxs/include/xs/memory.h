#ifndef XS_MEMORY_H
#define XS_MEMORY_H

#include "basetypes.h"

#define XS_DEFAULT_ALIGNMENT (16)

/*
 * Allocate a chunk of aligned memory.
 *
 * >n (xs_size_t): Size of the memory block to allocate. This will automatically
 *                 be rounded up to the nearest alignment boundary.
 * >alignment (xs_size_t): The alignment boundary. This should be a power of
 *                         two. If 0, XS_DEFAULT_ALIGNMENT will be used.
 * <returns (void *): Handle to the memory, or NULL if the allocation failed.
 *                    errno will be set with the details in such an event.
 */
void *XsAlloc(xs_size_t n, xs_size_t alignment);

/*
 * Reallocate a chunk of aligned memory.
 *
 * >p (void *): Handle to the memory previously allocated with XsAlloc(), or
 *              reallocated with XsRealloc(). The new block will preserve the
 *              contents of this memory block up to 'min(n, old_size)' bytes,
 *              where 'old_size' is the current size of the block of memory. If
 *              this parameter is NULL, XsMalloc() will be invoked.
 * >n (xs_size_t): New size of the memory block. This will automatically be
 *                 rounded up to the nearest alignment boundary. If 0, a NULL
 *                 pointer will be returned.
 * >alignment (xs_size_t): The alignment boundary. This should be a power of
 *                         two. If 0, XS_DEFAULT_ALIGNMENT will be used. This
 *                         should match the alignment used to allocate the block
 *                         originally.
 * <returns (void *): New handle to the memory, or NULL if the reallocation
 *                    failed. If the allocation does fail, errno will be set.
 */
void *XsRealloc(void *p, xs_size_t n, xs_size_t alignment);

/*
 * Deallocate a chunk of aligned memory.
 *
 * >p (void *): Handle to the memory previously allocated with XsAlloc(), or
 *              reallocated with XsRealloc(). If NULL, this function just
 *              returns.
 * >alignment (xs_size_t): The alignment boundary. This should match the
 *                         alignment used to allocate the memory block. This
 *                         should match the alignment used to allocate the block
 *                         originally.
 */
void XsFree(void *p, xs_size_t alignment);

/*
 * Manage or allocate a chunk of memory. This function internally uses
 * XsAlloc(), XsRealloc() and XsFree(), and is therefore interchangeable with
 * them. XS_DEFAULT_ALIGNMENT is used.
 *
 * >p (void *): Handle to the memory. If NULL the operation is an allocation if
 *              'n' > 0.
 * >n (xs_size_t): Size of the memory block you wish to allocate. See XsAlloc().
 *                 If 'p' != NULL and 'n' > 0 then the operation performed is a
 *                 reallocation. If 'p' != NULL and 'n' == 0 the operation
 *                 performed is a free. If 'p' == NULL and 'n' == 0 no operation
 *                 is performed.
 * <returns (void *): New handle to the memory. (The old handle, 'p', will be
 *                    invalidated.)
 *
 * ! If this function fails, it will log an error message (see XsLogMessage())
 *   then terminate the program via exit(EXIT_FAILURE).
 *
 * ! In the future, an alignment size other than 16 may be used. For example,
 *   the cache-line size may be used. If you rely on a specific alignment, your
 *   code may break in the future. Code that requires strict alignment should
 *   use XsAlloc(), XsRealloc(), and XsFree() instead.
 */
void *XsMemory(void *p, xs_size_t n);

#endif
