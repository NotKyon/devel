#include <stdio.h>
#include <stdlib.h>

void printchar(int c) {
	printf("'%c' %i ($%.2X)\n", c, c, c);
}

int main() {
	printchar('\b');
	printchar('\n');

	return EXIT_SUCCESS;
}
