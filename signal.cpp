#include <windows.h>
#include <cstdio>
#include <cstdlib>

#include <cstdarg>

CRITICAL_SECTION gThreadPrintfLock;
bool gDidInitThreadPrintfLock = false;
void XThreadPrintf(const char *format, ...) {
	va_list args;

	if (!gDidInitThreadPrintfLock) {
		InitializeCriticalSectionAndSpinCount(&gThreadPrintfLock, 1024);
		gDidInitThreadPrintfLock = true;
	}

	EnterCriticalSection(&gThreadPrintfLock);

	fprintf(stdout, "[%u]{", (unsigned int)GetCurrentThreadId());
	va_start(args, format);
	vfprintf(stdout, format, args);
	va_end(args);
	fprintf(stdout, "}\n");
	fflush(stdout);

	LeaveCriticalSection(&gThreadPrintfLock);
}
//#define ThreadPrintf(f,...) XThreadPrintf(f, ##__VA_ARGS__)
#define ThreadPrintf(f,...)

//------------------------------------------------------------------------------

typedef struct signal_s {
	unsigned int expCnt, refCnt;
	HANDLE ev;

	const char *name;
} signal_t;

void InitSignal(signal_t &sig) {
	sig.expCnt = 0;
	sig.refCnt = 0;

	sig.ev = CreateEvent(0, TRUE, FALSE, 0);
}
void FiniSignal(signal_t &sig) {
	sig.expCnt = 0;
	sig.refCnt = 0;

	if (!sig.ev)
		return;

	CloseHandle(sig.ev);
	sig.ev = (HANDLE)0;
}
void ResetSignal(signal_t &sig, unsigned int expCnt) {
	sig.expCnt = expCnt;
	sig.refCnt = 0;

	ResetEvent(sig.ev);
}
void RaiseSignal(signal_t &sig) {
	InterlockedIncrement((LONG *)&sig.refCnt);

	if (sig.refCnt>=sig.expCnt)
		SetEvent(sig.ev);
}
void SyncSignal(signal_t &sig) {
	ThreadPrintf("Sync[%s]-%i/%i", sig.name, sig.refCnt, sig.expCnt);
	WaitForSingleObject(sig.ev, INFINITE);
	ThreadPrintf("Done[%s]-%i/%i", sig.name, sig.refCnt, sig.expCnt);
}
bool TrySyncSignal(signal_t &sig) {
	if (WaitForSingleObject(sig.ev, 0) != WAIT_OBJECT_0)
		return false;

	return true;
}

//------------------------------------------------------------------------------

typedef CRITICAL_SECTION lock_t;

void InitLock(lock_t &lock) {
	InitializeCriticalSectionAndSpinCount(&lock, 4000);
}
void FiniLock(lock_t &lock) {
	DeleteCriticalSection(&lock);
}

bool TryLock(lock_t &lock) {
	if (!TryEnterCriticalSection(&lock))
		return false;

	return true;
}
void Lock(lock_t &lock) {
	EnterCriticalSection(&lock);
}
void Unlock(lock_t &lock) {
	LeaveCriticalSection(&lock);
}

typedef struct autolock_s {
	lock_t &mLock;

	autolock_s(lock_t &lock): mLock(lock) {
		Lock(mLock);
	}
	~autolock_s() {
		Unlock(mLock);
	}
} autolock_t;
#define AUTOLOCK(x) autolock_t __autolockScopeVar_##__COUNTER__##__(x)

//------------------------------------------------------------------------------

#define NUM_JOBS 1024

typedef void(*jobfn_t)(int);

jobfn_t  gJobFuncs[NUM_JOBS];
int      gJobParams[NUM_JOBS];
size_t   gCurJobIdx;

lock_t   gJobsLock;
signal_t gJobsSignal;

bool     gTerminate;
signal_t gTerminateSignal;

HANDLE   gStart;

void IsPrime_f(int i) {
	char buf[32];
	int j;

	if (i<=1)
		snprintf(buf, sizeof(buf) - 1, "%i isn't prime", i);
	else if(i==2)
		snprintf(buf, sizeof(buf) - 1, "%i is prime", i);
	else {
		for(j=3; j<i; j+=2) {
			if (i%j == 0) {
				snprintf(buf, sizeof(buf) - 1, "%i isn't prime", i);
				j = 0;
				break;
			}
		}

		if (j != 0)
			snprintf(buf, sizeof(buf) - 1, "%i is prime", i);
	}

	buf[sizeof(buf) - 1] = '\0';
	printf("%s\n", buf);
	//ThreadPrintf("%s", buf);
}

struct Autoraise {
	signal_t &mSig;

	inline Autoraise(signal_t &sig): mSig(sig) {
	}
	inline ~Autoraise() {
		RaiseSignal(mSig);
	}
};
DWORD WINAPI WorkerThread(void *p) {
	Autoraise autoraiseSig(gTerminateSignal);
	size_t index;

	if (p) {} //unused

	ThreadPrintf("Waiting for work");
	//while(!gStart)
	//	Sleep(1);
	WaitForSingleObject(gStart, INFINITE);
	ThreadPrintf("Working");

	while(!gTerminate) {
		{
			ThreadPrintf("Fetching");
			AUTOLOCK(gJobsLock);

			index = gCurJobIdx++;
			ThreadPrintf("Done fetching");
		}

		if (index >= NUM_JOBS) {
			ThreadPrintf("Out of jobs!");
			Sleep(10);
			continue;
		}

		gJobFuncs[index](gJobParams[index]);
		RaiseSignal(gJobsSignal);
	}

	//RaiseSignal(gTerminateSignal);
	ThreadPrintf("Exiting");
	return 0;
}

unsigned int GetCPUCount() {
	SYSTEM_INFO si;

	memset(&si, 0, sizeof(si));
	GetSystemInfo(&si);

	return (unsigned int)si.dwNumberOfProcessors;
}

int main() {
	unsigned int i, n;
	HANDLE threads[16];
	DWORD s, e;

	for(i=0; i<NUM_JOBS; i++) {
		gJobFuncs[i] = IsPrime_f;
		gJobParams[i] = 100000000 + i;
	}

	gCurJobIdx = 0;

	InitLock(gJobsLock);

	InitSignal(gJobsSignal);
	ResetSignal(gJobsSignal, NUM_JOBS);

	gJobsSignal.name = "Jobs";

	gTerminate = false;
	//gStart = true;
	gStart = CreateEvent(0, TRUE, FALSE, 0);

	n = GetCPUCount();
	if (n > sizeof(threads)/sizeof(threads[0]))
		n = sizeof(threads)/sizeof(threads[0]);

	InitSignal(gTerminateSignal);
	ResetSignal(gTerminateSignal, n);

	gTerminateSignal.name = "Term";

	for(i=0; i<n; i++) {
		threads[i] = CreateThread(0, 0, WorkerThread, 0, 0, 0);
		//SetThreadAffinityMask(threads[i], 3<<(i*2));
		//SetThreadPriority(threads[i], THREAD_PRIORITY_TIME_CRITICAL);
		SetThreadPriority(threads[i], THREAD_PRIORITY_HIGHEST);
	}
	//SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
	//SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);

	s = GetTickCount();
	SetEvent(gStart);

	SyncSignal(gJobsSignal);

	e = GetTickCount();
	printf("%.3f second(s)\n", double(e - s)/1000.0);

	gTerminate = true;
	SyncSignal(gTerminateSignal);

	for(i=0; i<n; i++)
		CloseHandle(threads[i]);

	CloseHandle(gStart);
	FiniSignal(gTerminateSignal);
	FiniSignal(gJobsSignal);
	FiniLock(gJobsLock);

	return EXIT_SUCCESS;
}
