#include <stdio.h>
#include <math.h>

unsigned int g_numTables = 0;
unsigned int g_memUsage = 0;
void generate_table( const char *name, unsigned int size, float base, float unit,
    float ( *func )( float ) )
{
	unsigned int i;
	float f, s, x;

	printf( "static float __%s_table[%u] =\n{\n\t", name, size );

	s = ( float )size;

	for( i = 0; i < size; i++ ) {
		f = ( float )i;
		x = base + i / s * unit;

		printf( "%f", func( x ) );

		if( i + 1 != size ) {
			printf( "," );
			if( ( i + 1 ) % 8 == 0 )
				printf( "\n\t" );
		} else
			printf( "\n" );
	}

	printf( "};\n" );
	printf( "float %s_fast(float x) {\n", name );
	printf( "\treturn __%s_table[ftoui_fast(((x-(%f))/%f)*%f)%%%u];\n", name, base,
	    unit, s, size );
	printf( "}\n\n" );

	g_numTables++;
	g_memUsage += sizeof( float ) * size;
}

#define PI 3.141592653589793238462643383279502884197169
#define TABLE_SIZE 1024

int main()
{
	printf( "/*\n" );
	printf( " * Fast Math Routines - AUTO-GENERATED: DO NOT EDIT!\n" );
	printf( " * See \"%s\" for more information.\n", __FILE__ );
	printf( " */\n\n" );

	printf( "float fabs_fast(float x) {\n" );
	printf( "\tunion { float f; unsigned int i; } v;\n\n" );
	printf( "\tv.f = x;\n" );
	printf( "\tv.i &= 0x7FFFFFFF;\n\n" );
	printf( "\treturn v.f;\n}\n\n" );

	/* source: Software Optimization Cookbook, 2006, Intel Press -- p. 187 */
	printf( "\nint ftoi_fast(float x) {\n" );
	printf( "\tunion { float f; int i; } v;\n" );
	printf( "\n\tv.f   = x + (float)(3<<21);\n" );
	printf( "\tv.i  -= 0x4AC00000;\n" );
	printf( "\tv.i >>= 1;\n" );
	printf( "\n\treturn v.i;\n}\n\n" );
	printf( "\nunsigned int ftoui_fast(float x) {\n" );
	printf( "\tunion { float f; int i; } v;\n" );
	printf( "\n\tv.f   = x + (float)(3<<21);\n" );
	printf( "\tv.i  -= 0x4AC00000;\n" );
	printf( "\tv.i >>= 1;\n" );
	printf( "\tv.i  &= 0x7FFFFFFF;\n" );
	printf( "\n\treturn *(unsigned int *)&v.i;\n}\n\n" );
	printf( "float sqrt_fast(float x) {\n" );
	printf( "\tunion { float f; int i; } v;\n\n" );
	printf( "\tv.f   = x;\n" );
	printf( "\tv.i  -= 0x3F800000;\n" );
	printf( "\tv.i >>= 1;\n" );
	printf( "\tv.i  += 0x3F800000;\n\n" );
	printf( "\treturn v.f;\n}\n\n" );

	/* source: http://en.wikipedia.org/wiki/Fast_inverse_square_root */
	printf( "float invsqrt_fast(float x) {\n" );
	printf( "\tunion { float f; int i; } v;\n\n" );
	printf( "\tv.f = x;\n" );
	printf( "\tv.i = 0x5F3759DF - (v.i>>1);\n\n" );
	printf( "\treturn v.f*(1.5f - 0.5f*x*v.f*v.f);\n}\n\n" );

	/*
       * NOTE: You can tweak the accuracy of each function here.
       *       The memory usage per table will be near `multiplier' kilobytes.
       *       Try to leave the multiplier at either 1, 2, 4, or 8. If it's a
       *       power of two, the modulus operation will be a simple bitwise and.
       *       If it's greater than 8, the table will consume a large amount of
       *       space and take a while to be written and processed.
       */
	generate_table( "sin", TABLE_SIZE * 4, 0, PI * 2, sinf );
	generate_table( "cos", TABLE_SIZE * 4, 0, PI * 2, cosf );
	generate_table( "tan", TABLE_SIZE * 1, 0, PI * 2, tanf );
	generate_table( "asin", TABLE_SIZE * 4, -1, 2, asinf );
	generate_table( "acos", TABLE_SIZE * 4, -1, 2, acosf );
	generate_table( "atan", TABLE_SIZE * 4, -1, 2, atanf );

	printf( "float atan2_fast(float y, float x) {\n" );
	printf( "\tstatic const float c_pi=3.14159265358979323846;\n" );
	printf( "\tfloat ax, ay, v;\n\n" );
	printf( "\tif (x==0 && y==0)\n\t\treturn 0;\n\n" );
	printf( "\tay = fabs_fast(y);\n" );
	printf( "\tax = fabs_fast(x);\n\n" );
	printf( "\tif (ay-ax==ay)\n\t\treturn y<0?-c_pi*2:c_pi*2;\n\n" );
	printf( "\tif (ax-ay==ax)\n\t\tv = 0;\n\telse\n\t\tv = atan_fast(y/x);\n\n" );
	printf( "\tif (x>0)\n\t\treturn v;\n\n" );
	printf( "\tif (y<0)\n\t\treturn v-c_pi;\n\n" );
	printf( "\treturn v+c_pi;\n}\n\n" );

	printf( "/*\n" );
	printf( " * %u tables generated with %u elements each.\n", g_numTables,
	    TABLE_SIZE );
	printf( " * Total table memory usage: %u bytes.\n", g_memUsage );
	printf( " */\n\n" );

	return 0;
}
