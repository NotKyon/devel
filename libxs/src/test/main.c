#include <xs/xs.h>
#include <stdio.h>
#include <string.h>

void write_byte(void *p, xs_size_t i, unsigned char v) {
	((unsigned char *)p)[i] = v;
}
unsigned char read_byte(void *p, xs_size_t i) {
	return ((unsigned char *)p)[i];
}

void test_realloc() {
	static const unsigned char chk[] = { 0xFF, 0xCB, 0x13, 0x27, 0x48 };
	xs_size_t i;
	void *p, *q;

	p = XsMemory((void *)0, sizeof(chk));
	printf("%p\n", p);
	memcpy((void *)p, (const void *)chk, sizeof(chk));

	fflush(stdout);

	for(i=0; i<sizeof(chk)/sizeof(chk[0]); i++)
		XS_ASSERT(read_byte(p, i)==chk[i]);

	q = XsMemory((void *)0, 0x100000);

	p = XsMemory(p, sizeof(chk) + 0x100000);
	printf("%p\n", p);

	fflush(stdout);

	q = XsMemory(q, 0);

	for(i=0; i<sizeof(chk)/sizeof(chk[0]); i++)
		XS_ASSERT(read_byte(p, i)==chk[i]);
}

void test_buffer() {
	xs_buffer_t buf;
	xs_uint_t curline;
	char text[8192];

	buf = XsLoadBuffer("src/test/test.config");

	printf("\"%s\":\n", XsBufferFilename(buf));
	while(!XsBufferEnd(buf)) {
		curline = XsBufferLine(buf);
		if (!XsReadBufferLine(buf, text, sizeof(text)))
			break;

        printf("  %.4u: %s", curline, text);
	}
	printf("\n");

	XsDeleteBuffer(buf);
}

void test_config() {
	xs_section_t prnt, sect;
	xs_config_t conf;
	xs_key_t key;

	if (!(conf = XsLoadConfig("src/test/test.config")))
		return;

	for(sect=XsFirstRootSection(conf); sect; sect=XsSectionAfter(sect)) {
		printf("root section: \"%s\"\n", XsSectionName(sect));
		for(key=XsFirstKey(sect); key; key=XsKeyAfter(key)) {
			printf("root section key: \"%s\"=\"%s\"\n",
				XsKeyName(key), XsKeyValue(key));
		}

		prnt = sect;
		for(sect=XsFirstSection(prnt); sect; sect=XsSectionAfter(sect)) {
			printf("  section: \"%s\"\n", XsSectionName(sect));
			for(key=XsFirstKey(sect); key; key=XsKeyAfter(key)) {
				printf("  section key: \"%s\"=\"%s\"\n",
					XsKeyName(key), XsKeyValue(key));
			}
		}
		sect = prnt;
	}

	XsDeleteConfig(conf);
}

int main() {
	test_realloc();
	test_buffer();
	test_config();

	return 0;
}
