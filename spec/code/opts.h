#ifndef OPTS_H
#define OPTS_H

#include "local.h"

enum {
	kOptFlag_Switch_Bit = (1<<0),
	kOptFlag_RunOnce_Bit = (1<<1),
	kOptFlag_Disabled_Bit = (1<<2),
	kOptFlag_Param_Bit = (1<<3),
	kOptFlag_Optional_Bit = (1<<4)
};

typedef void(*optfn_t)(const char *parm);

typedef struct opt_s {
	const char *name;	/* name of argument (e.g., "include-dir") */
	int flags;			/* details of how to process this argument */
	optfn_t fn;			/* callback function */
	int *bitfield;		/* pointer to bitfield to apply operation to */
	int bitmask;		/* mask of bits to apply operation with */
} opt_t;
#define OPT_SWITCH(n, bf,bm)\
	{ (n), kOptFlag_Switch_Bit, (optfn_t)NULL, bf, bm }
#define OPT_MP(n, fn)\
	{ (n), 0, (fn), (int *)NULL, 0 }
#define OPT_SP(n, fn)\
	{ (n), kOptFlag_RunOnce_Bit, (fn), (int *)NULL, 0 }
#define OPT_MP_PARM(n, fn)\
	{ (n), kOptFlag_Param_Bit, (fn), (int *)NULL, 0 }
#define OPT_SP_PARM(n, fn)\
	{ (n), kOptFlag_Param_Bit|kOptFlag_RunOnce_Bit, (fn), (int *)NULL, 0 }
#define OPT_MP_PARM_LOOSE(n, fn)\
	{ (n), kOptFlag_Param_Bit|kOptFlag_Optional_Bit, (fn), (int *)NULL, 0 }
#define OPT_SP_PARM_LOOSE(n, fn)\
	{ (n), kOptFlag_Param_Bit|kOptFlag_RunOnce_Bit|kOptFlag_Optional_Bit, (fn),\
	  (int *)NULL, 0 }
#define OPT_SWITCH_PARM_LOOSE(n, fn, bf,bm)\
	{ (n), kOptFlag_Switch_Bit|kOptFlag_Param_Bit|\
	       kOptFlag_RunOnce_Bit|kOptFlag_Optional_Bit, (fn), bf, bm }

extern opt_t gOptTable[]; /* implemented in main.c */
extern size_t gOptTableSize; /* implemented in main.c */
extern const char *gOptAlias[256]; /* defined in opts.c; set in main.c */

void PushFile(const char *f);
size_t CountFiles();
const char *GetFile(size_t i);
void FiniFiles();

void ProcessOpts(int argc, char **argv);

#endif
