#include <gamebase.h>

static char *g_appDir = (char *)NULL;
static char *g_appFil = (char *)NULL;
static char *g_appTitle = (char *)NULL;
static char *g_launchDir = (char *)NULL;

static size_t g_numArgs = 0, g_maxArgs = 0;
static char **g_args = (char **)NULL;

/* -------------------------------------------------------------------------- */

const char *gbAppDir() {
	return g_appDir;
}
const char *gbAppFile() {
	return g_appFil;
}

const char *gbAppTitle() {
	return g_appTitle;
}
void gbSetAppTitle(const char *title) {
	g_appTitle = gbCopy(g_appTitle, title);
}

size_t gbCountAppArgs() {
	return g_numArgs;
}
const char *gbAppArg(size_t i) {
	gbAssert(i < g_numArgs);

	return g_args[i];
}

const char *gbLaunchDir() {
	return g_launchDir;
}

/* -------------------------------------------------------------------------- */

void gbSetAppDir(const char *appdir) {
	g_appDir = gbCopy(g_appDir, appdir);
}
void gbSetAppFile(const char *appfile) {
	g_appFil = gbCopy(g_appFil, appfile);
}
void gbSetLaunchDir(const char *launchdir) {
	g_launchDir = gbCopy(g_launchDir, launchdir);
}

void gbResetAppArgs() {
	size_t i;

	for(i=0; i<g_numArgs; i++)
		g_args[i] = (char *)gbMemFree((void *)g_args[i]);

	g_numArgs = 0;
	g_maxArgs = 0;
	g_args = (char **)gbMemFree((void *)g_args);
}
void gbAddAppArgs(int argc, char **argv) {
	int i;

	gbAssert(argc >= 0);
	gbAssert(argv != (char **)NULL);

	if (!argc)
		return;

	if (g_numArgs + (size_t)argc > g_maxArgs) {
		g_maxArgs  = g_numArgs + (size_t)argc;
		g_maxArgs += 32;
		g_maxArgs -= g_maxArgs%32;

		g_args = (char **)gbMemExtend((void *)g_args, sizeof(char *)*g_maxArgs);
	}

	for(i=0; i<argc; i++)
		g_args[g_numArgs++] = gbDuplicate(argv[i]);
}
