#ifndef MESHIO_TEXTURE_H
#define MESHIO_TEXTURE_H

#ifndef meshio_texture_t_defined
#define meshio_texture_t_defined
typedef struct MeshIO_Texture *meshio_texture_t;
#endif

struct MeshIO_Texture {
	unsigned int refcnt;

	struct {
		size_t n;
		char *p;
	} name;

	bitfield_t flags;

	unsigned int coord_count;
	float translation[ 3 ];
	float scale[ 3 ];
	float rotation;

	struct MeshIO_Texture *prev,
	    *next;
};

extern meshio_texture_t meshio_alloc_texture();
extern meshio_texture_t meshio_dealloc_texture( meshio_texture_t tex );
extern void meshio_retain_texture( meshio_texture_t tex );
extern meshio_texture_t meshio_release_texture( meshio_texture_t tex );
extern size_t meshio_texture_count();
extern meshio_texture_t meshio_first_texture();
extern meshio_texture_t meshio_last_texture();
extern meshio_texture_t meshio_texture_before( meshio_texture_t tex );
extern meshio_texture_t meshio_texture_after( meshio_texture_t tex );
extern size_t meshio_texture_name_length( meshio_texture_t tex );
extern const char *meshio_texture_name( meshio_texture_t tex );
extern void meshio_set_texture_name( meshio_texture_t tex, const char *name );
extern bitfield_t meshio_texture_flags( meshio_texture_t tex );
extern void meshio_set_texture_flags( meshio_texture_t tex, bitfield_t flags );
extern unsigned int meshio_texture_coord_count( meshio_texture_t tex );
extern float *meshio_texture_translation( meshio_texture_t tex );
extern float *meshio_texture_scale( meshio_texture_t tex );
extern float meshio_texture_rotation( meshio_texture_t tex );
extern meshio_texture_t meshio_find_texture( const char *name );
extern meshio_texture_t meshio_add_texture( const char *name );

#endif
