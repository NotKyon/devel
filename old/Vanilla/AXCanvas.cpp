// includes
#include <Axia/Vanilla/Vanilla.h>
#include <Axia/Vanilla/Internal.h>

// AXCanvas: ContextRef: get the handle (HDC) of the window's device context
HDC AXCanvas::ContextRef(void) const
{
	return m_hdcRef;
}

// AXCanvas: OpenGLRef: get the rendering context (HGLRC)
HGLRC AXCanvas::OpenGLRef(void) const
{
	return m_hrcRef;
}

// AXCanvas: IsCurrent: determine whether this window has the opengl context
bool AXCanvas::IsCurrent(void)
{
	//if (::wglGetCurrentDC()==m_hdcRef && ::wglGetCurrentContext()==g_hrcGLRef)
	if (::wglGetCurrentDC()==m_hdcRef && ::wglGetCurrentContext()==m_hrcRef)
		return true;
	
	return false;
}

// AXCanvas: MakeCurrent: set this window to own the opengl context
void AXCanvas::MakeCurrent(void)
{
	//::wglMakeCurrent(m_hdcRef, g_hrcGLRef);
	::wglMakeCurrent(m_hdcRef, m_hrcRef);
}
