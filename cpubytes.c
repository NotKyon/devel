#include <stdio.h>
#include <stdlib.h>

void PrintBytes(const char *str) {
	printf("\"%s\": %.8X %.8X %.8X\n", str,
		((const unsigned int *)str)[0],
		((const unsigned int *)str)[1],
		((const unsigned int *)str)[2]);
}

int main() {
	PrintBytes("GenuineIntel");
	PrintBytes("AuthenticAMD");

	return EXIT_SUCCESS;
}
