#ifndef OPENGLFRAMEBUFFER_HPP
#define OPENGLFRAMEBUFFER_HPP

#pragma once

// Do we have everything from GL?
#ifndef GL_FRAMEBUFFER
# include <GL/glew.h>
#endif

// Assertions *are* used.
#ifndef GLFBASSERT
# ifdef AX_ASSERT
#  define GLFBASSERT(Expr_) AX_ASSERT(Expr_)
# else
#  ifndef assert
#   include <cassert>
#  endif
#  define GLFBASSERT(Expr_) assert(Expr_)
# endif
#endif

// Preconditions
#ifndef precondition
# define precondition(Expr_) assert(Expr_)
#endif
// Postconditions
#ifndef postcondition
# define postcondition(Expr_) assert(Expr_)
#endif

// OpenGL format short-hands
#define GL_D24S8 GL_DEPTH24_STENCIL8

// Maximum number of framebuffer color images
constexpr GLuint kMaxGLFBColorImages = 8;

// Framebuffer resolution
struct OpenGLResolution {
	GLuint x = 0;
	GLuint y = 0;

	OpenGLResolution() = default;
	OpenGLResolution( GLuint x, GLuint y )
	: x( x )
	, y( y )
	{
	}

	bool isValid() const {
		return
			x > 0 &&
			y > 0 &&
			x < 0x10000 &&
			y < 0x10000;
	}

	static OpenGLResolution fromViewport();
};

// Information about framebuffer color targets
struct OpenGLColorTargets {
	GLuint count = 0;
	GLuint formats[ kMaxGLFBColorImages ] {};
};
// Information about framebuffer depth-stencil targets
struct OpenGLDepthStencilTargets {
	GLuint format = 0;
};
// Information about framebuffer color and depth-stencil targets
struct OpenGLFramebufferTargets {
	OpenGLColorTargets        color;
	OpenGLDepthStencilTargets depthStencil;
};

// Information about a framebuffer
//
// Usage of this object follows the "builder" paradigm.
// e.g.,
//	OpenGLFramebufferInfo()
//		.setResolutionFromCurrentViewport()
//		.addColorTarget()
//		.addDepthStencilTarget()
// can be used to setup this structure
struct OpenGLFramebufferInfo {
	OpenGLResolution         resolution;
	OpenGLFramebufferTargets targets;

	OpenGLFramebufferInfo &setResolutionFromCurrentViewport() {
		resolution = OpenGLResolution::fromViewport();
		return *this;
	}
	OpenGLFramebufferInfo &setResolution( const OpenGLResolution &res ) {
		resolution = res;
		return *this;
	}
	OpenGLFramebufferInfo &setResolution( GLuint width, GLuint height ) {
		resolution.x = width;
		resolution.y = height;
		return *this;
	}

	OpenGLFramebufferInfo &addColorTarget( GLuint format = GL_RGBA8 ) {
		precondition( format != 0 );
		precondition( targets.color.count < kMaxGLFBColorImages );

		targets.color.formats[ targets.color.count ] = format;
		targets.color.count += 1;

		return *this;
	}
	OpenGLFramebufferInfo &addDepthStencilTarget( GLuint format = GL_D24S8 ) {
		precondition( format != 0 );
		precondition( targets.depthStencil.format == 0 );

		targets.depthStencil.format = format;

		return *this;
	}
};

// Resources used by OpenGLFramebuffer class instances
//
// `fbo` is the framebuffer object handle.
// `colorImages[]` are the color texture handles.
// `depthStencilRBO` is a renderbuffer object, and so cannot be used directly as
// a texture.
struct OpenGLFramebufferResources {
	GLuint fbo = 0;
	GLuint colorImages[ kMaxGLFBColorImages ] {};
	GLuint depthStencilRBO = 0;
};

// Framebuffer utility class
//
// Call `init()` to initialize with defaults. (Resolution comes from the current
// viewport.)
//
// Call `init(const OpenGLFramebufferInfo &)` to initialize with settings.
//
// Resizing can easily be done with either `resize()` method.
//
// Binding is done via `OpenGLFramebuffer::bind(const OpenGLFramebuffer &)`
// To unbind, just call `OpenGLFramebuffer::unbind()`
class OpenGLFramebuffer {
	OpenGLFramebufferInfo      m_info;
	OpenGLFramebufferResources m_resources;

public:
	OpenGLFramebuffer() = default;
	~OpenGLFramebuffer() {
		clearResources();
	}

	const OpenGLFramebufferInfo &getInfo() const {
		return m_info;
	}
	const OpenGLFramebufferResources &getResources() const {
		return m_resources;
	}

	void init() {
		init(
			OpenGLFramebufferInfo()
				.setResolutionFromCurrentViewport()
				.addColorTarget()
				.addDepthStencilTarget()
		);
	}
	void init( const OpenGLFramebufferInfo &info ) {
		precondition( info.resolution.isValid() );
		precondition( info.targets.color.count > 0 );

		clearResources();

		m_info = info;
		initResources();
	}

	void clearResources() {
		if( m_resources.fbo != 0 ) {
			glDeleteFramebuffers( 1, &m_resources.fbo );
			m_resources.fbo = 0;
		}

		if( m_resources.depthStencilRBO != 0 ) {
			glDeleteRenderbuffers( 1, &m_resources.depthStencilRBO );
			m_resources.depthStencilRBO = 0;
		}

		if( m_info.targets.color.count > 0 ) {
			glDeleteTextures(
				m_info.targets.color.count,
				&m_resources.colorImages[ 0 ]
			);
			for( GLuint i = 0; i < m_info.targets.color.count; i += 1 ) {
				m_resources.colorImages[ i ] = 0;
			}
		}
	}
	void initResources() {
		precondition( m_resources.fbo == 0 );
		precondition( m_info.targets.color.count > 0 );

		glGenFramebuffers( 1, &m_resources.fbo );
		glBindFramebuffer( GL_FRAMEBUFFER, m_resources.fbo );

		glGenTextures( m_info.targets.color.count, &m_resources.colorImages[ 0 ] );
		for( GLuint i = 0; i < m_info.targets.color.count; i += 1 ) {
			const GLuint image = m_resources.colorImages[ i ];
			const GLuint format = m_info.targets.color.formats[ i ];

			glBindTexture( GL_TEXTURE_2D, image );

			glTexImage2D( GL_TEXTURE_2D, 0, format, m_info.resolution.x, m_info.resolution.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr );

			// Don't want any weirdness when sampling for postfx
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

			glBindTexture( GL_TEXTURE_2D, 0 );

			glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, image, 0 );
		}

		if( m_info.targets.depthStencil.format != 0 ) {
			glGenRenderbuffers( 1, &m_resources.depthStencilRBO );
			glBindRenderbuffer( GL_RENDERBUFFER, m_resources.depthStencilRBO );

			glRenderbufferStorage( GL_RENDERBUFFER, m_info.targets.depthStencil.format, m_info.resolution.x, m_info.resolution.y );
			glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_resources.depthStencilRBO );
		}

		postcondition( glCheckFramebufferStatus( GL_FRAMEBUFFER ) == GL_FRAMEBUFFER_COMPLETE );

		glBindFramebuffer( GL_FRAMEBUFFER, 0 );
	}

	bool isInitialized() const {
		return m_resources.fbo != 0;
	}

	// return `true` if we did resize; `false` if not
	bool resize() {
		return resize( OpenGLResolution::fromViewport() );
	}
	// return `true` if we did resize; `false` if not
	bool resize( const OpenGLResolution &res ) {
		precondition( isInitialized() );
		precondition( res.isValid() );

		if( m_info.resolution.x == res.x && m_info.resolution.y == res.y ) {
			return false;
		}

		clearResources();
		m_info.resolution = res;
		initResources();

		return true;
	}

	static void bind( const OpenGLFramebuffer &fb ) {
		precondition( fb.isInitialized() );

		glBindFramebuffer( GL_FRAMEBUFFER, fb.m_resources.fbo );
	}
	static void unbind() {
		glBindFramebuffer( GL_FRAMEBUFFER, 0 );
	}
};

inline OpenGLResolution OpenGLResolution::fromViewport() {
	GLint vp[ 4 ] = {};
	glGetIntegerv( GL_VIEWPORT, &vp[ 0 ] );

	return
		OpenGLResolution
		(
			GLuint( vp[ 2 ] - vp[ 0 ] ),
			GLuint( vp[ 3 ] - vp[ 1 ] )
		);
}

#endif // OPENGLFRAMEBUFFER_HPP
