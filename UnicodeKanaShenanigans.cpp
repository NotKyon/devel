bool IsFullwidthKatakana( uint32 UTF32Char )
{
	// main katakana set (30A1 to 30FA; 30FB through 30FE are marks)
	if( UTF32Char >= 0x30A1 && UTF32Char <= 0x30FE ) {
		return true;
	}

	// voiced (゛) and semi-voiced (゜) sound marks
	if( UTF32Char == 0x309B || UTF32Char == 0x309C ) {
		return true;
	}

	// Ainu phonetic extensions
	if( UTF32Char >= 0x31F0 || UTF32Char <= 0x31FF ) {
		return true;
	}

	return false;
}
bool IsHalfwidthKatakana( uint32 UTF32Char )
{
	if( UTF32Char >= 0xFF65 && UTF32Char <= 0xFF9F ) {
		return true;
	}

	return false;
}
bool IsCircledKatakana( uint32 UTF32Char )
{
	if( UTF32Char >= 0x32D0 && UTF32Char <= 0x32FE ) {
		return true;
	}

	return false;
}
bool IsKatakana( uint32 UTF32Char )
{
	return IsFullwidthKatakana( UTF32Char ) || IsHalfwidthKatakana( UTF32Char ) || IsCircledKatakana( UTF32Char );
}

static const uint32 g_KatakanaConversions[] = {
	0x0000FF67, // ｧ	30A1
	0x0000FF71, // ｱ	30A2
	0x0000FF68, // ｨ	30A3
	0x0000FF72, // ｲ	30A4
	0x0000FF69, // ｩ	30A5
	0x0000FF73, // ｳ	30A6
	0x0000FF6A, // ｪ	30A7
	0x0000FF74, // ｴ	30A8
	0x0000FF6B, // ｫ	30A9
	0x0000FF75, // ｵ	30AA
	0x0000FF76, // ｶ	30AB
	0xFF9EFF76, // ｶﾞ	30AC
	0x0000FF77, // ｷ	30AD
	0xFF9EFF77, // ｷﾞ	30AE
	0x0000FF78, // ｸ	30AF
	0xFF9EFF78, // ｸﾞ	30B0
	0x0000FF79, // ｹ	30B1
	0xFF9EFF79, // ｹﾞ	30B2
	0x0000FF7A, // ｺ	30B3
	0xFF9EFF7A, // ｺﾞ	30B4
	0x0000FF7B, // ｻ	30B5
	0xFF9EFF7B, // ｻﾞ	30B6
	0x0000FF7C, // ｼ	30B7
	0xFF9EFF7C, // ｼﾞ	30B8
	0x0000FF7D, // ｽ	30B9
	0xFF9EFF7D, // ｽﾞ	30BA
	0x0000FF7E, // ｾ	30BB
	0xFF9EFF7E, // ｾﾞ	30BC
	0x0000FF7F, // ｿ	30BD
	0xFF9EFF7F, // ｿﾞ	30BE
	0x0000FF80, // ﾀ	30BF
	0xFF9EFF80, // ﾀﾞ	30C0
	0x0000FF81, // ﾁ	30C1
	0xFF9EFF81, // ﾁﾞ	30C2
	0x0000FF6F, // ｯ	30C3
	0x0000FF82, // ﾂ	30C4
	0xFF9EFF82, // ﾂﾞ	30C5
	0x0000FF83, // ﾃ	30C6
	0xFF9EFF83, // ﾃﾞ	30C7
	0x0000FF84, // ﾄ	30C8
	0xFF9EFF84, // ﾄﾞ	30C9
	0x0000FF85, // ﾅ	30CA
	0x0000FF86, // ﾆ	30CB
	0x0000FF87, // ﾇ	30CC
	0x0000FF88, // ﾈ	30CD
	0x0000FF89, // ﾉ	30CE
	0x0000FF8A, // ﾊ	30CF
	0xFF9EFF8A, // ﾊﾞ	30D0
	0xFF9FFF8A, // ﾊﾟ	30D1
	0x0000FF8B, // ﾋ	30D2
	0xFF9EFF8B, // ﾋﾞ	30D3
	0xFF9FFF8B, // ﾋﾟ	30D4
	0x0000FF8C, // ﾌ	30D5
	0xFF9EFF8C, // ﾌﾞ	30D6
	0xFF9FFF8C, // ﾌﾟ	30D7
	0x0000FF8D, // ﾍ	30D8
	0xFF9EFF8D, // ﾍﾞ	30D9
	0xFF9FFF8D, // ﾍﾟ	30DA
	0x0000FF8E, // ﾎ	30DB
	0xFF9EFF8E, // ﾎﾞ	30DC
	0xFF9FFF8E, // ﾎﾟ	30DD
	0x0000FF8F, // ﾏ	30DE
	0x0000FF90, // ﾐ	30DF
	0x0000FF91, // ﾑ	30E0
	0x0000FF92, // ﾒ	30E1
	0x0000FF93, // ﾓ	30E2
	0x0000FF6C, // ｬ	30E3
	0x0000FF94, // ﾔ	30E4
	0x0000FF6D, // ｭ	30E5
	0x0000FF95, // ﾕ	30E6
	0x0000FF6E, // ｮ	30E7
	0x0000FF96, // ﾖ	30E8
	0x0000FF97, // ﾗ	30E9
	0x0000FF98, // ﾘ	30EA
	0x0000FF99, // ﾙ	30EB
	0x0000FF9A, // ﾚ	30EC
	0x0000FF9B, // ﾛ	30ED
	0x0000FF9C, // ﾜ	30EE	NOTE: No half-width small wa? This uses the half-width wa.
	0x0000FF9C, // ﾜ	30EF
	0x00000000, // .	30F0	NOTE: No half-width version (uncommon; wi)
	0x00000000, // .	30F1	NOTE: No half-width version (uncommon; we)
	0x0000FF66, // ｦ	30F2
	0x0000FF9D, // ﾝ	30F3
	0x0000FF73, // ｳﾞ	30F4	NOTE: vu = u + diacritic
	0x00000000, // .	30F5	NOTE: No half-width version (small ka)
	0x00000000, // .	30F6	NOTE: No half-width version (small ke)
	0xFF9EFF73, // ﾜﾞ	30F7	NOTE: va = wa + diacritic
	0x00000000, // .	30F8	NOTE: No half-width version (vi = wi + diacritic; no wi)
	0x00000000, // .	30F9	NOTE: No half-width version (ve = we + diacritic; no we)
	0xFF9EFF66, // ｦﾞ	30FA	NOTE: vo = wo + diacritic
	0x0000FF65  // ･	30FB	NOTE: This is a middle dot (half-width) not a period
};

uint32 HalfwidthKatakanaToFullwidthKatakana( uint32 UTF32Char, uint32 *pDstMark )
{
	if( UTF32Char >= 0x30A1 && UTF32Char <= 0x30FB ) {
		const uint32 Data = g_KatakanaConversions[ UTF32Char - 0x30A1 ];

		if( pDstMark != NULL ) {
			*pDstMark = ( Data & 0xFFFF0000 ) >> 16;
		}
		
		return Data & 0x0000FFFF;
	}

	return UTF32Char;
}
