#ifndef TASKSHARE_SEMAPHORE_H
#define TASKSHARE_SEMAPHORE_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

__TS_NS_INIT__

typedef struct semaphore_s {
#if _WIN32
	HANDLE sem;
#else
	sem_t sem;
#endif
} semaphore_t;

inline bool InitSemaphore(semaphore_t &sem, u32 initCount);
inline void FiniSemaphore(semaphore_t &sem);
inline void WaitSemaphore(semaphore_t &sem);
inline bool TryWaitSemaphore(semaphore_t &sem);
inline void SignalSemaphore(semaphore_t &sem);
inline u32 ReadSemaphore(semaphore_t &sem);

inline bool InitSemaphore(semaphore_t &sem, u32 initCount) {
#if _WIN32
	if (!(sem.sem = CreateSemaphoreA(NULL, initCount, 0x7FFFFFFF, NULL)))
		return false;

	return true;
#else
	if (sem_init(&sem.sem, 0, curCnt)==-1)
		return false;

	return true;
#endif
}
inline void FiniSemaphore(semaphore_t &sem) {
#if _WIN32
	CloseHandle(sem.sem);
	sem.sem = (HANDLE)NULL;
#else
	sem_destroy(&sem.sem);
	memset(&sem.sem, 0, sizeof(sem.sem));
#endif
}
inline void WaitSemaphore(semaphore_t &sem) {
#if _WIN32
	WaitForSingleObject(sem.sem, INFINITE);
#else
	sem_wait(&sem.sem);
#endif
}
inline bool TryWaitSemaphore(semaphore_t &sem) {
#if _WIN32
	if (WaitForSingleObject(sem.sem, 0)==WAIT_OBJECT_0)
		return true;

	return false;
#else
	return !sem_trywait(&sem.sem);
#endif
}
inline void SignalSemaphore(semaphore_t &sem) {
#if _WIN32
	ReleaseSemaphore(sem.sem, 1, NULL);
#else
	sem_post(&sem.sem);
#endif
}
inline u32 ReadSemaphore(semaphore_t &sem) {
#if _WIN32
	LONG cnt;

	if (!ReleaseSemaphore(&sem.sem, 0, &cnt))
		cnt = 0;

	return (u32)cnt;
#else
	int cnt;

	if (sem_getvalue(&sem.sem)==-1)
		cnt = 0;

	return (u32)cnt;
#endif
}

__TS_NS_FINI__

#endif
