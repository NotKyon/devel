#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char buf[512];
	int i;

	while(1) {
		printf("READY: ");
		if (!fgets(buf, sizeof(buf) - 1, stdin))
			return;

		buf[sizeof(buf) - 1] = '\0';

		if (sscanf(buf, "%i\n", &i) < 1) {
			fprintf(stderr, "ERROR: Wrong input\n");
			continue;
		}

		break;
	}

	printf("INPUT: %i\n", i);
	return EXIT_SUCCESS;
}
