#define GB_INIT 1
#include <gamebase.h>

#include <stdlib.h> //for rand()

//
//	The following data structure is used to demonstrate how you might use locks.
//	This structure represents a linked list of integers using which can be added
//	and removed from the list in a thread-safe manner.
//
struct Data {
	int amount;
	gb_node_t link;

	static gb_list_t g_list;
	static gb_lock_t g_lock;

	inline Data() {
		gbInitNode(&link, (void *)this);

		gbLock(g_lock);
		gbAddLast(&g_list, &link);
		gbUnlock(g_lock);
	}
	inline ~Data() {
		gbLock(g_lock);
		gbRemoveNode(&link);
		gbUnlock(g_lock);
	}

	inline void SetAmount(int x) {
		amount = x;
	}
	inline int GetAmount() const {
		return amount;
	}
};

gb_list_t Data::g_list;
gb_lock_t Data::g_lock;

gb_bool_t g_start = GB_FALSE; //tells the threads to start working

//
//	Each thread will add a couple of random numbers to the list, then remove a
//	couple of the added numbers.
//
//	Threads will also wait until they have been explicitly told to start.
//
int Thread_f(void *unused) {
	size_t index;
	Data *p[50];

	if (unused) {} //unused

	// wait for the start signal
	while(!g_start)
		continue;

	// allocate random numbers for the list
	for(index=0; index<sizeof(p)/sizeof(p[0]); index++) {
		p[index] = new Data();
		p[index]->SetAmount(rand() % 1000);
	}

	// remove a couple of the random numbers
	for(index=0; index<sizeof(p)/sizeof(p[0]); index++) {
		// 75% of the added numbers will be removed
		if (rand() % 100 > 100 - 75) {
			delete p[index];
			p[index] = (Data *)NULL;
		}
	}

	// done
	return 0;
}

//
//	Create a couple of threads to test with. Then display the results
//
void GBMain() {
	gb_thread_t threads[32];
	size_t i;
	Data *p;

	// initialize the data structure
	gbInitList(&Data::g_list);
	Data::g_lock = gbNewLock();

	// create the threads
	for(i=0; i<sizeof(threads)/sizeof(threads[0]); i++)
		threads[i] = gbNewThread(Thread_f, (void *)NULL);

	// tell them all to get started!
	g_start = GB_TRUE;

	// wait for all of the threads to finish
	for(i=0; i<sizeof(threads)/sizeof(threads[0]); i++)
		threads[i] = gbDeleteThread(threads[i]);

	// display the results
	i = 0;
	for(p=(Data *)gbFirst(&Data::g_list); p; p=(Data *)gbAfter(&p->link))
		gbWriteStdout(gbF("%i%c", p->GetAmount(), ++i%8==0 ? '\n' : '\t'));

	// remove all entries
	while((p = (Data *)gbFirst(&Data::g_list)) != (Data *)NULL)
		delete p;

	// free the lock
	Data::g_lock = gbDeleteLock(Data::g_lock);
}
