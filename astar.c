//
//	A* (A-Star) Test
//	20120802 Aaron Miller <nocannedmeat@gmail.com>
//
//	NOTE: This is a learning example and is not meant to be efficient. There are
//	      many of various ways to improve performance. The goal here is to get
//	      the algorithm working.
//
//	BASIC ALGORITHM
//	---------------
//	1. Add the starting node to the list of open nodes.
//	2. If the open node list is not empty:
//	2a. Move this node from the open list to the closed list (so it is not
//	    walked again).
//  2b. If the goal node has been closed, this is the path. Therefore, stop.
//	2c. Compute the 'G' and 'H' scores of each adjacent walkable node.
//	2d. Compute the 'F' score (G + H) of these nodes.
//	2e. Set the current node to the node with the lowest 'F' score. (If there
//	    are multiple such nodes, it doesn't matter which you choose.)
//	2f. Go to 2.
//	3. There are no open nodes left in the list, the goal is unreachable.
//
//	The 'G' score is the cost to move from the starting point to the node.
//	The 'H' score is the cost to move from the node to the ending point.
//
//	The manhatten distance is used to compute the 'H' (heuristic) score.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct astar_search_node_s {
	unsigned int x, y;
	unsigned int f;

	unsigned int px, py; //parent location

	struct astar_search_node_s *prev, *next;
	struct astar_search_node_s **p_head, **p_tail;
} astar_search_node_t;

#define RES_X 7
#define RES_Y 7
unsigned char g_map[RES_X*RES_Y];
unsigned int g_start[2] = { 0,0 };
unsigned int g_end[2] = { RES_X-1,RES_Y-1 };
unsigned int g_cur[2] = { 0,0 };

astar_search_node_t *open_head = (astar_search_node_t *)NULL;
astar_search_node_t *open_tail = (astar_search_node_t *)NULL;
astar_search_node_t *clos_head = (astar_search_node_t *)NULL;
astar_search_node_t *clos_tail = (astar_search_node_t *)NULL;

astar_search_node_t *g_node_map[RES_X*RES_Y];

struct map_buffer_s {
	unsigned char map[RES_X*RES_Y];
};
struct map_buffer_s g_bufs[70/(RES_X + 2)];
size_t g_buf_cur = 0;

void FlushMapBuffers() {
	static unsigned int iter = 0;
	size_t buf, row, x;
	char chars[RES_X + 1], *p;

	if (!g_buf_cur)
		return;

	chars[RES_X] = '\0';

	for(buf=0; buf<g_buf_cur; buf++) {
		snprintf(chars, sizeof(chars), "%u", ++iter);
		p = strchr(chars, '\0');
		x = (size_t)(p - &chars[0]);
		while(x < RES_X)
			chars[x++] = '\xCD';
		printf("\xC9%s\xBB ", chars);
	}
	printf("\n");

	for(row=0; row<RES_Y; row++) {
		for(buf=0; buf<g_buf_cur; buf++)
			printf("\xBA%.*s\xBA ", RES_X, &g_bufs[buf].map[row*RES_X]);
		printf("\n");
	}

	memset(chars, '\xCD', RES_X);
	chars[RES_X] = '\0';

	for(buf=0; buf<g_buf_cur; buf++)
		printf("\xC8%s\xBC ", chars);
	printf("\n");

	g_buf_cur = 0;
}
void BufferMap() {
	astar_search_node_t *node;
	memcpy(g_bufs[g_buf_cur].map, g_map, RES_X*RES_Y);

	for(node=open_head; node; node=node->next)
		g_bufs[g_buf_cur].map[node->y*RES_X + node->x] = 'o';

	for(node=clos_head; node; node=node->next) {
		if (g_bufs[g_buf_cur].map[node->y*RES_X + node->x]!=' ')
			continue;

		g_bufs[g_buf_cur].map[node->y*RES_X + node->x] = 'x';
	}

	g_bufs[g_buf_cur].map[g_start[1]*RES_X + g_start[0]] = '\x01';
	g_bufs[g_buf_cur].map[g_end[1]*RES_X + g_end[0]] = '\xED';
	g_bufs[g_buf_cur].map[g_cur[1]*RES_X + g_cur[0]] = '\x02';

	if (++g_buf_cur == sizeof(g_bufs)/sizeof(g_bufs[0]))
		FlushMapBuffers();
}
void PrintMap() {
	astar_search_node_t *node;
	unsigned char orig[RES_X*RES_Y];
	size_t i;
	char chars[512];

	FlushMapBuffers();

	memcpy(orig, g_map, sizeof(orig));

	for(node=open_head; node; node=node->next)
		g_map[node->y*RES_X + node->x] = 'o';

	for(node=clos_head; node; node=node->next) {
		if (g_map[node->y*RES_X + node->x]!=' ')
			continue;

		g_map[node->y*RES_X + node->x] = 'x';
	}

	g_map[g_start[1]*RES_X + g_start[0]] = '\x01';
	g_map[g_end[1]*RES_X + g_end[0]] = '\xED';
	g_map[g_cur[1]*RES_X + g_cur[0]] = '\x02';

	for(i=0; i<RES_X; i++)
		chars[i] = '\xCD';
	chars[i] = '\0';

	printf(	"\xC9%s\xBB\n", chars);
	for(i=0; i<RES_Y; i++)
		printf("\xBA%.*s\xBA\n", RES_X, &g_map[i*RES_X]);
	printf(	"\xC8%s\xBC\n\n", chars);

	memcpy(g_map, orig, sizeof(orig));
}

void ClearMap() {
	memset(g_map, ' ', sizeof(g_map));
	memset(g_node_map, 0, sizeof(g_node_map));

	open_head = (astar_search_node_t *)NULL;
	open_tail = (astar_search_node_t *)NULL;
	clos_head = (astar_search_node_t *)NULL;
	clos_tail = (astar_search_node_t *)NULL;
}
void MarkObstacle(unsigned int x, unsigned int y) {
	g_map[y*RES_X + x] = '\xDB';
}
void MarkPath(unsigned int x, unsigned int y) {
	if (g_map[y*RES_X + x]==' ')
		g_map[y*RES_X + x] = '\xB0';
}
void SetStartPosition(unsigned int x, unsigned int y) {
	g_start[0] = x;
	g_start[1] = y;

	g_cur[0] = g_start[0];
	g_cur[1] = g_start[1];
}
void SetEndPosition(unsigned int x, unsigned int y) {
	g_end[0] = x;
	g_end[1] = y;
}

astar_search_node_t *FindNode(unsigned int x, unsigned int y) {
	return g_node_map[y*RES_X + x];
}

astar_search_node_t *NewNode(unsigned int x, unsigned int y,
astar_search_node_t **p_head, astar_search_node_t **p_tail) {
	astar_search_node_t *node;

	if (!(node = (astar_search_node_t *)malloc(sizeof(*node)))) {
		perror("Failed to allocate node");
		exit(EXIT_FAILURE);
	}

	node->x = x;
	node->y = y;
	node->f = 0;

	node->px = g_cur[0];
	node->py = g_cur[1];

	node->next = (astar_search_node_t *)NULL;
	if ((node->prev = *p_tail) != (astar_search_node_t *)NULL)
		(*p_tail)->next = node;
	else
		*p_head = node;
	*p_tail = node;

	node->p_head = p_head;
	node->p_tail = p_tail;

	g_node_map[y*RES_X + x] = node;

	return node;
}
astar_search_node_t *DeleteNode(astar_search_node_t *node) {
	if (!node)
		return (astar_search_node_t *)NULL;

	if (node->prev)
		node->prev->next = node->next;
	else
		*node->p_head = node->next;
	if (node->next)
		node->next->prev = node->prev;
	else
		*node->p_tail = node->prev;

	g_node_map[node->y*RES_X + node->x] = (astar_search_node_t *)NULL;

	free((void *)node);
	return (astar_search_node_t *)NULL;
}
void DeleteAllNodes() {
	while(open_head)
		DeleteNode(open_head);

	while(clos_head)
		DeleteNode(clos_head);
}
int IsNodeOpen(astar_search_node_t *node) {
	if (!node)
		return 0;

	return (node->p_head == &open_head) ? 1 : 0;
}
int IsNodeClosed(astar_search_node_t *node) {
	if (!node)
		return 0;

	return (node->p_head == &clos_head) ? 1 : 0;
}
void SetNodeList(astar_search_node_t *node, astar_search_node_t **p_head,
astar_search_node_t **p_tail) {
	if (node->p_head==p_head)
		return;

	if (node->prev)
		node->prev->next = node->next;
	else
		*node->p_head = node->next;
	if (node->next)
		node->next->prev = node->prev;
	else
		*node->p_tail = node->prev;

	node->next = (astar_search_node_t *)NULL;
	if ((node->prev = *p_tail) != (astar_search_node_t *)NULL)
		(*p_tail)->next = node;
	else
		*p_head = node;
	*p_tail = node;

	node->p_head = p_head;
	node->p_tail = p_tail;
}
void NodeToFront(astar_search_node_t *node) {
	if (node->prev)
		node->prev->next = node->next;
	else
		*node->p_head = node->next;
	if (node->next)
		node->next->prev = node->prev;
	else
		*node->p_tail = node->prev;

	node->prev = (astar_search_node_t *)NULL;
	if ((node->next = *node->p_head) != (astar_search_node_t *)NULL)
		(*node->p_head)->prev = node;
	else
		*node->p_tail = node;
	*node->p_head = node;
}

// set a location as open if it's not open or closed already (because closed
// nodes don't get re-opened)
void SetLocationToOpen(unsigned int x, unsigned int y) {
	if (FindNode(x, y))
		return;

	NewNode(x, y, &open_head, &open_tail);
}
// set a location as closed if a node for it is present (has to be because nodes
// are only added to the open list)
void SetLocationToClosed(unsigned int x, unsigned int y) {
	astar_search_node_t *node;

	if ((node = FindNode(x, y)) != (astar_search_node_t *)NULL)
		SetNodeList(node, &clos_head, &clos_tail);
}

int IsLocationOpen(unsigned int x, unsigned int y) {
	return IsNodeOpen(FindNode(x, y));
}
int IsLocationClosed(unsigned int x, unsigned int y) {
	return IsNodeClosed(FindNode(x, y));
}

// helper function to order two values
void SortIntegers(unsigned int *a, unsigned int *b) {
	if (*a > *b) {
		*a ^= *b;
		*b ^= *a;
		*a ^= *b;
	}
}

// calculate the manhatten distance (h blocks + v blocks)
unsigned int ManhattenDistance(unsigned int x1, unsigned int y1,
unsigned int x2, unsigned int y2) {
	// sort these
	SortIntegers(&x1, &x2);
	SortIntegers(&y1, &y2);

	// distance is simple now
	return (x2 - x1) + (y2 - y1); //horizontal movements + vertical movements
}

// calculate the 'f' score of the node
astar_search_node_t *CalcNode(int x, int y) {
	astar_search_node_t *node;
	unsigned int f, g, h;

	// mind the boundaries
	if ((unsigned int)(g_cur[0] + x) >= RES_X) //negative unsigned int is large
		return (astar_search_node_t *)NULL;
	if ((unsigned int)(g_cur[1] + y) >= RES_Y)
		return (astar_search_node_t *)NULL;

	// adjust 'x' and 'y' to be the coordinates we use
	x = g_cur[0] + x;
	y = g_cur[1] + y;

	// is this area walkable?
	if (g_map[y*RES_X + x]!=' ')
		return (astar_search_node_t *)NULL;

	// find the node
	node = FindNode(x, y);
	if (IsNodeClosed(node))
		return (astar_search_node_t *)NULL;

	// if the node is already open, we already calculated 'f' (and node is
	// necessarily valid)
	if (IsNodeOpen(node))
		return node;

	// calculate the cost from the starting point to this node
	g = ManhattenDistance(g_start[0], g_start[1], x, y);

	// calculate the cost from this node to the ending point
	h = ManhattenDistance(x, y, g_end[0], g_end[1]);

	// calculate 'f'
	f = g + h;

	// store the value in the open node
	// (NOTE: if the node is neither open nor closed, then it's not present at
	//        all: therefore it must be added to the open list)
	node = NewNode(x, y, &open_head, &open_tail);
	node->f = f;

	// return the node
	return node;
}

// find the lowest open node
astar_search_node_t *FindLowestNode() {
	astar_search_node_t *node, *pick;

	pick = (astar_search_node_t *)open_head;
	for(node=open_head; node; node=node->next) {
		if (node->f < pick->f)
			pick = node;
	}

	return pick;
}

// visualization helper: mark the path we found
void MarkFoundPath() {
	astar_search_node_t *node;

	node = FindNode(g_end[0], g_end[1]);
	while(node->x!=g_start[0] || node->y!=g_start[1]) {
		MarkPath(node->x, node->y);
		node = FindNode(node->px, node->py);
	}
}

int AStarPathFind() {
	static const int off_x[] = { +1,  0, -1,  0 };
	static const int off_y[] = {  0, +1,  0, -1 };
	astar_search_node_t *node, *pick;
	size_t i;

	// 1. Add the starting node to the list of open nodes.
	SetLocationToOpen(g_start[0], g_start[1]);
	g_cur[0] = g_start[0]; //we're starting from the starting node ;)
	g_cur[1] = g_start[1];

	// 2. If the open node list is not empty:
	while(open_head) {
#if 1
		BufferMap();
#endif

		// 2a. Move this node from the open list to the closed list (so it is
		//     not walked again).
		SetLocationToClosed(g_cur[0], g_cur[1]);

		// 2b. If the goal node has been closed, this is the path. So stop.
		if (g_cur[0]==g_end[0] && g_cur[1]==g_end[1]) {
			MarkFoundPath();
			return 1;
		}

		// 2c. Compute the 'G' and 'H' scores of each adjacent walkable node.
		pick = (astar_search_node_t *)NULL; //our best pick from this tangent
		for(i=0; i<4; i++) {
			// 2d. Compute the 'F' score (G + H) of these nodes.
			if (!(node = CalcNode(off_x[i], off_y[i])))
				continue;

			if (!pick || node->f < pick->f)
				pick = node;
		}

		// 2e. Set the current node to the node with the lowest 'F' score. (If
		//     there are multiple such nodes, it doesn't matter which you
		//     choose.)
		node = FindLowestNode();
		if (!node)
			break;

		if (pick && node->f==pick->f)
			node = pick; //travel down our current path

		// use the lowest node
		g_cur[0] = node->x;
		g_cur[1] = node->y;

		// 2f. Go to 2.
		continue;
	}

	// 3. There are no open nodes left in the list, the goal is unreachable.
	return 0;
}

int main() {
	int r;

	// initialize
	ClearMap();

	// where should we start from and end at?
	SetStartPosition(1,3);
	SetEndPosition(5,4);

	// place obstacles around this map
	MarkObstacle(1,4);
	MarkObstacle(2,4);
	MarkObstacle(0,4);
	MarkObstacle(3,4);
	MarkObstacle(3,3);
	MarkObstacle(3,2);
	MarkObstacle(3,1);

	//MarkObstacle(4,4);
	MarkObstacle(5,3);
	MarkObstacle(5,5);
	MarkObstacle(6,4);

	// now do the path finding
	r = AStarPathFind();
	PrintMap();

	// was the path finding successful?
	if (!r)
		printf("Goal is unreachable.\n");

	// finished; delete all the nodes
	DeleteAllNodes();
	return EXIT_SUCCESS;
}
