#include "memory.h"
#include <stdlib.h>
#include <string.h>

void *NewMemory(size_t n) {
	void *p;

	p = malloc(n);
	return p;
}
void *DeleteMemory(void *p) {
	if (!p)
		return (void *)0;

	free(p);
	return (void *)0;
}
void *RenewMemory(void *p, size_t size) {
	if (!p)
		return NewMemory(size);

	p = realloc(p, size);
	return p;
}

#define POKE(p,o,t,v) if(!(p)){return;} *((t *)AddPtr((p),(o)))=v
void PokeInt8(void *p, size_t offset, unsigned char v) {
	POKE(p, offset, unsigned char, v);
}
void PokeInt16(void *p, size_t offset, unsigned short v) {
	POKE(p, offset, unsigned short, v);
}
void PokeInt32(void *p, size_t offset, unsigned int v) {
	POKE(p, offset, unsigned int, v);
}
#undef POKE

#define PEEK(p,o,t) ((p) ? ((int)(*((t *)AddPtr((p),(o))))) : 0)
int PeekInt8(void *p, size_t offset) {
	return PEEK(p, offset, unsigned char);
}
int PeekInt16(void *p, size_t offset) {
	return PEEK(p, offset, unsigned short);
}
int PeekInt32(void *p, size_t offset) {
	return PEEK(p, offset, int);
}
#undef PEEK
