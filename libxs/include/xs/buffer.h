#ifndef XS_BUFFER_H
#define XS_BUFFER_H

#include "basetypes.h"
#include "logging.h"

typedef struct xs_buffer_s *xs_buffer_t;

/*
 * Allocate a new buffer.
 *
 * >filename (const char *): Name of the buffer as returned by
 *                           XsBufferFilename().
 * >source (const char *): Actual data of the buffer. This string will be
 *                         duplicated automatically. Once this function ends,
 *                         you can delete the value passed in.
 * <returns (xs_buffer_t): A handle to the buffer you can pass to any function
 *                         expecting a 'xs_buffer_t'. If this function fails
 *                         NULL will be returned.
 */
xs_buffer_t XsNewBuffer(const char *filename, const char *source);

/*
 * Load a buffer from a file on-disk.
 *
 * >filename (const char *) - Name of the file on the file system.
 * <returns (xs_buffer_t) - A handle to the buffer you can pass to any function
 *                          expecting a 'xs_buffer'. If this function fails NULL
 *                          will be returned.
 */
xs_buffer_t XsLoadBuffer(const char *filename);

/*
 * Remove a buffer from memory.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (Can be NULL.)
 * <returns (xs_buffer_t): Returns NULL. That allows you to do this:
 *                             buf = XsDeleteBuffer(buf);
 */
xs_buffer_t XsDeleteBuffer(xs_buffer_t buf);

/*
 * Retrieve the filename of a buffer.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (const char *): Constant pointer to the buffer's name. (DO NOT
 *                          OVERWRITE!)
 */
const char *XsBufferFilename(xs_buffer_t buf);

/*
 * Retrieve the source text of a buffer.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (const char *): Constant pointer to the buffer's data. (DO NOT
 *                          OVERWRITE!)
 */
const char *XsBufferSource(xs_buffer_t buf);

/*
 * Retrieve the current line of a buffer.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (xs_uint_t): Current line number.
 */
xs_uint_t XsBufferLine(xs_buffer_t buf);

/*
 * Retrieve the current index of a buffer.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (xs_size_t): Current index.
 */
xs_size_t XsBufferIndex(xs_buffer_t buf);

/*
 * Retrieve the length of a buffer (in characters).
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (xs_size_t): Size of the buffer.
 */
xs_size_t XsBufferLength(xs_buffer_t buf);

/*
 * Retrieve the character at the current index in a buffer, without incrementing
 * the index.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (char): Character at XsBufferSource(buf)[XsBufferIndex(buf)].
 */
char XsPeekBuffer(xs_buffer_t buf);

/*
 * Retrieve the character at the current index in a buffer, then increment the
 * index.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (char): Character at XsBufferSource(buf)[XsBufferIndex(buf)].
 */
char XsPullBuffer(xs_buffer_t buf);

/*
 * Increment the current index of the buffer, then retrieve the character at the
 * new index.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <return (char): Character at the new index.
 */
char XsEatBuffer(xs_buffer_t buf);

/*
 * Decrement the current index of a buffer, then retrieve the character at the
 * new index.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (char): Character at XsBufferSource(buf)[XsBufferIndex(buf) - 1].
 *
 * NOTE: It IS safe to call this function when XsBufferIndex(buf)==0.
 */
char XsUnpullBuffer(xs_buffer_t buf);

/*
 * Set the new current index of a buffer.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * >index (xs_size_t): The new index of a buffer. This will be limited to 0 ..
 *                     XsBufferLength() - 1.
 * <returns (xs_size_t): The new current index.
 */
xs_size_t XsSetBufferIndex(xs_buffer_t buf, xs_size_t index);

/*
 * Determine whether the buffer has reached its end.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (xs_bool_t): XS_TRUE if XsBufferIndex() + 1 > XsBufferLength();
 *                       XS_FALSE otherwise.
 */
xs_bool_t XsBufferEnd(xs_buffer_t buf);

/*
 * Skip whitespace in the buffer. (That includes newlines.) Specifically, the
 * current index of the buffer will be moved to the first instance of an ASCII
 * value > ' ' (0x20) after/on its current spot. This function will stop at the
 * end of the file if no such character is found.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 */
void XsSkipBufferWhitespace(xs_buffer_t buf);

/*
 * Skip the current line in the buffer. The current index will be moved to one
 * after the newline character.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 */
void XsSkipBufferLine(xs_buffer_t buf);

/*
 * Advance to the next line feed, consuming the characters prior to and
 * including that line feed.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * >line (char *): Memory block to hold the contents of the line.
 * >n (xs_size_t): Maximum size of the memory block (including NULL). This
 *                 should be greater than 1.
 * <returns (xs_bool_t): XS_TRUE (1) if there are more lines to read; XS_FALSE
 *                       (0) otherwise.
 */
xs_bool_t XsReadBufferLine(xs_buffer_t buf, char *line, xs_size_t n);

/*
 * Retrieve the first buffer.
 *
 * <returns (xs_buffer_t): Handle to a buffer.
 */
xs_buffer_t XsFirstBuffer();

/*
 * Retrieve the last buffer.
 *
 * <returns (xs_buffer_t): Handle to a buffer.
 */
xs_buffer_t XsLastBuffer();

/*
 * Retrieve the buffer before another.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (xs_buffer_t): Handle to the buffer before the one passed.
 */
xs_buffer_t XsBufferBefore(xs_buffer_t buf);

/*
 * Retrieve the buffer after another.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * <returns (xs_buffer_t): Handle to the buffer after the one passed.
 */
xs_buffer_t XsBufferAfter(xs_buffer_t buf);

/*
 * Invoke XsLogMessageV() with details generated from the given buffer and type.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * >type (int): The type of message to log. (kXsLog_Error, kXsLog_Warning,
 *              kXsLog_Remark, kXsLog_Message.)
 * >args (va_list): Argument list started with va_start(). (See <stdarg.h>.)
 */
void XsLogBufferV(xs_buffer_t buf, int type, const char *format, va_list args);

/*
 * Invoke XsLogBufferV() without an explicit 'va_list'.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 * >type (int): The type of message to log. (kXsLog_Error, kXsLog_Warning,
 *              kXsLog_Remark, kXsLog_Message.)
 */
void XsLogBuffer(xs_buffer_t buf, int type, const char *format, ...);

/*
 * Invoke XsLogBufferV() with a type of kXsLog_Error, and without an explicit
 * 'va_list'.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 */
void XsErrorBuffer(xs_buffer_t buf, const char *format, ...);

/*
 * Invoke XsLogBufferV() with a type of kXsLog_Warning, and without an explicit
 * 'va_list'.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 */
void XsWarnBuffer(xs_buffer_t buf, const char *format, ...);

/*
 * Invoke XsLogBufferV() with a type of kXsLogRemark, and without an explicit
 * 'va_list'.
 *
 * >buf (xs_buffer_t): Handle to a buffer. (See XsNewBuffer(), XsLoadBuffer().)
 */
void XsRemarkBuffer(xs_buffer_t buf, const char *format, ...);

#endif
