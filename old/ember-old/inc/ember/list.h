#ifndef EMBER_LIST_H
#define EMBER_LIST_H

#include "config.h"

typedef struct ListBase {
	struct ListLink *head,
	    *tail;
} ListBase;

typedef struct ListLink {
	void *obj;

	struct ListLink *prev,
	    *next;

	struct ListBase *base;
} ListLink;

extern void list_init( ListBase *base );
extern void list_remove_all( ListBase *base );
extern ListLink *list_first_link( ListBase *base );
extern ListLink *list_last_link( ListBase *base );
extern void *list_first( ListBase *base );
extern void *list_last( ListBase *base );
extern void list_link( ListLink *link, void *obj );
extern void list_remove( ListLink *link );
extern void list_add_first( ListBase *base, ListLink *link );
extern void list_add( ListBase *base, ListLink *link );
extern ListLink *list_prev_link( ListLink *link );
extern ListLink *list_next_link( ListLink *link );
extern void *list_prev( ListLink *link );
extern void *list_next( ListLink *link );
extern void list_set( ListLink *link, void *obj );
extern void *list_get( ListLink *link );
extern int list_for_each( ListBase *base, int ( *callback )( ListLink *, void * ), void *data );
extern void list_insert_before( ListLink *link, ListLink *before );
extern void list_insert_after( ListLink *link, ListLink *after );

#endif
