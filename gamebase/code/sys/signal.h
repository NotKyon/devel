#ifndef SIGNAL_H
#define SIGNAL_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "threadconf.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

typedef struct signal_s {
	gb_uint_t expCnt, curCnt;
#if _WIN32
	HANDLE ev;
#else
	pthread_mutex_t mutex;
	pthread_cond_t cond;
#endif
} signal_t;

GB_INLINE void InitSignal(signal_t *sig, gb_uint_t expCnt);
GB_INLINE void FiniSignal(signal_t *sig);

GB_INLINE void ResetSignal(signal_t *sig, gb_uint_t expCnt);
GB_INLINE void AllocSignalSlot(signal_t *sig);
GB_INLINE void RaiseSignal(signal_t *sig);

GB_INLINE void SyncSignal(signal_t *sig);
GB_INLINE gb_bool_t TrySyncSignal(signal_t *sig);

GB_INLINE void SpinSyncSignal(signal_t *sig, size_t spinCount);
GB_INLINE gb_bool_t SyncSignals(size_t *index, size_t n, signal_t **sigs,
	gb_bool_t all);

GB_INLINE void InitSignal(signal_t *sig, gb_uint_t expCnt) {
	sig->expCnt = expCnt;
	sig->curCnt = 0;

#if _WIN32
	sig->ev = CreateEvent(0, TRUE, FALSE, 0);
#else
	pthread_mutex_init(&sig->mutex, 0);
	pthread_cond_init(&sig->cond, 0);
#endif
}
GB_INLINE void FiniSignal(signal_t *sig) {
#if _WIN32
	if (sig->ev) {
		CloseHandle(sig->ev);
		sig->ev = 0;
	}
#else
	pthread_cond_destroy(&sig->cond);
	pthread_mutex_destroy(&sig->mutex);
#endif
}

GB_INLINE void ResetSignal(signal_t *sig, gb_uint_t expCnt) {
	if (expCnt)
		sig->expCnt = expCnt;
	sig->curCnt = 0;

#if _WIN32
	ResetEvent(sig->ev);
#else
#endif
}
GB_INLINE void AllocSignalSlot(signal_t *sig) {
	sig->expCnt++;

#if _WIN32
	if (sig->curCnt>=(sig->expCnt - 1))
		ResetEvent(sig->ev);
#endif
}
GB_INLINE void RaiseSignal(signal_t *sig) {
	gbAtomicIncrement32(&sig->curCnt);

	if (sig->curCnt==sig->expCnt) {
#if _WIN32
		SetEvent(sig->ev);
#else
		pthread_cond_signal(&sig->cond);
			//NOTE: don't use broadcast(); causes hang
#endif
	}
}

GB_INLINE void SyncSignal(signal_t *sig) {
#if _WIN32
	WaitForSingleObject(sig->ev, INFINITE);
#else
	//pthread_mutex_lock(&sig->mutex);
	while(sig->curCnt < sig->expCnt)
		pthread_cond_wait(&sig->cond, &sig->mutex);
	//pthread_mutex_unlock(&sig->mutex);
#endif
}
GB_INLINE gb_bool_t TrySyncSignal(signal_t *sig) {
#if _WIN32
	if (WaitForSingleObject(sig->ev, 0) != WAIT_OBJECT_0)
		return GB_FALSE;

	return GB_TRUE;
#else
	static const struct timespec ts = { 0, 0 };
	gb_bool_t r = GB_FALSE;

	//pthread_mutex_lock(&sig->mutex);
	if (pthread_cond_timedwait(&sig->cond, &sig->mutex, &ts) == 0)
		r = GB_TRUE;
	//pthread_mutex_unlock(&sig->mutex);

	return r;
#endif
}

GB_INLINE void SpinSyncSignal(signal_t *sig, size_t spinCount) {
	size_t i;

	for(i=0; i<spinCount; i++) {
		if (TrySyncSignal(sig))
			return;
	}

	SyncSignal(sig);
}
GB_INLINE gb_bool_t SyncSignals(size_t *index, size_t n, signal_t **sigs,
gb_bool_t all) {
#if _WIN32
	HANDLE h[128];
	size_t i;
	DWORD r;

	if (n > sizeof(h)/sizeof(h[0]))
		return GB_FALSE;

	if (!sigs)
		return GB_FALSE;

	for(i=0; i<n; i++) {
		if (!sigs[i])
			return GB_FALSE;
		h[i] = sigs[i]->ev;
	}

	r = WaitForMultipleObjects(n, h, (BOOL)all, INFINITE);
	if (index) {
		if (r - WAIT_OBJECT_0 < n)
			*index = r - WAIT_OBJECT_0;
		else if(r - WAIT_ABANDONED_0 < n)
			*index = r - WAIT_ABANDONED_0;
	}

	return GB_TRUE;
#else
	signal_t *h[128];
	size_t i, j;

	if (n > sizeof(h)/sizeof(h[0]))
		return GB_FALSE;

	if (!sigs)
		return GB_FALSE;

	memset((void *)h, 0, sizeof(h));

	j = 0;

	for(i=0; i<n; i++) {
		if (h[i])
			continue;

		if (TrySyncSignal(*(sigs[i]))) {
			if (!all) {
				if (index)
					*index = i;

				break;
			}

			h[i] = sigs[i];
			j++;

			if (j==n)
				break;
		}
	}

	return GB_TRUE;
#endif
}

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
