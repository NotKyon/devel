#include <stdio.h>

void PrintDouble(double x) {
	static const char *signs[2] = { "+", "-" };
	unsigned int h, l;
	const char *sign;

#if __i386__||__x86_64__||_M_IX86||_M_IX64
	h = ((unsigned int *)&x)[1];
	l = ((unsigned int *)&x)[0];

	((unsigned int *)&x)[1] &= 0x7FFFFFFF;
#else
	h = ((unsigned int *)&x)[0];
	l = ((unsigned int *)&x)[1];

	((unsigned int *)&x)[0] &= 0x7FFFFFFF;
#endif

	sign = signs[(h&(1<<31))>>31];

	printf("%s%.45f = 0x%.8X%.8X\n", sign, x, h, l);
}

void PrintFloat(float x) {
	static const char *signs[2] = { "+", "-" };
	unsigned int i;
	const char *sign;

	i = *(unsigned int *)&x;
	sign = signs[(i&(1<<31))>>31];
	(*(unsigned int *)&x) &= ~(1<<31);

	printf("%s%.12f%c = 0x%.8X\n", sign, x, 'f', i);
}

int main() {
	PrintDouble(3.141592653589793238462643383279502884197169);

	PrintDouble(-1.0);
	PrintDouble(+1.0);
	PrintDouble(+360.0);
	PrintDouble(+180.0);
	PrintDouble(-180.0);
	PrintDouble(-360.0);

	PrintDouble(+1.0 + 0.0000000000000001110223024625156663683148108874);
	PrintDouble(+1.0 + 0.00000000000000011102230246251566636831481088739);

	PrintFloat(-1.0f);
	PrintFloat(+1.0f);
	PrintFloat(+360.0f);
	PrintFloat(+180.0f);
	PrintFloat(-180.0f);
	PrintFloat(-360.0f);

	PrintFloat(+1.0f + 0.00001f);
	PrintFloat(+1.0f + 0.000001f);
	PrintFloat(+1.0f + 0.0000001f);
	PrintFloat(+1.0f + 0.00000009f);
	PrintFloat(+1.0f + 0.00000008f);
	PrintFloat(+1.0f + 0.00000007f);
	PrintFloat(+1.0f + 0.00000006f);
	PrintFloat(+1.0f + 0.000000059f);

	return 0;
}
