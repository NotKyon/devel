#ifndef AARON_ARG_H
#define AARON_ARG_H

#include "config.h"

#define OPT_ACCEPTS_ARG_BIT 0x01
#define OPT_MULTIPLE_BIT 0x02

typedef int ( *opt_callback_t )( struct stOptInput *head );

struct stOptInput {
	const char *name;

	struct stOptInput *next;
};
struct stOpt {
	char sname;
	char *lname;

	bitfield_t flags;
	opt_callback_t callback;

	struct stOptInput *in_head,
	    *in_tail;

	struct stOpt *next;
};

extern int opt_add( char sn, const char *ln, bitfield_t f, opt_callback_t cb );
extern void opt_deinit();
extern int opt_parse( int c, const char *const *v, opt_callback_t incallback );

#endif
