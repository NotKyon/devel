#ifndef AARON_CONFIG_H
#define AARON_CONFIG_H

#ifndef __config_int_types__
#define __config_int_types__
#if defined( _MSC_VER )
#if _MSC_VER >= 1600
#define INC_STDINT 1
#else
#define MSC_STDINT 1
#endif
#elif defined( __INTEL_COMPILER ) || defined( __BORLANDC__ )
#define MSC_STDINT 1
#elif defined( __clang__ ) || defined( __GNUC__ )
#define INC_STDINT 1
#endif

#if defined( INC_STDINT )
#include <stdint.h>
#elif defined( MSC_STDINT )
typedef signed __int8 int8_t;
typedef signed __int16 int16_t;
typedef signed __int32 int32_t;
typedef signed __int64 int64_t;
typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;
#else
typedef signed char int8_t;
typedef signed short int16_t;
typedef signed int int32_t;
typedef signed long long int64_t;
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;
#endif

#ifndef bitfield_t_defined
#define bitfield_t_defined
typedef uint32_t bitfield_t;
#endif
#endif

#endif
