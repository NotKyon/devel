#ifndef TASKSHARE_CONFIG_H
#define TASKSHARE_CONFIG_H

#if _MSC_VER > 1000
# pragma once
#endif

#if defined(EE_CONF_USE_PTHREAD) && !defined(TASK_CONF_USE_PTHREAD)
# define TASK_CONF_USE_PTHREAD EE_CONF_USE_PTHREAD
#endif

#undef __USE_WIN32_THREADS__
#if _WIN32 && !TASK_CONF_USE_PTHREAD
# define __USE_WIN32_THREADS__ 1

# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#else
# define __USE_WIN32_THREADS__ 0

# ifndef TASK_CONF_USE_PTHREAD
#  define TASK_CONF_USE_PTHREAD 1
# elif !TASK_CONF_USE_PTHREAD
#  error "Configured to not use pthread but only pthread available."
# endif

# ifndef _GNU_SOURCE
#  define _GNU_SOURCE 1
# endif
# include <sched.h>
# include <pthread.h>

# include <cstdio>
# include <cstdlib>
# include <cstring>
#endif

//------------------------------------------------------------------------------

#define __TS_NS_INIT__ namespace task {
#define __TS_NS_FINI__ }

//------------------------------------------------------------------------------

__TS_NS_INIT__

#if _MSC_VER
typedef unsigned __int32       u32;
typedef unsigned __int64       u64;
#else
typedef unsigned int           u32;
typedef unsigned long long int u64;
#endif

#if __USE_WIN32_THREADS__
# define THREADPROCAPI WINAPI
typedef HANDLE thread_t;
typedef CRITICAL_SECTION lock_t;
typedef DWORD threadResult_t;
#else
# define THREADPROCAPI
typedef pthread_t thread_t;
typedef pthread_mutex_t lock_t;
typedef void *threadResult_t;
#endif

#ifndef TASK_CONF_JOBPROCAPI
# define TASK_CONF_JOBPROCAPI __stdcall
#endif

typedef threadResult_t(THREADPROCAPI *threadFunc_t)(void *);
typedef void(TASK_CONF_JOBPROCAPI *jobFunc_t)(void *);

// user functions
extern void *Alloc(size_t n);
extern void *Realloc(void *p, size_t n);
extern void *Free(void *p);

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
