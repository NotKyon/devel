#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#define MAX_TRIES 12
int gNumTries = 0;

void GetText(const char *prompt, char *dst, size_t dstn) {
	int numTries;

	numTries = 0;

	do {
		if (++numTries > MAX_TRIES) {
			fprintf(stderr, "exhausted tries...\n");
			exit(0);
		}

		printf("%s", prompt);
	} while(!fgets(dst, dstn, stdin));

	while(*dst > ' ')
		dst++;

	*dst = '\0';
}

const char *GetEntryFromIndex(int i) {
	static char buf[32];

	snprintf(buf, sizeof(buf) - 1, "%i", i + 1);
	buf[sizeof(buf) - 1] = '\0';

	return buf;
}

int main(int argc, char **argv) {
	char buf[32];
	int i;

	if (argc < 3) {
		fprintf(stderr, "menu prompt choice1 choiceN...\n");
		return 0;
	}

	printf("%s\n\n", argv[1]);

	for(i=2; i<argc; i++)
		printf("  %s. %s\n", GetEntryFromIndex(i - 2), argv[i]);

	do {
		printf("\n[Type the number for your choice, then press ENTER.]\n"
			"        -= ~To cancel, press Ctrl+C~ =-\n");
		GetText("] ", buf, sizeof(buf));

		for(i=2; i<argc; i++) {
			if (!strcmp(buf, GetEntryFromIndex(i - 2))) {
				printf("\n --> %s\n", argv[i]);
				exit(i - 1);
			}
		}

		fprintf(stderr, "\n! Incorrect entry ! -- Try again.\n");
		gNumTries++;
	} while(gNumTries <= MAX_TRIES);

	fprintf(stderr, "Too many incorrect tries. Terminating.\n");
	exit(0);
}
