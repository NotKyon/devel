// includes
#include <Axia/Vanilla/Vanilla.h>
#include <Axia/Vanilla/Internal.h>

// AXWindow: Constructor
AXWindow::AXWindow(HWND hwndRef)
{
}

// AXWindow: Destructor
AXWindow::~AXWindow()
{
}

// AXWindow: MakeMain: set a window as the main window
void AXWindow::MakeMain(void)
{
	::SetActiveWindow(m_hwndRef);
}

// AXWindow: IsMain: determine whether a window is the main window
bool AXWindow::IsMain(void)
{
	return (::GetActiveWindow()==m_hwndRef)?true:false;
}

// AXWindow: PerformClose: simulate a close title-button press
void AXWindow::PerformClose(void)
{
	::SendMessageA(m_hwndRef, WM_SYSCOMMAND, SC_CLOSE, 0);
}

// AXWindow: PerformZoom: simulate a maximize/restore title-button press
void AXWindow::PerformZoom(void)
{
	if (::IsZoomed(m_hwndRef))
		::SendMessageA(m_hwndRef, WM_SYSCOMMAND, SC_RESTORE, 0);
	else
		::SendMessageA(m_hwndRef, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
}

// AXWindow: PerformMinify: simulate a minimize/restore title-button press
void AXWindow::PerformMinify(void)
{
	if (::IsIconic(m_hwndRef))
		::SendMessageA(m_hwndRef, WM_SYSCOMMAND, SC_RESTORE, 0);
	else
		::SendMessageA(m_hwndRef, WM_SYSCOMMAND, SC_MINIMIZE, 0);
}

// AXWindow: Zoom: maximize/restore a window
void AXWindow::Zoom(void)
{
	if (::IsZoomed(m_hwndRef))
		::ShowWindow(m_hwndRef, SW_RESTORE);
	else
		::ShowWindow(m_hwndRef, SW_MAXIMIZE);
}

// AXWindow: Minify: minimize/restore a window
void AXWindow::Minify(void)
{
	if (::IsIconic(m_hwndRef))
		::ShowWindow(m_hwndRef, SW_RESTORE);
	else
		::ShowWindow(m_hwndRef, SW_MINIMIZE);
}

// AXWindow: IsZoomed: determine whether a window is zoomed
bool AXWindow::IsZoomed(void)
{
	return (::IsZoomed(m_hwndRef))?true:false;
}

// AXWindow: IsMinified: determine whether a window is minified
bool AXWindow::IsMinified(void)
{
	return (::IsIconic(m_hwndRef))?true:false;
}

// AXWindow: SetAutoClose: set whether to autoclose
void AXWindow::SetAutoClose(bool autoClose)
{
	m_autoClose = autoClose;
}

// AXWindow: GetAutoClose: determine whether a window will be autoclosed
bool AXWindow::GetAutoClose(void)
{
	return m_autoClose;
}
