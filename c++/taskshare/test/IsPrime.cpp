//
//	NOTE: This file should be included into the main source. NOT compiled
//	      separately.
//

//------------------------------------------------------------------------------
// PRIMALITY TEST

#if __STDC_WANT_SECURE_LIB__
# define snprintf sprintf_s
#endif

void TASK_CONF_JOBPROCAPI IsPrime_f(void *p) {
	//char buf[32];
	int isPrime = 0;
	int j;
	int i;

	i = *(int *)p;

	if (i<=1)
		isPrime = 0;
		//snprintf(buf, sizeof(buf) - 1, "%i isn't prime", i);
	else if(i==2)
		isPrime = 1;
		//snprintf(buf, sizeof(buf) - 1, "%i is prime", i);
	else {
		for(j=3; j<i; j+=2) {
			if (i%j == 0) {
				isPrime = 0;
				//snprintf(buf, sizeof(buf) - 1, "%i isn't prime", i);
				j = 0;
				break;
			}
		}

		if (j != 0)
			isPrime = 1;
			//snprintf(buf, sizeof(buf) - 1, "%i is prime", i);
	}

	*(int *)p = isPrime;

	//buf[sizeof(buf) - 1] = '\0';
	//printf("%s\n", buf);

	//ThreadPrintf("%s", buf);
}
