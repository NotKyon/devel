#include <errno.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>

typedef size_t vmword_t;
typedef unsigned char vmbyte_t;
typedef int vmvec4i_t __attribute__((vector_size(16)));
typedef float vmvec4f_t __attribute__((vector_size(16)));

typedef union {
	struct {
		unsigned is_r:1; /*true*/
		unsigned is_j:1; /*false*/
		unsigned opcode:4;
		unsigned rd:5;
		unsigned rs:5;
		unsigned rt:5;
		unsigned function:11;
	} r;
	struct {
		unsigned is_r:1; /*false*/
		unsigned is_j:1; /*false*/
		unsigned opcode:4;
		unsigned rd:5;
		unsigned rs:5;
		unsigned immediate:16;
	} i;
	struct {
		unsigned is_r:1; /*false*/
		unsigned is_j:1; /*true*/
		unsigned opcode:4;
		unsigned addr:26;
	} j;
	struct {
		unsigned is_r:1; /*true*/
		unsigned is_j:1; /*true*/
		unsigned opcode:4;
		unsigned rd:5;
		unsigned unused:21;
	} jr;
	vmword_t bits;
} vminstr_t;

typedef struct {
	vmword_t r[31];
	vmword_t pc;
	union {
		struct {
			bool compare:1;
			bool overflow:1;
			bool supervisor:1;
			bool usemmap:1;
		} bits;
		vmword_t set;
	} flags;
#if USE_PTHREADS
	pthread_t thread;
	vmstate_t *vm;
#endif
} vmcore_t;
typedef struct {
	vmword_t numcores;
	vmcore_t *cores;
	vmword_t mmap; /*memory map*/
	vmword_t lockedcore; /*(vmword_t)-1 for "none"*/
	size_t ramsize;
	union {
		void *p;
		vmbyte_t *bytes;
		vmword_t *words;
		vminstr_t *instr;
	} ram;
	void *(*memoryfn)(void *p, size_t n);
} vmstate_t;

#if USE_BUILTIN_MEMORY_FN
static void *vm_memory(void *p, size_t n) {
	return !p&&n ? malloc(n) : p&&n ? realloc(p, n) : (free(p), (void *)0);
}
#endif

#if USE_PTHREADS
static void *vm_thread(void *arg) {
	vmcore_t *core;
	vmstate_t *vm;

	core = (vmcore_t *)arg;
	if (!core)
		return (void *)1;

	vm = core->vm;

	/*
	 * TODO: Run the core!
	 */

	return (void *)0;
}
#endif

vmstate_t *vm_new(size_t numcores, size_t numwords,
void *(*memoryfn)(void *p, size_t n)) {
	vmstate_t *vm;
	size_t i, j;

	if (!numcores) {
#if __linux__||__linux||linux
		if ((numcores = sysconf(_SC_NPROCESSORS_ONLN)) < 1)
			numcores = 1;
#elif _WIN32
		SYSTEM_INFO si;

		memset(&si, 0, sizeof(si));
		GetSystemInfo(&si);

		numcores = (size_t)si.dwNumberOfProcessors;
#else
		numcores = 1;
#endif
	}

#if USE_BUILTIN_MEMORY_FN
	if (!memoryfn)
		memoryfn = vm_memory;
#else
	if (!memoryfn) {
		errno = EFAULT;
		return (vmstate_t *)0;
	}
#endif

	if (numwords < 16) {
		errno = EINVAL;
		return (vmstate_t *)0;
	}

	if (!(vm = memoryfn((void *)0, sizeof(*vm)))) {
		if (!errno)
			errno = ENOMEM;
		return (vmstate_t *)0;
	}

	vm->memoryfn = memoryfn;

	vm->numcores = numcores;
	if (!(vm->cores = memoryfn((void *)0, sizeof(vmcore_t)*numcores))) {
		memoryfn((void *)vm, 0);
		if (!errno)
			errno = ENOMEM;
		return (vmstate_t *)0;
	}

	for(i=0; i<numcores; i++) {
		for(j=0; j<sizeof(vm->r)/sizeof(vm->r[0]); j++)
			vm->cores[i].r[j] = 0;

		vm->cores[i].pc = 0;
		vm->cores[i].flags.set = 0;
		vm->cores[i].flags.bits.supervisor = true;
	}

	vm->mmap = 0;
	vm->lockedcore = (vmword_t)-1;

	vm->ramsize = numwords;
	if (!(vm->ram.p = memoryfn((void *)0, numwords*sizeof(vmword_t)))) {
		memoryfn((void *)vm->cores, 0);
		memoryfn((void *)vm, 0);
		if (!errno)
			errno = ENOMEM;
		return (vmstate_t *)0;
	}

#if USE_PTHREADS
	for(i=0; i<vm->numcores; i++) {
		vm->cores[i].vm = vm;
		pthread_create(&vm->cores[i].thread, (const pthread_attr_t *)0,
			vm_thread, (void *)&vm->cores[i]);
	}
#endif

	return vm;
}
vmstate_t *vm_delete(vmstate_t *vm) {
	size_t i;

	if (!vm)
		return (vmstate_t *)0;

#if USE_PTHREADS
	for(i=0; i<vm->numcores; i++)
		pthread_join(vm->cores[i].thread, (void **)0);
#endif

	memoryfn(vm->ram.p, 0);
	memoryfn((void *)vm->cores, 0);
	memoryfn((void *)vm, 0);

	return (vmstate_t *)0;
}

