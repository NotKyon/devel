#ifndef AXTEK_THREADING_PUBLIC_H
#define AXTEK_THREADING_PUBLIC_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

#include "CPU.h"
#include "CLock.h"
#include "CTask.h"
#include "CEvent.h"
#include "CSignal.h"
#include "CThread.h"
#include "Atomics.h"
#include "CSemaphore.h"
#include "CCommandBuffer.h"

#endif
