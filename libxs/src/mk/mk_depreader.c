#include "mk_depreader.h"
#include "mk_dep.h"

enum {
	kDepTok_EOF = 0,
	kDepTok_Colon = ':',

	__kDepTok_Start__ = 256,

	kDepTok_Ident
};

static char dep_read(xs_buffer_t buf, char *cur, char *look) {
	/*
	 * ! This is based on old code.
	 */

	assert(buf != (xs_buffer_t)0);
	assert(cur != (char *)0);
	assert(look != (char *)0);

	*cur = XsPullBuffer(buf);
	*look = XsPeekBuffer(buf);

	return *cur;
}

static int dep_lex(xs_buffer_t buf, char *dst, size_t dstn, char *cur,
char *look) {
	size_t i;

	assert(buf != (xs_buffer_t)0);
	assert(dst != (char *)0);
	assert(dstn > 1);

	dst[i = 0] = 0;

	if (!*cur) {
		do {
			if (!dep_read(buf, cur, look))
				return kDepTok_EOF;
		} while(*cur <= ' ');
	}

	if (*cur==':') {
		dst[0] = ':';
		dst[1] = 0;

		*cur = 0; /* force 'cur' to be retrieved again, else infinite loop */

		return kDepTok_Colon;
	}

	while(1) {
		if (*cur=='\\') {
			/*
			 *	! Original code expected that '\r' was possible, hence this
			 *	  setup was needed to make this portion easier. This needs to be
			 *	  fixed later.
			 */
			if (*look=='\n') {
				do {
					if (!dep_read(buf, cur, look))
						break;
				} while(*cur <= ' ');

				continue;
			}

			if (*look==' ') {
				dst[i++] = dep_read(buf, cur, look);
				dep_read(buf, cur, look); /*eat the space*/
				continue;
			}

			*cur = '/'; /*correct path reads*/
		} else if(*cur==':') {
			if (*look<=' ') {
				dst[i] = 0;
				return kDepTok_Ident; /*leave 'cur' for the next call*/
			}
		} else if(*cur<=' ') {
			dst[i] = 0;
			*cur = 0; /*force read on next call*/
			return kDepTok_Ident;
		}

		if (i==dstn) {
			XsErrorBuffer(buf, "Overflow detected");
			exit(EXIT_FAILURE);
		}

		dst[i++] = *cur;

		dep_read(buf, cur, look);
	}
}

int ReadDeps(const char *filename) {
	xs_buffer_t buf;
	dep_t dep;
	char lexan[1024], ident[1024], cur, look;
	int tok;

	if (!(buf = XsLoadBuffer(buf, filename)))
		return 0;

	lexan[0] = 0;
	cur = 0;
	look = 0;

	dep_read(buf, &cur, &look);

	dep = (dep_t)0;

	do {
		tok = dep_lex(buf, lexan, sizeof(lexan), &cur, &look);

		if (tok==kDepTok_Colon) {
			if (ident[0]) {
				dep = NewDependency(ident);
				ident[0] = 0;
			}

			continue;
		}

		if (dep && ident[0]) {
			PushDependency(dep, ident);
			ident[0] = 0;
		}

		if (tok==kDepTok_Ident)
			XsStrCpy(ident, sizeof(ident), lexan);
	} while(tok != kDepTok_EOF);

	XsDeleteBuffer(buf);
	return 1;
}
