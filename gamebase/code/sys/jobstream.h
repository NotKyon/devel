#ifndef JOBSTREAM_H
#define JOBSTREAM_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "threadconf.h"
#include "lock.h"

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

#define TASK_JOBSTREAM_VERSION 4

#if !TASK_CONF_BUFFER_SIZE
# undef  TASK_CONF_BUFFER_SIZE
# define TASK_CONF_BUFFER_SIZE 4096
#endif

typedef struct jobToken_s {
	union { gb_jobFunc_t f; void *p; } fn;
	void *p;
} jobToken_t;
typedef struct jobStream_s {

	jobToken_t buffer[TASK_CONF_BUFFER_SIZE];

	volatile gb_u32_t numStreamers;

	volatile gb_u32_t insertionIndex;
	volatile gb_u32_t streamingIndex;
	volatile gb_u32_t numPendingJobs;

#if !defined(TASK_CONF_LOCKFREE)
# define TASK_CONF_LOCKFREE 1
#endif
#if !TASK_CONF_LOCKFREE
	lock_t lock;
#endif
} jobStream_t;

GB_INLINE void InitStream(jobStream_t *q);
GB_INLINE void FiniStream(jobStream_t *q);

GB_INLINE void EnqueueToken(jobStream_t *q, const jobToken_t *t);
GB_INLINE void Schedule(jobStream_t *q, gb_jobFunc_t fn, void *p);
GB_INLINE void ScheduleTermination(jobStream_t *q);

GB_INLINE void Streamer(jobStream_t *q);

GB_INLINE void SyncStream(jobStream_t *q);
GB_INLINE void SyncStreamActive(jobStream_t *q);

GB_INLINE void InitStream(jobStream_t *q) {
	memset((void *)q->buffer, 0, sizeof(q->buffer));
	q->numPendingJobs = 0;
	q->insertionIndex = 0;
	q->streamingIndex = 0;
	q->numStreamers   = 0;
#if !TASK_CONF_LOCKFREE
	InitLock(&q->lock);
#endif
}
GB_INLINE void FiniStream(jobStream_t *q) {
	ScheduleTermination(q);
	/*fflush(stdout);*/
	while(q->numStreamers > 0)
		gbYield();
#if !TASK_CONF_LOCKFREE
	FiniLock(&q->lock);
#endif
}

GB_INLINE void EnqueueToken(jobStream_t *q, const jobToken_t *t) {
	gb_u32_t index;

	if (t->fn.f==(gb_jobFunc_t)0)
		return;

#if !TASK_CONF_LOCKFREE
	Lock(&q->lock);
		q->numPendingJobs++;

		index = q->insertionIndex++ % TASK_CONF_BUFFER_SIZE;
	Unlock(&q->lock);

	while(q->buffer[index].fn.p != (void *)0)
		gbYield();

	Lock(&q->lock);
		q->buffer[index].p  = t->p   ;
		q->buffer[index].fn = t->fn.f;
	Unlock(&q->lock);
#else
	/* indicate an extra job is available */
	gbAtomicIncrement32(&q->numPendingJobs);

	/* find the next index */
	index = gbAtomicIncrement32(&q->insertionIndex) % TASK_CONF_BUFFER_SIZE;

	/* temporary value indicating "do not run until parameter is nonzero" */
	while(1) {
		if (gbAtomicExchangeIfEqualPtr(&q->buffer[index].fn.p,
		(void *)3, (void *)0)==(void *)3)
			break;
	}

	/* set the parameter first, then the function pointer */
	gbAtomicExchangePtr(&q->buffer[index].p  , t->p   );
	gbAtomicExchangePtr(&q->buffer[index].fn.p, t->fn.p);
#endif
}
GB_INLINE void Schedule(jobStream_t *q, gb_jobFunc_t fn, void *p) {
	jobToken_t t;

	t.fn.f = fn;
	t.p  = p ;

	EnqueueToken(q, &t);
}
GB_INLINE void ScheduleTermination(jobStream_t *q) {
#if !TASK_CONF_LOCKFREE
	jobToken_t t;
#endif
	gb_u32_t i;

#if !TASK_CONF_LOCKFREE
	t.fn.f.p = (void *)1;
	t.p   = (void *)0;

	Lock(&q->lock);
	for(i=0; i<TASK_CONF_BUFFER_SIZE; i++)
		q->buffer[i] = t;
	Unlock(&q->lock);
#else
	for(i=0; i<TASK_CONF_BUFFER_SIZE; i++)
		gbAtomicExchangePtr(&q->buffer[i].fn.p, (void *)1);
#endif
}

GB_INLINE void Streamer(jobStream_t *q) {
	gb_jobFunc_t fn;
	gb_u32_t index;

#if !TASK_CONF_LOCKFREE
	Lock(&q->lock);
#endif
	/*printf("++streamers\n");*/
	/*q->numStreamers++;*/
	gbAtomicIncrement32(&q->numStreamers);
#if !TASK_CONF_LOCKFREE
	/*Unlock(&q->lock);*/
#endif

	do {
#if !TASK_CONF_LOCKFREE
		/*Lock(&q->lock);*/
			index = q->streamingIndex++ % TASK_CONF_BUFFER_SIZE;
		Unlock(&q->lock);
#else
		index = gbAtomicIncrement32(&q->streamingIndex) % TASK_CONF_BUFFER_SIZE;
#endif

		while(q->buffer[index].fn.p==(void *)0)
			gbYield();

		if (q->buffer[index].fn.p==(void *)1)
			break;

		if (q->buffer[index].fn.p==(void *)2)
			continue;

#if TASK_CONF_LOCKFREE
		/* parameter isn't ready yet? */
		while(gbAtomicReadPtr(&q->buffer[index].fn.p)==(void *)3)
			gbYield();
#endif
		fn = q->buffer[index].fn.f;
		gbAtomicExchangePtr(&q->buffer[index].fn.p, (void *)2);

		fn(q->buffer[index].p);

		/*NOTE: no need to set 'p'*/
#if !TASK_CONF_LOCKFREE
		Lock(&q->lock);
			q->buffer[index].fn.p = (void *)0;
			q->numPendingJobs--;
		/*Unlock(&q->lock);*/
#else
		gbAtomicExchangePtr(&q->buffer[index].fn.p, (void *)0);
		gbAtomicDecrement32(&q->numPendingJobs);
#endif
	} while(1);

	/*
	 *	TODO: This lock is unnecessary; remove it.
	 */
#if !TASK_CONF_LOCKFREE
	Lock(&q->lock);
#endif
	/*printf("--streamers\n");*/
	gbAtomicDecrement32(&q->numStreamers);
#if !TASK_CONF_LOCKFREE
	Unlock(&q->lock);
#endif
}

GB_INLINE void SyncStream(jobStream_t *q) {
	while(q->numPendingJobs)
		gbYield();
}
GB_INLINE void SyncStreamActive(jobStream_t *q) {
	while(q->numPendingJobs) {
	}
}

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
