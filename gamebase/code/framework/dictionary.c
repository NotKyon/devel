#include <gamebase.h>

static int GenerateConvmap(int dst[256], const char *allowed) {
	int i, j, k;

	for(i=0; i<256; i++)
		dst[i] = -1;

	for(i=0; allowed[i]; i++)
		dst[(unsigned char)allowed[i]] = i;

	for(j=0; j<i; j++) {
		for(k=j+1; k<i; k++) {
			if (allowed[j]==allowed[k])
				return -1; /*duplicate entries*/
		}
	}

	return i;
}
static void DeleteEntries(gb_dictionary_t *dict, gb_entry_t *entries) {
	int i;

	if (!entries)
		return;

	for(i=0; i<dict->numEntries; i++) {
		if (entries[i].entries)
			DeleteEntries(dict, entries[i].entries);
	}

	gbMemFree((void *)entries);
}
static gb_entry_t *FindFromEntry(gb_dictionary_t *dict, gb_entry_t *entries,
const char *str, int create) {
	size_t n;
	int i;

	i = dict->convmap[*(unsigned char *)str++];
	if (i == -1)
		return (gb_entry_t *)NULL; /*invalid character*/

	/* if there's more to the string... */
	if (*str) {
		/* generate the entries if they're not there */
		if (!entries[i].entries) {
			if (create) {
				n = sizeof(gb_entry_t)*dict->numEntries;

				entries[i].entries = (gb_entry_t *)gbMemAllocZero(n);
			}

			if (!entries[i].entries)
				return (gb_entry_t *)NULL;
		}

		/* continue the search (recursive!) */
		return FindFromEntry(dict, entries[i].entries, str, create);
	}

	/* otherwise this is the last point */
	return &entries[i];
}

int gbInitDict(gb_dictionary_t *dict, const char *allowed) {
	size_t n;

	dict->numEntries = GenerateConvmap(dict->convmap, allowed);
	if (dict->numEntries < 1)
		return 0;

	n = sizeof(gb_entry_t)*dict->numEntries;

	dict->entries = (gb_entry_t *)gbMemAllocZero(n);
	if (!dict->entries)
		return 0;

	return 1;
}
void gbFiniDict(gb_dictionary_t *dict) {
	DeleteEntries(dict, dict->entries);
	dict->entries = (gb_entry_t *)NULL;

	dict->numEntries = 0;
}

gb_entry_t *gbFindEntry(gb_dictionary_t *dict, const char *str) {
	return FindFromEntry(dict, dict->entries, str, 0);
}
gb_entry_t *gbLookupEntry(gb_dictionary_t *dict, const char *str) {
	return FindFromEntry(dict, dict->entries, str, 1);
}

gb_entry_t *gbNewEntry(gb_dictionary_t *dict, const char *str, void *p) {
	gb_entry_t *entry;

	entry = gbLookupEntry(dict, str);
	if (!entry)
		return (gb_entry_t *)NULL;

	if (entry->p)
		return (gb_entry_t *)NULL;

	entry->p = p;
	return entry;
}
gb_entry_t *gbSetEntry(gb_dictionary_t *dict, const char *str, void *p) {
	gb_entry_t *entry;

	entry = gbFindEntry(dict, str);
	if (!entry)
		return (gb_entry_t *)NULL;

	entry->p = p;
	return entry;
}
void *gbGetEntry(gb_dictionary_t *dict, const char *str) {
	gb_entry_t *entry;

	entry = gbFindEntry(dict, str);
	if (!entry)
		return (void *)NULL;

	return entry->p;
}
