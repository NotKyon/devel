//
//	The stat system extends the existing file system commands by providing a way
//	to more efficiently collect information on a file, including information
//	that was previously inaccessible. Such as last modified time.
//
//	The last modified time is useful for determining whether certain media
//	should be reloaded because it was modified on-disk. For example, say you
//	have a shader viewer. Chances are someone might be using it to debug their
//	shaders. If they modify the shader, they probably want to reload it to see
//	their modifications take place. By tracking the modified time you can check
//	whether the shader needs to be reloaded or not.
//
[DefineVars=True]
Include "../Include/NukeExt.nb"

LoadNukeExt "../NukeExt.dll"

// We'll display the number of stats we can have open at a time.
// This is a plugin-defined value.
Print "Max Stat Files: " + CountMaxStatFiles()

// Let's get information about this file.
If Not StatFile("02-Stat.nb")
	Print "Unable to stat 02-Stat.nb!"
	SetConsoleWaitForKey
	End
EndIf
Print "02-Stat.nb:"
Print "  Created Time      " + StatCreatedTime()
Print "  Accessed Time     " + StatAccessedTime()
Print "  Modified Time     " + StatModifiedTime()
Print "  Is Directory?     " + StatIsDirectory()
Print "  Is File?          " + StatIsFile()

// Now compare that file to 01-Time.nb
// (Because we're comparing, we need to stat to another location)
If Not StatFile("01-Time.nb", 1)
	Print "Unable to stat 01-Time.nb!"
	SetConsoleWaitForKey
	End
EndIf

// Compare the two
CompareTimes "created" , "02-Stat.nb", "01-Time.nb", StatCreatedTime (0), StatCreatedTime (1)
CompareTimes "accessed", "02-Stat.nb", "01-Time.nb", StatAccessedTime(0), StatAccessedTime(1)
CompareTimes "modified", "02-Stat.nb", "01-Time.nb", StatModifiedTime(0), StatModifiedTime(1)

// Done
Print "Press any key to exit."
SetConsoleWaitForKey
End

// This is just a helper function to compare times and print the analysis
Function CompareTimes(timeType$, a_name$, b_name$, a_time%, b_time%)
	If a_time < b_time
		Print a_name + " was " + timeType + " sooner than " + b_name + "."
		Return
	EndIf

	If a_time > b_time
		Print a_name + " was " + timeType + " later than " + b_name + "."
		Return
	EndIf

	Print a_name + " and " + b_name + " were " + timeType + " at the same time."
EndFunction
