#ifndef TASKSHARE_CPU_H
#define TASKSHARE_CPU_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

//------------------------------------------------------------------------------
// CPU INFORMATION

__TS_NS_INIT__

inline bool IsHyperthreadingAvailable() {
	//
	//	TODO: Fix this.
	//
#if __i386__||__x86_64__ || _M_IX86 || _M_IX64
	static bool didInit = false;
	static bool htAvail;

	if (!didInit) {
		u32 d;

# if __GNUC__ || __clang__
		//get feature set
		__asm__ __volatile__("cpuid" : "=d" (d) : "a" (1), "d" (0));
# else
		__asm {
			xor edx, edx
			mov eax, 1
			cpuid
			mov [d], edx
		};
# endif

		htAvail = ((d>>28) & 1);
		didInit = true;
	}

	return htAvail;
#else
	return false;
#endif
}
inline u32 GetCPUCoreCount() {
#if _WIN32
	SYSTEM_INFO si;

	memset(&si, 0, sizeof(si));
	GetSystemInfo(&si);

	return (u32)si.dwNumberOfProcessors;
#elif __linux__||__linux||linux
	int count;

	if ((count = sysconf(_SC_NPROCESSORS_ONLN)) < 1) {
		fprintf(stderr, "Warning: sysconf(_SC_NPROCESSORS_ONLN) failed!\n");
		fflush(stderr);

		count = 1;
	}

	return count;
#else
	return 1; //TODO
#endif
}
inline u32 GetCPUThreadCount() {
	return GetCPUCoreCount()*(1 + (u32)IsHyperthreadingAvailable());
}

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
