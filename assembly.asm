section .code

; get_sum test
global get_sum
global _get_sum
get_sum:
_get_sum:
	mov eax, [esp+8] ;x
	add eax, [esp+4] ;y
	retn ;eax = x + y

; void *x86_call(void(*fn)(), size_t n, void **p)
global x86_call
global _x86_call
x86_call:
_x86_call:
	;save registers
	mov dword [.Lesp], esp
	mov dword [.Lecx], ecx
	;use esi as the index (doesn't require a save)
	xor esi, esi ;index
	;store the parameters for looping
	mov ecx, [esp+8] ;parameter:n
	mov eax, [esp+4] ;parameter:p
	;for each parameter
.L0:
	;push the parameter to the stack
	push dword [eax + esi*4]
	;increment esi
	add esi, 0x00000001
	loop .L0
	;make the call
	call [esp+12] ;parameter:fn
	;restore
	mov esp, dword [.Lesp]
	mov ecx, dword [.Lecx]
	;return from the function call
	retn
.Lecx: dd 0
.Lesp: dd 0
