#include <cstdarg>
#include <cstddef>

namespace ee {

typedef signed char i8;
typedef signed short i16;
typedef signed int i32;
typedef signed long long int i64;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long int u64;
typedef float f32;
typedef double f64;

class IBase;

class IEngine;
class IPipeline;

class ILogMgr;
class ISysMgr;
class ITaskMgr;

}

//------------------------------------------------------------------------------

class ee::IBase {
public:
	IBase(): mDbgName(NULL), mRefCnt(1) {
	}
	virtual ~IBase() {
		if (mRefCnt != 0)
			EE_OBJ_DEBUG_LOG(mDbgName, "Reference count not zero.");
	}

	virtual void Grab() const { mRefCnt++; }
	virtual bool Drop() const {
		return --mRefCnt==0 ? ((delete this), true) : false;
	}

	virtual const char *GetDebugName() const { return mDbgName; }

protected:
	virtual void SetDebugName(const char *dbgName) {
		mDbgName = dbgName;
	}

	const char *mDbgName;
	mutable u32 mRefCnt;
};

//------------------------------------------------------------------------------

class ee::IEngine: public virtual IBase {
public:
	virtual void *Memory(void *p, size_t n, size_t a=0) const = 0;
	virtual IPipeline *Pipeline() const = 0;

	virtual ILogMgr *LogMgr() const = 0;
	virtual ISysMgr *SysMgr() const = 0;
	virtual ITaskMgr *TaskMgr() const = 0;
	virtual IGfxMgr *GfxMgr() const = 0;
};

class ee::IPipeline: public virtual IBase {
public:
	virtual u32 GetFrameID() const = 0;
	virtual void Sync(bool incFrameID=true) = 0;
};

//------------------------------------------------------------------------------

class ee::IGfxMgr: public virtual IBase {
public:
	virtual void AddDriver(IGfxDrv *gd) = 0;

	virtual u32 GetDriverCount() const = 0;
	virtual IGfxDrv *GetDriver(u32 index=0) const = 0;
};

class ee::ILogMgr: public virtual IBase {
public:
	virtual void PrintX(u32 targets, color_t color, const char *s) = 0;
	virtual void PrintLn(const char *s) = 0;

	virtual void ReportV(logType_t type, const char *file, u32 line,
		const char *func, const char *format, va_list args) = 0;

	virtual void SetVerbosity(ee::verbosity_t verbosity) = 0;
	virtual ee::verbosity_t GetVerbosity() const = 0;
};

class ee::ISysMgr: public virtual IBase {
public:
	virtual u32 GetPhysCPUCount() const = 0;
	virtual u32 GetPhysCoreCount(u32 cpu=0) const = 0;
	virtual u32 GetPhysThreadCount(u32 cpu=0) const = 0;

	virtual u32 GetPhysMemory() const = 0; //MB
	virtual u32 GetVirtMemory() const = 0; //MB
};

class ee::ITaskSystem: public virtual IBase {
public:
	virtual IScheduler *NewScheduler(u32 affinity=0, bool bg=true) = 0;
	virtual IScheduler *GetScheduler(u32 hwthread) = 0;

	virtual ITask *NewTask(ITask *prnt=NULL) = 0;

	virtual void Flush() = 0;
	virtual void Finish() = 0;
};

//------------------------------------------------------------------------------

class ee::IScheduler: public virtual IBase {
public:
	virtual ITask *NewTask(ITask *prnt=NULL) = 0;

	virtual void Flush() = 0;
	virtual void Finish() = 0;
};

class ee::ITask: public virtual IBase {
public:
	virtual void SetFunction(jobFunc_t fn, void *p=NULL) = 0;

	virtual void SetValue(void *p) = 0;
	virtual void *GetValue() const = 0;

	virtual void SetSignal(ITask *sigTask) = 0;
	virtual ITask *GetSignal() const = 0;

	virtual void SetParent(ITask *prnt) = 0;
	virtual ITask *GetParent() const = 0;

	virtual void Sync() = 0;
};

//------------------------------------------------------------------------------

class ee::IGfxDrv: public virtual IBase {
public:
	virtual bool DeferredContextsAvailable() const = 0;
	virtual IGfxCtx *NewContext() = 0;
	virtual IGfxCtx *GetContext() = 0;

	virtual IFrameBuffer *NewFrameBuffer() = 0;
};

class ee::IGfxBase: public virtual IBase {
public:
	IGfxBase(IGfxDrv *gd): IBase(), mGfxDrv(gd) {}
	virtual ~IGfxBase() {}

	virtual IGfxDrv *GfxDrv() const { return mGfxDrv; }

protected:
	IGfxDrv *mGfxDrv;
};

class ee::IGfxCtx: public virtual IGfxBase {
public:
	virtual void SetFrameBuffer(IFrameBuffer *fb) = 0;
	virtual IFrameBuffer *GetFrameBuffer() = 0;
};
