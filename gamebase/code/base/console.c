#if _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif
#include <gamebase.h>

#if _WIN32
static HANDLE g_hConErr;
static HANDLE g_hConOut;
static HANDLE g_hConIn;
#endif
static char g_title[256];
static gb_consoleColor_t g_textColor, g_backColor;

static gb_bool_t g_didInit = GB_FALSE;
static gb_bool_t g_conColDirty;

static void InitConsole() {
	if (GB_LIKELY(g_didInit))
		return;

#if _WIN32
	g_hConErr = GetStdHandle(STD_ERROR_HANDLE);
	g_hConOut = GetStdHandle(STD_OUTPUT_HANDLE);
	g_hConIn  = GetStdHandle(STD_INPUT_HANDLE);

	{
		DWORD mode;

		if (GetConsoleMode(g_hConIn, &mode))
			SetConsoleMode(g_hConIn, mode|ENABLE_ECHO_INPUT|ENABLE_LINE_INPUT);
	}
#endif

	g_title[0] = '\0';

	g_conColDirty = GB_FALSE;
#if _WIN32
	{
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		BOOL has;

		has = GetConsoleScreenBufferInfo(g_hConOut, &csbi);
		if (!has)
			has = GetConsoleScreenBufferInfo(g_hConErr, &csbi);

		if (has) {
			g_textColor = (csbi.wAttributes&0x0F)>>0;
			g_backColor = (csbi.wAttributes&0xF0)>>4;
		} else {
			g_textColor = kGBConCol_White;
			g_backColor = kGBConCol_Black;
		}
	}
#else
	g_textColor = kGBConCol_White;
	g_backColor = kGBConCol_Black;
#endif

	g_didInit = GB_TRUE;
}
static void ApplyColor() {
	InitConsole();

	if (!g_conColDirty)
		return;

#if _WIN32
	{
		WORD attrib;

		attrib = ((g_backColor&0x0F)<<4)|(g_textColor&0x0F);

		SetConsoleTextAttribute(g_hConErr, attrib);
		SetConsoleTextAttribute(g_hConOut, attrib);
		SetConsoleTextAttribute(g_hConIn , attrib);
	}
#endif

	g_conColDirty = GB_FALSE;
}

void gbSetConsoleTitle(const char *title) {
	InitConsole();

#if _WIN32
	SetConsoleTitle(title);
#endif
}
const char *gbGetConsoleTitle() {
	InitConsole();

#if _WIN32
	GetConsoleTitle(g_title, sizeof(g_title));
#endif

	return g_title;
}

void gbSetConsoleTextColor(gb_consoleColor_t conCol) {
	InitConsole();

	g_textColor = conCol;
	g_conColDirty = GB_TRUE;
}
void gbSetConsoleBackColor(gb_consoleColor_t conCol) {
	InitConsole();

	g_backColor = conCol;
	g_conColDirty = GB_TRUE;
}
gb_consoleColor_t gbGetConsoleTextColor() {
	InitConsole();

	return g_textColor;
}
gb_consoleColor_t gbGetConsoleBackColor() {
	InitConsole();

	return g_backColor;
}

const char *gbReadStdin() {
	static char buf[32768];

	ApplyColor();

	buf[0] = '\0';
#if _WIN32
	{
		DWORD numRead;

		ReadConsole(g_hConIn, buf, sizeof(buf), &numRead, NULL);
	}
#else
	fgets(buf, sizeof(buf), stdin);
#endif

	return buf;
}
void gbWriteStdout(const char *message) {
	ApplyColor();

#if _WIN32
	WriteFile(g_hConOut, message, strlen(message), NULL, NULL);
#else
	fputs(stdout, message);
#endif
}
void gbWriteStderr(const char *message) {
	ApplyColor();

#if _WIN32
	WriteFile(g_hConErr, message, strlen(message), NULL, NULL);
#else
	fputs(stderr, message);
#endif
}

void gbPrint(const char *message) {
	gbWriteStdout(message);
	gbWriteStdout("\n");
}
