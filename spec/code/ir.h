#ifndef IR_H
#define IR_H

#include "local.h"

enum {
	kIR_Error_None,

	kIR_Error_StackOverflow,
	kIR_Error_StackUnderflow
};

/* library */
typedef struct lib_s {
	struct ns_s *ns;

	/*
	 * TODO: Add configuration support here
	 */

	struct lib_s *prev, *next;
} lib_t;

/* namespace */
typedef struct ns_s {
	char *name;
	char *abrv;

	struct type_s *type_head, *type_tail;
	struct enum_s *enum_head, *enum_tail;
	struct func_s *func_head, *func_tail;
	struct iface_s *iface_head, *iface_tail;

	struct lib_s *lib;
	struct ns_s *prnt;
	struct ns_s *head, *tail;
	struct ns_s *prev, *next;
} ns_t;

/* typedef */
typedef struct type_s {
	char *name;
	char *from;

	struct ns_s *prnt;
	struct type_s *prev, *next;
} type_t;

/* enumeration item */
typedef struct item_s {
	char *name;
	int value;
} item_t;

/* enum */
typedef struct enum_s {
	char *name;

	size_t itemMax, itemNum;
	item_t *items;

	struct ns_s *prnt;
	struct enum_s *prev, *next;
} enum_t;

/* parameter */
typedef struct parm_s {
	char *name;
	char *type;
	char *def;
} parm_t;

/* function */
typedef struct func_s {
	char *name;
	char *returnType;

	size_t parmMax, parmNum;
	parm_t *parms;

	ns_t *ns;
	struct iface_s *iface;
	struct func_s **p_head, **p_tail;
	struct func_s *prev, *next;
} func_t;

/* interface */
typedef struct iface_s {
	char *name;
	struct iface_s *base;

	struct func_s *func_head, *func_tail;

	struct ns_s *prnt;
	struct iface_s *prev, *next;
} iface_t;

/* IR token */
typedef enum {
	kIR_TokType_Emit,
	kIR_TokType_EmitC,
	kIR_TokType_EmitCPP,

	kIR_TokType_Namespace,

	kIR_TokType_Typedefs,
	kIR_TokType_Enumerations,
	kIR_TokType_Functions,
	kIR_TokType_Interfaces
} tokenType_t;
typedef struct token_s {
	tokenType_t tokenType;
	union {
		char *emit;
		ns_t *ns;
	} data;
} token_t;

/* IR */
typedef struct ir_s {
	size_t tokenMax, tokenNum;
	token_t *tokens;
} ir_t;

/* -------------------------------------------------------------------------- */

/* general */
int GetIRError();
const char *GetIRErrorText(int error);

/* library */
lib_t *NewLib();
lib_t *DeleteLib(lib_t *lib);

void SetLibNamespace(lib_t *lib, ns_t *ns);
ns_t *GetLibNamespace(lib_t *lib);

lib_t *LibBefore(lib_t *lib);
lib_t *LibAfter(lib_t *lib);
lib_t *FirstLib();
lib_t *LastLib();

void SetActiveLib(lib_t *lib);
lib_t *GetActiveLib();

/* namespace */
ns_t *NewNamespace(ns_t *prnt);
ns_t *DeleteNamespace(ns_t *ns);

void SetNamespaceName(ns_t *ns, const char *name);
void SetNamespaceAbrv(ns_t *ns, const char *abrv);
const char *GetNamespaceName(ns_t *ns);
const char *GetNamespaceAbrv(ns_t *ns);

lib_t *GetNamespaceLib(ns_t *ns);

ns_t *GetNamespaceParent(ns_t *ns);

ns_t *FirstNamespace(ns_t *ns);
ns_t *LastNamespace(ns_t *ns);
ns_t *NamespaceBefore(ns_t *ns);
ns_t *NamespaceAfter(ns_t *ns);

void PushActiveNamespace();
void PopActiveNamespace();

void SetActiveNamespace(ns_t *ns);
ns_t *GetActiveNamespace();

/* typedef */
type_t *NewTypedef();
type_t *DeleteTypedef(type_t *type);

void SetTypedefName(type_t *type, const char *name);
void SetTypedefFrom(type_t *type, const char *from);
const char *GetTypedefName(type_t *type);
const char *GetTypedefFrom(type_t *type);

ns_t *GetTypedefNamespace(type_t *type);

type_t *TypedefBefore(type_t *type);
type_t *TypedefAfter(type_t *type);
type_t *FirstTypedef();
type_t *LastTypedef();

/* enum */
enum_t *NewEnum();
enum_t *DeleteEnum(enum_t *e);

void SetEnumName(enum_t *e, const char *name);
const char *GetEnumName(enum_t *e);

size_t CountEnumItems(enum_t *e);
size_t AddEnumItem(enum_t *e);
void SetEnumItemName(enum_t *e, size_t i, const char *name);
void SetEnumItemValue(enum_t *e, size_t i, int v);
const char *GetEnumItemName(enum_t *e, size_t i);
int GetEnumItemValue(enum_t *e, size_t i);

ns_t *GetEnumNamespace(enum_t *e);
enum_t *EnumBefore(enum_t *e);
enum_t *EnumAfter(enum_t *e);
enum_t *FirstEnum();
enum_t *LastEnum();

/* func */
func_t *NewFunc(iface_t *iface);
func_t *DeleteFunc(func_t *func);

void SetFuncName(func_t *func, const char *name);
void SetFuncReturnType(func_t *func, const char *returnType);
const char *GetFuncName(func_t *func);
const char *GetFuncReturnType(func_t *func);

size_t CountFuncParms(func_t *func);
size_t AddFuncParm(func_t *func);
void SetFuncParmName(func_t *func, size_t i, const char *name);
void SetFuncParmType(func_t *func, size_t i, const char *type);
void SetFuncParmDef(func_t *func, size_t i, const char *def);
const char *GetFuncParmName(func_t *func, size_t i);
const char *GetFuncParmType(func_t *func, size_t i);
const char *GetFuncParmDef(func_t *func, size_t i);

ns_t *GetFuncNamespace(func_t *func);
iface_t *GetFuncInterface(func_t *func);

func_t *FuncBefore(func_t *func);
func_t *FuncAfter(func_t *func);
func_t *FirstFunc();
func_t *LastFunc();

/* interface */
iface_t *NewInterface();
iface_t *DeleteInterface(iface_t *iface);

void SetInterfaceName(iface_t *iface, const char *name);
const char *GetInterfaceName(iface_t *iface);

void SetInterfaceBase(iface_t *iface, iface_t *base);
iface_t *GetInterfaceBase(iface_t *iface);

func_t *FirstInterfaceFunc(iface_t *iface);
func_t *LastInterfaceFunc(iface_t *iface);

ns_t *GetInterfaceNamespace(iface_t *iface);

iface_t *InterfaceBefore(iface_t *iface);
iface_t *InterfaceAfter(iface_t *iface);
iface_t *FirstInterface();
iface_t *LastInterface();

/* intermediate representation */
ir_t *NewIR();
ir_t *DeleteIR(ir_t *ir);

size_t CountIRTokens(ir_t *ir);
size_t AddIRToken(ir_t *ir);

void SetIRTokenType(ir_t *ir, size_t i, tokenType_t type);
void SetIRTokenEmit(ir_t *ir, size_t i, const char *emit);
void SetIRTokenNamespace(ir_t *ir, size_t i, ns_t *ns);

tokenType_t GetIRTokenType(ir_t *ir, size_t i);
const char *GetIRTokenEmit(ir_t *ir, size_t i);
ns_t *GetIRTokenNamespace(ir_t *ir, size_t i);

size_t AddIREmitToken(ir_t *ir, const char *emit);
size_t AddIREmitCToken(ir_t *ir, const char *emit);
size_t AddIREmitCPPToken(ir_t *ir, const char *emit);
size_t AddIRNamespaceToken(ir_t *ir, ns_t *ns);
size_t AddIRTokenWithType(ir_t *ir, tokenType_t type);

#endif
