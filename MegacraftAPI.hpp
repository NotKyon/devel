﻿//
//	MegaCraft API
//

// Shared stuff
namespace Ax {

	// common types
	typedef ::int8_t s8_t;
	typedef ::int16_t s16_t;
	typedef ::int32_t s32_t;
	typedef ::int64_t s64_t;
	typedef ::uint8_t u8_t;
	typedef ::uint16_t u16_t;
	typedef ::uint32_t u32_t;
	typedef ::uint64_t u64_t;
	typedef ::intptr_t sPtr_t;
	typedef s32_t sWord_t;
	typedef ::uintptr_t uPtr_t;
	typedef u32_t uWord_t;

	// common stuff (containers mostly)
	namespace Core {

		template< size_t _size_ >
		class StaticBuffer {
		};
	
		template< typename _type_, size_t _size_ >
		class StaticArray {
		};
		
		template< typename _type_ >
		class Array {
		};

		// non-dynamic (uses a static buffer) string with UTF-8 encoding
		template< size_t _size_ >
		class StaticString {
		public:
		};
		// dynamic UTF-8 encoding
		class String {
		public:
		};
		
		template< typename _type_ >
		class IntrusiveList {
		public:
			class Node {
			};
		};
		
		template< typename _type_ >
		class IntrusiveBinaryTree {
		public:
			class Node {
			};
		};
		
		template< typename _type_ >
		class IntrusiveDictionary {
		public:
			class Node {
			};
		};
		
		template< typename _type_ >
		class Factory {
		public:
			class Node {
			};
		};
		template< typename _type_ >
		class Resource: public Factory< _type_ >::Node {
		};
		
		class Allocator {
		};
		
		class Allocator_StaticLinear {
		};
		class Allocator_StaticFixed {
		};
		class Allocator_StaticDeque {
		};
		
		class DebugSystem {
		};
		
	}

	// mathematical stuff
	namespace Math {
	
		constexpr float FLT_PI = 3.14159265358979323846f;
		constexpr double DBL_PI = 3.141592653589793238462643383279502884197169;

		constexpr float FLT_TAU = FLT_PI*2.0f;
		constexpr double DBL_TAU = DBL_PI*2.0;
		
		constexpr float FLT_HPI = FLT_PI*0.5f;
		constexpr double DBL_HPI = DBL_PI*0.5;

		namespace _Detail {

			static const u32_t __INF32 = 0x7F800000;
			static const u64_t __INF64 = 0x7F80000000000000;
			static const u32_t __NAN32 = 0x7FFFFFFF;
			static const u64_t __NAN64 = 0x7FFFFFFFFFFFFFFF;

		}
		
		constexpr float FLT_INF = *( float * )&_Detail::__INF32;
		constexpr double DBL_INF = *( double * )&_Detail::__INF64;
		constexpr float FLT_NAN = *( float * )&_Detail::__NAN32;
		constexpr double DBL_NAN = *( double * )&_Detail::__NAN64;
	
		template< typename _type_, size_t _size_ >
		class VectorX {
		};
		template< typename _type_ >
		class Vector2: public VectorX< _type_, 2 > {
		};
		template< typename _type_ >
		class Vector3: public VectorX< _type_, 3 > {
		};
		template< typename _type_ >
		class Vector4: public VectorX< _type_, 4 > {
		};
		template< typename _type_ >
		class Quaternion {
		};
		template< typename _type_ >
		class EulerAngles {
		};
		
		template< typename _type_, size_t _rows_, size_t _columns_ >
		class MatrixX {
		};
		template< typename _type_ >
		class Matrix2: public MatrixX< _type_, 2, 2 > {
		};
		template< typename _type_ >
		class Matrix3: public MatrixX< _type_, 3, 3 > {
		};
		template< typename _type_ >
		class Matrix4: public MatrixX< _type_, 4, 4 > {
		};
		template< typename _type_ >
		class Matrix23: public MatrixX< _type_, 2, 3 > {
		};
		template< typename _type_ >
		class Matrix24: public MatrixX< _type_, 2, 4 > {
		};
		template< typename _type_ >
		class Matrix32: public MatrixX< _type_, 3, 2 > {
		};
		template< typename _type_ >
		class Matrix34: public MatrixX< _type_, 3, 4 > {
		};
		template< typename _type_ >
		class Matrix42: public MatrixX< _type_, 4, 2 > {
		};
		template< typename _type_ >
		class Matrix43: public MatrixX< _type_, 4, 3 > {
		};
	
	}
	
}

// All of MegaCraft's specific stuff
namespace Megacraft {

	// main engine functionality
	namespace Engine {
	
		// handles graphics
		class RenderSystem {
		public:
			// the specific rendering API being used
			enum class API {
				Null,
				OpenGLDesktop_Legacy,
				OpenGLDesktop_Modern
			};
			// rendering phase (used by Material::Stage)
			enum class Phase {
				// run before managing all other phases
				Initial,
				// run before entering the viewport phase
				PreViewport,
				// run before entering the camera phase
				PreCamera,
				// render normal geometry
				Camera,
				// run after the camera phase
				PostCamera,
				// render viewport objects
				Viewport,
				// run after the viewport objects
				PostViewport,
				// run after managing all other phases
				Final
			};
		};
		// handles audio
		class SoundSystem {
		};
		// handles input from various devices
		class InputSystem {
		};
		
		// a 3D transformation (can exist in a hierarchy)
		class EntityTransform {
		};
		// same as an entity transform, but only 2D with optimizations
		class SpriteTransform {
		};
		
		// render target (can be color and/or depth/stencil)
		class FrameBuffer {
		};
		// viewport from which to render
		class Viewport {
		};
		// rendering queue (used internally); one per viewport
		class RenderQueue {
		};
		
		// font system (for rendering)
		class Font {
		};
		// text system (for rendering)
		class Text {
		};

		// image data
		class Texture {
		};
		// information about how to render a surface (this is a shader usually)
		class Material {
		public:
			// material rendering stage - equivalent to a shader pass
			class Stage {
			public:
			};
		};
		
		// small collection of geometry sharing common properties
		class RenderSurface {
		};
		// collection of surfaces
		class RenderModel {
		};
		// 2d item for display on the screen; e.g., text, texture, ...
		class RenderSprite {
		};
		// lighting information (will affect the scene if activated)
		class RenderLight {
		};
		// an individual "view" to render from
		class RenderView {
		};

		// individual mixing system; e.g., bgm mixer and sfx mixer
		class SoundMixer {
		};
		// individual channels for each mixer; each channel can play one track
		class SoundChannel {
		};
		// sound data
		class SoundTrack {
		};
		// special sound effect (can be chained and applied to channels)
		class SoundEffect {
		};

		// entity component: emits sound from a specific location
		class SoundEmitter {
		};
		// entity component: listens to sound (affects mixing)
		class SoundListener {
		};

		// representation of a form of input
		class InputDevice {
		};
		// input device: keyboard
		class InputDevice_Keyboard: public virtual InputDevice {
		};
		// input device: mouse / touch screen
		class InputDevice_Mouse: public virtual InputDevice {
		};
		// input device: gamepad, joystick, ...
		class InputDevice_Controller: public virtual InputDevice {
		};

		// scriptable events system for entities
		class EntityController {
		};
		// renders an entity
		class EntityRenderer {
		};
		// entity renderer: default renderer (does default behavior)
		class EntityRenderer_Default {
		};

		// an individual scene
		class Scene {
		};
		// a _thing_ within the scene hierarchy
		class Entity {
		protected:
			EntityTransform *transform;
			SpriteTransform *spriteTransform;
			RenderModel *model;
			RenderSprite *sprite;
			RenderLight *light;
			RenderView *view;
			SoundEmitter *soundEmitter;
			SoundListener *soundListener;
			EntityController *controller;
			EntityRenderer *renderer;
		
		private:
			Entity *parent;
			IntrusiveList< Entity >::Link siblingLink;
			IntrusiveList< Entity > childList;
		};
		
		// console/configuration variable
		class CVar {
		};
		// command for the developer console (e.g., bindkey W walk_forward)
		class Command {
		};
		// developer console
		class DeveloperConsole {
		};
		
	}

	// MegaCraft's main game system (the fun part!)
	namespace Game {

		// collection of configurations (e.g., "crafting recipes")
		class Registry {
		};
		
		// score system for multiplayer
		class ScoreBoard {
		};
		
		// a single block type
		class VoxelBlock {
		};
		// collection of voxels
		class VoxelChunk {
		};
		// polygon representation of voxels
		class Geometry {
		};
		// converts voxel chunks into geometry
		class Tessellator {
		};

		// collection of player items
		class Inventory {
		};
		// single item type (name, use, etc)
		class Item {
		};
		// representation of an item in the world (spawned)
		class SpawnedItem {
		};

		// non-voxel entity that's part of the world
		class Prop {
		};

		// "living" entity (e.g., pig, player, bear, zombie, ...)
		class Actor {
		};
		// a non-player character (e.g., villager, wolf, etc)
		class Mob: public Actor {
		};
		// a player character (either _the_ player, or a network player)
		class Player: public Actor {
		};
		
	}

	// Editor system for MegaCraft
	namespace Editor {

		// a gizmo for editing the world
		class Gizmo {
		};
		// gizmo: translation component for moving something in the world
		class Gizmo_Translation: public virtual Gizmo {
		};
		// gizmo: rotation component for altering orientation
		class Gizmo_Rotation: public virtual Gizmo {
		};
		// gizmo: scaling component for altering scale
		class Gizmo_Scale: public virtual Gizmo {
		};
	
	}

}
