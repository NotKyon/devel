/*

	Manager System Performance Test

	Globals (Ax::TConstructBuffer) vs Getters (Ax::TManager)


	Results with g++ 5.1:

		-O0

		"Getter" beats "Global" by 652,533 microseconds (652 milliseconds)
		  "Global": 8,393,950 microseconds (8,393 milliseconds)
		  "Getter": 7,741,417 microseconds (7,741 milliseconds)

		-O1

		"Global" beats "Getter" by 3,401,755 microseconds (3,401 milliseconds)
		  "Global": 716,538 microseconds (716 milliseconds)
		  "Getter": 4,118,293 microseconds (4,118 milliseconds)

		-O2

		"Global" beats "Getter" by 3,089,800 microseconds (3,089 milliseconds)
		  "Global": 3 microseconds (0 milliseconds)
		  "Getter": 3,089,803 microseconds (3,089 milliseconds)

		-O3

		"Global" beats "Getter" by 162,081 microseconds (162 milliseconds)
		  "Global": 2 microseconds (0 milliseconds)
		  "Getter": 162,083 microseconds (162 milliseconds)

*/

#define AXPRINTF_IMPLEMENTATION
#define   AXTIME_IMPLEMENTATION

#include "axlib/ax_platform.h"
#include "axlib/ax_types.h"
#include "axlib/ax_time.h"
#include "axlib/ax_printf.h"
#include "axlib/ax_manager.hpp"

#include "ConstructBuffer.hpp"

#include <stdio.h>
#include <stdlib.h>

class MProcessor
{
friend class Ax::TConstructBuffer<MProcessor>;
public:
	static MProcessor &GetInstance();

	void Reset()
	{
		for( unsigned i = 0; i < NUM_REGS; ++i ) {
			m_Regs.u[ i ] = 0;
		}
	}
	void Print()
	{
		printf( "A=0x%.2X X=0x%.2X Y=0x%.2X\n",
			+m_Regs.u[ REG_A ], +m_Regs.u[ REG_X ], +m_Regs.u[ REG_Y ] );
	}

	void LdA( Ax::uint8 v ) { m_Regs.u[ REG_A ] = v; }
	void LdX( Ax::uint8 v ) { m_Regs.u[ REG_X ] = v; }
	void LdY( Ax::uint8 v ) { m_Regs.u[ REG_Y ] = v; }
	void TAX() { m_Regs.u[ REG_X ] = m_Regs.u[ REG_A ]; }
	void TAY() { m_Regs.u[ REG_Y ] = m_Regs.u[ REG_A ]; }
	void TXA() { m_Regs.u[ REG_A ] = m_Regs.u[ REG_X ]; }
	void TYA() { m_Regs.u[ REG_A ] = m_Regs.u[ REG_Y ]; }
	void SAX() { Swap( m_Regs.u[ REG_A ], m_Regs.u[ REG_X ] ); }
	void SAY() { Swap( m_Regs.u[ REG_A ], m_Regs.u[ REG_Y ] ); }
	void SXY() { Swap( m_Regs.u[ REG_X ], m_Regs.u[ REG_Y ] ); }
	void Add() { m_Regs.u[ REG_A ] = m_Regs.u[ REG_X ] + m_Regs.u[ REG_Y ]; }
	void Sub() { m_Regs.u[ REG_A ] = m_Regs.u[ REG_X ] - m_Regs.u[ REG_Y ]; }
	void MulU() { m_Regs.u[ REG_A ] = m_Regs.u[ REG_X ]*m_Regs.u[ REG_Y ]; }
	void MulS() { m_Regs.i[ REG_A ] = m_Regs.i[ REG_X ]*m_Regs.i[ REG_Y ]; }
	void DivU() { m_Regs.u[ REG_A ] = m_Regs.u[ REG_X ]/m_Regs.u[ REG_Y ]; }
	void DivS() { m_Regs.i[ REG_A ] = m_Regs.i[ REG_X ]/m_Regs.i[ REG_Y ]; }
	void ModU() { m_Regs.u[ REG_A ] = m_Regs.u[ REG_X ]%m_Regs.u[ REG_Y ]; }
	void ModS() { m_Regs.i[ REG_A ] = m_Regs.i[ REG_X ]%m_Regs.i[ REG_Y ]; }

private:
	enum { REG_A, REG_X, REG_Y, NUM_REGS };

	union {
		Ax::int8  i[ NUM_REGS ];
		Ax::uint8 u[ NUM_REGS ];
	}             m_Regs;

	MProcessor()
	{
		Reset();
	}
	~MProcessor()
	{
	}

	static void Swap( Ax::uint8 &a, Ax::uint8 &b ) { Ax::uint8 c = a; a = b; b = c; }
};

MProcessor &MProcessor::GetInstance()
{
	static MProcessor instance;
	return instance;
}

struct SGlob
{
	Ax::TConstructBuffer<MProcessor> CPU;

	void Init()
	{
		CPU.Construct();
	}
	void Fini()
	{
		CPU.Destruct();
	}
};

SGlob G;

static Ax::TManager< MProcessor > g_CPU;

void Op1()
{
	G.CPU->TXA();
	G.CPU->LdY( 2 );
	G.CPU->Add();
}
void Op2()
{
	g_CPU->TXA();
	g_CPU->LdY( 2 );
	g_CPU->Add();
}

static const unsigned kNumTests = 0x08000000;

Ax::uint64 Test1()
{
	Ax::CTimer Timer;

	unsigned n = kNumTests;
	while( n-- > 0 ) {
		Op1();
		Op1();
		Op1();
		Op1();
	}

	return Timer.GetElapsed();
}
Ax::uint64 Test2()
{
	Ax::CTimer Timer;

	unsigned n = kNumTests;
	while( n-- > 0 ) {
		Op2();
		Op2();
		Op2();
		Op2();
	}

	return Timer.GetElapsed();
}

int main( int argc, char **argv )
{
	for( int i = 1; i < argc; ++i ) {
		printf( "%s\n", argv[ i ] );
	}

	if( argc > 1 ) {
		printf( "\n" );
	}

	const Ax::uint64 uElapsed1 = Test1();
	const Ax::uint64 uElapsed2 = Test2();

	const char *const pszMethod1 = "Global";
	const char *const pszMethod2 = "Getter";

	const char *const pszWinner = uElapsed1 < uElapsed2 ? pszMethod1 : pszMethod2;
	const char *const pszLoser = uElapsed1 < uElapsed2 ? pszMethod2 : pszMethod1;

	const Ax::uint64 uMarginMic = uElapsed1 < uElapsed2 ? uElapsed2 - uElapsed1 : uElapsed1 - uElapsed2;
	const Ax::uint64 uMarginMil = uMarginMic/1000;

	axpf( "\"%s\" beats \"%s\" by %\'qu microseconds (%\'qu milliseconds)\n",
		pszWinner, pszLoser, uMarginMic, uMarginMil );
	axpf( "  \"%s\": %\'qu microseconds (%\'qu milliseconds)\n",
		pszMethod1, uElapsed1, uElapsed1/1000 );
	axpf( "  \"%s\": %\'qu microseconds (%\'qu milliseconds)\n",
		pszMethod2, uElapsed2, uElapsed2/1000 );

	printf( "\n" );

	return EXIT_SUCCESS;
}
