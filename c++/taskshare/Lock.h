#ifndef TASKSHARE_LOCK_H
#define TASKSHARE_LOCK_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

//------------------------------------------------------------------------------
// MUTUAL EXCLUSION

__TS_NS_INIT__

inline void InitLock(lock_t &M) {
#if __USE_WIN32_THREADS__
	InitializeCriticalSectionAndSpinCount(&M, 4000);
#else
	static pthread_mutexattr_t defAttr;
	static bool didInitAttr = false;

	if (!didInitAttr) {
		pthread_mutexattr_init(&defAttr);
		pthread_mutexattr_settype(&defAttr, PTHREAD_MUTEX_RECURSIVE);

		didInitAttr = true;
	}

	if (pthread_mutex_init(&M, &defAttr) != 0) {
		fprintf(stderr, "Error: %s:%u: pthread_mutex_init() fail\n",
			__FILE__, __LINE__);
		exit(EXIT_FAILURE);
	}
#endif
}
inline void FiniLock(lock_t &M) {
#if __USE_WIN32_THREADS__
	DeleteCriticalSection(&M);
#else
	pthread_mutex_destroy(&M);
#endif
	memset((void *)&M, 0, sizeof(M));
}

inline void Lock(lock_t &M) {
#if __USE_WIN32_THREADS__
	EnterCriticalSection(&M);
	//WaitForSingleObject(&M, INFINITE);
#else
	pthread_mutex_lock(&M);
#endif
}
inline bool TryLock(lock_t &M) {
#if __USE_WIN32_THREADS__
	return (bool)(true & TryEnterCriticalSection(&M));
#else
	if (!pthread_mutex_trylock(&M))
		return true;

	return false;
#endif
}
inline void Unlock(lock_t &M) {
#if __USE_WIN32_THREADS__
	LeaveCriticalSection(&M);
#else
	pthread_mutex_unlock(&M);
#endif
}

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
