void blah() {}
typedef void(*pfv_t)();

int N = 1;
int *A[] = { &N };
pfv_t B[] = { &blah, &blah, &blah };

const char *const p = "Hello";
const char *P[] = { p, p, p };

bool do_not_i( int x ) { return !x; }
bool do_not_p( const void *p ) { return !p; }

bool do_and_i( int x, int y ) { return x && y; }
bool do_and_p( const void *x, const void *y ) { return x && y; }

bool do_or_i( int x, int y ) { return x || y; }
bool do_or_p( const void *x, const void *y ) { return x || y; }
