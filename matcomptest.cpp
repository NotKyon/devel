/*

	NOTE: This has been tested to work in:

		- MSVC 2013 (no warnings on default VS settings)
		- Clang 3.1 ("anonymous structs are a GNU extension [-pedantic,-Wgnu]")
		- GCC 4.8.0 ("ISO C++ prohibits anonymous structs [-Wpedantic]")

*/


	template< typename tVectorType, unsigned int tNumVectors >
	struct matrix_components
	{
		tVectorType columns[ tNumVectors ];
	};
	template< typename tVectorType >
	struct matrix_components< tVectorType, 1 >
	{
		union
		{
			tVectorType columns[ 1 ];
			tVectorType x;
		};
	};
	template< typename tVectorType >
	struct matrix_components< tVectorType, 2 >
	{
		union
		{
			tVectorType columns[ 2 ];
			struct
			{
				tVectorType x;
				tVectorType y;
			};
		};
	};
	template< typename tVectorType >
	struct matrix_components< tVectorType, 3 >
	{
		union
		{
			tVectorType columns[ 3 ];
			struct
			{
				tVectorType x;
				tVectorType y;
				tVectorType z;
			};
		};
	};
	template< typename tVectorType >
	struct matrix_components< tVectorType, 4 >
	{
		union
		{
			tVectorType columns[ 4 ];
			struct
			{
				tVectorType x;
				tVectorType y;
				tVectorType z;
				tVectorType w;
			};
		};
	};

struct float3
{
	float x, y, z;
};
struct float3x3: public matrix_components< float3, 3 >
{
};

int main()
{
	float3x3 m;

	for( unsigned int i = 0; i < 3; ++i ) {
		m.columns[ i ].x = 0.0f;
		m.columns[ i ].y = 0.0f;
		m.columns[ i ].z = 0.0f;
	}

	m.x.x = 1.0f;
	m.y.y = 1.0f;
	m.z.z = 1.0f;

	return 0;
}
