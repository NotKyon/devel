          /*
        */
        /*
      */         //========================================//
      /*        // GCC-D3D11 - DIRECT3D 11 IN GCC (TEST)  // GCC_D3D11-1
    */         // (C) Aaron J. Miller, 2012              // GPLv3
    /*        //========================================//
  */
  /*
*/ //this software comes with absolutely no warranty//
#undef UNICODE
#undef _UNICODE
#define INITGUID 1
#include <windows.h>
#include <d3d11.h>
#include <dxgi.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>

/*
 * ==========================================================================
 *
 *	GLOBALS
 *
 * ==========================================================================
 */

/* configurable */
int g_posX, g_posY;
int g_resX, g_resY;

/* d3d11 function pointers and library */
HINSTANCE g_d3d11Lib = (HINSTANCE)0;
PFN_D3D11_CREATE_DEVICE_AND_SWAP_CHAIN dxCreateDeviceAndSwapChain = \
(PFN_D3D11_CREATE_DEVICE_AND_SWAP_CHAIN)0;

/* windowing/application */
HWND g_hwnd = (HWND)0;
bool g_isRunning;

/* d3d11 */
IDXGISwapChain *g_swapChain = (IDXGISwapChain *)0;
ID3D11Device *g_device = (ID3D11Device *)0;
D3D_FEATURE_LEVEL g_featureLevel;
ID3D11DeviceContext *g_context = (ID3D11DeviceContext *)0;
ID3D11RenderTargetView *g_backBuffer = (ID3D11RenderTargetView *)0;

/*
 * ==========================================================================
 *
 *	UTILITY FUNCTIONS
 *
 * ==========================================================================
 */

void ErrorV(const char *file, int line, const char *format, va_list args) {
	if (file) {
		fprintf(stderr, "[%s", file);
		if (line)
			fprintf(stderr, "(%i)", line);
		fprintf(stderr, "] ");
	}

	fprintf(stderr, "Error: ");
	vfprintf(stderr, format, args);
	fflush(stderr);
}
void Error(const char *file, int line, const char *format, ...) {
	va_list args;

	va_start(args, format);
	ErrorV(file, line, format, args);
	va_end(args);
}
void ErrorExit(const char *file, int line, const char *format, ...) {
	va_list args;

	va_start(args, format);
	ErrorV(file, line, format, args);
	va_end(args);

	exit(EXIT_FAILURE);
}

/*
 * NOTE: Key parsing is pretty inefficient, but it works. Should be fine?
 */
const char *CheckKey(const char *test, const char *key) {
	const char *eq;

	eq = strchr(test, '=');
	if (!eq)
		return (const char *)0;

	if (strncmp(key, test, eq - test) != 0)
		return (const char *)0;

	eq++;
	return eq;
}
bool ParseKey(const char *test, const char *key, int &var) {
	const char *value;

	value = CheckKey(test, key);
	if (!value)
		return false;

	var = atoi(value);
	return true;
}

const char *FeatureLevelString(D3D_FEATURE_LEVEL featureLevel) {
	/*
	D3D_FEATURE_LEVEL_9_1 = 0x9100
	D3D_FEATURE_LEVEL_9_3 = 0x9300
	D3D_FEATURE_LEVEL_10_0 = 0xa000
	D3D_FEATURE_LEVEL_11_0 = 0xb000
	*/
	switch(*(unsigned int *)&featureLevel) {
	case D3D_FEATURE_LEVEL_9_1:		return "Direct3D 9.0 (SM1)";
	case D3D_FEATURE_LEVEL_9_2:		return "Direct3D 9.0b (SM2)";
	case D3D_FEATURE_LEVEL_9_3:		return "Direct3D 9.0c (SM3)";
	case D3D_FEATURE_LEVEL_10_0:	return "Direct3D 10 (SM4)";
	case D3D_FEATURE_LEVEL_10_1:	return "Direct3D 10.1 (SM4)";
	case D3D_FEATURE_LEVEL_11_0:	return "Direct3D 11 (SM5)";
	case 0xb100:					return "Direct3D 11.1 (SM5)";
	default:
		break;
	}

	return "(unknown)";
}

/*
 * ==========================================================================
 *
 *	INITIALIZATION AND DEINITIALIZATION
 *
 * ==========================================================================
 */
LRESULT CALLBACK MsgProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp) {
	switch(msg) {
	case WM_CLOSE:
		if (MessageBoxA(hwnd, "Really quit?", "Quit?", MB_YESNO)==IDNO)
			return 0;

		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return DefWindowProcA(hwnd, msg, wp, lp);
}

/* this is the deinitialization function */
void fini() {
	if (g_backBuffer) {
		g_backBuffer->Release();
		g_backBuffer = (ID3D11RenderTargetView *)0;
	}

	if (g_context) {
		g_context->Release();
		g_context = (ID3D11DeviceContext *)0;
	}

	if (g_device) {
		g_device->Release();
		g_device = (ID3D11Device *)0;
	}

	if (g_swapChain) {
		g_swapChain->Release();
		g_swapChain = (IDXGISwapChain *)0;
	}

	if (g_d3d11Lib) {
		FreeLibrary(g_d3d11Lib);
		g_d3d11Lib = (HINSTANCE)0;
	}

	if (g_hwnd && IsWindow(g_hwnd)) {
		DestroyWindow(g_hwnd);
		g_hwnd = (HWND)0;
	}

	UnregisterClassA("gcc_d3d11", GetModuleHandleA(0));
}
/* this function runs before init() to process configurable options */
void args(int argc, char **argv) {
#define DEF_RESX 640
#define DEF_RESY 480
#define KEY(x,y) if(ParseKey(argv[i],x,y))continue
	int i;

	g_posX = -65536;
	g_posY = -65536;
	g_resX = DEF_RESX;
	g_resY = DEF_RESY;

	for(i=1; i<argc; i++) {
		KEY("ResX", g_resX);
		KEY("ResY", g_resY);
		KEY("PosX", g_posX);
		KEY("PosY", g_posY);

		Error(0,0, "Unknown command-line option: '%s'", argv[i]);
	}

	if (g_posX==-65536)
		g_posX = GetSystemMetrics(SM_CXSCREEN)/2 - g_resX/2;
	if (g_posY==-65536)
		g_posY = GetSystemMetrics(SM_CYSCREEN)/2 - g_resY/2;
#undef KEY
#undef DEF_RESY
#undef DEF_RESX
}
/* initialization function (sorry for the undoc) */
void init() {
#define SYM(x) if(!(*((void(**)())&dx##x)=(void(*)())GetProcAddress(\
g_d3d11Lib, "D3D11" #x)))ErrorExit(0,0,"Failed to get \"D3D11" #x "\"!")
#define HR(x) if(FAILED(x))ErrorExit(__FILE__,__LINE__,"Failed: " #x)
	DXGI_SWAP_CHAIN_DESC scd;
	ID3D11Texture2D *backBufferTex;
	D3D11_VIEWPORT vp;
	WNDCLASSEXA wc;
	DWORD exstyle, style;
	RECT rc;

	wc.cbSize = sizeof(wc);
	wc.style = 0;
	wc.lpfnWndProc = MsgProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = GetModuleHandleA(0);
	wc.hIcon = LoadIconA(0, IDI_APPLICATION);
	wc.hCursor = LoadCursorA(0, IDC_ARROW);
	wc.hbrBackground = 0;
	wc.lpszMenuName = 0;
	wc.lpszClassName = "gcc_d3d11";
	wc.hIconSm = LoadIconA(0, IDI_APPLICATION);
	if (!RegisterClassExA(&wc))
		ErrorExit(0,0, "RegisterClassExA() failed");

	atexit(fini);

	exstyle = 0;
	style = WS_OVERLAPPEDWINDOW;

	rc.left = g_posX;
	rc.top = g_posY;
	rc.right = rc.left + g_resX;
	rc.bottom = rc.top + g_resY;

	if (!AdjustWindowRectEx(&rc, style, FALSE, exstyle))
		ErrorExit(0,0, "AdjustWindowRectEx() failed");

	rc.right -= rc.left;
	rc.bottom -= rc.top;
/*
	rc.left = g_posX;
	rc.top = g_posY;
*/

	g_hwnd = CreateWindowExA(exstyle, "gcc_d3d11", "Direct3D 11 from GCC",
		style, rc.left, rc.top, rc.right, rc.bottom, (HWND)0, (HMENU)0,
		GetModuleHandleA(0), 0);
	if (!g_hwnd)
		ErrorExit(0,0, "CreateWindowExA() failed");

	g_d3d11Lib = LoadLibraryA("d3d11.dll");
	if (!g_d3d11Lib)
		ErrorExit(0,0, "Failed to load \"d3d11.dll\"");

	SYM(CreateDeviceAndSwapChain);

	scd.BufferDesc.Width = g_resX;
	scd.BufferDesc.Height = g_resY;
	scd.BufferDesc.RefreshRate.Numerator = 1;
	scd.BufferDesc.RefreshRate.Denominator = 60;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	scd.SampleDesc.Count = 1;
	scd.SampleDesc.Quality = 0;
	scd.BufferUsage = DXGI_USAGE_BACK_BUFFER|DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.BufferCount = 2;
	scd.OutputWindow = g_hwnd;
	scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	scd.Flags = 0;

	HR(dxCreateDeviceAndSwapChain((IDXGIAdapter *)0, D3D_DRIVER_TYPE_HARDWARE, \
(HMODULE)0, 0/*Flags*/, (const D3D_FEATURE_LEVEL *)0, 0/*FeatureLevels*/, \
D3D11_SDK_VERSION, &scd, &g_swapChain, &g_device, &g_featureLevel, &g_context));
/*
HRESULT WINAPI D3D11CreateDeviceAndSwapChain(
    __in_opt IDXGIAdapter* pAdapter,
    D3D_DRIVER_TYPE DriverType,
    HMODULE Software,
    UINT Flags,
    __in_ecount_opt( FeatureLevels ) CONST D3D_FEATURE_LEVEL* pFeatureLevels,
    UINT FeatureLevels,
    UINT SDKVersion,
    __in_opt CONST DXGI_SWAP_CHAIN_DESC* pSwapChainDesc,
    __out_opt IDXGISwapChain** ppSwapChain,
    __out_opt ID3D11Device** ppDevice,
    __out_opt D3D_FEATURE_LEVEL* pFeatureLevel,
    __out_opt ID3D11DeviceContext** ppImmediateContext );
*/

	/*
        virtual HRESULT STDMETHODCALLTYPE Present( 
            UINT SyncInterval,
            UINT Flags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetBuffer( 
            UINT Buffer,
            __in  REFIID riid,
            __out  void **ppSurface) = 0;
	*/

	printf("Feature Level: %s\n", FeatureLevelString(g_featureLevel));
	fflush(stdout);

	HR(g_swapChain->GetBuffer(0, IID_ID3D11Texture2D, (void **)&backBufferTex));

	HR(g_device->CreateRenderTargetView(backBufferTex, 0, &g_backBuffer));
	backBufferTex->Release();
	backBufferTex = (ID3D11Texture2D *)0;

	g_context->OMSetRenderTargets(1, &g_backBuffer, 0);

	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width = g_resX;
	vp.Height = g_resY;

	g_context->RSSetViewports(1, &vp);

	ShowWindow(g_hwnd, SW_SHOW);
	UpdateWindow(g_hwnd);
#undef HR
#undef SYM
}
/*
typedef struct DXGI_MODE_DESC
{
    UINT Width;
    UINT Height;
    DXGI_RATIONAL RefreshRate;
    DXGI_FORMAT Format;
    DXGI_MODE_SCANLINE_ORDER ScanlineOrdering;
    DXGI_MODE_SCALING Scaling;
} DXGI_MODE_DESC;
typedef struct DXGI_SWAP_CHAIN_DESC
    {
    DXGI_MODE_DESC BufferDesc;
    DXGI_SAMPLE_DESC SampleDesc;
    DXGI_USAGE BufferUsage;
    UINT BufferCount;
    HWND OutputWindow;
    BOOL Windowed;
    DXGI_SWAP_EFFECT SwapEffect;
    UINT Flags;
    } 	DXGI_SWAP_CHAIN_DESC;
*/

/*
 * ==========================================================================
 *
 *	MAIN PROGRAM CODE
 *
 * ==========================================================================
 */

int main(int argc, char **argv) {
	static const float color[4] = { 0.1f,0.3f,0.5f, 1.0f };
	MSG msg;

	args(argc, argv);
	init();

	g_isRunning = true;

	while(g_isRunning) {
		/*
		 * TODO: main code here
		 */
		g_context->ClearRenderTargetView(g_backBuffer, color);
		g_swapChain->Present(0, 0);

		/* message pump */
		while(PeekMessageA(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessageA(&msg);

			if (msg.message==WM_QUIT)
				g_isRunning = false;
		}
	}

	return EXIT_SUCCESS;
}
