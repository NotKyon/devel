//
//	Sometimes it is undesirable to use GBMain(). For example, perhaps some other
//	library you're using requires control over main(). Or, maybe you're making a
//	dynamic library. Whatever the case, you don't have to use GBMain() if you do
//	not wish to. Here's how you would initialize the engine without it.
//

// include Windows first on the platform
#if _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif

// do not define GB_INIT!
#include <gamebase.h>

// we want the macro for EXIT_SUCCESS
#include <stdlib.h>

// main function...
int main(int argc, char **argv) {
	// we need to initialize the engine
	gbAppInit(argc, argv);

	// let's say hello
	gbPrint("Hello, from main()!");

	// and now we're done
	gbAppFini();
	return EXIT_SUCCESS;
}

// and for Windows...
#if _WIN32
int WINAPI WinMain(HINSTANCE a, HINSTANCE b, LPSTR c, int d) {
	if (a||b||c||d) {} //unused

# if _MSC_VER||__INTEL_COMPILER
	return main(__argc, __argv);
# else
	return main(0, (char **)NULL);
# endif
}
#endif
