#ifndef EMBER_SCENE_H
#define EMBER_SCENE_H

#include "config.h"
#include "gc.h"
#include "entity.h"

#ifndef __scene_t_defined__
#define __scene_t_defined__ 1
typedef struct stScene *scene_t;
#endif

struct stScene {
	void *data[ 2 ];

	struct stEntity *head, *tail;

	struct stScene *prev, *next;
};

__em_extrn scene_t g_scene_head __em_nil(), g_scene_tail __em_nil();
__em_extrn gc_pool_t g_scene_pool __em_nil();

extern int sce_init( void );
extern void sce_deinit();
extern scene_t sce_alloc();
extern void sce_dealloc( scene_t scene );
extern scene_t sce_first();
extern scene_t sce_last();
extern scene_t sce_before( scene_t scene );
extern scene_t sce_after( scene_t scene );
extern entity_t sce_first_ent( scene_t scene );
extern entity_t sce_last_ent( scene_t scene );
extern void sce_set_data( scene_t scene, void *data );
extern void *sce_data( scene_t scene );
extern void sce_set_sys_data( scene_t scene, void *data );
extern void *sce_sys_data( scene_t scene );

#endif
