#ifndef HEADER_H
#define HEADER_H

#pragma once

template<class T> class TMyClass {
protected:
	T m_value;

public:
	static int g_count;

	inline TMyClass(T v=T(0)): m_value(v) { g_count++; }
	inline ~TMyClass() { g_count--; }

	inline void SetValue(T v) { m_value = v; }
	inline T GetValue() const { return m_value; }
};

template<class T> int TMyClass<T>::g_count = 0;

typedef TMyClass<int> MyClassI;
typedef TMyClass<float> MyClassF;

#endif
