#define USE_SSE							0
#define USE_SSE2						0

#define TASK_CONF_BUFFER_SIZE			8192
#define TASK_CONF_LOCKFREE				1
#define TASK_CONF_INTERLOCKED_ATOMICS	1

#define TASK_CONF_APPHANG				1

#define TASK_CONF_JOBPROCAPI			__stdcall

//
//	XXX: Using SSE causes NB to crash.
//

//------------------------------------------------------------------------------

#include "../c++/taskshare/versions/Task5.h"
#include "../c++/taskshare/TaskShare.h"
typedef task::jobFunc_t jobFunc_t;

#define _WIN32_IE 0x0600
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#include <shlobj.h>

#include <math.h>
#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <dirent.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <vector>
#include <string>

#if USE_SSE2 && !USE_SSE
# undef USE_SSE
# define USE_SSE 1
#endif
#if USE_SSE
# include <xmmintrin.h>
#endif
#if USE_SSE2
#endif

#define EXPORT extern "C" __declspec(dllexport) __stdcall

#if __GNUC__ || __clang__ || __INTEL_COMPILER
# define likely(x) (__builtin_expect(!!(x), 1))
# define unlikely(x) (__builtin_expect(!!(x), 0))
#else
# define likely(x) (x)
# define unlikely(x) (x)
#endif

#if _MSC_VER || __INTEL_COMPILER
typedef unsigned __int16 u16_t;
typedef unsigned __int32 u32_t;
typedef unsigned __int64 u64_t;
#else
# include <stdint.h>
typedef uint16_t u16_t;
typedef uint32_t u32_t;
typedef uint64_t u64_t;
#endif
typedef unsigned char u8_t;
typedef unsigned int uint_t;

typedef int bool_t;
#define YES 1
#define NO 0

#define TAU (3.141592653589793238462643383279502884197169*2)

#if _MSC_VER
# include <intrin.h>

# pragma intrinsic(_InterlockedIncrement)
# pragma intrinsic(_InterlockedDecrement)
# pragma intrinsic(_InterlockedExchange)
# pragma intrinsic(_InterlockedCompareExchange)
# pragma intrinsic(_InterlockedAnd)
# pragma intrinsic(_InterlockedOr)
# pragma intrinsic(_InterlockedXor)

# if _WIN64
#  pragma intrinsic(_InterlockedIncrement64)
#  pragma intrinsic(_InterlockedDecrement64)
#  pragma intrinsic(_InterlockedExchange64)
#  pragma intrinsic(_InterlockedCompareExchange64)
#  pragma intrinsic(_InterlockedAnd64)
#  pragma intrinsic(_InterlockedOr64)
#  pragma intrinsic(_InterlockedXor64)
# else
#  define _InterlockedIncrement64 InterlockedIncrement64
#  define _InterlockedDecrement64 InterlockedDecrement64
#  define _InterlockedExchange64 InterlockedExchange64
#  define _InterlockedCompareExchange64 InterlockedCompareExchange64
#  define _InterlockedAnd64 InterlockedAnd64
#  define _InterlockedOr64 InterlockedOr64
#  define _InterlockedXor64 InterlockedXor64
# endif

# if 0
#  pragma intrinsic(_InterlockedExchangePointer)
#  pragma intrinsic(_InterlockedCompareExchangePointer)
# else
#  define _InterlockedExchangePointer InterlockedExchangePointer
#  define _InterlockedCompareExchangePointer InterlockedCompareExchangePointer
# endif
#endif

//------------------------------------------------------------------------------

template<typename T>
class linked_list {
protected:
	struct Item {
		T *p;
		Item *prev, *next;
	};
	Item *m_head, *m_tail, *m_curr;

public:
	inline linked_list(): m_head((Item *)NULL), m_tail((Item *)NULL),
	m_curr((Item *)NULL) {
	}
	inline ~linked_list() {
		clear();
	}

	inline T *pop_front() {
		Item *x;
		T *p;
		
		if (!(x = m_head))
			return (T *)NULL;

		p = x->p;

		if (x->next)
			x->next->prev = (Item *)NULL;
		else
			m_tail = (Item *)NULL;
		m_head = x->next;

		if (m_curr==x)
			m_curr = x->next;

		delete x;
		return p;
	}
	inline T *pop_back() {
		Item *x;
		T *p;
		
		if (!(x = m_tail))
			return (T *)NULL;

		p = x->p;

		if (x->prev)
			x->prev->next = (Item *)NULL;
		else
			m_head = (Item *)NULL;
		m_tail = x->prev;

		if (m_curr==x)
			m_curr = x->prev;

		delete x;
		return p;
	}
	
	inline void push_front(T *p) {
		Item *x;

		x = new Item();
		
		x->p = p;

		x->prev = (Item *)NULL;
		if ((x->next = m_head) != (Item *)NULL)
			m_head->prev = x;
		else
			m_tail = x;
		m_head = x;
	}
	inline void push_back(T *p) {
		Item *x;
		
		x = new Item();
		
		x->p = p;
		
		x->next = (Item *)NULL;
		if ((x->prev = m_tail) != (Item *)NULL)
			m_tail->next = x;
		else
			m_head = x;
		m_tail = x;
	}

	inline void insert_before(T *p) {
		Item *x;

		if (!m_curr) {
			push_front(p);
			return;
		}
		
		x = new Item();
		
		x->p = p;
		
		x->next = m_curr;
		if ((x->prev = m_curr->prev) != (Item *)NULL)
			m_curr->prev->next = x;
		else
			m_head = x;
	}
	inline void insert_after(T *p) {
		Item *x;

		if (!m_curr) {
			push_front(p);
			return;
		}
		
		x = new Item();
		
		x->p = p;
		
		x->prev = m_curr;
		if ((x->next = m_curr->next) != (Item *)NULL)
			m_curr->next->prev = x;
		else
			m_tail = x;
	}

	inline void to_first() {
		m_curr = m_head;
	}
	inline void to_last() {
		m_curr = m_tail;
	}
	inline void to_previous() {
		m_curr = m_curr ? m_curr->prev : (Item *)NULL;
	}
	inline void to_next() {
		m_curr = m_curr ? m_curr->next : (Item *)NULL;
	}
	inline T *get_current() {
		return m_curr ? m_curr->p : (T *)NULL;
	}
	inline void remove_current() {
		Item *x;

		if (!(x = m_curr))
			return;

		m_curr = m_curr->next ? m_curr->next : m_curr->prev;

		if (x->prev)
			x->prev->next = x->next;
		else
			m_head = x->next;
		if (x->next)
			x->next->prev = x->prev;
		else
			m_tail = x->prev;

		delete x;
	}
	inline void reset_current() {
		m_curr = (Item *)NULL;
	}

	inline bool is_empty() {
		return !m_head;
	}
	inline void clear() {
		while(!is_empty())
			pop_front();
	}
};

//------------------------------------------------------------------------------

typedef void(__stdcall *fnDestructor_t)(void *);

template<typename T, typename KT=unsigned int>
class id_map {
public:
	enum error_t {
		kErr_None,

		kErr_NotFound,
		kErr_NoMemory
	};

protected:
	struct entry {
		union {
			entry *e;
			T *p;
		} tb[256];
	};

	mutable entry m_table;

	mutable error_t m_error;

	//bool m_delete;
	fnDestructor_t m_fnDestructor;

	inline void set_error(error_t err) const {
		if (m_error)
			return;

		m_error = err;
	}

	T **access(KT key, bool create) const {
		unsigned char *k, j;
		size_t i;
		entry *p, *q;

		k = (unsigned char *)&key;
		p = &m_table;

		for(i=0; i<sizeof(key) - 1; i++) {
			j = k[i];

			if (!(q = p->tb[j].e)) {
				if unlikely (!create) {
					set_error(kErr_NotFound);
					return (T **)NULL;
				}

				if unlikely (!(p->tb[j].e = new entry())) {
					set_error(kErr_NoMemory);
					return (T **)NULL;
				}

				q = p->tb[j].e;
			}

			p = q;
		}

		return &p->tb[k[i]].p;
	}
	void remove_branch(entry *branch, size_t n) {
		size_t i;

		for(i=0; i<256; i++) {
			if (n && branch->tb[i].e)
				remove_branch(branch->tb[i].e, n - 1);
			else if(m_fnDestructor && branch->tb[i].p)
				m_fnDestructor(branch->tb[i].p);
		}

		delete branch;
	}

public:
	id_map() {
		size_t i;

		for(i=0; i<256; i++)
			m_table.tb[i].e = (entry *)NULL;

		m_error = kErr_None;
		m_fnDestructor = (fnDestructor_t)NULL;
	}
	~id_map() {
		remove_all();
	}

	void remove_all() {
		size_t i;

		for(i=0; i<256; i++) {
			if (m_table.tb[i].e) {
				remove_branch(m_table.tb[i].e, sizeof(KT) - 1);
				m_table.tb[i].e = (entry *)NULL;
			}
		}
	}

	void set_destructor(fnDestructor_t fnDtor) {
		m_fnDestructor = fnDtor;
	}
	fnDestructor_t get_destructor() const {
		return m_fnDestructor;
	}

	int get_error() const {
		return (int)m_error;
	}
	void clear_error() {
		m_error = kErr_None;
	}

	T *get(KT key) const {
		T **place;

		if (!(place = access(key, false)))
			return (T *)NULL;

		return *place;
	}

	bool set(KT key, T *p, bool if_exists=false) {
		T **place;

		if unlikely (!(place = access(key, !if_exists)))
			return false;

		*place = p;
		return true;
	}

	void destruct(KT key) {
		T **place;
		
		if unlikely (!(place = access(key, false)))
			return;

		if unlikely (!*place)
			return;

		if likely (m_fnDestructor)
			m_fnDestructor(*place);

		*place = (T *)NULL;
	}
};

//------------------------------------------------------------------------------

#define GB_DICT_LOWER "abcdefghijklmnopqrstuvwxyz"
#define GB_DICT_UPPER "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define GB_DICT_DIGIT "0123456789"
#define GB_DICT_UNDER "_"
#define GB_DICT_IDENT GB_DICT_LOWER GB_DICT_UPPER GB_DICT_DIGIT GB_DICT_UNDER

struct Entry {
	struct Entry *entries;
	void *p;
};
struct Dictionary {
	int convmap[256];

	size_t numEntries;
	Entry *entries;
};

//------------------------------------------------------------------------------

struct BTreeNode;
struct BTree;

struct BTreeNode {
	int key;
	void *obj;

	BTreeNode *prnt;
	BTreeNode *left, *rght;

	BTreeNode *prev, *next;

	BTree *base;
};
struct BTree {
	BTreeNode *root;

	BTreeNode *head, *tail;
	BTreeNode *prep;
};

//------------------------------------------------------------------------------

struct vector {
	float x, y, z, w;
};

//------------------------------------------------------------------------------

struct ChecklistItem {
	std::string name;
	int i[4];
	float f[4];

	inline ChecklistItem() {
		name.assign("");
		i[0] = i[1] = i[2] = i[3] = 0;
		f[0] = f[1] = f[2] = f[3] = 0;
	}
};

std::vector<ChecklistItem> g_checklist;

//------------------------------------------------------------------------------

namespace x86 {

inline void cpuid(u32_t f, u32_t regs[4]) {
#if __GNUC__ || __clang__
	__asm__ __volatile__
	(
		"cpuid"
	  :	"=a" (regs[0]),
		"=b" (regs[1]),
		"=c" (regs[2]),
		"=d" (regs[3])
	  :	"a" (f),
	    "c" (0) //set ECX to zero for function 4
	);
#else
# if _MSC_VER
	__cpuid((int *)regs, (int)f);
# else
	__asm {
		mov eax, [f]
		xor ecx, ecx
		cpuid
		mov [regs +  0], eax
		mov [regs +  4], ebx
		mov [regs +  8], ecx
		mov [regs + 12], edx
	}
# endif
#endif
}

inline void get_cpu_count(u32_t &cores, u32_t &threads, bool &hyperthreading) {
	u32_t regs[4], features;
	u32_t vendor[4];

	// vendor
	cpuid(0, vendor);
	vendor[3] = 0;

	// features
	cpuid(1, regs);
	features = regs[3];
	threads = (regs[1] >> 16) & 0xFF;
	cores = threads;

	// GenuineIntel
	if(vendor[0]==0x756E6547
	&& vendor[1]==0x49656E69
	&& vendor[2]==0x6C65746E) {
		cpuid(4, regs);
		cores = ((regs[0] >> 26) & 0x3F) + 1;
	} else
	// AuthenticAMD
	if(vendor[0]==0x68747541
	&& vendor[1]==0x69746E65
	&& vendor[2]==0x444D4163) {
		x86::cpuid(0x80000008, regs);
		cores = (regs[2] & 0xFF) + 1;
	}

	// check hyperthreading
	if (!(hyperthreading = (features & (1 << 28)) && (cores < threads)))
		threads = cores; //HT must be enabled to use the extra threads
}

}

//------------------------------------------------------------------------------

EXPORT double PerfTimer();
EXPORT void QueryNanosecs(u32_t *seconds, u32_t *nanoseconds);
EXPORT int GetIntegralDate();

//

EXPORT int Errno();
EXPORT const char *ErrnoString(int e);
EXPORT void SetErrno(int e);

EXPORT int Shell(const char *cmd);

EXPORT int Wildcard(const char *source, const char *pattern);
EXPORT int PerformChecklistForFiles(const char *path, const char *patterns);

//

EXPORT int CountMaxStatFiles();
EXPORT int StatFile(const char *filename, uint_t statIndex);
EXPORT int StatCreatedTime(uint_t statIndex);
EXPORT int StatAccessedTime(uint_t statIndex);
EXPORT int StatModifiedTime(uint_t statIndex);
EXPORT int StatIsDirectory(uint_t statIndex);
EXPORT int StatIsFile(uint_t statIndex);

EXPORT int MakeDirs(const char *dirs);

//

EXPORT void *MemAlloc(uint_t amnt);
EXPORT void *MemAllocZero(uint_t amnt);
EXPORT void *MemFree(void *p);
EXPORT void *MemExpand(void *p, uint_t amnt);

EXPORT void PokeBytePtr(char *p, char x);
EXPORT void PokeWordPtr(short *p, short x);
EXPORT void PokeDwordPtr(int *p, int x);
EXPORT int PeekBytePtr(unsigned char *p);
EXPORT int PeekWordPtr(unsigned short *p);
EXPORT int PeekDwordPtr(unsigned int *p);

EXPORT void PokeFloatPtr(float *p, float x);
EXPORT void PokeDoublePtr(double *p, double x);
EXPORT float PeekFloatPtr(float *p);
EXPORT double PeekDoublePtr(double *p);

#undef FillMemory
#undef CopyMemory
#undef MoveMemory
EXPORT void ClearMemory(void *p, uint_t n);
EXPORT void FillMemory(void *p, unsigned char x, uint_t n);
EXPORT void CopyMemory(void *dst, const void *src, uint_t n);
EXPORT void MoveMemory(void *dst, const void *src, uint_t n);

//

EXPORT linked_list<void> *NewList();
EXPORT linked_list<void> *DeleteList(linked_list<void> *p);

EXPORT void *ListPopFront(linked_list<void> *p);
EXPORT void *ListPopBack(linked_list<void> *p);

EXPORT void ListPushFront(linked_list<void> *p, void *x);
EXPORT void ListPushBack(linked_list<void> *p, void *x);

EXPORT void ListInsertBefore(linked_list<void> *p, void *x);
EXPORT void ListInsertAfter(linked_list<void> *p, void *x);

EXPORT void ListToFirst(linked_list<void> *p);
EXPORT void ListToLast(linked_list<void> *p);
EXPORT void ListToPrevious(linked_list<void> *p);
EXPORT void ListToNext(linked_list<void> *p);
EXPORT void *ListCurrent(linked_list<void> *p);
EXPORT void ListRemoveCurrent(linked_list<void> *p);
EXPORT void ListResetCurrent(linked_list<void> *p);

EXPORT int ListIsEmpty(linked_list<void> *p);
EXPORT void ListClear(linked_list<void> *p);

//

EXPORT Dictionary *NewAssoc(const char *allowed);
EXPORT Dictionary *DeleteAssoc(Dictionary *p);

EXPORT int ResetAssoc(Dictionary *p, const char *allowed);

EXPORT Entry *AssocFind(Dictionary *dict, const char *str);
EXPORT Entry *AssocLookup(Dictionary *dict, const char *str);

EXPORT Entry *AssocAdd(Dictionary *dict, const char *str, void *p);
EXPORT Entry *AssocSet(Dictionary *dict, const char *str, void *p);
EXPORT void *AssocGet(Dictionary *dict, const char *str);

//

EXPORT BTree *NewBTree();
EXPORT BTree *DeleteBTree(BTree *base);

EXPORT void BTreeRemoveAll(BTree *base);

EXPORT BTreeNode *BTreeLookup(BTree *base, int key);
EXPORT BTreeNode *BTreeAdd(BTree *base, int key);
EXPORT BTreeNode *BTreeFind(BTree *base, int key);

EXPORT BTreeNode *BTreeRemove(BTreeNode *node);

EXPORT void BTreeSet(BTreeNode *node, void *obj);
EXPORT void *BTreeGet(BTreeNode *node);

//

EXPORT u16_t BigEndian16(u16_t x);
EXPORT u32_t BigEndian32(u32_t x);

EXPORT u16_t LittleEndian16(u16_t x);
EXPORT u32_t LittleEndian32(u32_t x);

EXPORT float BigEndianFloat(float x);
EXPORT double BigEndianDouble(double x);

EXPORT float LittleEndianFloat(float x);
EXPORT double LittleEndianDouble(double x);

//

EXPORT const char *StrFromFloat(float f, uint_t digits);

EXPORT double DoubleEpsilon();
EXPORT float FloatEpsilon();

EXPORT float Infinity();

typedef float real_t;

EXPORT real_t SinFast(real_t x);
EXPORT real_t CosFast(real_t x);
EXPORT real_t TanFast(real_t x);
EXPORT real_t SecFast(real_t x);
EXPORT real_t CscFast(real_t x);
EXPORT real_t CotFast(real_t x);
EXPORT double Lerp(double x, double y, double t);

EXPORT double NormalizeAngle360(double angle);
EXPORT double NormalizeAngle180(double angle);
EXPORT double AngleDelta(double a, double b);

EXPORT int IntSign(int x);
EXPORT float FloatSign(float x);

EXPORT int Round(float x);

EXPORT float SqrtFast(float x);
EXPORT float InvSqrtFast(float x);

EXPORT int IntCopySign(int a, int b);
EXPORT float FloatCopySign(float a, float b);
EXPORT double DoubleCopySign(double a, double b);

EXPORT int NextPowerOfTwo(int x);
EXPORT int NextSquarePowerOfTwo(int x, int y);
EXPORT int IntClamp(int x, int l, int h);
EXPORT float FloatClamp(float x, float l, float h);
EXPORT double DoubleClamp(double x, double l, double h);

EXPORT float ClampSNorm(float x);
EXPORT float ClampUNorm(float x);

EXPORT float FloatMin(float x, float y);
EXPORT double DoubleMin(double x, double y);

EXPORT float FloatMax(float x, float y);
EXPORT double DoubleMax(double x, double y);

//

EXPORT vector *Vec3(float x, float y, float z, float w);
EXPORT vector *Vec3Snap(vector *dst);
EXPORT vector *Vec3Normalize360(vector *dst);
EXPORT vector *Vec3Normalize180(vector *dst);
EXPORT vector *Vec3AngleDelta(vector *dst, const vector *P,
	const vector *Q);

EXPORT vector *Vec3Add(vector *dst, const vector *P, const vector *Q);
EXPORT vector *Vec3Sub(vector *dst, const vector *P, const vector *Q);
EXPORT vector *Vec3Mul(vector *dst, const vector *P, const vector *Q);
EXPORT vector *Vec3Div(vector *dst, const vector *P, const vector *Q);

EXPORT vector *Vec3Right();
EXPORT vector *Vec3Up();
EXPORT vector *Vec3Forward();

EXPORT float VecX(vector *vec);
EXPORT float VecY(vector *vec);
EXPORT float VecZ(vector *vec);
EXPORT float VecW(vector *vec);

EXPORT float Vec3Distance(vector *P, vector *Q);
EXPORT float Vec3DistanceFast(vector *P, vector *Q);

//

EXPORT void ClearChecklist();
EXPORT bool_t IsChecklistEmpty();
EXPORT uint_t CountChecklistItems();
EXPORT uint_t AddChecklistItem();
EXPORT void SetChecklistItemName(uint_t i, const char *name);
EXPORT void SetChecklistItemInteger(uint_t i, int x, uint_t n);
EXPORT void SetChecklistItemFloat(uint_t i, int x, uint_t n);
EXPORT const char *ChecklistItemName(uint_t i);
EXPORT int ChecklistItemInteger(uint_t i, uint_t n);
EXPORT float ChecklistItemFloat(uint_t i, uint_t n);

//

EXPORT id_map<void> *NewIDMap();
EXPORT id_map<void> *DeleteIDMap(id_map<void> *p);

EXPORT void IDMapRemoveAll(id_map<void> *p);

EXPORT void IDMapSetDestructor(id_map<void> *p, fnDestructor_t fnDtor);
EXPORT fnDestructor_t IDMapGetDestructor(id_map<void> *p);

EXPORT int IDMapGetError(id_map<void> *p);
EXPORT const char *IDMapGetErrorName(id_map<void> *p);
EXPORT void IDMapClearError(id_map<void> *p);

EXPORT void *IDMapGet(id_map<void> *p, unsigned int i);
EXPORT int IDMapSet(id_map<void> *p, unsigned int i, void *v, int if_exists);

EXPORT void IDMapDestruct(id_map<void> *p, unsigned int i);

//

EXPORT u32_t AtomicIncrement(volatile u32_t *x);
EXPORT u32_t AtomicDecrement(volatile u32_t *x);
EXPORT u32_t AtomicExchange(volatile u32_t *x, u32_t y);
EXPORT u32_t AtomicExchangeIfEqual(volatile u32_t *x, u32_t y, u32_t z);

EXPORT u32_t AtomicAnd(volatile u32_t *x, u32_t y);
EXPORT u32_t AtomicOr(volatile u32_t *x, u32_t y);
EXPORT u32_t AtomicXor(volatile u32_t *x, u32_t y);

EXPORT u32_t AtomicRead(volatile u32_t *x);

EXPORT u32_t AtomicSpinEqual(volatile u32_t *x, u32_t y);
EXPORT u32_t AtomicSpinNotEqual(volatile u32_t *x, u32_t y);

//

EXPORT HANDLE NewSysEvent(int manualReset, int initialState);
EXPORT HANDLE DeleteSysEvent(HANDLE h);

EXPORT void SysEventSignal(HANDLE h);
EXPORT void SysEventClear(HANDLE h);
EXPORT int SysEventTrySync(HANDLE h);
EXPORT void SysEventSync(HANDLE h);

//

EXPORT task::signal_t *NewSignal(unsigned int expCnt);
EXPORT task::signal_t *DeleteSignal(task::signal_t *sig);

EXPORT void SignalClear(task::signal_t *sig);
EXPORT void SignalSetSlotCount(task::signal_t *sig, unsigned int expCnt);
EXPORT void SignalAddSlot(task::signal_t *sig);
EXPORT void SignalRaise(task::signal_t *sig);

EXPORT void SignalSync(task::signal_t *sig);
EXPORT int SignalTrySync(task::signal_t *sig);

//

EXPORT int IsHyperthreadingAvailable();
EXPORT u32_t GetCPUCoreCount();
EXPORT u32_t GetCPUThreadCount();

//

EXPORT void EnableAppHangQuery();
EXPORT void DisableAppHangQuery();
EXPORT int IsAppHangQueryEnabled();

EXPORT void SetAppHangQueryTimeout(unsigned int seconds);
EXPORT unsigned int GetAppHangQueryTimeout();

//

EXPORT void InitStreamers(uint_t count);
EXPORT void FiniStreamers();

EXPORT uint_t GetStreamerCount();

EXPORT void StreamDispatch(jobFunc_t fn, void *p);
EXPORT void StreamSync();

//

EXPORT const char *WinDir();
EXPORT const char *SysDir();

EXPORT const char *SysComputerName();
EXPORT const char *SysUserName();

EXPORT const char *AppDataDir();

//------------------------------------------------------------------------------

#define MAX_STATS 128
struct stat g_stats[MAX_STATS];

//------------------------------------------------------------------------------

EXPORT double PerfTimer() {
	u64_t t, f;

	QueryPerformanceFrequency((LARGE_INTEGER *)&f);
	QueryPerformanceCounter((LARGE_INTEGER *)&t);

	return ((double)t)/(double)f;
}
EXPORT void QueryNanosecs(u32_t *seconds, u32_t *nanoseconds) {
	u64_t f, t;

	QueryPerformanceFrequency((LARGE_INTEGER *)&f);
	QueryPerformanceCounter((LARGE_INTEGER *)&t);

	t = ((double)t)/(((double)f)/1000000000.0);

	if (likely(seconds))		*seconds     = t/1000000000;
	if (likely(nanoseconds))	*nanoseconds = t%1000000000;
}
EXPORT int GetIntegralDate() {
	struct tm *p;
	time_t t;

	t = time(0);
	p = localtime(&t);

	return (p->tm_year + 1900)*10000 + (p->tm_mon + 1)*100 + p->tm_mday;
}

//------------------------------------------------------------------------------

EXPORT int Errno() {
	return errno;
}
EXPORT const char *ErrnoString(int e) {
	return strerror(e);
}
EXPORT void SetErrno(int e) {
	errno = e;
}

EXPORT int Shell(const char *cmd) {
	return system(cmd);
}

EXPORT int Wildcard(const char *source, const char *pattern) {
	while(1) {
		if (*pattern=='*') {
			while(*pattern=='*')
				pattern++;

			if (*pattern=='\0')
				return 1;

			while(*source != *pattern) {
				if (*source=='\0')
					return 0;

				source++;
			}

			continue;
		}

		if (*source != *pattern)
			break;

		if (*source=='\0')
			return 1;

		source++;
		pattern++;
	}

	return 0;
}

static void GetPatterns(std::vector<std::string> &arr, const char *src) {
	const char *p;

	if unlikely (*src=='\0')
		return;

	for(p=src; 1; p++) {
		if (*p=='\\') {
			p++;
			continue;
		}

		if (*p!=';' && *p!='\0')
			continue;

		arr.push_back(std::string(src, p - src));
		if (!*p)
			break;

		src = p + 1;
	}
}
EXPORT int PerformChecklistForFiles(const char *path, const char *patterns) {
	std::vector<std::string> parr;
	struct dirent *de;
	size_t i, n;
	uint_t k;
	DIR *d;
	int e;

	GetPatterns(parr, patterns);
	n = parr.size();

	ClearChecklist();

	if (!(d = opendir(path)))
		return 0;

	while((de = readdir(d)) != (struct dirent *)NULL) {
		for(i=0; i<n; i++) {
			if (!Wildcard(de->d_name, parr[i].c_str()))
				continue;

			k = AddChecklistItem();
			SetChecklistItemName(k, de->d_name);
			break;
		}
	}

	e = errno;
	closedir(d);

	if (e) {
		errno = e;
		return 0;
	}

	if (errno)
		return 0;

	return 1;
}

//------------------------------------------------------------------------------

EXPORT int CountMaxStatFiles() {
	return MAX_STATS;
}
EXPORT int StatFile(const char *filename, uint_t statIndex) {
	if (unlikely(statIndex >= MAX_STATS))
		return 0;

	if (stat(filename, &g_stats[statIndex]) != 0)
		return 0;

	return 1;
}
EXPORT int StatCreatedTime(uint_t statIndex) {
	return g_stats[statIndex].st_ctime;
}
EXPORT int StatAccessedTime(uint_t statIndex) {
	return g_stats[statIndex].st_atime;
}
EXPORT int StatModifiedTime(uint_t statIndex) {
	return g_stats[statIndex].st_mtime;
}
EXPORT int StatIsDirectory(uint_t statIndex) {
	return (_S_ISDIR(g_stats[statIndex].st_mode)) ? 1 : 0;
}
EXPORT int StatIsFile(uint_t statIndex) {
	return (_S_ISREG(g_stats[statIndex].st_mode)) ? 1 : 0;
}

EXPORT int MakeDirs(const char *dirs) {
	const char *p;
	char buf[4096];
	char *path;

	// ignore the root directory
	if (dirs[0]=='/') {
		buf[0] = dirs[0];
		path = &buf[1];
		p = &dirs[1];
	} else if(dirs[1]==':' && dirs[2]=='/') {
		buf[0] = dirs[0];
		buf[1] = dirs[1];
		buf[2] = dirs[2];
		path = &buf[3];
		p = &dirs[3];
	} else {
		path = &buf[0];
		p = &dirs[0];
	}

	// make each directory, one by one
	while(1) {
		if (*p=='/' || *p==0) {
			*path = 0;

			errno = 0;
#if _WIN32
			mkdir(buf);
#else
			mkdir(buf, S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH);
#endif
			if (errno && errno!=EEXIST)
				return 0;

			if (!(*path++ = *p++))
				break;

			if (*p==0)
				break;
		} else
			*path++ = *p++;

		if (path==&buf[sizeof(buf) - 1])
			return 0;
	}

	return 1;
}

//------------------------------------------------------------------------------

EXPORT void *MemAlloc(uint_t amnt) {
	if (unlikely(!amnt))
		return (void *)NULL;

	return malloc(amnt);
}
EXPORT void *MemAllocZero(uint_t amnt) {
	void *p;

	if (unlikely(!(p = MemAlloc(amnt))))
		return (void *)NULL;

	memset(p, 0, amnt);
	return p;
}
EXPORT void *MemFree(void *p) {
	if (!p)
		return (void *)NULL;

	free((void *)p);
	return (void *)NULL;
}
EXPORT void *MemExpand(void *p, uint_t amnt) {
	void *q;

	if (!p)
		return MemAlloc(amnt);

	if (!amnt)
		return MemFree(p);

	if (unlikely(!(q = realloc(p, amnt))))
		return (void *)NULL;

	return q;
}
EXPORT void PokeBytePtr(char *p, char x) {
	if (unlikely(!p))
		return;

	*p = x;
}
EXPORT void PokeWordPtr(short *p, short x) {
	if (unlikely(!p))
		return;

	*p = x;
}
EXPORT void PokeDwordPtr(int *p, int x) {
	if (unlikely(!p))
		return;

	*p = x;
}
EXPORT int PeekBytePtr(unsigned char *p) {
	if (unlikely(!p))
		return 0;

	return (int)*p;
}
EXPORT int PeekWordPtr(unsigned short *p) {
	if (unlikely(!p))
		return 0;

	return (int)*p;
}
EXPORT int PeekDwordPtr(unsigned int *p) {
	if (unlikely(!p))
		return 0;

	return (int)*p;
}

EXPORT void PokeFloatPtr(float *p, float x) {
	if (unlikely(!p))
		return;

	*p = x;
}
EXPORT void PokeDoublePtr(double *p, double x) {
	if (unlikely(!p))
		return;

	*p = x;
}
EXPORT float PeekFloatPtr(float *p) {
	if (unlikely(!p))
		return 0.0f;

	return *p;
}
EXPORT double PeekDoublePtr(double *p) {
	if (unlikely(!p))
		return 0.0f;

	return *p;
}

EXPORT void ClearMemory(void *p, uint_t n) {
	memset(p, 0, n);
}
EXPORT void FillMemory(void *p, unsigned char x, uint_t n) {
	memset(p, x, n);
}
EXPORT void CopyMemory(void *dst, const void *src, uint_t n) {
	memcpy(dst, src, n);
}
EXPORT void MoveMemory(void *dst, const void *src, uint_t n) {
	memmove(dst, src, n);
}

//------------------------------------------------------------------------------

EXPORT linked_list<void> *NewList() {
	return new linked_list<void>();
}
EXPORT linked_list<void> *DeleteList(linked_list<void> *p) {
	delete p;
	return (linked_list<void> *)NULL;
}

EXPORT void *ListPopFront(linked_list<void> *p) {
	if (unlikely(!p))
		return (void *)NULL;

	return p->pop_front();
}
EXPORT void *ListPopBack(linked_list<void> *p) {
	if (unlikely(!p))
		return (void *)NULL;

	return p->pop_back();
}

EXPORT void ListPushFront(linked_list<void> *p, void *x) {
	if (unlikely(!p))
		return;

	p->push_front(x);
}
EXPORT void ListPushBack(linked_list<void> *p, void *x) {
	if (unlikely(!p))
		return;

	p->push_back(x);
}

EXPORT void ListInsertBefore(linked_list<void> *p, void *x) {
	if (unlikely(!p))
		return;

	p->insert_before(x);
}
EXPORT void ListInsertAfter(linked_list<void> *p, void *x) {
	if (unlikely(!p))
		return;

	p->insert_after(x);
}

EXPORT void ListToFirst(linked_list<void> *p) {
	if (unlikely(!p))
		return;

	p->to_first();
}
EXPORT void ListToLast(linked_list<void> *p) {
	if (unlikely(!p))
		return;

	p->to_last();
}
EXPORT void ListToPrevious(linked_list<void> *p) {
	if (unlikely(!p))
		return;

	p->to_previous();
}
EXPORT void ListToNext(linked_list<void> *p) {
	if (unlikely(!p))
		return;

	p->to_next();
}
EXPORT void *ListCurrent(linked_list<void> *p) {
	if (unlikely(!p))
		return (void *)NULL;

	return p->get_current();
}
EXPORT void ListRemoveCurrent(linked_list<void> *p) {
	if (unlikely(!p))
		return;

	p->remove_current();
}
EXPORT void ListResetCurrent(linked_list<void> *p) {
	if (unlikely(!p))
		return;

	p->reset_current();
}

EXPORT int ListIsEmpty(linked_list<void> *p) {
	if (unlikely(!p))
		return YES;

	return (int)p->is_empty();
}
EXPORT void ListClear(linked_list<void> *p) {
	if (unlikely(!p))
		return;

	p->clear();
}

//------------------------------------------------------------------------------

static int GenerateConvmap(int dst[256], const char *allowed) {
	int i, j, k;

	for(i=0; i<256; i++)
		dst[i] = -1;

	for(i=0; allowed[i]; i++)
		dst[(unsigned char)allowed[i]] = i;

	for(j=0; j<i; j++) {
		for(k=j+1; k<i; k++) {
			if (allowed[j]==allowed[k])
				return -1; /*duplicate entries*/
		}
	}

	return i;
}
static void DeleteEntries(Dictionary *dict, Entry *entries) {
	uint_t i;

	if (!entries)
		return;

	for(i=0; i<dict->numEntries; i++) {
		if (entries[i].entries)
			DeleteEntries(dict, entries[i].entries);
	}

	MemFree((void *)entries);
}
static Entry *FindFromEntry(Dictionary *dict, Entry *entries,
const char *str, int create) {
	size_t n;
	int i;

	i = dict->convmap[*(unsigned char *)str++];
	if (i == -1)
		return (Entry *)NULL; //invalid character

	// if there's more to the string...
	if (*str) {
		// generate the entries if they're not there
		if (!entries[i].entries) {
			if (create) {
				n = sizeof(Entry)*dict->numEntries;

				if (unlikely(!(entries[i].entries = (Entry *)MemAllocZero(n))))
					return (Entry *)NULL;
			}

			if (!entries[i].entries)
				return (Entry *)NULL;
		}

		// continue the search (recursive!)
		return FindFromEntry(dict, entries[i].entries, str, create);
	}

	// otherwise this is the last point
	return &entries[i];
}

static int InitDict(Dictionary *dict, const char *allowed) {
	size_t n;

	dict->numEntries = GenerateConvmap(dict->convmap, allowed);
	if (dict->numEntries < 1)
		return 0;

	n = sizeof(Entry)*dict->numEntries;

	if (unlikely(!(dict->entries = (Entry *)MemAllocZero(n))))
		return 0;

	return 1;
}
static void FiniDict(Dictionary *dict) {
	DeleteEntries(dict, dict->entries);
	dict->entries = (Entry *)NULL;

	dict->numEntries = 0;
}

EXPORT Dictionary *NewAssoc(const char *allowed) {
	Dictionary *p;

	if (unlikely(!(p = new Dictionary())))
		return (Dictionary *)NULL;

	if (!InitDict(p, allowed)) {
		delete p;
		return (Dictionary *)NULL;
	}

	return p;
}
EXPORT Dictionary *DeleteAssoc(Dictionary *p) {
	if (!p)
		return (Dictionary *)NULL;

	FiniDict(p);
	delete p;

	return (Dictionary *)NULL;
}

EXPORT int ResetAssoc(Dictionary *p, const char *allowed) {
	if (unlikely(!p))
		return 0;

	FiniDict(p);
	return InitDict(p, allowed);
}

EXPORT Entry *AssocFind(Dictionary *dict, const char *str) {
	if (unlikely(!dict))
		return (Entry *)NULL;

	return FindFromEntry(dict, dict->entries, str, 0);
}
EXPORT Entry *AssocLookup(Dictionary *dict, const char *str) {
	if (unlikely(!dict))
		return (Entry *)NULL;

	return FindFromEntry(dict, dict->entries, str, 1);
}

EXPORT Entry *AssocAdd(Dictionary *dict, const char *str, void *p) {
	Entry *entry;

	if (unlikely(!dict))
		return (Entry *)NULL;

	if (!(entry = AssocLookup(dict, str)))
		return (Entry *)NULL;

	if (entry->p)
		return (Entry *)NULL;

	entry->p = p;
	return entry;
}
EXPORT Entry *AssocSet(Dictionary *dict, const char *str, void *p) {
	Entry *entry;

	if (unlikely(!dict))
		return (Entry *)NULL;

	if (!(entry = AssocFind(dict, str)))
		return (Entry *)NULL;

	entry->p = p;
	return entry;
}
EXPORT void *AssocGet(Dictionary *dict, const char *str) {
	Entry *entry;

	if (unlikely(!dict))
		return (void *)NULL;

	if (!(entry = AssocFind(dict, str)))
		return (void *)NULL;

	return entry->p;
}

//------------------------------------------------------------------------------

EXPORT BTree *NewBTree();
EXPORT BTree *DeleteBTree(BTree *base);

EXPORT void BTreeRemoveAll(BTree *base);

EXPORT BTreeNode *BTreeLookup(BTree *base, int key);
EXPORT BTreeNode *BTreeAdd(BTree *base, int key);
EXPORT BTreeNode *BTreeFind(BTree *base, int key);

EXPORT BTreeNode *BTreeRemove(BTreeNode *node);

EXPORT void BTreeSet(BTreeNode *node, void *obj);
EXPORT void *BTreeGet(BTreeNode *node);

EXPORT BTree *NewBTree() {
	BTree *base;

	if (unlikely(!(base = (BTree *)MemAlloc(sizeof(*base)))))
		return (BTree *)NULL;

	base->root = (BTreeNode *)NULL;
	base->head = (BTreeNode *)NULL;
	base->tail = (BTreeNode *)NULL;
	base->prep = (BTreeNode *)NULL;

	return base;
}
EXPORT BTree *DeleteBTree(BTree *base) {
	if (!base)
		return (BTree *)NULL;

	BTreeRemoveAll(base);
	return (BTree *)MemFree((void *)base);
}

EXPORT void BTreeRemoveAll(BTree *base) {
	BTreeNode *node, *next;

	if (unlikely(!base))
		return;

	for(node=base->head; node; node=next) {
		next = node->next;
		node->prev = node->next = (BTreeNode *)NULL;
		node->prnt = (BTreeNode *)NULL;
		node->left = node->rght = (BTreeNode *)NULL;
		node->base = (BTree *)NULL;
		MemFree((void *)node);
	}

	base->root = (BTreeNode *)NULL;
	base->head = (BTreeNode *)NULL;
	base->tail = (BTreeNode *)NULL;
	base->prep = (BTreeNode *)MemFree((void *)base->prep);
}

static void BTreeListNode(BTree *base, BTreeNode *node, int key) {
	node->base = base;

	node->key = key;
	node->obj = (void *)NULL;

	node->prnt = (BTreeNode *)NULL;
	node->left = (BTreeNode *)NULL;
	node->rght = (BTreeNode *)NULL;

	node->next = (BTreeNode *)NULL;
	if (likely((node->prev = base->tail) != (BTreeNode *)NULL))
		base->tail->next = node;
	else
		base->head = node;
	base->tail = node;
}
static BTreeNode *__BTreeFind(BTree *base, int key, BTreeNode *create) {
	BTreeNode *node, *next, *top, **spot;

	if (!base->root && create) {
		BTreeListNode(base, create, key);

		base->root = create;
		return create;
	}

	top = (BTreeNode *)NULL;
	for(node=base->root; node; node=next) {
		if (node->key==key)
			return node;

		spot = (key < node->key) ? &node->left : &node->rght;
		top = node;

		if (!(next = *spot)) {
			if (create) {
				BTreeListNode(base, create, key);

				create->prnt = top;
				return *spot=create;
			}

			return (BTreeNode *)NULL;
		}
	}

	return (BTreeNode *)NULL;
}

EXPORT BTreeNode *BTreeLookup(BTree *base, int key) {
	BTreeNode *p;

	if (unlikely(!base))
		return (BTreeNode *)NULL;

	if (!base->prep) {
		if (unlikely(!(base->prep = (BTreeNode *)MemAlloc(sizeof(BTreeNode)))))
			return (BTreeNode *)NULL;
	}

	p = __BTreeFind(base, key, base->prep);
	if (p == base->prep)
		base->prep = (BTreeNode *)NULL;

	return p;
}
EXPORT BTreeNode *BTreeAdd(BTree *base, int key) {
	BTreeNode *p;

	if (unlikely(!base))
		return (BTreeNode *)NULL;

	if (!base->prep) {
		if (unlikely(!(base->prep = (BTreeNode *)MemAlloc(sizeof(BTreeNode)))))
			return (BTreeNode *)NULL;
	}

	p = __BTreeFind(base, key, base->prep);
	if (p != base->prep)
		return (BTreeNode *)NULL;

	base->prep = (BTreeNode *)NULL;
	return p;
}
EXPORT BTreeNode *BTreeFind(BTree *base, int key) {
	if (unlikely(!base))
		return (BTreeNode *)NULL;

	return __BTreeFind(base, key, (BTreeNode *)NULL);
}

EXPORT BTreeNode *BTreeRemove(BTreeNode *node) {
	BTree *base;
	BTreeNode *r, *s;

	if (unlikely(!node || !(base = node->base)))
		return (BTreeNode *)NULL;

	if (!(r = node->rght)) {
		if (node->prnt) {
			if (node==node->prnt->left)
				node->prnt->left = node->left;
			else
				node->prnt->rght = node->left;
		} else
			base->root = node->left;

		if (node->left)
			node->left->prnt = node->prnt;
	} else if(!r->left) {
		r->prnt = node->prnt;

		if ((r->left = node->left) != (BTreeNode *)NULL)
			node->left->prnt = r;

		if (node->prnt)
			node->prnt->left = r;
		else
			base->root = r;
	} else {
		s = r->left;

		while(s->left)
			s = s->left;
		s->left = node->left;

		if ((s->prnt->left = s->rght) != (BTreeNode *)NULL)
			s->rght->prnt = s->prnt;
		s->rght = r;

		if ((s->prnt = node->prnt) != (BTreeNode *)NULL) {
			if (node->prnt->left==node)
				node->prnt->left = s;
			else
				node->prnt->rght = s;
		} else
			base->root = s;
	}

	if (node->prev)
		node->prev->next = node->next;
	else
		node->base->head = node->next;

	if (node->next)
		node->next->prev = node->prev;
	else
		node->base->tail = node->prev;

	return (BTreeNode *)MemFree((void *)node);
}

EXPORT void BTreeSet(BTreeNode *node, void *obj) {
	if (unlikely(!node))
		return;

	node->obj = obj;
}
EXPORT void *BTreeGet(BTreeNode *node) {
	if (unlikely(!node))
		return (void *)NULL;

	return node->obj;
}

//------------------------------------------------------------------------------

#define BCW(c,x,i,s) ((((c)(((u8_t *)&(x))[(i)]))<<(s)))
#define B16(x,i,s) BCW(u16_t,(x),(i),(s))
#define B32(x,i,s) BCW(u32_t,(x),(i),(s))
#define B64(x,i,s) BCW(u64_t,(x),(i),(s))
EXPORT u16_t BigEndian16(u16_t x) {
	return B16(x,0,8)|B16(x,1,0);
}
EXPORT u32_t BigEndian32(u32_t x) {
	return B32(x,0,24)|B32(x,1,16)|B32(x,2,8)|B32(x,3,0);
}
u64_t BigEndian64(u64_t x) {
	return B64(x,0,56)|B64(x,1,48)|B64(x,2,40)|B64(x,3,32)|
	       B64(x,4,24)|B64(x,5,16)|B64(x,6, 8)|B64(x,7, 0);
}

EXPORT u16_t LittleEndian16(u16_t x) {
	return B16(x,0,0)|B16(x,1,8);
}
EXPORT u32_t LittleEndian32(u32_t x) {
	return B32(x,0,0)|B32(x,1,8)|B32(x,2,16)|B32(x,3,24);
}
u64_t LittleEndian64(u64_t x) {
	return B64(x,0,0) |B64(x,1,8) |B64(x,2,16)|B64(x,3,24)|
	       B64(x,4,32)|B64(x,5,40)|B64(x,6,48)|B64(x,7,56);
}
#undef B64
#undef B32
#undef B16
#undef BCW

EXPORT float BigEndianFloat(float x) {
	u32_t r;

	r = BigEndian32(*(u32_t *)&x);

	return *(float *)&r;
}
EXPORT double BigEndianDouble(double x) {
	u64_t r;

	r = BigEndian64(*(u64_t *)&x);

	return *(double *)&r;
}

EXPORT float LittleEndianFloat(float x) {
	u32_t r;

	r = LittleEndian32(*(u32_t *)&x);

	return *(float *)&r;
}
EXPORT double LittleEndianDouble(double x) {
	u64_t r;

	r = LittleEndian64(*(u64_t *)&x);

	return *(double *)&r;
}

//------------------------------------------------------------------------------

EXPORT const char *StrFromFloat(float f, uint_t digits) {
	static char buf[64];

	if (!digits) {
#if __STDC_WANT_SECURE_LIB__
		sprintf_s(buf, sizeof(buf), "%g", f);
#else
		snprintf(buf, sizeof(buf), "%g", f);
		buf[sizeof(buf) - 1] = '\0';
#endif
	} else {
#if __STDC_WANT_SECURE_LIB__
		sprintf_s(buf, sizeof(buf), "%.*f", digits, f);
#else
		snprintf(buf, sizeof(buf), "%.*f", digits, f);
		buf[sizeof(buf) - 1] = '\0';
#endif
	}

	return buf;
}

EXPORT double DoubleEpsilon() {
	return 0.0000000000000001110223024625156663683148108874;
}
EXPORT float FloatEpsilon() {
	return 0.00000006f;
}

//infinity
// Thank you LackOfGrace:
// http://www.gamedev.net/topic/465682-visual-studio-2005-and-infinity/
#ifndef INFINITY //this is defined in glibc, but not in MSVC :(
union MSVC_EVIL_FLOAT_HACK
{
   unsigned char Bytes[4];
   float Value;
};
static union MSVC_EVIL_FLOAT_HACK INFINITY_HACK = {{0x00, 0x00, 0x80, 0x7F}};
# define INFINITY (INFINITY_HACK.Value)
#endif
EXPORT float Infinity() {
	return INFINITY;
}

class SinTbl {
public:
	//
	//	Memory Usage
	//	------------
	//	TYPE		RES			USAGE
	//	double		2048		32K
	//	double		4096		64K
	//	double		8192		128K
	//	double		16384		256K
	//	double		32768		512K
	//	double		65536		1M
	//	double		131072		2M
	//	double		262144		4M
	//	double		524288		8M
	//	double		1050576		16M
	//	double		2101152		32M
	//

	enum {
		//
		// Should be a power-of-two; select one
		//

		//RESOLUTION=2048,
		//RESOLUTION=4096,
		//RESOLUTION=8192,
		//RESOLUTION=16384,
		//RESOLUTION=32768,
		//RESOLUTION=65536,
		RESOLUTION=131072,
		//RESOLUTION=262144,
		//RESOLUTION=524144,
		//RESOLUTION=1050576,
		//RESOLUTION=2101152
	};

	static real_t g_sin[RESOLUTION];

	static inline size_t Degrees(real_t f) {
		// NOTE: After optimizations, this is two multiplies, and one bit-and.
		return ((size_t)((f/360) * RESOLUTION)) % RESOLUTION;
	}
	static inline size_t Radians(real_t f) {
		return ((size_t)((f/TAU) * RESOLUTION)) % RESOLUTION;
	}

	inline SinTbl() {
		real_t rad;
		size_t i;

		for(i=0; i<RESOLUTION; i++) {
			rad = (((real_t)i)/RESOLUTION)*TAU;

			g_sin[i] = sin(rad);
		}
	}
	inline ~SinTbl() {
	}
};
real_t SinTbl::g_sin[SinTbl::RESOLUTION];
SinTbl __sinTbl_Singleton__;

EXPORT real_t SinFast(real_t x) { return SinTbl::g_sin[SinTbl::Degrees(x)]; }
EXPORT real_t CosFast(real_t x) { return SinFast(x + 90.0); }
EXPORT real_t TanFast(real_t x) { return SinFast(x)/CosFast(x); }
EXPORT real_t SecFast(real_t x) { return 1/CosFast(x); }
EXPORT real_t CscFast(real_t x) { return 1/SinFast(x); }
EXPORT real_t CotFast(real_t x) { return CosFast(x)/SinFast(x); }
EXPORT double Lerp(double x, double y, double t) { return x + (y - x)*t; }

EXPORT double NormalizeAngle360(double angle) {
	if (((unsigned int *)&angle)[1] >= 0x40768000)
		angle -= floor(angle/360.0)*360.0;

	return angle;
}
EXPORT double NormalizeAngle180(double angle) {
	angle = NormalizeAngle360(angle);
	return angle > 180.0 ? angle - 360.0 : angle;
}
EXPORT double AngleDelta(double a, double b) {
	return NormalizeAngle180(a - b);
}

#define LB(x) (sizeof(x)*8 - 1)
EXPORT int IntSign(int x) { return 1 | (x>>LB(x)); }
EXPORT float FloatSign(float x) {
	union { float f; int i; } r;

	r.f = x;
	return (float)IntSign(r.i);
}

EXPORT int Round(float x) {
	union { float f; int i; } v;

	v.f   = x + (float)(3 << 21);
	v.i  -= 0x4AC00000;
	v.i >>= 1;

	return v.i;
}

// Approximate the square root of 'x'
EXPORT float SqrtFast(float x) {
	// The SSE version is slower, but more accurate
	union { float f; int i; } v;

	v.f   = x;
	v.i  -= 0x3F800000;
	v.i >>= 1;
	v.i  += 0x3F800000;

	return v.f;
}
// Approximate the reciprocal square root of 'x'
EXPORT float InvSqrtFast(float x) {
#if USE_SSE
	_mm_store_ss(&x, _mm_rsqrt_ss(_mm_load_ss(&x)));
	return x;
#else
	union { float f; int i; } v;

	v.f = x;
	v.i = 0x5F3759DF - (v.i >> 1);

	return v.f*(1.5f - 0.5f*x*v.f*v.f);
#endif
}

// Copy the sign of 'b' into 'a'
EXPORT int IntCopySign(int a, int b) {
	int difmsk;

	// absolute
	//a = (a + (a >> (sizeof(a)*8 - 1))) ^ (a >> (sizeof(a)*8 - 1));

	difmsk = (((a ^ b) & (1<<LB(a)))>>LB(a));
	//printf("[difmsk=0x%.8X]", *(unsigned int *)&difmsk);

	return (-a & difmsk) | (a & ~difmsk);
}
EXPORT float FloatCopySign(float a, float b) {
	int i;

	i = ((*(int *)&a)&~(1<<LB(a)))|((*(int *)&b)&(1<<LB(b)));

	return *(float *)&i;
}
EXPORT double DoubleCopySign(double a, double b) {
	u64_t i;

	i = ((*(u64_t *)&a)&~(1ULL<<LB(a)))|((*(u64_t *)&b)&(1ULL<<LB(b)));

	return *(double *)&i;
}
#undef LB

// Round 'x' to the nearest fitting power-of-two */
EXPORT int NextPowerOfTwo(int x) {
	if ((x & (x - 1)) != 0) {
		x--;
		x |= x>>1;
		x |= x>>2;
		x |= x>>4;
		x |= x>>8;
		x |= x>>16;
		x++;
	}

	return x;
}
EXPORT int NextSquarePowerOfTwo(int x, int y) {
	return NextPowerOfTwo(x > y ? x : y);
}
EXPORT int IntClamp(int x, int l, int h) {
	return x < l ? l : x > h ? h : x;
}
EXPORT float FloatClamp(float x, float l, float h) {
#if USE_SSE
	_mm_store_ss(&x, _mm_min_ss(_mm_max_ss(_mm_set_ss(x),_mm_set_ss(l)),
		_mm_set_ss(h)));

	return x;
#else
	return x < l ? l : x > h ? h : x;
#endif
}
EXPORT double DoubleClamp(double x, double l, double h) {
#if TL_USE_CORE_SIMD && TL_X86 && __SSE2__
	_mm_store_sd(&x, _mm_min_sd(_mm_max_sd(_mm_set_sd(x),_mm_set_sd(l)),
		_mm_set_sd(h)));

	return x;
#else
	return x < l ? l : x > h ? h : x;
#endif
}

EXPORT float ClampSNorm(float x) { return FloatClamp(x, -1, +1); }
EXPORT float ClampUNorm(float x) { return FloatClamp(x, 0, +1); }

EXPORT float FloatMin(float x, float y) {
#if USE_SSE
	_mm_store_ss(&x, _mm_min_ss(_mm_set_ss(x), _mm_set_ss(y)));
	return x;
#else
	return x < y ? x : y;
#endif
}
EXPORT double DoubleMin(double x, double y) {
#if USE_SSE2
	_mm_store_sd(&x, _mm_min_sd(_mm_set_sd(x), _mm_set_sd(y)));
	return x;
#else
	return x < y ? x : y;
#endif
}

EXPORT float FloatMax(float x, float y) {
#if USE_SSE
	_mm_store_ss(&x, _mm_max_ss(_mm_set_ss(x), _mm_set_ss(y)));
	return x;
#else
	return x > y ? x : y;
#endif
}
EXPORT double DoubleMax(double x, double y) {
#if USE_SSE2
	_mm_store_sd(&x, _mm_max_sd(_mm_set_sd(x), _mm_set_sd(y)));
	return x;
#else
	return x > y ? x : y;
#endif
}

//------------------------------------------------------------------------------

EXPORT vector *Vec3(float x, float y, float z, float w) {
	static vector vec[16];
	static size_t i = 0;
	vector *p;

	p = &vec[i];
	i = (i + 1)%(sizeof(vec)/sizeof(vec[0]));

	p->x = x;
	p->y = y;
	p->z = z;
	p->w = w;

	return p;
}
EXPORT vector *Vec3Snap(vector *dst) {
	dst->x = floor(dst->x + 0.5);
	dst->y = floor(dst->y + 0.5);
	dst->z = floor(dst->z + 0.5);

	return dst;
}
EXPORT vector *Vec3Normalize360(vector *dst) {
	dst->x = NormalizeAngle360(dst->x);
	dst->y = NormalizeAngle360(dst->y);
	dst->z = NormalizeAngle360(dst->z);

	return dst;
}
EXPORT vector *Vec3Normalize180(vector *dst) {
	dst->x = NormalizeAngle180(dst->x);
	dst->y = NormalizeAngle180(dst->y);
	dst->z = NormalizeAngle180(dst->z);

	return dst;
}
EXPORT vector *Vec3AngleDelta(vector *dst, const vector *P, const vector *Q) {
	dst->x = AngleDelta(P->x, Q->x);
	dst->y = AngleDelta(P->y, Q->y);
	dst->z = AngleDelta(P->z, Q->z);

	return dst;
}

EXPORT vector *Vec3Add(vector *dst, const vector *P, const vector *Q) {
	dst->x = P->x + Q->x;
	dst->y = P->y + Q->y;
	dst->z = P->z + Q->z;

	return dst;
}
EXPORT vector *Vec3Sub(vector *dst, const vector *P, const vector *Q) {
	dst->x = P->x - Q->x;
	dst->y = P->y - Q->y;
	dst->z = P->z - Q->z;

	return dst;
}
EXPORT vector *Vec3Mul(vector *dst, const vector *P, const vector *Q) {
	dst->x = P->x*Q->x;
	dst->y = P->y*Q->y;
	dst->z = P->z*Q->z;

	return dst;
}
EXPORT vector *Vec3Div(vector *dst, const vector *P, const vector *Q) {
	dst->x = P->x/Q->x;
	dst->y = P->y/Q->y;
	dst->z = P->z/Q->z;

	return dst;
}

EXPORT vector *Vec3Right() {
	static vector v = { 1, 0, 0, 0 };

	return &v;
}
EXPORT vector *Vec3Up() {
	static vector v = { 0, 1, 0, 0 };

	return &v;
}
EXPORT vector *Vec3Forward() {
	static vector v = { 0, 0, 1, 0 };

	return &v;
}

EXPORT float VecX(vector *vec) {
	return likely(vec != (vector *)NULL) ? vec->x : 0.0f;
}
EXPORT float VecY(vector *vec) {
	return likely(vec != (vector *)NULL) ? vec->y : 0.0f;
}
EXPORT float VecZ(vector *vec) {
	return likely(vec != (vector *)NULL) ? vec->z : 0.0f;
}
EXPORT float VecW(vector *vec) {
	return likely(vec != (vector *)NULL) ? vec->w : 0.0f;
}

EXPORT float Vec3Distance(vector *P, vector *Q) {
	return sqrtf((P->x - Q->x)*(P->x - Q->x) +
	             (P->y - Q->y)*(P->y - Q->y) +
	             (P->z - Q->z)*(P->z - Q->z));
}
EXPORT float Vec3DistanceFast(vector *P, vector *Q) {
	return SqrtFast((P->x - Q->x)*(P->x - Q->x) +
	                (P->y - Q->y)*(P->y - Q->y) +
	                (P->z - Q->z)*(P->z - Q->z));
}

//------------------------------------------------------------------------------

EXPORT void ClearChecklist() {
	g_checklist.clear();
}
EXPORT bool_t IsChecklistEmpty() {
	return (bool_t)(g_checklist.size() > 0);
}
EXPORT uint_t CountChecklistItems() {
	return g_checklist.size();
}
EXPORT uint_t AddChecklistItem() {
	size_t i;

	i = g_checklist.size();
	g_checklist.push_back(ChecklistItem());

	return i;
}
EXPORT void SetChecklistItemName(uint_t i, const char *name) {
	if (unlikely(i >= g_checklist.size()))
		return;

	g_checklist[i].name.assign(name);
}
EXPORT void SetChecklistItemInteger(uint_t i, int x, uint_t n) {
	if (unlikely(i >= g_checklist.size() || n >= 4))
		return;

	g_checklist[i].i[n] = x;
}
EXPORT void SetChecklistItemFloat(uint_t i, int x, uint_t n) {
	if (unlikely(i >= g_checklist.size() || n >= 4))
		return;

	g_checklist[i].f[n] = x;
}
EXPORT const char *ChecklistItemName(uint_t i) {
	if (unlikely(i >= g_checklist.size()))
		return (const char *)NULL;

	return g_checklist[i].name.c_str();
}
EXPORT int ChecklistItemInteger(uint_t i, uint_t n) {
	if (unlikely(i >= g_checklist.size()))
		return 0;

	return g_checklist[i].i[n];
}
EXPORT float ChecklistItemFloat(uint_t i, uint_t n) {
	if (unlikely(i >= g_checklist.size()))
		return 0.0f ;

	return g_checklist[i].f[n];
}

//------------------------------------------------------------------------------

EXPORT id_map<void> *NewIDMap() {
	return new id_map<void>();
}
EXPORT id_map<void> *DeleteIDMap(id_map<void> *p) {
	delete p;
	return (id_map<void> *)NULL;
}

EXPORT void IDMapRemoveAll(id_map<void> *p) {
	if unlikely (!p)
		return;

	p->remove_all();
}

EXPORT void IDMapSetDestructor(id_map<void> *p, fnDestructor_t fnDtor) {
	if unlikely (!p)
		return;

	p->set_destructor(fnDtor);
}
EXPORT fnDestructor_t IDMapGetDestructor(id_map<void> *p) {
	if unlikely (!p)
		return (fnDestructor_t)NULL;

	return p->get_destructor();
}

EXPORT int IDMapGetError(id_map<void> *p) {
	if unlikely (!p)
		return 0;

	return p->get_error();
}
EXPORT const char *IDMapGetErrorName(id_map<void> *p) {
	if unlikely (!p)
		return "";

	switch(p->get_error()) {
	case id_map<void>::kErr_None:		return "None";
	case id_map<void>::kErr_NotFound:	return "Not Found";
	case id_map<void>::kErr_NoMemory:	return "No Memory";
	default:							break;
	}

	return "(unknown)";
}
EXPORT void IDMapClearError(id_map<void> *p) {
	if unlikely (!p)
		return;

	p->clear_error();
}

EXPORT void *IDMapGet(id_map<void> *p, unsigned int i) {
	if unlikely (!p)
		return (void *)NULL;

	return p->get(i);
}
EXPORT int IDMapSet(id_map<void> *p, unsigned int i, void *v, int if_exists) {
	if unlikely (!p)
		return 0;

	return (int)p->set(i, v, if_exists ? true : false);
}

EXPORT void IDMapDestruct(id_map<void> *p, unsigned int i) {
	if unlikely (!p)
		return;

	p->destruct(i);
}

//------------------------------------------------------------------------------

EXPORT uint_t BitAnd(uint_t a, uint_t b) {
	return a & b;
}
EXPORT uint_t BitOr(uint_t a, uint_t b) {
	return a | b;
}

//------------------------------------------------------------------------------

EXPORT u32_t AtomicIncrement(volatile u32_t *x) {
	return task::AtomicIncrement(x);
}
EXPORT u32_t AtomicDecrement(volatile u32_t *x) {
	return task::AtomicDecrement(x);
}
EXPORT u32_t AtomicExchange(volatile u32_t *x, u32_t y) {
	return task::AtomicExchange(x, y);
}
EXPORT u32_t AtomicExchangeIfEqual(volatile u32_t *x, u32_t y, u32_t z) {
	return task::AtomicExchangeIfEqual(x, y, z);
}

EXPORT u32_t AtomicAnd(volatile u32_t *x, u32_t y) {
	return task::AtomicAnd(x, y);
}
EXPORT u32_t AtomicOr(volatile u32_t *x, u32_t y) {
	return task::AtomicOr(x, y);
}
EXPORT u32_t AtomicXor(volatile u32_t *x, u32_t y) {
	return task::AtomicXor(x, y);
}

EXPORT u32_t AtomicRead(volatile u32_t *x) {
	return task::AtomicRead(x);
}

EXPORT u32_t AtomicSpinEqual(volatile u32_t *x, u32_t y) {
	return task::AtomicSpinEqual(x, y);
}
EXPORT u32_t AtomicSpinNotEqual(volatile u32_t *x, u32_t y) {
	return task::AtomicSpinNotEqual(x, y);
}

//------------------------------------------------------------------------------

EXPORT HANDLE NewSysEvent(int manualReset, int initialState) {
	return CreateEventA(NULL, manualReset, initialState, (const char *)NULL);
}
EXPORT HANDLE DeleteSysEvent(HANDLE h) {
	if (!h)
		return (HANDLE)NULL;

	CloseHandle(h);
	return (HANDLE)NULL;
}

EXPORT void SysEventSignal(HANDLE h) {
	if unlikely (!h)
		return;

	SetEvent(h);
}
EXPORT void SysEventClear(HANDLE h) {
	if unlikely (!h)
		return;

	ResetEvent(h);
}

EXPORT int SysEventTrySync(HANDLE h) {
	if unlikely (!h)
		return 0;

	if (WaitForSingleObject(h, 0) != WAIT_OBJECT_0)
		return 0;

	return 1;
}
EXPORT void SysEventSync(HANDLE h) {
	if unlikely (!h)
		return;

	WaitForSingleObject(h, INFINITE);
}

//------------------------------------------------------------------------------

EXPORT task::signal_t *NewSignal(unsigned int expCnt) {
	task::signal_t *p;

	if unlikely(!(p = new task::signal_t()))
		return (task::signal_t *)NULL;

	task::InitSignal(*p, expCnt);
	return p;
}

EXPORT task::signal_t *DeleteSignal(task::signal_t *sig) {
	if (!sig)
		return (task::signal_t *)NULL;

	task::FiniSignal(*sig);
	delete sig;
	return (task::signal_t *)NULL;
}

EXPORT void SignalClear(task::signal_t *sig) {
	if unlikely(!sig)
		return;

	task::ResetSignal(*sig);
}
EXPORT void SignalSetSlotCount(task::signal_t *sig, unsigned int expCnt) {
	if unlikely(!sig)
		return;

	task::ResetSignal(*sig, expCnt, true);
}
EXPORT void SignalAddSlot(task::signal_t *sig) {
	if unlikely(!sig)
		return;

	task::AllocSignalSlot(*sig);
}
EXPORT void SignalRaise(task::signal_t *sig) {
	if unlikely(!sig)
		return;

	task::RaiseSignal(*sig);
}

EXPORT void SignalSync(task::signal_t *sig) {
	if unlikely(!sig)
		return;

	task::SyncSignal(*sig);
}
EXPORT int SignalTrySync(task::signal_t *sig) {
	if unlikely(!sig)
		return false;

	return (int)task::TrySyncSignal(*sig);
}

//------------------------------------------------------------------------------

static u32_t g_cpuCores = 0;
static u32_t g_cpuThreads = 0;
static bool g_cpuHyperthreading = false;

inline void QueryCores() {
	if unlikely (g_cpuCores)
		return;

	x86::get_cpu_count(g_cpuCores, g_cpuThreads, g_cpuHyperthreading);
}

EXPORT int IsHyperthreadingAvailable() {
	QueryCores();
	return (int)g_cpuHyperthreading;
}
EXPORT u32_t GetCPUCoreCount() {
	QueryCores();
	return g_cpuCores;
}
EXPORT u32_t GetCPUThreadCount() {
	QueryCores();
	return g_cpuThreads;
}

//------------------------------------------------------------------------------

#if _DEBUG && TASK_CONF_APPHANG
bool g_useAppHangQuery = true;
#else
bool g_useAppHangQuery = false;
#endif

HANDLE g_appHangEvent;
task::thread_t g_appHangThread;
bool g_didAppHangInit = false;

unsigned int g_hangTime = 2; //in seconds

static task::threadResult_t THREADPROCAPI CatchAppHang_f(void *) {
	if (WaitForSingleObject(g_appHangEvent, 1000*g_hangTime)==WAIT_OBJECT_0)
		return (task::threadResult_t)NULL;

#if __GNUC__
	__asm__ __volatile__("int $3");
#else
	__asm { int 3 };
#endif

	return (task::threadResult_t)NULL;
}

void InitAppHangQuery() {
	if likely(!g_useAppHangQuery)
		return;

	if unlikely(g_didAppHangInit)
		return;

	g_appHangEvent = NewSysEvent(0, 0);
	g_appHangThread = task::NewThread(CatchAppHang_f, (void *)NULL);

	g_didAppHangInit = true;
}
void FiniAppHangQuery() {
	if (!g_didAppHangInit)
		return;

	SysEventSignal(g_appHangEvent);
	task::JoinThread(g_appHangThread);
	task::KillThread(g_appHangThread);
	DeleteSysEvent(g_appHangEvent);

	g_didAppHangInit = false;
}

EXPORT void EnableAppHangQuery() {
	g_useAppHangQuery = true;
}
EXPORT void DisableAppHangQuery() {
	g_useAppHangQuery = false;
}
EXPORT int IsAppHangQueryEnabled() {
	return (int)g_useAppHangQuery;
}

EXPORT void SetAppHangQueryTimeout(unsigned int seconds) {
	if (!seconds) {
		DisableAppHangQuery();
		return;
	}

	g_hangTime = seconds;
}
EXPORT unsigned int GetAppHangQueryTimeout() {
	return g_hangTime;
}

//------------------------------------------------------------------------------

task::queue_t g_mainQueue;
task::thread_t g_threads[48];
uint_t g_streamerCount = 0;
bool g_didInit = false;

static task::threadResult_t THREADPROCAPI ActiveStreamer(void *) {
	task::Streamer(g_mainQueue);
	return (task::threadResult_t)NULL;
}

EXPORT void InitStreamers(uint_t count) {
	uint_t i;

	if (g_didInit)
		return;

	if (!count) {
		GetCPUThreadCount();
		count = GetCPUThreadCount();
	}

	if (count > sizeof(g_threads)/sizeof(g_threads[0]))
		count = sizeof(g_threads)/sizeof(g_threads[0]);
	if (count < 2)
		count = 2;

	task::InitQueue(g_mainQueue);

	for(i=1; i<count; i++) {
		g_threads[i] = task::NewThread(ActiveStreamer, (void *)NULL);
		task::SetThreadCPU(g_threads[i], i/(IsHyperthreadingAvailable() + 1));
	}

	g_streamerCount = count;
	g_didInit = true;
}
EXPORT void FiniStreamers() {
	uint_t i;

	if (!g_didInit)
		return;

	InitAppHangQuery();
	task::FiniQueue(g_mainQueue);
	FiniAppHangQuery();

	for(i=0; i<g_streamerCount; i++) {
		task::JoinThread(g_threads[i]);
		task::KillThread(g_threads[i]);
	}

	g_didInit = false;
}

EXPORT uint_t GetStreamerCount() {
	return g_streamerCount;
}

EXPORT void StreamDispatch(jobFunc_t fn, void *p) {
	if unlikely (!g_didInit) {
		fn(p);
		return;
	}

	task::Schedule(g_mainQueue, fn, p);
}
EXPORT void StreamSync() {
	if unlikely (!g_didInit)
		return;

	InitAppHangQuery();
	SyncQueue(g_mainQueue);
	FiniAppHangQuery();
}

//------------------------------------------------------------------------------

EXPORT const char *WinDir() {
	static char buf[4096];
	static bool didinit = false;

	if (likely(didinit))
		return buf;

	GetWindowsDirectoryA(buf, sizeof(buf));
	didinit = true;

	return buf;
}
EXPORT const char *SysDir() {
	static char buf[4096];
	static bool didinit = false;

	if (likely(didinit))
		return buf;

	GetSystemDirectoryA(buf, sizeof(buf));
	didinit = true;

	return buf;
}

EXPORT const char *SysComputerName() {
	static char buf[4096];
	static bool didinit = false;
	DWORD size;

	if likely(didinit)
		return buf;

	size = sizeof(buf);
	if unlikely (!GetComputerNameA(buf, &size))
		return "";

	didinit = true;
	return buf;
}
EXPORT const char *SysUserName() {
	static char buf[4096];
	static bool didinit = false;
	DWORD size;

	if likely(didinit)
		return buf;

	size = sizeof(buf);
	if unlikely (!GetUserNameA(buf, &size))
		return "";

	didinit = true;
	return buf;
}

EXPORT const char *AppDataDir() {
	static char buf[4096];
	static bool didinit = false;

	if likely(didinit)
		return buf;

	if unlikely (!SHGetSpecialFolderPathA((HWND)NULL, buf, CSIDL_APPDATA, TRUE))
		return "";

	didinit = true;
	return buf;
}

//------------------------------------------------------------------------------

//
//	[ UPDATE - 201208131930 ]
//	Should just add path finding into a dedicated A.I. plug-in.
//

//
//	This path finding system (based on A*) has three primary concepts:
//	(1) Maps
//	(2) Agents
//	(3) Routes
//
//	Maps are individual path finding worlds. You could use a single large map,
//	or you could use several separate maps. Path finding can occur on any number
//	of these maps.
//
//	Agents exist in one of these maps. They travel along the map avoiding
//	obstacles in their way.
//
//	Routes are the individual paths agents may follow. Agents may have multiple
//	routes and choose to follow the closest route. Routes can also be updated
//	automatically.
//
//	------------------------------------------------------------------------
//
//	[ -TODO- ]
//
//	Instead of relying on 2D tiles, navmeshes should be used. The algorithm
//	would still work fine, but a node class may have to be written to support
//	both tiles and navmeshes. Additionally, 3D tiles could also be supported.
//	(i.e., instead of eight possible directions, there would be twenty-six.)
//
//	Per-agent obstacles would be helpful as well. That way entire nodes could be
//	masked out in case agents want to avoid things.
//

inline void SortIntegers(unsigned int &x, unsigned int &y) {
	if (y < x) {
		x ^= y;
		y ^= x;
		x ^= y;
	}
}


/*

namespace path {

class agent;
class map;

namespace tile {

enum {
	kFlag_NorthBlocked_Bit = (1<<0),
	kFlag_EastBlocked_Bit = (1<<1),
	kFlag_SouthBlocked_Bit = (1<<2),
	kFlag_WestBlocked_Bit = (1<<3),

	kFlag_NorthEastBlocked_Bit = (1<<4),
	kFlag_SouthEastBlocked_Bit = (1<<5),
	kFlag_SouthWestBlocked_Bit = (1<<6),
	kFlag_NorthWestBlocked_Bit = (1<<7)
};

struct location {
	unsigned int x, y;
};

struct search;

}

class agent {
public:
	class route;

protected:
	friend class route;

	task::lock_t m_routesLock;		//linked lists are not thread safe
	linked_list<route> m_routes;	//one per goal
	route *m_bestRoute;				//best route

	task::signal_t m_finiSig;		//triggered when all routes finish

	map *m_map;

	mutable task::lock_t m_locLock;
	unsigned int m_locID;			//incremented each time location is changed
	tile::location m_curLoc;		//current location

public:
	inline agent(map *m) {
		task::InitLock(m_routesLock);
		m_bestRoute = (route *)NULL;

		task::InitSignal(m_finiSig, 0);

		m_map = m;

		task::InitLock(m_locLock);
		m_locID = 0;
		m_curLoc.x = m_curLoc.y = 0;
	}
	inline ~agent() {
		task::FiniLock(m_locLock);
		task::FiniSignal(m_finiSig);
		task::FiniLock(m_routesLock);
	}

	inline void set_location(const tile::location &loc) {
		task::Lock(m_locLock);
		m_curLoc.x = loc.x;
		m_curLoc.y = loc.y;
		AtomicIncrement(&m_locLock); //atomic because locks aren't used in jobs
		task::Unlock(m_locLock);
	}
	inline void get_location(tile::location &loc) const {
		task::Lock(m_locLock);
		loc.x = m_curLoc.x;
		loc.y = m_curLoc.y;
		task::Unlock(m_locLock);
	}

	inline bool best_route_ready() const {
		if unlikely (m_routes.is_empty())
			return false;

		return task::TrySyncSignal(m_finiSig);
	}
	inline void sync_best_route() {
		if unlikely (m_routes.is_empty())
			return;

		task::SyncSignal(m_finiSig);
	}
	inline route *best_route() {
		sync_best_route();
		return AtomicRead(&m_bestRoute);
	}

	inline map *get_map() const {
		return m_map;
	}
};

class agent::route {
protected:
	friend class agent;

	int m_refCnt;					//current reference count
	bool m_run;						//true whenever the route should be run

	map *m_map;						//the map this route is taking

	agent *m_agent;					//the agent walking this route
	tile::location m_goalLoc;		//the goal of this route

	tile::search *m_openHead;		//open list - head
	tile::search *m_openTail;		//open list - tail
	tile::search *m_closHead;		//closed list - head
	tile::search *m_closTail;		//closed list - tail

	linked_list<tile::location>
		m_final;					//final path
	bool m_pathExists;				//true if there is a path; false otherwise
	HANDLE m_finiEvent;				//triggered when this route finishes

public:
	inline route(agent *agent, const tile::location &goal) {
	}
};

struct tile::search {
	tile::location self;
	tile::location prnt;
	unsigned int f;

	agent::route *route;

	struct tile::search *prev, *next;
	struct tile::search **p_head, **p_tail;
};

class map {
protected:
	friend class agent::route;

	unsigned int m_width, m_height;
	unsigned char *m_tiles; //mask of tile::kFlag_*s.

	inline unsigned char &locate_tile(tile::location &loc) {
		return m_tiles[(loc.y%m_height)*m_width + (loc.x%m_width)];
	}

public:
	inline unsigned int distance(tile::location &from,
	tile::location &to) const {
		unsigned int x1,y1, x2,y2;

		x1 = from.x;
		y1 = from.y;
		x2 =   to.x;
		y2 =   to.y;

		SortIntegers(x1, x2);
		SortIntegers(y1, y2);

		return (x2 - x1) + (y2 - y1);
	}

	inline map(): m_width(0), m_height(0), m_tiles((unsigned char *)NULL) {
	}
	inline ~map() {
		clear();
	}

	inline bool resize(unsigned int w, unsigned int h) {
		unsigned char *p;
		unsigned int x, y;

		if (!(w*h)) {
			clear();
			return true;
		}

		if unlikely(!(p = new unsigned char[w*h]))
			return false;

		memset((void *)p, 0, w*h);

		if (m_tiles) {
			for(x=0; x<m_width && x<w; x++) {
				for(y=0; y<m_height && y<h; y++)
					p[y*h + x] = m_tiles[y*m_width + x];
			}

			delete [] m_tiles;
		}

		m_tiles  = p;
		m_width  = w;
		m_height = h;

		return true;
	}
	inline void clear() {
		delete [] m_tiles;
		m_tiles = (unsigned char *)NULL;

		m_width = 0;
		m_height = 0;
	}

	inline unsigned int width() const {
		return m_width;
	}
	inline unsigned int height() const {
		return m_height;
	}

	inline unsigned char get_tile(unsigned int x, unsigned int y) const {
		if unlikely(!m_tiles)
			return 0;

		return m_tiles[(y%m_height)*m_width + (x%m_width)];
	}
	inline void set_tile(unsigned int x, unsigned int y, unsigned char c) {
		if unlikely(!m_tiles)
			return;

		m_tiles[(y%m_height)*m_width + (x%m_width)] = c;
	}

	inline void fill_walkable() {
		if unlikely(!m_tiles)
			return;

		memset(m_tiles, 0, m_width*m_height);
	}
	inline void fill_blocked() {
		if unlikely(!m_tiles)
			return;

		memset(m_tiles, 0xFF, m_width*m_height);
	}
};

}

EXPORT path::map *NewMap(unsigned int w, unsigned int h, int unwalkable) {
	path::map *m;

	m = new path::map();
	if unlikely(!m)
		return (path::map *)NULL;

	if unlikely(!m->resize(w, h)) {
		delete p;
		return (path::map *)NULL;
	}

	if (unwalkable)
		m->fill_blocked();

	return m;
}
EXPORT path::map *DeleteMap(path::map *m) {
	delete m;
	return (path::map *)NULL;
}

EXPORT int MapResize(path::map *m, unsigned int w, unsigned int h) {
	if unlikely(!m)
		return 0;

	return (int)m->resize(w, h);
}

EXPORT unsigned int MapSizeX(path::map *m) {
	if unlikely(!m)
		return 0;

	return m->width();
}
EXPORT unsigned int MapSizeZ(path::map *m) {
	if unlikely(!m)
		return 0;

	return m->height();
}

EXPORT unsigned char MapGetTile(path::map *m, unsigned int x, unsigned int z) {
	if unlikely(!m)
		return 0;

	return m->get_tile(x, z);
}
EXPORT void MapSetTile(path::map *m, unsigned int x, unsigned int z,
unsigned char c) {
	if unlikely(!m)
		return;

	m->set_tile(x, z, c);
}
EXPORT void MapFillWalkable(path::map *m) {
	if unlikely(!m)
		return;

	m->fill_walkable();
}
EXPORT void MapFillBlocked(path::map *m) {
	if unlikely(!m)
		return;

	m->fill_blocked();
}

EXPORT path::agent *NewAgent(path::map *m) {
	path::agent *a;

	if unlikely(!m)
		return;

	a = new path::agent(m);

	return a;
}
EXPORT path::agent *DeleteAgent(path::agent *a) {
	delete a;
	return (path::agent *)NULL;
}

EXPORT void AgentSetLocation(path::agent *a, unsigned int x, unsigned int z) {
	path::tile::location loc;

	if unlikely(!a)
		return;

	loc.x = x;
	loc.y = z;

	a->set_location(loc);
}
EXPORT void AgentGetLocation(path::agent *a, unsigned int *x, unsigned int *z) {
	path::tile::location loc;

	if unlikely(!a) {
		if likely(x) *x = 0;
		if likely(z) *z = 0;

		return;
	}

	a->get_location(loc);

	if likely(x) *x = loc.x;
	if likely(z) *z = loc.y;
}
EXPORT unsigned int AgentLocationX(path::agent *a) {
	unsigned int x;

	AgentGetLocation(a, &x, (unsigned int *)NULL);

	return x;
}
EXPORT unsigned int AgentLocationZ(path::agent *a) {
	unsigned int z;

	AgentGetLocation(a, (unsigned int *)NULL, &z);

	return z;
}

EXPORT int AgentIsBestRouteReady(path::agent *a) {
	if unlikely(!a)
		return 0;

	return (int)a->best_route_ready();
}
EXPORT void AgentWaitBestRoute(path::agent *a) {
	if unlikely(!a)
		return;

	a->sync_best_route();
}
EXPORT path::agent::route *AgentBestRoute(path::agent *a) {
	if unlikely(!a)
		return (path::agent::route *)NULL;

	return a->best_route();
}

EXPORT path::map *AgentMap(path::agent *a) {
	if unlikely(!a)
		return (path::map *)NULL;

	return a->get_map();
}

//*/
