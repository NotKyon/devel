﻿LoliOS
======

General
-------

Microkernel architecture, with mostly minimal kernel-level code.

Parts of the system should be able to restart without requiring other parts to
restart. (Concept of processes running in versions.)

Extensive use of the virtual file system.

64-bit exclusive.


Permissions
-----------

Goals:

- Ability to manage individual capabilities and role hierarchies.
- Processes receive their own permissions, virtual users, etc.
- Concept of multiple system administrators (such that some deal with specific
  aspects of the system).

Non-goals:

- "Root" (The "root" user is often required to do changes on *nix systems, even
  when certain actions shouldn't really require that access from a high-level
  perspective; for example, a user connecting to the Internet.)


Terms / Glossary
----------------

- "Program": GUI-based program sandbox
- "Command": Console-based program sandbox


File System Layout
------------------

Base ideas taken from:
http://www.osnews.com/story/19711/The_Utopia_of_Program_Management

(Other inspiration comes from Plan9 and BeOS)

Support for "live queries" are intended, such that users can make their own
query-based directories.

Files can have tags applied to them. Virtual "tag" directories can be created
that take all files of a given tag or expression and put them in that directory.

Overlay/union file systems may have two mutually exclusive per-entry attributes:

- **override**: This file must be overridden. If no file exists in the directory
  above, then this file is not available.
- **final**: This file can *not* be overriden. If a file with the same name
  exists in the above file system, then this file overrides it.

(Those terms are selected for parity with the equivalent C++11 concepts.)

This allows certain system files to be kept.

VFS -- view a user gets:

- `/`: The root of the virtual file system.
	- `/System`: Stores OS files which may be overwritten on upgrade / reinstall.
		- `/System/Boot`: Bootloader, kernel(s), etc.
		- `/System/Keys`: Public keys to verify various system files.
			- This is a virtual directory. The keys might be hard-coded, or come
			  from a trusted source, such as a TPM chip.
		- `/System/Settings`: Base OS settings.
		- `/System/Programs`: GUI-based programs. (e.g., terminal emulator)
		- `/System/Commands`: Console-based programs.
		- `/System/Libraries`: Dynamic libraries.
		- `/System/Drivers`: System drivers, such as the scheduler or VFS.
		- `/System/Daemons`: System daemons, such as a spooler.
		- `/System/Documentation`: Documentation for the system.
		- `/System/Devices`: Equivalent of `/dev` in *nix systems.
			- `/System/Devices/Null`: Null device (`/dev/null` in *nix.)
			- `/System/Devices/Swap`: Virtual swap space device
			- `/System/Devices/Internal`: Internal devices
				- `Drives/Disk<N>`: Hard disk drive *N*
				- `Drives/Floppy<N>`: Floppy
				- `Drives/Disc<N>`: CD, DVD, BD
				- `Partitions/Disk<N>.<M>`: Specific partition
				- `Adapters/GPU<N>`: Video card *N*
				- `Adapters/Monitor<N>`: Monitor *N*
				- `Controllers/USBHub<N>`: USB Hub *N*
				- `Controllers/PCI<N>`: PCI device *N*
				- `Networking/Ethernet<N>`: Ethernet device *N*
			- `/System/Devices/External`: External (attached) devices
				- `Drives/Disk<N>`: External hard drive *N*
				- `Drives/Thumb<N>`: External thumb drive *N*
				- `Drives/Disc<N>`: External (e.g., via USB) disc drive
				- `Partitions/Disk<N>.<M>`: Specific partition `Disk<N>`
				- `Partitions/Thumb<N>.<M>`: Specific partition of `Thumb<N>`
				- `Networking/Tether<N>`: Tethering device *N*
			- `/System/Devices/Input`: Human interface devices
				- `Keyboard<N>`: An attached keyboard
				- `Mouse<N>`: An attached mouse
				- `Tablet<N>`: Touch device
		- `/System/Processes`: Equivalent of `/proc` in *nix systems.
			- `/System/Processes/All`: All processes
			- `/System/Processes/Users/$USER`: Processes of "$USER"
			- `/System/Processes/User`: Processes owned by this user
	- `/Users`: Stores individual users
		- `/Users/$USR`: Home directory for "$USR" (whoever that is)
			- `/Users/$USR/Settings`: User settings
			- Desktop, Documents, Downloads, Music, Pictures, Videos
	- `/Installed`: Installations of various packages go here.
		- `/Installed/Programs`: Installed GUI programs.
		- `/Installed/Commands`: Installed console commands.
		- `/Installed/Libraries`: Installed libraries.
		- `/Installed/Drivers`: Installed drivers, such as a GPU driver.
		- `/Installed/Daemons`: Installed daemons.
		- `/Installed/Kits`: Installed development kits.
			- `/Installed/Kits/$KIT/$TARGET`: Target that this kit supports.
		- `/Installed/Documentation`: Documentation for installed programs
	- `/Development`: Virtual directory for development-related items.
		- `/Development/Host`: Virtual directory for targeting the host platform.
		- `/Development/$TARGET`: Alternate view of `/Installed/Kits` where
		  the "$TARGET" subdirectory is used for each kit. e.g., instead of
		  `/Installed/Kits/Live2D/Android-ARM32/`, you would have
		  `/Development/Android-ARM32/Live2D/` as an alias.
			- `/Development/$TARGET/C`: Umbrella for C-related files.
				- `/Development/$TARGET/C/include`: Union of include directories.
				- `/Development/$TARGET/C/lib`: Union of library directories.
			- `/Development/$TARGET/Builds`: Where software builds go.
	- `/Devices`: Mount-directory for devices attached to the computer.
	- `/Virtual`: Host to many virtual directories
		- `Settings`: Union of (top: `/Users/$USR/Settings`, and bottom: `/System/Settings`)
		- `Programs`: Union of (top: `/Installed/Programs`, and bottom: `/System/Programs`)
		- `Commands`: Union of (top: `/Installed/Commands`, and bottom: `/System/Commands`)
		- `Binaries`: Union of (top: `/Virtual/Commands`, and bottom: `/Virtual/Programs`)
			- NOTE: "PATH" is set to `/Virtual/Binaries` by default.
		- `Libraries`: Union of (top: `/Installed/Libraries`, and bottom: `/System/Libraries`)
		- `Logs`: General logging directory.
	- `/Documentation`: Union of (top: `/Installed/Documentation`, and bottom: `/System/Documentation`)
	- `/Temp` *hidden*: Temporary files.

VFS -- view a program/command gets:

- `/`: The root of the virtual file system.
	- `/Base`: Alias of the directory the program is installed to.
	- `/Data`: Data directory the program has access to.
		- `/Data/Local`: Local data. (Saved only to this device.)
		- `/Data/Roaming`: Roaming data. (Follows user.)
		- `/Data/Cache`: Cache-only data.
		- `/Data/Temp`: Temporary data.
	- `/Permissions`: Virtual directory containing the active permission set
		- A program can request more permission by creating an entry here, and
		  specifying the details in that file.
	- `/UI`: Umbrella directory for user interface systems.
		- `/UI/Listing`: Listing of various UI components.
			- `MainConsole`: Primary console for the program.
			- `Windows/<N>`: Window *N*'s directory
				- `Widgets`: Directory containing this window's widgets
				- `Window`: Actual window
				- `Console`: Window's specific console
				- `Keyboard`: Keyboard input for this window
				- `Mouse`: Mouse input for this window
		- `/UI/Window`: Window for current thread. (Each thread gets its own
		  window, opt-in. Main thread of a program gets one by default.)
			- Perform file writes directly to the window to create its layout.
			- For console programs and other background services, this is a
			  symlink to `/System/Devices/Null`.
		- `/UI/Console`: Console for the current window, or the thread.
			- Each *window* has its own console.
		- `/UI/Keyboard`: Keyboard input for the current window.
		- `/UI/Mouse`: Mouse input for the current window.


Initialization
--------------

**Boot**

1. The bootloader takes control, then verifies that the kernel it is loading is
   has been appropriately signed. (Always necessary.)
	- The bootloader also verifies that the BIOS is trusted.
2. The kernel takes control from the bootloader, then verifies that the
   bootloader has been appropriately signed. (Always necessary.)
3. Core system drivers (scheduler, VFS, etc) are loaded and initialized.
	- A "BIOS checker" driver is loaded, which monitors BIOS memory for changes
	- A basic video driver prepares the graphical video mode.
4. If not in recovery mode, installed drivers are loaded and initialized.
	- The user's video driver, if available, takes over the video mode.
5. The initialization program is verified and initialized.


**Initialization Program**

`/System/Boot/Stage3/Initialize`

1. Begin asynchronously preloading common startup programs. (Such as the window
   manager.)
2. Present the login prompt.
3. Once the user has logged in, run that user's specific startup script.


**Window Manager**

The window manager is `/System/Drivers/WindowManager`. It gives per-thread views
of windows, where each window also has its own console and input devices.


Memory Services and Swap
------------------------

**Encrypted Memory**

This is a per-process optional feature, with an "opt-in to on-by-default"
setting.

- When memory pages are allocated, they get encrypted. When a program tries to
  access a memory page, a page fault occurs and the system decrypts that memory
  page, then resumes the program with the decrypted content.
	- Decrypted pages will be re-encrypted at later points in time.
	- Keys not stored in RAM, but in certain CPU registers that never touch RAM.
		- When a key does touch RAM, that area of RAM gets scrubbed immediately,
		  and the process responsible for the encryption/decryption cannot be
		  locked, debugged, or otherwise investigated during that time.
- Memory that hits the swap disk is **ALWAYS** encrypted, regardless of whether
  per-process encryption is enabled or not.

**Compressed Memory**

Before sending pages to the swap disk, the system will try to compress memory
(if it makes sense to do so). If the compression frees up enough space, then the
compressed pages data will not be sent to swap. Instead, a page fault will be
triggered upon attempted access of that memory, which will trigger the
decompressor.
