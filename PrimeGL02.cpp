﻿//PrimeGL02

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <gl/gl.h>
#include <gl/glu.h>
//#include <gl/glut.h> //キョン修正：don't have GLUT


#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>



LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
void EnableOpenGL(HWND hwnd, HDC*, HGLRC*);
void DisableOpenGL(HWND, HDC, HGLRC);

#include <math.h>


#define KEY_ESCAPE 27

using namespace std;

/************************************************************************
  Window
 ************************************************************************/

typedef struct {
    int width;
	int height;
	char* title;

	float field_of_view_angle;
	float z_near;
	float z_far;
} glutWindow;



/***************************************************************************
  OBJ Loading // http://openglsamples.sourceforge.net/obj.html
 ***************************************************************************/

class Model_OBJ
{
  public:
	Model_OBJ();
    float* calculateNormal(float( &norm )[ 3 ], const float* coord1, const float* coord2, const float* coord3 );
    int  Load(const char *filename);	// Loads the model
	void  Draw();					// Draws the model on the screen
	int  Draw2();
	void  Release();				// Release the model

	float* normals;							// Stores the normals
    float* Faces_Triangles;					// Stores the triangles
	float* vertexBuffer;					// Stores the points which make the object
	long TotalConnectedPoints;				// Stores the total number of connected verteces
	long TotalConnectedTriangles;			// Stores the total number of connected triangles

};


#define POINTS_PER_VERTEX 3
#define TOTAL_FLOATS_IN_TRIANGLE 9
using namespace std;

Model_OBJ::Model_OBJ()
{
	this->TotalConnectedTriangles = 0;
	this->TotalConnectedPoints = 0;
}

// キョン修正：need to take 'norm' (output) as parameter; use 'const' to signify inputs rather than outputs
float* Model_OBJ::calculateNormal( float( &norm )[ 3 ], const float *coord1, const float *coord2, const float *coord3 )
{
	/* calculate Vector1 and Vector2 */
	float va[3], vb[3], vr[3], val;
	va[0] = coord1[0] - coord2[0];
	va[1] = coord1[1] - coord2[1];
	va[2] = coord1[2] - coord2[2];

	vb[0] = coord1[0] - coord3[0];
	vb[1] = coord1[1] - coord3[1];
	vb[2] = coord1[2] - coord3[2];

	/* cross product */
	vr[0] = va[1] * vb[2] - vb[1] * va[2];
	vr[1] = vb[0] * va[2] - va[0] * vb[2];
	vr[2] = va[0] * vb[1] - vb[0] * va[1];

	/* normalization factor */
	val = sqrt( vr[0]*vr[0] + vr[1]*vr[1] + vr[2]*vr[2] );

	// キョン修正：始まる
	// can't return a local array
	norm[0] = vr[0]/val;
	norm[1] = vr[1]/val;
	norm[2] = vr[2]/val;
	// キョン修正：終わり


	return norm;
}


template <typename T>
  string NumberToString ( T Number )
  {
     ostringstream ss;
     ss << Number;
     return ss.str();
  }

int Model_OBJ::Load(const char* filename)
{
	std::string line;
	ifstream objFile (filename);
	if (objFile.is_open())													// If obj file is open, continue
	{
		objFile.seekg (0, ios::end);										// Go to end of the file,
		long fileSize = objFile.tellg();									// get file size
		objFile.seekg (0, ios::beg);										// we'll use this to register memory for our 3d model

		vertexBuffer = (float*) malloc (fileSize);							// Allocate memory for the verteces
		Faces_Triangles = (float*) malloc(fileSize*sizeof(float));			// Allocate memory for the triangles
		normals  = (float*) malloc(fileSize*sizeof(float));					// Allocate memory for the normals

		int triangle_index = 0;												// Set triangle index to zero
		int normal_index = 0;

		int vertCount =0;
		int trisCount =0;									// Set normal index to zero

		while (! objFile.eof() )											// Start reading file data
		{
			getline (objFile,line);											// Get line from file

			if ((line.c_str()[0] == 'v') && (line.c_str()[1] == ' '))		// The first character is a v: on this line is a vertex stored.
			{                                                               // be sure first 2 chr 'v '
				line[0] = ' ';												// Set first character to 0. This will allow us to use sscanf

				sscanf(line.c_str(),"%f %f %f ",							// Read floats from the line: v X Y Z
					&vertexBuffer[TotalConnectedPoints],
					&vertexBuffer[TotalConnectedPoints+1],
					&vertexBuffer[TotalConnectedPoints+2]);

				/*
				cout << "V:" + NumberToString ( vertCount )
                    + ": " + NumberToString ( vertexBuffer[TotalConnectedPoints] )
                    + "," + NumberToString ( vertexBuffer[TotalConnectedPoints+1] )
                    + "," + NumberToString ( vertexBuffer[TotalConnectedPoints+2] )
                    +  char(10) ;
                */
				TotalConnectedPoints += POINTS_PER_VERTEX;	                // Add 3 to the total connected points
				vertCount++;

			}
			if ((line.c_str()[0] == 'f') && (line.c_str()[1] == ' '))   	// The first character is an 'f': on this line is a point stored
			{
		    	line[0] = ' ';												// Set first character to 0. This will allow us to use sscanf

				int vertexNumber[4] = { 0, 0, 0 };
                sscanf(line.c_str(),"%i%i%i",								// Read integers from the line:  f 1 2 3
					&vertexNumber[0],										// First point of our triangle. This is an
					&vertexNumber[1],										// pointer to our vertexBuffer list
					&vertexNumber[2] );										// each point represents an X,Y,Z.

				vertexNumber[0] -= 1;										// OBJ file starts counting from 1
				vertexNumber[1] -= 1;										// OBJ file starts counting from 1
				vertexNumber[2] -= 1;										// OBJ file starts counting from 1


				/*
				cout << "F:" + NumberToString ( trisCount )
                    + ": " + NumberToString ( vertexNumber[0] )
                    + "," + NumberToString ( vertexNumber[1] )
                    + "," + NumberToString ( vertexNumber[2] )
                    +  char(10) ;
                 */

				/********************************************************************
				 * Create triangles (f 1 2 3) from points: (v X Y Z) (v X Y Z) (v X Y Z).
				 * The vertexBuffer contains all verteces
				 * The triangles will be created using the verteces we read previously
				 */

				int tCounter = 0;
				for (int i = 0; i < POINTS_PER_VERTEX; i++)
				{
					Faces_Triangles[triangle_index + tCounter   ] = vertexBuffer[3*vertexNumber[i] ];
					Faces_Triangles[triangle_index + tCounter +1 ] = vertexBuffer[3*vertexNumber[i]+1 ];
					Faces_Triangles[triangle_index + tCounter +2 ] = vertexBuffer[3*vertexNumber[i]+2 ];
					tCounter += POINTS_PER_VERTEX;
				}

				/*********************************************************************
				 * Calculate all normals, used for lighting
				 */
				float coord1[3] = { Faces_Triangles[triangle_index], Faces_Triangles[triangle_index+1],Faces_Triangles[triangle_index+2]};
				float coord2[3] = {Faces_Triangles[triangle_index+3],Faces_Triangles[triangle_index+4],Faces_Triangles[triangle_index+5]};
				float coord3[3] = {Faces_Triangles[triangle_index+6],Faces_Triangles[triangle_index+7],Faces_Triangles[triangle_index+8]};
				float norm[ 3 ];
				calculateNormal( /*out*/norm, /*in*/coord1, coord2, coord3 );

				tCounter = 0;
				for (int i = 0; i < POINTS_PER_VERTEX; i++)
				{
					normals[normal_index + tCounter ] = norm[0];
					normals[normal_index + tCounter +1] = norm[1];
					normals[normal_index + tCounter +2] = norm[2];
					tCounter += POINTS_PER_VERTEX;
				}

				triangle_index += TOTAL_FLOATS_IN_TRIANGLE;
				normal_index += TOTAL_FLOATS_IN_TRIANGLE;
				TotalConnectedTriangles += TOTAL_FLOATS_IN_TRIANGLE;
				trisCount++;
			}
		}
		objFile.close();														// Close OBJ file
		cout << "Load obj file done : Tris:" +   NumberToString ( TotalConnectedTriangles/ TOTAL_FLOATS_IN_TRIANGLE )  +  char(10) ;
		cout << "Load obj file done : Verts:" +   NumberToString ( vertCount )   +  char(10) ;
		cout << "triangle_index:" +   NumberToString ( triangle_index )  +  char(10) ;
	}
	else
	{
		cout << "Unable to open file"+  char(10) ;
		return 1;
	}
	return 0;
}

void Model_OBJ::Release()
{
	free(this->Faces_Triangles);
	free(this->normals);
	free(this->vertexBuffer);
}

void Model_OBJ::Draw()
{
	// キョン修正：始まる - want some debugging information
#ifdef _DEBUG
	static bool didStat = false;
	if( !didStat ) {
		didStat = true;
		printf( "TOTAL CONNECTED POINTS = %i\n", ( int )TotalConnectedPoints );
		printf( "TOTAL CONNECTED TRIANGLES = %i\n", ( int )TotalConnectedTriangles );
		fflush( stdout );
	}
#endif
	// キョン修正：終わり

 	glEnableClientState(GL_VERTEX_ARRAY);						// Enable vertex arrays
 	glEnableClientState(GL_NORMAL_ARRAY);						// Enable normal arrays
	glVertexPointer(3,GL_FLOAT,	0,Faces_Triangles);				// Vertex Pointer to triangle array
	glNormalPointer(GL_FLOAT, 0, normals);						// Normal pointer to normal array
	// キョン修正：始まる - testing something -- revert back if this breaks anything
	//glDrawArrays(GL_TRIANGLES, 0, TotalConnectedTriangles);		// Draw the triangles
	glDrawArrays(GL_TRIANGLES, 0, TotalConnectedTriangles/3);		// Draw the triangles
	// キョン修正：終わり
	glDisableClientState(GL_VERTEX_ARRAY);						// Disable vertex arrays
	glDisableClientState(GL_NORMAL_ARRAY);						// Disable normal arrays
}

int Model_OBJ::Draw2()
{//  debug proved  the tris data is in the buffer .
    int trisRendred = 0 ;
    //cout << "Draw2 ~ :" +   NumberToString ( TotalConnectedTriangles )  +  char(10) ;
    for (int i = 0; i < TotalConnectedTriangles  ; i = i + 9 )
    {
        //cout << "Draw2 :: :" +   NumberToString ( i )  +  char(10) ;
        //int tri_index = i ;

        glColor4f(1.0f, 0.0f, 0.0f, 1.0f );
        glVertex3f( Faces_Triangles[0 + i] ,Faces_Triangles[1 + i] , Faces_Triangles[2 + i]);
        glColor4f(0.0f, 1.0f, 0.0f, 1.0f );
        glVertex3f( Faces_Triangles[3 + i] ,Faces_Triangles[4 + i] , Faces_Triangles[5 + i]);
        glColor4f(0.0f, 0.0f, 1.0f, 1.0f );
        glVertex3f( Faces_Triangles[6 + i] ,Faces_Triangles[7 + i] , Faces_Triangles[8 + i]);

        trisRendred++ ;


    }

    //cout << "Draw2 :: :" +   NumberToString ( trisRendred )  +  char(10) ;

    return trisRendred ;

}

/***************************************************************************
 * Program code
 ***************************************************************************/

Model_OBJ obj;
float g_rotation;


// キョン修正：始まる
namespace KYON
{

	//=======================================================================//
	//
	//	MATH
	//
	//=======================================================================//

	float Degrees(float x) { return x/3.1415926535f*180.0f; }
	float Radians(float x) { return x/180.0f*3.1415926535f; }

	float Cos(float x) { return cosf(Radians(x)); }
	float Sin(float x) { return sinf(Radians(x)); }
	float Tan(float x) { return tanf(Radians(x)); }

	float ACos(float x) { return Degrees(acosf(x)); }
	float ASin(float x) { return Degrees(asinf(x)); }
	float ATan(float x) { return Degrees(atanf(x)); }
	float ATan2(float y, float x) { return Degrees(atan2f(y, x)); }

	float Sqrt(float x) { return sqrtf(x); }
	float InvSqrt(float x) { return 1.0f/sqrtf(x); }

	//=======================================================================//
	//
	//	MATRIX
	//
	//=======================================================================//

	//
	//	PROJECTION
	//

	// Load a perspective matrix
	void LoadPerspective(float( &M )[ 16 ], float fov, float aspect, float zn, float zf) {
		float a, b, c, d;

		b = 1.0f/Tan(0.5f*fov);
		a = b/aspect;

		c =     zf/(zf - zn);
		d = -zn*zf/(zf - zn);

		M[ 0]=a; M[ 4]=0; M[ 8]=0; M[12]=0;
		M[ 1]=0; M[ 5]=b; M[ 9]=0; M[13]=0;
		M[ 2]=0; M[ 6]=0; M[10]=c; M[14]=d;
		M[ 3]=0; M[ 7]=0; M[11]=1; M[15]=0;
	}
	// Load an orthographic matrix
	void LoadOrtho(float( &M )[ 16 ], float l, float r, float b, float t, float zn, float zf) {
		float A, B, C, D, E, F;

		A =    2.0f/(r  -  l);
		B = (l + r)/(l  -  r);
		C =    2.0f/(t  -  b);
		D = (t + b)/(b  -  t);
		E =    1.0f/(zf - zn);
		F =      zn/(zn - zf);

		M[ 0]=A; M[ 4]=0; M[ 8]=0; M[12]=B;
		M[ 1]=0; M[ 5]=C; M[ 9]=0; M[13]=D;
		M[ 2]=0; M[ 6]=0; M[10]=E; M[14]=F;
		M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
	}

	//
	//	GENERAL
	//

	void LoadIdentity(float( &M )[ 16 ]) {
		M[ 0]=1; M[ 4]=0; M[ 8]=0; M[12]=0;
		M[ 1]=0; M[ 5]=1; M[ 9]=0; M[13]=0;
		M[ 2]=0; M[ 6]=0; M[10]=1; M[14]=0;
		M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
	}

	void LoadTranslation(float( &M )[ 16 ], float x, float y, float z) {
		M[ 0]=1; M[ 4]=0; M[ 8]=0; M[12]=x;
		M[ 1]=0; M[ 5]=1; M[ 9]=0; M[13]=y;
		M[ 2]=0; M[ 6]=0; M[10]=1; M[14]=z;
		M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
	}
	void LoadRotation(float( &M )[ 16 ], float x, float y, float z) {
		float sx,sy,sz, cx,cy,cz;
		float cz_nsx;

		sx=Sin(x); sy=Sin(y); sz=Sin(z);
		cx=Cos(x); cy=Cos(y); cz=Cos(z);

		cz_nsx = cz*-sx;

		M[ 0]= cz*cy;              M[ 4]=-sz*cx;  M[ 8]= cz*-sy + (sz*sx)*cy;
		M[ 1]= sz*cy + cz_nsx*sy;  M[ 5]= cz*cx;  M[ 9]= sz*-sy + cz_nsx*cy;
		M[ 2]= cx*sy;              M[ 6]= sx;     M[10]= cx*cy;

								   M[12]=0;
								   M[13]=0;
								   M[14]=0;
		M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
	}
	void LoadScaling(float( &M )[ 16 ], float x, float y, float z) {
		M[ 0]=x; M[ 4]=0; M[ 8]=0; M[12]=0;
		M[ 1]=0; M[ 5]=y; M[ 9]=0; M[13]=0;
		M[ 2]=0; M[ 6]=0; M[10]=z; M[14]=0;
		M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
	}

	void ApplyTranslation(float( &M )[ 16 ], float x, float y, float z) {
		M[12] += M[ 0]*x + M[ 4]*y + M[ 8]*z;
		M[13] += M[ 1]*x + M[ 5]*y + M[ 9]*z;
		M[14] += M[ 2]*x + M[ 6]*y + M[10]*z;
	}
	void ApplyXRotation(float( &M )[ 16 ], float x) {
		float c, s, t;

		c = Cos(x);
		s = Sin(x);

		t = M[ 4];
		M[ 4] = t*c  + M[ 8]*s;
		M[ 8] = t*-s + M[ 8]*c;

		t = M[ 5];
		M[ 5] = t*c  + M[ 9]*s;
		M[ 9] = t*-s + M[ 9]*c;

		t = M[ 6];
		M[ 6] = t*c  + M[10]*s;
		M[10] = t*-s + M[10]*c;
	}
	void ApplyYRotation(float( &M )[ 16 ], float y) {
		float c, s, t;

		c = Cos(y);
		s = Sin(y);

		t = M[ 0];
		M[ 0] = t*c  + M[ 8]*s;
		M[ 8] = t*-s + M[ 8]*c;

		t = M[ 1];
		M[ 1] = t*c  + M[ 9]*s;
		M[ 9] = t*-s + M[ 9]*c;

		t = M[ 2];
		M[ 2] = t*c  + M[10]*s;
		M[10] = t*-s + M[10]*c;
	}
	void ApplyZRotation(float( &M )[ 16 ], float z) {
		float c, s, t;

		c = Cos(z);
		s = Sin(z);

		t = M[ 0];
		M[ 0] = t*c  + M[ 4]*s;
		M[ 4] = t*-s + M[ 4]*c;

		t = M[ 1];
		M[ 1] = t*c  + M[ 5]*s;
		M[ 5] = t*-s + M[ 5]*c;

		t = M[ 2];
		M[ 2] = t*c  + M[ 6]*s;
		M[ 6] = t*-s + M[ 6]*c;
	}
	void ApplyRotation(float( &M )[ 16 ], float x, float y, float z) {
		ApplyZRotation(M, z);
		ApplyXRotation(M, x);
		ApplyYRotation(M, y);
	}

	void LoadAffineInverse(float( &M )[ 16 ], const float *P) {
		float x, y, z;

		x = -(P[12]*P[ 0] + P[13]*P[ 1] + P[14]*P[ 2]);
		y = -(P[12]*P[ 4] + P[13]*P[ 5] + P[14]*P[ 6]);
		z = -(P[12]*P[ 8] + P[13]*P[ 9] + P[14]*P[10]);

		M[ 0]=P[ 0]; M[ 4]=P[ 1]; M[ 8]=P[ 2]; M[12]=x;
		M[ 1]=P[ 4]; M[ 5]=P[ 5]; M[ 9]=P[ 6]; M[13]=y;
		M[ 2]=P[ 8]; M[ 6]=P[ 9]; M[10]=P[10]; M[14]=z;
		M[ 3]=   0 ; M[ 7]=   0 ; M[11]=   0 ; M[15]=1;
	}
	void AffineMultiply(float( &M )[ 16 ], const float *P, const float *Q) {
		M[ 0]=P[ 0]*Q[ 0] + P[ 4]*Q[ 1] + P[ 8]*Q[ 2];
		M[ 4]=P[ 0]*Q[ 4] + P[ 4]*Q[ 5] + P[ 8]*Q[ 6];
		M[ 8]=P[ 0]*Q[ 8] + P[ 4]*Q[ 9] + P[ 8]*Q[10];
		M[12]=P[ 0]*Q[12] + P[ 4]*Q[13] + P[ 8]*Q[14] + P[12];

		M[ 1]=P[ 1]*Q[ 0] + P[ 5]*Q[ 1] + P[ 9]*Q[ 2];
		M[ 5]=P[ 1]*Q[ 4] + P[ 5]*Q[ 5] + P[ 9]*Q[ 6];
		M[ 9]=P[ 1]*Q[ 8] + P[ 5]*Q[ 9] + P[ 9]*Q[10];
		M[13]=P[ 1]*Q[12] + P[ 5]*Q[13] + P[ 9]*Q[14] + P[13];

		M[ 2]=P[ 2]*Q[ 0] + P[ 6]*Q[ 1] + P[10]*Q[ 2];
		M[ 6]=P[ 2]*Q[ 4] + P[ 6]*Q[ 5] + P[10]*Q[ 6];
		M[10]=P[ 2]*Q[ 8] + P[ 6]*Q[ 9] + P[10]*Q[10];
		M[14]=P[ 2]*Q[12] + P[ 6]*Q[13] + P[10]*Q[14] + P[14];

		M[ 3]=0;
		M[ 7]=0;
		M[11]=0;
		M[15]=1;
	}
	void Multiply3(float( &M )[ 16 ], const float *P, const float *Q) {
		M[ 0]=P[ 0]*Q[ 0] + P[ 4]*Q[ 1] + P[ 8]*Q[ 2];
		M[ 4]=P[ 0]*Q[ 4] + P[ 4]*Q[ 5] + P[ 8]*Q[ 6];
		M[ 8]=P[ 0]*Q[ 8] + P[ 4]*Q[ 9] + P[ 8]*Q[10];
		M[12]=0;

		M[ 1]=P[ 1]*Q[ 0] + P[ 5]*Q[ 1] + P[ 9]*Q[ 2];
		M[ 5]=P[ 1]*Q[ 4] + P[ 5]*Q[ 5] + P[ 9]*Q[ 6];
		M[ 9]=P[ 1]*Q[ 8] + P[ 5]*Q[ 9] + P[ 9]*Q[10];
		M[13]=0;

		M[ 2]=P[ 2]*Q[ 0] + P[ 6]*Q[ 1] + P[10]*Q[ 2];
		M[ 6]=P[ 2]*Q[ 4] + P[ 6]*Q[ 5] + P[10]*Q[ 6];
		M[10]=P[ 2]*Q[ 8] + P[ 6]*Q[ 9] + P[10]*Q[10];
		M[14]=0;

		M[ 3]=0;
		M[ 7]=0;
		M[11]=0;
		M[15]=1;
	}
	void Multiply4(float( &M )[ 16 ], const float *P, const float *Q) {
		M[ 0]=P[ 0]*Q[ 0] + P[ 4]*Q[ 1] + P[ 8]*Q[ 2] + P[12]*Q[ 3];
		M[ 4]=P[ 0]*Q[ 4] + P[ 4]*Q[ 5] + P[ 8]*Q[ 6] + P[12]*Q[ 7];
		M[ 8]=P[ 0]*Q[ 8] + P[ 4]*Q[ 9] + P[ 8]*Q[10] + P[12]*Q[11];
		M[12]=P[ 0]*Q[12] + P[ 4]*Q[13] + P[ 8]*Q[14] + P[12]*Q[15];

		M[ 1]=P[ 1]*Q[ 0] + P[ 5]*Q[ 1] + P[ 9]*Q[ 2] + P[13]*Q[ 3];
		M[ 5]=P[ 1]*Q[ 4] + P[ 5]*Q[ 5] + P[ 9]*Q[ 6] + P[13]*Q[ 7];
		M[ 9]=P[ 1]*Q[ 8] + P[ 5]*Q[ 9] + P[ 9]*Q[10] + P[13]*Q[11];
		M[13]=P[ 1]*Q[12] + P[ 5]*Q[13] + P[ 9]*Q[14] + P[13]*Q[15];

		M[ 2]=P[ 2]*Q[ 0] + P[ 6]*Q[ 1] + P[10]*Q[ 2] + P[14]*Q[ 3];
		M[ 6]=P[ 2]*Q[ 4] + P[ 6]*Q[ 5] + P[10]*Q[ 6] + P[14]*Q[ 7];
		M[10]=P[ 2]*Q[ 8] + P[ 6]*Q[ 9] + P[10]*Q[10] + P[14]*Q[11];
		M[14]=P[ 2]*Q[12] + P[ 6]*Q[13] + P[10]*Q[14] + P[14]*Q[15];

		M[ 3]=P[ 3]*Q[ 0] + P[ 7]*Q[ 1] + P[11]*Q[ 2] + P[15]*Q[ 3];
		M[ 7]=P[ 3]*Q[ 4] + P[ 7]*Q[ 5] + P[11]*Q[ 6] + P[15]*Q[ 7];
		M[11]=P[ 3]*Q[ 8] + P[ 7]*Q[ 9] + P[11]*Q[10] + P[15]*Q[11];
		M[15]=P[ 3]*Q[12] + P[ 7]*Q[13] + P[11]*Q[14] + P[15]*Q[15];
	}

}
// キョン修正：終わり



struct float3
{       float x;  float y;  float z;
        float3( float _x = 0.0f , float _y = 0.0f , float _z = 0.0f  ) { x=_x; y=_y; z=_z; }
        virtual ~float3(){;}
        void set( float _x = 0.0f , float _y = 0.0f , float _z = 0.0f  ) { x=_x; y=_y; z=_z; }
        float3& operator=( const float3& a ) {  x=a.x; y=a.y ; z=a.z ;    return *this; }
};

struct float4
{ float x;  float y;  float z;  float w;
        float4( float _x = 0.0f , float _y = 0.0f , float _z = 0.0f , float _w = 0.0f ) { x=_x; y=_y; z=_z; w=_w ;}
        virtual ~float4(){;}
        void set( float _x = 0.0f , float _y = 0.0f , float _z = 0.0f , float _w = 0.0f ) { x=_x; y=_y; z=_z; w=_w ;}
        float4& operator=( const float4& a ) {  x=a.x; y=a.y ; z=a.z ; w=a.w ;  return *this; }
};

struct Vert{
    float3 pos;
    float4 color;
};

struct Tris{
    unsigned int verts[3] ;
};

namespace PYON
{
    // Prime's Junk.
    // requires KYON by Aaron .

    struct Quad{
        Vert verts[4];
        void setup(){

            verts[0].pos.set( -0.5f , 0.5f , 0.0f );    verts[0].color.set( 1.0f, 0.0f, 0.0f, 1.0f );
            verts[1].pos.set( 0.5f , 0.5f , 0.0f );     verts[1].color.set( 0.0f, 1.0f, 0.0f, 1.0f );
            verts[2].pos.set( -0.5f , -0.5f , 0.0f );   verts[2].color.set( 0.0f, 0.0f, 1.0f, 1.0f );
            verts[3].pos.set( 0.5f , -0.5f , 0.0f );    verts[3].color.set( 1.0f, 1.0f, 1.0f, 1.0f );
        ;}

        void draw(){
            int ct =0;
            glColor4f(verts[ct].color.x, verts[ct].color.y, verts[ct].color.z, verts[ct].color.w);
            glVertex3f(verts[ct].pos.x, verts[ct].pos.y, verts[ct].pos.z );
            ct =1;
            glColor4f(verts[ct].color.x, verts[ct].color.y, verts[ct].color.z, verts[ct].color.w);
            glVertex3f(verts[ct].pos.x, verts[ct].pos.y, verts[ct].pos.z );
            ct =2;
            glColor4f(verts[ct].color.x, verts[ct].color.y, verts[ct].color.z, verts[ct].color.w);
            glVertex3f(verts[ct].pos.x, verts[ct].pos.y, verts[ct].pos.z );
            ct =2;
            glColor4f(verts[ct].color.x, verts[ct].color.y, verts[ct].color.z, verts[ct].color.w);
            glVertex3f(verts[ct].pos.x, verts[ct].pos.y, verts[ct].pos.z );
            ct =1;
            glColor4f(verts[ct].color.x, verts[ct].color.y, verts[ct].color.z, verts[ct].color.w);
            glVertex3f(verts[ct].pos.x, verts[ct].pos.y, verts[ct].pos.z );
            ct =3;
            glColor4f(verts[ct].color.x, verts[ct].color.y, verts[ct].color.z, verts[ct].color.w);
            glVertex3f(verts[ct].pos.x, verts[ct].pos.y, verts[ct].pos.z );

        ;}

    };

    class Ent
    {
        public:
            Ent();
            virtual ~Ent();
            unsigned int GetCounter() { return e_Counter; }
            void SetCounter(unsigned int val) { e_Counter = val; }

            float projMatrix[ 16 ]; // may not need PURGE if unused

            char name[64];

            float3 Position;
            float3 Rotation;
            float3 Scale;

        protected:
        private:
            static unsigned int e_Counter ;
            static unsigned int e_CounterMaster ;

            unsigned int e_ID;
            //void* parent ;
            unsigned int myTypeCode ;

    };


    int RenderWorld(float theta )
    {
        int ret = 0;
        Quad testP;
        testP.setup();

		// キョン修正：始まる - play with lighting!
		float p[4] = {0, 0, 1, 1};
		float lAmb[4] = {0.1, 0.1, 0.1, 1.0};
		glLightfv(GL_LIGHT0, GL_POSITION, p);
		glLightfv(GL_LIGHT0, GL_AMBIENT, lAmb);
		glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, .1f);
		glEnable( GL_LIGHTING );
		// キョン修正：終わり

        glLoadIdentity();
        glTranslatef(-1.0f, 0.0f, 6.0f);
        glRotatef(theta, 1.0f, 1.0f, 0.0f);
        if( obj.TotalConnectedTriangles ) {
			// キョン修正：始まる - test original Draw() -OR- Draw2()
			#if 1 // ← Set to zero to use other code block
			glColor4f( 1.0f, 1.0f, 1.0f, 1.0f ); // ← glColor4f() from the dynamic triangle was making this blue
			obj.Draw();
			#else
            glBegin(GL_TRIANGLES);
              ret = obj.Draw2() ;
            glEnd();
			#endif
			// キョン修正：終わり
        }


        glLoadIdentity();
        glTranslatef(2.0f, 0.0f, 6.0f); //キョン修正：need to place in front of "camera"

		glDisable( GL_LIGHTING );
        glBegin(GL_TRIANGLES);
            testP.draw();
        glEnd();

        glLoadIdentity();
        glTranslatef(0.0f, 0.0f, 5.0f);
        glRotatef(theta, 0.0f, 0.0f, 1.0f);

        glBegin(GL_TRIANGLES); // GL_TRIANGLES / GL_POLYGON
                glColor4f(1.0f, 0.0f, 0.0f, 1.0f );    glVertex3f(0.0f, (theta -180.0f)/36.0f , 0.0f ); // glVertex2f
                glColor4f(0.0f, 1.0f, 0.0f, 1.0f );    glVertex3f(0.87f, -0.5f, 0.0f ); // (theta -180.0f)/36.0f )
                glColor4f(0.0f, 0.0f, 1.0f, 1.0f );    glVertex3f(-0.87f, -0.5f, 0.0f );
        glEnd();

        return ret;
    }


    unsigned int Ent::e_Counter = 0;
    unsigned int Ent::e_CounterMaster = 0 ;


    Ent::Ent()
    {
        //ctor
    }

    Ent::~Ent()
    {
        //dtor
    }

}




int WINAPI WinMain(HINSTANCE hInstance,
										HINSTANCE hPrevInstance,
										LPSTR lpCmdLine,
										int nCmdShow)
{
		WNDCLASSEX wcex;
		HWND hwnd;
		HDC hDC;
		HGLRC hRC;
		MSG msg;
		BOOL bQuit = FALSE;
		float theta = 0.0f;
		
		// キョン修正：始まる - unused parameters
		( void )hPrevInstance;
		( void )lpCmdLine;
		// キョン修正：終わり

		/* register window class */
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_OWNDC;
		wcex.lpfnWndProc = WindowProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wcex.lpszMenuName = NULL;
		wcex.lpszClassName = "GLSample";
		wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);;


		if (!RegisterClassEx(&wcex))
				return 0;

		/* create main window */
		hwnd = CreateWindowEx(0,
													"GLSample",
													"GL & KYON & PYON",
													WS_OVERLAPPEDWINDOW,
													CW_USEDEFAULT,
													CW_USEDEFAULT,
													512,
													512,
													NULL,
													NULL,
													hInstance,
													NULL);

		ShowWindow(hwnd, nCmdShow);

		/* enable OpenGL for the window */
		EnableOpenGL(hwnd, &hDC, &hRC);



        if( obj.Load("PrimeGL02-TestMesh.obj") ){  // cyborglowpoly.obj    box2.obj
            cout << "Object load error :" + char(10) ;
            bQuit = TRUE;}

		/* program main loop */
		while (!bQuit)
		{
				/* check for messages */
				if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
				{
						/* handle or dispatch messages */
						if (msg.message == WM_QUIT)
						{
								bQuit = TRUE;
						}
						else
						{
								TranslateMessage(&msg);
								DispatchMessage(&msg);
						}
				}
				else
				{
						/* OpenGL animation code goes here */

						// Enable depth testing/
						//glEnable(GL_DEPTH_TEST);

						// キョン修正：始まる - don't use black
						//glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
						glClearColor( 0.1f, 0.3f, 0.5f, 1.0f );
						// キョン修正：終わり

						//glClear(GL_COLOR_BUFFER_BIT); // must clear depth buffer too .
						glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); // 	| GL_STENCIL_BUFFER_BIT

						// キョン修正：始まる
						RECT rc = { 0, 0, 0, 0 };
						GetClientRect( hwnd, &rc );
						glViewport( 0, 0, rc.right, rc.bottom );

						float projMatrix[ 16 ];
						const float fov = 60.0f;
						const float w = ( float )rc.right;  // get window width
						const float h = ( float )rc.bottom; // get window height
						const float aspect = w/h;
						const float zn = 1.0f;
						const float zf = 4096.0f;
						KYON::LoadPerspective( projMatrix, fov, aspect, zn, zf );

						glMatrixMode( GL_PROJECTION );
						glLoadMatrixf( ( const GLfloat * )projMatrix );
						// キョン修正：終わり


						glMatrixMode( GL_MODELVIEW );

						glPushMatrix();
						glLoadIdentity();

                        // RenderWorld shapes
                        int trisRendred = PYON::RenderWorld(theta) ;
                        if( trisRendred ) {
                            string wtR = "GL Tris :" +   NumberToString ( trisRendred ) ;
                            SetWindowText(hwnd, wtR.c_str() );
                            //cout << "trisRendred :" +   NumberToString ( trisRendred )  +  char(10) ;
                        }


						glPopMatrix();

						//glFlush();

						SwapBuffers(hDC);

						theta += 1.0f;
						if ( theta > 360.0f ) theta = 0.0f ;
						Sleep (1);

						/* OpenGL animation code ends here */
				}
		}

		/* shutdown OpenGL */
		DisableOpenGL(hwnd, hDC, hRC);

		/* destroy the window explicitly */
		DestroyWindow(hwnd);

		return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
		switch (uMsg)
		{
				case WM_CLOSE:
						PostQuitMessage(0);
				break;

				case WM_DESTROY:
						return 0;

				case WM_KEYDOWN:
				{
						switch (wParam)
						{
								case VK_ESCAPE:
										PostQuitMessage(0);
								break;
						}
				}
				break;

				default:
						return DefWindowProc(hwnd, uMsg, wParam, lParam);
		}

		return 0;
}

void EnableOpenGL(HWND hwnd, HDC* hDC, HGLRC* hRC)
{
		PIXELFORMATDESCRIPTOR pfd;

		int iFormat;

		/* get the device context (DC) */
		*hDC = GetDC(hwnd);

		/* set the pixel format for the DC */
		ZeroMemory(&pfd, sizeof(pfd));

		pfd.nSize = sizeof(pfd);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW |
									PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 24;
		pfd.cDepthBits = 32;
		pfd.iLayerType = PFD_MAIN_PLANE;

		iFormat = ChoosePixelFormat(*hDC, &pfd);

		SetPixelFormat(*hDC, iFormat, &pfd);

		/* create and enable the render context (RC) */
		*hRC = wglCreateContext(*hDC);

		wglMakeCurrent(*hDC, *hRC);

        glMatrixMode(GL_MODELVIEW);
        glShadeModel( GL_SMOOTH );
        glClearColor( 0.0f, 0.1f, 0.0f, 0.5f );
        glClearDepth( 1.0f );
        glEnable( GL_DEPTH_TEST );
        glDepthFunc( GL_LEQUAL );
        glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );

        GLfloat amb_light[] = { 0.1, 0.1, 0.1, 1.0 };
        GLfloat diffuse[] = { 0.6, 0.6, 0.6, 1 };
        GLfloat specular[] = { 0.7, 0.7, 0.3, 1 };
        glLightModelfv( GL_LIGHT_MODEL_AMBIENT, amb_light );
        glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuse );
        glLightfv( GL_LIGHT0, GL_SPECULAR, specular );
        glEnable( GL_LIGHT0 );
        glEnable( GL_COLOR_MATERIAL );
        glShadeModel( GL_SMOOTH );
        glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE );
        glDepthFunc( GL_LEQUAL );
        glEnable( GL_DEPTH_TEST );
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
}

void DisableOpenGL (HWND hwnd, HDC hDC, HGLRC hRC)
{
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(hRC);
		ReleaseDC(hwnd, hDC);
}


