#include <stdlib.h>
#include <string.h>

#include "memory.h"

#include "report.h"

void *Alloc(size_t n) {
	void *p;

	if (!n)
		return (void *)0;

	p = malloc(n);
	if (!p)
		ErrorExit("Failed to allocate memory");

	return p;
}
void *AllocZero(size_t n) {
	void *p;

	p = Alloc(n);
	if (!p)
		return (void *)0;

	memset(p, 0, n);

	return p;
}
void *Free(void *p) {
	if (!p)
		return (void *)0;

	free(p);
	return (void *)0;
}

void *Memory(void *p, size_t n) {
	if (!p)
		return Alloc(n);

	if (!n)
		return Free(p);

	p = realloc(p, n);
	if (!p)
		ErrorExit("Failed to reallocate memory");

	return p;
}

char *DuplicateN(const char *src, size_t srcn) {
	size_t l;
	char *p;

	if (!src)
		return (char *)0;

	l = srcn ? srcn : strlen(src);

	p = (char *)Alloc(l + 1);
	memcpy((void *)p, (const void *)src, l);
	p[l] = 0;

	return p;
}
char *Duplicate(const char *src) {
	return DuplicateN(src, 0);
}

char *CopyN(char *dst, const char *src, size_t srcn) {
	size_t len;

	if (!src)
		return (char *)Free((void *)dst);

	len = srcn ? srcn : strlen(src);

	dst = (char *)Memory((void *)dst, len + 1);
	memcpy((void *)dst, (const char *)src, len);
	dst[len] = 0;

	return dst;
}
char *Copy(char *dst, const char *src) {
	return CopyN(dst, src, 0);
}

char *AppendNChar(char *dst, const char *src, size_t srcn, char ch) {
	size_t l1, l2, l3;

	if (!dst)
		return DuplicateN(src, srcn);

	l1 = strlen(dst);
	l2 = srcn ? srcn : strlen(src);
	l3 = ch!='\0' ? 1 : 0;

	dst = (char *)Memory((void *)dst, l1 + l2 + l3 + 1);
	memcpy(&dst[l1], (const void *)src, l2);
	dst[l1 + l2] = ch;
	if (ch != '\0')
		dst[l1 + l2 + l3] = '\0';

	return dst;
}
char *AppendN(char *dst, const char *src, size_t srcn) {
	return AppendNChar(dst, src, srcn, '\0');
}
char *Append(char *dst, const char *src) {
	return AppendN(dst, src, 0);
}

char *TrimAppendChar(char *dst, const char *src, char ch) {
	const char *p, *q;

	for(p=src; *p<=' ' && *p!='\0'; p++);

	q = strchr(p, '\0');

	while(q > p) {
		if (*q++ > ' ')
			break;
	}

	if (!(q - p))
		return dst;

	return AppendNChar(dst, p, q - p, ch);
}
char *TrimAppend(char *dst, const char *src) {
	return TrimAppendChar(dst, src, '\0');
}

char *StrCpy(char *dst, size_t dstn, const char *src) {
	return StrCpyN(dst, dstn, src, 0);
}
char *StrCpyN(char *dst, size_t dstn, const char *src, size_t srcn) {
	size_t l;
	char *r;

	if (!dst) {
		Error(__FILE__,__LINE__,__func__, "NULL destination string passed.");
		return (char *)0;
	}

	if (!src) {
		Error(__FILE__,__LINE__,__func__, "NULL source string passed.");
		return (char *)0;
	}

	if (dstn < 2) {
		Error(__FILE__,__LINE__,__func__, "No room in destination string.");
		return (char *)0;
	}

	l = srcn ? srcn : strlen(src);

	if (l + 1 > dstn) {
		l = dstn - 1;
		r = (char *)0;

		Warn(__FILE__,__LINE__,__func__, "Overflow prevented.");
	} else
		r = dst;

	if (l)
		memcpy((void *)dst, (const void *)src, l);

	dst[l] = 0;

	return r;
}

char *StrCat(char *dst, size_t dstn, const char *src) {
	return StrCatN(dst, dstn, src, 0);
}
char *StrCatN(char *dst, size_t dstn, const char *src, size_t srcn) {
	char *p;

	p = strchr(dst, '\0');

	p = StrCpyN(p, dstn - (p - dst), src, srcn);
	if (p != (char *)0)
		p = dst;

	return p;
}
