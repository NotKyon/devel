#ifndef BASE_LOCAL_H
#define BASE_LOCAL_H

#if _MSC_VER > 1000
# pragma once
#endif

#include <gamebase/base/handle.h>

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Handle System */
struct pool_s;
struct objh_s;

void gblocalInitHandleSystem();
void gblocalFiniHandleSystem();

void gblocalLockHandleSystem();
void gblocalUnlockHandleSystem();

void gblocalLockPoolAvailList(struct pool_s *pool);
void gblocalUnlockPoolAvailList(struct pool_s *pool);
void gblocalLockPoolAllocList(struct pool_s *pool);
void gblocalUnlockPoolAllocList(struct pool_s *pool);

gb_bool_t gblocalObjectConstructorChain(struct objh_s *objh);
void gblocalObjectDestructorChain(struct objh_s *objh);

gb_handleType_t gblocalPurgeHandleType(gb_handleType_t type);

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
