#ifndef DECLCONV_H
#define DECLCONV_H

#define YYSTYPE char *
#define YYDEBUG 1
#define YYDEBUG_LEXER_TEXT (yylval)

#ifdef _WIN32
# define _CRT_SECURE_NO_WARNINGS
# include <windows.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "declconv.parse.h"

#define ARG_REF_BIT	0x01
#define ARG_DEF_BIT	0x02
struct Arg
{
	char			name[256];
	char			type;
	int				flags;
	char			def[256];

	struct Arg		*next;
};

struct Func
{
	char			name[256],
					deco[256];
	char			type;

	struct Arg		*first_arg,
					*last_arg;

	struct Func		*next;
};

struct Constant
{
	char			name[256];
	char			value[256];

	struct Constant	*next;
};

struct Lib
{
	char			dll_name[256];

	struct Constant	*first_cnst,
					*last_cnst;

	struct Func		*first_func,
					*last_func;
};

#define MAX_ARGS	256
extern struct Lib	*lib;
extern struct Arg	*arg_stack[MAX_ARGS];
extern int			arg_sp;

void push_current_file_and_line();
void pop_current_file_and_line();

void set_current_file(const char *file);
void set_current_line(unsigned int line);
void reset_current_line();
void increment_current_line();
const char *get_current_file();
unsigned int get_current_line();

void reportv(const char *type, const char *format, va_list args);

void interror(const char *format, ...);
void error(const char *format, ...);
void warn(const char *format, ...);

#endif
