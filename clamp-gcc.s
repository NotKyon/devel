	.file	"clamp.c"
	.text
	.p2align 4,,15
	.globl	clamp
	.type	clamp, @function
clamp:
.LFB22:
	.cfi_startproc
	pushl	%ebx
	.cfi_def_cfa_offset 8
	.cfi_offset 3, -8
	movl	$1065353216, %ecx
	subl	$4, %esp
	.cfi_def_cfa_offset 12
	movl	12(%esp), %eax
	movl	%eax, %edx
	andl	$-2147483648, %edx
	orl	$1065353216, %edx
#APP
# 7 "clamp.c" 1
	andl $0x7FFFFFFF, %eax
	sub  %eax, %ecx
	shrl $31, %ecx
	neg  %ecx
	and  %ecx, %edx
	mov  %edx, %ebx
	xorl $0xFFFFFFFF, %ecx
	and  %eax, %ecx
	or   %ecx, %ebx
	
# 0 "" 2
#NO_APP
	movl	%ebx, (%esp)
	flds	(%esp)
	addl	$4, %esp
	.cfi_def_cfa_offset 8
	popl	%ebx
	.cfi_def_cfa_offset 4
	.cfi_restore 3
	ret
	.cfi_endproc
.LFE22:
	.size	clamp, .-clamp
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"clamp(%f) = %f (0x%.8X)\n"
	.text
	.p2align 4,,15
	.globl	test
	.type	test, @function
test:
.LFB23:
	.cfi_startproc
	subl	$60, %esp
	.cfi_def_cfa_offset 64
	movl	$1065353216, %ecx
	movl	64(%esp), %eax
	movl	$.LC1, 4(%esp)
	movl	$1, (%esp)
	movl	%eax, %edx
	andl	$-2147483648, %edx
	orl	$1065353216, %edx
#APP
# 7 "clamp.c" 1
	andl $0x7FFFFFFF, %eax
	sub  %eax, %ecx
	shrl $31, %ecx
	neg  %ecx
	and  %ecx, %edx
	mov  %edx, %edx
	xorl $0xFFFFFFFF, %ecx
	and  %eax, %ecx
	or   %ecx, %edx
	
# 0 "" 2
#NO_APP
	movl	%edx, 44(%esp)
	flds	44(%esp)
	fstpl	16(%esp)
	movl	%eax, 44(%esp)
	flds	44(%esp)
	movl	%edx, 24(%esp)
	fstpl	8(%esp)
	call	__printf_chk
	addl	$60, %esp
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
.LFE23:
	.size	test, .-test
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB24:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$16, %esp
	movl	$0xc0400000, (%esp)
	call	test
	movl	$0xbfc00000, (%esp)
	call	test
	movl	$0xbe800000, (%esp)
	call	test
	movl	$0x3eab020c, (%esp)
	call	test
	movl	$0x3f666666, (%esp)
	call	test
	movl	$0x3fdeb852, (%esp)
	call	test
	movl	$0x461c4000, (%esp)
	call	test
	movl	$0xc61c4000, (%esp)
	call	test
	movl	$0x00000000, (%esp)
	call	test
	xorl	%eax, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE24:
	.size	main, .-main
	.ident	"GCC: (Ubuntu/Linaro 4.6.3-1ubuntu3) 4.6.3"
	.section	.note.GNU-stack,"",@progbits
