#ifndef OPTS_C
#define OPTS_C

#include "opts.h"
#include "memory.h"
#include "report.h"
#include "utility.h"

const char *gOptAlias[256];

static char **gFiles = (char **)NULL;
static size_t gNumFiles = 0;
static size_t gMaxFiles = 0;

void PushFile(const char *f) {
	size_t n;

	n = gNumFiles + 1;

	if (n > gMaxFiles) {
		n = gMaxFiles + 16;

		gFiles = (char **)Memory((void *)gFiles, n*sizeof(char *));
		gMaxFiles = n;
	}

	gFiles[gNumFiles++] = Duplicate(f);
}
size_t CountFiles() {
	return gNumFiles;
}
const char *GetFile(size_t i) {
	return gFiles[i];
}
void FiniFiles() {
	size_t i;

	for(i=0; i<gNumFiles; i++)
		gFiles[i] = Copy(gFiles[i], (const char *)NULL);

	gFiles = (char **)Memory((void *)gFiles, 0);
}

static int FindOpt(const char *opt, size_t *j) {
	size_t i;

	for(i=0; i<gOptTableSize; i++) {
		if (!strcmp(opt, gOptTable[i].name)) {
			*j = i;
			return 1;
		}
	}

	return 0;
}

void ProcessOpts(int argc, char **argv) {
	const char *alias, *opts[2];
	size_t j, k;
	opt_t *opt;
	char *arg, *p, *parm, buf[256];
	int filemode, invert;
	int i;

	filemode = 0;

	for(i=1; i<argc; i++) {
		arg = argv[i];

		/* handle files passed to the command-line */
		if (*arg != '-' || filemode) {
			PushFile(arg);
			continue;
		}

		/* neither requirement above is true, therefore: *arg == '-' */
		arg++;

		/* did we encounter a single "-" meaning every command-line argument is
		   now actually just a file reference? */
		if (*arg=='\0') {
			filemode = 1;
			continue;
		}

		/* check to see if the argument is actually an alias */
		alias = gOptAlias[(int)(unsigned char)*arg];

		/* if the argument begins with "--" it's, by design, not an alias */
		invert = 0;
		if (*arg=='-') {
			arg++; /* NOTE: 'arg' is only incremented upon 'if true' */
			alias = (const char *)NULL;

			/* check if we're toggling with "--no-" */
			if (!strncmp(arg, "no-", 3)) {
				invert = 1;
				arg += 3;
			}
		}

		/* search for either a parameter, or the end of the argument string */
		parm = (char *)NULL;
		p = alias ? arg : strchr(arg, '=');
		if (p)
			parm = *(p + 1) ? p + 1 : (char *)NULL;
		else
			p = strchr(arg, '\0');
		/* if there is no more to this argument string, it's an error */
		if (arg==p && !alias) {
			WarnMessage("Ignoring invalid argument: %s", argv[i]);
			continue;
		}

		/* copy the string into the buffer */
		StrCpyN(buf, sizeof(buf), arg, p - arg);

		/* prepare to find first */
		if (alias) {
			opts[0] = alias;
			opts[1] = buf;
			k = 2;
		} else {
			opts[0] = buf;
			k = 1;
		}

		/* find first */
		while(k > 0) {
			if (FindOpt(opts[k - 1], &j))
				break;

			k--;
		}

		/* k will be nonzero due to the break above if it succeeded, but the
		   check occurs for k - 1; this is valid

		   NOTE: 'k' is post-decremented! */
		if (k--==0) {
			WarnMessage("Unknown argument: %s", argv[i]);
			continue;
		}

		/*
		 * TODO? In the event that something isn't right with the alias, check
		 *       the other option for validity. If that option is valid then use
		 *       it. (The 'k--' above is in preparation for this.)
		 */

		ASSERT(j < gOptTableSize);
		opt = &gOptTable[j];

		/* was 'no-' used on a non-switch option? */
		if (invert && (~opt->flags & kOptFlag_Switch_Bit))
			WarnMessage("Ignoring 'no-' on non-switch argument: %s", argv[i]);

		/* was a parameter passed to an option that takes none? */
		if (parm && (~opt->flags & kOptFlag_Param_Bit))
			WarnMessage("Ignoring parameter on argument: %s", argv[i]);

		/* was an option that is disabled used? */
		if (opt->flags & kOptFlag_Disabled_Bit) {
			if (opt->flags & kOptFlag_RunOnce_Bit)
				WarnMessage("Ignoring duplicate argument: %s", argv[i]);
			else
				WarnMessage("Ignoring disabled argument: %s", argv[i]);

			if (((opt->flags & kOptFlag_Param_Bit) &
			(~opt->flags & kOptFlag_Optional_Bit)) && !parm)
				i++; /* skip the potential argument here */

			continue;
		}

		/* handle switches */
		if (opt->flags & kOptFlag_Switch_Bit) {
			ASSERT(opt->bitfield != (int *)NULL);

			if (invert)
				*opt->bitfield &= ~opt->bitmask;
			else
				*opt->bitfield |= opt->bitmask;
		}

		/* handle parameters */
		if (opt->flags & kOptFlag_Param_Bit) {
			/* ensure there's a parameter */
			if (!parm) {
				if (i + 1 >= argc)
					goto __stop_it_;

				if ((opt->flags & kOptFlag_Optional_Bit) && *argv[i + 1]=='-')
					parm = (char *)NULL;
				else
					parm = argv[++i];
			} __stop_it_:
			if (!parm) {
				if (~opt->flags & kOptFlag_Optional_Bit) {
					WarnMessage("Parameter not given: %s", argv[i]);
					break;
				}
			}

			ASSERT(opt->fn != (optfn_t)NULL);
			opt->fn(parm);
		}

		/* run once? */
		if (opt->flags & kOptFlag_RunOnce_Bit) {
			opt->flags |= kOptFlag_Disabled_Bit;
			continue;
		}
	}
}

#endif
