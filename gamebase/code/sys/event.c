#include "local.h"
#include "lock.h"

struct EventQueue {
	gb_event_t ev;
	struct EventQueue *next;
};

static struct EventQueue *g_eqHead = (struct EventQueue *)NULL;
static struct EventQueue *g_eqTail = (struct EventQueue *)NULL;

static gb_event_t g_curEv = { 0, NULL, 0, 0, 0, 0, NULL };

/* -------------------------------------------------------------------------- */

static gb_bool_t g_didInit = GB_FALSE;
static lock_t g_lock;

static void FreeLockOnEnd_f() {
	if (!g_didInit)
		return;

	FiniLock(&g_lock);
	g_didInit = GB_FALSE;
}
void gblocalInitEventQueue() {
	if (g_didInit)
		return;

	InitLock(&g_lock);
	gbOnEnd(FreeLockOnEnd_f);

	g_didInit = GB_TRUE;
}

static void LockEventQueue() {
	Lock(&g_lock);
}
static void UnlockEventQueue() {
	Unlock(&g_lock);
}

/* -------------------------------------------------------------------------- */

gb_eventID_t gbPeekEvent(gb_event_t *ev) {
	gb_eventID_t r;

	gbAssert(ev != (gb_event_t *)NULL);

	LockEventQueue();

	if (!g_eqHead) {
		UnlockEventQueue();
		return kGBEvent_None;
	}

	r = g_eqHead->ev.id;
	*ev = g_eqHead->ev;

	UnlockEventQueue();

	return r;
}
gb_eventID_t gbPollEvent() {
	gb_eventID_t r;

	LockEventQueue();

	r = gbPeekEvent(&g_curEv);
	if (!r && !gblocalPollEvent()) {
		gbMemClear(&g_curEv, sizeof(g_curEv));
		UnlockEventQueue();

		return kGBEvent_None;
	}

	r = gbPeekEvent(&g_curEv);

	UnlockEventQueue();
	return r;
}
gb_eventID_t gbWaitEvent() {
	gb_eventID_t r;

	LockEventQueue();

	if (!gblocalWaitEvent()) {
		UnlockEventQueue();
		return kGBEvent_None;
	}

	r = gbPeekEvent(&g_curEv);

	UnlockEventQueue();
	return r;
}

gb_eventID_t gbCurrentEvent(gb_event_t *ev) {
	gbAssert(ev != (gb_event_t *)NULL);

	*ev = g_curEv;
	return g_curEv.id;
}

void gbEmitEventEx(gb_event_t *ev) {
	struct EventQueue *eq;

	gbAssert(ev != (gb_event_t *)NULL);

	eq = (struct EventQueue *)gbMemAlloc(sizeof(*eq));
	eq->ev = *ev;
	eq->next = (struct EventQueue *)NULL;

	LockEventQueue();

	g_eqTail = eq;
	if (!g_eqHead)
		g_eqHead = eq;

	UnlockEventQueue();
}
void gbEmitEvent(int id, void *source, int data, gb_uint_t mods, int x, int y,
void *extra) {
	gb_event_t ev;

	ev.id = id;
	ev.source = source;
	ev.data = data;
	ev.mods = mods;
	ev.x = x;
	ev.y = y;
	ev.extra = extra;

	gbEmitEventEx(&ev);
}

int gbEventID() {
	return g_curEv.id;
}
void *gbEventSource() {
	return g_curEv.source;
}
int gbEventData() {
	return g_curEv.data;
}
gb_uint_t gbEventMods() {
	return g_curEv.mods;
}
int gbEventX() {
	return (int)g_curEv.x;
}
int gbEventY() {
	return (int)g_curEv.y;
}
void *gbEventExtra() {
	return g_curEv.extra;
}
const char *gbEventExtraAsText() {
	return (const char *)g_curEv.extra;
}

gb_eventID_t gbAllocUserEventID() {
	static gb_eventID_t start = 4096;

	return start++;
}
