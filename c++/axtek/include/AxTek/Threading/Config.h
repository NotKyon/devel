#ifndef AXTEK_THREADING_CONFIG_H
#define AXTEK_THREADING_CONFIG_H

#if _MSC_VER > 1000
# pragma once
#endif

//
//	OS headers
//
#if _WIN32
# include <windows.h>
#else
# ifndef _GNU_SOURCE
#  define _GNU_SOURCE 1
# endif
# include <sched.h>
# include <pthread.h>

# include <cstdio>
# include <cstdlib>
# include <cstring>
#endif

#include "../Base/Config.h"

//
//	Namespace
//
#define __AX_TASK_NS_INIT__ __AX_NS_INIT__ namespace task {
#define __AX_TASK_NS_FINI__ } __AX_NS_FINI__

//
//	Thread Local Storage (TLS)
//
#if _MSC_VER || __DMC__
# ifndef __thread
#  define __thread __declspec(thread)
#  define __thread_defined 1
# else
#  ifndef __thread_defined
#   define __thread_defined 1
#  endif
# endif
#elif __GNUC__ || __clang__ || __INTEL_COMPILER
# ifndef __thread_defined
#  define __thread_defined 1
# endif
#else
# ifdef __thread
#  ifndef __thread_defined
#   define __thread_defined 1
#  endif
# else
#  ifndef __thread_defined
#   define __thread_defined 0
#  endif
# endif
#endif

//
//	Thread Procedure
//
__AX_TASK_NS_INIT__

#if _WIN32
# define THREADPROCAPI WINAPI
typedef DWORD result_t;
#else
# define THREADPROCAPI
typedef void *result_t;
#endif

__AX_TASK_NS_FINI__

__AX_NS_INIT__

typedef task::result_t(THREADPROCAPI *threadFunc_t)(void *);

__AX_NS_FINI__

#endif
