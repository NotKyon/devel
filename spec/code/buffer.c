#include "buffer.h"

#include "memory.h"
#include "report.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

static struct buffer_s *g_buf_head = (struct buffer_s *)0;
static struct buffer_s *g_buf_tail = (struct buffer_s *)0;

static void AnalyzeBufferLines(buffer_t *buf) {
	size_t i;

	ASSERT(buf != (buffer_t *)0);

	buf->numlines = 0;
	buf->lines = (size_t *)Memory((void *)buf->lines, 0);

	for(i=0; i<buf->length; i++) {
		if (buf->source[i] != '\n')
			continue;

		buf->lines = (size_t *)Memory((void *)buf->lines,
			sizeof(size_t)*(buf->numlines + 1));
		buf->lines[buf->numlines++] = i;
	}
}

buffer_t *NewBuffer(const char *filename, const char *source) {
	buffer_t *buf;

	ASSERT(filename != (const char *)0);
	ASSERT(source != (const char *)0);

	buf = (buffer_t *)Memory((void *)0, sizeof(*buf));

	buf->filename = Duplicate(filename);
	buf->numlines = 0;
	buf->lines = (size_t *)0;
	buf->index = 0;
	buf->source = Duplicate(source);
	buf->length = source ? strlen(source) : 0;
	buf->func = (char *)0;

	buf->next = (struct buffer_s *)0;
	if ((buf->prev = g_buf_tail) != (struct buffer_s *)0)
		g_buf_tail->next = buf;
	else
		g_buf_head = buf;
	g_buf_tail = buf;

	AnalyzeBufferLines(buf);

	return buf;
}

buffer_t *LoadBuffer(const char *filename) {
	buffer_t *buf;
	FILE *f;
	char curline[8192], *p;

	ASSERT(filename != (const char *)0);

	if (!(f = fopen(filename, "rb"))) {
		ErrorFile(filename, 0, "Failed to open file");
		return (buffer_t *)0;
	}

	buf = NewBuffer(filename, (const char *)0);

	while(fgets(curline, sizeof(curline) - 2, f) != 0) {
		curline[sizeof(curline) - 2] = 0;

		if ((p = strchr(curline, '\r')) != (char *)0) {
			*(p + 0) = '\n';
			*(p + 1) = 0;
		}

		buf->source = Append(buf->source, curline);
		buf->numlines++;
	}

	if (ferror(f)) {
		fclose(f);
		ErrorFile(filename, 0, "Failed to read from file");
		return DeleteBuffer(buf);
	}

	fclose(f);

	buf->length = buf->source ? strlen(buf->source) : 0;
	AnalyzeBufferLines(buf);

	return buf;
}

buffer_t *DeleteBuffer(buffer_t *buf) {
	if (!buf)
		return (buffer_t *)0;

	if (buf->prev)
		buf->prev->next = buf->next;
	if (buf->next)
		buf->next->prev = buf->prev;

	if (g_buf_head==buf)
		g_buf_head = buf->next;
	if (g_buf_tail==buf)
		g_buf_tail = buf->prev;

	Memory((void *)buf->filename, 0);
	Memory((void *)buf->source, 0);
	Memory((void *)buf->lines, 0);
	Memory((void *)buf->func, 0);

	return (buffer_t *)Memory((void *)buf, 0);
}
void DeleteAllBuffers() {
	while(g_buf_head)
		DeleteBuffer(g_buf_head);
}

const char *BufferFilename(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	return buf->filename;
}
const char *BufferSource(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	return buf->source;
}
size_t BufferLine(buffer_t *buf) {
	size_t i, line;

	ASSERT(buf != (buffer_t *)0);

	line = 0;

	for(i=0; i<buf->numlines; i++) {
		line++;

		if (buf->lines[i] >= buf->index)
			break;
	}

	return line;
}
size_t BufferIndex(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	return buf->index;
}
size_t BufferLength(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	return buf->length;
}

void SetBufferFunction(buffer_t *buf, const char *func) {
	ASSERT(buf != (buffer_t *)0);

	buf->func = Copy(buf->func, func);
}
const char *BufferFunction(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	return buf->func;
}

char PeekBuffer(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);
	ASSERT(buf->source != (char *)0);
	ASSERT(buf->index <= buf->length);

	return buf->source[buf->index];
}
char PullBuffer(buffer_t *buf) {
	char c;

	ASSERT(buf != (buffer_t *)0);

	c = buf->source[buf->index];
	if (buf->index != buf->length)
		buf->index++;


	return c;
}
char EatBuffer(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	PullBuffer(buf);
	return PeekBuffer(buf);
}
char UnpullBuffer(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	if (buf->index > 0)
		buf->index--;

	return buf->source[buf->index];
}
size_t SetBufferIndex(buffer_t *buf, size_t index) {
	ASSERT(buf != (buffer_t *)0);

	if (index > buf->length)
		index = buf->length;

	buf->index = index;
	return buf->index;
}
bool_t BufferEnd(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	if (buf->index==buf->length)
		return TRUE;

	return FALSE;
	/*return buf->index==buf->length ? TRUE : FALSE;*/
}

void SkipBufferWhitespace(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	while(PeekBuffer(buf) <= ' ' && !BufferEnd(buf))
		PullBuffer(buf);
}
void SkipBufferLine(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	while(PeekBuffer(buf) != '\n' && !BufferEnd(buf))
		PullBuffer(buf);

	PullBuffer(buf); /* eat the line ending */
}
bool_t ReadBufferLine(buffer_t *buf, char *line, size_t n) {
	size_t i;

	ASSERT(buf != (buffer_t *)0);
	ASSERT(line != (char *)0);

	ASSERT(n > 1);

	if (BufferEnd(buf))
		return FALSE;

	i = 0;

	while(PeekBuffer(buf) != '\n' && !BufferEnd(buf)) {
		if (i + 2 >= n)
			break;

		line[i++] = PullBuffer(buf);
	}

	line[i++] = PullBuffer(buf); /* eat the linefeed */
	line[i] = 0;

	return TRUE;
}

buffer_t *FirstBuffer() {
	return g_buf_head;
}
buffer_t *LastBuffer() {
	return g_buf_tail;
}
buffer_t *BufferBefore(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	return buf->prev;
}
buffer_t *BufferAfter(buffer_t *buf) {
	ASSERT(buf != (buffer_t *)0);

	return buf->next;
}

void ReportBufferV(buffer_t *buf, const char *type, const char *format,
va_list args) {
	ReportV(type, BufferFilename(buf), BufferLine(buf), BufferFunction(buf),
		format, args);
}
void ReportBuffer(buffer_t *buf, const char *type, const char *format, ...) {
	va_list args;

	va_start(args, format);
	ReportBufferV(buf, type, format, args);
	va_end(args);
}

void ErrorBuffer(buffer_t *buf, const char *format, ...) {
	va_list args;

	va_start(args, format);
	ReportBufferV(buf, TYPE_ERR, format, args);
	va_end(args);
}
void WarnBuffer(buffer_t *buf, const char *format, ...) {
	va_list args;

	va_start(args, format);
	ReportBufferV(buf, TYPE_WRN, format, args);
	va_end(args);
}
void NoteBuffer(buffer_t *buf, const char *format, ...) {
	va_list args;

	va_start(args, format);
	ReportBufferV(buf, "NOTE", format, args);
	va_end(args);
}
