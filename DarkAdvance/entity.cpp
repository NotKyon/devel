#include <stdlib.h>
#include <string.h>
#include "entity.h"
#include "simplify.h"

entity_t gEntityHead = (entity_t)0;
entity_t gEntityTail = (entity_t)0;

//==============================================================================
// ENTITY MANAGEMENT
// Create a new entity
entity_t NewEntity(entity_t prnt) {
	entity_t ent;

	// allocate the entity handle
	ent = (entity_t)NewMemory(sizeof(*ent));
	if (!ent)
		return (entity_t)0;

	// prepare the hierarchy
	ent->prnt = prnt;
	ent->p_head = prnt ? &prnt->head : &gEntityHead;
	ent->p_tail = prnt ? &prnt->tail : &gEntityTail;
	ent->head = (entity_t)0;
	ent->tail = (entity_t)0;
	ent->prev = (entity_t)0;
	ent->next = (entity_t)0;

	// add to the list
	if ((ent->prev = *ent->p_tail) != (entity_t)0)
		(*ent->p_tail)->next = ent;
	else
		*ent->p_head = ent;
	*ent->p_tail = ent;

	// prepare the transforms
	ent->tformFlags = 0;

	// set the local and offset matrices to the identity
	Matrix_Identity(&ent->matLocal);
	Matrix_Identity(&ent->matOffset);

	// the global and post matrices need to be recalculated
	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;

	// all of the rotation vectors need to be recalculated
	ent->tformFlags |= kEntity_RecalcLocalRot_Bit;
	ent->tformFlags |= kEntity_RecalcGlobalRot_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
	ent->tformFlags |= kEntity_RecalcOffsetRot_Bit;

	// the name and text are empty
	ent->name = (char *)0;
	ent->text = (char *)0;

	// return the entity
	return ent;
}
// Delete an existing entity
entity_t DeleteEntity(entity_t ent) {
	// ensure the handle is valid
	if (!ent)
		return (entity_t)0;

	// remove all sub-entities
	while(ent->head)
		DeleteEntity(ent->head);

	// remove the entity from the hierarchy
	if (ent->prev)
		ent->prev->next = ent->next;
	if (ent->next)
		ent->next->prev = ent->prev;

	if (*ent->p_head == ent)
		*ent->p_head = ent->next;
	if (*ent->p_tail == ent)
		*ent->p_tail = ent->prev;

	// free the name and text
	DeleteMemory((void *)ent->name);
	DeleteMemory((void *)ent->text);

	// remove the entity from memory
	return (entity_t)DeleteMemory((void *)ent);
}
// Delete all existing entities
void DeleteAllEntities() {
	while(gEntityHead)
		DeleteEntity(gEntityHead);
}
// Retrieve the first root entity in the hierarchy
entity_t FirstEntity() {
	return gEntityHead;
}
// Retrieve the last root entity in the hierarchy
entity_t LastEntity() {
	return gEntityTail;
}
// Retrieve the sibling entity before another
entity_t EntityBefore(entity_t ent) {
	return ent->prev;
}
// Retrieve the sibling entity after another
entity_t EntityAfter(entity_t ent) {
	return ent->next;
}
// Retrieve the first child entity of an entity
entity_t FirstChildEntity(entity_t prnt) {
	return prnt->head;
}
// Retrieve the last child entity of an entity
entity_t LastChildEntity(entity_t prnt) {
	return prnt->tail;
}
// Retrieve the parent entity to a (potential) child entity
entity_t EntityParent(entity_t ent) {
	return ent->prnt;
}
// Determine whether an entity is a descendant of another entity
int IsEntityDescendantOf(entity_t ent, entity_t prnt) {
	// all entities are descendants of "the void."
	if (!prnt)
		return 1;

	// loop through each entity level
	while(ent != (entity_t)0) {
		// if this entity's parent is the one passed, then yes!
		if (ent->prnt==prnt)
			return 1;

		// go to the next level
		ent = ent->prnt;
	}

	// ran out of entities, so no
	return 0;
}
// Set the parent of an entity
void SetEntityParent(entity_t ent, entity_t prnt) {
	// if the parents match, return
	if (ent->prnt==prnt)
		return;

	// if the new parent is a descendant of the existing entity...
	if (IsEntityDescendantOf(prnt, ent)) {
		// the new parent needs to be removed from the existing area, and placed
		// as a child of this entity's parent
		SetEntityParent(prnt, ent->prnt);
	}

	// unlink this entity from its existing list
	if (ent->prev)
		ent->prev->next = ent->next;
	if (ent->next)
		ent->next->prev = ent->prev;

	if (*ent->p_head == ent)
		*ent->p_head = ent->next;
	if (*ent->p_tail == ent)
		*ent->p_tail = ent->prev;

	// add this entity to the new parent
	ent->prnt = prnt;
	ent->p_head = prnt ? &prnt->head : &gEntityHead;
	ent->p_tail = prnt ? &prnt->tail : &gEntityTail;

	ent->next = (entity_t)0;
	if ((ent->prev = *ent->p_tail) != (entity_t)0)
		(*ent->p_tail)->next = ent;
	else
		*ent->p_head = ent;
	*ent->p_tail = ent;
}

//==============================================================================
// ENTITY MATRIX MANAGEMENT
// Mark several bits in an entity's settings
inline void InvalidateEntity(entity_t ent, bitfield_t bits) {
	ent->tformFlags |= bits;

	for(ent=ent->head; ent; ent=ent->next)
		InvalidateEntity(ent, bits);
}
// Set the local matrix
void SetEntityMatrix(entity_t ent, const matrix_t *src) {
	// copy the matrix over
	CopyMatrix(&ent->matLocal, src);

	// everything needs to be recalculated! (except offset rotation)
	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcLocalRot_Bit;
	ent->tformFlags |= kEntity_RecalcGlobalRot_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Set the offset matrix
void SetEntityOffsetMatrix(entity_t ent, const matrix_t *src) {
	// copy the matrix over
	CopyMatrix(&ent->matOffset, src);

	// various settings need to be updated
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Set the global matrix
void SetEntityGlobalMatrix(entity_t ent, const matrix_t *src) {
	// copy the global matrix to the local
	CopyMatrix(&ent->matLocal, src);

	// transform the local matrix to the global matrix
	EntityLocalToGlobal(ent);

	// the local/global rotation needs to be calculated
	ent->tformFlags |= kEntity_RecalcLocalRot_Bit;
	ent->tformFlags |= kEntity_RecalcGlobalRot_Bit;
}
// Retrieve the entity's local matrix (direct pointer)
matrix_t *GetEntityMatrix(entity_t ent) {
	return &ent->matLocal;
}
// Retrieve the entity's offset matrix (direct pointer)
matrix_t *GetEntityOffsetMatrix(entity_t ent) {
	return &ent->matOffset;
}
// Retrieve the entity's global matrix (direct pointer)
matrix_t *GetEntityGlobalMatrix(entity_t ent) {
	if (!ent->prnt)
		return &ent->matLocal;

	//FIXME: If this entity is valid, and a parent entity's global matrix is
	//       invalidated the child entity's global matrix won't be invalidated.
	//       Therefore, an entity should check whether its parent was modified
	//       (recursively) if it is marked as "valid."

	if (ent->tformFlags & kEntity_RecalcGlobalTransform_Bit) {
		Matrix_RigidMultiply(&ent->matGlobal,
							 GetEntityGlobalMatrix(ent->prnt),
							 &ent->matLocal);

		InvalidateEntity(ent, kEntity_RecalcGlobalTransform_Bit|kEntity_RecalcPostTransform_Bit|kEntity_RecalcGlobalRot_Bit|kEntity_RecalcPostRot_Bit);

		ent->tformFlags &= ~kEntity_RecalcGlobalTransform_Bit;
	}

	return &ent->matGlobal;
}
// Retrieve the entity's post matrix (direct pointer)
matrix_t *GetEntityPostMatrix(entity_t ent) {
	if (ent->tformFlags & kEntity_RecalcPostTransform_Bit) {
		Matrix_RigidMultiply(&ent->matPost,
							 GetEntityGlobalMatrix(ent),
							 &ent->matOffset);

		InvalidateEntity(ent, kEntity_RecalcGlobalTransform_Bit|kEntity_RecalcPostTransform_Bit|kEntity_RecalcGlobalRot_Bit|kEntity_RecalcPostRot_Bit);

		ent->tformFlags &= ~kEntity_RecalcPostTransform_Bit;
	}

	return &ent->matPost;
}
// Retrieve the global inverse of an entity
void CalculateEntityGlobalInverseMatrix(matrix_t *dst, entity_t ent) {
	Matrix_RigidInverse(dst, GetEntityGlobalMatrix(ent));
}
// Transform an entity's local matrix to its global matrix
void EntityLocalToGlobal(entity_t ent) {
	matrix_t ginv, tmp;

	// if the entity doesn't have a parent, no need to do anything else
	if (!ent->prnt)
		return;

	// calculate the global inverse transform of the entity's parent
	CalculateEntityGlobalInverseMatrix(&ginv, ent->prnt);

	// multiply by our current local transform
	Matrix_RigidMultiply(&tmp, &ginv, &ent->matLocal);

	// the entity's current local matrix is the new global matrix
	CopyMatrix(&ent->matGlobal, &ent->matLocal);

	// the entity's local matrix was calculated into "tmp"
	CopyMatrix(&ent->matLocal, &tmp);

	// there's now no need to calculate the entity's global matrix
	ent->tformFlags &= ~kEntity_RecalcGlobalTransform_Bit;

	// the local and global rotations now have to be calculated though
	ent->tformFlags |= kEntity_RecalcLocalRot_Bit;
	ent->tformFlags |= kEntity_RecalcGlobalRot_Bit;

	// all the post stuff needs to be recalculated now too
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;

	// invalidate the branch
	InvalidateEntity(ent, kEntity_RecalcGlobalTransform_Bit|kEntity_RecalcPostTransform_Bit|kEntity_RecalcGlobalRot_Bit|kEntity_RecalcPostRot_Bit);

}

//==============================================================================
// ENTITY ROTATION VECTOR MANAGEMENT
// Set the entity's local rotation
void SetEntityRotation(entity_t ent, const vec3_t *v) {
	vec3_t scale;

	// save the current scale
	Matrix_GetScaling(&scale, &ent->matLocal);

	// save the local rotation
	CopyVec3(&ent->vecLocalRot, v);

	// apply the rotation to the local matrix
	ent->matLocal.m11 = 1;
	ent->matLocal.m12 = 0;
	ent->matLocal.m13 = 0;
	ent->matLocal.m21 = 0;
	ent->matLocal.m22 = 1;
	ent->matLocal.m23 = 0;
	ent->matLocal.m31 = 0;
	ent->matLocal.m32 = 0;
	ent->matLocal.m33 = 1;
	Matrix_TurnZ(&ent->matLocal, v->z);
	Matrix_TurnX(&ent->matLocal, v->x);
	Matrix_TurnY(&ent->matLocal, v->y);

	// apply the scale again
	Matrix_Scale(&ent->matLocal, &scale);

	// no need to calculate the local rotation
	ent->tformFlags &= ~kEntity_RecalcLocalRot_Bit;

	// need to recalculate globals and posts
	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcGlobalRot_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Set the entity's offset rotation
void SetEntityOffsetRotation(entity_t ent, const vec3_t *v) {
	vec3_t scale;

	// save the current scale
	Matrix_GetScaling(&scale, &ent->matOffset);

	// save the offset rotation
	CopyVec3(&ent->vecOffsetRot, v);

	// apply the rotation to the offset matrix
	ent->matOffset.m11 = 1;
	ent->matOffset.m12 = 0;
	ent->matOffset.m13 = 0;
	ent->matOffset.m21 = 0;
	ent->matOffset.m22 = 1;
	ent->matOffset.m23 = 0;
	ent->matOffset.m31 = 0;
	ent->matOffset.m32 = 0;
	ent->matOffset.m33 = 1;
	Matrix_TurnZ(&ent->matOffset, v->z);
	Matrix_TurnX(&ent->matOffset, v->x);
	Matrix_TurnY(&ent->matOffset, v->y);

	// apply the scale again
	Matrix_Scale(&ent->matOffset, &scale);

	// no need to calculate the offset rotation
	ent->tformFlags &= ~kEntity_RecalcOffsetRot_Bit;

	// need to recalculate post transforms
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Set the entity's global rotation
void SetEntityGlobalRotation(entity_t ent, const vec3_t *v) {
	matrix_t *g;
	vec3_t scale;

	// put the global matrix in the
	g = GetEntityGlobalMatrix(ent);

	// save the current scale
	Matrix_GetScaling(&scale, g);

	// save the global rotation
	CopyVec3(&ent->vecGlobalRot, v);

	// apply the rotation to the local matrix
	ent->matLocal.m11 = 1;
	ent->matLocal.m12 = 0;
	ent->matLocal.m13 = 0;
	ent->matLocal.m21 = 0;
	ent->matLocal.m22 = 1;
	ent->matLocal.m23 = 0;
	ent->matLocal.m31 = 0;
	ent->matLocal.m32 = 0;
	ent->matLocal.m33 = 1;
	Matrix_TurnZ(&ent->matLocal, v->z);
	Matrix_TurnX(&ent->matLocal, v->x);
	Matrix_TurnY(&ent->matLocal, v->y);

	// apply scale to the local matrix
	Matrix_Scale(&ent->matLocal, &scale);

	// save the global translation to the local translation
	ent->matLocal.m14 = g->m14;
	ent->matLocal.m24 = g->m24;
	ent->matLocal.m34 = g->m34;

	// convert the local matrix to the global matrix
	EntityLocalToGlobal(ent);

	// we have the global rotation saved, no need to calculate
	ent->tformFlags &= ~kEntity_RecalcGlobalRot_Bit;

	// we have to recalculate post transformations and the local rotation
	ent->tformFlags |= kEntity_RecalcLocalRot_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Retrieve the entity's local rotation
vec3_t *GetEntityRotation(entity_t ent) {
	if (ent->tformFlags & kEntity_RecalcLocalRot_Bit)
		Matrix_GetRotation(&ent->vecLocalRot, GetEntityMatrix(ent));

	return &ent->vecLocalRot;
}
// Retrieve the entity's offset rotation
vec3_t *GetEntityOffsetRotation(entity_t ent) {
	if (ent->tformFlags & kEntity_RecalcOffsetRot_Bit)
		Matrix_GetRotation(&ent->vecOffsetRot, GetEntityOffsetMatrix(ent));

	return &ent->vecOffsetRot;
}
// Retrieve the entity's global rotation
vec3_t *GetEntityGlobalRotation(entity_t ent) {
	if (ent->tformFlags & kEntity_RecalcGlobalRot_Bit)
		Matrix_GetRotation(&ent->vecGlobalRot, GetEntityGlobalMatrix(ent));

	return &ent->vecGlobalRot;
}
// Retrieve the entity's post rotation
vec3_t *GetEntityPostRotation(entity_t ent) {
	if (ent->tformFlags & kEntity_RecalcPostRot_Bit)
		Matrix_GetRotation(&ent->vecPostRot, GetEntityPostMatrix(ent));

	return &ent->vecPostRot;
}

//==============================================================================
// ENTITY TRANSLATION MANAGEMENT
// Set the local position of an entity
void SetEntityPosition(entity_t ent, const vec3_t *v) {
	Matrix_SetTranslation(&ent->matLocal, v);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Set the offset position of an entity
void SetEntityOffsetPosition(entity_t ent, const vec3_t *v) {
	Matrix_SetTranslation(&ent->matOffset, v);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Set the global position of an entity
void SetEntityGlobalPosition(entity_t ent, const vec3_t *v) {
	matrix_t *g;

	g = GetEntityGlobalMatrix(ent);
	CopyMatrix(&ent->matLocal, g);

	Matrix_SetTranslation(&ent->matLocal, v);
	EntityLocalToGlobal(ent);
}
// Retrieve the local position of an entity
vec3_t *GetEntityPosition(entity_t ent) {
	return (vec3_t *)&GetEntityMatrix(ent)->m14;
}
// Retrieve the offset position of an entity
vec3_t *GetEntityOffsetPosition(entity_t ent) {
	return (vec3_t *)&GetEntityOffsetMatrix(ent)->m14;
}
// Retrieve the global position of an entity
vec3_t *GetEntityGlobalPosition(entity_t ent) {
	return (vec3_t *)&GetEntityGlobalMatrix(ent)->m14;
}
// Retrieve the post position of an entity
vec3_t *GetEntityPostPosition(entity_t ent) {
	return (vec3_t *)&GetEntityPostMatrix(ent)->m14;
}

//==============================================================================
// ENTITY SCALE MANAGEMENT
// Set the local scale of an entity
void SetEntityScale(entity_t ent, const vec3_t *v) {
	const vec3_t *newScale;
	vec3_t scale;

	Matrix_GetScaling(&scale, &ent->matLocal);

	newScale = Vec3((1/scale.x)*v->x,
					(1/scale.y)*v->y,
					(1/scale.z)*v->z);

	Matrix_Scale(&ent->matLocal, newScale);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Set the offset scale of an entity
void SetEntityOffsetScale(entity_t ent, const vec3_t *v) {
	const vec3_t *newScale;
	vec3_t scale;

	Matrix_GetScaling(&scale, &ent->matOffset);

	newScale = Vec3((1/scale.x)*v->x,
					(1/scale.y)*v->y,
					(1/scale.z)*v->z);

	Matrix_Scale(&ent->matOffset, newScale);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Retrieve the local scale of an entity
void CalculateEntityScale(vec3_t *dst, entity_t ent) {
	Matrix_GetScaling(dst, GetEntityMatrix(ent));
}
// Retrieve the offset scale of an entity
void CalculateEntityOffsetScale(vec3_t *dst, entity_t ent) {
	Matrix_GetScaling(dst, GetEntityOffsetMatrix(ent));
}
// Retrieve the global scale of an entity
void CalculateEntityGlobalScale(vec3_t *dst, entity_t ent) {
	Matrix_GetScaling(dst, GetEntityGlobalMatrix(ent));
}
// Retrieve the post scale of an entity
void CalculateEntityPostScale(vec3_t *dst, entity_t ent) {
	Matrix_GetScaling(dst, GetEntityPostMatrix(ent));
}

//==============================================================================
// ENTITY LOCAL TRANSFORMATIONS
// Move an entity on all local axes
void MoveEntity(entity_t ent, const vec3_t *v) {
	Matrix_Move(&ent->matLocal, v);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Move an entity along its local x-axis
void MoveEntityX(entity_t ent, float x) {
	Matrix_MoveX(&ent->matLocal, x);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Move an entity along its local y-axis
void MoveEntityY(entity_t ent, float y) {
	Matrix_MoveY(&ent->matLocal, y);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Move an entity along its local z-axis
void MoveEntityZ(entity_t ent, float z) {
	Matrix_MoveZ(&ent->matLocal, z);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Turn an entity on all local axes
void TurnEntity(entity_t ent, const vec3_t *v) {
	Matrix_Turn(&ent->matLocal, v);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcGlobalRot_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Turn an entity along its local x-axis
void TurnEntityX(entity_t ent, float x) {
	Matrix_TurnX(&ent->matLocal, x);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcGlobalRot_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Turn an entity along its local y-axis
void TurnEntityY(entity_t ent, float y) {
	Matrix_TurnY(&ent->matLocal, y);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcGlobalRot_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Turn an entity along its local z-axis
void TurnEntityZ(entity_t ent, float z) {
	Matrix_TurnZ(&ent->matLocal, z);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcGlobalRot_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Scale an entity on all local axes
void ScaleEntity(entity_t ent, const vec3_t *v) {
	Matrix_Scale(&ent->matLocal, v);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Scale an entity along its local x-axis
void ScaleEntityX(entity_t ent, float x) {
	Matrix_ScaleX(&ent->matLocal, x);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Scale an entity along its local y-axis
void ScaleEntityY(entity_t ent, float y) {
	Matrix_ScaleY(&ent->matLocal, y);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Scale an entity along its local z-axis
void ScaleEntityZ(entity_t ent, float z) {
	Matrix_ScaleZ(&ent->matLocal, z);

	ent->tformFlags |= kEntity_RecalcGlobalTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}

//==============================================================================
// ENTITY OFFSET TRANSFORMATIONS
// Move an entity on all offset axes
void MoveEntityOffset(entity_t ent, const vec3_t *v) {
	Matrix_Move(&ent->matOffset, v);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Move an entity along its offset x-axis
void MoveEntityOffsetX(entity_t ent, float x) {
	Matrix_MoveX(&ent->matOffset, x);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Move an entity along its offset y-axis
void MoveEntityOffsetY(entity_t ent, float y) {
	Matrix_MoveY(&ent->matOffset, y);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Move an entity along its offset z-axis
void MoveEntityOffsetZ(entity_t ent, float z) {
	Matrix_MoveZ(&ent->matOffset, z);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Turn an entity on all offset axes
void TurnEntityOffset(entity_t ent, const vec3_t *v) {
	Matrix_Turn(&ent->matOffset, v);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Turn an entity along its offset x-axis
void TurnEntityOffsetX(entity_t ent, float x) {
	Matrix_TurnX(&ent->matOffset, x);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Turn an entity along its offset y-axis
void TurnEntityOffsetY(entity_t ent, float y) {
	Matrix_TurnY(&ent->matOffset, y);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Turn an entity along its offset z-axis
void TurnEntityOffsetZ(entity_t ent, float z) {
	Matrix_TurnZ(&ent->matOffset, z);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
	ent->tformFlags |= kEntity_RecalcPostRot_Bit;
}
// Scale an entity on all offset axes
void ScaleEntityOffset(entity_t ent, const vec3_t *v) {
	Matrix_Scale(&ent->matOffset, v);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Scale an entity along its offset x-axis
void ScaleEntityOffsetX(entity_t ent, float x) {
	Matrix_ScaleX(&ent->matOffset, x);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Scale an entity along its offset y-axis
void ScaleEntityOffsetY(entity_t ent, float y) {
	Matrix_ScaleY(&ent->matOffset, y);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}
// Scale an entity along its offset z-axis
void ScaleEntityOffsetZ(entity_t ent, float z) {
	Matrix_ScaleZ(&ent->matOffset, z);

	ent->tformFlags |= kEntity_RecalcPostTransform_Bit;
}

//==============================================================================
// ENTITY NAMING
// Set the name of an entity
void SetEntityName(entity_t ent, const char *name) {
	if (ent->name)
		free((void *)ent->name);
	ent->name = _strdup(name);
}
// Set the text of an entity
void SetEntityText(entity_t ent, const char *text) {
	if (ent->text)
		free((void *)ent->text);
	ent->text = _strdup(text);
}
// Retrieve the name of an entity
const char *GetEntityName(entity_t ent) {
	return ent->name;
}
// Retrieve the text of an entity
const char *GetEntityText(entity_t ent) {
	return ent->text;
}
