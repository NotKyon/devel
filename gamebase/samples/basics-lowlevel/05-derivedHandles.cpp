//
//	TODO: Explain the purpose of derived handles, explain how this example
//	      works, make it developer-friendly, etc.
//
#define GB_INIT 1
#include <gamebase.h>

struct Enemy {
	int evilFactor;
};
struct Goomba {
	int speed;
};

gb_handleType_t g_enemyHandleType = (gb_handleType_t)NULL;
gb_handleType_t g_goombaHandleType = (gb_handleType_t)NULL;

gb_bool_t EnemyCtor_f(Enemy *p) {
	gbPrint("+Enemy");
	p->evilFactor = 0;
	return GB_TRUE;
}
void EnemyDtor_f(Enemy *p) {
	if (p) {} //unused
	gbPrint("-Enemy");
}

gb_bool_t GoombaCtor_f(Goomba *p) {
	gbPrint("+Goomba");
	p->speed = 2;
	return GB_TRUE;
}
void GoombaDtor_f(Goomba *p) {
	if (p) {} //unused
	gbPrint(gbF("-Goomba:%i", p->speed));
}

void Init() {
	g_enemyHandleType = gbAddHandleType(
		"Enemy", sizeof(Enemy),
		(gb_fnCtor_t)EnemyCtor_f,
		(gb_fnDtor_t)EnemyDtor_f);

	g_goombaHandleType = gbExtendHandleType(
		"Goomba", sizeof(Goomba),
		(gb_fnCtor_t)GoombaCtor_f,
		(gb_fnDtor_t)GoombaDtor_f,
		g_enemyHandleType);
}
void Fini() {
	g_goombaHandleType = gbDropHandleType(g_goombaHandleType);
	g_enemyHandleType = gbDropHandleType(g_enemyHandleType);
}

gb_bool_t IsEnemy(gb_handle_t handle) {
	return gbIsHandleDerived(handle, g_enemyHandleType);
}
gb_bool_t IsGoomba(gb_handle_t handle) {
	return gbIsHandleDerived(handle, g_goombaHandleType);
}

Enemy *GetEnemy(gb_handle_t handle) {
	gbAssert(IsEnemy(handle));

	return (Enemy *)gbObjectEx(handle, g_enemyHandleType);
}
Goomba *GetGoomba(gb_handle_t handle) {
	gbAssert(IsGoomba(handle));

	return (Goomba *)gbObjectEx(handle, g_goombaHandleType);
}

void SetEnemyEvilFactor(gb_handle_t enemy, int evilFactor) {
	GetEnemy(enemy)->evilFactor = evilFactor;
}
int GetEnemyEvilFactor(gb_handle_t enemy) {
	return GetEnemy(enemy)->evilFactor;
}

void SetGoombaSpeed(gb_handle_t goomba, int speed) {
	GetGoomba(goomba)->speed = speed;
}
int GetGoombaSpeed(gb_handle_t goomba) {
	return GetGoomba(goomba)->speed;
}

gb_handle_t NewGoomba(int evilFactor, int speed) {
	gb_handle_t handle;

	handle = gbNewHandle(g_goombaHandleType);

	SetEnemyEvilFactor(handle, evilFactor);
	SetGoombaSpeed(handle, speed);

	return handle;
}

void PrintLiveDead() {
	gbPrint(gbF("live: %u; dead: %u",
		(gb_uint_t)gbCountLiveHandles(g_goombaHandleType),
		(gb_uint_t)gbCountDeadHandles(g_goombaHandleType)));
}

void GBMain() {
	gb_handle_t a, b;

	Init();

	a = NewGoomba(4, 2);
	b = NewGoomba(8, 7);

	gbSetHandleDebugName(a, "a");
	gbSetHandleDebugName(b, "b");

	gbPrint(gbF("a: %i, %i", GetEnemyEvilFactor(a), GetGoombaSpeed(a)));
	gbPrint(gbF("b: %i, %i", GetEnemyEvilFactor(b), GetGoombaSpeed(b)));

	PrintLiveDead();
	gbDropHandle(b);
	PrintLiveDead();

	Fini();
}
