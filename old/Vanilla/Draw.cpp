// includes
#include <Axia/Vanilla/Vanilla.h>
#include <Axia/Vanilla/Internal.h>

/*
GLAPI void APIENTRY glTexImage2D( GLenum target, GLint level, GLint internalFormat,
GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid *pixels );
*/

// AXMatrixModeToProjectionGL: set the active matrix to projection
AX_FUNC void AX_API AXMatrixModeToProjectionGL(void)
{
	glMatrixMode(GL_PROJECTION);
}

// AXMatrixModeToModelViewGL: set the active matrix to model/view
AX_FUNC void AX_API AXMatrixModeToModelViewGL(void)
{
	glMatrixMode(GL_MODELVIEW);
}

// AXLoadIdentityGL: load the identity matrix
AX_FUNC void AX_API AXLoadIdentityGL(void)
{
	glLoadIdentity();
}

// AXOrthoGL: specify an orthographic matrix
AX_FUNC void AX_API AXOrthoGL(float left, float right, float bottom, float top)
{
	//glOrtho(left, right, top, bottom, znear, zfar);
	gluOrtho2D(left, right, bottom, top);
}

// AXTranslateGL: multiply the current matrix by a translation matrix
AX_FUNC void AX_API AXTranslateGL(float x, float y)
{
	glTranslatef(x, y, 0);
}

// AXRotateGL: multiply the current matrix by a rotation matrix
AX_FUNC void AX_API AXRotateGL(float angle)
{
	glRotatef(angle/180.0f*3.1415926535f, 0, 0, 1);
}

// AXScaleGL: multiply the current matrix by a scaling matrix
AX_FUNC void AX_API AXScaleGL(float x, float y)
{
	glScalef(x, y, 1);
}

// AXBeginTrianglesGL: begin rendering triangles
AX_FUNC void AX_API AXBeginTrianglesGL(void)
{
	glBegin(GL_TRIANGLES);
}

// AXBeginTriangleStripGL: begin rendering a triangle strip
AX_FUNC void AX_API AXBeginTriangleStripGL(void)
{
	glBegin(GL_TRIANGLE_STRIP);
}

// AXBeginLinesGL: begin rendering lines
AX_FUNC void AX_API AXBeginLinesGL(void)
{
	glBegin(GL_LINES);
}

// AXBeginLineStripGL: begin rendering a line strip
AX_FUNC void AX_API AXBeginLineStripGL(void)
{
	glBegin(GL_LINE_STRIP);
}

// AXEndGL: finish rendering
AX_FUNC void AX_API AXEndGL(void)
{
	glEnd();
}

// AXColor3fGL: specify a color with three floats
AX_FUNC void AX_API AXColor3fGL(float r, float g, float b)
{
	glColor3f(r, g, b);
}

// AXColor4fGL: specify a color with four floats
AX_FUNC void AX_API AXColor4fGL(float r, float g, float b, float a)
{
	glColor4f(r, g, b, a);
}

// AXColor3bGL: specify a color with three bytes
AX_FUNC void AX_API AXColor3bGL(AXByte r, AXByte g, AXByte b)
{
	glColor3ub(r, g, b);
}

// AXColor4bGL: specify a color with four bytes
AX_FUNC void AX_API AXColor4bGL(AXByte r, AXByte g, AXByte b, AXByte a)
{
	glColor4ub(r, g, b, a);
}

// AXColorGL: specify a color using a packed color
AX_FUNC void AX_API AXColorGL(unsigned int c)
{
	glColor4ub((c&0x000000FF), (c&0x0000FF00)>>8, (c&0x00FF0000)>>16, (c&0xFF000000)>>24);
}

// AXVertex2fGL: specify a vertex using floats
AX_FUNC void AX_API AXVertex2fGL(float x, float y)
{
	glVertex2f(x, y);
}

// AXVertex2iGL: specify a vertex using integers
AX_FUNC void AX_API AXVertex2iGL(int x, int y)
{
	glVertex2i(x, y);
}

// AXFillRectGL: draw a solid rectangle
AX_FUNC void AX_API AXFillRectGL(float left, float top, float right, float bottom, unsigned int c)
{
	AXBeginTriangleStripGL();
		AXColorGL(c);
		
		AXVertex2fGL(left, bottom);
		AXVertex2fGL(right, bottom);
		AXVertex2fGL(left, top);
		AXVertex2fGL(right, top);
	AXEndGL();
}

// AXFillRectHorzGL: draw a gradient rectangle (gradient from left to right)
AX_FUNC void AX_API AXFillRectHorzGL(float left, float top, float right, float bottom, unsigned int tl, unsigned int br)
{
	AXBeginTriangleStripGL();
		AXColorGL(tl);
		AXVertex2fGL(left, bottom);
		AXVertex2fGL(left, top);
		
		AXColorGL(br);
		AXVertex2fGL(right, top);
		AXVertex2fGL(right, bottom);
	AXEndGL();
}

// AXFillRectVertGL: draw a gradient rectangle (gradient from top to bottom)
AX_FUNC void AX_API AXFillRectVertGL(float left, float top, float right, float bottom, unsigned int tl, unsigned int br)
{
	AXBeginTriangleStripGL();
		AXColorGL(tl);
		AXVertex2fGL(left, top);
		AXVertex2fGL(right, top);
		
		AXColorGL(br);
		AXVertex2fGL(left, bottom);
		AXVertex2fGL(right, bottom);
	AXEndGL();
}

// AXCalcTextGL: calculate the rectangle needed to draw a specific set of text
AX_FUNC void AX_API AXCalcTextGL(AXGadget *pWidget, const char *pszText, int flags, int *pLeft, int *pTop, int *pRight, int *pBottom)
{
	// declarations
	RECT	rcArea = { 0, 0, 0, 0 };
	UINT	format;
	
	// determine the format
	format = DT_CALCRECT;
	if (flags&AXDrawTextSingleLineBit)	format |= DT_SINGLELINE;
	if (flags&AXDrawTextCenterBit)		format |= DT_CENTER;
	if (flags&AXDrawTextVCenterBit)		format |= DT_VCENTER;
	if (flags&AXDrawTextNoPrefixBit)	format |= DT_NOPREFIX;
	if (flags&AXDrawTextWordBreakBit)	format |= DT_WORDBREAK;
	
	// calculate
	/*
	::DrawTextA(pWidget->ContextRef(), pszText, -1, &rcArea, format);
	*/
	
	// store
	*pLeft		= rcArea.left;
	*pTop		= rcArea.top;
	*pRight		= rcArea.right;
	*pBottom	= rcArea.bottom;
}

// AXDrawTextGL: draw text in a given rectangle
AX_FUNC void AX_API AXDrawTextGL(AXGadget *pWidget, const char *pszText, int flags, int left, int top, int right, int bottom)
{
	// declarations
	RECT	rcArea = { 0, 0, 0, 0 };
	UINT	format;
	
	// determine the format
	format = 0;
	if (flags&AXDrawTextSingleLineBit)	format |= DT_SINGLELINE;
	if (flags&AXDrawTextCenterBit)		format |= DT_CENTER;
	if (flags&AXDrawTextVCenterBit)		format |= DT_VCENTER;
	if (flags&AXDrawTextNoPrefixBit)	format |= DT_NOPREFIX;
	if (flags&AXDrawTextWordBreakBit)	format |= DT_WORDBREAK;
	
	// specify the rectangle
	rcArea.left		= left;
	rcArea.top		= top;
	rcArea.right	= right;
	rcArea.bottom	= bottom;
	
	// draw
	glFlush();
	/*
	::DrawTextA(pWidget->ContextRef(), pszText, -1, &rcArea, format);
	*/
}

// AXFlushGL: flush all previous drawing commands out
AX_FUNC void AX_API AXFlushGL(void)
{
	glFlush();
}
