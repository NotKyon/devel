﻿#ifndef RAWKJOB_H
#define RAWKJOB_H

#ifndef RJOB_CALL
# define RJOB_CALL
#endif

typedef unsigned int rjob_uint_t;

#if defined( _MSC_VER ) || defined( __INTEL_COMPILER )
typedef unsigned __int64 rjob_uint64_t;
#else
# include <stdint.h>
typedef uint64_t rjob_uint64_t;
#endif
typedef rjob_uint64_t rjob_microsecs_t;

typedef struct rjob_item_s rjob_item_t;
typedef struct rjob_block_s rjob_block_t;
typedef struct rjob_queue_s rjob_queue_t;
typedef struct rjob_pool_s rjob_pool_t;

typedef void( RJOB_CALL *rjob_run_t )( void *cookie );

typedef void *( RJOB_CALL *rjob_alloc_t )( size_t n );
typedef void( RJOB_CALL *rjob_dealloc_t )( void *p );
typedef void( RJOB_CALL *rjob_assert_t )( const char *filename, int line,
	const char *function, const char *expression, const char *message );

typedef enum rjob_error_e {
	/* operation completed successfully */
	kRJob_Success = 1,

	/* an unknown error has occurred */
	kRJob_Error_Unknown = 0,
	/* memory limits exhausted */
	kRJob_Error_OutOfMemory = -1,
	/* invalid argument passed to function */
	kRJob_Error_InvalidArgument = -2,
	/* invalid operation requested */
	kRJob_Error_InvalidOperation = -3,
	/* internal state has been corrupted / is invalid */
	kRJob_Error_Corruption = -4
} rjob_error_t;

typedef enum rjob_runStatus_e {
	/* item has no jobs to submit and is not ready */
	kRJob_RunStatus_Creating = 0L,
	/* item has at least one job to submit and is ready for submission */
	kRJob_RunStatus_Ready = 1L,
	/* item has been submitted to a queue and can now be executed */
	kRJob_RunStatus_Submitted = 2L,
	/* item has been submitted but has stalled and is currently waiting */
	kRJob_RunStatus_Stalled = 3L,
	/* item has completed running successfully */
	kRJob_RunStatus_Completed = 4L,
	/* item is no longer running and the jobs did not complete */
	kRJob_RunStatus_Aborted = -1L
} rjob_runStatus_t;

typedef enum rjob_runMode_e {
	/* do an active loop to reduce number of OS stalls; good for games */
	kRJob_RunMode_Performance,
	/* do a sync-loop to reduce power usage; good for apps */
	kRJob_RunMode_Energy
} rjob_runMode_t;

typedef enum rjob_parallelism_e {
	/* run on default setting of different cores */
	kRJob_Parallelism_Default = 0,
	/* run on all logical (inc. hyperthread) cores */
	kRJob_Parallelism_LogicalCores = -1,
	/* run on all physical cores */
	kRJob_Parallelism_PhysicalCores = -2
} rjob_parallelism_t;

/*
===============================================================================

	RAWKJOB - ITEM

	A single job to be executed.

	This structure is used internally so there's no need to deal with it
	directly.

===============================================================================
*/
struct rjob_item_s {
	rjob_run_t run;
	void *cookie;
	unsigned short barrierIndex;
};

/*
===============================================================================

	RAWKJOB - BLOCK

	Collection of jobs to be executed.

	Add jobs to a block, then submit that block to a queue. Any existing pools
	that have that queue will then attempt to execute that block.

	You can only submit blocks to one queue.

	Creation of any given block is not itself thread-safe. Submitting blocks is
	thread-safe however.

===============================================================================
*/
struct rjob_block_s {
	rjob_alloc_t alloc;
	rjob_dealloc_t dealloc;

	rjob_uint_t jobIndex;
	rjob_uint_t jobCount;
	rjob_uint_t jobCapacity;
	rjob_item_t *jobs;

	int parallelism;
	rjob_uint_t rt_currentParallelism;
	rjob_uint_t rt_affinity;

	rjob_uint_t ct_lastSignal;
	rjob_uint_t ct_lastSync;

	rjob_runStatus_t rt_status;
	rjob_microsecs_t rt_workTime;
	rjob_microsecs_t rt_stallTime;
	rjob_microsecs_t rt_peakTime;

	rjob_uint_t rt_stallCount;

	unsigned short rt_barrierCount;
	void *rt_barriers;

	volatile rjob_uint_t rt_abort;

	rjob_queue_t *queue;
};

void rjob_block_init( rjob_block_t *block );
void rjob_block_fini( rjob_block_t *block );

rjob_error_t rjob_block_setAllocator( rjob_block_t *block, rjob_alloc_t alloc,
	rjob_dealloc_t dealloc );
void rjob_block_getAllocator( const rjob_block_t *block, rjob_alloc_t *alloc,
	rjob_dealloc_t *dealloc );

rjob_error_t rjob_block_addJob( rjob_block_t *block, rjob_run_t run,
	void *cookie );
rjob_error_t rjob_block_addSignal( rjob_block_t *block );
rjob_error_t rjob_block_addSync( rjob_block_t *block );

int rjob_block_isSubmitted( const rjob_block_t *block );
void rjob_block_abort( rjob_block_t *block );

rjob_uint_t rjob_block_getJobCount( const rjob_block_t *block );
rjob_runStatus_t rjob_block_getRunStatus( const rjob_block_t *block );
rjob_microsecs_t rjob_block_getWorkTime( const rjob_block_t *block );
rjob_microsecs_t rjob_block_getStallTime( const rjob_block_t *block );
rjob_microsecs_t rjob_block_getPeakTime( const rjob_block_t *block );
rjob_uint_t rjob_block_getStallCount( const rjob_block_t *block );

/*
===============================================================================

	RAWKJOB - QUEUE

	Collection of blocks to be executed.

	Queues hold a list of blocks which are to be executed by a collection of
	threads (usually spread out across multiple cores).

===============================================================================
*/
struct rjob_queue_s {
	rjob_alloc_t alloc;
	rjob_dealloc_t dealloc;

	rjob_uint_t priority;

	rjob_uint_t blockCount;
	rjob_uint_t blockCapacity;
	rjob_block_t **blocks;

	rjob_runStatus_t rt_status;
	rjob_microsecs_t rt_workTime;
	rjob_microsecs_t rt_stallTime;
	rjob_microsecs_t rt_peakTime;

	rjob_uint_t rt_stallCount;
	rjob_uint_t rt_blockIndex;
	rjob_uint_t rt_lastStalledBlock;

	volatile rjob_uint_t rt_abort;
	volatile rjob_uint_t rt_poolCount;
};

void rjob_queue_init( rjob_queue_t *queue );
void rjob_queue_fini( rjob_queue_t *queue );

rjob_error_t rjob_queue_setAllocator( rjob_queue_t *queue, rjob_alloc_t alloc,
	rjob_dealloc_t dealloc );
void rjob_queue_getAllocator( const rjob_queue_t *queue, rjob_alloc_t *alloc,
	rjob_dealloc_t *dealloc );

void rjob_queue_setPriority( rjob_queue_t *queue, rjob_uint_t priority );
rjob_uint_t rjob_queue_getPriority( const rjob_queue_t *queue );

rjob_error_t rjob_queue_submitBlock( rjob_queue_t *queue, rjob_block_t *block );
void rjob_queue_abort( rjob_queue_t *queue );

rjob_uint_t rjob_queue_getBlockCount( const rjob_queue_t *queue );
const rjob_block_t *rjob_queue_getBlock( const rjob_queue_t *queue,
	rjob_uint_t index );
rjob_runStatus_t rjob_queue_getRunStatus( const rjob_queue_t *queue );
rjob_microsecs_t rjob_queue_getWorkTime( const rjob_queue_t *queue );
rjob_microsecs_t rjob_queue_getStallTime( const rjob_queue_t *queue );
rjob_microsecs_t rjob_queue_getPeakTime( const rjob_queue_t *queue );
rjob_uint_t rjob_queue_getStallCount( const rjob_queue_t *queue );

/*
===============================================================================

	RAWKJOB - POOL

===============================================================================
*/
struct rjob_pool_s {
	rjob_alloc_t alloc;
	rjob_dealloc_t dealloc;

	rjob_runMode_t runMode;
	rjob_uint_t threadCount;
	void *threads;

	rjob_queue_t *queue;

	rjob_uint_t rt_terminate;
};

void rjob_pool_init( rjob_pool_t *pool );
void rjob_pool_fini( rjob_pool_t *pool );

rjob_error_t rjob_pool_setAllocator( rjob_pool_t *pool, rjob_alloc_t alloc,
	rjob_dealloc_t dealloc );
void rjob_pool_getAllocator( const rjob_pool_t *pool, rjob_alloc_t *alloc,
	rjob_dealloc_t *dealloc );

rjob_error_t rjob_pool_setInitThreadCount( rjob_pool_t *pool,
	rjob_uint_t threadCount );
rjob_error_t rjob_pool_setInitRunMode( rjob_pool_t *pool,
	rjob_runMode_t runMode );

rjob_error_t rjob_pool_start( rjob_pool_t *pool );
void rjob_pool_stop( rjob_pool_t *pool );

rjob_uint_t rjob_pool_getThreadCount( const rjob_pool_t *pool );
const rjob_queue_t *rjob_pool_getQueue( const rjob_pool_t *pool );

#endif
