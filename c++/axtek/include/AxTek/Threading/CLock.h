#ifndef AXTEK_THREADING_CLOCK_H
#define AXTEK_THREADING_CLOCK_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

__AX_TASK_NS_INIT__

class CLock {
protected:
#if _WIN32
	CRITICAL_SECTION m_cs;
#else
	pthread_mutex_t m_mutex;
#endif

public:
	inline CLock() {
#if _WIN32
		InitializeCriticalSectionAndSpinCount(&m_cs, 4096);
#else
		static pthread_mutexattr_t attr;
		static bool didInit = false;

		if unlikely (!didInit) {
			pthread_mutexattr_init(&attr);
			pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);

			didInit = true;
		}

		pthread_mutex_init(&m_mutex, &attr);
#endif
	}
	inline ~CLock() {
#if _WIN32
		DeleteCriticalSection(&m_cs);
#else
		pthread_mutex_destroy(&m_mutex);
#endif
	}

	inline void Lock() {
#if _WIN32
		EnterCriticalSection(&m_cs);
#else
		pthread_mutex_lock(&m_mutex);
#endif
	}
	inline bool TryLock() {
#if _WIN32
		return (bool)(TryEnterCriticalSection(&m_cs) & true);
#else
		if (!pthread_mutex_trylock(&m_mutex))
			return true;

		return false;
#endif
	}
	inline void Unlock() {
#if _WIN32
		LeaveCriticalSection(&m_cs);
#else
		pthread_mutex_unlock(&m_mutex);
#endif
	}
};

class CAutolock {
protected:
	CLock &m_lock;

public:
	inline CAutolock(CLock &l): m_lock(l) {
		m_lock.Lock();
	}
	inline ~CAutolock() {
		m_lock.Unlock();
	}
};

__AX_TASK_NS_FINI__

#endif
