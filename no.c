// ... no

#define THE_HEADER_INCLUDED_WITH_THE_STANDARD_C_LIBRARY_THAT_HAS_INPUT_AND_OUTPUT_FUNCTIONS stdio.h
#define THE_HEADER_INCLUDED_WITH_THE_STANDARD_C_LIBRARY_THAT_DEFINES_EXIT_SUCCESS_AND_SOME_OTHER_NEAT_STUFF stdlib.h
#define WE_ARE_INCLUDING_A_SYSTEM_HEADER <
#define WE_HAVE_INCLUDED_A_SYSTEM_HEADER >

#include WE_ARE_INCLUDING_A_SYSTEM_HEADER THE_HEADER_INCLUDED_WITH_THE_STANDARD_C_LIBRARY_THAT_HAS_INPUT_AND_OUTPUT_FUNCTIONS WE_HAVE_INCLUDED_A_SYSTEM_HEADER
#include WE_ARE_INCLUDING_A_SYSTEM_HEADER THE_HEADER_INCLUDED_WITH_THE_STANDARD_C_LIBRARY_THAT_DEFINES_EXIT_SUCCESS_AND_SOME_OTHER_NEAT_STUFF WE_HAVE_INCLUDED_A_SYSTEM_HEADER

#define THIS_MACRO_INDICATES_THAT_THIS_FUNCTION_DOES_NOT_ALTER_NOR_DEPEND_UPON_ANY_VARYING_GLOBAL_STATE_BUT_MIGHT_DEPEND_ON_GLOBAL_CONSTANTS __attribute__( ( const ) )

THIS_MACRO_INDICATES_THAT_THIS_FUNCTION_DOES_NOT_ALTER_NOR_DEPEND_UPON_ANY_VARYING_GLOBAL_STATE_BUT_MIGHT_DEPEND_ON_GLOBAL_CONSTANTS
int ThisCommandTakesTwoIntegerValuesOnTheStackUsingTheCDeclCallingConventionThenPutsThoseValuesIntoRegistersAndSumsThemIntoAnotherRegisterAndRestoresTheStackPointerThenIssuesARETInstructionToTheProcessor( int thisIsTheFirstValueToBeSummed, int thisIsTheSecondValueToBeSummed )
{
	return thisIsTheFirstValueToBeSummed + thisIsTheSecondValueToBeSummed;
}

int main()
{

	const int thisIsTheFirstNonvaryingValueToBeUsedInASummingTestForTheCommandDeclaredAbove = 2;
	const int thisIsTheSecondNonvaryingValueToBeUsedInASummingTestForTheCommandDeclaredAbove = 2;

	const int thisIsTheResultOfTheSummingTestOfTheTwoNonvaryingValuesDeclaredAbove = ThisCommandTakesTwoIntegerValuesOnTheStackUsingTheCDeclCallingConventionThenPutsThoseValuesIntoRegistersAndSumsThemIntoAnotherRegisterAndRestoresTheStackPointerThenIssuesARETInstructionToTheProcessor( thisIsTheFirstNonvaryingValueToBeUsedInASummingTestForTheCommandDeclaredAbove, thisIsTheSecondNonvaryingValueToBeUsedInASummingTestForTheCommandDeclaredAbove );

	printf( "%i\n", thisIsTheResultOfTheSummingTestOfTheTwoNonvaryingValuesDeclaredAbove );

#define THIS_PROGRAM_HAS_EXECUTED_SUCCESSFULLY_PROBABLY EXIT_SUCCESS
	return THIS_PROGRAM_HAS_EXECUTED_SUCCESSFULLY_PROBABLY;

}
