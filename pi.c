#include <stdio.h>
#include <stdlib.h>

typedef float f32;
static const f32 PI_F32 = 3.1415926535897935897932384626433832795028841971693993751058209f;

int main() {
	printf("%.15f\n", PI_F32);
	return EXIT_SUCCESS;
}
