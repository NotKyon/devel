/*
 * This file is not meant to be included directly...
 */

#if GB_INIT && _WIN32
# ifndef WIN32_LEAN_AND_MEAN
#  define WIN32_LEAN_AND_MEAN 1
# endif
# include <windows.h>
#endif
