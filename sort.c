#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>

int cmp(const void *a, const void *b) {
	if (*(int *)a > *(int *)b) return  1;
	if (*(int *)a < *(int *)b) return -1;

	return 0;
}

void sortInts(int *p, size_t n) {
	qsort((void *)p, n, sizeof(int), cmp);
}

void doSort(size_t n, ...) {
	va_list args;
	size_t i;
	int arr[32];

	if ((n - 1) >= sizeof(arr)/sizeof(arr[0]))
		return;

	printf("Unsorted:\n");

	va_start(args, n);
	for(i=0; i<n; i++) {
		arr[i] = va_arg(args, int);
		printf("  %2i/%i: %i\n", 1 + (int)i, (int)n, arr[i]);
	}
	va_end(args);

	printf("\nSorting...\n");fflush(stdout);
	sortInts(arr, n);

	printf("\nSorted:\n");

	for(i=0; i<n; i++)
		printf("  %2i/%i: %i\n", 1 + (int)i, (int)n, arr[i]);

	printf("\n");
}

int main() {
	doSort(3, 1,3,2);
	doSort(4, 5,9,12,3);
	doSort(2, 4,1);

	return 0;
}
