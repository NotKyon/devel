#include <errno.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef DEBUG_ENABLED
# if defined( DEBUG ) || defined( _DEBUG ) || defined( __debug__ )
#  define DEBUG_ENABLED 1
# else
#  define DEBUG_ENABLED 0
# endif
#endif

#ifndef ASSERT_ENABLED
# if DEBUG_ENABLED
#  define ASSERT_ENABLED 1
# else
#  define ASSERT_ENABLED 0
# endif
#endif

#ifndef NORETURN
# if defined( _MSC_VER )
#  define NORETURN __declspec( noreturn )
# else
#  define NORETURN __attribute__( ( noreturn ) )
# endif
#endif

#ifndef CURRENT_FUNCTION
# if defined( _MSC_VER )
#  define CURRENT_FUNCTION __FUNCTION__
# else
#  define CURRENT_FUNCTION __func__
# endif
#endif

#if ASSERT_ENABLED
# define ASSERT( expr )\
	( ( void )( !!( expr ) || ( Com_FatalError( __FILE__, __LINE__,\
		CURRENT_FUNCTION, "Assert failure: " #expr ), 0 ) ) )
#else
# define ASSERT( expr )\
	( ( void )0 )
#endif

typedef struct lineInfo_s lineInfo_t;

struct lineInfo_s {
	size_t offset;
	uint32_t line;
	uint32_t column;
};

NORETURN void Com_FatalError( const char *file, int line, const char *func,
const char *message ) {
	fprintf( stderr, "[%s(%i) %s] FATAL ERROR: %s\n", file, line, func,
		message );

#if DEBUG_ENABLED
# ifdef _WIN32
	if( IsDebuggerAttached() ) {
		DebugBreak();
	}
# else
	__builtin_trap();
# endif
#endif

	exit( EXIT_FAILURE );
}

void Com_LexerError( const char *filename, const lineInfo_t *linfo,
const char *message ) {
	ASSERT( filename != NULL );
	ASSERT( linfo != NULL );
	ASSERT( message != NULL );

	fprintf( stderr, "[%s(%u:%u)] ERROR: %s\n", filename, linfo->line,
		linfo->column );
}

int Lex_GetLineInfo( lineInfo_t *linfo, const char *base, const char *curr ) {
	const char *p;
	const char *s;

	linfo->offset = curr - base;
	linfo->line = 1;
	linfo->column = 0;

	s = base;
	for( p = base; p < curr; ++p ) {
		if( *p == '\0' ) {
			return 0;
		}

		if( *p == '\n' ) {
			s = p;
			++linfo->line;
		}
	}

	linfo->column = curr - base;
	return 1;
}

const char *Lex_SkipWhite( const char *p ) {
	ASSERT( p != NULL );

	while( *p <= ' ' ) {
		if( *p == '\0' ) {
			break;
		}

		++p;
	}

	return p;
}
const char *Lex_FindToken( const char *p, const char *

int main( int argc, char **argv ) {
	return EXIT_SUCCESS;
}
