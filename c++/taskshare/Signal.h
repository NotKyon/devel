#ifndef TASKSHARE_SIGNAL_H
#define TASKSHARE_SIGNAL_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

#include "Atomics.h"

//------------------------------------------------------------------------------
// SIGNALS

__TS_NS_INIT__

typedef struct signal_s {
	unsigned int expCnt, curCnt;
#if _WIN32
	HANDLE ev;
#else
	pthread_mutex_t mutex;
	pthread_cond_t cond;
#endif
} signal_t;

inline void InitSignal(signal_t &sig, unsigned int expCnt);
inline void FiniSignal(signal_t &sig);

inline void ResetSignal(signal_t &sig, unsigned int expCnt=0, bool force=false);
inline void AllocSignalSlot(signal_t &sig);
inline void RaiseSignal(signal_t &sig);

inline void SyncSignal(signal_t &sig);
inline bool TrySyncSignal(signal_t &sig);

inline void SpinSyncSignal(signal_t &sig, size_t spinCount=4000);
inline bool SyncSignals(size_t *index, size_t n, signal_t **sigs,
	bool all=false);

inline void InitSignal(signal_t &sig, unsigned int expCnt) {
	sig.expCnt = expCnt;
	sig.curCnt = 0;

#if _WIN32
	sig.ev = CreateEvent(0, TRUE, FALSE, 0);
#else
	pthread_mutex_init(&sig.mutex, 0);
	pthread_cond_init(&sig.cond, 0);
#endif
}
inline void FiniSignal(signal_t &sig) {
#if _WIN32
	if (sig.ev) {
		CloseHandle(sig.ev);
		sig.ev = 0;
	}
#else
	pthread_cond_destroy(&sig.cond);
	pthread_mutex_destroy(&sig.mutex);
#endif
}

inline void ResetSignal(signal_t &sig, unsigned int expCnt, bool force) {
	if (expCnt || force)
		sig.expCnt = expCnt;
	sig.curCnt = 0;

#if _WIN32
	ResetEvent(sig.ev);
#else
#endif
}
inline void AllocSignalSlot(signal_t &sig) {
	sig.expCnt++;

#if _WIN32
	if (sig.curCnt>=(sig.expCnt - 1))
		ResetEvent(sig.ev);
#endif
}
inline void RaiseSignal(signal_t &sig) {
	AtomicIncrement(&sig.curCnt);

	if (sig.curCnt==sig.expCnt) {
#if _WIN32
		SetEvent(sig.ev);
#else
		pthread_cond_signal(&sig.cond);
			//NOTE: don't use broadcast(); causes hang
#endif
	}
}

inline void SyncSignal(signal_t &sig) {
#if _WIN32
	WaitForSingleObject(sig.ev, INFINITE);
#else
	//pthread_mutex_lock(&sig.mutex);
	while(sig.curCnt < sig.expCnt)
		pthread_cond_wait(&sig.cond, &sig.mutex);
	//pthread_mutex_unlock(&sig.mutex);
#endif
}
inline bool TrySyncSignal(signal_t &sig) {
#if _WIN32
	if (WaitForSingleObject(sig.ev, 0) != WAIT_OBJECT_0)
		return false;

	return true;
#else
	static const struct timespec ts = { 0, 0 };
	bool r = false;

	//pthread_mutex_lock(&sig.mutex);
	if (pthread_cond_timedwait(&sig.cond, &sig.mutex, &ts) == 0)
		r = true;
	//pthread_mutex_unlock(&sig.mutex);

	return r;
#endif
}

inline void SpinSyncSignal(signal_t &sig, size_t spinCount) {
	size_t i;

	for(i=0; i<spinCount; i++) {
		if (TrySyncSignal(sig))
			return;
	}

	SyncSignal(sig);
}
inline bool SyncSignals(size_t *index, size_t n, signal_t **sigs, bool all) {
#if _WIN32
	HANDLE h[128];
	size_t i;
	DWORD r;

	if (n > sizeof(h)/sizeof(h[0]))
		return false;

	if (!sigs)
		return false;

	for(i=0; i<n; i++) {
		if (!sigs[i])
			return false;
		h[i] = sigs[i]->ev;
	}

	r = WaitForMultipleObjects(n, h, (BOOL)all, INFINITE);
	if (index) {
		if (r - WAIT_OBJECT_0 < n)
			*index = r - WAIT_OBJECT_0;
		else if(r - WAIT_ABANDONED_0 < n)
			*index = r - WAIT_ABANDONED_0;
	}

	return true;
#else
	signal_t *h[128];
	size_t i, j;

	if (n > sizeof(h)/sizeof(h[0]))
		return false;

	if (!sigs)
		return false;

	memset((void *)h, 0, sizeof(h));

	j = 0;

	for(i=0; i<n; i++) {
		if (h[i])
			continue;

		if (TrySyncSignal(*(sigs[i]))) {
			if (!all) {
				if (index)
					*index = i;

				break;
			}

			h[i] = sigs[i];
			j++;

			if (j==n)
				break;
		}
	}

	return true;
#endif
}

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
