#ifndef AXTEK_THREADING_ATOMICS_H
#define AXTEK_THREADING_ATOMICS_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "../Base/Config.h"

//
//	The following atomic operations are available on 32-bit and 64-bit integers.
//
//		atomic_inc			Increments 'dst' by one.
//		atomic_dec			Decrements 'dst' by one.
//		atomic_set			Sets 'dst' to value stored in 'src'.
//		atomic_set_eq		Sets 'dst' to value stored in 'src' if equals 'cmp'.
//		atomic_and			Performs bitwise and on 'dst' with 'src'.
//		atomic_or			Performs bitwise or on 'dst' with 'src'.
//		atomic_xor			Performs bitwise xor on 'dst' with 'src'.
//		atomic_get			Retrieves value stored in 'dst'.
//
//	Only atomic_set, atomic_set_eq, and atomic_get are available for pointers.
//
//	TODO: atomic_xxx_acq, and atomic_xx_rel for acquire/release semantics, but
//	      ignored on processors not supporting said semantics.
//
//	FIXME: Ensure each operation works uniformly across implementations.
//

#if _MSC_VER
# include <intrin.h>

# pragma intrinsic(_InterlockedIncrement)
# pragma intrinsic(_InterlockedDecrement)
# pragma intrinsic(_InterlockedExchange)
# pragma intrinsic(_InterlockedCompareExchange)
# pragma intrinsic(_InterlockedAnd)
# pragma intrinsic(_InterlockedOr)
# pragma intrinsic(_InterlockedXor)

# if _WIN64
#  pragma intrinsic(_InterlockedIncrement64)
#  pragma intrinsic(_InterlockedDecrement64)
#  pragma intrinsic(_InterlockedExchange64)
#  pragma intrinsic(_InterlockedCompareExchange64)
#  pragma intrinsic(_InterlockedAnd64)
#  pragma intrinsic(_InterlockedOr64)
#  pragma intrinsic(_InterlockedXor64)
# else
#  define _InterlockedIncrement64 InterlockedIncrement64
#  define _InterlockedDecrement64 InterlockedDecrement64
#  define _InterlockedExchange64 InterlockedExchange64
#  define _InterlockedCompareExchange64 InterlockedCompareExchange64
#  define _InterlockedAnd64 InterlockedAnd64
#  define _InterlockedOr64 InterlockedOr64
#  define _InterlockedXor64 InterlockedXor64
# endif

# if 0
#  pragma intrinsic(_InterlockedExchangePointer)
#  pragma intrinsic(_InterlockedCompareExchangePointer)
# else
#  define _InterlockedExchangePointer InterlockedExchangePointer
#  define _InterlockedCompareExchangePointer InterlockedCompareExchangePointer
# endif
#endif

// http://gcc.gnu.org/onlinedocs/gcc-4.4.2/gcc/Atomic-Builtins.html

__AX_TASK_NS_INIT__

//------------------------------------------------------------------------------
// 32-bit

__forceinline u32 atomic_inc(volatile u32 *x) {
#if _MSC_VER
	return _InterlockedIncrement((volatile LONG *)x);
#else
	return __sync_fetch_and_add(x, 1);
#endif
}
__forceinline u32 atomic_dec(volatile u32 *x) {
#if _MSC_VER
	return _InterlockedDecrement((volatile LONG *)x);
#else
	return __sync_fetch_and_sub(x, 1);
#endif
}
__forceinline u32 atomic_set(volatile u32 *x, u32 y) {
#if _MSC_VER
	return _InterlockedExchange((volatile LONG *)x, y);
#else
	return __sync_lock_test_and_set(x, y);
#endif
}
__forceinline u32 atomic_set_eq(volatile u32 *dst, u32 src, u32 cmp) {
#if _MSC_VER
	return _InterlockedCompareExchange((volatile LONG *)dst, src, cmp);
#else
	return __sync_val_compare_and_swap(dst, cmp, src);
#endif
}
__forceinline u32 atomic_and(volatile u32 *x, u32 y) {
#if _MSC_VER
	return _InterlockedAnd((volatile LONG *)x, y);
#else
	return __sync_fetch_and_and(x, y);
#endif
}
__forceinline u32 atomic_or(volatile u32 *x, u32 y) {
#if _MSC_VER
	return _InterlockedOr((volatile LONG *)x, y);
#else
	return __sync_fetch_and_or(x, y);
#endif
}
__forceinline u32 atomic_xor(volatile u32 *x, u32 y) {
#if _MSC_VER
	return _InterlockedXor((volatile LONG *)x, y);
#else
	return __sync_fetch_and_xor(x, y);
#endif
}

__forceinline u32 atomic_get(volatile u32 *x) {
	return atomic_or(x, 0);
}

//------------------------------------------------------------------------------
// 64-bit

__forceinline u64 atomic_inc(volatile u64 *x) {
#if _MSC_VER
	return _InterlockedIncrement64((volatile LONGLONG *)x);
#else
	return __sync_fetch_and_add(x, 1);
#endif
}
__forceinline u64 atomic_dec(volatile u64 *x) {
#if _MSC_VER
	return _InterlockedDecrement64((volatile LONGLONG *)x);
#else
	return __sync_fetch_and_sub(x, 1);
#endif
}
__forceinline u64 atomic_set(volatile u64 *x, u64 y) {
#if _MSC_VER
	return _InterlockedExchange64((volatile LONGLONG *)x, y);
#else
	return __sync_lock_test_and_set(x, y);
#endif
}
__forceinline u64 atomic_set_eq(volatile u64 *dst, u64 src, u64 cmp) {
#if _MSC_VER
	return _InterlockedCompareExchange64((volatile LONGLONG *)dst, src, cmp);
#else
	return __sync_val_compare_and_swap(dst, cmp, src);
#endif
}
__forceinline u64 atomic_and(volatile u64 *x, u64 y) {
#if _MSC_VER
	return _InterlockedAnd64((volatile LONGLONG *)x, y);
#else
	return __sync_fetch_and_and(x, y);
#endif
}
__forceinline u64 atomic_or(volatile u64 *x, u64 y) {
#if _MSC_VER
	return _InterlockedOr64((volatile LONGLONG *)x, y);
#else
	return __sync_fetch_and_or(x, y);
#endif
}
__forceinline u64 atomic_xor(volatile u64 *x, u64 y) {
#if _MSC_VER
	return _InterlockedXor64((volatile LONGLONG *)x, y);
#else
	return __sync_fetch_and_xor(x, y);
#endif
}

__forceinline u64 atomic_get(volatile u64 *x) {
	return atomic_or(x, 0);
}

//------------------------------------------------------------------------------
// pointer

template<typename T>
__forceinline T *atomic_set(T *volatile *x, T *y) {
#if _MSC_VER
	return _InterlockedExchangePointer((void *volatile *)x, (void *)y);
#else
	return __sync_lock_test_and_set(x, y);
#endif
}
template<typename T>
__forceinline T *atomic_set_eq(T *volatile *dst, T *src, T *cmp) {
#if _MSC_VER
	return _InterlockedCompareExchangePointer((void *volatile *)dst,
		(void *)src, (void *)cmp);
#else
	return __sync_val_compare_and_swap(dst, cmp, src);
#endif
}

template<typename T>
__forceinline T *atomic_get(T *volatile *x) {
#if AXTEK_ARCH_BITS==64
	return (T *)atomic_get((volatile u64 *)x);
#elif AXTEK_ARCH_BITS==32
	return (T *)atomic_get((volatile u32 *)x);
#else
	if (sizeof(T *)==8)
		return (T *)atomic_get((volatile u64 *)x);

	return (T *)atomic_get((volatile u32 *)x);
#endif
}

//------------------------------------------------------------------------------

__AX_TASK_NS_FINI__

#endif
