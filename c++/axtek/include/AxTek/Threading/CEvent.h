#ifndef AXTEK_THREADING_CEVENT_H
#define AXTEK_THREADING_CEVENT_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

__AX_TASK_NS_INIT__

class CEvent {
protected:
#if _WIN32
	HANDLE m_ev;
#else
	pthread_mutex_t m_mutex;
	pthread_cond_t m_cond;
#endif

public:
	inline CEvent() {
#if _WIN32
		m_ev = CreateEvent(0, TRUE, FALSE, 0);
#else
		pthread_mutex_init(&m_mutex, 0);
		pthread_cond_init(&m_cond, 0);
#endif
	}
	inline ~CEvent() {
#if _WIN32
		if (!m_ev)
			return;

		CloseHandle(m_ev);
		m_ev = (HANDLE)NULL;
#else
		pthread_cond_destroy(&m_cond);
		pthread_mutex_destroy(&m_mutex);
#endif
	}

	inline void Signal() {
#if _WIN32
		SetEvent(m_ev);
#else
		pthread_cond_signal(&m_cond);
#endif
	}
	inline void Reset() {
#if _WIN32
		ResetEvent(m_ev);
#else
		/* TODO */
#endif
	}

	inline void Sync() {
#if _WIN32
		WaitForSingleObject(m_ev, INFINITE);
#else
		pthread_mutex_lock(&m_mutex);
		pthread_cond_wait(&m_cond, &m_mutex);
		pthread_mutex_unlock(&m_mutex);
#endif
	}
	inline bool Wait(u32 milliseconds) {
#if _WIN32
		if (WaitForSingleObject(m_ev, milliseconds) != WAIT_OBJECT_0)
			return false;

		return true;
#else
		struct timespec ts = {
			milliseconds/1000, (milliseconds%1000)*1000000
		};
		bool r = false;

		pthread_mutex_lock(&m_mutex);
		if (pthread_cond_timedwait(&m_cond, &m_mutex, &ts) == 0)
			r = true;
		pthread_mutex_unlock(&m_mutex);

		return r;
#endif
	}
	inline bool TryWait() {
		return Wait(0);
	}
};

__AX_TASK_NS_FINI__

#endif
