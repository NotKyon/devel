#include <stdio.h>
#include <stdlib.h>

typedef unsigned int uint;

/*

	Quad-Row	Quad-Col
	0 1 4 5		0 2 8 A
	2 3 6 7		1 3 9 B
	8 9 C D		4 6 C E
	A B E F		5 7 D F

	Output for Quad-Row should be:
	
	0, 0	1, 0	0, 1	1, 1
	2, 0	3, 0	2, 1	3, 1
	0, 2	1, 2	0, 3	1, 3
	2, 2	3, 2	2, 3	3, 3
*/

#if 0
void LinearToQuadRow( uint linear, uint res, uint *qx, uint *qy, bool info )
{
	/*
	// NOTE: This WORKS for res=4
	const uint quadIndex = linear/res;
	const uint initQuadX = quadIndex%2;
	const uint initQuadY = quadIndex/2;

	//const uint offsetToQuad = quadIndex*res;
	//const uint localIndex = linear - offsetToQuad;
	const uint localIndex = linear%res;

	const uint localOffX = localIndex%2;
	const uint localOffY = localIndex/2;

	*qx = initQuadX*2 + localOffX;
	*qy = initQuadY*2 + localOffY;
	*/

	( void )res;

	/*
#define DO_OP( var, stmt )\
	if( info ) printf( "    %s; //%u -> ", #stmt, var );\
	stmt;\
	if( info ) printf( "%u\n", var )

#define DECL_CONST( var, initVal )\
	const uint var = initVal;\
	if( info ) printf( "  const uint %s = %s; //%u\n", #var, #initVal, var )

#define WRITE_VAR( var, expr )\
	var = expr;\
	if( info ) printf( "  %s = %s; //%u\n", #var, #expr, var )

	if( info )
	{
		printf( "Qrow<%u>(%u)\n", res, linear );
	}

	uint input = linear;
	uint level = 0;
	DECL_CONST( test, 4*4 );

	while( input >= test )
	{
		if( info )
		{
			printf( "  while... ( input >= test )\n" );
		}

		DO_OP( level, ++level );

		DO_OP( input, input /= test );
		//DO_OP( test, test *= 4 );
	}

	DECL_CONST( levelIndex, input/4 );
	DECL_CONST( localIndex, input%4 );

	DECL_CONST( levelOffX, levelIndex%2 );
	DECL_CONST( levelOffY, levelIndex/2 );

	DECL_CONST( localOffX, localIndex%2 );
	DECL_CONST( localOffY, localIndex/2 );

	WRITE_VAR( *qx, 4*( level%2 ) + levelOffX*2 + localOffX );
	WRITE_VAR( *qy, 4*( level/2 ) + levelOffY*2 + localOffY );

	if( info )
	{
		printf( "\n" );
	}
	*/
	
	( void )info;

	uint posX = 0;
	uint posY = 0;

	for( uint i = 0; i < 16; ++i )
	{
		posX |= ( linear & ( 1<<( i*2 ) ) )>>( i + 0 );
		posY |= ( linear & ( 2<<( i*2 ) ) )>>( i + 1 );
	}

	*qx = posX;
	*qy = posY;
}
#else
void LinearToQuadRow( uint linear, uint res, uint *qx, uint *qy, bool info )
{
	// http://fgiesen.wordpress.com/2009/12/13/decoding-morton-codes/

	// -f-e -d-c -b-a -9-8 -7-6 -5-4 -3-2 -1-0
	uint posX = ( linear >> 0 ) & 0x55555555;
	uint posY = ( linear >> 1 ) & 0x55555555;

	// --fe --dc --ba --98 --76 --54 --32 --10
	posX = ( posX ^ ( posX >> 1 ) ) & 0x33333333;
	posY = ( posY ^ ( posY >> 1 ) ) & 0x33333333;

	// ---- fedc ---- ba98 ---- 7654 ---- 3210
	posX = ( posX ^ ( posX >> 2 ) ) & 0x0F0F0F0F;
	posY = ( posY ^ ( posY >> 2 ) ) & 0x0F0F0F0F;

	// ---- ---- fedc ba98 ---- ---- 7654 3210
	posX = ( posX ^ ( posX >> 4 ) ) & 0x00FF00FF;
	posY = ( posY ^ ( posY >> 4 ) ) & 0x00FF00FF;

	// ---- ---- ---- ---- fedc ba98 7654 3210
	posX = ( posX ^ ( posX >> 8 ) ) & 0x0000FFFF;
	posY = ( posY ^ ( posY >> 8 ) ) & 0x0000FFFF;

	*qx = posX;
	*qy = posY;
}
#endif

void test( uint linear, uint res )
{
	uint qrx, qry;

	LinearToQuadRow( linear, res, &qrx, &qry, false );

	printf( "Qrow<%u>(%2u) = (%u,%u)\n", res, linear, qrx, qry );
}

int main()
{
	/*
	const uint res = 4;
	for( uint i = 0; i <= res*res; ++i )
	{
		test( i, res );
	}
	*/

	/*
	const uint res = 8;
	uint data[ res*res ];

	for( uint i = 0; i < sizeof( data )/sizeof( data[ 0 ] ); ++i )
	{
		data[ i ] = 1984;
	}

	printf( "Generating (%u)...\n", res ); fflush( stdout );

	for( uint i = 0; i < res*res; ++i )
	{
		uint x, y;
		LinearToQuadRow( i, res, &x, &y, true );
		if( x >= res || y >= res )
		{
			fprintf( stderr, "ERROR: Invalid output (%u,%u) from %u\n",
				x, y, i );
			return EXIT_FAILURE;
		}

		data[ y*res + x ] = i;
	}

	printf( "Checking...\n" ); fflush( stdout );

	// each value in 'data' should be unique
	for( uint i = 0; i < res*res; ++i )
	{
		const uint datum = data[ i ];
		const uint datum_x = i/res;
		const uint datum_y = i%res;

		for( uint j = i + 1; j < res*res; ++j )
		{
			const uint cmp = data[ j ];
			const uint cmp_x = j/res;
			const uint cmp_y = j%res;

			if( datum == cmp )
			{
				fprintf( stderr, "ERROR: Duplicate value %u at (%u,%u) "
					"and (%u,%u)\n", datum, datum_x, datum_y, cmp_x, cmp_y );
				fflush( stdout );

				uint sx, sy;
				LinearToQuadRow( i, res, &sx, &sy, true );
				LinearToQuadRow( j, res, &sx, &sy, true );

				return EXIT_FAILURE;
			}
		}
	}

	printf( "All valid!\n" );
	*/

	const uint res = 8;
	for( uint i = 0; i < res*res; ++i )
	{
		test( i, res );
	}

	return EXIT_SUCCESS;
}
