#include <stdio.h>

class C {
public:
	 C() { printf("+C\n"); }
	~C() { printf("-C\n"); }
};

void *operator new(size_t n, C &x) {
	return &x;
}
void operator delete(void *p, C &x) {
}

int main() {
	C x, *p;

	p = new(x) C; //placement new

	p->~C(); //invoke destructor manually
	operator delete(p, x); //invoke custom delete routine (does nothing in this example)

	return 0;
}
