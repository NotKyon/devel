#ifndef MESHIO_TRANSFORM_H
#define MESHIO_TRANSFORM_H

#define MESHIO_NANOSECONDS_PER_SECOND ( 1000000000 ) /* one billion */

#ifndef meshio_transform_t_defined
#define meshio_transform_t_defined
typedef struct MeshIO_Transform *meshio_transform_t;
#endif

struct MeshIO_Transform {
	unsigned int refcnt;

	struct {
		float x, y, z;
	} translation,
	    rotation,
	    scale;

	float rotation_quat[ 4 ];
	float rotation_mat[ 9 ];

	struct {
		size_t n;
		void *p;
	} data[ 2 ];

	struct {
		struct {
			uint32_t s; /* seconds */
			uint32_t n; /* nanoseconds (1000000000 per second) */
		} length;
		struct MeshIO_Transform *prev,
		    *next;
	} frame;

	struct MeshIO_Transform *prev,
	    *next;
};

#endif
