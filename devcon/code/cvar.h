#ifndef CVAR_H
#define CVAR_H

#include "basictypes.h"
#include "lookup.h"

/*
 * =============================================================================
 *
 *	CVAR SYSTEM
 *
 * =============================================================================
 */
enum {
	kCVar_String = 0,
	kCVar_Integer = 1,
	kCVar_Boolean = 2,
	kCVar_Float = 3,

	kCVar_NoCheat_Bit = BIT(2), /* not considered cheating if modified */
	kCVar_Const_Bit = BIT(3), /* read-only */
	kCVar_Modified_Bit = BIT(4), /* cvar has been modified */
	kCVar_MinMax_Bit = BIT(5), /* use the min/max */

	kCVar_NoModify_Bit = BIT(31), /* don't modify the bit flags (not saved) */

	kCVar_Type_Mask = BIT(0)|BIT(1)
};

typedef struct cvar_s {
	char *name;
	int flags;

	struct {
		char *s;
		int i;
		float f;
	} value;

	union {
		int i;
		float f;
	} l, h; /*min/max*/

	struct cvar_s *prev, *next;
} cvar_t;

typedef struct cvarSystem_s {
	struct cvar_s *head, *tail;
	lookup_t table;
} cvarSystem_t;

extern int g_cvarsAlive;
extern cvarSystem_t g_cvars;

/*
 * Initialize the CVar system.
 *
 * return: 1 if initialized successfully.
 */
int cvar_Init();

/*
 * Deinitialize the CVar system.
 */
void cvar_Deinit();

/*
 * Determine whether the CVar system is active.
 *
 * return: 1 if the CVar system has been initialized; 0 otherwise.
 */
int cvar_IsAlive();

/*
 * Find an existing CVar.
 *
 * name: Name (case-sensitive) of the cvar to find.
 * return: Handle to the CVar if it exists; NULL otherwise.
 */
cvar_t *cvar_Find(const char *name);

/*
 * Create a new CVar, or reuse an existing CVar if one of the same name exists.
 *
 * name: Name (case-sensitive) of the cvar to create.
 * flags: Flags (bit-OR'd together). A type should be specified (kCVar_String,
 *        kCVar_Integer, or kCVar_Boolean). If no type is specified, kCar_String
 *        is assumed. See the kCVar enumeration.
 * value: Value of the CVar represented as a string. (Will be converted.)
 * return: Handle to the CVar if successful; NULL otherwise.
 */
cvar_t *cvar_New(const char *name, int flags, const char *value);

/*
 * Delete an existing CVar.
 *
 * cvar: Handle to the CVar to delete.
 */
void cvar_Delete(cvar_t *cvar);

/*
 * Retrieve the name of the CVar.
 *
 * cvar: Handle to the CVar whose name shall be retrieved.
 * return: Name of the CVar. Do not modify or delete.
 */
const char *cvar_Name(const cvar_t *cvar);

/*
 * Retrieve the type of the CVar.
 *
 * cvar: Handle to the CVar whose type shall be retrieved.
 * return: kCVar_String, kCVar_Integer, or kCVar_Boolean.
 */
int cvar_Type(const cvar_t *cvar);

/*
 * Make a CVar constant.
 *
 * cvar: Handle to the CVar whose data shall cease to be allowed to be altered.
 */
void cvar_ConstOn(cvar_t *cvar);

/*
 * Make a CVar variable.
 *
 * cvar: Handle to the CVar whose data shall be allowed to be altered.
 */
void cvar_ConstOff(cvar_t *cvar);

/*
 * Determine whether a CVar is constant.
 *
 * cvar: Handle to the CVar whose constant state shall be retrieved.
 * return: 1 if kCVar_Const_Bit is set; 0 otherwise.
 */
int cvar_IsConst(const cvar_t *cvar);

/*
 * Allow the use of a CVar to not be considered cheating.
 *
 * cvar: Handle to the CVar whose no-cheat state will be enabled.
 */
void cvar_NoCheatOn(cvar_t *cvar);

/*
 * Allow the use of a CVar to be considered cheating.
 *
 * cvar: Handle to the CVar whose no-cheat state will be disabled.
 */
void cvar_NoCheatOff(cvar_t *cvar);

/*
 * Determine whether a CVar's no-cheat state is enabled.
 *
 * cvar: Handle to the CVar whose no-cheat state shall be retrieved.
 * return: 1 if kCVar_NoCheat_Bit is set; 0 otherwise.
 */
int cvar_IsNoCheat(const cvar_t *cvar);

/*
 * Set the CVar modified state.
 *
 * cvar: Handle to the CVar whose modified state will be enabled.
 */
void cvar_ModifiedOn(cvar_t *cvar);

/*
 * Clear the CVar modified state.
 *
 * cvar: Handle to the CVar whose modified state will be disabled.
 */
void cvar_ModifiedOff(cvar_t *cvar);

/*
 * Determine whether the CVar modified state is set.
 *
 * cvar: Handle to the CVar whose modified state shall be retrieved.
 * return: 1 if enabled; 0 otherwise.
 */
int cvar_IsModified(const cvar_t *cvar);

/*
 * Allow the CVar min/max values to be used.
 *
 * cvar: Handle to the CVar whose min/max state will be enabled.
 */
void cvar_MinMaxOn(cvar_t *cvar);

/*
 * Allow the CVar min/max values to be used.
 *
 * cvar: Handle to the CVar whose min/max state will be disabled.
 */
void cvar_MinMaxOff(cvar_t *cvar);

/*
 * Determine whether the CVar min/max values are being used.
 *
 * cvar: Handle to the CVar whose min/max state shall be retrieved.
 * return: 1 if enabled; 0 otherwise.
 */
int cvar_IsMinMax(const cvar_t *cvar);

/*
 * Set the string of a CVar.
 * NOTE: The CVar must be a string!
 *
 * cvar: Handle to the CVar whose string value shall be set.
 * s: String whose data shall be copied into the CVar's internal string.
 */
void cvar_SetString(cvar_t *cvar, const char *s);

/*
 * Set the integer of a CVar.
 * NOTE: The CVar must be an integer!
 *
 * cvar: Handle to the CVar whose integer value shall be set.
 * i: New integer value to use.
 */
void cvar_SetInteger(cvar_t *cvar, int i);

/*
 * Set the boolean of a CVar.
 * NOTE: The CVar must be a boolean!
 *
 * cvar: Handle to the CVar whose boolean value shall be set.
 * i: New boolean value to use.
 */
void cvar_SetBoolean(cvar_t *cvar, int i);

/*
 * Set the float of a CVar.
 * NOTE: The CVar must be a float!
 *
 * cvar: Handle to the CVar whose float value shall be set.
 * f: New float value to use.
 */
void cvar_SetFloat(cvar_t *cvar, float f);

/*
 * Retrieve the string of a CVar.
 * NOTE: The CVar must be a string!
 *
 * cvar: Handle to the CVar whose string value shall be retrieved.
 * return: String value of the CVar. Do not modify or delete this.
 */
const char *cvar_String(const cvar_t *cvar);

/*
 * Retrieve the integer of a CVar.
 * NOTE: The CVar must be an integer!
 *
 * cvar: Handle to the CVar whose integer value shall be retrieved.
 * return: Integer value of the CVar.
 */
int cvar_Integer(cvar_t *cvar);

/*
 * Retrieve the boolean of a CVar.
 * NOTE: The CVar must be a boolean!
 *
 * cvar: Handle to the CVar whose boolean value shall be retrieved.
 * return: Boolean value of the CVar.
 */
int cvar_Boolean(cvar_t *cvar);

/*
 * Retrieve the float of a CVar.
 * NOTE: The CVar must be a float!
 *
 * cvar: Handle to the CVar whose float shall be retrieved.
 * return: Float value of the CVar.
 */
float cvar_Float(cvar_t *cvar);

/*
 * Set the minimum/maximum integer values of a CVar.
 * NOTE: The CVar must be an integer!
 *
 * cvar: Handle to the CVar whose integer min/max values shall be set.
 * l: Minimum value.
 * h: Maximum value.
 */
void cvar_SetMinMaxI(cvar_t *cvar, int l, int h);

/*
 * Set the minimum/maximum float values of a CVar.
 * NOTE: The CVar must be a float!
 *
 * cvar: Handle to the CVar whose float min/max values shall be set.
 * l: Minimum value.
 * h: Maximum value.
 */
void cvar_SetMinMaxF(cvar_t *cvar, float l, float h);

/*
 * Retrieve the minimum integer value of a CVar.
 * NOTE: The CVar must be an integer!
 *
 * cvar: Handle to the CVar whose integer minimum value shall be retrieved.
 * return: Minimum value.
 */
int cvar_MinI(const cvar_t *cvar);

/*
 * Retrieve the maximum integer value of a CVar.
 * NOTE: The CVar must be an integer!
 *
 * cvar: Handle to the CVar whose integer maximum shall be retrieved.
 * return: Maximum value.
 */
int cvar_MaxI(const cvar_t *cvar);

/*
 * Retrieve the minimum float value of a CVar.
 * NOTE: The CVar must be a float!
 *
 * cvar: Handle to the CVar whose float minimum shall be retrieved.
 * return: Minimum value.
 */
float cvar_MinF(const cvar_t *cvar);

/*
 * Retrieve the maximum float value of a CVar.
 * NOTE: The CVar must be a float!
 *
 * cvar: Handle to the CVar whose float maximum shall be retrieved.
 * return: Maximum value.
 */
float cvar_MaxF(const cvar_t *cvar);

/*
 * Retrieve the first CVar in the internal list.
 *
 * return: Handle to the first CVar in the internal list.
 */
cvar_t *cvar_First();

/*
 * Retrieve the last CVar in the internal list.
 *
 * return: Handle to the last CVar in the internal list.
 */
cvar_t *cvar_Last();

/*
 * Retrieve the CVar before another CVar in the internal list.
 *
 * cvar: Handle to the CVar whose previous sibling shall be retrieved.
 * return: Handle to the CVar before the one passed.
 */
cvar_t *cvar_Before(cvar_t *cvar);

/*
 * Retrieve the CVar after another CVar in the internal list.
 *
 * cvar: Handle to the CVar whose next sibling shall be retrieved.
 * return: Handle to the CVar after the one passed.
 */
cvar_t *cvar_After(cvar_t *cvar);

#endif
