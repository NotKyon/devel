//
//	TODO: Provide a decent explanation of why it's necessary to expose memory
//	      management functions, even when the same compiler/RT as the one the
//	      library was built with/for is used to build the app.
//

#define GB_INIT 1
#include <gamebase.h>

// Test various memory management routines.
void GBMain() {
	char *s;
	int *p;

	// certain functions can be used for direct string management
	// (this may be less cumbersome than the gb_string_t system).
	s = gbDuplicate("test");
	gbPrint(s);
	s = (char *)gbMemFree((void *)s);

	// gbMemAlloc() never returns NULL
	p = (int *)gbMemAlloc(sizeof(int)*1);

	p[0] = 42;

	// add some more elements
	p = (int *)gbMemExtend((void *)p, sizeof(int)*3);

	// set them
	p[1] = 23;
	p[2] = 19;

	// display what we have currently
	gbPrint(gbF("%i, %i, %i", p[0], p[1], p[2]));

	// free 'p' and set it to NULL (it's safe to call this function with a NULL
	// pointer in both debug and release mode).
	p = (int *)gbMemFree((void *)p);
}
