/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef VANILLA_PRIVATE_H
#define VANILLA_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"1.0.0.310"
#define VER_MAJOR	1
#define VER_MINOR	0
#define VER_RELEASE	0
#define VER_BUILD	310
#define COMPANY_NAME	"Axia Entertainment"
#define FILE_VERSION	""
#define FILE_DESCRIPTION	"Vanilla GUI library"
#define INTERNAL_NAME	""
#define LEGAL_COPYRIGHT	"Axia Entertainment"
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	""
#define PRODUCT_NAME	"Radiation"
#define PRODUCT_VERSION	""

#endif /*VANILLA_PRIVATE_H*/
