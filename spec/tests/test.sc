// This is a comment. It won't affect anything.
/*
	This
	/*
		Is
		/*
			Also
			/*
				A
				/*
					Comment
				*/
				That
			*/
			Can
		*/
		Be
	*/
	Nested!
*/

//==============================================================================
// Every spec starts with a namespace

// This namespace applies to everything that follows it. Every SC file must have
// a namespace to separate it from other libraries.
namespace vm;

// Typedefs also work
typedef int i32;
	/* The output code may be something like this:
	>> typedef int vm_i32_t;
	** Or something like this...
	>> typedef int VMi32;
	** Or something like this...
	>> namespace vm { typedef int i32_t; }
	** It all depends on the configuration of the library specification.
	*/

//==============================================================================
// Specs usually expose functions!

// Functions can be specified as they are in C...
int Sum(int x, int y);

// Functions can have default parameters...
int FMA(int x, int y, int z=0);

// Functions can have no parameters as well
void DoNothing();

// You can remap types
<int> = 
/*
// You can define generic types...
<i> = int;
<f> = float;

// ... which allows you to specify multiple functions from one declaration
@1 Push{if}(@1 x);
	/* Same as...
	>> int Pushi(int x);
	>> float Pushf(float x);
	*/
*/

//==============================================================================
// Constants are exposed via enumerations.

enum Op {
	Add,
	Sub,
	Mul,
	Div
};
	/* This will produce something like the following in C (by default)...
	>> typedef enum {
	>>   kSysOp_Add,
	>>   kSysOp_Sub,
	>>   kSysOp_Mul,
	>>   kSysOp_Div
	>> } sys_Op_t;
	>>
	>> #ifdef __cplusplus
	>> namespace sys { typedef sys_Op_t Op_t; }
	>> #endif
	*/

//==============================================================================
// Functions can then specify they require specific constants.

// Here's one!
int Operi(Op op, int x, int y);
float Operf(Op op, float x, float y);
/*
// Here's an awesome one!
@1 Oper{if}(Op op, @1 x, @1 y);
	/* This will produce something similar to this:
	>> int Operi(sys_Op_t op, int x, int y);
	>> float Operf(sys_Op_t op, float x, float y);
	*/
*/

//==============================================================================
// You can inject code at any point...

// Here's code for just C...
@C{

	/* this code will appear in C! */
	it isn't processed by SCC, so you can put pretty much whatever you like
	in here. :P

@C}
	// NOTE: '@C{' and '@C}' must be on their own lines with no spaces before
	//       them. This is because lines are copied verbatim.

// Here's code for just C++...
@C++{

	Woo! C++-only code!

@C++}

// Here's code for both C and C++...
@C,C++{

	This code will be added to both C and C++!

@C,C++}

//==============================================================================
// You can define "interfaces" as well. These are COM-like interfaces.

// Here's a basic interface
interface Processor {
	// interfaces define only public information

	// you can define properties!
	property int accumulator;
	property int x, y;
	/* The above is basically equivalent to this:
	>> int GetAccumulator() const;
	>> void SetAccumulator(int accumulator);
	>> int GetX() const;
	>> void SetX(int x);
	>> int GetY() const;
	>> void SetY(int y);
	*/

	// note the ability to specify a function that does not modify the interface
	int CheckStatus() const;

	// you can reformat certain function names for C...
	CheckStatus: CheckProcessorStatus;
		/* NOTE: The above properties are automatically treated this way...
		>> GetAccumulator -> GetProcessorAccumulator;
		>> SetAccumulator -> SetProcessorAccumulator;
		>> GetX -> GetProcessorX;
		>> SetX -> SetProcessorX;
		>> GetY -> GetProcessorY;
		>> SetY -> SetProcessorY;
		// The basic formatting is 'Get|Set'<InterfaceName><Property>
		*/
};

// Interfaces can derive from other interfaces
interface CPU extends Processor {
	void UploadUserCode(void *p, size_t n);
};

//==============================================================================
// You can have documentation generated automatically!

@{
	This is the brief description of 'DoSomething'. It lasts until the first
	break. (Notice that this is still the brief description...)

	This is the more detailed description, based on the brief description. It is
	used to describe, in more detail, the operation of the function.

		NOTE: This describes a note.
		      it can span multiple lines if formatted correctly.

		TODO: This is a TODO item.
		FIXME: This is a bug! D:

	x: This is a description for the 'x' parameter!
	y: This is a description for the 'y' parameter!
	return: This is a description for the return value!
@}
int DoSomething(int x, int y);
