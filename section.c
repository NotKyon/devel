#include <stdio.h>
#include <stdlib.h>

typedef void(*initFunc_t)();
typedef void(*finiFunc_t)();
struct progModule_s {
	const char *name;
	initFunc_t initFunc;
	finiFunc_t finiFunc;
	int priority;
};

#define DEFINE_MODULE(name,init,fini,priority)\
	__attribute__((section("MODINT")))\
	static struct progModule_s __proginit_##name = {\
		#name, &init, &fini, priority\
	}

extern char __start_MODINT;
extern char __stop_MODINT;
static const struct progModule_s *__proginits = (void *)&__start_MODINT;
size_t __proginits_count() {
	return (&__stop_MODINT - &__start_MODINT)/sizeof(struct progModule_s);
}

void mymod_init() {
	printf( "INIT!\n" );
}
void mymod_fini() {
	printf( "FINI!\n" );
}

DEFINE_MODULE( mymod, mymod_init, mymod_fini, 0 );

void init() {
	size_t i;

	printf( "begin init...\n" );
	for( i=0; i<__proginits_count(); ++i ) {
		const struct progModule_s *p = &__proginits[ i ];

		printf( "%s (%i)\n", p->name, p->priority );
		p->initFunc();
	}
	printf( "end init...\n" );
}
void fini() {
	size_t i;

	printf( "begin fini...\n" );
	for( i=__proginits_count() - 1; i<__proginits_count(); --i ) {
		const struct progModule_s *p = &__proginits[ i ];

		printf( "%s (%i)\n", p->name, p->priority );
		p->finiFunc();
	}
	printf( "end fini...\n" );
}

int main() {
	atexit( &fini );

	printf( "%u\n", (unsigned int)(&__stop_MODINT - &__start_MODINT) );
	printf( "%p\n%p\n", &__start_MODINT, &__stop_MODINT );

	init();

	return EXIT_SUCCESS;
}
