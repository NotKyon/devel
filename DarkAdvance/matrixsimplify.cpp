#include "simplify.h"
#include <string.h>

matrix_t *NewMatrix() {
	matrix_t *p;

	p = (matrix_t *)NewMemory(sizeof(*p));
	if (!p)
		return (matrix_t *)0;

	memset((void *)p, 0, sizeof(*p));

	return p;
}

void CopyMatrix(matrix_t *dst, const matrix_t *src) {
	memcpy(&dst->m11, &src->m11, sizeof(matrix_t));
}
matrix_t *DuplicateMatrix(const matrix_t *src) {
	matrix_t *dst;

	dst = NewMatrix();
	if (!dst)
		return (matrix_t *)0;

	CopyMatrix(dst, src);
	return dst;
}

void Matrix_SetRow(matrix_t *dst, unsigned int rownum, const vec4_t *src) {
	float *base;

	if (rownum > 3)
		return;

	base = &(((float *)&dst->m11)[rownum]);
	base[0*4] = src->x;
	base[1*4] = src->y;
	base[2*4] = src->z;
	base[3*4] = src->w;
}
void Matrix_SetColumn(matrix_t *dst, unsigned int colnum, const vec4_t *src) {
	float *base;

	if (colnum > 3)
		return;

	base = &(((float *)&dst->m11)[colnum*4]);
	base[0] = src->x;
	base[1] = src->y;
	base[2] = src->z;
	base[3] = src->w;
}
void Matrix_SetElement(matrix_t *dst, unsigned int r, unsigned int c, float x) {
	float *base;

	if (r>3 || c>3)
		return;

	base = &dst->m11;
	base[r + c*4] = x;
}

void Matrix_GetRow(vec4_t *dst, const matrix_t *src, unsigned int rownum) {
	const float *base;

	if (rownum > 3)
		return;

	base = &(((const float *)&src->m11)[rownum]);
	dst->x = base[0*4];
	dst->y = base[1*4];
	dst->z = base[2*4];
	dst->w = base[3*4];
}
void Matrix_GetColumn(vec4_t *dst, const matrix_t *src, unsigned int colnum) {
	const float *base;

	if (colnum > 3)
		return;

	base = &(((const float *)&src->m11)[colnum*4]);
	dst->x = base[0];
	dst->y = base[1];
	dst->z = base[2];
	dst->w = base[3];
}
float Matrix_GetElement(const matrix_t *src, unsigned int r, unsigned int c) {
	const float *base;

	if (r>3 || c>3)
		return 0.0f;

	base = &src->m11;
	return base[r + c*4];
}

float Matrix_11(const matrix_t *src) { return src->m11; }
float Matrix_12(const matrix_t *src) { return src->m12; }
float Matrix_13(const matrix_t *src) { return src->m13; }
float Matrix_14(const matrix_t *src) { return src->m14; }
float Matrix_21(const matrix_t *src) { return src->m21; }
float Matrix_22(const matrix_t *src) { return src->m22; }
float Matrix_23(const matrix_t *src) { return src->m23; }
float Matrix_24(const matrix_t *src) { return src->m24; }
float Matrix_31(const matrix_t *src) { return src->m31; }
float Matrix_32(const matrix_t *src) { return src->m32; }
float Matrix_33(const matrix_t *src) { return src->m33; }
float Matrix_34(const matrix_t *src) { return src->m34; }
float Matrix_41(const matrix_t *src) { return src->m41; }
float Matrix_42(const matrix_t *src) { return src->m42; }
float Matrix_43(const matrix_t *src) { return src->m43; }
float Matrix_44(const matrix_t *src) { return src->m44; }
