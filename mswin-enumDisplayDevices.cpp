#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

struct GLAdapterDesc
{
	char	cDescription[ 128 ];
	UINT	nVendorId;
	UINT	nDeviceId;
	UINT	nSubsysId;
	UINT	nRevision;
};
struct GLOutputDesc
{
	char	cName[ 32 ];
	UINT	nFlags;
};

#define F_ADAPTER_OUTPUT_PRIMARY			0x0001
#define F_ADAPTER_OUTPUT_MODES_PRUNED		0x0002

class IReferenceCounted;
class IGLFactory;
class IGLAdapter;
class IGLOutput;

#ifndef _In_
# define _In_
#endif
#ifndef _In_opt_
# define _In_opt_
#endif
#ifndef _Out_
# define _Out_
#endif
#ifndef _Out_opt_
# define _Out_opt_
#endif

class IReferenceCounted
{
public:
	IReferenceCounted(): mRefCnt( 1 )
	{
	}
	virtual ~IReferenceCounted()
	{
	}

	inline virtual void Grab()
	{
		++mRefCnt;
	}
	inline virtual void Drop()
	{
		if( --mRefCnt == 0 )
		{
			delete this;
		}
	}

	inline void AddRef()
	{
		Grab();
	}
	inline void Release()
	{
		Drop();
	}
};
class IGLFactory: public virtual IReferenceCounted
{
public:
	static const UINT kMaxAdapters = 16;
	
	static bool Create( _Out_ IGLFactory **ppFactory );

	UINT GetAdapterCount() const;
	bool GetAdapter( _In_ UINT nIndex, _Out_ IGLAdapter **ppAdapter );

protected:
	IGLFactory();
	virtual ~IGLFactory();

	bool EnumAdapters();
	void FreeAdapters();

private:
	UINT mNumAdapters;
	IGLAdapter *mpAdapters[ kMaxAdapters ];
};
class IGLAdapter: public virtual IReferenceCounted
{
friend class IGLFactory;
public:
	static const UINT kMaxOutputs = 16;

	UINT GetOutputCount() const;
	bool GetOutput( _In_ UINT nIndex, _Out_ IGLOutput **ppOutput );

	void GetDesc( _Out_ GLAdapterDesc *pDesc );

protected:
	IGLAdapter();
	virtual ~IGLAdapter();

	void SetNullFactory();

	bool AddOutput( _In_ IGLOutput *pOutput );
	void FreeOutput( _In_ IGLOutput *pOutput );

private:
	GLAdapterDesc mDesc;

	UINT mNumOutputs;
	IGLOutput *mpOutputs[ kMaxOutputs ];

	void FreeOutputs();
};
class IGLOutput: public virtual IReferenceCounted
{
friend class IGLAdapter;
public:
	bool GetDesc( _Out_ GLOutputDesc *pDesc );

protected:
	IGLOutput();
	virtual ~IGLOutput();

private:
	GLOutputDesc mDesc;

	IGLAdapter *mpConnectedAdapters[ IGLFactory::kMaxAdapters ];
};

int main()
{
	// Use the straight Windows API to get a list of all the display devices
	// available. This will not be organized into GPUs and outputs.

	DISPLAY_DEVICEA device;

	device.cb = sizeof( device );

	DWORD iDevNum = 0;
	while( EnumDisplayDevicesA( NULL, iDevNum, &device, 0 ) )
	{
		printf( "%u. %s\n  DeviceString: %s\n  StateFlags: 0x%.8X\n"
			"  DeviceID: %s\n  DeviceKey: %s\n",
			( unsigned int )iDevNum,
			device.DeviceName,
			device.DeviceString,
			( unsigned int )device.StateFlags,
			device.DeviceID,
			device.DeviceKey );

		++iDevNum;
	}

	printf( "\n" );

	// Use the API presented here to gather a list

	// Create the factory
	IGLFactory *pFactory = nullptr;
	if( !IGLFactory::Create( &pFactory ) )
	{
		fprintf( stderr, "ERROR: Could not create factory.\n" );
		return EXIT_FAILURE;
	}

	// Enumerate each adapter
	UINT nAdapterCount = pFactory->GetAdapterCount();
	for( UINT nAdapter = 0; nAdapter < nAdapterCount; ++nAdapter )
	{
		// Capture the adapter
		IGLAdapter *pAdapter = nullptr;
		if( !pFactory->GetAdapter( nAdapter, &pAdapter ) )
		{
			fprintf( stderr, "ERROR: Could not get adapter %u.\n",
				nAdapter );
			continue;
		}

		// List details about the adapter
		GLAdapterDesc adapterDesc;

		pAdapter->GetDesc( &adapterDesc );
		printf( "%u. %s\n  VendorId: 0x%.4X\n  DeviceId: 0x%.4X\n  "
			"SubsysId: 0x%.4X\n  Revision: 0x%.4X\n",
			nAdapter,
			adapterDesc.cName,
			adapterDesc.nVendorId,
			adapterDesc.nDeviceId,
			adapterDesc.nSubsysId,
			adapterDesc.nRevision );

		// Enumerate each output attached to this adapter
		UINT nOutputCount = pAdapter->GetOutputCount();
		for( UINT nOutput = 0; nOutput < nOutputCount; ++nOutput )
		{
			// Capture the output
			IGLOutput *pOutput = nullptr;
			if( !pAdapter->GetOutput( nOutput, &pOutput ) )
			{
				fprintf( stderr, "ERROR: Could not get output %u.\n",
					nOutput );
				continue;
			}

			// Release the output
			pOutput->Drop();
		}

		// Release the adapter
		pAdapter->Drop();
	}
	
	// Release the factory
	pFactory->Drop();

	// Done
	return EXIT_SUCCESS;
}

/*
===============================================================================

	IGLFACTORY

===============================================================================
*/
	static const UINT kMaxAdapters = 16;
	
bool IGLFactory::Create( _Out_ IGLFactory **ppFactory )
{
	if( !ppFactory )
	{
		return false;
	}

	IGLFactory *pFactory = new IGLFactory();
	if( !pFactory )
	{
		return false;
	}

	if( !pFactory->EnumAdapters() )
	{
		delete pFactory;
		return false;
	}

	*ppFactory = pFactory;
	return true;
}

UINT IGLFactory::GetAdapterCount() const
{
	return mNumAdapters;
}
bool IGLFactory::GetAdapter( _In_ UINT nIndex, _Out_ IGLAdapter **ppAdapter )
{
	if( nIndex >= mNumAdapters )
	{
		return false;
	}
	if( !ppAdapter )
	{
		return false;
	}

	if( !mpAdapters[ nIndex ] )
	{
#ifdef _DEBUG
		DebugBreak(); //this shouldn't happen
#endif
		return false;
	}

	mpAdapters[ nIndex ]->Grab();
	*ppAdapter = mpAdapters[ nIndex ];

	return true;
}

IGLFactory::IGLFactory(): IReferenceCounted()
{
	mNumAdapters = 0;
	for( UINT nAdapterIndex = 0; nAdapterIndex < kNumAdapters; ++nAdapterIndex )
	{
		mpAdapters[ nAdapterIndex ] = nullptr;
	}
}
IGLFactory::~IGLFactory()
{
	FreeAdapters();
}

bool IGLFactory::EnumAdapters()
{
}
void IGLFactory::FreeAdapters()
{
	for( UINT nAdapterIndex = 0; nAdapterIndex < kNumAdapters; ++nAdapterIndex )
	{
		if( !mpAdapters[ nAdapterIndex ] )
		{
#ifdef _DEBUG
			DebugBreak();
#endif
			continue;
		}

		mpAdapters[ nAdapterIndex ]->SetNullFactory();
		mpAdapters[ nAdapterIndex ]->Drop();
		mpAdapters[ nAdapterIndex ] = nullptr;
	}
}


	static bool Create( _Out_ IGLFactory **ppFactory );

	UINT GetAdapterCount();
	bool GetAdapter( _In_ UINT nIndex, _Out_ IGLAdapter **ppAdapter );

protected:
	IGLFactory();
	virtual ~IGLFactory();

	bool EnumAdapters();

private:
	UINT mNumAdapters;
	IGLAdapter *mpAdapters[ kMaxAdapters ];

	
	
	
	