#ifndef AXTEK_THREADING_CSIGNAL_H
#define AXTEK_THREADING_CSIGNAL_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

#include "Atomics.h"
#include "CEvent.h"

__AX_TASK_NS_INIT__

class CSignal {
protected:
	u32 m_cur, m_exp;
	CEvent m_event;

public:
	inline CSignal(u32 exp=0): m_cur(0), m_exp(exp), m_event() {
	}
	inline ~CSignal() {
	}

	inline void Raise() {
		u32 x;

		if ((x = atomic_inc(&m_cur)) >= atomic_get(&m_exp) - 1)
			m_event.Signal();
	}
	inline void Lower() {
		u32 x;

		if ((x = atomic_dec(&m_cur)) < atomic_get(&m_exp))
			m_event.Reset();
	}

	inline void Clear() {
		atomic_set(&m_cur, 0);
		m_event.Reset();
	}
	inline void Reset(u32 exp) {
		atomic_set(&m_exp, exp);
		atomic_set(&m_cur, 0);
		m_event.Reset();
	}
	inline void Sync() {
		m_event.Sync();
	}
	inline bool Wait(u32 milliseconds) {
		return m_event.Wait(milliseconds);
	}
	inline bool TryWait() {
		return m_event.TryWait();
	}
};

__AX_TASK_NS_FINI__

#endif
