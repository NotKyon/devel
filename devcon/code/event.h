#ifndef EVENT_H
#define EVENT_H

enum {
	kEventTag_DummyTag, /*cause the next tag (the real tag) to start at 1*/

	kEventTag_Keyboard,
	kEventTag_Mouse,

	kEventTag_View
};

#define EVENT_TAG_SHIFT 7

#define EVENT_TAG(x) __kEvent_##x##__ = kEventTag_##x << EVENT_TAG_SHIFT
#define EVENT_NUM(x) __kEvent_##x##_Last__, kNum##x##Events =\
	__kEvent_##x##_Last__ - __kEvent_##x##__

enum {
	/* keyboard events */
	EVENT_TAG(Keyboard),
		kEvent_KeyDown,
		kEvent_KeyUp,
	EVENT_NUM(Keyboard),

	/* mouse events */
	EVENT_TAG(Mouse),
		kEvent_MouseDown,
		kEvent_MouseUp,

		kEvent_MouseMove,
	EVENT_NUM(Mouse),

	/* view events */
	EVENT_TAG(View),
		kEvent_ViewSize,
	EVENT_NUM(View),

	/* there should be no event >= kLastEvent */
	kLastEvent
};

typedef struct event_s {
	int tag; /*(code>>EVENT_TAG_SHIFT)&0xFF; the type of event*/
	int code; /*actual event code*/

	int time; /*time in milliseconds of when this event was delivered*/

	int x, y; /*parameters to the event*/

	struct event_s *prev, *next; /*list*/
} event_t;
typedef struct eventSystem_s {
	event_t *head, *tail;
	event_t *cur;
} eventSystem_t;

extern eventSystem_t g_ev;

void ev_Init(); /*initialize the whole system*/
void ev_Deinit(); /*deinitialize the whole system*/

event_t *ev_New(int code, int x, int y); /*create a new event*/
void ev_Delete(event_t *ev); /*delete a specific event (internal)*/
void ev_DeleteAll(); /*delete all events (including the current event)*/

void ev_Next(); /*grab the next event*/

int ev_Tag(); /*current event -> tag*/
int ev_Code(); /*current event -> code*/
int ev_Time(); /*current event -> time*/
int ev_X(); /*current event -> x*/
int ev_Y(); /*current event -> y*/

#endif
