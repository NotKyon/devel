#ifndef EMBER_MATH_H
#define EMBER_MATH_H

#ifndef M_PI
#define M_PI ( 3.141592653589793238462643383279502884197169 )
#endif

#ifndef degrees
#define degrees( x ) ( ( M_PI / 180.0 ) * ( x ) ) /* degrees to radians */
#endif

#ifndef radians
#define radians( x ) ( ( 180.0 / M_PI ) * ( x ) ) /* radians to degrees */
#endif

typedef struct Vec2 {
	float x, y;
} Vec2;
typedef struct Vec3 {
	float x, y, z;
} Vec3;
typedef struct Vec4 {
	float x, y, z, w;
} Vec4;

typedef struct Quaternion {
	float w, x, y, z;
} Quaternion;

typedef struct Plane {
	float a, b, c, d;
} Plane;

typedef union Matrix {
	struct {
		float _11, _21, _31, _41;
		float _12, _22, _32, _42;
		float _13, _23, _33, _43;
		float _14, _24, _34, _44;
	} cm;
	struct {
		float _11, _12, _13, _14;
		float _21, _22, _23, _24;
		float _31, _32, _33, _34;
		float _41, _42, _43, _44;
	} rm;
	float f[ 16 ];
	float m[ 4 ][ 4 ];
} Matrix;

typedef struct Ray {
	Vec3 org, dir;
} Ray;

extern float fabs_fast( float x );
extern int ftoi_fast( float x );
extern unsigned int ftoui_fast( float x );
extern float sqrt_fast( float x );
extern float invsqrt_fast( float x );
extern float sin_fast( float x );
extern float cos_fast( float x );
extern float tan_fast( float x );
extern float asin_fast( float x );
extern float acos_fast( float x );
extern float atan_fast( float x );
extern float atan2_fast( float y, float x );

extern float vec2_x( const Vec2 *src );
extern float vec2_y( const Vec2 *src );
extern void vec2_zero( Vec2 *dst );
extern void vec2_identity( Vec2 *dst );
extern void vec2_set( Vec2 *dst, float x, float y );
extern void vec2_copy( Vec2 *dst, const Vec2 *src );
extern void vec2_add( Vec2 *dst, const Vec2 *a, const Vec2 *b );
extern void vec2_subtract( Vec2 *dst, const Vec2 *a, const Vec2 *b );
extern void vec2_multiply( Vec2 *dst, const Vec2 *a, const Vec2 *b );
extern void vec2_divide( Vec2 *dst, const Vec2 *a, const Vec2 *b );
extern void vec2_scale( Vec2 *dst, const Vec2 *a, float b );
extern float vec2_dot( const Vec2 *a, const Vec2 *b );
extern void vec2_lerp_ex( Vec2 *dst, const Vec2 *a, const Vec2 *b, const Vec2 *w );
extern void vec2_lerp( Vec2 *dst, const Vec2 *a, const Vec2 *b, float w );
extern float vec2_length( const Vec2 *a );
extern float vec2_length_fast( const Vec2 *a );
extern void vec2_normalize( Vec2 *a );
extern void vec2_normalize_fast( Vec2 *a );
extern float vec2_distance( const Vec2 *a, const Vec2 *b );
extern float vec2_distance_fast( const Vec2 *a, const Vec2 *b );
extern float vec2_point( const Vec3 *a, const Vec3 *b );

extern const Vec3 *vec3_forward( void );
extern const Vec3 *vec3_up( void );
extern const Vec3 *vec3_right( void );
extern float vec3_x( const Vec3 *src );
extern float vec3_y( const Vec3 *src );
extern float vec3_z( const Vec3 *src );
extern void vec3_zero( Vec3 *dst );
extern void vec3_identity( Vec3 *dst );
extern void vec3_set( Vec3 *dst, float x, float y, float z );
extern void vec3_copy( Vec3 *dst, const Vec3 *src );
extern void vec3_add( Vec3 *dst, const Vec3 *a, const Vec3 *b );
extern void vec3_subtract( Vec3 *dst, const Vec3 *a, const Vec3 *b );
extern void vec3_multiply( Vec3 *dst, const Vec3 *a, const Vec3 *b );
extern void vec3_divide( Vec3 *dst, const Vec3 *a, const Vec3 *b );
extern void vec3_scale( Vec3 *dst, const Vec3 *a, float b );
extern float vec3_dot( const Vec3 *a, const Vec3 *b );
extern void vec3_cross( Vec3 *dst, const Vec3 *a, const Vec3 *b );
extern void vec3_lerp_ex( Vec3 *dst, const Vec3 *a, const Vec3 *b, const Vec3 *w );
extern void vec3_lerp( Vec3 *dst, const Vec3 *a, const Vec3 *b, float w );
extern float vec3_length( const Vec3 *a );
extern float vec3_length_fast( const Vec3 *a );
extern float vec3_invlength( const Vec3 *a );
extern float vec3_invlength_fast( const Vec3 *a );
extern int vec3_normalize( Vec3 *a );
extern int vec3_normalize_fast( Vec3 *a );
extern float vec3_distance( const Vec3 *a, const Vec3 *b );
extern float vec3_distance_fast( const Vec3 *a, const Vec3 *b );
extern void vec3_transform3_ex( Vec3 *dst, const Vec3 *a, const Matrix *b );
extern void vec3_transform_ex( Vec3 *dst, const Vec3 *a, const Matrix *b );
extern void vec3_transform3( Vec3 *dst, float x, float y, float z, const Matrix *b );
extern void vec3_transform( Vec3 *dst, float x, float y, float z, const Matrix *b );
extern void vec3_translate( Vec3 *dst, const Matrix *src );
extern void vec3_point( Vec3 *dst, const Vec3 *a, const Vec3 *b );
extern void vec3_point_fast( Vec3 *dst, const Vec3 *a, const Vec3 *b );
extern void vec3_matrix_euler( Vec3 *dst, const Matrix *src );
extern void vec3_matrix_euler_fast( Vec3 *dst, const Matrix *src );
extern void vec3_quaternion_euler( Vec3 *dst, const Quaternion *src );
extern void vec3_quaternion_euler_fast( Vec3 *dst, const Quaternion *src );
extern void vec3_triangle_normal( Vec3 *dst, const Vec3 *vtxs );
extern void vec3_triangle_slide( Vec3 *dst, const Vec3 *src, const Vec3 *normal, float speed );
extern void vec3_rotate_quaternion( Vec3 *dst, const Vec3 *src, const Quaternion *rot );
extern int vec3_in_triangle( const Vec3 *p, const Vec3 *verts, Vec2 *uv );
extern void vec3_unproject( Vec3 *dst, const Vec3 *p, float width, float height, const Matrix *invwlrdviewproj );

extern float vec4_x( const Vec4 *src );
extern float vec4_y( const Vec4 *src );
extern float vec4_z( const Vec4 *src );
extern float vec4_w( const Vec4 *src );
extern void vec4_zero( Vec4 *dst );
extern void vec4_identity( Vec4 *dst );
extern void vec4_set( Vec4 *dst, float x, float y, float z, float w );
extern void vec4_copy( Vec4 *dst, const Vec4 *src );
extern void vec4_add( Vec4 *dst, const Vec4 *a, const Vec4 *b );
extern void vec4_subtract( Vec4 *dst, const Vec4 *a, const Vec4 *b );
extern void vec4_multiply( Vec4 *dst, const Vec4 *a, const Vec4 *b );
extern void vec4_divide( Vec4 *dst, const Vec4 *a, const Vec4 *b );
extern void vec4_scale( Vec4 *dst, const Vec4 *a, float b );
extern float vec4_dot( const Vec4 *a, const Vec4 *b );
extern void vec4_cross( Vec4 *dst, const Vec4 *a, const Vec4 *b, const Vec4 *c );
extern float vec4_length( const Vec4 *a );
extern float vec4_length_fast( const Vec4 *a );
extern void vec4_normalize( Vec4 *a );
extern void vec4_normalize_fast( Vec4 *a );
extern float vec4_distance( const Vec4 *a, const Vec4 *b );
extern float vec4_distance_fast( const Vec4 *a, const Vec4 *b );
extern void vec4_transform_ex( Vec4 *dst, const Vec4 *a, const Matrix *b );
extern void vec4_transform( Vec4 *dst, float x, float y, float z, float w, const Matrix *b );

extern void mat_identity( Matrix *dst );
extern const Matrix *mat_identity_ptr();
extern void mat_copy( Matrix *dst, const Matrix *src );
extern void mat_copy3( Matrix *dst, const Matrix *src );
extern void mat_multiply3( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply3_cc( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply3_rc( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply3_cr( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply34( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply34_cc( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply34_rc( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply34_cr( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply_cc( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply_rc( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_multiply_cr( Matrix *dst, const Matrix *a, const Matrix *b );
extern void mat_transpose3( Matrix *dst, const Matrix *src );
extern void mat_transpose( Matrix *dst, const Matrix *src );
extern void mat_quaternion( Matrix *dst, const Quaternion *src );
extern void mat_quaternion_fast( Matrix *dst, const Quaternion *src );
extern void mat_quaternion3( Matrix *dst, const Quaternion *src );
extern void mat_quaternion3_fast( Matrix *dst, const Quaternion *src );
extern void mat_decompose3( const Matrix *src, Quaternion *rot, Vec3 *scale );
extern void mat_decompose_quaternion( const Matrix *src, Quaternion *rot );
extern void mat_decompose_quaternion_fast( const Matrix *src, Quaternion *rot );
extern void mat_decompose( const Matrix *src, Vec3 *trans, Quaternion *rot, Vec3 *scale );
extern void mat_decompose_scale( const Matrix *src, Vec3 *scale );
extern void mat_decompose_scale_fast( const Matrix *src, Vec3 *scale );
extern float mat_determinant3( const Matrix *src );
extern float mat_determinant( const Matrix *src );
extern void mat_inverse_world( Matrix *dst, const Matrix *src );
extern float mat_inverse3( Matrix *dst, const Matrix *src );
extern float mat_inverse( Matrix *dst, const Matrix *src );
extern void mat_translation_ex( Matrix *dst, const Vec3 *trans );
extern void mat_translation( Matrix *dst, float x, float y, float z );
extern void mat_scaling_ex( Matrix *dst, const Vec3 *scale );
extern void mat_scaling( Matrix *dst, float x, float y, float z );
extern void mat_rotation_x( Matrix *dst, float x );
extern void mat_rotation_x_fast( Matrix *dst, float x );
extern void mat_rotation_y( Matrix *dst, float y );
extern void mat_rotation_y_fast( Matrix *dst, float y );
extern void mat_rotation_z( Matrix *dst, float z );
extern void mat_rotation_z_fast( Matrix *dst, float z );
extern void mat_rotation3_x( Matrix *dst, float x );
extern void mat_rotation3_x_fast( Matrix *dst, float x );
extern void mat_rotation3_y( Matrix *dst, float y );
extern void mat_rotation3_y_fast( Matrix *dst, float y );
extern void mat_rotation3_z( Matrix *dst, float z );
extern void mat_rotation3_z_fast( Matrix *dst, float z );
extern void mat_translate_ex( Matrix *dst, const Vec3 *trans );
extern void mat_translate( Matrix *dst, float x, float y, float z );
extern void mat_perspective( Matrix *dst, float fov, float aspect, float zn, float zf );
extern void mat_scale3_ex( Matrix *dst, const Vec3 *scale );
extern void mat_scale3( Matrix *dst, float x, float y, float z );
extern void mat_turn_x( Matrix *dst, float x );
extern void mat_turn_x_fast( Matrix *dst, float x );
extern void mat_turn_y( Matrix *dst, float y );
extern void mat_turn_y_fast( Matrix *dst, float y );
extern void mat_turn_z( Matrix *dst, float z );
extern void mat_turn_z_fast( Matrix *dst, float z );
extern void mat_turn_ex( Matrix *dst, const Vec3 *v );
extern void mat_turn_fast_ex( Matrix *dst, const Vec3 *v );
extern void mat_turn( Matrix *dst, float x, float y, float z );
extern void mat_turn_fast( Matrix *dst, float x, float y, float z );
extern void mat_move_x( Matrix *dst, float x );
extern void mat_move_y( Matrix *dst, float y );
extern void mat_move_z( Matrix *dst, float z );
extern void mat_move_ex( Matrix *dst, const Vec3 *v );
extern void mat_move( Matrix *dst, float x, float y, float z );
extern void mat_rotate_ex( Matrix *dst, const Vec3 *p );
extern void mat_rotate_fast_ex( Matrix *dst, const Vec3 *p );
extern void mat_rotate( Matrix *dst, float x, float y, float z );
extern void mat_rotate_fast( Matrix *dst, float x, float y, float z );

extern float quat_w( const Quaternion *src );
extern float quat_x( const Quaternion *src );
extern float quat_y( const Quaternion *src );
extern float quat_z( const Quaternion *src );
extern void quat_set( Quaternion *dst, float w, float x, float y, float z );
extern void quat_identity( Quaternion *dst );
extern void quat_copy( Quaternion *dst, const Quaternion *src );
extern void quat_multiply( Quaternion *dst, const Quaternion *a, const Quaternion *b );
extern float quat_dot( const Quaternion *a, const Quaternion *b );
extern void quat_axis( Quaternion *dst, float angle, float x, float y, float z );
extern void quat_axis_fast( Quaternion *dst, float angle, float x, float y, float z );
extern void quat_rotation( Quaternion *dst, const Matrix *src );
extern void quat_rotation_fast( Quaternion *dst, const Matrix *src );
extern void quat_slerp( Quaternion *dst, const Quaternion *a, const Quaternion *b, float w );
extern void quat_rotate_ex( Quaternion *dst, const Vec3 *src );
extern void quat_rotate( Quaternion *dst, float x, float y, float z );
extern void quat_rotate_fast_ex( Quaternion *dst, const Vec3 *src );
extern void quat_rotate_fast( Quaternion *dst, float x, float y, float z );
extern void quat_conjugate( Quaternion *dst, const Quaternion *src );
extern float quat_length_sqr( const Quaternion *src );
extern float quat_length( const Quaternion *src );
extern float quat_length_fast( const Quaternion *src );
extern void quat_normalize( Quaternion *dst, const Quaternion *src );
extern void quat_normalize_fast( Quaternion *dst, const Quaternion *src );
extern void quat_inverse( Quaternion *dst, const Quaternion *src );
extern void quat_inverse_fast( Quaternion *dst, const Quaternion *src );

extern float ray_org_x( const Ray *src );
extern float ray_org_y( const Ray *src );
extern float ray_org_z( const Ray *src );
extern float ray_dir_x( const Ray *src );
extern float ray_dir_y( const Ray *src );
extern float ray_dir_z( const Ray *src );
extern void ray_set_ex( Ray *dst, const Vec3 *org, const Vec3 *dir );
extern void ray_set( Ray *dst, float px, float py, float pz, float dx, float dy, float dz );
extern void ray_copy( Ray *dst, const Ray *src );
extern int ray_intersects_tri_ex( const Ray *ray, const Vec3 *tris, Vec2 *uv, float *dist );
/*ray_intersects_tri() declaration goes here if you want it (see ray.c)*/
extern int ray_intersects_sphere_ex( const Ray *ray, const Vec3 *center, float radius, Vec3 *hit );
extern void ray_local( Ray *dst, const Ray *src, const Matrix *space );
extern void ray_pick( Ray *dst, float x, float y, float width, float height, const Matrix *invwrldviewproj );

#endif
