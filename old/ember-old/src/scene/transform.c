#include <ember/transform.h>
#include <ember/entity.h>

/*
 * Set to zero if you do not want to use the fast math routines.
 */
#define FAST_MATH 1

void tform_init( tform_t tform, entity_t ent )
{
	tform->ent = ent;

	tform->dirty = TFORM_DRTY_GWORLD_BIT;

	mat_identity( &tform->l_world );
	mat_identity( &tform->g_world );

	vec3_identity( &tform->scale );
}

void tform_translate_ex( tform_t tform, const Vec3 *v )
{
	mat_translate_ex( tform_local_matrix( tform ), v );
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}
void tform_rotate_ex( tform_t tform, const Vec3 *v )
{
	mat_rotate_ex( tform_local_matrix( tform ), v );
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}
void tform_scale_ex( tform_t tform, const Vec3 *v )
{
	vec3_copy( &tform->scale, v );
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}

void tform_translate( tform_t tform, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	tform_translate_ex( tform, &v );
}
void tform_rotate( tform_t tform, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	tform_rotate_ex( tform, &v );
}
void tform_scale( tform_t tform, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	tform_scale_ex( tform, &v );
}

void tform_move_ex( tform_t tform, const Vec3 *v )
{
	mat_move_ex( tform_local_matrix( tform ), v );
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}
void tform_turn_ex( tform_t tform, const Vec3 *v )
{
#if FAST_MATH == 1
	mat_turn_fast_ex( tform_local_matrix( tform ), v );
#else
	mat_turn_ex( tform_local_matrix( tform ), v );
#endif
}
void tform_move( tform_t tform, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	tform_move_ex( tform, &v );
}
void tform_turn( tform_t tform, float x, float y, float z )
{
	Vec3 v;

	v.x = x;
	v.y = y;
	v.z = z;
	tform_turn_ex( tform, &v );
}

void tform_move_x( tform_t tform, float x )
{
	mat_move_x( tform_local_matrix( tform ), x );
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}
void tform_move_y( tform_t tform, float y )
{
	mat_move_y( tform_local_matrix( tform ), y );
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}
void tform_move_z( tform_t tform, float z )
{
	mat_move_z( tform_local_matrix( tform ), z );
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}
void tform_turn_x( tform_t tform, float x )
{
#if FAST_MATH == 1
	mat_turn_x_fast( tform_local_matrix( tform ), x );
#else
	mat_turn_x( tform_local_matrix( tform ), x );
#endif
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}
void tform_turn_y( tform_t tform, float y )
{
#if FAST_MATH == 1
	mat_turn_y_fast( tform_local_matrix( tform ), y );
#else
	mat_turn_y( tform_local_matrix( tform ), y );
#endif
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}
void tform_turn_z( tform_t tform, float z )
{
#if FAST_MATH == 1
	mat_turn_z_fast( tform_local_matrix( tform ), z );
#else
	mat_turn_z( tform_local_matrix( tform ), z );
#endif
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}

void tform_get_translation_ex( tform_t tform, Vec3 *v )
{
	const Matrix *m;

	m    = tform_local_matrix( tform );
	v->x = m->rm._41;
	v->y = m->rm._42;
	v->z = m->rm._43;
}
void tform_get_rotation_ex( tform_t tform, Vec3 *v )
{
	Quaternion q;

#if FAST_MATH == 1
	mat_decompose_quaternion_fast( tform_local_matrix( tform ), &q );
	vec3_quaternion_euler_fast( v, &q );
#else
	mat_decompose_quaternion( tform_local_matrix( tform ), &q );
	vec3_quaternion_euler( v, &q );
#endif
}

void tform_get_scale_ex( tform_t tform, Vec3 *v )
{
	vec3_copy( v, &tform->scale );
}
void tform_get_translation( tform_t tform, float *x, float *y, float *z )
{
	Vec3 v;

	tform_get_translation_ex( tform, &v );
	*x = v.x;
	*y = v.y;
	*z = v.z;
}
void tform_get_rotation( tform_t tform, float *x, float *y, float *z )
{
	Vec3 v;

	tform_get_rotation_ex( tform, &v );
	*x = v.x;
	*y = v.y;
	*z = v.z;
}
void tform_get_scale( tform_t tform, float *x, float *y, float *z )
{
	*x = tform->scale.x;
	*y = tform->scale.y;
	*z = tform->scale.z;
}

float tform_translation_x( tform_t tform )
{
	return tform_local_matrix( tform )->rm._41;
}
float tform_translation_y( tform_t tform )
{
	return tform_local_matrix( tform )->rm._42;
}
float tform_translation_z( tform_t tform )
{
	return tform_local_matrix( tform )->rm._43;
}

float tform_rotation_x( tform_t tform )
{
	Vec3 v;

	tform_get_rotation_ex( tform, &v );
	return v.x;
}
float tform_rotation_y( tform_t tform )
{
	Vec3 v;

	tform_get_rotation_ex( tform, &v );
	return v.y;
}
float tform_rotation_z( tform_t tform )
{
	Vec3 v;

	tform_get_rotation_ex( tform, &v );
	return v.z;
}

float tform_scale_x( tform_t tform )
{
	return tform->scale.x;
}
float tform_scale_y( tform_t tform )
{
	return tform->scale.y;
}
float tform_scale_z( tform_t tform )
{
	return tform->scale.z;
}
Matrix *tform_local_matrix( tform_t tform )
{
	return &tform->l_world;
}
Matrix *tform_global_matrix( tform_t tform )
{
	Matrix t;

	if( ~tform->dirty & TFORM_DRTY_GWORLD_BIT )
		return &tform->g_world;

	if( tform->ent->prnt ) {
		t.rm._11 = tform->l_world.rm._11 * tform->scale.x;
		t.rm._12 = tform->l_world.rm._12 * tform->scale.y;
		t.rm._13 = tform->l_world.rm._13 * tform->scale.z;

		t.rm._21 = tform->l_world.rm._21 * tform->scale.x;
		t.rm._22 = tform->l_world.rm._22 * tform->scale.y;
		t.rm._23 = tform->l_world.rm._23 * tform->scale.z;

		t.rm._31 = tform->l_world.rm._31 * tform->scale.x;
		t.rm._32 = tform->l_world.rm._32 * tform->scale.y;
		t.rm._33 = tform->l_world.rm._32 * tform->scale.z;

		t.rm._41 = tform->l_world.rm._41;
		t.rm._42 = tform->l_world.rm._42;
		t.rm._43 = tform->l_world.rm._43;

		mat_multiply34( &tform->g_world, &t,
		    tform_global_matrix( &tform->ent->prnt->tform ) );
	} else {
		mat_copy( &tform->g_world, &tform->l_world );
		mat_scale3_ex( &tform->g_world, &tform->scale );
	}

	tform->dirty &= ~TFORM_DRTY_GWORLD_BIT;
	return &tform->g_world;
}

void tform_set_local_matrix( tform_t tform, const Matrix *m )
{
	mat_copy( &tform->l_world, m );
	tform->dirty |= TFORM_DRTY_GWORLD_BIT;
}
void tform_set_global_matrix( tform_t tform, const Matrix *m )
{
	const Matrix *g;
	Matrix i;

	mat_copy( &tform->g_world, m );

	/*
 * FIXME: Set transform global matrix not working.
 * The inverse operation works properly.
 * The transformation isn't.
 */
	if( tform->ent->prnt ) {
		g = tform_global_matrix( &tform->ent->prnt->tform );

		mat_inverse_world( &i, g );
		mat_multiply34( &tform->l_world, m, &i );
	} else
		mat_copy( &tform->l_world, m );

	tform->dirty &= ~TFORM_DRTY_GWORLD_BIT;
}
