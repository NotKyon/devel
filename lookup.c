#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

typedef struct entry_s {
	struct entry_s *entries;
	void *p;
} entry_t;
typedef struct table_s {
	int convmap[256];
	int numentries;
	entry_t *entries;
} table_t;

/* pass a string of the allowed characters */
int generateconvmap(int dst[256], const char *allowed) {
	int i, j, k;

	for(i=0; i<256; i++)
		dst[i] = -1;

	for(i=0; allowed[i]; i++)
		dst[(unsigned char)allowed[i]] = i;

	for(j=0; j<i; j++) {
		for(k=j+1; k<i; k++) {
			if (allowed[j]==allowed[k])
				return -1; /*duplicate entries*/
		}
	}

	return i;
}

int inittable(table_t *table, const char *allowed) {
	table->numentries = generateconvmap(table->convmap, allowed);
	if (table->numentries < 1)
		return 0;

	printf("numentries = %i\n", table->numentries);
	fflush(stdout);

	table->entries = (entry_t *)malloc(sizeof(entry_t)*table->numentries);
	if (!table->entries)
		return 0;

	memset((void *)table->entries, 0, sizeof(entry_t)*table->numentries);

	return 1;
}
void deleteentries(table_t *table, entry_t *entries) {
	int i;

	if (!entries)
		return;

	for(i=0; i<table->numentries; i++) {
		if (entries[i].entries)
			deleteentries(table, entries[i].entries);
	}

	free((void *)entries);
}
void deinittable(table_t *table) {
	deleteentries(table, table->entries);
	table->entries = (entry_t *)0;

	table->numentries = 0;
}

entry_t *findfromentry(table_t *table, entry_t *entries, const char *str) {
	size_t n;
	int i;

	i = table->convmap[*(unsigned char *)str++];
	if (i == -1)
		return (entry_t *)0; /*invalid character*/

	/* if there's more to the string... */
	if (*str) {
		/* generate the entries if they're not there */
		if (!entries[i].entries) {
			n = sizeof(entry_t)*table->numentries;

			entries[i].entries = (entry_t *)malloc(n);
			if (!entries[i].entries)
				return (entry_t *)0; /*malloc failure*/

			memset((void *)entries[i].entries, 0, n);
		}

		/* continue the search (recursive!) */
		return findfromentry(table, entries[i].entries, str);
	}

	/* otherwise this is the last point */
	return &entries[i];
}
entry_t *find(table_t *table, const char *str) {
	return findfromentry(table, table->entries, str);
}

entry_t *newentry(table_t *table, const char *str, void *p) {
	entry_t *entry;

	entry = find(table, str);

	if (!entry)
		return (entry_t *)0;

	if (entry->p != (void *)0)
		return (entry_t *)0;

	entry->p = p;
	return entry;
}
entry_t *setentry(table_t *table, const char *str, void *p) {
	entry_t *entry;

	entry = find(table, str);

	if (!entry)
		return (entry_t *)0;

	if (!entry->p)
		return (entry_t *)0;

	entry->p = p;
	return entry;
}
void *getentry(table_t *table, const char *str) {
	entry_t *entry;

	entry = find(table, str);

	if (!entry)
		return (void *)0;

	return entry->p;
}

/* -------------------------------------------------------------------------- */

table_t g_table;

typedef union { void *p; int i; } ptrint_t;

void newi(const char *name, int i) {
	ptrint_t v;

	v.p = (void *)0;
	v.i = i;

	newentry(&g_table, name, v.p);
}
void seti(const char *name, int i) {
	ptrint_t v;

	v.p = (void *)0;
	v.i = i;

	setentry(&g_table, name, v.p);
}
int geti(const char *name) {
	ptrint_t v;

	v.p = getentry(&g_table, name);
	return v.i;
}

void deinit() {
	deinittable(&g_table);
}
void init() {
	inittable(&g_table,
		"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_");

	atexit(deinit);
}

void test_new(const char *name, int i) {
	newi(name, i);

	printf("%s = %i (added)\n", name, i);
	fflush(stdout);
}
void test_set(const char *name, int i) {
	printf("%s = %i\n", name, i);
	fflush(stdout);

	seti(name, i);
}
void test_get(const char *name) {
	int i;

	printf("%s: ", name);
	fflush(stdout);

	i = geti(name);

	printf("%i\n", i);
	fflush(stdout);
}

int main() {
	init();

	test_new("r_multiSamples", 4);
	test_new("r_fullscreen", 0);
	test_set("r_fullscreen", 1);
	test_set("r_multiSamples", 1);
	test_new("r_multiSamples_Quality", 16);
	test_new("sv_noCheats", 1);
	test_new("g_godmode", 0);

	test_get("r_multiSamples");
	test_get("r_multiSamples_Quality");
	test_get("sv_noCheats");
	test_get("g_godmode");

	return 0;
}
