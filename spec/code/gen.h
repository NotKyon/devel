#ifndef GEN_H
#define GEN_H

#include "local.h"
#include "ir.h"

void Gen(ir_t *ir, const char *header);

#endif
