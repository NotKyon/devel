#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define _M_PI	(3.141592653589f)
#define _M_TAU	(_M_PI*2.0f)

#define _WRAPRAD_FLOOR(angle)\
	((angle) - ((float)((int)((angle)/_M_TAU)))*_M_TAU)
#define WRAPRAD(angle)\
	(_WRAPRAD_FLOOR(angle) > _M_PI ?\
	_WRAPRAD_FLOOR(angle) - _M_TAU : _WRAPRAD_FLOOR(angle))

#define WRAP360(angle) ((angle) - ((float)((int)((angle)/360.0f)))*360.0f)
#define SIGN(x) (x < 0 ? -1 : x > 0 ? + 1 : 0)
#define ABS(x) (x < 0 ? -x : x)

#define _SIN_B (4.0f/_M_PI)
#define _SIN_C (-4.0f/(_M_PI*_M_PI))
#define _SIN_Q (0.775f)
#define _SIN_P (0.225f)

#define _SIN_T1(x)\
	(_SIN_B*WRAPRAD(x) + SIGN(WRAPRAD(x))*_SIN_C*WRAPRAD(x)*WRAPRAD(x))

#define SINE(x)\
	(_SIN_P*(_SIN_T1(x)*ABS(_SIN_T1(x))*_SIN_T1(x) - _SIN_T1(x)) + _SIN_T1(x))
#define COSINE(x)\
	SINE((x) + _M_PI/2.0f)

int main() {
#define TEST(x) printf("%g -> %g\n", (x), ABS(sin(x) - SINE(x)))

	TEST(-5.4f);
	TEST(+2.7f);
	TEST(-3.1f);
	TEST(+9.8f);
	TEST(+_M_PI);
	TEST(+_M_TAU);
	TEST(-_M_PI);
	TEST(-_M_TAU);

#undef TEST

	return EXIT_SUCCESS;
}
