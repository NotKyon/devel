/*

	SUUGAKU DEMO PROGRAM
	====================

	Demonstrates how to use Suugaku's math routines to construct an object and
	camera transformation system, using the familiar "entity structure" of other
	projects (such as Blitz3D or Nuclear Fusion).

	Written by NotKyon (Aaron Miller) for Michael S.


	CONTROLS
	--------

	WASD moves the camera.
	Up-arrow and down-arrow moves the camera up and down.
	Left-arrow and right-arrow turns the camera left and right.
	<SPACE> will point the camera directly at the centered red cube.
	<ESC> exits the demo.


	COMPILATION
	-----------

	If you're building on the command-line:

		g++ -o suugaku_demo suugaku_demo.cpp suugaku.cpp
			suugaku_demo_window.cpp -lgdi32 -lopengl32

	If you're building from an IDE, be sure to add the Suugaku source files to
	your project and link against gdi32 and opengl32.


	DEPENDENCIES
	------------

	Depends on "ax_time," which is public domain. The dependency is only needed
	for this demo. Suugaku itself does not require it.

*/

#include "suugaku.hpp"
#include "suugaku_demo_window.hpp"

#define AXTIME_IMPLEMENTATION
#include "axlib/ax_time.h"

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#include <GL/gl.h>

void PrintMatrix( const suugaku::Mat3f &M )
{
	printf( "[ %.4f, %.4f, %.4f,\n  %.4f, %.4f, %.4f,\n  %.4f, %4.f, %.4f ]\n",
		M.xx, M.yx, M.zx,
		M.xy, M.yy, M.zy,
		M.xz, M.yz, M.zz );
}
void PrintMatrix( const suugaku::Mat4f &M )
{
	printf
	(
		"[ %.4f, %.4f, %.4f, %.4f,\n"
		"  %.4f, %.4f, %.4f, %.4f,\n"
		"  %.4f, %.4f, %.4f, %.4f,\n"
		"  %.4f, %.4f, %.4f, %.4f ]\n",
		M.xx, M.yx, M.zx, M.wx,
		M.xy, M.yy, M.zy, M.wy,
		M.xz, M.yz, M.zz, M.wz,
		M.xw, M.yw, M.zw, M.ww
	);
}

namespace engine
{

	typedef suugaku::Vec2f Vec2f;
	typedef suugaku::Vec3f Vec3f;
	typedef suugaku::Vec4f Vec4f;
	typedef suugaku::Mat3f Mat3f;
	typedef suugaku::Mat4f Mat4f;

	struct SCameraRenderInfo;
	struct SObjectRenderInfo;
	struct SRenderInfo;
	class IEntityRenderer;
	class Entity;
	class Camera;

	// Holds the parameters for rendering objects that are updated when the
	// camera is changed
	struct SCameraRenderInfo
	{
		Mat4f ViewXf;
		Mat4f ViewInvXf;
		Mat4f Proj;
	};
	// Holds the parameters for rendering objects that are updated when the
	// object is changed
	struct SObjectRenderInfo
	{
		Mat4f WorldXf;
		Mat4f WorldViewXf;
		Mat4f WorldViewProjXf;
	};

	// All rendering information needed to render an object
	struct SRenderInfo
	{
		// Camera rendering settings; updated per camera
		const SCameraRenderInfo *pCamera;
		// Object rendering settings; updated per object
		const SObjectRenderInfo *pObject;
	};

	// Interface that you can implement to render an object
	//
	// This interface doesn't do anything by itself. An implementation inherits
	// from this and provides its own OnRender() method.
	//
	// You attach a pointer of an instance of your own rendering interface to an
	// entity.
	class IEntityRenderer
	{
	public:
		// Constructor
		IEntityRenderer() {}
		// Destructor
		virtual ~IEntityRenderer() {}

		// Render the given entity with the given rendering information
		virtual void OnRender( Entity &Ent, const SRenderInfo &Info ) = 0;
	};

	// A node in the scene. This can be something that is rendered, or just be
	// an invisible object that influences other entities or the scene itself.
	//
	// All entities form hierarchies. An entity can be a parent to multiple
	// children, which can in turn be parents to their own children recursively.
	// Each entity will inherit the transform (position, rotation, scale) of its
	// parent entity. Thus its local transformation is merely an offset from the
	// parent transformation.
	//
	// Entities are not drawn by default. You must attach a renderer to them via
	// a call to SetRenderer().
	//
	// You can inherit from this class to make your own special type of entity.
	class Entity
	{
	friend class Camera;
	public:
		// Constructor
		Entity();
		// Destructor
		virtual ~Entity();

		// Set the parent entity of this to `pPrnt`
		//
		// If pPrnt is NULL then this is removed from all hierarchies.
		// Otherwise, this entity will be added as the last child of `pPrnt`.
		void SetParent( Entity *pPrnt );
		// Determine whether `Prnt` is an ancestor of this entity. That is,
		// check whether this entity exists within the hierarchy of `Prnt`.
		bool IsDescendantOf( const Entity &Prnt ) const;

		// Retrieve the parent entity
		/*-*/ Entity *Parent()       { return m_pPrnt; }
		const Entity *Parent() const { return m_pPrnt; }

		// Retrieve the previous sibling entity
		/*-*/ Entity *Prev()       { return m_pPrev; }
		const Entity *Prev() const { return m_pPrev; }

		// Retrieve the next sibling entity
		/*-*/ Entity *Next()       { return m_pNext; }
		const Entity *Next() const { return m_pNext; }

		// Retrieve the first child entity of this
		/*-*/ Entity *Head()       { return m_pHead; }
		const Entity *Head() const { return m_pHead; }

		// Retrieve the last child entity of this
		/*-*/ Entity *Tail()       { return m_pTail; }
		const Entity *Tail() const { return m_pTail; }

		// Set the local position of this entity to `Pos`.
		void Position( const Vec3f &Pos );
		void Position( float fPosX, float fPosY, float fPosZ ) { Position( Vec3f( fPosX, fPosY, fPosZ ) ); }
		// Set the local rotation of this entity to `Angles`.
		void Rotate( const Vec3f &Angles );
		void Rotate( float fRotX, float fRotY, float fRotZ ) { Rotate( Vec3f( fRotX, fRotY, fRotZ ) ); }
		// Set the local scale of this entity to `fScale`.
		void Scale( float fScale );

		// Retrieve the current local position of this entity
		const Vec3f &Position() const;
		inline float PositionX() const { return Position().x; }
		inline float PositionY() const { return Position().y; }
		inline float PositionZ() const { return Position().z; }
		// Retrieve the current local rotation of this entity
		const Mat3f &Rotation() const;
		// Retrieve the current local scale of this entity
		float Scale() const;

		// Retrieve the current global position of this entity
		Vec3f GlobalPosition() const;
		inline float GlobalPositionX() const { return GlobalPosition().x; }
		inline float GlobalPositionY() const { return GlobalPosition().y; }
		inline float GlobalPositionZ() const { return GlobalPosition().z; }
		// Retrieve the current global rotation of this entity
		Mat3f GlobalRotation() const;
		// Retrieve the current global scale of this entity
		float GlobalScale() const;

		// Move this entity relative to the direction it's facing
		void Move( const Vec3f &Dist );
		void MoveX( float fDist ) { Move( Vec3f( fDist, 0, 0 ) ); }
		void MoveY( float fDist ) { Move( Vec3f( 0, fDist, 0 ) ); }
		void MoveZ( float fDist ) { Move( Vec3f( 0, 0, fDist ) ); }
		inline void Move( float fDistX, float fDistY, float fDistZ ) { Move( Vec3f( fDistX, fDistY, fDistZ ) ); }

		// Turn this entity relative to its current orientation
		void Turn( const Vec3f &Delta );
		void TurnX( float fDelta );
		void TurnY( float fDelta );
		void TurnZ( float fDelta );
		inline void Turn( float fDeltaX, float fDeltaY, float fDeltaZ ) { Turn( Vec3f( fDeltaX, fDeltaY, fDeltaZ ) ); }

		// Point this entity directly at another entity
		void LookAt( const Vec3f &Point );
		void LookAt( const Entity &OtherEnt );
		inline void LookAt( float fGlobPtX, float fGlobPtY, float fGlobPtZ ) { LookAt( Vec3f( fGlobPtX, fGlobPtY, fGlobPtZ ) ); }

		// Create the complete local transform of this entity
		Mat4f ConstructLocalTransform() const;
		// Create the complete global transform of this entity
		Mat4f ConstructGlobalTransform() const;

		// Set the rendering interface for this entity
		void SetRenderer( IEntityRenderer *pRenderer );
		// Retrieve the rendering interface of this entity
		/*-*/ IEntityRenderer *GetRenderer()       { return m_pRenderer; }
		const IEntityRenderer *GetRenderer() const { return m_pRenderer; }

	private:
		Entity *         m_pPrnt;
		Entity *         m_pPrev, *m_pNext;
		Entity *         m_pHead, *m_pTail;

		Mat3f            m_XfRot;
		Vec3f            m_XfPos;
		float            m_fScale;

		IEntityRenderer *m_pRenderer;

		void Unlink();
	};

	// A camera entity, which allows a view into the scene
	class Camera: public Entity
	{
	public:
		// Constructor
		Camera();
		// Destructor
		virtual ~Camera();

		// Renders a given entity (treating `Ent` as a root node, and ignoring
		// its renderer)
		void RenderRoot( Entity &Ent );

		// Select the projection z-range (applies to both perspective and
		// orthographic projections)
		void SetZRange( float fZNear, float fZFar );
		float GetZNear() const { return m_fZNear; }
		float GetZFar () const { return m_fZFar ; }

		// Use a perspective projection with the given parameters
		void ProjToPersp( float fFovDeg, float fAspect );
		// Use an orthographic projection with the given parameters
		void ProjToOrtho( float fLeft, float fTop, float fRight, float fBottom );

		// Determine whether the projection is perspective
		bool IsPerspProj() const;
		// Determine whether the projection is orthographic
		bool IsOrthoProj() const;

		// Retrieve the perspective projection's field-of-view (or zero)
		float GetFov   () const { return IsPerspProj() ? m_ProjCfg.Persp.fFov    : 0; }
		// Retrieve the perspective projection's aspect ratio (or zero)
		float GetAspect() const { return IsPerspProj() ? m_ProjCfg.Persp.fAspect : 0; }

		// Retrieve the orthographic projection's left edge (or zero)
		float GetLeft  () const { return IsOrthoProj() ? m_ProjCfg.Ortho.fLeft   : 0; }
		// Retrieve the orthographic projection's top edge (or zero)
		float GetTop   () const { return IsOrthoProj() ? m_ProjCfg.Ortho.fTop    : 0; }
		// Retrieve the orthographic projection's right edge (or zero)
		float GetRight () const { return IsOrthoProj() ? m_ProjCfg.Ortho.fRight  : 0; }
		// Retrieve the orthographic projection's bottom edge (or zero)
		float GetBottom() const { return IsOrthoProj() ? m_ProjCfg.Ortho.fBottom : 0; }

	private:
		Mat4f    m_Proj;
		float    m_fZNear, m_fZFar;
		union {
			struct {
				float fFov;
				float fAspect;
			}    Persp;
			struct {
				float fLeft;
				float fTop;
				float fRight;
				float fBottom;
			}    Ortho;
		}        m_ProjCfg;

		void RenderFrom_r( Entity &Ent, const SRenderInfo &ParentInfo );
	};




	/*
	========================================================================
	
		ENTITY
	
	========================================================================
	*/

	Entity::Entity()
	: m_pPrnt( nullptr )
	, m_pPrev( nullptr )
	, m_pNext( nullptr )
	, m_pHead( nullptr )
	, m_pTail( nullptr )
	, m_XfRot()
	, m_XfPos()
	, m_fScale( 1.0f )
	, m_pRenderer( nullptr )
	{
	}
	Entity::~Entity()
	{
		Unlink();

		while( m_pHead != nullptr ) {
			delete m_pHead;
		}
	}

	void Entity::Unlink()
	{
		if( !m_pPrnt ) {
			return;
		}

		if( m_pPrev != nullptr ) {
			m_pPrev->m_pNext = m_pNext;
		} else {
			m_pPrnt->m_pHead = m_pNext;
		}

		if( m_pNext != nullptr ) {
			m_pNext->m_pPrev = m_pPrev;
		} else {
			m_pPrnt->m_pTail = m_pPrev;
		}

		m_pPrnt = nullptr;
		m_pPrev = nullptr;
		m_pNext = nullptr;
	}

	void Entity::SetParent( Entity *pPrnt )
	{
		if( !pPrnt ) {
			Unlink();
			return;
		}

		if( pPrnt->IsDescendantOf( *this ) ) {
			pPrnt->SetParent( m_pPrnt );
		}

		Unlink();
		m_pPrnt = pPrnt;
		m_pPrev = pPrnt->m_pTail;
		m_pNext = nullptr;
		if( pPrnt->m_pTail != nullptr ) {
			pPrnt->m_pTail->m_pNext = this;
		} else {
			pPrnt->m_pHead = this;
		}
		pPrnt->m_pTail = this;
	}
	bool Entity::IsDescendantOf( const Entity &Prnt ) const
	{
		const Entity *pTest = this;
		do {
			pTest = pTest->m_pPrnt;

			if( pTest == &Prnt ) {
				return true;
			}
		} while( pTest != nullptr );

		return false;
	}

	void Entity::Position( const Vec3f &Pos )
	{
		m_XfPos = Pos;
	}
	void Entity::Rotate( const Vec3f &Angles )
	{
		m_XfRot.LoadRotation( Angles );
	}
	void Entity::Scale( float fScale )
	{
		m_fScale = fScale;
	}

	const Vec3f &Entity::Position() const
	{
		return m_XfPos;
	}
	const Mat3f &Entity::Rotation() const
	{
		return m_XfRot;
	}
	float Entity::Scale() const
	{
		return m_fScale;
	}

	Vec3f Entity::GlobalPosition() const
	{
		const Mat4f GlobalXf = ConstructGlobalTransform();
		return Vec3f( GlobalXf.xw, GlobalXf.yw, GlobalXf.zw );
	}
	Mat3f Entity::GlobalRotation() const
	{
		const Mat4f GlobalXf = ConstructGlobalTransform();
		return Mat3f( GlobalXf );
	}
	float Entity::GlobalScale() const
	{
		float fScale = m_fScale;
		for( const Entity *pPrnt = m_pPrnt; pPrnt != nullptr; pPrnt = pPrnt->m_pPrnt ) {
			fScale *= pPrnt->m_fScale;
		}
		return fScale;
	}

	void Entity::Move( const Vec3f &Dist )
	{
		m_XfPos += MoveVector( m_XfRot, Dist );
	}

	void Entity::Turn( const Vec3f &Delta )
	{
		m_XfRot.ApplyRotation( Delta );
	}
	void Entity::TurnX( float fDelta )
	{
		m_XfRot.ApplyXRotation( fDelta );
	}
	void Entity::TurnY( float fDelta )
	{
		m_XfRot.ApplyYRotation( fDelta );
	}
	void Entity::TurnZ( float fDelta )
	{
		m_XfRot.ApplyZRotation( fDelta );
	}

	void Entity::LookAt( const Vec3f &Point )
	{
		// Retrieve the point relative to our current location
		const Vec3f MyGlobalPos = GlobalPosition();
		const Vec3f LocalTarget = Point - MyGlobalPos;

		// Figure out which rotation angles we'll need to look at the given
		// target from our point-of-view
		const Vec3f NewRot = suugaku::LookAt( m_XfPos, LocalTarget );

		// Use that rotation
		m_XfRot.LoadRotation( NewRot );
		
#if 0 // Debug print statements to check the rotation
		printf
		(
			"<LOOK-AT>\n"
			"  m_XfPos=(%.4f, %.4f, %.4f)\n"
			"  Point=(%.4f, %.4f, %.4f)\n"
			"  GlobalPos=(%.4f, %.4f, %.4f)\n"
			"  LocalTarget=(%.4f, %.4f, %.4f)\n"
			"  NewRot=(%.4f, %.4f, %.4f)\n"
			"\n",
			m_XfPos.x, m_XfPos.y, m_XfPos.z,
			Point.x, Point.y, Point.z,
			MyGlobalPos.x, MyGlobalPos.y, MyGlobalPos.z,
			LocalTarget.x, LocalTarget.y, LocalTarget.z,
			NewRot.x, NewRot.y, NewRot.z
		);
		printf( "*m_XfRot*:\n" );
		PrintMatrix( m_XfRot );
		printf( "</LOOK-AT>\n\n" );
#endif

#if 1 // Debug print to check the details of our rotation
		Vec3f e, s;
		m_XfRot.Decompose( e, s );
		printf( "NewRot = (%.4f, %.4f, %.4f)\n", NewRot.x, NewRot.y, NewRot.z );
		printf( "DecRot = (%.4f, %.4f, %.4f)\n", e.x, e.y, e.z );
		printf( "Scales = (%.4f, %.4f, %.4f)\n", s.x, s.y, s.z );
		printf( "\n" );
#endif
	}
	void Entity::LookAt( const Entity &OtherEnt )
	{
		// Point this entity at the given entity
		LookAt( OtherEnt.GlobalPosition() );
	}

	Mat4f Entity::ConstructLocalTransform() const
	{
		return
			Mat4f( m_XfRot )
				.ApplyScaling( m_fScale )
				.SetTranslation( m_XfPos );
	}
	Mat4f Entity::ConstructGlobalTransform() const
	{
		if( !m_pPrnt ) {
			return ConstructLocalTransform();
		}

		return
			Mat4f().LoadAffineMultiply
			(
				m_pPrnt->ConstructGlobalTransform(),
				ConstructLocalTransform()
			);
	}
	
	void Entity::SetRenderer( IEntityRenderer *pRenderer )
	{
		m_pRenderer = pRenderer;
	}




	/*
	========================================================================
	
		CAMERA ENTITY
	
	========================================================================
	*/

	Camera::Camera()
	: Entity()
	, m_fZNear( 1.0f )
	, m_fZFar( 16384.0f )
	{
		ProjToPersp( 70.0f, 1.777777777777f );
	}
	Camera::~Camera()
	{
	}

	// Render the hierarchy of Ent from the view of this camera
	void Camera::RenderRoot( Entity &Ent )
	{
		// Prepare the camera's rendering information
		SCameraRenderInfo CamInfo;
		CamInfo.ViewInvXf = ConstructGlobalTransform();
		CamInfo.ViewXf    = Mat4f().LoadAffineInverse( CamInfo.ViewInvXf );
		CamInfo.Proj      = m_Proj;

		// Prepare the object's rendering information (the root node)
		SObjectRenderInfo ObjInfo;
		ObjInfo.WorldXf   = Ent.ConstructGlobalTransform();

		// These are updated for everything rendered, so no need to calculate
		ObjInfo.WorldViewXf.LoadIdentity();
		ObjInfo.WorldViewProjXf.LoadIdentity();

		// Setup the general rendering structure
		SRenderInfo       Info;
		Info.pCamera      = &CamInfo;
		Info.pObject      = &ObjInfo;

		// Enter the main rendering loop
		RenderFrom_r( Ent, Info );
	}
	// [internal] Recursively render an entity hierarchy
	//
	// This function also handles creation of the WorldView and WorldViewProj
	// matrices.
	void Camera::RenderFrom_r( Entity &Ent, const SRenderInfo &ParentInfo )
	{
		SObjectRenderInfo ObjInfo;
		SRenderInfo       Info;

		Info.pCamera = ParentInfo.pCamera;
		Info.pObject = &ObjInfo;

		// Run through each child entity
		for( Entity *pEnt = Ent.Head(); pEnt != nullptr; pEnt = pEnt->Next() ) {
			// Calculate the world-space transform of the object
			ObjInfo.WorldXf.LoadAffineMultiply
			(
				ParentInfo.pObject->WorldXf,
				pEnt->ConstructLocalTransform()
			);

			// Determine if we can render this object
			if( pEnt->m_pRenderer != nullptr ) {
				// Calculate the world-view matrix
				ObjInfo.WorldViewXf.LoadAffineMultiply
				(
					ParentInfo.pCamera->ViewXf,
					ObjInfo.WorldXf
				);

				// Calculate hte world-view-projection matrix
				ObjInfo.WorldViewProjXf.LoadMultiply
				(
					ObjInfo.WorldViewXf,
					ParentInfo.pCamera->Proj
				);

				// Render the object
				pEnt->m_pRenderer->OnRender( *pEnt, Info );
			}

			// Render all child entities
			RenderFrom_r( *pEnt, Info );
		}
	}

	void Camera::SetZRange( float fZNear, float fZFar )
	{
		m_fZNear = fZNear;
		m_fZFar  = fZFar;

		if( IsPerspProj() ) {
			ProjToPersp( m_ProjCfg.Persp.fFov, m_ProjCfg.Persp.fAspect );
		} else {
			ProjToOrtho
			(
				m_ProjCfg.Ortho.fLeft,
				m_ProjCfg.Ortho.fTop,
				m_ProjCfg.Ortho.fRight,
				m_ProjCfg.Ortho.fBottom
			);
		}
	}

	void Camera::ProjToPersp( float fFovDeg, float fAspect )
	{
		m_ProjCfg.Persp.fFov    = fFovDeg;
		m_ProjCfg.Persp.fAspect = fAspect;

		m_Proj.LoadPerspProj( fFovDeg, fAspect, m_fZNear, m_fZFar );
	}
	void Camera::ProjToOrtho( float fLeft, float fTop, float fRight, float fBottom )
	{
		m_ProjCfg.Ortho.fLeft   = fLeft;
		m_ProjCfg.Ortho.fTop    = fTop;
		m_ProjCfg.Ortho.fRight  = fRight;
		m_ProjCfg.Ortho.fBottom = fBottom;

		m_Proj.LoadOrthoProj( fLeft, fRight, fBottom, fTop, m_fZNear, m_fZFar );
	}

	bool Camera::IsPerspProj() const
	{
		return m_Proj.IsProjPersp();
	}
	bool Camera::IsOrthoProj() const
	{
		return m_Proj.IsProjOrtho();
	}

}

// Takes care of rendering cube entities
class CCubeRenderer: public engine::IEntityRenderer
{
public:
	CCubeRenderer()
	{
	}
	virtual ~CCubeRenderer()
	{
	}

	virtual void OnRender( engine::Entity &Ent, const engine::SRenderInfo &Info ) override
	{
		static const float fVertexData[] = {
			-1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			 1.0f, 1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f, 1.0f,-1.0f,
			 1.0f,-1.0f, 1.0f,
			-1.0f,-1.0f,-1.0f,
			 1.0f,-1.0f,-1.0f,
			 1.0f, 1.0f,-1.0f,
			 1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f,-1.0f,
			 1.0f,-1.0f, 1.0f,
			-1.0f,-1.0f, 1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f,-1.0f, 1.0f,
			 1.0f,-1.0f, 1.0f,
			 1.0f, 1.0f, 1.0f,
			 1.0f,-1.0f,-1.0f,
			 1.0f, 1.0f,-1.0f,
			 1.0f,-1.0f,-1.0f,
			 1.0f, 1.0f, 1.0f,
			 1.0f,-1.0f, 1.0f,
			 1.0f, 1.0f, 1.0f,
			 1.0f, 1.0f,-1.0f,
			-1.0f, 1.0f,-1.0f,
			 1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f,-1.0f,
			-1.0f, 1.0f, 1.0f,
			 1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			 1.0f,-1.0f, 1.0f
		};

		( void )Ent;
		( void )Info;

#if 0 // DEBUG PRINTS
		printf( "<< ENTITY %p (of %p) >>\n", ( void * )&Ent, ( void * )Ent.Parent() );
		printf( "Camera->ViewXf:\n" );
		PrintMatrix( Info.pCamera->ViewXf );
		printf( "Camera->ViewInvXf:\n" );
		PrintMatrix( Info.pCamera->ViewInvXf );
		printf( "Camera->Proj:\n" );
		PrintMatrix( Info.pCamera->Proj );
		printf( "Object->WorldXf:\n" );
		PrintMatrix( Info.pObject->WorldXf );
		printf( "Object->WorldViewXf:\n" );
		PrintMatrix( Info.pObject->WorldViewXf );
		printf( "\n\n" );
#endif

		glMatrixMode( GL_PROJECTION );
		glLoadMatrixf( ( const GLfloat * )Info.pCamera->Proj.Ptr() );

		glMatrixMode( GL_MODELVIEW );
		glLoadMatrixf( ( const GLfloat * )Info.pObject->WorldViewXf.Ptr() );
		
		glEnable( GL_DEPTH_TEST );

		if( !Ent.Parent() || !Ent.Parent()->GetRenderer() ) {
			glColor4f( 1, 0, 0, 1 );
		} else {
			glColor4f( 1, 1, 1, 1 );
		}
		glEnable( GL_VERTEX_ARRAY );
		glVertexPointer( 3, GL_FLOAT, 0, &fVertexData[0] );
		glDrawArrays( GL_TRIANGLES, 0, 12*3 );
		glDisable( GL_VERTEX_ARRAY );
	}
};

int main()
{
	// Rendering resolution (set to zero initially to force some things to
	// update)
	unsigned uResX = 0, uResY = 0;

	// Initialize
	if( !InitWindow() ) {
		return EXIT_FAILURE;
	}

	// The cube rendering class (passed by pointer to the cube entities)
	CCubeRenderer CubeRenderer;

	// Scene root (isn't directly rendered, but its child nodes are)
	engine::Entity SceneRoot;
	// Cube entities in a three level hierarchy
	engine::Entity CubeL1, CubeL2A, CubeL2B, CubeL2AL3A;
	// Camera entity (scene is rendered from the camera)
	engine::Camera MainCam;

	// Put this cube in the scene
	CubeL1.SetParent( &SceneRoot );

	// Add the level two cubes as children of the level one cube
	CubeL2A.SetParent( &CubeL1 );
	CubeL2B.SetParent( &CubeL1 );

	// Add the level three cube as a child of the level-two-A cube
	CubeL2AL3A.SetParent( &CubeL2A );

	// Set the cube renderer to each of these cube entities, so we can see them
	// drawn as cubes
	CubeL1.SetRenderer( &CubeRenderer );
	CubeL2A.SetRenderer( &CubeRenderer );
	CubeL2B.SetRenderer( &CubeRenderer );
	CubeL2AL3A.SetRenderer( &CubeRenderer );

	// Position the cubes
	CubeL1.Position( 0, 0, 0 );
	CubeL2A.Position( -2, 2,  2 );
	CubeL2B.Position(  2, 2, -2 );
	CubeL2AL3A.Position( -1, 1, -1 );

	// Move the camera back a bit
	MainCam.Position( 0, 1, -10 );
	MainCam.LookAt( CubeL1 ); // ###FIXME### Doesn't work :<
	
	axtm_u64_t uLastTime = Ax::Microseconds();
	axtm_u64_t uElapsedTime = 0;

	// Main loop
	int t = 0;
	bool held = false;
	while( ProcessWindowEvents() && !KeyDown( EKey_Escape ) ) {
		// Current time
		const axtm_u64_t uCurrTime = Ax::Microseconds();

		// Update the number of elapsed microseconds
		uElapsedTime = uCurrTime - uLastTime;
		uLastTime = uCurrTime;

		// Current frame delta time
		const float fDeltaTime = Ax::MicrosecondsToSeconds( uElapsedTime );

		// Movement speed
		static const float fMoveSpd = 12.5f;
		// Turning speed
		static const float fTurnSpd = 36.0f;
		
		// Update the camera
		if( KeyDown( EKey_W ) ) { MainCam.MoveZ(  fDeltaTime*fMoveSpd ); }
		if( KeyDown( EKey_A ) ) { MainCam.MoveX( -fDeltaTime*fMoveSpd ); }
		if( KeyDown( EKey_S ) ) { MainCam.MoveZ( -fDeltaTime*fMoveSpd ); }
		if( KeyDown( EKey_D ) ) { MainCam.MoveX(  fDeltaTime*fMoveSpd ); }

		if( KeyDown( EKey_UArrow ) ) { MainCam.MoveY(  fDeltaTime*fMoveSpd ); }
		if( KeyDown( EKey_DArrow ) ) { MainCam.MoveY( -fDeltaTime*fMoveSpd ); }

		if( KeyDown( EKey_LArrow ) ) { MainCam.TurnY( -fDeltaTime*fTurnSpd ); }
		if( KeyDown( EKey_RArrow ) ) { MainCam.TurnY(  fDeltaTime*fTurnSpd ); }

		if( KeyDown( EKey_Space ) ) {
			if( !held ) {
				MainCam.LookAt( CubeL1 );
				held = true;
			}
		} else {
			held = false;
		}
		
		// TODO: Use fDeltaTime to update the animation here...
		
		++t;

		CubeL2AL3A.Rotate( 0, (float)((t/200)%360), 0 );
		CubeL2B.Rotate( (float)(((t+100)/125)%360), 0, 0 );
		CubeL1.Rotate( 0, 0, (float)(((t+321)/222)%360) );

		// If the resolution changed, update the camera (aspect needs to be
		// adjusted) and reset the OpenGL viewport
		{
			const unsigned uWinResX = GetWindowResX();
			const unsigned uWinResY = GetWindowResY();

			if( ( uResX != uWinResX || uResY != uWinResY ) && uWinResY != 0 ) {
				uResX = uWinResX;
				uResY = uWinResY;

				MainCam.ProjToPersp( 60.0f, float( uResX )/float( uResY ) );
				glViewport( 0, 0, uResX, uResY );
			}
		}

		// Clear the screen each frame
		glClearColor( 0.1, 0.3, 0.5, 1.0 );
		glClearDepth( 1.0 );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		// Render the scene from the perspective of our camera
		MainCam.RenderRoot( SceneRoot );
		
		// Draw a little yellow dot at the center
		//
		// Purpose: Visual guide for testing the LookAt() routines.
		{
			glDisable( GL_DEPTH_TEST );
			
			glMatrixMode( GL_MODELVIEW );
			glLoadIdentity();
			
			glMatrixMode( GL_PROJECTION );
			glLoadIdentity();

			glBegin( GL_TRIANGLE_STRIP );
				glColor4f( 1,1,0, 1 );

				glVertex2f( -0.0125, -0.0125 );
				glVertex2f(  0.0125, -0.0125 );
				glVertex2f( -0.0125,  0.0125 );
				glVertex2f(  0.0125,  0.0125 );
			glEnd();
		}
		
		// Update the screen
		SwapBuffers();
	}

	// Done
	return EXIT_SUCCESS;
}
