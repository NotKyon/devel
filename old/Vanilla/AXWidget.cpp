// includes
#include <stdio.h>
#include <string.h>
#include <Axia/Vanilla/Vanilla.h>
#include <Axia/Vanilla/Internal.h>

// child enumeration procedure
BOOL CALLBACK AXWidget::EnumChildren(HWND hwndRef, LPARAM lParam)
{
	// declarations
	AXGadget	*pSuper, *pSub;

	// store the super
	pSuper = (AXGadget *)lParam;

	// find the sub
	pSub = Find(hwndRef);
	if (!pSub)
		return FALSE;

	// add this child to the super
	pSuper->m_subList.Add(&pSub->m_subLink);

	// done
	return TRUE;
}

// subclass window procedure
LRESULT CALLBACK AXWidget::MsgProc(HWND hwndRef, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	// declarations
	//PAINTSTRUCT	ps;
	AXWidget	*pWidget,
				*pOtherWidget;
	WNDPROC		pfnMsgProc;
	NMHDR		*pNotify;
	RECT		*prcArea;
	int			mw, mh;
	int			modifier, x, y, eventType, button;

	// find the widget
	pWidget = Find(hwndRef);
	if (!pWidget)
		return ::DefWindowProcA(hwndRef, uiMsg, wParam, lParam);

	// get the message procedure
	pfnMsgProc = pWidget->m_pfnMsgProc;

	// messages
	switch(uiMsg)
	{
		// TODO: minimum/maximum size
		// close
		case WM_CLOSE:
			// make the event
			new AXEvent(AXWindowCloseEvent, (void *)pWidget);

			// done
			return 0;

		// destruction
		case WM_DESTROY:
			// set the appropriate callback handler
			::SetWindowLongPtrA(hwndRef, GWL_WNDPROC, (LONG_PTR)pfnMsgProc);

			// release the widget if the option is set
			if(pWidget->m_bRelease)
			{
				pWidget->m_hwndRef = (HWND)0;
				delete pWidget;
			}
			// or: unset the window handle
			else
				pWidget->m_hwndRef = (HWND)0;

			// make the event
			new AXEvent(AXWindowClosedEvent, (void *)pWidget);

			// done
			break;

		// commands (hot key, menu action)
		case WM_COMMAND:
			// check what type of event this is
			switch(HIWORD(wParam))
			{
				// menu / button click
				case 0:
					if (!lParam)
					{
						new AXEvent(AXMenuActionEvent, (void *)pWidget, 0, 0, 0, (void *)(int)LOWORD(wParam));
						break;
					}

					new AXEvent(AXButtonActionEvent, (void *)Find((HWND)lParam));
					break;

				// hot key
				case 1:
					new AXEvent(AXHotKeyHitEvent, (void *)pWidget, 0, 0, 0, (void *)(int)LOWORD(wParam));
					break;

				// unknown
				default:
					break;
			}

			// done
			break;

		// control notification
		case WM_NOTIFY:
			// get the handle
			pNotify = (NMHDR *)lParam;

			// check the item
			pOtherWidget = Find(pNotify->hwndFrom);

			// check the code
			switch(pNotify->code)
			{
				// treeview node expand/collapse (open/close)
				case TVN_ITEMEXPANDED:
					// TODO: Better reporting for nodes
					if (((NMTREEVIEW *)pNotify)->action==TVE_EXPAND)
						new AXEvent(AXNodeOpenEvent, (void *)pOtherWidget, 0, 0, 0, (void *)0);
					else
						new AXEvent(AXNodeCloseEvent, (void *)pOtherWidget, 0, 0, 0, (void *)0);

					break;

				// tab change
				case TCN_SELCHANGE:
				{
					int			index;

					if (pOtherWidget->Type() != AXTabberGadgetType)
						break;

					index = TabCtrl_GetCurSel(pNotify->hwndFrom);
					((AXTabber *)pOtherWidget)->ChangeSelection(((AXTabber *)pOtherWidget)->FindTab(index));

					::SetFocus(0);

					break;
				}

				default:
					break;
			}

			// done
			break;

		// change the title text
		case WM_SETTEXT:
			// calculate the new text length
			pWidget->m_titleLength = ::lstrlenA((const char *)lParam);
			if ((pWidget->m_pszTitle = (char *)realloc((void *)pWidget->m_pszTitle, pWidget->m_titleLength+1)))
				::lstrcpyA(pWidget->m_pszTitle, (const char *)lParam);

			// make the event
			new AXEvent(AXGadgetSetTextEvent, (void *)pWidget, 0, 0, 0, (void *)pWidget->m_pszTitle);

			// done
			break;

		// move the window
		case WM_MOVE:
			// store the new position
			::GetWindowRect(pWidget->m_hwndRef, &pWidget->m_rcScreen);

			// store the new position within the parent
			pWidget->m_rcClient.left = pWidget->m_rcScreen.left;
			pWidget->m_rcClient.top = pWidget->m_rcScreen.top;
			::ScreenToClient((pWidget->m_pSuper)?((AXWidget *)pWidget->m_pSuper)->m_hwndRef:(HWND)0, (POINT *)&pWidget->m_rcClient.left);

			// make the event
			new AXEvent(AXWindowMoveEvent, (void *)pWidget, 0, pWidget->m_rcClient.left, pWidget->m_rcClient.top);

			// done
			break;

		// resize the window
		case WM_SIZE:
			// store the new size
			::GetWindowRect(pWidget->m_hwndRef, &pWidget->m_rcScreen);

			// store the new client size
			pWidget->m_rcClient.right = (int)(short)LOWORD(lParam);
			pWidget->m_rcClient.bottom = (int)(short)HIWORD(lParam);

			// reflow
			pWidget->Reflow();

			// make the event
			new AXEvent(AXWindowSizeEvent, (void *)pWidget, 0, pWidget->m_rcClient.right, pWidget->m_rcClient.bottom);

			// done
			break;

		// minimum/maximum size
		case WM_SIZING:
		{
			// min/max size only supported for windows
			if (pWidget->Type() != AXWindowGadgetType)
				break;

			// ignore on no sizing info
			if (!pWidget->m_minSize.cx && !pWidget->m_minSize.cy && !pWidget->m_maxSize.cx && !pWidget->m_maxSize.cy)
				break;

			// store the proposed area
			prcArea = (RECT *)lParam;

			// store the width/height of the window (in x/y)
			x = prcArea->right-prcArea->left;
			y = prcArea->bottom-prcArea->top;

			// check maximum
			mw = pWidget->m_maxSize.cx;
			mh = pWidget->m_maxSize.cy;
			if (!mw)	mw = 32700;
			if (!mh)	mh = 32700;

			// check the minimum/maximum width
			if (x>=pWidget->m_minSize.cx && y>=pWidget->m_minSize.cy &&
				x<=mw && y<=mh)
				break;

			// check the sizing type
			switch(wParam)
			{
				// bottom
				case WMSZ_BOTTOM:
					// vertical
					if (y < pWidget->m_minSize.cy)
						prcArea->bottom = prcArea->top+pWidget->m_minSize.cy;
					else if(y > mh)
						prcArea->bottom = prcArea->top+mh;

					break;

				// bottom-left
				case WMSZ_BOTTOMLEFT:
					// horizontal
					if (x < pWidget->m_minSize.cx)
						prcArea->left = prcArea->right-pWidget->m_minSize.cx;
					else if(x > mw)
						prcArea->left = prcArea->right-mw;

					// vertical
					if (y < pWidget->m_minSize.cy)
						prcArea->bottom = prcArea->top+pWidget->m_minSize.cy;
					else if(y > mh)
						prcArea->bottom = prcArea->top+mh;

					break;

				// bottom-right
				case WMSZ_BOTTOMRIGHT:
					// horizontal
					if (x < pWidget->m_minSize.cx)
						prcArea->right = prcArea->left+pWidget->m_minSize.cx;
					else if(x > mw)
						prcArea->right = prcArea->left+mw;

					// vertical
					if (y < pWidget->m_minSize.cy)
						prcArea->bottom = prcArea->top+pWidget->m_minSize.cy;
					else if(y > mh)
						prcArea->bottom = prcArea->top+mh;

					break;

				// left
				case WMSZ_LEFT:
					// horizontal
					if (x < pWidget->m_minSize.cx)
						prcArea->left = prcArea->right-pWidget->m_minSize.cx;
					else if(x > mw)
						prcArea->left = prcArea->right-mw;

					break;

				// right
				case WMSZ_RIGHT:
					// horizontal
					if (x < pWidget->m_minSize.cx)
						prcArea->right = prcArea->left+pWidget->m_minSize.cx;
					else if(x > mw)
						prcArea->right = prcArea->left+mw;

					break;

				// top
				case WMSZ_TOP:
					// vertical
					if (y < pWidget->m_minSize.cy)
						prcArea->top = prcArea->bottom-pWidget->m_minSize.cy;
					else if(y > mh)
						prcArea->top = prcArea->bottom-mh;

					break;

				// top-left
				case WMSZ_TOPLEFT:
					// horizontal
					if (x < pWidget->m_minSize.cx)
						prcArea->left = prcArea->right-pWidget->m_minSize.cx;
					else if(x > mw)
						prcArea->left = prcArea->right-mw;

					// vertical
					if (y < pWidget->m_minSize.cy)
						prcArea->top = prcArea->bottom-pWidget->m_minSize.cy;
					else if(y > mh)
						prcArea->top = prcArea->bottom-mh;

					break;

				// top-right
				case WMSZ_TOPRIGHT:
					// horizontal
					if (x < pWidget->m_minSize.cx)
						prcArea->right = prcArea->left+pWidget->m_minSize.cx;
					else if(x > mw)
						prcArea->right = prcArea->left+mw;

					// vertical
					if (y < pWidget->m_minSize.cy)
						prcArea->top = prcArea->bottom-pWidget->m_minSize.cy;
					else if(y > mh)
						prcArea->top = prcArea->bottom-mh;

					break;

				// unknown
				default:
					break;
			}
			break;
		} // WM_SIZING

		// mouse press/release and movement
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
		case WM_MBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
		case WM_MBUTTONUP:
		case WM_MOUSEMOVE:
			// determine the modifier flags
			modifier = 0;
			if (wParam & MK_SHIFT)		modifier |= AXShiftModifierBit;
			if (wParam & MK_CONTROL)	modifier |= AXControlModifierBit;
			if (wParam & MK_LBUTTON)	modifier |= AXLeftButtonModifierBit;
			if (wParam & MK_RBUTTON)	modifier |= AXRightButtonModifierBit;
			if (wParam & MK_MBUTTON)	modifier |= AXMiddleButtonModifierBit;

			// store the position of the mouse
			x = (int)(short)LOWORD(lParam);
			y = (int)(short)HIWORD(lParam);

			// determine whether a mouse leave event should be tracked
			if (uiMsg==WM_MOUSEMOVE) // && pWidget->m_callbacks.MouseLeave)
			{
				// declarations
				TRACKMOUSEEVENT			tme;

				// fill the structure
				tme.cbSize				= sizeof(tme);
				tme.dwFlags				= TME_LEAVE;
				tme.hwndTrack			= pWidget->m_hwndRef;
				tme.dwHoverTime			= 0;

				// request mouse-leave events to be generated
				::TrackMouseEvent(&tme);
			}

			// make the event
			if (uiMsg==WM_LBUTTONDOWN)	{ eventType=AXMouseDownEvent; button=AXLeftMouseButton; }
			if (uiMsg==WM_RBUTTONDOWN)	{ eventType=AXMouseDownEvent; button=AXRightMouseButton; }
			if (uiMsg==WM_MBUTTONDOWN)	{ eventType=AXMouseDownEvent; button=AXMiddleMouseButton; }
			if (uiMsg==WM_LBUTTONUP)	{ eventType=AXMouseUpEvent; button=AXLeftMouseButton; }
			if (uiMsg==WM_RBUTTONUP)	{ eventType=AXMouseUpEvent; button=AXRightMouseButton; }
			if (uiMsg==WM_MBUTTONUP)	{ eventType=AXMouseUpEvent; button=AXMiddleMouseButton; }
			if (uiMsg==WM_MOUSEMOVE)	{ eventType=AXMouseMoveEvent; button=0; }
			new AXEvent(eventType, (void *)pWidget, modifier, x, y, (void *)button);

			// done
			break;

		// mouse wheel
		case WM_MOUSEWHEEL:
			// determine the modifier flags
			modifier = 0;
			if (wParam & MK_SHIFT)		modifier |= AXShiftModifierBit;
			if (wParam & MK_CONTROL)	modifier |= AXControlModifierBit;
			if (wParam & MK_LBUTTON)	modifier |= AXLeftButtonModifierBit;
			if (wParam & MK_RBUTTON)	modifier |= AXRightButtonModifierBit;
			if (wParam & MK_MBUTTON)	modifier |= AXMiddleButtonModifierBit;

			// store the position of the mouse
			x = (int)(short)LOWORD(lParam);
			y = (int)(short)HIWORD(lParam);

			// make the event
			new AXEvent(AXMouseWheelEvent, (void *)pWidget, modifier, x, y, (void *)(int)(((short)HIWORD(wParam))/WHEEL_DELTA));

			// done
			break;

		// mouse leave
		case WM_MOUSELEAVE:
			new AXEvent(AXMouseExitEvent, (void *)pWidget);
			break;
			//return 0;

		// key press
		case WM_KEYDOWN:
			x = (lParam&0x00FF0000)>>16;
			if (lParam&0x01000000)
				x += 128;
			new AXEvent(AXKeyDownEvent, (void *)pWidget, 0, 0, 0, (void *)x);
			break;

		// key up
		case WM_KEYUP:
			x = (lParam&0x00FF0000)>>16;
			if (lParam&0x01000000)
				x += 128;
			new AXEvent(AXKeyUpEvent, (void *)pWidget, 0, 0, 0, (void *)x);
			break;

		// key character
		case WM_CHAR:
			new AXEvent(AXKeyCharEvent, (void *)pWidget, 0, 0, 0, (void *)(char)wParam);
			break;

		// become/resign main window status
		case WM_ACTIVATE:
			new AXEvent((wParam==WA_INACTIVE)?AXWindowDeactivateEvent:AXWindowActivateEvent, (void *)pWidget);
			break;

		// get key window status
		case WM_SETFOCUS:
			new AXEvent(AXWindowAcceptFocusEvent, (void *)pWidget);
			break;

		// resign key window status
		case WM_KILLFOCUS:
			new AXEvent(AXWindowResignFocusEvent, (void *)pWidget);
			break;

		// app activation/deactivation
		case WM_ACTIVATEAPP:
			new AXEvent((((BOOL)wParam)==TRUE)?AXAppResumeEvent:AXAppSuspendEvent);
			break;
	}

	// determine whether the default window procedure should be invoked
	if (!pfnMsgProc)
		return ::DefWindowProcA(hwndRef, uiMsg, wParam, lParam);

	// invoke the subclass procedure
	return ::CallWindowProcA(pfnMsgProc, hwndRef, uiMsg, wParam, lParam);
}

/*

	The following code is from an older version; it's retained for reference.

		// edit control coloring
		case WM_CTLCOLOREDIT:
			SetTextColor((HDC)wParam, RGB(255, 255, 255));
			SetBkMode((HDC)wParam, TRANSPARENT);
			return (LRESULT)CreateSolidBrush(RGB(40, 40, 40));

		// draw a portion of the window
		case WM_ERASEBKGND:
			// determine whether the widget is handling drawing or not
			if (pWidget->m_callbacks.DrawRect)
				// it is, so don't let the system erase the background window
				return 0;

			// it isn't, so continue
			break;
		case WM_PAINT:
			// determine whether the widget is handling drawing or not
			if (!pWidget->m_callbacks.DrawRect)
				// it isn't, so let the control deal with it
				break;

			// prepare drawing with this handle
			::BeginPaint(pWidget->m_hwndRef, &ps);
			if (!pWidget->IsCurrent())
				pWidget->MakeCurrent();

			// handle viewport and clipping
			::glViewport(0, 0, pWidget->m_rcClient.right, pWidget->m_rcClient.bottom);
			::glDisable(GL_LIGHTING);
			::glDisable(GL_DEPTH_TEST);
			::glEnable(GL_SCISSOR_TEST);
			::glScissor(ps.rcPaint.left, ps.rcPaint.top, ps.rcPaint.right, ps.rcPaint.bottom);
			::glClear(GL_COLOR_BUFFER_BIT);

			// projection
			::glMatrixMode(GL_PROJECTION);
			::glLoadIdentity();
			::gluOrtho2D(0, 1, 0, 1);

			// model/view
			::glMatrixMode(GL_MODELVIEW);
			::glLoadIdentity();

			// call the draw function
			pWidget->m_callbacks.DrawRect(pWidget, ps.rcPaint.left, ps.rcPaint.top, ps.rcPaint.right, ps.rcPaint.bottom);
			//pWidget->m_callbacks.DrawRect(pWidget, 0, 0, pWidget->m_rcClient.right, pWidget->m_rcClient.bottom);

			// finish drawing
			::glFlush();
			//::SwapBuffers(ps.hdc);
			//::ValidateRect(pWidget->m_hwndRef, 0);
			::EndPaint(pWidget->m_hwndRef, &ps);

			// done
			return 1;
*/

// transform a client coordinate to global space or client space
void AXWidget::TransformClientCoord(int *pX, int *pY, bool bGlobal)
{
	// declarations
	POINT	pt;
	int		left, top, right, bottom;

	// add
	QueryClientArea(&left, &top, &right, &bottom);
	*pX += left;
	*pY += top;

	// ignore transform to same space
	if (!bGlobal)
		return;

	// convert
	pt.x = *pX;
	pt.y = *pY;
	::ClientToScreen(m_hwndRef, &pt);
	*pX = pt.x;
	*pY = pt.y;
}

// transform a global coordinate to global space or client space
void AXWidget::TransformGlobalCoord(int *pX, int *pY, bool bGlobal)
{
	// declarations
	POINT	pt;

	// ignore transform to same space
	if (bGlobal)
		return;

	// convert
	pt.x = *pX;
	pt.y = *pY;
	::ScreenToClient(m_hwndRef, &pt);
	*pX = pt.x;
	*pY = pt.y;
}

// adjust a client size to a global size (e.g., client frame to window frame)
void AXWidget::AdjustSize(int *pW, int *pH, bool bGlobal)
{
	// declarations
	DWORD	dwExStyle;
	DWORD	dwStyle;
	BOOL	bHasMenu;
	RECT	rcArea;

	// ignore transform to same space
	if (!bGlobal)
		return;

	// get window information
	dwExStyle = (DWORD)::GetWindowLongPtrA(m_hwndRef, GWL_EXSTYLE);
	dwStyle = (DWORD)::GetWindowLongPtrA(m_hwndRef, GWL_STYLE);
	bHasMenu = (::GetMenu(m_hwndRef)!=(HMENU)0)?true:false;

	// prepare the rectangle
	rcArea.left = 0;
	rcArea.top = 0;
	rcArea.right = *pW;
	rcArea.bottom = *pH;

	// convert
	::AdjustWindowRectEx(&rcArea, dwStyle, bHasMenu, dwExStyle);
	*pW = rcArea.right-rcArea.left;
	*pH = rcArea.bottom-rcArea.top;
}

// adjust a shape to global space
void AXWidget::AdjustShape(int *pLeft, int *pTop, int *pRight, int *pBottom, bool bGlobal)
{
	// declarations
	DWORD	dwExStyle;
	DWORD	dwStyle;
	BOOL	bHasMenu;
	RECT	rcArea;

	// ignore transform to same space
	if (bGlobal)
		return;

	// get window information
	dwExStyle = (DWORD)::GetWindowLongPtrA(m_hwndRef, GWL_EXSTYLE);
	dwStyle = (DWORD)::GetWindowLongPtrA(m_hwndRef, GWL_STYLE);
	bHasMenu = (::GetMenu(m_hwndRef)!=(HMENU)0)?true:false;

	// prepare the rectangle
	rcArea.left = *pLeft;
	rcArea.top = *pTop;
	rcArea.right = *pRight;
	rcArea.bottom = *pBottom;

	// convert
	::AdjustWindowRectEx(&rcArea, dwStyle, bHasMenu, dwExStyle);
	*pLeft = rcArea.left;
	*pTop = rcArea.top;
	*pRight = rcArea.right;
	*pBottom = rcArea.bottom;
}

// AXWidget: Constructor
AXWidget::AXWidget(int type, HWND hwndRef, AXGadget *pSuper)
: AXGadget(type, pSuper), m_hwndRef(hwndRef), m_layout(0)
{
	// allocate memory for the window title
	m_titleLength = ::GetWindowTextLengthA(m_hwndRef);
	if ((m_pszTitle = (char *)malloc(m_titleLength+1)) != (char *)0)
		::GetWindowTextA(m_hwndRef, m_pszTitle, m_titleLength+1);
	else
		m_titleLength = 0;

	// get the position of this window on the screen
	::GetWindowRect(m_hwndRef, &m_rcScreen);

	// get the size of this window's client area
	::GetClientRect(m_hwndRef, &m_rcClient);
	m_rcClient.left = m_rcScreen.left;
	m_rcClient.top = m_rcScreen.top;

	// specify the minimum and maximum sizes of the window
	m_minSize.cx = 0;	m_minSize.cy = 0;
	m_maxSize.cx = 0;	m_maxSize.cy = 0;

	// get the window's super
	if (!pSuper)
	{
		if ((m_pSuper = Find(::GetParent(m_hwndRef))) != (AXGadget *)0)
		{
			m_pSuperList->Remove(&m_subLink);
			m_pSuperList = &m_pSuper->m_subList;
			m_pSuperList->Add(&m_subLink);
		}
	}
	if (m_pSuper)
		::ScreenToClient(((AXWidget *)m_pSuper)->WindowRef(), (POINT *)&m_rcClient.left);
	else
		::ScreenToClient((HWND)0, (POINT *)&m_rcClient.left);

	// add each of the children
	::EnumChildWindows(m_hwndRef, EnumChildren, (LPARAM)this);

	// message procedure
	m_pfnMsgProc = (WNDPROC)::SetWindowLongPtrA(m_hwndRef, GWL_WNDPROC, (LONG_PTR)MsgProc);
}

// AXWidget: Destructor
AXWidget::~AXWidget()
{
	// close this window's handle
	m_bRelease = false;		// disabled to prevent catastrophy

	if (m_hwndRef)
		Close();			// closes all child windows too
}

// AXGadget: WindowRef: get the handle (HWND) of the window
HWND AXWidget::WindowRef(void) const
{
	return m_hwndRef;
}

// AXGadget: MessageProc: get the internal message procedure of the window's subclass
WNDPROC AXWidget::MessageProc(void) const
{
	return m_pfnMsgProc;
}

// find the widget with the given handle (HWND); create if necessary
AXWidget *AXWidget::Find(HWND hwndRef)
{
	// declarations
	AXGadget	*pTest;

	// no handle for the root widget
	if (!hwndRef)
		return (AXWidget *)0;

	// go through each handle
	for(pTest=g_gadgetList.First(); pTest; pTest=pTest->m_gadgetLink.After())
	{
		// skip non-widgets
		if (!pTest->IsWidget())
			continue;

		// check
		if (((AXWidget *)pTest)->m_hwndRef==hwndRef)
			return (AXWidget *)pTest;
	}

	// no handle found; create
	return new AXWidget(AXWidgetGadgetType, hwndRef);
}

// AXWidget: SetPosition: change the position of the window
void AXWidget::SetPosition(int x, int y, bool bGlobal)
{
	// position needed in client coordinates
	if (m_pSuper)
		((AXWidget *)m_pSuper)->TransformClientCoord(&x, &y, bGlobal);

	// position
	::SetWindowPos(m_hwndRef, 0, x, y, 0, 0, SWP_NOSIZE|SWP_NOZORDER);
}

// AXWidget: SetSize: change the size of the window
void AXWidget::SetSize(int w, int h, bool bGlobal)
{
	// size needed in global coordinates
	if (m_pSuper)
		((AXWidget *)m_pSuper)->AdjustSize(&w, &h, bGlobal);

	// resize
	::SetWindowPos(m_hwndRef, 0, 0, 0, w, h, SWP_NOMOVE|SWP_NOZORDER);

	// reflow
	Reflow();
}

// AXWidget: SetShape: change the position and size of the window
void AXWidget::SetShape(int left, int top, int right, int bottom, bool bGlobal)
{
	// position needed in client, size needed in global
	if (m_pSuper)
		((AXWidget *)m_pSuper)->AdjustShape(&left, &top, &right, &bottom, bGlobal);

	// reshape
	::MoveWindow(m_hwndRef, left, top, right-left, bottom-top, TRUE);
}

// AXWidget: SetMinimumSize: set the minimum size of the window
void AXWidget::SetMinimumSize(int w, int h)
{
	// store the size specified
	m_minSize.cx = w;
	m_minSize.cy = h;
}

// AXWidget: SetMaximumSize: set the maximum size of the window
void AXWidget::SetMaximumSize(int w, int h)
{
	// store the size specified
	m_maxSize.cx = w;
	m_maxSize.cy = h;
}

// AXWidget: PositionX: get the left position of the window
int AXWidget::PositionX(bool bGlobal)
{
	return (bGlobal)?m_rcScreen.left:m_rcClient.left;
}

// AXWidget: PositionY: get the top position of the window
int AXWidget::PositionY(bool bGlobal)
{
	return (bGlobal)?m_rcScreen.top:m_rcClient.top;
}

// AXWidget: Width: get the width of the window
int AXWidget::Width(bool bGlobal)
{
	return (bGlobal)?m_rcScreen.right-m_rcScreen.left:m_rcClient.right;
}

// AXWidget: Height: get the height of the window
int AXWidget::Height(bool bGlobal)
{
	return (bGlobal)?m_rcScreen.bottom-m_rcScreen.top:m_rcClient.bottom;
}

// AXWidget: MinimumWidth: get the minimum width of the window
int AXWidget::MinimumWidth(void) const
{
	return m_minSize.cx;
}

// AXWidget: MinimumHeight: get the minimum height of the window
int AXWidget::MinimumHeight(void) const
{
	return m_minSize.cy;
}

// AXWidget: MaximumWidth: get the maximum width of the window
int AXWidget::MaximumWidth(void) const
{
	return m_maxSize.cx;
}

// AXWidget: MaximumHeight: get the maximum height of the window
int AXWidget::MaximumHeight(void) const
{
	return m_maxSize.cy;
}

// AXWidget: Center: center the widget
void AXWidget::Center(void)
{
	// declarations
	DWORD	dwStyle;

	// retrieve the style of the widget (needed to determine how to center)
	dwStyle = ::GetWindowLongPtrA(m_hwndRef, GWL_STYLE);

	// sub-widgets (controls) need to center within the super-widget (window)
	if (dwStyle&WS_CHILD && m_pSuper)
		// adjust the position based on the super's resolution
		SetPosition(((AXWidget *)m_pSuper)->Width(false)/2-Width(false)/2,
					((AXWidget *)m_pSuper)->Height(false)/2-Height(false)/2,
					false);
	// super-widgets (windows) need to center within the screen
	else
		// adjust the position based on the screen's resolution
		SetPosition(::GetSystemMetrics(SM_CXSCREEN)/2-Width(false)/2, ::GetSystemMetrics(SM_CYSCREEN)/2-Height(false)/2, true);
}

// AXWidget: SetParent: change the parent of this widget
void AXWidget::SetParent(AXGadget *pSuper)
{
	// validate the super
	if (pSuper)
		if (!pSuper->IsWidget())
			return;

	// set the super
	AXGadget::SetParent(pSuper);

	// set the handle
	::SetParent(m_hwndRef, (m_pSuper)?((AXWidget *)m_pSuper)->m_hwndRef:(HWND)0);
}

// AXWidget: MakeKey: set a window as the key window
void AXWidget::MakeKey(void)
{
	::SetFocus(m_hwndRef);
}

// AXWidget: Show: set a window as the key window, and show it
void AXWidget::Show(void)
{
	::ShowWindow(m_hwndRef, SW_SHOW);
	::SetFocus(m_hwndRef);
}

// AXWidget: Hide: hide this window
void AXWidget::Hide(void)
{
	::ShowWindow(m_hwndRef, SW_HIDE);
}

// AXWidget: SetNeedsDisplay: mark a window as needing to be redrawn
void AXWidget::SetNeedsDisplay(void)
{
	::InvalidateRect(m_hwndRef, 0, TRUE);
}

// AXWidget: SetNeedsDisplay: mark a rectangle within a window as needing to be redrawn
void AXWidget::SetNeedsDisplay(int left, int top, int right, int bottom)
{
	// declarations
	RECT			rcArea;

	// fill the rectangle
	rcArea.left		= left;
	rcArea.top		= top;
	rcArea.right	= right;
	rcArea.bottom	= bottom;

	// invalidate
	::InvalidateRect(m_hwndRef, &rcArea, TRUE);
}

// AXWidget: IsKey: determine whether a window is the key window
bool AXWidget::IsKey(void)
{
	return (::GetFocus()==m_hwndRef)?true:false;
}

// AXWidget: Close: close a window
void AXWidget::Close(void)
{
	::DestroyWindow(m_hwndRef);
}

// AXWidget: IsClosed: determine whether a window is closed
bool AXWidget::IsClosed(void)
{
	return (::IsWindow(m_hwndRef))?true:false;
}

// AXWidget: SetLayout: change the layout
void AXWidget::SetLayout(int layout)
{
	m_layout = layout;
}

// AXWidget: GetLayout: retrieve the layout
int AXWidget::GetLayout(void)
{
	return m_layout;
}

// AXWidget: SetLayoutDistance: set the layout distances
void AXWidget::SetLayoutDistance(int left, int top, int right, int bottom)
{
	m_distance[0] = left;
	m_distance[1] = top;
	m_distance[2] = right;
	m_distance[3] = bottom;
}

// AXWidget: GetLayoutDistance: retrieve the layout distances
void AXWidget::GetLayoutDistance(int *pLeft, int *pTop, int *pRight, int *pBottom)
{
	if (pLeft)		*pLeft			= m_distance[0];
	if (pTop)		*pTop			= m_distance[1];
	if (pRight)		*pRight			= m_distance[2];
	if (pBottom)	*pBottom		= m_distance[3];
}

// AXWidget: QueryClientArea: retrieve client area information
void AXWidget::QueryClientArea(int *pLeft, int *pTop, int *pRight, int *pBottom)
{
	// declarations
	RECT	rcArea	= { 0, 0, 0, 0 };

	// retrieve the area of the widget
	::GetClientRect(m_hwndRef, &rcArea);

	// store the received area
	if (pLeft)		*pLeft			= rcArea.left;
	if (pTop)		*pTop			= rcArea.top;
	if (pRight)		*pRight			= rcArea.right;
	if (pBottom)	*pBottom		= rcArea.bottom;
}
