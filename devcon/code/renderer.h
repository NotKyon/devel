#ifndef RENDERER_H
#define RENDERER_H

#include "math.h"
#include "font.h"

extern color_t g_bgColor;


/*
 * =============================================================================
 *
 *	Renderer
 *
 * =============================================================================
 */

/*
 * Render a single frame.
 *
 * time: Number of seconds that have passed for smooth interpolation.
 */
void r_Frame(double time);

/*
 * Render the console.
 * NOTE: This function is called internally.
 */
void r_Console();

/*
 * Clear the frame buffer.
 */
void r_Clear();

/*
 * Swap buffers.
 */
void r_Flip();

/*
 * Draw a rectangle, immediately.
 * NOTE: GL immediate mode immediately.
 *
 * rct: Rectangle to draw. [0]=left, [1]=top, [2]=right, [3]=bottom.
 * color: Color to draw the rectangle. [0,1,2]=red, green, blue; [3]=alpha.
 */
void r_DrawRect(int rct[4], vec4_t color);

/*
 * =============================================================================
 *
 *	Viewport
 *
 * =============================================================================
 */

/*
 * Set the current offset of the viewport.
 *
 * x: Offset along the X-axis in screen space.
 * y: Offset along the Y-axis in screen space.
 */
void v_Offset(int x, int y);

/*
 * Resize the viewport.
 *
 * w: Extent along the X-axis (width) in screen space.
 * h: Extent along the Y-axis (height) in screen space.
 */
void v_Resize(int w, int h);

/*
 * Retrieve the current X-axis offset of the viewport.
 *
 * return: Offset along the X-axis in screen space.
 */
int v_X();

/*
 * Retrieve the current Y-axis offset of the viewport.
 *
 * return: Offset along the Y-axis in screen space.
 */
int v_Y();

/*
 * Retrieve the current width of the viewport.
 *
 * return: Extent along the X-axis (width) in screen space.
 */
int v_Width();

/*
 * Retrieve the current height of the viewport.
 *
 * return: Extent along the Y-axis (height) in screen space.
 */
int v_Height();

#endif
