// Define GB_INIT in your main source file (set it to 1)
#define GB_INIT 1
// Then include <gamebase.h> for the engine's API
#include <gamebase.h>

// GBMain() is your main entry point
void GBMain() {
	// gbPrint() writes text to stdout
	gbPrint("Hello, world!");
}
