#if _WIN32
# include <windows.h>
#endif
#include <GL/glfw.h>
#if __APPLE__
# include <OpenGL/gl.h>
#else
# include <GL/gl.h>
#endif

#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#if __amd64__ || _M_AMD64 || _M_X64 || __LP__
# define ARCH_BITS 64
#elif __x86__ || _M_IX86 || __arm__
# define ARCH_BITS 32
#else
# error "Did not detect bits."
#endif

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed long long s64;

#if ARCH_BITS==32
typedef u32 uint;
typedef s32 sint;
#elif ARCH_BITS==64
typedef u64 uint;
typedef s64 sint;
#else
# error "Error in source configuration; ARCH_BITS"
#endif

typedef float f32;
typedef double f64;

#if __GNUC__ || __clang__
# define __likely(x) (__builtin_expect(!!(x), 1))
# define __unlikely(x) (__builtin_expect(!!(x), 0))
#else
# define __likely(x) (x)
# define __unlikely(x) (x)
#endif

#if DEBUG || _DEBUG || __debug__
# undef _DEBUG
# define _DEBUG 1
#else
# ifndef NDEBUG
#  define NDEBUG 1
# endif
#endif

void handle_assert(const char *file, uint line, const char *func, const char *expr, const char *msg);
#undef assert
#if _DEBUG
# define assert_msg(x,m) if __unlikely(!(x)) { handle_assert(__FILE__,__LINE__,__func__, #x, (m)); }
# define assert(x) assert_msg((x),"Assert Failed!")
#else
# define assert_msg(x,m)
# define assert_msg(x)
#endif

//----------------------------------------------------------------------------------------------------------------------

#define MAX_CRITICAL_SECTIONS 1024
#define CONSOLE_LOCK_ID 0

double sys_curTime, sys_deltaTime;

#if _WIN32
CRITICAL_SECTION g_critSect[MAX_CRITICAL_SECTIONS];

void Sys_Init() {
	uint i;

	for(i=0; i<MAX_CRITICAL_SECTIONS; i++)
		InitializeCriticalSectionAndSpinCount(&g_critSect[i], 8192);
}

void Sys_EnterLock(uint i) {
	assert(i < MAX_CRITICAL_SECTIONS);

	EnterCriticalSection(&g_critSect[i]);
}
void Sys_LeaveLock(uint i) {
	assert(i < MAX_CRITICAL_SECTIONS);

	LeaveCriticalSection(&g_critSect[i]);
}
#else
# error "TODO: Write the POSIX equivalent."
#endif

#if _DEBUG
void handle_assert(const char *file, uint line, const char *func, const char *expr, const char *msg) {
	Sys_EnterLock(CONSOLE_LOCK_ID);

	if (file) {
		fprintf(stderr, "[%s", file);
		if (line)
			fprintf(stderr, "(%u)", (u32)line);
		if (func)
			fprintf(stderr, " %s", func);
	} else if(func)
		fprintf(stderr, "<%s> ", func);

	fprintf(stderr, "Error: ");
	fprintf(stderr, "%s\nExpression: %s\n\n", msg, expr);

	Sys_LeaveLock(CONSOLE_LOCK_ID);

	exit(EXIT_FAILURE);
}
#endif

//----------------------------------------------------------------------------------------------------------------------

float Degrees(float x) { return x/3.1415926535f*180.0f; }
float Radians(float x) { return x/180.0f*3.1415926535f; }

float Cos(float x) { return cosf(Radians(x)); }
float Sin(float x) { return sinf(Radians(x)); }
float Tan(float x) { return tanf(Radians(x)); }

float ACos(float x) { return Degrees(acosf(x)); }
float ASin(float x) { return Degrees(asinf(x)); }
float ATan(float x) { return Degrees(atanf(x)); }
float ATan2(float y, float x) { return Degrees(atan2f(y, x)); }

float Sqrt(float x) { return sqrtf(x); }
float InvSqrt(float x) { return 1.0f/sqrtf(x); }

/*
	M[ 0]=xx; M[ 4]=xy; M[ 8]=xz; M[12]=xw;
	M[ 1]=yx; M[ 5]=yy; M[ 9]=yz; M[13]=yw;
	M[ 2]=zx; M[ 6]=zy; M[10]=zz; M[14]=zw;
	M[ 3]=wx; M[ 7]=wy; M[11]=wz; M[15]=ww;
*/
void LoadIdentity(float *M) {
	M[ 0]=1; M[ 4]=0; M[ 8]=0; M[12]=0;
	M[ 1]=0; M[ 5]=1; M[ 9]=0; M[13]=0;
	M[ 2]=0; M[ 6]=0; M[10]=1; M[14]=0;
	M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
}
void LoadPerspective(float *M, float fov, float aspect, float zn, float zf) {
	float a, b, c, d;

	b = 1.0f/Tan(0.5f*fov);
	a = b/aspect;

	c =     zf/(zf - zn);
	d = -zn*zf/(zf - zn);

	M[ 0]=a; M[ 4]=0; M[ 8]=0; M[12]=0;
	M[ 1]=0; M[ 5]=b; M[ 9]=0; M[13]=0;
	M[ 2]=0; M[ 6]=0; M[10]=c; M[14]=d;
	M[ 3]=0; M[ 7]=0; M[11]=1; M[15]=0;
}
void LoadOrtho(float *M, float l, float r, float b, float t, float zn,
float zf) {
	float A, B, C, D, E, F;

	A =    2.0f/(r  -  l);
	B = (l + r)/(l  -  r);
	C =    2.0f/(t  -  b);
	D = (t + b)/(b  -  t);
	E =    1.0f/(zf - zn);
	F =      zn/(zn - zf);

	M[ 0]=A; M[ 4]=0; M[ 8]=0; M[12]=B;
	M[ 1]=0; M[ 5]=C; M[ 9]=0; M[13]=D;
	M[ 2]=0; M[ 6]=0; M[10]=E; M[14]=F;
	M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
}
void LoadTranslation(float *M, float x, float y, float z) {
	M[ 0]=1; M[ 4]=0; M[ 8]=0; M[12]=x;
	M[ 1]=0; M[ 5]=1; M[ 9]=0; M[13]=y;
	M[ 2]=0; M[ 6]=0; M[10]=1; M[14]=z;
	M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
}
void LoadRotation(float *M, float x, float y, float z) {
	float sx,sy,sz, cx,cy,cz;
	float cz_nsx;

	sx=Sin(x); sy=Sin(y); sz=Sin(z);
	cx=Cos(x); cy=Cos(y); cz=Cos(z);

	cz_nsx = cz*-sx;

	M[ 0]= cz*cy;              M[ 4]=-sz*cx;  M[ 8]= cz*-sy + (sz*sx)*cy;
	M[ 1]= sz*cy + cz_nsx*sy;  M[ 5]= cz*cx;  M[ 9]= sz*-sy + cz_nsx*cy;
	M[ 2]= cx*sy;              M[ 6]= sx;     M[10]= cx*cy;

	                           M[12]=0;
	                           M[13]=0;
	                           M[14]=0;
	M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
}
void LoadScaling(float *M, float x, float y, float z) {
	M[ 0]=x; M[ 4]=0; M[ 8]=0; M[12]=0;
	M[ 1]=0; M[ 5]=y; M[ 9]=0; M[13]=0;
	M[ 2]=0; M[ 6]=0; M[10]=z; M[14]=0;
	M[ 3]=0; M[ 7]=0; M[11]=0; M[15]=1;
}

void ApplyTranslation(float *M, float x, float y, float z) {
	M[12] += M[ 0]*x + M[ 4]*y + M[ 8]*z;
	M[13] += M[ 1]*x + M[ 5]*y + M[ 9]*z;
	M[14] += M[ 2]*x + M[ 6]*y + M[10]*z;
}
void ApplyXRotation(float *M, float x) {
	float c, s, t;

	c = Cos(x);
	s = Sin(x);

	t = M[ 4];
	M[ 4] = t*c  + M[ 8]*s;
	M[ 8] = t*-s + M[ 8]*c;

	t = M[ 5];
	M[ 5] = t*c  + M[ 9]*s;
	M[ 9] = t*-s + M[ 9]*c;

	t = M[ 6];
	M[ 6] = t*c  + M[10]*s;
	M[10] = t*-s + M[10]*c;
}
void ApplyYRotation(float *M, float y) {
	float c, s, t;

	c = Cos(y);
	s = Sin(y);

	t = M[ 0];
	M[ 0] = t*c  + M[ 8]*s;
	M[ 8] = t*-s + M[ 8]*c;

	t = M[ 1];
	M[ 1] = t*c  + M[ 9]*s;
	M[ 9] = t*-s + M[ 9]*c;

	t = M[ 2];
	M[ 2] = t*c  + M[10]*s;
	M[10] = t*-s + M[10]*c;
}
void ApplyZRotation(float *M, float z) {
	float c, s, t;

	c = Cos(z);
	s = Sin(z);

	t = M[ 0];
	M[ 0] = t*c  + M[ 4]*s;
	M[ 4] = t*-s + M[ 4]*c;

	t = M[ 1];
	M[ 1] = t*c  + M[ 5]*s;
	M[ 5] = t*-s + M[ 5]*c;

	t = M[ 2];
	M[ 2] = t*c  + M[ 6]*s;
	M[ 6] = t*-s + M[ 6]*c;
}
void ApplyRotation(float *M, float x, float y, float z) {
	ApplyZRotation(M, z);
	ApplyXRotation(M, x);
	ApplyYRotation(M, y);
}

void LoadAffineInverse(float *M, const float *P) {
	float x, y, z;

	x = -(P[12]*P[ 0] + P[13]*P[ 1] + P[14]*P[ 2]);
	y = -(P[12]*P[ 4] + P[13]*P[ 5] + P[14]*P[ 6]);
	z = -(P[12]*P[ 8] + P[13]*P[ 9] + P[14]*P[10]);

	M[ 0]=P[ 0]; M[ 4]=P[ 1]; M[ 8]=P[ 2]; M[12]=x;
	M[ 1]=P[ 4]; M[ 5]=P[ 5]; M[ 9]=P[ 6]; M[13]=y;
	M[ 2]=P[ 8]; M[ 6]=P[ 9]; M[10]=P[10]; M[14]=z;
	M[ 3]=   0 ; M[ 7]=   0 ; M[11]=   0 ; M[15]=1;
}
void AffineMultiply(float *M, const float *P, const float *Q) {
	M[ 0]=P[ 0]*Q[ 0] + P[ 4]*Q[ 1] + P[ 8]*Q[ 2];
	M[ 4]=P[ 0]*Q[ 4] + P[ 4]*Q[ 5] + P[ 8]*Q[ 6];
	M[ 8]=P[ 0]*Q[ 8] + P[ 4]*Q[ 9] + P[ 8]*Q[10];
	M[12]=P[ 0]*Q[12] + P[ 4]*Q[13] + P[ 8]*Q[14] + P[12];

	M[ 1]=P[ 1]*Q[ 0] + P[ 5]*Q[ 1] + P[ 9]*Q[ 2];
	M[ 5]=P[ 1]*Q[ 4] + P[ 5]*Q[ 5] + P[ 9]*Q[ 6];
	M[ 9]=P[ 1]*Q[ 8] + P[ 5]*Q[ 9] + P[ 9]*Q[10];
	M[13]=P[ 1]*Q[12] + P[ 5]*Q[13] + P[ 9]*Q[14] + P[13];

	M[ 2]=P[ 2]*Q[ 0] + P[ 6]*Q[ 1] + P[10]*Q[ 2];
	M[ 6]=P[ 2]*Q[ 4] + P[ 6]*Q[ 5] + P[10]*Q[ 6];
	M[10]=P[ 2]*Q[ 8] + P[ 6]*Q[ 9] + P[10]*Q[10];
	M[14]=P[ 2]*Q[12] + P[ 6]*Q[13] + P[10]*Q[14] + P[14];

	M[ 3]=0;
	M[ 7]=0;
	M[11]=0;
	M[15]=1;
}
void Multiply3(float *M, const float *P, const float *Q) {
	M[ 0]=P[ 0]*Q[ 0] + P[ 4]*Q[ 1] + P[ 8]*Q[ 2];
	M[ 4]=P[ 0]*Q[ 4] + P[ 4]*Q[ 5] + P[ 8]*Q[ 6];
	M[ 8]=P[ 0]*Q[ 8] + P[ 4]*Q[ 9] + P[ 8]*Q[10];
	M[12]=0;

	M[ 1]=P[ 1]*Q[ 0] + P[ 5]*Q[ 1] + P[ 9]*Q[ 2];
	M[ 5]=P[ 1]*Q[ 4] + P[ 5]*Q[ 5] + P[ 9]*Q[ 6];
	M[ 9]=P[ 1]*Q[ 8] + P[ 5]*Q[ 9] + P[ 9]*Q[10];
	M[13]=0;

	M[ 2]=P[ 2]*Q[ 0] + P[ 6]*Q[ 1] + P[10]*Q[ 2];
	M[ 6]=P[ 2]*Q[ 4] + P[ 6]*Q[ 5] + P[10]*Q[ 6];
	M[10]=P[ 2]*Q[ 8] + P[ 6]*Q[ 9] + P[10]*Q[10];
	M[14]=0;

	M[ 3]=0;
	M[ 7]=0;
	M[11]=0;
	M[15]=1;
}
void Multiply4(float *M, const float *P, const float *Q) {
	M[ 0]=P[ 0]*Q[ 0] + P[ 4]*Q[ 1] + P[ 8]*Q[ 2] + P[12]*Q[ 3];
	M[ 4]=P[ 0]*Q[ 4] + P[ 4]*Q[ 5] + P[ 8]*Q[ 6] + P[12]*Q[ 7];
	M[ 8]=P[ 0]*Q[ 8] + P[ 4]*Q[ 9] + P[ 8]*Q[10] + P[12]*Q[11];
	M[12]=P[ 0]*Q[12] + P[ 4]*Q[13] + P[ 8]*Q[14] + P[12]*Q[15];

	M[ 1]=P[ 1]*Q[ 0] + P[ 5]*Q[ 1] + P[ 9]*Q[ 2] + P[13]*Q[ 3];
	M[ 5]=P[ 1]*Q[ 4] + P[ 5]*Q[ 5] + P[ 9]*Q[ 6] + P[13]*Q[ 7];
	M[ 9]=P[ 1]*Q[ 8] + P[ 5]*Q[ 9] + P[ 9]*Q[10] + P[13]*Q[11];
	M[13]=P[ 1]*Q[12] + P[ 5]*Q[13] + P[ 9]*Q[14] + P[13]*Q[15];

	M[ 2]=P[ 2]*Q[ 0] + P[ 6]*Q[ 1] + P[10]*Q[ 2] + P[14]*Q[ 3];
	M[ 6]=P[ 2]*Q[ 4] + P[ 6]*Q[ 5] + P[10]*Q[ 6] + P[14]*Q[ 7];
	M[10]=P[ 2]*Q[ 8] + P[ 6]*Q[ 9] + P[10]*Q[10] + P[14]*Q[11];
	M[14]=P[ 2]*Q[12] + P[ 6]*Q[13] + P[10]*Q[14] + P[14]*Q[15];

	M[ 3]=P[ 3]*Q[ 0] + P[ 7]*Q[ 1] + P[11]*Q[ 2] + P[15]*Q[ 3];
	M[ 7]=P[ 3]*Q[ 4] + P[ 7]*Q[ 5] + P[11]*Q[ 6] + P[15]*Q[ 7];
	M[11]=P[ 3]*Q[ 8] + P[ 7]*Q[ 9] + P[11]*Q[10] + P[15]*Q[11];
	M[15]=P[ 3]*Q[12] + P[ 7]*Q[13] + P[11]*Q[14] + P[15]*Q[15];
}

//----------------------------------------------------------------------------------------------------------------------

void Com_Errorf(const char *fmt, ...) {
	va_list args;

	Sys_EnterLock(CONSOLE_LOCK_ID);

	fprintf(stderr, "Error: ");
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fprintf(stderr, "\n");

	Sys_LeaveLock(CONSOLE_LOCK_ID);
}

void R_Frame();
void Com_Frame() {
	R_Frame();
}

//----------------------------------------------------------------------------------------------------------------------

void R_Frame() {
	glClearColor(0.1f,0.3f,0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glfwSwapBuffers();
}

//----------------------------------------------------------------------------------------------------------------------

int GameMain(int argc, char **argv) {
	double p;

	Sys_Init();

	if (!glfwInit()) {
		Com_Errorf("glfwInit() failed");
		return EXIT_FAILURE;
	}

	if (!glfwOpenWindow(640, 480, 8,8,8,8, 24,8, GLFW_WINDOW)) {
		Com_Errorf("glfwOpenWindow() failed");
		return EXIT_FAILURE;
	}

	glfwSetWindowTitle("Rogue Arena");

	while(glfwGetWindowParam(GLFW_OPENED)) {
		p = sys_curTime;
		sys_curTime = glfwGetTime();
		sys_deltaTime = sys_curTime - p;

		Com_Frame();
	}

	return EXIT_SUCCESS;
}

//----------------------------------------------------------------------------------------------------------------------

int main(int argc, char **argv) { return GameMain(argc, argv); }
#if _WIN32
int WINAPI WinMain(HINSTANCE a, HINSTANCE b, LPSTR c, int d) { if(a||b||c||d){} return GameMain(__argc, __argv); }
#endif
