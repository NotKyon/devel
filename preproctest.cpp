// this file is used only to test the C preprocessor
#define INC include

//#INC <stdio.h>
//#INC <stdlib.h>
#include <stdio.h>
#include <stdlib.h>

#define DEF define
//#DEF MACRO(A) "Hello, " A "!"
#define MACRO(A) "Hello, " A "!"

int main()
{
	printf( "%s\n", MACRO( "world" ) );
	return EXIT_SUCCESS;
}
