#ifndef TASKSHARE_TIMING_H
#define TASKSHARE_TIMING_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

#if !defined(_WIN32)
# include <sys/time.h>
# include <ctime>
#endif

#include <cstdio>

//------------------------------------------------------------------------------
// TIMING

__TS_NS_INIT__

typedef long double f80;
typedef struct {
	f80 s, e;
} perfQuery_t;

inline f80 SampleTimer() {
#if _WIN32
# if 1
	u64 t, f;

	QueryPerformanceCounter((LARGE_INTEGER *)&t);
	QueryPerformanceFrequency((LARGE_INTEGER *)&f);

	return ((f80)t)/((f80)f);
# else
	DWORD t;

	t = GetTickCount();

	return ((f80)t)/((f80)1000);
# endif
#else
# if 0
	//
	//	NOTE: Documentation is inconsistent for hrtime_t cims.nyu.edu claims it
	//	      has a resolution of one nanosecond while cs.uccs.edu claims it
	//	      depends on the hardware. This portion of the code has been
	//	      disabled as a result of the inconsistency. It can be renabled by
	//	      changing the '0' above to a '1'. If you get compilation errors,
	//	      add <rtl_time.h> to the includes above.
	//
	//http://cims.nyu.edu/cgi-systems/man.cgi?section=3C&topic=gethrtime
	//http://cs.uccs.edu/~chow/pub/rtl/doc/html/MAN/gethrtime.3.html
	hrtime_t t;

	t = gethrtime();
	return ((f80)t)/((f80)1000000000);
# elif 1
	struct timeval tv;

	gettimeofday(&tv, NULL);
	return ((f80)tv.tv_sec) + ((f80)tv.tv_usec)/((f80)1000000000);
# else
	clock_t t;

	t = clock();
	return ((f80)t)/((f80)CLOCKS_PER_SEC);
# endif
#endif
}

inline void InitPerfQuery(perfQuery_t &pq) {
	pq.s = SampleTimer();
}
inline void FiniPerfQuery(perfQuery_t &pq) {
	f80 elapsed;

	pq.e = SampleTimer();

	elapsed = pq.e - pq.s;

#if _WIN32
	printf("%.3f\n", (double)elapsed);
#else
	printf("%.3Lf\n", elapsed);
#endif
}

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
