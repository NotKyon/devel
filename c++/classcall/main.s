	.file	"main.cpp"
	.intel_syntax noprefix
	.text
.globl __Z4MainP7MyClass
	.def	__Z4MainP7MyClass;	.scl	2;	.type	32;	.endef
__Z4MainP7MyClass:
LFB0:
	push	ebp
LCFI0:
	mov	ebp, esp
LCFI1:
	sub	esp, 24
LCFI2:
	mov	eax, DWORD PTR [ebp+8]
	mov	DWORD PTR [esp], eax
	call	__ZN7MyClass11DoSomethingEv
	leave
LCFI3:
	ret
LFE0:
	.section	.eh_frame,"w"
Lframe1:
	.long	LECIE1-LSCIE1
LSCIE1:
	.long	0x0
	.byte	0x1
	.ascii "\0"
	.uleb128 0x1
	.sleb128 -4
	.byte	0x8
	.byte	0xc
	.uleb128 0x4
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x1
	.align 4
LECIE1:
LSFDE1:
	.long	LEFDE1-LASFDE1
LASFDE1:
	.long	LASFDE1-Lframe1
	.long	LFB0
	.long	LFE0-LFB0
	.byte	0x4
	.long	LCFI0-LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x85
	.uleb128 0x2
	.byte	0x4
	.long	LCFI1-LCFI0
	.byte	0xd
	.uleb128 0x5
	.byte	0x4
	.long	LCFI3-LCFI1
	.byte	0xc5
	.byte	0xc
	.uleb128 0x4
	.uleb128 0x4
	.align 4
LEFDE1:
	.def	__ZN7MyClass11DoSomethingEv;	.scl	2;	.type	32;	.endef
