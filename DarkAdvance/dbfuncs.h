#ifndef DBFUNCS_H
#define DBFUNCS_H

#include "matrix.h"
#include "simplify.h"
#include "globstruct.h"

#define DBEXPORT __declspec(dllexport)

extern GlobStruct *gGlob;

inline int dbFloat(float x) {
	union { float f; int i; } v;

	v.f = x;
	return v.i;
}

inline char *dbString(char *oldstr, const char *newstr) {
	size_t l;
	char *p;

	p = (char *)0;

	if (oldstr)
		gGlob->CreateDeleteString((DWORD *)&oldstr, 0);

	if (!newstr)
		return (char *)0;

	l = strlen(newstr)+1;

	gGlob->CreateDeleteString((DWORD *)&p, l);
	memcpy((void *)p, (const void *)newstr, l);

	return p;
}

// dbfuncs.cpp
DBEXPORT void Constructor();
DBEXPORT void Destructor();
/*
DBEXPORT void PreDestructor();
*/
DBEXPORT void ReceiveCoreDataPtr(void *p);
/*
DBEXPORT int GetAssociatedDLLs();
DBEXPORT int GetNumDependencies();
DBEXPORT const char *GetDependencyID(int i);
DBEXPORT void ReceiveDependenciesHinstance(char *dllname, HINSTANCE hinst);
DBEXPORT void D3DDeviceLost();
DBEXPORT void D3DDeviceNotReset();
*/

// fastmath.cpp
EXPORT int dbFloatSign(float x);
EXPORT int dbFloatAbs(float x);
EXPORT int dbSqrtFast(float x);
EXPORT int dbInvSqrtFast(float x);
EXPORT int dbCopySign(float a, float b);

// vector.cpp
EXPORT int dbVec3_Dot(const vec3_t *a, const vec3_t *b);
EXPORT int dbVec3_LengthSq(const vec3_t *src);
EXPORT int dbVec3_Length(const vec3_t *src);
EXPORT int dbVec3_LengthFast(const vec3_t *src);
EXPORT int dbVec3_InvLengthFast(const vec3_t *src);
EXPORT int dbVec3_Distance(const vec3_t *a, const vec3_t *b);
EXPORT int dbVec3_DistanceFast(const vec3_t *a, const vec3_t *b);
EXPORT int dbVec4_Dot(const vec4_t *a, const vec4_t *b);
EXPORT int dbVec4_LengthSq(const vec4_t *src);
EXPORT int dbVec4_Length(const vec4_t *src);
EXPORT int dbVec4_LengthFast(const vec4_t *src);
EXPORT int dbVec4_InvLengthFast(const vec4_t *src);
EXPORT int dbVec4_Distance(const vec4_t *a, const vec4_t *b);
EXPORT int dbVec4_DistanceFast(const vec4_t *a, const vec4_t *b);

// quaternion.cpp
EXPORT int dbQuat_Dot(const quat_t *a, const quat_t *b);
EXPORT int dbQuat_LengthSq(const quat_t *src);
EXPORT int dbQuat_Length(const quat_t *src);
EXPORT int dbQuat_LengthFast(const quat_t *src);
EXPORT int dbQuat_InvLengthFast(const quat_t *src);

// vectorsimplify.cpp
EXPORT int dbVec_X(const vec3_t *src);
EXPORT int dbVec_Y(const vec3_t *src);
EXPORT int dbVec_Z(const vec3_t *src);
EXPORT int dbVec_W(const vec4_t *src);

// matrixsimplify.cpp
EXPORT int dbMatrix_GetElement(const matrix_t *src, unsigned int r, unsigned int c);
EXPORT int dbMatrix_11(const matrix_t *src);
EXPORT int dbMatrix_12(const matrix_t *src);
EXPORT int dbMatrix_13(const matrix_t *src);
EXPORT int dbMatrix_14(const matrix_t *src);
EXPORT int dbMatrix_21(const matrix_t *src);
EXPORT int dbMatrix_22(const matrix_t *src);
EXPORT int dbMatrix_23(const matrix_t *src);
EXPORT int dbMatrix_24(const matrix_t *src);
EXPORT int dbMatrix_31(const matrix_t *src);
EXPORT int dbMatrix_32(const matrix_t *src);
EXPORT int dbMatrix_33(const matrix_t *src);
EXPORT int dbMatrix_34(const matrix_t *src);
EXPORT int dbMatrix_41(const matrix_t *src);
EXPORT int dbMatrix_42(const matrix_t *src);
EXPORT int dbMatrix_43(const matrix_t *src);
EXPORT int dbMatrix_44(const matrix_t *src);

// quaternionsimplify.cpp
EXPORT int dbQuat_W(const quat_t *src);
EXPORT int dbQuat_X(const quat_t *src);
EXPORT int dbQuat_Y(const quat_t *src);
EXPORT int dbQuat_Z(const quat_t *src);

// entity.cpp
EXPORT char *dbGetEntityName(char *oldstr, entity_t ent);
EXPORT char *dbGetEntityText(char *oldstr, entity_t ent);

// entitysimplify.cpp
EXPORT int dbEntityPositionX(entity_t ent);
EXPORT int dbEntityPositionY(entity_t ent);
EXPORT int dbEntityPositionZ(entity_t ent);
EXPORT int dbEntityOffsetPositionX(entity_t ent);
EXPORT int dbEntityOffsetPositionY(entity_t ent);
EXPORT int dbEntityOffsetPositionZ(entity_t ent);
EXPORT int dbEntityGlobalPositionX(entity_t ent);
EXPORT int dbEntityGlobalPositionY(entity_t ent);
EXPORT int dbEntityGlobalPositionZ(entity_t ent);
EXPORT int dbEntityPostPositionX(entity_t ent);
EXPORT int dbEntityPostPositionY(entity_t ent);
EXPORT int dbEntityPostPositionZ(entity_t ent);
EXPORT int dbCalculateEntityScaleX(entity_t ent);
EXPORT int dbCalculateEntityScaleY(entity_t ent);
EXPORT int dbCalculateEntityScaleZ(entity_t ent);
EXPORT int dbCalculateEntityOffsetScaleX(entity_t ent);
EXPORT int dbCalculateEntityOffsetScaleY(entity_t ent);
EXPORT int dbCalculateEntityOffsetScaleZ(entity_t ent);
EXPORT int dbCalculateEntityGlobalScaleX(entity_t ent);
EXPORT int dbCalculateEntityGlobalScaleY(entity_t ent);
EXPORT int dbCalculateEntityGlobalScaleZ(entity_t ent);
EXPORT int dbCalculateEntityPostScaleX(entity_t ent);
EXPORT int dbCalculateEntityPostScaleY(entity_t ent);
EXPORT int dbCalculateEntityPostScaleZ(entity_t ent);
EXPORT int dbEntityRotationX(entity_t ent);
EXPORT int dbEntityRotationY(entity_t ent);
EXPORT int dbEntityRotationZ(entity_t ent);
EXPORT int dbEntityOffsetRotationX(entity_t ent);
EXPORT int dbEntityOffsetRotationY(entity_t ent);
EXPORT int dbEntityOffsetRotationZ(entity_t ent);
EXPORT int dbEntityGlobalRotationX(entity_t ent);
EXPORT int dbEntityGlobalRotationY(entity_t ent);
EXPORT int dbEntityGlobalRotationZ(entity_t ent);
EXPORT int dbEntityPostRotationX(entity_t ent);
EXPORT int dbEntityPostRotationY(entity_t ent);
EXPORT int dbEntityPostRotationZ(entity_t ent);

#endif
