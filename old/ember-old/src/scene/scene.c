#include <ember/scene.h>

static int __scene_init( scene_t scene )
{
	scene->data[ 0 ] = ( void * )0;
	scene->data[ 1 ] = ( void * )0;

	scene->head = ( entity_t )0;
	scene->tail = ( entity_t )0;

	scene->next = ( scene_t )0;
	if( ( scene->prev = g_scene_tail ) != ( scene_t )0 )
		g_scene_tail->next = scene;
	else
		g_scene_head = scene;
	g_scene_tail     = scene;

	return 1;
}
static void __scene_deinit( scene_t scene )
{
	entity_t ent, next;

	ent = scene->head;
	while( ent ) {
		next = ent->next;
		gc_dealloc( ent );

		ent = next;
	}

	if( scene->prev )
		scene->prev->next = scene->next;
	if( scene->next )
		scene->next->prev = scene->prev;

	if( g_scene_head == scene )
		g_scene_head = scene->next;
	if( g_scene_tail == scene )
		g_scene_tail = scene->prev;
}

int sce_init( void )
{
	g_scene_pool =
	    gc_register_pool( sizeof( struct stScene ), ( gc_init_t )__scene_init,
	        ( gc_deinit_t )__scene_deinit );
	if( !g_scene_pool )
		return 0;
	return 1;
}
void sce_deinit( void )
{
	gc_unregister_pool( g_scene_pool );
}

scene_t sce_alloc( void )
{
	return ( scene_t )gc_alloc( g_scene_pool );
}
void sce_dealloc( scene_t scene )
{
	gc_dealloc( ( void * )scene );
}

scene_t sce_first()
{
	return g_scene_head;
}
scene_t sce_last()
{
	return g_scene_tail;
}
scene_t sce_before( scene_t scene )
{
	return scene->prev;
}
scene_t sce_after( scene_t scene )
{
	return scene->next;
}

entity_t sce_first_ent( scene_t scene )
{
	return scene ? scene->head : g_scene_head->head;
}
entity_t sce_last_ent( scene_t scene )
{
	return scene ? scene->tail : g_scene_head->tail;
}

void sce_set_data( scene_t scene, void *data )
{
	( scene ? scene : g_scene_head )->data[ 0 ] = data;
}
void *sce_data( scene_t scene )
{
	return ( scene ? scene : g_scene_head )->data[ 0 ];
}

void sce_set_sys_data( scene_t scene, void *data )
{
	( scene ? scene : g_scene_head )->data[ 1 ] = data;
}
void *sce_sys_data( scene_t scene )
{
	return ( scene ? scene : g_scene_head )->data[ 1 ];
}
