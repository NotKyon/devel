#ifndef EMBER_BTREE_H
#define EMBER_BTREE_H

#include "config.h"

typedef struct BTreeNode {
	int key;
	void *obj;

	struct BTreeNode *prnt;
	struct BTreeNode *left,
	    *rght;

	struct BTreeNode *prev,
	    *next;

	struct BTreeBase *base;
} BTreeNode;

typedef struct BTreeBase {
	struct BTreeNode *root;

	struct BTreeNode *head,
	    *tail;
} BTreeBase;

extern void btree_init( BTreeBase *base );
extern void btree_remove_all( BTreeBase *base );
extern BTreeNode *btree_find( BTreeBase *base, int key, BTreeNode *create );
extern void btree_remove( BTreeNode *node );
extern void btree_set( BTreeNode *node, void *obj );
extern void *btree_get( BTreeNode *node );

#endif
