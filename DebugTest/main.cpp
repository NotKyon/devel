#include <stdio.h>
#include <stdlib.h>

int g_var = 0;

int main() {
	int i, j;

	j = g_var;
	for(i=0; i<14; i++)
		j = j*(i + 1) + (3*rand()%(20 - i))/2;

	g_var = j;
	printf("%i\n", g_var);

	return EXIT_SUCCESS;
}
