#include <stdio.h>

int cpucount() {
#if __i386__||__i386||i386 || __x86_64__||__x86_64||x86_64
  int r;
  __asm("movl $1,%%eax\n\tcpuid\n\tmovl %%eax,%0" : "=r" (r));
  return (r>>16)&0xFF;
#else
# error "Please port this routine."
#endif
}

int main() {
  printf("cpucount() = %i\n", cpucount());
  return 0;
}

