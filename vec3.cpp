#include <cstdio>
#include "vec3.h"

int main() {
	fvec3 a(5, 5, 5), b(10, 10, 10);

	printf("Hello, world!\n");

	printf("dot((%f,%f,%f), (%f,%f,%f))=%f\n", a[0],a[1],a[2], b.x,b.y,b.z,
		dot(a, b));
	printf("distance = %f\n", distance(a, b));

	return 0;
}
