#include "window.h"

const unsigned kDefaultWindowResX = 1280;
const unsigned kDefaultWindowResY = 720;

struct WM_s
{
	HWND hWindow;
	ATOM WindowClass;
	unsigned uResX, uResY;

	static WM_s &GetInstance();

private:
	WM_s()
	: hWindow( (HWND)0 )
	, WindowClass( (ATOM)0 )
	, uResX( 0 )
	, uResY( 0 )
	{
	}
	~WM_s()
	{
		FiniWindow();
	}
};

WM_s &WM_s::GetInstance()
{
	static WM_s instance;
	return instance;
}

#define WM WM_s::GetInstance()

static LRESULT CALLBACK MsgProc_f( HWND hWnd, UINT uMsg, WPARAM wParm, LPARAM lParm )
{
	switch( uMsg )
	{
	case WM_CLOSE:
		DestroyWindow( hWnd );
		return 0;

	case WM_DESTROY:
		PostQuitMessage( 0 );
		return 0;

	case WM_SIZE:
		WM.uResX = LOWORD(wParm);
		WM.uResY = HIWORD(wParm);
		return 0;

	case WM_ERASEBKGND:
		return 0;

	default:
		break;
	}

	return DefWindowProcW( hWnd, uMsg, wParm, lParm );
}

static bool InitWindowClass_Win32()
{
	WNDCLASSEXW wc;

	if( WM.WindowClass != (ATOM)0 ) {
		return true;
	}

	wc.cbSize = sizeof(wc);
	wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = &MsgProc_f;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = GetModuleHandleW((LPCWSTR)0);
	wc.hIcon = LoadIconW(wc.hInstance, (LPCWSTR)1);
	wc.hCursor = LoadCursorW((HINSTANCE)0, (LPCWSTR)IDC_ARROW);
	wc.hbrBackground = (HBRUSH)0;
	wc.lpszMenuName = (LPCWSTR)0;
	wc.lpszClassName = L"Window";
	wc.hIconSm = LoadIconW(wc.hInstance, (LPCWSTR)1);

	WM.WindowClass = RegisterClassExW(&wc);
	if (!WM.WindowClass) {
		return false;
	}

	return true;
}
static void FiniWindowClass_Win32()
{
	if( !WM.WindowClass ) {
		return;
	}

	UnregisterClassW( (LPCWSTR)WM.WindowClass, GetModuleHandleW( (LPCWSTR)0 ) );
	WM.WindowClass = (ATOM)0;
}

static bool InitWindow_Win32()
{
	if( WM.hWindow != (HWND)0 ) {
		return true;
	}

	DWORD dwExstyle = 0;
	DWORD dwStyle = WS_OVERLAPPEDWINDOW;

	const LONG uWResX = (LONG)kDefaultWindowResX;
	const LONG uWResY = (LONG)kDefaultWindowResY;

	const LONG uSResX = GetSystemMetrics( SM_CXSCREEN );
	const LONG uSResY = GetSystemMetrics( SM_CYSCREEN );

	RECT rcArea = {
		uSResX/2 - uWResX/2,
		uSResY/2 - uWResY/2,
		uSResX/2 + uWResX/2 + uWResX%2,
		uSResY/2 + uWResY/2 + uWResY%2
	};

	if( !AdjustWindowRectEx( &rcArea, dwStyle, FALSE, dwExstyle ) ) {
		return false;
	}

	WM.hWindow =
		CreateWindowExW
		(
			dwExstyle,
			L"Window",
			L"Test Window",
			dwStyle,
			rcArea.left,
			rcArea.top,
			rcArea.right - rcArea.left,
			rcArea.bottom - rcArea.top,
			(HWND)0,
			(HMENU)0,
			GetModuleHandleW((LPCWSTR)0),
			(LPVOID)0
		);
	if( !WM.hWindow ) {
		return false;
	}

	UpdateWindow( WM.hWindow );
	ShowWindow( WM.hWindow, SW_SHOW );

	RECT rcClient;
	if( GetClientRect( WM.hWindow, &rcClient ) ) {
		WM.uResX = rcClient.right;
		WM.uResY = rcClient.bottom;
	}

	return true;
}
static void FiniWindow_Win32()
{
	if( !WM.hWindow ) {
		return;
	}

	DestroyWindow( WM.hWindow );
	WM.hWindow = (HWND)0;
}

bool InitWindow()
{
	bool r = true;

	r = r && InitWindowClass_Win32();
	r = r && InitWindow_Win32();

	return r;
}
void FiniWindow()
{
	FiniWindow_Win32();
	FiniWindowClass_Win32();
}

#include <stdio.h>
unsigned GetWindowResX()
{
	printf( "RX:%u ", WM.uResX );
	return WM.uResX;
}
unsigned GetWindowResY()
{
	printf( "RY:%u\n", WM.uResY );
	return WM.uResY;
}

bool ProcessWindowEvents()
{
	bool bRunning = true;
	MSG Msg;

	while( PeekMessageW( &Msg, (HWND)0, 0, 0, PM_REMOVE ) ) {
		if( Msg.message == WM_QUIT ) {
			bRunning = false;
		}

		TranslateMessage( &Msg );
		DispatchMessageW( &Msg );
	}

	return bRunning && WM.hWindow != (HWND)0;
}

HWND GetWindowHandle_Win32()
{
	return WM.hWindow;
}
