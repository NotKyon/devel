#ifndef GB_CONFIG_H
#define GB_CONFIG_H

#if _MSC_VER > 1000
# pragma once
#endif

#include <stdarg.h>
#include <stddef.h>

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Basic Macros */
#ifndef GB_INLINE
# if __cplusplus
#  define GB_INLINE inline
#  define GB_HAS_INLINE 1
# else
#  if _MSC_VER||__INTEL_COMPILER||__GNUC__||__clang__
#   define GB_INLINE static __inline
#   define GB_HAS_INLINE 1
#  else
#   define GB_INLINE static
#   define GB_HAS_INLINE 0
#  endif
# endif
#elif !defined(GB_HAS_INLINE)
# define GB_HAS_INLINE 1
#endif

#ifndef GB_FORCEINLINE
# if __GNUC__ || __clang__
#  if __cplusplus
#   define GB_FORCEINLINE inline __attribute__((always_inline))
#  else
#   define GB_FORCEINLINE static __inline __attribute__((always_inline))
#  endif
#  define GB_HAS_FORCEINLINE 1
# elif _MSC_VER || __INTEL_COMPILER
#  define GB_FORCEINLINE __forceinline
#  define GB_HAS_FORCEINLINE 1
# else
#  define GB_FORCEINLINE GB_INLINE
#  define GB_HAS_FORCEINLINE 0
# endif
#elif !defined(GB_HAS_FORCEINLINE)
# define GB_HAS_FORCEINLINE 1
#endif

#ifndef GB_HAS_LIKELY_UNLIKELY
# if __GNUC__ || __clang__ || __INTEL_COMPILER
#  define GB_LIKELY(x) __builtin_expect((x), 1)
#  define GB_UNLIKELY(x) __builtin_expect((x), 0)
#  define GB_HAS_LIKELY_UNLIKELY 1
# else
#  define GB_LIKELY(x) (x)
#  define GB_UNLIKELY(x) (x)
#  define GB_HAS_LIKELY_UNLIKELY 0
# endif
#endif

/* Basic Typedefs */
#define GB_TRUE 1
#define GB_FALSE 0
typedef int gb_bool_t;
typedef void(*gb_fnPtr_t)();

#if _MSC_VER || __INTEL_COMPILER
typedef   signed __int8         gb_i8_t ;
typedef   signed __int16        gb_i16_t;
typedef   signed __int32        gb_i32_t;
typedef   signed __int64        gb_i64_t;
typedef unsigned __int8         gb_u8_t ;
typedef unsigned __int16        gb_u16_t;
typedef unsigned __int32        gb_u32_t;
typedef unsigned __int64        gb_u64_t;
#else
typedef   signed char           gb_i8_t ;
typedef   signed short          gb_i16_t;
typedef   signed int            gb_i32_t;
typedef   signed long long int  gb_i64_t;
typedef unsigned char           gb_u8_t ;
typedef unsigned short          gb_u16_t;
typedef unsigned int            gb_u32_t;
typedef unsigned long long int  gb_u64_t;
#endif

typedef   signed int gb_sint_t;
typedef unsigned int gb_uint_t;

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
