﻿#include <Windows.h>
#include <Gdiplus.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#define AXTIME_IMPLEMENTATION
#include "axlib/ax_time.h"

PBITMAPINFO CreateBitmapInfoStruct(HWND hwnd, HBITMAP hBmp)
{ 
    BITMAP bmp; 
    PBITMAPINFO pbmi; 
    WORD    cClrBits; 

    // Retrieve the bitmap color format, width, and height.  
    if (!GetObject(hBmp, sizeof(BITMAP), (LPSTR)&bmp)) 
        return NULL;

    // Convert the color format to a count of bits.  
    cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel); 
    if (cClrBits == 1) 
        cClrBits = 1; 
    else if (cClrBits <= 4) 
        cClrBits = 4; 
    else if (cClrBits <= 8) 
        cClrBits = 8; 
    else if (cClrBits <= 16) 
        cClrBits = 16; 
    else if (cClrBits <= 24) 
        cClrBits = 24; 
    else cClrBits = 32; 

    // Allocate memory for the BITMAPINFO structure. (This structure  
    // contains a BITMAPINFOHEADER structure and an array of RGBQUAD  
    // data structures.)  

     if (cClrBits < 24) 
         pbmi = (PBITMAPINFO) LocalAlloc(LPTR, 
                    sizeof(BITMAPINFOHEADER) + 
                    sizeof(RGBQUAD) * (1<< cClrBits)); 

     // There is no RGBQUAD array for these formats: 24-bit-per-pixel or 32-bit-per-pixel 

     else 
         pbmi = (PBITMAPINFO) LocalAlloc(LPTR, 
                    sizeof(BITMAPINFOHEADER)); 

    // Initialize the fields in the BITMAPINFO structure.  

    pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
    pbmi->bmiHeader.biWidth = bmp.bmWidth; 
    pbmi->bmiHeader.biHeight = bmp.bmHeight; 
    pbmi->bmiHeader.biPlanes = bmp.bmPlanes; 
    pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel; 
    if (cClrBits < 24) 
        pbmi->bmiHeader.biClrUsed = (1<<cClrBits); 

    // If the bitmap is not compressed, set the BI_RGB flag.  
    pbmi->bmiHeader.biCompression = BI_RGB; 

    // Compute the number of bytes in the array of color  
    // indices and store the result in biSizeImage.  
    // The width must be DWORD aligned unless the bitmap is RLE 
    // compressed. 
    pbmi->bmiHeader.biSizeImage = ((pbmi->bmiHeader.biWidth * cClrBits +31) & ~31) /8
                                  * pbmi->bmiHeader.biHeight; 
    // Set biClrImportant to 0, indicating that all of the  
    // device colors are important.  
     pbmi->bmiHeader.biClrImportant = 0; 
     return pbmi; 
 } 
void CreateBMPFile(HWND hwnd, LPWSTR pszFile, PBITMAPINFO pbi, 
                  HBITMAP hBMP, HDC hDC) 
 { 
     HANDLE hf;                 // file handle  
    BITMAPFILEHEADER hdr;       // bitmap file-header  
    PBITMAPINFOHEADER pbih;     // bitmap info-header  
    LPBYTE lpBits;              // memory pointer  
    DWORD dwTotal;              // total count of bytes  
    DWORD cb;                   // incremental count of bytes  
    BYTE *hp;                   // byte pointer  
    DWORD dwTmp; 

    pbih = (PBITMAPINFOHEADER) pbi; 
    lpBits = (LPBYTE) GlobalAlloc(GMEM_FIXED, pbih->biSizeImage);

    if (!lpBits) 
		return;

    // Retrieve the color table (RGBQUAD array) and the bits  
    // (array of palette indices) from the DIB.  
    if (!GetDIBits(hDC, hBMP, 0, (WORD) pbih->biHeight, lpBits, pbi, 
        DIB_RGB_COLORS)) 
    {
		return;
    }

    // Create the .BMP file.  
    hf = CreateFileW(pszFile, 
                   GENERIC_READ | GENERIC_WRITE, 
                   (DWORD) 0, 
                    NULL, 
                   CREATE_ALWAYS, 
                   FILE_ATTRIBUTE_NORMAL, 
                   (HANDLE) NULL); 
    if (hf == INVALID_HANDLE_VALUE) 
		return;
    hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M"  
    // Compute the size of the entire file.  
    hdr.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + 
                 pbih->biSize + pbih->biClrUsed 
                 * sizeof(RGBQUAD) + pbih->biSizeImage); 
    hdr.bfReserved1 = 0; 
    hdr.bfReserved2 = 0; 

    // Compute the offset to the array of color indices.  
    hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) + 
                    pbih->biSize + pbih->biClrUsed 
                    * sizeof (RGBQUAD); 

    // Copy the BITMAPFILEHEADER into the .BMP file.  
    if (!WriteFile(hf, (LPVOID) &hdr, sizeof(BITMAPFILEHEADER), 
        (LPDWORD) &dwTmp,  NULL)) 
    {
		return;
    }

    // Copy the BITMAPINFOHEADER and RGBQUAD array into the file.  
    if (!WriteFile(hf, (LPVOID) pbih, sizeof(BITMAPINFOHEADER) 
                  + pbih->biClrUsed * sizeof (RGBQUAD), 
                  (LPDWORD) &dwTmp, ( NULL)))
        return;

    // Copy the array of color indices into the .BMP file.  
    dwTotal = cb = pbih->biSizeImage; 
    hp = lpBits; 
    if (!WriteFile(hf, (LPSTR) hp, (int) cb, (LPDWORD) &dwTmp,NULL)) 
           return;

    // Close the .BMP file.  
     if (!CloseHandle(hf)) 
           return;

    // Free memory.  
    GlobalFree((HGLOBAL)lpBits);
}

bool outlinedText( HDC hDC, const wchar_t *wpszText )
{
#if 0
	const int fontHeight = MulDiv( 18, (int)GetDeviceCaps(hDC,LOGPIXELSY), 72);
	//printf( "fontHeight = %i\n", fontHeight );
	
	static HFONT font = CreateFontW( fontHeight, 0, 0, 0, 0, 0, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, VARIABLE_PITCH, L"Arial" );
	if( font == NULL ) {
		return false;
	}
	
	LOGBRUSH lbrush;
	lbrush.lbStyle = BS_SOLID;
	lbrush.lbColor = RGB( 0, 0, 0 );
	lbrush.lbHatch = HS_CROSS;
	
	static HBRUSH brush = CreateSolidBrush( RGB( 255, 255, 255 ) );
	static HPEN pen = ExtCreatePen( PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_ROUND | PS_JOIN_ROUND, 1, &lbrush, 0, NULL );
	
	SetTextColor( hDC, RGB( 255, 255, 255 ) );
	
	SelectObject( hDC, font );
	SetBkMode( hDC, TRANSPARENT );
	
	SelectObject( hDC, brush );
	SelectObject( hDC, pen );
	
	BeginPath( hDC );
	TextOutW( hDC, 20, 20, wpszText, wcslen( wpszText ) );
	EndPath( hDC );

	StrokeAndFillPath( hDC );
	
	return true;
#else
	using namespace Gdiplus;

	DWORD *pBitmapBits;

	BITMAPINFO bi;
	memset( &bi, 0, sizeof( bi ) );
	bi.bmiHeader.biSize = sizeof( BITMAPINFOHEADER );
	bi.bmiHeader.biWidth = 330;
	bi.bmiHeader.biHeight = 120;
	bi.bmiHeader.biPlanes = 1;
	bi.bmiHeader.biCompression = BI_RGB;
	bi.bmiHeader.biBitCount = 32;

	HDC hdcTemp = CreateCompatibleDC( hDC );

	HBITMAP bmp = CreateDIBSection( hdcTemp, &bi, DIB_RGB_COLORS, ( void ** )&pBitmapBits, NULL, 0 );
	SelectObject( hdcTemp, bmp );

	Graphics graphics( hdcTemp );
	graphics.SetSmoothingMode(SmoothingModeAntiAlias);
	graphics.SetInterpolationMode(InterpolationModeHighQualityBicubic);

	FontFamily fontFamily(L"MS Gothic");
	StringFormat strformat;


	Ax::CTimer timer;

	GraphicsPath path;
	path.AddString(wpszText, wcslen(wpszText), &fontFamily, FontStyleRegular, 18, Gdiplus::Point(10,10), &strformat );

	Pen pen(Color(255,0,0), 1);
    //pen.SetLineJoin(LineJoinRound);
	
	graphics.DrawPath(&pen, &path);
	//SelectObject( hDC, bmp );

	SolidBrush brush(Color(0,0,255));
	graphics.FillPath(&brush, &path);
	//SelectObject( hDC, bmp );

	graphics.Flush(FlushIntentionSync);

	const Ax::uint64 microsecs = timer.GetElapsed();
	const Ax::uint32 millisecs = ( Ax::uint32 )Ax::MicrosecondsToMilliseconds( microsecs );

	printf( "Time: %u\n", millisecs );

	SelectObject( hDC, bmp );
	BitBlt( hDC, 0, 0, bi.bmiHeader.biWidth, bi.bmiHeader.biHeight, hdcTemp, 0, 0, SRCCOPY );

	static bool bSaved = false;
	if( !bSaved ) {
		bSaved = true;
		CreateBMPFile( WindowFromDC( hDC ), L"gditext.bmp", CreateBitmapInfoStruct( WindowFromDC( hDC ), bmp ), bmp, hDC );
	}

	return true;
#endif
}

LRESULT CALLBACK msgProc_f( HWND hWnd, UINT uMsg, WPARAM wParm, LPARAM lParm )
{
	switch( uMsg )
	{
	case WM_CLOSE:
		DestroyWindow( hWnd );
		return 0;
	case WM_DESTROY:
		PostQuitMessage( 0 );
		return 0;
	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			BeginPaint( hWnd, &ps );
			outlinedText( ps.hdc, L"【ゆり】\n「Hello, world! こんにちは世界！」" );
			EndPaint( hWnd, &ps );
		}
		return 0;
	case WM_KEYUP:
		if( wParm == VK_ESCAPE ) {
			DestroyWindow( hWnd );
			return 0;
		}
		break;
	}
	
	return DefWindowProcW( hWnd, uMsg, wParm, lParm );
}

int appMain()
{
	{
		using namespace Gdiplus;

		GdiplusStartupInput gdiplusStartupInput;
		ULONG_PTR gdiplusToken;
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	}
   
	WNDCLASSEXW wc;

	wc.cbSize = sizeof( wc );
	wc.style = CS_OWNDC;
	wc.lpfnWndProc = &msgProc_f;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = GetModuleHandleW( NULL );
	wc.hIcon = LoadIcon( NULL, IDI_APPLICATION );
	wc.hCursor = LoadCursor( NULL, IDC_ARROW );
	wc.hbrBackground = CreateSolidBrush(RGB(25*3,75*2,125)); //GetSysColorBrush( COLOR_BTNFACE );
	wc.lpszMenuName = NULL;
	wc.lpszClassName = L"MyWnd";
	wc.hIconSm = LoadIcon( NULL, IDI_APPLICATION );

	ATOM wndClass = RegisterClassExW( &wc );
	if( !wndClass ) {
		fprintf( stderr, "Error. Failed to register window class.\n" );
		return EXIT_FAILURE;
	}

	DWORD style = WS_OVERLAPPEDWINDOW;
	DWORD exstyle = 0;

	const int sresx = GetSystemMetrics( SM_CXSCREEN );
	const int sresy = GetSystemMetrics( SM_CYSCREEN );
	const int wresx = sresx/2 + sresx/4;
	const int wresy = (int)(double(wresx)/(1280.0/720.0));
	RECT area = {
		sresx/2 - wresx/2,
		sresy/2 - wresy/2,
		sresx/2 - wresx/2 + wresx,
		sresy/2 - wresy/2 + wresy
	};
	AdjustWindowRectEx( &area, style, FALSE, exstyle );

	HWND hWnd =
		CreateWindowExW
		(
			exstyle,
			( LPCWSTR )( size_t )wndClass,
			L"GDI Text - Test Window",
			style,
			area.left,
			area.top,
			area.right - area.left,
			area.bottom - area.top,
			NULL,
			NULL,
			GetModuleHandleW( NULL ),
			NULL
		);
	if( !hWnd ) {
		fprintf( stderr, "Error. Failed to create window.\n" );
		return EXIT_FAILURE;
	}
	
	ShowWindow( hWnd, SW_SHOW );
	UpdateWindow( hWnd );

	MSG msg;
	while( GetMessage( &msg, NULL, 0, 0 ) > 0 ) {
		TranslateMessage( &msg );
		DispatchMessageW( &msg );
	}

	return EXIT_SUCCESS;
}

int main()
{
	return appMain();
}
int WINAPI WinMain( HINSTANCE, HINSTANCE, LPSTR, int )
{
	return appMain();
}
