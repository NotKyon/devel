#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

extern size_t SomeFunc1();
extern size_t SomeFunc2();

inline size_t ReturnCount1() {
	return SomeFunc1();
}
inline size_t ReturnCount2() {
	return SomeFunc2();
}

int main(int argc, char **argv) {
	size_t n;
	bool m;
	int i;

	n = 0;
	m = false;

	for(i=1; i<argc; i++) {
		if (!strcmp(argv[i], "--a"))
			m = true;
		else if(!strcmp(argv[i], "--b"))
			m = false;
		else
			fprintf(stderr, "(wtf is this? '%s')\n", argv[i]);
	}

	if (!n) {
		if (m)
			n = ReturnCount1();
		else
			n = ReturnCount2();
	}

	if (n > 32)
		n = 32;

	printf("%u\n", (unsigned int)n);
	return EXIT_SUCCESS;
}
