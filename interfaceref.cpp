#include <stdio.h>
#include <stdlib.h>

class Interface
{
public:
	Interface() {}
	virtual ~Interface() {}

	virtual void DoSomething() = 0;
};
class Implementation: public virtual Interface
{
public:
	Implementation(): Interface() {}
	virtual ~Implementation() {}

	virtual void DoSomething() { printf( "Hello!\n" ); }
};

class Test
{
public:
	Test( Interface &iface ): mInterface( iface ) {}
	~Test() {}

	int Run() { mInterface.DoSomething(); return 0; }

protected:
	Interface &mInterface;
};

int main()
{
	Implementation i;
	Test x( i );
	return x.Run();
}
