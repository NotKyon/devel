#if _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif

#include <gamebase.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct palette_s {
	gb_consoleColor_t type;
	gb_consoleColor_t msg;
};

static gb_consoleColor_t g_bracket = kGBConCol_Cyan;
static gb_consoleColor_t g_file = kGBConCol_BrightCyan;
static gb_consoleColor_t g_paren = kGBConCol_Blue;
static gb_consoleColor_t g_line = kGBConCol_BrightBlue;
static gb_consoleColor_t g_func = kGBConCol_Green;
static gb_consoleColor_t g_errnoParen = kGBConCol_Purple;
static gb_consoleColor_t g_errnoMsg = kGBConCol_BrightPurple;
static gb_consoleColor_t g_errnoCodeParen = kGBConCol_Blue;
static gb_consoleColor_t g_errnoCode = kGBConCol_BrightBlue;

static struct palette_s g_palette[kNumGBLogTypes] = {
	/* kGBLogType_Debug */
	{ kGBConCol_BrightGreen, kGBConCol_Green },

	/* kGBLogType_FatalError */
	{ kGBConCol_BrightWhite, kGBConCol_Red },

	/* kGBLogType_Error */
	{ kGBConCol_BrightRed, kGBConCol_White },

	/* kGBLogType_Warning */
	{ kGBConCol_BrightYellow, kGBConCol_White },

	/* kGBLogType_Remark */
	{ kGBConCol_BrightPurple, kGBConCol_White },

	/* kGBLogType_Normal */
	{ kGBConCol_White, kGBConCol_White }
};
static const char *g_types[kNumGBLogTypes] = {
	"DEBUG: *** ",
	"FATAL-ERROR: ",
	"ERROR: ",
	"WARNING: ",
	"NOTE: ",
	(const char *)NULL
};

static FILE *g_debugLog = (FILE *)NULL;
static void OpenDebugLog(gb_logType_t type) {
	if (type==kGBLogType_Debug)
		g_debugLog = fopen("debug.log", "a+");
}
static void CloseDebugLog(gb_logType_t type) {
	if (type==kGBLogType_Debug)
		fclose(g_debugLog);
}
static void PrintX(gb_logType_t type, gb_consoleColor_t col, const char *p) {
	gbSetConsoleTextColor(col);

	if (type==kGBLogType_Normal)
		gbWriteStdout(p);
	else
		gbWriteStderr(p);

	if (type==kGBLogType_Debug)
		fputs(p, g_debugLog);
}

void gbReportV(gb_logType_t type, const char *file, int line, const char *func,
const char *message, va_list args) {
	gb_consoleColor_t curColor;
#if __STDC_WANT_SECURE_LIB__
	char bufx[256];
#endif
	char buf[64];
	int e;

	gbAssert((int)type>=kGBLogType_Debug && (int)type<kNumGBLogTypes);
	gbAssert(message != (const char *)NULL);

	e = errno;
	curColor = gbGetConsoleTextColor();

	OpenDebugLog(type);

	if (file) {
		PrintX(type, g_bracket, "[");
		PrintX(type, g_file, file);
		if (line) {
#if __STDC_WANT_SECURE_LIB__
			sprintf_s(buf, sizeof(buf), "%i", line);
#else
			snprintf(buf, sizeof(buf)-1, "%i", line);
			buf[sizeof(buf)-1] = 0;
#endif

			PrintX(type, g_paren, "(");
			PrintX(type, g_line, buf);
			PrintX(type, g_paren, ")");
		}

		if (func) {
			PrintX(type, g_func, " ");
			PrintX(type, g_func, func);
		}

		PrintX(type, g_bracket, "]\n");
	} else if(func) {
		PrintX(type, g_bracket, "<");
		PrintX(type, g_func, func);
		PrintX(type, g_bracket, "> ");
	}

	PrintX(type, g_palette[type].type, g_types[type]);
	PrintX(type, g_palette[type].msg, gbFV(message, args));

	if (e) {
#if __STDC_WANT_SECURE_LIB__
		sprintf_s(buf, sizeof(buf), "%i", e);
#else
		snprintf(buf, sizeof(buf)-1, "%i", e);
		buf[sizeof(buf)-1] = 0;
#endif

		PrintX(type, g_errnoParen, " [");
#if __STDC_WANT_SECURE_LIB__
		if (strerror_s(bufx, sizeof(bufx), e) != 0)
			bufx[0] = '\0';

		PrintX(type, g_errnoMsg, bufx);
#else
		PrintX(type, g_errnoMsg, strerror(e));
#endif
		PrintX(type, g_errnoCodeParen, " (");
		PrintX(type, g_errnoCode, buf);
		PrintX(type, g_errnoCodeParen, ")");
		PrintX(type, g_errnoParen, "]");
	}

	PrintX(type, curColor, "\n");
	CloseDebugLog(type);

#if _WIN32
	if (type==kGBLogType_FatalError || type==kGBLogType_Error) {
		gb_string_t str;

		gbInitString(&str);

		if (file) {
			gbPushString(&str, gbF("[%s", file));

			if (line)
				gbPushString(&str, gbF("(%u)", line));

			if (func)
				gbPushString(&str, gbF(" %s", func));

			gbPushString(&str, "]\n\n");
		} else if(func)
			gbPushString(&str, gbF("[%s]\n\n", func));

		gbPushString(&str, gbFV(message, args));

		if (e) {
#if __STDC_WANT_SECURE_LIB__
			gbPushString(&str, gbF("\n%s (%i)", bufx, e));
#else
			gbPushString(&str, gbF("\n%s (%i)", strerror(e), e));
#endif
		}

		MessageBoxA(GetActiveWindow(), gbStringBuffer(&str), "Error",
			MB_ICONERROR|MB_OK);
		gbFiniString(&str);
	}
#endif

#if DEBUG||_DEBUG||__debug__ || MK_DEBUG || GB_DEBUG
	if (type==kGBLogType_FatalError || type==kGBLogType_Error)
		gbDebugStop();
#endif
}

/* -------------------------------------------------------------------------- */

void gbReport(gb_logType_t type, const char *file, int line, const char *func,
const char *message, ...) {
	va_list args;

	va_start(args, message);
	gbReportV(type, file, line, func, message, args);
	va_end(args);
}

void gbWarn(const char *file, int line, const char *func, const char *message,
...) {
	va_list args;

	va_start(args, message);
	gbReportV(kGBLogType_Warning, file, line, func, message, args);
	va_end(args);
}
void gbError(const char *file, int line, const char *func, const char *message,
...) {
	va_list args;

	va_start(args, message);
	gbReportV(kGBLogType_Error, file, line, func, message, args);
	va_end(args);
}

void gbWarnMessage(const char *message, ...) {
	va_list args;

	va_start(args, message);
	gbReportV(kGBLogType_Warning, NULL, 0, NULL, message, args);
	va_end(args);
}
void gbErrorMessage(const char *message, ...) {
	va_list args;

	va_start(args, message);
	gbReportV(kGBLogType_Error, NULL, 0, NULL, message, args);
	va_end(args);
}

void gbWarnFile(const char *file, int line, const char *message, ...) {
	va_list args;

	va_start(args, message);
	gbReportV(kGBLogType_Warning, file, line, NULL, message, args);
	va_end(args);
}
void gbErrorFile(const char *file, int line, const char *message, ...) {
	va_list args;

	va_start(args, message);
	gbReportV(kGBLogType_Error, file, line, NULL, message, args);
	va_end(args);
}

void gbErrorFileExit(const char *file, int line, const char *message, ...) {
	va_list args;

	va_start(args, message);
	gbReportV(kGBLogType_Error, file, line, NULL, message, args);
	va_end(args);

	gbEnd();
}
void gbErrorExit(const char *message, ...) {
	va_list args;

	va_start(args, message);
	gbReportV(kGBLogType_Error, NULL, 0, NULL, message, args);
	va_end(args);

	gbEnd();
}
