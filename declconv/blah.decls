.lib "blah"

Const WM_NULL                         =0x0000
Const WM_CREATE                       =0x0001
Const WM_DESTROY                      =0x0002
Const WM_MOVE                         =0x0003
Const WM_SIZE                         =0x0005
Const WM_ACTIVATE                     =0x0006
Const WM_SETFOCUS                     =0x0007
Const WM_KILLFOCUS                    =0x0008
Const WM_ENABLE                       =0x000A
Const WM_SETREDRAW                    =0x000B
Const WM_SETTEXT                      =0x000C
Const WM_GETTEXT                      =0x000D
Const WM_GETTEXTLENGTH                =0x000E
Const WM_PAINT                        =0x000F
Const WM_CLOSE                        =0x0010
Const WM_QUERYENDSESSION              =0x0011
Const WM_QUERYOPEN                    =0x0013
Const WM_ENDSESSION                   =0x0016
Const WM_QUIT                         =0x0012
Const WM_ERASEBKGND                   =0x0014
Const WM_SYSCOLORCHANGE               =0x0015
Const WM_SHOWWINDOW                   =0x0018
Const WM_WININICHANGE                 =0x001A
Const WM_DEVMODECHANGE                =0x001B
Const WM_ACTIVATEAPP                  =0x001C
Const WM_FONTCHANGE                   =0x001D
Const WM_TIMECHANGE                   =0x001E
Const WM_CANCELMODE                   =0x001F
Const WM_SETCURSOR                    =0x0020
Const WM_MOUSEACTIVATE                =0x0021
Const WM_CHILDACTIVATE                =0x0022
Const WM_QUEUESYNC                    =0x0023
Const WM_GETMINMAXINFO                =0x0024
Const WM_SIZING                       =0x0214

Const WS_OVERLAPPED							=0x00000000
Const WS_MAXIMIZEBOX 						=0x00010000
Const WS_MINIMIZEBOX 						=0x00020000
Const WS_THICKFRAME 						=0x00040000
Const WS_CAPTION 							=0x00C00000
Const WS_SYSMENU 							=0x00080000
Const WS_POPUP 								=0x80000000
Const WS_VISIBLE 							=0x10000000
Const WS_HSCROLL 							=0x00100000
Const WS_VSCROLL 							=0x00200000
Const WS_MINIMIZE 							=0x20000000
Const WS_MAXIMIZE 							=0x01000000
Const WS_DLGFRAME 							=0x00400000
Const WS_DISABLED 							=0x08000000
Const WS_CLIPSIBLINGS 						=0x04000000
Const WS_CLIPCHILDREN 						=0x02000000
Const WS_CHILD 								=0x40000000
Const WS_BORDER 							=0x00800000
Const WS_OVERLAPPEDWINDOW 					=13565952

Const WS_EX_DLGMODALFRAME     =0x00000001
Const WS_EX_NOPARENTNOTIFY    =0x00000004
Const WS_EX_TOPMOST           =0x00000008
Const WS_EX_ACCEPTFILES       =0x00000010
Const WS_EX_TRANSPARENT       =0x00000020
Const WS_EX_MDICHILD          =0x00000040
Const WS_EX_TOOLWINDOW        =0x00000080
Const WS_EX_WINDOWEDGE        =0x00000100
Const WS_EX_CLIENTEDGE        =0x00000200
Const WS_EX_CONTEXTHELP       =0x00000400
Const WS_EX_RIGHT             =0x00001000
Const WS_EX_LEFT              =0x00000000
Const WS_EX_RTLREADING        =0x00002000
Const WS_EX_LTRREADING        =0x00000000
Const WS_EX_LEFTSCROLLBAR     =0x00004000
Const WS_EX_RIGHTSCROLLBAR    =0x00000000
Const WS_EX_CONTROLPARENT     =0x00010000
Const WS_EX_STATICEDGE        =0x00020000
Const WS_EX_APPWINDOW         =0x00040000

Const WS_EX_NOINHERITLAYOUT   =0x00100000

Const WS_EX_LAYOUTRTL         =0x00400000
Const WS_EX_COMPOSITED        =0x02000000
Const WS_EX_NOACTIVATE 		  =0x08000000

Const RBS_TOOLTIPS                  =0x00000100
Const RBS_VARHEIGHT                 =0x00000200
Const RBS_BANDBORDERS               =0x00000400
Const RBS_FIXEDORDER                =0x00000800
Const RBS_REGISTERDROP              =0x00001000
Const RBS_AUTOSIZE                  =0x00002000
Const RBS_VERTICALGRIPPER           =0x00004000

Const RBS_DBLCLKTOGGLE              =0x00008000

Const RBBS_BREAK          =0x00000001

Const RBBS_FIXEDSIZE      =0x00000002

Const RBBS_CHILDEDGE      =0x00000004

Const RBBS_HIDDEN         =0x00000008

Const RBBS_NOVERT         =0x00000010

Const RBBS_FIXEDBMP       =0x00000020

Const RBBS_VARIABLEHEIGHT =0x00000040

Const RBBS_GRIPPERALWAYS  =0x00000080

Const RBBS_NOGRIPPER      =0x00000100

Const RBBS_USECHEVRON     =0x00000200

Const RBBS_HIDETITLE      =0x00000400

Const RBBS_TOPALIGN       =0x00000800

Const SBS_VERT =0x0001
Const SBS_HORZ =0x0000

Const IDOK          =1
Const IDCANCEL      =2
Const IDABORT       =3
Const IDRETRY       =4
Const IDIGNORE      =5
Const IDYES         =6
Const IDNO          =7
Const IDCLOSE       =8
Const IDHELP        =9
Const IDTRYAGAIN    =10
Const IDCONTINUE    =11

Const MB_OK                       =0x00000000
Const MB_OKCANCEL                 =0x00000001
Const MB_ABORTRETRYIGNORE         =0x00000002
Const MB_YESNOCANCEL              =0x00000003
Const MB_YESNO                    =0x00000004
Const MB_RETRYCANCEL              =0x00000005
Const MB_CANCELTRYCONTINUE        =0x00000006

Const MB_ICONHAND                 =0x00000010
Const MB_ICONQUESTION             =0x00000020
Const MB_ICONEXCLAMATION          =0x00000030
Const MB_ICONASTERISK             =0x00000040
Const MB_USERICON                 =0x00000080

Const ES_LEFT             =0x0000
Const ES_CENTER           =0x0001
Const ES_RIGHT            =0x0002
Const ES_MULTILINE        =0x0004
Const ES_UPPERCASE        =0x0008
Const ES_LOWERCASE        =0x0010
Const ES_PASSWORD         =0x0020
Const ES_AUTOVSCROLL      =0x0040
Const ES_AUTOHSCROLL      =0x0080
Const ES_NOHIDESEL        =0x0100
Const ES_OEMCONVERT       =0x0400
Const ES_READONLY         =0x0800
Const ES_WANTRETURN       =0x1000
Const ES_NUMBER           =0x2000

Const SB_LINEUP           =0
Const SB_LINELEFT         =0
Const SB_LINEDOWN         =1
Const SB_LINERIGHT        =1
Const SB_PAGEUP           =2
Const SB_PAGELEFT         =2
Const SB_PAGEDOWN         =3
Const SB_PAGERIGHT        =3
Const SB_THUMBPOSITION    =4
Const SB_THUMBTRACK       =5
Const SB_TOP              =6
Const SB_LEFT             =6
Const SB_BOTTOM           =7
Const SB_RIGHT            =7
Const SB_ENDSCROLL        =8

Const PBS_SMOOTH              =0x01
Const PBS_VERTICAL            =0x04
Const PBS_SMOOTHREVERSE       =0x10

Const LVS_ICON                =0x0000
Const LVS_REPORT              =0x0001

Const LVS_TYPEMASK            =0x0003
Const LVS_SINGLESEL           =0x0004
Const LVS_SHOWSELALWAYS       =0x0008
Const LVS_SORTASCENDING       =0x0010
Const LVS_SORTDESCENDING      =0x0020
Const LVS_SHAREIMAGELISTS     =0x0040
Const LVS_NOLABELWRAP         =0x0080
Const LVS_AUTOARRANGE         =0x0100
Const LVS_EDITLABELS          =0x0200
Const LVS_NOSCROLL            =0x2000
Const LVS_ALIGNTOP            =0x0000
Const LVS_ALIGNLEFT           =0x0800
Const LVS_ALIGNMASK           =0x0c00
Const LVS_OWNERDRAWFIXED      =0x0400
Const LVS_NOCOLUMNHEADER      =0x4000
Const LVS_NOSORTHEADER        =0x8000

Const LVS_EX_GRIDLINES        =0x00000001
Const LVS_EX_SUBITEMIMAGES    =0x00000002
Const LVS_EX_CHECKBOXES       =0x00000004
Const LVS_EX_TRACKSELECT      =0x00000008
Const LVS_EX_HEADERDRAGDROP   =0x00000010

Const LVS_EX_FULLROWSELECT    =0x00000020
Const LVS_EX_ONECLICKACTIVATE =0x00000040
Const LVS_EX_TWOCLICKACTIVATE =0x00000080
Const LVS_EX_FLATSB           =0x00000100
Const LVS_EX_REGIONAL         =0x00000200

Const LVS_EX_INFOTIP          =0x00000400
Const LVS_EX_UNDERLINEHOT     =0x00000800
Const LVS_EX_UNDERLINECOLD    =0x00001000
Const LVS_EX_MULTIWORKAREAS   =0x00002000

Const LVS_EX_LABELTIP         =0x00004000

Const LVS_EX_BORDERSELECT     =0x00008000
Const LVS_EX_DOUBLEBUFFER     =0x00010000
Const LVS_EX_HIDELABELS       =0x00020000
Const LVS_EX_SINGLEROW        =0x00040000

Const LVS_EX_SNAPTOGRID       =0x00080000

Const LVS_EX_SIMPLESELECT     =0x00100000

Const LVS_EX_JUSTIFYCOLUMNS   =0x00200000

Const LVS_EX_TRANSPARENTBKGND =0x00400000

Const LVS_EX_TRANSPARENTSHADOWTEXT =0x00800000

Const LVS_EX_AUTOAUTOARRANGE  =0x01000000

Const LVS_EX_HEADERINALLVIEWS =0x02000000
Const LVS_EX_AUTOCHECKSELECT  =0x08000000
Const LVS_EX_AUTOSIZECOLUMNS  =0x10000000
Const LVS_EX_COLUMNSNAPPOINTS =0x40000000
Const LVS_EX_COLUMNOVERFLOW   =0x80000000

Const LVSCW_AUTOSIZE              =-1
Const LVSCW_AUTOSIZE_USEHEADER    =-2

Const LVIS_FOCUSED            =0x0001
Const LVIS_SELECTED           =0x0002
Const LVIS_CUT                =0x0004
Const LVIS_DROPHILITED        =0x0008
Const LVIS_GLOW               =0x0010
Const LVIS_ACTIVATING         =0x0020
Const LVIS_OVERLAYMASK        =0x0F00
Const LVIS_STATEIMAGEMASK     =0xF000

Const CBS_SIMPLE            =0x0001
Const CBS_DROPDOWN          =0x0002
Const CBS_DROPDOWNLIST      =0x0003
Const CBS_OWNERDRAWFIXED    =0x0010
Const CBS_OWNERDRAWVARIABLE =0x0020
Const CBS_AUTOHSCROLL       =0x0040
Const CBS_OEMCONVERT        =0x0080
Const CBS_SORT              =0x0100
Const CBS_HASSTRINGS        =0x0200
Const CBS_NOINTEGRALHEIGHT  =0x0400
Const CBS_DISABLENOSCROLL   =0x0800
Const CBS_UPPERCASE         =0x2000
Const CBS_LOWERCASE         =0x4000

Const DDL_READWRITE       =0x0000
Const DDL_READONLY        =0x0001
Const DDL_HIDDEN          =0x0002
Const DDL_SYSTEM          =0x0004
Const DDL_DIRECTORY       =0x0010
Const DDL_ARCHIVE         =0x0020

Const DDL_POSTMSGS        =0x2000
Const DDL_DRIVES          =0x4000
Const DDL_EXCLUSIVE       =0x8000

Const CB_GETEDITSEL               =0x0140
Const CB_LIMITTEXT                =0x0141
Const CB_SETEDITSEL               =0x0142
Const CB_ADDSTRING                =0x0143
Const CB_DELETESTRING             =0x0144
Const CB_DIR                      =0x0145
Const CB_GETCOUNT                 =0x0146
Const CB_GETCURSEL                =0x0147
Const CB_GETLBTEXT                =0x0148
Const CB_GETLBTEXTLEN             =0x0149
Const CB_INSERTSTRING             =0x014A
Const CB_RESETCONTENT             =0x014B
Const CB_FINDSTRING               =0x014C
Const CB_SELECTSTRING             =0x014D
Const CB_SETCURSEL                =0x014E
Const CB_SHOWDROPDOWN             =0x014F
Const CB_GETITEMDATA              =0x0150
Const CB_SETITEMDATA              =0x0151
Const CB_GETDROPPEDCONTROLRECT    =0x0152
Const CB_SETITEMHEIGHT            =0x0153
Const CB_GETITEMHEIGHT            =0x0154
Const CB_SETEXTENDEDUI            =0x0155
Const CB_GETEXTENDEDUI            =0x0156
Const CB_GETDROPPEDSTATE          =0x0157
Const CB_FINDSTRINGEXACT          =0x0158
Const CB_SETLOCALE                =0x0159
Const CB_GETLOCALE                =0x015A
Const CB_GETTOPINDEX              =0x015b
Const CB_SETTOPINDEX              =0x015c
Const CB_GETHORIZONTALEXTENT      =0x015d
Const CB_SETHORIZONTALEXTENT      =0x015e
Const CB_GETDROPPEDWIDTH          =0x015f
Const CB_SETDROPPEDWIDTH          =0x0160
Const CB_INITSTORAGE              =0x0161

Const TCS_SCROLLOPPOSITE      =0x0001
Const TCS_BOTTOM              =0x0002
Const TCS_RIGHT               =0x0002

Const TCS_MULTISELECT         =0x0004
Const TCS_FLATBUTTONS         =0x0008
Const TCS_FORCEICONLEFT       =0x0010
Const TCS_FORCELABELLEFT      =0x0020
Const TCS_HOTTRACK            =0x0040
Const TCS_VERTICAL            =0x0080
Const TCS_TABS                =0x0000
Const TCS_BUTTONS             =0x0100
Const TCS_SINGLELINE          =0x0000
Const TCS_MULTILINE           =0x0200
Const TCS_RIGHTJUSTIFY        =0x0000
Const TCS_FIXEDWIDTH          =0x0400
Const TCS_RAGGEDRIGHT         =0x0800
Const TCS_FOCUSONBUTTONDOWN   =0x1000
Const TCS_OWNERDRAWFIXED      =0x2000
Const TCS_TOOLTIPS            =0x4000
Const TCS_FOCUSNEVER          =0x8000

Const TCS_EX_FLATSEPARATORS   =0x00000001
Const TCS_EX_REGISTERDROP     =0x00000002

Const TBSTYLE_TOOLTIPS        =0x0100
Const TBSTYLE_WRAPABLE        =0x0200
Const TBSTYLE_ALTDRAG         =0x0400
Const TBSTYLE_FLAT            =0x0800
Const TBSTYLE_LIST            =0x1000
Const TBSTYLE_CUSTOMERASE     =0x2000
Const TBSTYLE_REGISTERDROP    =0x4000
Const TBSTYLE_TRANSPARENT     =0x8000

Const TBSTYLE_EX_MIXEDBUTTONS             =0x00000008

Const TBSTYLE_EX_HIDECLIPPEDBUTTONS       =0x00000010

Const TBSTYLE_EX_DOUBLEBUFFER             =0x00000080
Const TBSTYLE_EX_DRAWDDARROWS =0x00000001

Const BTNS_BUTTON     =0x0000
Const BTNS_SEP        =0x0001
Const BTNS_CHECK      =0x0002
Const BTNS_GROUP      =0x0004
Const BTNS_DROPDOWN   =0x0008

Const BTNS_AUTOSIZE   =0x0010

Const BTNS_NOPREFIX   =0x0020

Const BTNS_SHOWTEXT   =0x0040

Const BTNS_WHOLEDROPDOWN  =0x0080

Const CCS_TOP                 =0x00000001
Const CCS_NOMOVEY             =0x00000002
Const CCS_BOTTOM              =0x00000003
Const CCS_NORESIZE            =0x00000004
Const CCS_NOPARENTALIGN       =0x00000008
Const CCS_ADJUSTABLE          =0x00000020
Const CCS_NODIVIDER           =0x00000040
Const CCS_VERT                =0x00000080

Const TTS_ALWAYSTIP           =0x01
Const TTS_NOPREFIX            =0x02
Const TTS_NOANIMATE           =0x10
Const TTS_NOFADE              =0x20
Const TTS_BALLOON             =0x40
Const TTS_CLOSE               =0x80

Const TTI_NONE                =0
Const TTI_INFO                =1
Const TTI_WARNING             =2
Const TTI_ERROR               =3
Const TTI_INFO_LARGE          =4
Const TTI_WARNING_LARGE       =5
Const TTI_ERROR_LARGE         =6

Const TTDT_AUTOMATIC          =0
Const TTDT_RESHOW             =1
Const TTDT_AUTOPOP            =2
Const TTDT_INITIAL            =3

Const TBS_AUTOTICKS           =0x0001
Const TBS_VERT                =0x0002
Const TBS_HORZ                =0x0000
Const TBS_TOP                 =0x0004
Const TBS_BOTTOM              =0x0000
Const TBS_LEFT                =0x0004
Const TBS_RIGHT               =0x0000
Const TBS_BOTH                =0x0008
Const TBS_NOTICKS             =0x0010
Const TBS_ENABLESELRANGE      =0x0020
Const TBS_FIXEDLENGTH         =0x0040
Const TBS_NOTHUMB             =0x0080
Const TBS_TOOLTIPS            =0x0100
Const TBS_REVERSED            =0x0200

Const SS_LEFT             =0x00000000
Const SS_CENTER           =0x00000001
Const SS_RIGHT            =0x00000002
Const SS_ICON             =0x00000003
Const SS_BLACKRECT        =0x00000004
Const SS_GRAYRECT         =0x00000005
Const SS_WHITERECT        =0x00000006
Const SS_BLACKFRAME       =0x00000007
Const SS_GRAYFRAME        =0x00000008
Const SS_WHITEFRAME       =0x00000009
Const SS_USERITEM         =0x0000000A
Const SS_SIMPLE           =0x0000000B
Const SS_LEFTNOWORDWRAP   =0x0000000C
Const SS_OWNERDRAW        =0x0000000D
Const SS_BITMAP           =0x0000000E
Const SS_ENHMETAFILE      =0x0000000F
Const SS_ETCHEDHORZ       =0x00000010
Const SS_ETCHEDVERT       =0x00000011
Const SS_ETCHEDFRAME      =0x00000012
Const SS_TYPEMASK         =0x0000001F
Const SS_REALSIZECONTROL  =0x00000040

Const SS_NOPREFIX         =0x00000080
Const SS_NOTIFY           =0x00000100
Const SS_CENTERIMAGE      =0x00000200
Const SS_RIGHTJUST        =0x00000400
Const SS_REALSIZEIMAGE    =0x00000800
Const SS_SUNKEN           =0x00001000
Const SS_EDITCONTROL      =0x00002000
Const SS_ENDELLIPSIS      =0x00004000
Const SS_PATHELLIPSIS     =0x00008000
Const SS_WORDELLIPSIS     =0x0000C000
Const SS_ELLIPSISMASK     =0x0000C000

Const UDS_WRAP                =0x0001

Const UDS_ALIGNRIGHT          =0x0004
Const UDS_ALIGNLEFT           =0x0008

Const UDS_ARROWKEYS           =0x0020

Const UDS_HORZ                =0x0040
Const UDS_NOTHOUSANDS         =0x0080

Const DISPID_BEFORENAVIGATE     =100
Const DISPID_NAVIGATECOMPLETE   =101
Const DISPID_STATUSTEXTCHANGE   =102
Const DISPID_QUIT               =103
Const DISPID_DOWNLOADCOMPLETE   =104
Const DISPID_COMMANDSTATECHANGE =105
Const DISPID_DOWNLOADBEGIN      =106
Const DISPID_NEWWINDOW          =107
Const DISPID_PROGRESSCHANGE     =108
Const DISPID_WINDOWMOVE         =109
Const DISPID_WINDOWRESIZE       =110
Const DISPID_WINDOWACTIVATE     =111
Const DISPID_PROPERTYCHANGE     =112
Const DISPID_TITLECHANGE        =113
Const DISPID_TITLEICONCHANGE    =114

Const DISPID_FRAMEBEFORENAVIGATE    =200
Const DISPID_FRAMENAVIGATECOMPLETE  =201
Const DISPID_FRAMENEWWINDOW         =204

Const DISPID_BEFORENAVIGATE2              =250
Const DISPID_NEWWINDOW2                   =251
Const DISPID_NAVIGATECOMPLETE2            =252
Const DISPID_ONQUIT                       =253
Const DISPID_ONVISIBLE                    =254
Const DISPID_ONTOOLBAR                    =255           
Const DISPID_ONMENUBAR                    =256           
Const DISPID_ONSTATUSBAR                  =257           
Const DISPID_ONFULLSCREEN                 =258           
Const DISPID_DOCUMENTCOMPLETE             =259           
Const DISPID_ONTHEATERMODE                =260           
Const DISPID_ONADDRESSBAR                 =261           
Const DISPID_WINDOWSETRESIZABLE           =262           
Const DISPID_WINDOWCLOSING                =263           
Const DISPID_WINDOWSETLEFT                =264           
Const DISPID_WINDOWSETTOP                 =265           
Const DISPID_WINDOWSETWIDTH               =266           
Const DISPID_WINDOWSETHEIGHT              =267           
Const DISPID_CLIENTTOHOSTWINDOW           =268           
Const DISPID_SETSECURELOCKICON            =269           
Const DISPID_FILEDOWNLOAD                 =270           
Const DISPID_NAVIGATEERROR                =271           
Const DISPID_PRIVACYIMPACTEDSTATECHANGE   =272           
Const DISPID_NEWWINDOW3                   =273
Const DISPID_VIEWUPDATE                   =281           
Const DISPID_SETPHISHINGFILTERSTATUS      =282           
Const DISPID_WINDOWSTATECHANGED           =283          

Const DISPID_PRINTTEMPLATEINSTANTIATION   =225           
Const DISPID_PRINTTEMPLATETEARDOWN        =226           
Const DISPID_UPDATEPAGESTATUS             =227           

Const CBENF_KILLFOCUS         =1

Const CBENF_RETURN            =2

Const CBENF_ESCAPE            =3

Const CBENF_DROPDOWN          =4

Const CBN_ERRSPACE        =-1
Const CBN_SELCHANGE       =1
Const CBN_DBLCLK          =2
Const CBN_SETFOCUS        =3
Const CBN_KILLFOCUS       =4
Const CBN_EDITCHANGE      =5
Const CBN_EDITUPDATE      =6
Const CBN_DROPDOWN        =7
Const CBN_CLOSEUP         =8
Const CBN_SELENDOK        =9
Const CBN_SELENDCANCEL    =10

Const LR_DEFAULTCOLOR     =0x00000000
Const LR_MONOCHROME       =0x00000001
Const LR_COLOR            =0x00000002
Const LR_COPYRETURNORG    =0x00000004
Const LR_COPYDELETEORG    =0x00000008
Const LR_LOADFROMFILE     =0x00000010
Const LR_LOADTRANSPARENT  =0x00000020
Const LR_DEFAULTSIZE      =0x00000040
Const LR_VGACOLOR         =0x00000080
Const LR_LOADMAP3DCOLORS  =0x00001000
Const LR_CREATEDIBSECTION =0x00002000
Const LR_COPYFROMRESOURCE =0x00004000
Const LR_SHARED           =0x00008000


Const ANSI_CHARSET            =0
Const DEFAULT_CHARSET         =1
Const SYMBOL_CHARSET          =2
Const SHIFTJIS_CHARSET        =128
Const HANGEUL_CHARSET         =129
Const HANGUL_CHARSET          =129
Const GB2312_CHARSET          =134
Const CHINESEBIG5_CHARSET     =136
Const OEM_CHARSET             =255
Const JOHAB_CHARSET           =130
Const HEBREW_CHARSET          =177
Const ARABIC_CHARSET          =178
Const GREEK_CHARSET           =161
Const TURKISH_CHARSET         =162
Const VIETNAMESE_CHARSET      =163
Const THAI_CHARSET            =222
Const EASTEUROPE_CHARSET      =238
Const RUSSIAN_CHARSET         =204
Const MAC_CHARSET             =77
Const BALTIC_CHARSET          =186

Const FS_LATIN1               =0x00000001
Const FS_LATIN2               =0x00000002
Const FS_CYRILLIC             =0x00000004
Const FS_GREEK                =0x00000008
Const FS_TURKISH              =0x00000010
Const FS_HEBREW               =0x00000020
Const FS_ARABIC               =0x00000040
Const FS_BALTIC               =0x00000080
Const FS_VIETNAMESE           =0x00000100
Const FS_THAI                 =0x00010000
Const FS_JISJAPAN             =0x00020000
Const FS_CHINESESIMP          =0x00040000
Const FS_WANSUNG              =0x00080000
Const FS_CHINESETRAD          =0x00100000
Const FS_JOHAB                =0x00200000
Const FS_SYMBOL               =0x80000000

Const FF_DONTCARE         =0x00000000
Const FF_ROMAN            =0x00000010
Const FF_SWISS            =0x00000014
Const FF_MODERN           =0x00000030
Const FF_SCRIPT           =0x00000040
Const FF_DECORATIVE       =0x00000050

Const FW_DONTCARE         =0
Const FW_THIN             =100
Const FW_EXTRALIGHT       =200
Const FW_LIGHT            =300
Const FW_NORMAL           =400
Const FW_MEDIUM           =500
Const FW_SEMIBOLD         =600
Const FW_BOLD             =700
Const FW_EXTRABOLD        =800
Const FW_HEAVY            =900

Const LWA_COLORKEY            =0x00000001
Const LWA_ALPHA               =0x00000002

Const FLASHW_ALL =0x00000003

Const FLASHW_CAPTION =0x00000001

Const FLASHW_STOP =0

Const FLASHW_TIMER =0x00000004

Const FLASHW_TIMERNOFG =0x0000000C

Const FLASHW_TRAY =0x00000002
