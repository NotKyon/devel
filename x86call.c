#include <stdio.h>

int get_sum(int x, int y) {
  printf("get_sum(%i, %i)\n", x, y);fflush(stdout);
  return x + y;
}
int average(int a, int b, int c) {
  printf("average(%i, %i, %i)\n", a, b, c);fflush(stdout);
  return (a + b + c)/3;
}

#pragma GCC push_options
#pragma GCC optimize "-O0"
void *x86_call(void(*fn)(), size_t n, void **p) {
#if 1
  size_t esp, i;
  void *ret;

  asm("movl %%esp, %0" : "=r" (esp));

  for(i=1; i<=n; i++)
    asm("pushl %0\n" :: "r" (p[n - i]));

  asm("call *%1\n\tmovl %2, %%esp" : "=a" (ret) : "r" (fn), "b" (esp));
#elif 1
  size_t i;
  void *ret;

  for(i=0; i<n; i++)
    asm("push %0\n" :: "r" (p[n - i - 1]));

  asm("call *%1\n\tadd %2, %%esp\n"
      : "=a" (ret)
      : "r" (fn), "r" (n*sizeof(void *)));
#else
  size_t i;
  void *ret;

  i = 0;

  asm
  (
    "argument_loop:\n\t"
    "cmpl %[i], %[n]\n\t"
    "jge exit_argument_loop\n\t"
    "pushl (%[p],%[i],4)\n\t"
    "addl 4,%[p]\n\t"
    "jmp argument_loop\n\t"
    "exit_argument_loop:\n\t"
    "call *%[f]\n\t"
    "addl %[n], %%esp\n"
    : "=a" (ret)
    : [i] "r" (i),
      [n] "r" (n*sizeof(void *)),
      [p] "r" (p),
      [f] "r" (fn)
  );
#endif

  return ret;
}
#pragma GCC pop_options

int main() {
  union { void *p; int i; } r1, r2, a[3];

  r1.p = (void *)0;
  r2.p = (void *)0;

  a[0].p = (void *)0;
  a[1].p = (void *)0;
  a[2].p = (void *)0;

  a[0].i = 3;
  a[1].i = 4;
  a[2].i = 5;

  printf("First call...\n");fflush(stdout);
  r1.p = x86_call((void(*)())get_sum, 2, &a[0].p);
  printf("Result: %i\n", r1.i);fflush(stdout);

  printf("Second call...\n");fflush(stdout);
  r2.p = x86_call((void(*)())average, 3, &a[0].p);
  printf("Result: %i\n", r2.i);fflush(stdout);

  return 0;
}
