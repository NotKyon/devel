/*

	This file implements the os() and arch() macros (from Swift) for C and C++
	
	Swift doesn't have a preprocessor, but it does have conditional compilation.

	Built-in macros:

		FUNCTION    VALID ARGUMENTS
		os()        OSX, iOS
		arch()      x86_64,arm,arm64,i386

	General Syntax:

		#if build configuration && !build configuration
			statements
		#elseif build configuration
			statements
		#else
			statements
		#endif

	Example:
	
		#if os(OSX)
		# include <OpenGL/OpenGL.h>
		#else
		# include <GL/gl.h>
		#endif

*/

#if defined( _WIN32 )
# if defined( __WINRT__ )
#  define os_Windows_active__ 0
#  define os_WinRT_active__ 1
# else
#  define os_Windows_active__ 1
#  define os_WinRT_active__ 0
# endif
#endif

#if defined( __linux__ ) || defined( __linux ) || defined( linux )
# if defined( ANDROID )
#  define os_Android_active__ 1
# else
#  define os_Android_active__ 0
# endif
# define os_Linux_active__ 1
#else
# define os_Linux_active__ 0
#endif

#if defined( __APPLE__ )
# if defined( IPHONE ) || defined( IOS ) || defined( IPHONE_SIMULATOR )
#  define os_OSX_active__ 0
#  define os_iOS_active__ 1
# else
#  define os_OSX_active__ 1
#  define os_iOS_active__ 0
# endif
#else
# define os_OSX_active__ 0
# define os_iOS_active__ 0
#endif

#if defined( __i386__ ) || defined( _M_X86 )
# define arch_i386_active__ 1
#else
# define arch_i386_active__ 0
#endif
#if defined( __x86_64__ ) || defined( __amd64__ ) || defined( _M_X64 )
# define arch_x86_64_active__ 1
#else
# define arch_x86_64_active__ 0
#endif

#define os(x) os_##x##_active__
#define arch(x) arch_##x##_active__

/*
//
//	EXAMPLE
//
*/

#include <stdio.h>
#include <stdlib.h>

#if os(OSX)
/* OS X code */
static const char *const g_pszOperatingSystem = "OSX";
#elif os(Windows)
/* Windows code */
static const char *const g_pszOperatingSystem = "Windows";
#else
static const char *const g_pszOperatingSystem = "Unknown (Unix or Linux)";
#endif

#if arch(x86_64)
static const char *const g_pszArchitecture = "x86-64";
#else
static const char *const g_pszArchitecture = "Unknown (probably x86)";
#endif


int main()
{
	printf( "os   : <%s>\n", g_pszOperatingSystem );
	printf( "arch : <%s>\n", g_pszArchitecture );

	return EXIT_SUCCESS;
}
