#ifndef LOCAL_H
#define LOCAL_H

#if _MSC_VER > 1000
# pragma once
#endif

#if _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif

#include <gamebase.h>

#if __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------- */

/* Event Management (Implemented by platforms) */
#if _WIN32
gb_bool_t gblocalTranslateMsg_win32(gb_event_t *ev, const MSG *msg);
#endif
gb_bool_t gblocalPollEvent();
gb_bool_t gblocalWaitEvent();

/* Event Management (Provided) */
void gblocalInitEventQueue();

/* Threading */
struct jobStream_s;

void gblocalInitThreading();
void gblocalFiniThreading();
struct jobStream_s *gblocalJobStream();

/* File System */
void gblocalInitVFS();
void gblocalFiniVFS();

/* -------------------------------------------------------------------------- */

#if __cplusplus
}
#endif

#endif
