/*	@@BUG@@ LINE 76; change from False to True
	SCRIPTING FOR NUCLEAR BASIC
	===========================

	; comment

	command-name parm1 parm2

	command-name (command) parm2 (command parm1 parm2)

	print "Hello, world!"

	function get-sum (x y) (+ x y)

	function get-diff (x y) (
		(- x y)
	)
*/

[DefineVars=True]

Const TT_EOF = 0
Const TT_NAME = 1
Const TT_STRLIT = 2
Const TT_NUMLIT = 3
Const TT_LPAREN = 4
Const TT_RPAREN = 5

Global Scr__kSemicolon% = Asc( ";" )
Global Scr__kLowerA% = Asc( "a" )
Global Scr__kLowerZ% = Asc( "z" )
Global Scr__kUpperA% = Asc( "A" )
Global Scr__kUpperZ% = Asc( "Z" )
Global Scr__kDigit0% = Asc( "0" )
Global Scr__kDigit9% = Asc( "9" )
Global Scr__kLF% = 10
Global Scr__kCR% = 13
Global Scr__kLParen% = Asc( "(" )
Global Scr__kRParen% = Asc( ")" )
Global Scr__kQuote% = 34
Global Scr__kDot% = Asc( "." )

Type TScriptSource
	Field File$
	Field Text$

	Field uOffset%
	Field cLength%

	Field Tok_Head.TScriptToken
	Field Tok_Tail.TScriptToken
	Field Tok_Curr.TScriptToken

	Field Str_Root.TScriptString

	Function Make()
		This\File = ""
		This\Text = ""

		This\uOffset = 0
		This\cLength = 0

		This\Tok_Head = Null
		This\Tok_Tail = Null
		This\Tok_Curr = Null

		This\Str_Root = Null
	EndFunction

	Function Kill()
		Local i = 0
		While This\Tok_Head <> Null
			Print "Loop " + Str( i ) + " (Tok_Head = " + Str( Handle( This\Tok_Head ) ) + ")"
			i = i + 1

			If False // Set to True for 「NB-BUG: Destructor does not function properly」
				Delete This\Tok_Head
			Else
				Scr__KillTok( This\Tok_Head )
			EndIf
		Wend
	EndFunction
EndType
Type TScriptString
	Field Value$

	Field Str_Next.TScriptString
EndType

Type TScriptToken
	// One of the TT_ constants (e.g., TT_NAME)
	Field Kind%
	// Index into the script source for the lexan's starting character
	Field uLexanStart%
	// Index into the script source for one past the lexan's ending character
	Field uLexanEnd%
	// Reference to the script source
	Field Source.TScriptSource

	Field Str_Value.TScriptString

	Field Tok_Prev.TScriptToken
	Field Tok_Next.TScriptToken

	Function Kill()
		Scr__KillTok( This )
	EndFunction

	Function ToString$()
		If This\Source = Null Or This\uLexanStart = This\uLexanEnd Then Return ""

		Return Mid( This\Source\Text, This\uLexanStart, This\uLexanEnd - This\uLexanStart )
	EndFunction
EndType

Function Scr__TokAlloc.TScriptToken( Src.TScriptSource, InTokKind% = TT_EOF, InTokLen% = 0 )
	Local Tok.TScriptToken = New TScriptToken

	Tok\Kind = InTokKind

	Tok\uLexanStart = Src\uOffset
	Tok\uLexanEnd = Src\uOffset + InTokLen

	Tok\Source = Src
	Tok\Tok_Prev = Src\Tok_Tail
	Tok\Tok_Next = Null
	If Src\Tok_Tail <> Null
		Src\Tok_Tail\Tok_Next = Tok
	Else
		Src\Tok_Head = Tok
	EndIf
	Src\Tok_Tail = Tok

	Print "Tok=" + Str(Handle(Tok)) + " (Prev="+Str(Handle(Tok\Tok_Prev)) + " Next="+Str(Handle(Tok\Tok_Next)) +") Head="+Str(Handle(Src\Tok_Head)) + " Tail="+Str(Handle(Src\Tok_Tail))

	Return Tok
EndFunction
Function Scr__StrAlloc.TScriptString( Src.TScriptSource )
	Local S.TScriptString = New TScriptString

	S\Str_Next = Src\Str_Root
	Src\Str_Root = S

	Return S
EndFunction

Function Scr__KillTok( Tok.TScriptToken )
	Print "[tok-kill tok=" +Str(Handle(Tok))+ " src="+Str(Handle(Tok\Source))+" next="+Str(Handle(Tok\Tok_Next))+"]"

	If Tok\Source = Null Then Print "WTF" : Return

	If Tok\Tok_Prev <> Null
		Print "A"
		Tok\Tok_Prev\Tok_Next = Tok\Tok_Next
	Else
		Print "B"
		Tok\Source\Tok_Head = Tok\Tok_Next
	EndIf

	If Tok\Tok_Next <> Null
		Print "C"
		Tok\Tok_Next\Tok_Prev = Tok\Tok_Prev
	Else
		Print "D"
		Tok\Source\Tok_Tail = Tok\Tok_Prev
	EndIf

	If Tok\Str_Value <> Null Then Delete Tok\Str_Value

	Local Src.TScriptSource = Tok\Source
	Print "Tok=" + Str(Handle(Tok)) + " (Prev="+Str(Handle(Tok\Tok_Prev)) + " Next="+Str(Handle(Tok\Tok_Next)) +") Head="+Str(Handle(Src\Tok_Head)) + " Tail="+Str(Handle(Src\Tok_Tail))
EndFunction

Function Scr__Asc%( Src.TScriptSource )
	Return Asc( Mid( Src\Text, Src\uOffset ) )
EndFunction
Function Scr__AscX%( Src.TScriptSource, InOffset% )
	Return Asc( Mid( Src\Text, Src\uOffset + InOffset ) )
EndFunction
Function Scr__Inc( Src.TScriptSource )
	Src\uOffset = Src\uOffset + 1
EndFunction

Function Scr__SkipLine( Src.TScriptSource )
	Local c%
	Local bHaveLine% = False

	While Src\uOffset < Src\cLength
		c = Scr__Asc( Src )
		If c = Scr__kCR
			Scr__Inc( Src )
			c = Scr__Asc( Src )
			bHaveLine = True
		EndIf

		If c = Scr__kLF
			Scr__Inc( Src )
			bHaveLine = True
		EndIf

		If bHaveLine
			Break
		EndIf

		Scr__Inc( Src )
	Wend
EndFunction

Function Scr__Unlex( Src.TScriptSource )
	If Src\Tok_Curr = Null
		Src\Tok_Curr = Src\Tok_Tail
	ElseIf Src\Tok_Curr <> Src\Tok_Head
		Src\Tok_Curr = Src\Tok_Curr\Tok_Prev
	EndIf
EndFunction
Function Scr__Lex.TScriptToken( Src.TScriptSource )
	Local Tok.TScriptToken
	Local c%
	Local bSkippedSpace%
	Local i%

	If Src\Tok_Curr <> Null
		Tok = Src\Tok_Curr
		Src\Tok_Curr = Src\Tok_Curr\Tok_Next
		Return Tok
	EndIf

	If Src\Tok_Tail <> Null And Src\Tok_Tail\Kind = TT_EOF
		Return Src\Tok_Tail
	EndIf

	Repeat
		bSkippedSpace = False

		c = Scr__Asc( Src )
		If c <= 32 And c >= 0
			Scr__Inc( Src )
		EndIf

		If c = Scr__kSemicolon
			bSkippedSpace = True
			Scr__SkipLine( Src )
		EndIf
	Until bSkippedSpace = False Or Src\uOffset = Src\cLength

	Tok = Scr__TokAlloc( Src )

	If Src\uOffset = Src\cLength
		Return Tok
	EndIf

	c = Scr__Asc( Src )

	If c = Scr__kLParen
		Scr__Inc( Src )

		Tok\Kind = TT_LPAREN
		Tok\uLexanEnd = Src\uOffset

		Return Tok
	EndIf

	If c = Scr__kRParen
		Scr__Inc( Src )

		Tok\Kind = TT_RPAREN
		Tok\uLexanEnd = Src\uOffset

		Return Tok
	EndIf

	If c = Scr__kQuote
		Scr__Inc( Src )

		i = 0
		While Scr__AscX( Src, i ) <> Scr__kQuote And Src\uOffset + i < Src\cLength
			i = i + 1
		Wend

		Tok\Str_Value = Scr__StrAlloc( Src )
		Tok\Str_Value\Value = Mid( Src\Text, Src\uOffset, i )

		Src\uOffset = Src\uOffset + i
		If Src\uOffset < Src\cLength
			Src\uOffset = Src\uOffset + 1
		EndIf

		Tok\uLexanEnd = Src\uOffset

		Tok\Kind = TT_STRLIT
		Return Tok
	EndIf

	If c >= Scr__kDigit0 And c <= Scr__kDigit9
		Scr__Inc( Src )

		i = 0
		Do
			c = Scr__AscX( Src, i )
			If c < Scr__kDigit0 Or c > Scr__kDigit9
				Break
			EndIf

			i = i + 1
		Loop

		If c = Scr__kDot
			c = Scr__AscX( Src, i + 1 )
			If c >= Scr__kDigit0 And c <= Scr__kDigit9
				i = i + 2
				Do
					c = Scr__AscX( Src, i )
					If c < Scr__kDigit0 Or c > Scr__kDigit9
						Break
					EndIf
				Loop
			EndIf
		EndIf

		Src\uOffset = Src\uOffset + i
		Tok\uLexanEnd = Src\uOffset

		Tok\Kind = TT_NUMLIT
		Return Tok
	EndIf

	While ( c < 0 Or c > 32 ) And c <> Scr__kLParen And c <> Scr__kRParen And Src\uOffset < Src\cLength
		Scr__Inc( Src )
		c = Scr__Asc( Src )
	Wend

	Tok\uLexanEnd = Src\uOffset
	Tok\Kind = TT_NAME
	Return Tok
EndFunction

Function Scr__LoadFile%( Src.TScriptSource, InFilename$ )
	Local nl$ = Chr( 10 )
	Local f%

	f = ReadFile( InFilename, False )
	If Not f
		Return False
	EndIf

	Src\File = InFilename

	While Not Eof( f )
		Src\Text = Src\Text + ReadLine( f ) + nl
	Wend

	CloseFile( f )

	Src\uOffset = 0
	Src\cLength = Len( Src\Text )

	Return True
EndFunction

Function Scr__TestLexer%()
	Local Src.TScriptSource
	Local Tok.TScriptToken

	Src = New TScriptSource

	If Not Scr__LoadFile( Src, "Script_Test.txt" )
		Delete Src
		Return False
	EndIf

	Do
		Tok = Scr__Lex( Src )
		If Not Tok Then Delete Src : Return False
		If Tok\Kind = TT_EOF Then Break

		Print "  " + Str( Tok\Kind ) + " <" + Tok\ToString() + ">"
	Loop

	Print "Lolicon"
	Delete Src
	Return True
EndFunction

MakeConsole()

Scr__TestLexer()

Print "Press any key to exit..."
SetConsoleWaitForKey()
End
