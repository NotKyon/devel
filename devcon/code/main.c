#include <GL/glfw.h>
#include <stdlib.h>
#include <math.h>

#include "local.h"

/*
 * TODO: Move all GLFW specific stuff to a GLFW specific file. (glfw.c)
 *       Move all "game"-specific stuff to a game specific file. (game.c)
 */

/*
 * GLFW Specific
 */
void ReshapeFunc(int width, int height) {
#if 0
	glViewport(0, 0, width, height);
#else
	sys_Log("ReshapeFunc: %i,%i\n", width, height);
	ev_New(kEvent_ViewSize, width, height);
	sys_Log("Exiting ReshapeFunc\n");
#endif
}

void KeyboardFunc(int key, int state) {
#if 0
	/* developer console activated with tilde keypress ('`'/'~') */
	if (key==96 && state==GLFW_RELEASE) {
		if (con_IsVisible())
			con_Hide();
		else
			con_Show();

		return;
	}

	/* escape key to exit */
	if (key==257 && state==GLFW_RELEASE) {
		glfwCloseWindow();
		return;
	}
#else
	ev_New(state==GLFW_PRESS ? kEvent_KeyDown : kEvent_KeyUp, key, 0);
#endif
}

/*
 * Game Specific
 */

void KeyEvent() {
	/* show the console */
	if (ev_X()==DEVCON_KEY && ev_Code()==kEvent_KeyUp) {
		con_Show();
		return;
	}

	/* exit the app */
	if (ev_X()==257/*esc*/ && ev_Code()==kEvent_KeyUp) {
		glfwCloseWindow();
		return;
	}
}

void MouseEvent() {
}

int main() {
	double newtime, oldtime, time;
	double angle;

	if (!glfwInit())
		return EXIT_FAILURE;

	atexit(glfwTerminate);

	ev_Init();
	atexit(ev_Deinit);

	sys_Log("Test\n");

	if (!glfwOpenWindow(640, 480, 8,8,8,0, 0,0, GLFW_WINDOW))
		exit(EXIT_FAILURE);

	glfwSetKeyCallback(KeyboardFunc);
	glfwSetWindowSizeCallback(ReshapeFunc);

	sys_Log("Hi!\n");

#if 0
	if (!InitConsoleFont()) {
		sys_Error("Failed to create console font!\n");
		exit(EXIT_FAILURE);
	}
#endif

	if (!cvar_Init())
		exit(EXIT_FAILURE);

	atexit(cvar_Deinit);

	sys_Log("cvar_Init() finished\n");

#if 0
	if (!tex_Init())
		exit(EXIT_FAILURE);

	atexit(tex_Deinit);
#endif

	con_Init();
	atexit(con_Deinit);

	sys_Log("Test 2 (this should appear in the dev console)\nExtra test ");
	sys_Log("... Complete!\n");

	cmd_Add("set r_fullscreen true");
	cmd_Add("r_fullscreen false");
	cmd_Add("r_fullscreen");
	cmd_Add("r_multiSamples 4");
	cmd_Add("set g_playerName \"Bob\"");
	cmd_Add("set g_serverName Server");
	cmd_Add("g_playerName");
	cmd_Add("g_serverName");

	if (!cmd_Execute())
		sys_Error("Failed to execute command buffer");

	newtime = glfwGetTime();

	angle = 0.0;

	while(glfwGetWindowParam(GLFW_OPENED)) {
		oldtime = newtime;
		newtime = glfwGetTime();
		time = newtime - oldtime;

		ev_Next();
		switch(ev_Tag()) {
		case kEventTag_Keyboard:
			if (con_IsVisible())
				con_KeyEvent();
			else
				KeyEvent();

			break;

		case kEventTag_Mouse:
			if (con_IsVisible())
				con_MouseEvent();
			else
				MouseEvent();

			break;

		case kEventTag_View:
			if (ev_Code()==kEvent_ViewSize) {
				sys_Log("kEvent_ViewSize: %i,%i\n", ev_X(), ev_Y());
				v_Resize(ev_X(), ev_Y());
			}
			break;

		default:
			break;
		}

		r_Frame(time);
	}

	return 0;
}
