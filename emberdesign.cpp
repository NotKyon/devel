#if EE_RUN_MODE
# define EE_USRVAR
#else
# define EE_USRVAR extern "C" extern
#endif

#ifndef NULL
# define NULL 0
#endif


//==============================================================================
//
//	FILE SYSTEM
//
//==============================================================================

class eeFile {
public:
	inline virtual ~eeFile() {}

	virtual void Close() = 0;
};
class eeDir {
public:
	inline virtual ~eeDir() {}

	virtual void Close() = 0;

	virtual const char *FindFirst(const char *pattern="*") = 0;
	virtual const char *FindNext() = 0;

	virtual const char *GetSearchPattern() const = 0;
};
class eeFileSystem  {
public:
	inline virtual ~eeFileSystem() {}

	virtual eeFile *OpenFile(const char *filename) = 0;
	virtual eeDir *OpenDir(const char *dirname) = 0;

	virtual bool EnterDir(const char *dirname) = 0;
	virtual void ExitDir() = 0;

	virtual bool SetDir(const char *dirname) = 0;
	virtual const char *GetDir() const = 0;

	inline void CloseFile(eeFile *file) { EE_ASSERT(file != 0); file->Close(); }
	inline void CloseDir(eeDir *dir) { EE_ASSERT(dir != 0); dir->Close(); }
};

EE_USRVAR eeFileSystem *FS;

inline eeFile *fs_OpenFile(const char *filename) {
	return FS->OpenFile(filename);
}
inline eeDir *fs_OpenDir(const char *dirname) {
	return FS->OpenDir(dirname);
}
inline bool fs_EnterDir(const char *dirname) {
	return FS->EnterDir(dirname);
}
inline void fs_ExitDir() {
	return FS->ExitDir();
}
inline bool fs_SetDir(const char *dirname) {
	return FS->SetDir(dirname);
}
inline const char *fs_GetDir() {
	return FS->GetDir();
}
inline void fs_CloseFile(eeFile *file) {
	return FS->CloseFile(file);
}
inline void fs_CloseDir(eeDir *dir) {
	return FS->CloseDir(dir);
}
inline const char *fs_FindFirst(eeDir *dir, const char *pattern="") {
	EE_NOTNULL(dir);

	return dir->FindFirst(pattern);
}
inline const char *fs_FindNext(eeDir *dir) {
	EE_NOTNULL(dir);

	return dir->FindNext();
}
inline const char *fs_GetSearchPattern(eeDir *dir) {
	EE_NOTNULL(dir);

	return dir->GetSearchPattern();
}

//==============================================================================
//
//	TEXT BUFFER
//
//==============================================================================

class eeTextBuffer {
public:
	inline virtual ~eeTextBuffer() {}

	virtual bool LoadFromString(const char *filename, const char *source) = 0;
	virtual bool LoadFromFile(const char *filename) = 0;

	virtual void Close() = 0;

	virtual const char *GetFilename() const = 0;
	virtual const char *GetSource() const = 0;
	virtual size_t GetLine() const = 0;
	virtual size_t GetIndex() const = 0;
	virtual size_t GetLength() const = 0;

	virtual void SetFunction(const char *func) = 0;
	virtual const char *GetFunction() const = 0;

	virtual char Peek() = 0;
	virtual char Pull() = 0;
	virtual char Eat() = 0;
	virtual char Unpull() = 0;

	virtual size_t SetIndex(size_t index) = 0;
	virtual bool IsEnd() const = 0;

	virtual void SkipWhitespace() = 0;
	virtual void SkipLine() = 0;
	virtual bool ReadLine(char *dst, size_t dstn) = 0;

	virtual void ReportBufferV(const char *type, const char *format,
	va_list args) = 0;
	virtual void ReportBuffer(const char *type, const char *format, ...) = 0;

	virtual void Error(const char *format, ...) = 0;
	virtual void Warn(const char *format, ...) = 0;
	virtual void Note(const char *format, ...) = 0;
};
