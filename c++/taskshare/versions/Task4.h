#ifndef TASK_H
#define TASK_H

#include "../TaskShare.h"

//------------------------------------------------------------------------------
// JOB QUEUE

#define TASK_JOBQUEUE_VERSION 4

__TS_NS_INIT__

typedef struct token_s {
	union { jobFunc_t fn; void *fnp; };
	void *p;
} token_t;
typedef struct queue_s {
	enum {
#if !TASK_CONF_BUFFER_SIZE
# undef  TASK_CONF_BUFFER_SIZE
# define TASK_CONF_BUFFER_SIZE 4096
#endif
		kBufferSize = TASK_CONF_BUFFER_SIZE
	};

	token_t buffer[kBufferSize];

	volatile u32 numStreamers;

	volatile u32 insertionIndex;
	volatile u32 streamingIndex;
	volatile u32 numPendingJobs;

#if !defined(TASK_CONF_LOCKFREE)
# define TASK_CONF_LOCKFREE 1
#endif
#if !TASK_CONF_LOCKFREE
	lock_t lock;
#endif
} queue_t;

inline void InitQueue(queue_t &q);
inline void FiniQueue(queue_t &q);

inline void EnqueueToken(queue_t &q, const token_t &t);
inline void Schedule(queue_t &q, jobFunc_t fn, void *p);
inline void ScheduleTermination(queue_t &q);

inline void Streamer(queue_t &q);

inline void SyncQueue(queue_t &q);
inline void SyncQueueActive(queue_t &q);

inline void InitQueue(queue_t &q) {
	memset((void *)q.buffer, 0, sizeof(q.buffer));
	q.numPendingJobs = 0;
	q.insertionIndex = 0;
	q.streamingIndex = 0;
	q.numStreamers   = 0;
#if !TASK_CONF_LOCKFREE
	InitLock(q.lock);
#endif
}
inline void FiniQueue(queue_t &q) {
	ScheduleTermination(q);
	//fflush(stdout);
	while(q.numStreamers > 0)
		Relinquish();
#if !TASK_CONF_LOCKFREE
	FiniLock(q.lock);
#endif
}

inline void EnqueueToken(queue_t &q, const token_t &t) {
	u32 index;

	if (t.fn==(jobFunc_t)0)
		return;

#if !TASK_CONF_LOCKFREE
	Lock(q.lock);
		q.numPendingJobs++;

		index = q.insertionIndex++ % queue_t::kBufferSize;
	Unlock(q.lock);

	while(q.buffer[index].fnp != (void *)0)
		Relinquish();

	Lock(q.lock);
		q.buffer[index].p  = t.p ;
		q.buffer[index].fn = t.fn;
	Unlock(q.lock);
#else
	// indicate an extra job is available
	AtomicIncrement(&q.numPendingJobs);

	// find the next index
	index = AtomicIncrement(&q.insertionIndex) % queue_t::kBufferSize;

	// temporary value indicating "do not run until parameter is nonzero"
	while(1) {
		if (AtomicExchangeIfEqual(&q.buffer[index].fnp,
		(void *)3, (void *)0)==(void *)3)
			break;
	}

	// set the parameter first, then the function pointer
	AtomicExchange(&q.buffer[index].p, t.p);
	AtomicExchange(&q.buffer[index].fnp, t.fnp);
#endif
}
inline void Schedule(queue_t &q, jobFunc_t fn, void *p) {
	token_t t;

	t.fn = fn;
	t.p  = p ;

	EnqueueToken(q, t);
}
inline void ScheduleTermination(queue_t &q) {
	token_t t;
	u32 i;

	t.fnp = (void *)1;
	t.p   = (void *)0;

#if !TASK_CONF_LOCKFREE
	Lock(q.lock);
	for(i=0; i<queue_t::kBufferSize; i++)
		q.buffer[i] = t;
	Unlock(q.lock);
#else
	for(i=0; i<queue_t::kBufferSize; i++)
		AtomicExchange(&q.buffer[i].fnp, (void *)1);
#endif
}

inline void Streamer(queue_t &q) {
	jobFunc_t fn;
	u32 index;

#if !TASK_CONF_LOCKFREE
	Lock(q.lock);
#endif
	//printf("++streamers\n");
	//q.numStreamers++;
	AtomicIncrement(&q.numStreamers);
#if !TASK_CONF_LOCKFREE
	//Unlock(q.lock);
#endif

	do {
#if !TASK_CONF_LOCKFREE
		//Lock(q.lock);
			index = q.streamingIndex++ % queue_t::kBufferSize;
		Unlock(q.lock);
#else
		index = AtomicIncrement(&q.streamingIndex) % queue_t::kBufferSize;
#endif

		while(q.buffer[index].fnp == (void *)0)
			Relinquish();

		if (q.buffer[index].fnp==(void *)1)
			break;

		if (q.buffer[index].fnp==(void *)2)
			continue;

#if TASK_CONF_LOCKFREE
		// parameter isn't ready yet?
		while(AtomicRead(&q.buffer[index].fnp)==(void *)3)
			Relinquish();
#endif
		fn = q.buffer[index].fn;
		AtomicExchange(&q.buffer[index].fnp, (void *)2);

		fn(q.buffer[index].p);

		//NOTE: no need to set 'p'
#if !TASK_CONF_LOCKFREE
		Lock(q.lock);
			q.buffer[index].fnp = (void *)0;
			q.numPendingJobs--;
		//Unlock(q.lock);
#else
		AtomicExchange(&q.buffer[index].fnp, (void *)0);
		AtomicDecrement(&q.numPendingJobs);
#endif
	} while(1);

	//
	//	TODO: This lock is unnecessary; remove it.
	//
#if !TASK_CONF_LOCKFREE
	Lock(q.lock);
#endif
	//printf("--streamers\n");
	AtomicDecrement(&q.numStreamers);
#if !TASK_CONF_LOCKFREE
	Unlock(q.lock);
#endif
}

inline void SyncQueue(queue_t &q) {
	while(q.numPendingJobs)
		Relinquish();
}
inline void SyncQueueActive(queue_t &q) {
	while(q.numPendingJobs) {
	}
}

__TS_NS_FINI__

//------------------------------------------------------------------------------

#endif
