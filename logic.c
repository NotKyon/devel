/*
//
//	LOGIC GATES TESTING PLATFORM
//
//	Use only bitwise operations (AND, OR, XOR, NAND, NOR, XNOR) to create logic.
//	Test against their equivalent C code.
//
*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* -------------------------------------------------------------------------- */

#define AND(x,y)  ((x)&(y))
#define OR(x,y)   ((x)|(y))
#define XOR(x,y)  ((x)^(y))
#define NOT(x)    (!(x))
#define NAND(x,y) (NOT(AND((x),(y))))
#define NOR(x,y)  (NOT(OR((x),(y))))
#define XNOR(x,y) (NOT(XOR((x),(y))))

/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */

enum {
	kInstrOp_Rd,        /* read an external input (a*4 + b) to accumulator */
	kInstrOp_Wr,        /* write an external input (a*4 + b) from accumulator */

	/* a = a <op> b; first parameter is destination and source */
	kInstrOp_And,       /* a = AND(a,b) */
	kInstrOp_Or,        /* a = OR(a,b) */
	kInstrOp_Xor,       /* a = XOR(a,b) */
	kInstrOp_Not,       /* a = NOT(a,b) */
	kInstrOp_Nand,      /* a = NAND(a,b) */
	kInstrOp_Nor,       /* a = NOR(a,b) */
	kInstrOp_Xnor,      /* a = XNOR(a,b) */

	/* set accumulator instead of first parameter */
	kInstrOp_AcAnd,     /* A = AND(a,b) */
	kInstrOp_AcOr,      /* A = OR(a,b) */
	kInstrOp_AcXor,     /* A = XOR(a,b) */
	kInstrOp_AcNot,     /* A = NOT(a) */
	kInstrOp_AcNand,    /* A = NAND(a,b) */
	kInstrOp_AcNor,     /* A = NOR(a,b) */
	kInstrOp_AcXnor     /* A = XNOR(a,b) */
};
enum {
	kInstrReg_A,
	kInstrReg_X,
	kInstrReg_Y,
	kInstrReg_Z
};
typedef struct instr_s {
	unsigned op:4;
	unsigned a:2;
	unsigned b:2;
} instr_t;
typedef struct str_s {
	char s[32];
} str_t;
typedef struct cpu_s {
	size_t romCnt, romCap;
	instr_t *romDat;

	size_t pc;

	str_t inputs[16];
	str_t outputs[16];

	unsigned rdSig:16;
	unsigned wrSig:16;

	unsigned regs:4;
	unsigned rsvd:4;
} cpu_t;

cpu_t *gCPU = (cpu_t *)0;

void init_cpu() {
	size_t i;

	gCPU = (cpu_t *)malloc(sizeof(cpu_t));
	if (!gCPU) {
		fprintf(stderr, "Error: %s (%i)\n", strerror(errno), errno);
		exit(EXIT_FAILURE);
	}

	gCPU->romCnt = 0;
	gCPU->romCap = 0;
	gCPU->romDat = (instr_t *)0;

	gCPU->pc = 0;

	for(i=0; i<16; i++) {
		gCPU->inputs[i].s[0] = '\0';
		gCPU->outputs[i].s[0] = '\0';
	}

	gCPU->rdSig = 0;
	gCPU->wrSig = 0;

	gCPU->regs = 0;
	gCPU->rsvd = 0;
}
void fini_cpu() {
	if (!gCPU)
		return;

	if (gCPU->romDat) {
		free((void *)gCPU->romDat);
		gCPU->romDat = (instr_t *)0;
	}

	gCPU->romCnt = 0;
	gCPU->romCap = 0;
	gCPU->romDat = (instr_t *)0;

	gCPU->pc = 0;

	free((void *)gCPU);
	gCPU = (cpu_t *)0;
}
void reset_cpu() {
	if (gCPU->romDat) {
		free((void *)gCPU->romDat);
		gCPU->romDat = (instr_t *)0;
	}

	gCPU->romCnt = 0;
	gCPU->romCap = 0;
	gCPU->romDat = (instr_t *)0;

	gCPU->pc = 0;

	for(i=0; i<16; i++) {
		gCPU->inputs[i].s[0] = '\0';
		gCPU->outputs[i].s[0] = '\0';
	}

	gCPU->rdSig = 0;
	gCPU->wrSig = 0;

	gCPU->regs = 0;
	gCPU->rsvd = 0;
}
instr_t *add_op() {
	instr_t *p;
	size_t n;

	if (gCPU->romCnt + 1 > gCPU->romCap) {
		gCPU->romCap += 16;
		n = gCPU->romCap*sizeof(instr_t);

		p = (instr_t *)malloc(n);
		if (!p) {
			fprintf(stderr, "Error: %s (%i)\n", strerror(errno), errno);
			exit(EXIT_FAILURE);
		}

		memcpy((void *)p, (const void *)gCPU->romDat,
			sizeof(instr_t)*gCPU->romCnt);

		free((void *)gCPU->romDat);
		gCPU->romDat = p;
	}

	return &gCPU->romDat[gCPU->romCnt++];
}
void emit_op(int type, int reg_a, int reg_b) {
	instr_t *p;

	p = add_op();

	p->op = type;
	p->a = reg_a;
	p->b = reg_b;
}
void emit_io(int type, int port) {
	instr_t *p;

	p = add_op();

	p->op = type;
	p->a = port/4;
	p->b = port%4;
}

void wr_rd(int port, int value) {
	gCPU->rdSig &= ~(1<<port);
	gCPU->rdSig |= (value<<port);
}
int rd_rd(int port) {
	return gCPU->rdSig & (1<<port);
}

void wr_wr(int port, int value) {
	gCPU->wrSig &= ~(1<<port);
	gCPU->wrSig |= (value<<port);
}
int rd_wr(int port) {
	return gCPU->wrSig & (1<<port);
}

void wr_reg(int reg, int value) {
	gCPU->regs &= ~(1<<reg);
	gCPU->regs |= (value<<reg);
}
int rd_reg(int reg) {
	return gCPU->regs & (1<<reg);
}

int step_cpu() {
	instr_t instr;

	if (gCPU->pc >= gCPU->romCnt) {
		printf("Program finished.\n");
		return 0;
	}

	instr = gCPU->romDat[gCPU->pc++];

	switch(instr.op) {
	case kInstrOp_Rd:
		wr_reg(kInstrReg_A, rd_rd(instr.a*4 + instr.b));
		break;
	case kInstrOp_Wr:
		wr_wr(instr.a*4 + instr.b, rd_reg(kInstrReg_A));
		break;

	case kInstrOp_And:
		wr_reg(instr.a, AND(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_Or:
		wr_reg(instr.a, OR(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_Xor:
		wr_reg(instr.a, XOR(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_Not:
		wr_reg(instr.a, NOT(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_Nand:
		wr_reg(instr.a, NAND(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_Nor:
		wr_reg(instr.a, NOR(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_Xnor:
		wr_reg(instr.a, XNOR(rd_reg(instr.a), rd_reg(instr.b)));
		break;

	case kInstrOp_AcAnd:
		wr_reg(kInstrReg_A, AND(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_AcOr:
		wr_reg(kInstrReg_A, OR(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_AcXor:
		wr_reg(kInstrReg_A, XOR(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_AcNot:
		wr_reg(kInstrReg_A, NOT(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_AcNand:
		wr_reg(kInstrReg_A, NAND(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_AcNor:
		wr_reg(kInstrReg_A, NOR(rd_reg(instr.a), rd_reg(instr.b)));
		break;
	case kInstrOp_AcXnor:
		wr_reg(kInstrReg_A, XNOR(rd_reg(instr.a), rd_reg(instr.b)));
		break;

	default:
		return -1;
	}

	return 1;
}

/* -------------------------------------------------------------------------- */

int main() {
	printf("LOGIC - 1.0\n");
	printf("READY\n");

	return EXIT_SUCCESS;
}
