#ifndef EMBER_EFFECT_H
#define EMBER_EFFECT_H

#include <ember/config.h>
#include <ember/gc.h>
#include <ember/buffer.h>

/*
 * Effects represent how an entity should be rendered. They're composed of
 * techniques, which are themselves composed of passes. Each of these layers
 * contain various renderer settings.
 */
#ifndef __effect_t_defined__
#define __effect_t_defined__ 1
typedef struct stEffect *effect_t;
#endif
struct stEffect {
	char *name;

	struct stProperty *pr_head, *pr_tail;
	struct stGroup *gr_head, *gr_tail;
	struct stTechnique *tc_head, *tc_tail;

	struct stEffect *fx_prev, *fx_next;
};
__em_extrn gc_pool_t g_fx_pool __em_nil();
__em_extrn effect_t g_fx_head __em_nil(), g_fx_tail __em_nil();

/*
 * Properties represent variables within the effect that have to be filled by
 * the engine.
 */
#ifndef __property_t_defined__
#define __property_t_defined__ 1
typedef struct stProperty *property_t;
#endif
typedef enum {
	EPropertyKind_Invalid,

	EPropertyKind_Range,
	EPropertyKind_Color,
	EPropertyKind_Texture2D,
	EPropertyKind_TextureRect,
	EPropertyKind_TextureCube,
	EPropertyKind_Float,
	EPropertyKind_Vector
} EPropertyKind;
typedef enum {
	EPropertyTexGen_None,

	EPropertyTexGen_ObjectLinear,
	EPropertyTexGen_EyeLinear,
	EPropertyTexGen_SphereMap,
	EPropertyTexGen_CubeReflect,
	EPropertyTexGen_CubeNormal
} EPropertyTexGen;
struct stProperty {
	char *name;
	char *desc;
	EPropertyKind kind;
	struct {
		float range[ 2 ];
		bitfield_t flags;
		EPropertyTexGen texgen;
	} options;
	union {
		float scalar;
		float vector[ 4 ];
		char *name;
	} value;

	struct stEffect *effect;
	struct stProperty *pr_prev, *pr_next;
};
__em_extrn gc_pool_t g_fx_pr_pool __em_nil();

/*
 * Groups are the components of techniques that actually contain the settings
 * for rendering.
 */
#ifndef __group_t_defined__
#define __group_t_defined__ 1
typedef struct stGroup *group_t;
#endif
struct stGroup {
	struct stEffect *effect;

	struct stGroup *gr_prev, *gr_next;
};
__em_extrn gc_pool_t g_fx_group_pool __em_nil();

/*
 * Tags represent special additional information about a effect or a portion of
 * a effect.
 */
#ifndef __tag_t_defined__
#define __tag_t_defined__ 1
typedef struct stTagOwner *tagowner_t;
typedef struct stTag *tag_t;
#endif
struct stTagOwner {
	struct stTag *tg_head, *tg_tail;
};
struct stTag {
	char *name;
	char *value;

	struct stTagOwner *owner;
	struct stTag *tg_prev, *tg_next;
};
__em_extrn gc_pool_t g_fx_tag_pool __em_nil();

/*
 * Techniques contain groups and passes. The groups don't have to be iterated to
 * properly render the technique... Only passes have to be.
 */
#ifndef __technique_t_defined__
#define __technique_t_defined__ 1
typedef struct stTechnique *technique_t;
#endif
struct stTechnique {
	struct stTagOwner tags;

	struct stEffect *effect;
	struct stPass *ps_head, *ps_tail;

	struct stTechnique *tc_prev, *tc_next;
};
__em_extrn gc_pool_t g_fx_tech_pool __em_nil();

/*
 * Each pass represents a separate rendering operation.
 */
#ifndef __pass_t_defined__
#define __pass_t_defined__ 1
typedef struct stPass *pass_t;
#endif
struct stPass {
	char *name;

	struct stTagOwner tags;

	struct stTechnique *owner;
	struct stGroup *group;

	struct stPass *ps_prev, *ps_next;
};
__em_extrn gc_pool_t g_fx_pass_pool __em_nil();

extern int fx_init();
extern void fx_deinit();
extern effect_t fx_alloc( const char *name );
extern void fx_dealloc( effect_t effect );
extern property_t fx_alloc_property( effect_t effect, const char *name );
extern void fx_dealloc_property( property_t pr );
extern group_t fx_alloc_group( effect_t effect );
extern void fx_dealloc_group( group_t group );
extern technique_t fx_alloc_technique( effect_t effect );
extern void fx_dealloc_technique( technique_t tech );
extern pass_t fx_alloc_pass( technique_t effect, const char *name );
extern void fx_dealloc_pass( pass_t pass );

extern tagowner_t fx_technique_tagowner( technique_t tech );
extern tagowner_t fx_pass_tagowner( pass_t pass );
extern tag_t fx_alloc_tag( tagowner_t owner, const char *name );
extern void fx_dealloc_tag( tag_t tg );

extern void fx_set_name( effect_t effect, const char *name );
extern void fx_set_technique_name( technique_t tech, const char *name );
extern void fx_set_pass_name( pass_t pass, const char *name );

extern void fx_set_tag_name( tag_t tg, const char *name );
extern void fx_set_tag_value( tag_t tg, const char *value );
extern const char *fx_tag_name( tag_t tg );
extern const char *fx_tag_value( tag_t tg );

extern void fx_set_property_name( property_t pr, const char *name );
extern void fx_set_property_desc( property_t pr, const char *desc );

extern void fx_set_property_kind( property_t pr, EPropertyKind kind );

extern void fx_set_property_range( property_t pr, float l, float h );
extern void fx_set_property_flags( property_t pr, bitfield_t flags );
extern void fx_set_property_texgen( property_t pr, EPropertyTexGen texgen );
extern void fx_set_property_lightmap_mode( property_t pr, int on );

extern void fx_set_property_scalar( property_t pr, float v );
extern void fx_set_property_vector( property_t pr, float x, float y, float z, float w );
extern void fx_set_property_string( property_t pr, const char *s );

extern const char *fx_name( effect_t effect );
extern const char *fx_property_name( property_t pr );
extern const char *fx_property_desc( property_t pr );
extern const char *fx_pass_name( pass_t pass );

extern EPropertyKind fx_property_kind( property_t pr );
extern float fx_property_range( property_t pr, int i );
extern bitfield_t fx_property_flags( property_t pr );
extern EPropertyTexGen fx_property_texgen( property_t pr );
extern int fx_property_lightmap_mode( property_t pr );

extern float fx_property_range_low( property_t pr );
extern float fx_property_range_high( property_t pr );

extern float fx_property_scalar( property_t pr );

extern float fx_property_vec_x( property_t pr );
extern float fx_property_vec_y( property_t pr );
extern float fx_property_vec_z( property_t pr );
extern float fx_property_vec_w( property_t pr );

extern const char *fx_property_string( property_t pr );

extern effect_t fx_first();
extern effect_t fx_last();
extern effect_t fx_before( effect_t effect );
extern effect_t fx_after( effect_t effect );

extern property_t fx_property_first( effect_t effect );
extern property_t fx_property_last( effect_t effect );
extern property_t fx_property_before( property_t pr );
extern property_t fx_property_after( property_t pr );

extern tag_t fx_tag_first( tagowner_t owner );
extern tag_t fx_tag_last( tagowner_t owner );
extern tag_t fx_tag_before( tag_t tg );
extern tag_t fx_tag_after( tag_t tg );

extern group_t fx_group_first( effect_t effect );
extern group_t fx_group_last( effect_t effect );
extern group_t fx_group_before( group_t cat );
extern group_t fx_group_after( group_t cat );

extern technique_t fx_technique_first( effect_t effect );
extern technique_t fx_technique_last( effect_t effect );
extern technique_t fx_technique_before( technique_t tech );
extern technique_t fx_technique_after( technique_t tech );

extern pass_t fx_pass_first( technique_t tech );
extern pass_t fx_pass_tail( technique_t tech );
extern pass_t fx_pass_before( pass_t pass );
extern pass_t fx_pass_after( pass_t pass );

extern effect_t fx_find( const char *name );
extern pass_t fx_find_pass( technique_t tech, const char *name );

/* shaderlab.c */
extern int fx_sl_parse( effect_t effect, buffer_t buf );
extern effect_t fx_sl_create( buffer_t buf );
extern effect_t fx_sl_load( const char *filename );
extern pass_t fx_sl_find_pass( const char *name );

#endif
