Name

    Extended Handle System

Contact

    Aaron Miller <nocannedmeat@gmail.com>

Overview

    A handle system with support for inheritence is required. Objects should be
    able to derive from other objects. Additionally, the handle system should be
    thread-safe.

    Handles are divided by types, where each handle type has its own 'pool'.
    That pool contains a list of allocated and available objects. Available
    objects are effectively "dead objects" that are in a reserve for being
    brought "back to life." It should be possible, on a per-pool and "global"
    basis, to set a threshold for how many "dead objects" there can be. (The
    default being "limited only by memory.")

    When a new object is requested, a "dead object" is retrieved if it exists.
    Otherwise, a new object is allocated. After the object is retrieved, its
    per-type constructors are run starting from the base class, ending with the
    final class of the object. When an object is destroyed, it will either be
    sent to the available list ("dead pool"), or purged from memory depending on
    the aforementioned threshold settings.

    Handles are reference counted and refer to objects. References can be
    incremented ("Grab") or decremented ("Drop"). When a handle's reference
    count reaches zero, the object will be destroyed. Objects are automatically
    purged from memory upon program termination. However, any live object (i.e.,
    any object with a reference count greater than zero) will cause a debug
    warning to be generated. ("Unterminated Object.")

    Any function expecting an object of type 'X' can take any object with a type
    derived from 'X'. Any function expecting an object of type 'Y' which is
    derived from 'X' cannot take an object of type 'X'. Take for example a
    function like "SetEntityPosition" which takes an object of type 'Entity'.
    Passing a 'LightEntity' -- which is derived from 'Entity' -- is valid.
    However for the function "SetLightColor", which takes an object of type
    'LightEntity', passing an object of type 'Entity' is not valid.

    The "Handle" and "Object" functions shall find the handle of a given object,
    and find the object from a given handle, respectively.

Issues

    * Are references handled per-object-class?

        No. Just per-object.

    * What information should a debug warning for "Unterminated Object" produce?

        The type of the object, its address (in 0x00000000[00000000] form), and
        why the debug warning was generated. It can optionally include the
        reference count.

    * How should unexpected types be handled by functions?

        Debug: Throw an error. (A macro should be provided by the handle system
               to enable handling this easily.)

        Release: Should not be checked. Just take the object passed. (Release
                 mode code is expected to be functioning.)

    * Are individual handles thread-safe?

        No. This would be an unnecessary management and performance burden. It's
        unnecessary because it may be desired for some handles avoid thread-
        safety for improved concurrency. Some handles may implement thread-
        safety on their own.

Revision History

    Date        Author          Changes
    --------    ------------    --------------------------------------------
    20120715    nocannedmeat    Initial version.
