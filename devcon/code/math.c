#include <math.h>
#include "math.h"

#define MAX_STATIC_VECS 16

vec_t *Vec2(vec_t x, vec_t y) {
	static vec2_t vecs[MAX_STATIC_VECS];
	static unsigned int i = 0;
	vec_t *v;

	v = (vec_t *)&vecs[i]; i = (i + 1) % MAX_STATIC_VECS;

	return v2_Set(v, x, y);
}

vec_t *Vec3(vec_t x, vec_t y, vec_t z) {
	static vec3_t vecs[MAX_STATIC_VECS];
	static unsigned int i = 0;
	vec_t *v;

	v = (vec_t *)&vecs[i]; i = (i + 1) % MAX_STATIC_VECS;

	return v3_Set(v, x, y, z);
}

vec_t *Vec4(vec_t x, vec_t y, vec_t z, vec_t w) {
	static vec4_t vecs[MAX_STATIC_VECS];
	static unsigned int i = 0;
	vec_t *v;

	v = (vec_t *)&vecs[i]; i = (i + 1) % MAX_STATIC_VECS;

	return v4_Set(v, x, y, z, w);
}
