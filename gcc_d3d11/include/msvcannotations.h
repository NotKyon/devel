#if !defined( __msvcannotations_h__ ) && !defined( _MSC_VER )
#define __msvcannotations_h__

#define __in
#define __out
#define __inout
#define __in_opt
#define __out_opt
#define __inout_opt
#define __in_bcount(x)
#define __out_bcount(x)
#define __inout_bcount(x)
#define __in_ecount(x)
#define __out_ecount(x)
#define __inout_ecount(x)
#define __in_bcount_opt(x)
#define __out_bcount_opt(x)
#define __inout_bcount_opt(x)
#define __in_ecount_opt(x)
#define __out_ecount_opt(x)
#define __inout_ecount_opt(x)
#define __in_bcount_part(x,y)
#define __out_bcount_part(x,y)
#define __inout_bcount_part(x,y)
#define __in_ecount_part(x,y)
#define __out_ecount_part(x,y)
#define __inout_ecount_part(x,y)
#define __in_bcount_part_opt(x,y)
#define __out_bcount_part_opt(x,y)
#define __inout_bcount_part_opt(x,y)
#define __in_ecount_part_opt(x,y)
#define __out_ecount_part_opt(x,y)
#define __inout_ecount_part_opt(x,y)

#endif
