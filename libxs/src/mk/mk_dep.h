#ifndef MK_DEP_H
#define MK_DEP_H

#include "mk_types.h"

typedef struct dep_s {
	char *name;
	xs_array_t deps;

	struct dep_s *prev, *next;
} *dep_t;
extern dep_t g_dep_head;
extern dep_t g_dep_tail;

dep_t NewDependency(const char *name);
void DeleteDependency(dep_t dep);
void DeleteAllDependencies();
const char *GetDependencyFile(dep_t dep);
void PushDependency(dep_t dep, const char *name);
size_t DependencyCount(dep_t dep);
const char *GetDependency(dep_t dep, size_t i);
dep_t FindDependency(const char *name);

#endif
