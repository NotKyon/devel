#ifndef EMBER_TRANSFORM_H
#define EMBER_TRANSFORM_H

#include "config.h"
#include "math.h"

#ifndef __entity_t_defined__
#define __entity_t_defined__ 1
typedef struct stEntity *entity_t;
#endif

#ifndef __tform_t_defined__
#define __tform_t_defined__ 1
typedef struct stTransform *tform_t;
#endif

#define TFORM_DRTY_GWORLD_BIT 0x00000001

struct stTransform {
	struct stEntity *ent;

	bitfield_t dirty;
	Matrix l_world, g_world;
	Vec3 scale;
};

extern void tform_init( tform_t tform, entity_t ent );
extern void tform_translate_ex( tform_t tform, const Vec3 *v );
extern void tform_rotate_ex( tform_t tform, const Vec3 *v );
extern void tform_scale_ex( tform_t tform, const Vec3 *v );
extern void tform_translate( tform_t tform, float x, float y, float z );
extern void tform_rotate( tform_t tform, float x, float y, float z );
extern void tform_scale( tform_t tform, float x, float y, float z );
extern void tform_move_ex( tform_t tform, const Vec3 *v );
extern void tform_turn_ex( tform_t tform, const Vec3 *v );
extern void tform_move( tform_t tform, float x, float y, float z );
extern void tform_turn( tform_t tform, float x, float y, float z );
extern void tform_move_x( tform_t tform, float x );
extern void tform_move_y( tform_t tform, float y );
extern void tform_move_z( tform_t tform, float z );
extern void tform_turn_x( tform_t tform, float x );
extern void tform_turn_y( tform_t tform, float y );
extern void tform_turn_z( tform_t tform, float z );

extern void tform_get_translation_ex( tform_t tform, Vec3 *v );
extern void tform_get_rotation_ex( tform_t tform, Vec3 *v );
extern void tform_get_scale_ex( tform_t tform, Vec3 *v );
extern void tform_get_translation( tform_t tform, float *x, float *y, float *z );
extern void tform_get_rotation( tform_t tform, float *x, float *y, float *z );
extern void tform_get_scale( tform_t tform, float *x, float *y, float *z );

extern float tform_translation_x( tform_t tform );
extern float tform_translation_y( tform_t tform );
extern float tform_translation_z( tform_t tform );

extern float tform_rotation_x( tform_t tform );
extern float tform_rotation_y( tform_t tform );
extern float tform_rotation_z( tform_t tform );

extern float tform_scale_x( tform_t tform );
extern float tform_scale_y( tform_t tform );
extern float tform_scale_z( tform_t tform );

extern Matrix *tform_local_matrix( tform_t tform );
extern Matrix *tform_global_matrix( tform_t tform );

extern void tform_set_local_matrix( tform_t tform, const Matrix *m );
extern void tform_set_global_matrix( tform_t tform, const Matrix *m );

/*
#define TFORM_DRTY_LWORLD_BIT	0x0001
#define TFORM_DRTY_LROTEU_BIT	0x0002
#define TFORM_DRTY_LSCALE_BIT	0x0004
#define TFORM_DRTY_LROTQU_BIT	0x0008
#define TFORM_DRTY_GWORLD_BIT	0x0010
#define TFORM_DRTY_GROTEU_BIT	0x0020
#define TFORM_DRTY_GROTQU_BIT	0x0040

struct stTransform {
  struct stEntity *ent;

  struct {
    Matrix world;
    Vec3 rotation_euler, scale;
    Quaternion rotation_quat;
  } l;
  struct {
    Matrix world;
    Vec3 rotation_euler;
    Quaternion rotation_quat;
  } g;
  bitfield_t dirty;
};

extern void tform_init(tform_t tform, entity_t ent);
extern const Vec3 *tform_get_translation(tform_t tform);
extern const Vec3 *tform_get_rotation(tform_t tform);
extern const Vec3 *tform_get_scale(tform_t tform);
extern const Quaternion *tform_get_rotation_quat(tform_t tform);
extern const Matrix *tform_get_matrix(tform_t tform);
extern const Vec3 *tform_get_gtranslation(tform_t tform);
extern const Vec3 *tform_get_grotation(tform_t tform);
extern const Quaternion *tform_get_grotation_quat(tform_t tform);
extern const Matrix *tform_get_gmatrix(tform_t tform);
extern void tform_set_translation(tform_t tform, const Vec3 *p);
extern void tform_set_rotation(tform_t tform, const Vec3 *p);
extern void tform_set_scale(tform_t tform, const Vec3 *p);
extern void tform_set_rotation_quat(tform_t tform, const Quaternion *p);
extern void tform_set_matrix(tform_t tform, const Matrix *p);
extern void tform_set_gtranslation(tform_t tform, const Vec3 *p);
extern void tform_set_grotation(tform_t tform, const Vec3 *p);
extern void tform_set_grotation_quat(tform_t tform, const Quaternion *p);
extern void tform_set_gmatrix(tform_t tform, const Matrix *p);
*/

#endif
