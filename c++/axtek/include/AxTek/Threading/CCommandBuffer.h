#ifndef AXTEK_THREADING_CCOMMANDBUFFER_H
#define AXTEK_THREADING_CCOMMANDBUFFER_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

#include "Atomics.h"
#include "CThread.h"

#include <stdio.h>

__AX_TASK_NS_INIT__

class CCommandBuffer {
protected:
	enum {
		ECMDBUF_BUFFER_SIZE =
#if !AXTEK_TASK_CONF_BUFFER_SIZE
		4096
#else
		AXTEK_TASK_CONF_BUFFER_SIZE
#endif
	};
	struct SToken {
		union {
			threadFunc_t Fn;
			void *Fnp;
		};
		void *Ptr;
	};

	SToken m_buf[ECMDBUF_BUFFER_SIZE];
	bool m_term; //terminate

	__align(32) volatile u32 m_numStreamers;
	__align(32) volatile u32 m_numPendingJobs;

	__align(32) volatile u32 m_insertionIndex;
	__align(32) volatile u32 m_streamingIndex;

	inline void EnqueueToken(const SToken &t) {
		u32 index;

		atomic_inc(&m_numPendingJobs);

		index = atomic_inc(&m_insertionIndex)%ECMDBUF_BUFFER_SIZE;

		while(atomic_get(&m_buf[index].Fnp) != (void *)NULL) {
			if (m_term)
				return;

			yield();
		}

		atomic_set(&m_buf[index].Ptr, t.Ptr);
		atomic_set(&m_buf[index].Fnp, t.Fnp);
	}

public:
	inline CCommandBuffer() {
		size_t i;

		for(i=0; i<ECMDBUF_BUFFER_SIZE; i++) {
			m_buf[i].Fnp = (void *)NULL;
			m_buf[i].Ptr = (void *)NULL;
		}
		m_term = false;

		m_numStreamers   = 0;
		m_numPendingJobs = 0;

		m_insertionIndex = 0;
		m_streamingIndex = 0;
	}
	inline ~CCommandBuffer() {
		SyncStreams();
	}

	inline void SyncStreams() {
		m_term = true;

		printf("terminate!\n");

		while(atomic_get(&m_numStreamers) > 0) {
			printf("~:%u\n", atomic_get(&m_numStreamers));
			yield();
		}
	}

	inline void Dispatch(threadFunc_t fn, void *p=(void *)NULL) {
		SToken t;

		t.Fn = fn;
		t.Ptr  = p ;

		EnqueueToken(t);
	}

	inline void IncrementStreamers() {
		atomic_inc(&m_numStreamers);
	}
	inline void DecrementStreamers() {
		atomic_dec(&m_numStreamers);
	}
	inline bool Stream() {
		SToken t;
		u32 index;

		index = atomic_inc(&m_streamingIndex)%ECMDBUF_BUFFER_SIZE;

		while((t.Fnp = atomic_get(&m_buf[index].Fnp))==(void *)NULL) {
			if (m_term)
				return false;

			yield();
		}

		t.Ptr = atomic_get(&m_buf[index].Ptr);
		atomic_set(&m_buf[index].Fnp, (void *)NULL);

		t.Fn(t.Ptr);
		atomic_dec(&m_numPendingJobs);

		return true;
	}

	inline void Sync() {
		while(atomic_get(&m_numPendingJobs) > 0)
			yield();
	}

	static inline result_t THREADPROCAPI streamer_f(void *p) {
		CCommandBuffer *cb;

		cb = (CCommandBuffer *)p;

		cb->IncrementStreamers();
		while(cb->Stream()) {
		}
		cb->DecrementStreamers();

		return (result_t)0;
	}
};

__AX_TASK_NS_FINI__

#endif
