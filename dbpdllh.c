/*
 * Dark Basic Pro Dynamic Link Library Header Exporter
 * Import DBP DLLs in C/C++
 */

#ifndef _WIN32
# error This is a Windows - only app by nature
#endif

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LIBNAME 256
#define MAX_CMDNAME 256
#define MAX_PTYPES 32
#define MAX_CNAME 256

enum
{
	kType_Void,		/*0*/
	kType_Bool,		/*B*/
	kType_Int32,	/*L*/
	kType_Int64,	/*R*/
	kType_UInt8,	/*Y*/
	kType_UInt16,	/*W*/
	kType_UInt32,	/*D*/
	kType_UInt64,	/*Q*/
	kType_Float32,	/*F*/
	kType_Float64,	/*O*/
	kType_String,	/*S*/
	kType_Array,	/*H*/
	kType_Any,		/*X*/

	/*
		UNHANDLED TYPES
		===============

		A - Don't know
	*/

	kType_Pointer = 32
};

typedef struct cmd_s
{
	struct lib_s *lib;
	char str[ MAX_CMDNAME ];

	char cname[ MAX_CNAME ];

	int rtype;

	int nparms;
	int ptypes[ MAX_PTYPES ];

	struct cmd_s *next;
} cmd_t;

typedef struct lib_s
{
	char dll[ MAX_LIBNAME ];

	struct cmd_s *head, *tail;
	struct lib_s *next;
} lib_t;
lib_t *g_lib_head = ( lib_t * )NULL;
lib_t *g_lib_tail = ( lib_t * )NULL;

void *MemAlloc( size_t n )
{
	void *p;

	p = malloc( n );
	if( !p )
	{
		perror( "Error : Failed to allocate memory" );
		exit( EXIT_FAILURE );
	}

	return p;
}
void *MemFree( void *p )
{
	if( !p )
	{
		return ( void * )NULL;
	}

	free( p );
	return ( void * )NULL;
}
void StrCpy( char *dst, size_t dstn, const char *src )
{
	if( strlen( src ) >= dstn )
	{
		fprintf( stderr, "Warning : Truncating string...\n" );
	}

#if __STDC_WANT_SECURE_LIB__
	strcpy_s( dst, dstn, src );
#else
	strncpy( dst, src, dstn - 1 );
	dst[ dstn - 1 ] = '\0';
#endif
}

void Stream( char *dst, size_t dstn, size_t *i, const char *src )
{
	size_t j, k;

	k = *i;

	if( k >= dstn - 1 || dstn - 1 > dstn )
	{
		return;
	}

	for( j = 0; src[ j ]; ++j )
	{
		if( k + j >= dstn - 1 )
		{
			dst[ k + j ] = '\0';
			return;
		}

		dst[ k + j ] = src[ j ];
	}

	dst[ k + j ] = '\0';
	*i = k + j;
}
void StreamChar( char *dst, size_t dstn, size_t *i, char c )
{
	char s[ 2 ];

	s[ 0 ] = c;
	s[ 1 ] = '\0';

	Stream( dst, dstn, i, s );
}

#define ToUpper( x )\
	( ( ( x ) >= 'a' && ( x ) <= 'z' ) ? ( ( x ) - 'a' + 'A' ) : ( x ) )
#define ToLower( x )\
	( ( ( x ) >= 'A' && ( x ) <= 'Z' ) ? ( ( x ) - 'A' + 'a' ) : ( x ) )

int TypeFromCode( const char **p )
{
	const char *s;
	int ptr;

	s = *p;

	ptr = 0;
	if( *( s + 1 ) == '*' )
	{
		ptr = kType_Pointer;
		( *p )++;
	}

	switch( *s )
	{
	case '0':
		return kType_Void | ptr;
	case 'L':
		return kType_Int32 | ptr;
	case 'F':
		return kType_Float32 | ptr;
	case 'S':
		return kType_String | ptr;
	case 'O':
		return kType_Float64 | ptr;
	case 'R':
		return kType_Int64 | ptr;
	case 'D':
		return kType_UInt32 | ptr;
	case 'B':
		return kType_Bool | ptr;
	case 'Y':
		return kType_UInt8 | ptr;
	case 'W':
		return kType_UInt16 | ptr;
	case 'Q':
		return kType_UInt64 | ptr;
	case 'H':
		return kType_Array | ptr;
	case 'X':
		return kType_Any | ptr;
	default:
		fprintf( stderr, "Warning: Unhandled type code '%c'\n", *s );
		return kType_Int32;
	}
}
const char *TypeToCName( int t )
{
	const int isPointer = ( t & kType_Pointer ) != 0;

	switch( t & ( kType_Pointer - 1 ) )
	{
	case kType_Void:
		return isPointer ? "void *" : "void";
	case kType_Int32:
		return isPointer ? "int *" : "int";
	case kType_Float32:
		return isPointer ? "float *" : "float";
	case kType_String:
		return isPointer ? "char **" : "const char *";
	case kType_Float64:
		return isPointer ? "double *" : "double";
	case kType_Int64:
		return isPointer ? "long long int *" : "long long int";
	case kType_UInt32:
		return isPointer ? "unsigned int *" : "unsigned int";
	case kType_Bool:
		return isPointer ? "bool *" : "bool";
	case kType_UInt8:
		return isPointer ? "unsigned char *" : "unsigned char";
	case kType_UInt16:
		return isPointer ? "unsigned short *" : "unsigned short";
	case kType_UInt64:
		return
			isPointer ? "unsigned long long int *" : "unsigned long long int";
	case kType_Array:
		return isPointer ? "/*ARRAYPTR*/ void **" : "/*ARRAY*/ void *";
	case kType_Any:
		return isPointer ? "/*ANYPTR*/ const void **" : "/*ANY*/ const void *";

	default:
		break;
	}

	return isPointer ? "/*UNKNOWNPTR*/ void **" : "/*UNKNOWN*/ const void *";
}

void AddCmd( lib_t *lib, const char *buf )
{
	const char *s;
	size_t i, dstn;
	cmd_t *cmd;
	char *dst;
	int mode, upper;
	int t;

	cmd = ( cmd_t * )MemAlloc( sizeof( *cmd ) );

	cmd->lib = lib;
	StrCpy( cmd->str, sizeof( cmd->str ), buf );

	cmd->rtype = kType_Void;
	cmd->nparms = 0;

	cmd->next = ( cmd_t * )NULL;

	if( lib->tail != ( cmd_t * )NULL )
	{
		lib->tail->next = cmd;
	}
	else
	{
		lib->head = cmd;
	}
	lib->tail = cmd;

	dstn = sizeof( cmd->cname );
	dst = cmd->cname;

	i = 0;
	Stream( dst, dstn, &i, "db" );
	upper = 1;

	mode = 0;
	for( s = &cmd->str[ 0 ]; *s; ++s )
	{
		switch( mode )
		{
		/* initial (name) */
		case 0:
			if( *s == '[' )
			{
				Stream( dst, dstn, &i, ": " );
				mode = 1;
			}
			else if( *s == '%' )
			{
				Stream( dst, dstn, &i, ": " );
				mode = 3;
			}
			else if( *s == ' ' || *s == '#' || *s == '$' )
			{
				upper = 1;
			}
			else
			{
				StreamChar( dst, dstn, &i, upper ? ToUpper( *s ) : ToLower( *s ) );
				/* this handles commands with names like 'XBlah' or 'BlahXYZ'
				 * while still respecting commands like 'BlahExist' (note the
				 * 'x' in the name) */
				upper =
				(
					(
						upper == 1
						&&
						( *s == 'X' || *s == 'Y' || *s == 'Z' )
					)
					||
					(
						ToLower( *s ) == 'w'
						&&
						(
							( *( s + 1 ) == 'X' ) ||
							( *( s + 1 ) == 'Y' ) ||
							( *( s + 1 ) == 'Z' )
						)
					)
				);
			}
			break;
		/* got '[' for return type; expecting '%' for types */
		case 1:
			if( *s != '%' )
			{
				fprintf( stderr, "Warning: Bad entry : %s\n", cmd->str );
			}

			mode = 2;
			break;
		/* processing return type */
		case 2:
			t = TypeFromCode( &s );
			Stream( dst, dstn, &i, "<- " );
			if( t != kType_String )
			{
				Stream( dst, dstn, &i, TypeToCName( t ) );
			}
			else
			{
				/* char *dbGetString( char *oldStr, actualParams... ); */
				Stream( dst, dstn, &i, "char *; char *oldStr" );
			}
			Stream( dst, dstn, &i, "; " );
			mode = 3;
			break;
		/* processing standard types */
		case 3:
			if( *s == '%' )
			{
				mode = 4;
				break;
			}
			t = TypeFromCode( &s );
			Stream( dst, dstn, &i, TypeToCName( t ) );
			Stream( dst, dstn, &i, "; " );
			break;
		default:
			break;
		}
	}
}
void AddLib( const char *name )
{
	HINSTANCE hinst;
	lib_t *lib;
	char buf[ MAX_CMDNAME ];
	UINT i;
	int success;

	hinst = LoadLibraryA( name );
	if( !hinst )
	{
		fprintf( stderr, "Error: Failed to open \"%s\"\n", name );
		return;
	}

	lib = ( lib_t * )MemAlloc( sizeof( *lib ) );

	lib->head = ( cmd_t * )NULL;
	lib->tail = ( cmd_t * )NULL;

	StrCpy( lib->dll, sizeof( lib->dll ), name );

	i = 1;
	while( 1 )
	{
		if( !LoadStringA( hinst, i++, buf, sizeof( buf ) ) )
		{
			break;
		}

		AddCmd( lib, buf );
	}

	FreeLibrary( hinst );

	lib->next = ( lib_t * )NULL;
	if( g_lib_tail )
	{
		g_lib_tail->next = lib;
	}
	else
	{
		g_lib_head = lib;
	}
	g_lib_tail = lib;
}

lib_t *FirstLib()
{
	return g_lib_head;
}
lib_t *LibAfter( lib_t *lib )
{
	return lib->next;
}

cmd_t *FirstCmd( lib_t *lib )
{
	return lib->head;
}
cmd_t *CmdAfter( cmd_t *cmd )
{
	return cmd->next;
}

const char *LibName( lib_t *lib )
{
	return lib->dll;
}

const char *CmdString( cmd_t *cmd )
{
	return cmd->str;
}
int CmdReturnType( cmd_t *cmd )
{
	return cmd->rtype;
}
int CmdCountParams( cmd_t *cmd )
{
	return cmd->nparms;
}
int CmdParamType( cmd_t *cmd, int i )
{
	if( *( unsigned int * )&i >= *( unsigned int * )&cmd->nparms )
	{
		return kType_Void;
	}

	return cmd->ptypes[ i ];
}

void Fini()
{
	lib_t *lib, *nextlib;
	cmd_t *cmd, *nextcmd;

	for( lib = FirstLib(); lib; lib = nextlib )
	{
		nextlib = LibAfter( lib );

		for( cmd = FirstCmd( lib ); cmd; cmd = nextcmd )
		{
			nextcmd = CmdAfter( cmd );

			MemFree( ( void * )cmd );
		}

		MemFree( ( void * )lib );
	}

	g_lib_head = ( lib_t * )NULL;
	g_lib_tail = ( lib_t * )NULL;
}

int main( int argc, char **argv )
{
	lib_t *lib;
	cmd_t *cmd;
	int i;

	atexit( Fini );

	for( i = 1; i < argc; ++i )
	{
		AddLib( argv[ i ] );
	}

	for( lib = FirstLib(); lib; lib = LibAfter( lib ) )
	{
		printf( "LIB: %s\n", LibName( lib ) );

		for( cmd = FirstCmd( lib ); cmd; cmd = CmdAfter( cmd ) )
		{
			printf( " %s\n", cmd->cname );
			/*printf( " %s\n", CmdString( cmd ) );*/
		}

		printf( "\n" );
	}

	return EXIT_SUCCESS;
}
