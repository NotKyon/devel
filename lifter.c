#include <errno.h>
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

/*
 * Manage a chunk of memory with appropriate error handling.
 */
void *memory(void *p, size_t n) {
	if (!p&&n) {
		if (!(p = malloc(n))) {
			perror("error: Failed to allocate memory");
			exit(EXIT_FAILURE);
		}
	} else if(p&&n) {
		if (!(p = realloc(p, n))) {
			free(p);
			perror("error: Failed to reallocate memory");
			exit(EXIT_FAILURE);
		}
	} else {
		if (p)
			free(p);

		p = (void *)0;
	}

	return p;
}

/*
 * Duplicate a string of memory.
 */
char *duplicate(const char *src) {
	size_t n;
	char *p;

	if (!src)
		return (char *)0;

	n = strlen(src);
	p = (char *)memory((void *)0, n + 1);
	memcpy((void *)p, (const void *)src, n + 1);

	return p;
}

/*
 * Append text to a string.
 */
char *append(char *dst, const char *src) {
	size_t dst_l, src_l;

	dst_l = dst ? strlen(dst) : 0;
	src_l = strlen(src) + 1 /*null*/;

	dst = (char *)memory((void *)dst, dst_l + src_l);
	memcpy(&dst[dst_l], (const void *)src, src_l);

	return dst;
}

/*
 * Printf() like formatting
 */
const char *va(const char *format, ...) {
	static char buf[32768];
	va_list args;

	va_start(args, format);
	vsnprintf(buf, sizeof(buf)-1, format, args);
	buf[sizeof(buf)-1] = 0;
	va_end(args);

	return buf;
}

/*
 * Copy text to a buffer without overflow.
 *
 * Returns the number of characters copied from src; NULL
 * terminator not included.
 */
size_t i_strcpy(char *dst, size_t dst_n, const char *src) {
	size_t i;

	assert(src != (const char *)0);

	i = 0;
	while(src[i] && (i + 1) < dst_n) {
		dst[i] = src[i];
		i++;
	}

	dst[i] = 0;
	return i;
}

size_t i_strncpy(char *dst, size_t dst_n, const char *src, size_t src_n) {
	size_t i;

	assert(src != (const char *)0);

	i = 0;
	while(src[i] && (i + 1) < dst_n && i < src_n) {
		dst[i] = src[i];
		i++;
	}

	dst[i] = 0;
	return i;
}

/*
 * Is a character part of an identifier starting sequence?
 */
int isidentstart(char c) {
	if ((c>='a'&&c<='z') || (c>='A'&&c<='Z'))
		return 1;

	if (c=='_')
		return 1;

	return 0;
}

/*
 * Is a character part of an identifier sequence?
 */
int isident(char c) {
	if (isidentstart(c))
		return 1;

	if (c>='0'&&c<='9')
		return 1;

	return 0;
}

typedef struct buffer_s *buffer_t;
struct buffer_s {
	char *file;
	int line;
	char *text;
	size_t i;
};

buffer_t loadbuffer(const char *filename) {
	buffer_t buf;
	FILE *f;
	char curline[8192], *p;

	buf = (buffer_t)memory((void *)0, sizeof(*buf));

	buf->file = duplicate(filename);
	buf->line = 1;
	buf->text = (char *)0;
	buf->i = 0;

	if (!(f = fopen(filename, "rb"))) {
		memory((void *)buf->file, 0);
		memory((void *)buf, 0);
		perror(va("%s: error", filename));
		return (buffer_t)0;
	}

	while(!feof(f)) {
		if (!(p = fgets(curline, sizeof(curline)-1, f))) {
			if (ferror(f)) {
				memory((void *)buf->file, 0);
				memory((void *)buf, 0);

				fprintf(stderr, "%s: error: Failed to read from file.\n",
					filename);
				fflush(stderr);

				return (buffer_t)0;
			}

			break;
		}

		curline[sizeof(curline) - 1] = 0;

		if ((p = strchr(p, '\r')) != (char *)0) {
			*(p + 0) = '\n';
			*(p + 1) = '\0';
		}

		buf->text = append(buf->text, curline);
	}

	fclose(f);
	return buf;
}
void deletebuffer(buffer_t buf) {
	if (!buf)
		return;

	memory((void *)buf->file, 0);
	memory((void *)buf->text, 0);
	memory((void *)buf, 0);
}
const char *bufferfilename(buffer_t buf) {
	return buf->file;
}
int bufferline(buffer_t buf) {
	return buf->line;
}
const char *buffertext(buffer_t buf) {
	return buf->text;
}
size_t bufferindex(buffer_t buf) {
	return buf->i;
}
char peekbuffer(buffer_t buf) {
	return buf->text[buf->i];
}
char pullbuffer(buffer_t buf) {
	char c;

	c = buf->text[buf->i];
	if (c != '\0')
		buf->i++;

	if (c == '\n')
		buf->line++;

	return c;
}
int endofbuffer(buffer_t buf) {
	return buf->text[buf->i]==0 ? 1 : 0;
}
char *readbufferline(buffer_t buf, char *dst, size_t dst_n) {
	size_t i;

	i = 0;

	assert(dst_n > 1);

	while(peekbuffer(buf) && peekbuffer(buf)!='\n') {
		dst[i++] = peekbuffer(buf);
		pullbuffer(buf);

		if ((i + 1) == dst_n)
			break;
	}

	dst[i++] = pullbuffer(buf);
	dst[i++] = 0;

	return dst;
}

void resetbuffer(buffer_t buf) {
	buf->line = 1;
	buf->i = 0;
}

enum {
	kLogBuffer_Error,		/* "landing gear broken... fsck." */
	kLogBuffer_Warning,		/* "this might cause an issue" */
	kLogBuffer_Remark,		/* "it would be more optimal if you did this" */
	kLogBuffer_Type_Mask = 0x03,

	kLogBuffer_IgnoreLine_Bit = 0x08
};
void vlog(const char *filename, int line, int type, const char *format,
va_list args) {
	static const char *typenames[] = { "error", "warning", "remark", "" };

	fflush(stdout);

	fprintf(stderr, "%s", filename);
	if ((~type & kLogBuffer_IgnoreLine_Bit) && (line > 0))
		fprintf(stderr, "(%i): ", line);
	else
		fprintf(stderr, ": ");
	fprintf(stderr, "%s: ", typenames[type&kLogBuffer_Type_Mask]);
	vfprintf(stderr, format, args);
	fprintf(stderr, "\n");
	fflush(stderr);
}
void vlogbuffer(buffer_t buf, int type, const char *format, va_list args) {
	vlog(bufferfilename(buf), bufferline(buf), type, format, args);
}
void logbuffer(buffer_t buf, int type, const char *format, ...) {
	va_list args;

	va_start(args, format);
	vlogbuffer(buf, type, format, args);
	va_end(args);
}
void errorbuffer(buffer_t buf, const char *format, ...) {
	va_list args;

	va_start(args, format);
	vlogbuffer(buf, kLogBuffer_Error, format, args);
	va_end(args);
}
void warnbuffer(buffer_t buf, const char *format, ...) {
	va_list args;

	va_start(args, format);
	vlogbuffer(buf, kLogBuffer_Warning, format, args);
	va_end(args);
}
void remarkbuffer(buffer_t buf, const char *format, ...) {
	va_list args;

	va_start(args, format);
	vlogbuffer(buf, kLogBuffer_Remark, format, args);
	va_end(args);
}

void printbuffer(buffer_t buf) {
	char curline[8192];
	int line;

	resetbuffer(buf);

	printf("\"%s\" - raw text\n", bufferfilename(buf));

	while(peekbuffer(buf) != 0) {
		line = bufferline(buf);
		readbufferline(buf, curline, sizeof(curline));
		printf("%.4i: %s", line, curline);
	}

	printf("\n");
	fflush(stdout);
}

enum {
	kLifterToken_EOF = 0,

	kLifterToken_Newline = '\n',
	kLifterToken_LParen = '(',
	kLifterToken_RParen = ')',

	__kLifterToken_StartSpecials__ = 256,

	kLifterToken_Ident,
	kLifterToken_String,

	kLifterToken_Error = -1 /*an error occurred*/
};

/*
 * Read a token from this buffer, storing the lexan in the
 * parameter specified.
 */
int lexlifterbuf(buffer_t buf, char *lexan, size_t lexan_n) {
	size_t base;
	char c;

	/* skip whitespace */
	while(peekbuffer(buf) <= ' ' && peekbuffer(buf) != '\0') {
		if (peekbuffer(buf)=='\n') /* we want '\n' */
			break;

		pullbuffer(buf);
	}

	/* skip comments */
	if (peekbuffer(buf)=='#') {
		while(peekbuffer(buf) != '\n' && peekbuffer(buf) != '\0') {
			pullbuffer(buf);
		}
	}

	/* end of file? */
	if (peekbuffer(buf)=='\0') {
		i_strcpy(lexan, lexan_n, "");
		return kLifterToken_EOF;
	}

	/* something else */
	base = bufferindex(buf);
	c = pullbuffer(buf);

	/* kLifterToken_Newline/LParen/RParen */
	if (c=='\n' || c=='(' || c==')') {
		i_strcpy(lexan, lexan_n, va("%c", c));
		return (int)c;
	}
	if (c==';') {
		i_strcpy(lexan, lexan_n, va("%c", c));
		return kLifterToken_Newline;
	}

	/* handle identifiers */
	if (isidentstart(c)) {
		while(isident(peekbuffer(buf)))
			pullbuffer(buf);

		i_strncpy(lexan, lexan_n, &buffertext(buf)[base],
			bufferindex(buf) - base);
		return kLifterToken_Ident;
	}

	/* handle strings */
	if (c=='\"') {
		base = bufferindex(buf); /*base++ works too*/

		while(peekbuffer(buf)!='\"' && peekbuffer(buf)!='\0') {
			c = pullbuffer(buf);

			/* escape sequence */
			if (c=='\\') {
				if (!(c = pullbuffer(buf))) {
					warnbuffer(buf, "Found EOF while reading string "
						"escape sequence; discarding string.");
					return kLifterToken_EOF;
				}

				/* escape sequences aren't translated here, but they're
				   read in here to ensure '\"' doesn't terminate */
			}
		}

		if (peekbuffer(buf)=='\0') {
			warnbuffer(buf, "Found EOF while reading string; discarding.");
			return kLifterToken_EOF;
		}

		i_strncpy(lexan, lexan_n, &buffertext(buf)[base],
			bufferindex(buf) - base);
		pullbuffer(buf); /*discard the '\"'*/

		return kLifterToken_String;
	}

	/* wtf is this? */
	errorbuffer(buf, "Unexpected character '%c' (0x%.2X)",
		c, (unsigned int)(unsigned char)c);
	return kLifterToken_Error;
}

/* retrieve the name of a token */
const char *liftertokenname(int token) {
	switch(token) {
	case kLifterToken_EOF:		return "EOF";
	case kLifterToken_Newline:	return "NL";
	case kLifterToken_LParen:	return "LPAREN";
	case kLifterToken_RParen:	return "RPAREN";
	case kLifterToken_Ident:	return "IDENT";
	case kLifterToken_String:	return "STRING";
	case kLifterToken_Error:	return "<!>";
	default:					break;
	}

	return "<?>";
}

/* display all the tokens of a file */
void printliftertokens(buffer_t buf) {
	char lexan[16384];
	int token;

	resetbuffer(buf);

	printf("\"%s\" - lifter tokens:\n", bufferfilename(buf));
	while(!endofbuffer(buf)) {
		token = lexlifterbuf(buf, lexan, sizeof(lexan));
		if (token==kLifterToken_Error)
			break;

		printf("%.3i - %6s - \"%s\"\n", token, liftertokenname(token), lexan);
	}
	printf("\n");

	fflush(stdout);
}

struct ast_s {
	char *lexan;
	int token;
	buffer_t buf;
	int line;

	struct ast_s *prnt, *root;
	struct ast_s *prev, *next;
	struct ast_s *head, *tail;
};
typedef struct ast_s *ast_t;

ast_t newast(ast_t prnt, buffer_t buf, const char *lexan, int token) {
	ast_t ast;

	ast = (ast_t)memory((void *)0, sizeof(*ast));

	ast->buf = buf;
	ast->lexan = duplicate(lexan);
	ast->token = token;

	ast->buf = buf;
	ast->line = buf ? bufferline(buf) : 0;

	ast->prnt = prnt;
	ast->root = prnt ? (prnt->root ? prnt->root : prnt) : (struct ast_s *)0;
	ast->prev = (struct ast_s *)0;
	ast->next = (struct ast_s *)0;
	ast->head = (struct ast_s *)0;
	ast->tail = (struct ast_s *)0;

	if (prnt) {
		if ((ast->prev = prnt->tail) != (struct ast_s *)0)
			prnt->tail->next = ast;
		else
			prnt->head = ast;
		prnt->tail = ast;
	}

	return ast;
}
void deleteast(ast_t ast) {
	if (!ast)
		return;

	while(ast->head)
		deleteast(ast->head);

	if (ast->prev)
		ast->prev->next = ast->next;
	if (ast->next)
		ast->next->prev = ast->prev;

	if (ast->prnt) {
		if (ast->prnt->head==ast)
			ast->prnt->head = ast->next;
		if (ast->prnt->tail==ast)
			ast->prnt->tail = ast->prev;
	}

	memory((void *)ast->lexan, 0);
	memory((void *)ast, 0);
}
const char *astlexan(ast_t ast) {
	return ast->lexan;
}
int asttoken(ast_t ast) {
	return ast->token;
}
buffer_t astbuffer(ast_t ast) {
	return ast->buf;
}
int astline(ast_t ast) {
	return ast->line;
}
ast_t astparent(ast_t ast) {
	return ast->prnt;
}
ast_t astroot(ast_t ast) {
	return ast->root;
}
ast_t astbefore(ast_t ast) {
	return ast->prev;
}
ast_t astafter(ast_t ast) {
	return ast->next;
}
ast_t firstast(ast_t ast) {
	return ast->head;
}
ast_t lastast(ast_t ast) {
	return ast->tail;
}
void vlogast(ast_t ast, int type, const char *format, va_list args) {
	ast_t root;

	assert(ast != (ast_t)0);

	if (!(root = astroot(ast)))
		root = ast;

	vlog(astlexan(root), astline(ast), type, format, args);
}
void logast(ast_t ast, int type, const char *format, ...) {
	va_list args;

	va_start(args, format);
	vlogast(ast, type, format, args);
	va_end(args);
}
void errorast(ast_t ast, const char *format, ...) {
	va_list args;

	va_start(args, format);
	vlogast(ast, kLogBuffer_Error, format, args);
	va_end(args);
}
void warnast(ast_t ast, const char *format, ...) {
	va_list args;

	va_start(args, format);
	vlogast(ast, kLogBuffer_Warning, format, args);
	va_end(args);
}
void remarkast(ast_t ast, const char *format, ...) {
	va_list args;

	va_start(args, format);
	vlogast(ast, kLogBuffer_Remark, format, args);
	va_end(args);
}

/*
 * Expect the token of a specific AST node to match one of the tokens passed to
 * this function. If there's a match, the index of the token passed (plus one)
 * is returned. Otherwise there's no match and an error is displayed and zero is
 * returned. The translator() function is used to retrieve the names of tokens.
 * If the translator function pointer passed is NULL, va() will be used with the
 * format string "<%.3i>" to obtain the name of a given token.
 */
int vexpectofast(ast_t ast, const char *(*translator)(int), int n,
va_list args) {
	size_t buf_p;
	char buf[8192];
	int token;
	int i, t;

	token = asttoken(ast);

	buf[0] = 0;
	buf_p = 0;

	for(i=0; i<n; i++) {
		if (token == (t = va_arg(args, int)))
			return i + 1;

		buf_p += i_strcpy(&buf[buf_p], sizeof(buf) - buf_p,
			translator ? translator(t) : va("<%.3i>", t));
		buf_p += i_strcpy(&buf[buf_p], sizeof(buf) - buf_p,
			i+2 == n ? " or " : i+1 < n ? ", " : "");
	}

	errorast(ast, "Expected %s; got %s", buf,
		translator ? translator(token) : va("<%.3i>", token));

	return 0;
}
int expectofast(ast_t ast, const char *(*translator)(int), int n, ...) {
	va_list args;
	int r;

	va_start(args, n);
	r = vexpectofast(ast, translator, n, args);
	va_end(args);

	return r;
}

/*
 * Same as vexpectofast()/expectofast(), except takes only one parameter.
 */
int expectast(ast_t ast, const char *(*translator)(int), int token) {
	return expectofast(ast, translator, 1, token);
}

/*
 * Display an AST node with a certain amount of margin. 'incramnt' parameter is
 * used to specify how far much extra margin should be added at each level.
 * Returns the number of items successfully printed. This should at least be one
 * or an error occurred. (Not logged.)
 */
int printastnode(ast_t node, int margin, int incramnt) {
	char margintext[512];
	int n, r, isroot;
	int i;

	if (margin >= (int)sizeof(margintext) - 1)
		return 0;

	if (incramnt==0)
		incramnt = 2;

	for(i=0; i<margin; i++)
		margintext[i] = ' ';
	margintext[i] = 0;

	if (!astroot(node)) {
		isroot = 1;
		printf("%s\"%s\" - abstract syntax tree\n", margintext, astlexan(node));
	} else {
		isroot = 0;
		printf("%s%.3i - %6s - \"%s\" (line:%i)\n", margintext, asttoken(node),
			liftertokenname(asttoken(node)), astlexan(node), astline(node));
	}

	n = 1;
	for(node=firstast(node); node; node=astafter(node)) {
		if (!(r = printastnode(node, margin + incramnt, incramnt)))
			break;

		n += r;
	}

	if (isroot)
		printf("\n");

	return r;
}

int expectoflifterast(ast_t ast, int n, ...) {
	va_list args;
	int r;

	va_start(args, n);
	r = vexpectofast(ast, liftertokenname, n, args);
	va_end(args);

	return r;
}
int expectlifterast(ast_t ast, int token) {
	return expectoflifterast(ast, 1, token);
}

/* construct an abstract syntax tree from a .lifter file */
ast_t parselifterast(buffer_t buf) {
	ast_t root, prnt ,ast;
	char lexan[16384];
	int token;

	resetbuffer(buf);

	root = newast((ast_t)0, buf, bufferfilename(buf), 0);
	prnt = (ast_t)0;

	while(!endofbuffer(buf)) {
		token = lexlifterbuf(buf, lexan, sizeof(lexan));
		if (token==kLifterToken_Error) {
			deleteast(root);
			return (ast_t)0;
		}

		if (token==kLifterToken_EOF)
			break;

		if (token==kLifterToken_Newline) {
			prnt = (ast_t)0;
			continue;
		}

		if (token!=kLifterToken_Ident && !prnt) {
			errorbuffer(buf, "Expected '%s' got '%s'",
				liftertokenname(kLifterToken_Ident), liftertokenname(token));
			deleteast(root);
			return (ast_t)0;
		}

		ast = newast(prnt ? prnt : root, buf, lexan, token);
		if (!prnt)
			prnt = ast;
	}

	return root;
}

enum {
	kRestrict_None,			/* not restricted; no need to check cpu features */

	kRestrict_FPU,			/* not x87 fpu, just normal fpu */
	kRestrict_SSE,			/* x86 sse */
	kRestrict_AltiVec,		/* powerpc altivec */
	kRestrict_NEON,	  		/* arm neon */

	kNumRestricts			/* number of restricts (used for array size) */
};
struct module_s {
	char *name;
	char *fnprefix[kNumRestricts];
	char *fnsuffix[kNumRestricts];

	struct module_s *prev, *next;
};
struct module_s *g_mod_head = (struct module_s *)0;
struct module_s *g_mod_tail = (struct module_s *)0;

typedef struct module_s *module_t;

const char *restrictionname(int restriction) {
	switch(restriction) {
	case kRestrict_None: return "None";
	case kRestrict_FPU: return "FPU";
	case kRestrict_SSE: return "SSE";
	case kRestrict_AltiVec: return "AltiVec";
	case kRestrict_NEON: return "NEON";
	default: break;
	}

	return "(unknown-restriction)";
}

module_t newmod(const char *name) {
	module_t mod;
	size_t i;

	mod = (module_t)memory((void *)0, sizeof(*mod));

	mod->name = duplicate(name);

	for(i=0; i<kNumRestricts; i++) {
		mod->fnprefix[i] = (char *)0;
		mod->fnsuffix[i] = (char *)0;
	}

	mod->next = (struct module_s *)0;
	if ((mod->prev = g_mod_tail) != (struct module_s *)0)
		g_mod_tail->next = mod;
	else
		g_mod_head = mod;
	g_mod_tail = mod;

	return mod;
}
void deletemod(module_t mod) {
	size_t i;

	if (!mod)
		return;

	if (mod->prev)
		mod->prev->next = mod->next;
	if (mod->next)
		mod->next->prev = mod->prev;

	if (g_mod_head==mod)
		g_mod_head = mod->next;
	if (g_mod_tail==mod)
		g_mod_tail = mod->prev;

	for(i=0; i<kNumRestricts; i++) {
		memory((void *)mod->fnprefix[i], 0);
		memory((void *)mod->fnsuffix[i], 0);
	}

	memory((void *)mod->name, 0);
	memory((void *)mod, 0);
}
const char *modname(module_t mod) {
	return mod->name;
}
const char *modfnprefix(module_t mod, int restriction) {
	return mod->fnprefix[restriction];
}
const char *modfnsuffix(module_t mod, int restriction) {
	return mod->fnsuffix[restriction];
}
module_t modbefore(module_t mod) {
	return mod->prev;
}
module_t modafter(module_t mod) {
	return mod->next;
}
module_t firstmod() {
	return g_mod_head;
}
module_t lastmod() {
	return g_mod_tail;
}
void setmodfnprefix(module_t mod, int restriction, const char *fnprefix) {
   	mod->fnprefix[restriction] =
		(char *)memory((void *)mod->fnprefix[restriction], 0);
	mod->fnprefix[restriction] = duplicate(fnprefix);
}
void setmodfnsuffix(module_t mod, int restriction, const char *fnsuffix) {
	mod->fnsuffix[restriction] =
		(char *)memory((void *)mod->fnsuffix[restriction], 0);
	mod->fnsuffix[restriction] = duplicate(fnsuffix);
}

int lifterreadargs(ast_t prnt, ast_t *array, size_t n, const char *format) {
	size_t i;
	ast_t node;
	int expected;

	i = 0;

	assert(prnt != (ast_t)0);
	node = firstast(prnt);

	assert(array != (ast_t *)0);
	assert(n >= strlen(format));

	assert(format != (const char *)0);

	while(*format) {
		switch(*format) {
		case 's': expected = kLifterToken_String; break;
		case 'I': expected = kLifterToken_Ident; break;
		case '(': expected = kLifterToken_LParen; break;
		case ')': expected = kLifterToken_RParen; break;
		default:
			expected = kLifterToken_Error;
			fflush(stdout);
			fprintf(stderr, "UNEXPECTED FORMAT CHARACTER '%c'\n", *format);
			fflush(stderr);
			break;
		}

		if (expected==kLifterToken_Error)
			return 0;

		if (!node) {
			errorast(prnt, "Expected '%s' in argument(s) to '%s'",
				liftertokenname(expected), astlexan(prnt));
			return 0;
		}

		if (!expectlifterast(node, expected))
			return 0;

		array[i++] = node;
		node = astafter(node);

		format++;
	}

	if (node != (ast_t)0) {
		errorast(node, "Unexpected token '%s'",
			liftertokenname(asttoken(node)));
		return 0;
	}

	return 1;
}
int asthasfirstchild(ast_t ast, int token) {
	if (!(ast = firstast(ast)))
		return 0;

	return token ? (asttoken(ast)==token ? 1 : 0) : 1;
}
int restrictionfromname(ast_t ast, const char *rname) {
#define CHECKRESTRICT(rn) if (!strcmp(rname, #rn)) return kRestrict_##rn

	assert(ast != (ast_t)0);

	if (!rname)
		rname = astlexan(ast);

	CHECKRESTRICT(None);
	CHECKRESTRICT(FPU);
	CHECKRESTRICT(SSE);
	CHECKRESTRICT(AltiVec);
	CHECKRESTRICT(NEON);

	errorast(ast, "Unknown restriction '%s'", rname);
	return -1;

#undef CHECKRESTRICT
}

int analyzelifterast(ast_t ast) {
#define N 4
	module_t mod;
	ast_t nodes[N];
	int restriction, stri;

	mod = (module_t)0;

	for(ast=firstast(ast); ast; ast=astafter(ast)) {
		assert(asttoken(ast)==kLifterToken_Ident);

		if (!strcmp(astlexan(ast), "Module")) {
			if (!lifterreadargs(ast, nodes, N, "s"))
				return 0;

			mod = newmod(astlexan(nodes[0]));
			continue;
		}

		if (!strcmp(astlexan(ast), "End")
		||  !strcmp(astlexan(ast), "EndModule")) {
			if (!mod) {
				errorast(ast, "Mismatched %s", astlexan(ast));
				return 0;
			}

			mod = (module_t)0;
			continue;
		}

		if (!strcmp(astlexan(ast), "FnPrefix")) {
			if (!mod) {
				errorast(ast, "'FnPrefix' outside of module.");
				return 0;
			}

			restriction = kRestrict_None;
			stri = 0;
			if (asthasfirstchild(ast, kLifterToken_LParen)) {
				if (!lifterreadargs(ast, nodes, N, "(I)s"))
					return 0;

				if ((restriction = restrictionfromname(nodes[1],
				(const char *)0)) < 0)
					return 0;
			} else if(!lifterreadargs(ast, nodes, N, "s"))
				return 0;

			setmodfnprefix(mod, restriction, astlexan(nodes[stri]));
			continue;
		}
		if (!strcmp(astlexan(ast), "FnSuffix")) {
			if (!mod) {
				errorast(ast, "'FnSuffix' outside of module.");
				return 0;
			}

			restriction = kRestrict_None;
			stri = 0;
			if (asthasfirstchild(ast, kLifterToken_LParen)) {
				if (!lifterreadargs(ast, nodes, N, "(I)s"))
					return 0;

				if ((restriction = restrictionfromname(nodes[1],
				(const char *)0)) < 0)
					return 0;
			} else if(!lifterreadargs(ast, nodes, N, "s"))
				return 0;

			setmodfnsuffix(mod, restriction, astlexan(nodes[stri]));
			continue;
		}

		/* TODO: Is FnMacro worth adding support for? */

		if (!strcmp(astlexan(ast), "Source")) {
			if (!mod) {
				errorast(ast, "'Source' outside of module.");
				return 0;
			}

			if (!lifterreadargs(ast, nodes, N, "s"))
				return 0;

			printf("TODO: Read the source at \"%s\"\n", astlexan(nodes[0]));
			continue;
		}

		errorast(ast, "Unknown identifier '%s'", astlexan(ast));
		return 0;
	}

	return 1;
}

/*
 * TODO: Add a C lexer
 * TODO: Add a C parser (AST-tree generator)
 * TODO: Add a C analyzer
 *
 * The C parser is only concerned with function tokens. Obviously type tokens
 * would exist as well (e.g., return type), and functions that return function
 * pointers needs to be taken into account.
 *
 * A map of the function names needs to be stored. e.g., eeAddVec3_FPU (with a
 * FnPrefix value of "ee"; FnSuffix "_FPU") and eeAddVec3_SSE (FnSuffix "_SSE")
 * should both map to the same function (AddVec3), which would then be selected
 * at runtime.
 *
 * The module exporter system needs to export the modules separately, so they
 * can be imported via function pointer. Every function can be populated with a
 * default function pointer (which will work because of cdecl) which says
 * something along the lines of "Attempt to call a function that was not
 * imported" or something along those lines. (Core commands only.)
 *
 * Additionally, Lua importer code can (eventually) be added. This isn't a high
 * priority though -- the rest of the engine is.
 *
 * A type management system should exist. Something that stores all the user's
 * types in a table, and returns an index into the table instead of a pointer to
 * the type -- that way they can't just delete the type. There would be some
 * conversion overhead, but the safety should be worth it. Additionally, in a
 * "pro" mode, the table can be completely ignored. That is, the table would
 * only be used in a form of a debug mode for type safety.
 */

enum {
	kCToken_EOF = 0,

	kCToken_Comma = ',',
	kCToken_Dot = '.',

	kCToken_Semicolon = ';',
	kCToken_Colon = ':',
	kCToken_LParen = '(',
	kCToken_RParen = ')',
	kCToken_LBracket = '[',
	kCToken_RBracket = ']',
	kCToken_LBrace = '{',
	kCToken_RBrace = '}',

	kCToken_Plus = '+',
	kCToken_Minus = '-',
	kCToken_Asterisk = '*',
	kCToken_Slash = '/',
	kCToken_Caret = '^',
	kCToken_Ampersand = '&',
	kCToken_Percent = '%',

	kCToken_Assign = '=',
	kCToken_Complement = '~',

	kCToken_CmpL = '<',
	kCToken_CmpG = '>',

	kCToken_Not = '!',

	kCToken_Ternary = '?',

	__kCToken_Start__ = 256,

	kCToken_Ident,
	kCToken_Number,
	kCToken_Character,
	kCToken_String,

	kCToken_Pointer, /* -> */

	kCToken_AddAssign, /* += */
	kCToken_SubAssign, /* -= */
	kCToken_MulAssign, /* *= */
	kCToken_DivAssign, /* /= */

	kCToken_ShiftLeft, /* << */
	kCToken_ShiftRight, /* >> */

	kCToken_SLAssign, /* <<= */
	kCToken_SRAssign, /* >>= */

	kCToken_CmpEQ, /* == */
	kCToken_CmpGE, /* >= */
	kCToken_CmpLE, /* <= */
	kCToken_CmpNE, /* != */

	kCToken_And, /* && */
	kCToken_Or, /* || */

	kCToken_KeywordTypedef,
	kCToken_KeywordStruct,
	kCToken_KeywordUnion,
	kCToken_KeywordEnum,

	kCToken_KeywordExtern,
	kCToken_KeywordStatic,
	kCToken_KeywordConst,
	kCToken_KeywordVolatile,
	kCToken_KeywordRegister,
	kCToken_KeywordRestrict,

	kCToken_KeywordSigned,
	kCToken_KeywordUnsigned,

	kCToken_KeywordVoid,
	kCToken_KeywordChar,
	kCToken_KeywordShort,
	kCToken_KeywordInt,
	kCToken_KeywordLong,
	kCToken_KeywordInt64,
	kCToken_KeywordFloat,
	kCToken_KeywordDouble,

	kCToken_KeywordIf,
	kCToken_KeywordElse,
	kCToken_KeywordDo,
	kCToken_KeywordFor,
	kCToken_KeywordWhile,

	kCToken_KeywordSwitch,
	kCToken_KeywordCatch,
	kCToken_KeywordDefault,

	kCToken_KeywordGoto,
	kCToken_KeywordReturn,
	kCToken_KeywordBreak,
	kCToken_KeywordContinue,

	kCToken_KeywordAsm,
	kCToken_KeywordInline,
	kCToken_KeywordSizeof,

	kCToken_Ellipsis, /* ... */

	kCToken_Invalid = -1
};

int lexcbuffer(buffer_t buf, char *lexan, size_t maxn) {
	/*
	 * TODO: Write the C lexer.
	 */
	return kCToken_EOF;
}

enum {
	kFlag_ShowLifterFile_Bit = 0x01,
	kFlag_ShowLifterTokens_Bit = 0x02,
	kFlag_ShowLifterAST_Bit = 0x04,
	kFlag_Verbose_Bit = 0x08
};

/* process command line arguments */
void init(int argc, char **argv) {
	static const char *table[256];
	const char *opt;
	buffer_t buf;
	ast_t ast;
	int i, flags, mode, bit;

	memset((void *)table, 0, sizeof(table));
	table[(unsigned char)'V'] = "verbose";

	flags = 0;

	for(i=1; i<argc; i++) {
		opt = argv[i];

		if (*opt=='-') {
			opt++;

			if (*opt=='-') {
				opt++;
				if (!strncmp(opt, "no-", 3)) {
					mode = 1;
					opt += 3;
				} else
					mode = 0;
			} else if(*opt) {
				char c;

				c = *opt;
				opt = table[(unsigned char)*opt];

				if (!opt) {
				    fprintf(stderr, "error: unknown option '%c'.\n", c);
					fflush(stderr);
					continue;
				}
			} else {
				fprintf(stderr, "error: stdin not supported yet.\n");
				fflush(stderr);
				continue;
				/* "-" */
			}

			bit = 0;

			if (!strcmp(opt, "show-lifter-file"))
				bit = kFlag_ShowLifterFile_Bit;
			else if(!strcmp(opt, "show-lifter-tokens"))
				bit = kFlag_ShowLifterTokens_Bit;
			else if(!strcmp(opt, "show-lifter-ast"))
				bit = kFlag_ShowLifterAST_Bit;
			else if(!strcmp(opt, "verbose"))
				bit = kFlag_Verbose_Bit;
			else {
				fprintf(stderr, "error: unknown option '%s'\n", argv[i]);
				fflush(stderr);
				continue;
			}

			if (mode==1)
				flags &= ~bit;
			else
				flags |= bit;
		} else {
			if (!(buf = loadbuffer(argv[i])))
				continue;

			if (flags & kFlag_ShowLifterFile_Bit)
				printbuffer(buf);
			if (flags & kFlag_ShowLifterTokens_Bit)
				printliftertokens(buf);

			ast = parselifterast(buf);

			if (flags & kFlag_ShowLifterAST_Bit)
				printastnode(ast, 0, 0);

			analyzelifterast(ast);
			deletebuffer(buf);
		}
	}
}

/* main program */
int main(int argc, char **argv) {
	init(argc, argv);
	return 0;
}
