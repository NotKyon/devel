%{

/* includes */
#include "declconv.h"

/* yylval */
#define YYRET(v)		{ yylval=strdup(yytext); return (v); }
#define YYINT(r,o)		{ yylval=strint_(o+(const char *)yytext,r); return TOK_NUMBER; }
static char *strint_(const char *text, int radix)
{
	static char buff[256];
	int	i;
	int	m;

	if (radix==10)
		return strdup(text);

	m = 1;
	if (*text=='-')
	{
		m = -1;
		text++;
	}
	else if(*text=='+')
	{
		text++;
	}

	i = strtol(text, 0, radix);
	sprintf(buff, "%d", i*m);

	return strdup(buff);
}


#define MAX_STACK_DEPTH 512
static struct {
	const char *file;
	unsigned int line;
} g_fileLineStack[MAX_STACK_DEPTH];
static unsigned int g_fileLineSP = 0;

void push_current_file_and_line() {
	if (++g_fileLineSP==MAX_STACK_DEPTH) {
		g_fileLineSP--;
		interror("Overflow [%s(%u)]", __FILE__,__LINE__);
	}

	g_fileLineStack[g_fileLineSP].file = NULL;
	g_fileLineStack[g_fileLineSP].line = 0;
}
void pop_current_file_and_line() {
	if (!g_fileLineSP)
		interror("Underflow [%s(%u)]", __FILE__,__LINE__);

	g_fileLineSP--;
}

void set_current_file(const char *file) {
	g_fileLineStack[g_fileLineSP].file = file;
}
void set_current_line(unsigned int line) {
	g_fileLineStack[g_fileLineSP].line = line;
}
void reset_current_line() {
	set_current_line(1);
}
void increment_current_line() {
	g_fileLineStack[g_fileLineSP].line++;
}
const char *get_current_file() {
	return g_fileLineStack[g_fileLineSP].file;
}
unsigned int get_current_line() {
	return g_fileLineStack[g_fileLineSP].line;
}

void reportv(const char *type, const char *format, va_list args) {
	unsigned int line;
	const char *file;

	file = get_current_file();
	line = get_current_line();

	if (file) {
		fprintf(stderr, "[%s", file);
		if (line)
			fprintf(stderr, "(%u)", line);
		fprintf(stderr, "] ");
	}

	fprintf(stderr, "%s: ", type);
	vfprintf(stderr, format, args);
	fprintf(stderr, "\n");

#ifdef _WIN32
	if (strcmp(type, "Warning") != 0) {
		char buf[512];

		vsnprintf(buf, sizeof(buf) - 1, format, args);
		buf[sizeof(buf) - 1] = '\0';

		MessageBoxA(GetActiveWindow(), buf, type, MB_ICONERROR|MB_OK);
	}
#endif
}

void interror(const char *format, ...) {
	va_list args;

	va_start(args, format);
	reportv("Internal Error", format, args);
	va_end(args);

	exit(EXIT_FAILURE);
}
void error(const char *format, ...) {
	va_list args;

	va_start(args, format);
	reportv("Error", format, args);
	va_end(args);
}
void warn(const char *format, ...) {
	va_list args;

	va_start(args, format);
	reportv("Warning", format, args);
	va_end(args);
}

static unsigned int g_commentStack = 0;

%}

NL						([\r]|([\r]?[\n]))

%x comment

%%

\/\*					{
							BEGIN comment;
							g_commentStack = 1;
						}
<comment>\/\*			g_commentStack++;
<comment>\*\/			{
							if (--g_commentStack==0) { BEGIN 0; }
						}
<comment>{NL}			increment_current_line();
<comment>.				/* ignored */;
\/\/[^\r\n]*			/* single-line comment */;
\;[^\r\n]*				/* single-line comment */;

\.lib					YYRET(TOK_LIB);
\".*\"					{
							size_t __l;
							yylval = strdup(&yytext[1]);
							__l = strlen(yylval);
							yylval[__l-1] = 0;
							return TOK_STRING;
						}
Ref						YYRET(TOK_REF);
Const					YYRET(TOK_CONST);
[a-zA-Z_][a-zA-Z0-9_]*	YYRET(TOK_IDENT);
\$[0-9A-Fa-f]+			YYINT(16,1);
0[xX][0-9A-Fa-f]+		YYINT(16,2);
\%[0-1]+				YYINT(2,1);
0[bB][0-1]+				YYINT(2,2);
(\-|\+)?[0-9]+			YYINT(10,0);
(\-|\+)?[0-9]*\.[0-9]+	YYRET(TOK_NUMBER);
\%|\#|\$|\*				YYRET(TOK_TYPE);
\:						YYRET(':');
\(						YYRET('(');
\)						YYRET(')');
\,						YYRET(',');
\=						YYRET('=');
{NL}					{
							increment_current_line();
							YYRET('\n');
						}
[ \t\r]+				/* whitespace */;
.						error("invalid character '%c'", *yytext);

%%
