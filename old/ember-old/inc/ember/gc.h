#ifndef EMBER_GC_H
#define EMBER_GC_H

#include <stddef.h>

typedef struct stGCPool *gc_pool_t;
typedef int ( *gc_init_t )( void * );
typedef void ( *gc_deinit_t )( void * );

extern gc_pool_t gc_register_pool( size_t element_size, gc_init_t init, gc_deinit_t deinit );
extern void gc_unregister_pool( gc_pool_t pool );
extern void gc_collect( void );
extern void gc_collect_ex( gc_pool_t pool );
extern void *gc_alloc( gc_pool_t pool );
extern void gc_retain( void *obj );
extern void gc_release( void *obj );
extern void gc_dealloc( void *obj );

#endif
