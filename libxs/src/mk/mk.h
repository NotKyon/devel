#ifndef MK_H
#define MK_H

#include "mk_types.h"

#include "mk_dep.h"
#include "mk_dir.h"
#include "mk_main.h"
#include "mk_error.h"
#include "mk_compmgr.h"
#include "mk_project.h"
#include "mk_utility.h"

#endif
