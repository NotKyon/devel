#include <ember/effect.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void __skip_whitespace( buffer_t buf )
{
	char c;

	while( 1 ) {
		if( !( c = buf_read( buf ) ) )
			return;

		/* skip whitespace */
		if( c <= ' ' ) /* includes '\t', '\r', '\n' */
			continue;

		/* skip multiline comments */
		if( c == '/' && buf_look( buf ) == '*' ) {
			while( 1 ) {
				if( !( c = buf_read( buf ) ) )
					return;
				if( c == '*' && buf_look( buf ) == '/' ) {
					buf_read( buf ); /* discard the found '/' */
					break;
				}
			}

			continue;
		}

		/* skip singleline comments */
		if( c == '/' && buf_look( buf ) == '/' ) {
			while( 1 ) {
				if( !( c = buf_read( buf ) ) )
					return;
				if( c == '\n' )
					break;
			}

			continue;
		}

		/* whatever character we found isn't whitespace, put it back */
		buf_unget( buf );
		return;
	}
}

enum {
	__TOK_START__ = 256,

	TOK_NUMBER,
	TOK_IDENT,
	TOK_STRING
};

static char __lex_text[ 4096 ];
static int __lex( buffer_t buf )
{
	size_t i;
	char c;
	int t, k;

	__skip_whitespace( buf );

	t = 0;
	i = 0;
	k = 0;
	while( 1 ) {
		c = buf_read( buf );

		if( c <= ' ' || c == '=' || c == ',' || c == '{' || c == '}' || c == '[' || c == ']' || c == '(' || c == ')' ) {
			if( t ) {
				buf_unget( buf );
				return t;
			}
			__lex_text[ 0 ] = c;
			__lex_text[ 1 ] = 0;
			return ( int )c;
		}
		__lex_text[ i++ ] = c;
		__lex_text[ i ]   = 0;

		switch( t ) {
		case 0:
			if( c == '_' || ( c >= 'a' && c <= 'z' ) || ( c >= 'A' && c <= 'Z' ) )
				t = TOK_IDENT;
			else if( c == '.' || ( c >= '0' && c <= '9' ) )
				t = TOK_NUMBER;
			else if( c == '\"' ) {
				i = 0;
				while( 1 ) {
					c = buf_read( buf );
					if( c == '\"' ) {
						__lex_text[ i ] = 0;
						return TOK_STRING;
					}
					__lex_text[ i++ ] = c;
				}
			} else {
				buf_report( buf, BUF_ERRLVL_ERROR, "unknown character '%c'", c );
				return -1;
			}
			break;
		case TOK_IDENT:
			if( c <= ' ' ) {
				__lex_text[ i - 1 ] = 0;
				buf_unget( buf );
				return TOK_IDENT;
			}
			if( c != '_' && c < 'a' && c > 'z' && c < 'A' && c > 'Z' && c < '0' && c > '9' ) {
				buf_report( buf, BUF_ERRLVL_ERROR, "malformed token '%s'", __lex_text );
				return -1;
			}
			break;
		case TOK_NUMBER:
			if( c == '.' ) {
				if( !k )
					k = 1;
				else {
					buf_report( buf, BUF_ERRLVL_ERROR, "malformed number '%s'",
					    __lex_text );
					return -1;
				}
				break;
			} else if( c < '0' || c > '9' ) {
				/* ugly hack for ShaderLab */
				if( __lex_text[ 0 ] == '2' && c == 'D' )
					return TOK_IDENT;

				buf_report( buf, BUF_ERRLVL_ERROR, "malformed number '%s'", __lex_text );
				return -1;
			}
			break;
		}
	}
}
static const char *__tok_name( int t )
{
	if( t == 0 )
		return "(eof)";
	if( t == '=' )
		return "'='";
	if( t == ',' )
		return "','";
	if( t == '{' )
		return "'{'";
	if( t == '}' )
		return "'}'";
	if( t == '[' )
		return "'['";
	if( t == ']' )
		return "']'";
	if( t == '(' )
		return "'('";
	if( t == ')' )
		return "')'";
	if( t == TOK_NUMBER )
		return "number";
	if( t == TOK_IDENT )
		return "name";
	if( t == TOK_STRING )
		return "text";

	return "...";
}
static int __expect( buffer_t buf, int t )
{
	int x;

	x = __lex( buf );
	if( x == -1 )
		return 0;

	if( x != t ) {
		buf_report( buf, BUF_ERRLVL_ERROR, "expected %s, got %s '%s'", __tok_name( t ),
		    __tok_name( x ), __lex_text );
		return 0;
	}

	return 1;
}

int __streq( const char *a, const char *b )
{
	char c[ 2 ];

	while( *a && *b ) {
		c[ 0 ] = *a++;
		c[ 1 ] = *b++;

		c[ 0 ] = c[ 0 ] >= 'a' && c[ 0 ] <= 'z' ? c[ 0 ] - 'a' + 'A' : c[ 0 ];
		c[ 1 ] = c[ 1 ] >= 'a' && c[ 1 ] <= 'z' ? c[ 1 ] - 'a' + 'A' : c[ 1 ];

		if( c[ 0 ] != c[ 1 ] )
			return 0;
	}

	return 1;
}
int __expect_keyword( buffer_t buf, const char *keyword )
{
	if( !__expect( buf, TOK_IDENT ) )
		return 0;

	if( !__streq( __lex_text, keyword ) ) {
		buf_report( buf, BUF_ERRLVL_ERROR, "expected %s, got %s", keyword,
		    __lex_text );
		return 0;
	}

	return 1;
}

static int __shader_parse_tags( tagowner_t owner, buffer_t buf )
{
	tag_t tg;
	int t;

	if( !__expect( buf, '{' ) )
		return 0;

	while( 1 ) {
		t = __lex( buf );

		if( t == '}' )
			return 1;

		if( t != TOK_STRING ) {
			buf_report( buf, BUF_ERRLVL_ERROR, "expected %s, got %s '%s'",
			    __tok_name( TOK_STRING ), __tok_name( t ), __lex_text );
			return 0;
		}

		if( !( tg = fx_alloc_tag( owner, __lex_text ) ) ) {
			buf_report( buf, BUF_ERRLVL_ERROR, "not enough memory" );
			return 0;
		}

		if( !__expect( buf, '=' ) )
			return 0;

		if( !__expect( buf, TOK_STRING ) )
			return 0;

		fx_set_tag_value( tg, __lex_text );
	}
}

static int __shader_parse_properties( effect_t shader, buffer_t buf )
{
	property_t pr;
	int t;

	if( !__expect( buf, '{' ) )
		return 0;

	t = __lex( buf );

	while( 1 ) {
		if( t == '}' )
			return 1;

		if( t != TOK_IDENT ) {
			buf_report( buf, BUF_ERRLVL_ERROR, "expected %s, got %s '%s'",
			    __tok_name( TOK_IDENT ), __tok_name( t ), __lex_text );
			return 0;
		}

		if( !( pr = fx_alloc_property( shader, __lex_text ) ) ) {
			buf_report( buf, BUF_ERRLVL_ERROR, "not enough memory" );
			return 0;
		}

		if( !__expect( buf, '(' ) )
			return 0;
		if( !__expect( buf, TOK_STRING ) )
			return 0;

		fx_set_property_desc( pr, __lex_text );

		if( !__expect( buf, ',' ) )
			return 0;
		if( !__expect( buf, TOK_IDENT ) )
			return 0;

		if( __streq( __lex_text, "range" ) )
			fx_set_property_kind( pr, EPropertyKind_Range );
		else if( __streq( __lex_text, "color" ) )
			fx_set_property_kind( pr, EPropertyKind_Color );
		else if( __streq( __lex_text, "2d" ) )
			fx_set_property_kind( pr, EPropertyKind_Texture2D );
		else if( __streq( __lex_text, "rect" ) )
			fx_set_property_kind( pr, EPropertyKind_TextureRect );
		else if( __streq( __lex_text, "cube" ) )
			fx_set_property_kind( pr, EPropertyKind_TextureCube );
		else if( __streq( __lex_text, "float" ) )
			fx_set_property_kind( pr, EPropertyKind_Float );
		else if( __streq( __lex_text, "vector" ) )
			fx_set_property_kind( pr, EPropertyKind_Vector );
		else {
			buf_report( buf, BUF_ERRLVL_ERROR, "unknown tag" );
			return 0;
		}

		if( fx_property_kind( pr ) == EPropertyKind_Range ) {
			float l, h;

			if( !__expect( buf, '(' ) )
				return 0;
			if( !__expect( buf, TOK_NUMBER ) )
				return 0;

			l = atof( __lex_text );

			if( !__expect( buf, ',' ) )
				return 0;
			if( !__expect( buf, TOK_NUMBER ) )
				return 0;

			h = atof( __lex_text );

			if( !__expect( buf, ')' ) )
				return 0;

			fx_set_property_range( pr, l, h );
		}

		if( !__expect( buf, ')' ) )
			return 0;

		if( !__expect( buf, '=' ) )
			return 0;

		if( pr->kind == EPropertyKind_Range || pr->kind == EPropertyKind_Float ) {
			if( !__expect( buf, TOK_NUMBER ) )
				return 0;

			fx_set_property_scalar( pr, atof( __lex_text ) );
		} else if( pr->kind == EPropertyKind_Color ||
		           pr->kind == EPropertyKind_Vector ) {
			float w, x, y, z;

			if( !__expect( buf, '(' ) )
				return 0;

			if( !__expect( buf, TOK_NUMBER ) )
				return 0;
			x = atof( __lex_text );

			if( !__expect( buf, ',' ) )
				return 0;

			if( !__expect( buf, TOK_NUMBER ) )
				return 0;
			y = atof( __lex_text );

			if( !__expect( buf, ',' ) )
				return 0;

			if( !__expect( buf, TOK_NUMBER ) )
				return 0;
			z = atof( __lex_text );

			if( !__expect( buf, ',' ) )
				return 0;

			if( !__expect( buf, TOK_NUMBER ) )
				return 0;
			w = atof( __lex_text );

			if( !__expect( buf, ')' ) )
				return 0;

			fx_set_property_vector( pr, x, y, z, w );
		} else if( pr->kind == EPropertyKind_Texture2D ||
		           pr->kind == EPropertyKind_TextureRect ||
		           pr->kind == EPropertyKind_TextureCube ) {
			if( !__expect( buf, TOK_STRING ) )
				return 0;

			fx_set_property_string( pr, __lex_text );

			if( !__expect( buf, '{' ) )
				return 0;

			while( 1 ) {
				t = __lex( buf );

				if( t == '}' )
					break;

				if( t != TOK_IDENT ) {
					buf_report( buf, BUF_ERRLVL_ERROR, "expected %s, got %s '%s'",
					    __tok_name( TOK_IDENT ), __tok_name( t ), __lex_text );
					return 0;
				}

				if( __streq( __lex_text, "texgen" ) ) {
					if( !__expect( buf, TOK_IDENT ) )
						return 0;

					if( __streq( __lex_text, "objectlinear" ) )
						fx_set_property_texgen( pr, EPropertyTexGen_ObjectLinear );
					else if( __streq( __lex_text, "eyelinear" ) )
						fx_set_property_texgen( pr, EPropertyTexGen_EyeLinear );
					else if( __streq( __lex_text, "spheremap" ) )
						fx_set_property_texgen( pr, EPropertyTexGen_SphereMap );
					else if( __streq( __lex_text, "cubereflect" ) )
						fx_set_property_texgen( pr, EPropertyTexGen_CubeReflect );
					else if( __streq( __lex_text, "cubenormal" ) )
						fx_set_property_texgen( pr, EPropertyTexGen_CubeNormal );
					else
						buf_report( buf, BUF_ERRLVL_WARN2, "unknown texgen mode '%s'",
						    __lex_text );
				} else if( __streq( __lex_text, "lightmapmode" ) )
					fx_set_property_lightmap_mode( pr, 1 );
				else {
					buf_report( buf, BUF_ERRLVL_WARN2, "unknown mode '%s'", __lex_text );
				}
			}
		}

		t = __lex( buf );
	}

	return 1;
}
static int __shader_parse_category( effect_t shader, buffer_t buf )
{
	if( shader ) {
	}

	buf_report( buf, BUF_ERRLVL_ERROR, "category commands aren't supported yet." );
	return 0;
}
static int __shader_parse_pass( pass_t pass, buffer_t buf )
{
	size_t i;
	int t;

	t = __lex( buf );

	if( t == TOK_STRING ) {
		/* shaderlab passes are in uppercase */
		for( i = 0; __lex_text[ i ]; i++ ) {
			__lex_text[ i ] = __lex_text[ i ] >= 'a' || __lex_text[ i ] <= 'z' ? __lex_text[ i ] - 'a' + 'A' : __lex_text[ i ];
		}

		fx_set_pass_name( pass, __lex_text );

		t = __lex( buf );
	}

	if( t != '{' ) {
		buf_report( buf, BUF_ERRLVL_ERROR, "expected %s, got %s '%s'",
		    __tok_name( '{' ), __tok_name( t ), __lex_text );
		return 0;
	}

	while( 1 ) {
		t = __lex( buf );

		if( t == '}' )
			return 1;

		if( t != TOK_IDENT ) {
			buf_report( buf, BUF_ERRLVL_ERROR, "syntax error" );
			return 0;
		}

		if( __streq( __lex_text, "tags" ) ) {
			if( !__shader_parse_tags( fx_pass_tagowner( pass ), buf ) )
				return 0;
		} else {
			buf_report( buf, BUF_ERRLVL_ERROR, "unsupported command [%d] '%s'", t,
			    __lex_text );
			return 0;
		}
	}
}
static int __shader_parse_subshader( technique_t subshader, buffer_t buf )
{
	pass_t pass;
	int t;

	if( !__expect( buf, '{' ) )
		return 0;

	while( 1 ) {
		t = __lex( buf );

		if( t == '}' )
			return 1;

		if( t != TOK_IDENT ) {
			buf_report( buf, BUF_ERRLVL_ERROR, "expected identifier or '}'" );
			return 0;
		}

		/*if (__streq(__lex_text, "category")) {
      if (!__shader_parse_category(shader, buf))
	return 0;
	} else */ if( __streq( __lex_text, "pass" ) ) {
			if( !( pass = fx_alloc_pass( subshader, ( const char * )0 ) ) )
				return 0;
			if( !__shader_parse_pass( pass, buf ) )
				return 0;
		} else if( __streq( __lex_text, "tags" ) ) {
			if( !__shader_parse_tags( fx_technique_tagowner( subshader ), buf ) )
				return 0;
		}
	}

	return 1;
}

int fx_sl_parse( effect_t shader, buffer_t buf )
{
	technique_t subshader;
	int t;

	if( !__expect_keyword( buf, "shader" ) )
		return 0;

	t = __lex( buf );
	if( t == TOK_STRING ) {
		fx_set_name( shader, __lex_text );

		t = __lex( buf );
	}

	if( t != '{' ) {
		buf_report( buf, BUF_ERRLVL_ERROR, "expected '{' got %s", __tok_name( t ) );
		return 0;
	}

	while( 1 ) {
		t = __lex( buf );

		switch( t ) {
		case TOK_IDENT:
			if( __streq( __lex_text, "properties" ) ) {
				if( !__shader_parse_properties( shader, buf ) )
					return 0;
			} else if( __streq( __lex_text, "subshader" ) ) {
				if( !( subshader = fx_alloc_technique( shader ) ) )
					return 0;
				if( !__shader_parse_subshader( subshader, buf ) )
					return 0;
			} else if( __streq( __lex_text, "category" ) ) {
				if( !__shader_parse_category( shader, buf ) )
					return 0;
			} else {
				buf_report( buf, BUF_ERRLVL_ERROR, "unknown command '%s'", __lex_text );
				return 0;
			}
			break;
		case '}':
			if( !fx_technique_first( shader ) ) {
				buf_report( buf, BUF_ERRLVL_ERROR, "shader has no subshaders" );
				return 0;
			}
			return 1;
		default:
			buf_report( buf, BUF_ERRLVL_ERROR, "syntax error" );
			return 0;
		}
	}

	return 0;
}

effect_t fx_sl_create( buffer_t buf )
{
	effect_t shader;

	if( !( shader = fx_alloc( buf_name( buf ) ) ) )
		return ( effect_t )0;

	if( !fx_sl_parse( shader, buf ) ) {
		fx_dealloc( shader );
		return ( effect_t )0;
	}

	return shader;
}
effect_t fx_sl_load( const char *filename )
{
	effect_t shader;
	buffer_t buf;

	if( !( buf = buf_load( filename, ( buffer_callback_t )0 ) ) )
		return ( effect_t )0;

	if( !( shader = fx_alloc( buf_name( buf ) ) ) ) {
		buf_dealloc( buf );
		return ( effect_t )0;
	}

	if( !fx_sl_parse( shader, buf ) ) {
		buf_dealloc( buf );
		fx_dealloc( shader );
		return ( effect_t )0;
	}

	buf_dealloc( buf );
	return shader;
}

pass_t fx_sl_find_pass( const char *name )
{
	technique_t tech;
	effect_t effect;
	pass_t pass;

	const char *ptr;
	size_t i;
	char effect_name[ 256 ];
	char pass_name[ 256 ];

	/* shader name/PASS NAME */
	if( !( ptr = strchr( name, '/' ) ) )
		return ( pass_t )0;

	i = 0;
	while( name != ptr )
		effect_name[ i++ ] = *name++;
	effect_name[ i ]       = 0;

	name++; /* skip the '/' */
	i = 0;
	while( *name )
		pass_name[ i++ ] = *name++;

	if( !( effect = fx_find( effect_name ) ) )
		return ( pass_t )0;

	for( tech = fx_technique_first( effect ); tech;
	     tech = fx_technique_after( tech ) ) {
		if( !( pass = fx_find_pass( tech, pass_name ) ) )
			continue;

		return pass;
	}

	return ( pass_t )0;
}
