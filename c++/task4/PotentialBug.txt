This version of Task potentially suffers from a bug I'm referring to as
"dispatcher-stall; streamer-ready."


BACKGROUND
----------
The task queue uses a fixed-size circular buffer with two indexes to mark where
writing (dispatching) and reading (streaming) can occur within the queue. When
the a job is dispatched, the dispatch index is fetched and incremented.
(Semantically this can be viewed as an atomic operation, but is currently
implemented with a critical section.) Meanwhile, a set of streamers perform the
same operation on the streaming index.

The dispatcher then waits until a function pointer at the index it retrieved has
been set to NULL. i.e., the slot is empty and available for another job.

The streamer waits until a function pointer at the index it retrieved has been
set to a non-NULL value. i.e., the slot is filled and therefore has a pending
job.


ORIGINAL ASSUMPTIONS
--------------------
My assumptions were:
* The dispatcher would never stall for too long because if it fails that means a
  job is already at a slot and ready to be pulled.
* The streamer would never halt indefinitely because the dispatcher is always
  trying to add tasks, and therefore will eventually reach the streamer.


POTENTIAL BUG
-------------
Let's imagine a situation where five jobs need to be added where the buffer is
only four slots wide. (The streamers are already running.)

Four jobs added (and fill) queue.

  [ X | X | X | X ] "Queue"

Fifth job waiting...

  [ X | X | X | X ]
    ^               "Dispatcher Index"

Dispatcher is currently stalling until this slot clears.
Third job finishes.

  [ X | X |   | X ]
    ^

Streamer now at the first index...

  [ X | X |   | X ]
    $               "Streamer Index"

Work is duplicated. (Two streamers now start executing the task.)
First instance of the task now finishes and sets the slot to NULL.

  [   | X |   | X ]

Dispatcher now inserts a new job into that slot.

  [ X | X |   | X ]
        ^

Second instance of the task now finishes and sets the slot to NULL. (Before the
fifth task even got to run!)

  [   | X |   | X ]
        $

To summarize: work can be duplicated; work can be lost. That's not acceptable.


POTENTIAL FIXES
---------------
(1) To fix only the latter issue, one could check the value of slot at the index
    and only change it if the value matches the function pointer just executed.
(2) Expanding upon the previous potential fix while taking into account the
    former issue, one could replace the value at the array slot index with a
    special "do not run" value. A streamer then considers any value that is
    non-special (as opposed to just non-NULL) as a valid job. Once a streamer
    finishes the job, it no longer has to check the previous value. It can
    instead just nullify the slot as it normally does.

The first potential fix benefits from a lock-free solution. However, it may
execute potentially expensive work more than once. Since it's usually (always?)
more important that the worst-case be optimized, this "solution" should be
discarded.

The second potential fix is somewhat correct, but it could (under extremely rare
and unlikely [usually error prone anyway] conditions) fail. It's probably the
right thing to do anyway.

In practice, the bug itself should be quite unlikely. Therefore, the solution
itself should also be quite unlikely to fail. The bug itself is mitigated by
introducing a larger buffer. (The default buffer size is 4,096 which should be
sufficient for what it's intended to be used for.) The first solution is a bit
too much of a hack, and pretty much undesired. The second solution is a much
better fit.

(3) One could change the entire algorithm and implement a lock-free queue...
    [Is this even a good idea? ...]

POSSIBLY FIXED (20120616)
-------------------------
Potential fix (2) has been elected for utilization and subsequently been
implemented.
