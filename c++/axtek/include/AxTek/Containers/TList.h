#ifndef AXTEK_CONTAINERS_TLIST_H
#define AXTEK_CONTAINERS_TLIST_H

#if _MSC_VER > 1000
# pragma once
#endif

#include "Config.h"

__AX_LIB_NS_INIT__

//
//	The TListBase<T> and TListLink<T> classes are useful for lower level TList
//	management. They allow a finer level of control than the TList<T> system.
//
//	The TList<T> system is for general ease-of-use.
//

template<typename T> class TListBase;

template<typename T>
class TListLink {
protected:
	friend class TListBase<T>;

	T *m_obj;
	TListLink<T> *m_prev, *m_next;
	TListBase<T> *m_base;

public:
	inline TListLink(T *obj=(T *)NULL,
	TListBase<T> *base=(TListBase<T> *)NULL): m_obj(obj),
	m_prev((TListLink<T> *)NULL), m_next((TListLink<T> *)NULL), m_base(base) {
		if (m_base) {
			if __likely ((m_prev = m_base->m_tail) != (TListLink<T> *)NULL)
				m_base->m_tail->m_next = this;
			else
				m_base->m_head = this;
			m_base->m_tail = this;
		}
	}
	inline ~TListLink() {
		Remove();
	}

	inline void Unlink() {
		if __unlikely (!m_base)
			return;

		if (m_prev)
			m_prev->m_next = m_next;
		else
			m_base->m_head = m_next;

		if (m_next)
			m_next->m_prev = m_prev;
		else
			m_base->m_tail = m_prev;

		m_base = (TListBase<T> *)NULL;
		m_prev = (TListLink<T> *)NULL;
		m_next = (TListLink<T> *)NULL;
	}

	inline void Remove(bool delObj=true, bool delLnk=true) {
		TListBase<T> *base;

		if __unlikely (!(base = m_base))
			return;

		Unlink();

		if (base->m_delObjOnRm & delObj)
			delete m_obj;

		if (base->m_delLnkOnRm & delLnk)
			delete this;
	}

	inline void SetObject(T *obj) {
		m_obj = obj;
	}

	inline const TListLink<T> *LinkBefore() const { return m_prev; }
	inline const TListLink<T> *LinkAfter () const { return m_next; }
	inline       TListLink<T> *LinkBefore()       { return m_prev; }
	inline       TListLink<T> *LinkAfter ()       { return m_next; }

	inline const TListBase<T> *Base      () const { return m_base; }
	inline       TListBase<T> *Base      ()       { return m_base; }

	inline const           T  *Object    () const { return m_obj ; }
	inline                 T  *Object    ()       { return m_obj ; }

	inline const T *Before() const {
		return m_prev ? m_prev->m_obj : (const T *)NULL;
	}
	inline       T *Before()       {
		return m_prev ? m_prev->m_obj : (      T *)NULL;
	}
	inline const T *After () const {
		return m_next ? m_next->m_obj : (const T *)NULL;
	}
	inline       T *After ()       {
		return m_next ? m_next->m_obj : (      T *)NULL;
	}

	inline void InsertBefore(TListLink<T> *p) {
		if __unlikely (!p || (m_base != p->m_base && m_base))
			return;

		Unlink();
		m_base = p->m_base;

		if ((m_prev = p->m_prev) != (TListLink<T> *)NULL)
			m_prev->m_next = this;
		else
			m_base->m_head = this;
		p->m_prev = this;
		m_next = p;
	}
	inline void InsertAfter(TListLink<T> *p) {
		if __unlikely (!p || (m_base != p->m_base && m_base))
			return;

		Unlink();
		m_base = p->m_base;

		if ((m_next = p->m_next) != (TListLink<T> *)NULL)
			m_next->m_prev = this;
		else
			m_base->m_tail = this;
		p->m_next = this;
		m_prev = p;
	}
};
template<typename T>
class TListBase {
protected:
	friend class TListLink<T>;

	bool m_delObjOnRm;
	bool m_delLnkOnRm;

	TListLink<T> *m_head, *m_tail;

public:
	inline TListBase(bool delObjOnRm=false, bool delLnkOnRm=false)
	: m_delObjOnRm(delObjOnRm), m_delLnkOnRm(delLnkOnRm),
	m_head((TListLink<T> *)NULL), m_tail((TListLink<T> *)NULL) {
	}
	inline ~TListBase() {
		RemoveAll();
	}

	inline void SetDeleteObjectOnRemove(bool delObjOnRm) {
		m_delObjOnRm = delObjOnRm;
	}
	inline void SetDeleteLinkOnRemove(bool delLnkOnRm) {
		m_delLnkOnRm = delLnkOnRm;
	}
	inline bool GetDeleteObjectOnRemove() const {
		return m_delObjOnRm;
	}
	inline bool GetDeleteLinkOnRemove() const {
		return m_delLnkOnRm;
	}

	inline void PushFront(TListLink<T> *link) {
		if __unlikely (!link)
			return;

		link->Unlink();
		link->m_base = this;

		if ((link->m_next = m_head) != (TListLink<T> *)NULL)
			m_head->m_prev = link;
		else
			m_tail = link;
		m_head = link;
	}
	inline void PushBack(TListLink<T> *link) {
		if __unlikely (!link)
			return;

		link->Unlink();
		link->m_base = this;

		if ((link->m_prev = m_tail) != (TListLink<T> *)NULL)
			m_tail->m_next = link;
		else
			m_head = link;
		m_tail = link;
	}

	inline TListLink<T> *PopFront(bool rm=false) {
		TListLink<T> *link;

		if ((link = m_head) != (TListLink<T> *)NULL) {
			if (rm)
				link->Remove();
			else
				link->Unlink();
		}

		return link;
	}
	inline TListLink<T> *PopBack(bool rm=false) {
		TListLink<T> *link;

		if ((link = m_tail) != (TListLink<T> *)NULL) {
			if (rm)
				link->Remove();
			else
				link->Unlink();
		}

		return link;
	}

	inline void AddFirst(TListLink<T> *link) { PushFront(link); }
	inline void AddLast (TListLink<T> *link) { PushBack (link); }

	inline void InsertBefore(TListLink<T> *link, TListLink<T> *before) {
		if __unlikely (!link || !before)
			return;

		link->InsertBefore(before);
	}
	inline void InsertAfter(TListLink<T> *link, TListLink<T> *after) {
		if __unlikely (!link || !after)
			return;

		link->InsertAfter(after);
	}

	inline const TListLink<T> *FirstLink() const { return m_head; }
	inline const TListLink<T> *LastLink () const { return m_tail; }
	inline       TListLink<T> *FirstLink()       { return m_head; }
	inline       TListLink<T> *LastLink ()       { return m_tail; }

	inline const T *First() const {
		return m_head ? m_head->Object() : (const T *)NULL;
	}
	inline const T *Last() const {
		return m_tail ? m_tail->Object() : (const T *)NULL;
	}

	inline T *First() {
		return m_head ? m_head->Object() : (T *)NULL;
	}
	inline T *Last() {
		return m_tail ? m_tail->Object() : (T *)NULL;
	}

	inline void RemoveAll() {
		while(m_head)
			m_head->Remove();
	}
};

template<typename T>
class TList {
protected:
	        TListBase<T>  m_list;
	mutable TListLink<T> *m_curr;

public:
	inline TList(): m_list(), m_curr((TListLink<T> *)NULL) {
		m_list.SetDeleteObjectOnRemove(false);
		m_list.SetDeleteLinkOnRemove(true);
	}
	inline ~TList() {
		Clear();
	}

	inline T *PopFront() {
		TListLink<T> *x;
		T *p;

		x = m_list.FirstLink();
		p = x ? x->Object() : (T *)NULL;

		x->Remove();
		return p;
	}
	inline T *PopBack() {
		TListLink<T> *x;
		T *p;

		x = m_list.LastLink();
		p = x ? x->Object() : (T *)NULL;

		x->Remove();
		return p;
	}
	
	inline void PushFront(T *p) {
		TListLink<T> *x;

		x = new TListLink<T>(p);
		if __unlikely(!x)
			return;

		m_list.AddFirst(x);
	}
	inline void PushBack(T *p) {
		TListLink<T> *x;

		x = new TListLink<T>(p);
		if __unlikely(!x)
			return;

		m_list.AddLast(x);
	}

	inline void InsertBefore(T *p) {
		TListLink<T> *x;

		if (!m_curr) {
			PushFront(p);
			return;
		}

		x = new TListLink<T>(p);
		if __unlikely(!x)
			return;

		x->InsertBefore(m_curr);
	}
	inline void InsertAfter(T *p) {
		TListLink<T> *x;

		if (!m_curr) {
			PushFront(p);
			return;
		}

		x = new TListLink<T>(p);
		if __unlikely(!x)
			return;

		x->InsertAfter(m_curr);
	}

	inline void ToFirst() const {
		m_curr = m_list.FirstLink();
	}
	inline void ToLast() const {
		m_curr = m_list.LastLink();
	}
	inline void ToPrevious() const {
		m_curr = m_curr ? m_curr->LinkBefore() : (TListLink<T> *)NULL;
	}
	inline void ToNext() const {
		m_curr = m_curr ? m_curr->LinkAfter () : (TListLink<T> *)NULL;
	}
	inline T *GetCurrent() const {
		return m_curr ? m_curr->Object() : (T *)NULL;
	}
	inline void RemoveCurrent() {
		TListLink<T> *x;

		if (!m_curr)
			return;

		x = m_curr->LinkAfter() ? m_curr->LinkAfter() : m_curr->LinkBefore();

		m_curr->Remove();
		m_curr = x;
	}
	inline void ResetCurrent() const {
		m_curr = (TListLink<T> *)NULL;
	}

	inline bool IsEmpty() const {
		return !m_list.FirstLink();
	}
	inline void Clear() {
		while(!IsEmpty())
			PopFront();
	}

	inline TListLink<T> *GetLink() const {
		return m_curr;
	}
};

__AX_LIB_NS_FINI__

#endif
