#ifndef MESHIO_HIERARCHY_H
#define MESHIO_HIERARCHY_H

#ifndef meshio_hierarchy_t_defined
#define meshio_hierarchy_t_defined
typedef struct MeshIO_Hierarchy *meshio_hierarchy_t;
#endif

struct MeshIO_Hierarchy {
	struct {
		size_t n;
		char *p;
	} name;

	struct {
		uint32_t count;
		struct MeshIO_Transform *head,
		    *tail;
	} frame;

	struct MeshIO_Geometry *geo;

	struct MeshIO_Hierarchy *prnt,
	    *head, *tail;
	*prev, *next;
};

#endif
