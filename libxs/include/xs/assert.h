#ifndef XS_ASSERT_H
#define XS_ASSERT_H

#include "logging.h"

#if _MSC_VER||__INTEL_COMPILER
# define __func__ __FUNCTION__
#endif

#if (MK_ARCH_X86 || MK_ARCH_X86_64) || (__i386__||__x86_64__) || (_M_IX86)
# if __GNUC__ || __clang__
#  define XS_BREAKPOINT() asm("int $3")
# else
#  define XS_BREAKPOINT() __asm int 3
# endif
#else
# error "This platform not yet supported. (Breakpoints.)"
#endif

#if XS_DEBUG
# define XS_ASSERT(x) if(!(x)){XsLogMessage(__FILE__,__LINE__,__func__,\
	kXsLog_Error,"XS_ASSERT(%s) failed!", #x);XS_BREAKPOINT();}
#else
# define XS_ASSERT(x) /*do nothing!*/
#endif

#endif
