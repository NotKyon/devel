#include <cerrno>
#include <cstdio>
#include <cassert>
#include <cstdarg>
#include <cstddef>
#include <cstdlib>
#include <cstring>

//==============================================================================
//
//	SUPPORT FUNCTIONS
//
//==============================================================================

namespace sys {

inline void Error(const char *message, ...) {
	va_list args;
	int e;

	e = errno;

	fprintf(stderr, "ERROR: ");

	va_start(args, message);
	vfprintf(stderr, message, args);
	va_end(args);

	if (e)
		fprintf(stderr, ": %s (%i)", strerror(e), e);

	fprintf(stderr, "\n");
	fflush(stderr);
}

inline void Warning(const char *message, ...) {
	va_list args;
	int e;

	e = errno;

	fprintf(stderr, "WARNING: ");

	va_start(args, message);
	vfprintf(stderr, message, args);
	va_end(args);

	if (e)
		fprintf(stderr, ": %s (%i)", strerror(e), e);

	fprintf(stderr, "\n");
	fflush(stderr);
}

inline void FatalExit() {
	exit(EXIT_FAILURE);
}

}

namespace mem {

inline void *New(size_t n) {
	void *p;

	if (!n)
		return (void *)0;

	if (!(p = malloc(n))) {
		sys::Error("malloc() failure");
		sys::FatalExit();
	}

	return p;
}
template<typename T>
inline T *Delete(T *p) {
	if (!p)
		return (T *)0;

	free((void *)p);
	return (T *)0;
}
template<typename T>
inline T *Renew(T *p, size_t n) {
	if (!p)
		return (T *)New(n);

	if (!n)
		return Delete(p);

	p = (T *)realloc((void *)p, n);
	if (!p) {
		sys::Error("realloc() failure");
		sys::FatalExit();
	}

	return (T *)p;
}

template<typename T>
inline void Copy(T *dst, const T *src, size_t n) {
	assert(dst != (T *)0);
	assert(src != (const T *)0);
	assert(n > 0);

	memcpy((void *)dst, (const void *)src, n);
}

}

//==============================================================================
//
//	LINKED LISTS
//
//==============================================================================

template<typename T> class ListItem;
template<typename T> class ListBase;

template<typename T>
class ListItem {
protected:
	friend class ListBase<T>;

	T *m_obj;
	ListItem<T> *m_prev, *m_next;
	ListBase<T> *m_base;

public:
	ListItem(T *obj=(T *)0, ListBase<T> *base=(ListBase<T> *)0): m_obj(obj),
	m_prev((ListItem<T> *)0), m_next((ListItem<T> *)0), m_base(base) {
		if (m_base) {
			m_next = (ListItem<T> *)0;
			if ((m_prev = m_base->m_tail) != (ListItem<T> *)0)
				m_base->m_tail->m_next = this;
			else
				m_base->m_head = this;
			m_base->m_tail = this;
		}
	}
	ListItem(const ListItem<T> &item) {
		*this = item;
	}
	~ListItem() {
		Remove();
	}

	void Remove() {
		if (m_prev)
			m_prev->m_next = m_next;
		if (m_next)
			m_next->m_prev = m_prev;

		if (m_base) {
			if (m_base->m_head == this)
				m_base->m_head = m_next;
			if (m_base->m_tail == this)
				m_base->m_tail = m_prev;
		}

		m_prev = (ListItem<T> *)0;
		m_next = (ListItem<T> *)0;

		m_base = (ListBase<T> *)0;
	}

	void SetObject(T *obj) {
		m_obj = obj;
	}
	T *Object() const {
		return m_obj;
	}

	const T *operator->() const {
		return m_obj;
	}
	T *operator->() {
		return m_obj;
	}

	ListItem<T> &operator=(const ListItem<T> &item) {
		m_obj = item.m_obj;
		m_prev = item.m_prev;
		m_next = item.m_next;
		m_base = item.m_base;

		if (m_prev)
			m_prev->m_next = this;
		if (m_next)
			m_next->m_prev = this;

		if (m_base) {
			if (m_base->m_head==&item)
				m_base->m_head = this;
			if (m_base->m_tail==&item)
				m_base->m_tail = this;
		}

		return *this;
	}

	ListItem<T> *ItemBefore() { return m_prev; }
	const ListItem<T> *ItemBefore() const { return m_prev; }

	ListItem<T> *ItemAfter() { return m_next; }
	const ListItem<T> *ItemAfter() const { return m_next; }

	T *Before() { return m_prev ? m_prev->m_obj : (T *)0; }
	const T *Before() const { return m_prev ? m_prev->m_obj : (const T *)0; }

	T *After() { return m_next ? m_next->m_obj : (T *)0; }
	const T *After() const { return m_next ? m_next->m_obj : (const T *)0; }
};

template<typename T>
class ListBase {
protected:
	friend class ListItem<T>;

	ListItem<T> *m_head, *m_tail;

public:
	ListBase(): m_head((ListItem<T> *)0), m_tail((ListItem<T> *)0) {
	}
	ListBase(const ListBase<T> &base) {
		*this = base;
	}
	~ListBase() {
		RemoveAll();
	}

	void RemoveAll() {
		while(m_head)
			m_head->Remove();
	}
	void DeleteAll() {
		while(m_head) {
			T *obj = m_head->Object();
			m_head->Remove();
			delete obj;
		}
	}

	ListBase<T> &operator=(const ListBase<T> &base) {
		ListItem<T> *item;

		if (this==&base)
			return *this;

		RemoveAll();

		for(item=base.m_head; item; item=item.m_next)
			item->m_base = this;

		return *this;
	}

	ListItem<T> *FirstItem() { return m_head; }
	const ListItem<T> *FirstItem() const { return m_head; }

	ListItem<T> *LastItem() { return m_tail; }
	const ListItem<T> *LastItem() const { return m_tail; }

	T *First() { return m_head ? m_head->m_obj : (T *)0; }
	const T *First() const { return m_head ? m_head->m_obj : (T *)0; }

	T *Last() { return m_tail ? m_tail->m_obj : (T *)0; }
	const T *Last() const { return m_tail ? m_tail->m_obj : (T *)0; }
};

//==============================================================================
//
//	STRINGS
//
//==============================================================================

template<typename T>
class TString {
protected:
	size_t m_chunkSize;
	size_t m_capacity, m_length;
	T *m_data;

public:
	TString(const T *p=(const T *)0, size_t chunkSize=32):
	m_chunkSize(chunkSize), m_capacity(0), m_length(0), m_data((T *)0) {
		*this = p;
	}
	TString(const TString<T> &s): m_chunkSize(s.m_chunkSize),
	m_capacity(0), m_length(0), m_data((T *)0) {
		*this = s;
	}
	~TString() {
		*this = (const T *)0;
	}

	void Assign(const T *p) {
		if (!p) {
			m_data = mem::Delete(m_data);
			m_length = 0;
			m_capacity = 0;
			return;
		}

		while(p[m_length++]);

		if (++m_length > m_capacity) {
			m_capacity = m_length;
			m_capacity = m_capacity + (m_capacity % m_chunkSize);

			m_data = mem::Renew(m_data, m_capacity);
		}

		mem::Copy(m_data, p, m_length*sizeof(T));
	}
	void Append(const T *p) {
		size_t l;

		if (!p)
			return;

		for(l=0; p[l]; l++);

		if (m_length + l > m_capacity) {
			m_capacity = m_length + l;
			m_capacity = m_capacity + (m_capacity % m_chunkSize);

			m_data = mem::Renew(m_data, m_capacity);
		}

		mem::Copy(&m_data[m_length], p, (++l)*sizeof(T));
		m_length += l;
	}

	size_t Length() const {
		return m_length;
	}
	size_t Capacity() const {
		return m_capacity;
	}

	void SetChunkSize(size_t chunkSize) {
		assert(chunkSize != 0);

		m_chunkSize = chunkSize;
	}
	size_t ChunkSize() const {
		return m_chunkSize;
	}

	T *Data() {
		return m_data;
	}
	const T *Data() const {
		return m_data;
	}

	inline operator T *() {
		return m_data;
	}
	inline operator const T *() const {
		return m_data;
	}

	inline TString<T> &operator=(const TString<T> &s) {
		m_chunkSize = s.m_chunkSize;
		m_length = s.m_length;

		if (!s.m_data) {
			m_capacity = 0;
			m_data = mem::Delete(m_data);
			return *this;
		}

		if (m_length > m_capacity) {
			m_capacity = s.m_capacity;
			m_data = mem::Renew(m_data, m_capacity);
		}

		mem::Copy(m_data, s.m_data, m_length*sizeof(T));
		return *this;
	}
	inline TString<T> &operator=(const T *p) {
		Assign(p);
		return *this;
	}
	inline T &operator[](size_t i) {
		assert(i < m_length);
		assert(m_data != (T *)0);

		return m_data[i];
	}

	inline TString<T> operator+(const T *p) {
		TString<T> r;

		r = m_data;
		r.Append(p);

		return r;
	}
	inline TString<T> &operator+=(const T *p) {
		Append(p);
		return *this;
	}
};
typedef TString<char> CString;
typedef TString<wchar_t> WString;
#if UNICODE || _UNICODE || __UNICODE || __UNICODE__
# define T(x) L#x
typedef WString String;
#else
# define T(x) x
typedef CString String;
#endif

String VA(const char *fmt, ...) {
	va_list args;
	char buf[8192];

	va_start(args, fmt);
	vsnprintf(buf, sizeof(buf) - 1, fmt, args);
	buf[sizeof(buf) - 1] = 0;
	va_end(args);

	return String(buf);
}

//==============================================================================
//
//	TEST
//
//==============================================================================
struct Item {
	int number;

	ListItem<Item> link;
	static ListBase<Item> g_list;

	inline Item(int n=0): number(n), link(this, &g_list) {}
	inline ~Item() { link.Remove(); }

	inline Item &operator=(int n) {
		number = n;
		return *this;
	}

	inline operator int &() { return number; }
	inline operator const int &() const { return number; }

	inline Item *Before() { return link.Before(); }
	inline Item *After() { return link.After(); }
	inline const Item *Before() const { return link.Before(); }
	inline const Item *After() const { return link.After(); }

	static inline Item *First() { return g_list.First(); }
	static inline Item *Last() { return g_list.Last(); }

	static inline void PrintList() {
		const Item *p;

		printf("List:\n");
		for(p=First(); p; p=p->After())
			printf("  %i\n", (int)*p);
		printf("\n");
	}
	static inline void DeleteAll() {
		while(First())
			delete First();
	}
};
ListBase<Item> Item::g_list;

// Copy the sign of 'b' into 'a'
inline int CopySign(int a, int b) {
	int difmsk;

	difmsk = (((a ^ b) & (1<<(sizeof(a)*8 - 1)))>>(sizeof(a)*8 - 1));
	//printf("[difmsk=0x%.8X]", *(unsigned int *)&difmsk);

	return (-a & difmsk) | (a & ~difmsk);

	// absolute
	//a  = (a + (a >> (sizeof(a)*8 - 1))) ^ (a >> (sizeof(a)*8 - 1));
}
void TestCopySign(int a, int b) {
	printf("CopySign(%i, %i) = %i\n", a, b, CopySign(a, b));
}

int main() {
	atexit(Item::DeleteAll);

	printf("%s\n", (const char *)VA("Hello, world! -- %i", 42));

	new Item(23);
	new Item(38);
	new Item(17);
	new Item(42);

	Item::PrintList();

	TestCopySign(42, -1);
	TestCopySign(42, -23);
	TestCopySign(23, 1);
	TestCopySign(23, 47);

	TestCopySign(-42, -1);
	TestCopySign(-42, -23);
	TestCopySign(-23, 1);
	TestCopySign(-23, 47);

	return 0;
}
