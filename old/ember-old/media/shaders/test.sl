/*
 * TEST SHADERLAB SHADER
 * Written by Aaron J. Miller, 2011
 *
 * This is meant to test out the parser
 */

Shader "Test Shader" {

       Properties {
       		  _Range("Some Range", Range(0,1)) = .5
       		  _Color("Some Color", Color) = (0, .25, .50, 1)
		  _Tex2D("Some Tex2D", 2D) = "media/textures/test.png" {}
		  _TexRc("Some TexRc", Rect) = "media/textures/test.png" {}
		  _TexCb("Some TexCb", Cube) = "media/textures/cube.png" {}
		  _Float("Some Float", Float) = 123.45
		  _Vec4D("Some Vec4D", Vector) = (1, 2, 3, 4)
       } /* Properties */

       SubShader {

       		 Tags {
		      "Tag1"="Value1"
		      "Tag2"="Value2"
		      } /* Tags */

       		 Pass "P1" {
		      Tags {
		      	   "Tag1A"="Value1A"
			   "Tag1B"="Value1B"
			   } /* Tags */
		 } /* Pass */

		 Pass "P2" {
		      Tags {
		      	   "Tag2A"="Value2A"
			   } /* Tags */
		 } /* Pass */

       }

       SubShader {

       		 Pass "Main" {
		      Tags { "What?"="Who?!" }
		 } /* Pass */

       } /* SubShader */

       SubShader {

       		 Pass "Test" { }

       } /* SubShader */

} /* Shader */
