﻿#pragma once

#include <stddef.h> // needed for `size_t`

// SUUGAKU_DEBUG_ENABLED
// ---------------------
// Controls whether debug paths in Suugaku should be enabled or disabled.
//
// Define to 1 prior to including this file to force enable debug paths.
// Define to 0 prior to including this file to force disable debug paths.
//
// If this is not defined then a number of common debug preprocessor defines
// will be checked. If any of them are defined then the debug paths will be
// enabled. Otherwise they will be disabled.
#ifndef SUUGAKU_DEBUG_ENABLED
# if defined(DEBUG)||defined(_DEBUG)||defined(__DEBUG__)
#  define SUUGAKU_DEBUG_ENABLED 1
# else
#  define SUUGAKU_DEBUG_ENABLED 0
# endif
#endif

// SUUGAKU_ASSERT
// --------------
// Macro called in some procedures to perform runtime sanity checks. (e.g.,
// verifying that indexes or parameters are within the expected bounds/ranges.)
//
// If SUUGAKU_DEBUG_ENABLED == 0 then all such checks will be disabled, by
// defining SUUGAKU_ASSERT as a blank macro function. Otherwise this will be
// defined to AX_ASSERT, if that is defined, or <assert.h> will be included and
// this will be defined to assert.
//
// You can optionally define SUUGAKU_ASSERT prior to including this file to
// force a specific assert method.
#ifndef SUUGAKU_ASSERT
# if SUUGAKU_DEBUG_ENABLED
#  if defined( AX_ASSERT )
#   define SUUGAKU_ASSERT       AX_ASSERT
#  else
#   include <assert.h>
#   define SUUGAKU_ASSERT       assert
#  endif
# else
#  define SUUGAKU_ASSERT(Expr_) ((void)0)
# endif
#endif

// SUUGAKU_FLOAT64_ENABLED
// -----------------------
// Determines whether the type used to represent a float is 64-bit (double
// precision) or 32-bit (single precision).
//
// Define to 1 prior to including this file to use 64-bit (double).
// Define to 0 prior to including this file to use 32-bit (float). [default]
//
// If left undefined this will default to disabled (32-bit floats).
#ifndef SUUGAKU_FLOAT64_ENABLED
# define SUUGAKU_FLOAT64_ENABLED 0
#endif

// SUUGAKU_PI
// ----------
// The value of pi
#ifndef SUUGAKU_PI
# ifdef _M_PI
#  define SUUGAKU_PI _M_PI // compiler might have tricks for better IEEE-754 representation
# else
#  define SUUGAKU_PI 3.1415926535897932384626433832795028841971693993751058209
# endif
#endif

// SUUGAKU_EPSILON
// ---------------
// Represents the range a value can be in for approximate floating-point
// equality tests.
#ifndef SUUGAKU_EPSILON
# define SUUGAKU_EPSILON 1e-6
#endif

// SUUGAKU_REVERSE_ORDER_ENABLED
// -----------------------------
// Disabled by default. If enabled, swaps rows with columns in the matrix
// classes.
//
// Define to 1 prior to including this file to use row-major storage.
// Define to 0 prior to including this file to use column-major. [default]
//
// This should pretty much never be needed.
#ifndef SUUGAKU_REVERSE_ORDER_ENABLED
# define SUUGAKU_REVERSE_ORDER_ENABLED 0
#endif

// Primary namespace for Suugaku
//
// Note: Suugaku is 数学, which means "mathematics" in Japanese.
namespace suugaku
{

	class Vec2f;
	class Vec3f;
	class Vec4f;
	class Mat3f;
	class Mat4f;

#if SUUGAKU_FLOAT64_ENABLED
	typedef double fp_t;
#else
	typedef float  fp_t;
#endif

	// Convert radians to degrees
	inline fp_t Degrees( fp_t fRadians )
	{
		return fRadians/SUUGAKU_PI*180;
	}
	// Convert degrees to radians
	inline fp_t Radians( fp_t fDegrees )
	{
		return fDegrees/180*SUUGAKU_PI;
	}

	// Invokes cos() with fAngleInDegrees converted to radians
	fp_t Cos( fp_t fAngleInDegrees );
	// Invokes sin() with fAngleInDegrees converted to radians
	fp_t Sin( fp_t fAngleInDegrees );
	// Invokes tan() with fAngleInDegrees converted to radians
	fp_t Tan( fp_t fAngleInDegrees );
	// Invokes sqrt(); this allows us to avoid including <math.h> here
	fp_t Sqrt( fp_t fValue );

	// Restrict a value to the range of -1 to +1
	fp_t ClampSNorm( fp_t fValue );
	// Restrict a value to the range of 0 to 1
	fp_t ClampUNorm( fp_t fValue );

	// Perform a dot-product on two vectors
	fp_t Dot( const Vec2f &A, const Vec2f &B );
	fp_t Dot( const Vec3f &A, const Vec3f &B );
	fp_t Dot( const Vec4f &A, const Vec4f &B );

	// Perform a cross-product on two vectors
	Vec3f Cross( const Vec3f &A, const Vec3f &B );

	// Convert a point that is in local-space (relative to the given object's
	// world-space transform) to global-space
	Vec3f PointLocalToGlobal( const Mat4f &ObjectWorldXf, const Vec3f &LocalPoint );
	// Convert a point that is in global-space to local-space (relative to the
	// given object's world-space transform)
	Vec3f PointGlobalToLocal( const Mat4f &ObjectWorldXf, const Vec3f &GlobalPoint );
	// Convert a vector that is in local-space (relative to the given object's
	// world-space transform) to global-space
	Vec3f VectorLocalToGlobal( const Mat3f &ObjectWorldXf, const Vec3f &LocalDirection );
	Vec3f VectorLocalToGlobal( const Mat4f &ObjectWorldXf, const Vec3f &LocalDirection );
	// Convert a vector that is in global-space to local-space (relative to the
	// given object's world-space transform)
	Vec3f VectorGlobalToLocal( const Mat3f &ObjectWorldXf, const Vec3f &GlobalDirection );
	Vec3f VectorGlobalToLocal( const Mat4f &ObjectWorldXf, const Vec3f &GlobalDirection );

	// Create a vector that represents a distance to have been moved based on a
	// rotation matrix
	Vec3f MoveVector( const Mat3f &ObjectLocalXf, const Vec3f &Distance );
	Vec3f MoveVector( const Mat4f &ObjectLocalXf, const Vec3f &Distance );

	// Create a vector pointing at the `Target` point from the given `Source`
	// point. `Source` and `Target` must be in the same space (e.g., world-
	// space, or the local-space of the same object). The returned direction
	// vector will be in the same space as the inputs.
	Vec3f LookAt( const Vec3f &Source, const Vec3f &Target );

	// 2D vector, or coordinate
	class Vec2f
	{
	public:
		static const unsigned kRows = 1;
		static const unsigned kColumns = 2;
		static const unsigned kDimensions = kRows*kColumns;
		typedef fp_t ElementType;

		fp_t x, y;

		inline Vec2f( fp_t v = 0 )
		: x( v )
		, y( v )
		{
		}
		inline Vec2f( fp_t x, fp_t y )
		: x( x )
		, y( y )
		{
		}
		inline Vec2f( const Vec2f &v )
		: x( v.x )
		, y( v.y )
		{
		}
		explicit Vec2f( const Vec3f &v );
		explicit Vec2f( const Vec4f &v );

		inline Vec2f &operator=( float v )
		{
			x = v;
			y = v;

			return *this;
		}
		inline Vec2f &operator=( const Vec2f &v )
		{
			x = v.x;
			y = v.y;

			return *this;
		}
		Vec2f &operator=( const Vec3f &v );
		Vec2f &operator=( const Vec4f &v );

		inline fp_t *Ptr()
		{
			return reinterpret_cast< fp_t * >( this );
		}
		inline const fp_t *Ptr() const
		{
			return reinterpret_cast< const fp_t * >( this );
		}

		inline size_t Num() const
		{
			return kDimensions;
		}

		inline fp_t &operator[]( size_t i )
		{
			SUUGAKU_ASSERT( i < 2 && "Index out of bounds" );
			return reinterpret_cast< fp_t * >( this )[ i ];
		}
		inline const fp_t &operator[]( size_t i ) const
		{
			SUUGAKU_ASSERT( i < 2 && "Index out of bounds" );
			return reinterpret_cast< const fp_t * >( this )[ i ];
		}

		inline bool operator==( const Vec2f &V ) const
		{
			return
				x > V.x - SUUGAKU_EPSILON && x < V.x + SUUGAKU_EPSILON &&
				y > V.y - SUUGAKU_EPSILON && y < V.y + SUUGAKU_EPSILON;
		}
		inline bool operator!=( const Vec2f &V ) const
		{
			return
				x <= V.x - SUUGAKU_EPSILON || x >= V.x + SUUGAKU_EPSILON ||
				y <= V.y - SUUGAKU_EPSILON || y >= V.y + SUUGAKU_EPSILON;
		}

		inline Vec2f &operator+=( const Vec2f &V )
		{
			x += V.x;
			y += V.y;

			return *this;
		}
		inline Vec2f &operator-=( const Vec2f &V )
		{
			x -= V.x;
			y -= V.y;

			return *this;
		}
		inline Vec2f &operator*=( const Vec2f &V )
		{
			x *= V.x;
			y *= V.y;

			return *this;
		}
		inline Vec2f &operator/=( const Vec2f &V )
		{
			x /= V.x;
			y /= V.y;

			return *this;
		}

		inline Vec2f operator+( const Vec2f &V ) const
		{
			return Vec2f( x + V.x, y + V.y );
		}
		inline Vec2f operator-( const Vec2f &V ) const
		{
			return Vec2f( x - V.x, y - V.y );
		}
		inline Vec2f operator*( const Vec2f &V ) const
		{
			return Vec2f( x*V.x, y*V.y );
		}
		inline Vec2f operator/( const Vec2f &V ) const
		{
			return Vec2f( x/V.x, y/V.y );
		}

		inline fp_t LengthSq() const
		{
			return Dot( *this, *this );
		}
		inline fp_t Length() const
		{
			return Sqrt( LengthSq() );
		}

		inline Vec2f Normalized() const
		{
			const fp_t fInvMag = fp_t(1)/LengthSq();
			return *this*fInvMag;
		}
	};

	// 3D vector, coordinate, or color
	class Vec3f
	{
	public:
		static const unsigned kRows = 1;
		static const unsigned kColumns = 3;
		static const unsigned kDimensions = kRows*kColumns;
		typedef fp_t ElementType;

		fp_t x, y, z;

		inline Vec3f( fp_t v = 0 )
		: x( v )
		, y( v )
		, z( v )
		{
		}
		inline Vec3f( fp_t x, fp_t y, fp_t z = 0 )
		: x( x )
		, y( y )
		, z( z )
		{
		}
		inline Vec3f( const Vec2f &xy, fp_t z = 0 )
		: x( xy.x )
		, y( xy.y )
		, z( z )
		{
		}
		inline Vec3f( fp_t x, const Vec2f &yz )
		: x( x )
		, y( yz.x )
		, z( yz.y )
		{
		}
		inline Vec3f( const Vec3f &v )
		: x( v.x )
		, y( v.y )
		, z( v.z )
		{
		}
		explicit Vec3f( const Vec4f &v );

		inline Vec3f &operator=( fp_t v )
		{
			x = v;
			y = v;
			z = v;

			return *this;
		}
		inline Vec3f &operator=( const Vec2f &v )
		{
			x = v.x;
			y = v.y;
			z = 0.0f;

			return *this;
		}
		inline Vec3f &operator=( const Vec3f &v )
		{
			x = v.x;
			y = v.y;
			z = v.z;

			return *this;
		}
		Vec3f &operator=( const Vec4f &v );

		inline fp_t *Ptr()
		{
			return reinterpret_cast< fp_t * >( this );
		}
		inline const fp_t *Ptr() const
		{
			return reinterpret_cast< const fp_t * >( this );
		}

		inline size_t Num() const
		{
			return kDimensions;
		}
		
		inline fp_t &operator[]( size_t i )
		{
			SUUGAKU_ASSERT( i < 3 && "Index out of bounds" );
			return reinterpret_cast< fp_t * >( this )[ i ];
		}
		inline const fp_t &operator[]( size_t i ) const
		{
			SUUGAKU_ASSERT( i < 3 && "Index out of bounds" );
			return reinterpret_cast< const fp_t * >( this )[ i ];
		}

		inline bool operator==( const Vec3f &V ) const
		{
			return
				x > V.x - SUUGAKU_EPSILON && x < V.x + SUUGAKU_EPSILON &&
				y > V.y - SUUGAKU_EPSILON && y < V.y + SUUGAKU_EPSILON &&
				z > V.z - SUUGAKU_EPSILON && z < V.z + SUUGAKU_EPSILON;
		}
		inline bool operator!=( const Vec3f &V ) const
		{
			return
				x <= V.x - SUUGAKU_EPSILON || x >= V.x + SUUGAKU_EPSILON ||
				y <= V.y - SUUGAKU_EPSILON || y >= V.y + SUUGAKU_EPSILON ||
				z <= V.z - SUUGAKU_EPSILON || z >= V.z + SUUGAKU_EPSILON;
		}

		inline Vec3f &operator+=( const Vec3f &V )
		{
			x += V.x;
			y += V.y;
			z += V.z;

			return *this;
		}
		inline Vec3f &operator-=( const Vec3f &V )
		{
			x -= V.x;
			y -= V.y;
			z -= V.z;

			return *this;
		}
		inline Vec3f &operator*=( const Vec3f &V )
		{
			x *= V.x;
			y *= V.y;
			z *= V.z;

			return *this;
		}
		inline Vec3f &operator/=( const Vec3f &V )
		{
			x /= V.x;
			y /= V.y;
			z /= V.z;

			return *this;
		}

		inline Vec3f operator+( const Vec3f &V ) const
		{
			return Vec3f( x + V.x, y + V.y, z + V.z );
		}
		inline Vec3f operator-( const Vec3f &V ) const
		{
			return Vec3f( x - V.x, y - V.y, z - V.z );
		}
		inline Vec3f operator*( const Vec3f &V ) const
		{
			return Vec3f( x*V.x, y*V.y, z*V.z );
		}
		inline Vec3f operator/( const Vec3f &V ) const
		{
			return Vec3f( x/V.x, y/V.y, z/V.z );
		}

		inline fp_t LengthSq() const
		{
			return Dot( *this, *this );
		}
		inline fp_t Length() const
		{
			return Sqrt( LengthSq() );
		}

		inline Vec3f Normalized() const
		{
			const fp_t fInvMag = fp_t(1)/LengthSq();
			return *this*fInvMag;
		}
	};

	// 4D vector, coordinate, or color
	class Vec4f
	{
	public:
		static const unsigned kRows = 1;
		static const unsigned kColumns = 4;
		static const unsigned kDimensions = kRows*kColumns;
		typedef fp_t ElementType;

		fp_t x, y, z, w;

		inline Vec4f( fp_t v = 0 )
		: x( v )
		, y( v )
		, z( v )
		, w( v )
		{
		}
		inline Vec4f( fp_t x, fp_t y, fp_t z = 0, fp_t w = 0 )
		: x( x )
		, y( y )
		, z( z )
		, w( w )
		{
		}
		inline Vec4f( const Vec2f &xy, fp_t z = 0, fp_t w = 0 )
		: x( xy.x )
		, y( xy.y )
		, z( z )
		, w( w )
		{
		}
		inline Vec4f( fp_t x, const Vec2f &yz, fp_t w = 0 )
		: x( x )
		, y( yz.x )
		, z( yz.y )
		, w( w )
		{
		}
		inline Vec4f( fp_t x, fp_t y, const Vec2f &zw )
		: x( x )
		, y( y )
		, z( zw.x )
		, w( zw.y )
		{
		}
		inline Vec4f( const Vec3f &xyz, fp_t w = 0 )
		: x( xyz.x )
		, y( xyz.y )
		, z( xyz.z )
		, w( w )
		{
		}
		inline Vec4f( fp_t x, const Vec3f &yzw )
		: x( x )
		, y( yzw.x )
		, z( yzw.y )
		, w( yzw.z )
		{
		}
		inline Vec4f( const Vec4f &v )
		: x( v.x )
		, y( v.y )
		, z( v.z )
		, w( v.w )
		{
		}

		inline Vec4f &operator=( fp_t v )
		{
			x = v;
			y = v;
			z = v;
			w = v;

			return *this;
		}
		inline Vec4f &operator=( const Vec2f &v )
		{
			x = v.x;
			y = v.y;
			z = 0;
			w = 0;

			return *this;
		}
		inline Vec4f &operator=( const Vec3f &v )
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = 0;

			return *this;
		}
		inline Vec4f &operator=( const Vec4f &v )
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;

			return *this;
		}

		inline fp_t *Ptr()
		{
			return reinterpret_cast< fp_t * >( this );
		}
		inline const fp_t *Ptr() const
		{
			return reinterpret_cast< const fp_t * >( this );
		}

		inline size_t Num() const
		{
			return kDimensions;
		}

		inline fp_t &operator[]( size_t i )
		{
			SUUGAKU_ASSERT( i < 4 && "Index out of bounds" );
			return reinterpret_cast< fp_t * >( this )[ i ];
		}
		inline const fp_t &operator[]( size_t i ) const
		{
			SUUGAKU_ASSERT( i < 4 && "Index out of bounds" );
			return reinterpret_cast< const fp_t * >( this )[ i ];
		}

		inline bool operator==( const Vec4f &V ) const
		{
			return
				x > V.x - SUUGAKU_EPSILON && x < V.x + SUUGAKU_EPSILON &&
				y > V.y - SUUGAKU_EPSILON && y < V.y + SUUGAKU_EPSILON &&
				z > V.z - SUUGAKU_EPSILON && z < V.z + SUUGAKU_EPSILON &&
				w > V.w - SUUGAKU_EPSILON && w < V.w + SUUGAKU_EPSILON;
		}
		inline bool operator!=( const Vec4f &V ) const
		{
			return
				x <= V.x - SUUGAKU_EPSILON || x >= V.x + SUUGAKU_EPSILON ||
				y <= V.y - SUUGAKU_EPSILON || y >= V.y + SUUGAKU_EPSILON ||
				z <= V.z - SUUGAKU_EPSILON || z >= V.z + SUUGAKU_EPSILON ||
				w <= V.w - SUUGAKU_EPSILON || w >= V.w + SUUGAKU_EPSILON;
		}

		inline Vec4f &operator+=( const Vec4f &V )
		{
			x += V.x;
			y += V.y;
			z += V.z;
			w += V.w;

			return *this;
		}
		inline Vec4f &operator-=( const Vec4f &V )
		{
			x -= V.x;
			y -= V.y;
			z -= V.z;
			w -= V.w;

			return *this;
		}
		inline Vec4f &operator*=( const Vec4f &V )
		{
			x *= V.x;
			y *= V.y;
			z *= V.z;
			w *= V.w;

			return *this;
		}
		inline Vec4f &operator/=( const Vec4f &V )
		{
			x /= V.x;
			y /= V.y;
			z /= V.z;
			w /= V.w;

			return *this;
		}

		inline Vec4f operator+( const Vec4f &V ) const
		{
			return Vec4f( x + V.x, y + V.y, z + V.z, w + V.w );
		}
		inline Vec4f operator-( const Vec4f &V ) const
		{
			return Vec4f( x - V.x, y - V.y, z - V.z, w - V.w );
		}
		inline Vec4f operator*( const Vec4f &V ) const
		{
			return Vec4f( x*V.x, y*V.y, z*V.z, w*V.w );
		}
		inline Vec4f operator/( const Vec4f &V ) const
		{
			return Vec4f( x/V.x, y/V.y, z/V.z, w/V.w );
		}

		inline fp_t LengthSq() const
		{
			return Dot( *this, *this );
		}
		inline fp_t Length() const
		{
			return Sqrt( LengthSq() );
		}

		inline Vec4f Normalized() const
		{
			const fp_t fInvMag = fp_t(1)/LengthSq();
			return *this*fInvMag;
		}
	};
	
	// 3x3 matrix (with column-major memory layout)
	class Mat3f
	{
	public:
		static const unsigned kRows = 3;
		static const unsigned kColumns = 3;
		static const unsigned kDimensions = kRows*kColumns;
		typedef fp_t ElementType;

#if !SUUGAKU_REVERSE_ORDER_ENABLED
		fp_t xx, yx, zx;
		fp_t xy, yy, zy;
		fp_t xz, yz, zz;
#else
		fp_t xx, xy, xz;
		fp_t yx, yy, yz;
		fp_t zx, zy, zz;
#endif

		inline Mat3f()
#if !SUUGAKU_REVERSE_ORDER_ENABLED
		: xx(1), yx(0), zx(0)
		, xy(0), yy(1), zy(0)
		, xz(0), yz(0), zz(1)
#else
		: xx(1), xy(0), xz(0)
		, yx(0), yy(1), yz(0)
		, zx(0), zy(0), zz(1)
#endif
		{
		}
		inline Mat3f( const Vec3f &cx, const Vec3f &cy, const Vec3f &cz )
#if !SUUGAKU_REVERSE_ORDER_ENABLED
		: xx(cx.x), yx(cy.x), zx(cz.x)
		, xy(cx.y), yy(cy.y), zy(cz.y)
		, xz(cx.z), yz(cy.z), zz(cz.z)
#else
		: xx(cx.x), xy(cx.y), xz(cx.z)
		, yx(cy.x), yy(cy.y), yz(cy.z)
		, zx(cz.x), zy(cz.y), zz(cz.z)
#endif
		{
		}
		Mat3f( const Mat4f &M );

		inline Vec3f ColumnX() const { return Vec3f( xx, xy, xz ); }
		inline Vec3f ColumnY() const { return Vec3f( yx, yy, yz ); }
		inline Vec3f ColumnZ() const { return Vec3f( zx, zy, zz ); }

		inline Vec3f RowX() const { return Vec3f( xx, yx, zx ); }
		inline Vec3f RowY() const { return Vec3f( xy, yy, zy ); }
		inline Vec3f RowZ() const { return Vec3f( xz, yz, zz ); }

		Mat3f &LoadIdentity();
		Mat3f &LoadTranspose();
		Mat3f &LoadTranspose( const Mat3f &Other );
		inline Mat3f Transposed() const { return Mat3f().LoadTranspose(*this); }

		Mat3f &LoadRotation( const Vec3f &AnglesDegrees );
		Mat3f &LoadScaling( const Vec3f &Scaling );

		Mat3f &ApplyRotation( const Vec3f &AnglesDegrees );
		Mat3f &ApplyXRotation( fp_t fXAngleDegrees );
		Mat3f &ApplyYRotation( fp_t fYAngleDegrees );
		Mat3f &ApplyZRotation( fp_t fZAngleDegrees );
		Mat3f &ApplyScaling( const Vec3f &Scaling );
		inline Mat3f Rotated( const Vec3f &AnglesDegrees ) const { return Mat3f(*this).ApplyRotation(AnglesDegrees); }
		inline Mat3f XRotated( fp_t fXAngleDegrees ) const { return Mat3f(*this).ApplyXRotation(fXAngleDegrees); }
		inline Mat3f YRotated( fp_t fYAngleDegrees ) const { return Mat3f(*this).ApplyYRotation(fYAngleDegrees); }
		inline Mat3f ZRotated( fp_t fZAngleDegrees ) const { return Mat3f(*this).ApplyZRotation(fZAngleDegrees); }
		inline Mat3f Scaled( const Vec3f &Scaling ) const { return Mat3f(*this).ApplyScaling(Scaling); }

		void Decompose( Vec3f &dstEulerAngles, Vec3f &dstScale ) const;
		inline Vec3f GetEulerAngles() const { Vec3f e, s; Decompose( e, s ); return e; }
		inline Vec3f GetScaling() const { Vec3f e, s; Decompose( e, s ); return s; }

		Mat3f &LoadMultiply( const Mat3f &A, const Mat3f &B );
		inline Mat3f &operator*=( const Mat3f &B ) { return LoadMultiply( Mat3f(*this), B ); }
		inline Mat3f operator*( const Mat3f &B ) const { return Mat3f().LoadMultiply( *this, B ); }

		fp_t Determinant() const;

		inline fp_t *Ptr()
		{
			return reinterpret_cast< fp_t * >( this );
		}
		inline const fp_t *Ptr() const
		{
			return reinterpret_cast< const fp_t * >( this );
		}

		inline size_t Num() const
		{
			return kDimensions;
		}

		inline fp_t &operator[]( size_t i )
		{
			SUUGAKU_ASSERT( i < 9 && "Index out of bounds" );
			return reinterpret_cast< fp_t * >( this )[ i ];
		}
		inline const fp_t &operator[]( size_t i ) const
		{
			SUUGAKU_ASSERT( i < 9 && "Index out of bounds" );
			return reinterpret_cast< const fp_t * >( this )[ i ];
		}
	};
	
	// 4x4 matrix (with column-major memory layout)
	class Mat4f
	{
	public:
		static const unsigned kRows = 4;
		static const unsigned kColumns = 4;
		static const unsigned kDimensions = kRows*kColumns;
		typedef fp_t ElementType;

#if !SUUGAKU_REVERSE_ORDER_ENABLED
		fp_t xx, yx, zx, wx;
		fp_t xy, yy, zy, wy;
		fp_t xz, yz, zz, wz;
		fp_t xw, yw, zw, ww;
#else
		fp_t xx, xy, xz, xw;
		fp_t yx, yy, yz, yw;
		fp_t zx, zy, zz, zw;
		fp_t wx, wy, wz, ww;
#endif

		inline Mat4f()
#if !SUUGAKU_REVERSE_ORDER_ENABLED
		: xx(1), yx(0), zx(0), wx(0)
		, xy(0), yy(1), zy(0), wy(0)
		, xz(0), yz(0), zz(1), wz(0)
		, xw(0), yw(0), zw(0), ww(1)
#else
		: xx(1), xy(0), xz(0), xw(0)
		, yx(0), yy(1), yz(0), yw(0)
		, zx(0), zy(0), zz(1), zw(0)
		, wx(0), wy(0), wz(0), ww(1)
#endif
		{
		}
		inline Mat4f( const Vec4f &cx, const Vec4f &cy, const Vec4f &cz, const Vec4f &cw )
#if !SUUGAKU_REVERSE_ORDER_ENABLED
		: xx(cx.x), yx(cy.x), zx(cz.x), wx(cw.x)
		, xy(cx.y), yy(cy.y), zy(cz.y), wy(cw.y)
		, xz(cx.z), yz(cy.z), zz(cz.z), wz(cw.z)
		, xw(cx.w), yw(cy.w), zw(cz.w), ww(cw.w)
#else
		: xx(cx.x), xy(cx.y), xz(cx.z), xw(cx.w)
		, yx(cy.x), yy(cy.y), yz(cy.z), yw(cy.w)
		, zx(cz.x), zy(cz.y), zz(cz.z), zw(cz.w)
		, wx(cw.x), wy(cw.y), wz(cw.z), ww(cw.w)
#endif
		{
		}
		inline Mat4f( const Mat3f &M )
#if !SUUGAKU_REVERSE_ORDER_ENABLED
		: xx(M.xx), yx(M.yx), zx(M.zx), wx(0)
		, xy(M.xy), yy(M.yy), zy(M.zy), wy(0)
		, xz(M.xz), yz(M.yz), zz(M.zz), wz(0)
		, xw(   0), yw(   0), zw(   0), ww(1)
#else
		: xx(M.xx), xy(M.xy), xz(M.xz), xw(0)
		, yx(M.yx), yy(M.yy), yz(M.yz), yw(0)
		, zx(M.zx), zy(M.zy), zz(M.zz), zw(0)
		, wx(   0), wy(   0), wz(   0), ww(1)
#endif
		{
		}

		inline Vec4f ColumnX() const { return Vec4f( xx, xy, xz, xw ); }
		inline Vec4f ColumnY() const { return Vec4f( yx, yy, yz, yw ); }
		inline Vec4f ColumnZ() const { return Vec4f( zx, zy, zz, zw ); }
		inline Vec4f ColumnW() const { return Vec4f( wx, wy, wz, ww ); }

		inline Vec4f RowX() const { return Vec4f( xx, yx, zx, wx ); }
		inline Vec4f RowY() const { return Vec4f( xy, yy, zy, wy ); }
		inline Vec4f RowZ() const { return Vec4f( xz, yz, zz, wz ); }
		inline Vec4f RowW() const { return Vec4f( xw, yw, zw, ww ); }

		Mat4f &LoadIdentity();
		Mat4f &LoadTranspose();
		Mat4f &LoadTranspose( const Mat4f &Other );
		inline Mat4f Transposed() const { return Mat4f().LoadTranspose(*this); }

		Mat4f &LoadPerspProj( fp_t fFovDeg, fp_t fAspect, fp_t fZNear, fp_t fZFar );
		Mat4f &LoadOrthoProj( fp_t fLeft, fp_t fRight, fp_t fBottom, fp_t fTop, fp_t fZNear, fp_t fZFar );

		bool IsProjPersp() const;
		bool IsProjOrtho() const;
		float GetPerspProjFov() const;
		float GetPerspZNear() const;
		float GetPerspZFar() const;

		Mat4f &LoadTranslation( const Vec3f &Translation );
		Mat4f &LoadRotation( const Vec3f &AnglesDegrees );
		Mat4f &LoadScaling( const Vec3f &Scaling );

		Mat4f &LoadAffine( const Mat3f &Rotation, const Vec3f &Translation );

		Mat4f &SetTranslation( const Vec3f &Translation );

		Mat4f &ApplyTranslation( const Vec3f &Translation );
		Mat4f &ApplyRotation( const Vec3f &AnglesDegrees );
		Mat4f &ApplyXRotation( fp_t fXAngleDegrees );
		Mat4f &ApplyYRotation( fp_t fYAngleDegrees );
		Mat4f &ApplyZRotation( fp_t fZAngleDegrees );
		Mat4f &ApplyScaling( const Vec3f &Scaling );
		inline Mat4f Translated( const Vec3f &Translation ) const { return Mat4f(*this).ApplyTranslation(Translation); }
		inline Mat4f Rotated( const Vec3f &AnglesDegrees ) const { return Mat4f(*this).ApplyRotation(AnglesDegrees); }
		inline Mat4f XRotated( fp_t fXAngleDegrees ) const { return Mat4f(*this).ApplyXRotation(fXAngleDegrees); }
		inline Mat4f YRotated( fp_t fYAngleDegrees ) const { return Mat4f(*this).ApplyYRotation(fYAngleDegrees); }
		inline Mat4f ZRotated( fp_t fZAngleDegrees ) const { return Mat4f(*this).ApplyZRotation(fZAngleDegrees); }
		inline Mat4f Scaled( const Vec3f &Scaling ) const { return Mat4f(*this).ApplyScaling(Scaling); }

		void Decompose( Vec3f &dstEulerAngles, Vec3f &dstScale ) const;
		void Decompose( Vec3f &dstTranslation, Vec3f &dstEulerAngles, Vec3f &dstScale ) const;
		inline Vec3f GetEulerAngles() const { Vec3f e, s; Decompose( e, s ); return e; }
		inline Vec3f GetScaling() const { Vec3f e, s; Decompose( e, s ); return s; }
		inline Vec3f GetTranslation() const { return Vec3f( xw, yw, zw ); }

		Mat4f &LoadAffineInverse( const Mat4f &AffineMatrix );
		Mat4f &LoadAffineMultiply( const Mat4f &AffineA, const Mat4f &AffineB );
		Mat4f &LoadMultiply( const Mat4f &A, const Mat4f &B );
		inline Mat4f AffineInverse() const { return Mat4f().LoadAffineInverse(*this); }
		inline Mat4f &operator*=( const Mat4f &B ) { return LoadMultiply( Mat4f(*this), B ); }
		inline Mat4f operator*( const Mat4f &B ) const { return Mat4f().LoadMultiply( *this, B ); }

		fp_t Determinant() const;

		inline fp_t *Ptr()
		{
			return reinterpret_cast< fp_t * >( this );
		}
		inline const fp_t *Ptr() const
		{
			return reinterpret_cast< const fp_t * >( this );
		}

		inline size_t Num() const
		{
			return kDimensions;
		}

		inline fp_t &operator[]( size_t i )
		{
			SUUGAKU_ASSERT( i < 16 && "Index out of bounds" );
			return reinterpret_cast< fp_t * >( this )[ i ];
		}
		inline const fp_t &operator[]( size_t i ) const
		{
			SUUGAKU_ASSERT( i < 16 && "Index out of bounds" );
			return reinterpret_cast< const fp_t * >( this )[ i ];
		}
	};

	inline Mat3f Transpose( const Mat3f &A ) { return A.Transposed(); }
	inline Mat4f Transpose( const Mat4f &A ) { return A.Transposed(); }

	inline Mat3f RotationMat3( const Vec3f &AnglesDegrees ) { return Mat3f().LoadRotation( AnglesDegrees ); }
	inline Mat3f ScalingMat3( const Vec3f &Scaling ) { return Mat3f().LoadScaling( Scaling ); }

	inline Mat4f TranslationMat4( const Vec3f &Translation ) { return Mat4f().SetTranslation( Translation ); }
	inline Mat4f RotationMat4( const Vec3f &AnglesDegrees ) { return Mat4f().LoadRotation( AnglesDegrees ); }
	inline Mat4f ScalingMat4( const Vec3f &Scaling ) { return Mat4f().LoadScaling( Scaling ); }

	inline Mat3f Multiply( const Mat3f &A, const Mat3f &B ) { return Mat3f().LoadMultiply( A, B ); }

	inline Mat4f AffineMultiply( const Mat4f &A, const Mat4f &B ) { return Mat4f().LoadAffineMultiply( A, B ); }
	inline Mat4f Multiply( const Mat4f &A, const Mat4f &B ) { return Mat4f().LoadMultiply( A, B ); }

	inline Mat4f AffineInverse( const Mat4f &A ) { return Mat4f().LoadAffineInverse( A ); }
	
	//--------------------------------------------------------------------//
	
	inline Vec2f::Vec2f( const Vec3f &v )
	: x( v.x )
	, y( v.y )
	{
	}
	inline Vec2f::Vec2f( const Vec4f &v )
	: x( v.x )
	, y( v.y )
	{
	}
	inline Vec2f &Vec2f::operator=( const Vec3f &v )
	{
		x = v.x;
		y = v.y;

		return *this;
	}
	inline Vec2f &Vec2f::operator=( const Vec4f &v )
	{
		x = v.x;
		y = v.y;

		return *this;
	}
	
	inline Vec3f::Vec3f( const Vec4f &v )
	: x( v.x )
	, y( v.y )
	, z( v.z )
	{
	}
	inline Vec3f &Vec3f::operator=( const Vec4f &v )
	{
		x = v.x;
		y = v.y;
		z = v.z;

		return *this;
	}

	inline Mat3f::Mat3f( const Mat4f &M )
#if !SUUGAKU_REVERSE_ORDER_ENABLED
	: xx(M.xx), yx(M.yx), zx(M.zx)
	, xy(M.xy), yy(M.yy), zy(M.zy)
	, xz(M.xz), yz(M.yz), zz(M.zz)
#else
	: xx(M.xx), xy(M.xy), xz(M.xz)
	, yx(M.yx), yy(M.yy), yz(M.yz)
	, zx(M.zx), zy(M.zy), zz(M.zz)
#endif
	{
	}

}
