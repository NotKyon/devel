#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>

#include <mpir.h> /* don't forget -lmpirxx -lmpir */

#define AX_IMPLEMENTATION
#include "axlib/axlib.hpp"

void test()
{
	mpz_t x;

	mpz_init( x );
	mpz_fac_ui( x, 4000 );
	
	mpz_clear( x );
}

void test2()
{
	mpz_t f3k, f4k;
	mpz_t ft1;
	mpf_t xn,xd,xr, yn,yd,yr;
	
	mpz_init( f3k );
	mpz_init( f4k );
	mpz_init( ft1 );

	mpz_fac_ui( f3k, 3998 );
	mpz_fac_ui( f4k, 4000 );

	mpz_sub_ui( ft1, f4k, 19 );

#define B 8192
	mpf_init2( xn, B ); mpf_init2( yn, B );
	mpf_init2( xd, B ); mpf_init2( yd, B );
	mpf_init2( xr, B ); mpf_init2( yr, B );

	mpf_set_z( xn, f3k ); mpf_set_z( yn, ft1 );
	mpf_set_z( xd, f4k ); mpf_set_z( yd, f4k );

	mpf_div( xr, xn, xd ); mpf_div( yr, yn, yd );

	gmp_printf( "A:%Fg\n", xr );
	gmp_printf( "B:%Fg\n", yr );

	mpf_clear( yr ); mpf_clear( xr );
	mpf_clear( yd ); mpf_clear( xd );
	mpf_clear( yn ); mpf_clear( xn );

	mpz_clear( ft1 );
	mpz_clear( f4k );
	mpz_clear( f3k );
}

int main( int argc, char **argv )
{
	
	((void)argc);
	((void)argv);

	Ax::CTimer t;
	static const size_t n = 4096;
	for( size_t i = 0; i < n; ++i ) {
		volatile int j = 0;
		test();
		j = 0;
	}
	
	const double f = double( t.getElapsedSeconds() );
	printf( "%.2f (avg %.5f)\n", f, f/double( n ) );

	test2();
	
	return EXIT_SUCCESS;
	
}
