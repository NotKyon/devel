#ifndef GB_GAMEBASE_H
#define GB_GAMEBASE_H

#if _MSC_VER > 1000
# pragma once
#endif

/* --special-- */
#include "gamebase/main_pre.h"
/* --special-- */

/* -------------------------------------------------------------------------- */

#include "gamebase/config.h"

#include "gamebase/framework.h"
#include "gamebase/base.h"
#include "gamebase/sys.h"

/* -------------------------------------------------------------------------- */

#if __cplusplus
extern "C" {
#endif

/* --special-- */
#include "gamebase/main_post.h"
/* --special-- */

#if __cplusplus
}
#endif

#endif
