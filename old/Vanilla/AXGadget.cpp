// includes
#include <Axia/Vanilla/Vanilla.h>
#include <Axia/Vanilla/Internal.h>

// AXGadget: globals
//HGLRC										AXGadget::g_hrcGLRef;
AXListBase<AXGadget>						AXGadget::g_gadgetList;
AXListBase<AXGadget>						AXGadget::g_rootList;

// AXGadget: Constructor
AXGadget::AXGadget(int type, AXGadget *pSuper)
: m_type(type), m_titleLength(0), m_tipLength(0), m_pszTitle((char *)0),
  m_pszTip((char *)0), m_bRelease(true), m_pSuper(pSuper),
  m_pSuperList(&g_rootList), m_pData((void *)0)
{
	m_subLink.SetObject(this);
	m_gadgetLink.SetObject(this);

	if (pSuper)
		m_pSuperList = &pSuper->m_subList;

	m_pSuperList->Add(&m_subLink);
	g_gadgetList.Add(&m_gadgetLink);
}

// AXGadget: Destructor
AXGadget::~AXGadget()
{
	// remove all sub-items
	m_subList.DeleteAll();

	// remove from lists
	g_gadgetList.Remove(&m_gadgetLink);
	m_pSuperList->Remove(&m_subLink);
}

// AXGadget: SetTitle: set the title of the window
void AXGadget::SetTitle(const char *pszTitle)
{
}

// AXWidget: SetTitle: set the title of the window
void AXWidget::SetTitle(const char *pszTitle)
{
	::SetWindowTextA(m_hwndRef, pszTitle);
}

// AXGadget: SetTip: set the tip of the window
void AXGadget::SetTip(const char *pszTip)
{
}

// AXGadget: Title: get the title of the window
const char *AXGadget::Title(void) const
{
	return m_pszTitle;
}

// AXGadget: Tip: get the tip of the window
const char *AXGadget::Tip(void) const
{
	return m_pszTip;
}

// AXGadget: SetReleaseOnClose: set whether this handle is deleted on close
void AXGadget::SetReleaseOnClose(bool bReleaseOnClose)
{
	m_bRelease = bReleaseOnClose;
}

// AXGadget: ReleaseOnClose: determine whether this handle is deleted on close
bool AXGadget::ReleaseOnClose(void) const
{
	return m_bRelease;
}

// AXGadget: Parent: get the parent window
AXGadget *AXGadget::Parent(void) const
{
	return m_pSuper;
}

// AXGadget: IsDescendantOf: determine whether this widget is a descendant of a particular super widget
bool AXGadget::IsDescendantOf(AXGadget *pSuper)
{
	// declarations
	AXGadget	*pTest;

	// null is treated as "root"
	if (!pSuper)
		return true;

	// enumerate each super window
	for(pTest=m_pSuper; pTest; pTest=pTest->m_pSuper)
	{
		// check this particular super window against the argument
		if (pTest==pSuper)
			// match: found
			return true;
	}

	// no matches found
	return false;
}

// AXGadget: SetParent: change the parent of this widget
void AXGadget::SetParent(AXGadget *pSuper)
{
	// check whether the new super is null
	if (pSuper != (AXGadget *)0)
	{
		// ensure that the parent isn't attempting to be set to self
		if (pSuper==this)
			// impossible
			return;

		// remove self from the current super's list
		m_pSuperList->Remove(&m_subLink);

		// determine whether the new super is a descendent of this handle;
		// requires special handling
		if (pSuper->IsDescendantOf(this))
			// store this handle's current super
			pSuper->SetParent(m_pSuper);

	}

	// set the new super
	m_pSuper = pSuper;
	m_pSuperList = &((m_pSuper)?m_pSuper->m_subList:g_rootList);
	m_pSuperList->Add(&m_subLink);
}

// AXGadget: AddChild: alternative call to SetSuper
void AXGadget::AddChild(AXGadget *pSub)
{
	// validate the sub widget
	if (!pSub)
		return;

	// set the widget's super to this handle
	pSub->SetParent(this);
}

// AXGadget: FirstChild: retrieve the first child widget
AXGadget *AXGadget::FirstChild(void)
{
	return m_subList.First();
}

// AXGadget: LastChild: retrieve the last child widget
AXGadget *AXGadget::LastChild(void)
{
	return m_subList.Last();
}

// AXGadget: SiblingBefore: retrieve the sibling widget before this handle
AXGadget *AXGadget::SiblingBefore(void)
{
	return m_subLink.Before();
}

// AXGadget: SiblingAfter: retrieve the sibling widget after this handle
AXGadget *AXGadget::SiblingAfter(void)
{
	return m_subLink.After();
}

// AXGadget: HandleBefore: retrieve the widget handle before this one
AXGadget *AXGadget::HandleBefore(void)
{
	return m_gadgetLink.Before();
}

// AXGadget: HandleAfter: retrieve the widget handle after this one
AXGadget *AXGadget::HandleAfter(void)
{
	return m_gadgetLink.After();
}

// AXGadget: FirstRoot: retrieve the first root widget
AXGadget *AXGadget::FirstRoot(void)
{
	return g_rootList.First();
}

// AXGadget: LastRoot: retrieve the last root widget
AXGadget *AXGadget::LastRoot(void)
{
	return g_rootList.Last();
}

// AXGadget: FirstHandle: retrieve the first widget handle
AXGadget *AXGadget::FirstHandle(void)
{
	return g_gadgetList.First();
}

// AXGadget: LastHandle: retrieve the last widget handle
AXGadget *AXGadget::LastHandle(void)
{
	return g_gadgetList.Last();
}

// AXGadget: Show: show the gadget
void AXGadget::Show(void)
{
}

// AXGadget: Hide: hide the gadget
void AXGadget::Hide(void)
{
}

// AXGadget: QueryClientArea: retrieve client area information
void AXGadget::QueryClientArea(int *pLeft, int *pTop, int *pRight, int *pBottom)
{
	if (pLeft)		*pLeft			= 0;
	if (pTop)		*pTop			= 0;
	if (pRight)		*pRight			= 0;
	if (pBottom)	*pBottom		= 0;
}

// AXGadget: Reflow: reflow the client area
void AXGadget::Reflow(void)
{
	// declarations
	AXGadget			*pGadget;
	AXWidget			*pWidget;
						// client rectangle of this gadget (the host)
	int					hostLeft, hostTop, hostRight, hostBottom;
						// rectangle of the sub-gadget within this gadget
	int					fromLeft, fromTop, fromRight, fromBottom;
						// size of the sub-gadget
	int					fromWidth, fromHeight;
						// rectangle of where the sub-gadget SHOULD be within this gadget
	int					toLeft, toTop, toRight, toBottom;
						// layout of the sub-widget
	int					layout;

	// retrieve the host rectangle (this gadget's client area)
	QueryClientArea(&hostLeft, &hostTop, &hostRight, &hostBottom);

	// tab gadgets need special handling
	if (m_type==AXTabberGadgetType)
	{
		for(pGadget=m_subList.First(); pGadget; pGadget=pGadget->m_subLink.After())
			pGadget->Reflow();

		return;
	}

	// enumerate each sub-gadget
	for(pGadget=m_subList.First(); pGadget; pGadget=pGadget->m_subLink.After())
	{
		// skip this sub-gadget if it is not a widget
		if (!GadgetIsWidget(pGadget))
			continue;

		// store the sub-widget handle and layout (convenience)
		pWidget			= reinterpret_cast<AXWidget *>(pGadget);
		layout			= pWidget->m_layout;

		// retrieve the "from" rectangle (where the sub-widget is within this)
		fromLeft		= pWidget->m_rcClient.left;
		fromTop			= pWidget->m_rcClient.top;
		fromRight		= pWidget->m_rcClient.right+fromLeft;
		fromBottom		= pWidget->m_rcClient.bottom+fromTop;
		fromWidth		= pWidget->m_rcClient.right;
		fromHeight		= pWidget->m_rcClient.bottom;

		// fix horizontal alignment
		switch(layout&(AXAlignLeft|AXAlignRight))
		{
			// no alignment
			case 0:
				toRight = fromRight;
				toLeft = fromLeft;
				break;

			// left aligned
			case AXAlignLeft:
				toLeft = hostLeft+pWidget->m_distance[0];
				toRight = toLeft+fromWidth;
				break;

			// right aligned
			case AXAlignRight:
				toRight = hostRight-pWidget->m_distance[2];
				toLeft = toRight-fromWidth;
				break;

			// left+right aligned
			default:
				toLeft = hostLeft+pWidget->m_distance[0];
				toRight = hostRight-pWidget->m_distance[2];

				if (layout&AXKeepWidth)
				{
					toLeft = (toLeft + (toRight-toLeft)/2) - fromWidth/2;
					toRight = toLeft + fromWidth;
				}
				break;
		}

		// fix vertical alignment
		switch(layout&(AXAlignTop|AXAlignBottom))
		{
			// no alignment
			case 0:
				toBottom = fromBottom;
				toTop = fromTop;
				break;

			// top aligned
			case AXAlignTop:
				toTop = hostTop+pWidget->m_distance[1];
				toBottom = toTop+fromHeight;
				break;

			// bottom aligned
			case AXAlignBottom:
				toBottom = hostBottom-pWidget->m_distance[3];
				toTop = toBottom-fromHeight;
				break;

			// top+bottom aligned
			default:
				toTop = hostTop+pWidget->m_distance[1];
				toBottom = hostBottom-pWidget->m_distance[3];

				if (layout&AXKeepHeight)
				{
					toTop = (toTop + (toBottom-toTop)/2) - fromHeight/2;
					toBottom = toTop + fromHeight;
				}
				break;
		}

		// apply the new position
		if (toLeft!=fromLeft || toTop!=fromTop)
			pWidget->SetPosition(toLeft, toTop);

		// apply the new size
		if (toRight!=fromRight || toBottom!=fromBottom)
			pWidget->SetSize(toRight-toLeft, toBottom-toTop);
	}
}
